{
  "quoteId" : "",
  "accountNumber" : ban_frkey,
  "msisdn" : msisdn_frkey,
  "totalDue" : 216.40,
  "items" : [ ],
  "paymentMethod" : {
    "code" : "Check",
    "store" : true,
    "bankAccount": {
		        "accountNumber":"aeebbc9fa1f36b7dcf69d22cb9d12f09fa27d7a44ea9ceaad668031a1272d8eff12f776aad836908d093e3e1851b3336a1894feb10b4ca112f0de53f4a3f54b408dd2fe4e23f4fe8dcf5d44364a1f6ffee8ee4a2123c454aeba7800833a700f92826131c69db05c23187e39afefbb10278870ec5f6f468799d9320ad61932ee3f9a603081e8e564cf249d2b603d0c67eb58eb230935cb960113ef919633f88bfe6c9d0246c7cc11bdad1010b895845a7aba4c0715127224108de139855fe8a9196747f0e2bdf73fd7835effa70dc127aecd1124669f7ec9ed45e39607d93ea76b4ade15d0e7d47ae25661b2500f6e09917152775ba3d1a8211d01fcfb99b59b1",
		         "routingNumber": "021000021",
		        "billingAddress": {
		         
		            "line1": "er",
		            "city" : "New york",
		            "countryCode": "US",
		            "state" : "New york",
		            "zip": "98021"
		         
		        },
		        "accountHolderName" : "Santosh Tondupally"
		        
		      },
    "addType" : "Add",
    "creditCards" : [ ],
    "bankAccounts" : [ ],
    "debitCards" : [ ]
  },
  "payment" : { },
  "primaryPhoneNumber" : "",
  "primaryEmailAddress" : "",
  "alternateEmailAddress" : email_frkey,
  "preferredLanguage" : "English",
  "taxAmount" : 0.0,
  "creditClass" : "C",
  "sedonaCustomer" : false,
  "accountHolderName" : "gopi manohar",
  "channelId" : "Desktop",
  "transactionType" : "MANAGEPAYMENTARRANGEMENT",
  "printReceiptIndicator" : true,
  "pushToBillIndicator" : false,
  "paymentArrangementCollection" : {
    "paymentArrangements" : [ {
      "paymentArrangementId" : "20",
      "paymentArrangementType" : "R",
      "actionCode" : paactionCode_frkey,
      "paymentArrangementCategory" : "82",
      "plannedPayments" : [ {
        "plannedPaymentId" : "1",
        "plannedPaymentAmount" : 43.28,
        "plannedPaymentDate" : "2019-05-14"
      },
      {
        "plannedPaymentId" : "2",
        "plannedPaymentAmount" : 173.12,
        "plannedPaymentDate" : "2019-05-28"
      }]
    } ]
  },
  "onetimecharge" : [ ],
  "notes" : [ ]
}