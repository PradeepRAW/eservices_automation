{
  "quoteId" : cartId_frkey,
  "accounts" : [ {
    "accountNumber" : ban_frkey,
    "status" : "OPERATIONAL",
    "accountType" : "INDIVIDUAL",
    "accountSubType" : "INDIVIDUAL_REGULAR",
    "marketCode" : "NYN",
    "isPuertoRicoIndicator" : "false",
    "bills" : [ {
      "billCycleDay" : "20"
    } ],
    "tenure" : 778,
    "contactFirstName" : "UAT",
    "contactFamilyName" : "Test"
  } ],
  "customerProfile" : {
    "classificationCode" : "5",
    "creditRiskProfileId" : "9",
    "emailCommunications" : [ {
      "emailAddress" : email_frkey
    } ],
    "phoneNumbers" : [ {
      "phoneNumber" : msisdn_frkey
    } ]
  },
  "lines" : [ {
    "eipIndicator" : "true",
    "phoneNumber" : msisdn_frkey,
    "lineId" : "lineId"
  } ]
}