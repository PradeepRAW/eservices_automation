{
  "quoteId": cartId_frkey,
  "orderId": orderId_frkey,
  "customerProfile": {
    "phoneNumbers": [
      {
        "phoneNumber": msisdn_frkey 
      }
    ],
    "emailCommunications": [
      {
        "emailAddress": email_frkey
      }
    ]
  },
  "shippingOptions": [
    {
      "shipmentDetailId":shipmentDetailId_frkey,
      "shippingOptionId":shippingOptionId_frkey
    }
  ],
  "eipIndicator": false,
  "estimateEip": false
}