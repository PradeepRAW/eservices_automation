{
 "quoteId": quoteId_frkey,
 "orderId": orderId_frkey,
 "customerProfile": {
   "phoneNumbers": [
     {
       "phoneNumber": msisdn_frkey
     }
   ],
   "emailCommunications": [
     {
       "emailAddress": "tester@testing.com"
     }
   ]
 },
 "shippingOptions": [
   {
     "shipmentDetailId": shipmentDetailId_frkey,
     "shippingOptionId": shippingOptionId_frkey
     
 
   }
 ]
}