{
    "shippingOptions" : [ {
    "shippingOptionId" : shippingOptionId_frkey,
    "shipmentDetailId" : shipmentDetailId_frkey
  } ],
  "customerProfile" : {
    "emailCommunications" : [ {
      "emailAddress" : email_frkey
    } ]
  },
  "orderId" : orderId_frkey,
  "quoteId" : cartId_frkey,
  "eipIndicator" : false
}