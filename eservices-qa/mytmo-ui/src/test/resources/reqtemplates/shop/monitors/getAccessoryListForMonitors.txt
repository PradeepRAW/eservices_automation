{
  "filterByFields" : [ {
    "fieldName" : "category",
    "fieldValues" : [ "ACCESSORIES" ]
  } ],
  "sortByFields" : [ {
    "fieldName" : "pricing.offerPrice",
    "sortOrder" : "ASC"
  } ],
  "deviceId" : deviceId_frkey,
  "familyId" : familyId_frkey,
  "crpList" : [ "8" ],
  "page" : 1,
  "size" : 30
}