{
  "lines" : [ {
    "ban" : ban_frkey,
    "msisdn" : msisdn_frkey,
    "items" : {
      "tradeIn" : {
        "quoteId" : quoteId_frkey,
        "deviceSku" : "",
        "deviceName" : deviceName_frkey,
        "deviceIMEI" : deviceIMEI_frkey,
        "deviceCarrier" : deviceCarrier_frkey,
        "deviceMake" : deviceMake_frkey,
        "deviceModel" : deviceName_frkey,
        "disposition" : "ACCEPTED DEFERRED",
        "tradeInType" : "STANDARD",
        "deviceCondition" : "GOOD",
        "tradeInPrice" : quotePrice_frkey,
        "excludeFairMarketValue" : true
      }
    }
  } ],
  "opCode" : "UPDATE"
}