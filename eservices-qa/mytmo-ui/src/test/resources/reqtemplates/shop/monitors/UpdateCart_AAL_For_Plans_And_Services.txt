{
    "opCode": "UPDATE",
    "lines": [
        {
            "ban": ban_frkey,
            "msisdn": msisdn_frkey,
            "subscriberName": "Test",
            "items": {
				"plans": [
                    {
                        "planFamilyId": "TMO1HANDSET",
                        "soc": "FRLTDATA"
                    }
                ],
                "services": [
                    {
                        "marketCode": "ALN",
                        "socCode": "P3605",
                        "action": {
                            "actionCode": "ADD",
                            "itemLevel": "LINE"
                        }
                    },
                    {
                        "marketCode": "ALN",
                        "socCode": "PHPBNDL",
                        "action": {
                            "actionCode": "REMOVE",
                            "itemLevel": "LINE"
                        }
                    }
                ]
            }
        }
    ]
}