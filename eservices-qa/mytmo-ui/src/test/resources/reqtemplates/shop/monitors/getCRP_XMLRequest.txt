<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sdo="http://retail.tmobile.com/sdo" xmlns:ws="http://www.services.t-mobile.com/ws">
   <SOAP-ENV:Header/>
   <SOAP-ENV:Body>
      <ws:queryCreditCheck>
         <sdo:creditCheckRequest serviceTransactionId="73949d90-7f4f-4dfd-a374-11c15145d58d">
             <sdo:header rspServiceVersion="1.20.1">
               <sdo:partnerId>MYTMO</sdo:partnerId>
               <sdo:partnerTransactionId>523d8c64-e436-4d06-8944-8b8a4677adf6</sdo:partnerTransactionId>
               <sdo:partnerTimestamp>2016-09-14T10:58:18</sdo:partnerTimestamp>
               <sdo:channel>WEB</sdo:channel>
               <sdo:storeId>NA</sdo:storeId>
               <sdo:dealerCode>0000000</sdo:dealerCode>
               <sdo:activityId>queryCreditCheck</sdo:activityId>
            </sdo:header>
            <sdo:ban>943489373</sdo:ban>
            <sdo:retrieveCreditRiskProfile>true</sdo:retrieveCreditRiskProfile>
         </sdo:creditCheckRequest>
      </ws:queryCreditCheck>
   </SOAP-ENV:Body>
</SOAP-ENV:Envelope>