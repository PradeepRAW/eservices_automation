{
  "cartMetadata" : {
    "cartId" : cartId_frkey
  },
  "cartPriceSummary" : {
    "type" : "FULL",
    "payNowAmount" : {
      "noOfMonths" : 0,
      "currency" : "USD",
      "display" : "display",
      "amount" : fullRetailPrice_frkey
    },
    "monthlyAmount" : {
      "currency" : "USD",
      "display" : "display",
      "amount" : 0,
      "noOfMonths" : 0
    }
  },
  "promotions" : [ {
    "promoType" : "auto_pay",
    "discountValue" : 0
  } ],
  "lines" : [ {
    "ban" : ban_frkey,
    "msisdn" : msisdn_frkey,
    "subscriberName" : "UAT",
     "linePriceSummary" : {
      "type" : "FULL",
      "payNowAmount" : {
        "noOfMonths" : 0,
        "currency" : "USD",
        "display" : "display",
        "amount" : fullRetailPrice_frkey
      },
      "monthlyAmount" : {
        "currency" : "USD",
        "display" : "display",
        "amount" : 0,
        "noOfMonths" : 0
      }
    },
    "items" : {
      "device" : {
        "deviceAvailability" : {
         "availabilityStatus" : availabilityStatus_frkey,
          "estimatedShipDateTo" : estimatedShipDateTo_frkey,
          "estimatedShipDateFrom" : estimatedShipDateFrom_frkey
        },
        "fullRetailPrice" : {
          "unit" : "USD",
          "value" : fullRetailPrice_frkey
        },
        "sku" : sku_frkey,
        "color" : color_frkey,
        "colorSwatch" : colorSwatch_frkey,
        "description" : description_frkey,
        "memory" : memory_frkey,
        "memoryUom" : memoryUom_frkey,
        "imageUrl" : imageUrl_frkey,
        "familyId" : familyId_frkey,
        "modelName" : modelName_frkey,
        "pricingOptions" : [ {
          "type" : "FULL",
          "payNowAmount" : {
            "noOfMonths" : 0,
            "currency" : "USD",
            "display" : "display",
            "amount" : fullRetailPrice_frkey
          },
          "monthlyAmount" : {
            "currency" : "USD",
            "display" : "display",
            "amount" : 0,
            "noOfMonths" : 0
          }
        } ]
      },      
      "services" : [ {
        "socCode" : socCode_frkey,
        "serviceName" : socName_frkey,
        "socIndicator" : socIndicator_frkey,
        "pricing" : {
          "type" : "FULL",
          "monthlyAmount" : {
            "amount" : socPrice_frkey,
            "currency" : "USD",
            "display" : "display",
            "noOfMonths" : "0"
          }
        },
        "action" : {
          "actionCode" : "ADD",
          "itemLevel" : "LINE"
        }
      } ]
    }
  } ],
  "opCode" : "UPDATE"
}