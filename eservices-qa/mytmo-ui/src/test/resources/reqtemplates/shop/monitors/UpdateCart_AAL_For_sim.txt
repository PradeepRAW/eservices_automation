{
    "opCode": "UPDATE",
    "lines": [
        {
            "ban": ban_frkey,
            "msisdn": msisdn_frkey,
            "subscriberName": "Test",
            "items": {
                "simStarterKit": {
                    "sku": sku_frkey,
                    "pricingOptions": [
                        {
                            "type": "FULL"
                        }
                    ]
                }
            }
        }
    ]
}