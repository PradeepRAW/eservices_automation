{
  "transactionMetadata" : {
    "transactionId" : correlationid_frkey,
    "sourceSystem" : "web",
    "requestDescription" : "Request description",
    "userId" : "User Id"
  },
  "lines" : [ {
    "ban" : ban_frkey,
    "msisdn" : msisdn_frkey,
    "subscriberName" : "UAT",
    "linePriceSummary" : {
      "type" : "EIP",
      "payNowAmount" : {
        "noOfMonths" : 0,
        "currency" : "USD",
        "display" : "display",
        "amount" : payNowAmount_frkey
      },
      "monthlyAmount" : {
        "currency" : "USD",
        "display" : "display",
    	"amount" : monthlyAmount_frkey,
    	 "noOfMonths" : monthlynoOfMonths_frkey
      }
    },
    "items" : {
      "device" : {
        "sku" : sku_frkey,
        "pricingOptions" : [ {
          "type" : "EIP",
          "payNowAmount" : {
            "noOfMonths" : 0,
            "currency" : "USD",
            "display" : "display",
            "amount" : payNowAmount_frkey
          },
          "monthlyAmount" : {
            "currency" : "USD",
            "display" : "display",
            "amount" : monthlyAmount_frkey,
            "noOfMonths" : monthlynoOfMonths_frkey
          }
        } ]
      }
    }
  } ]
}