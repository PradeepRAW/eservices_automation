{
	"cartPriceSummary": {
		"payNowAmount": {
			"amount": 800,
			"currency": "USD",
			"display": "$800.00"
		},
		"monthlyAmount": {
			"amount": 84,
			"currency": "USD",
			"display": "$84.00"
		}
	},
	"lines": [{
		"ban": ban_frkey,
		"msisdn": msisdn_frkey,
		"subscriberName": "test",
		"linePriceSummary": {
			"type": "EIP",
			"payNowAmount": {
				"amount": 800,
				"currency": "USD",
				"noOfMonths": "0",
				"display": "$800.00"
			},
			"monthlyAmount": {
				"amount": 84,
				"currency": "USD",
				"noOfMonths": "0",
				"display": "$84.00"
			}
		},
		"items": {
			"device": {
				"sku": sku_frkey,
				"pricingOptions": [{
					"type": "EIP"
				}]
			}
		}
	}]
}