{
  "transactionMetadata" : {
    "transactionId" : correlationid_frkey,
    "sourceSystem" : "web",
    "requestDescription" : "Request description",
    "userId" : "User Id"
  },
  "cartPriceSummary" : {
    "type" : "EIP",
    "payNowAmount" : {
      "noOfMonths" : 0,
      "currency" : "USD",
      "display" : "display",
      "amount" : payNowAmount_frkey
    },
    "monthlyAmount" : {
      "currency" : "USD",
      "display" : "display",
      "amount" :monthlyAmount_frkey,
      "noOfMonths" : monthlynoOfMonths_frkey
    }
  },
  "lines" : [ {
    "ban" : ban_frkey,
    "msisdn" : msisdn_frkey,
    "subscriberName" : "UAT",
    "linePriceSummary" : {
      "type" : "EIP",
      "payNowAmount" : {
        "noOfMonths" : 0,
        "currency" : "USD",
        "display" : "display",
        "amount" : payNowAmount_frkey
      },
      "monthlyAmount" : {
        "currency" : "USD",
        "display" : "display",
    	"amount" : monthlyAmount_frkey,
    	 "noOfMonths" : monthlynoOfMonths_frkey
      }
    },
    "items" : {
      "device" : {
        "deviceAvailability" : {
          "availabilityStatus" : availabilityStatus_frkey,
          "estimatedShipDateTo" : estimatedShipDateTo_frkey,
          "estimatedShipDateFrom" : estimatedShipDateFrom_frkey
        },
        "fullRetailPrice" : {
          "unit" : "USD",
          "value" : fullRetailPrice_frkey
        },
        "sku" : sku_frkey,
        "color" : color_frkey,
        "colorSwatch" : colorSwatch_frkey,
        "description" : description_frkey,
        "memory" : memory_frkey,
        "memoryUom" : memoryUom_frkey,
        "imageUrl" : imageUrl_frkey,
        "familyId" : familyId_frkey,
        "modelName" : modelName_frkey,
        "pricingOptions" : [ {
          "type" : "EIP",
          "payNowAmount" : {
            "noOfMonths" : 0,
            "currency" : "USD",
            "display" : "display",
            "amount" : payNowAmount_frkey
          },
          "monthlyAmount" : {
            "currency" : "USD",
            "display" : "display",
            "amount" : monthlyAmount_frkey,
            "noOfMonths" : monthlynoOfMonths_frkey
          }
        } ]
      }
    }
  } ]
}