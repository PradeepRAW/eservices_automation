{
    "opCode": "UPDATE",
    "lines": [
        {
           "ban": ban_frkey,
            "msisdn": msisdn_frkey,
            "subscriberName": "Test",
            "items": {
                "services": [
                    {
                        "marketCode": "ALN",
                        "socCode": "P3601",
                        "action": {
                            "actionCode": "ADD",
                            "itemLevel": "LINE"
                        }
                    },
                    {
                        "marketCode": "ALN",
                        "socCode": "PHPBNDL",
                        "action": {
                            "actionCode": "REMOVE",
                            "itemLevel": "LINE"
                        }
                    }
                ]
            }
        }
    ]
}