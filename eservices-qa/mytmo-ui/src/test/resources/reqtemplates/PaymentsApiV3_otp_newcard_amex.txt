{
    "quoteId": "",
    "accountNumber": "271039177",
    "msisdn": "6825827080",
    "items": [],
    "paymentMethod": {
        "code": "Credit",
        "creditCard": {
            "cardNumber": "4efeb21d0929d9f157752ed8a54b25b232cc0352f1c8972e969fafc9a803239f09ac99d7e07db153cdcb0f87b661091ade90076c364d9ffac05dc24d21ef5999edf2fe68f2cac73ae4d53cb756739917467d08ad581616d0874d6b9ccd6f0e4af17be12a99005322b1b5862ec287ab74f4a3248db531b3242283a78eb2988b572b74e57ab53c922fb65932394a50cd303148f68dec2259bec9f79bd4e9fa767727794766af4054027a9b39a35c8197e2bbb9ba5efd8e85d742cfcb2544c11116624dd06bfe6323415b598621ba12d63fcb2efd968e63915226dae440e02ad95a05e2511b31937c32b10d5d6c2d2ff3e6c65e2f2f1142d4325d73f5399df7922d",
            "cardHolderName": "Santosh Tondupally",
            "cardHolderLastName": null,
            "expirationMonthYear": "0726",
            "type": "AMEX",
            "billingAddress": {
                "zip": "98052"
            },
            "cvv": "6238"
        },
        "addType": "Add",
        "creditCards": [],
        "bankAccounts": [],
        "debitCards": []
    },
    "payment": {},
    "salesInfo": {
        "salesChannel": "WEB",
        "originalDealerCode": "1114144"
    },
    "custProfileDetails": {
        "preferredLanguage": "English",
        "primaryPhoneNumber": "7573003339",
        "primaryEmailAddress": "7573003339@gmail.com",
        "alternateEmailAddress": "",
        "alternatePhoneNumber": ""
    },
    "isSave": true,
    "taxAmount": 0,
    "chargeAmount": 1.14,
    "creditClass": "",
    "creationTime": "1547058600000",
    "onetimecharge": [],
    "dated": "Current",
    "paymentType": "New",
    "device": "Desktop",
    "notes": [],
    "isSedona": false
}