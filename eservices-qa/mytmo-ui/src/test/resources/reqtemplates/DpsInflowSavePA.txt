{
    "totalDue": 508,
    "transactionType": "MANAGEPAYMENTARRANGEMENT",
    "msisdn": msisdn_frkey,
    "accountNumber": ban_frkey,
    "sedonaCustomer": "true",
    "preferredLanguage": "English",
    "accountHolderName": "Gopi nawaz",
    "primaryEmailAddress": "4706852889pro@yopmail.com",
    "primaryPhoneNumber": msisdn_frkey,
    "alternateEmailAddress": "4706852889pro@yopmail.com",
    "creditClass": "1",
    "channelId": "Desktop",
    "quoteId": "10000257612",
    "taxAmount": 0,
    "printReceiptIndicator": true,
    "pushToBillIndicator": false,
    "payment": {
        "spmToBeDeleted": null
    },
    "paymentMethod": {
        "bladeNumber": "****5067",
        "creditCard": {
            "cardNumber": "68add9ab55be34f30bd38de04d783536dad2e37c912f37d37822b96ba336870f49e8d494ffcc237fafb8e884112f1d412fcf1fd90cd99987dae58c62f28d22805c1e2bc7fab3b4738266448e608f197d785ceb53de8d5818ffe569158cf7369271e8316208aa0fc37664a6daa5bed141a52d1e4c8e1dce23d4e52d2864d16dae5cef2a71ef5d9673fd7b86d0a21686f9b19fcec9467234e17f6497331f6d0ffde21646aa772aa7ce9c82a1606603b0d0f4a60c0b630035f27b3247c46baa2892cd7bd6d40c0d1143b6b7df71b35acc3659155a260945aa1d0acebe3039124f427f53e7dc353d21d11426d076e8c32e9dcf2faa7a019f30aba97ee84818b4e3ad",
            "cardHolderName": "test",
            "type": "VISA",
            "cvv": "362",
            "expirationMonthYear": "0627",
            "billingAddress": {
                "zip": "98052",
                "line1": "",
                "line2": "",
                "city": "",
                "state": ""
            }
        },
        "code": "Credit",
        "addType": "Add",
        "store": true
    },
    "isSecurePa": true,
    "paymentArrangementCollection": {
        "paymentArrangements": [
            {
                "paymentArrangementType": "R",
                "paymentArrangementCategory": "74",
                "actionCode": "INSERT",
                "paymentArrangementId": 1,
                "plannedPayments": [
                    {
                        "plannedPaymentId": 1,
                        "plannedPaymentAmount": 508,
                        "plannedPaymentDate": "2019-05-31"
                    }
                ]
            }
        ]
    }
}