{
  "code": "CREDIT",
  "accountNumber" : ban_frkey,
  "paymentInstrumentList": [
    {
      "creditCard": {
        "status": "VALID",
        "cardHolderName": "T mobile User",
         "cardAlias": "4000023163003343",
        "cardNumber": "4f5becc8dfd390b6845d1c9a73d7fadea393e1ce1e625f099bc601adf1344c5573cacca50f5e7e658c8f3295465ed6037403db966a06c6345865ce1dc3fab49b9868757f7b936c5c1edfc4cb779934c6a7e187a5ec29039a424d6334ad545eed66c64c4aa9afb41d27347e87e5eabc7a4539cea6312d6136a3bc9ccfb2c94547229031e9311462b11450a60a7a4124461722d6db750ab5a197961bd389b321bd0e00aee5f5000138b8357ddde44f82d63bc5903d0bf37171c4fa6d54f505c43b2e4e4462e081c59fee0dd47dbf7ce26f87c784c6bfd53e33ce426284e471d03ce95bda010ffb142637aa30ce968449c22bdfb811e60397859927cabdfd4dba24",
        "expirationMonthYear": "0826",
        "cvv":"628",
        "type": "VISA",
        "billingAddress": {
           "line1": "11950 sw garden pl",
          "line2": "gateway",
          "city": "Portland",
          "state": "OR",
          "zip": "98052",
          "countryCode": "US",
          "stateCode": "OR"
        }
      }
    }
  ]
}