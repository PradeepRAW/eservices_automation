package com.tmobile.eservices.qa.applitools;


import java.time.LocalDateTime;

import org.testng.annotations.Test;

import com.applitools.eyes.BatchInfo;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class TmngPages extends TmngCommonLib {
	ApplitoolsLib applitoolsLib = new ApplitoolsLib();
	BatchInfo batchInfo = new BatchInfo("accounts " +  LocalDateTime.now());
	/***
	 * Test Visual testing for Phones Page
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"tmng" })
	public void testHomePage(ControlTestData data, TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "HomePage", batchInfo);
	}

	/***
	 * Test Visual testing for Phones Page
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"tmng"})
	public void testPhonesPage(ControlTestData data, TMNGData tMNGData) {
		navigateToPhonesPlpPage(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "PhonesPage",batchInfo);
	}

	/***
	 * Test Visual testing for Phones Page
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "tmng" })
	public void testMainDevicePLP(ControlTestData data, TMNGData tMNGData) {
		navigateToPhonesPlpPage(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "MainDevicePLP",batchInfo);
	}

	/***
	 * Test Visual testing for Main Device PDP
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "tmng" })
	public void testMainDevicePDP(ControlTestData data, TMNGData tMNGData) {
		navigateToPhonesPDP(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "MainDevicePDP",batchInfo);
	}

	/***
	 * Test Visual testing for Accessory PLP
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "tmng" })
	public void testAccessoryMainPLP(ControlTestData data, TMNGData tMNGData) {
		navigateToAccessoryPLP(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "AccessoryPLPPage",batchInfo);
	}


	/***
	 * Test Visual testing for Cart Page
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "tmng" })
	public void testCartPage(ControlTestData data, TMNGData tMNGData) {
		navigateToCartPageBySelectingPhone(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "CartPage",batchInfo);
	}

	/***
	 * Test Visual testing for Check Out Page
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "tmng" })
	public void testCheckoutPage(ControlTestData data, TMNGData tMNGData) {
		navigateToCheckoutWithPhone(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "Checkoutpage",batchInfo);
	}

	/***
	 * Test Visual testing for BringYourOwnPhonePage
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testBringYourOwnPhonePage(ControlTestData data, TMNGData tMNGData) {
		navigateToBringYourOwnPhonePage(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "BringYourOwnPhonePage",batchInfo);
	}

	/***
	 * Test Visual testing for Deals Page
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testDealsPage(ControlTestData data, TMNGData tMNGData) {
		navigateToDealsPage(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "DealsPage",batchInfo);
	}

	/***
	 * Test Visual testing for testDigitsPage
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testDigitsPage(ControlTestData data, TMNGData tMNGData) {
		navigateToDigitsPage(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "DigitsPage",batchInfo);
	}

	/***
	 * Test Visual testing for International Calling Page
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testInternationalCallingPage(ControlTestData data, TMNGData tMNGData) {
		navigateToInternationalCallingPage(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "InternationalCallingPage",batchInfo);
	}

	/***
	 * Test Visual testing for Plans Page
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testPlansPage(ControlTestData data, TMNGData tMNGData) {
		navigateToPlansPage(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "PlansPage",batchInfo);
	}

	/***
	 * Test Visual testing for SmartDevicesPage
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testSwitchToTmobilePage(ControlTestData data, TMNGData tMNGData) {
		navigateToSwitchToTmobilePage(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "SwitchToTmobilePage",batchInfo);
	}

	/***
	 * Test Visual testing for SmartDevicesPage
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testTravelAbroadPage(ControlTestData data, TMNGData tMNGData) {
		navigateToTravelAbroadPage(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "TravelAbroadPage",batchInfo);
	}


	/***
	 * Test Visual testing for SmartDevicesPage
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testUnlimitedPlansPage(ControlTestData data, TMNGData tMNGData) {
		navigateToUnlimitedPlansPage(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "UnlimitedPlansPage",batchInfo);
	}


	/***
	 * Test Visual testing for SmartDevicesPage
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testWatchsandTabletsPage(ControlTestData data, TMNGData tMNGData) {
		navigateToTabletsAndDevicesPLP(tMNGData);
		applitoolsLib.doVisualTest(getDriver(),"tmng", "WatchandTabletsPage",batchInfo);
	}
}
