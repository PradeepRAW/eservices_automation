package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApiV3;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class AccountsApiV3Test extends AccountsApiV3 {
	public Map<String, String> tokenMap;

	@Test(dataProvider = "byColumnName", enabled = true, priority = 0, groups = { "AccountsApiV3Tests","testValidatePaymentMethodCard", Group.PAYMENTS,Group.SPRINT })
	public void testValidatePaymentMethodCard(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: EOS-Accounts validate card payment method");
		Reporter.log("Data Conditions:MyTmo registered msdsin");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: ValidatePaymentMethod card type");
		Reporter.log("Step 2: Verify card validation details returned-valid in response");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "validatePaymentMethodCard";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		
		//apiTestData.setCardNumber("4539055385244096");
		
		//generate encrypted card number
		String encyptedCard = getEncryptedCardNumber(apiTestData,tokenMap);
		tokenMap.put("cardNumber", encyptedCard);
		
		String requestBody = new ServiceTest().getRequestFromFile("validateCreditCardPaymentMethodV3.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		
		Response response = validatePaymentMethod(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "status"), "VALID", "Card Validation Failed");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

}