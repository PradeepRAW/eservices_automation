package com.tmobile.eservices.qa.shop.functional;


import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import org.testng.annotations.Test;

public class UnlockShopMonitorsData extends ShopCommonLib {

	/*@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeAddSocsEIPFlow(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB03(apiTestData);
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void AALPhoneOnEIPNoPDPSOC(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB03(apiTestData);
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void AAL_FRP_PDP_NY_WithDeposit(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB03(apiTestData);
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void AAL_EIP_PDP_WithSecurityDeposit(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB03(apiTestData);
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeFRPFlow(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB03(apiTestData);
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeEIPFlow(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB03(apiTestData);
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeAddAccessoriesEIPFlow(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB03(apiTestData);
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeTradeInEIPFlow(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB03(apiTestData);
	}*/
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeAddSocsEIPFlowQlab03(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB03(apiTestData);
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void AALPhoneOnEIPNoPDPSOCQlab03(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB03(apiTestData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeAddSocsEIPFlow(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB01(apiTestData);

	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void AALPhoneOnEIPNoPDPSOC(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB01(apiTestData);

	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void AAL_FRP_PDP_NY_WithDeposit(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB01(apiTestData);

	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void AAL_EIP_PDP_WithSecurityDeposit(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB01(apiTestData);
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeEIPFlow(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB01(apiTestData);

	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeFRPFlow(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB01(apiTestData);

	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeAddAccessoriesEIPFlow(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB01(apiTestData);

	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeTradeInEIPFlow(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB01(apiTestData);

	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void AALPhoneOnEIPNoPDPSOCQlab02(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB02(apiTestData);

	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void AAL_FRP_PDP_NY_WithDepositQlab02(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB02(apiTestData);

	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void AAL_EIP_PDP_WithSecurityDepositQlab02(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB02(apiTestData);
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeEIPFlowQlab02(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB02(apiTestData);

	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeFRPFlowQlab02(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB02(apiTestData);

	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeAddAccessoriesEIPFlowQlab02(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB02(apiTestData);

	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = "unlock")
	public void StandardUpgradeAddSocsEIPFlowQlab02(ApiTestData apiTestData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlockMsisdnQLAB02(apiTestData);

	}
	
}

