/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AddBankPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayLandingPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class AddBankPageTest extends PaymentCommonLib {

	/**
	 * US262236 Angular + GLUE - PCM Add Bank (L3) - UI Story US283008 GLUE Light
	 * Reskin - PCM Add Bank Spoke Page US262236 Angular + GLUE - PCM Add Bank (L3)
	 * - UI Story
	 * 
	 * @param data      Data conditions: Account should have past due
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testGLRPCMAddBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262236	Angular + GLUE - PCM Add Bank (L3) - UI Story");
		Reporter.log("Data Conditions - Account should should be eligible for bank payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill | One time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: Click on payment method blade | Payment method spoke page should be displayed");
		Reporter.log("Step 6: Click on Add Bank | Add Bank page should be displayed");
		Reporter.log("Step 7: Verify all elements are present on Add Bank page | All elements are displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddBankPage addBankPage = navigateToAddBankPage(myTmoData);
		addBankPage.verifyAddBankAllElements();
		addBankPage.verifySavePaymentMethodCheckBox();
	}

	/**
	 * US267098 Angular + GLUE - PCM Add Bank (L3) - Modal
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular user. Eligible for bank payments
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testAngularGLUEPCMAddBankModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US267098	Angular + GLUE - PCM Add Bank (L3) - Modal");
		Reporter.log("Data Conditions - Regular user. Eligible for bank payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill | One time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: Click on payment method blade | Payment method page should be displayed");
		Reporter.log("Step 6: Click on Add Bank | Add Bank page should be displayed");
		Reporter.log("Step 7: Verify tool tip for Routing number and Account Number | Tool Tips should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddBankPage addBankPage = navigateToAddBankPage(myTmoData);
		addBankPage.verifyBankInfoToolTips();
	}

	/**
	 * US267097 Angular + GLUE - PCM Add Bank (L3) - Validation
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular user. Eligible for bank payments
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testAngularGLUEPCMAddBankValidation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US267097	Angular + GLUE - PCM Add Bank (L3) - Validation");
		Reporter.log("Data Conditions - Regular user. Eligible for bank payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill | One time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: Click on payment method blade | Payment method page should be displayed");
		Reporter.log("Step 6: Click on Add Bank | Add Bank page should be displayed");
		Reporter.log("Step 7: Verify all bank validations | Bank validations should be working good");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddBankPage addBankPage = navigateToAddBankPage(myTmoData);
		addBankPage.validateAddBankAllFields(PaymentConstants.Add_bank_Header);
	}

	/**
	 * 
	 * Verify that Save Payment Method option is present for PAH/Full account
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testAddBankTreatmentforSPMOptionPAH(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Verify that Save Payment Method option is present for PAH/Full account");
		Reporter.log("Data Conditions - PAH/FULL account. Eligible for bank payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on pay bill  | One time payment page should be displayed");
		Reporter.log("Step 4: Click on Add Bank | Add Bank spoke page should be displayed");
		Reporter.log("Step 5: Verify save payment checkbox is displayed| Save payment checkbox should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AddBankPage addBankPage = navigateToAddBankPage(myTmoData);
		addBankPage.verifySavePaymentMethodCheckBox();
	}

	/**
	 * 
	 * Verify that Save Payment Method option is NOT present for Standard/Restricted
	 * account
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testAddBankTreatmentforSPMOptionStandard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Verify that Save Payment Method option is present for non-PAH/Full account");
		Reporter.log("Data Conditions - Standard/Restricted account. Eligible for bank payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on pay bill  | One time payment page should be displayed");
		Reporter.log("Step 4: Click on Add Bank | Add bank page should be displayed");
		Reporter.log(
				"Step 5: Verify save payment checkbox is not displayed| Save payment checkbox should not be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AddBankPage addBankPage = navigateToAddBankPage(myTmoData);
		addBankPage.verifyNoSavePaymentMethodCheckBox();
	}

	/**
	 * US280693 Angular + GLUE - PCM Negative File / Expired Bank
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testAngularGLUEPCMNegativeFileBankAccount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US280693	Angular + GLUE - PCM Negative File / Expired Bank");
		Reporter.log("Data Conditions - Standard/Restricted account. Not eligible for bank payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 6: verify negative file bank account | Error message should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyInvalidBankErrorMsg();
	}

	/**
	 * US355996: UI- ADD Bank validation
	 * 
	 * Container
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAddBankValidation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US355996: UI- ADD Bank validation");
		Reporter.log("Data Conditions - PAH or FULL user with invalid Routing number ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill button | One time payment page should be displayed");
		Reporter.log("Step 4: Click on add payment method  | Paymnet spoke page should be displayed");
		Reporter.log(
				"Step 5: Enter Invalid Routing number | Error message for invalid routing number should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

	}

	/**
	 * US444943:Angular 6 PCM- Add Bank
	 *
	 * 
	 * Container
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAddBankSpokePageWithStoredBankAccount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US444943-Angular 6 PCM- Add Bank");
		Reporter.log("Data Condition | Any PAH /Standard User with stored bank account ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on Add Bank blade| Add Bank Spoke Page should be displayed");
		Reporter.log("Step 7: verify 'Bank information' Header| 'Bank information' Header should be displayed");
		Reporter.log(
				"Step 8: verify Field Label text 'Name on Account' ,Routing Number ,Account Number| Field Label text for all Fields  should be displayed");
		Reporter.log("Step 9: verify 'Continue' button| 'Continue' button should be displayed");
		Reporter.log("Step 10: verify 'Cancel' button| 'Cancel' button should be displayed");
		Reporter.log(
				"Step 11: click on  'How to find my routing or account number' hyper  link under account number field | 'Find my routing or account number'  modal should be displayed");
		Reporter.log("Step 12: verify  'Save Payment Method' toggle| 'Save Payment Method' toggle should be displayed");
		Reporter.log(
				"Step 13: verify toggle unchecked  by default when having stored bank account  | toggle should be unchecked  by default when having stored bank account ");
		Reporter.log(
				"Step 14: select 'Save Payment Method' toggle   | 'You can only have 1 saved checking account on file. Proceed if you want to replace your existing stored checking account with a new checking account.'  text should be displayed");
		Reporter.log(
				"Step 15: Mouse Hover on  'Save Payment Method' tooltip   | 'Save account (optional). Allow T-Mobile to store your payment method for future use by you and verified users to make a payment.'  tooltip text should be displayed");
		Reporter.log(
				"Step 16: Enter vaild details and click on continue button |  Payment Method blade should be displayed ");
		Reporter.log(
				"Step 17: Verify stored bank account is replaced with new bank account and selected by default|Stored bank account should be replaced with new bank account and selected by default");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddBankPage addBankPage = navigateToNewAddBankPage(myTmoData);
		SpokePage spokePage = new SpokePage(getDriver());
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		addBankPage.verifyAddbankPageElements(PaymentConstants.SAVE_PAYMENT_ERROR_MSG, PaymentConstants.Tool_Tip_Msg);
		addBankPage.fillBankInfo(myTmoData.getPayment());
		addBankPage.clickContinueAddBank();
		addBankPage.verifyReplaceModelHeader();
		addBankPage.clickReplaceModelContinueBtn();
		oneTimePaymentPage.verifyPaymentMethodBlade();
		String storedPaymentNumber = oneTimePaymentPage.getStoredPaymentMethodNumber();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(myTmoData.getPayment().getAccountNumber());
		oneTimePaymentPage.clickPaymentMethodBlade();
		spokePage.verifyPageLoaded();
		spokePage.verifyDefaultSelectionofCardorBankInspokePage(storedPaymentNumber);

	}

	/**
	 * US444943:Angular 6 PCM- Add Bank
	 * 
	 * Container
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAddBankSpokePageWithoutStoredBankAccount(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US444943-Angular 6 PCM- Add Bank");
		Reporter.log("Data Condition | Any PAH /Standard User without stored bank account ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on Add Bank blade| Add Bank Spoke Page should be displayed");
		Reporter.log("Step 7: verify 'Bank information' Header| 'Bank information' Header should be displayed");
		Reporter.log(
				"Step 8: verify Field Label text 'Name on Account' ,Routing Number ,Account Number| Field Label text for all Fields  should be displayed");
		Reporter.log("Step 9: verify  'Save Payment Method' toggle| 'Save Payment Method' toggle should be displayed");
		Reporter.log(
				"Step 10: verify toggle checked  by default when not having stored bank account  | toggle should be checked  by default when not having stored bank account ");
		Reporter.log("Actual Result:");

		AddBankPage addBankPage = navigateToNewAddBankPage(myTmoData);
		addBankPage.verifyAddBanklabels();
		addBankPage.verifySavePaymentMethodCheckBox();
		addBankPage.verifySavePaymentMethodCheckBoxIsSelected();
	}

	/**
	 * US483099: Angular 6 PCM- [Unfinished] Angular 6 PCM- In session payment
	 * method logic
	 * 
	 * Container
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testInSessionLogicForBankPayments(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US444943-Angular 6 PCM- Add Bank");
		Reporter.log("Data Condition | Any PAH /Standard User without stored bank account ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on In session Bank blade| Add Bank Spoke Page should be displayed");
		Reporter.log("Step 7: verify 'Bank information' Header| 'Bank information' Header should be displayed");
		Reporter.log(
				"Step 8: verify in session details 'Name on Account' ,Routing Number ,Account Number| the in session details should be displayed");
		Reporter.log(
				"step 9: Enter the new account number and click on continue | Should be able to enter the new account no. and able to click on continue ");
		Reporter.log(
				"Step 10: Verify new OTP Page loaded with the updated bank account no. | Should be able to see the updated account no.");

		Reporter.log("Actual Result:");
		AddBankPage addBankPage = navigateToNewAddBankPage(myTmoData);
		addBankPage.fillBankInfo(myTmoData.getPayment());
		Long accNumber = addBankPage.clickContinueAddBank();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.clickInSessionBlade();
		addBankPage.verifyInSessionSavedPaymentDetails(accNumber, myTmoData.getPayment());
		addBankPage.enterAccountNumber();
		accNumber = addBankPage.clickContinueAddBank();
		oneTimePaymentPage.verifyNewOTPPageLoaded();
		oneTimePaymentPage.verifySavedBankAccount(accNumber.toString());
	}

	/**
	 * 
	 * DE217271 MyTMO [Payments] NO message about deleting/replacing the payment
	 * method on OTP confirmation (pitch) page
	 * 
	 * Container
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testReplacementAlertforStoredBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condition | Any PAH /Standard User with stored bank account ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on Add Bank blade| Add Bank Spoke Page should be displayed");
		Reporter.log("Step 7: verify 'Bank information' Header| 'Bank information' Header should be displayed");
		Reporter.log(
				"Step 8: select 'Save Payment Method' toggle   | 'You can only have 1 saved checking account on file. Proceed if you want to replace your existing stored checking account with a new checking account.'  text should be displayed");
		Reporter.log(
				"Step 9: verify replacement alert for stored bank|  replacement alert for stored bank should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddBankPage addBankPage = navigateToAddBankPage(myTmoData);
		addBankPage.selectSavePaymentOptionCheckBox();
		addBankPage.verifySaveThisPaymentErrorMessage(
				"You can only have 1 saved checking account on file. Proceed if you want to replace your existing stored checking account with a new checking account");

	}

	/**
	 * US571469: 6.0 Add Bank page-Default toggle and nickname field.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testNickNameFieldForAddBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condition | Any PAH /Standard/Full/Restricted User with Valid AddBank account ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to AutoPay Page | AutoPay page should be displayed");
		Reporter.log("Step 4: click on  payment method blade | Payment collection Page should be displayed");
		Reporter.log("Step 5: click on Add Bank blade| Add Bank Page should be displayed");
		Reporter.log("Step 6: verify 'NickName' Field| 'Nick Name'Field should be displayed");
		Reporter.log("Step 7: verify 'NickName' Field Header| 'Nick Name'Field Header should be displayed");
		Reporter.log("Step 8: verify 'NickName' Field toggle| 'Nick Name'Field toggle should be displayed");
		Reporter.log("Step 9: verify 'NickName' Field Surpressed| 'Nick Name'Field  should be surpressed");
		Reporter.log("Step 10: Select 'Save Payment Method'| 'Save Payment Method' should selected");
		Reporter.log("Step 11: verify 'NickName' Field Enabled| 'Nick Name'Field  should be Enabled");
		Reporter.log(
				"Step 12: Enter Bank Info along with NickName Field| Bank info should be appear in all the fileds ");
		Reporter.log("Step 13: Click on 'Continue' Button| 'Continue' Button is clicked successfully");
		Reporter.log("Step 14: Verify Payment Collection Screen|  Payment Collection screen Should be displayed");
		Reporter.log(
				"Step 15: Verify 'NickName' is updated in Payment Collections Page   | 'NickName' should displayed in added bank");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		if (System.getProperty("environment").contains("qata03")) {
			OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
			oneTimePaymentPage.clickPaymentMethodBlade();
		} else {
			NewAutopayLandingPage newAutopayPage = navigateToNewAutoPayLandingPage(myTmoData);
			newAutopayPage.clickAddPaymentmethod();
		}
		/*
		 * //SpokePage paymentsCollectionPage =
		 * navigateToPaymentsCollectionPage(myTmoData); SpokePage spokePage =
		 * navigateToSpokePage(myTmoData); //SpokePage paymentsCollectionPage =
		 * navigateToPaymentsCollectionPage(myTmoData);
		 * getDriver().get(System.getProperty("environment") +
		 * "/payments/paymentblade"); OneTimePaymentPage ontimepaymentPage = new
		 * OneTimePaymentPage(getDriver()); ontimepaymentPage.clickPaymentMethodBlade();
		 */
		SpokePage paymentsCollectionPage = new SpokePage(getDriver());
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.verifyNewPageUrl();
		paymentsCollectionPage.clickAddBank();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		addBankPage.verifyNoNickNameandDefaultCheckBoxDisplay();
		addBankPage.selectSavePaymentOptionCheckBox();
		addBankPage.verifyNickNameHeader();
		addBankPage.verifyNickNameDisplay();
		addBankPage.verifyDefaultCheckBox();
		addBankPage.verifyDefaultCheckBoxLabel();
		addBankPage.fillBankInfo(myTmoData.getPayment());
		addBankPage.ClickDefaultCheckBox();
		addBankPage.clickContinueAddBank();
		/*
		 * paymentsCollectionPage.verifyNewPageLoaded();
		 * paymentsCollectionPage.verifySavedPaymentMethodNickName(myTmoData.getNickName
		 * ());
		 * paymentsCollectionPage.ClickSavedPaymentMethodNickName(myTmoData.getNickName(
		 * )); System.out.println(RandomString.make(15).toString());
		 * addBankPage.enterNickName("sample123"); addBankPage.clickContinueAddBank();
		 * paymentsCollectionPage.verifySavedPaymentMethodNickName("sample123");
		 */

	}

	/**
	 * CDCDWG2-319 [PCM] Negative file Bank Account alert is missing on PCM Add Bank
	 * page
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular user. Eligible for bank payments
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testNegativeFileBankAlertValidationInAddBankPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-319	[PCM] Negative file Bank Account alert is missing on PCM Add Bank page");
		Reporter.log("Data Conditions - Regular user. Eligible for bank payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill | One time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: Click on payment method blade | Payment method page should be displayed");
		Reporter.log("Step 6: Click on Add Bank | Add Bank page should be displayed");
		Reporter.log("Step 7: Verify Negative File Bank Alert | Negative file Bank Alert should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddBankPage addBankPage = navigateToAddBankPage(myTmoData);
		addBankPage.validateAddBankAllFields(PaymentConstants.Add_bank_Header);
	}
}