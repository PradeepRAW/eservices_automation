package com.tmobile.eservices.qa.payments.api;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.api.EOSCommonLib;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.payments.api.EOSActivitiesV3;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetRefundInformation{
	
	
	
	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test
	public void getRefundinformation() throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		String query = "$.[*].ban";
		List<String> bans = com.jayway.jsonpath.JsonPath.parse(new URL(Constants.TEST_DATA_FILTER_LOCATION_PROD)).read(query);
		Set<String> setbans = new HashSet<>(bans);
		
		LocalDate a = LocalDate.now();	
		String fromdate="2016-03-10";
		String todate=a.toString();
		String urole="PAH";
		EOSCommonLib ecl = new EOSCommonLib();
		String auth=ecl.getaccesscode();
		EOSActivitiesV3 activity=new EOSActivitiesV3();
		 Map<String, Object[]> data = new TreeMap<String, Object[]>();
		 int i=1;
		 data.put(Integer.toString(i), new Object[] {"Ban", "Line", "PaymentID", "CardNumber", "PaymentDate", "PaymentAmount", "Comments"});
		
		for(String ban:setbans) {
		 
			String[]getjwt= {"",auth,auth,ban,urole};
		    Response Accountsummaryresponse=activity.getResponseAccountsummary(getjwt,fromdate,todate);
		
		  
		  if (Accountsummaryresponse.body().asString() != null && Accountsummaryresponse.getStatusCode() == 200) {
				JsonPath jsonPathEvaluator=Accountsummaryresponse.jsonPath();
				 if(jsonPathEvaluator.get("accountActivity.findAll{it.eventType=='PaymentPosted'}.confirmationNumber")!=null) {
				
					 List<String> confnumbers=jsonPathEvaluator.get("accountActivity.findAll{it.eventType=='PaymentPosted'}.confirmationNumber");
					 for(String confnum:confnumbers) {
						 if(jsonPathEvaluator.get("accountActivity.findAll{it.confirmationNumber=='"+confnum+"'}[0].paymentDetails")!=null) {
						 String paymentMethod=jsonPathEvaluator.get("accountActivity.findAll{it.confirmationNumber=='"+confnum+"'}[0].paymentDetails.paymentMethod").toString();
						 if(!paymentMethod.trim().equalsIgnoreCase("CHECK")) {
							 String cardnum="";
							 String paymentDate="";
							 String paymentAmount="";
							 String line="";
							 if(jsonPathEvaluator.get("accountActivity.findAll{it.confirmationNumber=='"+confnum+"'}[0].paymentDetails.cardNumber")!=null) {
							 cardnum=jsonPathEvaluator.get("accountActivity.findAll{it.confirmationNumber=='"+confnum+"'}[0].paymentDetails.cardNumber").toString();}
							 if(jsonPathEvaluator.get("accountActivity.findAll{it.confirmationNumber=='"+confnum+"'}[0].paymentDetails.paymentDate")!=null) {
							 paymentDate=jsonPathEvaluator.get("accountActivity.findAll{it.confirmationNumber=='"+confnum+"'}[0].paymentDetails.paymentDate").toString();}
							 if(jsonPathEvaluator.get("accountActivity.findAll{it.confirmationNumber=='"+confnum+"'}[0].paymentDetails.paymentAmount")!=null) {
							 paymentAmount=jsonPathEvaluator.get("accountActivity.findAll{it.confirmationNumber=='"+confnum+"'}[0].paymentDetails.paymentAmount").toString();}
							 if(jsonPathEvaluator.get("accountActivity.findAll{it.confirmationNumber=='"+confnum+"'}[0].line")!=null) {
							 line=jsonPathEvaluator.get("accountActivity.findAll{it.confirmationNumber=='"+confnum+"'}[0].line").toString();}
							 i++;
							 data.put(Integer.toString(i), new Object[] {ban, line, confnum, cardnum, paymentDate, paymentAmount, ""});
							
						     }
						 
						 }
					 }
						 
				 }
				
		  
		  }else { i++;data.put(Integer.toString(i), new Object[] {ban, "", "", "", "", "", "500-servernot found"});}
		  
		  
		}
		writetoExcel(data);
}
	

	
public void writetoExcel(Map<String, Object[]> data) {
	
	

	        //Blank workbook
		XSSFWorkbook workbook = new XSSFWorkbook();
	         
	        //Create a blank sheet
	        XSSFSheet sheet = workbook.createSheet("Refund data");
	          
	        //This data needs to be written (Object[])
	     
	          
	        //Iterate over data and write to sheet
	        Set<String> keyset = data.keySet();
	        int rownum = 0;
	        for (String key : keyset)
	        {
	            Row row = sheet.createRow(rownum++);
	            Object [] objArr = data.get(key);
	            int cellnum = 0;
	            for (Object obj : objArr)
	            {
	               Cell cell = row.createCell(cellnum++);
	               if(obj instanceof String)
	                    cell.setCellValue((String)obj);
	                else if(obj instanceof Integer)
	                    cell.setCellValue((Integer)obj);
	            }
	        }
	        try
	        {
	          String file1=System.getProperty("user.dir") + "/src/test/resources/testdata/refund.xlsx";
	        	//Write the workbook in file system
	            FileOutputStream out = new FileOutputStream(new File(file1));
	            workbook.write(out);
	            out.close();
	            System.out.println("howtodoinjava_demo.xlsx written successfully on disk.");
	            workbook.close();
	        }
	        catch (Exception e)
	        {
	            e.printStackTrace();
	        }
	
}
	
}