package com.tmobile.eservices.qa.monitors;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class ShopUiMonitors extends ShopCommonLib {

	/**
	 * Standard Upgrade With FRP Flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void testStandardUpgradeFRPFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Standard upgrade flow with FRP");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
		Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
		Reporter.log("4. Click on the device on Device PLP page |Device PDP page should be displayed ");
		Reporter.log(
				"5. Select FRP from payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select eligible line on LS page | Line Selector Details page should be displayed");
		Reporter.log("7. Click on 'Skip trade-in' CTA | PHP page should be displayed");
		Reporter.log("8. Click on Continue button on PHP page | Accessory PLP page should be displayed");
		Reporter.log("9. Click 'OK' on modal and 'Skip Accessories' link | Cart page should be displayed");
		Reporter.log("10. Click 'Continue to shipping' CTA | Shipping tab should be displayed");
		Reporter.log("11. Click 'Continue to payment' CTA | Payment tab page should be displayed");
		Reporter.log("12. Click 'Accept and Continue' CTA | Order Confirmation page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		//cartPage.clickAcceptAndPlaceOrder();
	}

	/**
	 * Standard Upgrade With EIP Flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void testStandardUpgradeEIPFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Standard upgrade flow with FRP");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
		Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
		Reporter.log("4. Click on the device on Device PLP page |Device PDP page should be displayed ");
		Reporter.log(
				"5. Select Monthly from payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select eligible line on LS page | Line Selector Details page should be displayed");
		Reporter.log("7. Click on 'Skip trade-in' CTA | PHP page should be displayed");
		Reporter.log("8. Click on Continue button on PHP page | Accessory PLP page should be displayed");
		Reporter.log("9. Click 'OK' on modal and 'Skip Accessories' link | Cart page should be displayed");
		Reporter.log("10. Click 'Continue to shipping' CTA | Shipping tab should be displayed");
		Reporter.log("11. Click 'Continue to payment' CTA | Payment tab page should be displayed");
		Reporter.log("12. Click 'Accept and Continue' CTA | Order Confirmation page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		//cartPage.clickAcceptAndPlaceOrder();
	}

	/**
	 * Trade-In With EIP Flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void testTradeInEIPFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Standard upgrade flow with FRP");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
		Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
		Reporter.log("4. Click on the device on Device PLP page |Device PDP page should be displayed ");
		Reporter.log(
				"5. Select Monthly from payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select eligible line on LS page | Line Selector Details page should be displayed");
		Reporter.log("7. Click on 'Trade in this phone' CTA | Trade In Device Condition page should be displayed");
		Reporter.log("8. Click on 'Its in good condition' CTA | Trade In Device Value page should be displayed");
		Reporter.log("9. Click on Continue CTA | PHP page should be displayed");
		Reporter.log("10. Click on Continue button on PHP page | Accessory PLP page should be displayed");
		Reporter.log("11. Click 'OK' on modal and 'Skip Accessories' link | Cart page should be displayed");
		Reporter.log("12. Click 'Continue to shipping' CTA | Shipping tab should be displayed");
		Reporter.log("13. Click 'Continue to payment' CTA | Payment tab page should be displayed");
		Reporter.log("14. Click 'Accept and Continue' CTA | Order Confirmation page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByFeaturedDevicesWithTradeInFlow(myTmoData);
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		//cartPage.clickAcceptAndPlaceOrder();
	}

	/**
	 * Trade-In With FRP Flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void testTradeInFRPFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Standard upgrade flow with FRP");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
		Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
		Reporter.log("4. Click on the device on Device PLP page |Device PDP page should be displayed ");
		Reporter.log("5. Select full retail price and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select eligible line on LS page | Line Selector Details page should be displayed");
		Reporter.log("7. Click on 'Trade in this phone' CTA | Trade In Device Condition page should be displayed");
		Reporter.log("8. Click on 'Its in good condition' CTA | Trade In Device Value page should be displayed");
		Reporter.log("9. Click on Continue CTA | PHP page should be displayed");
		Reporter.log("10. Click on Continue button on PHP page | Accessory PLP page should be displayed");
		Reporter.log("11. Click 'OK' on modal and 'Skip Accessories' link | Cart page should be displayed");
		Reporter.log("12. Click 'Continue to shipping' CTA | Shipping tab should be displayed");
		Reporter.log("13. Click 'Continue to payment' CTA | Payment tab page should be displayed");
		Reporter.log("14. Click 'Accept and Continue' CTA | Order Confirmation page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByFeaturedDevicesWithTradeInFlow(myTmoData);
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		//cartPage.clickAcceptAndPlaceOrder();
	}

	/**
	 * Standard Upgrade EIP Flow By Adding Accessories
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void testStandardUpgradeEIPFlowWithAccessories(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Standard upgrade flow with EIP & Accessories");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
		Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
		Reporter.log("4. Click on the device on Device PLP page |Device PDP page should be displayed ");
		Reporter.log(
				"5. Select Monthly from payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select eligible line on LS page | Line Selector Details page should be displayed");
		Reporter.log("7. Click on 'Skip trade-in' CTA | PHP page should be displayed");
		Reporter.log("8. Click on Continue button on PHP page | Accessory PLP page should be displayed");
		Reporter.log("9. Click 'OK' on modal | Accessory PLP page should be displayed");
		Reporter.log("10. Select few accessories and click Continue CTA | Cart page should be displayed");
		Reporter.log("11. Click 'Continue to shipping' CTA | Shipping tab should be displayed");
		Reporter.log("12. Click 'Continue to payment' CTA | Payment tab page should be displayed");
		Reporter.log("13. Click 'Accept and Continue' CTA | Order Confirmation page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoryPLPPage accessoryPLPPage = navigateToAccessoryPLPPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.continueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		//cartPage.clickAcceptAndPlaceOrder();
	}
	
	/**
	 * Standard Upgrade Flow With FRP Flow By Adding Accessories
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void testStandardUpgradeFRPFlowWithAccessories(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Standard upgrade flow with FRP & Accessories");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
		Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
		Reporter.log("4. Click on the device on Device PLP page |Device PDP page should be displayed ");
		Reporter.log(
				"5. Select full retail price option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select eligible line on LS page | Line Selector Details page should be displayed");
		Reporter.log("7. Click on 'Skip trade-in' CTA | PHP page should be displayed");
		Reporter.log("8. Click on Continue button on PHP page | Accessory PLP page should be displayed");
		Reporter.log("9. Click 'OK' on modal | Accessory PLP page should be displayed");
		Reporter.log("10. Select few accessories and click Continue CTA | Cart page should be displayed");
		Reporter.log("11. Click 'Continue to shipping' CTA | Shipping tab should be displayed");
		Reporter.log("12. Click 'Continue to payment' CTA | Payment tab page should be displayed");
		Reporter.log("13. Click 'Accept and Continue' CTA | Order Confirmation page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoryPLPPage accessoryPLPPage = navigateToAccessoryPLPPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.continueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		//cartPage.clickAcceptAndPlaceOrder();
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void testStandardUpgradeEipChangingInsuranceSocFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Standard upgrade flow with FRP");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
		Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
		Reporter.log(
				"4. Click on the device on Device PLP page. Device should be from other price tier |Device PDP page should be displayed ");
		Reporter.log(
				"5. Select Monthly from payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select eligible line on LS page | Line Selector Details page should be displayed");
		Reporter.log("7. Click on 'Skip trade-in' CTA | PHP page should be displayed");
		Reporter.log(
				"8. Choose any protaction plan, other then existing and click Continue | Accessory PLP page should be displayed");
		Reporter.log("9. Click 'OK' on modal and 'Skip Accessories' link | Cart page should be displayed");
		Reporter.log("10. Click 'Continue to shipping' CTA | Shipping tab should be displayed");
		Reporter.log("11. Click 'Continue to payment' CTA | Payment tab page should be displayed");
		Reporter.log("12. Click 'Accept and Continue' CTA | Order Confirmation page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(
				myTmoData);
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.selectNewSOCdifferentFromPrevious();
				deviceProtectionPage.clickContinueButton();
			}
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		//cartPage.clickAcceptAndPlaceOrder();
	}
}
