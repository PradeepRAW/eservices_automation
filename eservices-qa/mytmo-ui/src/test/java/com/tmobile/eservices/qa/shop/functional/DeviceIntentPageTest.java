package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.global.ContactUSPage;
import com.tmobile.eservices.qa.pages.shop.ConsolidatedRatePlanPage;
import com.tmobile.eservices.qa.pages.shop.DeviceIntentPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

import io.appium.java_client.ios.IOSDriver;

public class DeviceIntentPageTest extends ShopCommonLib {

	/**
	 * US370260 AAL - Device Intent Page -Selection Tiles layout and configuration
	 * US447565- Device Intent page check URL US452509- AAL - Device Intent Page -
	 * remove Footers
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION})
	public void testDeviceIntentPageSelectionTilesLayout(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case#US370260 - AAL - Device Intent Page -Selection Tiles layout and configuration");
		Reporter.log("Data Condition | STD PAH with TMO one rate  plan that is not maxed SOC");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application|Home Page should be displayed");
		Reporter.log("2. Click On Shop|Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button|Add A Line page should be displayed");
		Reporter.log(
				"4. verify 3 tiles with titles(buy a new phone,Use my own phone,Watches,tablets& more)| tiles with titles(buy a new phone,Use my own phone,Watches,tablets& more) should be displayed");
		Reporter.log(
				"5. verify 'some phones may not be compatible' text under tile 'Use my own phone'|'some phones may not be compatible' text should be displayed");
		Reporter.log("6. verify image for each tile| image for each tile should be displayed");
		Reporter.log("7. Verify Footer element | Footer elements should not displayed ");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
		deviceIntentPage.verifyUseMyOwnPhoneTileIsDisplayed();
		deviceIntentPage.verifyWatchesTabletsAndMoreTileIsDisplayed();
		deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
		deviceIntentPage.verifyUseMyOwnPhoneTileImageDisplayed();
		deviceIntentPage.verifyWatchesTabletsAndMoreTileImageDisplayed();
		deviceIntentPage.verifyDeviceIntentsPageFooterNotDisplayed();
		deviceIntentPage.clickUseMyPhoneOption();
	}
	
	/**
	 * US560067: Device Intent Page - Update existing tiles to integrate with new HTML for phase 2 AAL
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testDeviceIntentPageOptions(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US560067: Device Intent Page - Update existing tiles to integrate with new HTML for phase 2 AAL");
		Reporter.log("Data Condition | STD PAH and User should have AAL eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Verify 'Buy a new phone' shopping box and phone image image | 'Buy a new phone' image should be displayed");
		Reporter.log("5. Verify authorable text under 'Buy a new phone' image | Authorable text should be displayed under 'Buy a new phone' image");
		Reporter.log("6. Verify 'Use my own phone'  phone image | 'Use my own phone' image should be displayed");
		Reporter.log("7. Verify authorable text under 'Use my own phone' image | Authotable text should be displayed under 'Use my own phone' image");
		Reporter.log("8. Verify 'Buy a new watch' watch image | 'Buy a new watch' image should be displayed");
		Reporter.log("9. Verify authorable text under 'Buy a new watch' image | Authorable text should be displayed under 'Buy a new watch' image");
		Reporter.log("10. Select 'Buy a new phone' option | PLP page should be displayed");
		Reporter.log("11. Navigate back | Device-intent page should be displayed");
		Reporter.log("12. Select 'Use my own phone' option | Cosilated Rate plan page should be displayed");
		Reporter.log("13. Navigate back | Device-intent page should be displayed");
		Reporter.log("14. Select 'Buy a new watch' option | Smart-watches plp page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
		deviceIntentPage.verifyBuyANewPhoneTextIsDisplayed();
		deviceIntentPage.verifyUseMyOwnPhoneTileImageDisplayed();
		deviceIntentPage.verifyUseMyOwnPhoneTextIsDisplayed();
		deviceIntentPage.verifyWatchesTabletsAndMoreTileImageDisplayed();
		deviceIntentPage.verifyWatchesTabletsTextIsDisplayed();
		deviceIntentPage.clickBuyANewPhoneOption();
		PlpPage plpPageTmo = new PlpPage (getDriver());
		plpPageTmo.verifyPhonesPlpPageLoaded();
		plpPageTmo.navigateBack();
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.navigateBack();
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyAWatchOption();
		plpPageTmo.verifyWatchesPlpPageLoaded();
				
	}
	
	/**
	 * CDCSM-89:-
	 * US560086: Device Intent Page - Update existing tiles to integrate Eligibility Check API and show error messages for Max MBB OR max VOICE
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testDeviceIntentPageOptionsErrorMessageForMaxVoiceLines(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US560086: Device Intent Page - Update existing tiles to integrate Eligibility Check API and show error messages for Max MBB OR max VOICE");
		Reporter.log("Data Condition | STD PAH and User should have AAL eligible, user should have max Voice lines");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Verify 'Buy a new phone' shopping box and phone image image | 'Buy a new phone' image should be displayed");
		Reporter.log("5. Verify 'Use my own phone'  phone image | 'Use my own phone' image should be displayed");
		Reporter.log("6. Verify 'Buy a new watch' watch image | 'Buy a new watch' image should be displayed");
		Reporter.log("7. Verify Authorable error message | Authorable error message should be displayed");
		Reporter.log("8. Verify option is grayed out | Option should be grayed out");
		Reporter.log("9. Click on Authorable error message | User should be navigate to Contact us page");
		
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");		
		
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
		deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
		deviceIntentPage.verifyBringMyPhoneTileIsDisplayed();		
		deviceIntentPage.verifyBringMyPhoneTileImageDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileIsDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileImageDisplayed();
		deviceIntentPage.verifyMaxVoiceLinesErrorMessageOnBuyNewPhoneTileDisplayed();
		deviceIntentPage.verifyMaxVoiceLinesErrorMessageOnUseYourOwnPhoneTileDisplayed();
		deviceIntentPage.verifyBuyNewPhoneTileDisabled();
		deviceIntentPage.verifyUseYourOwnPhoneTileDisabled();
		deviceIntentPage.verifyOnPhoneNumberLinkOnBuyANewPhoneTile();
		deviceIntentPage.clickOnPhoneNumberLinkOnBuyANewPhoneTile();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
		
	}
	
	/**
	 * CDCSM-89:-
	 * US560086: Device Intent Page - Update existing tiles to integrate Eligibility Check API and show error messages for Max MBB OR max VOICE
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testDeviceIntentPageOptionsErrorMessageForMaxMBBLines(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US560086: Device Intent Page - Update existing tiles to integrate Eligibility Check API and show error messages for Max MBB OR max VOICE");
		Reporter.log("Data Condition | STD PAH and User should have AAL eligible, user should have max MBB lines");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Verify 'Buy a new phone' shopping box and phone image image | 'Buy a new phone' image should be displayed");
		Reporter.log("5. Verify 'Use my own phone'  phone image | 'Use my own phone' image should be displayed");
		Reporter.log("6. Verify 'Buy a new watch' watch image | 'Buy a new watch' image should be displayed");
		Reporter.log("7. Verify Authorable error message for 'Buy a new watch' option | Authorable error message should be displayed");
		Reporter.log("8. Verify 'Buy a new watch' option is grayed out | Option should be grayed out");
		Reporter.log("9. Click on Authorable error message | User should be navigate to Contact us page");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
		deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
		deviceIntentPage.verifyBringMyPhoneTileIsDisplayed();		
		deviceIntentPage.verifyBringMyPhoneTileImageDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileIsDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileImageDisplayed();
		deviceIntentPage.verifyMBBErrorMessageOnBuyNewWatchTileDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileDisabled();
		deviceIntentPage.verifyOnPhoneNumberLinkOnBuyANewWatchTile();
		deviceIntentPage.clickOnPhoneNumberLinkOnBuyANewWatchTile();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
		
	}
	
	/**
	 * CDCSM-89:-
	 * US560157: Device Intent Page - Update existing tiles to integrate Eligibility Check API and show error messages for ineligible digital AAL
	 * 
	 * 10th step should be verified in Mobile as well.
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testDeviceIntentPageOptionsErrorMessageForUserReachedMaxSocs(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US560157: Device Intent Page - Update existing tiles to integrate Eligibility Check API and show error messages for ineligible digital AAL");
		Reporter.log("Data Condition | STD PAH and User should have AAL eligible, user should have max SOC or Unsupported plans");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Verify 'Buy a new phone' shopping box and phone image image | 'Buy a new phone' image should be displayed");
		Reporter.log("5. Verify 'Bring a phone'  phone image | 'Bring a phones' image should be displayed");
		Reporter.log("6. Verify 'Buy a new watch' watch image | 'Buy a new watch' image should be displayed");
		Reporter.log("7. Verify max SOC error Authorable error message | Authorable error message should be displayed with 'Due to your current rate plan, this option isn't available online. Please call 1-800-937-8997.'");
		Reporter.log("8. Verify Unsupported Plan error | Authorable error message should be displayed with 'DUe to your current rate plan, this option isn't available online. Please call 1-800-937-8997.'");
		Reporter.log("9. Verify user selection option is grayed out | option should be grayed out");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
		deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
		deviceIntentPage.verifyBringMyPhoneTileIsDisplayed();
		deviceIntentPage.verifyBringMyPhoneTileImageDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileIsDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileImageDisplayed();
		deviceIntentPage.verifyErrorMessageOnBuyNewPhoneTileDisplayed();
		deviceIntentPage.verifyErrorMessageOnUseYourOwnPhoneTileDisplayed();
		deviceIntentPage.verifyBuyNewPhoneTileDisabled();
		deviceIntentPage.verifyUseYourOwnPhoneTileDisabled();
		deviceIntentPage.verifyOnPhoneNumberLinkOnBuyANewPhoneTile();
		deviceIntentPage.clickOnPhoneNumberLinkOnBuyANewPhoneTile();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
		
				
	}
	
	/**
	 * CDCSM-89
	 * US597280: Device Intent Page - UI - 12 lines max, no GSM line
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testDeviceIntentPageErrorMessageForUserReachedMaxLines(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US597280: Device Intent Page - UI - 12 lines max, no GSM line");
		Reporter.log("Data Condition | STD PAH and User should have AAL eligible, user should reached 12 lines max");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Verify 'Buy a new phone' shopping box and phone image image | 'Buy a new phone' image should be displayed");
		Reporter.log("5. Verify 'Use my own phone'  phone image | 'Use my own phone' image should be displayed");
		Reporter.log("6. Verify 'Buy a new watch' watch image | 'Buy a new watch' image should be displayed");
		Reporter.log("7. Verify Authorable error message as 'Sorry, you are unable to add a watch at this time due to the amount of lines on your account' | Authorable error message should be displayed");
		Reporter.log("8. Verify option is grayed out |Optin should be grayed out");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
		deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
		deviceIntentPage.verifyBringMyPhoneTileIsDisplayed();
		deviceIntentPage.verifyBringMyPhoneTileImageDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileIsDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileImageDisplayed();
		deviceIntentPage.verifyMaxMbbErrorMessageOnBuyNewWatchTileDisplayed();
		deviceIntentPage.verifyBuyNewPhoneTileDisabled();
		deviceIntentPage.verifyUseYourOwnPhoneTileDisabled();
		deviceIntentPage.verifyBuyANewWatchTileDisabled();
		deviceIntentPage.verifyOnPhoneNumberLinkOnBuyANewWatchTile();
		deviceIntentPage.clickOnPhoneNumberLinkOnBuyANewWatchTile();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
	}
	
	/**
	 * CDCSM-89
	 * US597280: Device Intent Page - UI - 12 lines max, no GSM line
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testDeviceIntentPageErrorMessageForUserDontHaveGSMLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US597280: Device Intent Page - UI - 12 lines max, no GSM line");
		Reporter.log("Data Condition | STD PAH and User should have AAL eligible, user should does not have a GSM line");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Verify 'Buy a new phone' shopping box and phone image image | 'Buy a new phone' image should be displayed");
		Reporter.log("5. Verify 'Use my own phone'  phone image | 'Use my own phone' image should be displayed");
		Reporter.log("6. Verify 'Buy a new watch' watch image | 'Buy a new watch' image should be displayed");
		Reporter.log("7. Verify Authorable error message as 'Sorry, you are unable to add a watch at this time due to having no voice lines on your account' | Authorable error message should be displayed");
		Reporter.log("8. Verify selecting option is grayed out | Selecting Option should be grayed out");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
		deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
		deviceIntentPage.verifyBringMyPhoneTileIsDisplayed();
		deviceIntentPage.verifyBringMyPhoneTileImageDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileIsDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileImageDisplayed();
		deviceIntentPage.verifyNoGsmErrorMessageOnBuyNewWatchTileDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileDisabled();
		deviceIntentPage.verifyOnPhoneNumberLinkOnBuyANewWatchTile();
		deviceIntentPage.clickOnPhoneNumberLinkOnBuyANewWatchTile();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
				
	}
	
	/**
	 * US612992: UI - Allow customers with to progress pass Shop Home Page (max voice for BAN, but not max MBB)
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testMaxVoiceLineCustomersNavigatedToDeviceIntentPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US612992: UI - Allow customers with to progress pass Shop Home Page (max voice for BAN, but not max MBB)");
		Reporter.log("Data Condition | AAL eligible, user should max Voice lines, but not max MBB lines");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Verify 'Buy a new phone' shopping box and phone image image | 'Buy a new phone' image should be displayed");
		Reporter.log("5. Verify 'Use my own phone'  phone image | 'Use my own phone' image should be displayed");
		Reporter.log("6. Verify 'Buy a new watch' watch image | 'Buy a new watch' image should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToShopPage(myTmoData);
        ShopPage shopPage = new ShopPage(getDriver());
        shopPage.clickQuickLinks("ADD A LINE");
        DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
        deviceIntentPage.verifyDeviceIntentPage();
        deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
        deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
        deviceIntentPage.verifyUseMyOwnPhoneTileIsDisplayed();
        deviceIntentPage.verifyUseMyOwnPhoneTileImageDisplayed();
        deviceIntentPage.verifyWatchesTabletsAndMoreTileIsDisplayed();
        deviceIntentPage.verifyWatchesTabletsAndMoreTileImageDisplayed();

	}	
	
	/**
	 * US612992: UI - Allow customers with to progress pass Shop Home Page (max voice for BAN, but not max MBB)
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testMaxMBBLineCustomersNavigatedToDeviceIntentPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US612992: UI - Allow customers with to progress pass Shop Home Page (max voice for BAN, but not max MBB)");
		Reporter.log("Data Condition | AAL eligible, user should max MBB lines but not max Voice lines");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Verify 'Buy a new phone' shopping box and phone image image | 'Buy a new phone' image should be displayed");
		Reporter.log("5. Verify 'Use my own phone'  phone image | 'Use my own phone' image should be displayed");
		Reporter.log("6. Verify 'Buy a new watch' watch image | 'Buy a new watch' image should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToShopPage(myTmoData);
        ShopPage shopPage = new ShopPage(getDriver());
        shopPage.clickQuickLinks("ADD A LINE");
        DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
        deviceIntentPage.verifyDeviceIntentPage();
        deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
        deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
        deviceIntentPage.verifyUseMyOwnPhoneTileIsDisplayed();
        deviceIntentPage.verifyUseMyOwnPhoneTileImageDisplayed();
        deviceIntentPage.verifyWatchesTabletsAndMoreTileIsDisplayed();
        deviceIntentPage.verifyWatchesTabletsAndMoreTileImageDisplayed();
	}
	
	/**
	 * CDCSM-89 : [CONT] Device Intent Page - Enable actionable error state for all tiles
	 * TC-2103: CDCSM-89:-Validate in Desktop web for the actionable error state for wearable tiles	 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testActionableErrorMessageForWearableTile(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition |  max account  level voice line");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on 'Add a Line' link in shop page | DeviceIntent Page should be displayed");
		Reporter.log("4. Verify authorable error message under Wearable tiles | Authorable error message should be displayed under Wearable tiles");
		Reporter.log("5. Click on error message | User should navigate to Contact us page");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
		deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
		deviceIntentPage.verifyBringMyPhoneTileIsDisplayed();		
		deviceIntentPage.verifyBringMyPhoneTileImageDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileIsDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileImageDisplayed();
		deviceIntentPage.verifyMBBErrorMessageOnBuyNewWatchTileDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileDisabled();
		deviceIntentPage.verifyOnPhoneNumberLinkOnBuyANewWatchTile();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		deviceIntentPage.clickOnPhoneNumberLinkOnBuyANewWatchTile();
		contactUSPage.verifyContactUSPage();
		}	
	
	
	/**
	 * CDCSM-89 : [CONT] Device Intent Page - Enable actionable error state for all tiles
	 * TC-2298 : CDCSM-89:-Validate in Desktop web for the actionable error state for voice tiles	 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testActionableErrorMessageForBYOD(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User max voice and wearable MISDIN");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on 'Add a Line' link in shop page | DeviceIntent Page should be displayed");
		Reporter.log("4. Verify authorable error message under Buy a new device tile | Authorable error message should be displayed under voice tiles");
		Reporter.log("5. When user Click on the error message | User should navigate to Contact Us Page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		 
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
		deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
		deviceIntentPage.verifyBringMyPhoneTileIsDisplayed();		
		deviceIntentPage.verifyBringMyPhoneTileImageDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileIsDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileImageDisplayed();
		deviceIntentPage.verifyMaxVoiceLinesErrorMessageOnBuyNewPhoneTileDisplayed();
		deviceIntentPage.verifyMaxVoiceLinesErrorMessageOnUseYourOwnPhoneTileDisplayed();
		deviceIntentPage.verifyBuyNewPhoneTileDisabled();
		deviceIntentPage.verifyUseYourOwnPhoneTileDisabled();
		deviceIntentPage.verifyOnPhoneNumberLinkOnBuyANewPhoneTile();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		deviceIntentPage.clickOnPhoneNumberLinkOnBuyANewPhoneTile();
		contactUSPage.verifyContactUSPage();
		
	}
	
	// Remove this test when merging
	/**
	 * CDCSM-89 : [CONT] Device Intent Page - Enable actionable error state for all tiles
	 * TC-2104 : CDCSM-89:-Validate in Mobile web for the actionable error state for wearable tiles	 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testActionableErrorMessageForWearableTileForMobile(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User with max digit line");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on 'Add a Line' link in shop page | DeviceIntent Page should be displayed");
		Reporter.log("4. Verify authorable error message under Wearable tiles | Authorable error message should be displayed under Wearable tiles");
		Reporter.log("5. Verify authorable phone no in error message  | Authorable phone no should be displayed in error message");
		//Reporter.log("6. Click on error message | Dialer with authored number should be displayed");
		//Reporter.log("7. Click on close modal | User should navigate back to Device intent page");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
		deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
		deviceIntentPage.verifyBringMyPhoneTileIsDisplayed();		
		deviceIntentPage.verifyBringMyPhoneTileImageDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileIsDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileImageDisplayed();
		deviceIntentPage.verifyMBBErrorMessageOnBuyNewWatchTileDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileDisabled();
		deviceIntentPage.verifyOnPhoneNumberLinkOnBuyANewWatchTile();
		deviceIntentPage.clickOnPhoneNumberLinkOnBuyANewWatchTile();
		
	}
	
	// Remove this test when merging
	/**
	 * CDCSM-89 : [CONT] Device Intent Page - Enable actionable error state for all tiles
	 * TC-2105 : CDCSM-89:-Validate in Mobile web for the actionable error state for voice tiles	 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testActionableErrorMessageForBYODOnlyForMobile(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User with max account level voice line");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on 'Add a Line' link in shop page | DeviceIntent Page should be displayed");
		Reporter.log("4. Verify authorable error message under Buy a new device tile | Authorable error message should be displayed under voice tiles");
		Reporter.log("5. Verify authorable phone no in error message  | Authorable phone no should be displayed in error message");
		//Reporter.log("6. Click on error message | Dialer with authored number should be displayed");
		//Reporter.log("7. Click on close modal | User should navigate back to Device intent page");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
		deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
		deviceIntentPage.verifyBringMyPhoneTileIsDisplayed();		
		deviceIntentPage.verifyBringMyPhoneTileImageDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileIsDisplayed();
		deviceIntentPage.verifyBuyANewWatchTileImageDisplayed();
		deviceIntentPage.verifyMaxVoiceLinesErrorMessageOnBuyNewPhoneTileDisplayed();
		deviceIntentPage.verifyMaxVoiceLinesErrorMessageOnUseYourOwnPhoneTileDisplayed();
		deviceIntentPage.verifyBuyNewPhoneTileDisabled();
		deviceIntentPage.verifyUseYourOwnPhoneTileDisabled();
		deviceIntentPage.verifyOnPhoneNumberLinkOnBuyANewPhoneTile();
		deviceIntentPage.clickOnPhoneNumberLinkOnBuyANewPhoneTile();
		
	}
	
	/**
	 * CDCSM-90 : [CONT] Device Intent Page - Enable actionable error state for all tiles
	 * TC-2036 : CDCSM-90:-Validate customer with plan level max voice line will eligible to navigate to device intent page
	 * @param data
	 * @param myTmoData
	 */     
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testUserNavigatedToDeviceIntentPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("User Story: CDCSM-90 : [CONT] Device Intent Page - Enable actionable error state for all tiles");
		Reporter.log("Test Case: TC-2036 : CDCSM-90:-Validate customer with plan level max voice line will eligible to navigate to device intent page");
		Reporter.log("Data Condition | PAH User with max plan level voice lines");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the MyTmo application And Login | MyTmo Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Launch direct URL'https://e2.my.t-mobile.com/purchase/device-intent'| User should redirect to device intent page");		
		Reporter.log("3. Verify error message for voice tiles | Voice lines should be grayed out with error message");
		Reporter.log("4. Verify Wearable tile is enabled | Wearable should be enabled");
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToHomePage(myTmoData);
        HomePage homePage = new HomePage(getDriver());
        homePage.verifyHomePage();
        
        DeviceIntentPage deviceIntentPage = navigateToDeviceIntentUsingDeeplink();
        deviceIntentPage.verifyBuyNewPhoneTileDisabled();
        deviceIntentPage.verifyErrorMessageOnBuyNewPhoneTileDisplayed();
        deviceIntentPage.verifyUseYourOwnPhoneTileDisabled();
        deviceIntentPage.verifyErrorMessageOnUseYourOwnPhoneTileDisplayed();
        deviceIntentPage.verifyBuyANewWatchTileIsDisplayed();
        deviceIntentPage.verifyBuyANewWatchTileImageDisplayed();

	}
	
	/**
	 * CDCSM-90 : [CONT] Device Intent Page - Enable actionable error state for all tiles	
	 * TC-2037 : CDCSM-90:-Validate customer with max MI line will eligible to navigate to device intent page	 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testMaxMILinesUserGetErrorMessageForWearablesInDeviceIntentPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("User Story: CDCSM-90 : [CONT] Device Intent Page - Enable actionable error state for all tiles");
		Reporter.log("Test Case: TC-2037 : CDCSM-90:-Validate customer with max MI line will eligible to navigate to device intent page");
		Reporter.log("Data Condition | PAH User with max MI lines");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the MyTmo application And Login | MyTmo Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Launch direct URL'https://e2.my.t-mobile.com/purchase/device-intent'| User should redirect to device intent page");
		Reporter.log("3. Verify error message for Wearable tile | Error messages should be displayed for wearable tile and grayed out");
		Reporter.log("4. Verify Voice lines is enabled | Voice lines should be enabled");
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToHomePage(myTmoData);
        HomePage homePage = new HomePage(getDriver());
        homePage.verifyHomePage();
        
        DeviceIntentPage deviceIntentPage = navigateToDeviceIntentUsingDeeplink();
        deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
        deviceIntentPage.verifyBuyANewPhoneTileImageDisplayed();
        deviceIntentPage.verifyBringMyPhoneTileIsDisplayed();        
        deviceIntentPage.verifyBringMyPhoneTileImageDisplayed();
        deviceIntentPage.verifyBuyANewWatchTileIsDisplayed();
        deviceIntentPage.verifyBuyANewWatchTileImageDisplayed();
        deviceIntentPage.verifyMBBErrorMessageOnBuyNewWatchTileDisplayed();
        deviceIntentPage.verifyBuyANewWatchTileDisabled();
        
	}
	
	/**
	 * CDCSM-90 : [CONT] Device Intent Page - Enable actionable error state for all tiles	
	 * TC-2040 : CDCSM-90:-Validate customer with account level max voice line will eligible to navigate to device intent page	 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testVoiceLinesGrayedOutForMaxVoiceLineUsers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("User Story: CDCSM-90 : [CONT] Device Intent Page - Enable actionable error state for all tiles");
		Reporter.log("Test Case: TC-2040 : CDCSM-90:-Validate customer with account level max voice line will eligible to navigate to device intent page");
		Reporter.log("Data Condition | PAH User with max voice lines");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the MyTmo application And Login | MyTmo Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Launch direct URL'https://e2.my.t-mobile.com/purchase/device-intent'| User should redirect to device intent page");
		Reporter.log("3. Verify error message for Wearable  | Error messages should not be displayed for wearable tile");
		Reporter.log("4. Verify voice lines grayed out | Voice lines should be grayed out");
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToHomePage(myTmoData);
        HomePage homePage = new HomePage(getDriver());
        homePage.verifyHomePage();
        
        DeviceIntentPage deviceIntentPage = navigateToDeviceIntentUsingDeeplink();
        deviceIntentPage.verifyErrorMessageOnBuyNewWatchTileNotDisplayed();
        deviceIntentPage.verifyBuyNewPhoneTileDisabled();
		deviceIntentPage.verifyMaxVoiceLinesErrorMessageOnBuyNewPhoneTileDisplayed();
		deviceIntentPage.verifyUseYourOwnPhoneTileDisabled();
	    deviceIntentPage.verifyMaxVoiceLinesErrorMessageOnUseYourOwnPhoneTileDisplayed();
	}
	
}
