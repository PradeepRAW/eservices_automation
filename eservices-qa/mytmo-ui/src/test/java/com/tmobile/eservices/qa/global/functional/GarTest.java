package com.tmobile.eservices.qa.global.functional;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.AssertJUnit;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GarTest {

	static List<String> pageURLs;
	static List<String> imageURLs;
	static HashMap<String, List<String>> tmoCacheResult;

	static WebDriver driver;
	static String env = "e9";
	static String applicationUrl = System.getProperty("environment");

	/***
	 * list all the pages which contain s.tmocache.com
	 * 
	 * @return
	 */
	public static void getPagesWithCDN() {
		pageURLs = new ArrayList<String>();
		pageURLs.add(applicationUrl + "/home");
		pageURLs.add(applicationUrl + "/plans.html");
		pageURLs.add(applicationUrl + "/myphone/myphonedetails.html");
		pageURLs.add(applicationUrl + "/purchase/shop");
		pageURLs.add(applicationUrl + "/purchase/productlist");
		pageURLs.add(applicationUrl + "/purchase/productdetails");
		pageURLs.add(applicationUrl + "/purchase/lineselector");
		pageURLs.add(applicationUrl + "/purchase/accessorylist");
		pageURLs.add(applicationUrl + "/purchase/accessorydetail");
		pageURLs.add(applicationUrl + "/shop/accessories");

	}

	/**
	 * 
	 * This will instantiate the driver and integarte with the sauce labs
	 * 
	 * @param local
	 */
	@SuppressWarnings("deprecation")
	public static void instantiateDriver(boolean local) {
		if (local) {
			System.setProperty("webdriver.chrome.driver", "C:\\chromedriver_win32\\chromedriver.exe");
			DesiredCapabilities caps = DesiredCapabilities.chrome();
			LoggingPreferences logPrefs = new LoggingPreferences();
			logPrefs.enable(LogType.PERFORMANCE, Level.INFO);
			caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
			driver = new ChromeDriver(caps);

		} else {
			try {
				LoggingPreferences logPrefs = new LoggingPreferences();
				logPrefs.enable(LogType.PERFORMANCE, Level.INFO);

				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setBrowserName("chrome");
				capabilities.setCapability("platform", "Windows 10");
				capabilities.setCapability("version", "73.0");
				capabilities.setCapability("name", "GAR_Code_Validation");
				capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
				capabilities.setCapability("parentTunnel", "CTFrameworkPlatform");
				capabilities.setCapability("tunnelIdentifier", "CTFrameworkPlatform");
				driver = new RemoteWebDriver(new URL(
						"http://tmo_eServices:a138d0ba-09c7-4a2b-9565-67e15a5887c3@ondemand.saucelabs.com:80/wd/hub"),
						capabilities);
				SessionId sessionId = ((RemoteWebDriver) driver).getSessionId();
				Reporter.log("Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/"
						+ sessionId + "\">SauceLabs</a>");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * This will login to tmobile website wih missdn and password
	 */
	public void loginToMyTMO() {
		try {
			driver.get(applicationUrl);
			driver.manage().window().maximize();
			Thread.sleep(2000);
			driver.findElement(By.xpath("//input[@aria-label='Email or Phone Number']")).sendKeys("4257771702");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//button[contains(@class,'btn til-btn ripple')]")).click();
			Thread.sleep(8000);
			driver.findElement(By.xpath("//input[@autocomplete='new-password']")).sendKeys("Auto12345");
			Thread.sleep(5000);
			driver.findElement(By.xpath("//button[contains(text(),'Log in')]")).click();
			Thread.sleep(8000);
			driver.findElement(By.cssSelector("input[value='security_question']")).click();
			Thread.sleep(2000);
			driver.findElement(By.cssSelector(".btn-primary")).click();
			Thread.sleep(2000);
			driver.findElement(By.cssSelector("input[id='que_0_field']")).sendKeys("test");
			Thread.sleep(2000);
			driver.findElement(By.cssSelector("input[id='que_1_field']")).sendKeys("test");
			Thread.sleep(8000);
			driver.findElement(
					By.xpath("//button[contains(@class,'btn btn-primary continue-btn full-on-mobile ng-binding')]"))
					.click();
			Thread.sleep(2000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * This will get all the images in tmobile website pages which are pointed to
	 * cdn.tmobile.com
	 * 
	 * @return
	 */
	public static List<String> getAllImageURLsPointedToCdnUrl() {
		imageURLs = new ArrayList<String>();
		imageURLs.clear();
		try {
			List<LogEntry> entries = driver.manage().logs().get(LogType.PERFORMANCE).getAll();
			for (LogEntry entry : entries) {
				String service_String = entry.getMessage();
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(service_String);
				if (service_String.contains("cdn.tmobile.com/images") && service_String.contains("responseReceived")) {
					JsonNode deviceFamilyInfoPathNode1 = jsonNode.path("message").path("params").path("response");
					imageURLs.add(deviceFamilyInfoPathNode1.get("url").toString());

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return imageURLs;
		}
	}

	/**
	 * This will get all the images in tmobile site which are pointed to
	 * s.tmocache.com
	 * 
	 * @return
	 */
	public static List<String> getAllImageURLsPointedToTmoCache() {
		List<String> tmo_imageURLs = new ArrayList<String>();
		try {
			List<LogEntry> entries = driver.manage().logs().get(LogType.PERFORMANCE).getAll();

			for (LogEntry entry : entries) {
				String service_String = entry.getMessage();
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(service_String);

				if (service_String.contains("s.tmocache.com") && service_String.contains("responseReceived")) {
					JsonNode deviceFamilyInfoPathNode1 = jsonNode.path("message").path("params").path("response");
					tmo_imageURLs.add(deviceFamilyInfoPathNode1.get("url").toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return tmo_imageURLs;
		}
	}

	@Test
	public void runGARTestWithCDN() {
		try {
			tmoCacheResult = new HashMap<String, List<String>>();
			getPagesWithCDN();
			instantiateDriver(false);
			loginToMyTMO();
			for (String url : pageURLs) {
				driver.navigate().to(url);
				Thread.sleep(15000);
				tmoCacheResult.put(url, getAllImageURLsPointedToCdnUrl());
				if (url.contains("purchase/productlist")) {
					driver.findElement(By.xpath("(//img[contains(@class,'imag')])[3]")).click();
					Thread.sleep(10000);
					if (driver.getCurrentUrl().contains("purchase/productdetails")) {
						tmoCacheResult.put(driver.getCurrentUrl(), getAllImageURLsPointedToCdnUrl());

					}
				}
				if (url.contains("shop/accessories")) {
					driver.navigate().to(url);
					Thread.sleep(15000);
					driver.findElement(By.xpath("(//img[contains(@class,'img')])[1]")).click();
					Thread.sleep(10000);
					if (driver.getCurrentUrl().contains("shop/accessoriesdetails")) {
						tmoCacheResult.put(driver.getCurrentUrl(), getAllImageURLsPointedToCdnUrl());
						driver.findElement(By.xpath("//button[contains(text(),'Add to cart')]")).click();
						Thread.sleep(8000);
						tmoCacheResult.put(driver.getCurrentUrl(), getAllImageURLsPointedToCdnUrl());

					}
				}
			}
			int imageNotFoundCounter = 0;
			List<String> imageNotFoundList = new ArrayList<String>();
			for (Map.Entry<String, List<String>> entry : tmoCacheResult.entrySet()) {
				String page = entry.getKey().replace(applicationUrl, "");
				if (!entry.getValue().isEmpty()) {
					Reporter.log("The page " + "'" + page + "'" + " has all images pointed to cdn.tmobile.com");
					Reporter.log("");
					for (String imageUrl : entry.getValue()) {
						try {
							if (!imageUrl.isEmpty()) {
								URL url = new URL(imageUrl.replace("\"", ""));
								HttpURLConnection connection = (HttpURLConnection) url.openConnection();
								connection.setRequestMethod("GET");
								connection.connect();
								int statusCode = connection.getResponseCode();
								if (statusCode == 200) {
									Reporter.log(
											"Image Url:  " + imageUrl + "  status code: " + statusCode + " - found");
								} else if (statusCode == 404) {
									imageNotFoundCounter++;
									imageNotFoundList.add("Image Url:  " + imageUrl + "  status code: " + statusCode
											+ " - not found");
								} else if (statusCode >= 500) {
									imageNotFoundCounter++;
									imageNotFoundList.add("Image Url:  " + imageUrl + "  status code: " + statusCode
											+ " - not found");
								}
							}
						} catch (Exception e) {

						}
					}
					Reporter.log("");

				} else {
					Reporter.log("The page " + "'" + page + "'" + " has no images pointed to cdn.tmobile.com");
				}
			}
			if (imageNotFoundList.size() > 0) {
				Reporter.log(imageNotFoundCounter + " images have  error");
				for (String image : imageNotFoundList) {

					Reporter.log(image);
				}
				AssertJUnit.fail(imageNotFoundCounter + " images have  error");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			driver.quit();
		}
	}

}