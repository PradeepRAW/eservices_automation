package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.ManageEasyPayApiV2;

import io.restassured.response.Response;

public class ManageEasyPayApiV2Test extends ManageEasyPayApiV2 {

	public Map<String, String> tokenMap;

	/**
	 * UserStory# Description: testEasyPaywithNewCard
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "" })
	public void testEasyPaywithNewCard(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testEasyPaywithNewCard");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the testEasyPaywithNewCard of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		String cancelAutopayRequestBody = "{\"accountNumber\":\"" + apiTestData.getBan()
				+ "\",\"paymentMethod\":{\"code\":\"credit\"}}";
		String cancelAutopayRequest = prepareRequestParam(cancelAutopayRequestBody, tokenMap);
		cancelAutopay(apiTestData, cancelAutopayRequest, tokenMap);
		String operationName = "testEasyPaywithNewCard";
		// tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("EasypaywithNewCard.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = easypaywithNewcard(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertNotNull(getPathVal(jsonNode, "accountNumber"), "accountNumber not dispalyed in Response");
			Assert.assertNotNull(getPathVal(jsonNode, "msisdn"), "msisdn not dispalyed in Response");
			Assert.assertNotNull(getPathVal(jsonNode, "paymentMethod.creditCard.cardNumber"),
					"cardNumber not dispalyed in Response");
			Assert.assertNotNull(getPathVal(jsonNode, "paymentMethod.creditCard.cvv"), "cvv not dispalyed in Response");
			Assert.assertEquals("true", getPathVal(jsonNode, "paymentMethod.store"));

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# Description: testEasyPaywithStoredCard
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "" })
	public void testEasyPaywithStoredCard(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testEasyPaywithStoredCard");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the testEasyPaywithStoredCard of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testEasyPaywithStoredCard";

		tokenMap = new HashMap<String, String>();
		String cancelAutopayRequestBody = "{\"accountNumber\":\"" + apiTestData.getBan()
				+ "\",\"paymentMethod\":{\"code\":\"credit\"}}";
		String cancelAutopayRequest = prepareRequestParam(cancelAutopayRequestBody, tokenMap);
		cancelAutopay(apiTestData, cancelAutopayRequest, tokenMap);

		// tokenMap = new HashMap<String, String>();
		tokenMap = new HashMap<String, String>();
		String requestBody = new ServiceTest().getRequestFromFile("EasypaywithStoredCard.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = easypaywithStoredcard(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertNotNull(getPathVal(jsonNode, "accountNumber"), "accountNumber not dispalyed in Response");
			Assert.assertNotNull(getPathVal(jsonNode, "msisdn"), "msisdn not dispalyed in Response");
			Assert.assertNotNull(getPathVal(jsonNode, "paymentMethod.creditCard.cardNumber"),
					"cardNumber not dispalyed in Response");
			Assert.assertEquals("false", getPathVal(jsonNode, "paymentMethod.store"));

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# Description: testCancelEasypay
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "" })
	public void testCancelEasypay(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testCancelEasypay");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the testCancelEasypay of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testCancelEasypay";
		tokenMap = new HashMap<String, String>();
		String requestBody = "{\"accountNumber\":\"" + apiTestData.getBan()
				+ "\",\"paymentMethod\":{\"code\":\"credit\"}}";

		// tokenMap.put("ban", apiTestData.getBan());
		// tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = cancelAutopay(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertNotNull(getPathVal(jsonNode, "accountNumber"), "accountNumber not dispalyed in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# Description: testEasyPaywithNewbankAccount
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = false, groups = { "" })
	public void testEasyPaywithNewBankAccount(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testEasyPaywithNewbankAccount");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the EasyPaywithNewbankAccount of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testEasyPaywithNewbankAccount";
		tokenMap = new HashMap<String, String>();
		String requestBody = new ServiceTest().getRequestFromFile("EasypaywithNewBankAccount.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = easypaywithNewBankAccount(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertNotNull(getPathVal(jsonNode, "accountNumber"), "accountNumber not dispalyed in Response");
			Assert.assertNotNull(getPathVal(jsonNode, "msisdn"), "msisdn not dispalyed in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# Description: testEasyPaywithStoredBankAccount
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = false, groups = { "" })
	public void testEasyPaywithStoredBankAccount(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testEasyPaywithStoredBankAccount");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the EasyPaywithStoredBankAccount of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "EasyPaywithStoredBankAccount";
		tokenMap = new HashMap<String, String>();
		String requestBody = new ServiceTest().getRequestFromFile("EasypaywithStoredBankAccount.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = easypaywithStoredBankAccount(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertNotNull(getPathVal(jsonNode, "accountNumber"), "accountNumber not dispalyed in Response");
			Assert.assertNotNull(getPathVal(jsonNode, "msisdn"), "msisdn not dispalyed in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

}
