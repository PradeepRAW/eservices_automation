package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;

public class MyTMOCloudTest extends GlobalCommonLib {

	/**
	 * US245322 - dotNET Migration - I/A - Regression Testing of Site
	 * Functionality US245321 - dotNET Migration - I/A - Add Account to Consumer
	 * Security Group US245320 - dotNET Migration - I/A - Add Accounts to MyTMO
	 * Cloud
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "MyTMO Cloud")
	public void testIAAccountValidation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :Verify IA account validation");
		Reporter.log("");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop | Shop page should be displayed");
		Reporter.log("5. Verify add a line | Add A Line should be enabled");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();

		homePage.clickShoplink();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyShoppage();
		shopPage.verifyAddALineIsEnabledOrDisabled();
	}

	/**
	 * US245306 - dotNET Migration - S/K - Regression Testing of Site
	 * Functionality US245296 - dotNET Migration - S/K - Add Account to Consumer
	 * Security Group US245291 - dotNET Migration - S/K - Add Accounts to MyTMO
	 * Cloud US245289, US245285, US245286
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "MyTMO Cloud")
	public void testSKAndSTAccountValidation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :Verify SK And ST account validation");
		Reporter.log("");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop | Shop page should be displayed");
		Reporter.log("5. Verify add a line | Add A Line should be disabled");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();

		homePage.clickShoplink();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyShoppage();
		shopPage.verifyAddALineIsDisabled();
	}

	/**
	 * US245323, US245324, US245325, US245327, US245329, US245330
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "MyTMO Cloud")
	public void testSCAndSDAccountValidation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :Verify SC And SD account validation");
		Reporter.log("");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop | Shop page should be displayed");
		Reporter.log("5. Verify add a line | Add A Line should be disabled");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();

		homePage.clickShoplink();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyShoppage();
		shopPage.verifyAddALineIsDisabled();
	}

}
