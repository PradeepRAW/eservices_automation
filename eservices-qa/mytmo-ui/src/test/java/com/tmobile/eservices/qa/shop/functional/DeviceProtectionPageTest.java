package com.tmobile.eservices.qa.shop.functional;

import com.tmobile.eservices.qa.pages.shop.*;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class DeviceProtectionPageTest extends ShopCommonLib {

	/**
	 * US349949: myTMO > PDP Simplification > Standard Upgrade > Enrollment and
	 * Migration > Protection 360 description
	 * US388894 AAL - UnKnown intent Blocked path -
	 * US240613: New Cart - Device Insurance Image(Non Jump) - Migration or
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void test360DeviceProtectionSocAndEditAndRemoveFunctionality(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US349949: myTMO > PDP Simplification > Standard Upgrade > Enrollment and Migration > Protection 360 description");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select FRP | FRP is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button | Insurance migration page should be displayed");
		Reporter.log("11. Verify 'Protection 360'Soc insurance | 'Protection 360' soc insurance should be displayed");
		Reporter.log(
				"12. Verify 'Protection 360'Soc insurance amount | 'Protection 360' soc insurance amount should be displayed");
		Reporter.log(
				"13. Verify 'Protection 360'Soc insurance description | 'Protection 360' soc insurance description should be displayed");
		Reporter.log("1. Click  Edit Insurance Soc in cart page | Insurance migration page should be displayed");

		Reporter.log("12. Click Insurance Remove button | Insurance Remove model window should be displayed");
		Reporter.log(
				"11. Verify Remove insurance window header message | Remove Insurance modal header message should not be displayed");
		Reporter.log(
				"12. Verify Remove insurance window body message | Remove Insurance modal body message should not be displayed");
		Reporter.log("13. Click Remove insurance window ok button | Cart page should be displayed");
		Reporter.log("14. Verify Insurance name in cart page | Insurance name should not be displayed in cart page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickOtherProtectionOption();
		deviceProtectionPage.verify360SOCName();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickEditInsuranceSoc();
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickContinueButton();

		accessoryPLPPage.clickSkipAccessoriesCTA();
		cartPage.verifyCartPage();

		cartPage.clickInsuranceRemoveButton();
		cartPage.verifyRemoveInsuranceModalWindow();
		cartPage.verifyRemoveInsuranceModalWindowHeader();
		cartPage.verifyRemoveInsuranceModalWindowBodyMessage();
		cartPage.clickRemoveInsuranceModalWindowOkButton();
		cartPage.verifyCartPage();
	}
	
	/**
	 * US350430: myTMO > PDP Simplication > Standard upgrade > Enrollment >
	 * Other Protection options
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testOtherProtectionOptionsForNonNYInInsuranceEnrollmentPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US350430: myTMO > PDP Simplication > Standard upgrade > Enrollment > Other Protection options");
		Reporter.log("Data Condition | IR PAH Customer, non NY, customer should not have device protection");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select EIP | EIP is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log(
				"9. Select a line and Expand the Selected line | I should see remaining lines should not be Displayed");
		Reporter.log("10. Click 'No,skip trade-in' button | Insurance migration page should be displayed");
		Reporter.log(
				"11. Click 'Other protection options' button | Extended Insurance migration page options should be exposed");
		Reporter.log(
				"12. Verify header as 'How do you want to protect your new phone?' | 'How do you want to protect your new phone?' should be displayed as a header");
		Reporter.log("13. Verify 'BEST VALUE' text below header | 'BEST VALUE' text below header should be displayed");
		Reporter.log("14. Verify 'Protection 360' Soc insurance | 'Protection 360' soc insurance should be displayed");
		Reporter.log(
				"15. Verify 'Protection 360' Soc insurance is selected by default | 'Protection 360' soc insurance should be selected by default");
		Reporter.log("16. Verify 'Protection 360' Soc price | 'Protection 360' soc price should be displayed");
		Reporter.log(
				"17. Verify 'Protection 360' Soc insurance description | 'Protection 360' soc insurance description should be displayed");
		Reporter.log("18. Verify BasicSOC name as 'Basic' | Basic Soc name as 'Basic' should be displayed");
		Reporter.log("19. Verify BasicSOC price | Basic Soc price should be displayed");
		Reporter.log("20. Verify BasicSOC description | BasicSOC description should be displayed");
		Reporter.log("21. Verify Skip protection | Skip protection should be displayed");
		Reporter.log(
				"22. Verify legal text for Non-NY customers | Legal text for Non-NY customers should be displayed");
		Reporter.log(
				"23. Verify Insurance migration page Continue button | Insurance migration page Continue button should be displayed");
		Reporter.log("24. Click 'I don't need protection for my phone' radiobutton | Popup modal should be displayed");
		Reporter.log("25. Click close modal | Extended Insurance migration page should be displayed");
		Reporter.log("25. Click Navigate to back | Insurance migration page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();

		deviceProtectionPage.verifyHighLevelProtectionTitle();
		deviceProtectionPage.verifyOtherProtectionOption();
		deviceProtectionPage.clickOtherProtectionOption();

		deviceProtectionPage.verifyLegalTextForBestValue();
		deviceProtectionPage.verify360SOCName();
		deviceProtectionPage.verify360SOCPrice();
		deviceProtectionPage.verify360SOCDescription();
		deviceProtectionPage.verifyProtectionBasicSOCName();
		deviceProtectionPage.verifyProtectionBasicSOCPrice();
		deviceProtectionPage.verifyProtectionBasicSOCDescription();
		deviceProtectionPage.verifyRadioButtonsPresent();
		deviceProtectionPage.verifyLegalTextFornonNYCustomers();
		deviceProtectionPage.clickIDontNeedProtectionRadioButton();

		deviceProtectionPage.clickContinueButton();
		deviceProtectionPage.verifyDeclineModalPopUpWindow();
	}

	/**
	 * US342377 myTMO > PDP > Simplification > Standard Upgrade > Enrollment >
	 * NY customers > Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testDeviceProtectionSocsDisplayedForNYcustomersAndDeclineFlow(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test US342377: myTMO > PDP > Simplification > Standard Upgrade > Enrollment > NY customers > Display");
		Reporter.log("Data Condition | IR STD PAH Customer, NY customers, no device protection");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select FRP | FRP is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button | Insurance migration page should be displayed");
		Reporter.log("11. Verify Insurance header as'How do you want to protect your phone?' | "
				+ "Insurance header as'How do you want to protect your phone?' should be displayed");
		Reporter.log("12. Verify 'Best Value!' text below header| 'Best Value!' text should be displayed");
		Reporter.log(
				"13. Verify 'Protection 360' Soc insurance description | 'Protection 360' soc insurance description should be displayed");
		Reporter.log(
				"14. Verify 'Protection 360' SOC selected by default |  'Protection 360' SOC should be selected by default");
		Reporter.log("15. Verify All available New York SOCs |  All available New York SOCs should be displayed");
		Reporter.log(
				"16. Verify NY SOCs displayed in order 'Extended warranty SOC' , 'Insurance only SOC',' Extended warranty and insurance SOC'| NY SOCs should be  displayed in order 'Extended warranty SOC' , 'Insurance only SOC',' Extended warranty and insurance SOC'");
		Reporter.log(
				"17. Verify Amount  for Extended warranty SOC below the SOC name '$6.75/mo'| Amount  for Extended warranty SOC below the SOC name '$6.75/mo' should be displayed");
		Reporter.log("18. Verify two decimal points for pricing| pricing should be two decimal points");
		Reporter.log(
				"19. Verify Extended warranty benefit description| Extended warranty benefit description should be displayed");
		Reporter.log(
				"20. Verify Amount  for Insurance only SOC below the SOC name '$4.00/mo'| Amount  for Insurance only SOC below the SOC name '$4.00/mo' should be displayed");
		Reporter.log("21. Verify two decimal points for pricing| pricing should be two decimal points");
		Reporter.log(
				"22. Verify Insurance only benefit description|  Insurance only benefit description should be displayed");
		Reporter.log(
				"23. Verify Amount for Extended warranty and insurance SOC below the SOC name '$8.50/mo'| Amount for Extended warranty and insurance SOC below the SOC name '$8.50/mo' should be displayed");
		Reporter.log("24. Verify two decimal points for pricing | pricing should be two decimal points");
		Reporter.log(
				"25. Verify Extended warranty and insurance benefit description.|  Extended warranty and insurance benefit description should be displayed");
		Reporter.log(
				"26. Verify 'I don't need protection for my phone' radio button |  'I don't need protection for my phone' radio button should be displayed");
		Reporter.log("27. Verify legal text for NY customers | Legal text for NY customers should be displayed");
		Reporter.log(
				"28. Select 'I don't need protection for my phone.' and click on continue button | Decline insurance modal pop up window should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		deviceProtectionPage.verifyInsuranceHeaderForNY();
		deviceProtectionPage.verifyLegalTextForBestValue();
		deviceProtectionPage.verify360SOCDescription();
		deviceProtectionPage.verify360SOCIsSelected();
		deviceProtectionPage.verifyAllNYSOCPresent();
		deviceProtectionPage.verifyExtendedWarrantySOCDescriptionPresent();
		deviceProtectionPage.verifyExtendedWarrantySOCPricePresent();
		deviceProtectionPage.verifyInsuranceOnlySOCDescriptionPresent();
		deviceProtectionPage.verifyInsuranceOnlySOCPricePresent();
		deviceProtectionPage.verifyExtendedWarrantyAndInsuranceSOCDescriptionPresent();
		deviceProtectionPage.verifyExtendedWarrantyAndInsuranceSOCPricePresent();
		deviceProtectionPage.verifyDontNeedProtectionRadioButtonForNY();
		deviceProtectionPage.verifyLegalTextForNYCustomers();
		deviceProtectionPage.clickDontNeedProtectionRadioButtonForNY();
		deviceProtectionPage.clickContinueButton();
		deviceProtectionPage.verifyDeclineModalPopUpWindow();
	}

	/**
	 * US342386 myTMO > PDP Simplification > Standard Upgrade > Migration > Non
	 * - NY customers > Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testDeviceMigrationNonNYCustomersDisplayAndDeclineFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test  US342386:  myTMO > PDP Simplification > Standard Upgrade > Migration > Non - NY customers > Display");
		Reporter.log("Data Condition | IR STD PAH Customer, Non-NY customers, with device protection");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select more expensive device (different tier) | Product details page should be displayed");
		Reporter.log("7. Select EIP | EIP is selected");
		Reporter.log("8. Click Upgrade button | Line selection page should be displayed");
		Reporter.log("9. Select line | Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button | Insurance migration page should be displayed");
		Reporter.log(
				"11. Verify header 'Your new phone requires higher level of protection. Let's keep it protected.' | 'Your new phone requires higher level of protection. Let's keep it protected.'  should be displayed");
		Reporter.log(
				"12. Verify current SOC status changed as 'Previous'| 'current SOC status should be changed as 'Previous' ");
		Reporter.log(
				"13. Verify the previous SOC name (grandfathered SOC name if the current SOC hasn't been migrated to the new SOC)| the previous SOC name should be displayed");
		Reporter.log(
				"14. Verify the amount of the previous SOC '$12.00/mo.'| the amount of the previous SOC '$12.00/mo.' should be displayed");
		Reporter.log(
				"15. Verify 'Protection 360' as a recommended SOC with pricing and description| 'Protection 360' as a recommended SOC with pricing and description should be displayed");
		Reporter.log(
				"16. Verify radiobutton for both current and recommended SOCs.| radiobutton for both current and recommended SOCs. should be displayed");
		Reporter.log(
				"17. Verify 'I don't need protection for my phone' radio button |  'I don't need protection for my phone' radio button should be displayed");
		Reporter.log(
				"18. Verify legal text for non NY customers | Legal text for non NY customers should be displayed");
		Reporter.log(
				"19. Select 'I don't need protection for my phone.' and click on continue button | Decline insurance modal pop up window should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.verifyHighLevelProtectionTitle();
		deviceProtectionPage.verifyPrevoiusSOCStatus();
		deviceProtectionPage.verifyPrevoiusSOCName();
		deviceProtectionPage.verifyPrevoiusSOCPrice();
		deviceProtectionPage.verify360SOCName();
		deviceProtectionPage.verify360SOCPrice();
		deviceProtectionPage.verify360SOCDescription();
		deviceProtectionPage.verifyIDOntNeedProtectionSOC();
		deviceProtectionPage.verifyRadioButtonsPresent();
		deviceProtectionPage.verifyLegalTextFornonNYCustomers();
		deviceProtectionPage.clickDontNeedProtectionRadioButton();
		deviceProtectionPage.clickContinueButton();
		deviceProtectionPage.verifyDeclineModalPopUpWindow();
	}

	/**
	 * US342387 myTMO > PDP Simplification > Standard Upgrade > Migration > NY
	 * customers > Display
	 * US376107 myTMO > PDP Simplification > Standard Upgrade/JUMP > NY/Non-NY >
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testDeviceMigrationNYCustomersDisplayAndDeclineFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test  US342387: myTMO > PDP Simplification > Standard Upgrade > Migration > NY customers > Display");
		Reporter.log("Data Condition | IR STD PAH Customer, NY customers, with device protection");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select a more expensive device (different tier) | Product details page should be displayed");
		Reporter.log("7. Select EIP | EIP is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button | Insurance migration page should be displayed");
		Reporter.log(
				"11. Verify header 'Your new phone requires higher level of protection. Let's keep it protected.' | 'Your new phone requires higher level of protection. Let's keep it protected.'  should be displayed");
		Reporter.log(
				"12. Verify current SOC status as 'Previous'| 'current SOC status as 'Previous' should be displayed");
		Reporter.log("13. Verify the Previous SOC name| the Previous SOC name should be displayed");
		Reporter.log(
				"14. Verify  Strikethrough treatment for pricing | Strikethrough treatment for pricing should be displayed");
		Reporter.log(
				"15. Verify 'Protection 360' as a recommended SOC with pricing and description| 'Protection 360' as a recommended SOC with pricing and description should be displayed");
		Reporter.log(
				"16. Verify  radiobuttons for all recommended SOCs | radiobutton for radiobuttons for all recommended SOCs should be displayed");
		Reporter.log(
				"17. Verify 'I don't need protection for my phone' radio button |  'I don't need protection for my phone' radio button should be displayed");
		Reporter.log("18. Verify legal text for  NY customers | Legal text for  NY customers should be displayed");
		Reporter.log(
				"19. select 'I don't need protection for my phone.' and click on continue button| decline insurance modal pop up window should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.verifyHighLevelProtectionTitle();
		deviceProtectionPage.verifyPrevoiusSOCStatus();
		deviceProtectionPage.verifyPrevoiusSOCName();
		deviceProtectionPage.verifyPrevoiusSOCPrice();
		deviceProtectionPage.verify360SOCName();
		deviceProtectionPage.verify360SOCPrice();
		deviceProtectionPage.verify360SOCDescription();
		deviceProtectionPage.verifyIDOntNeedProtectionSOC();
		deviceProtectionPage.verifyRadioButtonsPresent();
		deviceProtectionPage.verifyLegalTextForNYCustomers();
		deviceProtectionPage.clickDontNeedProtectionRadioButton();
		deviceProtectionPage.clickContinueButton();
		deviceProtectionPage.verifyDeclineModalPopUpWindow();
	}

	/**
	 * US356518: myTMO > PDP Simplification > Migration > NY > Protection 360
	 * Selected
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testNYCustomerProtection360Migration(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US356518:myTMO > PDP Simplification > Migration > NY > Protection 360 Selected");
		Reporter.log("Data Condition | NY Customer. Device have protection");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select Device from different tier | PDP Page is Displayed");
		Reporter.log("6. Select Monthly payments | Monthly payments is selected");
		Reporter.log("7. Click Add to Cart | Line Selector Page should be displayed");
		Reporter.log("8. Select line | Trade In page should be displayed");
		Reporter.log("9. Select 'Continue Trade In this phone' CTA | Device condition Page should be displayed");
		Reporter.log("10. Select Device condition and click continue button | Trade-in Value Page should be displayed");
		Reporter.log("11. Click 'Continue Trade In this phone' button | Insurance migration page should be displayed");
		Reporter.log("12. Verify Protection 360 as a recommended SOC | Protection 360 SOC should be Displayed");
		Reporter.log("13. Verify Protection 360 price | Price for 360 SOC should be Displayed");
		Reporter.log("14. Verify Protection 360 description | Description for 360 SOC should be Displayed");
		Reporter.log(
				"15. Verify Protection 360 is selected be default | Protection 360 SOC should be selected by default");
		Reporter.log("16. Verify Extended warranty | Extended warranty should be Displayed");
		Reporter.log("17. Verify Extended warranty price | Price for Extended warranty SOC should be Displayed");
		Reporter.log(
				"18. Verify Extended warranty description | Description for Extended warranty SOC should be Displayed");
		Reporter.log("19. Verify Insurance only soc | Insurance only soc should be Displayed");
		Reporter.log("20. Verify Insurance only price | Price for Insurance only SOC should be Displayed");
		Reporter.log("21. Verify Insurance only description | Description for Insurance only SOC should be Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.selectDevice();
		TradeinDeviceConditionPage tradeInDeviceConditionPage = new TradeinDeviceConditionPage(getDriver());
		tradeInDeviceConditionPage.verifyOldTradeinDeviceConditionPage();
		tradeInDeviceConditionPage.clickContinueButton();
		TradeInConfirmationPage tradeInConfirmationPage = new TradeInConfirmationPage(getDriver());
		tradeInConfirmationPage.verifyTradeInConfirmationPage();
		tradeInConfirmationPage.clickContinueTradeInButton();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.verify360SOCName();
		deviceProtectionPage.verify360SOCPrice();
		deviceProtectionPage.verify360SOCDescription();
		deviceProtectionPage.verify360SOCIsSelected();
		deviceProtectionPage.verifyExtendedWarrantySOCPresent();
		deviceProtectionPage.verifyExtendedWarrantySOCDescriptionPresent();
		deviceProtectionPage.verifyExtendedWarrantySOCPricePresent();
		deviceProtectionPage.verifyInsuranceOnlySOCPresent();
		deviceProtectionPage.verifyInsuranceOnlySOCDescriptionPresent();
		deviceProtectionPage.verifyInsuranceOnlySOCPricePresent();
	}

	/**
	 * US300205: PHP: Bullet-ed description
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testBulletedDescriptionIndeviceProtectionPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US300205: PHP: Bullet-ed description");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select FRP | FRP is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line |  Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button |  Insurance migration page should be displayed");
		Reporter.log("11. Verify Bulleted descriptions points |  Bulleted descriptions points should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

	}

	/**
	 * US406461:PHP page AAL - NON NY (Enrollment)
	 * US414675:Suppress Accessories - Phone purchase and BYOD
	 * US377613 - AAL - Known intent Blocked path - lead Eligible (max soc) plan
	 * US406462:PHP page AAL - Non NY (Enrollment)
	 * @param data(425) 435-7091
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
	public void testAALFlowPHPNonNYEnrollmentChangesWithNoProtection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US406461: PHP page AAL - NON NY (Enrollment)");
		Reporter.log("Data Condition | NON NY PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Click on Shop tab | User Should be navigated to Shop home page");
		Reporter.log("4. Click on ADD a Line(From Quick links) | Device intent page should be displayed");
		Reporter.log("5. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("6. Select a product from Product list page | Product display page should be displayed");
		Reporter.log(
				"7. Select variant/Color and click on 'Add to Cart' button on product display page | Consolidated Charges page should be displayed");
		Reporter.log(
				"8. Click 'Choose a phone' button on Consolidated charges page | Premium handset protection(PHP) page should be displayed");
		Reporter.log(
				"9. Verify Protection <360> SOC for NON NY customer and selected by default | Protection <360> SOC selected by  default");
		Reporter.log("10. Verify Title  | Title should be displayed");
		Reporter.log("11. Verify Title description | Title description  should be displayed");
		Reporter.log("12. Verify see more options link  | See more options link should be displayed");
		Reporter.log(
				"13. Click on see more options link  | More options should be loaded and top one is selected by default");
		Reporter.log("14. Click on I dont want protection  | i dont want protection should be selected");
		Reporter.log("15. Click on Continue button  | Cart page should be displayed");
		Reporter.log("16. Verify Cart page should display with without soc | NO soc should be displayed in Cart page");
		
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");

		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.clickBuyANewPhoneOption();
		
		ConsolidatedRatePlanPage ratePlanpage = navigateToRatePlanPageFromAALBuyNewPhoneFlow(myTmoData);
		ratePlanpage.verifyConsolidatedRatePlanPage();
		ratePlanpage.clickOnContinueCTA();

		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.clickDeviceByName(myTmoData.getDeviceName());

		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.clickAddToCart();

		InterstitialTradeInPage interstitialTradeInPage=new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.clickOnSkipTradeInCTA();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();

		deviceProtectionPage.clickOnSeeMoreOptions();
		deviceProtectionPage.verify360SOCIsSelected();
		deviceProtectionPage.clickDontNeedProtectionRadioButton();
		deviceProtectionPage.clickContinueButton();
		deviceProtectionPage.clickYesIamSureButton();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyNoDeviceProtection();
		
	}

	/**
	 * US406462:PHP page AAL - NY (Enrollment)
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION})
	public void testAALFlowPHPNYEnrollmentChangesWithProtection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US406462: PHP page AAL -  NY (Enrollment)");
		Reporter.log("Data Condition | NY PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Click on Shop tab | User Should be navigated to Shop home page");
		Reporter.log("4. Click on ADD a Line(From Quick links) | Device intent page should be displayed");
		Reporter.log("5. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("6. Select a product from Product list page | Product display page should be displayed");
		Reporter.log(
				"7. Select variant/Color and click on 'Add to Cart' button on product display page | Consolidated Charges page should be displayed");
		Reporter.log(
				"8. Click 'Choose a phone' button on Consolidated charges page | Premium handset protection(PHP) page should be displayed");
		Reporter.log("9. Verify Title  | Title should be displayed");
		Reporter.log("10. Verify Title description | Title description  should be displayed");
		Reporter.log(
				"11. Verify Multiple SOC's and first one selected by default | Top soc should be selected by default");
		Reporter.log("15. Verify i dont want protection  | i dont want protection should be displayed");
		Reporter.log("16. Click on Continue button  | Cart page should be displayed");
		Reporter.log(
				"17. Verify Cart page should display with selected soc | Selected soc should be displayed in Cart page");
		
		navigateToCartPageFromAALNonBYODFlow(myTmoData);

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyDeviceProtectionSelectedSoc();
	}
	
	/**
	 * US485312 :Skip Trade in and Continue CTA behavior for trade in page

	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION})
	public void testUserNavigatedToDeviceProtectionPageThroughInterstitialPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test CasetestPAHUserAccessoriesIntegrationChoosingEIPDeviceandOneAccessoriesWithSkipTradeInFlow :US485312 :Skip Trade in and Continue CTA behavior for trade in page");
		Reporter.log("Data Condition | PAH User with AAL Eligible");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("=======testDeviceGoodConditionPagech the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");		
		Reporter.log("4. Click on Shop link | Shop page should be displayed");		
		Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click Contune button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Click Contune button | Interstitial page should be displayed ");				
		Reporter.log("10. Verify Authorable Image | Authorable Image should be displayed ");		
		Reporter.log("11. Verify Trade-In tile 'Yes, I want to trade in a device' | Trade-In tile should be displayed");
		Reporter.log("12. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
		Reporter.log("13. Verify Authorable title | Authorable title  should be displayed");
		Reporter.log("14. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
		Reporter.log("15. Click on Skip trade-in button | PHP page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyAuthorableTradeInImage();
		interstitialTradeInPage.verifyYesTradeinTile();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.verifyContinueButtonEnabled();
		tradeInAnotherDevicePage.clickOnSkipTradeInBtn();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		
	}
	
	/**
	 * US487381 :AAL: Trade In Value - Bad Condition (recycle) display
	 * US488234 :AAL: Trade In Value - Page Title and Body Text and Image display
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION})
	public void testUserSelectBadConditionDeviceUserNavigateToDeviceProtectionPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US487381 :AAL: Trade In Value - Bad Condition (recycle) display");
		Reporter.log("Test Case :US488234 :AAL: Trade In Value - Page Title and Body Text and Image display");
		Reporter.log("Data Condition | PAH User with AAL Eligible");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");		
		Reporter.log("4. Click on Shop link | Shop page should be displayed");		
		Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click Contune button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Click Contune button | Interstitial page should be displayed ");			
		Reporter.log("10. Verify Authorable Image | Authorable Image should be displayed ");		
		Reporter.log("11. Verify Trade-In tile 'Yes, I want to trade in a device' | Trade-In tile should be displayed");
		Reporter.log("12. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
		Reporter.log("13. Verify Authorable title | Authorable title  should be displayed");
		Reporter.log("14. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
		Reporter.log("15. Click on Continue button | Device condition page should be displayed");
		Reporter.log("16. Verify bad condition authorable title | Bad condition authorable title should be displayed");
		Reporter.log("17. Select Bad condition option | Device condition value page should be displayed");
		Reporter.log("18. Verify Bad condition header text contains 'Sorry - due to its condition' | Bad condition header text should be displayed");
		Reporter.log("19. Verify Bad condition legal text contains 'but we be happy to recycle' | Bad condition legal text should be displayed");
		Reporter.log("20. Verify 'Skip trade-in' button | 'Skip trade-in' button should be displayed");
		Reporter.log("21. Verify 'Recycle with us' button | 'Recycle with us' should be displayed");
		Reporter.log("22. Click 'Recycle with us' button | PHP page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyAuthorableTradeInImage();
		interstitialTradeInPage.verifyYesTradeinTile();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.verifyContinueButtonEnabled();
		tradeInAnotherDevicePage.clickContinueButton();

		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneNoButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDNoButton();
		tradeInDeviceConditionValuePage.clickWaterDamageYesButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONNoButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();

		TradeInDeviceConditionConfirmationPage tradeInValuePageConfirmation = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInValuePageConfirmation.verifyTradeInDeviceConditionConfirmationPage();
		tradeInValuePageConfirmation.verifyBadConditionText();
		tradeInValuePageConfirmation.verifyLegaltextInBadConditionDisplayed();
		tradeInValuePageConfirmation.verifySkipTradeInButton();
		tradeInValuePageConfirmation.verifyRecycleWithUsCTADisplayed();
		tradeInValuePageConfirmation.clickRecycleButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
	}
	
	/**
	 * US487382 :AAL: Trade In Value - good/bad condition (skip trade in) display
	 * US488234 :AAL: Trade In Value - Page Title and Body Text and Image display
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION })
	public void testUserSelectGoodConditionDeviceUserNavigateToDeviceProtectionPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US487382 :AAL: Trade In Value - good/bad condition (skip trade in) display");
		Reporter.log("Test Case :US488234 :AAL: Trade In Value - Page Title and Body Text and Image display");
		Reporter.log("Data Condition | PAH User with AAL Eligible and Eligible for Trade-In");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");		
		Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click Contune button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Click Contune button | Interstitial page should be displayed ");					
		Reporter.log("10. Verify Authorable Image | Authorable Image should be displayed ");		
		Reporter.log("11. Verify Trade-In tile 'Yes, I want to trade in a device' | Trade-In tile should be displayed");
		Reporter.log("12. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
		Reporter.log("13. Verify Authorable title | Authorable title  should be displayed");
		Reporter.log("14. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
		Reporter.log("15. Click on Continue button | Device condition page should be displayed");
		Reporter.log("16. Verify good condition authorable title | Good condition authorable title should be displayed");
		Reporter.log("17. Select good condition option | Device condition value page should be displayed");
		Reporter.log("18. Verify good condition header text contains 'Thanks your estimated trade-in credit is XX.XX' | Good condition header text should be displayed");
		Reporter.log("19. Verify Device Trade-in promotion value | Trade-in promotion value should be displayed");
		Reporter.log("20. Verify Device Trade-in promotion legal text points | Trade-in promotion legal text points should be displayed");
		Reporter.log("21. Verify 'Skip trade-in' button | 'Skip trade-in' button should be displayed");
		Reporter.log("22. Verify 'Trade in this phone' button | 'Trade in this phone' should be displayed");
		Reporter.log("23. Click 'Skip trade-in' button | PHP page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyAuthorableTradeInImage();
		interstitialTradeInPage.verifyYesTradeinTile();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();

		TradeInConfirmationPage tradeInConfirmationPage = new TradeInConfirmationPage(getDriver());
		tradeInConfirmationPage.verifyTradeInConfirmationPage();
		tradeInConfirmationPage.verifyEligibilityMessage();
		tradeInConfirmationPage.verifyTradeInValueMessageBulletPoints();
		tradeInConfirmationPage.verifyLegaltextDisplayed();
		tradeInConfirmationPage.verifySkipTradeInButton();
		tradeInConfirmationPage.verifyTradeInThisPhoneButtonDisplayed();
		tradeInConfirmationPage.clickOnNoThanksChangeMyMindButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
	}
	
	
	/**
	 * US552570 :Galaxy Fold : Device protection migration	
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testGalaxyFoldDiviceInDeviceProtectionMigration(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US552570 :Galaxy Fold : Device protection migration");		
		Reporter.log("Data Condition | PAH User should already have line with protection");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");		
		Reporter.log("5. Click on See all phone link | PLP page should be displayed");
		Reporter.log("6. Select on Galaxy Fold device | PDP page should be displayed");
		Reporter.log("7. Click on Add to cart button | LineSelector  page should be displayed");
		Reporter.log("8. Select line | LineSelector detals  page should be displayed");
		Reporter.log("9. Click on Skip tradein CTA | Device protection page should be displayed");		
		Reporter.log("10. Verify Ineligible for protection message | Ineligible for protection message should be displayed");
		Reporter.log("11. Verify decline protection service option check box is unchecked | Decline protection service option check box should be unchecked");
		Reporter.log("12. Verify Previous insurance Soc | Previous insurance Soc should be displayed");
		Reporter.log("13. Verify 'New' verbiage | 'New' verbiage should not be displayed");		
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyIneligiblProtectionMessageHeader();
		deviceProtectionPage.verifyProtectionServiceOptionCheckBoxUnChecked();
		deviceProtectionPage.verifyNewTextVerbiage();		
		deviceProtectionPage.verifyPrevoiusSOCName();
	}
	
	/**
	 * US552569 :Galaxy Fold : Device protection enrollment
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testGalaxyFoldDiviceInDeviceProtectionEnrollment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US552569 :Galaxy Fold : Device protection enrollment");		
		Reporter.log("Data Condition | PAH User without protection on the line");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");		
		Reporter.log("5. Click on See all phone link | PLP page should be displayed");
		Reporter.log("6. Select on Galaxy Fold device | PDP page should be displayed");
		Reporter.log("7. Click on Add to cart button | LineSelector  page should be displayed");
		Reporter.log("8. Select line | LineSelector detals  page should be displayed");
		Reporter.log("9. Click on Skip tradein CTA | Device protection page should be displayed");		
		Reporter.log("10. Verify Ineligible for protection message | Ineligible for protection message should be displayed");
		Reporter.log("11. Verify decline protection service option check box is unchecked | Decline protection service option check box should be unchecked");
		Reporter.log("12. Verify 'New' verbiage | 'New' verbiage should not be displayed");		
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyIneligiblProtectionMessageHeader();
		deviceProtectionPage.verifyProtectionServiceOptionCheckBoxUnChecked();
		deviceProtectionPage.verifyNewTextVerbiage();		
	}
	
	/**
	 * US559491 - Tier6 : Standard Upgrade Migration	
	 * 		C500537: Standard upgrade Migration_ Verify previous Soc price is displayed with Soc name
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TIER_SIX })
	public void testPreviousSocDisplayedInDeviceProtectionPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("TestCase:US559491 - Tier6 : Standard Upgrade Migration");
		Reporter.log("Data Condition | PAH user and User should have Protection Soc");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Deive which is compatible for Tier 6 soc | PDP page should be displayed");
		Reporter.log("6. Click on 'Add to cart' CTA | LineSelection Page  should be displayed ");
		Reporter.log("7. Select a line | LineSelector details page should be displayed");
		Reporter.log("8. Click on Skip trade-in option | PHP page should be displayed");		
		Reporter.log("9. Verify Previous soc and Price | Previous Soc and price should be displayed");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		

		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.verifyPrevoiusSOCName();
		deviceProtectionPage.verifyPrevoiusSOCPrice();
	}
	

}
