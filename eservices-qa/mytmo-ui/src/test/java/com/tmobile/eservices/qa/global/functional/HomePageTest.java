package com.tmobile.eservices.qa.global.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.global.GlobalConstants;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.accounts.FamilyControlsPage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.global.PhonePage;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.UsageOverviewPage;
//import com.tmobile.eservices.qa.pages.shop.DealsHubPage;
import com.tmobile.eservices.qa.pages.shop.PLPPage;
import com.tmobile.eservices.qa.pages.shop.PhonePages;
import com.tmobile.eservices.qa.pages.shop.ShopPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;

public class HomePageTest extends GlobalCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(HomePageTest.class);

	/**
	 * P1_TC_Regression_Desktop_Set up a call Modal TestCases Id : 3241491
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyHomepageSetupCallmodel(ControlTestData data, MyTmoData myTmoData) {

		logger.info("Verify verifyHomepageSetupCallmodel");
		Reporter.log("Test Case : Verify set up call model details");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on call me link | Call me popup should be displayed");
		Reporter.log("5. Verify default option in dropdown | 'No topic selected' should be selected as default option");
		Reporter.log("6. Vefify which number should we call? link | Which number should we call should be displayed");
		Reporter.log("7. Vefify scheduled call me disabled | Schedule call me should be disabled");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickCallmeLink();
		homePage.getTextCallModalHeader();
		homePage.getTextdefaultTopicOptDropdown(GlobalConstants.DEFAULT_TEXT);
		homePage.clickTopicListDropDown();
		homePage.validateDropDownOptions(GlobalConstants.LIST_TOPIC_BILLING);
		homePage.validateDropDownOptions(GlobalConstants.LIST_TOPIC_PLAN);
		homePage.validateDropDownOptions(GlobalConstants.LIST_TOPIC_DEVICE);
		homePage.validateDropDownOptions(GlobalConstants.LIST_TOPIC_ACC_MANG);
		homePage.validateDropDownOptions(GlobalConstants.LIST_TOPIC_OTHER);
		homePage.clickTopicListDropDown();
		if (!(getDriver() instanceof AppiumDriver)) {
			homePage.getTextNumbertoCall(GlobalConstants.WHICH_CALL_NUMBER_TEXT);
			homePage.enterCallMeNumber(GlobalConstants.NUMBER_TO_ENTER);
		}
		if (getDriver() instanceof IOSDriver) {
			homePage.scheduledCallMeDisplay();
		}
		if (!(getDriver() instanceof IOSDriver))
			homePage.scheduledCallMeDisabled();
	}

	/**
	 * P1_TC_Regression_Desktop_HomePage_MSISDN Info
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID, Group.IOS })
	public void verifyBladeuserInfo(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBladeuserInfo P1_TC_Regression_Desktop_HomePage_MSISDN Info");
		Reporter.log("Test Case : Verify blade user information");
		Reporter.log("Test Data Conditions: Only Blade user Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify MSSIDN phone number in my line section| MSSIDN phone number should be displayed");
		Reporter.log("5. Verify profile name | Profile name should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyMissdnInMyLineSection();
		homePage.verifyProfileName();
	}

	/**
	 * TC_Regression_Desktop-Usage Page with Virtual Lines
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyUsagepageWithVirtualLine(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("TC_Regression_Desktop-Usage Page with Virtual Lines");
		Reporter.log("Test Data Conditions: Msisdn with virtual line active");
		Reporter.log("Expected Test Steps:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click usage menu | Usage overview page is displayed should be displayed");
		Reporter.log("5. Verify minutes tab | Balance due amount should be displayed");
		Reporter.log("6. Verify message tab | Message tab should be displayed");
		Reporter.log("7. Verify data tab | Data tab should be displayed");
		Reporter.log("==============================");
		Reporter.log("Actual Output:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickUsageMenu();

		UsageOverviewPage usageOverviewPage = new UsageOverviewPage(getDriver());
		usageOverviewPage.verifyUsageOverviewPage();
		usageOverviewPage.verifyMinutesTab();
		usageOverviewPage.verifyMessageTab();
		usageOverviewPage.verifyDataTab();
	}

	/**
	 * Ensure user can navigate to each page via the header navigation; Home,
	 * Billing, Usage, Phone, & Shop
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyHeaderNavigationLinks(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyHeaderNavigationLinks method called in HomePageTest");
		Reporter.log("Test Case : Verify header navigation links redirect to related pages");
		Reporter.log("Test Data Conditions: Any Msisdn.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on billing tab | Billing summary page should be displayed");
		Reporter.log("5. Click on usage link | Usage overview page should be displayed");
		Reporter.log("7. Click on phone tab | Phone page should be displayed");
		Reporter.log("8. Verify shop unav menu | Shop unav menu should be displayed");
		Reporter.log("9. Click on shop tab | Shop page should be displayed");
		Reporter.log("========================");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickBillingLink();

		BillAndPaySummaryPage BillAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
		BillAndPaySummaryPage.verifyPageLoaded();

		homePage.clickUsageMenu();

		UsageOverviewPage usageOverviewPage = new UsageOverviewPage(getDriver());
		usageOverviewPage.verifyUsageOverviewPage();

		homePage.clickPhoneMenu();

		PhonePages phonePages = new PhonePages(getDriver());
		phonePages.verifyPhonesPage();

		homePage.verifyShopUnavMenu();
		homePage.clickShoplink();

		ShopPage newShopPage = new ShopPage(getDriver());
		newShopPage.verifyNewShoppage();
	}

	/**
	 * US592458:myTMO - update web alerts endpoints for Card Engine
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", groups = { Group.DESKTOP, Group.ANDROID, Group.IOS, Group.PROFILE })
	public void verifyEnsureAlertsAreProperlyDisplayed(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US592458:myTMO - update web alerts endpoints for Card Engine");
		Reporter.log("================================");

		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify web alerts | Alert should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyWebAlerts();
	}

	/**
	 * US139946 DIGITS - Remove Show/Hide Logic for Manage Multi-Line Settings [D]
	 * US139947 DIGITS - Remove Show/Hide Logic for Manage Multi-Line Settings [M]
	 * US140233 DIGITS - Add Link for Manage Multi-Line Settings for all Lines [D]
	 * US140234 DIGITS - Add Link for Manage Multi-Line Settings for all Lines [M]
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyMultiLineSettingsLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyMultiLineSettingsLink method in DiagnosticsTest");
		Reporter.log(
				"Test case: DIGITS - Add Link for Manage Multi-Line Settings for all Lines without any show/hide functionality.");
		Reporter.log("Test Data Conditions: Msisdn with virtual line active");
		Reporter.log(GlobalConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone menu | Phone page should be displayed");
		Reporter.log("5. Verify multi line settings link | Multiline settings link should be displayed.");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickPhoneMenu();
		PhonePages phonesPage = new PhonePages(getDriver());
		phonesPage.verifyPhonesPage();
		phonesPage.veifyMultiLineSettingsLink();
	}

	/**
	 * US166922 DIGITS - Change Copy on Phone Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testVirtualLineOnLineSelectorInPhonePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test case: DIGITS - Change Copy on Phone Page - verify virtual line text in line selector is removed in phone page");
		Reporter.log("Test Data Conditions: Any Msisdn with DIGITS active");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Phone link | Phone page should be displayed");
		Reporter.log(
				"5. Click on Line selector and verify virtual line name | Line selector drop down should be displayed and Virtual line text should not be displayed.");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickPhoneMenu();
		PhonePages phonePage = new PhonePages(getDriver());
		phonePage.verifyPhonesPage();
		phonePage.clickLineselectorDropdown();
		phonePage.verifyVirtualLineNameDisplayed();
	}

	/**
	 * US254502:Search | Autocomplete Search Phrases
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testAutoCompleteSearchFunctionality(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US478004: Search | Replace Search Field with Search Text | Implement and testing");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Login to the application | User Should be login successfully");
		Reporter.log("3.Verify Home page | Home page should be displayed");
		Reporter.log(
				"4.Enter any character and verify autocomplete list of results having the matching character| All the results loaded should have the matching character");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.enterSearchData(GlobalConstants.SEARCH_TEXT_DATA);
		homePage.verifyAutoCompleteSearchResultsForHomePage(GlobalConstants.SEARCH_TEXT_DATA);
	}

	/**
	 * US420581:Home Page | UI | Global Elements
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testHomePageGlobalElements(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US478004: Search | Replace Search Field with Search Text | Implement and testing");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Login to the application | User Should be login successfully");
		Reporter.log("3.Verify Home page | Home page should be displayed");
		Reporter.log("4.Verify welcome text | Welcome text should be displayed");
		Reporter.log("5.Verify customer account number | Customer account number should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyWelcomeText();
		homePage.verifyCustomerAccountNumber();
	}

	/**
	 * US420632:Home Page | UI | Account Component
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testHomePageMySectionAccountComponent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US420632: Home Page |  UI | Account Component");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify my line section | My line section should be displayed");
		Reporter.log("5. Verify missdn in my line section | Missdn in my line section should be displayed");
		Reporter.log("6. Verify view account CTA | View account CTA should be displayed");
		Reporter.log("7. Click view account CTA | Account overview page should be displayed");
		Reporter.log("8. Click tmobile icon | Home page should be displayed");
		Reporter.log("9. Verify device image in my line section | Device image in my line section should be displayed");
		Reporter.log("10.Verify upgrade CTA in my line section | Upgrade CTA in my line section should be displayed");
		Reporter.log("11.Click upgrade CTA in my line section | PLP page should be displayed");
		Reporter.log("12.Click tmobile icon | Home page should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyMyLineSection();
		homePage.verifyMissdnInMyLineSection();
		homePage.verifyViewAccountTextInMyLineSection();
		homePage.clickViewAccountTextInMyLineSection();

		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		accountOverviewPage.verifyAccountOverviewPage();

		homePage.clickTMobileIcon();
		homePage.verifyHomePage();

		homePage.verifyDeviceImageInMyLineSection();
		homePage.verifyUpgradeCTAInMyLineSection();
		homePage.clickUpgradeCTAInMyLineSection();

		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickOnTMobileIcon();

		homePage.verifyHomePage();
	}

	/**
	 * US494119/US494321/US420667:Home Page | UI | Plan Benefit Tiles part 2
	 * DE223448:Offline stack - Homepage - Copy of Free wifi calling tile is not
	 * TC-688:Ensure benefits section loads for eligible user on home page
	 * displayed
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testHomePagePlanBenefitTiles(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US494119: Home Page |  UI | Plan Benefit Tiles part 2");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Login to the application | User Should be login successfully");
		Reporter.log("3.Verify Home page | Home page should be displayed");
		Reporter.log("4.Verify tmobile tuesday image | TMobile tuesday image should be displayed");
		Reporter.log("5.Verify gogo in flight image | GoGo in flight image should be displayed");
		Reporter.log("6.Verify free wifi calling image | Free wifi calling image should be displayed");
		Reporter.log("7.Verify free wifi calling text | Free wifi calling text should be displayed");
		Reporter.log("8.Verify see more cta | See more cta should be displayed");
		Reporter.log("9.Click see more cta | Benefit redemption page should be displayed");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyPlanBenefitTiles();
		homePage.verifyTMobileTuesdayImg();
		homePage.verifyGoGoInFlightImg();
		homePage.verifyFreeWiFiCallingImg();
		homePage.verifyFreeWiFiText();
		homePage.verifySeeMoreCTA();
		homePage.clickSeeMoreCTA();
		homePage.verifyCurrentPageURL("Benefit redemption page","benefit-redemption");
	}

	/**
	 * US469912:Home Page | UI | Bill Component New (Not One site) CDCDWR2-405
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testHomePageBillComponentSection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US469912:Home Page |  UI | Bill Component New (Not One site)");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify amount due | Amount due should be displayed");
		Reporter.log("5. Verify payment CTA | Payment CTA should be displayed");
		Reporter.log("6. Verify last payment |Last payment should be displayed");
		Reporter.log("7. Verify past due alert | Past due alert should be displayed");
		Reporter.log("8.Verify autopay CTA | Autopay CTA should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyViewBillCTA();
		homePage.verifyAmountDue();
		homePage.verifyPaymentCTA();
		homePage.verifyLastPayment();
		homePage.verifyPastDueAlert();
		homePage.verifyAutoPaySetUpQuickLink();
	}

	/**
	 * CDCDWR2-521
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testAccountHistoryPageFromHomePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click account history from profile dropdown | Account history page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickAccountHistoryMenu();

		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyLogOutOperation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click logout menu | Login page should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickLogOutMenu();

		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPageLoaded();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyNotificationAlerts(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify notification divison | Notification divison should be displayed");
		Reporter.log("5. Click notification dropdown | Notification dropdown should be clicked");
		Reporter.log("6. Verify see all button | See all button should be displayed");
		Reporter.log("7. Click see all button | Account history page should be displayed");

		Reporter.log("========================");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyNotificationDiv();
		homePage.clickNotificationDropDown();
		homePage.verifySeeAllBtn();
		homePage.clickSeeAllBtn();

		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyAccountSuspendAlertForPastDueCustomers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify account suspend alert divison| Account suspend alert divison should be displayed");
		Reporter.log("5. Verify pay now link | Pay now link should be displayed");
		Reporter.log("6. Click pay now link |One time payment page should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyAccountSuspendAlertDiv();
		homePage.verifyPayNowLink();
		homePage.clickPayNowLink();

		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
	}

	/**
	 * US521865:Home Page | Sedona Customer Suspend and Restore Charges Messaging
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX })
	public void verifySuspendedSedonaCustomerRestoreMessage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify account suspend alert divison | Account suspend alert divison displayed");
		Reporter.log("5. Verify service restore message | Service restore message should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyAccountSuspendAlertDiv();
		homePage.verifyServiceRestoreMessage();

	}

	/**
	 * US521865:Home Page | Sedona Customer Suspend and Restore Charges Messaging
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifySuspendedSedonaCustomerRestoreMessageForNoRestorationFee(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify account suspend alert divison | Account suspend alert divison displayed");
		Reporter.log("5. Verify service restore message | Service restore message should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyAccountSuspendAlertDiv();
		homePage.verifyServiceRestoreMessage();

	}

	/**
	 * US517214:Home Page | UI | Last Login
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyLastLoginDiv(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify last login divison  | Last login divison should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyLastLoginDiv();
	}

	/**
	 * US541411:Home Page | Quick Links | Family Allowance
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyFamilyAllowanceQuickLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log(
				"4. Verify family allowance link in quicklinks  | Family allowance link in quicklinks should be displayed");
		Reporter.log("5. Click family allowance quick link | Family controls page should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyFamilyAllowanceInQuickLinks();
		homePage.clickFamilyAllowanceQuickLink();

		FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
		familyControlsPage.verifyFamilyControlsPage();
	}

	/**
	 * DE214009:Account History Alerts_Device blocked or suspended alerts are not
	 * redirecting to the exact line which got suspended or blocked on phone page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testAccountHistoryPageAlertsNavigationForSuspendedLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click account history from profile dropdown | Account history page should be displayed");
		Reporter.log("5. Fetch suspened  account missdn details | Suspened account missdn  should be fetched");
		Reporter.log("6. Click suspened account alert | Phone page should be displayed");
		Reporter.log(
				"7. Verify suspened account missdn matched with line selector dropdown missdn | Suspened account missdn should match with line selector dropdown missdn");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickAccountHistoryMenu();

		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();

		String suspenedLineMissdn = accountHistoryPage.getSuspendedLineMissdn();
		accountHistoryPage.clickSuspendedLineAlert();

		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		phonePage.verifySuspendedMissdnNumberInLineSelectorDropDown(suspenedLineMissdn);
	}

	/**
	 * US547897/US558900:Home Page | MPM device details API | NG Changes
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testDeviceImageForKnownIMEI(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify image for known IMEI devices | Image should be displayed for known IMEI devices");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyImageForKnownIMEI();
	}

	/**
	 * US547897/US558900:Home Page | MPM device details API | NG Changes
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testDeviceImageForUnKnownIMEI(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log(
				"4. Verify gray image for unknown IMEI devices | Gray image should be displayed for unknown IMEI devices");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyGrayImageForUnknownIMEIDevices();
	}

	/**
	 * US558895:Home Page | PA Eligibility Check | Testing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyPaymentArrangementCTAForPAEligibleCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify payment arrangement cta | Payment Arrangement cta should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyPaymentArrangementCTA();
	}

	/**
	 * US558895:Home Page | PA Eligibility Check | Testing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyPaymentArrangementCTAForPANotEligibleCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify payment arrangement cta | Payment Arrangement cta should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyPaymentArrangementCTANotDisplayed();
	}

	/**
	 * DE228577:MyTMO Desktop: Unexpected PA link on Homepage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyPaymentArrangementCTAForSedonaCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Sedona Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify payment arrangement cta | Payment Arrangement cta should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyPaymentArrangementCTANotDisplayed();
	}

	/**
	 * DE223677:offline stack: Non-master user_Change plan, AAL, Manage data add on
	 * links are available at home page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyHomePageQuickLinkForNonMaster(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Non master Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify change my plan quicklink | Change my plan quicklink should not be displayed");
		Reporter.log(
				"5. Verify add a person or device quicklink | Add a person or device quicklink should not be displayed");
		Reporter.log("6. Verify manage addons quicklink | Manage addons quicklink should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyChangeMyPlanQuickLinkForNonMaster();
		homePage.verifyAddAPersonOrDeviceQuickLinkForNonMaster();
		homePage.verifyManageAddOnsQuickLinkForNonMaster();
	}

	/**
	 * US517230: Home Page | UI | Plan Benefit Tiles | Clickable TC297334
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testTMobileTuesdayPlanBenefitTileClickable(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US494119: Home Page |  UI | Plan Benefit Tiles part 2");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Login to the application | User Should be login successfully");
		Reporter.log("3.Verify Home page | Home page should be displayed");
		Reporter.log(
				"4.Verify the display of tmobile tuesday tile | TMobile tuesday plan benefit tile should be displayed");
		Reporter.log(
				"5.Verify the display of free wifi calling tile | Free wifi calling plan benefit tile should be displayed");
		Reporter.log(
				"6.Verify the display of gogo in flight tile | GoGo in flight plan benefit tile should be displayed");
		Reporter.log(
				"7.Verify tmobile tuesday tile is enabled upon hovering(AUTHORED AS CLICKABLE) | Tmobile tuesday tile should be enabled");
		Reporter.log("8.Click on tmobile tuesday tile | tmobile tuesday tile should be clickable");
		Reporter.log(
				"9.Verify it redirects to the URL | Redirected to the TMobile Tuesday placeholder URL successfully upon click");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyTMobileTuesdayTileReg2();
		homePage.verifyFreeWiFiCallingTileReg2();
		homePage.verifyGoGoInFlightImgReg2();
		homePage.verifyTMobileTuesdayEnabled();
		homePage.clickOnTMobileTuesdayTile();
		homePage.verifyRedirectedUrlUponTMobileTuesdayClick();
	}

	/**
	 * US517230: Home Page | UI | Plan Benefit Tiles | Clickable TC297361
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testFreeWiFiCallingPlanBenefitTileClickable(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US494119: Home Page |  UI | Plan Benefit Tiles part 2");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Login to the application | User Should be login successfully");
		Reporter.log("3.Verify Home page | Home page should be displayed");
		Reporter.log(
				"4.Verify the display of free wifi calling tile | Free wifi calling plan benefit tile should be displayed");
		Reporter.log(
				"5.Verify free wifi calling tile is enabled upon hovering(AUTHORED AS CLICKABLE) | Free wifi calling tile should be enabled");
		Reporter.log("6.Click on free wifi calling tile | Free wifi calling tile should be clickable");
		Reporter.log(
				"7.Verify it redirects to the URL | Redirected to the Free WiFi Calling placeholder URL successfully upon click");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyFreeWiFiCallingTileReg2();
		homePage.verifyFreeWiFiCallingEnabled();
		homePage.clickOnFreeWiFiCallingTile();
		homePage.verifyRedirectedUrlUponFreeWiFiCallingClick();
	}

	/**
	 * US517230: Home Page | UI | Plan Benefit Tiles | Clickable TC297563
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testGoGoInFlightPlanBenefitTileClickable(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US494119: Home Page |  UI | Plan Benefit Tiles part 2");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Login to the application | User Should be login successfully");
		Reporter.log("3.Verify Home page | Home page should be displayed");
		Reporter.log(
				"4.Verify the display of GoGo in flight tile | GoGo in flight plan benefit tile should be displayed");
		Reporter.log(
				"5.Verify GoGo in flight tile is enabled upon hovering(AUTHORED AS CLICKABLE) | GoGo in flight tile should be enabled");
		Reporter.log("6.Click on GoGo in flight tile | GoGo in flight tile should be clickable");
		Reporter.log(
				"7.Verify it redirects to the URL | Redirected to the GoGo in flight placeholder URL successfully upon click");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyGoGoInFlightImgReg2();
		homePage.verifyGoGoInFlightEnabled();
		homePage.clickOnGoGoInFlightTile();
		homePage.verifyRedirectedUrlUponGoGoInFlightClick();
	}

	/**
	 * DE224871:Sedona Customer is not displayed Restore from Suspend Charge and
	 * taxes
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void verifySuspendChargeMessageForSedonaCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("DE224871:Sedona Customer is not displayed Restore from Suspend Charge and taxes");
		Reporter.log("Test Data Conditions: Sedona customer with past due");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Veirfy suspended account restore message | Account restore message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyAccountRestoreMessage();
	}

	/**
	 * US580084:Home Page | Disable Links to New Account Pages
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyDisableLinksToNewAccountPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US580084:Home Page | Disable Links to New Account Pages");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify my line section | My line section should be displayed");
		Reporter.log(
				"5. Verify view account text in my line section | View account text in my line section should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyMyLineSection();
		homePage.verifyViewAccountTextInMyLineSection();
	}

	/**
	 * DE250289-MyTMO [Account Hist.] Small BC accounts unable to access account
	 * history
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void testAccountHistoryPageFromHomePageForB2BAccount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click account history from profile dropdown | Account history page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickAccountHistoryMenu();

		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
	}

	/**
	 * CDCDWR-287 - Home Page | suppress payment arrangement link when user has
	 * auto-pay enabled
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testPaymentArrangementLink(MyTmoData myTmoData) {

		Reporter.log("CDCDWR-287 - Home Page | suppress payment arrangement link when user has auto-pay enabled");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify payment Arrangement Link | Link should be hidden successfully");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyPaymentLinkIsHidden();
	}

	/**
	 * CDCDWR-368:Home Page Banner | Legal Copy Modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void testHomePageBanner(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-368:Home Page Banner | Legal Copy Modal");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify home banner legal terms link | Legal terms link should be displayed");
		Reporter.log("5. Click home banner legal terms link | Legal terms modal window should be displayed");
		Reporter.log("6. Verify modal window title | Modal window title should be displayed");
		Reporter.log("7. Verify modal window close cta | Modal window close cta should be displayed");
		Reporter.log("8. Click modal window close cta | Modal window close cta should be success");
		Reporter.log("9. Verify modal window | Modal window should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyLegalTermsLink();
		homePage.clickLegalTermsLink();
		homePage.verifyLegalTermsModalWindow();
		homePage.verifyModalWindowTitle();
		homePage.verifyModalWindowdCloseCTA();
		homePage.clickModalWindowdCloseCTA();
		homePage.verifyLegalTermsModalWindowNotDisplayed();
	}

	/**
	 * US512577:Home Page | IQ Users | Hide Autopay
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyAutopayLinkForIQUsers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify autopay in quicklinks  | Autopay in quicklinks should not be displayed");
		Reporter.log(
				"5. Verify autopay link in billing component | Autopay link in billing component should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyAutopayQuickLink();
		homePage.verifyAutopayLinkAtBillingComponent();

	}

	/**
	 * US521840:Home Page | Hide Pmt Arrangement for IQ Customer | Automation
	 * Testing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyPaymentArrangementCTAForIQCustomerWithPastDue(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify autopay in quicklinks  | Autopay in quicklinks should not be displayed");
		Reporter.log(
				"5. Verify payment arrangement cta in billing component | Payment arrangement cta in billing component should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyAutopayLinkAtBillingComponent();

	}

	/**
	 * US521840:Home Page | Hide Pmt Arrangement for IQ Customer | Automation
	 * Testing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyPaymentArrangementCTAForIQCustomerWithNoPastDue(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify autopay in quicklinks  | Autopay in quicklinks should not be displayed");
		Reporter.log(
				"5. Verify payment arrangement cta in billing component | Payment arrangement cta in billing component should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyAutopayLinkAtBillingComponent();

	}

	/**
	 * US521840:Home Page | Hide Pmt Arrangement for IQ Customer | Automation
	 * Testing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyPaymentArrangementCTAForNonIQCustomerWithPastDue(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log(
				"5. Verify payment arrangement cta in billing component | Payment arrangement cta in billing component should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyAutopayLinkAtBillingComponent();
	}

	/**
	 * US525014:Home Page | Hide Quick Links for IS Customers
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS  })
	public void verifyHideQuickLinksForISCustomers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US525014:Home Page | Hide Quick Links for IS Customers");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify change my plan quicklink | Change my plan quick should not be displayed");
		Reporter.log("5. Verify manage add on quicklink | Manage add on quicklink should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyChangeMyPlanQuickLinkForIScustomers();
		homePage.verifyManageAddOnsQuickLinkForISCustomers();
	}
	
	
	/**
	 * CDCDWR2-520 - Home Page | Quick Links 
	 * TC-679:Ensure correct Quick links are shown and redirecting to appropriate page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyAvailableQuickLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify quick link navigations in I want to section | Quick links  navigation verification should be success");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyQuickLinksNavigationsFromHomePage();
	}
	
	/**
	 * TC1536:Ensure change plan and Manage data add on links should not be shown on quick links for large business user
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS  })
	public void verifyHideQuickLinksForLargeBusinessUsers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("TC1536:Ensure change plan and Manage data add on links should not be shown on quick links for large business user");
		Reporter.log("Test Data Conditions: Large Business User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify change my plan quicklink | Change my plan quick should not be displayed");
		Reporter.log("5. Verify manage add on quicklink | Manage add on quicklink should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyChangeMyPlanQuickLinkForLargeBusinessCustomers();
		homePage.verifyManageAddOnsQuickLinkForForLargeBusinessCustomers();
	}
	
	/**
	 * CDCDWR2-875 Defects- Mustangs - S23 - EP-18523 - MyTMO AAL Quick link not appearing - Retired
	 */
}