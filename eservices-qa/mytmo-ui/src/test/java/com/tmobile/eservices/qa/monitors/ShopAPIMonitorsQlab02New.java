package com.tmobile.eservices.qa.monitors;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.api.eos.monitors.ShopAPIMonitorsHelperNew;
import com.tmobile.eservices.qa.data.ApiTestData;

public class ShopAPIMonitorsQlab02New extends ShopAPIMonitorsHelperNew {

	@BeforeMethod(alwaysRun = true)
	public void clearTokenMapCommon() {
		tokenMapCommon = null;
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeFRPFlowQlab02(ApiTestData apiTestData) {
		standardUpgradeFRPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeEIPFlowQlab02(ApiTestData apiTestData) {
		standardUpgradeEIPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeAddSocsEIPFlowQlab02(ApiTestData apiTestData) {
		standardUpgradeAddSocsEIPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeAddAccessoriesEIPFlowQlab02(ApiTestData apiTestData) {
		standardUpgradeAddAccessoriesEIPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeTradeInEIPFlowQlab02(ApiTestData apiTestData) {
		standardUpgradeTradeInEIPFlow(apiTestData);
	}

	//@Test(dataProvider = "byColumnName", enabled = true)
	public void AALPhoneOnEIPNoPDPSOCQlab02(ApiTestData apiTestData) {
		aalPhoneOnEIPNoPDPSOC(apiTestData);
	}

	//@Test(dataProvider = "byColumnName", enabled = true)
	public void AAL_FRP_PDP_NY_WithDepositQlab02(ApiTestData apiTestData) {
		aal_FRP_PDP_NY_WithDeposit(apiTestData);
	}

	//@Test(dataProvider = "byColumnName", enabled = true)
	public void AAL_EIP_PDP_WithSecurityDepositQlab02(ApiTestData apiTestData) {
		aal_EIP_PDP_WithSecurityDeposit(apiTestData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardBYODFlowQlab02(ApiTestData apiTestData) {
		BYODFlow(apiTestData);
	}
}
