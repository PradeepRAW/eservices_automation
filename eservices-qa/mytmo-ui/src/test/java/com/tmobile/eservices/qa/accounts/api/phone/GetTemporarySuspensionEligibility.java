
package com.tmobile.eservices.qa.accounts.api.phone;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetTemporarySuspensionEligibility {

	String environment;

	@Test(groups = "phone")
	public void getTemorarySuspentionEligibility() {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();

		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("interactionId", "123456787");
		request.header("workflowId", "1234");
		request.header("suspensionCount", "7");
		request.header("suspensionDuration", "364");
		request.header("sender_id", "MYTMO");
		request.header("application_id", "ESERVICE");
		request.header("channel_id", "WEB");
		request.contentType("application/json");

		JSONObject requestParams = new JSONObject();
		JSONArray array = new JSONArray();
		array.put("3072204689");
		requestParams.put("msisdn", array);
		requestParams.put("imei", "");
		requestParams.put("accountNumber", "760739900");
		requestParams.put("productType", "MBB");
		// System.out.println(requestParams.toString());

		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/temporarySuspensionEligibility");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating temporarySuspensionEligibility Operation Response ");
		Reporter.log(
				"Note : /myPhone/temporarySuspensionEligibility Service Internally calls getDeviceUnlockAttribute ");
		Reporter.log("Test data : Account eligible for temporary suspension");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("eligibleForSuspension").contains("true"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}
}
