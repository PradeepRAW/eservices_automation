package com.tmobile.eservices.qa.tmng.functional;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.pages.tmng.functional.TravelComponentPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

public class TravelComponentPageTest extends TmngCommonLib {

    TravelComponentPage travelComponentPage;
    private final static Logger logger = LoggerFactory.getLogger(TravelComponentPageTest.class);

    @Test(enabled = true, groups = { Group.SPRINT })
    public void validateTravelRoamingPage() throws InterruptedException {
        pageInvoke();
        travelComponentPage.validateTravelRoamingHeader("Traveling internationally?")
                            .validateTravelRoamingSubHeadline("Subheadline goes here")
                            .validateTravelRoamingParagraph("With T-Mobile ONE™, you get unlimited texting and data at up to 2G speeds in 210+ countries and destinations—all at no extra cost.  ")
                            .validateTravelRoamingBackgroundImage();
    }

    @Test(enabled = true, groups = { Group.SPRINT })
    public void validateTravelRoamingPageLegalTerms(){
        pageInvoke();
        travelComponentPage.validateRoamingLegalText();
    }

    @Test(enabled = true, groups = { Group.SPRINT })
    public void validateTravelRoamingResultsPage() throws InterruptedException {
        pageInvoke();
        travelComponentPage.inputTravelRoamingLocation("Japan")
                            .searchTravelRoamingLocation()
                            .validateCountrySearchResultsColNames()
                            .validateCountrySearchResultsTable();
    }

    @Test(enabled = true, groups = { Group.SPRINT })
    public void validateCruiseHeader() throws InterruptedException {
        pageInvoke();
        travelComponentPage.validateTravelCruiseHeader("Going on a cruise?")
                            .validateTravelCruiseSubHeadline("Subheadline goes here")
                            .validateTravelCruiseParagraph("As long as you have a capable device, T-Mobile customers can still make calls and send texts from hundreds of cruise ships and ferries.")
                            .validateTravelCruiseBackgroundImage();
    }

    @Test(enabled = true, groups = { Group.SPRINT })
    public void validateCruiseLegalTerms() {
        pageInvoke();
        travelComponentPage.validateCruiseLegalText();
    }

    @Test(enabled = true, groups = { Group.SPRINT })
    public void validateCruiseSearchResultsPage() throws InterruptedException {
        pageInvoke();
        travelComponentPage.inputCruiseName("AIDA: Stella")
                .searchCruise()
                .validateCruiseSearchResultsColNames()
                .validateCruiseSearchResultsTable();
    }


    private void pageInvoke(){
        String env = System.getProperty("environment");
        getDriver().get(env);
        checkPageIsReady();
        travelComponentPage = new TravelComponentPage(getDriver());
    }

}
