package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.UNAVPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

/**
 * @author sputti
 *
 */
public class UNAVPageTest extends TmngCommonLib {

	/***
	 * US467705 UNAV - On Site Search 
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testUNAVSearch(TMNGData tMNGData) {
		Reporter.log("US467705 UNAV - On Site Search");
		Reporter.log("Data Condition | Any Query Parameter for Search Bar ");
		Reporter.log("========================================================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Navigate to T-Mobile UNAV Page | T-Mobile UNAV page is displayed");
		Reporter.log("2. Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log("3. Verify search icon and enter value in Search Text Box and click search icon | Value as Search Query parameter is entered and clicked on search icon");
		Reporter.log("4. Verify Search result section is loading | Search result section loaded successfully");
		Reporter.log("========================================================================");
		Reporter.log("Actual Output:");
		loadTmngURL(tMNGData);
		getDriver().get("http://dmo-tmo-publisher.corporate.t-mobile.com/content/t-mobile/test_unav.html");
		UNAVPage uPage = new UNAVPage(getDriver());
		uPage.verifyUNAVPageLoaded();
		uPage.verifySearchIconIsDisplayed();
		uPage.clickSearchIcon();
		uPage.verifySearchTextBoxIsset();
		uPage.clickSearchIcon1();
	}
	/***
	 * US504325 UNAV Kit
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void UNAVHeaderFooter(TMNGData tMNGData) {
		Reporter.log("US467705 UNAV - On Site Search");
		Reporter.log("Data Condition | Any Query Parameter for Search Bar ");
		Reporter.log("========================================================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Navigate to T-Mobile UNAV Page | T-Mobile UNAV page is displayed");
		Reporter.log("2. Verify UNAV Header Links are loading | Header should be loaded successfully");
		Reporter.log("3. Verify UNAV Footer Links are loading | Footer should be loaded successfully");
		Reporter.log("4. Verify Header Links Navigation | Header Links should be navigate with correct links");
		Reporter.log("5. Verify Footer Links Navigation | Footer Links should be navigate with correct links");
		Reporter.log("========================================================================");
		Reporter.log("Actual Output:");
		loadTmngURL(tMNGData);
		getDriver().get("http://dmo-tmo-publisher.corporate.t-mobile.com/content/t-mobile/test_unav.html");
		UNAVPage uPage = new UNAVPage(getDriver());
		uPage.testHeader();
		uPage.testFooter();
	}
	/***
	 * US236307 IBA - turn off
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testIBATurnOff(TMNGData tMNGData) {
		Reporter.log("US236307 IBA - turn off");
		Reporter.log("========================================================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Navigate to T-Mobile iba Page | T-Mobile iba page is displayed");
		Reporter.log("2. Verify Click on Turn off CTA in IBA Modal | Should Navigate to the IBA turnoff page");
		Reporter.log("========================================================================");
		Reporter.log("Actual Output:");
		loadTmngURL(tMNGData);
		getDriver().get("http://dmo-tmo-publisher.corporate.t-mobile.com/iba?tid=WlEuipbMit/9aQ==");
		UNAVPage uPage = new UNAVPage(getDriver());
		uPage.verifyIBAPageLoaded();
		uPage.clickTurnOff();
		uPage.clickModalTurnOff();
	}
}