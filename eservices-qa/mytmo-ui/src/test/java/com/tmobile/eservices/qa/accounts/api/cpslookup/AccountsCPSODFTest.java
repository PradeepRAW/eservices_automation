package com.tmobile.eservices.qa.accounts.api.cpslookup;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.util.*;

import com.google.common.collect.Lists;
import com.tmobile.eservices.qa.accounts.api.LineDetails.responses.AllCurrentServiceDetails;
import com.tmobile.eservices.qa.accounts.api.LineDetails.responses.CPSLookupODF;
import com.tmobile.eservices.qa.accounts.api.LineDetails.responses.GroupedServices;
import io.restassured.mapper.ObjectMapperType;
import org.hamcrest.number.BigDecimalCloseTo;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.api.eos.JWTTokenApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;


import io.restassured.response.Response;

public class AccountsCPSODFTest extends AccountsApi {

	public Map<String, String> tokenMap;

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testODF_TMObileONEPLUSDATAPLAN(ApiTestData apiTestData) throws Exception {

		Reporter.log(
				"Note : CPSlookUp Service Internally calls getEligiblePasses,getEligibleServices,getEligibleDataServices ");
		Reporter.log("Test data : TMO ONE POOLED Msisdn with  One Plus  Data plan");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		String operationName = "ODF - TMObile ONE PLUS DATA PLAN";

		Reporter.log("Test Case : Validating CPSLookUp Operation Response ");

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			Reporter.log("Response is"+ response.body().asString());
			System.out.println("getOnDeviceFulFillment Response :"+response.body().asString());
			Assert.assertTrue(response.jsonPath().getString("planSoc").contains("TM1TI2"), "Rate Plan Incorrect");

			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.soc").contains("FRLTDATA"));
			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.name").contains("T-Mobile ONE"));
			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.price").contains("0"));
			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.taxTreatment").contains("TI"));
			// Assert.assertTrue(response.jsonPath().getList("dataPassList.soc").size()
			// == 2);
			// assertThat(response.jsonPath().getList("dataPassList.soc"),
			// hasItems("HDVPASS0", "SNPASS24"));
			/*assertThat(response.jsonPath().getList("dataServiceList.soc"),
					hasItems("NODATA", "ESNDATA", "ESNDATA", "ESN10GBDT"));

			assertThat(response.jsonPath().getList("sharedServicesList.soc"),
					hasItems("FAMALL10"));
			assertThat(response.jsonPath().getList("sharedServicesList.name"),
					hasItems("Family Allowances"));*/
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testODF_ONEDATAPLAN(ApiTestData apiTestData) throws Exception {

		Reporter.log(
				"Note : CPSlookUp Service Internally calls getEligiblePasses,getEligibleServices,getEligibleDataServices ");
		Reporter.log("Test data : TMO ONE POOLED Msisdn with  One Plus  Data plan");

		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		String operationName = "ODF _ ONEDATAPLAN";

		logRequest(updatedRequest, operationName);

		Reporter.log("Test Case : Validating CPSLookUp Operation Response ");

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			Assert.assertTrue(response.jsonPath().getString("planSoc").contains("MGPLSFM"), "Rate Plan Incorrect");
			//Assert.assertTrue(response.xmlPath().getString("planSoc").contains("MGPLSFM"), "Rate Plan Incorrect");
			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.soc").contains("NODATA"));
			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.name").contains("No Data"));
			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.price").contains("0"));
			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.taxTreatment").contains("TI"));
			Assert.assertTrue(response.jsonPath().getList("dataPassList").size() == 0);
			/*assertThat(response.jsonPath().getList("sharedServicesList.soc"),
					hasItems("2SNFLXHDP", "FAMALL10", "4SNFLXHDP", "FAMMODE"));
			assertThat(response.jsonPath().getList("sharedServicesList.name"),
					hasItems("2ScreenHD Nfx $12.99 Magenta Plus", "Family Allowances",
							"4ScreenHD Nfx $15.99 Magenta Plus",
							"Family Mode"));*/
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODF_NODATAPLAN(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating CPSLookUp Operation Response ");
		Reporter.log(
				"Note : CPSlookUp Service Internally calls getEligiblePasses,getEligibleServices,getEligibleDataServices ");
		Reporter.log("Test data : TMO ONE POOLED Msisdn with  NO DATA Plan and with tab(not mobile)");

		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		String operationName = "ODF _ NODATAPLAN";
		logRequest(updatedRequest, operationName);

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.soc").contains("MAGDATA"));
			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.name").contains("Magenta"));
			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.price").contains("0"));
			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.taxTreatment").contains("TI"));
			Assert.assertTrue(response.jsonPath().getString("planSoc").contains("MAGENTA"), "Rate Plan Incorrect");
			//Assert.assertTrue(response.jsonPath().getList("dataPassList.soc").size() == 1);

			assertThat(response.jsonPath().getList("dataServiceList.soc"), hasItems("NODATA", "MAGDATA","MG13GBDTA","GLOBAL15"));
			assertThat(response.jsonPath().getList("dataServiceList.name"), hasItems("No Data","Magenta™","13GB Hotspot Data","Global Plus 15GB"));
			/*assertThat(response.jsonPath().getList("sharedServicesList.soc"),
					hasItems("1SNFLXSD", "FAMALL10", "2SNFLXHD", "4SNFLXHD","FAMMODE"));
			assertThat(response.jsonPath().getList("sharedServicesList.name"),
					hasItems("1 Screen SD Netflix $8.99", "Family Allowances",
							"2 Screens HD Netflix $12.99","4 Screens HD Netflix $15.99","Family Mode"));*/
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODF_SC(ApiTestData apiTestData) throws Exception {

		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		String operationName = "ODF _ SC";
		logRequest(updatedRequest, operationName);

		Reporter.log("Test Case : Validating CPSLookUp Operation Response ");
		Reporter.log(
				"Note : CPSlookUp Service Internally calls getEligiblePasses,getEligibleServices,getEligibleDataServices ");
		Reporter.log("Test data : SC");
		response.prettyPrint();

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			System.out.println("output Success");
			logSuccessResponse(response, operationName);
			// Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.soc").contains("NODATA")
			// ||
			// response.jsonPath().getString("currentDataServiceDetails.soc").contains("V2GBXDATA")
			// ||
			// response.jsonPath().getString("currentDataServiceDetails.soc").contains("V2GBXDATA")
			// ||
			// response.jsonPath().getString("currentDataServiceDetails.soc").contains("V2GBDATB")
			// );
			// Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.name").contains("No
			// Data") ||
			// response.jsonPath().getString("currentDataServiceDetails.name").contains("2
			// GB High-Speed Data"));
			// Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.price").contains("0"));
			// Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.taxTreatment").contains("TE"));
			Assert.assertTrue(response.jsonPath().getString("planSoc").contains("UTTVN"), "Rate Plan Incorrect");
			Assert.assertTrue(response.jsonPath().getList("dataPassList.soc").size() == 2);
			assertThat(response.jsonPath().getList("dataPassList.soc"), hasItems("OND1DAYT", "OND7DAYT"));
			assertThat(response.jsonPath().getList("dataServiceList.soc"), hasItems("NODATA", "V2GBXDATA"));

			assertThat(response.jsonPath().getList("sharedServicesList.soc"), hasItems("FAMMODE", "FAMALLOW"));
			assertThat(response.jsonPath().getList("sharedServicesList.name"),
					hasItems("Family Allowances", "Family Mode"));
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODF_TE(ApiTestData apiTestData) throws Exception {

		Reporter.log(
				"Note : CPSlookUp Service Internally calls getEligiblePasses,getEligibleServices,getEligibleDataServices ");
		Reporter.log("Test data :TE ");

		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		String operationName = "ODF_ TE";
		logRequest(updatedRequest, operationName);

		Reporter.log("Test Case : Validating CPSLookUp Operation Response ");

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			System.out.println("output Success");
			logSuccessResponse(response, operationName);
			System.out.println(response.prettyPrint());

			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.taxTreatment").contains("TE"));
			Assert.assertTrue(response.jsonPath().getString("planSoc").contains("LTUNL"), "Rate Plan Incorrect");
			// Assert.assertTrue(response.jsonPath().getList("dataPassList.soc").size()
			// == 1);
			// assertThat(response.jsonPath().getList("dataPassList.soc"),
			// hasItems( "NAT7DAY"));
			assertThat(response.jsonPath().getList("sharedServicesList.soc"), hasItems("FAMALLOW", "FAMMODE"));
			assertThat(response.jsonPath().getList("sharedServicesList.name"),
					hasItems("Family Allowances", "Family Mode"));

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODF_Pappy(ApiTestData apiTestData) throws Exception {

		Reporter.log(
				"Note : CPSlookUp Service Internally calls getEligiblePasses,getEligibleServices,getEligibleDataServices ");
		Reporter.log("Test data : Pappy TI data");

		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		String operationName = "ODF_ Pappy";
		logRequest(updatedRequest, operationName);

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			System.out.println("Output Success");

			Reporter.log("Test Case : Validating CPSLookUp Operation Response ");

			logSuccessResponse(response, operationName);

			Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.taxTreatment").contains("TI"));
			Assert.assertTrue(response.jsonPath().getString("planSoc").contains("1PLS552"), "Rate Plan Incorrect");
			// Assert.assertTrue(response.jsonPath().getList("dataPassList.soc").size()
			// == 1);
			// assertThat(response.jsonPath().getList("dataPassList.soc"),
			// hasItems( "NAT7DAY"));

			assertThat(response.jsonPath().getList("sharedServicesList.soc"),
					hasItems("FAMALLOW", "FAMMODE"));
			assertThat(response.jsonPath().getList("sharedServicesList.name"),
					hasItems("Family Allowances","Family Mode"));
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = "ppp")
	public void testAPIUI(ApiTestData apiTestData) throws Exception {

		
		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);
		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));
		
		String operationName = "ODF_ Pappy";
		
		
		if (response.body().asString() != null && response.getStatusCode() == 200) {
		String rateplanSOC  =  response.jsonPath().getList("ratePlans.code").toString();
		
		
		LinkedList<String> list  = (LinkedList) response.jsonPath().getList("accountAddOns.name");
		
		HashMap<List<?>, List<?>> namesPrice = new HashMap<List<?>, List<?>>();
		
		int sharedAddOnsSize = list.size();
		
		 for (int initialsize=0;  initialsize<= list.size(); initialsize++){
			 
			 String SharedServiceListnames = "sharedServicesList" + "[" +initialsize+ "]"+ ".name";
				
				String SharedServiceListPrice = "sharedServicesList" + "[" +initialsize+ "]"+ ".price";
			 
			 namesPrice.put((response.jsonPath().getList(SharedServiceListnames)),response.jsonPath().getList(SharedServiceListPrice));
		 }
		 
		
//		LinkedList<Object> list =  (LinkedList) response.jsonPath().getList("allCurrentServiceDetails.level");
//		LinkedList productlevelServices = new LinkedList();
//		 for (int i=0; i<= list.size();i++){
//			 
//			String productlevelsocs = "allCurrentServiceDetails" + "[" +i+ "]"+ ".level";
//			
//			String productlevelNames = "allCurrentServiceDetails" + "[" +i+ "]"+ ".name";
//			
//			
//			if((response.jsonPath().getList(productlevelsocs).contains("PRODUCT")))
//			{
//				
//				productlevelServices.add(response.jsonPath().getList(productlevelNames));
//				
//			}
//			
//		 }
//		
//		 LinkedList<Object> list1 =  (LinkedList) response.jsonPath().getList("sharedServicesList.name");
//		 
//		 HashMap namesPrice = new HashMap();
//		 
//		 for (int k=0 ; k<= list1.size(); k++) {
//			 
//			 String SharedServiceListnames = "sharedServicesList" + "[" +k+ "]"+ ".name";
//				
//				String SharedServiceListPrice = "sharedServicesList" + "[" +k+ "]"+ ".price";
//		
//		 
//		 for ( int j=0 ; j<= productlevelServices.size(); j++){
//			 
//			 if( list1.get(k) == productlevelServices.get(j)) {
//				 
//				 namesPrice.put((response.jsonPath().getList(SharedServiceListnames)),response.jsonPath().getList(SharedServiceListPrice));
//				
//			 }
//			
//			 productlevelServices.get(j) ;
//		 }
//		 
//		 }
		
		String requestBody = new ServiceTest().getRequestFromFile("MPMPlans.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response1 = getMPMRatePlans(apiTestData, updatedRequest, rateplanSOC);
		
		response.jsonPath().getList("allCurrentServiceDetails[*].level");
		
			System.out.println("Output Success");

			Reporter.log("Test Case : Validating CPSLookUp Operation Response ");

			logSuccessResponse(response, operationName);

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODFNewNetflixSocPriceDetailsFor1SSDNFLX(ApiTestData apiTestData) throws  Exception{
		Reporter.log("Verify for the new Netflix soc Price details | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		AllCurrentServiceDetails currentServiceDetails = cpsLookupODF.findCurrentServiceDetails("1SSDNFLX");
		assertThat(currentServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		assertThat(currentServiceDetails.getIndicator(), equalTo("NFX"));

		AllCurrentServiceDetails sharedServiceDetails = cpsLookupODF.findSharedServiceDetails("1SSDNFLX");
		assertThat(sharedServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		assertThat(sharedServiceDetails.getIndicator(), equalTo("NFX"));

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Netflix");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("1SSDNFLX", groupServiceDetails.getEligibleServicesByGroup());
		assertThat(eligibleServicesGroup.getPrice(), BigDecimalCloseTo.closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		assertThat(eligibleServicesGroup.getIndicator(), equalTo("NFX"));
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODFNewNetflixSocPriceDetailsForHD2SNFLX(ApiTestData apiTestData) throws  Exception{
		Reporter.log("Verify for the new Netflix soc Price details and Indicator HD2SNFLX | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		AllCurrentServiceDetails currentServiceDetails = cpsLookupODF.findCurrentServiceDetails("HD2SNFLX");
		assertThat(currentServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal("4"), BigDecimal.ZERO));
		assertThat(currentServiceDetails.getIndicator(), equalTo("NFX"));

		AllCurrentServiceDetails sharedServiceDetails = cpsLookupODF.findSharedServiceDetails("HD2SNFLX");
		assertThat(sharedServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(4), BigDecimal.ZERO));
		assertThat(sharedServiceDetails.getIndicator(), equalTo("NFX"));

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Netflix");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("HD2SNFLX", groupServiceDetails.getEligibleServicesByGroup());
		assertThat(eligibleServicesGroup.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(4), BigDecimal.ZERO));
		assertThat(eligibleServicesGroup.getIndicator(), equalTo("NFX"));
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODFNewNetflixSocPriceDetailsForHD4SNFLX(ApiTestData apiTestData) throws  Exception{
		Reporter.log("Verify for the new Netflix soc Price details | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		AllCurrentServiceDetails currentServiceDetails = cpsLookupODF.findCurrentServiceDetails("HD4SNFLX");
		assertThat(currentServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(7), BigDecimal.ZERO));
		assertThat(currentServiceDetails.getIndicator(), equalTo("NFX"));

		AllCurrentServiceDetails sharedServiceDetails = cpsLookupODF.findSharedServiceDetails("HD4SNFLX");
		assertThat(sharedServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(7), BigDecimal.ZERO));
		assertThat(sharedServiceDetails.getIndicator(), equalTo("NFX"));

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Netflix");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("HD4SNFLX", groupServiceDetails.getEligibleServicesByGroup());
		assertThat(eligibleServicesGroup.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(7), BigDecimal.ZERO));
		assertThat(eligibleServicesGroup.getIndicator(), equalTo("NFX"));
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODFNewNetflixSocPriceDetailsFor2SPHDNFLX(ApiTestData apiTestData) throws  Exception{
		Reporter.log("Verify for the new Netflix soc Price details | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		AllCurrentServiceDetails currentServiceDetails = cpsLookupODF.findCurrentServiceDetails("2SPHDNFLX");
		assertThat(currentServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		assertThat(currentServiceDetails.getIndicator(), equalTo("NFX"));

		AllCurrentServiceDetails sharedServiceDetails = cpsLookupODF.findSharedServiceDetails("2SPHDNFLX");
		assertThat(sharedServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		assertThat(sharedServiceDetails.getIndicator(), equalTo("NFX"));

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Netflix");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("2SPHDNFLX", groupServiceDetails.getEligibleServicesByGroup());
		assertThat(eligibleServicesGroup.getPrice(), BigDecimalCloseTo.closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		assertThat(eligibleServicesGroup.getIndicator(), equalTo("NFX"));

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODFNewNetflixSocPriceDetailsFor4SPHDNFLX(ApiTestData apiTestData) throws  Exception{
		Reporter.log("Verify for the new Netflix soc Price details | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		AllCurrentServiceDetails currentServiceDetails = cpsLookupODF.findCurrentServiceDetails("4SPHDNFLX");
		assertThat(currentServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(3), BigDecimal.ZERO));
		assertThat(currentServiceDetails.getIndicator(), equalTo("NFX"));

		AllCurrentServiceDetails sharedServiceDetails = cpsLookupODF.findSharedServiceDetails("4SPHDNFLX");
		assertThat(sharedServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(3), BigDecimal.ZERO));
		assertThat(sharedServiceDetails.getIndicator(), equalTo("NFX"));

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Netflix");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("4SPHDNFLX", groupServiceDetails.getEligibleServicesByGroup());
		assertThat(eligibleServicesGroup.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(3), BigDecimal.ZERO));
		assertThat(eligibleServicesGroup.getIndicator(), equalTo("NFX"));

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODFNewNetflixSocPriceDetailsFor1SNFLXSD(ApiTestData apiTestData) throws  Exception{
		Reporter.log("Verify for the new Netflix soc Price details | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		AllCurrentServiceDetails currentServiceDetails = cpsLookupODF.findCurrentServiceDetails("1SNFLXSD");
		assertThat(currentServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		assertThat(currentServiceDetails.getIndicator(), equalTo("MAP"));

		AllCurrentServiceDetails sharedServiceDetails = cpsLookupODF.findSharedServiceDetails("1SNFLXSD");
		assertThat(sharedServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		assertThat(sharedServiceDetails.getIndicator(), equalTo("MAP"));

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Netflix");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("1SNFLXSD", groupServiceDetails.getEligibleServicesByGroup());
		assertThat(eligibleServicesGroup.getPrice(), BigDecimalCloseTo.closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		assertThat(eligibleServicesGroup.getIndicator(), equalTo("MAP"));

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODFNewNetflixSocPriceDetailsFor2SNFLXHD(ApiTestData apiTestData) throws  Exception{
		Reporter.log("Verify for the new Netflix soc Price details | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		AllCurrentServiceDetails currentServiceDetails = cpsLookupODF.findCurrentServiceDetails("2SNFLXHD");
		assertThat(currentServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(4), BigDecimal.ZERO));
		assertThat(currentServiceDetails.getIndicator(), equalTo("MAP"));

		AllCurrentServiceDetails sharedServiceDetails = cpsLookupODF.findSharedServiceDetails("2SNFLXHD");
		assertThat(sharedServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(4), BigDecimal.ZERO));
		assertThat(sharedServiceDetails.getIndicator(), equalTo("MAP"));

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Netflix");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("2SNFLXHD", groupServiceDetails.getEligibleServicesByGroup());
		assertThat(eligibleServicesGroup.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(4), BigDecimal.ZERO));
		assertThat(eligibleServicesGroup.getIndicator(), equalTo("MAP"));


	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODFNewNetflixSocPriceDetailsFor4SNFLXHD(ApiTestData apiTestData) throws  Exception{
		Reporter.log("Verify for the new Netflix soc Price details | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		AllCurrentServiceDetails currentServiceDetails = cpsLookupODF.findCurrentServiceDetails("4SNFLXHD");
		assertThat(currentServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(7), BigDecimal.ZERO));
		assertThat(currentServiceDetails.getIndicator(), equalTo("MAP"));

		AllCurrentServiceDetails sharedServiceDetails = cpsLookupODF.findSharedServiceDetails("4SNFLXHD");
		assertThat(sharedServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(7), BigDecimal.ZERO));
		assertThat(sharedServiceDetails.getIndicator(), equalTo("MAP"));

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Netflix");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("4SNFLXHD", groupServiceDetails.getEligibleServicesByGroup());
		assertThat(eligibleServicesGroup.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(7), BigDecimal.ZERO));
		assertThat(eligibleServicesGroup.getIndicator(), equalTo("MAP"));


	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODFNewNetflixSocPriceDetailsFor2SNFLXHDP(ApiTestData apiTestData) throws  Exception{
		Reporter.log("Verify for the new Netflix soc Price details | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		AllCurrentServiceDetails currentServiceDetails = cpsLookupODF.findCurrentServiceDetails("2SNFLXHDP");
		assertThat(currentServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		assertThat(currentServiceDetails.getIndicator(), equalTo("MAP"));

		AllCurrentServiceDetails sharedServiceDetails = cpsLookupODF.findSharedServiceDetails("2SNFLXHDP");
		assertThat(sharedServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		assertThat(sharedServiceDetails.getIndicator(), equalTo("MAP"));

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Netflix");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("2SNFLXHDP", groupServiceDetails.getEligibleServicesByGroup());
		assertThat(eligibleServicesGroup.getPrice(), BigDecimalCloseTo.closeTo(BigDecimal.ZERO, BigDecimal.ZERO));
		assertThat(eligibleServicesGroup.getIndicator(), equalTo("MAP"));


	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODFNewNetflixSocPriceDetailsFor4SNFLXHDP(ApiTestData apiTestData) throws  Exception{
		Reporter.log("Verify for the new Netflix soc Price details | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		AllCurrentServiceDetails currentServiceDetails = cpsLookupODF.findCurrentServiceDetails("4SNFLXHDP");
		assertThat(currentServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(3), BigDecimal.ZERO));
		assertThat(currentServiceDetails.getIndicator(), equalTo("MAP"));

		AllCurrentServiceDetails sharedServiceDetails = cpsLookupODF.findSharedServiceDetails("4SNFLXHDP");
		assertThat(sharedServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(3), BigDecimal.ZERO));
		assertThat(sharedServiceDetails.getIndicator(), equalTo("MAP"));

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Netflix");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("4SNFLXHDP", groupServiceDetails.getEligibleServicesByGroup());
		assertThat(eligibleServicesGroup.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(3), BigDecimal.ZERO));
		assertThat(eligibleServicesGroup.getIndicator(), equalTo("MAP"));


	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODFNewNetflixSocPriceDetailsForFAMNFX9(ApiTestData apiTestData) throws  Exception{
		Reporter.log("Verify for the new Netflix soc Price details | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		AllCurrentServiceDetails currentServiceDetails = cpsLookupODF.findCurrentServiceDetails("FAMNFX9");
		assertThat(currentServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(2), BigDecimal.ZERO));
		assertThat(currentServiceDetails.getIndicator(), equalTo("MAP"));

		AllCurrentServiceDetails sharedServiceDetails = cpsLookupODF.findSharedServiceDetails("FAMNFX9");
		assertThat(sharedServiceDetails.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(2), BigDecimal.ZERO));
		assertThat(sharedServiceDetails.getIndicator(), equalTo("MAP"));

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Netflix");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("FAMNFX9", groupServiceDetails.getEligibleServicesByGroup());
		assertThat(eligibleServicesGroup.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(2), BigDecimal.ZERO));
		assertThat(eligibleServicesGroup.getIndicator(), equalTo("MAP"));

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testODFNewNetflixSocPriceDetailsForFAMNFX13(ApiTestData apiTestData) throws  Exception{
		Reporter.log("Verify for the new Netflix soc Price details | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		AllCurrentServiceDetails soc1SSDNFLX = cpsLookupODF.findCurrentServiceDetails("FAMNFX13");
		assertThat(soc1SSDNFLX.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(5), BigDecimal.ZERO));
		assertThat(soc1SSDNFLX.getIndicator(), equalTo("MAP"));

		AllCurrentServiceDetails sharedServiceDetails1SSDNFLX = cpsLookupODF.findSharedServiceDetails("FAMNFX13");
		assertThat(sharedServiceDetails1SSDNFLX.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(5), BigDecimal.ZERO));
		assertThat(sharedServiceDetails1SSDNFLX.getIndicator(), equalTo("MAP"));

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Netflix");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("FAMNFX13", groupServiceDetails.getEligibleServicesByGroup());
		assertThat(eligibleServicesGroup.getPrice(), BigDecimalCloseTo.closeTo(new BigDecimal(5), BigDecimal.ZERO));
		assertThat(eligibleServicesGroup.getIndicator(), equalTo("MAP"));

	}


	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG }, priority = 1)
	public void testNetflixConflictingServices(ApiTestData apiTestData) throws  Exception {
		Reporter.log("Verify for the new Netflix soc Price details | Should be able to verify the price for new Netflix soc ");
		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("CpsLookupODF.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getOnDeviceFulFillment(apiTestData, updatedRequest);

		assertThat(response.body().asString(), notNullValue());
		assertThat(response.getStatusCode(), equalTo(200));

		CPSLookupODF cpsLookupODF = response.body().as(CPSLookupODF.class, ObjectMapperType.JACKSON_2);

		GroupedServices groupServiceDetails = cpsLookupODF.findGroupServiceDetails("Video Streaming");
		AllCurrentServiceDetails eligibleServicesGroup = CPSLookupODF.findServiceDetails("2SPHDNFLX", groupServiceDetails.getEligibleServicesByGroup());
		List<String> conflictingServices = Lists.newArrayList(eligibleServicesGroup.getConflictingServiceList());
		assertThat(conflictingServices, hasItem("4SPHDNFLX"));
	}

}
