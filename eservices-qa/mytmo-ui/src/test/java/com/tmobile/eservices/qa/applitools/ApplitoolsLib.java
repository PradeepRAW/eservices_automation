package com.tmobile.eservices.qa.applitools;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.applitools.eyes.BatchInfo;
import com.applitools.eyes.MatchLevel;
import com.applitools.eyes.RectangleSize;
import com.applitools.eyes.TestResults;
import com.applitools.eyes.selenium.Eyes;

public class ApplitoolsLib{
	
	//static public ApplitoolsLib me = null;
	
	Eyes eyes = new Eyes();
//	BatchInfo batchInfo;
//
//	WebDriver webDriver;
//	String appName;
//	String testName;
//	
//	public ApplitoolsLib(WebDriver webDriver, String appName,String testName, BatchInfo batchInfo) {
//		super();
//		this.webDriver = webDriver;
//		this.appName=appName;
//		this.testName=testName;
//		this.batchInfo = batchInfo;
//	}
	
	public void doVisualTest(WebDriver webDriver, String appName, String pageName, BatchInfo batchInfo) {
		try {
			eyes.setApiKey("VhdKy8IaHANFsMX97zyrTMfKXdwp7InwLvVs9AUhUBHw110");
			eyes.setBatch(batchInfo);
			eyes.setForceFullPageScreenshot(true);
			//eyes.setStitchMode(StitchMode.CSS);
			eyes.setMatchLevel(MatchLevel.LAYOUT);
//			eyes.setSendDom(false); // temporary
			eyes.open(webDriver, appName, pageName, new RectangleSize(800, 600));
			eyes.checkWindow(pageName);
			Thread.sleep(5000);
			TestResults testResults=eyes.close(false);
			System.out.println(testResults.isPassed());
			Assert.assertEquals(testResults, testResults.isPassed());
		} catch (Exception e) {
			System.out.println(e.getMessage());
			Reporter.log(e.getMessage());
		}
	}

//	public void setUpApplitools() {
//		try {
//			eyes.setApiKey("VhdKy8IaHANFsMX97zyrTMfKXdwp7InwLvVs9AUhUBHw110");
//			eyes.setBatch(batchInfo);
//			eyes.setForceFullPageScreenshot(true);
//			eyes.setStitchMode(StitchMode.CSS);
//			eyes.setMatchLevel(MatchLevel.LAYOUT);
//			eyes.setSendDom(false); // temporary
//			eyes.open(webDriver, appName,testName, new RectangleSize(800, 600));
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			Reporter.log(e.getMessage());
//		}
//	}
	
//	public void tearDownApplitools() {
//		try {
//			@SuppressWarnings("unused")
//			TestResults testResults=eyes.close(false);
//			System.out.println("APPLITOOLS ="+testResults.getExactMatches());
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//			Reporter.log(e.getMessage());
//		}
//	}

}
