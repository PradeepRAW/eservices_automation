/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.exception.FrameworkException;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.AddCardPage;
import com.tmobile.eservices.qa.pages.payments.AutoPayPage;
import com.tmobile.eservices.qa.pages.payments.OTPAmountPage;
import com.tmobile.eservices.qa.pages.payments.OTPConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.PaymentArrangementPage;
import com.tmobile.eservices.qa.pages.payments.PaymentErrorPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class OTPConfirmationPageTest extends PaymentCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(OTPConfirmationPageTest.class);

	/**
	 * US217726 Error Handling Module - UI - Duplicate Transaction
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testErrorHandlingModuleDuplicateTransaction(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testErrorHandlingModuleDuplicateTransaction method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. hit on error page url|payment Error page should be displayed");
		Reporter.log("4. Click on dublicate transaction button |  dublicate Transaction error  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		PaymentErrorPage paymenterrorpage = navigateToPaymentErrorPage(myTmoData);
		paymenterrorpage.clickDublicateTrasactionButton();
		paymenterrorpage.verifyDublicateTransactionHeader();
		paymenterrorpage.verifyDublicateTrasactionErrorMessage();
		paymenterrorpage.verifyBackBtnDispalyed();
		paymenterrorpage.verifyUpdatePaymentDetailsBtn();
		Reporter.log(" dublicate Transaction error is displayed");

	}

	/**
	 * US262377 Error Handling Module - UI - Transaction Hold
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testErrorHandlingModuleTransactionHold(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testErrorHandlingModuleTransactionHold method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. hit on error page url|payment Error page should be displayed");
		Reporter.log(
				"4. Click on  transaction hold button for card  |  Transaction hold error for card should be displayed");
		Reporter.log(
				"5. Click on  transaction hold button for bank  |  Transaction hold error for bank should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		PaymentErrorPage paymenterrorpage = navigateToPaymentErrorPage(myTmoData);
		paymenterrorpage.clickTrasactionHoldButtonCard();
		paymenterrorpage.verifyTransactionHoldHeader();
		paymenterrorpage.verifyTrasactionHoldErrorMessageCard();
		paymenterrorpage.verifyPageLoaded();
		paymenterrorpage.clickTrasactionHoldButtonBank();
		paymenterrorpage.verifyTransactionHoldHeader();
		paymenterrorpage.verifyTrasactionHoldErrorMessageBank();
		paymenterrorpage.verifyBackBtnDispalyed();
		paymenterrorpage.verifyUpdatePaymentDetailsBtn();
		Reporter.log("transaction hold error for bank is displayed");

	}

	/**
	 * US217725 Error Handling Module - UI - Unknown Error
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testErrorHandlingModuleUnknownError(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testErrorHandlingModuleUnknownError method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. hit on error page url|payment Error page should be displayed");
		Reporter.log("4. Click on  unknown error button |   unknown  error  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		PaymentErrorPage paymenterrorpage = navigateToPaymentErrorPage(myTmoData);
		paymenterrorpage.clickUnKnownErrorButton();
		paymenterrorpage.verifyUnKnownErrorHeader();
		paymenterrorpage.verifyUnKnownErrorMessage();
		paymenterrorpage.verifyBackBtnDispalyed();
		paymenterrorpage.verifyUpdatePaymentDetailsBtn();

	}

	/**
	 * US217723 Error Handling Module - UI - Rejected Transaction *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testErrorHandlingModuleRejectedTransaction(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testErrorHandlingModuleRejectedTransaction method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. hit on error page url|payment Error page should be displayed");
		Reporter.log(
				"4. Click on rejected transaction Add card button |  Rejected Transaction error for card should be displayed");
		Reporter.log(
				"4. Click on rejected transaction Add bank button |  Rejected Transaction error for bank should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		PaymentErrorPage paymenterrorpage = navigateToPaymentErrorPage(myTmoData);
		paymenterrorpage.clickRejectedTrasactionButtonAddCard();
		paymenterrorpage.verifyRejectedTransactionHeader();
		paymenterrorpage.verifyrejectedTrasactionErrorMessage();
		paymenterrorpage.verifyBackBtnDispalyed();
		paymenterrorpage.verifyUpdatePaymentDetailsBtn();
		getDriver().get("https://deva0b.eservice.t-mobile.com/testPaymentError");
		paymenterrorpage.verifyPageLoaded();
		paymenterrorpage.clickRejectedTrasactionButtonAddBank();
		paymenterrorpage.verifyRejectedTransactionHeader();
		paymenterrorpage.verifyrejectedTrasactionErrorMessage();
		getDriver().get("https://deva0b.eservice.t-mobile.com/testPaymentError");
		paymenterrorpage.verifyPageLoaded();
		paymenterrorpage.clickRejectedTrasactionButtonBank();
		paymenterrorpage.verifyRejectedTransactionHeader();
		paymenterrorpage.verifyrejectedTrasactionErrorMessage();
	}

	/**
	 * US217721 Error Handling Module - UI - Hard Decline
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testErrorHandlingModuleHardDecline(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testErrorHandlingModuleHardDecline method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. hit on error page url|payment Error page should be displayed");
		Reporter.log("4. Click on  hard decline button |    hard decline  error  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		PaymentErrorPage paymenterrorpage = navigateToPaymentErrorPage(myTmoData);
		paymenterrorpage.clickHardDeclineErrorButton();
		paymenterrorpage.verifyHardDeclineErrorHeader();
		paymenterrorpage.verifyHardDeclineErrorMessage();
		paymenterrorpage.verifyBackBtnDispalyed();
		paymenterrorpage.verifyUpdatePaymentDetailsBtn();

	}

	/**
	 *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testErrorHandlingModuleSystemError(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testErrorHandlingModuleSystemError method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. hit on error page url|payment Error page should be displayed");
		Reporter.log("4. Click on  system error button |system   error  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		PaymentErrorPage paymenterrorpage = navigateToPaymentErrorPage(myTmoData);
		paymenterrorpage.clickSystemErrorButton();
		paymenterrorpage.verifySystemErrorHeader();
		paymenterrorpage.verifySystemErrorMessage();
		paymenterrorpage.verifyBackBtnDispalyed();

	}

	// *************chillana's sprint 6 ******************

	/**
	 * US277555
	 * 
	 * AutoPay ON OTP Confirmation Page Alert – Outside Blackout Date
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testAutoPayOnOTPConfirmationPageAlertOutSideBlackoutPeriod(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testAutoPayOnOTPConfirmationPageAlertOutSideBlackoutPeriod method called in OneTimePaymentPageTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on Agree and submit | OTP confirmation page should be displayed");
		Reporter.log(
				"5. verify Autopay alerts on OTP confirmation page | Autopay alerts on OTP confirmation page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OTPConfirmationPage onetimepaymentconfirmationpage = navigateToOTPConfirmationPage(myTmoData);
		onetimepaymentconfirmationpage.verifyAutoPayAlerts();

	}

	/**
	 * US277557 AutoPay ON OTP Confirmation Page Alert – ANYTIME
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DUPLICATE, Group.RELEASE_READY })
	public void testAutoPayOnOTPConfirmationPageAlert(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testAutoPayOnOTPConfirmationPageAlert method called in OneTimePaymentPageTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on amount icon| amount spoke page should be displayed");
		Reporter.log("5. enter amount and submit |OTP confirmation page  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OTPConfirmationPage otpConfirmationPage = navigateToOTPConfirmationPage(myTmoData);
		otpConfirmationPage.verifyAutoPayNotProcessAlerts();
		otpConfirmationPage.clickReturnToHome();
		verifyPage("Home page", "Home");
		removedStoredBankAccounts();
	}

	/**
	 * US283067 GLUE Light Reskin - OTP Confirmation Page
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Condition - Regular user with stored payment method.
	 *            Autopay ON
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testGLROTPConfirmationPageApOn(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Click on amount Icon| amount spoke page should be displayed");
		Reporter.log("Step 5: Setup other amount and update| amount should be updated");
		Reporter.log("Step 6: Click add payment method| payment spoke page should be displayed");
		Reporter.log("Step 7: Click on add Bank| Bank information page should be displayed");
		Reporter.log("Step 8: Fill all bank details and click continue button| OTP page should be displayed");
		Reporter.log("Step 9: Click submit button| OTP confirmation page should be displayed");
		Reporter.log("Step 10: Verify OTP Confirmation page elements| All expected page elements should be displayed.");
		Reporter.log("Step 11: Click Return Home | Home page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		if (!homePage.verifyAutoPay("ON")) {
			setupAutopay(myTmoData);
		}
		displayBillDueDateAndAmount(homePage);
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double payAmount = generateRandomAmount();
		editAmountPage.setOtherAmount(payAmount.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		Long accNo = addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.verifyOTPConfirmationPage();
		otpConfirmationPage.verifyPaymentBankDetails(accNo.toString(), myTmoData.getPayment().getRoutingNumber());
		otpConfirmationPage.clickReturnToHome();
		verifyPage("Home page", "Home");
		removedStoredBankAccounts();
	}

	/**
	 * US283067 GLUE Light Reskin - OTP Confirmation Page
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Regular user. Autopay OFF
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testGLROTPConfirmationPageApOff(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Conditions - Regular user. Autopay OFF");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on amount Icon| amount spoke page should be displayed");
		Reporter.log("Step 5: setup other amount and update| amount should be updated");
		Reporter.log("Step 6: click add payment method| payment spoke page should be displayed");
		Reporter.log("Step 7: click on add Bank| Bank information page should be displayed");
		Reporter.log("Step 8: Fill all bank details and click continue button| OTP page should be displayed");
		Reporter.log("Step 9: click submit button| OTP confirmation page should be displayed");
		Reporter.log("Step 10: Verify OTP page elements| All expected page elements should be displayed.");
		Reporter.log(
				"Step 11: Verify Autopay elements on OTP page| All expected Autopay page elements should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		cancelAutoPay(myTmoData);
		displayBillDueDateAndAmount(homePage);
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double payAmount = generateRandomAmount();
		editAmountPage.setOtherAmount(payAmount.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		Long accNo = addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.verifyOTPConfirmationPage();
		otpConfirmationPage.verifySetupAutoPayBlock();
		otpConfirmationPage.verifyPaymentBankDetails(accNo.toString(), myTmoData.getPayment().getRoutingNumber());
		otpConfirmationPage.clickReturnToHome();
		verifyPage("Home page", "Home");
		removedStoredBankAccounts();
	}

	/**
	 * US262847 Angular + GLUE - OTP - Confirmation Spoke Page (BAU)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEOTPConfirmationSpokePageCardPayment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("testAngularGLUEOTPConfirmationSpokePageCardPayment");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on amount Icon| amount spoke page should be displayed");
		Reporter.log("Step 5: setup other amount and update| amount should be updated");
		Reporter.log("Step 6: click add payment method| payment spoke page should be displayed");
		Reporter.log("Step 7: click on add card| Card information page should be displayed");
		Reporter.log("Step 8: Fill all card details and click continue button| OTP page should be displayed");
		Reporter.log("Step 9: click submit button| OTP confirmation page should be displayed");
		Reporter.log("Step 10: verify all card payment details| Card payment details should be displayed correct.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		displayBillDueDateAndAmount(homePage);
		String balanceAmount = homePage.getBalanceDueAmount();
		if (balanceAmount.equals("Error")) {
			throw new FrameworkException("Balance amount is not displayed on the home page");
		}
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double payAmount = generateRandomAmount();
		editAmountPage.setOtherAmount(payAmount.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyOtpConfirmationPage();
		otpConfirmationPage.verifyPaymentCardDetails();
		otpConfirmationPage.clickReturnToHome();
	}

	/**
	 * US262847 Angular + GLUE - OTP - Confirmation Spoke Page (BAU)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void testAngularGLUEOTPConfirmationSpokePageBankPayment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("testAngularGLUEOTPConfirmationSpokePageBankPayment");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on amount Icon| amount spoke page should be displayed");
		Reporter.log("Step 5: setup other amount and update| amount should be updated");
		Reporter.log("Step 6: click add payment method| payment spoke page should be displayed");
		Reporter.log("Step 7: click on add Bank| Bank information page should be displayed");
		Reporter.log("Step 8: Fill all bank details and click continue button| OTP page should be displayed");
		Reporter.log("Step 9: click submit button| OTP confirmation page should be displayed");
		Reporter.log("Step 10: verify all bank payment details| Bank payment details should be displayed correct.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OTPConfirmationPage otpConfirmationPage = navigateToOTPConfirmationPage(myTmoData);
		// otpConfirmationPage.verifyPaymentBankDetails();
		otpConfirmationPage.clickReturnToHome();
	}

	/**
	 * US262949 Angular + GLUE - AutoPay Pitch Page Layout
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEAutopayPitchPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("testAngularGLUEAutopayPitchPage");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on amount Icon| amount spoke page should be displayed");
		Reporter.log("Step 5: setup other amount and update| amount should be updated");
		Reporter.log("Step 6: click add payment method| payment spoke page should be displayed");
		Reporter.log("Step 7: click on add Bank| Bank information page should be displayed");
		Reporter.log("Step 8: Fill all bank details and click continue button| OTP page should be displayed");
		Reporter.log("Step 9: click submit button| OTP confirmation page should be displayed");
		Reporter.log("Step 10: verify and click Autopay button| Autopay landing page should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OTPConfirmationPage otpConfirmationPage = navigateToOTPConfirmationPage(myTmoData);
		otpConfirmationPage.clickAutoPaySignUpBtn();
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyAutoPayLandingPageFields();
	}

	/**
	 * Ensure payment confirmation page shows correct details
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyPaymentconfirmationPagewithCorrectdetails(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPaymentconfirmationPagewithCorrectdetails method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Ensure payment confirmation page shows correct details");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Select Radio button Stored credit Card | Radio button should be selected ");
		Reporter.log("6. Click next | Review payment page should be loaded");
		Reporter.log("7. Verify Credit Card account number | Credit card number should be displayed correctly");
		Reporter.log("8. Verify Zip code | Zip code will be displayed correctly");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.fillCardInfo(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(myTmoData.getPayment().getCardNumber());
	}

	/**
	 * US356344 PII Masking > Variables on OTP Page
	 * US408163 PII Masking> Stored Payment Methods
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void testPIIMaskingOnOTPPages(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US356344	PII Masking > Variables on OTP Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: verify PII masking | PII masking attribute should be present for payment method blade");
		Reporter.log("Step 5: click on amount Icon| amount spoke page should be displayed");
		Reporter.log("Step 6: setup other amount and update| amount should be updated");
		Reporter.log("Step 7: click add payment method| payment spoke page should be displayed");
		Reporter.log("Step 8: verify PII masking | PII masking attribute should be present for stored payment details");
		Reporter.log("Step 9: click on add Bank| Bank information page should be displayed");
		Reporter.log("Step 10: Fill all bank details and click continue button| OTP page should be displayed");
		Reporter.log("Step 11: click submit button| OTP confirmation page should be displayed");
		Reporter.log("Step 12: verify all bank payment details| Bank payment details should be displayed correct.");
		Reporter.log(
				"Step 13: verify PII masking | PII masking attributes should be present for all payment information");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPIIMasking(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
/*		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyPIImasking(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage oTPConfirmationPage = new OTPConfirmationPage(getDriver());
		oTPConfirmationPage.verifyPageLoaded();
		oTPConfirmationPage.verifyPIIMasking();*/
	}

	/**
	 * US268500:PA Messaging Enhancements - Secured PA - OTP Confirmation Page
	 * (Inside blackout) (Inside blackout)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testSecuredPAMessageonOTPInsideBlackoutPeriod(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case: US268500:PA Messaging Enhancements - Secured PA - OTP Confirmation Page (Inside blackout)");
		Reporter.log("Data Condition | PA secured outside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("1. Navigate to my t-mobile.com | Login page should be displayed");
		Reporter.log("2. Login as a mytmo customer | Home page should be displayed");
		Reporter.log("3. Click on paybill | One time payment page should be displayed");
		Reporter.log("4. Click on other and enter the amount| Should be able to add the amount");
		Reporter.log(
				"4. Add the payment method and add the payment details| Should be able to add the payment details");
		Reporter.log("4. Click on agree and	submit button | One time payment confirmation page should be displayed");
		Reporter.log(
				"5. Verify scheduled payment notification and check scheduled amount due of [$XX.XX] from <ICON> ****<XXXX>  | Should be able to check the amount and method icon and last 4 digits of payment method");
		Reporter.log("6. Verify vew payment schedule hyperlink | View payment schedule hyperlink should be displayed");
		Reporter.log(
				"7. Click on view payment schedule hyperlink | Payment arrangement landing page should be displayed");
		Reporter.log("8. Verify amount due | Amount due should be displayed");
		Reporter.log("9. Verify payment method icon | Payment method icon should be displayed");
		Reporter.log(
				"10.Verify last 4 digits of the payment method | Last 4 digits of the payment method should be displayed");
		Reporter.log(
				"11. The details should match to the schedule payment notification message in OTP confirmation page| Should be able to match");
		Reporter.log(
				"12. Go to home page and click on Account history | Should be able to navigate to account history page");
		Reporter.log(
				"13. Check the schedule activity PA notification amount, method Icon and  last 4 digits | Should be able to match with details with PA notification details in OTP confirmation page");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		if (oneTimePaymentPage.verifyReviewPAModalIsDisplayedOrNot()) {
			oneTimePaymentPage.clickContinueWithPaymentCTA();
		}
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double payAmount = generateRandomAmount();
		editAmountPage.setOtherAmount(payAmount.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.verifyPANotification(PaymentConstants.PAYMENT_ARRANGEMENT_OTP_CONFIRMATION_PAGE);
		otpConfirmationPage.verifyPALink();
		otpConfirmationPage.clickPALink();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
	}

	/**
	 * US355145:PA Messaging Enhancements - Missed PA - OTP Confirmation Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testMissedPAMessageonOTPConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US355145:PA Messaging Enhancements - Missed PA - OTP Confirmation Page");
		Reporter.log("Data Condition | PA Messaging Enhancements - Missed PA - OTP Confirmation Page");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("1. Navigate to my t-mobile.com | Login page should be displayed");
		Reporter.log("2. Login as a mytmo customer | Home page should be displayed");
		Reporter.log("3. Click on paybill | One time payment page should be displayed");
		Reporter.log("4. Click on agree and	submit button | One time payment confirmation page should be displayed");
		Reporter.log(
				"5. If payment amount  > = missed installment due | This payment covers your Payment Arrangement amount due today notification should be displayed");
		Reporter.log(
				"6. IF payment amount < missed installment due | This payment alone will not satisfy the total [$XX.XX] due today for your Payment Arrangement to avoid suspension. Review previous payments notification should be displayed");
		Reporter.log(
				"8. Verify amount due for the missed installment | Amount due for the missed installment should be displayed");
		Reporter.log("9. Verify review payments made hyperlink | Review payments made hyperlink should be displayed");
		Reporter.log("10.Click on review payments made hyperlink | Account activity page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();

		editAmountPage.clickAmountRadioButton("Other");
		Double bal = Double.parseDouble(paDetails.get("Amount")) - 1;
		editAmountPage.setOtherAmount(bal.toString());
		editAmountPage.clickUpdateAmount();

		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.verifyPANotification(PaymentConstants.PA_OTP_MISSED_LESS_AMOUNT);
		otpConfirmationPage.clickReviewPreviousPayments();
		AccountHistoryPage alertsActivitiesPage = new AccountHistoryPage(getDriver());
		alertsActivitiesPage.verifyPageLoaded();
	}

	//
	/**
	 * US272292 PA Messaging Enhancements - Unsecured PA - OTP Confirmation Page
	 * 
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUnsecuredPAMessageonOTPConfirmationPageAmountGreaterthanInstallmentdueInsideBlackout(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US272292:  PA Messaging Enhancements - Unsecured PA - OTP Confirmation Page");
		Reporter.log("Data Condition | PA unsecured inside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade |  OTP edit amount page should be displayed");
		Reporter.log("Step 5: enter other amount greater than installment due and update | amount should be updated");
		Reporter.log("Step 6: enter payment details and submit | OTP confirmation page should be displayed");
		Reporter.log(
				"Step 7: verify 'This payment covers your payment arrangement amount due today' message on OTP confirmation page|'This payment covers your payment arrangement amount due today' message on OTP confirmation page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double bal = Double.parseDouble(paDetails.get("Amount")) + 5;
		editAmountPage.setOtherAmount(bal.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.verifyPAConfirmationAlert(PaymentConstants.PA_OTP_UNSECURE_MORE_AMOUNT_AFTER12AM_DUEDATE);
	}

	/**
	 * US272292 PA Messaging Enhancements - Unsecured PA - OTP Confirmation Page
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUnsecuredPAMessageonOTPConfirmationPageAmountGreaterthanInstallmentdueOutsideBlackout(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US272292:  PA Messaging Enhancements - Unsecured PA - OTP Confirmation Page");
		Reporter.log("Data Condition | PA unsecured outside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade |  OTP edit amount page should be displayed");
		Reporter.log("Step 5: enter other amount greater than installment due and update | amount should be updated");
		Reporter.log("Step 6: enter payment details and submit | OTP confirmation page should be displayed");
		Reporter.log(
				"Step 7: verify 'This payment covers your payment arrangement amount due [DUE DATE FOR INSTALLMENT] JUST SATISFIED' message on OTP confirmation page|'This payment covers your payment arrangement amount due [DUE DATE FOR INSTALLMENT] JUST SATISFIED'  message on OTP confirmation page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		if (oneTimePaymentPage.verifyReviewPAModalIsDisplayedOrNot()) {
			oneTimePaymentPage.clickContinueWithPaymentCTA();
		}
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double bal = Double.parseDouble(paDetails.get("Amount").replace("$", "").trim()) + generateRandomAmount();
		editAmountPage.setOtherAmount(bal.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		String paText = PaymentConstants.PA_OTP_UNSECURE_MORE_AMOUNT_BEFORE12AM_DUEDATE.replace("[INSTALLMENT DATE]",
				paDetails.get("Date"));
		otpConfirmationPage.verifyPAConfirmationAlert(paText);
	
	}

	/**
	 * US272292 PA Messaging Enhancements - Unsecured PA - OTP Confirmation Page
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUnsecuredPAMessageonOTPConfirmationPageAmountLessthanInstallmentdueInsideBlackout(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US272292:  PA Messaging Enhancements - Unsecured PA - OTP Confirmation Page");
		Reporter.log("Data Condition | PA unsecured inside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade |  OTP edit amount page should be displayed");
		Reporter.log("Step 5: enter other amount less than installment due and update | amount should be updated");
		Reporter.log("Step 6: enter payment details and submit | OTP confirmation page should be displayed");
		Reporter.log(
				"Step 7: verify 'This payment alone will not satisfy the total [$XX.XX] due today for your Payment Arrangement to avoid suspension. Review previous payments' message on OTP confirmation page|'This payment alone will not satisfy the total [$XX.XX] due today for your Payment Arrangement to avoid suspension. Review previous payments'  message on OTP confirmation page should be displayed");
		Reporter.log("Step 8: verify [$XX.XX] is current installemnet due|current installemnet due should be verified");
		Reporter.log("Step 9: click on 'review previous payments' link|account activity page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		oneTimePaymentPage.clickamountIcon();
		Double bal = Double.parseDouble(paDetails.get("Amount"));
		bal = bal - 6;
		editAmountPage.setOtherAmount(bal.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		String paText = PaymentConstants.PA_OTP_UNSECURE_MORE_AMOUNT_AFTER12AM_DUEDATE2.replace("[$XX.XX]",
				paDetails.get("Amount"));
		otpConfirmationPage.verifyPAConfirmationAlert(paText);
		otpConfirmationPage.verifyReviewPreviousPayments();

	}

	/**
	 * US272292 PA Messaging Enhancements - Unsecured PA - OTP Confirmation Page
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUnsecuredPAMessageonOTPConfirmationPageAmountLessthanInstallmentdueOutsideBlackout(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US272292:  PA Messaging Enhancements - Unsecured PA - OTP Confirmation Page");
		Reporter.log("Data Condition | PA unsecured outside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade |  OTP edit amount page should be displayed");
		Reporter.log("Step 5: enter other amount less than installment due and update | amount should be updated");
		Reporter.log("Step 6: enter payment details and submit | OTP confirmation page should be displayed");
		Reporter.log(
				"Step 7: verify ' This payment alone will not satisfy the total [$XX.XX] due on [INSTALLMENT DATE] for your Payment Arrangement to avoid suspension. Review previous payments' message|' This payment alone will not satisfy the total [$XX.XX] due on [INSTALLMENT DATE] for your Payment Arrangement to avoid suspension. Review previous payments'  message on OTP confirmation page should be displayed");
		Reporter.log("Step 8: verify [$XX.XX] is current installemnet due|current installemnet due should be verified");
		Reporter.log("Step 9: click on 'review previous payments' link|account activity page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		if (oneTimePaymentPage.verifyReviewPAModalIsDisplayedOrNot()) {
			oneTimePaymentPage.clickContinueWithPaymentCTA();
		}
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyAmountRadioButton("Other");
		editAmountPage.clickAmountRadioButton("Other");
		double dAmount = Double.parseDouble((paDetails.get("Amount").replace("$", "").trim()))-generateRandomAmount();
		editAmountPage.setOtherAmount(String.valueOf(dAmount));
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		String paText = PaymentConstants.PA_OTP_UNSECURE_LESS_AMOUNT_BEFORE12AM_DUEDATE2
				.replace("[$XX.XX]", paDetails.get("Amount")).replace("[INSTALLMENT DATE]", paDetails.get("Date"));
		otpConfirmationPage.verifyPAConfirmationAlert(paText);
		otpConfirmationPage.verifyReviewPreviousPayments();
		otpConfirmationPage.clickReviewPreviousPayments();
		accountHistoryPage.verifyPageLoaded();
	}

	
	
	/**
	 * US346429 MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-OTP Vairant confirmaiton Screen
	 *  */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { })
	public void testDigitalPASupportOnMakingQualifyingPaymentGreaterThan30daysPastDue(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US346429: MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-OTP Vairant confirmaiton Screen");
		Reporter.log("Data Condition | MSDSIN with past due balance aged greater than 30 days");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade | OTP edit amount page should be displayed");
		Reporter.log("Step 5: Enter a qualifying payment amount in other amount fileld | Amount should be updated");
		Reporter.log("Step 6: Enter valid payment details and submit | OTP confirmation page should be displayed");
		Reporter.log(
				"Step 7: Verify payment arrangement eligible message on OTP confirmation page|Payment arrangement eligible message should be displayed");
		Reporter.log("Step 8: click on continue CTA|PA Landing page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");
		
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage amountPage = new OTPAmountPage(getDriver());
		amountPage.verifyPageLoaded();
		Double amount = Double.parseDouble(amountPage.getOtherAmount())+1;
		amountPage.setOtherAmount(amount.toString());
		amountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		/*oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage confirmationPage = new OTPConfirmationPage(getDriver());
		confirmationPage.verifyPageLoaded();
		confirmationPage.verifyPANotification("");*/
	}

	
	
	/**
	 * US346429 MyTMO - PAYMENTS - Digital PA Support for <30 Days Past Due-OTP Vairant Confirmation Screen
	 *  */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  })
	public void testDigitalPASupportOnNotMakingQualifyingPaymentLessThan30DaysPastDue(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US346429: MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-OTP Vairant confirmaiton Screen");
		Reporter.log("Data Condition | MSDSIN with past due balance aged greater than 30 days");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade | OTP edit amount page should be displayed");
		Reporter.log("Step 5: Enter not qualifying amount for  PA Less than 30 days past due | Amount should be updated and ");
		Reporter.log("Step 6: Enter valid payment details and submit | Should be able to add the payment details and OTP confirmation page should be displayed");
		Reporter.log("Step 7: Verify 'You must still pay $XXXX to be eligible for a Payment Arrangement' message |Should be able to verify the message on OTP confirmation page");
		Reporter.log("Step 8: AND 2 CTAs will be present| Should be able to see 2 CTAs");
		Reporter.log("Step 9: Click on CTA 1 which contains the text [Continue to pay past due amount ] | Should be able to return to OTP Variant landing page ");
		Reporter.log("Step 10:Click on CTA 2 which contains the text [Return Home]| Should be able to return to the home page");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage amountPage = new OTPAmountPage(getDriver());
		amountPage.verifyPageLoaded();
		Double amount = Double.parseDouble(amountPage.getOtherAmount())-1;
		amountPage.setOtherAmount(amount.toString());
		amountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage confirmationPage = new OTPConfirmationPage(getDriver());
		confirmationPage.verifyPageLoaded();
		confirmationPage.verifyPANotification("");
	}


	/**
	 * US346429 MyTMO - PAYMENTS - Digital PA Support for >= past due balance -OTP Vairant Confirmation Screen
	 *  */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { })
	public void testDigitalPASupportAmountPaidGreaterThanAndEqualToTotalPastDue(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US346429: MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-OTP Vairant confirmaiton Screen");
		Reporter.log("Data Condition | MSDSIN with past due balance aged greater than 30 days");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade | OTP edit amount page should be displayed");
		Reporter.log("Step 5: Enter amount greater than or equal to past due balance | Amount should be updated and ");
		Reporter.log("Step 6: Enter valid payment details and submit | Should be able to add the payment details and OTP confirmation page should be displayed");
		Reporter.log("Step 7: Verify 'XXXX is the greater than 30 days past due amount. |Should be able to verify the message on OTP confirmation page");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");
		
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage amountPage = new OTPAmountPage(getDriver());
		amountPage.verifyPageLoaded();
		//Double amount = Double.parseDouble(amountPage.getTotalDueAmount())+1;
		//amountPage.setOtherAmount(amount.toString());
		amountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage confirmationPage = new OTPConfirmationPage(getDriver());
		confirmationPage.verifyPageLoaded();
		confirmationPage.verifyPANotification("");
	}
	
	
	
	//Qlab03 tests
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "qlab03"})
	public void testOTPConfirmationPagewithnewBankQlab(ControlTestData data, MyTmoData myTmoData) {
	
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on Agree and submit | OTP confirmation page should be displayed");
		Reporter.log(
				"5. verify elements on OTP confirmation page | All elements on OTP confirmation page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
	
		getDriver().get(System.getProperty("environment") + "/onetimepayment");
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double payAmount = generateRandomAmount();
		editAmountPage.setOtherAmount(payAmount.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage oTPConfirmationPage = new OTPConfirmationPage(getDriver());
		oTPConfirmationPage.verifyPageLoaded();
		oTPConfirmationPage.verifyOTPConfirmationPage();
	

	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "qlab03"})
	public void testOTPConfirmationPagewithnewCardQlab(ControlTestData data, MyTmoData myTmoData) {
	
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on Agree and submit | OTP confirmation page should be displayed");
		Reporter.log(
				"5. verify elements on OTP confirmation page | All elements on OTP confirmation page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
	
		getDriver().get(System.getProperty("environment") + "/onetimepayment");
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double payAmount = generateRandomAmount();
		editAmountPage.setOtherAmount(payAmount.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage oTPConfirmationPage = new OTPConfirmationPage(getDriver());
		oTPConfirmationPage.verifyPageLoaded();
		oTPConfirmationPage.verifyOTPConfirmationPage();
		oTPConfirmationPage.verifyPaymentCardDetails();

	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "qlab03"})
	public void testOTPConfirmationPagewithsavedCardQlab(ControlTestData data, MyTmoData myTmoData) {
	
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on Agree and submit | OTP confirmation page should be displayed");
		Reporter.log(
				"5. verify elements on OTP confirmation page | All elements on OTP confirmation page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
	
		getDriver().get(System.getProperty("environment") + "/onetimepayment");
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double payAmount = generateRandomAmount();
		editAmountPage.setOtherAmount(payAmount.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.selectStoredCard();;
		otpSpokePage.clickContinueButton();
		
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage oTPConfirmationPage = new OTPConfirmationPage(getDriver());
		oTPConfirmationPage.verifyPageLoaded();
		oTPConfirmationPage.verifyOTPConfirmationPage();
		oTPConfirmationPage.verifyPaymentCardDetails();

	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "qlab03"})
	public void testOTPConfirmationPagewithsavedBankQlab(ControlTestData data, MyTmoData myTmoData) {
	
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on Agree and submit | OTP confirmation page should be displayed");
		Reporter.log(
				"5. verify elements on OTP confirmation page | All elements on OTP confirmation page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
	
		getDriver().get(System.getProperty("environment") + "/onetimepayment");
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double payAmount = generateRandomAmount();
		editAmountPage.setOtherAmount(payAmount.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.selectStoredCheckingAccount();;
		otpSpokePage.clickContinueButton();
		
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage oTPConfirmationPage = new OTPConfirmationPage(getDriver());
		oTPConfirmationPage.verifyPageLoaded();
		
		oTPConfirmationPage.verifyOTPConfirmationPage();
	

	}
}
