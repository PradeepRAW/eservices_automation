
package com.tmobile.eservices.qa.global.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.UNAVCommonPage;
import com.tmobile.eservices.qa.pages.accounts.AccountVerificationPage;
import com.tmobile.eservices.qa.pages.global.ChangeSimPage;
import com.tmobile.eservices.qa.pages.global.PhonePage;

public class PhonePageTest extends GlobalCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(PhonePageTest.class);

	/**
	 * US283095:SIM Swap | Hidden for Restricted Customer
	 * TC-710:Sim Swap_Ensure Restricted user not able to enter into Sim swap flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testSimSwapForRestrictedCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US283095:SIM Swap | Hidden for Restricted Customer");
		Reporter.log("TC-710:Sim Swap_Ensure Restricted user not able to enter into Sim swap flow");
		Reporter.log("Test Data : Restricted Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone link | Phone page should be displayed");
		Reporter.log("5. Verify change sim link | Change sim link should not be displayed");
		Reporter.log(
				"6. Verify  insufficent permissions  message| Insuffcient permissions message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PhonePage phonePage = navigateToPhonePage(myTmoData);
		phonePage.verifyChangeSimMenuLink();
		phonePage.verifyInsufficientPermissionsMessage();
	}

	/**
	 * US283095:SIM Swap | Hidden for Restricted Customer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testSimSwapForDigitsCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US283095:SIM Swap | Hidden for Restricted Customer");
		Reporter.log("Test Data : Digits Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone link | Phone page should be displayed");
		Reporter.log("5. Verify change sim link | Change sim link should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PhonePage phonePage = navigateToPhonePage(myTmoData);
		phonePage.verifyChangeSimMenuLink();
	}

	/**
	 * US283095:SIM Swap | PAH customer
	 * TC-709:Sim Swap_Ensure PAH user able to enter into sim swap flow
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testSimSwapForPAHCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US283095:SIM Swap | Hidden for Restricted Customer");
		Reporter.log("TC-709:Sim Swap_Ensure PAH user able to enter into sim swap flow");
		Reporter.log("Test Data : PAH");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone link | Phone page should be displayed");
		Reporter.log("5. Click on change sim link | Change sim page should be displayed");
		Reporter.log("6. Click on next button | Account verification page should be displayed");
		Reporter.log("7. Verify enter verification code header text | Enter verification code header text should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PhonePage phonePage = navigateToPhonePage(myTmoData);
		phonePage.clickChangeSimMenuLink();

		ChangeSimPage changeSimPage = new ChangeSimPage(getDriver());
		changeSimPage.verifyChangeSimPage();
		changeSimPage.clickNextBtn();

		AccountVerificationPage accountVerificationPage = new AccountVerificationPage(getDriver());
		accountVerificationPage.verifyAccountVerificationPage();
		accountVerificationPage.clickOnNextBtn();
		accountVerificationPage.verifyEnterVerificationCodeHeaderText();
	}

	/**
	 * US303680:SIM Swap | Digits Customer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testSimSwapWithPairedDigitsDataForDigitsLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US303680:SIM Swap | Digits Customer");
		Reporter.log("Test Data : Digits Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone link | Phone page should be displayed");
		Reporter.log("5. Verify change sim link | Change sim link should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PhonePage phonePage = navigateToPhonePage(myTmoData);
		phonePage.verifyChangeSimMenuLink();
	}

	/**
	 * US303680:SIM Swap | Digits Customer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testSimSwapWithPairedDigitsDataForPAH(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US303680:SIM Swap | Digits Customer");
		Reporter.log("Test Data : PAH");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone link | Phone page should be displayed");
		Reporter.log("5. Click on change sim link | Change sim page should be displayed");
		Reporter.log("6. Click on next button | Account verification page should be displayed");
		Reporter.log(
				"7. Verify enter verification code header text | Enter verification code header text should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PhonePage phonePage = navigateToPhonePage(myTmoData);
		phonePage.clickChangeSimMenuLink();

		ChangeSimPage changeSimPage = new ChangeSimPage(getDriver());
		changeSimPage.verifyChangeSimPage();
		changeSimPage.clickNextBtn();

		AccountVerificationPage accountVerificationPage = new AccountVerificationPage(getDriver());
		accountVerificationPage.verifyAccountVerificationPage();
		accountVerificationPage.clickOnNextBtn();
		accountVerificationPage.verifyEnterVerificationCodeHeaderText();
	}

	/**
	 * US303680/US325954:SIM Swap | Digits Customer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testSimSwapForCustomerWithMIRatePlan(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US303680:SIM Swap | Digits Customer");
		Reporter.log("Test Data : Digits line configured with rate plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click mi plan line | mi plan line should be clicked");
		Reporter.log("5. Verify mi plan line | mi plan line should be displayed");
		Reporter.log("6. Click on phone menu | Phone menu should be clicked");
		Reporter.log("7. Verify phone page | Phone page should be displayed");
		Reporter.log(
				"8. Select digits line and verify change sim menu link | Change sim menu link should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);

		HomePage homePage = new HomePage(getDriver());
		homePage.clickMIPlanLine();
		homePage.verifyMIPlanLine();
		homePage.clickPhoneLink();

		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		phonePage.selectDigitsLine();
		phonePage.verifyChangeSimMenuLink();
	}

	/**
	 * US283087:SIM Swap | Remove Line Selector from Authentication Step
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testLineSelectorForPAH(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US283087:SIM Swap | Remove Line Selector from Authentication Step");
		Reporter.log("Test Data : PAH");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone link | Phone page should be displayed");
		Reporter.log("5. Click on change sim link | Change sim page should be displayed");
		Reporter.log("6. Click on next button | Account verification page should be displayed");
		Reporter.log(
				"7. Verify enter verification code header text | Enter verification code header text should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PhonePage phonePage = navigateToPhonePage(myTmoData);
		phonePage.clickChangeSimMenuLink();

		ChangeSimPage changeSimPage = new ChangeSimPage(getDriver());
		changeSimPage.verifyChangeSimPage();
		changeSimPage.verifyUndisplayLineSelectorDropDown();
		changeSimPage.clickNextBtn();

		AccountVerificationPage accountVerificationPage = new AccountVerificationPage(getDriver());
		accountVerificationPage.verifyAccountVerificationPage();
		accountVerificationPage.clickOnNextBtn();
		accountVerificationPage.verifyEnterVerificationCodeHeaderText();
	}

	/**
	 * US283088:SIM Swap | Read Only Line Selector for Non-PAH User
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testLineSelectorForNonPAH(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US283088:SIM Swap | Read Only Line Selector for Non-PAH User");
		Reporter.log("Test Data : PAH");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone link | Phone page should be displayed");
		Reporter.log("5. Click on change sim link | Change sim page should be displayed");
		Reporter.log("6. Click on next button | Account verification page should be displayed");
		Reporter.log(
				"7. Verify enter verification code header text | Enter verification code header text should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PhonePage phonePage = navigateToPhonePage(myTmoData);
		phonePage.clickChangeSimMenuLink();

		ChangeSimPage changeSimPage = new ChangeSimPage(getDriver());
		changeSimPage.verifyChangeSimPage();
		changeSimPage.clickNextBtn();

		AccountVerificationPage accountVerificationPage = new AccountVerificationPage(getDriver());
		accountVerificationPage.verifyAccountVerificationPage();
		accountVerificationPage.clickOnNextBtn();
		accountVerificationPage.verifyEnterVerificationCodeHeaderText();
	}

	/**
	 * DE161495:SIM Swap - Phone page line selection is not retained when user
	 * navigated to changesim page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX })
	public void verifySelectedLineInChangeSimPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US283087:SIM Swap | Remove Line Selector from Authentication Step");
		Reporter.log("Test Data : PAH");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone link | Phone page should be displayed");
		Reporter.log(
				"5. Change to next line from line selector dropdown and click on change sim link | Change sim page should be displayed");
		Reporter.log(
				"6. Verify selected line in change sim page | Selected line should be retained same in the change sim page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PhonePage phonePage = navigateToPhonePage(myTmoData);
		phonePage.changeToNextLine();
		String selectedLine = phonePage.getSelectedLine();
		phonePage.clickChangeSimMenuLink();

		ChangeSimPage changeSimPage = new ChangeSimPage(getDriver());
		changeSimPage.verifyChangeSimPage();
		changeSimPage.verifySelectedLine(selectedLine);
	}

	/**
	 * US584348:.NET Migration | Gov & Bus | Phone
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void verifyPhonePageForGovtCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US584348:.NET Migration | Gov & Bus | Phone");
		Reporter.log("Test Data : Govt Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone link | Phone page should be displayed");
		Reporter.log("5. Verify manage digits settings cta | Manage digits settings cta should not be displayed");
		Reporter.log("6. Verify device unlock status blade | Device unlock status blade should not be displayed");
		Reporter.log("7. Verify line selector divison | Line selector divison should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PhonePage phonePage = navigateToPhonePage(myTmoData);
		phonePage.verifyDigitsSettingsCTA();
		phonePage.verifyDeviceUnlockBlade();
		phonePage.verifyLineSelectorDiv();
	}

	/**
	 * US563214:SIM Swap | Re-direct unauthenticated user to login page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void testSimSwapForUnauthUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US563214:SIM Swap | Re-direct unauthenticated user to login page");
		Reporter.log("Test Data : Digits Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Navigate to changesim url | Change sim page should be displayed");
		Reporter.log("5. Click on logout button | User should logout successfully");
		Reporter.log("6. Navigate to changesim url | Deeplink Change sim page should not displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToHomePage(myTmoData);
		getDriver().get(getDriver().getCurrentUrl().replace("/home", "").concat("/myphone/changesim.html"));
		String deepLink = getDriver().getCurrentUrl();
		ChangeSimPage changeSimPage = new ChangeSimPage(getDriver());
		changeSimPage.verifyChangeSimPage();
		HomePage homePage = new HomePage(getDriver());
		homePage.clickLogoutButton();
		changeSimPage.deeplink(deepLink);

	}

	/**
	 * CDCDWR-327 - SIM Swap | Prevent Deep Linking for VIP Customers
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testSimSwapForVIPDeepLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test CDCDWR-327 - SIM Swap | Prevent Deep Linking for VIP Customers");
		Reporter.log("Test Data : VIP Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Navigate to changesim url | re-directed to the vip/home depplink page for VIP Customer");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		getDriver().get(getDriver().getCurrentUrl().replace("/vip/home", "").concat("/myphone/changesim.html"));
		homePage.verifyCurrentPageURL("VIPCustomers splash page", "/vip/home");
		homePage.verifyStarImage();
	}

	/**
	 * CDCDWR-327 - SIM Swap | Prevent Deep Linking for VIP Customers
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testSimSwapForIRDeepLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test CDCDWR-327 - SIM Swap | Prevent Deep Linking for VIP Customers");
		Reporter.log("Test Data : IR Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Navigate to changesim url | Change sim page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToNewHomePage(myTmoData);
		getDriver().get(getDriver().getCurrentUrl().replace("/home", "").concat("/myphone/changesim.html"));
		ChangeSimPage changeSimPage = new ChangeSimPage(getDriver());
		changeSimPage.verifyChangeSimPage();
	}

	/**
	 * CDCDWR-602:.NET Migration | Prepaid | Phone Page Changes
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void verifyPhonePageForPrepaidCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test CDCDWR-602:.NET Migration | Prepaid | Phone Page Changes");
		Reporter.log("Test Data : Prepaid Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone link | Phone page should be displayed");
		Reporter.log("5. Verify Upgrade CTA | Upgrade CTA should not be displayed");
		Reporter.log("6. Verify manage digits settings cta | Manage digits settings cta should not be displayed");
		Reporter.log("7. Verify Change SIM CTA | Change SIM CTA should not be displayed");
		Reporter.log("8. Verify TradeInLink | TradeInLink should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyHeaderPhoneLabel();
		unavPage.clickHeaderPhoneLink();
		unavPage.verifyHeaderPhonePage();
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyupgradeCTA();
		phonePage.verifyDigitsSettingsCTA();
		phonePage.verifyChangeSimMenuLink();
		phonePage.verifyTradeInLink();
	}
}
