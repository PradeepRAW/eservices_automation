package com.tmobile.eservices.qa.monitors;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.CommonLibrary;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.AutoPayPage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.UsagePage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.PhonePages;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class PlabMonitoring extends CommonLibrary {

	/**
	 * Regular Upgrade With FRP Flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PERFORMANCE })
	public void testRegularUpgrade(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Regular Upgrade Flow With FRP");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
		Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
		Reporter.log("4. Click on the device on Device PLP page |Device PDP page should be displayed ");
		Reporter.log(
				"5. Select FRP from payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select eligible line on LS page | Line Selector Details page should be displayed");
		Reporter.log("7. Click on 'Skip trade-in' CTA | PHP page should be displayed");
		Reporter.log("8. Click on Continue button on PHP page | Accessory PLP page should be displayed");
		Reporter.log("9. Click 'OK' on modal and 'Skip Accessories' link | Cart page should be displayed");
		Reporter.log("10. Click 'Continue to shipping' CTA | Shipping tab should be displayed");
		Reporter.log("11. Click 'Continue to payment' CTA | Payment tab page should be displayed");
		Reporter.log("12. Click on T-Mobile Icon | Home page should be displayed");
		Reporter.log("12. Click on Logout | Login page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopCommonLib shopCommonLib = new ShopCommonLib();
		CartPage cartPage = shopCommonLib
				.navigateToCartPageWithoutDeviceProtectionBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickOnTMobileIcon();

		HomePage homePage = new HomePage(getDriver());
		homePage.clickLogoutButton();

		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPage();
	}

	/**
	 * Billing And Payment Flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PERFORMANCE })
	public void testBillingAndPaymentFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Billing And Payment Flow");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Bill tab | Bill and pay page should be displayed");
		Reporter.log("3. Click on make a payment button | One time payment page should be displayed");
		Reporter.log("4. Click on cancel button | Home page should be displayed");
		Reporter.log("5. Click on auto pay | Auto pay page should be displayed");
		Reporter.log("6. Click on cancel button | Home page should be displayed");
		Reporter.log("4. Click on logout | Login page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickBillingLink();
		
		BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
		billAndPaySummaryPage.verifyPageUrl();
		billAndPaySummaryPage.clickMakeAPaymentLinkOnBBPage();

		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.clickCancelBtn();
		
		oneTimePaymentPage.clickContinueBtnCancelModal();
	
		homePage.verifyPageLoaded();
		homePage.clickBillingLink();
		billAndPaySummaryPage.verifyPageUrl();
		//billAndPaySummaryPage.clickMakeAPaymentLinkOnBBPage();

		billAndPaySummaryPage.clickAutopayShortcut();
		
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyAutoPayPage();
		autoPayPage.clickCancelButton();
		autoPayPage.clickYesButton();
		homePage.verifyPageLoaded();
		homePage.clickLogoutButton();

		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPage();
	}

	/**
	 * Verify Usage Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PERFORMANCE })
	public void testUsagePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Verify Usage Page");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Usage tab | Usage page should be displayed");
		Reporter.log("3. Click on logout | Login page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickUsageLink();
		UsagePage usagePage = new UsagePage(getDriver());
		usagePage.verifyPageUrl();
		homePage.clickLogoutButton();
		
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPage();
	}

	/**
	 * Verify Phone Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PERFORMANCE })
	public void testPhonePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Verify Phone Page");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Phone tab | Phone page should be displayed");
		Reporter.log("3. Click on logout | Login page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickPhoneLink();

		PhonePages phonePages = new PhonePages(getDriver());
		phonePages.verifyPhonesPage();
		homePage.clickLogoutButton();
		
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPage();
	}

	/**
	 * Verify Profile Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PERFORMANCE })
	public void testProfilePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Verify Profile Page");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Profile tab | Profile page should be displayed");
		Reporter.log("3. Click on logout | Login page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickProfileMenu();

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
		homePage.clickLogoutButton();
		
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPage();
	}

	/**
	 * Verify Account History Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PERFORMANCE })
	public void testAccountHistoryPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case name : Verify Account History Page");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Account History tab | Account History page should be displayed");
		Reporter.log("3. Click on logout | Login page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();

		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		homePage.clickLogoutButton();
		
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPage();
	}

	/**
	 * Verify Login
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PERFORMANCE })
	public void testLogin(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : Verify Login");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on logout | Login page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickLogoutButton();
		
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPage();
	}

}
