/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.BillingAndPaymentsPage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.payments.AutoPayPage;
import com.tmobile.eservices.qa.pages.payments.BillingPaymentsPage;
import com.tmobile.eservices.qa.pages.payments.EipJodPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayLandingPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class ProfilePageTest extends PaymentCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(ProfilePageTest.class);

	/**
	 * Verify with AutoPay enabled the correct payment-type icon AND text (VISA,
	 * AMEX…) on the Profile page (billing & payments tab)
	 * 
	 * @param data
	 * @param myTmoData This test is excluded as per the latest profile page
	 *                  changes. This functionality no longer exists.
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.EXCLUDE })
	public void verifyAutopayCardIconDisplayedProfilepage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAutopayCardIconDisplayedProfilepage method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Verify with AutoPay enabled the correct payment-type icon AND text (VISA, AMEX…) on the Profile page (billing & payments tab)	");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Verify AutoPay Card Icon | AutoPay Card Icon is loaded");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());

		verifyPage("Home page", "Home");
		homePage.clickUserProfileLinks(PaymentConstants.HOME_PAGE_RIGHT_HEADERS_PROFILE);
		ProfilePage profilePage = new ProfilePage(getDriver());
		/*
		 * profilePage.clickProfile(); profilePage.waitForMenuLinksDisplay();
		 * profilePage.verifyProfilePage(); if (getDriver() instanceof AppiumDriver) {
		 * profilePage.clickBillingPayments(); } profilePage.verifyAutoPaycardIcon();
		 */
		Reporter.log(PaymentConstants.AUTOPAY_CARD_ICON_DISPLAYED);
	}

	/**
	 * Verify scheduled AutoPay date shows correctly on Profile page (billing and
	 * payments tab)
	 * 
	 * @param data
	 * @param myTmoData This test is excluded as per the latest profile page
	 *                  changes. This functionality no longer exists.
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.EXCLUDE })
	public void verifyScheduledautoPaydateProfilepage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyScheduledautoPaydateProfilepage method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Verify scheduled AutoPay date shows correctly on Profile page (billing and payments tab)	");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("4. Verify AutoPay Payment Schdeuled date | AutoPay Payment Schdeuled date is loaded");
		Reporter.log("================================");

		// HomePage homePage = navigateToHomePage(myTmoData);
		// homePage.clickUserProfileLinks(PaymentConstants.HOME_PAGE_RIGHT_HEADERS_PROFILE);
		ProfilePage profilePage = new ProfilePage(getDriver());
		/*
		 * profilePage.clickProfile(); profilePage.waitForMenuLinksDisplay();
		 * profilePage.verifyProfilePage(); profilePage.clickBillingPayments();
		 * profilePage.verifyAutoPayscheduledDateProfilepage();
		 */
		Reporter.log(PaymentConstants.AUTOPAY_PAYEMENT_SCHEDULEDDATE_DISPLAYED);
	}

	/**
	 * US545936 CCS: Profile | Payment Methods | Payment Method Notifications
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyPaymentMethodsNotificationsAutopayExpired(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US545936 CCS: Profile | Payment Methods | Payment Method Notifications");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Go to billing and payments | Should be able to go to billing and payments");
		Reporter.log("6. Check for the  'Saved payment methods' |  Should be able to see the 'saved payment methods'");
		Reporter.log(
				"7. Check for the message 'Autopay payment method expired' Should be able to see message 'Autopay payment method expired");

		Reporter.log("================================");
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink("Billing & Payments");
		BillingAndPaymentsPage billingAndPaymentsPage = new BillingAndPaymentsPage(getDriver());
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.verifySavedPaymentMethods();
		billingAndPaymentsPage.verifyPaymentMethodExpirationMsgsAutopay("Expired");
	}

	/**
	 * US545936 CCS: Profile | Payment Methods | Payment Method Notifications
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyPaymentMethodsNotificationsAutopayExpiringSoon(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US545936 CCS: Profile | Payment Methods | Payment Method Notifications");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Go to billing and payments | Should be able to go to billing and payments");
		Reporter.log("6. Check for the  'Saved payment methods' |  Should be able to see the 'saved payment methods'");
		Reporter.log(
				"7. Check for the message 'Autopay payment method expiring soon' Should be able to see message 'Autopay payment method expiring soon");

		Reporter.log("================================");
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink("Billing & Payments");
		BillingAndPaymentsPage billingAndPaymentsPage = new BillingAndPaymentsPage(getDriver());
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.verifySavedPaymentMethods();
		billingAndPaymentsPage.verifyPaymentMethodExpirationMsgsAutopay("Expiring soon");
	}

	/**
	 * US545936 CCS: Profile | Payment Methods | Payment Method Notifications
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyPaymentMethodsNotificationsExpired(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US545936 CCS: Profile | Payment Methods | Payment Method Notifications");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Go to billing and payments | Should be able to go to billing and payments");
		Reporter.log("6. Check for the  'Saved payment methods' |  Should be able to see the 'saved payment methods'");
		Reporter.log(
				"7. Check for the message '1 payment method(s) expired.' Should be able to see message '1 payment method(s) expired.'");

		Reporter.log("================================");
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink("Billing & Payments");
		BillingAndPaymentsPage billingAndPaymentsPage = new BillingAndPaymentsPage(getDriver());
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.verifySavedPaymentMethods();
		billingAndPaymentsPage.verifyPaymentMethodNotifications("1 payment method Expired");
	}

	/**
	 * US545936 CCS: Profile | Payment Methods | Payment Method Notifications
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyPaymentMethodsNotificationsExpiringSoon(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US545936 CCS: Profile | Payment Methods | Payment Method Notifications");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Go to billing and payments | Should be able to go to billing and payments");
		Reporter.log("6. Check for the  'Saved payment methods' |  Should be able to see the 'saved payment methods'");
		Reporter.log(
				"7. Check for the message '1 payment method(s) expiring soon.' Should be able to see message '1 payment method(s) expiring soon.'");

		Reporter.log("================================");
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink("Billing & Payments");
		BillingAndPaymentsPage billingAndPaymentsPage = new BillingAndPaymentsPage(getDriver());
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.verifySavedPaymentMethods();
		billingAndPaymentsPage.verifyPaymentMethodNotifications("1 payment method(s) expiring soon.");
	}

	/**
	 * US545936 CCS: Profile | Payment Methods | Payment Method Notifications
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyPaymentMethodsNotificationsNegativeCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US545936 CCS: Profile | Payment Methods | Payment Method Notifications");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Go to billing and payments | Should be able to go to billing and payments");
		Reporter.log("6. Check for the  'Saved payment methods' |  Should be able to see the 'saved payment methods'");
		Reporter.log(
				"7. Check for the message '1 payment method(s) no longer accepted.' Should be able to see message '1 payment method(s) no longer accepted.'");

		Reporter.log("================================");
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink("Billing & Payments");
		BillingAndPaymentsPage billingAndPaymentsPage = new BillingAndPaymentsPage(getDriver());
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.verifySavedPaymentMethods();
		billingAndPaymentsPage.verifyPaymentMethodNotifications("1 payment method(s) no longer accepted.");
	}

	/**
	 * US545936 CCS: Profile | Payment Methods | Payment Method Notifications
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyPaymentMethodsNotificationsNegativeBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US545936 CCS: Profile | Payment Methods | Payment Method Notifications");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Go to billing and payments | Should be able to go to billing and payments");
		Reporter.log("6. Check for the  'Saved payment methods' |  Should be able to see the 'saved payment methods'");
		Reporter.log(
				"7. Check for the message '1 payment method(s) no longer accepted.' Should be able to see message '1 payment method(s) no longer accepted.'");

		Reporter.log("================================");
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink("Billing & Payments");
		BillingAndPaymentsPage billingAndPaymentsPage = new BillingAndPaymentsPage(getDriver());
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.verifySavedPaymentMethods();
		billingAndPaymentsPage.verifyPaymentMethodNotifications("1 payment method(s) no longer accepted.");
	}

	/**
	 * US545936 CCS: Profile | Payment Methods | Payment Method Notifications
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyPaymentMethodsNotificationsEmptyWallet(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US545936 CCS: Profile | Payment Methods | Payment Method Notifications");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Go to billing and payments | Should be able to go to billing and payments");
		Reporter.log("6. Check for the  'Saved payment methods' |  Should be able to see the 'saved payment methods'");
		Reporter.log(
				"7. Check for the message ' Add payment methods to your wallet.' Should be able to see message ' Add payment methods to your wallet.'");

		Reporter.log("================================");
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink("Billing & Payments");
		BillingAndPaymentsPage billingAndPaymentsPage = new BillingAndPaymentsPage(getDriver());
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.verifySavedPaymentMethods();
		billingAndPaymentsPage.verifyPaymentMethodNotifications("Add payment methods to your wallet.");
	}

	/**
	 * US545936 CCS: Profile | Payment Methods | Payment Method Notifications
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyPaymentMethodsNotificationsMigration(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US545936 CCS: Profile | Payment Methods | Payment Method Notifications");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Go to billing and payments | Should be able to go to billing and payments");
		Reporter.log("6. Check for the  'Saved payment methods' |  Should be able to see the 'saved payment methods'");
		Reporter.log(
				"7. Check for the message 'Please re-enter you payment info for additional security.' Should be able to see message 'Please re-enter you payment info for additional security.'");

		Reporter.log("================================");
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink("Billing & Payments");
		BillingAndPaymentsPage billingAndPaymentsPage = new BillingAndPaymentsPage(getDriver());
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.verifySavedPaymentMethods();
		billingAndPaymentsPage
				.verifyPaymentMethodNotifications("Please re-enter you payment info for additional security.");
	}

	/**
	 * US545955-CCS: PCM Entrypoint Update : Verify user can enter PCM thru Profile
	 * page : Verify user can enter PCM thru Home page : Verify user can enter PCM
	 * thru EIPJOD page : Verify user can enter PCM thru Autopay page
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PUB_RELEASE })
	public void testverifyPCMentryPointProfilePage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPCMentry point  method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Verify PCM entryPoint from  Profile page,Home Page,AutoPay Page,EIP Page,OTP Page 	");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Enter the PCM url  | PCM entry Point page should be displayed");
		Reporter.log("6. Navigate to Home Page | Home Page should be displayed");
		Reporter.log("7. Enter the PCM url  | PCM entry Point page should be displayed");
		Reporter.log("8. NaviGate AutoPay Page | AutoPay Page should be displayed");
		Reporter.log("9. Enter the PCM url  | PCM entry Point page should be displayed");
		Reporter.log("10. Navigate to EIPJOD Page| EIP JOD Page should be displayed");
		Reporter.log("11. Enter the PCM url  | PCM entry Point page should be displayed");
		Reporter.log("12. Navigate to OTP Page| OTP Page should be displayed");
		Reporter.log("13. Enter the PCM url  | PCM entry Point page should be displayed");
		Reporter.log("================================");

		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.verifyProfilePage();
		getDriver().get(System.getProperty("environment") + "ccs/payments/paymentcollection");
		SpokePage paymentsCollectionPage = new SpokePage(getDriver());
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.verifyNewPageUrl();
		getDriver().get(System.getProperty("environment") + "home");
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		getDriver().get(System.getProperty("environment") + "ccs/payments/paymentcollection");
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.verifyNewPageUrl();
		getDriver().get(System.getProperty("environment") + "autopay");
		AutoPayPage autopayPage = new AutoPayPage(getDriver());
		autopayPage.verifyPageLoaded();
		getDriver().get(System.getProperty("environment") + "ccs/payments/paymentcollection");
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.verifyNewPageUrl();
		getDriver().get(System.getProperty("environment") + "eipjod");
		EipJodPage eipjodPage = new EipJodPage(getDriver());
		eipjodPage.verifyPageLoaded();
		getDriver().get(System.getProperty("environment") + "ccs/payments/paymentcollection");
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.verifyNewPageUrl();
		getDriver().get(System.getProperty("environment") + "onetimepayment");
		OneTimePaymentPage otpPage = new OneTimePaymentPage(getDriver());
		otpPage.verifyPageLoaded();
		getDriver().get(System.getProperty("environment") + "ccs/payments/paymentcollection");
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.verifyNewPageUrl();
	}

	/**
	 * TC 2 P1_TC_Regression_Desktop_Easy pay signup via Checking Account
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyUserAbleTOReachAutoPayFromProfile(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOEasyPayViaCheckingAccount method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Easy Pay Via CheckingAccount");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on profile | Profile page should be displayed");
		Reporter.log(" 5. Click on Billing and Payments link| Should be able to click on Billing and Payments");
		Reporter.log("5. Click on autopay pay button | Autopay landing Page should be loaded");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink("Billing & Payments");
		profilePage.clickProfilePageLink("AutoPay");
		NewAutopayLandingPage autoPayPage = new NewAutopayLandingPage(getDriver());
		autoPayPage.verifyPageLoaded();
	}

	/**
	 * US545933-CCS: Profile | Payment Methods | Create Payment Methods Blade Verify
	 * user level wallet appears on Billing and Payments page for PAH Verify user
	 * level wallet appears on Billing and Payments page for FULL Verify user level
	 * wallet appears on Billing and Payments page for STANDARD Verify user level
	 * wallet appears on Billing and Payments page for RESTRICTED
	 * 
	 * @param data
	 * @param myTmoData This test is excluded as per the latest profile page
	 *                  changes. This functionality no longer exists.
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testProfilePaymnetMethod(ControlTestData data, MyTmoData myTmoData) {
		logger.info("TestPaymnetMethod method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Verify with  (billing & payments tab)	");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Profile page | Profile should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Verify Billing and paymnets");
		Reporter.log("6.Click Billing and paymnets|Payment method should be displayed");
		Reporter.log("7.Click on Paymentmethod page|Device intentpage should load");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickBillingPayments();
		BillingPaymentsPage billingAndPaymentsPage = new BillingPaymentsPage(getDriver());
		billingAndPaymentsPage.verifyBreadCrumbActiveStatus("Billing Address");
		billingAndPaymentsPage.clickPaymentmethod();
		SpokePage paymentsCollectionPage = new SpokePage(getDriver());
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.verifyNewPageUrl();
	}
}
