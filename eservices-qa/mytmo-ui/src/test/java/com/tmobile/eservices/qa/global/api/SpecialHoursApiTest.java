package com.tmobile.eservices.qa.global.api;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.SpecialHoursApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class SpecialHoursApiTest extends SpecialHoursApi {
	
	
	/**
	 * US591770: [Continued] SH Internal Tool | UI & API integration - Create event
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SpecialHoursApiTest","testCreateSpecialHoursEvent",Group.GLOBAL,Group.SPRINT  })
	public void testCreateSpecialHoursEvent(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: createSpecialHoursEvent test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with new event created.");
		Reporter.log("Step 2: Verify Event created successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile("SpecialHours_createEvent.txt");
		System.out.println("requestBody = "+requestBody);
		String operationName="testCreateSpecialHoursEvent";
		Response response =createSpecialHoursEvents(apiTestData, requestBody);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {	
			logSuccessResponse(response, operationName);
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * US616515: [Continued] SH Internal Tool | UI & API integration - retrieve events
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SpecialHoursApiTest","testGetSpecialHoursEvents",Group.GLOBAL,Group.SPRINT  })
	public void testGetSpecialHoursEvents(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getSpecialHoursEvents test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with special hours event details.");
		Reporter.log("Step 2: Verify special hours event details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetSpecialHoursEvents";
		Response response =getSpecialHoursEvents(apiTestData);
		Reporter.log("Success response code: "+response.getStatusCode()+" is displayed");
		Reporter.log("Special hours event details returned successfully");
        if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String reasonName= getPathVal(jsonNode, "reasonName");
				String reasonText= getPathVal(jsonNode, "reasonText");
				String eventType= getPathVal(jsonNode, "eventType");
				String eventDescription= getPathVal(jsonNode, "eventDescription");
				String startDate= getPathVal(jsonNode, "startDate");
				String endDate= getPathVal(jsonNode, "endDate");
				String opens= getPathVal(jsonNode, "opens");
				String closes= getPathVal(jsonNode, "closes");
				String userEmail= getPathVal(jsonNode, "userEmail");
				String transactionDate= getPathVal(jsonNode, "transactionDate");
				String eventId= getPathVal(jsonNode, "eventId");
				
				Assert.assertNotNull(reasonName,"Invalid reasonName");
				Assert.assertNotNull(reasonText,"Invalid reasonText");
				Assert.assertNotNull(eventType,"Invalid eventType");
				Assert.assertNotNull(eventDescription,"Invalid eventDescription");
				Assert.assertNotNull(startDate,"Invalid startDate");
				Assert.assertNotNull(endDate,"Invalid endDate");
				Assert.assertNotNull(opens,"Invalid opens");
				Assert.assertNotNull(closes,"Invalid closes");
				Assert.assertNotNull(userEmail,"Invalid userEmail");
				Assert.assertNotNull(transactionDate,"Invalid transactionDate");
				Assert.assertNotNull(eventId,"Invalid eventId");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	
	/**
	 * US591770: [Continued] SH Internal Tool | UI & API integration - Create event
	 * US616515: [Continued] SH Internal Tool | UI & API integration - retrieve events
	 * US616517: [Continued] SH Internal Tool | UI & API integration - delete events
	 *  
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SpecialHoursApiTest","testCreateRetrieveDeleteSpecialHoursEvent",Group.GLOBAL,Group.SPRINT  })
	public void testCreateRetrieveDeleteSpecialHoursEvent(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: createSpecialHoursEvent test");
		Reporter.log("TestName: getSpecialHoursEvents test");
		Reporter.log("TestName: deleteSpecialHoursEvent test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the request to create new event created | Request to create an event displayed successfully");
		Reporter.log("Step 2: Get the store Id's from the request | Store Id's parsed to create an event displayed successfully");
		Reporter.log("Step 3: Get the response with new event created.");
		Reporter.log("Step 4: Verify Event created successfully or not");
		Reporter.log("Step 5: Verify Success services response code for Create event | Response code should be 200 OK.");
		Reporter.log("Step 6: Get the response with special hours event details.");
		Reporter.log("Step 7: Verify Success services response code for Get event | Response code should be 200 OK.");
		Reporter.log("Step 8: Verify special hours event details returned successfully or not");
		Reporter.log("Step 9: Verify Store Id's retrieved | Store Id's retrieved by Get call, after creating an event ");
		Reporter.log("Step 10: Verify Event Id for the above stores | Event ID for the above stores should be displayed");
		Reporter.log("Step 11: Get the response with event deleted.");
		Reporter.log("Step 12: Verify Success services response code for Delete event | Response code should be 200 OK.");
		Reporter.log("Step 13: Verify Event gets deleted | Successfully deleted an event");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile("SpecialHours_createRetrieveDeleteEvent.txt");
		Reporter.log("Request to create an event: "+requestBody);
		ObjectMapper mapper_create = new ObjectMapper();
			JsonNode jsonNode = mapper_create.readTree(requestBody.toString());
			String storeId_create= getPathVal(jsonNode, "storeIds");
			Reporter.log("Store ID's parsed to Create an Event: "+storeId_create);
			String operationName_create="testCreateSpecialHoursEvent";
			Response response_create =createSpecialHoursEvents(apiTestData, requestBody);
			if (response_create != null && "200".equals(Integer.toString(response_create.getStatusCode()))) {	
				Reporter.log("Success response code for Create event: "+response_create.getStatusCode()+" is displayed");
				logSuccessResponse(response_create, operationName_create);
			}else{
				failAndLogResponse(response_create, operationName_create);
			}	
			String matchedEventId = "";
			String operationName_get="testGetSpecialHoursEvents";
			Response response_get =getSpecialHoursEvents(apiTestData);
	        if (response_get != null && "200".equals(Integer.toString(response_get.getStatusCode()))) {
	        	Reporter.log("Success response code for Get event: "+response_get.getStatusCode()+" is displayed");
				Reporter.log("Special hours event details returned successfully");
				logSuccessResponse(response_get, operationName_get);
				ObjectMapper mapper_get = new ObjectMapper();
				JsonNode jsonNode2 = mapper_get.readTree(response_get.asString());
				
				for (int i=0; i<jsonNode2.size();i++) {
					if (!jsonNode2.get(i).isMissingNode()) {
					String reasonName= getPathVal(jsonNode2.get(i), "reasonName");
					String reasonText= getPathVal(jsonNode2.get(i), "reasonText");
					String eventType= getPathVal(jsonNode2.get(i), "eventType");
					String eventDescription= getPathVal(jsonNode2.get(i), "eventDescription");
					String startDate= getPathVal(jsonNode2.get(i), "startDate");
					String endDate= getPathVal(jsonNode2.get(i), "endDate");
					String opens= getPathVal(jsonNode2.get(i), "opens");
					String closes= getPathVal(jsonNode2.get(i), "closes");
					String userEmail= getPathVal(jsonNode2.get(i), "userEmail");
					String transactionDate= getPathVal(jsonNode2.get(i), "transactionDate");
					String eventId_get= getPathVal(jsonNode2.get(i), "eventId");
					String storeId_get= getPathVal(jsonNode2.get(i), "storeIds");

					Assert.assertNotNull(reasonName,"Invalid reasonName");
					Assert.assertNotNull(reasonText,"Invalid reasonText");
					Assert.assertNotNull(eventType,"Invalid eventType");
					Assert.assertNotNull(eventDescription,"Invalid eventDescription");
					Assert.assertNotNull(startDate,"Invalid startDate");
					Assert.assertNotNull(endDate,"Invalid endDate");
					Assert.assertNotNull(opens,"Invalid opens");
					Assert.assertNotNull(closes,"Invalid closes");
					Assert.assertNotNull(userEmail,"Invalid userEmail");
					Assert.assertNotNull(transactionDate,"Invalid transactionDate");
					Assert.assertNotNull(eventId_get,"Invalid eventId");
					Assert.assertNotNull(storeId_get,"Invalid storeId");

					if(storeId_create.equals(storeId_get)) {
						matchedEventId = eventId_get;
					Reporter.log("Store Id's retrieved by Get call, after creating an event: "+storeId_get);
					Reporter.log("Event ID for the above stores: "+eventId_get);
					break;		
				}
			}else{
				failAndLogResponse(response_get, operationName_get);
			}
	        }  
	        }     
	        String operationName_delete="testDeleteSpecialHoursEvent";
			Response response_delete =deleteSpecialHoursEvent(apiTestData, matchedEventId);
			Assert.assertEquals("200", Integer.toString(response_delete.getStatusCode()));
			if (response_delete != null && "200".equals(Integer.toString(response_delete.getStatusCode()))) {
	        	Reporter.log("Success response code for Delete event: "+response_delete.getStatusCode()+" is displayed");
				Reporter.log("Successfully deleted an event");
				logSuccessResponse(response_delete, operationName_delete);
			}else{
				failAndLogResponse(response_delete, operationName_delete);
			}
	
	}
	
	
	/**
	 * DE259685: SH Internal Tool | Stores are not filtered by FPR/TPR in store search results
	 *  
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SpecialHoursApiTest", "testGetFpTprStoresByCityAndState", Group.GLOBAL, Group.SPRINT })
	public void testGetFpTprStoresByCityAndState(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getFpTprStoresByCityAndState test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with store list count.");
		Reporter.log("Step 2: Verify store List details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code | Response code should be 200 OK.");
		Reporter.log("Step 4: Verify the total count of FPR and TPR stores | FPR and TPR stores count displayed successfully.");
		Reporter.log("Step 5: Verify the count of FPR stores | FPR stores count displayed successfully.");
		Reporter.log("Step 6: Verify the count of TPR stores | TPR stores count displayed successfully.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testGetStoresInCityState";
		Response response = getStoresByCityState(apiTestData, "wa", "bellevue");
		Reporter.log("Success response code: " + response.getStatusCode() + " is displayed");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode storeList = jsonNode.path("storeList");
			String storeCount = getPathVal(jsonNode, "storeCount");
			Reporter.log("Total stores count is: " + storeCount);
			int fprCount = 0;
			int tprCount = 0;
			for (int i = 0; i < storeList.size(); i++) {
				if (!storeList.isMissingNode() && storeList.isArray()) {
					JsonNode dataNode = storeList.get(i);
					if (dataNode.isObject()) {
						String storeDefinition = dataNode.path("storeDefinition").textValue();
						Reporter.log("Stores: " + storeDefinition);
						if (storeDefinition.equals("(FPR)First Party Retail")) {
							fprCount++;
						} else if (storeDefinition.equals("(TPR)Third Party Retail")) {
							tprCount++;
						}
					}
				}
			}
			Reporter.log("FPR stores count:" + fprCount);
			Reporter.log("TPR stores count:" + tprCount);
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	
}

