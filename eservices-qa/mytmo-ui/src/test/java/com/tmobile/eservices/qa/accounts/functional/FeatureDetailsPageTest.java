/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.accounts.FeatureDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.PlanDetailsPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class FeatureDetailsPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(FeatureDetailsPageTest.class);

	/*
	 * VerifyFeatureDescriptionfromplandetailspage US414647 : Feature Details Page:
	 * Descriptive Text for the Feature
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyFeatureDescriptionfromplandetailspage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyFeatureDescriptionfromplandetailspage");
		Reporter.log("Test Case : Verify that Feature descriptive text for the feature in Account Level");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log("5.Verify that List of Plan Feature | All the feature should be displayed on plan details Page ");
		Reporter.log("6.Clik on the feature from the listed |Application should navigate to Feature Details Page ");
		Reporter.log("7.Verify that Descriptive Text for the Feature | Descriptive Text is available for the feature");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage planDetailsPage = new PlanDetailsPage(getDriver());
		planDetailsPage.verifyPlanDetailsPageHeaders();
		planDetailsPage.clickOnIncludedPlan();

		// featureDetailsPage.verifyFeatureDetailsPage();
		// featureDetailsPage.verifyDescriptiveText();

	}

	/*
	 * VerifyFeatureDescriptionfromlinedetailspage US414647 : Feature Details Page:
	 * Descriptive Text for the Feature
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyFeatureDescriptionfromlinedetailspage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyFeatureDescriptionfromlinedetailspage");
		Reporter.log("Test Case : Verify that Feature descriptive text for the feature in LineLevel");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Line | User should be navigated to LineDetails Page ");
		Reporter.log("5.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log("6.Verify that List of Plan Feature | All the feature should be displayed on plan details Page ");
		Reporter.log("7.Clik on the feature from the listed |Application should navigate to Feature Details Page ");
		Reporter.log("8.Verify that Descriptive Text for the Feature | Descriptive Text is available for the feature");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PlanDetailsPage planDetailsPage = navigateToPlanDetailsPage(myTmoData);

		planDetailsPage.verifyPlanDetailsPageHeaders();
		planDetailsPage.clickOnIncludedPlan();

		FeatureDetailsPage featureDetailsPage = new FeatureDetailsPage(getDriver());

		featureDetailsPage.verifyFeatureDetailsPage();
		featureDetailsPage.verifybenefitDetails();
	}

	/*
	 * VerifyBenefitDetailsHeadingOnAccountLevel US414642 : Feature Details
	 * Page_Page Heading
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyBenefitDetailsHeadingOnAccountLevel(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBenefitDetailsHeadingOnAccountLevel");
		Reporter.log("Test Case : Verify that BenefitDetails Heading for the feature in Account Level");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log("5.Verify that List of Plan Feature | All the feature should be displayed on plan details Page ");
		Reporter.log("6.Clik on the feature from the listed |Application should navigate to Feature Details Page ");
		Reporter.log("7.Verify that Descriptive Text for the Feature | Descriptive Text is available for the feature");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage planDetailsPage = new PlanDetailsPage(getDriver());
		planDetailsPage.verifyPlanDetailsPageHeaders();
		planDetailsPage.clickOnIncludedPlan();

		// featureDetailsPage.verifyFeatureDetailsPage();
		// featureDetailsPage.verifybenefitDetails();
	}

	/*
	 * VerifyBenefitDetailsHeadingOnLineLevel US414642 : Feature Details Page_Page
	 * Heading
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyBenefitDetailsHeadingOnLineLevel(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBenefitDetailsHeadingOnLineLevel");
		Reporter.log("Test Case : Verify that BenefitDetails Heading for the feature in LineLevel");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Line | User should be navigated to LineDetails Page ");
		Reporter.log("5.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log("6.Verify that List of Plan Feature | All the feature should be displayed on plan details Page ");
		Reporter.log("7.Clik on the feature from the listed |Application should navigate to Feature Details Page ");
		Reporter.log("8.Verify that BenefitHeading | BenefitHeading is available ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage planDetailsPage = new PlanDetailsPage(getDriver());
		planDetailsPage.verifyPlanDetailsPageHeaders();
		planDetailsPage.clickOnIncludedPlan();

		// featureDetailsPage.verifyFeatureDetailsPage();
		// featureDetailsPage.verifybenefitDetails();

	}

	/*
	 * VerifyBackCTAonFeatureDetailsPage US414653 : Feature Details Page: Back CTA
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyBackCTAonFeatureDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBackCTAonFeatureDetailsPage");
		Reporter.log("Test Case : Verify that Back CTA on Feature Details Page");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log("5.Verify that List of Plan Feature | All the feature should be displayed on plan details Page ");
		Reporter.log("6.Clik on the feature from the listed |Application should navigate to Feature Details Page ");
		Reporter.log("7.Click on Back CTA | Application should navigate back to PlanDetails Page ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PlanDetailsPage planDetailsPage = navigateToPlanDetailsPage(myTmoData);

		planDetailsPage.verifyPlanDetailsPageHeaders();
		planDetailsPage.clickOnIncludedPlan();

		FeatureDetailsPage featureDetailsPage = new FeatureDetailsPage(getDriver());
		featureDetailsPage.verifyFeatureDetailsPage();
		featureDetailsPage.clickBackCTA();
		planDetailsPage.verifyPlanDetailsPage();
	}
}