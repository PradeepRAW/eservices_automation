package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.PaymentsWalletsV4;

import io.restassured.response.Response;

public class PaymentsWalletsV4Test extends PaymentsWalletsV4 {

	public Map<String, String> tokenMap;

	/**
	/**
	 * UserStory# US577077: Validate V4;
	 * UserStory# 
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV4Test","testVerifyPaymentMethodCardv4",Group.PAYMENTS,Group.SPRINT  })
	public void testVerifyPaymentMethodCardv4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testVerifyPaymentMethodCardv4");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with validatePaymentMethod API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testVerifyPaymentMethodCardv4";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("VerifyPaymentMethodCardV4.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("tmoid", apiTestData.gettmoId());
		tokenMap.put("cardAlias", apiTestData.getCardAlias());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = verifypaymentmethod(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"status"), 
						"SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode,"reasonCode"),
						"2710" , "Reason Code is Mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"reasonDescription"),
						"Verified Successfully" , "Reason Description is Mismatched");
				
							}
		} else {

			failAndLogResponse(response, operationName);
		}
	}


	/**
	 * UserStory# US577077: Validate V4;
	 * UserStory# 
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV4Test","testVerifyPaymentMethodBankv4",Group.PAYMENTS,Group.SPRINT  })
	public void testVerifyPaymentMethodBankv4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testVerifyPaymentMethodBankv4");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with validatePaymentMethod API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testVerifyPaymentMethodBankv4";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("VerifyPaymentMethodBankV4.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("tmoid", apiTestData.gettmoId());
		tokenMap.put("bankAccountAlias", apiTestData.getBankAccountAlias());
		tokenMap.put("bankroutingNo", apiTestData.getRoutingNo());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = verifypaymentmethod(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"status"), 
						"SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode,"reasonCode"),
						"2710" , "Reason Code is Mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"reasonDescription"),
						"Verified Successfully" , "Reason Description is Mismatched");
				
							}
		} else {

			failAndLogResponse(response, operationName);
		}
	}




	/**
	/**
	 * UserStory# US577042: Update Create to V4;
	 * UserStory# 
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV4Test","testCreatePaymentMethodWithCardv4",Group.PAYMENTS,Group.SPRINT  })
	public void testCreatePaymentMethodWithCardv4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testCreatePaymentMethodWithCardv4");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with mpi API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testCreatePaymentMethodWithCardv4";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("CreateandPaymentWalletCardV4.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("tmoid", apiTestData.gettmoId());
		

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = WalletCreation(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"status"), 
						"COMPLETED", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode,"reasonCode"),
						"2491" , "Reason Code is Mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"reasonDescription"),
						"Transaction completed successfully" , "Reason Description is Mismatched");
				
							}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	/**
	 * UserStory# US577042: Update Create to V4;
	 * UserStory# 
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV4Test","testCreatePaymentMethodWithBankv4",Group.PAYMENTS,Group.SPRINT  })
	public void testCreatePaymentMethodWithBankv4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testCreatePaymentMethodWithBankv4");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with mpi API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testCreatePaymentMethodWithBankv4";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("CreateandPaymentWalletBankV4.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("tmoid", apiTestData.gettmoId());
		

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = WalletCreation(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"status"), 
						"COMPLETED", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode,"reasonCode"),
						"2491" , "Reason Code is Mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"reasonDescription"),
						"Transaction completed successfully" , "Reason Description is Mismatched");
				
							}
		} else {

			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * US534366: Update update to V4
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV4Test","testUpdatePaymentWallet",Group.PAYMENTS,Group.SPRINT  })
	public void testUpdatev4PaymentWallet(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testUpdatev4PaymentWallet");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with UpdatePaymentWallet API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testUpdatePaymentWallet";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("UpdatePaymentWalletv4.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("cardHolderFirstName", apiTestData.getCardHolderFirstName());
		tokenMap.put("cardHolderLastName", apiTestData.getCardHolderLastName());
		tokenMap.put("nickName", apiTestData.getNickName());
		tokenMap.put("expirationMonthYear", apiTestData.getExpirationMonthYear());
		tokenMap.put("cvv", apiTestData.getCvvCode());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = walletUpdate(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"status"), 
						"SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode,"reasonDescription"),
						"Transaction completed successfully" , "Reason Description is Mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"reasonCode"),
						"2491" , "Reason Code is Mismatched");
							}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US534365: Update search to V4
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV4Test","testSearchv4PaymentWallet",Group.PAYMENTS,Group.SPRINT  })
	public void testSearchv4PaymentWallet(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Update search to V4");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with SearchPaymentWallet API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testSearchPaymentWalletv4";

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		Response response = walletSearch(apiTestData,tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertNotNull(getPathVal(jsonNode,"paymentInstrumentList.walletId"),"walletId node missing");
				Assert.assertNotNull(getPathVal(jsonNode,"paymentInstrumentList.paymentMethodStatus"),"paymentMethodStatus node missing");
			}
		} else {

			failAndLogResponse(response, operationName);
		}


	}


	/**
	/**
	 * US534367: Update Delete to V4
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV4Test","testDeletev4PaymentWallet",Group.PAYMENTS,Group.SPRINT  })
	public void testDeletev4PaymentWallet(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testDeletev4PaymentWallet");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with DeletePaymentWallet API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testDeletePaymentWallet";

		String requestBody =  new ServiceTest().getRequestFromFile("DeletePaymentWalletv4.txt");	

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("cardAlias", apiTestData.getCardAlias());
		
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		
		Response response = walletDelete(apiTestData, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"status"), 
						"COMPLETED", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode,"reasonDescription"),
						"Transaction completed successfully" , "Reason Description is Mismatched");
				
							}
		} else {

			failAndLogResponse(response, operationName);
		}
	}


	/**
	 * UserStory# US577077: Validate V4;
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV4Test","testValidatePaymentMethodCardv4",Group.PAYMENTS,Group.SPRINT  })
	public void testValidatePaymentMethodCardv4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testValidatePaymentMethodCardv4");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with validatePaymentMethod API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testValidatePaymentMethodCardv4";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("ValidatePaymentMethodCardV4.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		Response response = validatePaymentMethod(apiTestData, requestBody, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"status"), 
						"VALID", " Status is Mismatched ");
							}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# US577077: Validate V4;
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV4Test","testValidatePaymentMethodBankv4",Group.PAYMENTS,Group.SPRINT  })
	public void testValidatePaymentMethodBankv4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testValidatePaymentMethodBankv4");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with validatePaymentMethod API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testValidatePaymentMethodBankv4";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("ValidatePaymentMethodBankV4.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("bankroutingNo", apiTestData.getRoutingNo());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = validatePaymentMethod(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"status"), 
						"SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode,"reasonCode"),
						"2710" , "Reason Code is Mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"reasonDescription"),
						"Verified Successfully" , "Reason Description is Mismatched");
				
							}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# US577077: Validate V4;
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV4Test","testValidateBankRoutingNumberv4",Group.PAYMENTS,Group.SPRINT  })
	public void testValidateBankRoutingNumberv4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testValidateBankRoutingNumberv4");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with validatePaymentMethod API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testValidateBankRoutingNumberv4";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("ValidateBankRoutingNumberV4.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("bankroutingNo", apiTestData.getRoutingNo());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = validateRoutingNumber(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertNotNull(jsonNode.get("bankName"), "Bank Name is empty in response");
							}
		} else {

			failAndLogResponse(response, operationName);
		}
	}
}
