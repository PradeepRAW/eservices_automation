/**
 * 
 */
package com.tmobile.eservices.qa.monitors;

import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.ShopAPIMonitorsHelper;
import com.tmobile.eservices.qa.api.eos.CustomerAccountApiV1;
import com.tmobile.eservices.qa.api.eos.JWTTokenApi;
import com.tmobile.eservices.qa.api.soap.AccountMezooServices;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

/**
 * @author csudheer
 *
 */
public class AccountsAPIMonitorsHelper extends ApiCommonLib {

	CustomerAccountApiV1 customerAccountApiV1 = new CustomerAccountApiV1();

	public Response response = null;
	public String requestBody = null;
	public String updatedRequest = "";
	JWTTokenApi jwtTokenApi = new JWTTokenApi();

	AccountMezooServices accountMezooServices = new AccountMezooServices();
	ShopAPIMonitorsHelper eOSAPIMonitorsHelper = new ShopAPIMonitorsHelper();

	void verifyLogin(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);

		if (null != jwtTokens.get("jwtToken") && null != jwtTokens.get("accessToken")) {
			Reporter.log("Login Successful");
		} else {
			Reporter.log(" <b>Login Not Successful :</b> ");
			Assert.fail("<b>" + "Login Not Successful" + " Response :</b> " + " response.getStatusCode() : "
					+ response.body().asString());
			throw new Exception("<b>" + "Login Not Successful" + " Response :</b> " + " response.getStatusCode() : "
					+ response.body().asString());
		}
	}

	private Map<String, String> verifyNumberOfActiveLines(Response response, Map<String, String> tokenMap) {
		System.out.println("Response " + response.asString());

		tokenMap.put("numberOfActiveLines", response.xmlPath()
				.getString("Envelope.Body.getLinesResponse.getLinesResponse.account.lines[0].numberOfLinesActive"));

		int numberOfActiveLine = Integer.parseInt(tokenMap.get("numberOfActiveLines"));

		if (numberOfActiveLine >= 1) {
			Reporter.log("<b>" + "Number Of Active Lines :" + numberOfActiveLine + "</b> ");
		} else {
			Assert.fail("<b>" + "Number of lines should not be Zero" + " Response :</b> "
					+ " response.getStatusCode() : " + response.body().asString());
			Reporter.log("<b>" + "Number of lines should not be Zero" + " Exception Response :</b> ");
		}
		return tokenMap;
	}

	private void validateResposeCode(Response response, String operationName) {

		if (response.statusCode() == 200) {
			//System.out.println(response.asString());
			logSuccessResponse(response, operationName);
		} else {
			Assert.fail("<b>" + operationName + " Response :</b> " + " response.getStatusCode() : "
					+ response.body().asString());
			Reporter.log("Response status code : " + response.getStatusCode());
		}
	}

	public void callService(Map<String, String> tokenMap, String fileName, String requestType, String url)
			throws Exception {
		requestBody = new ServiceTest().getRequestFromFile(fileName);
		updatedRequest = prepareRequestParamXML(requestBody, tokenMap);
		logXMLRequest(updatedRequest, requestType);
		response = accountMezooServices.getSoapUrl(updatedRequest, requestType, url);
		logLargeResponse(response, requestType);
		validateResposeCode(response, requestType);
	}

	public void invokeGetCustomerAccountDetails(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		String operationName = "CustomerAccountApi-getCustomerAccountDetails";
		Response rs = customerAccountApiV1.getCustomerAccountDetails(apiTestData, tokenMap);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				String accountNumber = getPathVal(jsonNode, "accountNumber");
				Assert.assertEquals(accountNumber, tokenMap.get("ban"), "Invalid ban.");
				String accountType = getPathVal(jsonNode, "accountType");
				Assert.assertNotEquals(accountType, "", "Invalid accountType.");
				String accountSubType = getPathVal(jsonNode, "accountSubType");
				Assert.assertNotEquals(accountSubType, "", "Invalid accountSubType.");
			}
		} else {
			failAndLogResponse(rs, operationName);
		}
	}

	public Map<String, String> invokeGetSubScriberAccount(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		CustomerAccountApiV1 customerAccountApiV1 = new CustomerAccountApiV1();
		response = customerAccountApiV1.getCustomerAccountDetails(apiTestData, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "getCustomerAccountDetails");
			tokenMap = prepareTokenMap("getCustomerAccountDetails", response, tokenMap);
			Reporter.log("Get Customer Account Details Verified");
		} else {
			failAndLogResponse(response, "getCustomerAccountDetails");
		}
		return tokenMap;
	}

	public Map<String, String> invokeGetSubScriberLines(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		CustomerAccountApiV1 customerAccountApiV1 = new CustomerAccountApiV1();
		response = customerAccountApiV1.getSubscriberLines(apiTestData, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "getSubscriberLines");
			tokenMap = prepareTokenMap("getSubscriberLines", response, tokenMap);
			Reporter.log("Get Subscriber Lines Verified");
		} else {
			failAndLogResponse(response, "getSubscriberLines");
		}
		return tokenMap;
	}

	public Map<String, String> prepareTokenMap(String requestName, Response response, Map<String, String> tokenMap)
			throws Exception {

		switch (requestName) {

		case "getSubScriberLines":
			tokenMap = getSubScriberLines(response, tokenMap);
			break;
		case "getSubScriberAccount":
			tokenMap = getSubScriberAccount(response, tokenMap);
			break;
		}
		return tokenMap;
	}

	private Map<String, String> getSubScriberLines(Response response, Map<String, String> tokenMap) {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {
			/*
			 * tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
			 * tokenMap.put("quotePrice", getPathVal(jsonNode, "quotePrice"));
			 */
		}
		return tokenMap;
	}

	private Map<String, String> getSubScriberAccount(Response response, Map<String, String> tokenMap) {

		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {
			/*
			 * tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
			 * tokenMap.put("quotePrice", getPathVal(jsonNode, "quotePrice"));
			 */
		}
		return tokenMap;
	}

	/*private JsonNode getParentNodeFromResponse(Response response) {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;
		try {
			jsonNode = mapper.readTree(response.asString());
		} catch (JsonProcessingException e) {
			Assert.fail("<b>JsonProcessingException Response :</b> " + e);
			Reporter.log(" <b>JsonProcessingException Response :</b> ");
			Reporter.log(" " + e);
			e.printStackTrace();
		} catch (IOException e) {
			Assert.fail("<b>IOException Response :</b> " + e);
			Reporter.log(" <b>IOException Response :</b> ");
			Reporter.log(" " + e);
			e.printStackTrace();
		}
		return jsonNode;
	}*/

	public void invokePhoneTabServices(ApiTestData apiTestData, Map<String, String> tokenMap,String pValue) throws Exception {

		testGetSuspensionHistory(apiTestData, tokenMap,pValue);
		testGetSubscriptionDetails(apiTestData, tokenMap);
	}

	public Map<String, String> testGetSuspensionHistory(ApiTestData apiTestData, Map<String, String> tokenMap,String pValue) throws Exception {
		callService(tokenMap, "getSuspensionHistory_XMLRequest.txt", "Get Suspension History", "getProfile");
		Assert.assertTrue(response.xmlPath().getString("suspendRemovalReasonCode").contains(pValue), "Suspend Removal Reason Code Incorrect");
		return tokenMap;
	}

	public void testGetSubscriptionDetails(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		callService(tokenMap, "getSubscriptionDetails_XMLRequest.txt", "Get Subscription Details", "getPhone");
		Assert.assertTrue(response.xmlPath().getString("msisdn").contains(apiTestData.getMsisdn()), "Suspend Removal Reason Code Incorrect");

	}

	public void invokeSelectASublineServices(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		testGetSubscriberServiceList(apiTestData, tokenMap);
	}

	public void testGetSubscriberServiceList(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		callService(tokenMap, "getDeviceUnlockAttributes_Composite_XMLRequest.txt", "Get Device Unlock Attributes",
				"getPhone");
		Assert.assertTrue(response.xmlPath().getString("unlockEligible").contains("true"), "Unlock Eligible Code Incorrect");
	}

	public void invokeSelectReportLostOrStolenLinkServices(ApiTestData apiTestData, Map<String, String> tokenMap,String pValue)
			throws Exception {
		testGetProfile(apiTestData, tokenMap,pValue);
	}

	public void testGetProfile(ApiTestData apiTestData, Map<String, String> tokenMap,String pValue) throws Exception {
		callService(tokenMap, "getProfile_XMLRequest.txt", "Get profile", "getProfile");
		Assert.assertTrue(response.xmlPath().getString("firstName").contains(pValue), "firstName is Incorrect");		
	}

	public void invokeSuspendOrKeepTheLineServices(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		testBlockDeviceRequest(apiTestData, tokenMap);
		Assert.assertTrue(response.xmlPath().getString("description").contains("blocked with reason code"), "RSP is Incorrect");		
	}

	public void testBlockDeviceRequest(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		callService(tokenMap, "updateBlockDevice_XML.txt", "Get Device Unlock Attributes", "getPhone");
	}

	public void invokeDeviceUnlockServices(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		testGetDeviceUnlockAttributes_Composite(apiTestData, tokenMap);
	}

	public void testGetDeviceUnlockAttributes_Composite(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		callService(tokenMap, "getDeviceUnlockAttributes_Composite_XMLRequest.txt", "Get Device Unlock Attributes",
				"getPhone");
		Assert.assertTrue(response.xmlPath().getString("unlockEligible").contains("true"), "Unlock Eligible is Incorrect");		
	}

	public void invokePlanTabServices(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {

		testGetHandsetInfo(apiTestData, tokenMap);
		// testGetEligibleDiscounts(apiTestData, tokenMap);
	}

	public void testGetHandsetInfo(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		callService(tokenMap, "getHandsetInfo_XMLRequest.txt", "Get Handset Info", "getPhone");
		Assert.assertTrue(response.xmlPath().getString("manufacturerName").contains("Samsung"), "Manufacturer Name is Incorrect");
	}

	public void invokeSelectManageAddOnsServices(ApiTestData apiTestData, Map<String, String> tokenMap,String pValue)
			throws Exception {
		testGetSubscriptionDetails(apiTestData, tokenMap);
		testGetProfile(apiTestData, tokenMap,pValue);
		verifyPlanDetails(apiTestData, tokenMap);
	}

	public void verifyPlanDetails(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		callService(tokenMap, "getPlanDetails_XMLRequest.txt", "Get plan details", "getPlan");
		
	}

	public void invokeLinePickerSelectAMsisdn(ApiTestData apiTestData, Map<String, String> tokenMap,String pValue) throws Exception {
		testGetProfile(apiTestData, tokenMap,pValue);
	}

	public void invokeChangePlanAtAccountLevelServices(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		verifyPlanDetails(apiTestData, tokenMap);
	}

	public void invokeChangePlanOnTheFeaturedPlansPage(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		// testGetPlanSelectionOperation(apiTestData, tokenMap);
	}

	public void invokePlanOnPlansComparisonPageSelectPlan(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		// testGetEligibleDiscounts(apiTestData, tokenMap);
		testGetEligibleServices(apiTestData, tokenMap);
		testgetServiceChangeEffectiveDates(apiTestData, tokenMap);
	}

	public void testGetEligibleServices(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		callService(tokenMap, "getEligibleServices_XMLRequest.txt", "Get Eligible Services", "getPlan");
	}

	public void testGetInstallmentPlanHistory(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {

		callService(tokenMap, "getInstallmentPlanHistory_XMLRequest.txt", "Get Installment Plan History", "getPlan");
	}

	public void testgetServiceChangeEffectiveDates(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		callService(tokenMap, "getServiceChangeEffectiveDates_XMLRequest.txt", "Get Service Change Effective Dates",
				"getPlan");
	}
}
