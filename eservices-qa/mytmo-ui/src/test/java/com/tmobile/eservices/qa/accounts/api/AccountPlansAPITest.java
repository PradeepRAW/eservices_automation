/**
 * 
 */
package com.tmobile.eservices.qa.accounts.api;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.api.eos.JWTTokenApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class AccountPlansAPITest extends AccountsApi {

	public Map<String, String> tokenMap;
	
	@BeforeMethod(alwaysRun = true)
	public void refreshTokenMap() {
		tokenMapCommon = null;
	}

	/**
	 * UserStory# Account plans api saas:
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })

	public void testGetAccountPlansRatePlanDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Create Cart");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: OTYyMzgzNDU5 6057281418/Auto12345 ban");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, "testGetAccountPlansRatePlanDetails");
				JsonPath jsonPathEvaluator = response.jsonPath();
				if (jsonPathEvaluator.get("addOns") != null) {
					JsonNode addOnsNode = jsonPathEvaluator.get("addOns");
					JsonNode codeNode = addOnsNode.get(0);
					Assert.assertNotNull(codeNode.asText());
					/*Reporter.log("Rate plan code is displaying as accepted  status code : " + response.getStatusCode());
					String ratePlanName = JsonPath.from(response.asString()).get("ratePlans[*].name").toString();
					Assert.assertTrue(ratePlanName.contains("T-Mobile ONE"), "Reate plan name is not accepted");
					Reporter.log("Rate plan name is displaying as accepted : " + response.getStatusCode());*/
				}

			} else {
				failAndLogResponse(response, "testGetAccountPlansRatePlanDetails");
			}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "yy")

	public void testGetAccountPlansNetflixAndAddonsDetails(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: Create Cart");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: OTYyMzgzNDU5 6057281418/Auto12345 ban");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			logSuccessResponse(response, "testGetAccountPlansNetflixAndAddonsDetails");
			Assert.assertTrue(response.jsonPath().getString("accountAddOns[0].code").contains("FAMNFX13"));
			Assert.assertTrue(response.jsonPath().getString("accountAddOns[0].listPrice").contains("19"));
			Assert.assertTrue(response.jsonPath().getString("accountAddOns[0].nonUsageDiscountsSum").contains("16"));
			Assert.assertTrue(response.jsonPath().getString("accountAddOns[0].netPrice").contains("3"));
			Assert.assertTrue(response.jsonPath().getString("ratePlans[0].code").contains("FRLTULF2"));
			if (response.jsonPath().getString("lines[1].msisdn") == "6057281418") {
				assertThat(response.jsonPath().getList("lines[1].addOns[*].code"),
						hasItems("BMSGS", "CONTCTRL", "FRTMO1PLS", "P360Y5"));
			} else {
				assertThat(response.jsonPath().getList("lines[0].addOns[*].code"),
						hasItems("BMSGS", "CONTCTRL", "FRTMO1PLS", "P360Y5"));
			}

		} else {
			failAndLogResponse(response, "testGetAccountPlansRatePlanDetails");
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true,groups = { Group.ACCOUNTS, Group.APIREG })

	public void testGetAccountPlansAutoPayDiscount(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Create Cart");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: OTYyNDYyMzMy 3125221692/Auto12345 ban");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, "testGetAccountPlansAutoPayDiscount");
				Assert.assertTrue(response.jsonPath().getString("autoPayEnabled").contains("true"));
				if (response.jsonPath().getString("accountDiscounts[0].name") == "AutoPay Discount") {
					Assert.assertNotNull(response.jsonPath().getString("accountDiscounts[0].amount"));
				} else {
					Assert.assertNotNull(response.jsonPath().getString("accountDiscounts[1].amount"));
				}
		} else {
			failAndLogResponse(response, "testGetAccountPlansAutoPayDiscount");
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true,  groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetAccountPlansMBBLine(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Create Cart");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: OTU5MDkxMjYx 4254997731/Auto12345 ban");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);
		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "testGetAccountPlansMBBLine");
			Assert.assertTrue(response.jsonPath().getString("autoPayEnabled").contains("true"));
			assertThat(response.jsonPath().getList("lines[0].addOns[*].code"),hasItems("INTELMCAM", "MBBBMSGS", "MBBWEBGRD"));
			
			//Commented below lines, waiting on feedback as to what is required to test based above assert condition which is an inValid json expression ?
			//String expectedCodeValList[] ={"INTELMCAM", "MBBBMSGS", "MBBWEBGRD"};
			/*JsonNode jsonNode = getParentNodeFromResponse(response);
			if (!jsonNode.isMissingNode() && jsonNode.has("lines")) {
				JsonNode linesNodeArray = jsonNode.path("lines");
				if (!linesNodeArray.isMissingNode() && linesNodeArray.isArray()) {
					for (int i = 0; i < linesNodeArray.size(); i++) {
						String codeVal = linesNodeArray.get(i).get("code").asText();
						for(String ecvl:expectedCodeValList) {
							if(ecvl.)
						}
				}
			}*/
					
				
			} else {
				failAndLogResponse(response, "testGetAccountPlansMBBLine");
			}
	}

	@Test(dataProvider = "byColumnName", enabled = true,  groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetAccountPlans_allRatePlanSums_allEquipmentAgreementsSum(ControlTestData data,
			ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Create Cart");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: OTU5MDkxMjYx 4254997731/Auto12345 ban");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);
		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "testGetAccountPlans_allRatePlanSums_allEquipmentAgreementsSum");
			Assert.assertTrue(response.jsonPath().getString("autoPayEnabled").contains("true"));
			Assert.assertNotNull(response.jsonPath().getString("allRatePlanSums.listPriceSums"));
			Assert.assertNotNull(response.jsonPath().getString("allRatePlanSums.discountSums"));
			Assert.assertNotNull(response.jsonPath().getString("allRatePlanSums.netPriceSums"));
			Assert.assertNotNull(response.jsonPath().getString("allEquipmentAgreementsSum.listPriceSums"));
			Assert.assertNotNull(response.jsonPath().getString("allEquipmentAgreementsSum.discountSums"));
			Assert.assertNotNull(response.jsonPath().getString("allEquipmentAgreementsSum.netPriceSums"));

		} else {
			failAndLogResponse(response, "testGetAccountPlans_allRatePlanSums_allEquipmentAgreementsSum");
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true,  groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetAccountPlans_Essentialplan(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Create Cart");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: OTYyODE2MTIy 3609327641/Auto12345 ban");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);
		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "testGetAccountPlans_Essentialplan");
			String ratePlanCode = JsonPath.from(response.asString()).get("ratePlans[*].code").toString();
			Assert.assertTrue(ratePlanCode.contains("TMESNTL2"), "Rate plan code not accepted");
			String ratePlanName = JsonPath.from(response.asString()).get("ratePlans[*].name").toString();
			Assert.assertTrue(ratePlanName.contains("T-Mobile Essentials"), "Reate plan name is not accepted");
			Assert.assertFalse(response.jsonPath().getList("ratePlans[*].capabilities[*].soc").contains("NOU"));
			Assert.assertTrue(response.jsonPath().getString("ratePlans[*].description")
					.contains("Get just the essentials: unlimited talk, text, and high-speed data."));

			List<String> capabilities = response.jsonPath().getList("ratePlans[*].capabilities[*].name");
			Assert.assertTrue(capabilities.size() == 14);

		} else {
			failAndLogResponse(response, "testGetAccountPlans_Essentialplan");
		}
	}

}