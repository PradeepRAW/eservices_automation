package com.tmobile.eservices.qa.accounts;

import com.tmobile.eservices.qa.pages.accounts.*;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.commonlib.CommonLibrary;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.shop.PhonePages;
//import io.appium.java_client.AppiumDriver;

public class AccountsCommonLib extends CommonLibrary {

	// NetFLix common methods
	public ManageAddOnsConfirmationPage addNetFLixFamilyAllowanceSoc(MyTmoData myTmoData) {
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.checkfamilyAllowanceCheckBoxs();
		manageAddOnsSelectionPage.verifyAddingFASocDescription();
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ManageAddOnsReviewPage manageAddOnsReviewPage = new ManageAddOnsReviewPage(getDriver());
		manageAddOnsReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		navigateToODFURlWithMsisdn(myTmoData, "home");
		return newODFConfirmationPage;
	}

	public ManageAddOnsConfirmationPage addNetFLixPremiumSoc(MyTmoData myTmoData) {
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.checkNetFlixPremiumSocCheckBox();
		manageAddOnsSelectionPage.verifyAddingPremiumSocDescription();
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ManageAddOnsReviewPage manageAddOnsReviewPage = new ManageAddOnsReviewPage(getDriver());
		manageAddOnsReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		navigateToODFURlWithMsisdn(myTmoData, "home");
		return newODFConfirmationPage;
	}

	public ManageAddOnsConfirmationPage addNetFLixStandardSoc(MyTmoData myTmoData) {
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.checkStandardCheckBoxs();
		manageAddOnsSelectionPage.verifyAddingNetFlixStandatdSocDescription();
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ManageAddOnsReviewPage manageAddOnsReviewPage = new ManageAddOnsReviewPage(getDriver());
		manageAddOnsReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		navigateToODFURlWithMsisdn(myTmoData, "account-overview");
		return newODFConfirmationPage;
	}

	// Navigate to Plan Comparison Page from Account overview-common methods
	public boolean navigateToPlanComparisonPageFromAccountOverViewTest(MyTmoData myTmoData, String planName) {
		ExplorePlanPage exploreplanpage = navigateToExplorePlansPage(myTmoData);
		exploreplanpage.navigateToPlanComparisionPage(planName);

		PlanDetailsPage planDetailsPage = new PlanDetailsPage(getDriver());
		boolean isPlanDetailspage = false;
		if (planDetailsPage.verifyPlanDetailsTitle()) {
			planDetailsPage.clickOnSelectPlanButton();
			isPlanDetailspage = true;
		} else {
			PlanComparisonPage planComparisonPage = new PlanComparisonPage(getDriver());
			planComparisonPage.verifyComparePlan();
		}
		return isPlanDetailspage;
	}

	// Navigate to Plan Comparison Page from Home
	public boolean navigateToPlanComparisonPageTestDirectly(MyTmoData myTmoData, String planName) {
		navigateToExplorePlansPageDirectly(myTmoData);

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.navigateToPlanComparisionPage(planName);

		PlanDetailsPage planDetailsPage = new PlanDetailsPage(getDriver());
		boolean isPlanDetailspage = false;
		if (planDetailsPage.verifyPlanDetailsTitle()) {
			planDetailsPage.clickOnSelectPlanButton();
			isPlanDetailspage = true;
		} else {
			PlanComparisonPage planComparisonPage = new PlanComparisonPage(getDriver());
			planComparisonPage.verifyComparePlan();
		}
		return isPlanDetailspage;
	}

	// Navigate to Plan Review Page from Home
	public void navigateToPlanReviewPageTest(MyTmoData myTmoData, String planName) {
		boolean isPlanDetailspage = navigateToPlanComparisonPageTestDirectly(myTmoData, planName);
		if (!isPlanDetailspage) {
			PlanComparisonPage planComparisonPage = new PlanComparisonPage(getDriver());
			planComparisonPage.choosePlanFromChoosePlanSection(planName);
			planComparisonPage.clickOnSelectPlanCta();
		}
	}

	// Navigate to Explore Plans Page from Account Overview
	public ExplorePlanPage navigateToExplorePlansPage(MyTmoData myTmoData) {

		PlanDetailsPage plandetailspage = navigateToPlanDetailsPage(myTmoData);
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
		return exploreplanpage;
	}

	// Navigate to Explore Plans Page from Home
	public ExplorePlanPage navigateToExplorePlansPageDirectly(MyTmoData myTmoData) {

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickOnChangeMyPlanLink();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
		return exploreplanpage;
	}
	
	// Navigate to Plans Comparison Page via deeplink
	public AccountsCommonLib navigateToPlansComparisonPageDirectly(MyTmoData myTmoData) {

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickOnChangeMyPlanLink();
		
		String[] currentUrl = getDriver().getCurrentUrl().split("/home");
		String comparisonPageURL = currentUrl[0] + "/account/change-plan/plan-comparison";

		getDriver().get(comparisonPageURL);
		PlanComparisonPage planComparisonPage = new PlanComparisonPage(getDriver());
		planComparisonPage.verifyPlanComparisionPage();
		return this;
	}

	// Navigate to ODF url with Msisdn for home or Account overview or other
	// page
	protected AccountsCommonLib navigateToODFURlWithMsisdn(MyTmoData myTmoData, String pageName) {
		try {
			String[] currentUrl = getDriver().getCurrentUrl().split(pageName);
			String odfUrl = currentUrl[0] + "odf/Msisdn:" + myTmoData.getLoginEmailOrPhone()
					+ "/DataService:ALL/Service:ALL/DataPass:ALL";
			getDriver().get(odfUrl);
			Reporter.log("Manage addons page is displayed");
		} catch (Exception e) {
			Assert.fail("Manage addons page is not displayed");
		}
		return this;
	}

	// Navigate to ODF url from any page(Home or plan-confirmation or profile or
	// home ...etc)
	protected AccountsCommonLib navigateToODFUrlFromAnyPage(String pagename) {
		try {
			String[] currentUrl = getDriver().getCurrentUrl().split(pagename);
			String odfUrl = currentUrl[0] + "/odf/DataService:ALL/Service:ALL/DataPass:ALL";
			getDriver().get(odfUrl);
			Reporter.log("ODF page is displayed");
		} catch (Exception e) {
			Assert.fail("ODF page is not displayed");
		}
		return this;
	}

	// Addons Upsell URL
	protected AccountsCommonLib navigateToAddOnsUpsellURL(String urlName, String upsellService) {
		try {
			String[] currentUrl = getDriver().getCurrentUrl().split(urlName);
			String upSellURL = currentUrl[0] + "odf/isUpsell:true/" + upsellService + "";
			getDriver().get(upSellURL);
			Reporter.log("Add Ons Review page is displayed");
		} catch (Exception e) {
			Assert.fail("Add Ons Review page is not displayed");
		}
		return this;
	}

	// navigate to any URL
	public AccountsCommonLib navigateToAnyURL(String urlName, String urlToLoad) {
		String currentURL[] = getDriver().getCurrentUrl().split(urlName);
		currentURL[0] = currentURL[0] + urlToLoad;
		getDriver().get(currentURL[0]);
		return this;
	}

	public AccountsCommonLib navigateToAccountOverviewPage(MyTmoData myTmoData) {
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();

		// homePage.clickOnHomePageHeaderLinks("ACCOUNT");
		String currentURL[] = getDriver().getCurrentUrl().split("home");
		currentURL[0] = currentURL[0] + "account-overview";
		getDriver().get(currentURL[0]);

		AccountOverviewPage accountOverview = new AccountOverviewPage(getDriver());
		accountOverview.verifyAccountOverviewPage();
		return this;
	}

	// login to crazy legs benefit page using url
	public AccountsCommonLib navigateToFutureURLFromHome(MyTmoData myTmoData, String FutureURLName) {
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		String currentURL[] = getDriver().getCurrentUrl().split("home");
		currentURL[0] = currentURL[0] + FutureURLName;
		getDriver().get(currentURL[0]);
		homePage.checkPageIsReady();
		Reporter.log("Navigated to " + getDriver().getCurrentUrl());
		return this;
	}

	protected boolean navigateToChangeDataPlansConfigurationPage(MyTmoData myTmoData) {
		navigateToAccountoverViewPageFlow(myTmoData);
		NewHomePage homePage = new NewHomePage(getDriver());
		homePage.clickChangeMyPlanQuickLink();

		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
		verifyPlansComparisonPageIsDisplayingBestTalkAndTextPlan(myTmoData);
		return plansComparisonPage.verifyPlansComparisonPageBestTalkAndTextPlan();
	}

	// Login to Manage Addon's Confirmation page
	public AccountsCommonLib navigateToManageAddOnsConfirmationPage(MyTmoData myTmoData) {
		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		String activePlan = manageAddOnsSelectionPage.getActiveDataPlan();
		String activeDataPlanPrice = manageAddOnsSelectionPage.getPriceOfDataPlan(activePlan);
		manageAddOnsSelectionPage.selectDataPlan(activePlan);
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ManageAddOnsReviewPage manageAddOnsReviewPage = new ManageAddOnsReviewPage(getDriver());
		manageAddOnsReviewPage.verifyRemovedItemsNames(activePlan);
		manageAddOnsReviewPage.verifyPriceText(activeDataPlanPrice);
		manageAddOnsReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		return this;
	}

	// My Promotions Page
	public LineDetailsPage navigateToMyPromotionsPage(MyTmoData myTmoData) {
		LineDetailsPage lineDetailsPage = navigateToLineDetailsPage(myTmoData);
		lineDetailsPage.clickOnMypromotions();

		verifyMyPromotionsPage();
		return lineDetailsPage;
	}

	/**
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected PlansComparisonPage navigateToNewPlansComaprisonPage(MyTmoData myTmoData) {
		navigateToPlansComparisionPage(myTmoData);
		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
		plansComparisonPage.verifyNewPlansComparisonPageHeader("Your current plan");
		return plansComparisonPage;
	}

	// ODF Review page flow
	protected ManageAddOnsReviewPage navigateToODFDataPassReviewPage(MyTmoData myTmoData) {
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		navigateToODFURlWithMsisdn(myTmoData, "home");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		verifyManageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnDataPassRadioButton();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ManageAddOnsReviewPage manageAddOnsReviewPage = new ManageAddOnsReviewPage(getDriver());
		manageAddOnsReviewPage.verifyMonthlyBillIsNotDispalying();
		return manageAddOnsReviewPage;
	}

	protected ManageAddOnsReviewPage navigateToODFDataPassReviewPage(MyTmoData myTmoData, String dataPassName) {

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyUnblockRoamingAndUnblockIt();
		String price = manageAddOnsSelectionPage.clickOnDataPassRadioButton(dataPassName);
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ManageAddOnsReviewPage manageAddOnsReviewPage = new ManageAddOnsReviewPage(getDriver());
		manageAddOnsReviewPage.verifyDataPassNameAndTextReviewpage(dataPassName, price);
		manageAddOnsReviewPage.verifyMonthlyBillIsNotDispalying();
		return manageAddOnsReviewPage;
	}

	// ODF Review page flow
	protected ManageAddOnsSelectionPage navigateToODFDecreaseReviewPage(MyTmoData myTmoData) {
		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnDecreaseService();
		manageAddOnsSelectionPage.verifyRemoveService();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		return manageAddOnsSelectionPage;
	}

	// ODF Review page flow
	protected ManageAddOnsSelectionPage navigateToODFFreeRadioButtonToReviewPage(MyTmoData myTmoData, String type) {
		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnFreeRadioOrCheckBoxButton(type);
		// manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		return manageAddOnsSelectionPage;
	}

	// ODF Review page flow
	protected ManageAddOnsSelectionPage navigateToODFIncreaseReviewPage(MyTmoData myTmoData) {
		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnNumberRadioButton();
		manageAddOnsSelectionPage.clickOnDataplanConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		return manageAddOnsSelectionPage;
	}

	// ODF Review page flow
	protected ManageAddOnsSelectionPage navigateToODFReviewPageBySelectingDataPass(MyTmoData myTmoData) {

		navigateToHomePage(myTmoData);
		navigateToODFURlWithMsisdn(myTmoData, "home");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		verifyManageDataAndAddOnsPage();

		manageAddOnsSelectionPage.verifyUnblockRoamingAndUnblockIt();
		manageAddOnsSelectionPage.removedAddedDataPass();

		manageAddOnsSelectionPage.clickOnDataPassRadioButton();
		// odfUpgradeYourServicesPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		return manageAddOnsSelectionPage;
	}

	// ODF Review page flow
	protected String navigateToODFReviewPageFlow(MyTmoData myTmoData, String planOrService) {
		String radiobuttonName = null;
		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		if (!planOrService.isEmpty() && planOrService.contains("Radio")) {
			radiobuttonName = manageAddOnsSelectionPage.clickOnNumberRadioButtonAndGetText();
		} else {
			manageAddOnsSelectionPage.clickOnFreeRadioOrCheckBoxButton("Service");
			manageAddOnsSelectionPage.clickOnConflictContinue();
		}
		// manageAddOnsSelectionPage.verifyWereConflictedContinueBtn();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		return radiobuttonName;
	}

	// ODF Review page flow
	protected ManageAddOnsSelectionPage navigateToODFReviewPageFlowByCheckBoxname(MyTmoData myTmoData,
			String checkBoxName) {
		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnCheckBoxByName(checkBoxName);
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		return manageAddOnsSelectionPage;
	}

	// ODF Review page flow
	protected ManageAddOnsSelectionPage navigateToODFReviewPageFlowBySelectingDataPassDataPlanAndService(
			MyTmoData myTmoData, String checkBoxName) {

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		manageAddOnsSelectionPage.clickOnCheckBoxByName(checkBoxName);
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		return manageAddOnsSelectionPage;
	}

	// ODF Review page flow
	protected ManageAddOnsSelectionPage navigateToODFReviewPageWithServiceAndDataPlan(MyTmoData myTmoData) {

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		verifyManageDataAndAddOnsPage();

		manageAddOnsSelectionPage.clickOnNumberRadioButton();
		// manageAddOnsSelectionPage.clickOnFreeRadioOrCheckBoxButton("Service");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		return manageAddOnsSelectionPage;
	}

	// ODF Data pass
	protected void navigateToODFUpGradeYourLinePageFlow(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		navigateToODFURlWithMsisdn(myTmoData, "home");
		verifyManageDataAndAddOnsPage();
	}

	/**
	 * Navigate To Plan Comparison Page
	 */
	protected boolean navigateToPlanComaprisionPage(MyTmoData myTmoData) {
		boolean isBestTalkAndTextPlan = true;
		navigateToPlanComparisionPage(myTmoData);
		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
		plansComparisonPage.checkPageIsReady();
		/*
		 * if (!(getDriver() instanceof AppiumDriver)) {
		 * verifyPlansComparisonPageIsDisplayingBestTalkAndTextPlan(myTmoData); return
		 * plansComparisonPage.verifyPlansComparisonBestTalkAndTextPlan(); }
		 */
		return isBestTalkAndTextPlan;
	}

	/**
	 * Navigate To Plan Comparision Page
	 * 
	 * @param myTmoData
	 */
	protected FeaturedPlanPage navigateToPlanComparisionPage(MyTmoData myTmoData) {
		navigateToPlanFeaturesPage(myTmoData);
		FeaturedPlanPage featuredPlanPage = new FeaturedPlanPage(getDriver());
		// featuredPlanPage.verifyFeaturedPlanPage();
		featuredPlanPage.verifyFeaturedPlanPage();
		featuredPlanPage.clickPlanFeatureTaxInclusive();
		return featuredPlanPage;
	}

	protected PlansComparisonPage navigatePlanPageToPlanConfigurationPage(MyTmoData myTmoData) {

		navigateToPlanComparisionPage(myTmoData);

		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
		plansComparisonPage.clickSelectandContinueButton();
		verifyPlanConfigurePageTitle();
		return plansComparisonPage;
	}

	/**
	 * Navigate to Plan Features Page
	 * 
	 * @param myTmoData
	 */
	protected PlanDetailsPage navigateToPlanFeaturesPage(MyTmoData myTmoData) {
		navigateToPlanDetailsPage(myTmoData);
		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.clickOnChangePlanButton();
		return plandetailspage;
	}

	/**
	 * Navigate to Plans List Page
	 * 
	 * @param myTmoData
	 * @return
	 */

	public PlansListPage navigateToPlanListPage(MyTmoData myTmoData) {
		navigateToPlansComparisionPage(myTmoData);
		PlansListPage plansListPage = new PlansListPage(getDriver());
		plansListPage.verifyPlansListPage();
		return plansListPage;

	}

	// Login to plan Confirmation page
	/*
	 * public AccountsCommonLib navigateToPlanReviewPage(MyTmoData myTmoData) {
	 * navigateToPlanListPage(myTmoData); ChangePlanPage changePlan = new
	 * ChangePlanPage(getDriver()); changePlan.clickTmobileOneWearable();
	 * changePlan.clickNextbutton(); changePlan.clickContinuebtn();
	 * changePlan.verifyReviewChangespage(); return this; }
	 */

	protected PlansComparisonPage navigateToPlansComparisionPage(MyTmoData myTmoData) {

		navigateToAccountoverViewPageFlow(myTmoData);
		// NewHomePage homePage = new NewHomePage(getDriver());
		// homePage.clickChangeMyPlanQuickLink();

		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		if (accountOverviewPage.verifyPlanName()) {
			accountOverviewPage.clickOnPlanName();
		} else {
			accountOverviewPage.clickOnLineName();

			LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
			linedetailspage.verifyLineDetailsPage();
			linedetailspage.clickOnPlanName();
		}
		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
		FeaturedPlanPage featuredPlanPage = new FeaturedPlanPage(getDriver());
		featuredPlanPage.clickPlanFeatureTaxInclusive();
		return plansComparisonPage;
	}

	protected PlansComparisonPage navigateToPlansComparisionPageDirectly(MyTmoData myTmoData) {

		navigateToAccountoverViewPageFlow(myTmoData);
		// NewHomePage homePage = new NewHomePage(getDriver());
		// homePage.clickChangeMyPlanQuickLink();

		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		if (accountOverviewPage.verifyPlanName()) {
			accountOverviewPage.clickOnPlanName();
		} else {
			accountOverviewPage.clickOnLineName();

			LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
			linedetailspage.verifyLineDetailsPage();
			linedetailspage.clickOnPlanName();
		}
		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
		FeaturedPlanPage featuredPlanPage = new FeaturedPlanPage(getDriver());
		featuredPlanPage.clickPlanFeatureTaxInclusive();
		return plansComparisonPage;
	}

	protected AccountsCommonLib navigateToServicesAllODFReviewPageFlow(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		navigateToODFURlWithMsisdn(myTmoData, "home");
		verifyManageDataAndAddOnsPage();
		return this;
	}

	// Data Pass List page continue
	protected ManageAddOnsSelectionPage oDFdataPassListPageSelectDataPassAndContinue() {
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		verifyManageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnDataPass();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyManageAddOnsPage();
		return manageAddOnsSelectionPage;
	}

	public AccountsCommonLib oDFDataPassReviewPageCancelVerification(MyTmoData myTmoData) {

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.checkPageIsReady();
		manageAddOnsSelectionPage.clickCancelButton();

		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		accountOverviewPage.verifyAccountOverviewPage();
		return this;
	}

	// Verify list of Legal links
	public ManageAddOnsReviewPage verifyListOfLegalLinks() {
		ManageAddOnsReviewPage manageAddOnsReviewPage = new ManageAddOnsReviewPage(getDriver());

		manageAddOnsReviewPage.clickOnServiceAgreementLink();
		manageAddOnsReviewPage.verifyLegalHeaderTextServiceAgreement("Service Agreement");
		manageAddOnsReviewPage.clickonCloseServiceAgreement();

		manageAddOnsReviewPage.clickOnTermsAndConditionsLink();
		manageAddOnsReviewPage.verifyLegalHeaderTextTermsAndCond("Terms & Conditions");
		manageAddOnsReviewPage.clickonCloseTermsAndCond();

		manageAddOnsReviewPage.clickOnElectronicSignatureLink();
		manageAddOnsReviewPage.verifyLegalHeaderTextElectronicSign("Electronic signature terms");
		manageAddOnsReviewPage.closeElectronicSign();
		return manageAddOnsReviewPage;
	}

	// Data Pass Upgrade Line Page - Sprint work
	public AccountsCommonLib verifyManageAddOnsPage() {
		try {
			ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
			manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		} catch (Exception e) {
			Assert.fail("Manage Add Ons Selection Page is not displayed ");
		}
		return this;
	}

	// verify My Promotions page
	public PromotionsPage verifyMyPromotionsPage() {
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.verifyMyPromotionsPage("My promotions");
		return promotionsPage;
	}

	// ODF Data Pass Review Page
	public AccountsCommonLib verifyODFDataPassReviewPage() {
		try {
			ManageAddOnsReviewPage manageAddOnsReviewPage = new ManageAddOnsReviewPage(getDriver());
			manageAddOnsReviewPage.waitforSpinner();
			manageAddOnsReviewPage.checkPageIsReady();
			Verify.assertTrue(manageAddOnsReviewPage.verifyReviewText(), Constants.DATAPASS_REVIEW);
		} catch (Exception e) {
			Assert.fail("ODF data pass review page is not displayed");
		}
		return this;

	}

	// ODF Data Pass Review & Pay Page
	public AccountsCommonLib verifyODFDataPassReviewAndPayPage() {
		try {
			CommonPage commonPage = new CommonPage(getDriver());
			commonPage.checkPageIsReady();
			ManageAddOnsReviewPage manageAddOnsReviewPage = new ManageAddOnsReviewPage(getDriver());
			manageAddOnsReviewPage.reviewPageWaitForSpinner();
			Verify.assertTrue(manageAddOnsReviewPage.verifyReviewAndPayText(), Constants.DATAPASS_REVIEW);
		} catch (Exception e) {
			Assert.fail("Data pass review & pay page is not displayed");
		}
		return this;

	}

	public PlansComparisonPage verifyPlansComparisonPageIsDisplayingBestTalkAndTextPlan(MyTmoData myTmoData) {
		waitForPageLoad(getDriver());
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.waitForImageSpinnerInvisibility();
		commonPage.checkPageIsReady();
		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
		if (plansComparisonPage.verifyPlansComparisonPageBestTalkAndTextPlan()) {
			commonPage.clickOnlogOutBtn();
			waitForPageLoad(getDriver());

			LoginPage loginPage = new LoginPage(getDriver());
			waitForPageLoad(getDriver());
			getDriver().manage().deleteAllCookies();
			loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(), myTmoData.getLoginPassword());

			HomePage homePage = new HomePage(getDriver());
			waitForPageLoad(getDriver());
			homePage.clickAccountLink();

			AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
			accountOverviewPage.verifyAccountOverviewPage();
			accountOverviewPage.clickOnPlanName();

			PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
			plandetailspage.verifyPlanDetailsPageLoad();
			plandetailspage.clickOnChangePlanButton();

			FeaturedPlanPage featuredPlanPage = new FeaturedPlanPage(getDriver());
			featuredPlanPage.clickPlanFeatureTaxInclusive();
		}
		return plansComparisonPage;
	}

	protected void verifyReviewAndConfirm() {
		waitForPageLoad(getDriver());
		Assert.assertTrue(getDriver().getTitle().contains("Review and Confirm"), Constants.ERROR_MANAGE_SERVICES_PAGE);
	}

	/**
	 * Navigate To Phone Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public PhonePages navigateToPhonePage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickPhoneLink();
		PhonePages phonePage = new PhonePages(getDriver());
		phonePage.verifyPhonesPage();
		return phonePage;
	}

	protected ODFDataPassReviewPage backNavigationOfUpsellURL() {
		ODFDataPassReviewPage reviewPageAddon = new ODFDataPassReviewPage(getDriver());
		reviewPageAddon.checkPageIsReady();
		reviewPageAddon.clickOnReviewPageBackButton();
		ManageAddOnsSelectionPage manageadon = new ManageAddOnsSelectionPage(getDriver());
		manageadon.verifymanageDataAndAddOnsPage();
		return reviewPageAddon;
	}

	protected ProfilePage checkForBlockInternationalRoamingStatus() {
		ProfilePage profilepage = new ProfilePage(getDriver());
		profilepage.verifyInternationalBlockingAndClickingOnIt();
		return profilepage;
	}

	public AccountsCommonLib verifyUnblockRoamingMessageForUpsell(MyTmoData myTmoData) {
		navigateToProfilePage(myTmoData);
		checkForBlockInternationalRoamingStatus();
		navigateToAddOnsUpsellURL("profile", "DataPass:INTL24NA");
		ManageAddOnsSelectionPage manageadon = new ManageAddOnsSelectionPage(getDriver());
		manageadon.verifymanageDataAndAddOnsPage();
		// reviewPageAddon.verifyUnblockRoamingMessage();
		// reviewPageAddon.clickContinueButton();
		manageadon.verifymanageDataAndAddOnsPage();

		return this;
	}

	public AccountsCommonLib verifyUnblockRoamingLinkRedirection(MyTmoData myTmoData) {
		navigateToProfilePage(myTmoData);
		checkForBlockInternationalRoamingStatus();
		navigateToAddOnsUpsellURL("profile", "DataPass:INTL24NA");
		ManageAddOnsSelectionPage manageadon = new ManageAddOnsSelectionPage(getDriver());
		manageadon.verifymanageDataAndAddOnsPage();
		ODFDataPassReviewPage reviewPageAddon = new ODFDataPassReviewPage(getDriver());
		reviewPageAddon.verifyUnblockRoamingMessage();
		reviewPageAddon.verifyUnblockRoamingMsg();
		reviewPageAddon.clickUnblockRoaming();
		ProfilePage profilepage = new ProfilePage(getDriver());
		profilepage.verifyBlockingPage();
		return this;
	}

	// Login to odf Review & Pay
	public AccountsCommonLib navigateToReviewAndPay(MyTmoData myTmoData) {
		launchAndPerformLogin(myTmoData);
		verifyODFDataPassReviewAndPayPage();
		return this;
	}

	protected ODFDataPassReviewPage verifyCaretExpandedOfUpsellURL() {
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.caretExpandedForDeepLink();
		return oDFDataPassReviewPage;
	}

	// Navigate to Plans Breakdown page
	public AccountsCommonLib navigateToPlansReviewPage(MyTmoData myTmoData) {
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		String currentURL[] = getDriver().getCurrentUrl().split("home");
		currentURL[0] = currentURL[0] + "account-overview";
		getDriver().get(currentURL[0]);
		AccountOverviewPage accountOverview = new AccountOverviewPage(getDriver());
		accountOverview.verifyAccountOverviewPage();
		accountOverview.clickOnChangePlanCTAOnAccountOverviewPage();
		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();
		return this;
	}

	// Navigate to Plans Breakdown page
	public AccountsCommonLib navigateToPlansBreakdownPage(MyTmoData myTmoData) {
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		String currentURL[] = getDriver().getCurrentUrl().split("home");
		currentURL[0] = currentURL[0] + "account-overview";
		getDriver().get(currentURL[0]);
		AccountOverviewPage accountOverview = new AccountOverviewPage(getDriver());
		accountOverview.verifyAccountOverviewPage();
		accountOverview.clickOnChangePlanCTAOnAccountOverviewPage();
		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();
		changePlansReviewPage.clickOnPlansBlade();
		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.clickOnPlansBlade();
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();
		return this;
	}

	/*
	 * public String getEncryptedCardNumber(ApiTestData apiTestData) throws
	 * Exception { AccountsApi accountsApi = new AccountsApi();
	 * 
	 * String publicKey = accountsApi.getPublicKey(apiTestData); String
	 * encryptedCardNum = ""; try {
	 * 
	 * getDriver().get(
	 * "E:/New-Repositories/Sam-Repo-New/eservices-qa/mytmo-ui/src/test/resources/encrypt-generic.html"
	 * ); CommonPage commonPage = new CommonPage(getDriver());
	 * commonPage.checkPageIsReady();
	 * 
	 * enterPublicKey(publicKey); enterCardNumber(apiTestData.getCardNumber());
	 * clickEncrytedBtn(); encryptedCardNum = getEncryptedCardNumber();
	 * 
	 * 
	 * } catch (NoSuchElementException e) { Assert.fail(
	 * "Accessory PLP Page is not displayed"); } return encryptedCardNum;
	 * 
	 * }
	 * 
	 */

	/*
	 * public AccountsCommonLib enterPublicKey(String publicey) { try {
	 * 
	 * publicKey.isDisplayed(); publicKey.sendKeys(publicey); Reporter.log(
	 * "publicKey is displayed"); } catch (Exception e) { Assert.fail(
	 * "publicKey is not displayed"); } return this; }
	 * 
	 * 
	 * 
	 * public AccountsCommonLib enterCardNumber(String CardNumber) { try {
	 * 
	 * cardNumberTextArea.sendKeys(CardNumber); Reporter.log(
	 * "cardNumberTextArea is displayed"); } catch (Exception e) { Assert.fail(
	 * "cardNumberTextArea is not displayed"); } return this; }
	 * 
	 * public AccountsCommonLib clickEncrytedBtn() { try {
	 * 
	 * encrytedBtn.click(); Reporter.log("encrytedBtn is displayed"); } catch
	 * (Exception e) { Assert.fail("encrytedBtn is not displayed"); } return this; }
	 * 
	 * public String getEncryptedCardNumber() { String encryptedCardNum=""; try {
	 * 
	 * encryptedCardNum= encrytedcardNumber.getText(); Reporter.log(
	 * "cardNumberTextArea is displayed"); } catch (Exception e) { Assert.fail(
	 * "cardNumberTextArea is not displayed"); } return encryptedCardNum; }
	 */

	public LineDetailsPage navigateToLineDetailsPage(MyTmoData myTmoData) {
		navigateToAccountoverViewPageFlow(myTmoData);
		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());

		if (!accountoverviewpage.verifyCurrentUrlIsLineDetails()) {
			accountoverviewpage.clickOnLineName();
		}
		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		return linedetailspage.verifyLineDetailsPage();
	}

	public AccountsCommonLib removeNetflixService(String netFlixSocName) {
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		manageAddOnsSelectionPage.unCheckCheckBox(netFlixSocName);
		manageAddOnsSelectionPage.verifyWarningMsgRemovingNetFlixisDisplayed(
				"By removing Netflix Std. $10.99 w/Fam Allowances ($16 value) you'll lose Netflix on us, unless you add Netflix Prem. $13.99 w/Fam Allowances ($19 value) for $3.00/mo. This change will result in immediate removal of T-mobile as your method of payment, with potential changes to any previous method of payment on file with Netflix");
		manageAddOnsSelectionPage.verifyContinueRemovalBtnisDisplayed();
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();

		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		verifyODFDataPassReviewPage();
		oDFDataPassReviewPage.agreeAndSubmitButton();

		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();

		navigateToAnyURL("com",
				"com/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");

		return this;
	}

	public PlanDetailsPage navigateToPlanDetailsPage(MyTmoData myTmoData) {

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();

		homePage.clickAccountLink();
		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		accountOverviewPage.verifyAccountOverviewPage();

		if (accountOverviewPage.verifyPlanName()) {
			accountOverviewPage.clickOnPlanName();
		} else {
			accountOverviewPage.clickOnLineName();

			LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
			linedetailspage.verifyLineDetailsPage();
			linedetailspage.clickOnPlanName();
		}
		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		return plandetailspage;
	}

	public double navigateToPlanDetailsPageAndGetMonthlyTotalCostFromAccountOverviewPage(MyTmoData myTmoData) {

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();

		homePage.clickAccountLink();
		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		accountOverviewPage.verifyAccountOverviewPage();

		double monthlyTotalCostOnAccountOverviewPage = accountOverviewPage.verifyTotalCostOnAccountOverviewPage();

		if (accountOverviewPage.verifyPlanName()) {
			accountOverviewPage.clickOnPlanName();
		} else {
			accountOverviewPage.clickOnLineName();

			LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
			linedetailspage.verifyLineDetailsPage();
			linedetailspage.clickOnPlanName();
		}

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		return monthlyTotalCostOnAccountOverviewPage;
	}

	// ODF Common methods
	public ManageAddOnsSelectionPage navigateToManageDataAndAddOnsFlow(MyTmoData myTmoData) {
		navigateToPlanDetailsPage(myTmoData);
		PlanDetailsPage planDetailsPage = new PlanDetailsPage(getDriver());
		planDetailsPage.clickOnManageAddOnsButton();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		verifyManageDataAndAddOnsPage();
		return manageAddOnsSelectionPage;
	}

	// ODF Common methods
	public ManageAddOnsSelectionPage navigateToManageAddOnsFlowFromHomeLink(MyTmoData myTmoData) {
		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		verifyManageDataAndAddOnsPage();
		return manageAddOnsSelectionPage;
	}

	public CrazyLegsBenefitsPage navigateToPlanBenefitsPageFlow(MyTmoData myTmoData) {
		navigateToPlanDetailsPage(myTmoData);
		PlanDetailsPage planDetailsPage = new PlanDetailsPage(getDriver());
		planDetailsPage.clickOnIncludedplanByName("Netflix™ On Us");
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.verifyBenifitsPage();
		return crazyLegsBenefitsPage;
	}

	// Login to Manage Add Ons Selection Page
	public ManageAddOnsSelectionPage navigateToManageAddOnsSelectionPage(MyTmoData myTmoData) {
		launchAndPerformLogin(myTmoData);
		NewHomePage homePage = new NewHomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickOnManageAddOnsLink();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		verifyManageAddOnsPage();
		return manageAddOnsSelectionPage;
	}

	// Login to Manage Add Ons Selection Page
	public CostDetailsPage navigateToCostDetailsPage(MyTmoData myTmoData) {
		AccountOverviewPage accountoverviewpage = navigateToAccountoverViewPageFlow(myTmoData);
		accountoverviewpage.clickOnMonthlyTotalDetails();

		CostDetailsPage costdetailspage = new CostDetailsPage(getDriver());
		costdetailspage.verifyCostDetailsPage();
		return costdetailspage;
	}

	public String selectPlanOnPlansComparisonPage(MyTmoData myTmoData, String planName) {

		navigateToHomePage(myTmoData);
		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());

		navigateToFutureURLFromHome(myTmoData, "change-plan/explore-plans");

		exploreplanpage.checkPageIsReady();
		String currentPlanName = exploreplanpage.getCurrentPlanName();

		exploreplanpage.verifyExplorePage();
		exploreplanpage.navigateToPlanComparisionPage(planName);

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.checkPageIsReady();
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.choosePlanFromChoosePlanSection(currentPlanName);
		newplancomparisionpage.checkPageIsReady();
		newplancomparisionpage.clickOnSelectPlanCta();
		return currentPlanName;
	}

	public String getCurrentPlanNameFromAccountOverviewPage() {
		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		String planName = plandetailspage.getCurrentPlanName();
		System.out.println("Current Plan is " + planName);
		return planName;
	}

	/**
	 * This method is used to get the URL from property file and perform the login
	 * to mytmo
	 * 
	 * @param myTmoData
	 */

	public AccountOverviewPage navigateToAccountoverViewPageFlow(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);

		homePage.clickAccountLink();
		int i = 0;
		try {
			while (getDriver().getTitle().contains("Home") && i < 4) {
				homePage.clickAccountLink();
				i++;
			}
		} catch (Exception e) {
			Assert.fail("Account Link is not displayed ");
		}
		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
		return accountoverviewpage;
	}

	public LineDetailsPage navigateToLineDetailsPageDirectly(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickAccountLink();
		int i = 0;
		try {
			while (getDriver().getTitle().contains("Home") && i < 4) {
				homePage.clickAccountLink();
				i++;
			}
		} catch (Exception e) {

		}
		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
		return linedetailspage;
	}

	protected ODFDataPassReviewPage navigateToUpsellURLOnReviewPage(MyTmoData myTmoData) {
		getDriver().get("https://dev7.eservice.t-mobile.com/odf/isUpsell:true/DataPass:OND1DAYT");
		Reporter.log("loaded Plan page Deep link URL(https://my.t-mobile.com/plans/services-list.html)");
		// performLogin(myTmoData, loginPage);
		Reporter.log("User logged in  successfully");
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyReviewAndPAyPage();
		return oDFDataPassReviewPage;
	}

	protected ODFDataPassReviewPage redirectToUpsellURLOnReviewPage(MyTmoData myTmoData, String url) {
		navigateToHomePage(myTmoData);
		navigateToAddOnsUpsellURL("home", url);
		ODFDataPassReviewPage reviewPageAddon = new ODFDataPassReviewPage(getDriver());
		reviewPageAddon.verifyReviewPageOfManageDataAndAddOnsPage();
		return reviewPageAddon;
	}

	protected ManageAddOnsSelectionPage verifyDeepLinkingwithNetflixServices(MyTmoData myTmoData) {
		getDriver().get("https://my.t-mobile.com/plans/services-list.html");
		Reporter.log("loaded Plan page Deep link URL(https://my.t-mobile.com/plans/services-list.html)");
		// SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
		// performLogin(myTmoData, loginPage);
		Reporter.log("User logged in  successfully");
		ManageAddOnsSelectionPage manageadon = new ManageAddOnsSelectionPage(getDriver());
		return manageadon;
	}

	protected ManageAddOnsSelectionPage verifyDeepLinkingManagAddOnsPage(MyTmoData myTmoData) {
		getDriver().get("https://my.t-mobile.com/plans/services-list.html");
		Reporter.log("loaded Plan page Deep link URL(https://my.t-mobile.com/plans/services-list.html)");
		// SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
		LoginPage loginPage = new LoginPage(getDriver());
		performLogin(myTmoData, loginPage);
		Reporter.log("User logged in  successfully");
		ManageAddOnsSelectionPage manageadon = new ManageAddOnsSelectionPage(getDriver());
		return manageadon;
	}

	// Data Pass Upgrade Line Page - Sprint work
	public CommonLibrary verifyManageDataAndAddOnsPage() {
		try {
			ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
			manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		} catch (Exception e) {
			Assert.fail("Manage Data and Add-ons page is not displayed ");
		}
		return this;
	}

	public AccountsCommonLib getManageAddonsNetflixUrl() {
		String[] currentURL = null;
		if (getDriver().getCurrentUrl().contains("home")) {
			currentURL = getDriver().getCurrentUrl().split("home");

		} else if (getDriver().getCurrentUrl().contains("addons-confirmation")) {
			currentURL = getDriver().getCurrentUrl().split("addons-confirmation");

		} else if (getDriver().getCurrentUrl().contains("plan-benefits")) {
			currentURL = getDriver().getCurrentUrl().split("plan-benefits");
		}

		currentURL[0] = currentURL[0] + "odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP";
		getDriver().get(currentURL[0]);

		verifyManageDataAndAddOnsPage();
		return this;
	}

	public boolean submitChangeToReviewPageAndThenConfirmationPage(String name) {
		boolean selectionStatus = false;
		try {
			ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
			manageAddOnsSelectionPage.clickOnContinueBtn();
			ManageAddOnsReviewPage manageAddOnsReviewPage = new ManageAddOnsReviewPage(getDriver());
			manageAddOnsReviewPage.reviewPageWaitForSpinner();
			manageAddOnsReviewPage.clickOnAgreeandSubmitButton();
			ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
			newODFConfirmationPage.verifyODFConfirmationPage();
		} catch (Exception e) {
			Assert.fail("" + name + " is Not displayed");
		}
		return selectionStatus;
	}

	public boolean checkWhetherServiceIsSelectedAndThenUncheckThatServiceAndSubmitChanges(String name) {
		boolean selectionStatus = false;
		try {
			ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
			selectionStatus = manageAddOnsSelectionPage.verifyServiceCheckBoxByNameIsSelectedOrUnselected(name);
			if (selectionStatus == true) {
				manageAddOnsSelectionPage.unCheckCheckBox(name);
				submitChangeToReviewPageAndThenConfirmationPage(name);
				ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(
						getDriver());
				manageAddOnsConfirmationPage.checkAndClickOnReturnToHomeCTA();

				AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
				accountoverviewpage.verifyAccountOverviewPage();
				// navigateToODFPageFromAccountsOVerviewPage();
				manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
				Reporter.log("Checkbox unchecked");
			} else
				Reporter.log("Checkbox already unchecked");
		} catch (Exception e) {
			Assert.fail("" + name + " is Not displayed");
		}
		return selectionStatus;
	}

	public boolean checkExistingCopyAndNewCopyOfNetflixSocOnConfirmationPage(String name) {
		boolean selectionStatus = true;
		try {
			ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
			selectionStatus = manageAddOnsSelectionPage.verifyServiceCheckBoxByNameIsSelectedOrUnselected(name);
			if (selectionStatus == false) {
				ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
				submitChangeToReviewPageAndThenConfirmationPage(name);
				newODFConfirmationPage.verifyExistingSignUpNextFlixBtnIsDisplayedOrNot();
				newODFConfirmationPage.verifyExistingNetFilxEligibleMessageIsDisplayedOrNot();
				newODFConfirmationPage.verifyNextStepsComponentOnConfirmationPage();
			} else if (selectionStatus == true) {
				Assert.fail("" + name
						+ " soc is selected even after tried to unselect that. Unable to proceed for transaction");
			}
		} catch (Exception e) {
			Assert.fail("" + name + " is Not displayed");
		}
		return selectionStatus;
	}

	public EmployeeVerificationPage navigateToEmployeeVerificationPage(MyTmoData myTmoData) {
		ProfilePage profile = navigateToProfilePage(myTmoData);
		profile.clickEmployeeVerification();
		EmployeeVerificationPage employeeVerificationPage = new EmployeeVerificationPage(getDriver());
		employeeVerificationPage.verifyPageLoaded();
		return employeeVerificationPage;
	}

	public EmploymentUploadPage navigateToEmploymentUploadPage(MyTmoData myTmoData) {
		navigateToEmployeeVerificationPage(myTmoData);
		EmploymentUploadPage employmentUploadPage = new EmploymentUploadPage(getDriver());
		employmentUploadPage.verifyPageLoaded();
		return employmentUploadPage;
	}

	/**
	 * Navigate to privacy and notifications page
	 *
	 * @param myTmoData
	 * @return
	 */
	public PrivacyAndNotificationsPage navigateToPrivacyAndNotificationsPage(MyTmoData myTmoData, String linkName) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink(linkName);

		PrivacyAndNotificationsPage privacyAndNotificationsPage = new PrivacyAndNotificationsPage(getDriver());
		privacyAndNotificationsPage.verifyPrivacyAndNotificationsPage();
		return privacyAndNotificationsPage;
	}

	/**
	 * Navigate to privacy and Advertising page
	 *
	 * @param myTmoData
	 * @return
	 */
	public AdvertisingAndInsightsPage navigateToAdvertisingPage(MyTmoData myTmoData, String linkName) {
		PrivacyAndNotificationsPage privacyAndNotificationsPage = navigateToPrivacyAndNotificationsPage(myTmoData,
				"Privacy and Notifications");
		privacyAndNotificationsPage.verifyPrivacyAndNotificationsPage();
		privacyAndNotificationsPage.clickAdvertisingLink();
		AdvertisingAndInsightsPage advertisingAndInsightsPage = new AdvertisingAndInsightsPage(getDriver());
		advertisingAndInsightsPage.verifyAdvertisingandInsightsPage();
		return advertisingAndInsightsPage;
	}
	
}