package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.PaymentMicroserviceApiV1;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class PaymentApiTest extends PaymentMicroserviceApiV1 {
     
	public Map<String, String> tokenMap;
	/**
	 * US546900# Description:EOS updateCart API integration with DCP v3 endpoints- Upgrade Flow-EIP
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"PaymentsAPIv3", Group.SHOP, Group.SPRINT })
	public void testEosEncryptionPaymentCreditUS546900(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Update Cart for EIP");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Test create cart with EIP terms|cartId should be generated and Response should be 200 OK");
		Reporter.log("Step 1: Update Cart should update specific items to cart request for specific cart id");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK and cart should be updated successfully.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName = "testEosEncryptionPaymentCredit";

		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		String cardNumberEncrypted = getEncryptedCardNumber(apiTestData, tokenMap);
		tokenMap.put("version", "v1");
		//tokenMap.put("banId", apiTestData.getSku());
		//tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");
		operationName = "check validation";
		if(cardNumberEncrypted!=null) {
			tokenMap.put("cardNumber", cardNumberEncrypted);
			String requestBody = new ServiceTest().getRequestFromFile("validateCreditCardPaymentMethodV3.txt");
			String updatedRequest = prepareRequestParam(requestBody, tokenMap);
			logRequest(updatedRequest, operationName);
			
			Response response = paymentValidation(apiTestData, updatedRequest, tokenMap);
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {
					Assert.assertEquals(getPathVal(jsonNode, "status"), "VALID", "Card Validation Failed");
				}
			} else {
				failAndLogResponse(response, operationName);
			}
		}else {
			System.out.println("Issue while generating encrypted creditcard number.");
		}
	}
	
}
