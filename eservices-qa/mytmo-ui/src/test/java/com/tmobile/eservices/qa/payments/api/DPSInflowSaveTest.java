package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.JWTTokenApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.DPSInflowSave;

import io.restassured.response.Response;

public class DPSInflowSaveTest extends DPSInflowSave {

	public Map<String, String> tokenMap;

	/**
	 * US531143: OTP Non Sedona--Start using DPS inflow save
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "DPSInflowSaveTest", "testDpsInflowSaveNonSedona",
			Group.PAYMENTS, Group.SPRINT })
	public void testDpsInflowSaveNonSedona(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testDpsInflowSaveNonSedona");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN: Non Sedona.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with DPSInflowSaveTest.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testDpsInflowSaveNonSedona";

		tokenMap = new HashMap<String, String>();
		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap.put("version", "v1");
		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		String requestBody = new ServiceTest().getRequestFromFile("DpsInflowSaveNonSedona.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("chargeAmount", String.valueOf(Math.round(generateRandomAmount() * 100.0) / 100.0));
		tokenMap.put("token", jwtTokens.get("jwtToken"));
		
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = dpsInflowSaveNonSedona(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "statusCode"), "A", "Status code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode, "statusMessage"), "Approved  Successfully ",
						"Status message is mismatched");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * US531149: Autopay --Start using DPS inflow save
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "DPSInflowSaveTest", "testDpsInflowSaveAutopay",
			Group.PAYMENTS, Group.SPRINT })
	public void testDpsInflowSaveAutopay(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testDpsInflowSaveAutopay");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN: Non Sedona.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with DPSInflowSaveTest.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName = "testDpsInflowSaveAutopay";
		tokenMap = new HashMap<String, String>();
		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap.put("version", "v1");
		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		String requestBody = new ServiceTest().getRequestFromFile("DpsInflowSaveAutopay.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("token", jwtTokens.get("jwtToken"));
		
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = dpsInflowSaveAutopay(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "Status.statusCode"), "COMPLETED", "Status code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode, "StatusBody.reasonDescription"), "Transaction completed successfully",
						"Status Body is mismatched");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * US531147: PA --Start using DPS inflow save
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "DPSInflowSaveTest", "testDpsInflowSavePA",
			Group.PAYMENTS, Group.SPRINT })
	public void testDpsInflowSavePA(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testDpsInflowSaveAutopay");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN: PA eligible.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with DPSInflowSaveTest.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName = "testDpsInflowSavePA";
		tokenMap = new HashMap<String, String>();
		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap.put("version", "v1");
		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		String requestBody = new ServiceTest().getRequestFromFile("DpsInflowSavePA.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("token", jwtTokens.get("jwtToken"));
		
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = dpsInflowSavePA(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "Status.statusCode"), "COMPLETED", "Status code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode, "StatusBody.reasonDescription"), "Transaction completed successfully",
						"Status Body is mismatched");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US570942: EIP- Start using DPS inflow save EOS
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "DPSInflowSaveTest", "testDpsInflowSaveEIP",
			Group.PAYMENTS, Group.SPRINT })
	public void testDpsInflowSaveEIP(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testDpsInflowSaveEIP");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN: EIP eligible.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with DPSInflowSaveTest.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName = "testDpsInflowSaveEIP";
		tokenMap = new HashMap<String, String>();
		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap.put("version", "v1");
		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);
		
		String requestBody = new ServiceTest().getRequestFromFile("DpsInflowSaveEIP.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("imei", apiTestData.getiMEINumber());
		tokenMap.put("token", jwtTokens.get("jwtToken"));
		tokenMap.put("chargeAmount", String.valueOf(Math.round(generateRandomAmount() * 100.0) / 100.0));		
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = dpsInflowSaveEIP(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "statusCode"), "A", "Status code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode, "statusMessage"), "Approved  Successfully approved",
						"Status Body is mismatched");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
}