package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EOSPaymentsV4;

import io.restassured.response.Response;

public class EOSPaymentsV4Test extends EOSPaymentsV4 {

	public Map<String, String> tokenMap;

	/**
	 * US604999 OTP migration to EOS payments V4
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "testEosProcessEipPaymentV4" })
	public void testEosProcessEipPaymentV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testEosProcessEipPaymentV4");
		Reporter.log("Data Conditions:MyTmo registered EIP misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Processes EIP payment of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName = "testEosProcessEipPaymentV4";
		tokenMap = new HashMap<String, String>();
		String requestBody = new ServiceTest().getRequestFromFile("EOSProcessEipPaymentV4.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("tmoid", apiTestData.gettmoId());
		tokenMap.put("imei", apiTestData.getiMEINumber());
		tokenMap.put("chargeAmount", generateRandomAmount().toString());
		tokenMap.put("isSave", "true");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = verifyProcessEIPPayment(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "statusCode"), "A", "Status code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode, "statusMessage"), "Approved  Successfully approved",
						"Status message is mismatched");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US604999 OTP migration to EOS payments V4
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "testEosProcessNonSedonaOTPV4" })
	public void testEosProcessNonSedonaOTPV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testEosProcessNonSedonaOTPV4");
		Reporter.log("Data Conditions:MyTmo registered Non Sedona misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Processes the Non sedona payment of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName = "testEosProcessNonSedonaOTPV4";
		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("EOSProcessNonSedonaOTPV4.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("tmoid", apiTestData.gettmoId());
		tokenMap.put("chargeAmount", generateRandomAmount().toString());
		tokenMap.put("isSave", "true");
		
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = processNonSedonaOTP(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "payment.statusCode"), "A", "Status code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode, "payment.statusMessage"), "Approved  Successfully",
						"Status message is mismatched");
				Assert.assertEquals(getPathVal(jsonNode, "payment.isSave"), "true", "isSave value is mismatched");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US604999 OTP migration to EOS payments V4
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEosProcessPaymentPostV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testEosProcessPaymentPostV4");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Post Process payment of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName = "testEosProcessPaymentPostV4";
		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("EosProcessPaymentPostV4.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("tmoid", apiTestData.gettmoId());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = processPaymentPost(apiTestData, updatedRequest, tokenMap);

		checkexpectedvalues(response, "statusCode", "100");
		checkexpectedvalues(response, "statusMessage", "Success");
	}

	/**
	 * US604999 OTP migration to EOS payments V4
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEosProcessSedonaOTPV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testEosProcessSedonaOTPV4");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Processes the sedona payment of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName = "testEosProcessSedonaOTPV4";
		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("EosProcessSedonaOTPV4.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("tmoid", apiTestData.gettmoId());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = processSedonaOTP(apiTestData, tokenMap);

		checkexpectedvalues(response, "statusCode", "100");
		checkexpectedvalues(response, "statusMessage", "Success");
	}

}