/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.ChangePlanPlansBreakdownPage;
import com.tmobile.eservices.qa.pages.accounts.ChangePlansReviewPage;

/**
 * @author prokarma US515750 - Plans detail breakdown page - Multi-line pooled:
 *         Plan name
 */

public class ChangePlanPlansBreakdownPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(ChangePlanPlansBreakdownPageTest.class);

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyHeaderAndPlanNameOnPlansBreakDownPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyHeaderAndPlanNameOnPlansBreakDownPage method called in ChangePlanPlansBreakdownPageTest");
		Reporter.log("Test Case : Verify Plans Header and Subheader on Plans Breakdown page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Plans page | Plans Page Should be displayed");
		Reporter.log("5. Verify Featured Plans page | Featured Plans Page Should be displayed");
		Reporter.log("6. Verify Plans Comparison page | Plans Comparison Page Should be displayed");
		Reporter.log("7. Verify Plans Configure Page |Plans Configure Page Should be displayed");
		Reporter.log("8. Click on Plans blade. | Plans Breadkdown Should be displayed");
		Reporter.log("9. Check Header. | Header should be Plan Change details.");
		Reporter.log("10. Check below subtitle. | Subtitle should be Here's how [Plan Name] applies to your account.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.clickOnPlansBlade();

		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();
		changePlanPlansBreakdownPage.checkHeaderOfPlansBreakDownPage();
		changePlanPlansBreakdownPage.checkSubHeaderOfPlansBreakDownPage(planName);
	}

	// //**
	// *
	// * US516443 - Plans detail breakdown page - Multi-line pooled: Total
	// * monthly plan cost
	// US516579 - Plans detail breakdown page: 'Back to summary' CTA
	// *//
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyTotalMonthlyCostForPooledAccountOnPlansBreakdownWhenDataFlagIsFalse(ControlTestData data,
			MyTmoData myTmoData) throws InterruptedException {
		logger.info(
				"verifyTotalMonthlyCostForPooledAccountOnPlansBreakdownWhenDataFlagIsFalse method called in ChangePlanPlansBreakdownPageTest");
		Reporter.log("Test Case : Check Total Monthly cost on Plans Breakdown page");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Plans page | Plans Page Should be displayed");
		Reporter.log("5. Verify Featured Plans page | Featured Plans Page Should be displayed");
		Reporter.log("6. Verify Plans Comparison page | Plans Comparison Page Should be displayed");
		Reporter.log("7. Verify Plans Configure Page |Plans Configure Page Should be displayed");
		Reporter.log("8. Click on Plans blade. | Plans Breadkdown Should be displayed");
		Reporter.log("9. Check Total Monthly cost text. | Text Total Monthly cost should be displayed.");
		Reporter.log(
				"10. Check Total Monthly cost amount. | Amount should be sum of Account level cost + All Line level costs");
		Reporter.log("11. Check text Taxes and fees included. | Text Taxes and fees included should be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.clickOnPlansBlade();

		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();
		changePlanPlansBreakdownPage.verifyNavigationOfBackToSummaryCTA();
		changePlansReviewPage.clickOnPlansBlade();
		changePlanPlansBreakdownPage.checkHeaderOfPlansBreakDownPage();
		changePlanPlansBreakdownPage.checkSubHeaderOfPlansBreakDownPage(planName);

		changePlanPlansBreakdownPage.checkAndVerifyTotalMonthlyLevelCostWithAccountLevelAndLineLevel();
	}

	/**
	 * @author prokarma
	 *
	 *         US516502 - Plans detail breakdown page - Multi-line pooled: Removed
	 *         plan
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyRemovePlanSectionUnderAccountSection(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyRemovePlanSectionUnderAccountSection method called in ChangePlanPlansBreakdownPageTest");
		Reporter.log("Test Case : Check Total Monthly cost on Plans Breakdown page");
		Reporter.log("Test Data : Any MSDSIN PAH/Full on TE plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Plans page | Plans Page Should be displayed");
		Reporter.log("5. Verify Featured Plans page | Featured Plans Page Should be displayed");
		Reporter.log("6. Verify Plans Comparison page | Plans Comparison Page Should be displayed");
		Reporter.log("7. Verify Plans Configure Page |Plans Configure Page Should be displayed");
		Reporter.log("8. Click on Plans blade. | Plans Breadkdown Should be displayed");
		Reporter.log(
				"9. Check Remove plan change section under Account section. | Plan which is being removed should be displayed under that section.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		String currentRatePlan = "T-Mobile ONE";

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.clickOnPlansBlade();

		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();
		changePlanPlansBreakdownPage.verifyWarningIcon();
		changePlanPlansBreakdownPage.verifyWarningMessageForRemovedSection();
		changePlanPlansBreakdownPage.verifyCurrentPlanNameUnderRemovedSection(currentRatePlan);
	}

	/**
	 * @author prokarma
	 *
	 *         US516514 - Plans detail breakdown page - Multi-line pooled: Each line
	 *         name & MSISDN display
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void checkEachLineNameAndNumberOnPlansBreakDownPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("checkEachLineNameAndNumberOnPlansBreakDownPage method called in ChangePlanPlansBreakdownPageTest");
		Reporter.log("Test Case : Check Each line name and MSISDN on Plans breakdown page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Plans page | Plans Page Should be displayed");
		Reporter.log("5. Verify Featured Plans page | Featured Plans Page Should be displayed");
		Reporter.log("6. Verify Plans Comparison page | Plans Comparison Page Should be displayed");
		Reporter.log("7. Verify Plans Configure Page |Plans Configure Page Should be displayed");
		Reporter.log("8. Click on Plans blade. | Plans Breadkdown Should be displayed");
		Reporter.log("9. Check Line name afor each line. | It should be properly displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.clickOnPlansBlade();

		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();

		changePlanPlansBreakdownPage.checkHeaderOfPlansBreakDownPage();
		changePlanPlansBreakdownPage.checkSubHeaderOfPlansBreakDownPage(planName);
		changePlanPlansBreakdownPage.verifyTaxesAndFeesText(planName);
		changePlanPlansBreakdownPage.verifyPlanNameAndPriceUnderAccountLevelAndEachLineLevel();
		changePlanPlansBreakdownPage.checkAndVerifyTotalMonthlyLevelCostWithAccountLevelAndLineLevel();
		changePlanPlansBreakdownPage.verifyMultipleIncompatibleMessagesDuringHybridMigrationTEToTI();
	}

	/**
	 * @author prokarma
	 *
	 *         US529838 - Detailed review - Impacted and non-impacted lines
	 * @throws InterruptedException
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void checkAccountAndImpactedAndNonImpactedSectionWhenChangePlanHappensInSameTaxTreatment(
			ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		logger.info(
				"checkAccountAndImpactedAndNonImpactedSectionWhenChangePlanHappensInTITaxTreatment method called in ChangePlanPlansBreakdownPageTest");
		Reporter.log(
				"Test Case : Check Account,Impacted and Non impacted section when change plan happens in TI Tax treatment and GSM started change plan.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full on TI plan with atleast 1 MBB line");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA | Plans Review Page Should be displayed");
		Reporter.log("6. Click on Plans blade. | Plans Breadkdown Should be displayed");
		Reporter.log("7. Check Account section. | Account section should be displayed with shared plan.");
		Reporter.log("8. Check Impacted section. | Impacted section should be displayed.");
		Reporter.log("9. Check Non Impacted section. | Non Impacted section should be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.clickOnPlansBlade();

		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();

		changePlanPlansBreakdownPage.verifyAllDetailsDuringChangePlanInSameTaxTreatment(planName);
	}

	/**
	 * @author prokarma
	 *
	 *         US529838 - Detailed review - Impacted and non-impacted lines
	 * @throws InterruptedException
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void checkAccountAndImpactedAndNonImpactedSectionWhenChangePlanHappensInDifferentTaxTreatment(
			ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		logger.info(
				"checkAccountAndImpactedAndNonImpactedSectionWhenChangePlanHappensInDifferentTaxTreatment method called in ChangePlanPlansBreakdownPageTest");
		Reporter.log(
				"Test Case : Check Account,Impacted and Non impacted section when change plan happens from TE to TI Tax treatment and GSM started change plan.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full on TE plan with atleast 1 MBB line");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA | Plans Review Page Should be displayed");
		Reporter.log("6. Click on Plans blade. | Plans Breadkdown Should be displayed");
		Reporter.log("7. Check Account section. | Account section should be displayed with shared plan.");
		Reporter.log("8. Check Impacted section. | Impacted section should be displayed.");
		Reporter.log("9. Check Non Impacted section. | Non Impacted section should be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.clickOnPlansBlade();

		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();

		changePlanPlansBreakdownPage.verifyAllDetailsDuringHybridMigration(planName);
	}

	/**
	 * @author prokarma
	 *
	 *         US529838 - Detailed review - Impacted and non-impacted lines
	 * @throws InterruptedException
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void checkAccountAndImpactedAndNonImpactedSectionWhenChangePlanHappensForNonPooledAccount(
			ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		logger.info(
				"checkAccountAndImpactedAndNonImpactedSectionWhenChangePlanHappensForNonPooledAccount method called in ChangePlanPlansBreakdownPageTest");
		Reporter.log(
				"Test Case : Check Account,Impacted and Non impacted section when change plan happens for Non pooled account and 1 line started change plan");
		Reporter.log("Test Data : Any MSDSIN PAH/Full on non pooled plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA | Plans Review Page Should be displayed");
		Reporter.log("6. Click on Plans blade. | Plans Breadkdown Should be displayed");
		Reporter.log("7. Check Account section. | Account section should be displayed with shared plan.");
		Reporter.log("8. Check Impacted section. | Impacted section should be displayed.");
		Reporter.log("9. Check Non Impacted section. | Non Impacted section should be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.clickOnPlansBlade();

		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();

		changePlanPlansBreakdownPage.verifyAllDetailsDuringChangePlanForNonPooledAccount(planName);
	}

	/**
	 * @author prokarma
	 *
	 *         US529838 - Detailed review - Impacted and non-impacted lines
	 * @throws InterruptedException
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void checkAccountAndImpactedAndNonImpactedSectionWhenSingleLineStartedChangePlan(ControlTestData data,
			MyTmoData myTmoData) throws InterruptedException {
		logger.info(
				"checkAccountAndImpactedAndNonImpactedSectionWhenSingleLineStartedChangePlan method called in ChangePlanPlansBreakdownPageTest");
		Reporter.log(
				"Test Case : Check Account,Impacted and Non impacted section when Single line started change plan.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full single line msisdn on TE/TI plan ");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA | Plans Review Page Should be displayed");
		Reporter.log("6. Click on Plans blade. | Plans Breadkdown Should be displayed");
		Reporter.log("7. Check Account section. | Account section should be displayed with shared plan.");
		Reporter.log("8. Check Impacted section. | Impacted section should be displayed.");
		Reporter.log("9. Check Non Impacted section. | Non Impacted section should be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.clickOnPlansBlade();

		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();

		changePlanPlansBreakdownPage.verifyAllDetailsDuringChangePlanForSingleLine(planName);
	}
}
