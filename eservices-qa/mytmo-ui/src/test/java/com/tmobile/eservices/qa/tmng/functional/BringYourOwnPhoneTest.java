package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.CartPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class BringYourOwnPhoneTest extends TmngCommonLib {

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TMNG })
	public void testBringYourOwnPhonePage(TMNGData tMNGData) {
		navigateToBringYourOwnPhonePage(tMNGData);
	}

	/**
	 * C347311: TMO- verify Bring Your Own Phone/ MI device - Phone
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyBringYourOwnPhoneCTAForPhone(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : TMO- verify Bring Your Own Phone/ MI device - Phone");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on BYOD |BYOD page should be displayed");
		Reporter.log(
				"4. Click on Sim Starter Kit for phone| PDP  of T-Mobile 3-in-1 SIM Starter Kit should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.clickOnBYODLinkOnCartEssentials();

		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifySimKitPdpPageLoaded();

		/*
		 * BringYourOwnPhonePage bringYourOwnPhonePage = new
		 * BringYourOwnPhonePage(getDriver()); bringYourOwnPhonePage.verifyPageLoaded();
		 */
	}

	/**
	 * US436776 TMNG > Product First > BYOD PDP > Create AEM headers for SIM Starter
	 * Kit Modal US521335 TMNG > Product First > BYOD PDP > UX - Display Drop Down
	 * header With Options
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyBYODPDPHeadersFromMainPLP(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US436776	TMNG > Product First > BYOD  PDP > Create AEM headers for SIM Starter Kit Modal");
		Reporter.log(
				"Test Case : US521335	TMNG > Product First > BYOD  PDP > UX - Display Drop Down header With Options");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click Add to Cart CTA | Cart Page should be Displayed");
		Reporter.log("5. Click BYOD CTA on cart | SIM Statrter KIT PDP should be displayed.");

		// Reporter.log("10. Verify dropdown header for Wearables |Header 'SIM
		// Card types' should be diplayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnBYODLinkOnCartEssentials();
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifySimKitPdpPageLoaded();
		/*
		 * BringYourOwnPhonePage byodPage = new BringYourOwnPhonePage(getDriver());
		 * byodPage.verifyPageLoaded();
		 */

	}
}
