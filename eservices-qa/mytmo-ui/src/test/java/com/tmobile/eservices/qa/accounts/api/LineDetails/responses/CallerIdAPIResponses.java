package com.tmobile.eservices.qa.accounts.api.LineDetails.responses;

import java.util.HashMap;
import java.util.Map;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class CallerIdAPIResponses extends AccountsApi {

	public Map<String, String> tokenMap;

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testCallerIDNameFromFirstOrianAPI(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Test Caller Id name from first Orian API ");
		Reporter.log("Test data : Enter valid phone number");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testCallerIDNameFromFirstOrianAPI";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("getCallerID.txt");
		System.out.println(tokenMap);
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getCallerIdV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits = new AccountsApi();
			String alltags[] = { "name" };
			for (String tag : alltags) {
				benefits.checkjsontagitems(response, tag);
			}

			benefits.checkexpectedvalues(response, "name", "true");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void verifyUpdateCallerIdApiResponseForEmptyFirstName(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Test Caller Id name from first Orian API ");
		Reporter.log("Test data : Enter valid phone number");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testCallerIDNameFromFirstOrianAPI";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("getCallerID.txt");
		System.out.println(tokenMap);
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getCallerIdV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits = new AccountsApi();
			String alltags[] = { "name" };
			for (String tag : alltags) {
				benefits.checkjsontagitems(response, tag);
			}

			benefits.checkexpectedvalues(response, "name", "true");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void verifyUpdateCallerIdApiResponse(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Test Caller Id name from first Orian API ");
		Reporter.log("Test data : Enter valid phone number");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testCallerIDNameFromFirstOrianAPI";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("getCallerID.txt");
		System.out.println(tokenMap);
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getCallerIdV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits = new AccountsApi();
			String alltags[] = { "name" };
			for (String tag : alltags) {
				benefits.checkjsontagitems(response, tag);
			}

			benefits.checkexpectedvalues(response, "name", "true");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void verifyUpdateCallerIdApiResponseForVulgarWord(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Test Caller Id name from first Orian API ");
		Reporter.log("Test data : Enter valid phone number");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testCallerIDNameFromFirstOrianAPI";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("getCallerID.txt");
		System.out.println(tokenMap);
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getCallerIdV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits = new AccountsApi();
			String alltags[] = { "name" };
			for (String tag : alltags) {
				benefits.checkjsontagitems(response, tag);
			}

			benefits.checkexpectedvalues(response, "name", "true");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

}
