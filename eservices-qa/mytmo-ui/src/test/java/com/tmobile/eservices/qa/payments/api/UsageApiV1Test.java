package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.UsageApiV1 ;

import io.restassured.response.Response;

public class UsageApiV1Test extends UsageApiV1{
	public Map<String, String> tokenMap;
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "UsageApiV1","testgetUsageSummay",Group.PAYMENTS,Group.APIREG  })
	public void testgetUsageSummay(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getUsageSummay test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with usage details.");
		Reporter.log("Step 2: Verify usage details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="getUsageSummay";
		
		tokenMap = new HashMap<String, String>();
		//String requestBody="{\"customerInfo\": [{\"msisdn\": \"4252365726\",\"name\": \"Test\"}]}";
		String requestBody="{\"customerInfo\": [{\"msisdn\": \""+apiTestData.getMsisdn()+"\",\"name\": \"Test\"}]}";
	
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getUsageSummay(apiTestData, updatedRequest, tokenMap);
	
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode= getPathVal(jsonNode, "statusCode");
				String statusMessage= getPathVal(jsonNode, "statusMessage");
				Assert.assertNotEquals(statusCode,"","statusCode is Null or Empty.");
				Assert.assertNotEquals(statusMessage,"","statusMessage is Null or Empty.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "UsageApiV1","testgetUsageSummay_datastash",Group.PAYMENTS,Group.APIREG  })
	public void testgetUsageSummay_datastash(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testgetUsageSummay_datastash test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with usage data stash details.");
		Reporter.log("Step 2: Verify usage data stash details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="getUsageSummay_datastash";
		tokenMap = new HashMap<String, String>();
		//String requestBody="{\"customerInfo\": [{\"msisdn\": \"4252365726\",\"name\": \"Test\"}]}";
		String requestBody="{\"customerInfo\": [{\"msisdn\": \""+apiTestData.getMsisdn()+"\",\"name\": \"Test\"}]}";
	
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getUsageSummay_datastash(apiTestData, updatedRequest);
		//Response response =getUsageSummay_datastash(apiTestData, "{\"customerInfo\": [{\"msisdn\": \"4252365726\",\"name\": \"Test\"}]}");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
			
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "UsageApiV1","testGetUsageDetails_unbilled",Group.PAYMENTS,Group.APIREG  })
	public void testGetUsageDetails_unbilled(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getUsageDetails unbilled test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with usage unbilled details.");
		Reporter.log("Step 2: Verify usage unbilled details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetUsageDetails_unbilled";
		Response response =getUsageDetails(apiTestData, "0","");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "UsageApiV1","testGetUsageDetails_billed",Group.PAYMENTS,Group.APIREG  })
	public void testGetUsageDetails_billed(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getUsageDetails billed test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with usage billed details.");
		Reporter.log("Step 2: Verify usage billed details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetUsageDetails_billed";
		Response response =getUsageDetails(apiTestData, "8574467718","");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "UsageApiV1","testGetUsageDetails_BriteBill_Billed",Group.PAYMENTS,Group.APIREG  })
	public void testGetUsageDetails_BriteBill_Billed(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getUsageDetails britebill billed test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with usage britebill billed details.");
		Reporter.log("Step 2: Verify usage britebill billed returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetUsageDetails_BriteBill_Billed";
		Response response =getUsageDetails(apiTestData, "","69940240-0963284601_2");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
}
