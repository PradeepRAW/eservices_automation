package com.tmobile.eservices.qa.tmng.npi;


import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.CartPage;
import com.tmobile.eservices.qa.pages.tmng.functional.CheckOutPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

import org.testng.Reporter;
import org.testng.annotations.Test;

public class NewProductIntroductionTest extends TmngCommonLib {
	
	/**
	 * Add multiple type of devices to cart
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.DESKTOP })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku3(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku6(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku9(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING})
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku12(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku15(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku18(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku21(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku24(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku27(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku30(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku33(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku36(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku39(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testTMNGFlowForAddMultipleTypeOfDevicesToCartsku42(TMNGData tMNGData) {
		tmngFlowForAddMultipleTypeOfDevicesToCart(tMNGData);
	}
	
	
	public void tmngFlowForAddMultipleTypeOfDevicesToCart(TMNGData tMNGData) {
		Reporter.log("Test Case :  Add multiple type of devices to cart");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones link | Cell Phones page should be displayed");
		Reporter.log("3. Select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart | Cart page should be displayed");
		
		Reporter.log("5. Click on 'Add a phone' from cart | Cell Phones page should be displayed");
		Reporter.log("6. Select any device | PDP page should be displayed");
		Reporter.log("7. Click on Add to Cart | Cart page should be displayed");
		
		Reporter.log("8. Click on 'Add a phone' from cart | Cell Phones page should be displayed");
		Reporter.log("9. Select any device | PDP page should be displayed");
		Reporter.log("10. Select 'Want to pay in full?' link | FRP price should be displayed");
		Reporter.log("11. Click on Add to Cart | Cart page should be displayed");
		
		Reporter.log("12. Click on 'Add a tablet' from cart | Tablets PLP page should be displayed");
		Reporter.log("13. Select any tablet device | PDP page should be displayed");
		Reporter.log("14. Click on Add to Cart | Cart page should be displayed");
		
		Reporter.log("15. Click on 'Add a wearable' from cart | Wearable PLP page should be displayed");
		Reporter.log("16. Select any wearable device | PDP page should be displayed");
		Reporter.log("17. Click on Add to Cart | Cart page should be displayed");
		
		Reporter.log("18. Click on 'Add an accessory' from cart | Accessory PLP page should be displayed");
		Reporter.log("19. Select any accessory device | PDP page should be displayed");
		Reporter.log("20. Click on Add to Cart | Cart page should be displayed");
		
		Reporter.log("21. Click 'See More' link on Add ons Extras section for First line  | Enter Your ZIP Code model should be displayed");
		Reporter.log("22. Enter valid ZIP Code and click on Next button | Select services should be displayed");
		Reporter.log("23. Select any Recommended services and Click on Update button | Select services should be displayed in cartPage");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
//		Device #1 EIP
		PlpPage plpPage = navigateToPhonesPlpPage(tMNGData);
		plpPage.clickDevice(tMNGData.getDeviceName());
//		plpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.selectMemoryOption(tMNGData.getDeviceMemoryOption());
		phonesPdpPage.selectColorOption(tMNGData.getDeviceColorOption());
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
//		Adding Protection SOC
		cartPage.clickProtectionInAddOnsAndExtras();
		cartPage.enterYourZIPCode(tMNGData.getZipcode());
		cartPage.clickOnNextBtn();
		
//		Device #2 EIP
		cartPage.clickOnAddAPhoneLinkOnCart();
		plpPage.verifyPhonesPlpPageLoaded();
		plpPage.clickDevice(tMNGData.getSecondDeviceName());
//		plpPage.clickDeviceWithAvailability(tMNGData.getSecondDeviceName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.selectMemoryOption(tMNGData.getSecondDeviceMemoryOption());
		phonesPdpPage.selectColorOption(tMNGData.getSecondDeviceColorOption());
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		
//		Device #3 FRP
		cartPage.clickOnAddAPhoneLinkOnCart();
		plpPage.verifyPhonesPlpPageLoaded();
//		plpPage.clickDevice(tMNGData.getThirdDeviceName());		
		plpPage.clickDeviceWithAvailability(tMNGData.getThirdDeviceName());
		pdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.selectMemoryOption(tMNGData.getThirdDeviceMemoryOption());
		phonesPdpPage.selectColorOption(tMNGData.getThirdDeviceColorOption());
		pdpPage.clickPayInFullPaymentOption();
		pdpPage.verifyFRPPriceAfterClickingOnPayInFull();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		
//		Tablet EIP
		cartPage.clickOnAddATabletLinkOnCart();
		plpPage.verifyTabletsAndDevicesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		pdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		phonesPdpPage.selectMemoryOption(tMNGData.getTabletMemoryOption());
		phonesPdpPage.selectColorOption(tMNGData.getTabletColorOption());
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		
//		Watch EIP
		/*cartPage.clickOnAddAWearableLinkOnCart();
		plpPage.verifyWatchesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getWatchName());
		pdpPage.verifyWatchesPdpPageLoaded();
		pdpPage.selectMemoryOption(tMNGData.getWatchMemoryOption());
		pdpPage.selectColorOption(tMNGData.getWatchColorOption());
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();*/
		
//		Accessory
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		plpPage.verifyAccessoriesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		pdpPage.verifyAccessoriesPdpPageLoaded();
		pdpPage.selectColorOption(tMNGData.getAccessoryColorOption());
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		
		Double todayTotalAtCartPage = Double.parseDouble(cartPage.getTotalTodayPriceInStickyBanner());
		Double monthlyTotalAtCartPage = Double.parseDouble(cartPage.getTotalMonthlyPriceStickyBanner());
		
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		
		Double todayTotalAtCreditCheckModal = Double.parseDouble(checkOutPage.getTodayTotalPricingAtCreditCheckModal());
		Double monthlyTotalAtCreditCheckModal= Double.parseDouble(checkOutPage.getMonthlyTotalPricingAtCreditCheckModal());
		
		checkOutPage.compareTwoDoubleValuesNotEqual(todayTotalAtCartPage, todayTotalAtCreditCheckModal);
		checkOutPage.compareTwoDoubleValuesNotEqual(monthlyTotalAtCartPage, monthlyTotalAtCreditCheckModal);
		checkOutPage.clickCheckOutNowCTAOrderModal();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.fillPaymentInfo(tMNGData);
		checkOutPage.setSecurityPin(tMNGData);
		checkOutPage.clickAgreeandNextBtn();
		checkOutPage.verifyReviewandSubmitPage();
		Double todayTotalAtCheckOutReviewPage = checkOutPage.getTotalTodayAmount();
		Double monthlyTotalAtCheckOutReviewPage= checkOutPage.getMonthlyPriceAtCheckOutReviewPage();
		
		checkOutPage.compareTwoPrices(todayTotalAtCreditCheckModal, todayTotalAtCheckOutReviewPage);
		checkOutPage.compareTwoPrices(monthlyTotalAtCreditCheckModal, monthlyTotalAtCheckOutReviewPage);
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());
		
	}
	
	
	
	
	/**
	 * Validate device family name and color variants in PLP Page
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.DESKTOP })
	public void testDeviceFamilyNameAndColorVariantsInPlpPage(TMNGData tMNGData) {
		Reporter.log("Test Case : Validate device family name and color variants in PLP Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones link | Cell Phones page should be displayed");
		Reporter.log("3. Verify Device family name | Device family name should be displayed");
		Reporter.log("4. Verify Device color variants | Device color variants should be displayed");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToPhonesPlpPage(tMNGData);
		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyDevicesFamilyName(tMNGData.getDeviceName());
		plpPage.verifyColorSwatchForGivenFamilyName(tMNGData.getDeviceName(), tMNGData.getDeviceColorCount());
		
		plpPage.verifyDevicesFamilyName(tMNGData.getSecondDeviceName());
		plpPage.verifyColorSwatchForGivenFamilyName(tMNGData.getSecondDeviceName(), tMNGData.getSecondDeviceColorCount());		
//		
//		plpPage.verifyDevicesFamilyName(tMNGData.getThirdDeviceName());
//		plpPage.verifyColorSwatchForGivenFamilyName(tMNGData.getThirdDeviceName(), tMNGData.getThirdDeviceColorCount());
	}
	
	/**
	 * Validate device family name and color variants in PLP Page
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.DESKTOP })
	public void testDeviceColorAndMemoryOptionsWithSKUInPdpPage(TMNGData tMNGData) {
		Reporter.log("Test Case : Validate device family name and color variants in PLP Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | Device pdp page should be displayed");
		Reporter.log("4. Verify Device color and memory options with SKU | Device color and memory should be displayed based on SKU");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PlpPage phonesPLP = navigateToPhonesPlpPage(tMNGData);
		phonesPLP.clickDevice(tMNGData.getDeviceName());
		
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();

		phonesPdpPage.verifyDeviceSKUwithColorandMemory(tMNGData.getDeviceSKU(),tMNGData.getDeviceColorOption(),tMNGData.getDeviceMemoryOption());	
		
		phonesPdpPage.clickPhonesLinkInPDP();
		
		phonesPLP.verifyPhonesPlpPageLoaded();		
		phonesPLP.clickDevice(tMNGData.getSecondDeviceName());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.verifyDeviceSKUwithColorandMemory(tMNGData.getSecondDeviceSKU(),tMNGData.getSecondDeviceColorOption(),tMNGData.getSecondDeviceMemoryOption());	

//		pdpPage.clickPhonesLinkInPDP();
//		phonesPLP.verifyPhonesPlpPageLoaded();		
//		phonesPLP.clickDevice(tMNGData.getThirdDeviceName());
//		pdpPage.verifyPhonePdpPageLoaded();
//		pdpPage.verifyDeviceSKUwithColorandMemory(tMNGData.getThirdDeviceSKU(),tMNGData.getThirdDeviceColorOption(),tMNGData.getThirdDeviceMemoryOption());	

	}
	
	/**
	 * Validate device family name and color variants in PLP Page
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testDeviceFamilyNameAndColorVariantsInWatchesPlpPage(TMNGData tMNGData) {
		Reporter.log("Test Case : Validate device family name and color variants in PLP Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones link | Cell Phones page should be displayed");
		Reporter.log("3. Verify Device family name | Device family name should be displayed");
		Reporter.log("4. Verify Device color variants | Device color variants should be displayed");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PlpPage plpPage = navigateToWatchesPLP(tMNGData);
		plpPage.verifyDevicesFamilyName(tMNGData.getDeviceName());
		plpPage.verifyColorSwatchForGivenFamilyName(tMNGData.getDeviceName(), tMNGData.getDeviceColorCount());
		
		plpPage.verifyDevicesFamilyName(tMNGData.getSecondDeviceName());
		plpPage.verifyColorSwatchForGivenFamilyName(tMNGData.getSecondDeviceName(), tMNGData.getSecondDeviceColorCount());		
	}
	
	/**
	 * Validate device family name and color variants in PLP Page
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testDeviceColorAndMemoryOptionsWithSKUInWatchesPdpPage(TMNGData tMNGData) {
		Reporter.log("Test Case : Validate device family name and color variants in PLP Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | Device pdp page should be displayed");
		Reporter.log("4. Verify Device color and memory options with SKU | Device color and memory should be displayed based on SKU");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PlpPage plpPage = navigateToWatchesPLP(tMNGData);
		plpPage.clickDevice(tMNGData.getDeviceName());
		
		PdpPage wearablePdpPage = new PdpPage(getDriver());
		wearablePdpPage.verifyWatchesPdpPageLoaded();
		
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyDeviceSKUwithColorandMemory(tMNGData.getDeviceSKU(),tMNGData.getDeviceColorOption(),tMNGData.getDeviceMemoryOption());	
		
		pdpPage.clickWatchesLinkInPDP();
		plpPage.verifyWatchesPlpPageLoaded();		
		plpPage.clickDevice(tMNGData.getSecondDeviceName());
		pdpPage.verifyWatchesPdpPageLoaded();
		pdpPage.verifyDeviceSKUwithColorandMemory(tMNGData.getSecondDeviceSKU(),tMNGData.getSecondDeviceColorOption(),tMNGData.getSecondDeviceMemoryOption());	

	}
	
	
	
}
