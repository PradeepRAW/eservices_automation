/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.PrepaidLineSettingsPage;
import com.tmobile.eservices.qa.pages.accounts.PrepaidProfileLandingPage;
import com.tmobile.eservices.qa.pages.accounts.PrepaidTMobileIDPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class PrepaidLineSettingPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(PrepaidLineSettingPageTest.class);

	/*
	 * CDCAM-862 [Prepaid Profile Page .NET Migration] Profile Page_ Line Settings
	 * TAB
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkLineSettingsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkLineSettingsPage");
		Reporter.log("Test Case : Check Line Settings page url");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Line Settings tab | Line Settings page should be displayed");
		Reporter.log("6. Verify URL. | It should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);

		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.directURLForPrepaidProfilePage();
		prepaidProfilePage.verifyPrePaidProfilePage();
		prepaidProfilePage.clickOnLineSettingsTab();

		PrepaidLineSettingsPage prepaidLineSettingsPage = new PrepaidLineSettingsPage(getDriver());
		prepaidLineSettingsPage.verifyLineSettingsPage();
	}

}