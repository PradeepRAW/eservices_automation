/**
 * 
 */
package com.tmobile.eservices.qa.accounts.api.cpslookup;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class GetEligiblePlansTest extends AccountsApi {

	@BeforeMethod(alwaysRun = true)
	public void refreshTokenMap() {
		tokenMapCommon = null;
	}

	// Pooled line military plan --getEligiblePlans
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT })
	public void testPooledLineMilitaryPlanGetEligiblePlans(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: Pooled line military plan -- getEligiblePlans");
		Reporter.log("Data Conditions:Pooled line military plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3:  4256259035/Auto12345 ban -962381132");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		Reporter.log("requestbody is");
		Reporter.log(requestBody);
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Reporter.log("Updated request is is");
		Reporter.log(updatedRequest);
		Reporter.log("tokenMap is");
		Reporter.log(tokenMap.toString());

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);
		Reporter.log("SttausLine");
		Reporter.log(response.getStatusLine());
		Reporter.log("headers");
		Reporter.log(response.headers().toString());
		Reporter.log("body1");
		Reporter.log(response.getBody().asString());
		Reporter.log("body2");
		Reporter.log(response.getBody().prettyPrint());
		Reporter.log("body3");
		Reporter.log(response.getBody().print());
		Reporter.log("body4");
		Reporter.log(response.getBody().toString());

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

				JsonNode jsonNode = getParentNodeFromResponse(response);

				if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
					JsonNode parentNode = jsonNode.path("ratePlanList");
					if (!parentNode.isMissingNode() && parentNode.isArray()) {
						for (int i = 0; i < parentNode.size(); i++) {

							if (parentNode.get(i).get("planSoc").textValue().trim().contains("TM1TI2")) {

								JsonNode featureNode = parentNode.get(i).get("features");
								for (int j = 0; j < featureNode.size(); j++) {

									if (!featureNode.isMissingNode() && featureNode.isArray()) {

										String labelNode = featureNode.get(j).get("label").textValue();

										Assert.assertTrue(getRatePlanFeatureTM1TI2().contains(labelNode),
												"" + labelNode + " soc Features lables are not displaying correctly");
									}
								}
							} else if (parentNode.get(i).get("planSoc").textValue().trim().contains("TMESNTL2")) {

								JsonNode featureNode = parentNode.get(i).get("features");
								for (int j = 0; j < featureNode.size(); j++) {

									if (!featureNode.isMissingNode() && featureNode.isArray()) {

										String labelNode = featureNode.get(j).get("label").textValue();

										Assert.assertTrue(getRatePlanFeatureTMESNTL2().contains(labelNode),
												"" + labelNode + " soc Features lables are not displaying correctly");
									}
								}
							} else if (parentNode.get(i).get("planSoc").textValue().trim().contains("TMSERVE2")) {

								JsonNode featureNode = parentNode.get(i).get("features");
								for (int j = 0; j < featureNode.size(); j++) {

									if (!featureNode.isMissingNode() && featureNode.isArray()) {

										String labelNode = featureNode.get(j).get("label").textValue();

										Assert.assertTrue(getRatePlanFeatureTMSERVE2().contains(labelNode),
												"" + labelNode + " soc Features lables are not displaying correctly");
									}
								}
							}
						}
					}
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	// testGetEligiblePlansforTMOONEPLUSWithNoCreditCheck
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT })
	public void testGetEligiblePlansforTMOONEPLUSWithNoCreditCheck(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: Tmo one with one plus no credit check -- getEligiblePlans");
		Reporter.log("Data Conditions: pooled tmo one with one plus no credit check plan - NC1PLSALL");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3:  4042471849/Auto12345 ban - 913637457");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> planSoclist = new ArrayList<String>();
			planSoclist.add("NCCONE2");
			planSoclist.add("NCESNTL2");
			planSoclist.add("NC1PLSALL");

			ArrayList<String> namelist = new ArrayList<String>();
			namelist.add("T-Mobile ONE with ONE Plus No Credit Check");
			namelist.add("T-Mobile ONE No Credit Check");
			namelist.add("T-Mobile Essentials No Credit Check");

			if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
				JsonNode parentNode = jsonNode.path("ratePlanList");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {

						if (planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim())) {

							Assert.assertTrue(planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()),
									"Plan Soc list is not displaying");
							Assert.assertTrue(namelist.contains(parentNode.get(i).get("name").textValue().trim()),
									"Name list is not displaying");

							Reporter.log("Expected value :" + namelist);
							Reporter.log("Actual value:" + parentNode.get(i).get("name").textValue());
							Reporter.log("Expected value:" + planSoclist);
							Reporter.log("Actual value:" + parentNode.get(i).get("planSoc").textValue());
						}
					}
				}
			}

		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT })
	public void testGetEligiblePlansforSimpleChoiceNorthAmericaNoCreditCheck(ControlTestData data,
			ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: TestGetEligiblePlansforSimpleChoiceNorthAmericaNoCreditCheck -- getEligiblePlans");
		Reporter.log("Data Conditions:  - NCNAUTTF");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3:  4252403603/Auto12345 ban - 959083804");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> planSoclist = new ArrayList<String>();
			planSoclist.add("NCNAUTTF");
			planSoclist.add("NC1PLSALL");
			planSoclist.add("NCCONE2");
			planSoclist.add("NCESNTL2");

			ArrayList<String> namelist = new ArrayList<String>();
			namelist.add("Simple Choice North America");
			namelist.add("T-Mobile ONE with ONE Plus No Credit Check");
			namelist.add("T-Mobile ONE No Credit Check");
			namelist.add("T-Mobile Essentials No Credit Check");

			if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
				JsonNode parentNode = jsonNode.path("ratePlanList");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {
						if (planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim())) {

							Assert.assertTrue(planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()),
									"Plan Soc list is not displaying");
							Assert.assertTrue(namelist.contains(parentNode.get(i).get("name").textValue().trim()),
									"Name list is not displaying");

							Reporter.log("Expected value :" + namelist);
							Reporter.log("Actual value:" + parentNode.get(i).get("name").textValue());
							Reporter.log("Expected value:" + planSoclist);
							Reporter.log("Actual value:" + parentNode.get(i).get("planSoc").textValue());
						}
					}
				}
			}

		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT })
	public void testGetEligiblePlansforSimpleChoiceNorthAmerica(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: TestGetEligiblePlansforSimpleChoiceNorthAmerica -- getEligiblePlans");
		Reporter.log("Data Conditions: NAUTTF");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3:  3253201040/Auto12345 ban - 962814745");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> planSoclist = new ArrayList<String>();
			planSoclist.add("NAUTTF");
			planSoclist.add("1PLSALL");
			planSoclist.add("TM1TI2");
			planSoclist.add("ONE55TI2");
			planSoclist.add("TMSERVE2");
			planSoclist.add("1PLS552");
			planSoclist.add("TMESNTL2");

			ArrayList<String> namelist = new ArrayList<String>();

			namelist.add("T-Mobile ONE Unlimited 55+ with ONE Plus");
			namelist.add("T-Mobile ONE with ONE Plus");
			namelist.add("Simple Choice North America");
			namelist.add("T-Mobile ONE Unlimited 55");
			namelist.add("T-Mobile ONE");
			namelist.add("T-Mobile Essentials");
			namelist.add("T-Mobile ONE Military");

			if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
				JsonNode parentNode = jsonNode.path("ratePlanList");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {

						if (planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim())) {

							Assert.assertTrue(planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()),
									" " + parentNode.get(i).textValue() + "Plan Soc list is not displaying");
							Assert.assertTrue(namelist.contains(parentNode.get(i).get("name").textValue().trim()),
									" " + parentNode.get(i).textValue() + "  Name list is not displaying");

							Reporter.log("Expected value :" + namelist);
							Reporter.log("Actual value:" + parentNode.get(i).get("name").textValue());
							Reporter.log("Expected value:" + planSoclist);
							Reporter.log("Actual value:" + parentNode.get(i).get("planSoc").textValue());
						}
					}
				}
			}

		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT })
	public void testGetEligiblePlansforTMobileONE(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: TestGetEligiblePlansforTMobileONE -- getEligiblePlans");
		Reporter.log("Data Conditions: FRLTULFM2");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3:  4256916481/Auto12345 ban - 962382015");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> planSoclist = new ArrayList<String>();
			planSoclist.add("1PLS552");
			planSoclist.add("1PLSALL");
			planSoclist.add("FRLTULFM2");
			planSoclist.add("ONE55TI2");
			planSoclist.add("TM1TI2");
			planSoclist.add("TMESNTL2");
			planSoclist.add("TMSERVE2");

			ArrayList<String> namelist = new ArrayList<String>();

			namelist.add("T-Mobile ONE Unlimited 55+ with ONE Plus");
			namelist.add("T-Mobile ONE with ONE Plus");
			namelist.add("T-Mobile ONE Unlimited 55");
			namelist.add("T-Mobile ONE");
			namelist.add("T-Mobile Essentials");
			namelist.add("T-Mobile ONE Military");

			if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
				JsonNode parentNode = jsonNode.path("ratePlanList");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {

						if (planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim())) {

							Assert.assertTrue(planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()),
									" " + parentNode.get(i).textValue() + "Plan Soc list is not displaying");

							Assert.assertTrue(namelist.contains(parentNode.get(i).get("name").textValue().trim()),
									" " + parentNode.get(i).textValue() + "  Name list is not displaying");

							Reporter.log("Expected value :" + namelist);
							Reporter.log("Actual value:" + parentNode.get(i).get("name").textValue());
							Reporter.log("Expected value:" + planSoclist);
							Reporter.log("Actual value:" + parentNode.get(i).get("planSoc").textValue());
						}
					}
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT })
	public void testGetEligiblePlansforTMobileONEwithONEPlus(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: TestGetEligiblePlansforTMobileONEwithONEPlus -- getEligiblePlans");
		Reporter.log("Data Conditions: 1PLSALL");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3:  3236903741/Auto12345 ban - 962816414");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> planSoclist = new ArrayList<String>();
			planSoclist.add("1PLSALL");
			planSoclist.add("ONE55TI2");
			planSoclist.add("TMESNTL2");
			planSoclist.add("1PLS552");
			planSoclist.add("TMSERVE2");
			planSoclist.add("TM1TI2");

			ArrayList<String> namelist = new ArrayList<String>();

			namelist.add("T-Mobile ONE with ONE Plus");
			namelist.add("T-Mobile ONE Unlimited 55");
			namelist.add("T-Mobile Essentials");
			namelist.add("T-Mobile ONE Unlimited 55+ with ONE Plus");
			namelist.add("T-Mobile ONE Military");
			namelist.add("T-Mobile ONE");

			if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
				JsonNode parentNode = jsonNode.path("ratePlanList");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {

						if (planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim())) {

							Assert.assertTrue(planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()),
									" " + parentNode.get(i).textValue() + "Plan Soc list is not displaying");

							Assert.assertTrue(namelist.contains(parentNode.get(i).get("name").textValue().trim()),
									" " + parentNode.get(i).textValue() + "  Name list is not displaying");

							Reporter.log("Expected value :" + namelist);
							Reporter.log("Actual value:" + parentNode.get(i).get("name").textValue());
							Reporter.log("Expected value:" + planSoclist);
							Reporter.log("Actual value:" + parentNode.get(i).get("planSoc").textValue());
						}
					}
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT, "sprin" })
	public void testGetEligiblePlansforTMobileEssentials(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: TestGetEligiblePlansforTMobileEssentials -- getEligiblePlans");
		Reporter.log("Data Conditions: TMESNTL2");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3:  3609327641/Auto12345 ban - 962816122 ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> planSoclist = new ArrayList<String>();
			planSoclist.add("TMESNTL2");
			planSoclist.add("1PLSALL");
			planSoclist.add("TM1TI2");
			planSoclist.add("TMSERVE2");
			planSoclist.add("TMSERVE2");
			planSoclist.add("1PLS552");

			ArrayList<String> namelist = new ArrayList<String>();

			namelist.add("T-Mobile Essentials");
			namelist.add("T-Mobile ONE with ONE Plus");
			namelist.add("T-Mobile ONE");
			namelist.add("T-Mobile ONE Unlimited 55");
			namelist.add("T-Mobile ONE Military");
			namelist.add("T-Mobile ONE w/ ONE Plus Family Unlimited 55");

			if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
				JsonNode parentNode = jsonNode.path("ratePlanList");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {
						if (planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim())) {

							Reporter.log("in if condition loop ");

							Assert.assertTrue(planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()),
									" " + parentNode.get(i).textValue() + "Plan Soc list is not displaying");

							Assert.assertTrue(namelist.contains(parentNode.get(i).get("name").textValue().trim()),
									" " + parentNode.get(i).textValue() + "  Name list is not displaying");

							Reporter.log("Actual value:" + parentNode.get(i).get("planSoc").textValue());
						}
						Reporter.log("Expected value:" + planSoclist);
					}
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT, })
	public void testGetEligiblePlansforTMobileONEMilitary(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: TestGetEligiblePlansforTMobileONEMilitary -- getEligiblePlans");
		Reporter.log("Data Conditions: TMSERVE9");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: 9899091982/Auto12345 ban - 962860462 ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> planSoclist = new ArrayList<String>();
			planSoclist.add("TMSERVE9");
			planSoclist.add("1PLSALL");
			planSoclist.add("TM1TI2");

			ArrayList<String> namelist = new ArrayList<String>();

			namelist.add("T-Mobile ONE with ONE Plus");
			namelist.add("T-Mobile ONE");
			namelist.add("T-Mobile ONE Military");

			if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
				JsonNode parentNode = jsonNode.path("ratePlanList");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {
						if (planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim())) {

							Assert.assertTrue(planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()),
									" " + parentNode.get(i).textValue() + "Plan Soc list is not displaying");

							Assert.assertTrue(namelist.contains(parentNode.get(i).get("name").textValue().trim()),
									" " + parentNode.get(i).textValue() + "  Name list is not displaying");

							Reporter.log("Expected value :" + namelist);
							Reporter.log("Actual value:" + parentNode.get(i).get("name").textValue());
							Reporter.log("Expected value:" + planSoclist);
							Reporter.log("Actual value:" + parentNode.get(i).get("planSoc").textValue());
						}
					}
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT })
	public void testGetEligiblePlansforMobileInternet2GB(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: TestGetEligiblePlansforMobileInternet2GB -- getEligiblePlans");
		Reporter.log("Data Conditions: MITI2GB");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: 4254997731/Auto12345 ban -959091261 ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> planSoclist = new ArrayList<String>();
			planSoclist.add("TM1TBLT");
			planSoclist.add("MITI18GB");
			planSoclist.add("MITI14GB");
			planSoclist.add("MITI10GB");
			planSoclist.add("MITI6GB");
			planSoclist.add("MITI2GB");

			ArrayList<String> namelist = new ArrayList<String>();

			namelist.add("Mobile Internet 22GB");
			namelist.add("Mobile Internet 6GB");
			namelist.add("Mobile Internet 10GB");
			namelist.add("Mobile Internet 14GB");
			namelist.add("Mobile Internet 2GB");
			namelist.add("Mobile Internet 18GB");
			namelist.add("T-Mobile ONE Tablet");

			if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
				JsonNode parentNode = jsonNode.path("ratePlanList");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {

						if (planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()))

						{
							Assert.assertTrue(planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()),
									" " + parentNode.get(i).textValue() + "  Plan Soc list is not displaying");

							Assert.assertTrue(namelist.contains(parentNode.get(i).get("name").textValue().trim()),
									" " + parentNode.get(i).textValue() + "  Name list is not displaying");

							Reporter.log("Expected value :" + namelist);
							Reporter.log("Actual value:" + parentNode.get(i).get("name").textValue());
							Reporter.log("Expected value:" + planSoclist);
							Reporter.log("Actual value:" + parentNode.get(i).get("planSoc").textValue());
						}

					}
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT })
	public void testGetEligiblePlansforUnlimited55Plus(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: TestGetEligiblePlansforUnlimited55Plus -- getEligiblePlans");
		Reporter.log("Data Conditions: ONE55TI2");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: 4253657675/Auto12345 ban - 964602975");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			Reporter.log(" response:" + response.asString());
			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> planSoclist = new ArrayList<String>();
			planSoclist.add("MAG55TI2");
			planSoclist.add("MAGENTA2");
			planSoclist.add("ONE55TI2");
			planSoclist.add("MPLSMLFM");
			planSoclist.add("TMESNTL2");
			planSoclist.add("MGPLSFM");
			planSoclist.add("MGPLS552");
			planSoclist.add("MGMIL2");

			ArrayList<String> namelist = new ArrayList<String>();

			namelist.add("Magenta™ Unlimited 55");
			namelist.add("Magenta™");
			namelist.add("T-Mobile ONE Unlimited 55");
			namelist.add("Magenta® Plus Military");
			namelist.add("T-Mobile Essentials");
			namelist.add("Magenta™ Plus");
			namelist.add("Magenta® Plus Unlimited 55");
			namelist.add("Magenta™ Military");

			if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
				JsonNode parentNode = jsonNode.path("ratePlanList");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {
						Assert.assertTrue(planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()),
								" " + parentNode.get(i).textValue() + "Plan Soc list is not displaying");

						Assert.assertTrue(namelist.contains(parentNode.get(i).get("name").textValue().trim()),
								" " + parentNode.get(i).textValue() + "  Name list is not displaying");

						Reporter.log("Expected value :" + namelist);
						Reporter.log("Actual value:" + parentNode.get(i).get("name").textValue());
						Reporter.log("Expected value:" + planSoclist);
						Reporter.log("Actual value:" + parentNode.get(i).get("planSoc").textValue());
					}
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT })
	public void testGetEligiblePlansforLegacyPlan_TalkandText(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: TestGetEligiblePlansforLegacyPlan -- getEligiblePlans");
		Reporter.log("Data Conditions: FUTTC");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: 2026428079/Auto12345 ban - 955433990");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			Reporter.log("Response : " + response.body().asString());
			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> planSoclist = new ArrayList<String>();

			planSoclist.add("MAG55TI2");
			planSoclist.add("MAGENTA2");
			planSoclist.add("MPLSMLFM");
			planSoclist.add("TMESNTL2");
			planSoclist.add("FUTTC");
			planSoclist.add("MGPLSFM");
			planSoclist.add("MGPLS552");
			planSoclist.add("MGMIL2");

			if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
				JsonNode parentNode = jsonNode.path("ratePlanList");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {
						Assert.assertTrue(planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()),
								" " + parentNode.get(i).textValue() + "Plan Soc list is not displaying");

						Reporter.log("Expected value:" + planSoclist);
						Reporter.log("Actual value:" + parentNode.get(i).get("planSoc").textValue());
					}
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT })
	public void testGetEligiblePlansforIOTPlan_SyncUPDRIVE(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: TestGetEligiblePlansforIOTPlan_SyncUPDRIVE -- getEligiblePlans");
		Reporter.log("Data Conditions: NADR2GB");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: 3609324189/Auto12345 is pah 2nd line is IOT  2063765166, ban - 962815836");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TE");
		tokenMap.put("productType", "MBB");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			Reporter.log("Response : " + response.body().asString());
			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> planSoclist = new ArrayList<String>();

			planSoclist.add("DRTI14GB");
			planSoclist.add("DRTI18GB");
			planSoclist.add("NADR6GB");
			planSoclist.add("NADR14GB");
			planSoclist.add("NADR10GB");
			planSoclist.add("NADR2GB");
			planSoclist.add("DRTI22GB");
			planSoclist.add("NADR22GB");
			planSoclist.add("NADR18GB");
			planSoclist.add("DRTI6GB");
			planSoclist.add("DRTI2GB");
			planSoclist.add("DRTI10GB");

			ArrayList<String> namelist = new ArrayList<String>();

			namelist.add("SyncUP DRIVE 14GB");
			namelist.add("SyncUP DRIVE 18GB");
			namelist.add("SyncUP DRIVE North America 6GB");
			namelist.add("SyncUP DRIVE North America 14GB");
			namelist.add("SyncUP DRIVE North America 10GB");
			namelist.add("SyncUP DRIVE North America 2GB");
			namelist.add("SyncUP DRIVE 22GB");
			namelist.add("SyncUP DRIVE North America 22GB");
			namelist.add("SyncUP DRIVE North America 18GB");
			namelist.add("SyncUP Drive 6GB");
			namelist.add("SyncUP DRIVE 2GB");
			namelist.add("SyncUP DRIVE 10GB");
			Reporter.log("Expected value :" + namelist);
			Reporter.log("Expected value:" + planSoclist);
			if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
				JsonNode parentNode = jsonNode.path("ratePlanList");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {

						Reporter.log("Actual value:" + parentNode.get(i).get("name").textValue());
						Reporter.log("Actual value:" + parentNode.get(i).get("planSoc").textValue());

						// Reporter.log(parentNode.get(i).get("planSoc").textValue().trim());
						Assert.assertTrue(planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()),
								" " + parentNode.get(i).textValue() + "Plan Soc list is not displaying");
						// Reporter.log(parentNode.get(i).get("name").textValue().trim());
						Assert.assertTrue(namelist.contains(parentNode.get(i).get("name").textValue().trim()),
								" " + parentNode.get(i).textValue() + "  Name list is not displaying");
					}
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT, "ll" })
	public void testGetEligiblePlansfor_HomeInternetLine(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: TestGetEligiblePlansfor_HomeInternetLine -- getEligiblePlans");
		Reporter.log("Data Conditions: NADR2GB");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3:  4708196827/Auto12345 is pah 2nd line is Home Internet  4253658114, ban - 946268822");
		Reporter.log("Step4:  Product Type MBB");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TE");
		tokenMap.put("productType", "MBB");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			if (response.statusCode() == 200) {
				System.out.println(response.jsonPath().getList("ratePlanList.planSoc").size());
				Assert.assertTrue(response.jsonPath().getList("ratePlanList.planSoc").size() == 2);

			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.SPRINT, })
	public void testGetEligiblePlansfor_WearablePlan(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: TestGetEligiblePlansfor_WearablePlan -- getEligiblePlans");
		Reporter.log("Data Conditions: TM1WRTE");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: 4156297432/Auto12345 is pah 2nd line is wearable   (425) 373-6706, ban - 964805244");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TE");
		tokenMap.put("productType", "MBB");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			Reporter.log("Response : " + response.body().asString());
			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> planSoclist = new ArrayList<String>();

			planSoclist.add("NAWR500MB");
			planSoclist.add("TM1WRTE");
			planSoclist.add("TM1WR");

			ArrayList<String> namelist = new ArrayList<String>();

			namelist.add(
					"Simple Choice North America for Wearables with Separate Number: Unlimited Talk + Text + 500MB High-Speed Data");
			namelist.add("T-Mobile ONE Wearable TE with Separate Number");
			namelist.add("T-Mobile ONE Wearable with Separate Number");

			if (!jsonNode.isMissingNode() && jsonNode.has("ratePlanList")) {
				JsonNode parentNode = jsonNode.path("ratePlanList");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {
						Assert.assertTrue(planSoclist.contains(parentNode.get(i).get("planSoc").textValue().trim()),

								" " + parentNode.get(i).textValue() + "Plan Soc list is not displaying");

						Assert.assertTrue(namelist.contains(parentNode.get(i).get("name").textValue().trim()),
								" " + parentNode.get(i).textValue() + "  Name list is not displaying");

						Reporter.log("Expected value :" + namelist);
						Reporter.log("Actual value:" + parentNode.get(i).get("name").textValue());
						Reporter.log("Expected value:" + planSoclist);
						Reporter.log("Actual value:" + parentNode.get(i).get("planSoc").textValue());
					}
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = { Group.ACCOUNTS, Group.SPRINT })
	public void testGetEligiblePlansfor_DigitsLine(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGetEligiblePlansfor_DigitsLine -- getEligiblePlans");
		Reporter.log("Data Conditions: DIGITTT5");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step3: 4253898178/Auto12345 is pah 4th line is digits (425) 443-1421, ban - 966610177");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "MBB");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			Reporter.log("Response : " + response.body().asString());
			JsonNode jsonNode = getParentNodeFromResponse(response);

			ArrayList<String> namelist = new ArrayList<String>();

			namelist.add("$5 DIGITS T&T Promo");
			namelist.add("DIGITS Talk & Text");

			if (response.body().asString() != null && response.getStatusCode() == 200) {
				System.out.println("output Success");
				// response.body().toString()

				assertThat(response.jsonPath().getList("ratePlanList.planSoc"), hasItems("DIGITTT5"));
				assertThat(response.jsonPath().getList("ratePlanList.name"), hasItems("$5 DIGITS T&T Promo"));
				Reporter.log("size" + response.jsonPath().getList("ratePlanList.planSoc").size());
				Assert.assertTrue(response.jsonPath().getList("ratePlanList.planSoc").size() == 1);

				Reporter.log("");
				Reporter.log("");
				Reporter.log("Actual :" + response.jsonPath().getList("ratePlanList.planSoc"));
				Reporter.log("Expected :" + "DIGITTT5,DIGITSTT2");
				Reporter.log("Actual :" + response.jsonPath().getList("ratePlanList.name"));
				Reporter.log("Expected :" + "$5 DIGITS T&T Promo,DIGITS Talk & Text");

			} else {
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Response status code : " + response.getStatusCode());
				Reporter.log("Response : " + response.body().asString());
				logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
				Assert.fail("Failure Reason : " + response.body().asString());
			}
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = { Group.ACCOUNTS, Group.SPRINT, })
	public void testAutopayDetailsInGetEligiblePlansService(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testAutopayDetailsInGetEligiblePlansService -- getEligiblePlans");
		Reporter.log("Data Conditions: ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 3:   3253201040/Auto12345 ban - 962814745 ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			System.out.println("output Success");
			// response.body().toString()

			Reporter.log(response.jsonPath().getString("autopayDetails.isEligible"));

			Assert.assertTrue(response.jsonPath().getString("autopayDetails.isEligible").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("autopayDetails.isEnrolled").contains("false"));

		}

		else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = { Group.ACCOUNTS, Group.SPRINT, "kl" })
	public void testConflictandNonConflictDataPlanScenarioInRatePlanChange(ControlTestData data,
			ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testConflictDataPlanScenarioInRatePlanChange -- getEligiblePlans");
		Reporter.log("Data Conditions: FRLTULF2 Autopay on ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: this Account has ONe plus interantion data service which is not compatable for 1PLSALL");
		Reporter.log(
				"Step 3: verify response has FRTMO1PLS in conflict list  for 1PLSALL Plan and 1PLUS0 as Default soc");
		Reporter.log("Step 3: verify response no conflict SOC in TM2TI2 plan and FRTMO1PLS as Default soc");
		Reporter.log("Step 4:   6057281418/Auto12345 ban - 962383459 ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			System.out.println("output Success");
			int planSocSize = response.jsonPath().getList("ratePlanList.planSoc").size();

			for (int i = 0; i < planSocSize; i++) {

				if (response.jsonPath().get("ratePlanList[" + i + "].planSoc").toString().contains("1PLSALL")) {

					Assert.assertTrue(response.jsonPath().get("ratePlanList[" + i + "].conflictingSocs[0]").toString()
							.contains("FRLTDATA"), "conflictingSocs for 1PLSALL is incorrect ");
					Assert.assertTrue(response.jsonPath().get("ratePlanList[" + i + "].defaultDataServiceSoc")
							.toString().contains("1PLUS0"), "defaultDataServiceSoc for 1PLSALL is incorrect ");
					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].sum.listSum").toString().contains("150"),
							"listSum for 1PLSALL is incorrect ");
					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].sum.netSum").toString().contains("150"),
							"netSum for 1PLSALL is incorrect ");

				} else if (response.jsonPath().get("ratePlanList[" + i + "].planSoc").toString().contains("TM1TI2")) {
					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].conflictingSocs").toString().contains(""),
							"conflictingSocs for TMESNTL2 is incorrect ");
					Assert.assertTrue(response.jsonPath().get("ratePlanList[" + i + "].defaultDataServiceSoc")
							.toString().contains("FRLTDATA"), "defaultDataServiceSoc for TMESNTL2 is incorrect ");
				}
			}
		}

		else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = { Group.ACCOUNTS, Group.SPRINT, })
	public void testAutopayDetailsLineLevelInGetEligiblePlansService(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testAutopayDetailsInGetEligiblePlansService -- getEligiblePlans");
		Reporter.log("Data Conditions: ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 3:   8588323995/Auto12345 ban - 966828917 ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		System.out.println(updatedRequest.toString());

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			System.out.println(response.body().asString());
			// response.body().toString()

			Reporter.log(response.jsonPath().getString("autopayDetails.isEligible"));

			Assert.assertTrue(response.jsonPath().getString("autopayDetails.isEligible").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("autopayDetails.isEnrolled").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("ratePlanList.sum.listSum").contains("75"));
			Assert.assertTrue(response.jsonPath().getString("ratePlanList.sum.netSum").contains("70"));
			Assert.assertTrue(response.jsonPath().getString("ratePlanList.sum.discountSum").contains("5"));
			Assert.assertTrue(
					response.jsonPath().getString("ratePlanList.discounts.name").contains("Auto Pay Discount"));
			Assert.assertTrue(response.jsonPath().getString("ratePlanList.discounts.type").contains("Autopay"));
			Assert.assertTrue(response.jsonPath().getString("ratePlanList.discounts.totalDiscount").contains("5"));
			Assert.assertTrue(response.jsonPath().getString("ratePlanList.discounts.banLevelDiscount").contains("0"));
			Assert.assertTrue(
					response.jsonPath().getString("ratePlanList.discounts.lineLevelDiscount.price").contains("5"));
		}

		else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = { Group.ACCOUNTS, Group.SPRINT, "lll" })
	public void testAutopayDetailsBanLevelInGetEligiblePlansService(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testAutopayDetailsBanLevelInGetEligiblePlansService -- getEligiblePlans");
		Reporter.log("Data Conditions: ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 3:   3253209835/Auto12345 ban - 966342980 ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		System.out.println(updatedRequest.toString());

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response.body().asString() != null && response.getStatusCode() == 200) {

			Reporter.log("Response : " + response.body().asString());
			Reporter.log(response.jsonPath().getString("autopayDetails.isEligible"));
			Assert.assertTrue(response.jsonPath().getString("autopayDetails.isEligible").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("autopayDetails.isEnrolled").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("ratePlanList.sum.listSum").contains("150"));
			Assert.assertTrue(response.jsonPath().getString("ratePlanList.sum.netSum").contains("140"));
			Assert.assertTrue(response.jsonPath().getString("ratePlanList.sum.discountSum").contains("10"));
			Assert.assertTrue(
					response.jsonPath().getString("ratePlanList.discounts.name").contains("Auto Pay Discount"));
			Assert.assertTrue(response.jsonPath().getString("ratePlanList.discounts.type").contains("Autopay"));
			Assert.assertTrue(response.jsonPath().getString("ratePlanList.discounts.totalDiscount").contains("10"));
			Assert.assertTrue(response.jsonPath().getString("ratePlanList.discounts.banLevelDiscount").contains("10"));
			Reporter.log(
					response.jsonPath().getList("ratePlanList.discounts.lineLevelDiscount").size() + "is empty or not");
			Assert.assertTrue(response.jsonPath().getList("ratePlanList.discounts.lineLevelDiscount").size() == 0);
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = { Group.ACCOUNTS, Group.SPRINT, "lll" })
	public void testGSM_MBBSceanrioINPlanComparisonPage(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testGSM_MBBSceanrioINPlanComparisonPage -- getEligiblePlans");
		Reporter.log("Data Conditions: ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 3:    ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			Reporter.log(response.asString());
			assertThat(response.jsonPath().getList("ratePlanList.planSoc"),
					hasItems("UNLMTT", "MAGSLPL", "MAG55TI", "MGMIL", "MAGENTA", "TMESNTL", "TM1TI"));

			int planSocSize = response.jsonPath().getList("ratePlanList.planSoc").size();

			for (int i = 0; i < planSocSize; i++) {

				if (response.jsonPath().get("ratePlanList[" + i + "].planSoc").toString().contains("1PLSALL")) {

					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].sum.listSum").toString().contains("150"),
							"listSum for 1PLSALL is incorrect ");
					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].sum.netSum").toString().contains("150"),
							"netSum for 1PLSALL is incorrect ");

				} else if (response.jsonPath().get("ratePlanList[" + i + "].planSoc").toString().contains("ONE55TI2")) {
					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].sum.listSum").toString().contains("80"),
							"listSum for ONE55TI2 is incorrect ");
					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].sum.netSum").toString().contains("80"),
							"listSum for ONE55TI2 is incorrect ");

				} else if (response.jsonPath().get("ratePlanList[" + i + "].planSoc").toString().contains("TMESNTL2")) {
					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].sum.listSum").toString().contains("100"),
							"listSum for TMESNTL2 is incorrect ");
					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].sum.netSum").toString().contains("100"),
							"listSum for TMESNTL2 is incorrect ");

				} else if (response.jsonPath().get("ratePlanList[" + i + "].planSoc").toString().contains("1PLS552")) {
					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].sum.listSum").toString().contains("100"),
							"listSum for 1PLS552 is incorrect ");
					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].sum.netSum").toString().contains("100"),
							"listSum for 1PLS552 is incorrect ");

				} else if (response.jsonPath().get("ratePlanList[" + i + "].planSoc").toString().contains("TMSERVE2")) {
					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].sum.listSum").toString().contains("90"),
							"listSum for TMSERVE2 is incorrect ");
					Assert.assertTrue(
							response.jsonPath().get("ratePlanList[" + i + "].sum.netSum").toString().contains("90"),
							"listSum for TMSERVE2 is incorrect ");
				}
			}

		}

		else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = { Group.ACCOUNTS, Group.SPRINT, "lll" })
	public void testGSM_MBBSceanrioINPlanReviewPage(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGSM_MBBSceanrioINPlanComparisonPage -- getEligiblePlans");
		Reporter.log("Data Conditions: ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 3:    ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("taxTreatment", "TI");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePlans.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = getEligiblePlansMethod(apiTestData, tokenMap, updatedRequest);
		Reporter.log(response.asString());

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			System.out.println(updatedRequest.toString());
			Assert.assertTrue(response.jsonPath().get("ratePlanList[0].sum.listSum").toString().contains("20"),
					"listSum for 1PLSALL is incorrect ");
			Assert.assertTrue(response.jsonPath().get("ratePlanList[0].sum.netSum").toString().contains("20"),
					"netSum for 1PLSALL is incorrect ");
			// you will get onlyone plan soc verify sum,discounts
		}

		else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}

}