package com.tmobile.eservices.qa.accounts.api;

import java.io.File;
import java.io.FileInputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.api.EOSCommonLib;

public class Testdatavalidator {

	EOSCommonLib ecl = new EOSCommonLib();

	public Set<String> findMethods(String[] packageName, String grp) throws ClassNotFoundException {
		Set<String> meths = new HashSet<>();
		for (String pkg : packageName) {
			Set<String> clas = getallClasses(pkg);
			Iterator<String> iterator = clas.iterator();
			while (iterator.hasNext()) {
				String cls = iterator.next();
				Class<?> c = Class.forName(cls);
				Method[] methods = c.getDeclaredMethods();
				for (Method method : methods) {
					Annotation annotation = method.getAnnotation(Test.class);
					if (annotation instanceof Test) {
						Test customAnnotation = (Test) annotation;
						if (Arrays.stream(customAnnotation.groups()).anyMatch(grp::equals)) {
							// if(customAnnotation.groups().toString().contains("payments"))
							// {
							meths.add(method.getName());
							System.out.println(method.getName());
						}

					}
				}

			}

		}

		return meths;

	}

	public Set<String> getallClasses(String packageName) {
		Set<String> classes = new HashSet<>();
		String pkg = packageName.replace(".", "/");
		File directory = new File(System.getProperty("user.dir") + "/src/test/java/" + pkg);
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.getName().endsWith(".java")) {
				classes.add(packageName + '.' + file.getName().substring(0, file.getName().length() - 5));

			}
		}
		return classes;
	}

	/*
	 * private static Set<String> findClasses(String packageName) throws
	 * ClassNotFoundException { Set<String> classes = new HashSet<>(); String
	 * pkg=packageName.replace(".","/"); File directory=new
	 * File(System.getProperty("user.dir")+"/src/test/java/"+pkg); if
	 * (!directory.exists()) { return classes; } File[] files =
	 * directory.listFiles(); for (File file : files) { if
	 * (file.getName().endsWith(".java")) { classes.add(packageName + '.' +
	 * file.getName().substring(0, file.getName().length() - 5));
	 * 
	 * } } return classes; }
	 * 
	 * 
	 */

	@DataProvider(name = "data-provider", parallel = true)
	public Object[][] dataProviderMethod() {
		return new Object[][] { { "pub" }, { "shop" }, { "global" }, { "am" } };

	}

	// @Parameters({ "capability" })
	@Test(dataProvider = "data-provider")
	public void Testdataevaluation(String capability1) throws ClassNotFoundException {
		String FILE_NAME;
		Set<String> meths = new HashSet<>();

		switch (capability1) {
		case "pub":
			FILE_NAME = System.getProperty("user.dir") + "/src/test/resources/testdata/Payments.xls";
			String[] packageNamep = { "com.tmobile.eservices.qa.payments.functional",
					"com.tmobile.eservices.qa.payments.acceptance" };
			meths = findMethods(packageNamep, "payments");
			break;
		case "shop":
			FILE_NAME = System.getProperty("user.dir") + "/src/test/resources/testdata/Shop.xls";
			String[] packageNames = { "com.tmobile.eservices.qa.shop.acceptance",
					"com.tmobile.eservices.qa.shop.functional" };
			meths = findMethods(packageNames, "shop");
			break;
		case "global":
			FILE_NAME = System.getProperty("user.dir") + "/src/test/resources/testdata/Global.xlsx";
			String[] packageNameg = { "com.tmobile.eservices.qa.global.acceptance",
					"com.tmobile.eservices.qa.global.functional" };
			meths = findMethods(packageNameg, "global");
			break;
		case "am":
			FILE_NAME = System.getProperty("user.dir") + "/src/test/resources/testdata/Accounts.xls";
			String[] packageNameam = { "com.tmobile.eservices.qa.accounts.functional",
					"com.tmobile.eservices.qa.accounts.acceptance" };
			meths = findMethods(packageNameam, "accounts");
			break;
		default:
			FILE_NAME = "nofile";
			break;
		}
		if (FILE_NAME.equalsIgnoreCase("nofile")) {
			Assert.fail("No file is given for provided capability");
		}

		try {
			boolean itest = true;
			Reporter.log(
					"<table><tbody><tr><td>Misdin</td><td>Password</td><td>Is Valid</td><td>Ban</td><td>UserType</td><td>MailID</td><td>FirstName</td><td>LastName</td></tr>");
			Set<String> misdinpawords = getallmisdins(FILE_NAME, meths);
			Iterator<String> iterator = misdinpawords.iterator();

			while (iterator.hasNext()) {
				String misdpwd = iterator.next();
				String misdin = misdpwd.split("/")[0];
				String pwd = misdpwd.split("/")[1];
				if (misdin.trim().length() > 0 && pwd.trim().length() > 0) {

					Reporter.log("<tr>");

					String code = ecl.getCodefromauthtokenresponse(misdin, pwd);
					if (code != null) {

						String[] getjwt = ecl.getparametersfromusertokenresponse(code, misdin);
						if (getjwt != null) {
							String accesscode = getjwt[1];
							String jwttoken = getjwt[2];
							String ban = getjwt[3];
							String usertype = getjwt[4];
							String mailid = getjwt[5];
							String firstname = getjwt[6];
							String lastname = getjwt[7];
							Reporter.log("<td><font face=\"verdana\" color=\"green\">" + misdin + "</font></td>");
							Reporter.log("<td><font face=\"verdana\" color=\"green\">" + pwd + "</font></td>");
							Reporter.log("<td><font face=\"verdana\" color=\"green\">Valid</font></td>");
							Reporter.log("<td><font face=\"verdana\" color=\"green\">" + ban + "</font></td>");
							Reporter.log("<td><font face=\"verdana\" color=\"green\">" + usertype + "</font></td>");
							Reporter.log("<td><font face=\"verdana\" color=\"green\">" + mailid + "</font></td>");
							Reporter.log("<td><font face=\"verdana\" color=\"green\">" + firstname + "</font></td>");
							Reporter.log("<td><font face=\"verdana\" color=\"green\">" + lastname + "</font></td>");

						}

					} else {
						itest = false;
						System.out.println(misdin);
						Reporter.log("<td><font face=\"verdana\" color=\"red\">" + misdin + "</font></td>");
						Reporter.log("<td><font face=\"verdana\" color=\"red\">" + pwd + "</font></td>");
						Reporter.log("<td><font face=\"verdana\" color=\"red\">In Valid</font></td>");
						Reporter.log("<td><font face=\"verdana\" color=\"red\">NA</font></td>");
						Reporter.log("<td><font face=\"verdana\" color=\"red\">NA</font></td>");
						Reporter.log("<td><font face=\"verdana\" color=\"red\">NA</font></td>");
						Reporter.log("<td><font face=\"verdana\" color=\"red\">NA</font></td>");
						Reporter.log("<td><font face=\"verdana\" color=\"red\">NA</font></td>");

					}

				}

				Reporter.log("</tr>");
			}
			Reporter.log("</tbody></table>");
			if (!itest) {
				Assert.fail();
			}

		} catch (Exception eq11) {
			eq11.printStackTrace();

		}
	}

	public Set<String> getallmisdins(String excel, Set<String> mths) {
		Set<String> misdinpassword = new HashSet<>();
		try {

			Workbook workbook;
			FileInputStream excelFile = new FileInputStream(new File(excel));
			if (excel.contains("xlsx")) {
				workbook = new XSSFWorkbook(excelFile);
			} else {
				workbook = new HSSFWorkbook(excelFile);
			}

			Sheet datatypeSheet = workbook.getSheetAt(0);
			Iterator<Row> iterator = datatypeSheet.iterator();

			while (iterator.hasNext()) {

				Row currentRow = iterator.next();

				if (currentRow.getCell(0) != null && currentRow.getCell(1) != null && currentRow.getCell(2) != null) {
					String tc = currentRow.getCell(0).getStringCellValue();
					String misdin = currentRow.getCell(1).getStringCellValue();
					String pwd = currentRow.getCell(2).getStringCellValue();

					if (!misdin.trim().equals("")) {
						if (!misdin.equalsIgnoreCase("loginEmailOrPhone")) {
							if (mths.contains(tc)) {
								misdinpassword.add(misdin.trim() + "/" + pwd.trim());
							}

						}
					}
				}
			}
			workbook.close();
			excelFile.close();

		} catch (Exception e) {

		}
		return misdinpassword;
	}

}
