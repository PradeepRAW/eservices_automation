package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.FamilyControlsPage;
import com.tmobile.eservices.qa.pages.accounts.FamilyWherePage;

public class FamilyWherePageTest extends GlobalCommonLib {

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyFamilyWhere(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on family controls link | Family controls page should be displayed");
		Reporter.log("6. Click family where link | Family where page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData, "Family Controls");
		familyControlsPage.clickFamilyWhere();

		FamilyWherePage familyWherePage = new FamilyWherePage(getDriver());
		familyWherePage.verifyFamilyWherePage();
	}

	/**
	 * US596547-[Continued] PI | Integrating ALL Partners except FamilyWhere
	 * TC325552
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testIntegratingALLPartnersExceptFamilyWhereWithPostPaidMissdn(MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on family controls link | Family controls page should be displayed");
		Reporter.log("6. Click family where link | It should be navigated to 3rd party URL");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData, "Family Controls");
		familyControlsPage.clickFamilyWhere();

		FamilyWherePage familyWherePage = new FamilyWherePage(getDriver());
		familyWherePage.verifyFamilyWherePageNavigatedToThirdPartyUrl();
	}
}
