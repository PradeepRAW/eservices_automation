package com.tmobile.eservices.qa.payments.functional;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.payments.UsageDetailsPage;
import com.tmobile.eservices.qa.pages.payments.UsagePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class UsageDetailsPageTest extends PaymentCommonLib {

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUsagePageFiltering(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: US306452 Usage Modernization: Usage Details Pagination");
		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on data and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.Click on filtering and select the data | Should be able to select the data accordingly");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		// UsagePage usagePage = navigateToUsagePage(myTmoData);
		launchAndPerformLogin(myTmoData);
		getDriver().get("https://qata03.eservice.t-mobile.com/usage");
		UsagePage usagePage = new UsagePage(getDriver());

		usagePage.clickOptionViewByCategory("Data");
		usagePage.clickLineViewByCategory();

		UsageDetailsPage usageDeatilsPage = new UsageDetailsPage(getDriver());
		usageDeatilsPage.verifyUsageDetailsPage();
		usageDeatilsPage.clickOnFilter();
		usageDeatilsPage.selectData();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUsageDetailsPageSorting(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: US306452 Usage Modernization: Usage Details Pagination");
		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on data and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.Select the type/Usage/location sorting | Should be able to sort data ascending");
		Reporter.log("7.Click the type/Usage/location sorting down| Should be able to sort data descending");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		// UsagePage usagePage = navigateToUsagePage(myTmoData);
		launchAndPerformLogin(myTmoData);
		getDriver().get("https://qata03.eservice.t-mobile.com/usage");
		UsagePage usagePage = new UsagePage(getDriver());

		usagePage.clickOptionViewByCategory("Data");
		usagePage.clickLineViewByCategory();

		UsageDetailsPage usageDeatilsPage = new UsageDetailsPage(getDriver());
		usageDeatilsPage.verifyUsageDetailsPage();
		usageDeatilsPage.clickSortingArrow();
		usageDeatilsPage.verifySorting();
	}



	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP})
	public void testverifyFilterfordata(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on data which is not zero and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.Apply filter on all columns and check | Filter should be worked as expected");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		UsageDetailsPage usagedetails=NavigatetoUsagedetailspage(myTmoData,"Data","Data details");



		//	usagedetails.clickdropdown();
		//usagedetails.checkprevioususagedropdowns();

		//usagedetails.selectBillCyclewithData();
		usagedetails.verifyDownloadUsageRecordsCTA();
		String[] tobechecked= {"Date & Time (Pacific)","Service","Usage"};
		//String headerval="Date & Time (Pacific)";
		for(String headerval:tobechecked) {

			int headercol=usagedetails.getheadercolumn(headerval);
			if(headercol!=-1) {
				usagedetails.clickOnFilter();
				usagedetails.checkdagetlinksonfilter(true);
				List<WebElement> vals=usagedetails.getallrowdatfromgivencolumn(headercol+1,headerval);
				if(vals.size()>0) {
					String texttobecheck=usagedetails.clickongivenelemt(vals.get(0));
					vals=usagedetails.getallrowdatfromgivencolumn(headercol+1,headerval);
					usagedetails.checkfilteredvalues(vals,texttobecheck);
					usagedetails.checkfiltertext(texttobecheck);
					usagedetails.closefilter();
					usagedetails.clickOnFilter();
					usagedetails.checkdagetlinksonfilter(false);
				}


			}}

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP})
	public void testverifyFilterforMessages(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on messages which is not zero and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.Apply filter on all columns and check | Filter should be worked as expected");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");


		UsageDetailsPage usagedetails=NavigatetoUsagedetailspage(myTmoData,"Messages","Message details");

		//usagedetails.clickdropdown();
		//usagedetails.checkprevioususagedropdowns();
		//usagedetails.selectBillCyclewithData();
		usagedetails.verifyDownloadUsageRecordsCTA();
		String[] tobechecked= {"Date & Time (Pacific)","Destination","Number","Direction","Type"};
		//String headerval="Date & Time (Pacific)";

		for(String headerval:tobechecked) {

			int headercol=usagedetails.getheadercolumn(headerval);
			if(headercol!=-1) {
				usagedetails.clickOnFilter();
				usagedetails.checkdagetlinksonfilter(true);
				List<WebElement> vals=usagedetails.getallrowdatfromgivencolumn(headercol+1,headerval);
				if(vals.size()>0) {
					String texttobecheck=usagedetails.clickongivenelemt(vals.get(0));
					vals=usagedetails.getallrowdatfromgivencolumn(headercol+1,headerval);
					usagedetails.checkfilteredvalues(vals,texttobecheck);
					usagedetails.checkfiltertext(texttobecheck);
					usagedetails.closefilter();
					usagedetails.clickOnFilter();
					usagedetails.checkdagetlinksonfilter(false);
				}


			}}



	}




	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP})
	public void testverifyFilterforCalls(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on calls which is not zero and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.Apply filter on all columns and check | Filter should be worked as expected");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		UsageDetailsPage usagedetails=NavigatetoUsagedetailspage(myTmoData,"Calls","Call details");

		//usagedetails.clickdropdown();
		//usagedetails.checkprevioususagedropdowns();
		//usagedetails.selectBillCyclewithData();
		usagedetails.verifyDownloadUsageRecordsCTA();
		String[] tobechecked= {"Date & Time (Pacific)","Destination","Number","Min","Type"};
		//String headerval="Date & Time (Pacific)";
		for(String headerval:tobechecked) {

			int headercol=usagedetails.getheadercolumn(headerval);
			if(headercol!=-1) {
				usagedetails.clickOnFilter();
				usagedetails.checkdagetlinksonfilter(true);
				List<WebElement> vals=usagedetails.getallrowdatfromgivencolumn(headercol+1,headerval);
				if(vals.size()>0) {
					String texttobecheck=usagedetails.clickongivenelemt(vals.get(0));
					vals=usagedetails.getallrowdatfromgivencolumn(headercol+1,headerval);
					usagedetails.checkfilteredvalues(vals,texttobecheck);
					usagedetails.checkfiltertext(texttobecheck);
					usagedetails.closefilter();
					usagedetails.clickOnFilter();
					usagedetails.checkdagetlinksonfilter(false);
				}


			}}


	}



	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged"})
	public void testdownloadusagerecords(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on data which is not zero and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.verify download usage records button | Download usage button shoulbe existed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		UsageDetailsPage usagedetails=NavigatetoUsagedetailspage(myTmoData,"Data","Data details");
		usagedetails.verifyDownloadUsageRecordsCTA();


	}



	@Test(dataProvider = "byColumnName", enabled = true, groups = {"merged"})
	public void testdropdownbuttonandcheckprevioususage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on data which is not zero and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.verify download usage records button | Download usage button shoulbe existed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		UsageDetailsPage usagedetails=NavigatetoUsagedetailspage(myTmoData,"Data","Data details");
		usagedetails.clickdropdown();
		usagedetails.checkprevioususagedropdowns();
	}



	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP})
	public void testverifysortingfunctionalityfordata(ControlTestData data, MyTmoData myTmoData){

		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on data which is not zero and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.Select the sorting for all fields | Should be able to sort data ascending");
		Reporter.log("7.Click the sorting down for all fields| Should be able to sort data descending");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		UsageDetailsPage usagedetails=NavigatetoUsagedetailspage(myTmoData,"Data","Data details");
		String[] tobechecked= {"Date & Time (Pacific)","Service","Usage"};
		//String headerval="Date & Time (Pacific)";
		for(String headerval:tobechecked) {

			//String headerval="Date & Time (Pacific)";
			int headercol=usagedetails.getheadercolumn(headerval);

			if(headercol!=-1) {
				usagedetails.checkappisinfirstpage();
				usagedetails.keepcolumndscorder(headercol);
				List<String> desarray=usagedetails.getalldatainarray(headercol);
				usagedetails.checkappisinfirstpage();
				usagedetails.keepcolumnascorder(headercol);
				List<String> ascarray=usagedetails.getalldatainarray(headercol);
				Collections.reverse(desarray);
				if(ascarray.equals( desarray))Reporter.log("sorting is good for header :"+headerval);
				else Assert.fail("sorting is not good for header :"+headerval);

			}
		}
	}





	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PAYMENTS, Group.DESKTOP})
	public void testverifysortingfunctionalityforMessages(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on Messages which is not zero and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.Select the sorting for all fields | Should be able to sort data ascending");
		Reporter.log("7.Click the sorting down for all fields| Should be able to sort data descending");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		UsageDetailsPage usagedetails=NavigatetoUsagedetailspage(myTmoData,"Messages","Message details");
		String[] tobechecked= {"Date & Time (Pacific)","Destination","Number","Direction","Type"};
		//String headerval="Date & Time (Pacific)";
		for(String headerval:tobechecked) {

			//String headerval="Date & Time (Pacific)";
			int headercol=usagedetails.getheadercolumn(headerval);

			if(headercol!=-1) {
				usagedetails.checkappisinfirstpage();
				usagedetails.keepcolumndscorder(headercol);
				List<String> desarray=usagedetails.getalldatainarray(headercol);
				usagedetails.checkappisinfirstpage();
				usagedetails.keepcolumnascorder(headercol);
				List<String> ascarray=usagedetails.getalldatainarray(headercol);
				Collections.reverse(desarray);
				if(ascarray.equals( desarray))Reporter.log("sorting is good for header :"+headerval);
				else Assert.fail("sorting is not good for header :"+headerval);

			}
		}
	}


	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testverifysortingfunctionalityforCalls(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on Calls which is not zero and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.Select the sorting for all fields | Should be able to sort data ascending");
		Reporter.log("7.Click the sorting down for all fields| Should be able to sort data descending");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		UsageDetailsPage usagedetails=NavigatetoUsagedetailspage(myTmoData,"Calls","Call details");
		String[] tobechecked= {"Date & Time (Pacific)","Destination","Number","Min","Type"};
		//String headerval="Date & Time (Pacific)";
		for(String headerval:tobechecked) {

			//String headerval="Date & Time (Pacific)";
			int headercol=usagedetails.getheadercolumn(headerval);

			if(headercol!=-1) {
				usagedetails.checkappisinfirstpage();
				usagedetails.keepcolumndscorder(headercol);
				List<String> desarray=usagedetails.getalldatainarray(headercol);
				usagedetails.checkappisinfirstpage();
				usagedetails.keepcolumnascorder(headercol);
				List<String> ascarray=usagedetails.getalldatainarray(headercol);
				Collections.reverse(desarray);
				if(ascarray.equals( desarray))Reporter.log("sorting is good for header :"+headerval);
				else Assert.fail("sorting is not good for header :"+headerval);

			}
		}
	}






	/**
	 * DE215874 MyTMO [Usage] MSISDN on Picture usage details not displaying correctly

	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PAYMENTS, Group.DESKTOP})
	public void testPictureMessageDetailsonUsageDetailsPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data: ");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on messages which is not zero and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.Apply filter on picture column | MSISDN  column ,picture colum should be verified");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");


		UsageDetailsPage usagedetails=NavigatetoUsagedetailspage(myTmoData,"Messages","Message details");

		boolean exitallloops=false;
		for(WebElement cycle:usagedetails.getallusagecycles()) {
			usagedetails.clickdropdown();
			usagedetails.elementClick(cycle);
			boolean loopwhile=true;
			while(loopwhile) {
				usagedetails.waittillspinnerdisapper();
				int typecol=usagedetails.getheadercolumn("Type");
				int numcol=usagedetails.getheadercolumn("Number");
				if(typecol==-1||numcol==-1) {Assert.fail("required clumns are not there in table");}

				List<WebElement> vals=usagedetails.getallrowdatfromgivencolumn(typecol+1,"Type");
				for(WebElement element :vals) {
					if(element.getText().trim().equalsIgnoreCase("Picture")) {
						if(usagedetails.checkmobilenumber(element,numcol+1)=="valid") {
							exitallloops=true;
							Reporter.log("mobile number format passed for picture");

							break;
						}

						if(usagedetails.checkmobilenumber(element,numcol+1)=="invalid") {
							exitallloops=true;
							Assert.fail("mobile number format failed for picture");

							break;
						}
					}



				}if(exitallloops)break;

				if(usagedetails.checkhtenextbuttonenable())usagedetails.clicknextbutton();
				else loopwhile=false;
			}if(exitallloops)break;

		} 

	}







	/**
	 * DE228797 MyTMO [Usage] 500 returned for certain accounts' specific bill cycles

	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PAYMENTS, Group.DESKTOP })
	public void testprevioususagecyclesformessage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data: ");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on messages which is not zero and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.Check all previous cycles loaded| All previuos cycles should be loaded");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");


		UsageDetailsPage usagedetails=NavigatetoUsagedetailspage(myTmoData,"Messages","Message details");


		int ucycle=0;
		for(WebElement cycle:usagedetails.getallusagecycles()) {

			usagedetails.clickdropdown();
			String billcycle=cycle.getText();
			usagedetails.elementClick(cycle);
			usagedetails.waittillspinnerdisapper();
			if (ucycle!=0) {

				Reporter.log("----------Usage Cycle"+billcycle+"-----------");
				String[] tobechecked= {"Date & Time (Pacific)","Destination","Number","Direction","Type","Charge"};
				for(String header:tobechecked) {
					if(usagedetails.getheadercolumn(header)==-1) Verify.fail(header+" Header name is not Listed");
					else Reporter.log(header+" Header name is Listed");
				}
				usagedetails.checkFilterbuttonExists();

				usagedetails.checkTotalsmessage("Messages");
				usagedetails.checkTotalcharges("Messages");
				usagedetails.checkwhetherpaginationimplemented("Type");
				usagedetails.checkrowbasedonmessagetext("Type");
			}

			ucycle++;
		} 

	}




	/**
	 * DE228797 MyTMO [Usage] 500 returned for certain accounts' specific bill cycles

	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PAYMENTS, Group.DESKTOP})
	public void testprevioususagecyclesforCalls(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data: ");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on Calls which is not zero and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.Check all previous cycles loaded| All previuos cycles should be loaded");


		Reporter.log("================================");
		Reporter.log("Actual Test Steps");



		UsageDetailsPage usagedetails=NavigatetoUsagedetailspage(myTmoData,"Calls","Call details");


		int ucycle=0;
		for(WebElement cycle:usagedetails.getallusagecycles()) {

			usagedetails.clickdropdown();
			String billcycle=cycle.getText();
			usagedetails.elementClick(cycle);
			usagedetails.waittillspinnerdisapper();
			if (ucycle!=0) {

				Reporter.log("----------Usage Cycle"+billcycle+"-----------");

				String[] tobechecked= {"Date & Time (Pacific)","Destination","Number","Min","Type","Charge"};
				for(String header:tobechecked) {
					if(usagedetails.getheadercolumn(header)==-1) Verify.fail(header+" Header name is not Listed");
					else Reporter.log(header+" Header name is Listed");
				}
				usagedetails.checkFilterbuttonExists();

				usagedetails.checkTotalsmessage("Calls");
				usagedetails.checkTotalcharges("Calls");
				usagedetails.checkrowbasedonmessagetext("Type");
			}

			ucycle++;
		} 

	}




	/**
	 * DE228797 MyTMO [Usage] 500 returned for certain accounts' specific bill cycles

	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PAYMENTS, Group.DESKTOP })
	public void testprevioususagecyclesforData(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data: ");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click on Data which is not zero and Select the lines| Should be able to select the lines ex Anastasia");
		Reporter.log("6.Check all previous cycles loaded| All previuos cycles should be loaded");


		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		UsageDetailsPage usagedetails=NavigatetoUsagedetailspage(myTmoData,"Data","Data details");

		int ucycle=0;
		for(WebElement cycle:usagedetails.getallusagecycles()) {

			usagedetails.clickdropdown();
			String billcycle=cycle.getText();
			usagedetails.elementClick(cycle);
			usagedetails.waittillspinnerdisapper();
			if (ucycle!=0) {

				Reporter.log("----------Usage Cycle"+billcycle+"-----------");


				String[] tobechecked= {"Date & Time (Pacific)","Service","Usage","Charge"};
				for(String header:tobechecked) {
					if(usagedetails.getheadercolumn(header)==-1) Verify.fail(header+" Header name is not Listed");
					else Reporter.log(header+" Header name is Listed");
				}
				usagedetails.checkFilterbuttonExists();

				usagedetails.checkTotalsmessage("Data");
				usagedetails.checkTotalcharges("Data");
				usagedetails.checkrowbasedonmessagetext("Usage");
			}

			ucycle++;
		} 

	}




	/**
	 * DE235321 MyTMO [Usage] Date selector cycle is not displayed if the same cycle selected twice or more
	 * *DE234524
	MyTMO [Usage] Date selector does not collapse
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUsageDateSelectorCycle(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : DE235321-MyTMO [Usage] Date selector cycle is not displayed if the same cycle selected twice or more");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.navigate to usage details page|sage details page should be dispalyed");
		Reporter.log("6. select same bill cycle twice| selected bill cycle should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		UsagePage usage = navigateToUsagePage(myTmoData);
		Map<String, String> usageheaders = new HashMap<String, String>();
		usageheaders.put("Data", "Data details");
		usageheaders.put("Messages", "Message details");
		usageheaders.put("Calls", "Call details");
		UsageDetailsPage usagedetails = new UsageDetailsPage(getDriver());
		for (Map.Entry<String, String> entry : usageheaders.entrySet()) {

			navigatetoUsagedetailspageFromUsagePage(myTmoData,entry.getKey(),entry.getValue());
			int count=1;
			for(WebElement cycle:usagedetails.getallusagecycles()) {

				usagedetails.clickdropdown();
				String billcycle=cycle.getText();
				usagedetails.elementClick(cycle);
				usagedetails.verifySelectedBillCycleDisplayed(billcycle);
				usagedetails.verifyDateSelectorDropdownCollapsed();
				count++;

				if(count>=5) {
					break;
				}
			}
			usagedetails.clickBackButton();
			usage.verifyPageUrl();
		}
	}


	/**
	 * US552841 Usage - Mask PII on Usage Modernization flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPiiMaskingOnMessageDetails(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US552841-Usage - Mask PII on Usage Modernization flow");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.navigate to Message details page|Message details page should be dispalyed");
		Reporter.log("6. verify Destination, Number PII are masked | Destination, Number details should be masked");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		UsageDetailsPage usagedetails = NavigatetoUsagedetailspage(myTmoData, "Messages", "Message details");
		usagedetails.verifyUserNamePiiMasking();
		for (WebElement cycle : usagedetails.getallusagecycles()) {
			usagedetails.clickdropdown();
			usagedetails.elementClick(cycle);
			boolean loopwhile = true;
			while (loopwhile) {
				usagedetails.waittillspinnerdisapper();

				String[] tobechecked = { "Destination", "Number" };
				for (String headerval : tobechecked) {

					int headercol = usagedetails.getheadercolumn(headerval);

					List<WebElement> vals = usagedetails.getallrowdatfromgivencolumn(headercol + 1, headerval);
					for (WebElement val : vals) {

						usagedetails.verifyPiiMasking(val, headerval);
						// usagedetails.verifyPiiMasking(val,"cust_city",headerval);
					}
				}
				if (usagedetails.checkhtenextbuttonenable())
					usagedetails.clicknextbutton();
				else
					loopwhile = false;
			}

		}

	}

	/**
	 * US552841 Usage - Mask PII on Usage Modernization flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPiiMaskingOnCallDetails(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US552841-Usage - Mask PII on Usage Modernization flow");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.navigate to call details page|call details page should be dispalyed");
		Reporter.log("6. verify Destination, Number PII are masked | Destination, Number details should be masked");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		UsageDetailsPage usagedetails = NavigatetoUsagedetailspage(myTmoData, "Calls", "Call details");
		usagedetails.verifyUserNamePiiMasking();
		for (WebElement cycle : usagedetails.getallusagecycles()) {
			usagedetails.clickdropdown();
			usagedetails.elementClick(cycle);
			boolean loopwhile = true;
			while (loopwhile) {
				usagedetails.waittillspinnerdisapper();

				String[] tobechecked = { "Destination", "Number" };
				for (String headerval : tobechecked) {

					int headercol = usagedetails.getheadercolumn(headerval);

					List<WebElement> vals = usagedetails.getallrowdatfromgivencolumn(headercol + 1, headerval);
					for (WebElement val : vals) {

						usagedetails.verifyPiiMasking(val, headerval);
					
					}
				}
				if (usagedetails.checkhtenextbuttonenable())
					usagedetails.clicknextbutton();
				else
					loopwhile = false;
			}

		}

	}


}
