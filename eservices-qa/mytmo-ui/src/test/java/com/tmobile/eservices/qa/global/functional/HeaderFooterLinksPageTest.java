package com.tmobile.eservices.qa.global.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.global.GlobalConstants;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.AccessibilityPage;
import com.tmobile.eservices.qa.pages.global.ContactUSPage;
import com.tmobile.eservices.qa.pages.global.CopyrightsPage;
import com.tmobile.eservices.qa.pages.global.CoveragePage;
import com.tmobile.eservices.qa.pages.global.NewContactUSPage;
import com.tmobile.eservices.qa.pages.global.OpenInternetPolicyPage;
import com.tmobile.eservices.qa.pages.global.PrivacyPolicyPage;
import com.tmobile.eservices.qa.pages.global.PrivacyResourcePage;
import com.tmobile.eservices.qa.pages.global.PublicSafetyPage;
import com.tmobile.eservices.qa.pages.global.ReturnPolicyPage;
import com.tmobile.eservices.qa.pages.global.StoreLocatorPage;
import com.tmobile.eservices.qa.pages.global.TermsAndConditionsPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author sllaithambigai
 * 
 */
public class HeaderFooterLinksPageTest extends GlobalCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(HeaderFooterLinksPageTest.class);

	/**
	 * TC002_MYTMO_Cloud_Footer_Verify the Contact us link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyContactusLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyContactusLink method");
		Reporter.log("Test Case : MYTMO_Cloud_Footer_Verify the Contact us link");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Contact Us link | Contact Us Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickContactUSlink();

		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
	}

	/**
	 * TC003_MYTMO_Cloud_Footer_Verify the Coverage link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID })
	private void verifyCoveragelink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyCoveragelink method");
		Reporter.log("Test Case : MYTMO_Cloud_Footer_Verify the Coverage link");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Coverage link | Coverage page should be displayed");

		Reporter.log("===========================");
		Reporter.log("Actual Results:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickCoverageLink();
		homePage.switchToWindow();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		CoveragePage coveragePage = new CoveragePage(getDriver());
		coveragePage.verifyCoveragePage();
	}

	/**
	 * TC005_MYTMO_Cloud_Footer_Verify the Return Policy link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID })
	public void verifyReturnPolicyLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyReturnPolicyLink method");
		Reporter.log("Test Case : MYTMO_Cloud_Footer_Verify the Return Policy link");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Return Policy link | Return policy Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickReturnPolicyLink();
		homePage.switchToWindow();

		ReturnPolicyPage returnPolicyPage = new ReturnPolicyPage(getDriver());
		returnPolicyPage.verifyReturnPolicyPage();
		closeNewWindow();
	}

	/**
	 * TC006_MYTMO_Cloud_Footer_Verify the store locator link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DUPLICATE })
	public void verifyStoreLocator(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : MYTMO_Cloud_Footer_Verify the store locator link");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on footer store locator link | store locator link should be clicked");
		Reporter.log("5. Verify store locator page | store locator page should be displayed");

		Reporter.log("=======================================");
		Reporter.log("Actual Result:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickFooterStoreLocatorlink();
		homePage.switchToWindow();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreLocatorPage();
	}

	/**
	 * TC024_MYTMO_cloud_Header_Verify the t mobile link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID })
	public void verifyTmobileLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyTmobileLink method");
		Reporter.log("Test Case : MYTMO_cloud_Header_Verify the t mobile link");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on T-Mobile Link | T-Mobile Page should be displayed");
		Reporter.log("==========================");
		Reporter.log("Actual Result:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		// homePage.clickTMobileLink();
		homePage.switchToWindow();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		// homePage.verifyCloudTmobilePage();
		Reporter.log("T-Mobile Page is displayed");
	}

	/**
	 * Verify Terms And Conditions Link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DUPLICATE })
	public void verifyTermsAndConditions(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyTermsAndConditions method");
		Reporter.log("Test Case : MYTMO_Cloud_Footer_Verify Terms And Conditions Link");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Terms & Conditions | Terms & Conditions page should be displayed");
		Reporter.log("==========================");
		Reporter.log("Actual Results:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickTermsAndConditionsLink();
		homePage.switchToWindow();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		TermsAndConditionsPage termsAndConditonsPage = new TermsAndConditionsPage(getDriver());
		termsAndConditonsPage.verifyTermsAndConditionsPage();
		closeNewWindow();
	}

	/**
	 * Verify Messaging in Contact US
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PENDING })
	public void verifyMessagingInContactus(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on contact us link | Contact us page should be displayed");
		Reporter.log("5. Click on message us cta | Clicked on message us cta");
		Reporter.log("6. Verify message us chat pop up | Message us chat pop up should be displayed");

		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
		contactUSPage.clickMessageUsBtn();
		contactUSPage.verifyChatPopUp();
	}

	/**
	 * AAL - 01_MYTMO_Cloud_Footer_Verify the About link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "Header Footer Links")
	public void verifyAboutLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyContactusLink method");
		Reporter.log("Test Case : MYTMO_Cloud_Footer_Verify the About link");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps");
		Reporter.log("1. Launch the application");
		Reporter.log("2. Login to the application");
		Reporter.log("3. Verify Home page");
		Reporter.log("4. Click on About T MobileUs link");
		Reporter.log("5. Verify About TMobile Page");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickAboutTmobileLink();
		homePage.switchToWindow();
		Assert.assertTrue(getDriver().getTitle().contains("Company"), GlobalConstants.ABOUTTMOBILE_PAGE_ERROR_MESSAGE);
		Reporter.log("About T-Mobile Page is displayed");

	}

	/**
	 * US153687 Lodestar - Messaging Outage - Change ""Message Us"" CTA Click
	 * Behavior for Disabled State US153689 Lodestar - Messaging Outage - Change
	 * ""Call Us"" CTA Click Behavior for Disabled State US157610 [Continued]
	 * Lodestar - Messaging Outage - Both ""Call Us"" and ""Message US"" CTA Error
	 * Messaging for Disabled State
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "Header Footer Links")
	public void verifyErrorMsgForMessagingInAndCallMeInContactus(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyMessagingInContactus method");
		Reporter.log("Test Case : Verify Messaging In Contact US page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log(GlobalConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Footer ContactUS Link |Contact US page should be displayed");
		Reporter.log(
				"5: Verify Message US & Call US buttons are enabled or not | If button is not enabled then respective Error message should be displayed.");
		Reporter.log("==========================");
		Reporter.log(GlobalConstants.ACTUAL_TEST_STEPS);

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickContactUSlink();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		Assert.assertTrue(getDriver().getTitle().contains("Contact Us"), GlobalConstants.CONTACT_PAGE_ERROR_MESSAGE);
		Reporter.log("Contact US page is displayed");
		Boolean isMessageUsBtnDisabled = contactUSPage.verifyMessageUsBtnDisabled();
		Boolean isCallUsBtnEnabled = contactUSPage.verifyCallUsBtn();
		if (isMessageUsBtnDisabled && !isCallUsBtnEnabled) {
			contactUSPage.verifyErrorTextForMessageUsAndCallUsBtnsDisablity();

		} else if (isMessageUsBtnDisabled) {
			contactUSPage.verifyErrorTextForMessageUsBtnDisablity();

		} else if (!isCallUsBtnEnabled) {
			contactUSPage.verifyErrorTextForCallUsBtnDisablity();

		}
	}

	/**
	 * Verify Privacy And Policy Link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID })
	public void verifyPrivacyAndPolicy(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyPrivacyAndPolicy method");
		Reporter.log("Test Case : Verify Privacy And Policy Link redirects to privacy policy page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on privacy policy link | Privacy policy Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickPrivacyPolicyLink();
		homePage.switchToWindow();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		PrivacyPolicyPage privacyPolicyPage = new PrivacyPolicyPage(getDriver());
		privacyPolicyPage.verifyPrivacyPolicyPage();
		Reporter.log("Privacy policy page is displayed");
	}

	/**
	 * Verify Interest Based Ads Link
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void verifyInterestBasedAdsLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyInterestBasedAdsLink method");
		Reporter.log(
				"Test Case : Verify Interest-Based Ads Link redirects to Interest-Based Ads info on Privacy Policy Page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Interest-Based Ads link | Interest-Based Ads Info should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickInterestBasedAdsLink();
		homePage.switchToWindow();
		PrivacyPolicyPage privacyPolicyPage = new PrivacyPolicyPage(getDriver());
		privacyPolicyPage.verifyInterestBasedAds();
		Reporter.log("Interest based ads is displayed");

	}

	/**
	 * Verify Privacy Resources Link
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void verifyPrivacyResourcesLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyPrivacyResources method");
		Reporter.log("Test Case : Verify Privacy Resources Link redirects to Privacy Resources page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Privacy Resources link | Privacy Resources Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickPrivacyResourcesLink();
		homePage.switchToWindow();
		PrivacyResourcePage privacyResourcePage = new PrivacyResourcePage(getDriver());
		privacyResourcePage.verifyPrivacyResourcePage();
		Reporter.log("Privacy Resource page is displayed");
	}

	/**
	 * Verify Privacy Resources Link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void verifyOpenInternetPolicyLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyOpenInternetPolicyLink method");
		Reporter.log("Test Case : Verify Open Internet Policy Link redirects to Open Internet Policy page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Open Internet Policy link | Open Internet Policy Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickOpenInternetPolicyLink();
		homePage.switchToWindow();
		OpenInternetPolicyPage openInternetPolicyPage = new OpenInternetPolicyPage(getDriver());
		openInternetPolicyPage.verifyOpenInternetPrivacyPage();
		Reporter.log("Open Internet Policy is displayed");
	}

	/**
	 * Verify Privacy Resources Link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void verifyCopyrightsLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyCopyrightsLink method");
		Reporter.log("Test Case : Verify Copyrights Link redirects to Copyrights page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Copyrights link | Copyrights Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickCopyrightsLink();
		homePage.switchToWindow();
		CopyrightsPage copyrightsPage = new CopyrightsPage(getDriver());
		copyrightsPage.verifyCopyrightsPage();
		Reporter.log("Copyrights Page is displayed");
	}

	/**
	 * Verify Scheduled Call Back in Contact US
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyScheduledCallBackContactus(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyScheduledCallBackContactus method");
		Reporter.log("Test Case : Verify Scheduled Call Back Flow in Contact US page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Footer ContactUS Link |Contact US page should be displayed");
		Reporter.log("5: Click on call me link | Let's set up a call window should be displayed");
		Reporter.log("6: Click Schedule call back button |Select time slot window should be displayed");
		Reporter.log("7: Select Time and Click on Schedule call back button | Confirmation window should be displayed");
		Reporter.log("8: Click on Schedule Call Back button | Call should be placed");

		Reporter.log("==========================");
		Reporter.log(GlobalConstants.ACTUAL_TEST_STEPS);

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
		contactUSPage.clickCallMeBtn();
		if (contactUSPage.verifyAlreadyScheduledCallBackAndChange()) {
			contactUSPage.clickMakeAChangeButton();
		}
		Reporter.log("Let's set up a call window is displayed");
		contactUSPage.selectCategoryTopicForCall();
		contactUSPage.clickScheduledCallback();
		contactUSPage.verifyScheduledCallBackDiv();
		contactUSPage.selectScheduleDate();
		contactUSPage.selectScheduleTime();
		contactUSPage.selectScheduleZone();
		contactUSPage.clickFindATimeButton();
		contactUSPage.clickScheduleSlot();
		Reporter.log("Select time slot window is displayed");
		contactUSPage.clickScheduleCallConfirmButton();
		contactUSPage.verifyScheduledCallBackConfirmDiv();
		Reporter.log("Confirmation window is displayed");
		contactUSPage.clickScheduleCallDoneButton();
		Reporter.log("Call is placed");
	}

	/**
	 * Verify Call Me in Contact US
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyCallMeContactus(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyCallMeContactus method");
		Reporter.log("Test Case : Verify Call me Flow in Contact US page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Footer ContactUS Link |Contact US page should be displayed");
		Reporter.log("5: Click on call me link | Let's set up a call window should be displayed");
		Reporter.log("6: Enter call reason | Call Me should be enabled");
		if (!(getDriver() instanceof AppiumDriver)) {
			Reporter.log("7: click on Call Me button| Call confirmation window should be displayed");
			Reporter.log("8: Click on Done button | Call should be placed");
		}

		Reporter.log("==========================");
		Reporter.log(GlobalConstants.ACTUAL_TEST_STEPS);
		navigateToContactUsPage(myTmoData);
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
		contactUSPage.clickCallMeBtn();
		Reporter.log("Let's set up a call window is displayed");
		contactUSPage.selectCategoryTopicForCall();
		Reporter.log("Call me is enabled");
		if (!(getDriver() instanceof AppiumDriver)) {
			contactUSPage.clickCallMeNowBtn();
			contactUSPage.verifyConfirmCallMeDiv();
			Reporter.log("Call confirmation window is displayed");
			contactUSPage.clickCallMeDoneBtn();
			Reporter.log("Call is placed");
		}
	}

	/**
	 * US515001:Home Page | UI | Footer | Accessibility CTA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	private void verifyAccessbilityLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US515001:Home Page | UI | Footer | Accessibility CTA");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on accessbility link | Accessbility page should be displayed");

		Reporter.log("===========================");
		Reporter.log("Actual Results:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickAccessbilityLink();
		homePage.switchToWindow();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		AccessibilityPage accessibilityPage = new AccessibilityPage(getDriver());
		accessibilityPage.verifyAccessibilityPage();
	}

	/**
	 * US515001:Home Page | UI | Footer | Accessibility CTA US564136:Home Page |
	 * Accessibility testing | Footer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	private void verifyPublicSafetyLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US515001:Home Page | UI | Footer | Accessibility CTA");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch thePublic  application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on public safety link | Public safety page should be displayed");

		Reporter.log("===========================");
		Reporter.log("Actual Results:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickPublicSafetyLink();
		homePage.switchToWindow();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		PublicSafetyPage publicSafetyPage = new PublicSafetyPage(getDriver());
		publicSafetyPage.verifyPublicSafetyPage();
	}

}
