
package com.tmobile.eservices.qa.accounts.api.phone;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class UpdateSubscriberStatus {

	String environment;

	@Test(groups = "phne")
	public void updateSubscriberDetails() {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();

		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("interactionId", "123456787");
		request.header("workflowId", "1234");
		// request.header("suspensionCount", "7");
		// request.header("suspensionDuration", "364");
		request.header("sender_id", "MYTMO");
		request.header("application_id", "ESERVICE");
		request.header("channel_id", "WEB");
		request.contentType("application/json");

		JSONObject requestParams = new JSONObject();
		JSONObject deviceAccountDetails = new JSONObject();
		deviceAccountDetails.put("imei", "");
		deviceAccountDetails.put("accountNumber", "836025408");
		deviceAccountDetails.put("productType", "GSM");
		JSONArray array = new JSONArray();
		array.put("3344972797");
		deviceAccountDetails.put("msisdn", array);
		requestParams.put("deviceAccountDetails", deviceAccountDetails);

		JSONObject suspensionDetails = new JSONObject();
		suspensionDetails.put("isSuspension", "true");
		suspensionDetails.put("suspendDate", "2018-12-26");
		suspensionDetails.put("suspendReasonCode", "ELST");
		requestParams.put("suspensionDetails", suspensionDetails);

		JSONObject restorationDetails = new JSONObject();
		restorationDetails.put("isRestore", "true");
		restorationDetails.put("restoreDate", "2018-12-31");
		restorationDetails.put("restoreReasonCode", "ELST");
		requestParams.put("restorationDetails", restorationDetails);

		JSONObject deviceBlockInfo = new JSONObject();
		deviceBlockInfo.put("blockIndicator", "true");
		deviceBlockInfo.put("phoneMissingType", "ST");
		requestParams.put("deviceBlockInfo", deviceBlockInfo);

		System.out.println(requestParams.toString());

		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/updateSubscriberStatus");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating UpdateSubcriberStatus Operation Response ");
		Reporter.log("Note :  UpdateSubcriberStatus Service Internally calls getDeviceUnlockAttribute ");
		Reporter.log("Test data : Account eligible for suspension");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("status").contains("SUCCESS"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

}
