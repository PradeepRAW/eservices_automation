package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.PrivacyResourcePage;

public class PrivacyResourcePageTest extends GlobalCommonLib{

	/**
	 * Verify Privacy Resources Link
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true,groups= {Group.PENDING})
	public void verifyPrivacyResourcesLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyPrivacyResources method");
		Reporter.log("Test Case : Verify Privacy Resources Link redirects to Privacy Resources page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Privacy Resources link | Privacy Resources Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage=navigateToNewHomePage(myTmoData);
		homePage.clickPrivacyResourcesLink();
		homePage.switchToWindow();
		
		PrivacyResourcePage privacyResourcePage = new PrivacyResourcePage(getDriver());
		privacyResourcePage.verifyPrivacyResourcePage();
	}
}
