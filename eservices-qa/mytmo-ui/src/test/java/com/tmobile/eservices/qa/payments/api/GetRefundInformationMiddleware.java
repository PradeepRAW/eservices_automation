package com.tmobile.eservices.qa.payments.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.payments.api.AccountActivitiesMiddleware;

import groovy.json.StringEscapeUtils;
import io.restassured.response.Response;

public class GetRefundInformationMiddleware {

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test
	public void getRefundinformationmware() throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String query = "$.[*].ban";
		List<String> bans = com.jayway.jsonpath.JsonPath.parse(new URL(Constants.TEST_DATA_FILTER_LOCATION_PROD))
				.read(query);
		// List<String> bans = new ArrayList<String>();
		// bans.add("966655994");

		Set<String> setbans = new HashSet<>(bans);

		LocalDate a = LocalDate.now();
		String fromdate = "2019-01-01";
		String todate = a.toString();
		String fileName = System.getProperty("user.dir")
				+ "/src/test/resources/reqtemplates/PaymentHistorymiddleware.txt";
		String requestBody = new String(Files.readAllBytes(Paths.get(fileName)));
		// String requestBody = new
		// ServiceTest().getRequestFromFile("PaymentHistorymiddleware.txt");
		Map tokenMap = new HashMap<String, String>();
		tokenMap.put("startdate", todate);
		tokenMap.put("enddate", fromdate);
		AccountActivitiesMiddleware mwactivity = new AccountActivitiesMiddleware();

		Map<String, Object[]> data = new TreeMap<String, Object[]>();
		int i = 1;
		data.put(Integer.toString(i),
				new Object[] { "Ban", "Line", "PaymentID", "CardNO", "PaymentDate", "PaymentAmount", "Comments" });

		for (String ban : setbans) {

			tokenMap.put("ban", ban);
			String updatedRequest = prepareRequestParamXML(requestBody, tokenMap);

			Response Accountsummaryresponse = mwactivity.getResponsePaymentHistory(updatedRequest);

			if (Accountsummaryresponse.body().asString() != null && Accountsummaryresponse.getStatusCode() == 200) {

				DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
				DocumentBuilder db = dbf.newDocumentBuilder();
				InputSource is = new InputSource(new StringReader(Accountsummaryresponse.asString()));

				Document doc = db.parse(is);
				doc.getDocumentElement().normalize();

				// XPath xpath = XPathFactory.newInstance().newXPath();

				NodeList nodeList = doc.getElementsByTagName("svo:payment");
				Map<String, String> paymentheader = new HashMap<String, String>();
				Map<String, String> receipt = new HashMap<String, String>();
				Map<String, String> card = new HashMap<String, String>();
				String postdate;

				for (int nde = 0; nde < nodeList.getLength(); nde++) {

					Node subnde = nodeList.item(nde);
					NodeList childnods = subnde.getChildNodes();
					paymentheader = null;
					receipt = null;
					card = null;
					postdate = null;
					for (int cnde = 0; cnde < childnods.getLength(); cnde++) {

						if (childnods.item(cnde).getNodeName().equalsIgnoreCase("ns:paymentHeader")) {
							String[] tags = { "ns:paymentMethod", "ns:amount" };
							paymentheader = getvaluesfromnode(tags, childnods.item(cnde));

						}

						if (childnods.item(cnde).getNodeName().equalsIgnoreCase("ns:receipt")) {
							String[] tags = { "ns:id", "ns:originalAccountNumber", "ns:MSISDN" };
							receipt = getvaluesfromnode(tags, childnods.item(cnde));
						}

						if (childnods.item(cnde).getNodeName().equalsIgnoreCase("ns:card")) {
							String[] tags = { "ns:cardNumber" };
							card = getvaluesfromnode(tags, childnods.item(cnde));
						}

						if (childnods.item(cnde).getNodeName().equalsIgnoreCase("ns:postingDate")) {
							postdate = childnods.item(cnde).getTextContent().trim();
						}
					}
					if (paymentheader.containsKey("ns:paymentMethod")) {
						if (!paymentheader.get("ns:paymentMethod").trim().equalsIgnoreCase("CHECK")) {
							i++;
							data.put(Integer.toString(i),
									new Object[] { receipt.get("ns:originalAccountNumber"), receipt.get("ns:MSISDN"),
											receipt.get("ns:id"), card.get("ns:cardNumber"), postdate,
											paymentheader.get("ns:amount"), "" });
						}
					}

				}

			} else {
				i++;
				data.put(Integer.toString(i), new Object[] { ban, "", "", "", "", "", "500-servernot found" });
			}

		}
		writetoExcel(data);
	}

	public Map<String, String> getvaluesfromnode(String[] tags, Node parentnode) {
		Map<String, String> vals = new HashMap<String, String>();
		NodeList receiptlist = parentnode.getChildNodes();
		for (int nde = 0; nde < receiptlist.getLength(); nde++) {
			for (String tag : tags) {
				if (receiptlist.item(nde).getNodeName().trim().equalsIgnoreCase(tag)) {
					vals.put(receiptlist.item(nde).getNodeName().trim(), receiptlist.item(nde).getTextContent().trim());
				}

			}
		}
		return vals;
	}

	public void writetoExcel(Map<String, Object[]> data) {

		// Blank workbook
		XSSFWorkbook workbook = new XSSFWorkbook();

		// Create a blank sheet
		XSSFSheet sheet = workbook.createSheet("Refund data");

		// This data needs to be written (Object[])

		// Iterate over data and write to sheet
		Set<String> keyset = data.keySet();
		int rownum = 0;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object[] objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof String)
					cell.setCellValue((String) obj);
				else if (obj instanceof Integer)
					cell.setCellValue((Integer) obj);
			}
		}
		try {
			String file1 = System.getProperty("user.dir") + "/src/test/resources/testdata/refund.xlsx";
			// Write the workbook in file system
			FileOutputStream out = new FileOutputStream(new File(file1));
			workbook.write(out);
			out.close();
			System.out.println("howtodoinjava_demo.xlsx written successfully on disk.");
			workbook.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings({ "rawtypes" })
	public String prepareRequestParamXML(String requestParam, Map<String, String> tokenMap) {
		String replacedRequestParam = requestParam;
		if (requestParam != null) {
			if (tokenMap != null) {
				Iterator it = tokenMap.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();
					String key = pair.getKey().toString();
					String depVal = pair.getValue().toString();
					depVal = StringEscapeUtils.escapeJava(depVal);
					String frameworkKey = key + "_frkey";
					// depVal = "\"" + depVal + "\"";
					// if(requestParam.contains(key+"_frkey")) {
					if (replacedRequestParam.contains(frameworkKey)) {
						replacedRequestParam = replacedRequestParam.replace(frameworkKey, depVal);
					}
				}
			}
		}
		return replacedRequestParam;
	}

}