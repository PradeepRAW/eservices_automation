package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.EOSCommonMethods;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EOSAccountsV3Filters;
import com.tmobile.eservices.qa.pages.payments.api.EOSencription;
import com.tmobile.eservices.qa.pages.payments.api.EOSservicequotefilter;
import com.tmobile.eservices.qa.pages.payments.api.EOSv3OTP;

import io.restassured.response.Response;

public class EOSNonsedonaOTPV3Test extends EOSCommonMethods{
	
	public Map<String, String> tokenMap;
	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true,priority = 1,  groups = { "otpv3" })
	public void checkotpnewbank(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		EOSv3OTP otp= new EOSv3OTP();
		EOSAccountsV3Filters accounts=new EOSAccountsV3Filters();
		EOSencription encri=new EOSencription();
		EOSservicequotefilter servicequote=new EOSservicequotefilter();
		Map <Object,Object>optpara=new HashMap<Object,Object>();
		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);
		
		if (getjwt != null) {
			Response publickeyresponse=accounts.getResponsePublickKey(getjwt);
			String publickey=accounts.getpublicKey(publickeyresponse);
			//publickey="jbMOmMZkNN0hrVGuQZixC7lSS27pkTc2H3hrOngdSGBsmX12ai4wXTqOSU8btYlr4AtA11nJelW4znKjNB2-MBu8WsB05Gyv2lpcFWATRZFepAEKfIsK27vMspZGrJCyiIaTdf9fBlawf0pce8jSU_mnv_rcUP3wysLZOFpOGavlv0ATZM2XW6ONwGUPS4Hz9jMLlxdE6zrA_H2MhP2g-B6vjJN6uoCEkOS2Yl3n9solAiRK3wW6zo-3FfC5VsMTMaUAaxbHQj44plduyL18LuifRbxIr3GQQoP1fxa9uVbu18RQ-PgxYGIU2a7jaLdrHQ2cfUctM9vEjtnHuA1V3Q";
			String encrypts = encri.RsaEncryption("RSA","AQAB",publickey,"345678934");
			Response servicequoteresponse=servicequote.getResponsesedonaquote(getjwt);
			String quote=servicequote.getquoteid(servicequoteresponse);
			optpara.put("quoteId", quote);
			optpara.put("accountNumber", encrypts);
			optpara.put("routingNumber", "021000021");
			optpara.put("addType", "Checking");
			optpara.put("isSave", true);
			optpara.put("chargeAmount", Math.round(generateRandomAmount()* 100.0) / 100.0);
			optpara.put("isSedona", true);
			Response response = otp.getResponsenewbanknonsedonaotp(getjwt, optpara);
			System.out.println("sreeee");
	}
	
}
}