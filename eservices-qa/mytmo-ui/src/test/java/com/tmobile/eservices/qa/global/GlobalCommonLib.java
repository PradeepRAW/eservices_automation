package com.tmobile.eservices.qa.global;

import com.tmobile.eservices.qa.pages.accounts.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.commonlib.CommonLibrary;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.MultipleDevicesPage;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.ChooseMethodOptionsPage;
import com.tmobile.eservices.qa.pages.global.FamilyModePage;
import com.tmobile.eservices.qa.pages.global.ForgotPasswordPage;
import com.tmobile.eservices.qa.pages.global.LanguageSettingsPage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.global.NewContactUSPage;
import com.tmobile.eservices.qa.pages.global.PhonePage;

import io.appium.java_client.ios.IOSDriver;

public class GlobalCommonLib extends CommonLibrary {

	/**
	 * Navigate to privacy and policy page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public GlobalCommonLib navigateToPrivacyAndPolicyPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickPrivacyPolicyLink();
		return this;
	}

	/**
	 * Navigate to interest based ads page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public GlobalCommonLib navigateToInterestBasedAdsPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickInterestBasedAdsLink();
		return this;
	}

	/**
	 * Navigate to terms and conditions page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public GlobalCommonLib navigateToTermsAndConditionsPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickTermsAndConditionsLink();
		return this;
	}

	/**
	 * Navigate to coverage page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public GlobalCommonLib navigateToCoveragePage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickCoverageLink();
		return this;
	}

	/**
	 * Navigate to return policy page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public GlobalCommonLib navigateToReturnPolicyPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickCloudReturnPolicyLink();
		return this;
	}

	/**
	 * Navigate to support page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public GlobalCommonLib navigateToSupportPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickFooterSupportlink();
		return this;
	}

	/**
	 * Navigate to store locator page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public GlobalCommonLib navigateToStoreLocatorPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickFooterStoreLocatorlink();
		return this;
	}

	/**
	 * Navigate to t-mobile page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public GlobalCommonLib navigateToTmobilePage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickTMobileLink();
		return this;
	}

	protected ChooseMethodOptionsPage loginAndNavigateToMethodOptions(MyTmoData myTmoData) {
		getTMOURL(myTmoData);
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.clickForgotPassword();
		ForgotPasswordPage forgotPasswordPage = new ForgotPasswordPage(getDriver());
		forgotPasswordPage.getResetPasswordText();
		Reporter.log("Forgot Password Page is diplayed");
		forgotPasswordPage.enterEmailOrPhone(myTmoData.getLoginEmailOrPhone());
		forgotPasswordPage.clickNextButton();
		forgotPasswordPage.enterZipCode(myTmoData.getPayment().getZipCode());
		Reporter.log("User is able to enter Zip code");
		forgotPasswordPage.clickNextButton();
		ChooseMethodOptionsPage methodOptionsPage = new ChooseMethodOptionsPage(getDriver());
		methodOptionsPage.getChooseMethodText();
		Reporter.log("Choose Method options Screen is displayed");
		return methodOptionsPage;
	}

	/**
	 * Navigate to employee line designation page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public EmployeeLineDesignationPage navigateToEmployeeLineDesignationPage(MyTmoData myTmoData, String linkName) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink(linkName);

		EmployeeLineDesignationPage employeeLineDesignationPage = new EmployeeLineDesignationPage(getDriver());
		employeeLineDesignationPage.verifyEmployeeLineDesignationPage();
		return employeeLineDesignationPage;
	}

	public void loginToProfilePageAndClickOnLink(MyTmoData myTmoData, String linkName) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink(linkName);
	}

	/**
	 * Navigate to family mode page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public FamilyModePage navigateToMFamilyModePage(MyTmoData myTmoData, String linkName) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink(linkName);

		FamilyModePage familyModePage = new FamilyModePage(getDriver());
		familyModePage.verifyFamilyModePage();
		return familyModePage;
	}

	/**
	 * Navigate to profile page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public GlobalCommonLib navigateToProfilesPage(MyTmoData myTmoData) {
		getDriver().get(System.getProperty("environment"));
		return this;

	}

	/**
	 * Navigate to Login page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public GlobalCommonLib navigateToLoginPage(MyTmoData myTmoData) {
		unlockAccount(myTmoData);
		SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
		Reporter.log("Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/"
				+ sessionId + "\">SauceLabs</a>");
		getDriver().get(System.getProperty("environment"));
		return this;

	}

	/**
	 * Navigate to family controls page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public FamilyControlsPage navigateToFamilyControlsPage(MyTmoData myTmoData, String linkName) {
			ProfilePage profilePage = navigateToProfilePage(myTmoData);
			profilePage.clickProfilePageLink(linkName);
			FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
			familyControlsPage.verifyFamilyControlsPage();
		return familyControlsPage;
	}

	/**
	 * Navigate to Language Settings
	 * 
	 * @param myTmoData
	 * @return
	 */
	public LanguageSettingsPage navigateToLanguageSettingsPage(MyTmoData myTmoData, String linkName) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink(linkName);

		LanguageSettingsPage languageSettingsPage = new LanguageSettingsPage(getDriver());
		languageSettingsPage.verifyLanguageSettingsPage();
		return languageSettingsPage;
	}

	/**
	 * Navigate to contactus page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public NewContactUSPage navigateToContactUsPage(MyTmoData myTmoData) {
		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickContactUSlink();

		NewContactUSPage newContactUSPage = new NewContactUSPage(getDriver());
		newContactUSPage.verifyContactUSPage();
		return newContactUSPage;
	}

	/**
	 * Navigate to media settings page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public MediaSettingsPage navigateToMediaSettingsPage(MyTmoData myTmoData, String linkName) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink(linkName);

		MediaSettingsPage mediaSettingsPage = new MediaSettingsPage(getDriver());
		mediaSettingsPage.verifyMediaSettingsPage();
		return mediaSettingsPage;
	}

	/**
	 * Navigate to line settings page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public LineSettingsPage navigateToLineSettingsPage(MyTmoData myTmoData, String linkName) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink(linkName);

		LineSettingsPage lineSettingsPage = new LineSettingsPage(getDriver());
		lineSettingsPage.verifyLineSettingsPage();
		return lineSettingsPage;
	}

	/**
	 * Navigate to coverage device page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public CoverageDevicePage navigateToCoverageDevicePage(MyTmoData myTmoData, String linkName) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink(linkName);

		CoverageDevicePage coverageDevicePage = new CoverageDevicePage(getDriver());
		coverageDevicePage.verifyCoverageDevicePage();
		return coverageDevicePage;
	}

	/**
	 * Navigate to multiple devices page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public MultipleDevicesPage navigateToMultipleDevicesPage(MyTmoData myTmoData, String linkName) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink(linkName);

		MultipleDevicesPage multipleDevicesPage = new MultipleDevicesPage(getDriver());
		multipleDevicesPage.verifyMultipleDevicesPage();
		return multipleDevicesPage;
	}

	/**
	 * Navigate to TMobileID page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public TMobileIDPage navigateToTMobileIDPage(MyTmoData myTmoData, String linkName) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink(linkName);

		TMobileIDPage tmobileIDPage = new TMobileIDPage(getDriver());
		tmobileIDPage.verifyTMobileIDPage();
		return tmobileIDPage;
	}

	/**
	 * Navigate to blocking page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public BlockingPage navigateToBlockingPage(MyTmoData myTmoData, String linkName) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink(linkName);

		BlockingPage blockingPage = new BlockingPage(getDriver());
		blockingPage.verifyBlockingPage();
		return blockingPage;
	}

	/**
	 * Navigate to billing and payments page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public BillingAndPaymentsPage navigateToBillingAndPaymentsPage(MyTmoData myTmoData, String linkName) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink(linkName);

		BillingAndPaymentsPage billingAndPaymentsPage = new BillingAndPaymentsPage(getDriver());
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		return billingAndPaymentsPage;
	}

	/**
	 * Navigate to Phone Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public PhonePage navigateToPhonePage(MyTmoData myTmoData) {
		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickPhoneMenu();

		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		return phonePage;
	}


	/**
	 * This will navigate to bookmark url
	 * 
	 * @return
	 */
	public GlobalCommonLib navigateToBookMarkedDashBoardURL(String bookmarkURL) {
		try {
			getDriver().navigate().to(bookmarkURL);
		} catch (Exception e) {
			Assert.fail("Unable to navigate to bookmark url");
		}
		return this;
	}

	public void closeNewWindow() {
		if (!(getDriver() instanceof IOSDriver)) {
			String currentWindow = getDriver().getWindowHandle();
			for (String winHandle : getDriver().getWindowHandles()) {
				if (winHandle.equalsIgnoreCase(currentWindow)) {
					getDriver().switchTo().window(winHandle).close();
				}
			}
		}
	}
	
	/**
	 * Login To MyTmo
	 * 
	 * @param myTmoData
	 */
	public GlobalCommonLib launchAndPerformLoginForChooseAccountPage(MyTmoData myTmoData) {
		String misdinpwd = getmisdinpaswordfromfilter(myTmoData);
		if (misdinpwd == null) {
			Assert.fail("No valid data identified");
		}
		String misdin = misdinpwd.split("/")[0];
		String pwd = misdinpwd.split("/")[1];
		myTmoData.setLoginEmailOrPhone(misdin);
		myTmoData.setLoginPassword(pwd);

		SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
		Reporter.log(
				"Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/" + sessionId
						+ "\">SauceLabs</a>|<a target=\"_blank\" href=\"http://prdplctep000d.unix.gsm1900.org/videos/"
						+ sessionId + ".mp4" + "\"> Sbox</a>");
		LoginPage loginPage = new LoginPage(getDriver());
		launchSite(myTmoData, loginPage);
		performLogin(myTmoData, loginPage);
		loginPermission();
		return this;
	}

}
