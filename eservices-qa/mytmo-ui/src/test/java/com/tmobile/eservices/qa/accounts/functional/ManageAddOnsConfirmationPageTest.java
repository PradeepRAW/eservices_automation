package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsConfirmationPage;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsSelectionPage;
import com.tmobile.eservices.qa.pages.accounts.ODFDataPassReviewPage;

public class ManageAddOnsConfirmationPageTest extends AccountsCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(ManageAddOnsConfirmationPageTest.class);

	/**
	 * US263121#Review Add-Ons - Effective date for add on data plan - Downgrade
	 * This test case for E2E Downgrade Data plan flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyAddOnDataPlanDowngradeE2EFlowFunctionality(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnDataPlanDowngradeE2EFlowFunctionality method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case  Name : Verify downgrade data plan");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"6. Read Currently Active Data Plan name and Price | User should be able to read Data plan name and Price");
		Reporter.log(
				"7. Choose data plan (here we have to choose lower data plan than currently active data plan). Read Name and price | User should be able to select data plan");

		Reporter.log("8. On Review page, Click on 'Agree & Submit' button. | Confirmation page should be displayed.");
		Reporter.log(
				"9. On confirmation page Check Thanks message, username. | On Confirmation page message should be 'Thanks Username(username should be logged in username)'. Below that message should be 'Your order is complete.'");
		Reporter.log(
				"10. On confirmation page, check 'Order details' blade. | Order Details blade should be displayed with downward carat");
		Reporter.log(
				"11. On confirmation page, check 'Return Home' CTA and Legal text below that. | 'Return Home' CTA should be displayed and Legal text should be displayed below that.(Compare legal text, it should match)");
		Reporter.log(
				"12. On confirmation page, click on 'Order details' carat and check details. | Order Details blade should be expanded and it should show 'New Add-ons' Items and 'Remaved Items'. (New Add-Ons items include SOC which added in Step7, check name and Price. Removed Items should show SOC which is removed, check name and Price from Step6");
		Reporter.log(
				"13. On confirmation page, click on 'Return Home' CTA. | User should be redirected to Plans landing page(plans.html)");
		Reporter.log("Actual Result:");
		Reporter.log("===================");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		String activePlan = manageAddOnsSelectionPage.getActiveDataPlan();
		String activeDataPlanPrice = manageAddOnsSelectionPage.getPriceOfDataPlan(activePlan);
		manageAddOnsSelectionPage.selectDataPlan(activePlan);
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyRemovedItemsNames(activePlan);
		oDFDataPassReviewPage.verifyPriceText(activeDataPlanPrice);

		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		newODFConfirmationPage.verifyThanksHeaderOnConfirmationPage();
		newODFConfirmationPage.verifyOrderDetailsBladeDisplayedwithDownwarCarat();
		newODFConfirmationPage.clickOnOrderDetailsBlade();

		newODFConfirmationPage.clickOnReturnHome();

	}

	/**
	 * US270325#Date logic - Effective date options - Data plan upgrade This is for
	 * E2E upgrade flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyAddOnDataPlanUpgradeE2EFlowFunctionality(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAddOnDataPlanUpgradeE2EFlowFunctionality method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case  Name : Verify Upgrade data plan functionality");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"6. Read Currently Active Data Plan name and Price | User should be able to read Data plan name and Price");
		Reporter.log(
				"7. Choose data plan (here we have to choose higher data plan than currently active data plan) | User should be able to select data plan");
		Reporter.log(
				"8. Read Name and price of selected data plan and go to Review Page | User should be able to select data plan and redirect to Review page");
		Reporter.log(
				"9. Calcuate price difference between currently selected and active data plans (it will always be a +ve value). If currently active data plan show Free then treat this value as 0. | User should be able to calculate difference");

		Reporter.log("10. On Review page, Click on Agree & Submit button. | Confirmation page should be displayed.");
		Reporter.log(
				"11. On confirmation page Check Thanks message, username. | On Confirmation page message should be Thanks Username(username should be logged in username).Below that message should be Your order is complete.");
		Reporter.log(
				"12. On confirmation page, check Order details blade. | Order Details blade should be displayed with downward carat");
		Reporter.log(
				"13. On confirmation page, check Return Home CTA and Legal text below that. | Return Home CTA should be displayed and Legal text should be displayed below that.(Compare legal text, it should match)");
		Reporter.log(
				"14. On confirmation page, click on Order details carat and check details. | Order Details blade should be expanded and it should show New Add-ons Items and Removed Items. (New Add-Ons items include SOC which added in Step7, check name and Price. Removed Items should show SOC which is removed from Step6, check name and Price");
		Reporter.log(
				"15. On confirmation page, click on Return Home CTA. | User should be redirected to Plans landing page(plans.html)");
		Reporter.log("Actual Result:");
		Reporter.log("===================");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		String activePlan = manageAddOnsSelectionPage.getActiveDataPlan();
		String activeDataPlanPrice = manageAddOnsSelectionPage.getPriceOfDataPlan(activePlan);
		manageAddOnsSelectionPage.selectDataPlan(activePlan);
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyRemovedItemsNames(activePlan);
		oDFDataPassReviewPage.verifyPriceText(activeDataPlanPrice);

		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		newODFConfirmationPage.verifyThanksHeaderOnConfirmationPage();
		newODFConfirmationPage.verifyOrderDetailsBladeDisplayedwithDownwarCarat();
		newODFConfirmationPage.clickOnOrderDetailsBlade();
		newODFConfirmationPage.clickOnReturnHome();

	}

	/**
	 * US290446#Confirm Add Ons - Submit services and data plan changes
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyAddOnConfirmPageForDataPlanAndServices(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAddOnConfirmPageForDataPlanAndServices method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case Name : Verify ODF review page Cancel button for services");
		Reporter.log("Test Data :  Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"7. Choose Any data plan from Data Plan section. Read Data plan name and price. | Should be able to read the name and price.");
		Reporter.log(
				"8. Choose Any Service from service section. Read service name and price. | Should be able to read the name and price.");
		Reporter.log("9. Click on Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"10. Check Details in AddOn and Removed Items section | Should be able to check AddOn and Removed item section.");
		Reporter.log("11. Click on Agree And Submit CTA | Confirmation page should be displayed.");
		Reporter.log(
				"12. Verify added Data plan and Service. Also check removed data plan. | It should be matched with step6 and Step7");
		Reporter.log("13. Click on BackArrow on Confirmation page. | Back Arrow should be in disabled state.");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		String activePlan = manageAddOnsSelectionPage.getActiveDataPlan();
		String activeDataPlanPrice = manageAddOnsSelectionPage.getPriceOfDataPlan(activePlan);
		manageAddOnsSelectionPage.selectDataPlanODFPage();
		manageAddOnsSelectionPage.clickOnDecreaseService();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyRemovedItemsNames(activePlan);
		oDFDataPassReviewPage.verifyPriceText(activeDataPlanPrice);

		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		newODFConfirmationPage.verifyThanksHeaderOnConfirmationPage();
		newODFConfirmationPage.verifyBackArrowIsDisabledOnConfirmationPage();

	}

	/**
	 * US290456 # Confirm Add Ons - Submit data pass, services and data plan changes
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyAddOnConfirmPageForDataPlanServicesAndDataPass(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnConfirmPageForDataPlanServicesAndDataPass method called in NewConfirmationPageForDP_DS_DP_OD");
		Reporter.log(
				"Test Case Name : Verify ODF Confirmation page when user submits AddOns for Data Plan, Service and Data pass together.");
		Reporter.log("Test Data :  Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"6. Choose Any data plan from Data Plan section. Read Data plan name and price. | Should be able to read the name and price.");
		Reporter.log(
				"7. Choose Any Data Pass from data Pass section. Read service name and price. | Should be able to read the name and price.");
		Reporter.log(
				"8. Choose Any Service from service section. Read service name and price. | Should be able to read the name and price.");
		Reporter.log("9. Click on Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"10. Check Details in AddOn and Removed Items section | Should be able to check AddOn and Removed item section.");
		Reporter.log("11. Click on Agree And Submit CTA | Confirmation page should be displayed.");
		Reporter.log(
				"12. Verify added Data plan,Service and Pass. Also check removed data plan. | It should be matched with step6 and Step7 and Step8");
		Reporter.log("13. Click on BackArrow on Confirmation page. | Back Arrow should be in disabled state.");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		String activePlan = manageAddOnsSelectionPage.getActiveDataPlan();
		String activeDataPlanPrice = manageAddOnsSelectionPage.getPriceOfDataPlan(activePlan);
		manageAddOnsSelectionPage.selectDataPlanODFPage();
		manageAddOnsSelectionPage.clickOnDataPassRadioButton("International Data 10 Day - 1 GB");
		manageAddOnsSelectionPage.clickOnDecreaseService();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyRemovedItemsNames(activePlan);
		oDFDataPassReviewPage.verifyPriceText(activeDataPlanPrice);

		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		newODFConfirmationPage.verifyThanksHeaderOnConfirmationPage();
		newODFConfirmationPage.verifyBackArrowIsDisabledOnConfirmationPage();
	}

	/**
	 * US303756- Confirmation page - Display Netflix sign up CTA or message
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyNetflixMessageOnConfirmationPageWhenUserFirstTimeSignupForNetflix(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("verifyPromoMessageForDataPlan method called in ODFReviewPageTest");
		Reporter.log(
				"Test Case  Name : Verify Netflix message on confirmation page when user first time signup for Netflix.");
		Reporter.log("Test Data : Any PAH/Full User with TMOONETI plan ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"6. Select Netflix soc and read the value and price. | User should be able to select and read the name.");
		Reporter.log("7. Conflict popup should come up. Click Continue. | Conflict modal should disappear.");
		Reporter.log("8. Click on Continue Button and Go to Review page. | Review page should be displayed.");
		Reporter.log(
				"9. Check details on Reviw page. Click on Agree & Submit CTA. | Details should be matched. Confirmation page should be displayed.");
		Reporter.log("10. Check message for Netflix. | Message " + "Your account's now eligible for Netflix"
				+ " should be displayed");
		Reporter.log(
				"11. Check CTA to signup for Netflix. | CTA " + "Sign up for Netflix on us" + "should be displayed.");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnCheckBox("Netflix Premium $13.99 with Fam Allowances ($19 value)");
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.clickOnContinueBtn();

		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		newODFConfirmationPage.verifyNetFilxEligibleMessage("Your account's now eligible for Netflix");
		newODFConfirmationPage.verifySignUpNextFlixBtnDisplayed();
		// adding below code to make sure Netflix soc is removed (to make sure
		// same data working for multiple runs)
		navigateToODFUrlFromAnyPage("plan-confirmation");
		verifyManageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnCheckBox("Netflix Premium $13.99 with Fam Allowances ($19 value)");
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.clickOnContinueBtn();

		verifyODFDataPassReviewPage();

		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		newODFConfirmationPage.verifyODFConfirmationPage();

	}

	/**
	 * US303756- Confirmation page - Display Netflix sign up CTA or message
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyNetflixMessageOnConfirmationPageWhenUserMadeChangesToActiveNetflixSocButNotRegistered(
			ControlTestData data,

			MyTmoData myTmoData) {
		logger.info(
				"verifyNetflixMessageOnConfirmationPageWhenUserMadeChangesToActiveNetflixSocButNotRegistered method called in ODFReviewPageTest");
		Reporter.log(
				"Test Case  Name : Verify Netflix message on confirmation page when user made any changes to active Netflix soc. But user has not yet registered for Netflix.");
		Reporter.log("Test Data : Any PAH/Full User with any Plan. Netflix soc active on account.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"6. Select other Netflix soc or made changes to active netflix soc. | User should be able to select and read the name.");
		Reporter.log("7. Conflict popup should come up. Click Continue. | Conflict modal should disappear.");
		Reporter.log("8. Click on Continue Button and Go to Review page. | Review page should be displayed.");
		Reporter.log(
				"9. Check details on Reviw page. Click on Agree & Submit CTA. | Details should be matched. Confirmation page should be displayed.");
		Reporter.log("10. Check message for Netflix. | Message " + "Your account's now eligible for Netflix"
				+ " should be displayed");
		Reporter.log(
				"11. Check CTA to signup for Netflix. | CTA " + "Sign up for Netflix on us" + "should be displayed.");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.modifyActiveNetFlixSOC("Netflix Premium $13.99 with Fam Allowances ($19 value)",
				"Family Allowances");
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.clickOnContinueBtn();

		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		newODFConfirmationPage.verifyNetFilxEligibleMessage("Your account's now eligible for Netflix");
		newODFConfirmationPage.verifySignUpNextFlixBtnDisplayed();
	}

	/**
	 * US204208 - [Continued] Bearmode - Show App Section on the ODF Confirmation
	 * Page (D, M) US283531 - [Continued] Bearmode - Show Accessory Section on the
	 * ODF Confirmation Page (D, M) US306449 - Bearmode - Plan landing Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyBearModeFamilymode(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBearModeFamilymode method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case name : verify Bear Mode Family mode");

		Reporter.log("Test Data : Any PAH Customer with Family Mode Plans ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Clcik on Plan Page | Plan Landing page should be displayed");
		Reporter.log(
				"5. Clcik on Plus Symbol on Family allowance under the Account section | Family allowance information should be displayed with App Store Badges");
		Reporter.log("6. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"7. Click on Family Mode plan under Family and Entertainment | Family mode confirmation window should be displayed");
		Reporter.log("8. Click on Add Service button on the confirmation window | Family Mode paln should be sleceted");
		Reporter.log("9. Click on Continue button | Plan Review page should be displayed.");
		Reporter.log("10. Click on Agree & Submit button | Plan Confirmation page should be displayed");
		Reporter.log(
				"11. Verify that App Store Badegs below the authorable content | App Store Bages should be displayed below the authorable content");
		Reporter.log(
				"12. Verify the 'Contact US ' CTA |'Contact US ' CTA should be displayed below the authorable content");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		String newPrice = manageAddOnsSelectionPage.getCheckBoxPrice("Family Mode");
		manageAddOnsSelectionPage.clickOnCheckBoxByName("Family Mode");
		manageAddOnsSelectionPage.clickOnAddRemoveService();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyAddedItemsText();
		Assert.assertTrue(oDFDataPassReviewPage.verifyServiceTextByPrice("Family Mode").contains(newPrice),
				"Family Mode price is displaying wrong");
		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		newODFConfirmationPage.googlePlayIcon();
		newODFConfirmationPage.appStoreIcon();
		newODFConfirmationPage.contactUsButton();
		newODFConfirmationPage.clickOnReturnHome();

	}

	/**
	 * US263121#Review Add-Ons - Effective date for add on data plan - Downgrade
	 * This test case for E2E Downgrade Data plan flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyAddOnDataPassE2EFlowFunctionality(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnDataPlanDowngradeE2EFlowFunctionality method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case  Name : Verify downgrade data plan");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log("6. Choose a data pass and click on Review | Review page with Data pass details should be loaded");
		Reporter.log(
				"7. verify Data pass details and price in Review Page | Details of Data Pass in Review page is Verified");
		Reporter.log("8. On Review page, Click on 'Agree & Submit' button. | Confirmation page should be displayed.");
		Reporter.log(
				"9. On confirmation page verify thanks Message Header | On Confirmation page message should be displayed");
		Reporter.log(
				"10. On confirmation page, check 'Order details' blade. | Order Details blade should be displayed with downward carat");
		Reporter.log(
				"11. On confirmation page, check 'Return Home' CTA and Legal text below that. | 'Return Home' CTA should be displayed and Legal text should be displayed below that.(Compare legal text, it should match)");
		Reporter.log(
				"12. On confirmation page, click on 'Order details' carat and check details. | Order Details blade should be expanded and it should show 'New Add-ons' Items and 'Remaved Items'. (New Add-Ons items include SOC which added in Step7, check name and Price. Removed Items should show SOC which is removed, check name and Price from Step6");
		Reporter.log(
				"13. On confirmation page, click on 'Return Home' CTA. | User should be redirected to Plans landing page(plans.html)");
		Reporter.log("Actual Result:");
		Reporter.log("===================");

		// navigateToODFDataPassReviewPage(myTmoData, "International Data 10 Day
		// - 1 GB");
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		newODFConfirmationPage.verifyThanksHeaderOnConfirmationPage();
		newODFConfirmationPage.verifyOrderDetailsBladeDisplayedwithDownwarCarat();
		newODFConfirmationPage.clickOnOrderDetailsBlade();
		newODFConfirmationPage.clickOnReturnHome();

	}

	// Test Netflix Premium Soc end to end for Registered Customer
	// *************************

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")

	public void testAddAndRemovePremiumNetflixSocforAlreadyRegisteredCustomer(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyServiceReviewPageAndConfirmationPagePremiumNetflixSoc method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case : Test Adding and Removing Netflix Premium SOC.");
		Reporter.log("Test Data : TMO TI Pooled PAH Msisdn Registered for Netflix with Premium");
		Reporter.log("==============================");
		Reporter.log("Verify Service Review page premium Netflix soc");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click On plan icon | Plan Page should be displayed");
		Reporter.log("4. Click on Change Services btn | Services List Page should be verified");
		Reporter.log("5. Verify FA Soc  | FA soc should displayed");
		Reporter.log(
				"6. Select Netflix Prem. $13.99 with Fam Allowance ($19 value) Soc  | Netflix Prem. $13.99 with Fam Allowance ($19 value) should be selected");
		Reporter.log("7. Click On Next button  | Services Review Page should be displayed");
		Reporter.log(
				"8. Verify Added FA soc  | Netflix Prem. $13.99 with Fam Allowance ($19 value) should be displayed in Review page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		if (manageAddOnsSelectionPage.verifyNetFlixPremiumSocCheckBoxisSelected() == false) {
			addNetFLixPremiumSoc(myTmoData);
		}

		manageAddOnsSelectionPage.checkNetFlixPremiumSocCheckBox();
		manageAddOnsSelectionPage.verifyRemovingPremiumSocDescription();
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.clickOnContinueBtn();

		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		// need to verify if any messages related RemovingPremiumNetflixSoc on
		// odf confirmation page

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void testAddAndRemoveNetFlixFamilySocforAlreadyRegisteredCustomer(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyServiceReviewPageAndConfirmationPagePremiumNetflixSoc method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case : Test Adding and Removing Netflix Premium SOC.");
		Reporter.log("Test Data : TMO TI Pooled PAH Msisdn Registered for Netflix with Premium");
		Reporter.log("==============================");
		Reporter.log("Verify Service Review page premium Netflix soc");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click On plan icon | Plan Page should be displayed");
		Reporter.log("4. Click on Change Services btn | Services List Page should be verified");
		Reporter.log("5. Verify FA Soc  | FA soc should displayed");
		Reporter.log(
				"6. Select Netflix Prem. $13.99 with Fam Allowance ($19 value) Soc  | Netflix Prem. $13.99 with Fam Allowance ($19 value) should be selected");
		Reporter.log("7. Click On Next button  | Services Review Page should be displayed");
		Reporter.log(
				"8. Verify Added FA soc  | Netflix Prem. $13.99 with Fam Allowance ($19 value) should be displayed in Review page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		if (manageAddOnsSelectionPage.verifyFamilyAllowanceCheckBoxisSelected() == false) {
			addNetFLixFamilyAllowanceSoc(myTmoData);
		}
		manageAddOnsSelectionPage.checkfamilyAllowanceCheckBoxs();
		manageAddOnsSelectionPage.verifyRemovingFASocDescription();
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		// need to verify if any messages related Removing Family Soc on odf
		// confirmation page
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")

	public void testAddAndRemoveNetFlixStandatdSocforAlreadyRegisteredCustomer(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyServiceReviewPageAndConfirmationPagePremiumNetflixSoc method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case : Test Adding and Removing Netflix Premium SOC.");
		Reporter.log("Test Data : TMO TI Pooled PAH Msisdn Registered for Netflix with Premium");
		Reporter.log("==============================");
		Reporter.log("Verify Service Review page premium Netflix soc");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click On plan icon | Plan Page should be displayed");
		Reporter.log("4. Click on Change Services btn | Services List Page should be verified");
		Reporter.log("5. Verify FA Soc  | FA soc should displayed");
		Reporter.log(
				"6. Select Netflix Prem. $13.99 with Fam Allowance ($19 value) Soc  | Netflix Prem. $13.99 with Fam Allowance ($19 value) should be selected");
		Reporter.log("7. Click On Next button  | Services Review Page should be displayed");
		Reporter.log(
				"8. Verify Added FA soc  | Netflix Prem. $13.99 with Fam Allowance ($19 value) should be displayed in Review page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		if (manageAddOnsSelectionPage.verifyStandardCheckBoxisSelected() == false) {
			addNetFLixStandardSoc(myTmoData);
		}
		manageAddOnsSelectionPage.checkStandardCheckBoxs();
		manageAddOnsSelectionPage.verifyRemovingStandardSocDescription();
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		// need to verify if any messages related Removing standatd Soc on odf
		// confirmation page
	}

	/**
	 * DE153958
	 * 
	 * Enable hd link is missing in Confirmation page after adding tmo one plus data
	 * plan
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyEnableHDLinkOnODFConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEnableHDLinkOnODFConfirmationPage method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case  verify Enable HD Link On ODF ConfirmationPage");
		Reporter.log("Test Data : Any PAH/Full User with TM0 One plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"6. select tmo one plus plan and click on continue | Review page with Data plan details should be loaded");
		Reporter.log("8. On Review page, Click on 'Agree & Submit' button. | Confirmation page should be displayed.");
		Reporter.log(
				"9. On confirmation page verify Enable HD Link |  Enable HD Link On Confirmation page message should be displayed");

		Reporter.log("Actual Result:");
		Reporter.log("===================");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.selectDataPlanODFPAge("ONE Plus");
		manageAddOnsSelectionPage.clickOnContinueBtn();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		verifyODFDataPassReviewPage();
		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyAddOnConfirmPageForDataPass(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAddOnConfirmPageForDataPass method called in NewConfirmationPageForDP_DS_DP_OD");
		Reporter.log(
				"Test Case Name : Verify ODF Confirmation page when user submits AddOns for Data Plan, Service and Data pass together.");
		Reporter.log("Test Data :  Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"6. Choose Any Data Pass from data Pass section. Read service name and price. | Should be able to read the name and price.");
		Reporter.log("7. Click on Continue to Review Page | Review page should be displayed.");
		Reporter.log("8. Verify added Data  Pass. . | It should be matched");
		Reporter.log(
				"9. verify all Elements on Confirmation page and click on return home| Plan Page should be displayed.");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		manageAddOnsSelectionPage.verifyUnblockRoamingAndUnblockIt();
		manageAddOnsSelectionPage.removedAddedDataPass();

		manageAddOnsSelectionPage.clickOnDataPassRadioButton("");
		manageAddOnsSelectionPage.clickOnContinueBtn();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		verifyODFDataPassReviewPage();
		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		newODFConfirmationPage.verifyThanksHeaderOnConfirmationPage();
		newODFConfirmationPage.verifyOrderDetailsBladeDisplayedwithDownwarCarat();
		newODFConfirmationPage.clickOnOrderDetailsBlade();

		newODFConfirmationPage.clickOnReturnHome();
		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyAddOnConfirmPageForDataService(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAddOnConfirmPageForDataService method called in NewConfirmationPageForDP_DS_DP_OD");
		Reporter.log(
				"Test Case Name : Verify ODF Confirmation page when user submits AddOns for Data Plan, Service and Data pass together.");
		Reporter.log("Test Data :  Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"6. Choose Any Data Pass from data Pass section. Read service name and price. | Should be able to read the name and price.");
		Reporter.log("7. Click on Continue to Review Page | Review page should be displayed.");
		Reporter.log("8. Verify added Data  Service. . | It should be matched");
		Reporter.log(
				"9. verify all Elements on Confirmation page and click on return home| Plan Page should be displayed.");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnCheckBox("");

		manageAddOnsSelectionPage.clickOnContinueBtn();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		verifyODFDataPassReviewPage();
		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		newODFConfirmationPage.verifyThanksHeaderOnConfirmationPage();
		newODFConfirmationPage.verifyOrderDetailsBladeDisplayedwithDownwarCarat();
		newODFConfirmationPage.clickOnOrderDetailsBlade();

		newODFConfirmationPage.clickOnReturnHome();
		// AccountOverviewPage accountoverviewpage = new
		// AccountOverviewPage(getDriver());
//		accountoverviewpage.verifyAccountOverviewPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyConfirmPageForStdResDataPass(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyConfirmPageForStdResDataPass method called in ConfirmationPage");
		Reporter.log("Test Case Name : Verify ODF Confirmation page when Std/Res user submits Data pass.");
		Reporter.log("Test Data :  Any STD/RES User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Choose Any Data Pass from data Pass section | Data Pass should be selected successfully");
		Reporter.log("7. Click on Continue to Review and Pay Page | Review and Pay page should be displayed.");
		Reporter.log("8. Verify added Data  Pass. | It should be matched");
		Reporter.log(
				"9. verify all Elements on Confirmation page and click on return home| Plan Page should be displayed.");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToODFReviewPageBySelectingDataPass(myTmoData);

		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		verifyODFDataPassReviewAndPayPage();
		oDFDataPassReviewPage.verifyUpfrontPaymentMethod();
		oDFDataPassReviewPage.clickPaymentMethodBlade();
		// oDFDataPassReviewPage.verifyEditPaymentMethodText();
		oDFDataPassReviewPage.verifyAddCardBlade();
		oDFDataPassReviewPage.clickAddCard();
		oDFDataPassReviewPage.verifyCardInformationPage("Card information");
		oDFDataPassReviewPage.enterCardName();
		oDFDataPassReviewPage.enterCardNumber();
		oDFDataPassReviewPage.enterexpirationDate();
		oDFDataPassReviewPage.enterCvv();
		oDFDataPassReviewPage.enterZip();
		oDFDataPassReviewPage.clickContinueCTA();
		// oDFDataPassReviewPage.agreeAndSubmitButton();
		// ManageAddOnsConfirmationPage newODFConfirmationPage = new
		// ManageAddOnsConfirmationPage(getDriver());
		// newODFConfirmationPage.verifyODFConfirmationPage();
		// newODFConfirmationPage.clickOnReturnHome();
	}
}