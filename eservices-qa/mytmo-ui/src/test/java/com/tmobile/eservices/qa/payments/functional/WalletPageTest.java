package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AddBankPage;
import com.tmobile.eservices.qa.pages.payments.AddCardPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.pages.payments.WalletPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

public class WalletPageTest extends PaymentCommonLib {

	/**
	 * US586782 Standalone Wallet URL
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testWalletPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US586782 Standalone Wallet URL");
		Reporter.log("Data Conditions - PAH user");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to /mywallet | My Wallet page should be displayed");
		Reporter.log("Step 4: Verify Wallet Header | Wallet Header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		WalletPage walletPage = new WalletPage(getDriver());
		getDriver().navigate().to(System.getProperty("environment") + "/mywallet");
		;
		walletPage.verifyPageLoaded();
		walletPage.clickCloseModal();
	}

	/**
	 * US579593 Stand alone save add bank.
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testWalletSaveAddBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US579593 Stand alone save add bank.");
		Reporter.log("Data Conditions - PAH user");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to /mywallet | My Wallet page should be displayed");
		Reporter.log("Step 4: Verify Wallet Header | Wallet Header should be displayed");
		Reporter.log("Step 5: click on Add Bank blade| Add Bank Page should be displayed");
		Reporter.log("Step 6: verify Page Header| 'Bank information' Header should be displayed");
		Reporter.log(
				"Step 7: Enter bank information and click Save button | Wallet page should be displayed with saved bank ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		WalletPage walletPage = navigateToMyWalletPage(myTmoData);
		if (walletPage.verifyWalletIsFullMsg()) {
			deleteAndVerifyStoredBankWallet(walletPage);
		}
		AddBankPage addBankPage = addBankWallet(myTmoData, walletPage);
		if (!walletPage.verifyWalletIsFullMsg()) {
			walletPage.clickAddBankBlade();
			addBankPage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);
			addBankPage.clickBackButton();
			walletPage.verifyPageLoaded();
		}
	}

	/**
	 * US598467 Phase2 Wallet Full Treatment
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testWalletFullTreatment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US598467 Phase2 Wallet Full Treatment");
		Reporter.log("Data Conditions - PAH user with 10 saved payment methods");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to /mywallet | My Wallet page should be displayed");
		Reporter.log("Step 4: Verify Wallet Header | Wallet Header should be displayed");
		Reporter.log("Step 5: click on Add Bank blade| Add Bank Page should be displayed");
		Reporter.log("Step 6: verify Page Header| 'Bank information' Header should be displayed");
		Reporter.log(
				"Step 7: Enter bank information and click Save button | Wallet page should be displayed with saved bank ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		WalletPage walletPage = navigateToMyWalletPage(myTmoData);
		while (!walletPage.verifyWalletIsFullMsg()) {
			addBankWallet(myTmoData, walletPage);
		}
		getDriver().get(System.getProperty("environment") + "/payments/paymentblade");
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyNewPageLoaded();
		spokePage.verifyAngular6WalletFullMsg(PaymentConstants.WALLET_FULL_MESSAGE);
		spokePage.clickAddBank();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		addBankPage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);
		addBankPage.verifyWalletsizereachedtext(PaymentConstants.WALLET_FULL_MESSAGE);
		addBankPage.verifySavePaymentCheckboxDisabled();
		addBankPage.clickBackButton();
		spokePage.verifyNewPageLoaded();
		spokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.verifyWalletsizereachedtext(PaymentConstants.WALLET_FULL_MESSAGE);
		addCardPage.verifySavePaymentCheckboxDisabled();
		getDriver().navigate().to(System.getProperty("environment") + "/onetimepayment");
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		spokePage.verifyPageLoaded();
		spokePage.verifyWalletFullMsg(PaymentConstants.WALLET_FULL_MESSAGE);
		spokePage.clickAddBank();
		addBankPage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);
		addBankPage.verifyWalletsizereachedtext(PaymentConstants.WALLET_FULL_MESSAGE);
		addBankPage.verifySavePaymentCheckboxDisabled();
		addBankPage.clickBackButton();
		spokePage.verifyPageLoaded();
		spokePage.clickAddCard();
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.verifyWalletsizereachedtext(PaymentConstants.WALLET_FULL_MESSAGE);
		addCardPage.verifySavePaymentCheckboxDisabled();
	}

	/**
	 * US577098 Stand alone save add card.(v4)
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testWalletSaveAddCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US577098 Stand alone save add card.(v4)");
		Reporter.log("Data Conditions - PAH user");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to /mywallet | My Wallet page should be displayed");
		Reporter.log("Step 4: Verify Wallet Header | Wallet Header should be displayed");
		Reporter.log("Step 5: click on Add Card blade| Add Card Page should be displayed");
		Reporter.log("Step 6: verify Page Header| 'Card information' Header should be displayed");
		Reporter.log(
				"Step 7: Enter Card information and click Save button | Wallet page should be displayed with saved Card ");
		Reporter.log("Step 5: click on Add Card blade| Add Card Page should be displayed");
		Reporter.log("Step 6: verify Page Header| 'Card information' Header should be displayed");
		Reporter.log("Step 7: Click Back button | Wallet page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		WalletPage walletPage = navigateToMyWalletPage(myTmoData);
		if (walletPage.verifyWalletIsFullMsg()) {
			deleteAndVerifyStoredCardWallet(walletPage);
		}
		AddCardPage addCardPage = addCardWallet(myTmoData, walletPage);
		if (!walletPage.verifyWalletIsFullMsg()) {
			walletPage.clickAddCardBlade();
			addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
			addCardPage.clickBackBtn();
			walletPage.verifyPageLoaded();
		}
	}

}
