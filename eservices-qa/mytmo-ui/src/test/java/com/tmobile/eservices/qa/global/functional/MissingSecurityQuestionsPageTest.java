package com.tmobile.eservices.qa.global.functional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.global.GlobalConstants;
import com.tmobile.eservices.qa.pages.global.ConfirmationCodePage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.global.MissingSecurityQuestionsPage;
import com.tmobile.eservices.qa.pages.global.RegistrationPage;
import com.tmobile.eservices.qa.pages.global.SignUpPage;

public class MissingSecurityQuestionsPageTest extends GlobalCommonLib{
	
	private static final Logger logger = LoggerFactory.getLogger(MissingSecurityQuestionsPageTest.class);

	private static final String SIGN_UP_HEADER = "Successfuly Navigated to Signup Screen";
	
	
	/**
	 * TestCase ID:3273870 TC001_MyTMO Cloud - Verify Email Notifications for
	 * TMO ID Updates and Registration
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
	public void signUpNewUser(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("signUpNewUser method called in MyTMORegistrationTest");
		getTMOURL(myTmoData);
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.clickSignUpButton();
		Reporter.log(SIGN_UP_HEADER);
		SignUpPage signUpPage = new SignUpPage(getDriver());
		Assert.assertEquals(signUpPage.getSignUpText(), GlobalConstants.SIGNUP_HEADER_TEXT);
		signUpPage.enterValuesAndClickNext(myTmoData);
		ConfirmationCodePage confirmationPage = new ConfirmationCodePage(getDriver());
		Assert.assertEquals(myTmoData.getLoginEmailOrPhone().trim(), confirmationPage.verifyBan());
		Reporter.log("Successfuly Redirect  to Confirm Screen");
		String profilePin = getPinFromTMOProfile(myTmoData);
		if (StringUtils.isEmpty(profilePin)) {
			confirmationPage.clickResendCodeLink();
			confirmationPage.enterConfirmationPin(profilePin);
		} else {
			confirmationPage.enterConfirmationPin(profilePin);
		}
		confirmationPage.clickNextButton();
		Reporter.log("Successfuly Navigated to Missing Sequrity Questions Screen");
		MissingSecurityQuestionsPage securityQuestionsPage = new MissingSecurityQuestionsPage(getDriver());
		securityQuestionsPage.answerSecurityQuestionsAndSubmit(myTmoData.getSignUpData());

	}
	
	/**
	 * Sign Up For Not Registered User
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void signUpForNotRegisteredUser(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("signUpForNotRegisteredUser method called in MyTMORegistrationTest");
		Reporter.log("Test Case : Verify user is able to reset the password using Security Questions");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | MISDIN is not Registered to TMO App");
		Reporter.log("3. Click on Sign Up button | Sign Up Page should be displayed.");
		Reporter.log(
				"4. Enter accout information(Email or Phone) and click next | User should be able to enter confirmation code");
		Reporter.log(
				"5. Enter confirmation code and click next | Missing Security questions screen should be displayed");
		Reporter.log(
				"6. Users enters Security Questions and Answers then click on next button | User should be redirected to Homepage");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		launchAndPerformLogin(myTmoData);
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyNotRegisteredUser();
		loginPage.clickSignUp();
		SignUpPage signUpPage = new SignUpPage(getDriver());
		signUpPage.enterValuesAndClickNext(myTmoData);
		ConfirmationCodePage confirmationPage = new ConfirmationCodePage(getDriver());
		Assert.assertEquals(myTmoData.getLoginEmailOrPhone().trim(), confirmationPage.verifyBan());
		Reporter.log("Successfuly Redirect  to Confirm Screen");
		String profilePin = getPinFromTMOProfile(myTmoData);
		if (StringUtils.isEmpty(profilePin)) {
			confirmationPage.clickResendCodeLink();
		}
		confirmationPage.enterConfirmationPin(profilePin);
		confirmationPage.clickNextButton();
		Reporter.log("Successfuly Navigated to Missing Security Questions Screen");
		MissingSecurityQuestionsPage securityQuestionsPage = new MissingSecurityQuestionsPage(getDriver());
		securityQuestionsPage.answerSecurityQuestionsAndSubmit(myTmoData.getSignUpData());
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true)
	public void registerUserWithLowerEnvironment(MyTmoData myTmoData, ControlTestData controlTestData) {
		getDriver().get("http://10.92.28.199:9000/lab11");
		RegistrationPage registrationPage = new RegistrationPage(getDriver());
		registrationPage.unlockAccount(myTmoData);
		if (registrationPage.verifyUnlockSuccessMessage()) {
			LoginPage loginPage = new LoginPage(getDriver());
			loginPage.clickSignUp();
			SignUpPage signUpPage = new SignUpPage(getDriver());
			signUpPage.enterValuesAndClickNext(myTmoData);
			ConfirmationCodePage confirmationPage = new ConfirmationCodePage(getDriver());
			String confirmationCodeURL = getDriver().getCurrentUrl();
			getDriver().get("http://10.92.28.199:9000/lab11/msisdn");
			String pin = registrationPage.getPIN(myTmoData);
			getDriver().get(confirmationCodeURL);
			confirmationPage.enterConfirmationPin(pin);
			confirmationPage.clickNextButton();
			MissingSecurityQuestionsPage securityQuestionsPage = new MissingSecurityQuestionsPage(getDriver());
			securityQuestionsPage.answerSecurityQuestionsAndSubmit(myTmoData.getSignUpData());
		}
	}

}
