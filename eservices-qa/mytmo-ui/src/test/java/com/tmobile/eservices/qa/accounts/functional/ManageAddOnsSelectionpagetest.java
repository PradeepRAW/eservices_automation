/**
 *
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.accounts.AddonsLineSelectorPage;
import com.tmobile.eservices.qa.pages.accounts.BlockingPage;
import com.tmobile.eservices.qa.pages.accounts.CrazyLegsBenefitsPage;
import com.tmobile.eservices.qa.pages.accounts.LineDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsConfirmationPage;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsSelectionPage;
import com.tmobile.eservices.qa.pages.accounts.MediaSettingsPage;
import com.tmobile.eservices.qa.pages.accounts.ODFDataPassReviewPage;
import com.tmobile.eservices.qa.pages.accounts.PlanDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.PlansComparisonPage;

/**
 * @author csudheer
 *
 */
public class ManageAddOnsSelectionpagetest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(ManageAddOnsSelectionpagetest.class);

	// ===============================onlyDatapasses================================

	/**
	 * US288900 :: Review Add-On s - Hide monthly change message for data pass only
	 * purchase US217075 :: Review Add Ons - Tax display for TI vs. TE
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyAddOnReviewPageCancelCTAFunctionalityForDataPass(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnReviewPageCancelCTAFunctionalityForDataPass method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Review Page Cancel CTA Functionality For Data pass");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"6. Choose Any Data Pass (International Pass) and Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"7. Read selected Data pass name and price. | User should be able to read Data pass name and Price.");
		Reporter.log("8.Click on Continue CTA | Review page should be displayed");
		Reporter.log(
				"8. Check name and price of Data passon Review page. | Name and Price of data pass should match with step7.");
		Reporter.log("9. Verify Monthly billing is not displayed | Monthly billing is should not displayed");
		Reporter.log(
				"1. Click on Cancel CTA on Review Page and check redirection. | User should be redirected to Plans landing page(plans.html)");
		Reporter.log("13. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToODFDataPassReviewPage(myTmoData, "");
		oDFDataPassReviewPageCancelVerification(myTmoData);
	}

	/**
	 * US278715 - Selection page - Suspended account
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyMessageOnSelectionPageWhenAccountIsSuspendedInVoluntarilyWithPAHAndFull(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPAgeWhenAccountIsSuspendedInVoluntarilyWithPAHAndFull method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Message on Selection page when account is suspended Voluntarily");
		Reporter.log("Test Data : Any Voluntarily suspended account, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn page. | Message "
				+ "Your account is suspended.  Please contact Customer Care to make changes" + " should be displayed.");
		Reporter.log("7. Click on Customer Care link. | It should be redirected to "
				+ " currentEnvironment/contact-us.html.");
		Reporter.log(
				"8. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("9. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifySuspendedorContactPAHMessage(
				"Your account is suspended. Please contact Customer Care to make changes");
		manageAddOnsSelectionPage.clickOnCustomerCareLink();
		manageAddOnsSelectionPage.verifyContactUsPageDispalyed();
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();

	}

	/**
	 * US270493#Selection page - Display currently active data plan message
	 * US270797#Selection page - Display currently active service with label
	 * 'Active' US274549#Selection page - Display currently active data plan message
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyCurrentlyActiveDataPassInSelectionPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyCurrentlyActiveDataPassInSelectionPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Currently Active DataPass In Selection Page");
		Reporter.log("Test Data : Active data pass ");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7. verify currently active service | currently active service should be noted");
		Reporter.log(
				"8. click on currently active service and continue| ODF data pass review Page should be displayed");
		Reporter.log("9. verify removed , added items text| removed , added items text should be displayed");
		Reporter.log("10. verify  Removed Service names|  Removed Service names should be currently active service");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.clickManageaddOnButton();

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyPlanNameHeader("Data Plan");
		manageAddOnsSelectionPage.verifyCurrentlyActiveText();
		// manageAddOnsSelectionPage.verifyCurrentlyActiveServices();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();

		String currentlyActiveServices = manageAddOnsSelectionPage.getCurrentlyActiveServicesText();
		manageAddOnsSelectionPage.clickOnCurrentlyActiveCheckBoxe();
		manageAddOnsSelectionPage.waitForConflictDialogAndClickContinueBtn();
		// manageAddOnsSelectionPage.checkFirstCheckBox();

		manageAddOnsSelectionPage.clickOnFreeRadioOrCheckBoxButton("Radio");
		manageAddOnsSelectionPage.unselectActiveCheckBox();
		manageAddOnsSelectionPage.clickOnConflictContinue();

		manageAddOnsSelectionPage.verifyContinueBtnIsEnabled();
		manageAddOnsSelectionPage.clickOnContinueBtn();

		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyRemovedItemsText();
		oDFDataPassReviewPage.verifyAddedItemsText();
		oDFDataPassReviewPage.verifyRemovedItemsNames(currentlyActiveServices);
	}

	/**
	 * US275677 - Select Add ons - JUMP add on service removal message
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyAddOnSelectionPageForJumpUsers(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAddOnSelectionPageForJumpUsers method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case name : Check whether Standard and Restricted users can be able to select any data plan/Service/Pass options.");
		Reporter.log("Test Data : any PAH/Full User with any jump on line.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log("6. Check message on AddOn Selection page under Jump service. | Message "
				+ "Handset Protection Extended Warranty cannot be added back and you will no longer be covered for mechanical or electrical breakdown."
				+ "below Jump service should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyHandsetProtectionWarranty(
				"Handset Protection Extended Warranty cannot be added back and you will no longer be covered for mechanical or electrical breakdown.");
	}

	/**
	 * US250669#Select Add-Ons - Selection page: Hide CTA US250834#Select Add-Ons -
	 * Selection page: Display CTA US250833#Select Add-Ons - Selection page:
	 * Selection for multiple services US251731#Select Add-Ons - Selection page:
	 * Select add on data plan or data pass
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyODFSelectionPageContinueButtonFunctionality(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyODFSelectionPageContinueButtonFunctionality method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case name : Check whether Standard and Restricted users can be able to select any data plan/Service/Pass options.");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7. Verify Continue Button is Disabled when Customer did no change");
		Reporter.log("8. Choose a Data pass or service or Data plan");
		Reporter.log("9. Continue Button Should be Enabled");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
		manageAddOnsSelectionPage.clickOnCurrentlyActiveCheckBoxe();
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.verifyContinueBtnIsEnabled();
	}

	/**
	 * US228087#Reduce clicks on single eligible item selection US262321#Select Add
	 * Ons - Undo auto select of single SOC US262321#Select Add Ons - Undo auto
	 * select of single SOC
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyODFUndoAutoSingleSocSelection(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyODFUndoAutoSingleSocSelection method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case : Verify ODF Upgrade your Service page reduce clicks on single eligible item selection");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade your Service page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7. Verify Disabled Continue Button| Disabled Continue Button should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);
		navigateToFutureURLFromHome(myTmoData, "/odf/Service:2SNFLXHDP");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		verifyManageAddOnsPage();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	// ================================OnlyDataPlan========================================

	/**
	 * Verify No Conflict At Data Plan Options Change
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyODFSupressDataPlanConflictModel(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyODFReviewPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify functionality for Conflict modal");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7.Verify no Conflict Model is displayed while Changing Data Plans");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.clickManageaddOnButton();

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnAllDataPlans();
	}

	/**
	 * Verify No Conflict At Data Plan Options Change
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void testManageAddOnsSelectionPageUpsellURL(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Test Manage AddOns Selection Page Upsell URL");
		Reporter.log("Test Case  Name : test Manage AddOns Selection Page Upsell URL");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Manage AddOns Selection Page|Manage AddOns Selection Page should be displayed");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/account/odf/DataService:ALL/Service:ALL/DataPass:ALL");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
	}

	/**
	 * Verify Selection page legalese terms US370539 - Legal copy - display update
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifySelctionPageLegaleseTerms(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyODFReviewPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify functionality for Conflict modal");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Legalese terms and Conditions");

		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		// manageAddOnsSelectionPage.verifyNewTermsAndConditions();
		manageAddOnsSelectionPage.verifyLegaleseText(
				"On all T-Mobile plans, during congestion, the small fraction of customers using >50GB/mo. may notice reduced speeds until next bill cycle due to data prioritization Service may be slowed, suspended, terminated, or restricted for misuse, abnormal use, interference with our network or ability to provide quality service to other users, or significant roaming. On-device usage is prioritized over tethering usage, which may result in higher speeds for data used on device. See T-Mobile.com/OpenInternet for details. Plan Changes: Credit approval, deposit, and $25 SIM starter kit or, in stores & con customer service calls; $20 upgrade support charge may be required. For plans where taxes and fees are not included in the monthly recurring charge: Monthly Regulatory Programs (RPF) & Telco Recovery Fee (TRF) totaling $2.71 per voice line ($0.60 for RPF & &2.11 for TRF) applies. Taxes approx. 6-28% of bill; add'l usage taxed in some countries. These changes will take effect on the dates you've selected. This is the recurring amount you will pay each month for your plans and services. Other items such as taxes, fees, bill credits, overage charges (if applicable), or pro-rated charges (for changes made mid-billing cycle) are not included. Please visit the billing section for more information on your monthly bill. Data Passes: Post-paid only; pass charges will appear on bill statement for effective date of the pass. Partial megabytes rounded up. Full speeds available up to specified data allotment, then slowed to up to 2G speeds. No domestic or international roaming on Domestic Pass, unless Pass is specifically for roaming. International Pass usage does not impact Mobile Internet plan data allotment. Data pass charges are one-time charges. These charges will be applied immediately on the start of your pass. No refunds will be issues after the start of your pass.");
	}

	/**
	 * US278715 - Selection page - Suspended account
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageOnSelectionPageWhenAccountIsSuspendedInVoluntarilyWithStandardAndRestricted(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPageWhenAccountIsSuspendedInVoluntarilyWithStandardAndRestricted method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Message on Selection page when account is suspended Voluntarily");
		Reporter.log("Test Data : Any Voluntarily suspended account, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn Seelction page. | Message "
				+ "Contact the primary account holder to make any changes." + " should be displayed");
		Reporter.log(
				"7. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("8. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage
				.verifySuspendedorContactPAHMessage("Contact the primary account holder to make any changes");
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US278715 - Selection page - Suspended account
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageOnSelectionPageWhenAccountHasPendingPlanAndSuspendedInVoluntarilyWithPAHAndFull(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPAgeWhenAccountIsSuspendedVoluntarilyWithPAHAndFull method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify Message on Selection page when account is suspended InVoluntarily and account has pending rate plan");
		Reporter.log("Test Data : Any InVoluntarily suspended account with Pending rate plan, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn page. | Message "
				+ "Your account is suspended.  Please contact Customer Care to make changes" + "should be displayed.");
		Reporter.log("7. Click on Customer Care link. | It should be redirected to "
				+ " currentEnvironment/contact-us.html.");
		Reporter.log(
				"8. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("9. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifySuspendedorContactPAHMessage(
				"Your account is suspended. Please contact Customer Care to make changes");
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyCancelButtonisInDisabled();
		manageAddOnsSelectionPage.clickOnCustomerCareLink();
		manageAddOnsSelectionPage.verifyContactUsPageDispalyed();
	}

	/**
	 * US278715 - Selection page - Suspended account
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageOnSelectionPageWhenAccountHasPendingPlanAndIsSuspendedInVoluntarilyWithStandardAndRestricted(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPageWhenAccountHasPendingPlanAndIsSuspendedInVoluntarilyWithStandardAndRestricted method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify Message on Selection page when account is suspended InVoluntarily and when account has pending plan");
		Reporter.log(
				"Test Data : Any InVoluntarily suspended account with Pending plan, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn Seelction page. | Message "
				+ "Contact the primary account holder to make any changes" + " should be displayed");
		Reporter.log(
				"7. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("8. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifySuspendedorContactPAHMessage(
				"You aren't authorized to make changes to this account. Contact the primary account holder to make any changes");
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * - Selection page - Suspended Pending changes account
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void VerifyManageAddonServicesdisableForSuspendedAccountWithPendingChanges(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"VerifyManageAddonServicesdisableForSuspendedAccountWithPendingChanges method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify Message on Selection page when account is suspended InVoluntarily and when account has pending plan");
		Reporter.log(
				"Test Data : Any InVoluntarily suspended account with Pending plan, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn Seelction page. | Message "
				+ "Contact the primary account holder to make any changes" + " should be displayed");
		Reporter.log(
				"7. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("8. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = navigateToManageAddOnsFlowFromHomeLink(myTmoData);
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US278715 - Selection page - Suspended account
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageOnSelectionPageWhenAccountIsSuspendedVoluntarilyWithPAHAndFull(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPAgeWhenAccountIsSuspendedVoluntarilyWithPAHAndFull method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Message on Selection page when account is suspended Voluntarily");
		Reporter.log("Test Data : Any Voluntarily suspended account, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn page. | Message "
				+ "Your account is suspended.  Please contact Customer Care to make changes" + " should be displayed.");
		Reporter.log("7. Click on Customer Care link. | It should be redirected to "
				+ " currentEnvironment/contact-us.html.");
		Reporter.log(
				"8. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("9. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifySuspendedorContactPAHMessage(
				"Your account is suspended. Please contact Customer Care to make changes");
		manageAddOnsSelectionPage.clickOnCustomerCareLink();
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US278715 - Selection page - Suspended account US278690 - Selection page -
	 * Standard & Restricted user
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageOnSelectionPageWhenAccountIsSuspendedVoluntarilyWithStandardAndRestricted(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPageWhenAccountIsSuspendedVoluntarilyWithStandardAndRestricted method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Message on Selection page when account is suspended Voluntarily");
		Reporter.log("Test Data : Any Voluntarily suspended account, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn Seelction page. | Message "
				+ "Contact the primary account holder to make any changes." + " should be displayed");
		Reporter.log(
				"7. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("8. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage
				.verifySuspendedorContactPAHMessage("Contact the primary account holder to make any changes.");
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US278715 - Selection page - Suspended account
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageOnSelectionPageWhenAccountHasPendingPlanAndSuspendedVoluntarilyWithPAHAndFull(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPAgeWhenAccountIsSuspendedVoluntarilyWithPAHAndFull method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify Message on Selection page when account is suspended Voluntarily and account has pending rate plan");
		Reporter.log("Test Data : Any Voluntarily suspended account with Pending rate plan, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn page. | Message "
				+ "Your account is suspended.  Please contact Customer Care to make changes" + " should be displayed.");
		Reporter.log("7. Click on Customer Care link. | It should be redirected to "
				+ " currentEnvironment/contact-us.html.");
		Reporter.log(
				"8. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("9. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifySuspendedorContactPAHMessage(
				"Your account is suspended. Please contact Customer Care to make changes");
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyCancelButtonisInDisabled();
		manageAddOnsSelectionPage.clickOnCustomerCareLink();
		manageAddOnsSelectionPage.verifyContactUsPageDispalyed();
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US278715 - Selection page - Suspended account
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageOnSelectionPageWhenAccountHasPendingPlanAndIsSuspendedVoluntarilyWithStandardAndRestricted(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPageWhenAccountHasPendingPlanAndIsSuspendedInVoluntarilyWithStandardAndRestricted method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify Message on Selection page when account is suspended Voluntarily and when account has pending plan");
		Reporter.log(
				"Test Data : Any Voluntarily suspended account with Pending plan, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn Seelction page. | Message "
				+ "Contact the primary account holder to make any changes." + " should be displayed");
		Reporter.log(
				"7. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("8. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage
				.verifySuspendedorContactPAHMessage("Contact the primary account holder to make any changes.");
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US278732 - Selection page - Suspended line
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageOnSelectionPageWhenLineIsSuspendedVoluntarilyWithPAHAndFull(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPageWhenLineIsSuspendedVoluntarilyWithPAHAndFull method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Message on Selection page when account is suspended Voluntarily");
		Reporter.log("Test Data : Any Voluntarily suspended account, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn page. | Message "
				+ "This line is suspended.  Please contact Customer Care to make changes" + " should be displayed.");
		Reporter.log("7. Click on Customer Care link. | It should be redirected to "
				+ " currentEnvironment/contact-us.html.");
		Reporter.log(
				"8. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("9. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifySuspendedorContactPAHMessage(
				"This line is suspended. Please contact Customer Care to make changes");
		manageAddOnsSelectionPage.clickOnCustomerCareLink();
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US278732 - Selection page - Suspended line
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageOnSelectionPageWhenLineIsSuspendedVoluntarilyWithStandardAndRestricted(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPageWhenLineIsSuspendedVoluntarilyWithStandardAndRestricted method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Message on Selection page when account is suspended Voluntarily");
		Reporter.log("Test Data : Any Voluntarily suspended account, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("7. Check message on AddOn Seelction page. | Message "
				+ "Please contact your primary account holder to make changes on this line" + " should be displayed");
		Reporter.log(
				"8. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("9. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = navigateToManageDataAndAddOnsFlow(myTmoData);
		manageAddOnsSelectionPage.verifySuspendedorContactPAHMessage(
				"You aren't authorized to make changes to this account. Contact the primary account holder to make any changes.");
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US278732 - Selection page - Suspended line
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageOnSelectionPageWhenLineHasPendingPlanAndSuspendedVoluntarilyWithPAHAndFull(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPageWhenLineHasPendingPlanAndSuspendedVoluntarilyWithPAHAndFull method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify Message on Selection page when line is suspended Voluntarily and when account has pending plan");
		Reporter.log(
				"Test Data : Any Voluntarily suspended line and account with Pending plan, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn page. | Message "
				+ "This line is suspended.  Please contact Customer Care to make changes" + " should be displayed.");
		Reporter.log("7. Click on Customer Care link. | It should be redirected to "
				+ " currentEnvironment/contact-us.html.");
		Reporter.log(
				"8. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("9. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifySuspendedorContactPAHMessage(
				"Your account is suspended. Please contact Customer Care to make changes");
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyCancelButtonisInDisabled();
		manageAddOnsSelectionPage.clickOnCustomerCareLink();
		manageAddOnsSelectionPage.verifyContactUsPageDispalyed();
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US278732 - Selection page - Suspended line
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageOnSelectionPageWhenLineIsSuspendedVoluntarilyAndAccountHasPendingPlanWithStandardAndRestricted(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPageWhenLineIsSuspendedVoluntarilyAndAccountHasPendingPlanWithStandardAndRestricted method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify Message on Selection page when line is suspended Voluntarily and when account has pending plan");
		Reporter.log(
				"Test Data : Any Voluntarily suspended line and account with Pending plan, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn Selection page. | Message "
				+ "Please contact your primary account holder to make changes on this line" + " should be displayed");
		Reporter.log(
				"7. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("8. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("===================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifySuspendedorContactPAHMessage(
				"You aren't authorized to make changes to this account. Contact the primary account holder to make any changes");
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US271030 - Selection page - Pending data plan message
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageOnSelectionPageWhenLineHasPendingDataPlanWithStandardAndRestricted(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPageWhenLineHasPendingDataPlanWithStandardAndRestricted method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Message on Selection page when line has pending data plan");
		Reporter.log("Test Data : Any line with Pending data plan, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check message on AddOn Selection page. | Message "
				+ "Please contact your primary account holder to make changes on this line" + " should be displayed");
		Reporter.log(
				"7. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("8. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifySuspendedorContactPAHMessage(
				"Please contact your primary account holder to make changes on this line");
		manageAddOnsSelectionPage.verifyRadioBtnsAndCheckBoxes();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US270493#Selection page - Display currently active data plan message
	 * US270797#Selection page - Display currently active service with label
	 * 'Active' US274549#Selection page - Display currently active data plan message
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyCurrentlyActiveDataPlanMessageInSelectionPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyCurrentlyActiveDataPlanMessageInSelectionPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify currently active data plan message is displayed");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7. verify currently active service | currently active service should be noted");
		Reporter.log(
				"8. click on currently active service and continue| ODF data pass review Page should be displayed");
		Reporter.log("9. verify removed   items text| removed  items text should be displayed");
		Reporter.log("10. verify  Removed Service names|  Removed Service names should be currently active service");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyCurrentlyActiveText();
		manageAddOnsSelectionPage.verifyCurrentlyActiveServices();

		String currentlyActiveServices = manageAddOnsSelectionPage.getCurrentlyActiveServicesText();
		manageAddOnsSelectionPage.clickOnCurrentlyActiveCheckBoxe();
		manageAddOnsSelectionPage.waitForConflictDialogAndClickContinueBtn();
		// manageAddOnsSelectionPage.checkFirstCheckBox();
		manageAddOnsSelectionPage.unselectActiveCheckBox();

		manageAddOnsSelectionPage.clickOnContinueBtn();

		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyRemovedItemsText();
		oDFDataPassReviewPage.verifyRemovedItemsNames(currentlyActiveServices);
	}

	/**
	 * US276959 - Selection page - Hide hard coded 'No Data' option This test case
	 * is for checking No Data option in Data plans section when user does not have
	 * active No Data SOC.
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyAddOnSelectionPageForNoDataOptionWhenItIsNotActive(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnSelectionPageForNoDataOptionWhenItIsNotActive method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case name : Check No Data option in Data plans section when user does not have active No Data SOC");
		Reporter.log("Test Data : any PAH/Full User with any plan. No Data SOC should not be active on line.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade services page should be displayed");
		Reporter.log("7. Select No Data data plan | User should be able to select No Data option.");
		Reporter.log("8. Click on Continue to Review Page | Review page should be displayed.");
		Reporter.log("9. Check by effective date| Effective ate PopUp should be displayed");
		Reporter.log("10. Check Next bill cycle radio button | Effective date should be Next Billing cycle.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		// manageAddOnsSelectionPage.verifyPromoPlanMessage();
		manageAddOnsSelectionPage.verifyDeprioritizationMessage();

		manageAddOnsSelectionPage.getCurrentlyActiveServicesText();
		manageAddOnsSelectionPage.clickOnCurrentlyActiveCheckBoxe();
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnDataPlanRadioButton("No Data");
		// manageAddOnsSelectionPage.checkFirstCheckBox();
		manageAddOnsSelectionPage.clickOnContinueBtn();

		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickOnChangeDateLink();
		oDFDataPassReviewPage.verifyEffectiveDatePopUpDisplayed();
		oDFDataPassReviewPage.verifyNextBillCycleRadioBtnDisplayed();
	}

	/**
	 * US303862 - Selection page - Display messages for data plan section
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, Group.MULTIREG })
	public void verifyPromoMessageForDataPlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPromoMessageForDataPlan method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Promo Message for Data Plan");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Check Promo message above Data Plan | Promo Message should be displayed. "
				+ "If on a promo plan, you'll lose promotional discount(s) when migrating to a new plan.");
		Reporter.log("7. Check dePrioritization message. | Deprioritization message "
				+ "On all T-Mobile plans, during congestion, the small fraction of customers using >50GB/mo. may notice reduced speeds until next bill cycle due to data prioritization."
				+ " should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.clickManageaddOnButton();

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyPromoPlanMessage();
		manageAddOnsSelectionPage.verifyDeprioritizationMessage();
	}

	/**
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyCurrentlyActiveDataPlanTitleAndPlanName(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyCurrentlyActiveDataPlanTitleAndPlanName method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case :Verify currently active data plan name and title are displayed in upgrade your services page ");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7.Verify Currently Active Message is Displayed for all Active Passes,Plan,Services");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyPlanNameHeader("Data Plan");
		manageAddOnsSelectionPage.verifyCurrentlyActiveText();
	}

	/**
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyLineSelectionDropDown(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyLineSelectionDropDown method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Test Case :Verify number of lines in Plan page and Upgrade your services are same");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("4. Get number of lines size| Number of lines should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log(
				"7. Verify number of lines in Upgrade your Service page| Plan and Upgrade your Service Page lines should be same");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToAccountoverViewPageFlow(myTmoData);
		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		int noLines = accountOverviewPage.getNumberOfLines();
		accountOverviewPage.clickOnLineName();

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
		linedetailspage.clickManageaddOnButton();

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnSelectADifferentLineLink();

		manageAddOnsSelectionPage.verifyNumberOfLines(noLines);
	}

	/**
	 * US303855#Review page - Display pro-rate message for data upgrade
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, Group.MULTIREG })
	public void verifyReviewPageDisplayProRateMessageForDataUpgrade(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyReviewPageDisplayProRateMessageForDataUpgrade method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Review Page pro-rate message for data upgrade");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"6. Choose Any Data Pass (Mobile HotSpot 1 Week - 1GB) and Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"7. Read selected Data pass name and price. | User should be able to read Data pass name and Price.");
		Reporter.log("8.Click on Continue CTA | Review page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.getActiveDataPlan();
		manageAddOnsSelectionPage.selectDataPlanODFPage();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
	}

	// ===========================================OnlyServices==========================================

	/**
	 * US256707#Conflict management - Cancel on conflict
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void verifyODFConflictManagement(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyODFConflictManagement method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case name :Verify conflict dialog is displayed on selecting McAfee® Security for T-Mobile check box and on cancelling it upgrade services page is displayed");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log(
				"7. Click on McAfee® Security for T-Mobile check box | Conflict Modal overlay should be displayed");
		Reporter.log("8. Verify Conflict Modal | Conflict Modal should displayed with Cancel button");
		Reporter.log("9. Click on Cancel Button | Upgrade Your Services page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.clickManageaddOnButton();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnCheckBox("Family Mode");
		manageAddOnsSelectionPage.verifyConflictModal();
		manageAddOnsSelectionPage.clickOnConflictModalCancelBtn();
		verifyManageAddOnsPage();
	}

	/**
	 * US251937#Select Add-Ons - Selection page: Display currently active service
	 * and data plan
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyODFSelectionPageCurrentlyActiveService(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyODFSelectionPageCurrentlyActiveService method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case name :Verify currently active message is displayed for all active passes, plans and services");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7.Verify Currently Active Message is Displayed for all Active Passes,Plan,Services");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyCurrentlyActiveServices();
	}

	/**
	 * US276198 - Date logic - Effective date options for service additions and
	 * removal (non SITT) This test case is for checking dates options on Review
	 * page when user selected any Non SITT soc service when Conflict occurs
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired0805")
	public void verifySelectionPageandReviewPageSOCPricesAreSame(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnReviewPageEffectiveDateForNonSITTSOCDuringConflict method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case name : Check dates on Review page when user changes the Service and Conflict occurs");
		Reporter.log("Test Data : any PAH/Full User with any plan. Line has Block All Messages service on it");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"6. Check service Block Content Downloads is active or not. Also read the price. | Checkbox should be in selected state. Should be able to read price");
		Reporter.log(
				"7. Choose Service which has conflict(choose T-Mobile Caller Tunes 5). Also read the price | User should be able to select Service.Conflict modal should be displayed.");
		Reporter.log("8. On Conflict modal, click on Continue CTA. | Conflict modal should be disappeared.");
		Reporter.log("9. Check service Block Content Downloads is active or not. | Checkbox should be unselected.");
		Reporter.log("10. Click on Continue CTA. | Review page should be displayed.");
		Reporter.log(
				"11. Check AddOn Item | (T-Mobile Caller Tunes 5) should be displayed under AddOn Item. Also price should match from step7.");
		Reporter.log(
				"12. Check Removed Item | (Block Content Downloads ) should be displayed under Removed Item. Also price should match from step6.");
		Reporter.log(
				"13. Check Change Date link under AddOn Item and Removed Item| Change Date link should not be displayed.");
		Reporter.log("14. Check date for AddOn item. | Date should be Next Bill Cycle date.");
		Reporter.log("15. Check date for Removed item. | Next bill cycle option should be displayed.");
		Reporter.log("16. Verify dates for AddOn and for Removed item. | Both dates shoudl be matched.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		manageAddOnsSelectionPage.verifyCheckBoxByNameIsSelected("Block Content Downloads");
		manageAddOnsSelectionPage.getCheckBoxPrice("Block Content Downloads");
		String newPrice = manageAddOnsSelectionPage.getCheckBoxPrice("T-Mobile Caller Tunes 5");
		manageAddOnsSelectionPage.clickOnCheckBoxByName("T-Mobile Caller Tunes 5");
		manageAddOnsSelectionPage.clickOnCheckBoxByName("Block Content Downloads");
		manageAddOnsSelectionPage.verifyTextInConflictedOverlay("Block Content Downloads");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();

		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyRemovedItemsNames("Block Content Downloads");
		Assert.assertTrue(oDFDataPassReviewPage.verifyServiceTextByPrice("T-Mobile Caller Tunes 5").contains(newPrice),
				"T-Mobile Caller Tunes 5 price is displaying wrong");
		oDFDataPassReviewPage.verifyNextBillCycle();
	}

	/**
	 * US278112 - Date logic - Effective date options for channel parity SOCs (SITT)
	 * This test case is for checking dates options on Review page when user
	 * selected any SITT soc service when no Conflict occurs
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired0825")
	public void verifyAddOnReviewPageForEffectiveDateForRemovalOfSITTSOC(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnReviewPageForEffectiveDateForAdditionOfSITTSOC method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case name : Check dates on Review page when user removes the SITT Service and no Conflict occurs");
		Reporter.log(
				"Test Data : any PAH/Full User with any plan. International Roaming Service should be active on line.Data plan should not be ONE PLUS/International ONE PLUS");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"6. UnChoose SITT Service like North America Stateside International Talk | User should be able to unselect SITT service.");
		Reporter.log("7. Click on Continue to Review Page | Review page should be displayed.");
		Reporter.log("8. Check Change Date link | Change Date link should not be displayed.");
		Reporter.log("9. Check by effective date | Effective date should be Next Billing cycle.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.getCheckBoxPrice("North America Stateside International Talk");
		manageAddOnsSelectionPage.clickOnCheckBoxByName("North America Stateside International Talk");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();

		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		/*
		 * Assert.assertTrue( oDFDataPassReviewPage.verifyServiceTextByPrice(
		 * "North America Stateside International Talk") .contains(price),
		 * "North America Stateside International Talk price is displaying wrong" );
		 */
		oDFDataPassReviewPage.verifyEffectiveDate();
	}

	/**
	 * US278112 - Date logic - Effective date options for channel parity SOCs (SITT)
	 * US288904 - Date logic - Effective date for data plan upgrade when (Channel
	 * parity) SITT is dropped due to conflict This test case is for checking dates
	 * options on Review page when user selected any SITT soc service when Conflict
	 * occurs
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired0825")
	public void verifyAddOnReviewPageForEffectiveDateForRemovalOfSITTSOCWhenConflictOccurs(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnReviewPageForEffectiveDateForRemovalOfSITTSOCWhenConflictOccurs method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case name : Check dates on Review page when SITT Service removed due to Conflict occurs");

		Reporter.log(
				"Test Data : any PAH/Full User with any plan. International Roaming Service should be active on line.Data plan should not be ONE PLUS/International ONE PLUS");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade services page should be displayed");
		Reporter.log(
				"6. verify North America Stateside International Talk with mobile - selected |  North America Stateside International Talk with mobile is selected.");
		Reporter.log("7. Choose T-Mobile ONE Plus  Data plan. | User should be able to select Data Plan");
		Reporter.log("8. On Conflict modal, click on Continue CTA. | ODF data pass review page should be displayed.");

		Reporter.log("9. Check Removed item section | Removed item Name iD should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		manageAddOnsSelectionPage.verifyCheckBoxByNameIsSelected("Name ID");
		manageAddOnsSelectionPage.getCheckBoxPrice("Name ID");

		String newPrice = manageAddOnsSelectionPage.getCheckBoxPrice("T-Mobile ONE Plus");
		manageAddOnsSelectionPage.clickOnONEPlusInternationalRadioBtn();
		manageAddOnsSelectionPage.verifyTextInConflictedOverlay("Name ID");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();

		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyRemovedItemsNames("Name ID");
		Assert.assertTrue(oDFDataPassReviewPage.verifyServiceTextByPrice("T-Mobile ONE Plus").contains(newPrice),
				"T-Mobile ONE Plus price is displaying wrong");
	}

	/**
	 * US254187#Conflict management - Conflict with currently active service
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyODFSelectionPageConflictWithCurrentlyActiveService(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyODFSelectionPageConflictWithCurrentlyActiveService method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case : Verify conflict dialog box is displayed when two conflicting socs are selected in upgrade your services page");
		Reporter.log("Test Data : SC PAH Customer with NameID SOCS Present");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7.Select two Conflicting Socs ");
		Reporter.log("8.Verify Conflcit Dialog box| Conflict box should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.checkNameIDCheckBoxs();
		manageAddOnsSelectionPage.verifyConflictModal();
		manageAddOnsSelectionPage.clickOnConflictContinue();
		// verifyDataPassReviewPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyBANLevelServicesNotVisibleToStandardCustomer(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyBANLevelServicesNotVisibleToStandardCustomer method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : verifyBANLevelServicesNotVisibleToStandardCustomer");
		Reporter.log("Test Data : Any Standard user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");

		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log(
				"7.Verify Family services not available for standard customer| Family services should not available for standard customer ");
		Reporter.log("Actual Result:");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = navigateToManageAddOnsSelectionPage(myTmoData);
		manageAddOnsSelectionPage.verifyFamilyServicesNotAvailableforStandardCustomer();
	}

	// ==================alltogether=========================

	/**
	 * US152074#Sedona - ODF - Updating the inline error graphical treatments
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyODFInlineErrorForPartialRestrictedUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyODFInlineErrorForPartialRestrictedUser method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify ODF Inline Error For Partial Restricted User");
		Reporter.log("Test Data : Standarad or restricted user with any rate plan ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Restricted message");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifySuspendedorContactPAHMessage(
				"You aren't authorized to make changes to this account. Contact the primary account holder to make any changes.");
		manageAddOnsSelectionPage.verifyAllRadioButtonsNotDisplayed();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US271183#Conflict management - Continue on conflict of data plan and services
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired0825")
	public void verifyConflictContinueWhileAddingDataPlanAndServices(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyConflictContinueWhileAddingDataPlanAndServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Conflict modal continue button functionality");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7. Select T-Mobile ONE Plus Radio button | T-Mobile ONE Plus should be selected");
		Reporter.log(
				"8. Select International Service first check box | International Service first check box should be checked");
		Reporter.log("9. Verify Conflict Modal | Conflict Modal should displayed with Cancel button");
		Reporter.log("10. Click on Continue Button | Upgrade Your Services page should be displayed");
		Reporter.log("11. Verify International service is checked | International service should be checked");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnONEPlusInternationalRadioBtn();

		String internationalServiceName = manageAddOnsSelectionPage.getInternationalFirstCheckBoxText();

		manageAddOnsSelectionPage.clickOnFirstInternationalCheckbox();
		manageAddOnsSelectionPage.verifyConflictModal();
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.verifyInternationalServiceIsChecked(internationalServiceName);
	}

	// **
	// US271231#Conflict management - Cancel on conflict between data plan and
	// service
	// US272095#Selection page - Suppress data plan conflict modal
	// *
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pendingneeddatanameidandtmooneplus_bothnotselected")
	public void verifyConflictCancelWhileAddingDataPlanAndServices(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyConflictCancelWhileAddingDataPlanAndServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case : Verify Conflict Cancel While Adding DataPlan And Services in upgrade your services page");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7. Select T-Mobile ONE Plus Radio button | T-Mobile ONE Plus should be selected");
		Reporter.log(
				"8. Select International Service first check box | International Service first check box should be checked");
		Reporter.log("9. Verify Conflict Modal | Conflict Modal should displayed with Cancel button");
		Reporter.log("10. Click on Cancel Button | Upgrade Your Services page should be displayed");
		Reporter.log("11. Verify International service is unchecked | International service should be unchecked");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnONEPlusInternationalRadioBtn();
		String internationalServiceName = manageAddOnsSelectionPage.getInternationalFirstCheckBoxText();
		manageAddOnsSelectionPage.clickOnFirstInternationalCheckbox();
		manageAddOnsSelectionPage.verifyConflictModal();
		manageAddOnsSelectionPage.clickOnConflictModalCancelBtn();
		manageAddOnsSelectionPage.verifyInternationalServiceIsNotChecked(internationalServiceName);
	}

	/**
	 * US271001 - [Continued] Selection page - Pending rate plan message (Test Only)
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyMessageOnSelectionPageWhenAccountHasPendingRatePlanWithPAHAndFull(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPageWhenAccountHasPendingRatePlanWithPAHAndFull method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Message on Selection page when line has pending data plan");
		Reporter.log("Test Data : Any line with Pending data plan, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Click On View Changes link on Plan page.| Plan Services Pending page should be displayed");
		Reporter.log(
				"6. Read the Take effect Date. Convert date in Month DD,Year format.| User should be able to convert the date.");
		Reporter.log("7. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("8. Check message on AddOn Selection page. | Message "
				+ "Your account has pending changes that will take effect on <next bill cycle date>.  Please call Customer Care to make additional changes."
				+ " should be displayed");
		Reporter.log(
				"9. Check any radio buttons and checkboxes. | Radio buttons and checkboxes should not be displayed on AddOn selection page.");
		Reporter.log("10. Check Continue CTA. | Continue CTA should be in disabled state.");
		Reporter.log("11. Click on Customer Care link. | It should be redirected to Contact us page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// navigateToPlanAndServiceViewPendingPage(myTmoData);
		// PlanandServiceViewPendingPage planandServiceViewPendingPage = new
		// PlanandServiceViewPendingPage(getDriver());
		// String date = planandServiceViewPendingPage.getEffectDate();
		navigateToODFURlWithMsisdn(myTmoData, "account-overview");
		verifyManageAddOnsPage();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyRadioBtnsAndCheckBoxes();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US302848 # Old/retired / bookmarked URLs redirect
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyBookmarkedURLOfServiceListPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBookmarkedURLOfServiceListPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify when user hit direct url of Service List page then whether it's redirect to new AddOn page or not");
		Reporter.log("Test Data : Any PAH/Full User with any plan. ");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log(
				"3. Hit direct url of Service list page. Environment/plans/services-list.html | AddOn selection page should be displayed");
		Reporter.log("================================");

		verifyDeepLinkingManagAddOnsPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

	}

	/**
	 * Verify Description For Each Data Plan TMO ONE TI
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyDescriptionForEachDataPlanTMOONETI(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyDescriptionForEachDataPlanTMOONETI method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Description For Each Data Plan TMO ONE TI");

		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Click on any data plan and verify description | Description  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.randomClickOnDataPlanOrDataPassOrServiceAndVerifyDescriptionForEachPlan("Data Plan");
	}

	/**
	 * Verify Description For Each Data Plan TMO ONE TI
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyDescriptionForEachDataPassTMOONETI(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyDescriptionForEachDataPlanTMOONETI method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Description For Each Data Plan TMO ONE TI");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Click on any Data Pass and verify description | Description  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.randomClickOnDataPlanOrDataPassOrServiceAndVerifyDescriptionForEachPlan("Data Pass");
	}

	/**
	 * Verify Description For Each Data Plan TMO ONE TI
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyDescriptionForEachServicesTMOONETI(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyDescriptionForEachDataPlanTMOONETI method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Description For Each Data Plan TMO ONE TI");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Click on any Service and verify description | Description  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.randomClickOnDataPlanOrDataPassOrServiceAndVerifyDescriptionForEachPlan("Service");
	}

	/**
	 * US312590 # Redirect from other CTAs or links DE153429 Navigation from home to
	 * Manage add on's page is Navigating to old Plan-Configure page
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void verifyChangeDataLinkFromHomePageForBingeOnEligibileUsersForPAHAndFull(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyChangeDataLinkFromHomePageForBingeOnEligibileUsersForPAHAndFull method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify when PAH/Full user click ChangeData link under BingeOn section at Home page then whether it's redirects to AddOn page or not");
		Reporter.log("Test Data : Any PAH/Full User with Simple Choice Plan. Eligible for BingeON ");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log(
				"4. On Home page, click on change data link | Manage data and AddOns  page should be should be displayed");

		Reporter.log("================================");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickChangeDataLink();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
	}

	/**
	 * US330801 # BearMode : Conflict modal
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyBearModeConflictModalWindow(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBearModeConflictModalWindow method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test case Name : Verify Bear  Mode Conflict Modal Window ");
		Reporter.log("Test Data : PAH TMO ONE PLUS and Web Guard Service added Missdin");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Plan  | Plan Details page should be open");
		Reporter.log("4. Click on Change Service | ODF page should be open");
		Reporter.log("5. Slect Family mode plan | Plan pop up window should be open");
		Reporter.log("6.Click on Add service | Bear Mode Conflict modal window should be open");
		Reporter.log(
				"7.verify Family Mode Conflict Dialog message   | Family Mode Conflict Dialog message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnCheckBox("Family Mode");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnCheckBox("Family Allowances");
		manageAddOnsSelectionPage.verifyPlanPopupModal();
	}

	/**
	 * DE153328 manage Add ons unblock roaming is not navigating to Blocking page in
	 * profile
	 *
	 * DE157210 international data pass purchase not being blocked for SCNA or TMO1
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void verifyUnBlockRoamingLinknavigatingtoBlockingPageinProfile(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyUnBlockRoamingLinknavigatingtoBlockingPageinProfile method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify UnBlock Roaming Link navigating to Blocking Page in Profile");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("7. Click on unblock roaming link| Blocking page  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.unBlockRoamingLink();
		BlockingPage blockingPage = new BlockingPage(getDriver());
		blockingPage.verifyBlockingPage();
	}

	/**
	 * US331360 DE153038
	 *
	 * Add-ons: Enable HD link redirect to Profile page
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void testVerifyEnableHDLinkNavigatesToMediaSettingsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testVerifyEnableHDLinkNavigatesToMediaSettingsPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : test Verify Enable HD Link Navigates To MediaSettings Page");
		Reporter.log("Test Data : PAH/Full/Standard User on TMO ONE TE plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("5. Click on enableHD link| Media Settings page  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnEnableHDLink();
		MediaSettingsPage mediaSettingsPage = new MediaSettingsPage(getDriver());
		mediaSettingsPage.verifyMediaSettingsPage();
	}

	/**
	 * US335435 Add-ons: Best plan message redirect for change data
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyRedirectionOfChangeDataLinkOfBestPlanMessageToManageDataAddOnPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyRedirectionOfChangeDataLinkOfBestPlanMessageToManageDataAddOnPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case : Verify when user clicks on Change Data link of Best plan message on Plan comparison page at that time user should redirect to AddOns Selection page.");
		Reporter.log("Test Data : PAH/Full User who can see Best Plan message on Plans comparison page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("3. Load Plans page | Plans page should be displayed");
		Reporter.log("3. Click on Change Plan button | Plans comparison page should be displayed");
		Reporter.log(
				"3. Click on Change Data link of Best plan message. | Manage Addon Selection page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToNewPlansComaprisonPage(myTmoData);
		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
		plansComparisonPage.verifyPlanComparisionPage();
		plansComparisonPage.clickChangeDataLink();
		// navigateToPlansComparisonPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
	}

	/**
	 * US358449 PDP - Display warning modal on removing PDP
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void testVerifyWarmingModalOnRemovingPDPSOC(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testVerifyWarmingModalOnRemovingPDPSOC method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : test Verify Warming Modal On Removing of PDP SOC");
		Reporter.log("Test Data : PAH/Full User with PDP SOC active on line");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Goto Plans page. | Plans page should be displayed.");
		Reporter.log("5. Click on Manage Data & AddOns CTA. | AddOns Page page should be displayed.");
		Reporter.log("6. Uncheck PDP/PHP soc from line. | Warming Modal On Removing PHP/PDP SOC  should be displayed");
		Reporter.log("7. Click on cancel button| AddOn selection page should be displayed");
		Reporter.log("8. Uncheck PDP/PHP soc from line. | Warming Modal On Removing PHP/PDP SOC  should be displayed");
		Reporter.log(
				"9. Click on Continue button| PDP/PHP soc should be unchecked and user should be redirected to AddOns Selection page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnCheckBoxForPDPSOC();
		manageAddOnsSelectionPage.verifyWarningModalRemovingPDPSOCIsDisplayed();
		manageAddOnsSelectionPage.verifyWarningMsgRemovingPDPSOCisDisplayed(
				"By removing Stateside International with Mobile, you will lose these additional free benefits");
		manageAddOnsSelectionPage.clickCancelBtnOfRemoveWarningOfPHPSoc();
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnCheckBoxForPDPSOC();
		manageAddOnsSelectionPage.verifyContinuePDPRemovalBtnisDisplayed();
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
	}

	/**
	 * 7/31 data passes middle ware changes
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifySNPASS24DataPassForTMOONECustomerGSM(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySNPASS24DataPassForTMOONECustomer method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Review Page  For  SNPASS24 Data pass");
		Reporter.log("Test Data : Any PAH/Full with any SCNA or TMO ONE or TMO ONE TI");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Choose International Passand Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"7. Read selected Data pass name and price. | User should be able to read Data pass name and Price.");
		Reporter.log("8.Click on Continue CTA | Review page should be displayed");
		Reporter.log(
				"8. Check name and price of Data passon Review page. | Name and Price of data pass should match with step7.");
		Reporter.log("9. Verify Monthly billing is not displayed | Monthly billing is should not displayed");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToODFDataPassReviewPage(myTmoData, "International Pass");
	}

	/**
	 * 7/31 data passes middle ware changes
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifySGPASS24DataPassForSCCustomerGSM(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySGPASS24DataPassForSCCustomer method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Review Page  For  SGPASS24 Data pass");
		Reporter.log("Test Data : Any PAH/Full with SC Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Choose International Passand Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"7. Read selected Data pass name and price. | User should be able to read Data pass name and Price.");
		Reporter.log("8.Click on Continue CTA | Review page should be displayed");
		Reporter.log(
				"8. Check name and price of Data passon Review page. | Name and Price of data pass should match with step7.");
		Reporter.log("9. Verify Monthly billing is not displayed | Monthly billing is should not displayed");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToODFDataPassReviewPage(myTmoData, "International Pass");
	}

	/**
	 * 7/31 data passes middle ware changes
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = Group.RELEASE_READY)
	public void verifySGPASS24MDataPassForSCCustomerMILine(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySGPASS24MDataPassForSCCustomer method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Review Page  For  SGPASSM24 Data pass");
		Reporter.log("Test Data : Any PAH/Full with SC MI Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. ChooseInternational Pass and Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"7. Read selected Data pass name and price. | User should be able to read Data pass name and Price.");
		Reporter.log("8.Click on Continue CTA | Review page should be displayed");
		Reporter.log(
				"8. Check name and price of Data passon Review page. | Name and Price of data pass should match with step7.");
		Reporter.log("9. Verify Monthly billing is not displayed | Monthly billing is should not displayed");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToODFDataPassReviewPage(myTmoData, "International Pass");
	}

	/**
	 * 7/31 data passes middle ware changes
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifySNPASS24MDataPassForTMOONECustomerMILine(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySGPASS24MDataPassForTMOONECustomer method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Review Page  For  SNPASSM24 Data pass");
		Reporter.log("Test Data : Any PAH/Full with SCNA or TMO ONE MI Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. ChooseInternational Pass and Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"7. Read selected Data pass name and price. | User should be able to read Data pass name and Price.");
		Reporter.log("8.Click on Continue CTA | Review page should be displayed");
		Reporter.log(
				"8. Check name and price of Data passon Review page. | Name and Price of data pass should match with step7.");
		Reporter.log("9. Verify Monthly billing is not displayed | Monthly billing is should not displayed");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToODFDataPassReviewPage(myTmoData, "International Pass");
	}

	/**
	 * 10/06 NOD release
	 *
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = Group.RELEASE_READY)
	public void verifySGPASS24MDataPassForPappyEssecntials(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySGPASS24MDataPassForSCCustomer method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Review Page  For  SGPASSM24 Data pass");
		Reporter.log("Test Data : Any PAH/Full with SC MI Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Choose International Pass and Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"7. Read selected Data pass name and price. | User should be able to read Data pass name and Price.");
		Reporter.log("8.Click on Continue CTA | Review page should be displayed");
		Reporter.log(
				"8. Check name and price of Data passon Review page. | Name and Price of data pass should match with step7.");
		Reporter.log("9. Verify Monthly billing is not displayed | Monthly billing is should not displayed");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToODFDataPassReviewPage(myTmoData, "International Pass");
	}

	/**
	 * 7/31 data passes middle ware changes
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "proddefect")
	public void verifyPuertoricoCusotmerIsAbleTOChooseDataPasses(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySNPASS24DataPassForTMOONECustomer method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Review Page  For  SNPASS24 Data pass");
		Reporter.log("Test Data : Any PAH/Full with any SCNA or TMO ONE or TMO ONE TI");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"6. Choose Day Pass 512 MB Data and Unlimited Voice 24hrs and Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"7. Read selected Data pass name and price. | User should be able to read Data pass name and Price.");
		Reporter.log("8.Click on Continue CTA | Review page should be displayed");
		Reporter.log(
				"8. Check name and price of Data passon Review page. | Name and Price of data pass should match with step7.");
		Reporter.log("9. Verify Monthly billing is not displayed | Monthly billing is should not displayed");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);

	}

	// US372402 [Continued] No eligible service - Redirect to displaying all
	// >US377481 [Continued] Add-ons selection page - Load SOC description
	// expanded

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyWhenUserEligibleForOneSOCOnlyWhetherDescriptionExpanded(ControlTestData data,
			MyTmoData myTmoData) {

		logger.info(
				"verifyWhenUserEligibleForOneSOCOnlyWhetherDescriptionExpanded method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case : When user is eligible for only 1 SOC and user hit direct url with that soc, then check whether descrption of that soc expanded or not");
		Reporter.log(
				"Test Data : Any account which is eligible to go to Manage AddOns Page. Also note down valid data SOC. As we have to pass those in url.");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Hit direct url of AddOns with the valid Data SOC. | Add Ons Selection page should be displayed.");
		Reporter.log("4. Check Description of SOC. | Description should be expanded.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyAddOnPageWhenOnlyOneSOCEligible();
		Reporter.log("Eligible SOC is expanded and  description also displayed.");
	}

	// US372402 [Continued] No eligible service - Redirect to displaying all
	// >US377481 [Continued] Add-ons selection page - Load SOC description
	// expanded

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyWhenUserDoesNotHaveAnyEligibleSOCInURL(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyWhenUserDoesNotHaveAnyEligibleSOCInURL method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case : When user is not eligible for any SOC and user hit direct url with ineligible soc, then check behaviour.");
		Reporter.log(
				"Test Data : Any account which is eligible to go to Manage AddOns Page. We have to hit AddOn url with ineligible SOC codes.");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Hit direct url of AddOns with the invalid SOC. | Add Ons Selection page should be displayed with error message.");
		Reporter.log(
				"4. Check error message and check Continue CTA. | Error message should be displayed and CTA should be displayed.");
		Reporter.log("5. Click on Continue CTA. | AddOn Page should be displayed with all valid options.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/DataService:adadad,QWEQWEqwe,QWEQWEWQE");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyAddOnPageWhenUserNotEligibleForAnySOC();
	}

	/**
	 * PR210348 SyncUp Fleet Pro Plans- US437393
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "sprint")
	public void verifyFleetProPlansAreNotVisibleOnManageAddonPage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyFleetProPlansAreNotVisibleOnManageAddonPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : sync up fleet pro rate plans should not visible on Manage add onspage");
		Reporter.log("Test Data : Pah Missdn with two soc's ZFLEETPRO,GFLEETPRO");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Navigate to Mnaage Add on's page and verify above 2 Active sync up SOcs not displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifySyncUpFleetNotPresent();
	}

	/**
	 * US437624 Active data pass on add-ons page - currently / future active - UI
	 * update
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void verifyActiveDataPassesCurrentlyandFutureDate(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyActiveDataPassesCurrentlyandFutureDate method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Active Test is present in ");
		Reporter.log("Test Data : TI PAH MSISDN");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Navigate to Mnaage Add on's page and verify Active Data Passes on the Page");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		for (int a = 0; manageAddOnsSelectionPage.verifyActivePassesIsActive() < 3; a++) {
			if (manageAddOnsSelectionPage.verifyActivePassesIsActive() == 2) {
				manageAddOnsSelectionPage.verifyActivePassesHeader();
				manageAddOnsSelectionPage.verifyActivePassesWithDate();
				// manageAddOnsSelectionPage.verifyChangeDateFunctionality();
				// manageAddOnsSelectionPage.verifyRemovelink();
			} else {
				try {
					manageAddOnsSelectionPage.clickOnDataPassRadioButton("HD 24 Hour Pass");
				} catch (Exception e) {

				}
			}
			manageAddOnsSelectionPage.clickOnContinueBtn();
			ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
			verifyODFDataPassReviewPage();
			oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
			ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
			newODFConfirmationPage.verifyODFConfirmationPage();
			newODFConfirmationPage.verifyThanksHeaderOnConfirmationPage();
			newODFConfirmationPage.clickOnReturnHome();
			/*
			 * PlanPage planPage = new PlanPage(getDriver()); planPage.verifyPlanpage();
			 * planPage.clickOnManageDataAddOnsBtn();
			 */
			verifyManageAddOnsPage();
		}
	}

	/**
	 * US437651 UI update - Add Remove option
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyChangeDateAndRemoveLinksForDataPass(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyChangeDateAndRemoveLinksForDataPass method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Active Test is present in ");// 8589146045/Auto12345
		Reporter.log("Test Data : TI/TE PAH MSISDN and Data Pass should be activated from Future date");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Navigate to Mnaage Add on's page and verify Active Data Passes on the Page");
		Reporter.log("3.Verify Change Date And Remove links. | Change Date and Remove links should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// navigateToManageDataAndAddOnsFlow(myTmoData);
		redirectToUpsellURLOnReviewPage(myTmoData, "DataPass:HDPASS0");
		// ManageAddOnsSelectionPage manageAddOnsSelectionPage = new
		// ManageAddOnsSelectionPage(getDriver());
		// manageAddOnsSelectionPage.verifyDataPassLinksStatus();
		// manageAddOnsSelectionPage.verifyWhetherPipeCharacterIsDisplayedOrNot();
	}

	/**
	 * US437624 Active data pass on add-ons page - currently / future active - UI
	 * update US446316 Remove/Edit: Change date - Effective date modal load
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyChangeDateFunctionality(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyActiveDataPassesCurrentlyandFutureDate method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Active Test is present in ");
		Reporter.log("Test Data : TI PAH MSISDN");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Navigate to Mnaage Add on's page and verify Active Data Passes on the Page");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		while (manageAddOnsSelectionPage.verifyActivePassesIsActive() < 3) {
			if (manageAddOnsSelectionPage.verifyActivePassesIsActive() == 2) {

				manageAddOnsSelectionPage.verifyChangeDateFunctionality();
				verifyManageAddOnsPage();
				manageAddOnsSelectionPage.verifyChangeDateFunctionality();
				ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
				oDFDataPassReviewPage.clickPickUpDateRadioButton();
				manageAddOnsSelectionPage.selectdatepick();
				oDFDataPassReviewPage.selectNextDate();
				oDFDataPassReviewPage.clickOverlaySelectButton();
			} else {
				try {
					manageAddOnsSelectionPage.clickOnDataPassRadioButton("HD 24 Hour Pass");
				} catch (Exception e) {

				}
			}
			manageAddOnsSelectionPage.clickOnContinueBtn();
			ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
			verifyODFDataPassReviewPage();
			oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
			ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
			newODFConfirmationPage.verifyODFConfirmationPage();
			newODFConfirmationPage.verifyThanksHeaderOnConfirmationPage();
			newODFConfirmationPage.clickOnReturnHome();
			/*
			 * PlanPage planPage = new PlanPage(getDriver()); planPage.verifyPlanpage();
			 * planPage.clickOnManageDataAddOnsBtn();
			 */
			verifyManageAddOnsPage();
		}
	}

	/**
	 * US439825 Remove data pass - Remove confirmation modal US451695 Remove data
	 * pass - Remove confirm US437651 UI update - Add Remove option
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyRemoveDataPassFunctionality(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyActiveDataPassesCurrentlyandFutureDate method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Active Test is present in ");
		Reporter.log("Test Data : TI PAH MSISDN");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Navigate to Mnaage Add on's page and verify Active Data Passes on the Page");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		for (int a = 0; manageAddOnsSelectionPage.verifyActivePassesIsActive() < 3; a++) {
			if (manageAddOnsSelectionPage.verifyActivePassesIsActive() == 2) {
				manageAddOnsSelectionPage.verifyRemovelink();
				manageAddOnsSelectionPage.cancelbuttonOnRemoveDataPass();
				Assert.assertTrue(manageAddOnsSelectionPage.verifyActivePassesIsActive() == 2);
				manageAddOnsSelectionPage.removebuttonOnRemoveDataPass();
				verifyManageAddOnsPage();
				Assert.assertTrue(manageAddOnsSelectionPage.verifyActivePassesIsActive() == 1);
				manageAddOnsSelectionPage.clickOnDataPassRadioButton("International");
				manageAddOnsSelectionPage.clickOnContinueBtn();
				verifyODFDataPassReviewPage();
			} else {
				manageAddOnsSelectionPage.clickOnDataPassRadioButton("International");
				manageAddOnsSelectionPage.clickOnContinueBtn();
				ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
				verifyODFDataPassReviewPage();
				oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
				ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
				newODFConfirmationPage.verifyODFConfirmationPage();
				newODFConfirmationPage.verifyThanksHeaderOnConfirmationPage();
				newODFConfirmationPage.clickOnReturnHome();

				AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
				accountoverviewpage.verifyAccountOverviewPage();
			}
		}
	}

	/**
	 * US446315 Remove / edit - Non-permissioned users
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "44444")
	public void verifyStandardCustomerDontSeeChangeDateandRemoveLink(ControlTestData data, MyTmoData myTmoData) {

		logger.info(
				"verifyStandardCustomerDontSeeChangeDateandRemoveLink method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case : Verify Change date and remove link not available for Standard and restricted Cutomers ");
		Reporter.log("Test Data : TI Standard MSISDN");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Navigate to Mnaage Add on's page and verify Active Data Passes on the Page");
		Reporter.log("3. Verify Change date and Remove link is not present");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		for (int a = 0; manageAddOnsSelectionPage.verifyActivePassesIsActive() < 3; a++) {
			if (manageAddOnsSelectionPage.verifyActivePassesIsActive() == 2) {

				manageAddOnsSelectionPage.verifyRemovelinkandChangeDataNotPresent();
			} else {

			}
		}

	}

	/**
	 * US488766 ASG - Allow Restricted and Standard Users to purchase Data Pass
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyStandardandRestrictedUsersabilitytoPurchaseDataPass(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Standard and restricted user doesn't have any active data passes");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Choose any Dass pass | Data Pass should be selected.");
		Reporter.log("7.Click on Continue CTA | Review page should be displayed");
		Reporter.log("8.Verify Review and Pay page ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyDataPassLabel();
		manageAddOnsSelectionPage.clickOnDataPassRadioButton("International Pass");
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewAndPayPage();
	}

	/**
	 *
	 * verify Removable SOC in Plan page and Manage addon's Page
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyRemovableSOCinPlanPageAndManageAddOn(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyRemovableSOCinPlanPageAndManageAddOn method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case : Removable SOC should  be displayed in plan page and in Add on's page and user should be able to remove ");
		Reporter.log("Test Data : T-Mobile CallerTunes Free - this is a Removable SOC added from back end");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. verify Napster Premier is  displayed on plan landing page");
		Reporter.log("3. verify Napster Premier is  displayed on Manage Add on's page and User is able to remove it");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnCheckBoxByName("T-Mobile Caller Tunes");
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
	}

	/**
	 * verify NON-removable SOC in Plan page and Manage addon's Page
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyNonRemovableSOCinPlanPageAndManageAddOn(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyNonRemovableSOCinPlanPageAndManageAddOn method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : NON removable  SOC should  be displayed in plan page and in Add on's page ");
		Reporter.log("Test Data :  - this is a non removable SOC added from back end");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Login to the application | User Should be login successfully");
		Reporter.log("2.Verify  is  displayed on plan landing page");
		Reporter.log("3.Verify  is  displayed on Manage Add on's page and user should not be able to remove it");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyNonRemovableSoc();
	}

	// Netflix

	/*
	 * Conflict and warning verification in Downgrading DE153090
	 *
	 * existing prod pop up is not being Displayed while removing standard netflix
	 * SOC DE153097
	 *
	 * Existing prod pop up is not showing while removing premium Netflix soc
	 *
	 *
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void testverifyConflictMessagesWhileDownGradingNetFlixSoc(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyServiceReviewPageAndConfirmationPagePremiumNetflixSoc method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case : Test Adding and Removing Netflix Premium SOC.");
		Reporter.log("Test Data : TMO TI Pooled PAH Msisdn Registered for Netflix with Premium");
		Reporter.log("==============================");
		Reporter.log("Verify Service Review page premium Netflix soc");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click On plan icon | Plan Page should be displayed");
		Reporter.log("4. Click on Change Services btn | Services List Page should be verified");
		Reporter.log("5. Verify FA Soc  | FA soc should displayed");
		Reporter.log(
				"6. Select standard Netflix. $10.99 with Fam Allowance ($16 value) Soc  | Nstandard Netflix. $10.99 with Fam Allowance ($16 value) Soc should be selected");
		Reporter.log("7. Click On Next button  | Services Review Page should be displayed");
		Reporter.log(
				"8. Verify Added FA soc  | standard Netflix. $10.99 with Fam Allowance ($16 value) Soc should be displayed in Review page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		manageAddOnsSelectionPage.checkStandardCheckBoxs();
		manageAddOnsSelectionPage.verifyAddingNetFlixStandatdSocDescription();
		manageAddOnsSelectionPage.verifyNetFlixDownGradingAlerts(
				"There's a conflict between the service you're adding and one on your account. If you add Netflix Standard $10.99 with Fam Allowances ($16 value) the following will be removed:");
		manageAddOnsSelectionPage.clickOnConflictModalCancelBtn();
		manageAddOnsSelectionPage.checkfamilyAllowanceCheckBoxs();
		manageAddOnsSelectionPage.verifyAddingFASocDescription();
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage
				.verifyNetFlixDownGradingAlertsRemovedService("Netflix Premium $13.99 with Fam Allowances ($19 value)");
	}

	/*
	 * Conflict and warning verification in UpGrading US294868
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void testverifyConflictMessagesWhileUPGradingNetFlixSoc(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyServiceReviewPageAndConfirmationPagePremiumNetflixSoc method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case : Test Adding and Removing Netflix Premium SOC.");
		Reporter.log("Test Data : TMO TI Pooled PAH Msisdn Registered for Netflix with Premium");
		Reporter.log("==============================");
		Reporter.log("Verify Service Review page premium Netflix soc");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click On plan icon | Plan Page should be displayed");
		Reporter.log("4. Click on Change Services btn | Services List Page should be verified");
		Reporter.log("5. Verify FA Soc  | FA soc should displayed");
		Reporter.log(
				"6. Select Netflix Prem. $13.99 with Fam Allowance ($19 value) Soc  | Netflix Prem. $13.99 with Fam Allowance ($19 value) should be selected");
		Reporter.log("7. Click On Next button  | Services Review Page should be displayed");
		Reporter.log(
				"8. Verify Added FA soc  | Netflix Prem. $13.99 with Fam Allowance ($19 value) should be displayed in Review page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		// if
		// (manageAddOnsSelectionPage.verifyFamilyAllowanceCheckBoxisSelected()
		// == false) {
		// addNetFLixFamilyAllowanceSoc(myTmoData);
		// }
		manageAddOnsSelectionPage.checkNetFlixPremiumSocCheckBox();
		manageAddOnsSelectionPage.verifyAddingNetFlixPremiumSocDescription();
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.verifyNetFlixUPGradingAlerts(
				"There's a conflict between the service you're adding and one on your account. If you add Netflix Premium $13.99 with Fam Allowances ($19 value) the following will be removed");
		manageAddOnsSelectionPage.acceptAddServicePopup();
		// manageAddOnsSelectionPage.verifyNetFlixUpGradingAlertsRemovedService("Netflix
		// Standard $10.99 with Fam Allowances ($16 value)");
	}

	/**
	 * US254187#FA for Free SOC not appearing in AddOns flow
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyConflictbetweenFAwithFMandFA(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyConflictbetweenFAwithFMandFA method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case : Verify conflict dialog box is displayed when two conflicting socs are selected in upgrade your services page");
		Reporter.log("Test Data :  PAH Customer with FAMALW0 SOC added");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7.Select FA service | it should conflict with FA with FM");
		Reporter.log("8.Verify Conflcit Dialog box| Conflict box should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnCheckBox("Netflix Standard $10.99 with Fam Allowances ($16 value)");
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.verifyConflictModal();
		manageAddOnsSelectionPage.verifyNetFlixDownGradingAlertsRemovedService("FamilyAllow for FamilyMode");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		// verifyDataPassReviewPage();
	}

	// 4708196846/Auto12345

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyBANLevelServicesToPAHInTheODFFlow(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBANLevelServicesToPAHInTheODFFlow method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : verify BAN Level Services To PAH In The ODFFlow");
		Reporter.log("Test Data : Any PAH user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log(
				"7.verify netflix premiun soc  present under Family Entertainment Section | netflix premiun soc should be present under Family Entertainment Section");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyNetFlixPremiumPresentUnderFamilyEntertainmentSection();
	}

	/**
	 *
	 * verifynaviagtionfromPlanBenefitspagetoAddons DE153288 navigation from
	 * plan-benefits page to manage Addon's page is breaking
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifynaviagtionfromPlanBenefitspagetoAddons(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifynaviagtionfromPlanBenefitspagetoAddons method called in ManageAddonsSelectionPage Test");
		Reporter.log("TestCase Name: verify naviagtion from Plan Benefits page to Addons");
		Reporter.log("Test Data : ");
		Reporter.log("========================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. verify home page | Phome page  should be displayed");
		Reporter.log("4. click on signup for netflix |plan benefits page  should be displayed");
		Reporter.log("5. Click sign up my netflix account button|Manage data and AddOns page should be displayed");
		Reporter.log("6. verify Family and entertainment header |Family and entertainment header should be displayed");
		Reporter.log(
				"7. verify family and entertainment  services | family and entertainment  services  should be displayed");
		Reporter.log("==============================");
		Reporter.log("Actual Output:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.verifyBenifitsPage();
		crazylegspage.inEligibleRatePlanHeaders("Good news!  You get Netflix, too.");
		crazylegspage.verifyButton("Set up Netflix on My Plan");
		crazylegspage.clickOnContactCareButton();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyFamilySectionSortingOrderdirectedfromplanbenefits();
	}

	/**
	 * US330094 Add-ons: Add Manage Family Allowance link below SOC description
	 * DE153044 Manage Family Allowance link is missing in Add ons page under
	 * Standard Netflix Description
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyManageFamilyAllowanceLinkunderStandardNetflixDescription(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyManageFamilyAllowanceLinkunderStandardNetflixDescription method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Manage Family Allowance Link underStandard Netflix Description");
		Reporter.log("Test Data : ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("7. Click on Expand netflix standard soc | Manage family allowances link should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.expandnetFlixStandardSoc();
		manageAddOnsSelectionPage.verifyAndClickOnManageFamilyAllowanceLink();
		manageAddOnsSelectionPage.verifyRedirectionOfManageFamilyAllowanceLink();
	}

	/**
	 * DE153059 existing service list page has family socs displayed as per price
	 * ascending order
	 *
	 * Add-ons: Family Allowance add-ons list order
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void testFamilyAllowanceSorting(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testFamilyAllowanceSorting method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Test Family Allowance Sorting Functionality in Addons page.");
		Reporter.log("Test Data : TMO TI Pooled PAH  Msisdn");

		Reporter.log("==============================");
		Reporter.log("Verify Service Review page premium Netflix soc");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click On plan icon | Plan Page should be displayed");
		Reporter.log("4. Click on Change Services btn | Services List Page should be verified");
		Reporter.log("5. Verify FA Soc  | FA soc should displayed");
		Reporter.log(
				"6. Verify Sorting order of family and entertainment 1) On US 2) ON US 3) $3.00/mo 4) 10$ bear mode");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyFamilySectionSortingOrder();
		Reporter.log("Netflix Family Sorting Order is displayed");
	}

	/**
	 * US330094 - Add-ons: Add Manage Family Allowance link below SOC description
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired0825")
	public void verifyManageFamilyAllowanceLinkUnderActiveNetflixPlan(ControlTestData data, MyTmoData myTmoData) {

		logger.info(
				"verifyManageFamilyAllowanceLinkUnderActiveNetflixPlan method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verifying Manage Family Allowance link under active Netflix soc on AddOn Selection.");
		Reporter.log(
				"Test Data : TMO One TI plan with Standard and premium Netflix socs active on it. Logged in with PAH/Full Msisdn");
		Reporter.log("==============================");
		Reporter.log("Verify Service Review page premium Netflix soc");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click On plan icon | Plan Page should be displayed");
		Reporter.log("4. Click on Manage Addon and Services CTA. | Add On selection page should be displayed.");
		Reporter.log("5. Click on downarrow of Active Netflix soc  | Netflix description should be displayed");
		Reporter.log("6. Check Manage Family Allowances link | Link should be displayed.");
		Reporter.log(
				"6. Click on Manage Family Allowances link | It should redirect to https://manage.my.t-mobile.com/FamilyAllowances/default.aspx");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyActiveNetflixPlanAndManageFAmilyAllowanceLink();
		// manageAddOnsSelectionPage.verifyManageFamilyAllowancesLink();
	}

	/**
	 * US331439 US331462 Add-ons - Warming modal on removing standard Netflix SOC
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testVerifyWarmingModalOnRemovingStandardNetflixSOC(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testVerifyWarmingModalOnRemovingStandardNetflixSOC method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : test Verify Warming Modal On Removing Standard Netflix SOC");
		Reporter.log("Test Data : PAH/Full/Standard User with Netflix standarad soc active");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log(
				"5. select on Netflix Standard $10.99 with Fam Allowances ($16 value)| Warming Modal On Removing Standard Netflix SOC  should be displayed");
		Reporter.log("6. verify continue removal button| continue removal button  should be displayed");
		Reporter.log("7. click on cancel button| upgrade services page should be displayed");
		Reporter.log(
				"8. verify Netflix Standard $10.99 with Fam Allowances ($16 value) in checked state| Netflix Standard $10.99 with Fam Allowances ($16 value) should be in checked state");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnCheckBox("Netflix Standard $10.99 with Fam Allowances ($16 value)");
		manageAddOnsSelectionPage.verifyWarningModalRemovingNetFlixisDisplayed();
		manageAddOnsSelectionPage.verifyWarningMsgRemovingNetFlixisDisplayed(
				"By removing Netflix Std. $10.99 w/Fam Allowances ($16 value) you'll lose Netflix on us, unless you add Netflix Prem. $13.99 w/Fam Allowances ($19 value) for $3.00/mo. This change will result in immediate removal of T-mobile as your method of payment, with potential changes to any previous method of payment on file with Netflix");
		manageAddOnsSelectionPage.verifyContinueRemovalBtnisDisplayed();
		manageAddOnsSelectionPage.clickCancelBtn();
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage
				.verifyCheckBoxSelectedByName("Netflix Standard $10.99 with Fam Allowances ($16 value)");
	}

	/**
	 * US331439 US331462 Add-ons - Warming modal on removing Premium Netflix SOC
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void testVerifyWarmingModalOnRemovingPremiumNetflixSOC(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testVerifyWarmingModalOnRemovingPremiumNetflixSOC method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : test Verify Warming Modal On Removing Premium Netflix SOC");
		Reporter.log("Test Data : PAH/Full/Standard User with Netflix Premium soc active");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log(
				"5. select on Netflix Premium $13.99 with Fam Allowances ($19 value)| Warming Modal On Removing Premium Netflix SOC  should be displayed");
		Reporter.log("6. verify continue removal button| continue removal button  should be displayed");
		Reporter.log("7. click on cancel button| upgrade services page should be displayed");
		Reporter.log(
				"8. verify Netflix Premium $13.99 with Fam Allowances ($19 value) in checked state| Netflix Premium $13.99 with Fam Allowances ($19 value) should be in checked state");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.clickManageaddOnButton();

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnCheckBox("Netflix Premium $13.99 with Fam Allowances ($19 value)");
		manageAddOnsSelectionPage.verifyWarningModalRemovingNetFlixisDisplayed();
		manageAddOnsSelectionPage.verifyWarningMsgRemovingNetFlixisDisplayed(
				"By removing Netflix Prem. $13.99 w/Fam Allowances ($19 value) you'll lose Netflix on us, unless you add Netflix Std. $10.99 w/Fam Allowances ($16 value) on us. This change will result in immediate removal of T-Mobile as your method of payment, with potential changes to any previous method of payment on file with Netflix");
		manageAddOnsSelectionPage.verifyContinueRemovalBtnisDisplayed();
		manageAddOnsSelectionPage.clickCancelBtn();
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage
				.verifyCheckBoxSelectedByName("Netflix Premium $13.99 with Fam Allowances ($19 value)");
	}

	/**
	 * user stories US322688 and US322799
	 */

	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyCategoryFunctionalityinManageAddonspage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyCategoryFunctionalityinManageAddonspage method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test case Name :Verify Category Functionality in Manage Addons page");
		Reporter.log("Test Data : Any PAH/STANDARD Msdsin");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Plan  | Plan Details page should be open");
		Reporter.log("4. Click on on manage data add Ons Button | manage data add Ons page should be open");
		Reporter.log("5. load family entertainment soc url|  family entertainment soc url should be displayed");
		Reporter.log(
				"6. verify only Family and Entertainment header| only Family and Entertainment header should be displayed");
		Reporter.log(
				"7.verify All family entertainment soc services|  All family entertainment soc services should be displayed as expected");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/Service:ALL/Group:B5B04A7D-615B-43F5-B44D-6F5773F77C8B");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		Assert.assertTrue(manageAddOnsSelectionPage.verifyOnlyFamilyAndEntertainmentHeaderDispalyed());
		manageAddOnsSelectionPage.verifyFamilyAndEntertainmentSocDispalyed("Family Allowances");
		manageAddOnsSelectionPage.verifyFamilyAndEntertainmentSocDispalyed("Family Mode");
		manageAddOnsSelectionPage
				.verifyFamilyAndEntertainmentSocDispalyed("Netflix Premium $13.99 with Fam Allowances ($19 value)");
		manageAddOnsSelectionPage
				.verifyFamilyAndEntertainmentSocDispalyed("Netflix Standard $10.99 with Fam Allowances ($16 value)");
	}

	/**
	 * user stories US322688 and US322799
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "release_ready")
	public void verifyNetflixServicesforSpecialTestPhonesAccount(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyNetflixServicesforSpecialTestPhonesAccount method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test case Name :Verify Category Functionality in Manage Addons page");
		Reporter.log("Test Data : Special test phone netflix eligible msisdn");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Plan  | Plan Details page should be open");
		Reporter.log("4. Click on on manage data add Ons Button | manage data add Ons page should be open");
		Reporter.log("5. load family entertainment soc url|  family entertainment soc url should be displayed");
		Reporter.log(
				"6. verify only Family and Entertainment header| only Family and Entertainment header should be displayed");
		Reporter.log(
				"7.verify All family entertainment soc services|  All family entertainment soc services should be displayed as expected");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/Service:ALL/Group:B5B04A7D-615B-43F5-B44D-6F5773F77C8B");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		Assert.assertTrue(manageAddOnsSelectionPage.verifyOnlyFamilyAndEntertainmentHeaderDispalyed());
		manageAddOnsSelectionPage.verifyFamilyAndEntertainmentSocDispalyed("Family Allowances");
		manageAddOnsSelectionPage.verifyFamilyAndEntertainmentSocDispalyed("Family Mode");
		manageAddOnsSelectionPage
				.verifyFamilyAndEntertainmentSocDispalyed("Netflix Premium $13.99 with Fam Allowances ($19 value)");
		manageAddOnsSelectionPage
				.verifyFamilyAndEntertainmentSocDispalyed("Netflix Standard $10.99 with Fam Allowances ($16 value)");
	}

	/**
	 * user stories US322688 and US322799
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyNetflixServicesforIndividualAssociationAccount(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"VerifyNetflixServicesforIndividualAssociationAccount method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test case Name :Verify Category Functionality in Manage Addons page");
		Reporter.log("Test Data : Netflix eligible individual Associate type customer");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Plan  | Plan Details page should be open");
		Reporter.log("4. Click on on manage data add Ons Button | manage data add Ons page should be open");
		Reporter.log("5. load family entertainment soc url|  family entertainment soc url should be displayed");
		Reporter.log(
				"6. verify only Family and Entertainment header| only Family and Entertainment header should be displayed");
		Reporter.log(
				"7.verify All family entertainment soc services|  All family entertainment soc services should be displayed as expected");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/Service:ALL/Group:B5B04A7D-615B-43F5-B44D-6F5773F77C8B");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		// Assert.assertTrue(manageAddOnsSelectionPage.verifyOnlyFamilyAndEntertainmentHeaderDispalyed());

		manageAddOnsSelectionPage.verifyFamilyAndEntertainmentSocDispalyed("Family Mode");
		manageAddOnsSelectionPage.verifyFamilyAndEntertainmentSocDispalyed("Family Allowances");
		// manageAddOnsSelectionPage
		// .verifyFamilyAndEntertainmentSocDispalyed("Netflix Standard $12.99
		// with Fam Allowances ($18 value)");
	}

	/**
	 * user stories US322688 and US322799
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyNetflixServicesforSpecialContractorccount(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"VerifyNetflixServicesforIndividualAssociationAccount method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test case Name :Verify Category Functionality in Manage Addons page");
		Reporter.log("Test Data : Special contractor pah msisdn");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Plan  | Plan Details page should be open");
		Reporter.log("4. Click on on manage data add Ons Button | manage data add Ons page should be open");
		Reporter.log("5. load family entertainment soc url|  family entertainment soc url should be displayed");
		Reporter.log(
				"6. verify only Family and Entertainment header| only Family and Entertainment header should be displayed");
		Reporter.log(
				"7.verify All family entertainment soc services|  All family entertainment soc services should be displayed as expected");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/Service:ALL/Group:B5B04A7D-615B-43F5-B44D-6F5773F77C8B");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		manageAddOnsSelectionPage.verifyFamilyAndEntertainmentSocDispalyed("Family Allowances");
		manageAddOnsSelectionPage.verifyFamilyAndEntertainmentSocDispalyed("Family Mode");
	}

	/**
	 * user stories US322688 and US322799
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "release_ready")
	public void verifyNetflixServicesforSpecialDealerAccount(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyNetflixServicesforSpecialDealerccount method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test case Name :Verify Category Functionality in Manage Addons page");
		Reporter.log("Test Data : Special delaer Pooled netflix eligible account");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Plan  | Plan Details page should be open");
		Reporter.log("4. Click on on manage data add Ons Button | manage data add Ons page should be open");
		Reporter.log("5. load family entertainment soc url|  family entertainment soc url should be displayed");
		Reporter.log(
				"6. verify only Family and Entertainment header| only Family and Entertainment header should be displayed");
		Reporter.log(
				"7.verify All family entertainment soc services|  All family entertainment soc services should be displayed as expected");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/Service:ALL/Group:B5B04A7D-615B-43F5-B44D-6F5773F77C8B");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		// Assert.assertTrue(manageAddOnsSelectionPage.verifyOnlyFamilyAndEntertainmentHeaderDispalyed());
		manageAddOnsSelectionPage.verifyFamilyAndEntertainmentSocDispalyed("Family Allowances");
		manageAddOnsSelectionPage.verifyFamilyAndEntertainmentSocDispalyed("Family Mode");
		manageAddOnsSelectionPage
				.verifyFamilyAndEntertainmentSocDispalyed("Netflix Premium $13.99 with Fam Allowances ($19 value)");
		manageAddOnsSelectionPage
				.verifyFamilyAndEntertainmentSocDispalyed("Netflix Standard $10.99 with Fam Allowances ($16 value)");
	}

	/**
	 * user stories US322688 and US322799
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifySupressServicesFunctionalityinManageAddonspage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"VerifySupressServicesFunctionalityinManageAddonspage method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test case Name :Verify Supress Services Functionality in ManageAddonspage");
		Reporter.log("Test Data : Any PAH/STANDARD Msdsin");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Plan  | Plan Details page should be open");
		Reporter.log("4. Click on on manage data add Ons Button | manage data add Ons page should be open");
		Reporter.log("5. load family entertainment soc url|  family entertainment soc url should be displayed");
		Reporter.log(
				"6. verify All headers on Manage Addons Page|  All headers on Manage Addons Page  should be displayed");
		Reporter.log(
				"7.verify Netflix Standard $10.99 with Fam Allowances ($16 value) not displayed|  Netflix Standard $10.99 with Fam Allowances ($16 value) should not be not displayed as expected");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/Service:ALL/Sup:FAMNFX9");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyfamilyandEntertainmentHeader();
		manageAddOnsSelectionPage
				.verifyFamilyAndEntertainmentSocNotDisplayed("Netflix Standard $10.99 with Fam Allowances ($16 value)");
	}

	/**
	 * user stories US322688 and US322799
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyCategoryAndSupressFunctionalityinManageAddonsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyCategoryAndSupressFunctionalityinManageAddonsPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test case Name :verify Category And SupressFunctionalityinManageAddonsPage");
		Reporter.log("Test Data : Any PAH/STANDARD Msdsin");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Plan  | Plan Details page should be open");
		Reporter.log("4. Click on on manage data add Ons Button | manage data add Ons page should be open");
		Reporter.log("5. load family entertainment soc url|  family entertainment soc url should be displayed");
		Reporter.log(
				"6. verify only Family and Entertainment header| only Family and Entertainment header should be displayed");
		Reporter.log(
				"7.verify Netflix Standard $10.99 with Fam Allowances ($16 value) not displayed|  Netflix Standard $10.99 with Fam Allowances ($16 value) should not be not displayed as expected");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:ALL/Group:B5B04A7D-615B-43F5-B44D-6F5773F77C8B/Sup:FAMNFX9");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());

		// Assert.assertTrue(manageAddOnsSelectionPage.verifyOnlyFamilyAndEntertainmentHeaderDispalyed());
		manageAddOnsSelectionPage
				.verifyFamilyAndEntertainmentSocNotDisplayed("Netflix Standard $10.99 with Fam Allowances ($16 value)");
	}

	/**
	 * US262791
	 *
	 * Crazy Legs phase 2 - Service Review page page -Display the price for premium
	 * Netflix soc as $3
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true)
	public void testVerifyPriceforPremiumNetflixSocServiceReviewPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testVerifyPriceforPremiumNetflixSocServiceReviewPage method called in PlanBenefitsPageTest");
		Reporter.log(
				"Test Case : Test Customer with pending(future dated) Premium netflix Service in Service pending page.");
		Reporter.log("Test Data : PAH Msisdn with Premium netflix Service Added with Future dated");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | odf page should be displayed");
		Reporter.log("6. verify price for premium Netflix soc |price for premium Netflix soc should be $3");
		Reporter.log("========================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifypriceforpremiumNetflixSoc();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyPappyTIisEligibleforNetflix(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyPappyTIisEligibleforNetflix method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Test Netflix Service is present for PAppy TI.");
		Reporter.log("Test Data : Pappy TI Pooled PAH  Msisdn");
		Reporter.log("==============================");

		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click On plan icon | Plan Page should be displayed");
		Reporter.log("4. Click on Change Services btn | Services List Page should be verified");
		Reporter.log("5. Verify Family and Entertainment header and Services");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyNetFlixPremiumPresentUnderFamilyEntertainmentSection();
		Reporter.log("Netflix Family Services are displayed forPappyTI");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, Group.MULTIREG })
	public void verifyMilitaryBanisEligibleforNetflix(ControlTestData data, MyTmoData myTmoData) {

		logger.info(
				"VerifyPaVerifyMilitaryBanisEligibleforNetflixppyTIisEligibleforNetflix method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Test Netflix Service is present for PAppy Military.");
		Reporter.log("Test Data : Pooled Military Ban");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click On plan icon | Plan Page should be displayed");
		Reporter.log("4. Click on Change Services btn | Services List Page should be verified");
		Reporter.log("5. Verify Family and Entertainment header and Services");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyNetFlixPremiumPresentUnderFamilyEntertainmentSection();
	}

	/**
	 * VerifyMagentaPlusNetflixServices
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMagentaPlusNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMagentaPlusNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Magenta plus is eligible for 2SNFLXHDP and 4SNFLXHDP");
		Reporter.log("Test Data :  - MGPLSFM rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMagentaPlus9NetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMagentaPlus9LinesNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMagentaPlus9LinesNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Magenta plus 9 lines is eligible for 2SNFLXHDP and 4SNFLXHDP");
		Reporter.log("Test Data :  - MGPLSFM9 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMilitaryPlusSevenLineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMilitaryPlusSevenLineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMilitaryPlusSevenLineNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Military plus 7 line is eligible for 2SNFLXHDP and 4SNFLXHDP");
		Reporter.log("Test Data :  - MPLSMLFM9 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyPlusNetflixServicesareDispalyed();
	}

	// -------------------------------

	/**
	 * VerifyMilitaryPlusNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMilitaryPlusNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMilitaryPlusNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Military plus  lines is eligible for 2SNFLXHDP and 4SNFLXHDP");
		Reporter.log("Test Data :  - MPLSMLFM rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyAmplifiedNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyAmplifiedNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyAmplifiedNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Amplified is eligible for 1SNFLXSD,2SNFLXHD and 4SNFLXHDP");
		Reporter.log("Test Data :  - MGAMP2 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyNONPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyAmplifiedNineLineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyAmplifiedNineLineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyAmplifiedNineLineNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Amplified Nine Line is eligible for 1SNFLXSD,2SNFLXHD and 4SNFLXHDP");
		Reporter.log("Test Data :  - MGAMP9 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyNONPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMagentaTwoLineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMagentaTwoLineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMagentaTwoLineNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Magenta Two Line is eligible for 1SNFLXSD,2SNFLXHD and 4SNFLXHDP");
		Reporter.log("Test Data :  - MAGENTA2 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyNONPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMagentaNineLineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMagentaNineLineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMagentaNineLineNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Magenta 9 Line is eligible for 1SNFLXSD,2SNFLXHD and 4SNFLXHDP");
		Reporter.log("Test Data :  - MAGENTA9 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyNONPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMilitaryTwoLineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMilitaryTwoLineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMilitaryTwoLineNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Military two  lines is eligible for 1SNFLXSD,2SNFLXHD and 4SNFLXHDP");
		Reporter.log("Test Data :  - MGMIL2 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyNONPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMilitarySevenLineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMilitarySevenLineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMilitarySevenLineNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Military Seven  lines is eligible for 1SNFLXSD,2SNFLXHD and 4SNFLXHDP");
		Reporter.log("Test Data :  - MGMIL7 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyNONPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMilitaryBusinessTwoLineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMilitaryBusinessTwoLineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMilitaryBusinessTwoLineNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Military Business Two  lines is eligible for 1SNFLXSD,2SNFLXHD and 4SNFLXHDP");
		Reporter.log("Test Data :  - MGMILZ2 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyNONPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMilitaryBusinessSevenLineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMilitaryBusinessSevenLineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMilitaryBusinessSevenLineNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case : verify Military Business Seeven  lines is eligible for 1SNFLXSD,2SNFLXHD and 4SNFLXHDP");
		Reporter.log("Test Data :  - MGMILZ7 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyNONPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMagentaBusinessPlusNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMagentaBusinessPlusNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMagentaBusinessPlusNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Magenta Business Plus is eligible for 2SNFLXHDP and 4SNFLXHDP");
		Reporter.log("Test Data :  - ZMGPLS rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMagentaBusinessPlus9LineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMagentaBusinessPlus9LineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMagentaBusinessPlusNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Magenta Business Plus 9 Line is eligible for 2SNFLXHDP and 4SNFLXHDP");
		Reporter.log("Test Data :  - ZMGPLS9 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMilitaryBusinessPlusNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMilitaryBusinessPlusNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMilitaryBusinessPlusNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Military Business plus is eligible for 2SNFLXHDP and 4SNFLXHDP");
		Reporter.log("Test Data :  - ZMGMLBZ rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMilitaryBusinessPlus7LineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMilitaryBusinessPlus7LineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMilitaryBusinessPlus7LineNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Military Business plus 7 Lineis eligible for 2SNFLXHDP and 4SNFLXHDP");
		Reporter.log("Test Data :  - ZMGMLBZ7 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMagentaBusiness2LineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMagentaBusiness2LineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMagentaBusiness2LineNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Magenta Business plus 2 Line is eligible for 1SNFLXSD,2SNFLXHD and 4SNFLXHD");
		Reporter.log("Test Data :  - ZMAGENTA2 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyNONPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyMagentaBusiness9LineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyMagentaBusiness9LineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyMagentaBusiness9LineNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Magenta Business plus 9 Line is eligible for 1SNFLXSD,2SNFLXHD and 4SNFLXHD");
		Reporter.log("Test Data :  - ZMAGENTA9 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyOnlyNONPlusNetflixServicesareDispalyed();
	}

	/**
	 * VerifyUnlimited55PlusNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyUnlimited55PlusNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyUnlimited55PlusNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Unlimited 55 Plus is eligible for 2SNFLXHDP and 4SNFLXHDP");
		Reporter.log("Test Data :  - MGPLS552 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyMessageForFeatureNotAvailableDispalyed();
	}

	/**
	 * VerifyUnlimited55TwoLineNetflixServices
	 *
	 *
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void VerifyUnlimited55TwoLineNetflixServices(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyUnlimited55TwoLineNetflixServices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : verify Unlimited 55 2 Line is eligible for 1SNFLXSD,2SNFLXHD and 4SNFLXHD");
		Reporter.log("Test Data :  - MAG55TI2 rate plan Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2.Naviagte to Manage Addon's page and verify Eligible Netflix Services and their Prices");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyMessageForFeatureNotAvailableDispalyed();
	}

	/**
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "ll")
	public void testAddStandardNetflixSOCAndRemoveCompleteFlow(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testAddStandardNetflixSOCAndRemoveCompleteFlow method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : test Verify Warming Modal On Removing Standard Netflix SOC");
		Reporter.log("Test Data : PAH/Full/Standard User with Netflix standarad soc active");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log(
				"5. select on Netflix Standard $10.99 with Fam Allowances ($16 value)| Warming Modal On Removing Standard Netflix SOC  should be displayed");
		Reporter.log("6. verify continue removal button| continue removal button  should be displayed");
		Reporter.log("7. click on cancel button| upgrade services page should be displayed");
		Reporter.log(
				"8. verify Netflix Standard $10.99 with Fam Allowances ($16 value) in checked state| Netflix Standard $10.99 with Fam Allowances ($16 value) should be in checked state");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		if (!manageAddOnsSelectionPage
				.removeNetflixFA16Soc("Netflix Standard $10.99 with Fam Allowances ($16 value)")) {
			getManageAddonsNetflixUrl();
		}
		manageAddOnsSelectionPage.clickOnCheckBox("Netflix Standard $10.99 with Fam Allowances ($16 value)");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyNewAddOnsListText("Netflix Standard $10.99 with Fam Allowances ($16 value)");
		oDFDataPassReviewPage.agreeAndSubmitButton();

		ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		manageAddOnsConfirmationPage.verifyODFConfirmationPage();
		manageAddOnsConfirmationPage.clickOnNextStepSignUpBtn();

		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.verifyBenifitsPage();

		getManageAddonsNetflixUrl();

		manageAddOnsSelectionPage.clickdowncarotOfAddedNetflix();
		manageAddOnsSelectionPage.verifyAndClickOnManageNetflixLink();
		crazylegspage.verifyBenifitsPage();
		crazylegspage.verifyButton("Activate your Netflix offer");

		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		navigateToAnyURL("plan-benefits", "/account-overview");
		accountOverviewPage.verifyAccountOverviewPage();
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifySharedAddonComponent();
		plandetailspage.verifyforNetflixAddonsandPrice("Netflix Standard $10.99 with Fam Allowances ($16 value)",
				"On Us");
	}

	/**
	 *
	 * US563337 - May 2019 - MyTMO [D&M] - 1S messaging for Plan Benefit page
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void testAdd_1SNFLXSD_NetflixSOCAndRemoveCompleteFlow(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testAdd_1SNFLXSD_NetflixSOCAndRemoveCompleteFlow method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case :test end to end flow for 1SNFLXSD Service");
		Reporter.log("Test Data : PAH/Full non plues PLAN ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("5. select on 1SNFLXSD| Add on Pop message should be displayed");
		Reporter.log("6. verify continue add button| continue add button  should be displayed");
		Reporter.log("7. verify Add on SOC in Review Page");
		Reporter.log("8. verify Add on SOC on Confirmation page");
		Reporter.log("9. Click on Next step sign up button | Verify Plan-Benefits Page is Dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.removeNetflixFA16Soc("1 Screen SD Netflix $8.99");

		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		navigateToAnyURL("com", "com/plan-benefits");
		crazylegspage.verifyBenifitsPage();
		crazylegspage.clickOnSetUpNetFlixBtn();
		verifyManageDataAndAddOnsPage();

		manageAddOnsSelectionPage.clickOnCheckBox("1 Screen SD Netflix $8.99");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyNewAddOnsListText("1 Screen SD Netflix $8.99");
		oDFDataPassReviewPage.agreeAndSubmitButton();

		ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		manageAddOnsConfirmationPage.verifyODFConfirmationPage();
		manageAddOnsConfirmationPage.clickOnNextStepSignUpBtn();

		crazylegspage.verifyBenifitsPage();
		crazylegspage.verifyButton("Activate your Netflix offer");

		// Clicking Sign up Netflix button will take user to Netflix site to
		// complete the redemption.
		//      Verify 1S SOC links user to 2S Netflix plan

		navigateToAnyURL("com",
				"com/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		manageAddOnsSelectionPage.clickdowncarotOfAddedNetflix("1 Screen SD Netflix $8.99");
		manageAddOnsSelectionPage.verifyAndClickOnManageNetflixLink();

		crazylegspage.verifyBenifitsPage();

		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		navigateToAnyURL("plan-benefits", "/account-overview");
		accountOverviewPage.verifyAccountOverviewPage();
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifySharedAddonComponent();
		plandetailspage.verifyforNetflixAddonsandPrice("1 Screen SD Netflix $8.99", "On Us");
	}

	/**
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void testAdd_2SNFLXSD_NetflixSOCAndRemoveCompleteFlow(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testAdd_2SNFLXSD_NetflixSOCAndRemoveCompleteFlow method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case :test end to end flow for 2SNFLXSD Service");
		Reporter.log("Test Data : PAH/Full ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("5. select on 2SNFLXSD| Add on Pop message should be displayed");
		Reporter.log("6. verify continue add button| continue add button  should be displayed");
		Reporter.log("7. verify Add on SOC in Review Page");
		Reporter.log("8. verify Add on SOC on Confirmation page");
		Reporter.log("9. Click on Next step sign up button | Verify Plan-Benefits Page is Dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.removeNetflixFA16Soc("2 Screens HD Netflix $12.99");

		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		navigateToAnyURL("com", "com/plan-benefits");
		crazylegspage.verifyBenifitsPage();
		crazylegspage.clickOnSetUpNetFlixBtn();
		verifyManageDataAndAddOnsPage();

		manageAddOnsSelectionPage.clickOnCheckBox("2 Screens HD Netflix $12.99");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyNewAddOnsListText("2 Screens HD Netflix $12.99");
		oDFDataPassReviewPage.agreeAndSubmitButton();

		ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		manageAddOnsConfirmationPage.verifyODFConfirmationPage();
		manageAddOnsConfirmationPage.clickOnNextStepSignUpBtn();

		crazylegspage.verifyBenifitsPage();
		crazylegspage.verifyButton("Activate your Netflix offer");

		navigateToAnyURL("com",
				"com/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		manageAddOnsSelectionPage.clickdowncarotOfAddedNetflix("2 Screens HD Netflix $12.99");
		manageAddOnsSelectionPage.verifyAndClickOnManageNetflixLink();

		crazylegspage.verifyBenifitsPage();

		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		navigateToAnyURL("plan-benefits", "/account-overview");
		accountOverviewPage.verifyAccountOverviewPage();
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifySharedAddonComponent();
		plandetailspage.verifyforNetflixAddonsandPrice("2 Screens HD Netflix $12.99", "$4.00");
	}

	/**
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testAdd_3SNFLXSD_NetflixSOCAndRemoveCompleteFlow(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testAdd_3SNFLXSD_NetflixSOCAndRemoveCompleteFlow method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case :test end to end flow for 3SNFLXSD Service");
		Reporter.log("Test Data : PAH/Full ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("5. select on 3SNFLXSD| Add on Pop message should be displayed");
		Reporter.log("6. verify continue add button| continue add button  should be displayed");
		Reporter.log("7. verify Add on SOC in Review Page");
		Reporter.log("8. verify Add on SOC on Confirmation page");
		Reporter.log("9. Click on Next step sign up button | Verify Plan-Benefits Page is Dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.removeNetflixFA16Soc("4 Screens HD Netflix $15.99");

		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		navigateToAnyURL("com", "com/plan-benefits");
		crazylegspage.verifyBenifitsPage();
		crazylegspage.clickOnSetUpNetFlixBtn();
		verifyManageDataAndAddOnsPage();

		manageAddOnsSelectionPage.clickOnCheckBox("4 Screens HD Netflix $15.99");

		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyNewAddOnsListText("4 Screens HD Netflix $15.99");
		oDFDataPassReviewPage.agreeAndSubmitButton();

		ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		manageAddOnsConfirmationPage.verifyODFConfirmationPage();
		manageAddOnsConfirmationPage.clickOnNextStepSignUpBtn();

		crazylegspage.verifyBenifitsPage();
		crazylegspage.verifyButton("Activate your Netflix offer");

		navigateToAnyURL("com",
				"com/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		manageAddOnsSelectionPage.clickdowncarotOfAddedNetflix("4 Screens HD Netflix $15.99");
		manageAddOnsSelectionPage.verifyAndClickOnManageNetflixLink();

		crazylegspage.verifyBenifitsPage();

		// AccountOverviewPage accountOverviewPage = new
		// AccountOverviewPage(getDriver());
		// navigateToAnyURL("plan-benefits","/account-overview");
		// accountOverviewPage.verifyAccountOverviewPage();
		// accountOverviewPage.clickOnPlanName();
		//
		// PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		// plandetailspage.verifyPlanDetailsPageLoad();
		// plandetailspage.verifySharedAddonComponent();
		// plandetailspage.verifyforNetflixAddonsandPrice("4 Screens HD Netflix
		// $15.99", "$7.00");

	}

	/**
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testAdd_2SNFLXHDP_NetflixSOCAndRemoveCompleteFlow(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testAdd_2SNFLXHDP_NetflixSOCAndRemoveCompleteFlow method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case :test end to end flow for 2SNFLXHDP Service");
		Reporter.log("Test Data : PAH/Full ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("5. select on 2SNFLXHDP| Add on Pop message should be displayed");
		Reporter.log("6. verify continue add button| continue add button  should be displayed");
		Reporter.log("7. verify Add on SOC in Review Page");
		Reporter.log("8. verify Add on SOC on Confirmation page");
		Reporter.log("9. Click on Next step sign up button | Verify Plan-Benefits Page is Dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.removeNetflixFA16Soc("Netflix Premium plus Family Allowances ($21 value)");

		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		navigateToAnyURL("com", "com/plan-benefits");
		crazylegspage.verifyBenifitsPage();
		crazylegspage.clickOnSetUpNetFlixBtn();
		verifyManageDataAndAddOnsPage();

		manageAddOnsSelectionPage.clickOnCheckBox("Netflix Premium plus Family Allowances ($21 value)");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyNewAddOnsListText("Netflix Premium plus Family Allowances ($21 value)");
		oDFDataPassReviewPage.agreeAndSubmitButton();

		ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		manageAddOnsConfirmationPage.verifyODFConfirmationPage();
		manageAddOnsConfirmationPage.clickOnNextStepSignUpBtn();

		crazylegspage.verifyBenifitsPage();
		crazylegspage.verifyButton("Activate your Netflix offer");

		navigateToAnyURL("com",
				"com/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		manageAddOnsSelectionPage.clickdowncarotOfAddedNetflix("Netflix Premium plus Family Allowances ($21 value)");
		manageAddOnsSelectionPage.verifyAndClickOnManageNetflixLink();

		crazylegspage.verifyBenifitsPage();

		// AccountOverviewPage accountOverviewPage = new
		// AccountOverviewPage(getDriver());
		// navigateToAnyURL("plan-benefits","/account-overview");
		// accountOverviewPage.verifyAccountOverviewPage();
		// accountOverviewPage.clickOnPlanName();
		//
		// PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		// plandetailspage.verifyPlanDetailsPageLoad();
		// plandetailspage.verifySharedAddonComponent();
		// plandetailspage.verifyforNetflixAddonsandPrice("Netflix Premium plus
		// Family Allowances ($21 value)", "On Us");
	}

	/**
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void testAdd_4SNFLXHDP_NetflixSOCAndRemoveCompleteFlow(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testAdd_4SNFLXHDP_NetflixSOCAndRemoveCompleteFlow method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case :test end to end flow for 4SNFLXHDP Service");
		Reporter.log("Test Data : PAH/Full ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("5. select on 4SNFLXHDP| Add on Pop message should be displayed");
		Reporter.log("6. verify continue add button| continue add button  should be displayed");
		Reporter.log("7. verify Add on SOC in Review Page");
		Reporter.log("8. verify Add on SOC on Confirmation page");
		Reporter.log("9. Click on Next step sign up button | Verify Plan-Benefits Page is Dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		manageAddOnsSelectionPage.removeNetflixFA16Soc("4ScreenHD Nfx $15.99 MagentaPl");

		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		navigateToAnyURL("com", "com/plan-benefits");
		crazylegspage.verifyBenifitsPage();
		crazylegspage.clickOnSetUpNetFlixBtn();
		verifyManageDataAndAddOnsPage();

		manageAddOnsSelectionPage.clickOnCheckBox("4ScreenHD Nfx $15.99 MagentaPlus");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyNewAddOnsListText("4ScreenHD Nfx $15.99 MagentaPlus");
		oDFDataPassReviewPage.agreeAndSubmitButton();

		ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		manageAddOnsConfirmationPage.verifyODFConfirmationPage();
		manageAddOnsConfirmationPage.clickOnNextStepSignUpBtn();

		crazylegspage.verifyBenifitsPage();
		navigateToAnyURL("com",
				"com/odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		manageAddOnsSelectionPage.clickdowncarotOfAddedNetflix("4ScreenHD Nfx $15.99 MagentaPlus");
		manageAddOnsSelectionPage.verifyAndClickOnManageNetflixLink();
		crazylegspage.verifyBenifitsPage();
		crazylegspage.verifyButton("Activate your Netflix offer");

		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		navigateToAnyURL("plan-benefits", "/account-overview");
		accountOverviewPage.verifyAccountOverviewPage();
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifySharedAddonComponent();
		plandetailspage.verifyforNetflixAddonsandPrice("4ScreenHD Nfx $15.99 MagentaPlus", "$7.00");

	}

	/**
	 * Verifying Netflix Upgrade Flow from 1 Screen SD Netflix $8.99 to 4 Screens HD
	 * Netflix $15.99
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")

	public void testUpgradeDowngradeNetflixFlowFrom1ScreenSDTo4ScreensHD(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testUpgradeDowngradeNetflixFlowFrom1ScreenSDTo4ScreensHD method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : test Verify Netflix Upgrade Flow");
		Reporter.log("Test Data : PAH/Full/Standard User with Netflix standarad soc active");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("5. Verify Netfix plan exist");
		Reporter.log("6. Verify Netfix plan can be upgraded");
		Reporter.log(
				"7. Upgrade and downgrade  the Netflix plan from 1 Screen SD Netflix $8.99 to 4 Screens HD Netflix $15.99 and vice versa");
		Reporter.log("8. Click on Next step sign up button | Verify Plan-Benefits Page is Dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyUpgradeDowgradeFunctionality("1 Screen SD Netflix $8.99",
				"4 Screens HD Netflix $15.99");
	}

	/**
	 * Verifying Netflix Upgrade Flow from 2 Screens HD Netflix $12.99 to 4 Screens
	 * HD Netflix $15.99
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void testUpgradeDowngradeFromNetflixFlowFrom2ScreenHDTo4ScreensHD(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("testUpgradeNetflixFlowFrom2ScreenHDTo4ScreensHD method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : test Verify Netflix Upgrade Flow");
		Reporter.log("Test Data : PAH/Full/Standard User with Netflix standarad soc active");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("5. Verify Netfix plan exist");
		Reporter.log("6. Verify Netfix plan can be upgraded");
		Reporter.log(
				"7. Upgrade and downgrade from the Netflix plan from 2 Screens HD Netflix $12.99 to 4 Screens HD Netflix $15.99 and vise versa");
		Reporter.log("8. Click on Next step sign up button | Verify Plan-Benefits Page is Dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyUpgradeDowgradeFunctionality("2 Screens HD Netflix $12.99",
				"4 Screens HD Netflix $15.99");
	}

	/**
	 * Verifying Netflix Upgrade Downgrade Flow from 2ScreenHD Nfx $12.99
	 * MagentaPlus to 4ScreenHD Nfx $15.99 MagentaPlus
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testUpgradeDowngradeNetflixFlowFrom2ScreenHDMagentaPlTo4ScreensHDMagentaPl(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"testUpgradeDowngradeNetflixFlowFrom2ScreenHDMagentaPlTo4ScreensHDMagentaPl method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : test Verify Netflix Upgrade and downgrade flow Flow");
		Reporter.log("Test Data : PAH/Full/Standard User with Netflix standarad soc active");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("5. Verify Netfix plan exist");
		Reporter.log("6. Verify Netfix plan can be upgraded");
		Reporter.log(
				"7. Upgrade the Netflix plan from 2ScreenHD Nfx $12.99 MagentaPlus to 4ScreenHD Nfx $15.99 MagentaPlus");
		Reporter.log("8. verify Upgraded netfix plan to 4ScreenHD Nfx $15.99 MagentaPlus");
		Reporter.log("9. Click on Next step sign up button | Verify Plan-Benefits Page is Dispalyed");
		Reporter.log("10. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("11. Verify Netfix plan exist");
		Reporter.log(
				"12. Verify Netfix plan can be downgrade from 4ScreenHD Nfx $15.99 MagentaPlus to 2ScreenHD Nfx $12.99 MagentaPlus");
		Reporter.log("13. downgrade the Netflix plan to 2ScreenHD Nfx $12.99 MagentaPlus");
		Reporter.log("14. verify downgrade netfix plan to 2ScreenHD Nfx $12.99 MagentaPlus");
		Reporter.log("15. Click on Next step sign up button | Verify Plan-Benefits Page is Dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyUpgradeDowgradeFunctionality(
				"Netflix Standard plus Family Allowances ($18 value)",
				"Netflix Premium plus Family Allowances ($21 value)");
	}

	/**
	 *
	 * Verifying prices of non plus netflix services
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyPriceOfNONPlusNetflixPrices(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPriceOfNONPlusNetflixPrices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : test Verify Netflix proces");
		Reporter.log("Test Data : PAH/Full/ user with non plus netflix services");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("5. Verify Netfix plan exist");
		Reporter.log("6. Verify proces of all 3 netflix servies");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("Netflix Basic for T-Mobile ($8.99 value)", "On us");
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("Netflix Standard for T-Mobile ($12.99 value)", "$4.00/mo");
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("Netflix Premium for T-Mobile ($15.99 value)", "$7.00/mo");
		// manageAddOnsSelectionPage.verifypriceforNetflixSoc("Netflix Premium
		// plus Family Allowances ($21 value)", "$7.00/mo");
		manageAddOnsSelectionPage.verifyNetflixServicesSize(3);
	}

	/**
	 *
	 * Verifying prices of plus netflix services
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyPriceOfPlusNetflixPrices(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPriceOfPlusNetflixPrices method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : test Verify Netflix proces");
		Reporter.log("Test Data : PAH/Full/ user with  plus netflix services");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Load  ODF url | upgrade services page should be displayed");
		Reporter.log("5. Verify Netfix plan exist");
		Reporter.log("6. Verify prices of all 2 plus netflix servies");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("Netflix Standard for T-Mobile ($12.99 value)", "On us");
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("Netflix Premium for T-Mobile ($15.99 value)", "$3.00/mo");
		// manageAddOnsSelectionPage.verifypriceforNetflixSoc("Netflix Standard
		// plus Family Allowances ($18 value)","On us");
		// manageAddOnsSelectionPage.verifypriceforNetflixSoc("Netflix Premium
		// plus Family Allowances ($21 value)","$3.00/mo");
		manageAddOnsSelectionPage.verifyNetflixServicesSize(2);
	}

	/**
	 * US567937 - Selection page - Update pending plan change message
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void getServiceName(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyMessageOnSelectionPageWhenUserHasPendingRatePlanChange method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Message on Selection page when account has pending rate plan");
		Reporter.log("Test Data : Any account with Pending rate plan, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log("5. Click On View Changes link on Plan page.| Plan Services Pending page should be displayed");
		Reporter.log("6. Check message on AddOn Selection page. | Message "
				+ "This account has a pending change, which takes effect on <next bill cycle date>.  To make any additional changes, please contact Customer Care."
				+ " should be displayed");
		Reporter.log("7. Check date in message.| Message should have next bill cycle date in correct format");
		Reporter.log("8. Check Customer Care link.| It should redirect to /contact-us page in same browser.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		// navigateToManageAddonsPage(myTmoData);
		navigateToODFURlWithMsisdn(myTmoData, "account-overview");
		verifyManageAddOnsPage();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		String serviceName = manageAddOnsSelectionPage.getCurrentlyActiveFutureDatedText();

		System.out.println("Service name " + serviceName);
		String activeService[];
		activeService = serviceName.split("-");
		String futureDatedService = activeService[0].trim();
		System.out.println("Future dated service name is " + futureDatedService);

		// manageAddOnsSelectionPage.verifyCheckBoxByNameIsSelected(futureDatedService);
		String newFutureDatedServiceName = manageAddOnsSelectionPage.getFutureDatedServiceNames();
		System.out.println("New Future dated service is " + newFutureDatedServiceName);
		manageAddOnsSelectionPage.clickOnCheckBoxByName(newFutureDatedServiceName);
		manageAddOnsSelectionPage.checkForConflictModal();
		// manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();

		ODFDataPassReviewPage odfDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		odfDataPassReviewPage.clickChangeDateLinknotdisplayed();

		// manageAddOnsSelectionPage.checkPendingRatePlanMessage();
		// manageAddOnsSelectionPage.clickOnCustomerCareLink();
		// manageAddOnsSelectionPage.verifyContactUsPageDispalyed();
	}

	/**
	 * US578365 - Improve line selector - Navigation back from line selector page
	 * US579766 - Improve line selector - Authorable page header for line selector
	 * page
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyHeaderAndBackNavigationFromLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBackNavigationFromLineSelectorPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify back navigation from Line selector page");
		Reporter.log("Test Data : Any account with Addons page access, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log("5. Click On Select Different line link.| Line selector page should be displayed");
		Reporter.log("6. Check header on Line Selector page.| Header should be 'Select a line to Manage'");
		Reporter.log("7. Click on Back Nav arrow. | User should be redirected to Manage Addons page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnSelectDifferentLineLink();

		AddonsLineSelectorPage addonsLineSelectorPage = new AddonsLineSelectorPage(getDriver());
		addonsLineSelectorPage.verifyLineSelectorPage();
		addonsLineSelectorPage.verifyHeaderOnLineSelectorPage();
		addonsLineSelectorPage.selectTheLine();
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

	}

	/**
	 * US586022 - Improve line selector - Order of lines displayed
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkOrderOfLinesInLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkOrderOfLinesInLineSelectorPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify back navigation from Line selector page");
		Reporter.log("Test Data : Any account with Addons page access, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log("5. Click On Select Different line link.| Line selector page should be displayed");
		Reporter.log(
				"6. Check order of lines on Line Selector page. | It should be like logged in user first, then all GSM lines and then MBB line.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		navigateToODFURlWithMsisdn(myTmoData, "account-overview");
		verifyManageAddOnsPage();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnSelectDifferentLineLink();

		AddonsLineSelectorPage addonsLineSelectorPage = new AddonsLineSelectorPage(getDriver());
		addonsLineSelectorPage.verifyLineSelectorPage();
		addonsLineSelectorPage.verifyLinesOnLineSelectorPage();
	}

	/**
	 * US586852 - Integration Next steps - DO NOT Suppress return CTA on add-ons
	 * confirmation page (updated) US599773#Integration Next steps - Suppress return
	 * CTA on add-ons confirmation page when Next Steps component is displayed
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkReturnToHomeCTAOnConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkOrderOfLinesInLineSelectorPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify back navigation from Line selector page");
		Reporter.log("Test Data : Any account with Addons page access, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log("5. Click On any service and submit it.| Review page should be displayed.");
		Reporter.log("6. Click On any Agree and Submit CTA. | Confirmation page should be displayed.");
		Reporter.log("7. Check 'Return to Home' CTA. | CTA should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		String name = "Name ID"; // myTmoData.getplanname();
		manageAddOnsSelectionPage.selectOrUnselectService(name);
		// manageAddOnsSelectionPage.clickOnConflictContinue();
		submitChangeToReviewPageAndThenConfirmationPage(name);
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.checkAndClickOnReturnToHomeCTA();

		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();

	}

	/**
	 * US587440 - Integration Next steps - Do not display component on non-eligible
	 * partner benefit (Netflix) SOC addition US586156 - Integration Next steps -
	 * Display component built by FrozenMonkeys on Manage add-ons confirmation page
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void checkExistingAndNewCopyOfNetflixOnConfirmationPageWhenUserSelectsNetflixSoc(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"checkExistingAndNewCopyOfNetflixOnConfirmationPageWhenUserSelectsNetflixSoc method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify existing and New copy of netflix on confirmation page when user has selected Standard Netflix soc");
		Reporter.log("Test Data : Any account with Addons page access, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log("5. Click On Netflix standard SOC and submit it.| Review page should be displayed.");
		Reporter.log("6. Click On any Agree and Submit CTA. | Confirmation page should be displayed.");
		Reporter.log("7. Check 'Netflix' CTA. | CTA should not be displayed.");
		Reporter.log("8. Check New copy of Netflix. | New Netflix copy should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		navigateToODFURlWithMsisdn(myTmoData, "account-overview");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		String name = myTmoData.getplanname();

		checkWhetherServiceIsSelectedAndThenUncheckThatServiceAndSubmitChanges(name);
		checkExistingCopyAndNewCopyOfNetflixSocOnConfirmationPage(name);
	}

	/**
	 * US567964 - Selection page - No pending plan: Enable check box for
	 * future-dated service add (lowest priority) *
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkWhetherUserAbleToUncheckFutureDatedServiceOrNot(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"checkWhetherUserAbleToUncheckFutureDatedServiceOrNot method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify whether user able to uncheck future dated service or not");
		Reporter.log("Test Data : Any account with Future dated service on it, user should be PAH/Full");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log(
				"5. Check whether Future dated service already added on it ot not. | Future dated service should exist.");
		Reporter.log("6. UnCheck future dated service. | User should be able to uncheck future dated service.");
		Reporter.log("7. Click Continue .| Review page should be loaded.");
		Reporter.log("8. Check Next Bill cycle option. | Next Bill cycle option should not be displayed.");
		Reporter.log("9. Click on Agree & Submit CTA. | Confirmation page should be displayed.");
		Reporter.log(
				"10. Goto ODF page and check wehther that service unchecked or not. | Service should be displayed in unselected state.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		// navigateToManageAddonsPage(myTmoData);
		navigateToODFURlWithMsisdn(myTmoData, "account-overview");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		/*
		 * String serviceName =
		 * manageAddOnsSelectionPage.getCurrentlyActiveFutureDatedText();
		 *
		 * System.out.println("Service name "+serviceName); String activeService[];
		 * activeService = serviceName.split("-"); String futureDatedService =
		 * activeService[0].trim(); System.out.println(
		 * "Future dated service name is "+futureDatedService);
		 *
		 * //manageAddOnsSelectionPage.verifyCheckBoxByNameIsSelected(
		 * futureDatedService);
		 */
		String newFutureDatedServiceName = manageAddOnsSelectionPage.getFutureDatedServiceNames();
		System.out.println("New Future dated service is " + newFutureDatedServiceName);
		manageAddOnsSelectionPage.clickOnCheckBoxByName(newFutureDatedServiceName);
		manageAddOnsSelectionPage.checkForConflictModal();
		// manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();

		ODFDataPassReviewPage odfDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		odfDataPassReviewPage.clickChangeDateLinknotdisplayed();
		odfDataPassReviewPage.reviewPageWaitForSpinner();
		odfDataPassReviewPage.clickOnAgreeandSubmitButton();

		ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		manageAddOnsConfirmationPage.verifyODFConfirmationPage();
		manageAddOnsConfirmationPage.clickOnReturnToHomeCTA();

		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
		navigateToODFURlWithMsisdn(myTmoData, "account-overview");
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		manageAddOnsSelectionPage.checkWhetherFutureDatedServiceUnselectedOrNot(newFutureDatedServiceName);
	}

	/**
	 * US580166 - [Continued] Enable add-ons with pending plan - No changes allowed
	 * & Copy update for Restricted and Standard permission users
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkMessageForStandardUserWhenUserDoesNotHaveAnyPendingChanges(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"checkMessageForStandardUserWhenUserDoesNotHaveAnyPendingChanges method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify message for standard user when account doesnot have any pending changes.");
		Reporter.log("Test Data : Any account without pending changes, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log(
				"5. Check message on selection page. | Message ''Looking to make additional changes? Please reach out to your primary account holder.' should be displayed.");
		Reporter.log("6. Check radio buttons for data passes. | Radio buttons should be enabled.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		navigateToODFURlWithMsisdn(myTmoData, "account-overview");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		manageAddOnsSelectionPage.checkMessageForStandardOrRestrictedUsers();
		manageAddOnsSelectionPage.checkRadioButtonsForDataPassesAreDisplayedOrNotForStandardOrRestrictedUsers();
	}

	/**
	 * US580166 - [Continued] Enable add-ons with pending plan - No changes allowed
	 * & Copy update for Restricted and Standard permission users
	 *
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkMessageForStandardUserWhenUserHasAnyPendingChanges(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"checkMessageForStandardUserWhenUserHasAnyPendingChanges method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify message for standard user when account have any pending changes and check data pass options");
		Reporter.log("Test Data : Any account without pending changes, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log(
				"5. Check message on selection page. | Message ''Looking to make additional changes? Please reach out to your primary account holder.' should be displayed.");
		Reporter.log("6. Check radio buttons for data passes. | Radio buttons should not be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		navigateToODFURlWithMsisdn(myTmoData, "account-overview");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		manageAddOnsSelectionPage.checkMessageForStandardOrRestrictedUsers();
		manageAddOnsSelectionPage
				.checkRadioButtonsForDataPassesForStandardOrRestrictedUsersWhenAccountHasPendingChanges();
	}

	/**
	 * US596760#[Test Only] Tinbox Ph2 - Manage Add-ons page: No conflict between
	 * Family Allowance and new Netflix SOCs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void verifynoconflictBetweenFamilyAllowanceAndNewNetflixSOCs(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Check for the no conflict modal display for the new Netflix soc when adding family allowance");
		Reporter.log(
				"Test Case  Name : Verify message for standard user when account have any pending changes and check data pass options");
		Reporter.log("Test Data : Any account without pending changes, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log(
				"5. check for the new netflix premium soc. | should be able to select the new Netflix premium soc.");
		Reporter.log(
				"6. Check for radio button for family allowance. | Should be able to select the family allowance.");
		Reporter.log(
				"7. Check the Add service popup is display instead of conflict | Should be able to see Add service popup");
		Reporter.log(
				"7. Check the Add service popup is display instead of conflict | Should be able to see Add service popup");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.checkfamilyAllowanceCheckBoxs().acceptAddServicePopup().clickOnContinueBtn();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testPendingChangesSelectionAddOn(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkfortheselectionAddonPendingChagnes");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log(
				"5. Check message on selection page. | Message ''Looking to make additional changes? Please reach out to your primary account holder.' should be displayed.");
		Reporter.log("6. Check for the elligible addons. | Addons should be enable for selection");
		Reporter.log(" 7. Select the enable addon | Should be able to add the addon");
		Reporter.log("8. click continue and go to confirmation page | Should be go to confirmation page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyPendingChangesDataOnAddOnsPage();
		manageAddOnsSelectionPage.checkfamilyAllowanceCheckBoxs();
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
	}

	/**
	 * US588849#Improve line selector - Remove line selector dropdown from Manage
	 * add-ons selection page US572052#Deeplink to Review for services - Eligible
	 * service; no conflict; no pending rate plan change
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testNewLineSelectorDropdownInManageAddOnsSelectionPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testNewLineSelectorDropdownInManageAddOnsSelectionPage method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify message for standard user when account have any pending changes and check data pass options");
		Reporter.log("Test Data : Any account without pending changes, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		// New line selector dropdown
	}

	/**
	 * US600899#Tinbox Ph2: Enable HD Video link on Manage Add on page for HDVPASS0
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testManageAddOnsSelectionPageEnableHDVideolinkHDVPASS0(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testManageAddOnsSelectionPageEnableHDVideolinkHDVPASS0 method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify message for standard user when account have any pending changes and check data pass options");
		Reporter.log("Test Data : Any account without pending changes, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		// Enable HD Video link on Manage Add on page for HDVPASS0
	}

	/**
	 * US601399#Tinbox Ph2 - Manage Add-ons page: Remove ‘Manage Netflix’ & ‘Manage
	 * Family Allowance‘ links
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testManageAddOnsSelectionPageManageNetflixAndFamilyAllowanceLinks(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"testManageAddOnsSelectionPageManageNetflixAndFamilyAllowanceLinks method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify message for standard user when account have any pending changes and check data pass options");
		Reporter.log("Test Data : Any account without pending changes, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		// Then do not show these links below SOC description (as they will be
		// available as part of the description) - 
		// Manage Netflix 
		// Manage Family allowance 
	}

	/**
	 * US604881#Tinbox Ph2 - Accurate Netflix price display - S15
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testAccurateNetflixprices(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testAccurateNetflixprices method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify message for standard user when account have any pending changes and check data pass options");
		Reporter.log("Test Data : Any account without pending changes, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();

		// CFS SOC xcatentry_DiscountedPrice
		// NFX 1SSDNFLX $0
		// NFX HD2SNFLX $4
		// NFX HD4SNFLX $7
		// NFX 2SPHDNFLX $0
		// NFX 4SPHDNFLX $3
		// MAP 1SNFLXSD $0
		// MAP 2SNFLXHD $4
		// MAP 4SNFLXHD $7
		// MAP 2SNFLXHDP $0
		// MAP 4SNFLXHDP $3
		// MAP FAMNFX9 $2
		// MAP FAMNFX13 $5 
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testLabelMostPopularFor2SNetflixSoc(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testAccurateNetflixprices method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case  Name : Verify message for standard user when account have any pending changes and check data pass options");
		Reporter.log("Test Data : Any account without pending changes, user should be Standard/Restricted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Manage Addons link| ADDons page should be displayed");
		Reporter.log("5. Check for the recomded label link | Should be able to see the recomended lable");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = navigateToManageAddOnsFlowFromHomeLink(myTmoData);
		manageAddOnsSelectionPage
				.checkForMostPopularLabelOnlyFor2Screen("Netflix Standard for T-Mobile ($12.99 value)");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void verifyDeeplinkInternationalDataPass(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Verify the deeplink for the international data pass");
		Reporter.log("Test Case  Name : Verify the deeplink for the International data pass");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the Deeplink url | User Should be logged in successfully");
		Reporter.log("3. Verify data passes | Data passes should be visible");
		Reporter.log("4. Select the data pass| Data pass should be selected");
		Reporter.log("5. Check for the continue button | Continue button should be enabled");

		navigateToFutureURLFromHome(myTmoData, "/account/odf/DataPass:ALL/DPGroup:INTERNATIONAL_ROAMING_DATA");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnDataPassRadioButton();
		manageAddOnsSelectionPage.verifyContinueBtnIsEnabled();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void testSelectInternationDayPassFromNewGroupCategoryPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testSelectInternationDayPassFromNewGroupCategoryPage method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case  Name : test Select Internation Day Pass From New Group Category Page");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log("6. Load the Data pass International categeory | Data pass Internation category page should load");

		navigateToFutureURLFromHome(myTmoData, "/account/odf/DataPass:ALL/DPGroup:INTERNATIONAL_ROAMING_DATA");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
		manageAddOnsSelectionPage.clickOnCurrentlyActiveCheckBoxe();
		manageAddOnsSelectionPage.verifyContinueBtnIsEnabled();
	}

	@Test(dataProvider = "byColumnName", enabled = true,groups = { Group.RELEASE })
	public void testTetheringSocUnderHotspotDataExtrasForMIRatePlans(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testSelectInternationDayPassFromNewGroupCategoryPage method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case  Name : test Select Internation Day Pass From New Group Category Page");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"6. Check for the Tethering soc displays under Hotspot Data Extras | Tethering Soc should be displayed at the Hotspot Data Extras");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = navigateToManageAddOnsFlowFromHomeLink(myTmoData);
		manageAddOnsSelectionPage.verifyHotspotAndDataExtrasSection();
		//manageAddOnsSelectionPage.checkForTetheringSocIsUnderHotspotAndDataExtras("Tmobile one plus");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void testAddingTetheringSocUnderHotspotDataExtras(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testSelectInternationDayPassFromNewGroupCategoryPage method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case  Name : test Select Internation Day Pass From New Group Category Page");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"6. Check for the Tethering soc displays under Hotspot Data Extras | Tethering Soc should be displayed at the Hotspot Data Extras");
		Reporter.log("7. Add the tethering soc | Should be able to add tethering soc successfully");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = navigateToManageAddOnsFlowFromHomeLink(myTmoData);
		manageAddOnsSelectionPage.verifyHotspotAndDataExtrasSection();
		manageAddOnsSelectionPage.checkForTetheringSocIsUnderHotspotAndDataExtras("Data Extras");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void testTetheringSocSuppressForNoDataPlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testSelectInternationDayPassFromNewGroupCategoryPage method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case  Name : test Select Internation Day Pass From New Group Category Page");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log("6. Check for the Tethering soc suppress | Tethering Soc should not be displayed");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = navigateToManageAddOnsFlowFromHomeLink(myTmoData);
		manageAddOnsSelectionPage.verifyNoDataPlanIsSelected();
		manageAddOnsSelectionPage.verifyHotspotAndDataExtrasSectionisNotPresent();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testTetheringSocSuppressForNoDataPlanForPendingChanges(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testSelectInternationDayPassFromNewGroupCategoryPage method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case  Name : test Select Internation Day Pass From New Group Category Page");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log("6. Check for the Tethering soc suppress | Tethering Soc should not be displayed");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = navigateToManageAddOnsFlowFromHomeLink(myTmoData);
		manageAddOnsSelectionPage.verifyNoDataPlanIsSelected();
		manageAddOnsSelectionPage.verifyHotspotAndDataExtrasSectionisNotPresent();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void testTetheringSocDeeplink10GBHSDAT(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testSelectInternationDayPassFromNewGroupCategoryPage method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case  Name : test Select Internation Day Pass From New Group Category Page");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log("6. Check for the deeplink 10GBHSDAT | Should be able to see the 10GBHSDAT soc");

		navigateToFutureURLFromHome(myTmoData, "account/odf/DataService:10GBHSDAT");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyHotspotAndDataExtrasSection();
		manageAddOnsSelectionPage.checkForTetheringSocIsUnderHotspotAndDataExtras("Data Extras");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void testTetheringSocDeeplinkWithFilterUrl(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testSelectInternationDayPassFromNewGroupCategoryPage method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case  Name : test Select Internation Day Pass From New Group Category Page");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log("6. Check for the deeplink with Filter url | Should be able to see the 10GBHSDAT soc");

		navigateToFutureURLFromHome(myTmoData, "account/odf/DataService:ALL");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyHotspotAndDataExtrasSection();
		manageAddOnsSelectionPage.checkForTetheringSocIsUnderHotspotAndDataExtras("Data Extras");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void testHotspotDataExtrasConflictModel(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testSelectInternationDayPassFromNewGroupCategoryPage method called in ManageAddOnsConfirmationPageTest");
		Reporter.log("Test Case  Name : test Select Internation Day Pass From New Group Category Page");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log("6. Check for the conflict service 'No Data' | Should be able to see the 'No Data'");
		Reporter.log("7. Checkt the Nodata and see for the conflict model | Conflict model should be able to shown");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = navigateToManageAddOnsFlowFromHomeLink(myTmoData);
		manageAddOnsSelectionPage.verifyHotspotAndDataExtrasSection();
		manageAddOnsSelectionPage.verifyConflictModelOnSelectingNoData();

	}
}
