package com.tmobile.eservices.qa.global.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.NewContactUSPage;
import com.tmobile.eservices.qa.pages.shop.CheckOrderPage;
import com.tmobile.eservices.qa.pages.shop.PhonePages;

public class PageBottomIconsTest extends GlobalCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(PageBottomIconsTest.class);

	/**
	 * Verify contact us link redirects to contact us page
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID, Group.IOS })
	public void verifyContacticon(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyContacticon data");
		Reporter.log("Verify contact us link redirects to contact us page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Contact Us link | Contact Us Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
	}

	/**
	 * Verify phone Trade-In Status link redirect to check order page
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID, Group.IOS })
	public void verifyTradeInStatus(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyTradeInStatus data");
		Reporter.log("Test Case : Verify phone Trade-In Status link redirect to check order page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on phone Link | Phone page should be displayed");
		Reporter.log("4. Click on Trade-In status link | Check order page should be displayed");
		Reporter.log("==============================");
		Reporter.log("Actual Results:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickPhoneMenu();
		PhonePages phonesPage = new PhonePages(getDriver());
		phonesPage.verifyPhonesPage();
		phonesPage.clickTradeInStatusLink();
		phonesPage.switchToWindow();

		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyCheckOrderPage();
	}

}
