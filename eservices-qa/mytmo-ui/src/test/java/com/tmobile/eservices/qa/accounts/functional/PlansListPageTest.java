package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.PlansListPage;

public class PlansListPageTest extends AccountsCommonLib {

	/**
	 * 
	 * US444597 - MBB Plans - Filtering in migration flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyMILineSeesOnlyMIOptionsInPlanListPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name : Verify MI Line Customer Sees only MI options in Plan List Page ");
		Reporter.log("Test Data : Single Line PAH/Full User on  SC MBB plan with MobileInternet 6 GB Plan");
		Reporter.log("========================");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Plans page | Plans Page Should be displayed");
		Reporter.log("5. Click on change eplan button | Plans List page should be displayed ");
		Reporter.log("6. Verify only MI Lines is Displayed");
		Reporter.log("==============================");
		Reporter.log("Actual Output:");

		navigateToPlanListPage(myTmoData);
		PlansListPage planslistpage = new PlansListPage(getDriver());
		planslistpage.verifyOptionsareOnlyMBB();
	}

	/**
	 * 
	 * US470772 - Suppress JUMPONDMI and NTDIGITTT socs on MYTMO
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "Sprint")
	public void verifySupressionOfLifetimeCoverageGuarantee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name : verify supression of LifetimeCoverageGuarantee  in planslist page ");
		Reporter.log("Test Data : SCNA with 22GB MI with Data stash");
		Reporter.log("========================");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Plans page | Plans Page Should be displayed");
		Reporter.log("5. Click on change eplan button | Plans List page should be displayed ");
		Reporter.log("6. Verify LifetimeCoverageGuarantee  is supressed");
		Reporter.log("==============================");
		Reporter.log("Actual Output:");

		navigateToPlanListPage(myTmoData); // 3nd line logic should change
											// 3606856494/Auto12345
		PlansListPage planslistpage = new PlansListPage(getDriver());
		planslistpage.verifysupressionofJUMPONDMI();
	}
}
