package com.tmobile.eservices.qa.galen;

import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.data.MyTmoData;

public class AccountsPages extends AccountsCommonLib {
	GalenLib galenLib = new GalenLib();

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAccountHistoryPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PlanPage","","");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanFeaturePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPlanFeaturesPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PlanFeaturePage","","");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanComparisionPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPlanComparisionPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PlanComparisionPage","","");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanConfigurePage(ControlTestData data, MyTmoData myTmoData) {
		//navigateToPlanConfigurationPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PlanConfigurePage","","");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	// @Test(dataProvider = "byColumnName", enabled = true,groups =
	// {"accounts"})
	public void testPlanConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
	//	navigateToPlanConfirmationPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PlanConfirmationPage","","");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanListPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPlanListPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PlanListPage","","");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanReviewPage(ControlTestData data, MyTmoData myTmoData) {
		//navigateToPlanReviewPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PlanReviewPage","","");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testManageAddonsSelectionPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToManageAddOnsSelectionPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "ManageAddonsSelectionPage","","");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testManageAddonsReviewPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToODFFreeRadioButtonToReviewPage(myTmoData, "Service");
		galenLib.doVisualTest(getDriver(), "ManageAddonsReviewPage","","");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	// @Test(dataProvider = "byColumnName", enabled = true,groups =
	// {"accounts"})
	public void testManageAddonsConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToManageAddOnsConfirmationPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "ManageAddonsConfirmationPage","","");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanBenefitsPage(ControlTestData data, MyTmoData myTmoData) {
		//navigateToPlanBenefitsPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PlanBenefitsPage","","");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPromotionsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToMyPromotionsPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PromotionsPage","","");
	}

}
