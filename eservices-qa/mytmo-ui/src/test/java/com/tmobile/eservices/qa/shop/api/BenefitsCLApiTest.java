package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.BenefitsCLApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class BenefitsCLApiTest extends BenefitsCLApi{
	
	public Map<String, String> tokenMap;
	/**
	 * UserStory# Description: Benefits Search Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */


	@Test(dataProvider = "byColumnName", enabled = true,priority = 1,  groups = {  "BenefitsCLApi","testBenefitsSearch", Group.SHOP,Group.PENDING})
	public void testBenefitsSearch(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Benefits Search");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the benefits of requested msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		String operationName="BenefitsSearch";
		String requestBody = "{ \r\n\"accountNumber\":\""+apiTestData.getBan()+"\",\r\n\"phoneNumber\":\""+apiTestData.getMsisdn()+"\",\r\n\"userRole\":\"PAH\"\r\n}";
		logRequest(requestBody, operationName);
		Response response = benefitsSearch(apiTestData, requestBody);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertEquals(apiTestData.getBan(), getPathVal(jsonNode,"benefits[0].ban"));
			Assert.assertNotNull(getPathVal(jsonNode,"benefits[0].ban"));
			tokenMap.put("benefitId", getPathVal(jsonNode, "benefits[0].partner"));
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory# Description: Benefits Eligibility status for requested misisdn
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "BenefitsCLApi", "testBenefitsEligibility", Group.SHOP,Group.APIREG })
	public void testBenefitsEligibility(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Benefits Eligibility status for requested misisdn");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results eligibility status of requested misisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="benefitsEligibility";
		String requestBody = "{ \r\n\"accountNumber\":\""+apiTestData.getBan()+"\",\r\n\"phoneNumber\":\""+apiTestData.getMsisdn()+"\",\r\n\"userRole\":\"PAH\"\r\n}";  
		logRequest(requestBody, operationName);
		Response response = benefitsEligibility(apiTestData, requestBody);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			//Assert.assertEquals(apiTestData.getBan(), getPathVal(jsonNode,"eligibleBenefits[0].benefit.ban"));
			Assert.assertNotNull(getPathVal(jsonNode,"eligibleBenefits[0].benefit.partner"));
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory# Description: Benefits Subscription search for requested misisdn
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, priority = 2, groups = {"BenefitsCLApi", "testBenefitsSubscriptionSearch", Group.SHOP,

			Group.APIREG })
	public void testBenefitsSubscriptionSearch(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Benefits Subscription search for requested misisdn");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results Benefits Subscription search for requested misisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="benefitsSubscriptionSearch";
		String requestBody = "{ \r\n\"accountNumber\":\""+apiTestData.getBan()+"\"\r\n}";
		logRequest(requestBody, operationName);
		Response response = benefitsSubscriptionSearch(apiTestData, requestBody);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertEquals(apiTestData.getBan(), getPathVal(jsonNode,"subscriptions[0].ban"));
			Assert.assertNotNull(getPathVal(jsonNode,"subscriptions[0].partner"));
		} else {
			failAndLogResponse(response, operationName);
		}
	}

}
