package com.tmobile.eservices.qa.global.acceptance;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.accounts.BillingAndPaymentsPage;
import com.tmobile.eservices.qa.pages.accounts.FamilyControlsPage;
import com.tmobile.eservices.qa.pages.accounts.LineDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.MediaSettingsPage;
import com.tmobile.eservices.qa.pages.accounts.PlanDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.global.NewContactUSPage;
import com.tmobile.eservices.qa.pages.payments.OTPAmountPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;

/**
 * @author rnallamilli
 *
 */
public class GovernmentCustomerTests extends GlobalCommonLib {

	/**
	 * US562605:.NET Migration | Gov | Other Lines Component
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
	public void verifyGovCustomerOtherLineSection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify other line section | Other line section should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyOtherLineSectionNotDisplayed();
	}

	/**
	 * US562603:.NET Migration | Gov | My Line Component
	 * TC-682:Ensure Gov master seeing Single line user experience  with the pop up message
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
	public void verifyGovCustomerMyLineSection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US562603:.NET Migration | Gov | My Line Component");
		Reporter.log("TC-682:Ensure Gov master seeing Single line user experience  with the pop up message");
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify my line section | My line section should be displayed");
		Reporter.log("5. Click usage menu |  .Net migration usage page should be displayed");
		Reporter.log("6. Navigate to back in browser | Home page should be displayed");
		Reporter.log("7. Verify call to upgrade early text | Call to upgrade early text should be displayed");
		Reporter.log("8. Click call to upgrade early button | Contact us page should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyMyLineSection();
		homePage.clickUsageMenu();
		homePage.verifyCurrentPageURL(".Net migration usage  page", "sage");
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.verifyCallToUpgradeText();
		homePage.clickCallToUpgradeText();

		NewContactUSPage newContactUSPage = new NewContactUSPage(getDriver());
		newContactUSPage.verifyContactUSPage();

	}

	/**
	 * US562594:.NET Migration | Gov | UNAV
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyCallUsAndMessageUSIconForGovtCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify call us icon | Call us icon should not be displayed");
		Reporter.log("5. Verify message us icon | Message us icon should not be displayed");
		Reporter.log("6. Verify account history menu | Account history menu should not be displayed");
		Reporter.log("8. Click usage menu | .Net migration usage page should be displayed");
		Reporter.log("9. Navigate to back in browser | Home page should be displayed");
		Reporter.log("10.Click bill menu | .Net migration page should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyCallUsIconNotDispalyed();
		homePage.verifyMessageUSIconNotDisplayed();
		homePage.verifyAccountHistoryMenuNotDisplayed();
		homePage.clickUsageMenu();
		homePage.verifyCurrentPageURL(".Net Migration Usage Page", "Usage");
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.clickBillingLink();
		homePage.verifyCurrentPageURL(".Net Migration Bill Page", "Bill");
	}

	/**
	 * US562600:.NET Migration | Gov | Billing Component
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
	public void verifyHomePageBillComponentSectionForGovtCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US562600:.NET Migration | Gov | Billing Component");
		Reporter.log("Test Data Conditions: Any GovtMsisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify view bill cta | View bill CTA should be displayed");
		Reporter.log("5. Click view bill cta  | .Net migration bill page should be displayed");
		Reporter.log("6. Navigate to back in browser | Home page should be displayed");
		Reporter.log("7. Click pay now link | One time payment page should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyViewBillCTA();
		homePage.clickViewBillCTA();
		homePage.verifyCurrentPageURL(".Net Migration Bill Page", "Bill");
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.clickPayBillbutton();

		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
	}

	/**
	 * US562600:.NET Migration | Gov | Billing Component
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
	public void verifyPaymentArrangementCTAForGovCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify payment arrangement cta | Payment Arrangement cta should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyPaymentArrangementCTANotDisplayed();
	}
	
	/**
	 * US562602:.NET Migration | Gov | Quick Links
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyGovCustomerQuickLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US562602:.NET Migration | Gov | Quick Links");
		Reporter.log("Test Data Conditions: Any Gov Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify purchase accessories quick link | Puchase accessories quick link should be displayed");
		Reporter.log("5. Click on purchase accessories quick link | .Net migration Accessories  page should be displayed");
		Reporter.log("6. Navigate to back in browser | Home page should  be displayed");
		Reporter.log("7. Verify check rebate status quicklink | Check rebate status quicklink should  be displayed");
		Reporter.log("8. Click check rebate status quicklink | Rebate page should  be displayed");
		Reporter.log("9. Navigate to back in browser | Home page should  be displayed");
		Reporter.log("10.Verify check trade in status quicklink | Check trade in status quicklink should be displayed");
		Reporter.log("11.Click check trade in status quicklink | Trade in page should be displayed");
		Reporter.log("12.Navigate to back in browser | Home page should  be displayed");
		Reporter.log(
				"13.Verify report a lost or stolen device quicklink | Report a lost or stolen device quicklink should be displayed");
		Reporter.log("14.Click report a lost or stolen device quicklink | My phone page should be displayed");
		Reporter.log("15.Navigate to back in browser | Home page should  be displayed");
		Reporter.log("13.Verify family allowances quicklink | Family allowances quicklink should be displayed");
		Reporter.log("14.Click family allowances quicklink | Family controls page should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyPurchaseAccessoriesQuickLink();
		homePage.clickPurchaseAccessoriesQuickLink();
		homePage.verifyCurrentPageURL(".Net migration Accessories  page", "accessories");
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.verifyCheckRebateStatusForGovtCustomer();
		homePage.clickCheckRebateStatusForGovtCustomer();
		homePage.verifyCurrentPageURL("Rebate page", "OrderSearch");
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.verifyCheckTradeInStausQuickLinkForGovtCustomer();
		homePage.clickCheckTradeInStausQuickLinkForGovtCustomer();
		homePage.verifyCurrentPageURL("TradeIn page", "checkorder");
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.verifyReportALostOrStolenDeviceQuickLink();
		homePage.clickReportALostOrStolenDeviceQuickLink();
		homePage.verifyCurrentPageURL("My phone page", "myphone");
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.verifyFamilyAllowanceInQuickLinks();
		homePage.clickFamilyAllowanceQuickLink();

		FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
		familyControlsPage.verifyFamilyControlsPage();

	}

	/**
	 * US562608:.NET Migration | Gov | Plan Benefits
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
	public void verifyBenefitsSectionForGovCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US562608:.NET Migration | Gov | Plan Benefits");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify benefits section | Benefits section should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyBenefitsSectionNotDisplayed();
	}

	/**
	 * US562609:.NET Migration | Gov | Footer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
	public void verifyFooterLinksForGovCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US562609:.NET Migration | Gov | Footer");
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify footer support link | Footer support link displayed");
		Reporter.log("5. Verify footer contact us link | Footer contact us link displayed");
		Reporter.log("6. Verify footer store locator link | Footer store locator link displayed");
		Reporter.log("7. Verify footer coverage link | Footer coverage link displayed");
		Reporter.log("8. Verify footer tmobile link | Footer tmobile link displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyFooterSupportLink();
		homePage.verifyFooterContactUsLink();
		homePage.verifyFooterStoreLocatorLink();
		homePage.verifyFooterCoverageLink();
		homePage.verifyFooterTMobileLink();

	}

	/**
	 * US568166:.NET Migration | Gov | Contact Us
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
	public void verifyContactUsPageForGovtCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US568166:.NET Migration | Gov");
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click contact us link | Contact us page should be displayed");
		Reporter.log("5. Verify team picture | Team picture should not be displayed");
		Reporter.log("6. Verify team content | Team content should not be displayed");
		Reporter.log("7. Verify call us button | Call us button should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewContactUSPage newContactUSPage = navigateToContactUsPage(myTmoData);
		newContactUSPage.verifyTeamPictureNotDisplayed();
		newContactUSPage.verifyTeamContentNotDisplayed();
		newContactUSPage.verifyCallUsButtonNotDispalyed();
	}

	/**
	 * US571053:.NET Migration | Gov | OTP Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX })
	public void verifyOTPPageForGovtCustomerWithMasterUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US571053:.NET Migration | Gov | OTP Page");
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click make a payment button | One time payment page should be displayed");
		Reporter.log("5. Click amount icon | OTPAmount page should be displayed");
		Reporter.log("6. Verify other amount divison | Other amount divison should not displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickPayBillbutton();

		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();

		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyOtherAmountDiv();
	}

	/**
	 * US571053:.NET Migration | Gov | OTP Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX })
	public void verifyOTPPageForGovtCustomerWithNonMasterUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US571053:.NET Migration | Gov | OTP Page");
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify payment arrangement cta | Payment Arrangement cta should not be displayed");
		Reporter.log("5. Verify no payment header | No payment header should not be displayed");
		Reporter.log("6. Click make a payment button | One time payment page should be displayed");
		Reporter.log("7. Click amount icon | OTPAmount page should be displayed");
		Reporter.log("8. Verify other amount divison | Other amount divison should not displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyPaymentArrangementCTANotDisplayed();
		homePage.verifyNoPaymentHeader();
		homePage.clickPayBillbutton();

		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();

		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyOtherAmountDiv();
	}

	/**
	 * US566240:.NET Migration | Gov | Account Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyAccountPageForGovtCustomerWithNonMasterUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US566240:.NET Migration | Gov | Account Page");
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click account menu | Line details page should be displayed");
		Reporter.log("5. Click on any plan name | Plan details page should be displayed");
		Reporter.log("6. Verify change plan cta | Change plan cta should not be displayed");
		Reporter.log("7. Verify shared addon component | Shared addon component should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickAccountMenu();

		LineDetailsPage lineDetailsPage = new LineDetailsPage(getDriver());
		lineDetailsPage.verifyLineDetailsPage();
		lineDetailsPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifyChangePlanCTA();
		plandetailspage.verifySharedAddonComponentNotDisplayed();
	}

	/**
	 * US566240:.NET Migration | Gov | Account Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX })
	public void verifyAccountPageForGovtCustomerForBookMarkedURl(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US566240:.NET Migration | Gov | Account Page");
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Navigate to account page bookmarked url | Line details page should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		navigateToNewHomePage(myTmoData);
		navigateToBookMarkedDashBoardURL("https://my.t-mobile.com/account-overview");
		LineDetailsPage lineDetailsPage = new LineDetailsPage(getDriver());
		lineDetailsPage.verifyLineDetailsPage();
	}

	/**
	 * US571065:.NET Migration | Gov | AutoPay Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void verifyAutoPayPageForGovtCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US571065:.NET Migration | Gov | AutoPay Page");
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click account menu | Line details page should be displayed");
		Reporter.log("5. Verify manage addons cta | Manage addons cta should not be displayed");
		Reporter.log("6. Verify settings section | Setttings section should not be displayed");
		Reporter.log("7. Click on any plan name | Plan details page should be displayed");
		Reporter.log("8. Verify change plan cta | Change plan cta should not be displayed");
		Reporter.log("9. Verify shared addon component | Shared addon component should not be displayed");
		Reporter.log("10.Verify associated lines | Associated lines should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickAccountMenu();

		LineDetailsPage lineDetailsPage = new LineDetailsPage(getDriver());
		lineDetailsPage.verifyLineDetailsPage();
		lineDetailsPage.verifyManageAddOnsCTA();
		lineDetailsPage.verifySettingsTitle();
		lineDetailsPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifyChangePlanCTA();
		plandetailspage.verifySharedAddonComponentNotDisplayed();
		plandetailspage.associatedlineSharedAddonNotDisplayed();
	}

	/**
	 * US577361:.NET Migration | Gov & Bus | Notifications
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyNoAlertsInNotificationTrayForGovtCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US577361:.NET Migration | Gov & Bus | Notifications");
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log(
				"4. Verify alert count from notifications tray | Alerts count in notifications tray should be zero");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyAlertsCountInNotificationTray();
	}

	/**
	 * US566240:.NET Migration | Gov | Account Page
	 * TC-682:Ensure Gov master seeing Single line user experience  with the pop up message
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
	public void verifyAccountPageForGovtCustomerWithMasterUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US566240:.NET Migration | Gov | Account Page");
		Reporter.log("TC-682:Ensure Gov master seeing Single line user experience  with the pop up message");
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click account menu | Line details page should be displayed");
		Reporter.log("5. Verify manage addons cta | Manage addons cta should not be displayed");
		Reporter.log("6. Verify settings section | Setttings section should not be displayed");
		Reporter.log("7. Click on any plan name | Plan details page should be displayed");
		Reporter.log("8. Verify change plan cta | Change plan cta should not be displayed");
		Reporter.log("9. Verify shared addon component | Shared addon component should not be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickAccountMenu();

		LineDetailsPage lineDetailsPage = new LineDetailsPage(getDriver());
		lineDetailsPage.verifyLineDetailsPage();
		lineDetailsPage.verifyManageAddOnsCTA();
		lineDetailsPage.verifySettingsTitle();
		lineDetailsPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifyChangePlanCTA();
		plandetailspage.verifySharedAddonComponentNotDisplayed();
	}

	/**
	 * US584896:.NET Migration | Gov & Bus | Non-Master View Changes
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void verifyHomePageForGovtCustomerWithNonMasterViewPermission(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US584896:.NET Migration | Gov & Bus | Non-Master View Changes");
		Reporter.log("Test Data Conditions: Any Gov customer with non master view permission");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify amount due | Amount due should not be displayed");
		Reporter.log("5. Verify due date | Due date should not be displayed");
		Reporter.log("6. Verify payment arrangement cta | Payment arrangement cta should not be displayed");
		Reporter.log("7. Click on profile menu | Profile page should be displayed");
		Reporter.log("8. Click on Billing and Payments link | Billing and payments page should be displayed");
		Reporter.log("9. Verify autopay blade | Autopay blade should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyAmountDueNotDisplayed();
		homePage.verifyDueDateNotDisplayed();
		homePage.verifyPaymentArrangementCTANotDisplayed();
		homePage.clickProfileMenu();

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
		profilePage.clickProfilePageLink("Billing & Payments");

		BillingAndPaymentsPage billingAndPaymentsPage = new BillingAndPaymentsPage(getDriver());
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.verifyAutoPayBlade();
	}

	/**
	 * US584896:.NET Migration | Gov & Bus | Non-Master View Changes
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void verifyHomePageForGovtCustomerWithMasterViewPermission(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US584896:.NET Migration | Gov & Bus | Non-Master View Changes");
		Reporter.log("Test Data Conditions: Any Gov customer with non master view permission");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify amount due | Amount due should  be displayed");
		Reporter.log("5. Verify autopay cta | Autopay cta should  be displayed");
		Reporter.log("6. Click on profile menu | Profile page should be displayed");
		Reporter.log("7. Click on Billing and Payments link | Billing and payments page should be displayed");
		Reporter.log("8. Verify autopay blade | Autopay blade should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyAmountDue();
		homePage.verifyDueDate();
		homePage.clickProfileMenu();

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
		profilePage.clickProfilePageLink("Billing & Payments");

		BillingAndPaymentsPage billingAndPaymentsPage = new BillingAndPaymentsPage(getDriver());
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.isAutoPayBladePresent();
	}
	
	/**
	 * US584261:.NET Migration | Gov & Bus | Accessories Re-Direct to .NET
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
	public void verifyPurchaseAccessoriesQuickLinkForGovCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US584261:.NET Migration | Gov & Bus | Accessories Re-Direct to .NET");
		Reporter.log("Test Data Conditions: Any Gov Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify purchase accessories quick link | Puchase accessories quick link should be displayed");
		Reporter.log(
				"5. Click on purchase accessories quick link | .Net migration Accessories  page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyPurchaseAccessoriesQuickLink();
		homePage.clickPurchaseAccessoriesQuickLink();
		homePage.verifyCurrentPageURL(".Net migration Accessories  page", "accessories");
	}

	/**
	 * US586589:.NET Migration | Gov & Bus | BingeOn Toggle
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyMediaSettingsForGovCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US586589:.NET Migration | Gov & Bus | BingeOn Toggle");
		Reporter.log("Test Data Conditions: Any Gov Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile Link |  Profile page should be displayed");
		Reporter.log("5. Click on Media Settings link | Media Settings should be displayed");
		Reporter.log("6. verify BingeOn header | BingeOn header  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		MediaSettingsPage mediaSettingsPage = navigateToMediaSettingsPage(myTmoData, "Media Settings");
		mediaSettingsPage.verifyMediaSettingsPage();
		mediaSettingsPage.verifyBingeOnHeader();
	}

	/**
	 * US587426:.NET Migration | Gov & Bus | No View User Re-Direct for Billing
	 * & Usage US593346:.NET Migration | Gov & Bus | Home Page Billing & Usage
	 * CTA re-direct for non-master restricted
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyBillingAndUsageMenuForNonMasterNoViewGovCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US587426:.NET Migration | Gov & Bus | No View User Re-Direct for Billing & Usage");
		Reporter.log("Test Data Conditions: Any Gov Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click usage menu | Billing restricted page should be displayed");
		Reporter.log("5. Verify restricted message content | Restricted message content should be displayed");
		Reporter.log("6. Navigate to back in browser | Home page should be displayed");
		Reporter.log("7. Click bill menu | .Net migration page should be displayed");
		Reporter.log("8. Verify restricted message content | Restricted message content should be displayed");
		Reporter.log("9. Navigate to back in browser | Home page should be displayed");
		Reporter.log("10.Click view bill menu | Billing restricted page should be displayed");
		Reporter.log("11.Verify restricted message content | Restricted message content should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickUsageMenu();
		homePage.verifyCurrentPageURL("Billing restricted page", "gov");
		homePage.verifyRestrictedMessageContent();
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.clickBillingLink();
		homePage.verifyCurrentPageURL("Billing restricted page", "gov");
		homePage.verifyRestrictedMessageContent();
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.clickViewBillCTA();
		homePage.verifyCurrentPageURL("Billing restricted page", "gov");
		homePage.verifyRestrictedMessageContent();
	}
	
	/**
	 * DE248005:Offline Stack - Master & Non-Master users are able to access the Language Setting Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT})
	public void verifyLanguageSettingPageForGovCustomerWithMasterUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("DE248005:Offline Stack - Master & Non-Master users are able to access the Language Setting Page");
		Reporter.log("Test Data Conditions: Any Gov Msisdn(Master user)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify purchase accessories quick link | Puchase accessories quick link should be displayed");
		Reporter.log(
				"5. Click on purchase accessories quick link | .Net migration Accessories  page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyPurchaseAccessoriesQuickLink();
		homePage.clickPurchaseAccessoriesQuickLink();
		homePage.verifyCurrentPageURL(".Net migration Accessories  page", "accessories");
	}
	
	/**
	 * DE248005:Offline Stack - Master & Non-Master users are able to access the Language Setting Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT})
	public void verifyLanguageSettingPageForGovCustomerWithNonMasterUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(" DE248005:Offline Stack - Master & Non-Master users are able to access the Language Setting Page");
		Reporter.log("Test Data Conditions: Any Gov Msisdn(NonMaster user)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Navigate to language settings page |System should be navigated to profile/home page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		getDriver().get("https://my.t-mobile.com/profile/language_settings");
		homePage.verifyLanguageSettingsPageNavigation();
	}
	/**
	 * CDCDWR-342:.NET Migration | Gov & Bus | Updates for Bill and Usage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void verifyGovCustomerMyLineViewBillClick(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Govt Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify my line section | My line section should be displayed");
		Reporter.log("5. Verify View Bill | View Bill should be displayed");
		Reporter.log("6. Click View Bill |  View Bill is clicked");
		Reporter.log("7. Verify Bill Page |  Navigate to Bill Page");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyViewBillCTA();
		homePage.clickViewBillCTA();
		homePage.verifyCurrentPageURL("Bill Page", "/billandpay");
		homePage.navigateBack();
	}

}
