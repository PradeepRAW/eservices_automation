package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AutopayManagementApi;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class AutopayApiV3Test extends AutopayManagementApi {
	public Map<String, String> tokenMap;
	/**
	/**
	 * UserStory# Description:US475791:MyTMO - PAYMENTS - Tokenization - Migrate existing AutoPay code to EOS-PAYMENTS
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, priority = 0, groups= {"testSetupAutopayV3WithCard"})
	public void testSetupAutopayV3WithCard(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: EOS-Payments create Autopay using card");
		Reporter.log("Data Conditions:MyTmo registered msdsin eligible for Autopay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Autopay  using card");
		Reporter.log("Step 2: Verify autopay setup created for the account and returned in response");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile("AutoPay_setup.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		String operationName="AutoPay_setup";
		logRequest(requestBody, operationName);
		Response response =autopay_Setup(apiTestData, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"Reason.reasonCode"),
						"2491" , "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"StatusBody.reasonDescription"), 
						"Transaction completed successfully", "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"Status.statusCode"), 
						"COMPLETED", "Status code is mismatched");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	/**
	 * UserStory# Description:US475791:MyTMO - PAYMENTS - Tokenization - Migrate existing AutoPay code to EOS-PAYMENTS
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups= {"testSearchAutopayV3WithCard"})
	public void testSearchAutopayV3WithCard(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: EOS-Payments create Autopay using card");
		Reporter.log("Data Conditions:MyTmo registered msdsin eligible for Autopay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Autopay  using card");
		Reporter.log("Step 2: Verify autopay setup created for the account and returned in response");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile("AutoPay_search.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		String operationName="AutoPay_search";
		logRequest(requestBody, operationName);
		Response response =autopaySearch(apiTestData, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String cardAlias = getPathVal(jsonNode,"paymentInstrumentList.creditCard.cardAlias");
				Assert.assertEquals(cardAlias.substring(1, cardAlias.length()-1),
						apiTestData.getCardAlias(), "Failed to compare card alias number");
				Assert.assertEquals(getPathVal(jsonNode,"paymentInstrumentList.creditCard.paymentMethodCode"), 
						"[CC]", "Failed to compare payment method code value");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	/**
	 * UserStory# Description:US475791:MyTMO - PAYMENTS - Tokenization - Migrate existing AutoPay code to EOS-PAYMENTS
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, priority = 2, groups= {"testUpdateAutopayWithBank"})
	public void testUpdateAutopayWithBank(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: EOS-Payments  update autopay using card");
		Reporter.log("Data Conditions:MyTmo registered msdsin signed up with Autopay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: update Autopay  using card");
		Reporter.log("Step 2: Verify autopay updated and returned in response");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile("Autopay_setup_withBank.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		String operationName="AutoPay_update";
		logRequest(requestBody, operationName);
		Response response =updateAutopay(apiTestData, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"Reason.reasonCode"),
						"2491" , "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"StatusBody.reasonDescription"), 
						"Transaction completed successfully", "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"Status.statusCode"), 
						"COMPLETED", "Status code is mismatched");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}


	/**
	/**
	 * UserStory# Description:US475791:MyTMO - PAYMENTS - Tokenization - Migrate existing AutoPay code to EOS-PAYMENTS
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, priority = 3, groups= {"testDeleteAutopayWithBank"})
	public void testDeleteAutopayWithBank(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: EOS-Payments  delete autopay ");
		Reporter.log("Data Conditions:MyTmo registered msdsin signed up with Autopay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: delete Autopay  ");
		Reporter.log("Step 2: Verify autopay deleted and returned in response");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile("AutoPay_DeleteWithBank.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		String operationName="AutoPay_update";
		logRequest(requestBody, operationName);
		Response response =deleteAutopay(apiTestData, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"Reason.reasonCode"),
						"2491" , "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"StatusBody.reasonDescription"), 
						"Transaction completed successfully", "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"Status.statusCode"), 
						"COMPLETED", "Status code is mismatched");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	/**
	 * UserStory# Description:US475791:MyTMO - PAYMENTS - Tokenization - Migrate existing AutoPay code to EOS-PAYMENTS
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, priority = 4, groups= {"testSetupAutopayV3WithBank"})
	public void testSetupAutopayV3WithBank(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: EOS-Payments create Autopay using Bank");
		Reporter.log("Data Conditions:MyTmo registered msdsin eligible for Autopay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Autopay  using Bank");
		Reporter.log("Step 2: Verify autopay setup created for the account and returned in response");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile("Autopay_setup_withBank.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		String operationName="AutoPay_setup";
		logRequest(requestBody, operationName);
		Response response =autopay_Setup(apiTestData, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"Reason.reasonCode"),
						"2491" , "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"StatusBody.reasonDescription"), 
						"Transaction completed successfully", "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"Status.statusCode"), 
						"COMPLETED", "Status code is mismatched");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	/**
	 * UserStory# Description:US475791:MyTMO - PAYMENTS - Tokenization - Migrate existing AutoPay code to EOS-PAYMENTS
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, priority = 5, groups= {"testSearchAutopayV3WithBank"})
	public void testSearchAutopayV3WithBank(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: EOS-Payments SEARCH operation");
		Reporter.log("Data Conditions:MyTmo registered msdsin signed up with Autopay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get autopay should return the autopay status for specific MSDSIN");
		Reporter.log("Step 2: Verify expected autopay status|expected autopay status should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");

		String requestBody = new ServiceTest().getRequestFromFile("AutoPay_search.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		String operationName="AutoPay_search";
		logRequest(requestBody, operationName);
		Response response =autopaySearch(apiTestData, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String banAccountAlias = getPathVal(jsonNode,"paymentInstrumentList.bankAccount.bankAccountAlias");
				Assert.assertEquals(banAccountAlias.substring(1, banAccountAlias.length()-1),
						apiTestData.getBankAccountAlias(), "Failed to compare card alias number");
				Assert.assertEquals(getPathVal(jsonNode,"paymentInstrumentList.bankAccount.paymentMethodCode"), 
						"[CHECK]", "Failed to compare payment method code value");
			}

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	/**
	 * UserStory# Description:US475791:MyTMO - PAYMENTS - Tokenization - Migrate existing AutoPay code to EOS-PAYMENTS
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, priority = 6, groups= {"testUpdateAutopayWithCard"})
	public void testUpdateAutopayWithCard(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: EOS-Payments  update autopay using bank");
		Reporter.log("Data Conditions:MyTmo registered msdsin signed up with Autopay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: update Autopay  using bank");
		Reporter.log("Step 2: Verify autopay updated and returned in response");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile("AutoPay_setup.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		String operationName="AutoPay_update";
		logRequest(requestBody, operationName);
		Response response =updateAutopay(apiTestData, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"Reason.reasonCode"),
						"2491" , "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"StatusBody.reasonDescription"), 
						"Transaction completed successfully", "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"Status.statusCode"), 
						"COMPLETED", "Status code is mismatched");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}


	/**
	/**
	 * UserStory# Description:US475791:MyTMO - PAYMENTS - Tokenization - Migrate existing AutoPay code to EOS-PAYMENTS
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, priority = 7, groups= {"testDeleteAutopayWithCard"})
	public void testDeleteAutopayWithCard(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: EOS-Payments  delete autopay ");
		Reporter.log("Data Conditions:MyTmo registered msdsin signed up with Autopay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: delete Autopay  ");
		Reporter.log("Step 2: Verify autopay deleted and returned in response");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile("AutoPay_delete.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		String operationName="AutoPay_update";
		logRequest(requestBody, operationName);
		Response response =deleteAutopay(apiTestData, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"Reason.reasonCode"),
						"2491" , "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"StatusBody.reasonDescription"), 
						"Transaction completed successfully", "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"Status.statusCode"), 
						"COMPLETED", "Status code is mismatched");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
}