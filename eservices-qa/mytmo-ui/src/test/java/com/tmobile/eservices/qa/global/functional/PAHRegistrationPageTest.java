package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.PAHRegistrationPage;

public class PAHRegistrationPageTest extends GlobalCommonLib{
	
	/**
	 * US307205:Sunset Claim PAH | Test Support
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyPAHRegistrationPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US307205:Sunset Claim PAH | Test Support");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be launched");
		Reporter.log("2. Login to the application | PAHRegistration page should be displayed");
		Reporter.log("3. Click on skip button | Modal window popup should be displayed");
		Reporter.log("4. Click on ok button in modal window popup | Home page should be displayed");
		Reporter.log("5. Click on notification dropdown | Click on notification dropdown should be success");
		Reporter.log("6. Click on complete account setup link | PAHRegistration page should be displayed");
	
		launchAndPerformLogin(myTmoData);
		
		PAHRegistrationPage pahRegistrationPage = new PAHRegistrationPage(getDriver());
		pahRegistrationPage.verifyPAHRegistrationPage();
		pahRegistrationPage.clickOnSkipBtn();
		pahRegistrationPage.clickOnModalWindowOkBtn();
		
		NewHomePage homePage = new NewHomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickNotificationDropDown();
		homePage.clickCompleteAccountSetUpLink();
		
		pahRegistrationPage = new PAHRegistrationPage(getDriver());
		pahRegistrationPage.verifyPAHRegistrationPage();
	}
	

	/**
	 * US307205:Sunset Claim PAH | Test Support
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyOldPAHRegistrationPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US307205:Sunset Claim PAH | Test Support");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be launched");
		Reporter.log("2. Login to the application | PAHRegistration page should be displayed");
		Reporter.log("3. Verify primary account missing alert header | Primary account holder missing header should be displayed");
		Reporter.log("4. Verify ssn text box | SSN text box should be displayed");
		Reporter.log("5. Verify confirm button | Confirm button should be displayed");
		Reporter.log("6. Verify ask me later button | Ask me later button should be displayed");
		Reporter.log("7. Click on ask me later button | Home page should be displayed");
		Reporter.log("8. Verify primary account missing alert divison | Primary account holder missing alert div should be displayed");
		Reporter.log("9. Click on primary account holder missing link | PAHRegistration page should be displayed");
		Reporter.log("Test Case : US307205:Sunset Claim PAH | Test Support");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be launched");
		Reporter.log("2. Login to the application | PAHRegistration page should be displayed");
		Reporter.log("3. Verify primary account missing alert header | Primary account holder missing header should be displayed");
		Reporter.log("4. Verify ssn text box | SSN text box should be displayed");
		Reporter.log("5. Verify confirm button | Confirm button should be displayed");
		Reporter.log("6. Verify ask me later button | Ask me later button should be displayed");
		Reporter.log("7. Click on ask me later button | Home page should be displayed");
		Reporter.log("8. Verify primary account missing alert divison | Primary account holder missing alert div should be displayed");
		Reporter.log("9. Click on primary account holder missing link | PAHRegistration page should be displayed");
	

		launchAndPerformLogin(myTmoData);
		
		PAHRegistrationPage pahRegistrationPage = new PAHRegistrationPage(getDriver());
		pahRegistrationPage.verifyPAHRegistrationPage();
		pahRegistrationPage.verifyPrimaryAccountHolderMissingHeader();
		pahRegistrationPage.verifySSNTextBox();
		pahRegistrationPage.verifyConfirmBtn();
		pahRegistrationPage.verifyAskMeLaterBtn();
		pahRegistrationPage.clickOnAskMeLaterBtn();
		
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.verifyPrimaryAccountMissingAlertDiv();
		homePage.clickPrimaryAccountHolderMissingLink();
		
		pahRegistrationPage = new PAHRegistrationPage(getDriver());
		pahRegistrationPage.verifyPAHRegistrationPage();
	}
}
