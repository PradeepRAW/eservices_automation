package com.tmobile.eservices.qa.global.api;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AppointmentsApiV1;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class AppointmentApiV1Test extends AppointmentsApiV1{
	
	public Map<String, String> tokenMap;
	/**
	 * UserStory# Description: Appointments AppointmentContext Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true,  groups = {"Appointments",  "testGetAppointmentCallRoutingContext", Group.GLOBAL,Group.APIREG })
	public void testGetAppointmentCallRoutingContext(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGetAppointmentCallRoutingContext");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the AppointmentContext of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		Response response = getAppointmentCallRoutingContext(apiTestData, "");
		String operationName="AppointmentContext";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String senderId= getPathVal(jsonNode, "header.sender.senderId");
				String channelId= getPathVal(jsonNode, "header.sender.channelId");
				String applicationId= getPathVal(jsonNode, "header.sender.applicationId");
				String code= getPathVal(jsonNode, "responseStatus.code");
				Assert.assertNotEquals(senderId,"","senderId is Null or Empty.");
				Assert.assertNotEquals(channelId,"","channelId is Null or Empty.");
				Assert.assertNotEquals(applicationId,"","applicationId is Null or Empty.");
				Assert.assertEquals(code,"100","invalid responseStatus.code.");
			}else{
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory# Description: Appointments getAppointment Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true,  groups = {"Appointments","testGetAppointment", Group.GLOBAL,Group.APIREG })
	public void testGetAppointment(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getAppointment");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the Appointment of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="createAppointment";
		tokenMap = new HashMap<String, String>();
		List<String> elements=new ArrayList<String>();
		elements.add("statusCode");
		elements.add("statusMessage");
		elements.add("availableSlots");
		Map<String,String> responseMap=getAvailableSlotsResponseMap(tokenMap, apiTestData, elements);
		String availableSlots=responseMap.get("availableSlots");
		tokenMap.put("startTime", availableSlots);
		responseMap=createNewAppointment(apiTestData,tokenMap,operationName);
		Assert.assertNotEquals(responseMap.get("appointmentId"),"","AppointmentId is Null or Empty.");
		responseMap=getAppointment(apiTestData,elements);
		deleteAppointment(apiTestData,tokenMap);
	}
	
	/**
	 * UserStory# Description: Appointments createAppointment Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true,  groups = {"Appointments","testCreateAppointment", Group.GLOBAL,Group.APIREG })
	public void testCreateAppointment(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: createAppointment");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the created Appointment of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String operationName = "createAppointment";
		tokenMap = new HashMap<String, String>();
		List<String> elements=new ArrayList<String>();
		elements.add("statusCode");
		elements.add("statusMessage");
		elements.add("availableSlots");
		Map<String,String> reslutMap=getAppointment(apiTestData,elements);
		if(!"400".contentEquals(reslutMap.get("statusCode"))) {
			deleteAppointment(apiTestData,tokenMap);
		}
		
		Map<String,String> resultMap=getAvailableSlotsResponseMap(tokenMap,apiTestData,elements);
		String availableSlots=resultMap.get("availableSlots");
		Assert.assertNotEquals(availableSlots,"","availableSlots is Null or Empty.");
		tokenMap.put("startTime", availableSlots);
		
		operationName = "createAppointment";
		reslutMap=createNewAppointment(apiTestData,tokenMap,operationName);
		Assert.assertNotEquals(reslutMap.get("appointmentId"),"","Invalid AppointmentId.");
		
		operationName = "getAppointment";
		reslutMap=getAppointment(apiTestData,elements);
		Assert.assertEquals(reslutMap.get("statusCode"),"200","Invalid statusCode.");
		
		deleteAppointment(apiTestData,tokenMap);
	}
	
	/**
	 * UserStory# Description: Appointments deleteAppointment Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = {"Appointments","testDeleteAppointment", Group.GLOBAL,Group.APIREG })
	public void testDeleteAppointment(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: deleteAppointment");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the delete Appointment of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "createAppointment";
		tokenMap = new HashMap<String, String>();
		List<String> elements=new ArrayList<String>();
		elements.add("statusCode");
		elements.add("statusMessage");
		elements.add("availableSlots");
		Map<String,String> reslutMap=getAppointment(apiTestData,elements);
		if(!"400".contentEquals(reslutMap.get("statusCode"))) {
			deleteAppointment(apiTestData,tokenMap);
		}else {
		
			Map<String,String> resultMap=getAvailableSlotsResponseMap(tokenMap,apiTestData,elements);
			String availableSlots=resultMap.get("availableSlots");
			Assert.assertNotEquals(availableSlots,"","availableSlots is Null or Empty.");
			tokenMap.put("startTime", availableSlots);
			
			operationName = "createAppointment";
			reslutMap=createNewAppointment(apiTestData,tokenMap,operationName);
			Assert.assertNotEquals(reslutMap.get("appointmentId"),"","Invalid AppointmentId.");
			tokenMap.put("appointmentId", reslutMap.get("appointmentId"));
			
			operationName = "getAppointment";
			reslutMap=getAppointment(apiTestData,elements);
			Assert.assertNotEquals(reslutMap.get("appointmentId"),"","Invalid AppointmentId.");
			tokenMap.put("appointmentId", reslutMap.get("appointmentId"));
			deleteAppointment(apiTestData,tokenMap);
			
			operationName = "getAppointment";
			reslutMap=getAppointment(apiTestData,elements);
			Assert.assertEquals(reslutMap.get("statusCode"),"400","Invalid statusCode.");
		}
	}

	/**
	 * UserStory# Description: Appointments getAvailableSlots Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true,  groups = {"Appointments","testGetAvailableSlots", Group.GLOBAL,Group.APIREG })
	public void testGetAvailableSlots(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getAvailableSlots");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the getAvailableSlots of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile("Appointment-getAvailableSlots.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = new Date();
        Calendar calCurr = Calendar.getInstance();
        calCurr.setTime(date);        
        calCurr.add(Calendar.MINUTE, 5);
        Date currentDate = calCurr.getTime();
		tokenMap.put("startTime", sdf.format(currentDate));
		String operationName="Appointment-getAvailableSlots";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(requestBody, operationName);
		Response response = getAvailableSlots(apiTestData, updatedRequest);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode= getPathVal(jsonNode, "statusCode");
				String statusMessage= getPathVal(jsonNode, "statusMessage");
				String availableSlots= getPathVal(jsonNode, "availableSlots");
				Assert.assertEquals(statusCode,"200","statusCode is Null or Empty.");
				Assert.assertEquals(statusMessage,"Success","statusMessage is Null or Empty.");
				Assert.assertNotEquals(availableSlots,"","availableSlots is Null or Empty.");
			}	
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	public Map<String,String>  getAvailableSlotsResponseMap(Map<String,String> tokenMap,ApiTestData apiTestData,List<String> elements) throws Exception {
		String operationName="getAvailableSlots";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = new Date();
        Calendar calCurr = Calendar.getInstance();
        calCurr.setTime(date);        
        calCurr.add(Calendar.MINUTE, 5);
        Date currentDate = calCurr.getTime();
		tokenMap.put("startTime", sdf.format(currentDate));
		String requestBody = new ServiceTest().getRequestFromFile("Appointment-getAvailableSlots.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Map<String, String> elementsMap = new HashMap<String, String>();
		Response response = getAvailableSlots(apiTestData, updatedRequest);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			elementsMap.put("statusCode", jsonNode.path("statusCode").textValue());
			elementsMap.put("statusMessage", jsonNode.path("statusMessage").textValue());
			JsonNode availableSlots = jsonNode.path("availableSlots");
			if (!availableSlots.isMissingNode() && availableSlots.isArray()) {
				elementsMap.put("availableSlots", availableSlots.get(0).asText());
				elementsMap.put("availableSlots1", availableSlots.get(1).asText());
				elementsMap.put("availableSlots2", availableSlots.get(2).asText());
				elementsMap.put("availableSlots3", availableSlots.get(3).asText());
			}	
		} else {
			failAndLogResponse(response, operationName);
		}
		return elementsMap;
	}
	
	/**
	 * UserStory# Description: Appointments getEstimateWaitTime Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = {"Appointments","testGetEstimateWaitTime", Group.GLOBAL,Group.APIREG })
	public void testGetEstimateWaitTime(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getEstimateWaitTime");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the getAvailableSlots of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		String operationName ="";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		
		deleteAppointment(apiTestData,tokenMap);
		List<String> elements = new ArrayList<String>();
		elements.add("statusCode");
		elements.add("statusMessage");
		elements.add("availableSlots");
		Map<String, String> resultMap = getAppointment(apiTestData, elements);

		Map<String, String> AvailableSlotsResultMap = getAvailableSlotsResponseMap(tokenMap, apiTestData, elements);
		String availableSlots = AvailableSlotsResultMap.get("availableSlots");
		Assert.assertNotEquals(availableSlots, "", "availableSlots is Null or Empty.");
		tokenMap.put("startTime",availableSlots);
		if(resultMap.get("statusCode").equals("400") && resultMap.get("appointmentId").equals("")) {
			operationName = "createAppointment";
			resultMap = createNewAppointment(apiTestData, tokenMap, operationName);
			Assert.assertNotEquals(resultMap.get("appointmentId"), "", "Invalid AppointmentId.");
		}
		
		operationName = "getEstimateWaitTime";
		tokenMap.put("startTime", availableSlots);
		tokenMap.put("appointmentId", resultMap.get("appointmentId"));
		String requestBody = new ServiceTest().getRequestFromFile("Appointment-getEstimateWaitTime.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getEstimateWaitTime(apiTestData, updatedRequest);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String estimateWaitTime = getPathVal(jsonNode, "estimateWaitTime");
				String status = getPathVal(jsonNode, "status");
				Assert.assertNotEquals(estimateWaitTime, "", "estimateWaitTime is Null or Empty.");
				Assert.assertNotEquals(status, "", "status is Null or Empty.");
			}else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		deleteAppointment(apiTestData,tokenMap);
	}

	/**
	 * UserStory# Description: Appointments updateAppointment Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true,  groups = {"Appointments","testUpdateAppointment", Group.GLOBAL,Group.APIREG })
	public void testUpdateAppointment(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: updateAppointment");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the updated Appointment of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		String operationName = "createAppointment";
		tokenMap = new HashMap<String, String>();
		List<String> elements=new ArrayList<String>();
		elements.add("statusCode");
		elements.add("statusMessage");
		elements.add("availableSlots");
		Map<String,String> reslutMap=getAppointment(apiTestData,elements);
		if(!"400".contentEquals(reslutMap.get("statusCode"))) {
			deleteAppointment(apiTestData,tokenMap);
		}
		
		Map<String,String> resultMap=getAvailableSlotsResponseMap(tokenMap,apiTestData,elements);
		String availableSlots=resultMap.get("availableSlots");
		Assert.assertNotEquals(availableSlots,"","availableSlots is Null or Empty.");
		tokenMap.put("startTime", availableSlots);
		
		operationName = "createAppointment";
		reslutMap=createNewAppointment(apiTestData,tokenMap,operationName);
		Assert.assertNotEquals(reslutMap.get("appointmentId"),"","Invalid AppointmentId.");
		
		operationName = "getAppointment";
		reslutMap=getAppointment(apiTestData,elements);
		Assert.assertNotEquals(reslutMap.get("appointmentId"),"","Invalid AppointmentId.");
		
		resultMap=getAvailableSlotsResponseMap(tokenMap,apiTestData,elements);
		availableSlots=resultMap.get("availableSlots1");
		Assert.assertNotEquals(availableSlots,"","availableSlots is Null or Empty.");
		tokenMap.put("startTime", availableSlots);
		
		operationName = "updateAppointment";
		String requestBody = new ServiceTest().getRequestFromFile("Appointment-updateAppointment.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = updateAppointment(apiTestData, updatedRequest);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode= getPathVal(jsonNode, "statusCode");
				String statusMessage= getPathVal(jsonNode, "statusMessage");
				Assert.assertEquals(statusCode,"200","statusCode is Invalid.");
				Assert.assertEquals(statusMessage,"Success","statusMessage is Invalid.");
			}	
		} else {
			failAndLogResponse(response, operationName);
		}
		deleteAppointment(apiTestData,tokenMap);
	}
	
	private Map<String,String> getAppointment(ApiTestData apiTestData,List<String> elements) throws Exception, IOException, JsonProcessingException {
		Response response = getAppointment(apiTestData, "America/New_York");
		String operationName="getAppointment";
		Map<String,String> reslutMap=new HashMap<String, String>();
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode = getPathVal(jsonNode, "statusCode");
				if(!statusCode.equals("400")) {
					String appointmentId = getPathVal(jsonNode, "appointmentDetails.appointmentID");
					reslutMap.put("appointmentId", appointmentId);
					tokenMap.put("appointmentId", appointmentId);
				}else {
					tokenMap.put("appointmentId", "");
					reslutMap.put("appointmentId", "");
				}
				reslutMap.put("statusCode", statusCode);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return reslutMap;
	}
	
	private Response deleteAppointment(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {
		Response response = deleteAppointment(apiTestData, tokenMap.get("appointmentId"));
		String operationName="deleteAppointment";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		} else {
			failAndLogResponse(response, operationName);
		}
		return response;
	}
	
	private Map<String,String> createNewAppointment(ApiTestData apiTestData,Map<String,String> tokenMap,String operationName) throws Exception, IOException, JsonProcessingException {
		Map<String,String> resultMap=new HashMap<String, String>();
		String requestBody = new ServiceTest().getRequestFromFile("Appointment-createAppointment.txt");
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		//Date date = new Date();
		//Calendar calCurr = Calendar.getInstance();
		//calCurr.setTime(date);        
		//calCurr.add(Calendar.MINUTE, 5);
		//Date currentDate = calCurr.getTime();
		//tokenMap.put("startTime", sdf.format(currentDate));
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = createAppointment(apiTestData, updatedRequest);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String appointmentId= getPathVal(jsonNode, "appointmentId");
				resultMap.put("appointmentId", appointmentId);
				tokenMap.put("appointmentId", appointmentId);
			}
		}else {
			failAndLogResponse(response, operationName);
		}
		return resultMap;
	}
}
