package com.tmobile.eservices.qa.tmng.functional;

import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class DigitsPageTest extends TmngCommonLib {
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TMNG })
	public void testDigitsPageTest(TMNGData tMNGData) {
		navigateToDigitsPage(tMNGData);
	}
}
