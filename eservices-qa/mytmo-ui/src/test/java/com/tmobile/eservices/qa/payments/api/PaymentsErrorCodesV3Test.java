package com.tmobile.eservices.qa.payments.api;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.PaymentsErrorCodesV3;

import io.restassured.response.Response;

public class PaymentsErrorCodesV3Test extends PaymentsErrorCodesV3 {
	
	public Map<String, String> tokenMap;
	
	/**
	/**
	 * UserStory# US533688:EOS- Remove dynamo DB and handle error codes and CTA in EOS
	 *          # 400 Error
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsErrorCodesV3Test","testPaymentsErrorCode400",Group.PAYMENTS,Group.SPRINT  })
	public void testPaymentsErrorCode400(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testPaymentsErrorCode400");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with paymentsapi.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 400 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testPaymentsErrorCode400";
		
		tokenMap = new HashMap<String, String>();
		
		String requestBody =  new ServiceTest().getRequestFromFile("PaymentsErrorCodesV3.txt");	
        
        tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("primaryMsisdn", apiTestData.getPrimaryMsisdn());
		tokenMap.put("email", apiTestData.getPrimaryMsisdn()+"@yopmail.com");
		tokenMap.put("chargeAmount", String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));
		/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
        Calendar calCurr = Calendar.getInstance();
        calCurr.setTime(date);
        calCurr.add(Calendar.DATE, 2);
        Date twoDaysAheadDate = calCurr.getTime();*/
        tokenMap.put("fdpdate", "");
		//System.out.println(String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
			
		Response response = paymentErrorCodes(apiTestData, updatedRequest, tokenMap);
	
		if (response != null && "400".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==400);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"category"),
						"SYSTEM_FAILURE" , "Category is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"message"), 
						"System Error", "Message  is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"code"), 
						"E", "Error Code  is mismatched");
			}
		} else {
			
			failAndLogResponse(response, operationName);
		}
	}
	
}
