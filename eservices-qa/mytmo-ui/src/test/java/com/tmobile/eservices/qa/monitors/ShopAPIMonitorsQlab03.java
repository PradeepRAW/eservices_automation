package com.tmobile.eservices.qa.monitors;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.tmobile.eservices.qa.api.ShopAPIMonitorsHelper;
import com.tmobile.eservices.qa.data.ApiTestData;

public class ShopAPIMonitorsQlab03 extends ShopAPIMonitorsHelper {

	@BeforeMethod(alwaysRun = true)
	public void clearTokenMapCommon() {
		tokenMapCommon = null;
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeFRPFlowQlab03(ApiTestData apiTestData) {
		standardUpgradeFRPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeEIPFlowQlab03(ApiTestData apiTestData) {
		standardUpgradeEIPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeAddSocsEIPFlowQlab03(ApiTestData apiTestData) {
		standardUpgradeAddSocsEIPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeAddAccessoriesEIPFlowQlab03(ApiTestData apiTestData) {
		standardUpgradeAddAccessoriesEIPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeTradeInEIPFlowQlab03(ApiTestData apiTestData) {
		standardUpgradeTradeInEIPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void AALPhoneOnEIPNoPDPSOCQlab03(ApiTestData apiTestData) {
		aalPhoneOnEIPNoPDPSOC(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void AAL_FRP_PDP_NY_WithDepositQlab03(ApiTestData apiTestData) {
		aal_FRP_PDP_NY_WithDeposit(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void AAL_EIP_PDP_WithSecurityDepositQlab03(ApiTestData apiTestData) {
		aal_EIP_PDP_WithSecurityDeposit(apiTestData);
	}
	
	//@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardBYODFlow(ApiTestData apiTestData) {
		BYODFlow(apiTestData);
	}
}
