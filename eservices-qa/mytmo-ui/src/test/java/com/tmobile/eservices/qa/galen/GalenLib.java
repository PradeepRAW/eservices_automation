package com.tmobile.eservices.qa.galen;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.galenframework.api.Galen;
import com.galenframework.reports.GalenTestInfo;
import com.galenframework.reports.HtmlReportBuilder;
import com.galenframework.reports.model.LayoutReport;

public class GalenLib {

	public void doVisualTest(WebDriver driver, String pageName, String spec, String device) {

		LayoutReport layoutReport = null;
		try {
			if (device == "desktop")
				driver.manage().window().setSize(new Dimension(1024, 800));
			else if (device == "mobile")
				driver.manage().window().setSize(new Dimension(450, 800));
			else
				driver.manage().window().setSize(new Dimension(750, 800));
			
			layoutReport = Galen.checkLayout(driver, spec, Arrays.asList(device));
			List<GalenTestInfo> tests = new LinkedList<GalenTestInfo>();
	        GalenTestInfo test = GalenTestInfo.fromString(pageName +" layout");
	        test.getReport().layout(layoutReport, "check "+pageName+" layout");
	        tests.add(test);
	        HtmlReportBuilder htmlReportBuilder = new HtmlReportBuilder();
	        htmlReportBuilder.build(tests, "target");
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (layoutReport.errors() > 0) {
			Assert.fail("Layout test failed");
		}

	}

}