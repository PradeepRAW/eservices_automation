package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.IdentityFraudRejectPage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewIntroductionPage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewQuestionPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class IdentityReviewIntroductionPageTest extends ShopCommonLib{
	
	
	/**
	 * US448176 - Fraud - Review Screen
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = Group.AAL_PENDING)
	public void verifyFraudReviewScreenNonBYOD(ControlTestData data, MyTmoData myTmoData) {
	Reporter.log("Test Case name : US448176 - Fraud - Review Screen");
	Reporter.log(
			"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("1. Launch the application | Application Should be Launched");
	Reporter.log("2. Login to the application | User Should be login successfully");
	Reporter.log("3. Verify Home page | Home page should be displayed");
	Reporter.log("4. Click on shop link | Shop page should be displayed");
	Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
	Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
	Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
	Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
	Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
	Reporter.log("10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
	Reporter.log("11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
	Reporter.log("12. Click on 'Continue' CTA | Payment Information section should be displayed");
    Reporter.log("13. Click on Accept and Continue|Fraud review page should be displayed");
	Reporter.log("17. Verify Authorable Header|Authorable Header 'Let's verify your identity' should be displayed");
	Reporter.log("16. Verify Authorable Text | Authorable Text should be displayed");
	Reporter.log("17. Verify primary and secondary  CTA |Primary and Secondary  CTA should be displayed");
	Reporter.log("18. Verify header and footer |Header and Footer  should not be displayed");
	Reporter.log("================================");
	Reporter.log("Actual Output:");
	
	navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
	CartPage cartPage = new CartPage(getDriver());
	cartPage.clickContinueToPaymentButton();
	cartPage.verifyPaymentInformationForm();
	
	cartPage.placeOrderCTADisabled();
	cartPage.serviceAgreementCheckBoxDisplayed();
	
	cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
	cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
	cartPage.enterCardNumber(myTmoData.getCardNumber());
	cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
	cartPage.enterCVV(myTmoData.getPayment().getCvv());
	
	cartPage.clickServiceCustomerAgreementCheckBox();
	cartPage.placeOrderCTAEnabled();
	
	cartPage.clickAcceptAndPlaceOrder();
	
	IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionPageHeaderText();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionPageTextBelowHeader();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionCancelCTA();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionheaderAndFooter();
	
	}

	
	/**
	 * US448176 - Fraud - Review Screen
	 * @param data
	 * @param myTmoData
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true, groups =Group.AAL_PENDING)
	public void verifyFraudReviewScreenBYOD(ControlTestData data, MyTmoData myTmoData) {
	Reporter.log("Test Case name : US448176 - Fraud - Review Screen");
	Reporter.log(
			"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("1. Launch the application | Application Should be Launched");
	Reporter.log("2. Login to the application | Home page should be displayed");
	Reporter.log("3. Click on shop | shop page should be displayed");
	Reporter.log("4. Click on 'Add A LINE' button | Customer intent page should be displayed");
	Reporter.log("5. Select on 'Buy my own device' option | Consolidated Rate Plan Page  should be displayed");
	Reporter.log("6. Click Continue button on Consolidated Rate Plan Page | Cart Page should be displayed");
    Reporter.log("7. Click 'Continue to shipping' button | Shipping details should be displayed");	
	Reporter.log("8. Click on 'Continue' CTA | Payment Information section should be displayed");
    Reporter.log("9. Click on Accept and Continue|Fraud review page should be displayed");
	Reporter.log("10. Verify Authorable Header|Authorable Header 'Let's verify your identity' should be displayed");
	Reporter.log("11. Verify Authorable Text | Authorable Text should be displayed");
	Reporter.log("12. Verify primary and secondary  CTA |Primary and Secondary  CTA should be displayed");
	Reporter.log("13. Verify header and footer |Header and Footer  should not be displayed");
	Reporter.log("================================");
	Reporter.log("Actual Output:");
	
	CartPage cartPage = navigateToCartPageFromAALBYODFlow(myTmoData);
	cartPage.verifyCartPage();
	cartPage.clickContinueToShippingButton();
	cartPage.clickContinueToPaymentButton();
	cartPage.verifyPaymentInformationForm();
	
	cartPage.placeOrderCTADisabled();
	cartPage.serviceAgreementCheckBoxDisplayed();
	
	cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
	cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
	cartPage.enterCardNumber(myTmoData.getCardNumber());
	cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
	cartPage.enterCVV(myTmoData.getPayment().getCvv());
	
	cartPage.clickServiceCustomerAgreementCheckBox();
	
	
	cartPage.clickAcceptAndPlaceOrder();
	
	IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionPageHeaderText();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionPageTextBelowHeader();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionCancelCTA();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionheaderAndFooter();
	
	}
	
	
	/**
	 * US448177 - Fraud - Review Screen CTAs
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING })
	public void verifyFraudReviewScreenCTANonBYOD(ControlTestData data, MyTmoData myTmoData) {
	Reporter.log("Test Case name : US448177 - Fraud - Review Screen Primary CTAs");
	Reporter.log(
			"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("1. Launch the application | Application Should be Launched");
	Reporter.log("2. Login to the application | User Should be login successfully");
	Reporter.log("3. Verify Home page | Home page should be displayed");
	Reporter.log("4. Click on shop link | Shop page should be displayed");
	Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
	Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
	Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
	Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
	Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
	Reporter.log("10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
	Reporter.log("11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
	Reporter.log("12. Click on 'Continue' CTA | Payment Information section should be displayed");
    Reporter.log("13. Click on Accept and Continue|Fraud review page should be displayed");
	Reporter.log("14. Verify primary and secondary  CTA |Primary and Secondary  CTA should be displayed");
	Reporter.log("15. Click on Cancel CTA | Warning Modal should be displayed");
	Reporter.log("16. Click on Close Icon | Warning Modal should close and Review Screen Should be displayed");
	Reporter.log("17. Click on Continue CTA | Review Questions Page should be displayed");
	Reporter.log("================================");
	Reporter.log("Actual Output:");
	
	navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
	CartPage cartPage = new CartPage(getDriver());
	cartPage.clickContinueToPaymentButton();
	cartPage.verifyPaymentInformationForm();
	
	cartPage.placeOrderCTADisabled();
	cartPage.serviceAgreementCheckBoxDisplayed();
	
	cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
	cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
	cartPage.enterCardNumber(myTmoData.getCardNumber());
	cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
	cartPage.enterCVV(myTmoData.getPayment().getCvv());
	
	cartPage.clickServiceCustomerAgreementCheckBox();
	cartPage.placeOrderCTAEnabled();
	
	cartPage.clickAcceptAndPlaceOrder();
	
	IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionCancelCTA();
	identityReviewIntroductionPage.clickIdentityReviewIntroductionCancelCTA();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionWarningModal();
	identityReviewIntroductionPage.clickCloseIconOnIdentityReviewIntroductionWarningModal();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
	identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();

	IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
	identityReviewQuestionPage.verifyReviewQuestionPage();
	
	}
	
	
	/**
	 * US448177 - Fraud - Review Screen CTAs
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = Group.AAL_PENDING)
	public void verifyFraudReviewScreenCTABYOD(ControlTestData data, MyTmoData myTmoData) {
	Reporter.log("Test Case name : US448177 - Fraud - Review Screen Primary CTAs");
	Reporter.log(
			"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("1. Launch the application | Application Should be Launched");
	Reporter.log("2. Login to the application | Home page should be displayed");
	Reporter.log("3. Click on shop | shop page should be displayed");
	Reporter.log("4. Click on 'Add A LINE' button | Customer intent page should be displayed");
	Reporter.log("5. Select on 'Buy my own device' option | Consolidated Rate Plan Page  should be displayed");
	Reporter.log("6. Click Continue button on Consolidated Rate Plan Page | Cart Page should be displayed");
    Reporter.log("7. Click 'Continue to shipping' button | Shipping details should be displayed");	
	Reporter.log("8. Click on 'Continue' CTA | Payment Information section should be displayed");
    Reporter.log("9. Click on Accept and Continue|Fraud review page should be displayed");
	Reporter.log("10. Verify primary and secondary  CTA |Primary and Secondary  CTA should be displayed");
	Reporter.log("11. Click on Cancel CTA | Warning Modal should be displayed");
	Reporter.log("12. Click on Close Icon | Warning Modal should close and Review Screen Should be displayed");
	Reporter.log("13. Click on Continue CTA | Review Questions Page should be displayed");
	Reporter.log("================================");
	Reporter.log("Actual Output:");
	
	CartPage cartPage = navigateToCartPageFromAALBYODFlow(myTmoData);
	cartPage.verifyCartPage();
	cartPage.clickContinueToShippingButton();
	cartPage.clickContinueToPaymentButton();
	cartPage.verifyPaymentInformationForm();
	
	cartPage.placeOrderCTADisabled();
	cartPage.serviceAgreementCheckBoxDisplayed();
	
	cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
	cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
	cartPage.enterCardNumber(myTmoData.getCardNumber());
	cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
	cartPage.enterCVV(myTmoData.getPayment().getCvv());
	
	cartPage.clickServiceCustomerAgreementCheckBox();
	cartPage.placeOrderCTAEnabled();
	
	cartPage.clickAcceptAndPlaceOrder();
	
	IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionCancelCTA();
	identityReviewIntroductionPage.clickIdentityReviewIntroductionCancelCTA();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionWarningModal();
	identityReviewIntroductionPage.clickCloseIconOnIdentityReviewIntroductionWarningModal();
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
	identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
	
	IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
	identityReviewQuestionPage.verifyReviewQuestionPage();
	}
	
	
	/**
	 * US450033 - Fraud - Modal for Cancel Button (or back button)
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = Group.AAL_PENDING)
	public void verifyFraudRejectScreenCloseModalThroughcancel(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : US450033 - Fraud - Modal for Cancel Button (or back button)");
		Reporter.log(
				"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("5. Select on 'Buy my own device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("6. Click Continue button on Consolidated Rate Plan Page | Cart Page should be displayed");
	    Reporter.log("7. Click 'Continue to shipping' button | Shipping details should be displayed");	
		Reporter.log("8. Click on 'Continue' CTA | Payment Information section should be displayed");
        Reporter.log("9. Click on Accept and Continue|Fraud verification page should be displayed");
		Reporter.log("10. Click on Continue|Review question page should be displayed");
		Reporter.log("11. Select cancel |  cancel modal should be displayed");
		Reporter.log("12. Select Yes, cancel this order|Reject Screen will be displayed");
		Reporter.log("13. Verify authorable Text and Header |Authorable Text and Header should be displayed");
		Reporter.log("14. Verify primary and secondary  CTA |'Yes, cancel this order' CTA should be displayed");
		Reporter.log("15. Verify secondary  CTA |'No, answer questions' CTA should be displayed");
		Reporter.log("16. Verify close modal('X')|Close button should be displayed");
		Reporter.log("17. Click 'continue with question'  CTA  |Identity review question page should be displayed");
		Reporter.log("18. click continue |Identity question page should be displayed");                   
		Reporter.log("19. click cancel|warning modal should be  displayed");   
		Reporter.log("20. Click cancel CTA  |Reject screen"); 
		Reporter.log("Actual Output:");
		Reporter.log("================================");
		
		
		CartPage cartPage = navigateToCartPageFromAALBYODFlow(myTmoData);
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		
		cartPage.placeOrderCTADisabled();
		cartPage.serviceAgreementCheckBoxDisplayed();
		
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.placeOrderCTAEnabled();
		
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionCancelCTA();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionWarningModal();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionWarningModalTextBody();
		identityReviewIntroductionPage.verifyPrimaryContinueCTAIconOnIdentityReviewIntroductionWarningModal();
		identityReviewIntroductionPage.verifySecondaryCancelCTAIconOnIdentityReviewIntroductionWarningModal();
		identityReviewIntroductionPage.verifyCloseIconOnIdentityReviewIntroductionWarningModal();
		identityReviewIntroductionPage.clickPrimaryContinueCTAIconOnIdentityReviewIntroductionWarningModal();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
		
		IdentityReviewQuestionPage identityReviewQuestionPage=new IdentityReviewQuestionPage(getDriver());
		identityReviewQuestionPage.verifyReviewQuestionPage();
		
		identityReviewQuestionPage.verifyReviewQuestionCancelCTA();
		identityReviewQuestionPage.clickReviewQuestionCancelCTA();
		identityReviewQuestionPage.clickWarningCancelCTA();
		IdentityFraudRejectPage identityFraudRejectPage= new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
	}
	
	
	/**
	 * US450033 - Fraud - Modal for Cancel Button (or back button)
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = Group.AAL_PENDING)
	public void verifyFraudRejectScreenCloseModalThroughcancelNonBYOD(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : US450033 - Fraud - Modal for Cancel Button (or back button)");
		Reporter.log(
				"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
		Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log("10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
		Reporter.log("11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("12. Click on 'Continue' CTA | Payment Information section should be displayed");
        Reporter.log("13. Click on Accept and Continue|Fraud verification page should be displayed");
		Reporter.log("14. Click on Continue|Review question page should be displayed");
		Reporter.log("15. Select cancel |  cancel modal should be displayed");
		Reporter.log("15. Select Yes, cancel this order|Reject Screen will be displayed");
		Reporter.log("16. Verify authorable Text and Header |Authorable Text and Header should be displayed");
		Reporter.log("17. Verify primary and secondary  CTA |'Yes, cancel this order' CTA should be displayed");
		Reporter.log("17. Verify secondary  CTA |'No, answer questions' CTA should be displayed");
		Reporter.log("18. Verify close modal('X')|Close button should be displayed");
		Reporter.log("19. Click 'continue with question'  CTA  |Identity review question page should be displayed");
		Reporter.log("20. click continue |Identity question page should be displayed");                   
		Reporter.log("21. click cancel|warning modal should be  displayed");   
		Reporter.log("22. Click cancel CTA  |Reject screen"); 
		Reporter.log("Actual Output:");
		Reporter.log("================================");
		
		
		navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		
		cartPage.placeOrderCTADisabled();
		cartPage.serviceAgreementCheckBoxDisplayed();
		
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.placeOrderCTAEnabled();
		
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionCancelCTA();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionWarningModal();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionWarningModalTextBody();
		identityReviewIntroductionPage.verifyPrimaryContinueCTAIconOnIdentityReviewIntroductionWarningModal();
		identityReviewIntroductionPage.verifySecondaryCancelCTAIconOnIdentityReviewIntroductionWarningModal();
		identityReviewIntroductionPage.verifyCloseIconOnIdentityReviewIntroductionWarningModal();
		identityReviewIntroductionPage.clickPrimaryContinueCTAIconOnIdentityReviewIntroductionWarningModal();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
		
		IdentityReviewQuestionPage identityReviewQuestionPage=new IdentityReviewQuestionPage(getDriver());
		identityReviewQuestionPage.verifyReviewQuestionPage();
		
		identityReviewQuestionPage.verifyReviewQuestionCancelCTA();
		identityReviewQuestionPage.clickReviewQuestionCancelCTA();
		identityReviewQuestionPage.clickWarningCancelCTA();
		IdentityFraudRejectPage identityFraudRejectPage= new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
	}
	
}
