/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AddBankPage;
import com.tmobile.eservices.qa.pages.payments.AddCardPage;
import com.tmobile.eservices.qa.pages.payments.ClaimPage;
import com.tmobile.eservices.qa.pages.payments.NewAddCardPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayLandingPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.PaymentArrangementPage;
import com.tmobile.eservices.qa.pages.payments.PaymentCollectionPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class SpokePageTest extends PaymentCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(SpokePageTest.class);

	/**
	 * 05_Desktop_MC 2 Series BIN_AutoPay_Set Incorrect Autopay information_Validate
	 * system displays error
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "moved to newaddcardpagetest" })
	public void verifyErrorMessageMCBINSeriesCardsForAutopay(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyErrorMessageMCBINSeriesCardsForAutopay method called in EservicesPaymnetsTest");

		Reporter.log("Test Case : verify Error Message MC BIN Series Cards For Autopay");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill invalid card | Error Message should display");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		NewAutopayLandingPage autopay = navigateToAutoPayPagefromBilling(myTmoData);
		autopay.clickAddPaymentmethod();
		PaymentCollectionPage pcp = new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickAddCard();
		NewAddCardPage addcard = new NewAddCardPage(getDriver());
		addcard.verifyCardPageLoaded();
		addcard.enterCardNumber("222100-272099");
		addcard.Entervalidcardnumber();
		/*
		 * AutoPayPage autopayLandingPage = navigateToAutoPayPage(myTmoData);
		 * autopayLandingPage.verifyAutoPayLandingPageFields(); SpokePage otpSpokePage =
		 * new SpokePage(getDriver()); autopayLandingPage.clickAddPaymentMethod();
		 * 
		 * otpSpokePage.verifyPageLoaded(); otpSpokePage.clickAddCard(); AddCardPage
		 * addCardPage = new AddCardPage(getDriver());
		 * addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		 * addCardPage.fillCardInfo(myTmoData.getPayment());
		 * otpSpokePage.verifyInvalidCCErrorMsgAutoPay();
		 */
	}

	/**
	 * Ensure user can remove a saved payment method
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "to be fixed" })
	public void verifyUserAbleTORemoveSavedPayment(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTORemoveSavedPayment method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Remove SavedPayment");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5.Verify  payment is saved |  payment method shoud be saved");
		Reporter.log("6.Click remove saved checking account | checking account payment should be removed");

		Reporter.log("================================");

		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyRemovecheckingAccountlink();
	}

	/**
	 * Verify a stored payment method can be removed via the OTP payment flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void verifyOnetimepaymentStoredpaymentRemoveoption(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyOnetimepaymentStoredpaymentRemoveoption method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Verify a stored payment method can be removed via the OTP payment flow");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Verify Remove option | Remove options should be displayed");
		Reporter.log("6. Click Remove Option | Confirmation Dialog box will be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		// otpSpokePage.clickRemoveStoredBankLink();
		otpSpokePage.clickRemoveStoredCardIcon();
		otpSpokePage.verifyRemoveStoredCardLink();
	}

	/**
	 * P1_TC_Regression_Desktop_Save payment option_Satndard/Restricted
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void verifyPayBillSaveOptionRestrictedUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPayBillSaveOptionRestrictedUser method");
		Reporter.log("Test Case : Save payment option should not be displayed for Standard/Restricted User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home page Should be displayed");
		Reporter.log("4. Click Pay Bill  | One Time Payment page Should be displayed");
		Reporter.log("5. Verify Save payment option | Save payment option should not be displayed for restricted user");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.verifyNoSavePaymentMethodCheckBox();
		Reporter.log("Save payment option is not loaded for restricted user");
	}

	/**
	 * frozen-monkeys US137398 [R] Payment Module H&S - Entire Payment Type on Neg.
	 * File US129112 [Continued] [R] Payment Module H&S - Entire Payment Type on
	 * Neg. File US137399 [Unfinished] [R] Payment Module H&S - Entire Payment Type
	 * on Neg. File
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testVerifyProhibitedPaymentMethod(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: verify stored payments status | Stored  payments are prohibited for negative file.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.isStoredBankAccountEnabled();
		otpSpokePage.verifyProhibitedCardPayments();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testPaymentsModuleFieldsPlaceHolderText(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click add card | add card spoke Page should be displayed");
		Reporter.log("Step 5: validate all placeholders text | place holders should be displayed correctly");
		Reporter.log("Step 6: click add  bank | add bank spoke Page should be displayed");
		Reporter.log("Step 7: validate all placeholders text | place holders should be displayed correctly");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		// addCardPage.verifyCardNumberplaceHolder("XXXX-XXXX-XXXX-XXXX");
		// addCardPage.verifyCardPageLoaded("Card information");
		addCardPage.verifyCardNumberplaceHolder("Credit Card Number");
		addCardPage.verifyCardNameplaceHolder("Name on Card");
		addCardPage.verifyCardExpDateplaceHolder("Expiration Date");
		// addCardPage.verifyCardExpiryYearplaceHolder("YY");
		addCardPage.verifyCardCVVCodeplaceHolder("CVV");
		addCardPage.verifyCardZipCodeplaceHolder("Zip");
		addCardPage.clickBackBtn();

		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddBank();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		addBankPage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);
		addBankPage.verifyAccountNamePlaceholder("Name on Account");
		addBankPage.verifyRoutingNumberPlaceholder("Routing Number");
		addBankPage.verifyBankAccNumberPlaceholder("Account Number");
		addBankPage.clickBackButton();
		Reporter.log("place holders are displayed correctly");
	}

	/**
	 * US262231 Angular + GLUE - PCM Payment Method Spoke Page (L2) - Generic UI
	 * Story US274287 Angular + GLUE - PCM Payment Method Spoke Page (L2) - Generic
	 * Implementation of Spoke
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEPCMpaymentSpokePageWithSavedPaymentMethods(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262231	Angular + GLUE - PCM Payment Method Spoke Page (L2) - Generic UI Story");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: verify and click payment method blade | Payment Spoke page should be displayed");
		Reporter.log("================================");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.clickAddBank();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		addBankPage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);
		getDriver().navigate().back();
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		getDriver().navigate().back();
		otpSpokePage.verifyStoredPaymentType();
		otpSpokePage.verifyRemovingStoredPayments();
	}

	/**
	 * US280693 Angular + GLUE - PCM Negative File / Expired Card US487844 Angular
	 * 6.0 PCM- Negative file messaging payment method.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEPCMNegativeFileIneligibleBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US280693	Angular + GLUE - PCM Negative File / Expired Card");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 6: verify negative file Ineligible Bank | Error message should be displayed.");
		Reporter.log("================================");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyInvalidBankErrorMsg();
		otpSpokePage.verifyAddBankDisabled();
	}

	/**
	 * US280693 Angular + GLUE - PCM Negative File / Expired Card US487844 Angular
	 * 6.0 PCM- Negative file messaging payment method.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEPCMNegativeFileIneligibleCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US280693	Angular + GLUE - PCM Negative File / Expired Card");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 6: verify negative file Ineligible Card | Error message should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyInvalidCardErrorMsg();
		otpSpokePage.verifyAddCardDisabled();
	}

	/**
	 * US280693 Angular + GLUE - PCM Negative File / Expired Card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEPCMNegativeFileCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US280693	Angular + GLUE - PCM Negative File / Expired Card");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 6: verify negative file card | Error message should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyInvalidCardErrorMsg();
	}

	/**
	 * US280693 Angular + GLUE - PCM Negative File / Expired Card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEPCMNegativeFileIneligibleCardAndBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US280693	Angular + GLUE - PCM Negative File / Expired Card");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 7: verify negative file Ineligible Card & Bank| Error message should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		// Assert.assertTrue(otpSpokePage.verifyInvalidCardErrorMsg(),"Ineligible
		// card error message is not displayed");
		// Reporter.log("Ineligible card error message is displayed");
		// Assert.assertTrue(otpSpokePage.verifyInvalidBankErrorMsg(),"Ineligible
		// bank error message is not displayed");
		// Reporter.log("Ineligible bank error message is displayed");
		otpSpokePage.verifyAddBankDisabled();
		otpSpokePage.verifyAddCardDisabled();
	}

	/**
	 * US262368 Payment Defect Fixes - Payment method incorrectly added to PA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testPaymentMethodCorrectlyAdded(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262368 Payment Defect Fixes - Payment method incorrectly added to PA");
		Reporter.log("Data Conditions - Regular/Sedona user. Eligible for payment arrengment");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 01: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 02: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 03: Click on paybill | One time payment page should be displayed");
		Reporter.log("Step 04: Click on Payment Arrangment link | Verify PA landing page");
		Reporter.log("Step 05: Click on Cancel CTA | Verify exit popup is present");
		Reporter.log("Step 06: Click on YES CTA | One time payment page should be displayed");
		Reporter.log("Step 07: Click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 08: Click on AddBank / AddCard blade | AddBank / AddCard page should be displayed.");
		Reporter.log(
				"Step 09: Provide all bank/card info and save payment method | Payment method should be present on OTP Spoke page");
		Reporter.log("Step 10: Return to OTP landing page | OTP landing page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickCancelButton();
		paymentArrangementPage.clickYesCTAOnCancelModal();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		Long accNo = addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifySavedBankAccount(accNo.toString());

	}

	/**
	 * US304541 : [PHASE 2] UI - L2 - Payment Method Spoke Page - Limit Exceeded
	 * messaging US304523 : [PHASE 2] UI - L2 - Payment Method Spoke Page –
	 * Container
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testPaymentMethodSpokePageExceedMessage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US304541 : [PHASE 2] UI - L2 - Payment Method Spoke Page - Limit Exceeded messaging");
		Reporter.log("US304523 : [PHASE 2] UI - L2 - Payment Method Spoke Page – Container");
		Reporter.log("Data Conditions - PAH or FULL user with 10 stored payment methods on BAN ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Verify Balance amount on Home page | Balance amount should be displayed");
		Reporter.log("Step 4: Click on paybill button | One time payment page should be displayed");
		Reporter.log("Step 5: Click on add payment method  | Payment spoke page should be displayed");
		Reporter.log(
				"Step 6: Verify 10 stored payments are present on spoke page | 10 stored payments should be displayed");
		Reporter.log("Step 7: Click on add card icon | Add Card page should be displayed");
		Reporter.log("Step 8: Click on Add Bank icon | Add Bank page should be displayed");
		Reporter.log("Step 9: Enter vaild details  | Details should be entered successfully");
		Reporter.log("Step 10: Click the save this payment check box | Warning Message should be displayed");
		Reporter.log(
				"Step 11: Verify the in session payment method radio button | In session Payment Method should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyRemovebuttonscount(10);
		otpSpokePage.clickAddBank();

		AddBankPage addbankpage = new AddBankPage(getDriver());
		addbankpage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);

		addbankpage.Checkdisabilityofsavepayment();
		// addbankpage.fillBankInfo(myTmoData.getPayment());

		// addbankpage.verifyAndSelectSavePaymentOption();
		addbankpage.verifyWalletsizereachedtext("Your wallet is full! Please delete a method to save another.");

		addbankpage.clickBackButton();
		// addbankpage.clickContinueAddBank();
		// OneTimePaymentPage oneTimePaymentPage = new
		// OneTimePaymentPage(getDriver());
		// oneTimePaymentPage.clickPaymentMethodBlade();
		// otpSpokePage.verifyInsessionPayment();

		otpSpokePage.clickAddCard();

		AddCardPage addcardpage = new AddCardPage(getDriver());
		addcardpage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		// addcardpage.verifyAndSelectSavePaymentOption();
		// addcardpage.fillCardInfo(myTmoData.getPayment());
		addcardpage.Checkdisabilityofsavepayment();
		addcardpage.verifyWalletsizereachedtext("Your wallet is full! Please delete a method to save another.");
	}

	/**
	 * US372584 : [PHASE 2] UI - L2 - Payment Method Spoke Page - Negative
	 * file/expired.
	 * 
	 * Container
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testPaymentMethodSpokePageNegativeFileExpired(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US372584 : [PHASE 2] UI - L2 - Payment Method Spoke Page - Negative file/expired.");
		Reporter.log("Data Conditions - PAH or FULL user with expired/negative file stored payment methods on BAN ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill button | One time payment page should be displayed");
		Reporter.log("Step 4: Click on add payment method  | Paymnet spoke page should be displayed");
		Reporter.log(
				"Step 5: Click on expired/negative file stored payments  present on spoke page | Expired/Negative file should be disabled and Warning  should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		ArrayList<String> messages = new ArrayList<String>();
		messages.add("Card is EXPIRED. Please add a new card.");
		messages.add("Sorry, we can't accept payments made with this card. Please use a different payment method.");

		otpSpokePage.checkdisabledpaymentmethods(messages);

	}

	/**
	 * US372584 : [PHASE 2] UI - L2 - Payment Method Spoke Page - Negative
	 * file/expired.
	 * 
	 * Container
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "sreekanth")
	public void testPaymentMethodremove(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US375516 : [Continued] [PHASE 2] UI- L2- Delete Card/Bank-UI");
		Reporter.log("Data Conditions - PAH or FULL user with expired/negative file stored payment methods on BAN ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill button | One time payment page should be displayed");
		Reporter.log("Step 4: Click on add payment method  | Paymnet spoke page should be displayed");
		Reporter.log("Step 5: delete any payment method |Payment method should be deleted ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		if (otpSpokePage.gettotalremovebuttons() == 0) {
			otpSpokePage.clickAddBank();
			AddBankPage addbankpage = new AddBankPage(getDriver());
			addbankpage.validateAddBankAllFields(PaymentConstants.Add_bank_Header);
			OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
			oneTimePaymentPage.verifyPageLoaded();
			oneTimePaymentPage.clickPaymentMethodBlade();
			otpSpokePage.verifyPageLoaded();
		}
		int storedpaymentmethods = otpSpokePage.gettotalremovebuttons();
		WebElement ele = otpSpokePage.identifypaymentmethodtoberemoved();
		otpSpokePage.clickonselectedremovebutton(ele);
		otpSpokePage.clickcorrespondingYesbutton(ele);
		otpSpokePage.verifyPageLoaded();
		if (otpSpokePage.gettotalremovebuttons() == storedpaymentmethods - 1)
			Reporter.log("Stored payment remove button is performed");
		else
			Assert.fail("Stored payment remove button is not performed");
	}

	/**
	 * US445095-Angualar 6 PCM Delete payment method
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testverifyDeleteSavedPaymentMethodCard(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US445095-Angualar 6 PCM Delete payment method");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved card .");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment  Spoke Page should be displayed");
		Reporter.log("Step 6: click on  delete Ion of Saved Card | Delete payment method modal should be displayed");
		Reporter.log("Step 7: verify 'Delete this card?' Header|'Delete this card?' Header should be displayed");
		Reporter.log(
				"Step 8: Verify 'Are you sure you want to delete:' sub header on the modal|'Are you sure you want to delete:' sub header on the modal should be displayed");
		Reporter.log(
				"Step 9: Verify payment method information | overwritten payment method ( ****XXXX is the last 4 of the payment method) on the modal should be displayed");
		Reporter.log(
				"Step 10: Verify No thanks and Yes, delete CTAs  | No thanks and Yes, delete CTAs should be displayed");
		Reporter.log("Step 11: click on Yes, delete CTA |  payment method should be deleted");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		SpokePage otpSpokePage = navigateToNewSpokePage(myTmoData);
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickDeletLinkAndVerifyCardDeleteModel(PaymentConstants.Card_Delete_Model_Header,
				PaymentConstants.Delete_Model_Sub_Header);

	}

	/**
	 * US445095-Angualar 6 PCM Delete payment method
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testverifyDeleteSavedPaymentMethodBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US445095-Angualar 6 PCM Delete payment method");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved Bank .");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment  Spoke Page should be displayed");
		Reporter.log("Step 6: click on  delete Ion of Saved Bank | Delete payment method modal should be displayed");
		Reporter.log(
				"Step 7: verify 'Delete this bank account?' Header|'Delete this bank account?' Header should be displayed");
		Reporter.log(
				"Step 8: Verify 'Are you sure you want to delete:' sub header on the modal|'Are you sure you want to delete:' sub header on the modal should be displayed");
		Reporter.log(
				"Step 9: Verify payment method information | overwritten payment method ( ****XXXX is the last 4 of the payment method) on the modal should be displayed");
		Reporter.log(
				"Step 10: Verify No thanks and Yes, delete CTAs  | No thanks and Yes, delete CTAs should be displayed");
		Reporter.log("Step 11: click on Yes, delete CTA |  payment method should be deleted");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		SpokePage otpSpokePage = navigateToNewSpokePage(myTmoData);
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickDeletLinkAndVerifyBankDeleteModel(PaymentConstants.Bank_Delete_Model_Header,
				PaymentConstants.Delete_Model_Sub_Header);
	}

	/**
	 * US484470 Angular 6.0 PCM- Negative file messaging payment Type/BAN
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testPCMNegativeFileOnBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US484470 Angular 6.0 PCM- Negative file messaging payment Type/BAN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 6: verify negative file Ineligible Bank | Error message should be displayed.");
		Reporter.log("================================");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyInvalidBankErrorMsg();
		otpSpokePage.verifyAddBankDisabled();
	}

	/**
	 * US484470 Angular 6.0 PCM- Negative file messaging payment Type/BAN
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testPCMNegativeFileOnCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US484470 Angular 6.0 PCM- Negative file messaging payment Type/BAN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 6: verify negative file Ineligible Card | Error message should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyInvalidCardErrorMsg();
		otpSpokePage.verifyAddCardDisabled();
	}

	/**
	 * US484470 Angular 6.0 PCM- Negative file messaging payment Type/BAN
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testPCMNegativeFileOnBAN(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US484470 Angular 6.0 PCM- Negative file messaging payment Type/BAN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 6: verify negative file on Ban | Error message should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyInvalidErrorMsgOnBAN();
		otpSpokePage.verifyAddCardDisabled();
		otpSpokePage.verifyAddBankDisabled();
	}

	/**
	 * US544509 Migration treatment Lock stored payment method until verified.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyMigrationTreatmentLockStoredPaymentMethodUntilVerified(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : US544509 Migration treatment Lock stored payment method until verified.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log(
				"4. Go to the stored payment and check the blade whether disable or not |  The blade should be greyed out");
		Reporter.log(
				"5. Check for the message 'you must verify this payment method' | Should be able to see the message");
		Reporter.log("6. Check for the Chevron | Should be able to see the Chevron");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyStoredPaymentMethodDisabled();
		spokePage.verifyPaymentVerificationmessage();
		spokePage.verifyChevronForDisabledStoredPaymentMethod();
	}

	/**
	 * US544495 Modify stored payment method UI blade to display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyModifiedStoredPaymentMethodUI(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US544495 Modify stored payment method UI blade to display");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the Spoke page |  Spoke page should be displayed");
		Reporter.log("5. Check for the 'Name' in stored payment method blade | Should be able to see the 'Name'");
		Reporter.log("6. Check for the 'Default' in stored payment method blade | Should be able to see the 'Default'");
		Reporter.log(
				"7. Check for the 'Expiration date' in stored payment method blade | Should be able to see the 'Expiration date'");
		Reporter.log(
				"8. Check for the Chevron | Should be able to see the Chevron for the stored payment method blade");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyModifiedStoredPaymentMethodBlade();
	}

	/**
	 * US546081 1.5 Expiration date messaging logic US602056: Search v4 angular 1.5
	 * integration with V4 dormancy flag
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void verifyExpirationDateMessagingLogic(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US546081 1.5 Expiration date messaging logic");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the New Spoke page | New Spoke page should be displayed");
		Reporter.log(
				"5. Check for the 'Expired MM/YY' in stored payment method blade for expired saved method| Should be able to see the 'Expired MM/YY'");
		Reporter.log(
				"6. Check for the 'Expiring soon MM/YY' in stored payment method blade for expiring soon saved method| Should be able to see the 'Expiring soon MM/YY'");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyExpirationMsgStoredPaymentMethod("Expired");
		// spokePage.verifyExpirationMsgStoredPaymentMethod("Expiring soon");

	}

	/**
	 * US524296 Angular 1.5 PCM Spoke Page-page Nudge
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifySpokePageNudge(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US524296 Angular 1.5 PCM Spoke Page-page Nudge");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the Spoke page | Spoke page should be displayed");
		Reporter.log("5. Check for the 'Nudge' | Should be able to verify Nudge");
		Reporter.log("6. verify messaging on the Nudge | Should be able to see the messaging");
		Reporter.log("7. Verify the link in the Nudge | Should be able to see the link");
		Reporter.log("8. Click on the link | Should be able to redirect the user to a modal popup");
		Reporter.log("9. Verify the text in the popup | Should be able to see the text");
		Reporter.log("10. Check for the Close CTA | Should be able to verify the Close CTA at the bottom of the page");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyNudge();
		spokePage.verifyMsgOnNudge();
		spokePage.verifyandClickLinkOnNudge();
		spokePage.verifyNudgeModalPopUp();
		spokePage.verifyCloseCTAOnNudgePopUp();
	}

	/**
	 * US524277: Angular 1.5 PCM Spoke Page-In blade treatment
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifySpokePageInBaldeTreatment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US524277: Angular 1.5 PCM Spoke Page-In blade treatment");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log(
				"4. Go to the stored payment and check the blade whether disable or not |  The blade should be greyed out");
		Reporter.log(
				"5. Check for the message 'you must verify this payment method' | Should be able to see the message");
		Reporter.log("6. Check for the Chevron | Should be able to see the Chevron");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyStoredPaymentMethodDisabled();
		spokePage.verifyPaymentVerificationmessage();
		spokePage.verifyChevronForDisabledStoredPaymentMethod();
		spokePage.clickLinkOnGreyedOutBlade();

	}

	/**
	 * US570468: 1.5 implement Wallet Full functionality
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyWalletFullFunctionality(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US570468: 1.5 implement Wallet Full functionality");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the Spoke page | Spoke page should be displayed");
		Reporter.log("5. Check for the 'Wallet Full' message | Should be able to see the 'Wallet Full' message");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyWalletFullMsg(PaymentConstants.WALLET_FULL_MESSAGE);
		spokePage.verifyAddBankDisabled();
		spokePage.verifyAddCardDisabled();
	}

	/**
	 * US579700 PII Masking update
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPIIMaskingOnOTPpages(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US356344	PII Masking > Variables on OTP Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: verify PII masking | PII masking attribute should be present for payment method blade");
		Reporter.log("Step 5: click on amount Icon| amount spoke page should be displayed");
		Reporter.log("Step 6: setup other amount and update| amount should be updated");
		Reporter.log("Step 7: click add payment method| payment spoke page should be displayed");
		Reporter.log("Step 8: verify PII masking | PII masking attribute should be present for stored payment details");
		Reporter.log("Step 9: click on add Bank| Bank information page should be displayed");
		Reporter.log("Step 10: Fill all bank details and click continue button| OTP page should be displayed");
		Reporter.log("Step 11: click submit button| OTP confirmation page should be displayed");
		Reporter.log("Step 12: verify all bank payment details| Bank payment details should be displayed correct.");
		Reporter.log(
				"Step 13: verify PII masking | PII masking attributes should be present for all payment information");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyPIIMasking(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		spokePage.clickAddBank();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		addBankPage.verifyPiiMasking(PaymentConstants.PII_CUSTOMER_CUSTOMERNAME_PID,
				PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		addBankPage.clickBackButton();
		spokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyPiiMasking(PaymentConstants.PII_CUSTOMER_CUSTOMERNAME_PID,
				PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID, PaymentConstants.PII_CUSTOMER_ZIPCODE_PID);
		getDriver().navigate().to(System.getProperty("environment") + "/claimflow");
		ClaimPage claimPage = new ClaimPage(getDriver());
		claimPage.verifyPiiMasking(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
	}

	/**
	 * US602056: Search v4 angular 1.5 integration with V4 dormancy flag
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testverifytenPaymentMethods(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US602056,US602057: Search v4  angular 1.5  integration with V4 dormancy flag");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the Spoke page | Spoke page should be displayed");
		Reporter.log("5. Check for the 10 Payment Methods are addedd | Should be able to see the 10 Payment Methods");
		Reporter.log(
				"6. Check for the 10 Payment Methods wallet Message | 'Your Wallet is full, you must delete a payment method before you can save another' is displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyWalletFullMsg(PaymentConstants.WALLET_FULL_MESSAGE);
		if (spokePage.gettotalPaymentbuttons() == 10) {
			Reporter.log("Total no of Payment Method count: " + spokePage.gettotalPaymentbuttons());
		}

	}

	/**
	 * US602056: Search v4 angular 1.5 integration with V4 dormancy flag
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testtoverifyDefaultPaymentMethods(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US602056,US602057: Search v4  angular 1.5  integration with V4 dormancy flag");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the Spoke page | Spoke page should be displayed");
		Reporter.log("5. Check for Defualt Payment Method | Default Payment Method should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyDefaultPaymentMethod();
	}

}