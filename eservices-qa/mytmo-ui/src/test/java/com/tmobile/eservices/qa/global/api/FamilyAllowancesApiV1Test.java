package com.tmobile.eservices.qa.global.api;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.FamilyAllowancesApiV1;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import groovy.json.StringEscapeUtils;
import io.restassured.response.Response;

public class FamilyAllowancesApiV1Test extends FamilyAllowancesApiV1 {

	public Map<String, String> tokenMap;

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "FamilyAllowancesApiV1", "testGetParent",Group.GLOBAL, Group.APIREG })
	public void testGetParent(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGetParent test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with profile details.");
		Reporter.log("Step 2: Verify profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		Response response = getParent(apiTestData, tokenMap);
		String operationName = "getParent";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String ban = jsonNode.path("ban").textValue();
				String statusCode = jsonNode.path("statusCode").textValue();
				String statusMessage = jsonNode.path("statusMessage").textValue();
				Assert.assertEquals(ban, tokenMap.get("ban"), "msisdn invalid.");
				Assert.assertNotEquals(statusMessage, "", "statusMessage is Null or Empty.");
				Assert.assertNotEquals(statusCode, "", "statusCode is Null or Empty.");
			}

		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "FamilyAllowancesApiV1", "testGetMemoDetails",Group.GLOBAL, Group.APIREG })
	public void testGetMemoDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGetMemoDetails test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with profile details.");
		Reporter.log("Step 2: Verify profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
        Calendar calCurr = Calendar.getInstance();
        calCurr.setTime(date);
        calCurr.add(Calendar.DATE, 2);
        Date toDate = calCurr.getTime();
        tokenMap.put("toDate", sdf.format(toDate));
        calCurr.add(Calendar.DATE, -20);
        Date fromDate = calCurr.getTime();
        tokenMap.put("fromDate", sdf.format(fromDate));
		String requestBody = new ServiceTest().getRequestFromFile("FamilyAllowances_getMemoDetails.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getMemoDetails(apiTestData,updatedRequest,tokenMap);
		String operationName = "getMemoDetails";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode = jsonNode.path("statusCode").textValue();
				String statusMessage = jsonNode.path("statusMessage").textValue();
				Assert.assertNotEquals(statusMessage, "", "statusMessage is Null or Empty.");
				Assert.assertNotEquals(statusCode, "", "statusCode is Null or Empty.");
			}

		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "FamilyAllowancesApiV1", "testGetUsageSummary",Group.GLOBAL, Group.APIREG })
	public void testGetUsageSummary(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGetUsageSummary test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with profile details.");
		Reporter.log("Step 2: Verify profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		String operationName = "getUsageSummary";
		String requestBody="{\"pageNumber\":\"1\",\"msisdnList\":[\""+apiTestData.getMsisdn()+"\"]}";
		logRequest(requestBody, operationName);
		Response response = getUsageSummary(apiTestData,requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode = jsonNode.path("statusCode").textValue();
				String statusMessage = jsonNode.path("statusMessage").textValue();
				Assert.assertNotEquals(statusMessage, "", "statusMessage is Null or Empty.");
				Assert.assertNotEquals(statusCode, "", "statusCode is Null or Empty.");
			}

		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "FamilyAllowancesApiV1", "testSetParent",Group.GLOBAL, Group.APIREG })
	public void testSetParent(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testSetParent test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with profile details.");
		Reporter.log("Step 2: Verify profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		Response response = getParent(apiTestData, tokenMap);
		String operationName = "getParent";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode = jsonNode.path("statusCode").textValue();
				String banSeqNo = jsonNode.path("banSeqNo").textValue();
				String statusMessage = jsonNode.path("statusMessage").textValue();
				Assert.assertEquals(statusMessage, "Success", "statusMessage is Null or Empty.");
				Assert.assertEquals(statusCode, "0", "statusCode is Null or Empty.");
				Assert.assertNotEquals(banSeqNo, "", "banSeqNo is Null or Empty.");
				tokenMap.put("banSeqNo", banSeqNo);
				
			}

		} else {
			failAndLogResponse(response, operationName);
		}
		String requestBody = new ServiceTest().getRequestFromFile("FamilyAllowances_setParent.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response = setParent(apiTestData,updatedRequest, tokenMap);
		operationName = "setParent";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String code = jsonNode.path("code").textValue();
				String message = jsonNode.path("message").textValue();
				Assert.assertNotEquals(code, "", "code invalid.");
				Assert.assertNotEquals(message, "", "message is Null or Empty.");
			}

		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "FamilyAllowancesApiV1", "testUpdateUsageSettings",Group.GLOBAL, Group.PENDING })
	public void testUpdateUsageSettings(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testUpdateUsageSettings test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with profile details.");
		Reporter.log("Step 2: Verify profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		String operationName = "getUsageSummary";
		String requestBody="{\"pageNumber\":\"1\",\"msisdnList\":[\""+apiTestData.getMsisdn()+"\"]}";
		logRequest(requestBody, operationName);
		Response response = getUsageSummary(apiTestData,requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode = jsonNode.path("statusCode").textValue();
				String statusMessage = jsonNode.path("statusMessage").textValue();
				Assert.assertNotEquals(statusMessage, "", "invalid statusMessage.");
				Assert.assertNotEquals(statusCode, "", "invalid statusCode.");
				JsonNode lineUsage = jsonNode.path("lineUsage");
				JsonNode lu=lineUsage.get(0);
				tokenMap.put("lineUsage",lu.toString() );
			}

		} else {
			failAndLogResponse(response, operationName);
		}
		requestBody = new ServiceTest().getRequestFromFile("FamilyAllowances_updateUsageSettings.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		updatedRequest=StringEscapeUtils.unescapeJava(updatedRequest);
		response = updateUsageSettings(apiTestData, updatedRequest,tokenMap);
		operationName = "updateUsageSettings";
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode = jsonNode.path("statusCode").textValue();
				String statusMessage = jsonNode.path("statusMessage").textValue();
				Assert.assertEquals(statusCode, "0", "invalid statusCode.");
				Assert.assertEquals(statusMessage, "Success", "invalid statusMessage.");
			}

		} else {
			failAndLogResponse(response, operationName);
		}
	}
	


/**
 * @param data
 * @param apiTestData
 * @throws Exception
 */
@Test(dataProvider = "byColumnName", enabled = true, groups = { "FamilyAllowancesApiV1", "testEosChangesForMidCycle",Group.GLOBAL, Group.APIREG })
public void testEosChangesForMidCycle(ControlTestData data, ApiTestData apiTestData) throws Exception {
	Reporter.log("TestName: testEosChangesForMidCycle test");
	Reporter.log("Data Conditions:MyTmo registered misdn.");
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Get the response from subscriber lines API.");
	Reporter.log("Step 2: Verify response returned successfully or not");
	Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
	Reporter.log("Step 3: Verify Effective Date Format |Format should be 'YYYY-MM-DD'");
	Reporter.log("================================");
	Reporter.log("Actual Result:");
	tokenMap = new HashMap<String, String>();
	tokenMap.put("ban", apiTestData.getBan());
	tokenMap.put("msisdn", apiTestData.getMsisdn());
	tokenMap.put("password", apiTestData.getPassword());
	tokenMap.put("version", "v1");
	String operationName = "getMidCycleChanges";
	Response response = getMidCycleChanges(apiTestData, tokenMap);
	System.out.println(response.getStatusCode());
	System.out.println(response.getBody());
	if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
		logSuccessResponse(response, operationName);
		System.out.println("res "+response.getBody());
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(response.asString());
		if (!jsonNode.isMissingNode()) {
			String billingStartDate = jsonNode.path("billingStartDate").textValue();
			Assert.assertTrue(isValidFormat("yyyy-MM-dd",billingStartDate),"Invalid billingStartDate format.");
			String billingEndDate = jsonNode.path("billingEndDate").textValue();
			Assert.assertTrue(isValidFormat("yyyy-MM-dd",billingEndDate),"Invalid billingEndDate format.");
		}

	} else {
		failAndLogResponse(response, operationName);
	}
	
}
}
