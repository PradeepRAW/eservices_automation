package com.tmobile.eservices.qa.payments.api;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.PaymentsFdpApiV3;

import io.restassured.response.Response;

public class PaymentsFdpApiV3Test extends PaymentsFdpApiV3 {
	
	public Map<String, String> tokenMap;
	
	/**
	/**
	 * UserStory# US473443:MyTMO - PAYMENTS - Tokenization - FDP-Order Orchestration-EOS Setup - Credit Card;
	 *          # US473446:MyTMO - PAYMENTS - Tokenization - FDP Payment Saves;
	 *          # US572184:FDP-Start using DPS inflow save.
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsFdpApiV3Test","testPaymentsFdpApiWithCard",Group.PAYMENTS,Group.SPRINT  })
	public void testPaymentsFdpApiWithCard(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testPaymentsFdpApiWithCard");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with PaymentsFdpApiV3.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testPaymentsFdpApiWithCard";
		
		tokenMap = new HashMap<String, String>();
		
		String requestBody =  new ServiceTest().getRequestFromFile("PaymentsFdpwithCardV3.txt");	
        
        tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("primaryMsisdn", apiTestData.getPrimaryMsisdn());
		tokenMap.put("email", apiTestData.getPrimaryMsisdn()+"@yopmail.com");
		tokenMap.put("chargeAmount", String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
        Calendar calCurr = Calendar.getInstance();
        calCurr.setTime(date);
        calCurr.add(Calendar.DATE, 2);
        Date twoDaysAheadDate = calCurr.getTime();
        tokenMap.put("fdpdate", sdf.format(twoDaysAheadDate));
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
			
		Response response = PaymentsFdpApi(apiTestData, updatedRequest, tokenMap);
	
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"payment.isSave"), 
						"true", "Card is not saved");				
				Assert.assertEquals(getPathVal(jsonNode,"payment.statusCode"),
						"A" , "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"payment.reasonDescription"), 
						"Transaction completed successfully", "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"payment.statusMessage"), 
						"Approved  Successfully ", "Status code is mismatched");
			}
		} else {
			
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# US473445:MyTMO - PAYMENTS - Tokenization -FDP-Order Orchestration-EOS Setup - Bank;
	 *          # US473446:MyTMO - PAYMENTS - Tokenization - FDP Payment Saves;
	 *          # US572184:FDP-Start using DPS inflow save.
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsFdpApiV3Test","testPaymentsFdpApiWithBank",Group.PAYMENTS,Group.SPRINT  })
	public void testPaymentsFdpApiWithBank(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testPaymentsFdpApiWithBank");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with PaymentsFdpApiV3.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testPaymentsFdpApiWithBank";
		
		tokenMap = new HashMap<String, String>();
		
		String  requestBody = new ServiceTest().getRequestFromFile("PaymentsFdpwithBankV3.txt");	
       
        tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("primaryMsisdn", apiTestData.getPrimaryMsisdn());
		tokenMap.put("email", apiTestData.getPrimaryMsisdn()+"@yopmail.com");
		tokenMap.put("chargeAmount", String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));
		//String cardNumberEncrypted=getEncryptedCardNumber(apiTestData,tokenMap);
		//tokenMap.put("cardNumber", cardNumberEncrypted);
		//System.out.println(String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
        Calendar calCurr = Calendar.getInstance();
        calCurr.setTime(date);
        calCurr.add(Calendar.DATE, 2);
        Date twoDaysAheadDate = calCurr.getTime();
        tokenMap.put("fdpdate", sdf.format(twoDaysAheadDate));

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
				
		Response response = PaymentsFdpApi(apiTestData, updatedRequest, tokenMap);
	
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"payment.statusCode"),
						"Completed" , "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"payment.reasonDescription"), 
						"Transaction completed successfully", "Reason code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"payment.statusMessage"), 
						"Approved  Successfully approved", "Status code is mismatched");
			}
		} else {
			
			failAndLogResponse(response, operationName);
		}
	
	}
	
	
	
	/**
	/**
	 * UserStory# US473449:MyTMO - PAYMENTS - Tokenization - FDP-Order Orchestration-EOS Cancel - Credit Card;
	 *          # US473451:MyTMO - PAYMENTS - Tokenization - FDP-Order Orchestration-EOS Cancel - Bank;
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsFdpApiV3Test","testPaymentsFdpApiDelete",Group.PAYMENTS,Group.SPRINT  })
	public void testPaymentsFdpApiDelete(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testPaymentsFdpApiDelete");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with PaymentsFdpApiV3.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testPaymentsFdpApiDelete";
		
		tokenMap = new HashMap<String, String>();
       
        tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
				
			 Response response = PaymentsFdpApiDelete(apiTestData,tokenMap,apiTestData.getBan());
		
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				Assert.assertTrue(response.statusCode()==200);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {
					Assert.assertEquals(getPathVal(jsonNode,"status.statusCode"),
							"COMPLETED" , "Reason code is mismatched");
					Assert.assertEquals(getPathVal(jsonNode,"payment.messages.uiMessage"),
							"[Transaction completed successfully]","Reason code is mismatched");
					
				}
			} else {
				
				failAndLogResponse(response, operationName);
			}
		
		
	
	
	}
}
