package com.tmobile.eservices.qa.commonlib;

/***
 * This class indicates test script life cycle in various stages happening 
 * in eservices from development to production, please follow for the accurate results
 * this avoids to manage test in TestNG xmls, these group filters will be added in TestNG suites.
 * 
 * @author tsiripireddy
 *
 */
public class Group {
	
	/***
	 * add this group while test is under development and it will run in local jenkins.
	 */
	public static final String SPRINT = "sprint";
	/***
	 * add this group when test is ready to run in devops jenkins
	 */
	public static final String PROGRESSION ="progression";
	/***
	 * add this group when test is ready to run in devops jenkins
	 */
	public static final String REGRESSION ="regression";
	/***
	 * add this group when test is ready for RELEASE but not released yet
	 */
	public static final String RELEASE_READY ="ready for release";
	/***
	 * add this group when test is MARKED for RELEASE progression
	 */
	public static final String RELEASE ="release";	
	/***
	 * add this group when test is MARKED for RELEASE progression for Mobile
	 */
	public static final String RELEASE_MOBILE ="release mobile";	
	/***
	 * add this group when test is required for SHOP capability regression
	 */
	
	public static final String SHOP ="shop";
	/***
	 * add this group when test is required for ACCOUNTS capability regression
	 */
	public static final String ACCOUNTS ="accounts";
	
	/***
	 * add this group when test is not required for Mobile platform
	 */
	public static final String NON_MOBILE ="nonmobile";
	
	/***
	 * add this group when test is required for ACCOUNTS MANAGE ADD ONS capability regression
	 */
	public static final String ACCOUNTS_MANAGEADDONS ="accounts manage addons";
	
	/***
	 * add this group when test is required for ACCOUNTS MANAGE ADD ONS capability regression
	 */
	public static final String ACCOUNTS_PLANBENEFITS ="planBenefits";
	
	/***
	 * add this group when test is required for ACCOUNTS MANAGE ADD ONS capability regression
	 */
	public static final String ACCOUNTS_PHONEPAGE ="phonePage";
	
	/***
	 * add this group when test is required for change plan capability regression
	 */
	public static final String ACCOUNTS_CPS ="cps";
	
	/***
	 * add this group when test is required for PAYMENTS capability regression
	 */
	public static final String PAYMENTS ="payments";
	/***
	 * add this group when test is required for GLOBAL capability regression
	 */
	public static final String GLOBAL ="global";
	/***
	 * add this group when test is READY for run in full regression i.e Chrome,IE and FF
	 * When you add this group it will run in above three browsers
	 */
	public static final String DESKTOP ="desktop";
	
	/***
	 * add this group when test scripts belongs to TPP
	 */
	public static final String TPP ="tpp";
	
	/***
	 * add this group when test scripts belongs to TPP 
	 */
	public static final String TPP_CHECK ="tpp_check";
	
	/***
	 * add this group when test scripts belongs to POD
	 */
	public static final String POD ="pod";
	
	/***
	 * add this group when test is required for ANDROID run in full regression
	 */
	public static final String ANDROID ="android";
	/***
	 * add this group when test is required for IOS run in full regression
	 */
	public static final String IOS ="ios";
	/***
	 * add this group when test is marked for retired
	 */
	public static final String RETIRED ="retired";
	
	/***
	 * add this group when test is marked for retired
	 */
	public static final String PROMO_REGRESSION ="promo";
	/***
	 * add this group when test is marked for Blocked due to any reason 
	 * eg: Test data
	 */
	public static final String PENDING ="pending";
	
	/***
	 * add this group when test is marked for Blocked due to any reason 
	 * eg: Test data
	 */
	public static final String PENDING_REGRESSION ="pendingregression";
	
	/***
	 * add this group when test is required to exclude in certain suites due to any reason 
	 * eg: Test data
	 */
	public static final String EXCLUDE ="exclude";
	
	/***
	 * add this group for all those tests related to SEDONA 
	 * 
	 */
	public static final String SEDONA ="sedona";
	
	/***
	 * add this group when test functionality is already covered in other Test 
	 * 
	 */
	public static final String DUPLICATE = "duplicate";

	/***
	 * add this group for running the test cases for Performance team
	 *
	 */
	public static final String PERFORMANCE = "performance";
	
	/***
	 * add this group for running the test cases for Performance team
	 *
	 */
	public static final String TMNG = "tmng";
	
	/***
	 * add this group for running the api test cases for regression
	 *
	 */
	public static final String APIREG = "apiregression";
	
	/***
	 * add this group for running test scripts in 5 diff browsers for in regression only few test cases
	 *
	 */
	public static final String MULTIREG = "multiregression";
	
	/***
	 * add this group for running the AAL test cases for regression
	 *
	 */
	public static final String AAL = "aal";
	/***
	 * add this group for running the AAL test cases for regression
	 *
	 */
	public static final String PENDING_AAL = "pending_aal";
	/***
	 * add this group for running the flex test cases for regression
	 *
	 */
	public static final String FLEX = "flex";
	
	/***
	 * add this group for running the flex  test cases for sprint
	 *
	 */
	public static final String FLEX_SPRINT = "flex_sprint";
	
	/***
	 * add this group for running the ondevicefullfillment api test cases for regression
	 *
	 */
	public static final String FULLFILLMENT = "ondevicefullfillment";

	/***
	 * add this group for running the smoke job
	 *
	 */
	public static final String SMOKE ="smoke";
	
	public static final String PROFILE ="profile";

	/***
	 * add this group for running the aal pending tests
	 *
	 */
	public static final String AAL_PENDING ="aal_pending";
	
	/***
	 * add this group for running the Tier_Six tests
	 *
	 */
	public static final String TIER_SIX ="tier_six";
	
	/***
	 * add this group for running the UAT Flows tests
	 *
	 */
	public static final String UAT ="UAT";
	
	/***
	 * These aal tests were already in live and no need of regression(Covered in other regression flows)
	 *
	 */
	public static final String AAL_REGRESSION ="aal_regression";
	public static final String AALFRAUDCHECK = "aal_fraudcheck";
	
	/***
	 * add this group for running the prepaid  test cases for sprint
	 *
	 */
	public static final String PREPAID = "prepaid";
	
	/***
	 * add this group for running the all the PUB stories ready for release
	 *
	 */
	public static final String PUB_RELEASE = "pub_release";
}
