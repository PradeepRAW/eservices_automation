package com.tmobile.eservices.qa.accessibility;

import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;

public class GlobalPages extends GlobalCommonLib {
	AccessibilityCommonLib accessibilityCommonLib = new AccessibilityCommonLib();

	/**
	 * Verify ContactUsPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testContactUsPage(ControlTestData data, MyTmoData myTmoData) {
	navigateToContactUsPage(myTmoData);
	accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ContactUsPage");
		accessibilityCommonLib.testAccessibility(getDriver(),"ContactUsPage");
	}
		/**
		 * Verify PrivacyAndPolicyPage Information
		 * 
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
		public void testPrivacyAndPolicyPage(ControlTestData data, MyTmoData myTmoData) {
	navigateToPrivacyAndPolicyPage(myTmoData);
	accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PrivacyAndPolicyPage");
			accessibilityCommonLib.testAccessibility(getDriver(),"PrivacyAndPolicyPage");
		}

	/**
	 * Verify InterestBasedAdsPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testInterestBasedAdsPage(ControlTestData data, MyTmoData myTmoData) {
		 navigateToInterestBasedAdsPage(myTmoData);
		 accessibilityCommonLib.collectPageServiceCalls(getDriver(),"InterestBasedAdsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "InterestBasedAdsPage");
	}

	/**
	 * Verify TermsAndConditionsPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testTermsAndConditionsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTermsAndConditionsPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"TermsAndConditionsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "TermsAndConditionsPage");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testCoveragePage(ControlTestData data, MyTmoData myTmoData) {
		 navigateToCoveragePage(myTmoData);
		 accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PlanPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "PlanPage");
	}

	/**
	 * Verify ReturnPolicyPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testReturnPolicyPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToReturnPolicyPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ReturnPolicyPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "ReturnPolicyPage");
	}

	/**
	 * Verify SupportPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testSupportPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToSupportPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"SupportPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "SupportPage");
	}

	/**
	 * Verify StoreLocatorPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void verifyStoreLocatorPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToStoreLocatorPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"StoreLocatorPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "StoreLocatorPage");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testTmobilePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTmobilePage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"TmobilePage");
		accessibilityCommonLib.testAccessibility(getDriver(), "TmobilePage");
	}

	/**
	 * Verify TMobileIDPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testTMobileIDPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTMobileIDPage(myTmoData, "T-Mobile ID");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"TMobileIDPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "TMobileIDPage");
	}

	/**
	 * Verify LineSettingsPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testLineSettingsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSettingsPage(myTmoData, "Line Settings");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"LineSettingsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "LineSettingsPage");
	}

	/**
	 * Verify MediaSettingsPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testMediaSettingsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToMediaSettingsPage(myTmoData, "Media Settings");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"MediaSettingsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "MediaSettingsPage");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testBlockingPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToBlockingPage(myTmoData, "Blocking");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"BlockingPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "BlockingPage");
	}


	/**
	 * Verify FamilyControlsPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testFamilyControlsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToFamilyControlsPage(myTmoData, "Family Controls");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"FamilyControlsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "FamilyControlsPage");
	}

	/**
	 * Verify MultipleDevicesPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testMultipleDevicesPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToMultipleDevicesPage(myTmoData, "Multiple Devices");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"MultipleDevicesPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "MultipleDevicesPage");
	}

	/**
	 * Verify CoverageDevicePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testCoverageDevicePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToCoverageDevicePage(myTmoData, "Coverage Device");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"CoverageDevicePage");
		accessibilityCommonLib.testAccessibility(getDriver(), "CoverageDevicePage");
	}

	/**
	 * Verify EmployeeLineDesignationPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testEmployeeLineDesignationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEmployeeLineDesignationPage(myTmoData, "Employee Line Designation");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"EmployeeLineDesignationPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "EmployeeLineDesignationPage");
	}

	/**
	 * Verify ProfilePageAndClickOnLink Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testProfilePageAndClickOnLink(ControlTestData data, MyTmoData myTmoData) {
		loginToProfilePageAndClickOnLink(myTmoData, "");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ProfilePageAndClickOnLink");
		accessibilityCommonLib.testAccessibility(getDriver(), "ProfilePageAndClickOnLink");
	}
}
