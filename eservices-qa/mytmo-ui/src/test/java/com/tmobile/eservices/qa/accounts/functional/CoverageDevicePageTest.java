package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.CoverageDevicePage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;

public class CoverageDevicePageTest extends GlobalCommonLib {

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyAssignedMobileNumber(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with coveragedevice blade");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on coverage device link| Coverage device should be displayed");
		Reporter.log("6. Click on assigned mobile number| Linked phone number matched with logged missdn");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CoverageDevicePage coverageDevicePage = navigateToCoverageDevicePage(myTmoData, "Coverage Device");
		coverageDevicePage.clickAssignedMobileNumberLink();
		coverageDevicePage.verifyMissdnNumber(myTmoData.getLoginEmailOrPhone());
	}

	/**
	 * US326603:Coverage Device saveChanges button state message
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void testCoverageDeviceAddressSaveChangesBtnState(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with coveragedevice blade");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on coverage device link| Coverage device should be displayed");
		Reporter.log("6. Click on device address link | Device address link should be clicked");
		Reporter.log("7: Verify Device Address bread crumb active status| Device Address bread crumb should be active");
		Reporter.log(
				"8: Click on Coverage Device bread crumb and verify Coverage Device bread crumb active status | Coverage Device crumb should be active");
		Reporter.log(
				"9: Click on profile bread crumb and verify profile bread crumb active status | Profile bread crumb should be active");
		Reporter.log("10:Verify profile page | Profile page should be displayed");
		Reporter.log("11.Click on coverage device link| Coverage device should be displayed");
		Reporter.log("12.Click on device address link | Device address link should be clicked");
		Reporter.log(
				"13.Re enter the same adress again on device address | Same address with out any changes should be entered");
		Reporter.log("14.Verify save changes button state | Save changes button should be in disable state");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CoverageDevicePage coverageDevicePage = navigateToCoverageDevicePage(myTmoData, "Coverage Device");
		coverageDevicePage.clickDeviceAddressLink();
		coverageDevicePage.verifyBreadCrumbActiveStatus("Device Address");
		coverageDevicePage.clickBreadCrumb("Coverage Device");
		coverageDevicePage.verifyCoverageDevicePage();
		coverageDevicePage.verifyBreadCrumbActiveStatus("Coverage Device");
		coverageDevicePage.clickProfileHomeBreadCrumb();
		coverageDevicePage.verifyBreadCrumbActiveStatus("Profile");

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
		profilePage.clickProfilePageLink("Coverage Device");

		coverageDevicePage.clickDeviceAddressLink();
		coverageDevicePage.updateAddressWithOutChanges();
		coverageDevicePage.verifySaveChangesBtnState();
	}

	/**
	 * US327336/US454528:Profile | Re-Launch Enhancements | Rename Home |
	 * Breadcrumbs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyCoverageDevicesPageBreadCrumbsLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with coveragedevice blade");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on coverage device link| Coverage device should be displayed");
		Reporter.log("6. Click on device address link | Device address link should be clicked");
		Reporter.log("7: Verify Device Address bread crumb active status| Device Address bread crumb should be active");
		Reporter.log(
				"8: Click on Coverage Device bread crumb and verify Coverage Device bread crumb active status | Coverage Device crumb should be active");
		Reporter.log(
				"9: Click on profile bread crumb and verify profile bread crumb active status | Profile bread crumb should be active");
		Reporter.log("10:Verify profile page | Profile page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CoverageDevicePage coverageDevicePage = navigateToCoverageDevicePage(myTmoData, "Coverage Device");
		coverageDevicePage.clickDeviceAddressLink();

		coverageDevicePage.verifyBreadCrumbActiveStatus("Device Address");
		coverageDevicePage.clickBreadCrumb("Coverage Device");
		coverageDevicePage.verifyCoverageDevicePage();
		coverageDevicePage.verifyBreadCrumbActiveStatus("Coverage Device");
		coverageDevicePage.clickProfileHomeBreadCrumb();
		coverageDevicePage.verifyBreadCrumbActiveStatus("Profile");

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testDeviceAddressChange(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with coveragedevice blade");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on coverage device link| Coverage device page should be displayed");
		Reporter.log(
				"6. Click on device address link and update address| update address and click on save changes button should be success");
		Reporter.log("7. Verify update operation dialog box |  update operation dialog box should not be displayed");
		Reporter.log("8. Verify coverage device page| Coverage device page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CoverageDevicePage coverageDevicePage = navigateToCoverageDevicePage(myTmoData, "Coverage Device");
		coverageDevicePage.clickDeviceAddressLink();
		coverageDevicePage.updateDeviceAddress();
		coverageDevicePage.verifyUpdateOperationFailedDialogBox();
		coverageDevicePage.verifyCoverageDevicePage();
	}
}
