/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import java.util.HashMap;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.OTPAmountPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class OTPAmountPageTest extends PaymentCommonLib {

	/**
	 * US283064 GLUE Light Reskin - OTP Edit Amount Page
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular user. With due only
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testGLREditAmountPageOnlyDue(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US283064 GLUE Light Reskin - OTP Edit Amount Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade | Payment amount page should be displayed");
		Reporter.log("Step 5: verify Total radio button | Total radio button should be displayed");
		Reporter.log("Step 6: verify Other radio button | Other radio button should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);
		editAmountPage.verifyAmountRadioButton("Other");
		editAmountPage.verifyAmountRadioButton("Total Balance");
		editAmountPage.verifyUpdateAmountCTA();
		editAmountPage.verifyCancelCTA();
		editAmountPage.clickCancelBtn();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
	}

	/**
	 * US283064 GLUE Light Reskin - OTP Edit Amount Page
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular user. With due and past due
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testGLREditAmountPageWithDueAndPastDue(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US283064 GLUE Light Reskin - OTP Edit Amount Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one t	ime payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade | Payment amount page should be displayed");
		Reporter.log("Step 5: verify Total radio button | Total radio button should be displayed");
		Reporter.log("Step 6: verify Past Due radio button | Past Due radio button should be displayed");
		Reporter.log("Step 7: verify Other radio button | Other radio button should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);
		editAmountPage.verifyAmountRadioButton("Other");
		editAmountPage.verifyAmountRadioButton("Total Balance");
		editAmountPage.verifyAmountRadioButton("Past due");
		editAmountPage.verifyUpdateAmountCTA();
		editAmountPage.verifyCancelCTA();
		editAmountPage.clickCancelBtn();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
	}

	/**
	 * US283064 GLUE Light Reskin - OTP Edit Amount Page
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Sedona user. With past due
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.SEDONA })
	public void testGLREditAmountPageSedonaPastDue(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US283064 GLUE Light Reskin - OTP Edit Amount Page");
		Reporter.log("Data Conditions - Sedona user. With past due");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one t	ime payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade | Payment amount page should be displayed");
		Reporter.log("Step 5: verify Total radio button | Total radio button should be displayed");
		Reporter.log("Step 6: verify Other radio button | Other radio button should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);
		editAmountPage.verifyAmountRadioButton("Other");
		editAmountPage.verifyAmountRadioButton("Past due");
		editAmountPage.verifyViewDetailsLink();
		editAmountPage.verifyUpdateAmountCTA();
		editAmountPage.verifyCancelCTA();
		editAmountPage.clickCancelBtn();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
	}

	/**
	 * US283064 GLUE Light Reskin - OTP Edit Amount Page
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Sedona user. With current due and past due
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY, Group.SEDONA })
	public void testGLREditAmountPageSedonaDueAndPastDue(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US283064 GLUE Light Reskin - OTP Edit Amount Page");
		Reporter.log("Data Conditions - Sedona user. With current due and past due");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one t	ime payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade | Payment amount page should be displayed");
		Reporter.log("Step 5: verify Total radio button | Total radio button should be displayed");
		Reporter.log("Step 6: verify Other radio button | Other radio button should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);
		editAmountPage.verifyAmountRadioButton("Other");
		editAmountPage.verifyAmountRadioButton("Due today");
		editAmountPage.verifyAmountRadioButton("Pay in full");
		editAmountPage.verifyViewDetailsLink();
		editAmountPage.verifyUpdateAmountCTA();
		editAmountPage.verifyCancelCTA();
		editAmountPage.clickCancelBtn();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
	}

	/**
	 * US262822 Angular + GLUE - OTP - Landing Page - Payment Amount Blade
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEOTPLandingPagePaymentAmountBlade(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262822	Angular + GLUE - OTP - Landing Page - Payment Amount Blade");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade | Payment amount page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToEditAmountPage(myTmoData);
	}

	/**
	 * US262837 Angular + GLUE - OTP - Landing Page - Payment Amount Cache
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEOTPLandingPagePaymentAmountBladeCache(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262837	Angular + GLUE - OTP - Landing Page - Payment Amount Cache");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade | Payment amount page should be displayed");
		Reporter.log(
				"Step 5: Enter amount in the amount blade and refresh| After refresh amount cache should be cleared");
		Reporter.log("Step 6: Set other amount and click cancel button| OTP landing page should be displayed");
		Reporter.log("Step 7: Click on amount icon and verify other amount| Other amount should be cleared");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		getDriver().navigate().refresh();
		editAmountPage.verifyOtherAmountCacheCleared();
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		editAmountPage.clickCancelBtn();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyOtherAmountCacheCleared();
	}

	/**
	 * US262869 Angular + GLUE - OTP - Payment Amount Spoke Page - Alerts
	 * (Non-Sedona & Suspended)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testAngularGLUEOTPPaymentAmountSpokePageNonSedonaSuspended(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262869	Angular + GLUE - OTP - Payment Amount Spoke Page - Alerts (Non-Sedona & Suspended)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade | Payment amount page should be displayed");
		Reporter.log("Step 5: click view details link & verify breakdown modal | Breakdown modal should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		String pastDue = homePage.getpastDueAmount();
		System.out.println("pastDue " + pastDue);
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double payAmount = generateRandomAmount();

		editAmountPage.setOtherAmount(payAmount.toString());
		editAmountPage.verifyPaymentAmountSedonaAlertMsg(PaymentConstants.AUTOPAY_ALERT_SEDONA_LESS);

		String pastDueAmount = pastDue.substring(pastDue.indexOf('$'), pastDue.indexOf(' '));
		Double abovePastDue = Double.parseDouble(pastDueAmount.replaceAll("\\$", "")) + payAmount;
		editAmountPage.setOtherAmount(abovePastDue.toString());
		editAmountPage.verifyPaymentAmountSedonaAlertMsg(PaymentConstants.AUTOPAY_ALERT_SEDONA_FULL);
		editAmountPage.clickAmountRadioButton("Due today");
		editAmountPage.verifyPaymentAmountSedonaAlertMsg(PaymentConstants.AUTOPAY_ALERT_SEDONA_FULL);
		editAmountPage.clickAmountRadioButton("Pay in full");
		editAmountPage.verifyPaymentAmountSedonaAlertMsg(PaymentConstants.AUTOPAY_ALERT_SEDONA_FULL);
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(pastDue);
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPaymentAmountSedonaAlertMsg(PaymentConstants.AUTOPAY_ALERT_SEDONA_FULL);
	}

	/**
	 * US284807 GLUE Light Reskin - OTP Breakdown Modal
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Sedona user. With due
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testGLROTPBreakdownModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262871	Angular + GLUE - OTP - Breakdown Modal (Sedona & Past due/Due today)");
		Reporter.log("Data Conditions - Sedona user. With due");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade | Payment amount page should be displayed");
		Reporter.log("Step 5: click view details link & verify breakdown modal | Breakdown modal should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);
		editAmountPage.clickamountIcon();
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyViewDetailsLink();
		editAmountPage.clickViewDetails();
		editAmountPage.verifyBreakdownModal();
		editAmountPage.verifyPageLoaded();
	}

	/**
	 * 
	 * US320404 UI OTP Edit amount page modification for upgrade flow. US321566 UI
	 * OTP Edit amount page notification for upgrade flow.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SEDONA })
	public void testJumpOtpAmountPageModificationsAndNotifications(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US320404 UI OTP Edit amount page modification for upgrade flow");
		Reporter.log("US321566 UI OTP Edit amount page notification for upgrade flow");
		Reporter.log("Data Conditions| IR PAH With past due balance and not suspended");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3. Click on Shop link | Shop page should be displayed");
		Reporter.log("Step 4. Click on See all phones | Product list page should be displayed");
		Reporter.log("Step 5. Click on add to cart button | Line selector page should be displayed");
		Reporter.log("Step 6. Verify payment pop up modal window | Payment pop up modal should not be present");
		Reporter.log("Step 7. Click on X CTA | Modal should not be visible. Line selector page should be greyed out");
		Reporter.log("Step 8.Click on the banner | Billing and payments one time payment page should be displayed");
		Reporter.log(
				"Step 9: Verify the message below total Balance radio button|Become eligible for upgrade and pay the total balance message should be displayed");
		Reporter.log(
				"Step 10: Verify the message below past due balance radio button|Become eligible for upgrade and Pay past due balance message should be displayed");
		Reporter.log("Step 11: Verify the default value in Other field|Default value should be past due Amount");
		Reporter.log(
				"Step 12: Select the Other Radio button and Verify the notification|Upon submission, your account will become eligible to upgrade or add a line should be displayed above CTA");
		Reporter.log(
				"Step 13: Verify the message below Other radio button|$XX.XX required to upgrade message should be displayed in grey");
		Reporter.log(
				"Step 14: Enter a custom Amount lesser than past due amount and Verify the message|$XX.XX required to upgrade message should be displayed in red");
		Reporter.log(
				"Step 15: Enter the custom Amount lesser than the past due Amount and verify the notification |This payment will not make you eligible to upgrade should be dispalyed above CTA");
		Reporter.log(
				"Step 16: Enter a custom Amount greater than past due amount and Verify the message|$XX.XX required to upgrade message should be displayed in grey");
		Reporter.log(
				"Step 17: Enter the custom Amount greater than the past due Amount and verify the notification|Upon submission, your account will become eligible to upgrade or add a line should be displayed above CTA");
		Reporter.log(
				"Step 18: Enter the custom Amount equal to the past due Amount and verify the notification |Upon submission, your account will become eligible to upgrade or add a line should be displayed above CTA");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickOnFeaturedDevices("Apple iPhone 8 Plus");
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		/*
		 * lineSelectorPage.verifyLineSelectorPage();
		 * lineSelectorPage.verifyModalPastDueCustomer();
		 * lineSelectorPage.clickCloseCTAPastDueModal();
		 */
		lineSelectorPage.clickStickBannerNextIcon();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage oTPAmountPage = new OTPAmountPage(getDriver());
		oTPAmountPage.verifyTextUnderTotalBal("Pay total balance, become eligible to upgrade/add a line.");
		oTPAmountPage.verifyTextUnderPastDue("Pay past-due amount, become eligible to upgrade/add a line.");
		oTPAmountPage.clickAmountRadioButton("Other");
		oTPAmountPage.verifyWarningText("Once you make this payment, you will be eligible to upgrade.");
		oTPAmountPage.verifyTextUnderOther("required to upgrade/add a line.");
		String amount = oTPAmountPage.getOtherAmount();
		Double changedAmount = Double.parseDouble(amount) - 1;
		oTPAmountPage.setOtherAmount(changedAmount.toString());
		oTPAmountPage.verifyWarningTextsWhenLessAmountEntered();
		changedAmount = changedAmount + 2;
		oTPAmountPage.setOtherAmount(changedAmount.toString());
		oTPAmountPage.verifyWarningText("Once you make this payment, you will be eligible to upgrade.");
		oTPAmountPage.setOtherAmount(amount);
		oTPAmountPage.verifyWarningText("Once you make this payment, you will be eligible to upgrade.");
	}

	//
	/**
	 * US304471 PA Messaging Enhancements - Unsecured PA - OTP Edit Amount Page
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUnsecuredPAMessageonOTPEditAmountPageInsideBlackoutPeriod(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test US304471:   PA Messaging Enhancements - Unsecured PA - OTP Edit Amount Page");
		Reporter.log("Data Condition | PA unsecured inside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade |  OTP edit amount page should be displayed");
		Reporter.log(
				"Step 5: verify Other radio button selected by default| Other radio button should be selected by default");
		Reporter.log(
				"Step 6: verify current installment due pre-populated  in the Other Amount field| current installment due should be pre-populated in the Other Amount field");
		Reporter.log("Step 7: change the payment amount < installment due");
		Reporter.log(
				"Step 8: verify 'This payment alone will not satisfy the total [$XX.XX] due today for your Payment Arrangement to avoid suspension. Increase amount or review previous payments' message|message should be displayed");
		Reporter.log("Step 9: verify [$XX.XX] is current installemnet due|current installemnet due should be verified");
		Reporter.log("Step 10: click on 'review previous payments' link|account activity page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		// OTPAmountPage oneTimePaymentPage = navigateToEditAmountPage(myTmoData);

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyAmountRadioButton("Other");
		editAmountPage.verifyotherAmountIsPopulated(paDetails.get("Amount"));
		double dAmount = Double.parseDouble((paDetails.get("Amount"))) - 5;
		editAmountPage.setOtherAmount(String.valueOf(dAmount));
		editAmountPage.clickUpdateAmount();
		String paText = PaymentConstants.PA_OTP_EDIT_AMOUNT_PAGE_UPDATE_LESSAMOUNT.replace("[$XX.XX]",
				paDetails.get("Amount"));
		oneTimePaymentPage.verifySecuredPAMessageNotification(paText);
		oneTimePaymentPage.verifyReviewPaymentsLink();
		oneTimePaymentPage.clickReviewPreviouspayments();
		accountHistoryPage.verifyPageLoaded();
	}

	/**
	 * US304471 PA Messaging Enhancements - Unsecured PA - OTP Edit Amount Page
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUnsecuredPAMessageonOTPEditAmountPageOutsideBlackoutPeriod(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case:US304471:   PA Messaging Enhancements - Unsecured PA - OTP Edit Amount Page");
		Reporter.log("Data Condition | PA unsecured outside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade |  OTP edit amount page should be displayed");
		Reporter.log(
				"Step 5: verify Other radio button selected by default| Other radio button should be selected by default");
		Reporter.log(
				"Step 6: verify current installment due pre-populated  in the Other Amount field| current installment due should be pre-populated in the Other Amount field");
		Reporter.log("Step 7: change the payment amount < installment due");
		Reporter.log(
				"Step 8: verify message'This payment alone will not satisfy the total [$XX.XX] due on [INSTALLMENT DATE] for your Payment Arrangement to avoid suspension. Increase amount or review previous payments'|message should be displayed");
		Reporter.log("Step 9: verify [$XX.XX] is current installemnet due|current installemnet due should be verified");
		Reporter.log(
				"Step 10: verify [INSTALLMENT DATE] is current installemnet due date|current installemnet due date should be verified");
		Reporter.log("Step 11: click on 'review previous payments' link|account activity page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		// OTPAmountPage oneTimePaymentPage = navigateToEditAmountPage(myTmoData);

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		if (oneTimePaymentPage.verifyReviewPAModalIsDisplayedOrNot()) {
			oneTimePaymentPage.clickContinueWithPaymentCTA();
		}
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyAmountRadioButton("Other");
		editAmountPage.verifyotherAmountIsPopulated(paDetails.get("Amount").replace("$", ""));
		Double dAmount = Double.parseDouble(paDetails.get("Amount").replace("$", "")) - generateRandomAmount();
		editAmountPage.setOtherAmount(dAmount.toString());
		String paText = PaymentConstants.PA_OTP_EDIT_AMOUNT_PAGE_UPDATE_MOREAMOUNT
				.replace("[$XX.XX]", paDetails.get("Amount")).replace("[INSTALLMENT DATE]", paDetails.get("Date"));
		editAmountPage.verifyPAAlert(paText);
		editAmountPage.clickReviewPreviouspayments();
		accountHistoryPage.verifyPageLoaded();

	}

	/**
	 * US346427 MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-OTP
	 * Variant-Edit amount Screen amount titles Modification
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testDigitalPAGreaterThan30DaysEditAmountScreenMessages(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US346427:   PA Messaging Enhancements - Unsecured PA - OTP Edit Amount Page");
		Reporter.log("Data Condition | past due balance aged greater than 30 days");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Click on Payment amount blade |  OTP edit amount page should be displayed");
		Reporter.log(
				"Step 5: Verify message below  pay in full amount | Pay my total balance message should be displayed");
		Reporter.log(
				"Step 6: Verify message below  due today | Become eligible for payment arrangement and pay 30 day past due balance message should be displayed");
		Reporter.log(
				"Step 7: Verify message below  other amount |Amount required for payment arrangement should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentArrangementLink();
		oneTimePaymentPage.verifyPAInterruptModal();
		String pa30DaysPastDueAmount = oneTimePaymentPage.get30DaysPastDueAmount();
		oneTimePaymentPage.clickContinueOnPAInterruptModal();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage amountPage = new OTPAmountPage(getDriver());
		amountPage.verifyPageLoaded();
		amountPage.verifyTextUnderTotalBal(PaymentConstants.TEXT_UNDER_TOTAL_DUE);
		amountPage.verifyTextUnderPastDue(PaymentConstants.TEXT_UNDER_PAST_DUE);
		amountPage.verifyTextUnderOther(pa30DaysPastDueAmount + PaymentConstants.TEXT_UNDER_OTHER_AMOUNT);
	}

	/**
	 * US346428 MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-OTP
	 * Variant-Edit amount Screen (messaging)
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testDigitalPAEditAmountScreenNotifications(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case:US346428: MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-OTP Variant-Edit amount Screen");
		Reporter.log("Data Condition | past due balance aged greater than 30 days");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log(
				"Step 4: Change default amount >= 30 day past due amount and verify message | Your account will be eligible for a payment arrangement notification should be displayed");
		Reporter.log(
				"Step 5: Change default amount < 30 day past due amount and verify message | This payment will not make you eligible for payment arrangement notification should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentArrangementLink();
		oneTimePaymentPage.verifyPAInterruptModal();
		String pastDue30DAmount = oneTimePaymentPage.get30DaysPastDueAmount().replace("$", "");
		oneTimePaymentPage.clickContinueOnPAInterruptModal();
		// oneTimePaymentPage.verifyNewOTPPageLoaded();
		oneTimePaymentPage.verifyPageLoaded();
		String totalDueAmount = oneTimePaymentPage.getTotalDueAmount().replace("$", "");
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage amountPage = new OTPAmountPage(getDriver());
		amountPage.verifyPageLoaded();

		Double updatedAmount = Double.parseDouble(pastDue30DAmount) + 1;
		amountPage.setOtherAmount(updatedAmount.toString());
		amountPage.verifyDigitalPAAlert(PaymentConstants.DIGITAL_PA_AMOUNT_GREATER_THAN_PAST_DUE_EDIT_AMOUNT_PAGE);

		// commented below code ..removed from acceptance criteria
		/*
		 * updatedAmount = Double.parseDouble(totalDueAmount)+1;
		 * amountPage.setOtherAmount(updatedAmount.toString());
		 * amountPage.verifyDigitalPAAlert(PaymentConstants.
		 * DIGITAL_PA_AMOUNT_GREATER_THAN_PAST_DUE_EDIT_AMOUNT_PAGE);
		 */

		updatedAmount = Double.parseDouble(pastDue30DAmount) - 1;
		amountPage.setOtherAmount(updatedAmount.toString());
		amountPage.verifyDigitalPAAlert(PaymentConstants.DIGITAL_PA_AMOUNT_LESS_THAN_PAST_DUE_EDIT_AMOUNT_PAGE);
	}

	/**
	 * US426879 PA Messaging Enhancements-Clean up Edit amount page.
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAMessagingEnhancementsCleanUpEditAmountPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US426879: PA Messaging Enhancements-Clean up Edit amount page.");
		Reporter.log(
				"Data Condition | MSDSIN with Scheduled PA(1 installment is missed and 2nd installment is  upcoming");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Click on Payment amount blade | OTP edit amount page should be displayed");
		Reporter.log(
				"Step 5: Verify 'Other' payment option is selected by default | 'Other' payment option should be selected by default");
		Reporter.log(
				"Step 6: Verify amount that was displayed on the landing page is pre-populated in the Other amount field | Amount that was displayed on the landing page should be pre-populated in the Other amount field");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");
	}

	/**
	 * US578984 verify balance amount is suppressed for
	 * Business_User_NonMaster_NonPAH
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testTotalandPastDueBalanceRadioButtonSupressedforBusinessUserNonMasterNonPAH(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : US578984:OTP Amount Section - Suppress Dollar Amounts for role types");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the One time payment page | One time payment page should be displayed");
		Reporter.log("5. click on Amount Icon | One Amount  page should be displayed");
		Reporter.log(
				"6. verify balance amount is suppressed in total balance ,last payment text| balance amount should be suppressed in total balance ,last payment text");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyTotalandPastDueBalanceRadioButtonSupressedforNonMasterUser(myTmoData);

	}

	/**
	 * US578984 verify balance amount is suppressed for Business_User_NonMaster_PAH
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testTotalandPastDueBalanceRadioButtonSupressedforBusinessUserNonMasterPAH(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : US578984:OTP Amount Section - Suppress Dollar Amounts for role types");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the One time payment page | One time payment page should be displayed");
		Reporter.log("5. click on Amount Icon | One Amount  page should be displayed");
		Reporter.log(
				"6. verify balance amount is suppressed in total balance ,last payment text| balance amount should be suppressed in total balance ,last payment text");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyTotalandPastDueBalanceRadioButtonSupressedforNonMasterUser(myTmoData);

	}

	/**
	 * US578984 verify balance amount is suppressed for
	 * Governament_User_NonMaster_NonPAH
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testTotalandPastDueBalanceRadioButtonSupressedforGovernamentUserNonMasterNonPAH(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : US578984:OTP Amount Section - Suppress Dollar Amounts for role types");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the One time payment page | One time payment page should be displayed");
		Reporter.log("5. click on Amount Icon | One Amount  page should be displayed");
		Reporter.log(
				"6. verify balance amount is suppressed in total balance ,last payment text| balance amount should be suppressed in total balance ,last payment text");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyTotalandPastDueBalanceRadioButtonSupressedforNonMasterUser(myTmoData);

	}

	/**
	 * US578984 verify balance amount is suppressed for
	 * Governament_User_NonMaster_PAH
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testTotalandPastDueBalanceRadioButtonSupressedforGovernamentUserNonMasterPAH(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : US578984:OTP Amount Section - Suppress Dollar Amounts for role types");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the One time payment page | One time payment page should be displayed");
		Reporter.log("5. click on Amount Icon | One Amount  page should be displayed");
		Reporter.log(
				"6. verify balance amount is suppressed in total balance ,last payment text| balance amount should be suppressed in total balance ,last payment text");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyTotalandPastDueBalanceRadioButtonSupressedforNonMasterUser(myTmoData);

	}

}