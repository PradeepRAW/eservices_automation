package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.ChargePlanPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class ChargePlanPageTest extends PaymentCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(ChargePlanPageTest.class);

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testVerfiyChargePlanPageLoad(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
		Reporter.log("Billing - Verify EIP Bill Summary Details");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("6. Click on Plans | Plans page should be loaded");
		Reporter.log("================================");
		ChargePlanPage chargePlanPage = navigateToChargePlanPage(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS ,"billing"})
	public void testVerfyVoiceLinesandAmountsForPlansInBriteBill(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
		Reporter.log("Billing - Verify EIP Bill Summary Details");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("6. Click on Plans | Plans page should be loaded");
		Reporter.log("7. Check the Voice lines and associated details");
		Reporter.log("8. Check the Total amount for Charge Plans");
		Reporter.log("================================");
		ChargePlanPage chargePlanPage = navigateToChargePlanPage(myTmoData);
		chargePlanPage.checkvoicelinesheader();
		chargePlanPage.verifyvoicelines();
		chargePlanPage.checklabelTotal("Total");
		chargePlanPage.checktotalAmount();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testTheTotalAmountForPlansInBriteBill(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
		Reporter.log("Billing - Verify EIP Bill Summary Details");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("6. Click on Plans | Plans page should be loaded");
		Reporter.log("7. Check the Total amount for Charge Plans");
		Reporter.log("================================");
		ChargePlanPage chargePlanPage = navigateToChargePlanPage(myTmoData);
		chargePlanPage.checklabelTotal("Total");
		chargePlanPage.checktotalAmount();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "history"})
	public void testTheIncludedTaxesAndFeesUrl(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
		Reporter.log("Billing - Verify EIP Bill Summary Details");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("6. Click on Plans | Plans page should be loaded");
		Reporter.log("7. Click on teh link Taxes And FeesUrl | User should be able to click on it");
		Reporter.log("================================");
		ChargePlanPage chargePlanPage = navigateToChargePlanPage(myTmoData);
		if(chargePlanPage.checkExcemptionsapplied()) {
		chargePlanPage.clickviewIncludedTaxesAndFeesLink();
		chargePlanPage.verifyIncludedTaxesAndFeesModal();}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testbacktoSummaryLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
		Reporter.log("Billing - Verify EIP Bill Summary Details");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("6. Click on Plans | Plans page should be loaded");
		Reporter.log("7. Click on Back to Summary Link | User should be able to see BillingSummary page");
		Reporter.log("================================");
		ChargePlanPage chargePlanPage = navigateToChargePlanPage(myTmoData);
		chargePlanPage.clickBackToSummaryLink();
		BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
		billAndPaySummaryPage.verifyPageLoaded();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testcheckSliderfunctionality(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
		Reporter.log("Billing - Verify EIP Bill Summary Details");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("6. Click on Plans | Plans page should be loaded");
		Reporter.log("7. Validate the slider functionality | Slider should work as expected");
		Reporter.log("================================");
		ChargePlanPage chargePlanPage = navigateToChargePlanPage(myTmoData);
		chargePlanPage.checktotalAmountwithslideramount();
		chargePlanPage.checkleftslider();
		chargePlanPage.checkrightslider();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testverifyViewDetailslinkfunctionality(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
		Reporter.log("Billing - Verify EIP Bill Summary Details");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("6. Click on Plans | Plans page should be loaded");
		Reporter.log("7. Click on view details link| Application should be navigated to plans page");
		Reporter.log("================================");
		ChargePlanPage chargePlanPage = navigateToChargePlanPage(myTmoData);
		chargePlanPage.clickviewdetailslink();
		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
	}

}
