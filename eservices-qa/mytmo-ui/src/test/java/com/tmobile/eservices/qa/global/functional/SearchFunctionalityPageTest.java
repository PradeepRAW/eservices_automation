/**
 * 
 */
package com.tmobile.eservices.qa.global.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.global.GlobalConstants;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.NewHomePage;

/**
 * @author charshavardhana
 *
 */
public class SearchFunctionalityPageTest extends GlobalCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(SearchFunctionalityPageTest.class);

	/**
	 * TC002_My TMO_Search functionality_Seacrh term page_Show_Number of
	 * contents per page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PENDING})
	public void searchFunctionalityMyTmo(ControlTestData data, MyTmoData myTmoData) {

		logger.info("SearchFunctionality data");
		Reporter.log("My TMO_Search functionality_Seacrh term page_Show_Number of contents per page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on search and enter search data | Search data should be displayed in support tab");	
		Reporter.log("5. Verify pagination in support tab | Pagination should be displayed in support tab");	
		Reporter.log("6. Click on devices tab | Search data should be displayed in devices tab");	
		Reporter.log("7. Verify pagination in devices tab| Pagination should be displayed in devices tab");
		Reporter.log("8. Click on accessories tab | Search data should be displayed in accessories tab");	
		Reporter.log("9. Verify pagination in accessories tab| Pagination should be displayed in accessories tab");	
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage=navigateToNewHomePage(myTmoData);
		homePage.enterSearchData(GlobalConstants.SEARCH_TEXT_DATA_1);

		homePage.getSupportTab();
		homePage.getYourSelectionTextSupport(GlobalConstants.SEARCH_TEXT_DATA_1);
		Reporter.log("Search data "+GlobalConstants.SEARCH_TEXT_DATA_1+" is displayed in support tab");
		logStep(StepStatus.PASS, "Verify '"+GlobalConstants.SEARCH_TEXT_DATA_1+"' ", "Verified '"+GlobalConstants.SEARCH_TEXT_DATA_1+"' ");
		homePage.getSupportPagination();
		Reporter.log("Pagination is displayed in support tab");

		homePage.getDevicesTab();
		homePage.clickDevicesTab();

		homePage.getYourSelectionTextDevices();
		Reporter.log("Search data "+GlobalConstants.SEARCH_TEXT_DATA_1+" is displayed in devices tab");
		Assert.assertTrue(homePage.getDevicePagination(), "Pagination not displayed-devices");
		Reporter.log("Pagination is displayed in devices tab");

		homePage.getAccessoriesTab();
		homePage.clickAccessoriesTab();
		homePage.getYourSelectionTextAccessories();
		Reporter.log("Search data "+GlobalConstants.SEARCH_TEXT_DATA_1+"is displayed in accessories tab");
		homePage.getAccessoriesPagination();
		Reporter.log("Pagination is displayed in accessories tab");
		
	}

	/**
	 * US478004:Search | Replace Search Field with Search Text | Implement and testing
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testSearchFunctionality(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US478004: Search | Replace Search Field with Search Text | Implement and testing");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on search and enter search data | Data should be entered successfully");	
		Reporter.log("5. Verify search result head line | Search result head line should be displayed");	
		Reporter.log("6. Verify search result count | Search result count should be displayed");	
		Reporter.log("7. Verify text in search result | Search keyword should match with the keyword in search result");
		Reporter.log("8. Verify go cta | Go cta should not be displayed");	
		Reporter.log("9. Verify search text in search field in unav | Search text should be displayed in search field in unav");	
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		HomePage homePage =navigateToHomePage(myTmoData);
		homePage.enterSearchData(GlobalConstants.SEARCH_TEXT_DATA);
		homePage.verifySearchResultsHeadLine();
		homePage.verifySeachResultCount();
		homePage.verifyTextInSearchResult();
		homePage.verifyGoCTA();
		homePage.verifyNavigationSearchPlaceHolderText();
	}
}
