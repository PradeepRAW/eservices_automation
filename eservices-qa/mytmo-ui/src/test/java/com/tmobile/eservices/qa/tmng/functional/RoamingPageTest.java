/**
 * 
 */
package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.RoamingPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

/**
 * @author ksrivani
 *
 */
public class RoamingPageTest extends TmngCommonLib {

	/**
	 * US485660: Roaming tools| Destination | Check Rates & Coverage
	 * US485992: Roaming tools| Destination | Display Rates & Coverage
	 * TC280730
	 * TC286048
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testDestinationRoamingForTMOONEAndSC(TMNGData tMNGData) {

		Reporter.log("US485660: Roaming tools| Destination | Check Rates & Coverage");
		Reporter.log("US485992: Roaming tools| Destination | Display Rates & Coverage");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("8. Verify destination type ahead list | Selected the value from the type ahead list");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Verify destination name in header. | Destination name in header should be displayed");
		Reporter.log("11. Verify data, text and talk columns | Data, text  and talk columns should be displayed");
		Reporter.log("12. Verify 'T mobile ONE & simple choice' plans header | 'T mobile ONE & simple choices' header should be displayed");
		Reporter.log("13. Verify information for T-Mobile One and Simple choice plans | Data is displayed successfully for TMO One and Simple choice");
		Reporter.log("14. Verify data in Data column | 'Unlimited' is shown in Data column for TMO One and Simple choice plan, as there is no data");
		Reporter.log("15. Verify data in Text column | 'Unlimited' is shown in Text column for TMO One and Simple choice plan, as there is no data");
		Reporter.log("16. Verify data in Talk column | Data is displayed successfully in Talk column for TMO One and Simple choice");
		/*Reporter.log("17. Verify and click on 'See more' CTA | Clicked on 'See More' CTA");
		Reporter.log("18. Verify MYTMO login page is displayed | MYTMO login page should be displayed");*/
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyTypeAheadListOfDestinationDetails();
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyDestinationNameInHeader(tMNGData.getDestinationName());
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyTMobileONEandSimpleChoicePlans();
		/*roamingPage.verifyAndClickSeeMoreCTA();
		roamingPage.verifyMyTMOLoginPage();*/
		
		}
	
	/**
	 * US485660: Roaming tools| Destination | Check Rates & Coverage
	 * US485992: Roaming tools| Destination | Display Rates & Coverage
	 * TC280939
	 * TC286050
	 * * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testDestinationRoamingForOtherTMO(TMNGData tMNGData) {

		Reporter.log("US485660: Roaming tools| Destination | Check Rates & Coverage");
		Reporter.log("US485992: Roaming tools| Destination | Display Rates & Coverage");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("8. Verify destination type ahead list | Selected the value from the type ahead list");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Verify destination name in header. | Destination name in header should be displayed");
		Reporter.log("11. Verify data, text and talk columns | Data, text  and talk columns should be displayed");
		Reporter.log("12. Verify 'Other T-Mobile Plans' Header | 'Other T-Mobile Plans' Header is displayed");
		Reporter.log("13. Validate information for Other T-Mobile plans | Data is displayed successfully for Other TMO plans");
		Reporter.log("14. Verify data in Data column | Data is displayed successfully in Data column for Other TMO plans");
		Reporter.log("15. Verify data in Text column | Data is displayed successfully in Text column for Other TMO plans");
		Reporter.log("16. Verify data in Talk column | Data is displayed successfully in Talk column for Other TMO plans");
		Reporter.log("17. Verify and click on 'Log in to My T-Mobile' link | Clicked on Login to My T-Mobile link successfully");
		Reporter.log("18. Verify MYTMO login page is displayed | MYTMO login page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyTypeAheadListOfDestinationDetails();
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyDestinationNameInHeader(tMNGData.getDestinationName());
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyOtherTMobilePlans();
		roamingPage.verifyAndClickOnLoginToTMobileLink();
		roamingPage.verifyMyTMOLoginPage();
		}
	
	/**
	 * US485660: Roaming tools| Destination | Check Rates & Coverage
	 * US485992: Roaming tools| Destination | Display Rates & Coverage
	 * TC286444
	 * TC286446
	 * * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testDestinationRoamingForAllTMO(TMNGData tMNGData) {

		Reporter.log("US485660: Roaming tools| Destination | Check Rates & Coverage");
		Reporter.log("US485992: Roaming tools| Destination | Display Rates & Coverage");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("8. Verify destination type ahead list | Selected the value from the type ahead list");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Verify destination name in header. | Destination name in header should be displayed");
		Reporter.log("11. Verify data, text and talk columns | Data, text  and talk columns should be displayed");
		Reporter.log("12. Verify 'All T-Mobile Plans' Header | 'All T-Mobile Plans' Header is displayed");
		Reporter.log("13. Validate information for All T-Mobile plans | Data is displayed successfully for All T-Mobile Plans");
		Reporter.log("14. Verify data in Data column | 'No Data' is shown in Data column for All T-Mobile plans, as we received Data column with Empty array values");
		Reporter.log("15. Verify data in Text column | Data is displayed successfully in Text column for All T-Mobile plans");
		Reporter.log("16. Verify data in Talk column | Data is displayed successfully in Talk column for All T-Mobile plans");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyTypeAheadListOfDestinationDetails();
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyDestinationNameInHeader(tMNGData.getDestinationName());
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyAllTMobilePlans();
	}
	
	/**
	 * US485660: Roaming tools| Destination | Check Rates & Coverage
	 * US485992: Roaming tools| Destination | Display Rates & Coverage
	 * TC280933
	 * TC286049
	 * * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testDestinationRoamingWithInvalidData(TMNGData tMNGData) {

		Reporter.log("US485660: Roaming tools| Destination | Check Rates & Coverage");
		Reporter.log("US485992: Roaming tools| Destination | Display Rates & Coverage");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter invalid destination details | Entered destination information in the text box.");
		Reporter.log("8. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("9. Verify the Result Message | Message 'Sorry, we don’t have coverage there yet.' is displayed, as the entered destination is invalid");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyMsgForInvalidDestination();
		}
	
	/**
	 * US485988: Roaming tools| Cruise | Check Rates & Coverage
	 * US485990: Roaming tools| Cruise | Display Rates & Coverage
	 * TC280944
	 * TC286052
	 * * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testCruiseRoamingForAllTMO(TMNGData tMNGData) {

		Reporter.log("US485988: Roaming tools| Cruise | Check Rates & Coverage");
		Reporter.log("US485990: Roaming tools| Cruise | Display Rates & Coverage");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify cruise text box and enter cruise details | Entered cruise information in the text box.");
		Reporter.log("8. Click Check rates and coverage CTA | Clicked 'Check Rates & Coverage' CTA successfully.");
		Reporter.log("9. Verify cruise name in header. | Cruise name in header should be displayed");
		Reporter.log("10. Verify data, text and talk columns | Data, text  and talk columns should be displayed");
		Reporter.log("11. Verify 'All T-Mobile Plans' Header | 'All T-Mobile Plans' Header should be displayed");
		Reporter.log("12. Validate information for All T-Mobile plans | Data is displayed successfully for All T-Mobile Plans");
		Reporter.log("14. Verify data in Data column | 'No Data' is shown in Data column for All T-Mobile plans, as we received Data column with Empty array values");
		Reporter.log("15. Verify data in Text column | Data is displayed successfully in Text column for All T-Mobile plans");
		Reporter.log("16. Verify data in Talk column | Data is displayed successfully in Talk column for All T-Mobile plans");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterCruiseOrFerryDetails(tMNGData.getCruiseName());
		roamingPage.verifyTypeAheadListOfCruiseOrFerryDetails();
		roamingPage.verifyAndClickCheckRatesAndCoveragesButtonForCruise();
		roamingPage.verifyCruiseNameInHeader(tMNGData.getCruiseName());
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyAllTMobilePlans();
	}
	
	/**
	 * US485988: Roaming tools| Cruise | Check Rates & Coverage
	 * US485990: Roaming tools| Cruise | Display Rates & Coverage
	 * TC280947
	 * TC286053
	 * * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testCruiseRoamingWithInvalidData(TMNGData tMNGData) {

		Reporter.log("US485988: Roaming tools| Cruise | Check Rates & Coverage");
		Reporter.log("US485990: Roaming tools| Cruise | Display Rates & Coverage");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify cruise text box and enter invalid cruise details | Entered cruise information in the text box.");
		Reporter.log("8. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("9. Verify the Result Message | Message 'Sorry, we don’t have coverage there yet.' is displayed, as the entered cruise is invalid");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterCruiseOrFerryDetails(tMNGData.getCruiseName());
		roamingPage.verifyAndClickCheckRatesAndCoveragesButtonForCruise();
		roamingPage.verifyMsgForInvalidCruise();
	}
	
	
	/**
	 * US486813 - Roaming Tools| Cruise | Rates & Coverage | Device Compatibility Results
	 * US486810 - Roaming Tools| Cruise | Rates & Coverage | Device Compatibility
	 * 	TC286979
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testDeviceCompatibilityforCruise(TMNGData tMNGData) {
		
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify cruise text box and enter cruise details | Entered cruise information in the text box.");
		Reporter.log("8. Click Check rates and coverage CTA | Clicked 'Check Rates & Coverage' CTA successfully.");
		Reporter.log("9. Verify cruise name in header. | Cruise name in header should be displayed");
		Reporter.log("10. Verify enter right device name in text box | Entered right device in the cruise text box"); 
		Reporter.log("11. Click on Check Compatibility CTA| Clicked on Check Compatibility  CTA successfully"); 
		Reporter.log("12. Verify header text “Your device has limited coverage.” | Header text for device compatibility successfully verified"); 
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterCruiseOrFerryDetails(tMNGData.getCruiseName());
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyCruiseNameInHeader(tMNGData.getCruiseName());
		roamingPage.verifyAndEnterDeviceName(tMNGData.getDeviceName());
		roamingPage.verifyAndClickCruiseCheckCompatabilityButton();
		roamingPage.verifyHeaderTextDeviceCompatabilityForCruise();

		}
	
		/**
		 * US486813 - Roaming Tools| Cruise | Rates & Coverage | Device Compatibility Results
		 * US486810 - Roaming Tools| Cruise | Rates & Coverage | Device Compatibility
		 * TC286980
		 * @param tMNGData
		 */
		@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
		public void testWrongDeviceCompatibilityforCruise(TMNGData tMNGData) {
			
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify cruise text box and enter cruise details | Entered cruise information in the text box.");
		Reporter.log("8. Click Check rates and coverage CTA | Clicked 'Check Rates & Coverage' CTA successfully.");
		Reporter.log("9. Verify cruise name in header. | Cruise name in header should be displayed");
		Reporter.log("10. Verify enter wrong device name in text box | Entered wrong device in the cruise text box"); 
		Reporter.log("11. Click on Check Compatibility CTA| Clicked on Check Compatibility  CTA successfully");  
		Reporter.log("12. Verify header text “Sorry, your device is not compatible.” | Header text for device compatibility successfully verified");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterCruiseOrFerryDetails(tMNGData.getCruiseName());
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyCruiseNameInHeader(tMNGData.getCruiseName());
		roamingPage.verifyAndEnterDeviceName(tMNGData.getDeviceName());
		roamingPage.verifyAndClickCruiseCheckCompatabilityButton();
		roamingPage.verifyHeaderTextDeviceUnCompatabilityMessage();
		} 

		/**
		 * US486802 - Roaming Tools| Destination | Rates & Coverage | Device Compatibility
		 * US486809 - Roaming Tools| Destination | Rates & Coverage | Device Compatibility Results	
		 * TC286971
		 * @param tMNGData
		 */
		@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
		public void testDeviceCompatibilityforDestination(TMNGData tMNGData) {
		
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("8. Verify destination type ahead list | Selected the value from the type ahead list");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Verify destination name in header. | Destination name in header should be displayed");
		Reporter.log("11. Verify enter right device name in text box | Entered right device in the destination text box"); 
		Reporter.log("12. Click on Check Compatibility CTA| Clicked on Check Compatibility  CTA successfully"); 
		Reporter.log("13. Verify header text “Awesome!Your Device is compatible”| Header text for device compatibility successfully verified");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyDestinationNameInHeader(tMNGData.getDestinationName());
		roamingPage.verifyAndEnterDeviceName(tMNGData.getDeviceName());
		roamingPage.verifyAndClickDestinationCheckCompatabilityButton();
		roamingPage.verifyHeaderTextDeviceCompatabilityForDestination();
		}
		
		/**
		 * US486802 - Roaming Tools| Destination | Rates & Coverage | Device Compatibility
		 * US486809 - Roaming Tools| Destination | Rates & Coverage | Device Compatibility Results	
		 * TC286962	
		 * @param tMNGData
		 */
		@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
		public void  testWrongDeviceCompatibilityforDestination (TMNGData tMNGData) {

		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("8. Verify destination type ahead list | Selected the value from the type ahead list");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Verify destination name in header. | Destination name in header should be displayed");
		Reporter.log("11. Verify enter wrong device name in text box | Entered wrong device in the cruise text box"); 
		Reporter.log("12. Click on Check Compatibility CTA| Clicked on Check Compatibility CTA successfully"); 
		Reporter.log("13. Verify header text “Sorry, your device is not compatible.” | Header text for wrong device compatibility successfully verified"); 
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyDestinationNameInHeader(tMNGData.getDestinationName());
		roamingPage.verifyAndEnterDeviceName(tMNGData.getDeviceName());
		roamingPage.verifyAndClickDestinationCheckCompatabilityButton();
		roamingPage.verifyHeaderTextDeviceUnCompatabilityMessage();
		}
		
	/**
	 * US552324: Roaming tools| Destination | Rate change when allowance -1
	 * TC289475 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testDestinationRoamingForTMOONEAndSCForAllowanceMinusOne(TMNGData tMNGData) {

		Reporter.log("US552324: Roaming tools| Destination | Rate change when allowance -1");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("8. Verify destination type ahead list | Selected the value from the type ahead list");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Verify information for T-Mobile One and Simple choice plans | Data is displayed successfully for TMO One and Simple choice");
		Reporter.log("11. Verify data in Data column | 'Unlimited' is shown in Data column for TMO One and Simple choice plan, as there is no data");
		Reporter.log("12. Verify data in Text column | 'Unlimited' is shown in Text column for TMO One and Simple choice plan, as there is no data");
		Reporter.log("13. Verify data in Talk column | 'Unlimited' is shown in Talk column for TMO One and Simple choice plan, as there is no data");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage = new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyTypeAheadListOfDestinationDetails();
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyDataDisplayedForTMOOneAndSimpleChoice();
	}

	/**
	 * US552324: Roaming tools| Destination | Rate change when allowance -1
	 * TC289476 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testDestinationRoamingForOtherTMOForAllowanceMinusOne(TMNGData tMNGData) {

		Reporter.log("US552324: Roaming tools| Destination | Rate change when allowance -1");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("8. Verify destination type ahead list | Selected the value from the type ahead list");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Validate information for Other T-Mobile plans | Data is displayed successfully for Other TMO plans");
		Reporter.log("11. Verify data in Data column | 'Unlimited' is shown in Data column for Other TMO plans");
		Reporter.log("12. Verify data in Text column | 'Unlimited' is shown in Text column for Other TMO plans");
		Reporter.log("13. Verify data in Talk column | 'Unlimited' is shown in Talk column for Other TMO plans");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage = new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyTypeAheadListOfDestinationDetails();
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyDataDisplayedForOtherTMOPlans();
	}

	/**
	 * US552324: Roaming tools| Destination | Rate change when allowance -1
	 * TC286477 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testDestinationRoamingForAllTMOForAllowanceMinusOne(TMNGData tMNGData) {

		Reporter.log("US552324: Roaming tools| Destination | Rate change when allowance -1");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("8. Verify destination type ahead list | Selected the value from the type ahead list");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Validate information for All T-Mobile plans | Data is displayed successfully for All T-Mobile Plans");
		Reporter.log("11. Verify data in Data column | 'Unlimited' is shown in Data column for All T-Mobile plans");
		Reporter.log("12. Verify data in Text column | 'Unlimited' is shown in Text column for All T-Mobile plans");
		Reporter.log("13. Verify data in Talk column | 'Unlimited' is shown in Talk column for All T-Mobile plans");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage = new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyTypeAheadListOfDestinationDetails();
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyDataDisplayedForAllTMobilePlans();
	}

	/**
	 * US552324: Roaming tools| Destination | Rate change when allowance -1
	 * TC289478 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCruiseRoamingForAllTMOForAllowanceMinusOne(TMNGData tMNGData) {

		Reporter.log("US552324: Roaming tools| Destination | Rate change when allowance -1");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify cruise text box and enter cruise details | Entered cruise information in the text box.");
		Reporter.log("8. Click Check rates and coverage CTA | Clicked 'Check Rates & Coverage' CTA successfully.");
		Reporter.log("9. Validate information for All T-Mobile plans | Data is displayed successfully for All T-Mobile Plans");
		Reporter.log("10. Verify data in Data column | 'Unlimited' is shown in Data column for All T-Mobile plans");
		Reporter.log("11. Verify data in Text column | 'Unlimited' is shown in Text column for All T-Mobile plans");
		Reporter.log("12. Verify data in Talk column | 'Unlimited' is shown in Talk column for All T-Mobile plans");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage = new RoamingPage(getDriver());
		roamingPage.verifyAndEnterCruiseOrFerryDetails(tMNGData.getCruiseName());
		roamingPage.verifyTypeAheadListOfCruiseOrFerryDetails();
		roamingPage.verifyAndClickCheckRatesAndCoveragesButtonForCruise();
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyDataDisplayedForAllTMobilePlans();
	}
	/**
	 * DE234207 - TMO - On entering a destination and clicking enter, second listed item is being selected
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testSearchDestinationValue(TMNGData tMNGData) {

		Reporter.log("DE234207 - TMO - On entering a destination and clicking enter, second listed item is being selected");
		Reporter.log("====================================================================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		//Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		//Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		//Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		//Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("2. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("3. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("4. Verify destination type ahead list | Selected the value from the type ahead list");
		Reporter.log("5. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("6. Validate information for All T-Mobile plans | Data is displayed successfully for All T-Mobile Plans");
		Reporter.log("7. Verify searched destination country in destination text box | searched destination country in destination text box");
		Reporter.log("====================================================================================================");
		Reporter.log("Actual Output:");

		//navigateToRoamingPage(tMNGData);
		loadTmngURL(tMNGData);
		getDriver().get("https://dmo.digital.t-mobile.com/coverage/roaming");
		RoamingPage roamingPage = new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails("panama");
		roamingPage.verifyTypeAheadListOfDestinationDetails();
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyDataDisplayedForAllTMobilePlans();
		roamingPage.verifyPostDestinationDetails("panama");
	}
	
	/**
	 * US552340: Roaming tools| Destination |Alias
	 * TC299652 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testDestinationRoamingWithAliasName(TMNGData tMNGData) {
		Reporter.log("US552340: Roaming tools| Destination |Alias");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter Destination/Country which has Alias | Entered Alias name in the text box.");
		Reporter.log("8. Verify destination type ahead list | User should see name of the country to which Alias belongs");
		Reporter.log("9. Select the country | Selected the value from the type ahead list");
		Reporter.log("10. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("11. Verify destination name in header. | Destination name in header should be displayed");
		Reporter.log("12. Verify 'T mobile ONE & simple choice' plans header | 'T mobile ONE & simple choices' header should be displayed");
		Reporter.log("13. Verify information for T-Mobile One and Simple choice plans | Data is displayed successfully in Data, Text and Talk columns for TMO One and Simple choice");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage = new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyDestinationNameForTheAliasNameEntered();
		roamingPage.verifyTypeAheadListOfDestinationDetails();
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifySelectedDestinationNameInHeader();
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyTMobileONEandSimpleChoicePlans();
	}
	
	/**
	 * US605879: Roaming | Results | Add Dynamic text
	 * TC313399
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testDestinationRoamingForUnlimitedData(TMNGData tMNGData) {

		Reporter.log("US605879: Roaming | Results | Add Dynamic text");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Verify destination name in header. | Destination name in header should be displayed");
		Reporter.log("11. Verify data, text and talk columns | Data, text  and talk columns should be displayed");
		Reporter.log("12. Verify 'Plans with Simple Global' header | 'Plans with Simple Global' header should be displayed");
		Reporter.log("13. Verify information for T-Mobile One and Simple choice plans | Data is displayed successfully for Plans with Simple Global");
		Reporter.log("14. Verify data in Data column | 'Unlimited' is shown in Data column for Plans with Simple Global, as there is no data");
		Reporter.log("15. Verify data in Text column | Data is displayed successfully in Text column for Plans with Simple Global");
		Reporter.log("16. Verify data in Talk column | Data is displayed successfully in Talk column for Plans with Simple Global");
		Reporter.log("17. Verify the header display for Unlimited data | Unlimited data in {{country}} is displayed successfully in header for Plans with Simple Global");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyDestinationNameInHeader(tMNGData.getDestinationName());
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyDataDisplayedForPlansWithSimpleGlobal();
		roamingPage.verifyHeaderForUnlimitedData(tMNGData.getDestinationName());
		}
	
	/**
	 * US605879: Roaming | Results | Add Dynamic text
	 * TC313401
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testDestinationRoamingForUnlimitedText(TMNGData tMNGData) {

		Reporter.log("US605879: Roaming | Results | Add Dynamic text");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Verify destination name in header. | Destination name in header should be displayed");
		Reporter.log("11. Verify data, text and talk columns | Data, text  and talk columns should be displayed");
		Reporter.log("12. Verify 'Plans with Simple Global' header | 'Plans with Simple Global' header should be displayed");
		Reporter.log("13. Verify information for T-Mobile One and Simple choice plans | Data is displayed successfully for Plans with Simple Global");
		Reporter.log("14. Verify data in Data column | Data is displayed successfully in Data column for Plans with Simple Global");
		Reporter.log("15. Verify data in Text column | 'Unlimited' is shown in Text column for Plans with Simple Global, as there is no data");
		Reporter.log("16. Verify data in Talk column | Data is displayed successfully in Talk column for Plans with Simple Global");
		Reporter.log("17. Verify the header display for Unlimited text | Unlimited texting in {{country}} is displayed successfully in header for Plans with Simple Global");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyDestinationNameInHeader(tMNGData.getDestinationName());
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyDataDisplayedForPlansWithSimpleGlobal();
		roamingPage.verifyHeaderForUnlimitedText(tMNGData.getDestinationName());
		}
	
	/**
	 * US605879: Roaming | Results | Add Dynamic text
	 * TC313397
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testDestinationRoamingForUnlimitedDataAndText(TMNGData tMNGData) {

		Reporter.log("US605879: Roaming | Results | Add Dynamic text");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Verify destination name in header. | Destination name in header should be displayed");
		Reporter.log("11. Verify data, text and talk columns | Data, text  and talk columns should be displayed");
		Reporter.log("12. Verify 'Plans with Simple Global' header | 'Plans with Simple Global' header should be displayed");
		Reporter.log("13. Verify information for T-Mobile One and Simple choice plans | Data is displayed successfully for Plans with Simple Global");
		Reporter.log("14. Verify data in Data column | 'Unlimited' is shown in Data column for Plans with Simple Global, as there is no data");
		Reporter.log("15. Verify data in Text column | 'Unlimited' is shown in Text column for Plans with Simple Global, as there is no data");
		Reporter.log("16. Verify data in Talk column | Data is displayed successfully in Talk column for Plans with Simple Global");
		Reporter.log("17. Verify the header display for Unlimited data | Unlimited data and texting in {{country}} is displayed successfully in header for Plans with Simple Global");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyDestinationNameInHeader(tMNGData.getDestinationName());
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyDataDisplayedForPlansWithSimpleGlobal();
		roamingPage.verifyHeaderForUnlimitedDataAndText(tMNGData.getDestinationName());
		}
	
	/**
	 * US605879: Roaming | Results | Add Dynamic text
	 * TC313402
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testDestinationRoamingForUnlimitedEverything(TMNGData tMNGData) {

		Reporter.log("US605879: Roaming | Results | Add Dynamic text");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Verify destination name in header. | Destination name in header should be displayed");
		Reporter.log("11. Verify data, text and talk columns | Data, text  and talk columns should be displayed");
		Reporter.log("12. Verify 'Plans with Simple Global' header | 'Plans with Simple Global' header should be displayed");
		Reporter.log("13. Verify information for T-Mobile One and Simple choice plans | Data is displayed successfully for Plans with Simple Global");
		Reporter.log("14. Verify data in Data column | 'Unlimited' is shown in Data column for Plans with Simple Global, as there is no data");
		Reporter.log("15. Verify data in Text column | 'Unlimited' is shown in Text column for Plans with Simple Global, as there is no data");
		Reporter.log("16. Verify data in Talk column | 'Unlimited' is shown in Talk column for Plans with Simple Global, as there is no data");
		Reporter.log("17. Verify the header display for Unlimited data | Unlimited everything in {{country}} is displayed successfully in header for Plans with Simple Global");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyDestinationNameInHeader(tMNGData.getDestinationName());
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyDataDisplayedForPlansWithSimpleGlobal();
		roamingPage.verifyHeaderForUnlimitedEverything(tMNGData.getDestinationName());
		}
	
	/**
	 * US605879: Roaming | Results | Add Dynamic text
	 * TC315173
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testDestinationRoamingForDataTextTalkValues(TMNGData tMNGData) {

		Reporter.log("US605879: Roaming | Results | Add Dynamic text");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify  travelling abroad link| Travelling abroad link should be displayed");
		Reporter.log("4. Click on travelling abroad link | Travelling abroad page should be displayed");
		Reporter.log("5. Verify and click check a destination cta | Verified and Clicked on Check a destination cta");
		Reporter.log("6. Verify Roaming page is displayed | Roaming page should be displayed");
		Reporter.log("7. Verify destination text box and enter details | Entered destination information in the text box.");
		Reporter.log("9. Click on check rates and coverage cta | Clicked on 'Check Rates & Coverage' CTA successfully");
		Reporter.log("10. Verify destination name in header. | Destination name in header should be displayed");
		Reporter.log("11. Verify data, text and talk columns | Data, text  and talk columns should be displayed");
		Reporter.log("12. Verify 'Plans with Simple Global' header | 'Plans with Simple Global' header should be displayed");
		Reporter.log("13. Verify information for T-Mobile One and Simple choice plans | Data is displayed successfully for Plans with Simple Global");
		Reporter.log("14. Verify data in Data column | 'No Data' is shown in Data column while displaying Rates for country");
		Reporter.log("15. Verify data in Text column | 'No Data' is shown in Data column while displaying Rates for country");
		Reporter.log("16. Verify data in Talk column | Data is displayed successfully in Talk column for Plans with Simple Global");
		Reporter.log("17. Verify the header display for Unlimited data | Rates for {{country}} is displayed successfully in header while displaying the Rates for all the columns");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToRoamingPage(tMNGData);
		RoamingPage roamingPage =new RoamingPage(getDriver());
		roamingPage.verifyAndEnterDestinationDetails(tMNGData.getDestinationName());
		roamingPage.verifyAndClickCheckRatesAndCoveragesButton();
		roamingPage.verifyDestinationNameInHeader(tMNGData.getDestinationName());
		roamingPage.verifyResultantTableRowsandColumns();
		roamingPage.verifyDataDisplayedForPlansWithAllDataTextTalkValues();
		roamingPage.verifyHeaderForRatesForCountry(tMNGData.getDestinationName());
		}
	}