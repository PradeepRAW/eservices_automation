package com.tmobile.eservices.qa.payments.monitors;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.EOSCommonMethods;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EOSAccountsV3Filters;
import com.tmobile.eservices.qa.pages.payments.api.EOSFDPV3;
import com.tmobile.eservices.qa.pages.payments.api.EOSPaymentmanager;
import com.tmobile.eservices.qa.pages.payments.api.EOSencription;
import com.tmobile.eservices.qa.pages.payments.api.EOSservicequotefilter;

import io.restassured.response.Response;

public class FDPMonitor extends EOSCommonMethods {

	public Map<String, String> tokenMap;

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = { "FDPPUBMonitor" })
	public void monitorFDPWithNewBank(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSFDPV3 fdp = new EOSFDPV3();
		EOSPaymentmanager pm = new EOSPaymentmanager();
		EOSAccountsV3Filters accounts = new EOSAccountsV3Filters();
		EOSencription encri = new EOSencription();
		EOSservicequotefilter servicequote = new EOSservicequotefilter();
		Map<Object, Object> optpara = new HashMap<Object, Object>();
		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {

			Response responseschedulepayment = pm.getsschedulepaymentresponse(getjwt);
			String payindicator = pm.checkschedulepayment(responseschedulepayment);

			if (payindicator != null) {
				if (payindicator.equalsIgnoreCase("True")) {
					Response responsedeletefdp = fdp.responseDeleteFDP(getjwt);
					checkexpectedvalues(responsedeletefdp, "status.statusCode", "COMPLETED");
				}
			}

			Response publickeyresponse = accounts.getResponsePublickKey(getjwt);
			String publickey = accounts.getpublicKey(publickeyresponse);

			String encrypts = encri.RsaEncryption("RSA", "AQAB", publickey, "3245456789");
			LocalDate date = LocalDate.now();
			LocalDate featruedate = date.plusDays(7);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd");

			optpara.put("date", formatter.format(featruedate).toString());
			optpara.put("accountNumber", encrypts);
			optpara.put("routingNumber", "125000105");
			optpara.put("addType", "Checking");
			optpara.put("isSave", false);
			optpara.put("chargeAmount", Math.round(generateRandomAmount() * 100.0) / 100.0);

			// Response responsevalidate=accounts.getResponsevalidatebank(getjwt, optpara);
			Response response = fdp.getResponsenewbankfdp(getjwt, optpara);

			checkexpectedvalues(response, "payment.accountNumber", getjwt[3]);
			checkexpectedvalues(response, "payment.msisdn", getjwt[0]);

			String paymenttags[] = { "payment.paymentId", "payment.transactionType" };
			for (String tag : paymenttags) {
				checkjsontagitems(response, tag);
			}

		} else
			Assert.fail("JWT token retrieval failed check msisdn and password");

	}
}