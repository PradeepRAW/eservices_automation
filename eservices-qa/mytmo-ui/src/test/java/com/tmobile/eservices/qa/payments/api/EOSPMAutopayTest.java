package com.tmobile.eservices.qa.payments.api;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.api.EOSCommonMethods;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.payments.api.EOSPAAutopayLanding;
import com.tmobile.eservices.qa.pages.payments.api.IAMProfileV2;
import com.tmobile.eservices.qa.pages.payments.api.TIBCODiscountEligible;
import com.tmobile.eservices.qa.pages.payments.api.TIBCODiscountinfo;
import com.tmobile.eservices.qa.pages.payments.api.TIBCOSubscriber;

import io.restassured.response.Response;

public class EOSPMAutopayTest extends  EOSCommonMethods{
	
	
	
	
	@DataProvider(name = "testdata", parallel = true)
	public Iterator<String> prepareBooleans() throws MalformedURLException, IOException {
		String query = "$.[?(@.login!='invalid'&&@.usrtype=='PAH')].misdin";
		List<String> misdinpawords = com.jayway.jsonpath.JsonPath.parse(new URL(Constants.TEST_DATA_FILTER_LOCATION_PROD)).read(query);
		//Set<Object[]> misdinpawords = getallmisdins(FILE_NAME);
		return misdinpawords.iterator();
	}
	
	
	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "testdata")
	public void testEosPAAutopay(String misdinpwd) throws Exception {
		Reporter.log("TestName: GetEOSPAAP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		EOSPAAutopayLanding eospaap=new EOSPAAutopayLanding();
		EOSCommonMethods ecm=new EOSCommonMethods();
		IAMProfileV2 iamprofile=new IAMProfileV2();
		String misdin = misdinpwd.split("/")[0];
		String pwd = misdinpwd.split("/")[1];
		
	// String[] getjwt = getauthorizationandregisterinapigee("4252460074","TM0Test1");
	String[] getjwt = getauthorizationandregisterinapigee(misdin,pwd);
		if (getjwt != null) {
			
			
			Response responsePAAutopay=eospaap.getResponsePAAPLanding(getjwt);
			
			TIBCOSubscriber tsubinfo=new TIBCOSubscriber();
			Map<Object, Object> tokenMap = new HashMap<Object, Object>();
			
			tokenMap.put("msisdn",getjwt[0]);
			tokenMap.put("ban",getjwt[3]);
			Response subscriberinfo=tsubinfo.geResponseTibco_Getsubscriberdetails(tokenMap);
			
			Map<Object,Object> valssubinfo=tsubinfo.getcreditclass(subscriberinfo);
			
			Response iamprofileresponse=iamprofile.getResponseIAMProfileV2(getjwt);
			Map<Object,Object> iamprofilevals=iamprofile.getvaluesforEOSPAALP(iamprofileresponse, getjwt[0]);
			
	
			
			String Acctype=getjwt[8];
			String usrType=getjwt[4].trim();
			int lines=Integer.parseInt(getjwt[10]);
			boolean autopayeligible;
			String isB2B;
			String isSCNC;
		
			if(Acctype.startsWith("B"))isB2B="true";
			else isB2B="false";
			
			if(Acctype.equalsIgnoreCase("IQ"))isSCNC="true";
			else isSCNC="false";
			
			
			if(isSCNC.equalsIgnoreCase("true"))autopayeligible=false;
			
			else if(isB2B.equalsIgnoreCase("true")&&usrType.equalsIgnoreCase("GMP_NM_P")) autopayeligible=true;
			else if(usrType.equalsIgnoreCase("NPAH_NP")||usrType.equalsIgnoreCase("NPAH_R")||usrType.equalsIgnoreCase("GMP_NM_P")||usrType.equalsIgnoreCase("GMP_NM_NP"))autopayeligible=false;
			else autopayeligible=true;
			String reqeasypaystatus=valssubinfo.get("easypay").toString();
			Map<Object,Object> discountinfo;
			if(reqeasypaystatus.equalsIgnoreCase("true")) {
				TIBCODiscountinfo discinfo=new TIBCODiscountinfo();
				Response responseDiscinfo=discinfo.geResponseTibco_Discuntinfo(tokenMap);
				discountinfo=discinfo.getResponseValuesFromDiscountinfo(responseDiscinfo,lines);
			}
			else {
				TIBCODiscountEligible disceligible=new TIBCODiscountEligible();
				Response responsedisceligible=disceligible.geResponseTibco_EligibleDiscount(tokenMap);
				discountinfo=disceligible.getResponseValuesFromDiscounteligible(responsedisceligible,lines);
			}
			
			
			checkexpectedvalues(responsePAAutopay, "easyPayInfo.isEligibleForAutoPay", Boolean.toString(autopayeligible));
			checkexpectedvalues(responsePAAutopay, "easyPayInfo.isAutoPayEnrrolled", reqeasypaystatus.toString());
			//checkexpectedvalues(responsePAAutopay, "easyPayInfo.autopayRecurringDay",valssubinfo.get("aprecuringdate").toString());
			checkexpectedvalues(responsePAAutopay, "easyPayInfo.isInsideBlackoutPeriod", valssubinfo.get("insideblackoutperiod").toString());
			checkexpectedvalues(responsePAAutopay, "easyPayInfo.autoPayDate", valssubinfo.get("autopaydate").toString());
			
			
			
			
			
			checkexpectedvalues(responsePAAutopay, "subscriberInfo.address.addressLine1", valssubinfo.get("Address1").toString());
			checkexpectedvalues(responsePAAutopay, "subscriberInfo.address.cityName", valssubinfo.get("city").toString());
			checkexpectedvalues(responsePAAutopay, "subscriberInfo.address.stateCode", valssubinfo.get("state").toString());
			checkexpectedvalues(responsePAAutopay, "subscriberInfo.address.postalCode", valssubinfo.get("postalcode").toString());
			
			checkexpectedvalues(responsePAAutopay, "subscriberInfo.creditClass", valssubinfo.get("Creditclass").toString());
			checkexpectedvalues(responsePAAutopay, "subscriberInfo.dueDate", valssubinfo.get("duedate").toString());
			checkexpectedvalues(responsePAAutopay, "subscriberInfo.pastDueAmount", valssubinfo.get("pastdue").toString());
			checkexpectedvalues(responsePAAutopay, "subscriberInfo.balanceDue", valssubinfo.get("balancedue").toString());
			checkexpectedvalues(responsePAAutopay, "subscriberInfo.hasPastDue", valssubinfo.get("haspastdue").toString());
			
			
			checkexpectedvalues(responsePAAutopay, "discountInfo.isAutoPayDiscountAvailable", discountinfo.get("discounteligible").toString());
			checkexpectedvalues(responsePAAutopay, "discountInfo.autoPayCreditAmount", discountinfo.get("discount").toString());
			
			checkexpectedvalues(responsePAAutopay, "profileInfo.accountHolderName", iamprofilevals.get("name").toString());
			
			
			
			
			
			
		
	}
		
		
		
		
	
}
	
	
	

	
	
}