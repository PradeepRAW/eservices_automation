package com.tmobile.eservices.qa.accounts.api.cpslookup;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;

import io.restassured.response.Response;

public class MPMBingeOn {

	public static List<String> getallratePlans(String excel) {
		List<String> misdinpassword = new ArrayList<>();
		try {

			Workbook workbook;
			FileInputStream excelFile = new FileInputStream(new File(excel));
			if (excel.contains("xlsx")) {
				workbook = new XSSFWorkbook(excelFile);
			} else {
				workbook = new HSSFWorkbook(excelFile);
			}

			org.apache.poi.ss.usermodel.Sheet datatypeSheet = workbook.getSheetAt(0);

			Iterator<Row> iterator = datatypeSheet.iterator();

			while (iterator.hasNext()) {

				Row currentRow = iterator.next();

				if (currentRow.getCell(0) != null) {

					String rp = currentRow.getCell(0).getStringCellValue();

					if (!rp.trim().equals("")) {
						if (!rp.equalsIgnoreCase("loginEmailOrPhone")) {

							misdinpassword.add(rp.trim());

						}
					}
				}
			}
			workbook.close();
			excelFile.close();

		} catch (Exception e) {

		}
		return misdinpassword;
	}

	@Test(groups = "mpm1")
	public void TestMPM() throws Exception {
		String FILE_NAME = System.getProperty("user.dir") + "/src/test/resources/testdata/WPC2.xlsx";

		List<String> misdinpawords = getallratePlans(FILE_NAME);
		Iterator<String> iterator = misdinpawords.iterator();
		Response response;
		List<String> Al = new ArrayList<String>();
		List<String> A0 = new ArrayList<String>();
		List<String> A1 = new ArrayList<String>();
		List<String> A2 = new ArrayList<String>();
		List<String> A4 = new ArrayList<String>();
		List<String> A3 = new ArrayList<String>();

		while (iterator.hasNext()) {

			String misdpwd = iterator.next();
			String rateplan = misdpwd;

			RestService restService = new RestService("", "https://core.op.api.internal.t-mobile.com:443");

			restService.setContentType("application/json");
			restService.addHeader("Authorization",
					"Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlVGSlBSRVJCVkMweU1ERTUifQ.eyJpYXQiOjE1NzE3ODA2OTYsImV4cCI6MTU3MTgxNjY5NiwiaXNzIjoiaHR0cHM6Ly9icmFzcy5hY2NvdW50LnQtbW9iaWxlLmNvbSIsImF1ZCI6Ik1ZVE1PIiwibm9uY2UiOiIyMDY4MDI1Mjg1IiwiQVQiOiIwMi5VU1IuNTZUZTdWSmlkdU15dmpEbEIiLCJzdWIiOiJVLWRhYjVjZDYxLTA3MDUtNGVlMy05OTc2LWIzNzE3YTU0OTFlZiIsImFjciI6ImxvYTIiLCJhbXIiOlsicGFzc3dvcmQiLCJzZWN1cml0eV9xdWVzdGlvbiJdLCJ1c24iOiI3Nzc3NDMxMzM1MjJmNWIyIiwiZW50X3R5cGUiOiJkaXJlY3QiLCJlbnQiOnsiYWNjdCI6W3siciI6IkFPIiwiaWQiOiI5MTg3MjEyNTYiLCJ0c3QiOiJJUiIsImxpbmVfY291bnQiOjEwLCJsaW5lcyI6W3sicGhudW0iOiIyMDY4MDI1Mjg1IiwiciI6IkQiLCJkaXNhYmxlZCI6dHJ1ZX1dfV19fQ.gpQXbd3IEP6E73qd3WKK7ZyNElrLXrc701bOIkSSJbQEGippRdqTdLtsPe1c3vAtezITSyTvBxwHgVRR-tc2ddl4n9yJbhhQ3Ww1MkXYq0N1lOoG0mSiCw2YLUsOxD-y8giwTBt8ucZ51bIXW--Eh5W0dQamuGuLJP1twsqsbtJ_pAla83_P33dKGAwNIWoyvj7blAUhGGhB557Yy19XGktsXjQNotpvkJC6ERjHrRC2s9blpEhPGe3N1cG4q-hJeGMO_VWE_B5uHnX4tMA1aAGZ36XS-VrNvAkNJOCVLOAsFZp2y_eIc8IyaACRXXeiRmjUKs4ML8g_XGuHdwHYrg");
			restService.addHeader("X-Auth-Originator",
					"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlVGSlBSRVJCVkMweU1ERTUifQ.eyJpYXQiOjE1NzE3ODA2OTYsImV4cCI6MTU3MTgxNjY5NiwiaXNzIjoiaHR0cHM6Ly9icmFzcy5hY2NvdW50LnQtbW9iaWxlLmNvbSIsImF1ZCI6Ik1ZVE1PIiwibm9uY2UiOiIyMDY4MDI1Mjg1IiwiQVQiOiIwMi5VU1IuNTZUZTdWSmlkdU15dmpEbEIiLCJzdWIiOiJVLWRhYjVjZDYxLTA3MDUtNGVlMy05OTc2LWIzNzE3YTU0OTFlZiIsImFjciI6ImxvYTIiLCJhbXIiOlsicGFzc3dvcmQiLCJzZWN1cml0eV9xdWVzdGlvbiJdLCJ1c24iOiI3Nzc3NDMxMzM1MjJmNWIyIiwiZW50X3R5cGUiOiJkaXJlY3QiLCJlbnQiOnsiYWNjdCI6W3siciI6IkFPIiwiaWQiOiI5MTg3MjEyNTYiLCJ0c3QiOiJJUiIsImxpbmVfY291bnQiOjEwLCJsaW5lcyI6W3sicGhudW0iOiIyMDY4MDI1Mjg1IiwiciI6IkQiLCJkaXNhYmxlZCI6dHJ1ZX1dfV19fQ.gpQXbd3IEP6E73qd3WKK7ZyNElrLXrc701bOIkSSJbQEGippRdqTdLtsPe1c3vAtezITSyTvBxwHgVRR-tc2ddl4n9yJbhhQ3Ww1MkXYq0N1lOoG0mSiCw2YLUsOxD-y8giwTBt8ucZ51bIXW--Eh5W0dQamuGuLJP1twsqsbtJ_pAla83_P33dKGAwNIWoyvj7blAUhGGhB557Yy19XGktsXjQNotpvkJC6ERjHrRC2s9blpEhPGe3N1cG4q-hJeGMO_VWE_B5uHnX4tMA1aAGZ36XS-VrNvAkNJOCVLOAsFZp2y_eIc8IyaACRXXeiRmjUKs4ML8g_XGuHdwHYrg");
			restService.addHeader("Accept", "application/json");
			restService.addHeader("activity-id", "321321312");
			restService.addHeader("X-B3-TraceId", "222");
			restService.addHeader("X-B3-SpanId", "222");
			restService.addHeader("application", "222");
			restService.addHeader("channel-id", "222");
			restService.addHeader("consumertype", "222");
			restService.addHeader("Postman-Token", "eef93768-ed14-4076-afe5-9cbbf947a8f2");

			String rateplans = rateplan;
			String url = "products/v3/rateplans?soc=" + rateplans + "&channel=MYT";

			try {
				response = restService.callService(url, RestCallType.GET);
				if (response.statusCode() == 200 && response.jsonPath().getInt("totalResultCount") == 0) {
					Al.add(rateplans);
				} else {
					A3 = response.jsonPath().getList("rateplanSpecs[0].customerFacingServices.code");

					Iterator<String> it = A3.iterator();

					int c = 0;
					int b = 0;
					while (it.hasNext()) {

						Object s = it.next();

						if (s.toString().contains("BNGN")) {
							c = c + 1;
						}

						else if (s.toString().contains("BGON")) {
							b = b + 1;
						}

						else {
						}

					} // while
					if (b == 1 && c == 1) {
						A0.add(rateplans);
					} else if (b == 1 && c == 0) {
						A1.add(rateplans);
					} else if (b == 0 && c == 1) {
						A2.add(rateplans);
					} else {
						A4.add(rateplans);
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (A0 != null) {
			Reporter.log("both codes" + "  ----------    " + A0.toString());
		}

		if (A1 != null) {
			Reporter.log(" BGON" + "  ------------    " + A1.toString());
		}
		if (A2 != null) {
			Reporter.log("BNGN" + " --------------     " + A2.toString());
		}
		if (A4 != null) {
			Reporter.log("no codes" + " ------------     " + A4.toString());
		}
	}
}
