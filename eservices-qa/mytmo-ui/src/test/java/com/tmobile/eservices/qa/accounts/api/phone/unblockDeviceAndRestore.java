
package com.tmobile.eservices.qa.accounts.api.phone;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class unblockDeviceAndRestore {

	String environment;

	@Test(groups = "phone")
	public void unblockDeviceAndRestoreOnlyUnblock() {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();

		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("interactionId", "123456787");
		request.header("workflowId", "1234");
		request.header("sender_id", "MYTMO");
		request.header("application_id", "ESERVICE");
		request.header("channel_id", "WEB");
		request.contentType("application/json");

		JSONObject requestParams = new JSONObject();
		requestParams.put("isRestore", "false");
		JSONObject deviceAccountDetails = new JSONObject();
		deviceAccountDetails.put("imei", "357795070005570");
		deviceAccountDetails.put("accountNumber", "944658096");
		deviceAccountDetails.put("productType", "GSM");
		JSONArray array = new JSONArray();
		array.put("5627087830");
		deviceAccountDetails.put("msisdn", array);
		requestParams.put("deviceAccountDetails", deviceAccountDetails);

		JSONObject restorationDetails = new JSONObject();
		restorationDetails.put("restoreDate", "2018-12-31");
		restorationDetails.put("restoreReasonCode", "ELST");
		requestParams.put("restorationDetails", restorationDetails);

		JSONObject deviceBlockInfo = new JSONObject();
		deviceBlockInfo.put("phoneMissingType", "LT");
		requestParams.put("deviceBlockInfo", deviceBlockInfo);

		System.out.println(requestParams.toString());

		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/unblockDeviceAndRestore");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating unblockDeviceAndRestore Operation Response ");
		Reporter.log("Note :  unblockDeviceAndRestore Service Internally calls getDeviceUnlockAttribute ");
		Reporter.log("Test data : Account eligible for suspension");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(
					response.jsonPath().getString("message").contains("IMEI 357795070005570 blocking status updated"));
			Assert.assertTrue(response.jsonPath().getString("blockIndicator").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("reasonDescription")
					.contains("Reported lost by a T-Mobile customer"));
			Assert.assertTrue(response.jsonPath().getString("serviceStatus.code").contains("100"));
			Assert.assertTrue(response.jsonPath().getString("serviceStatus.result").contains("SUCCESS"));
			Assert.assertTrue(
					response.jsonPath().getString("serviceStatus.reason").contains("Your changes have been made"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(groups = "phone2")
	public void BothUnblockAndRestore() {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();

		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("interactionId", "123456787");
		request.header("workflowId", "1234");
		request.header("sender_id", "MYTMO");
		request.header("application_id", "ESERVICE");
		request.header("channel_id", "WEB");
		request.contentType("application/json");

		JSONObject requestParams = new JSONObject();
		requestParams.put("isRestore", "false");
		JSONObject deviceAccountDetails = new JSONObject();
		deviceAccountDetails.put("imei", "357795070005570");
		deviceAccountDetails.put("accountNumber", "944658096");
		deviceAccountDetails.put("productType", "GSM");
		JSONArray array = new JSONArray();
		array.put("5627087830");
		deviceAccountDetails.put("msisdn", array);
		requestParams.put("deviceAccountDetails", deviceAccountDetails);

		JSONObject restorationDetails = new JSONObject();
		restorationDetails.put("restoreDate", "2018-12-31");
		restorationDetails.put("restoreReasonCode", "ELST");
		requestParams.put("restorationDetails", restorationDetails);

		JSONObject deviceBlockInfo = new JSONObject();
		deviceBlockInfo.put("phoneMissingType", "LT");
		requestParams.put("deviceBlockInfo", deviceBlockInfo);

		System.out.println(requestParams.toString());

		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/unblockDeviceAndRestore");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating unblockDeviceAndRestore Operation Response ");
		Reporter.log("Note :  unblockDeviceAndRestore Service Internally calls getDeviceUnlockAttribute ");
		Reporter.log("Test data : Account eligible for suspension");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(
					response.jsonPath().getString("message").contains("IMEI 357795070005570 blocking status updated"));
			Assert.assertTrue(response.jsonPath().getString("blockIndicator").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("isRestored").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("reasonDescription")
					.contains("Reported lost by a T-Mobile customer"));
			Assert.assertTrue(response.jsonPath().getString("serviceStatus.code").contains("1"));
			Assert.assertTrue(response.jsonPath().getString("serviceStatus.result").contains("SUCCESS"));
			Assert.assertTrue(response.jsonPath().getString("serviceStatus.reason")
					.contains("Your device has been unblocked and the line has been restored"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(groups = "phone3")
	public void RestoreSuccessUnblockFalied() {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();

		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("interactionId", "123456787");
		request.header("workflowId", "1234");
		request.header("sender_id", "MYTMO");
		request.header("application_id", "ESERVICE");
		request.header("channel_id", "WEB");
		request.contentType("application/json");

		JSONObject requestParams = new JSONObject();
		requestParams.put("isRestore", "true");
		JSONObject deviceAccountDetails = new JSONObject();
		deviceAccountDetails.put("imei", "357795070005570");
		deviceAccountDetails.put("accountNumber", "944658096");
		deviceAccountDetails.put("productType", "GSM");
		JSONArray array = new JSONArray();
		array.put("5627087830");
		deviceAccountDetails.put("msisdn", array);
		requestParams.put("deviceAccountDetails", deviceAccountDetails);

		JSONObject restorationDetails = new JSONObject();
		restorationDetails.put("restoreDate", "2018-12-31");
		restorationDetails.put("restoreReasonCode", "ELST");
		requestParams.put("restorationDetails", restorationDetails);

		JSONObject deviceBlockInfo = new JSONObject();
		deviceBlockInfo.put("phoneMissingType", "LT");
		requestParams.put("deviceBlockInfo", deviceBlockInfo);

		System.out.println(requestParams.toString());

		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/unblockDeviceAndRestore");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating unblockDeviceAndRestore Operation Response ");
		Reporter.log("Note :  unblockDeviceAndRestore Service Internally calls getDeviceUnlockAttribute ");
		Reporter.log("Test data : Account eligible for suspension");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(
					response.jsonPath().getString("message").contains("IMEI 357795070005570 blocking status updated"));
			Assert.assertTrue(response.jsonPath().getString("serviceStatus.code").contains("2"));
			Assert.assertTrue(response.jsonPath().getString("serviceStatus.result").contains("SUCCESS"));
			Assert.assertTrue(response.jsonPath().getString("serviceStatus.reason")
					.contains("Your device was not unblocked and the line has been restored."));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(groups = "phone3")
	public void UnblockSuccessRestoreFailed() {

		Response response = null;

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();

		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("interactionId", "123456787");
		request.header("workflowId", "1234");
		request.header("sender_id", "MYTMO");
		request.header("application_id", "ESERVICE");
		request.header("channel_id", "WEB");
		request.contentType("application/json");

		JSONObject requestParams = new JSONObject();
		requestParams.put("isRestore", "true");
		JSONObject deviceAccountDetails = new JSONObject();
		deviceAccountDetails.put("imei", "357795070005570");
		deviceAccountDetails.put("accountNumber", "944658096");
		deviceAccountDetails.put("productType", "GSM");
		JSONArray array = new JSONArray();
		array.put("5627087830");
		deviceAccountDetails.put("msisdn", array);
		requestParams.put("deviceAccountDetails", deviceAccountDetails);

		JSONObject restorationDetails = new JSONObject();
		restorationDetails.put("restoreDate", "2018-12-30");
		restorationDetails.put("restoreReasonCode", "ELST");
		requestParams.put("restorationDetails", restorationDetails);

		JSONObject deviceBlockInfo = new JSONObject();
		deviceBlockInfo.put("phoneMissingType", "LT");
		requestParams.put("deviceBlockInfo", deviceBlockInfo);

		System.out.println(requestParams.toString());

		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/unblockDeviceAndRestore");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating unblockDeviceAndRestore Operation Response ");
		Reporter.log("Note :  unblockDeviceAndRestore Service Internally calls getDeviceUnlockAttribute ");
		Reporter.log("Test data : Account eligible for suspension");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(
					response.jsonPath().getString("message").contains("IMEI 357795070005570 blocking status updated"));
			Assert.assertTrue(response.jsonPath().getString("blockIndicator").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("reasonDescription")
					.contains("Reported lost by a T-Mobile customer"));
			Assert.assertTrue(response.jsonPath().getString("isRestored").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("serviceStatus.code").contains("3"));
			Assert.assertTrue(response.jsonPath().getString("serviceStatus.result").contains("SUCCESS"));
			Assert.assertTrue(response.jsonPath().getString("serviceStatus.reason")
					.contains("Your device has been unblocked and the line was not restored."));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}
}
