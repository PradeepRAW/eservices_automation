package com.tmobile.eservices.qa.payments.api;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.EOSCommonMethods;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EOSBillingFilters;
import com.tmobile.eservices.qa.pages.payments.api.EOSusageFilter;

import io.restassured.response.Response;

public class EOSUsageV1Test extends EOSCommonMethods {

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEosusagesummaryV1(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSusageFilter usage = new EOSusageFilter();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response usagesummary = usage.getResponseusagesummary(getjwt);
			checkexpectedvalues(usagesummary, "statusCode", "200");
			checkexpectedvalues(usagesummary, "statusMessage", "Success");

		}

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEosusagesummarydatastashV1(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetBillList");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the billlist of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSusageFilter usage = new EOSusageFilter();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response datastashresponse = usage.getResponseusagesummarydatastash(getjwt);
			checkexpectedvalues(datastashresponse, "statusCode", "200");
			checkexpectedvalues(datastashresponse, "statusMessage", "Success");

		}

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEosusagedetailsebillV1(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetBillList");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the billlist of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSBillingFilters billing = new EOSBillingFilters();
		EOSusageFilter usage = new EOSusageFilter();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response billlingresponse = billing.getResponsebillList(getjwt);
			String statementid = billing.getebillstatemenid(billlingresponse);
			Response usagedetailsebillresponse = usage.getUsagedetailseresponsebill(getjwt, statementid);

			checkexpectedvalues(usagedetailsebillresponse, "statusCode", "200");
			checkexpectedvalues(usagedetailsebillresponse, "statusMessage", "Success");

		}

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEosusagedetailsbbillV1(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetBillList");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the billlist of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSBillingFilters billing = new EOSBillingFilters();
		EOSusageFilter usage = new EOSusageFilter();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response billlingresponse = billing.getResponsebillList(getjwt);
			String docid = billing.getdocumentid(billlingresponse);
			Response usagedetailsbbillresponse = usage.getUsagedetailsresponsebbill(getjwt, docid);

			checkexpectedvalues(usagedetailsbbillresponse, "statusCode", "200");
			checkexpectedvalues(usagedetailsbbillresponse, "statusMessage", "Success");

		}

	}

}