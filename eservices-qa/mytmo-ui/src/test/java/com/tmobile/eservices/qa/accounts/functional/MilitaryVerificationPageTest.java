/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.MilitaryVerificationPage;

/**
 * @author agarpaw1
 *
 */
public class MilitaryVerificationPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(MilitaryVerificationPageTest.class);

	// ===============================Military Verification
	// Page================================

	/**
	 * US390216 :: Military Verification Page - Page build out (super set with
	 * static data) *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyHeadeAndSubHeaderOnMillitaryVerificationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyHeadeAndSubHeaderOnMillitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify header and subheader on Millitary Verification Page");
		Reporter.log("Test Data : Any PAH/Full and Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check Millitary verification page | It should be displayed correctly.");
		Reporter.log("6. Check Header | Header should be displayed.");
		Reporter.log("7. Check SubHeader | SubHeader should be displayed.");
		Reporter.log("8. Check Description | Description should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		// navigateToHomePage(myTmoData);
		navigateToProfilePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkHeaderSubHeaderAndDescriptionOnMilitaryVerificationPage();
	}

	/**
	 * US390216 :: Military Verification Page - Page build out (super set with
	 * static data) *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyRedirectionOfFAQLinkInDescriptionOnMillitaryVerificationPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyRedirectionOfFAQLinkInDescriptionOnMillitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify FAQ link redirection on Millitary Verification Page");
		Reporter.log("Test Data : Any PAH/Full and Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check FAQ link in Description on verification page | It should be displayed correctly.");
		Reporter.log("6. Click on FAQ link | It should be opened in new tab.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		// navigateToHomePage(myTmoData);
		navigateToProfilePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkRedirectionOfFAQURLOnMilitaryVerificationPage();
	}

	/**
	 * US390216 :: Military Verification Page - Page build out (super set with
	 * static data) US390364 :: Military Verification Page - Veteran or retiree
	 * *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyMillitaryVerificationPageForVeteranOrRetiree(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyMillitaryVerificationPageForVeteranOrRetiree method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Millitary Verification Page For Veteran Or Retiree users");
		Reporter.log("Test Data : Any PAH/Full and Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check Millitary verification page | It should be displayed correctly.");
		Reporter.log("6. Check Veteran or Retiree radion button. | It should be selected by default.");
		Reporter.log("7. Check Veteran Label, Sublabels. | It should be displayed.");
		Reporter.log(
				"8. Check Branch of Service dropdown and select valid value from it. | Dropdown should be displayed and user should be able to select valid value from dropdown.");
		Reporter.log(
				"9. Check Discharge Date dropdown and select valid year from it. | Dropdown should be displayed and user should be able to select valid year from dropdown.");
		Reporter.log("10. Check First name of military member. | It should be disabled.");
		Reporter.log("11. Check Last name of military member. | It should be disabled.");
		Reporter.log(
				"12. Check Month dropdown and select valid month from it. | Dropdown should be displayed and user should be able to select valid month from dropdown.");
		Reporter.log(
				"13. Check Day dropdown and select valid Day from it. | Dropdown should be displayed and user should be able to select valid Day from dropdown.");
		Reporter.log(
				"14. Check Year dropdown and select valid Year from it. | Dropdown should be displayed and user should be able to select valid Year from dropdown.");
		Reporter.log("15. Check Email Address field. | It should be displayed and should be editable.");
		Reporter.log(
				"16. Check Confirm Email Address field. | It should be displayed and should be editable. It should be blank");
		Reporter.log("17. Check Phone number field. | Phone number should be displayed and should be disabled.");
		Reporter.log("18. Check Submit button. | Submit button should be disabled.");
		Reporter.log(
				"19. Enter valid data in all fields and then check Submit button. | Submit button should be Enabled.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		// navigateToHomePage(myTmoData);
		navigateToProfilePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifyEachComponentForVeteranOrRetireeOnMilitaryVerificationPage();

	}

	/**
	 * US390216 :: Military Verification Page - Page build out (super set with
	 * static data) US400251 :: Military Verification Page - Active duty *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyMillitaryVerificationPageForActiveDuty(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyMillitaryVerificationPageForActiveDuty method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Millitary Verification Page For Active Duty users");
		Reporter.log("Test Data : Any PAH/Full and Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check Millitary verification page | It should be displayed correctly.");
		Reporter.log("6. Check Active Duty radio button. | User should be able to check it.");
		Reporter.log("7. Check Active Duty Label, Sublabels. | It should be displayed.");
		Reporter.log(
				"8. Check Branch of Service dropdown. | Dropdown should be displayed and user should be able to select valid value from dropdown.");
		Reporter.log("9. Check Discharge Date dropdown. | Dropdown should not be displayed.");
		Reporter.log("10. Check First name of military member. | It should be editable.");
		Reporter.log("11. Check Last name of military member. | It should be editable.");
		Reporter.log(
				"12. Check Month dropdown and select valid month from it. | Dropdown should be displayed and user should be able to select valid month from dropdown.");
		Reporter.log(
				"13. Check Day dropdown and select valid Day from it. | Dropdown should be displayed and user should be able to select valid Day from dropdown.");
		Reporter.log(
				"14. Check Year dropdown and select valid Year from it. | Dropdown should be displayed and user should be able to select valid Year from dropdown.");
		Reporter.log("15. Check Email Address field. | It should be displayed and should be editable.");
		Reporter.log(
				"16. Check Confirm Email Address field. | It should be displayed and should be editable. It should be blank");
		Reporter.log("17. Check Phone number field. | Phone number should be displayed and should be disabled.");
		Reporter.log("18. Check Submit button. | Submit button should be disabled.");
		Reporter.log(
				"19. Enter valid data in all fields and then check Submit button. | Submit button should be Enabled.");
		Reporter.log("20. Check Sheer ID text. | Sheer ID text should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifyEachComponentForActiveDutyOnMilitaryVerificationPage();

	}

	/**
	 * US390216 :: Military Verification Page - Page build out (super set with
	 * static data) US400467 :: Military Verification Page - National Guard or
	 * Reserve *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyMillitaryVerificationPageForNationalGuardOrReserve(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyMillitaryVerificationPageForNationalGuardOrReserve method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Millitary Verification Page For Active Duty users");
		Reporter.log("Test Data : Any PAH/Full and Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check Millitary verification page | It should be displayed correctly.");
		Reporter.log("6. Check National Guard Or Reserve radio button. | User should be able to check it.");
		Reporter.log("7. Check National Guard Or Reserve Label, Sublabels. | It should be displayed.");
		Reporter.log(
				"8. Check Branch of Service dropdown. | Dropdown should be displayed and user should be able to select valid value from dropdown.");
		Reporter.log("9. Check Discharge Date dropdown. | Dropdown should not be displayed.");
		Reporter.log("10. Check First name of military member. | It should be disabled.");
		Reporter.log("11. Check Last name of military member. | It should be disabled.");
		Reporter.log(
				"12. Check Month dropdown and select valid month from it. | Dropdown should be displayed and user should be able to select valid month from dropdown.");
		Reporter.log(
				"13. Check Day dropdown and select valid Day from it. | Dropdown should be displayed and user should be able to select valid Day from dropdown.");
		Reporter.log(
				"14. Check Year dropdown and select valid Year from it. | Dropdown should be displayed and user should be able to select valid Year from dropdown.");
		Reporter.log("15. Check Email Address field. | It should be displayed and should be editable.");
		Reporter.log(
				"16. Check Confirm Email Address field. | It should be displayed and should be editable. It should be blank");
		Reporter.log("17. Check Phone number field. | Phone number should be displayed and should be disabled.");
		Reporter.log("18. Check Submit button. | Submit button should be disabled.");
		Reporter.log(
				"19. Enter valid data in all fields and then check Submit button. | Submit button should be Enabled.");
		Reporter.log("20. Check Sheer ID text. | Sheer ID text should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifyEachComponentForNationalGuardOrReserveOnMilitaryVerificationPage();

	}

	/**
	 * US390216 :: Military Verification Page - Page build out (super set with
	 * static data) US400681 :: Military Verification Page - Gold Star *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyMillitaryVerificationPageForGoldStar(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyMillitaryVerificationPageForGoldStar method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Millitary Verification Page For Active Duty users");
		Reporter.log("Test Data : Any PAH/Full and Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check Millitary verification page | It should be displayed correctly.");
		Reporter.log("6. Check National Guard Or Reserve radio button. | User should be able to check it.");
		Reporter.log("7. Check National Guard Or Reserve Label, Sublabels. | It should be displayed.");
		Reporter.log("8. Check Branch of Service dropdown. | Dropdown should not be displayed.");
		Reporter.log("9. Check Discharge Date dropdown. | Dropdown should not be displayed.");
		Reporter.log(
				"10. Check Relatioship to Military Member dropdown. | Dropdown should be displayed and user should be able to select valid value from dropdown.");
		Reporter.log("11. Check First name of military member. | It should be disabled.");
		Reporter.log("12. Check Last name of military member. | It should be disabled.");
		Reporter.log(
				"13. Check Month dropdown and select valid month from it. | Dropdown should be displayed and user should be able to select valid month from dropdown.");
		Reporter.log(
				"14. Check Day dropdown and select valid Day from it. | Dropdown should be displayed and user should be able to select valid Day from dropdown.");
		Reporter.log(
				"15. Check Year dropdown and select valid Year from it. | Dropdown should be displayed and user should be able to select valid Year from dropdown.");
		Reporter.log("16. Check Email Address field. | It should be displayed and should be editable.");
		Reporter.log(
				"17. Check Confirm Email Address field. | It should be displayed and should be editable. It should be blank");
		Reporter.log("18. Check Phone number field. | Phone number should be displayed and should be disabled.");
		Reporter.log("19. Check Submit button. | Submit button should be disabled.");
		Reporter.log(
				"20. Enter valid data in all fields and then check Submit button. | Submit button should be Enabled.");
		Reporter.log("21. Check Sheer ID text. | Sheer ID text should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifyEachComponentForGoldStarOnMilitaryVerificationPage();

	}

	/**
	 * US400718 :: Military Verification Page - Field validations *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyBranchOfServiceAndDischargeDateFieldValidationForVeteranOnMillitaryVerificationPage(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyFieldValidationForVeteranOnMillitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Validate fields on Millitary Verification Page");
		Reporter.log("Test Data : Any PAH/Full and Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log(
				"8. Select option Select Branch of service from Branch  of Service dropdown. | User should be able to select it.");
		Reporter.log("9. Check error message. | Error message - Please select branch of service  should be displayed.");
		Reporter.log(
				"10. Select option Select discharge date from Discharge date dropdown. | User should be able to select it.");
		Reporter.log("11. Check error message. | Error message - Please select discharge date  should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkBranChOfServiceAndDischargeDateFieldValidationForVeteran();

	}

	/**
	 * US400718 :: Military Verification Page - Field validations *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyFieldValidationForFirstAndLastNameFieldOnMillitaryVerificationPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyFieldValidationForEmailAddressAndNameFieldOnMillitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Validate fields on Millitary Verification Page");
		Reporter.log("Test Data : Any PAH/Full and Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("7. Click on Active Duty radio button. | User should be able to check it.");
		Reporter.log("8. Enter invalid First name. | User should be able to enter it.");
		Reporter.log("9. Check error message. | Error message - Enter a valid name should be displayed.");
		Reporter.log("10. Enter invalid Last name. | User should be able to enter it.");
		Reporter.log("11. Check error message. | Error message - Enter a valid name should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkFirstNameAndLastNameFieldValidationsOnMilitaryVerificationPage();

	}

	/**
	 * US400718 :: Military Verification Page - Field validations *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyEmailAddressAndRelationshipFieldValidationForGoldStartUserOnMillitaryVerificationPage(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyEmailAddressAndRelationshipFieldValidationForGoldStartUserOnMillitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Validate fields on Millitary Verification Page");
		Reporter.log("Test Data : Any PAH/Full and Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Click on Gold Start radio button. | User should be able to check it.");
		Reporter.log(
				"6. Select option Select relationship from Relationship to military member dropdown. | User should be able to select it.");
		Reporter.log(
				"7. Check error message. | Error message - Please select relationship status should be displayed.");
		Reporter.log("8. Enter wrong email address in Confirm Email Address field. | User should be able to enter it.");
		Reporter.log("9. Check error message. | Error message - Email does not match  should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkRelationshipAndEmailAddressFieldValidationOnMilitaryVerificationPage();

	}

	/**
	 * US390216 :: Military Verification Page - Page build out (super set with
	 * static data) US399442 :: Military Verification Page - Business user *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyMillitaryVerificationPageForVeteranOrRetireeWhenBusinessUserLoggedIn(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyMillitaryVerificationPageForVeteranOrRetireeWhenBusinessUserLoggedIn method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Millitary Verification Page For Veteran Or Retiree users");
		Reporter.log("Test Data : Only Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check Millitary verification page | It should be displayed correctly.");
		Reporter.log("6. Check Veteran or Retiree radion button. | It should be selected by default.");
		Reporter.log("7. Check Veteran Label, Sublabels. | It should be displayed.");
		Reporter.log(
				"8. Check Branch of Service dropdown and select valid value from it. | Dropdown should be displayed and user should be able to select valid value from dropdown.");
		Reporter.log(
				"9. Check Discharge Date dropdown and select valid year from it. | Dropdown should be displayed and user should be able to select valid year from dropdown.");
		Reporter.log("10. Check First name of military member. | It should be editable.");
		Reporter.log("11. Check Last name of military member. | It should be editable.");
		Reporter.log(
				"12. Check Month dropdown and select valid month from it. | Dropdown should be displayed and user should be able to select valid month from dropdown.");
		Reporter.log(
				"13. Check Day dropdown and select valid Day from it. | Dropdown should be displayed and user should be able to select valid Day from dropdown.");
		Reporter.log(
				"14. Check Year dropdown and select valid Year from it. | Dropdown should be displayed and user should be able to select valid Year from dropdown.");
		Reporter.log("15. Check Email Address field. | It should be displayed and should be editable.");
		Reporter.log(
				"16. Check Confirm Email Address field. | It should be displayed and should be editable. It should be blank");
		Reporter.log("17. Check Phone number field. | Phone number should be displayed and should be disabled.");
		Reporter.log("18. Check Submit button. | Submit button should be disabled.");
		Reporter.log(
				"19. Enter valid data in all fields and then check Submit button. | Submit button should be Enabled.");
		Reporter.log("20. Check Sheer ID text. | Sheer ID text should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifyEachComponentForVeteranOrRetireeForBusinessUserOnMilitaryVerificationPage();

	}

	/**
	 * US390216 :: Military Verification Page - Page build out (super set with
	 * static data) US399442 :: Military Verification Page - Business user *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyMillitaryVerificationPageForActiveDutyWhenBusinessUserLoggedIn(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyMillitaryVerificationPageForActiveDutyWhenBusinessUserLoggedIn method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Millitary Verification Page For Active Duty");
		Reporter.log("Test Data : Only Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check Millitary verification page | It should be displayed correctly.");
		Reporter.log("6. Check Veteran or Retiree radion button. | It should be selected by default.");
		Reporter.log("7. Check Veteran Label, Sublabels. | It should be displayed.");
		Reporter.log(
				"8. Check Branch of Service dropdown and select valid value from it. | Dropdown should be displayed and user should be able to select valid value from dropdown.");
		Reporter.log("9. Check Discharge Date dropdown. | Dropdown should not be displayed.");
		Reporter.log("10. Check First name of military member. | It should be editable.");
		Reporter.log("11. Check Last name of military member. | It should be editable.");
		Reporter.log(
				"12. Check Month dropdown and select valid month from it. | Dropdown should be displayed and user should be able to select valid month from dropdown.");
		Reporter.log(
				"13. Check Day dropdown and select valid Day from it. | Dropdown should be displayed and user should be able to select valid Day from dropdown.");
		Reporter.log(
				"14. Check Year dropdown and select valid Year from it. | Dropdown should be displayed and user should be able to select valid Year from dropdown.");
		Reporter.log("15. Check Email Address field. | It should be displayed and should be editable.");
		Reporter.log(
				"16. Check Confirm Email Address field. | It should be displayed and should be editable. It should be blank");
		Reporter.log("17. Check Phone number field. | Phone number should be displayed and should be disabled.");
		Reporter.log("18. Check Submit button. | Submit button should be disabled.");
		Reporter.log(
				"19. Enter valid data in all fields and then check Submit button. | Submit button should be Enabled.");
		Reporter.log("20. Check Sheer ID text. | Sheer ID text should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifyEachComponentForActiveDutyForBusinessUserOnMilitaryVerificationPage();

	}

	/**
	 * US390216 :: Military Verification Page - Page build out (super set with
	 * static data) US399442 :: Military Verification Page - Business user *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyMillitaryVerificationPageForNationalGuardOrReserveWhenBusinessUserLoggedIn(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyMillitaryVerificationPageForNationalGuardOrReserveWhenBusinessUserLoggedIn method called in MillitaryVerificationPageTest");
		Reporter.log(
				"Test Case  Name : Verify Millitary Verification Page For National Guard when Business user logged in");
		Reporter.log("Test Data : Only Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check Millitary verification page | It should be displayed correctly.");
		Reporter.log("6. Check Veteran or Retiree radion button. | It should be selected by default.");
		Reporter.log("7. Check Veteran Label, Sublabels. | It should be displayed.");
		Reporter.log(
				"8. Check Branch of Service dropdown and select valid value from it. | Dropdown should be displayed and user should be able to select valid value from dropdown.");
		Reporter.log("9. Check Discharge Date dropdown. | Dropdown should not be displayed.");
		Reporter.log("10. Check First name of military member. | It should be editable.");
		Reporter.log("11. Check Last name of military member. | It should be editable.");
		Reporter.log(
				"12. Check Month dropdown and select valid month from it. | Dropdown should be displayed and user should be able to select valid month from dropdown.");
		Reporter.log(
				"13. Check Day dropdown and select valid Day from it. | Dropdown should be displayed and user should be able to select valid Day from dropdown.");
		Reporter.log(
				"14. Check Year dropdown and select valid Year from it. | Dropdown should be displayed and user should be able to select valid Year from dropdown.");
		Reporter.log("15. Check Email Address field. | It should be displayed and should be editable.");
		Reporter.log(
				"16. Check Confirm Email Address field. | It should be displayed and should be editable. It should be blank");
		Reporter.log("17. Check Phone number field. | Phone number should be displayed and should be disabled.");
		Reporter.log("18. Check Submit button. | Submit button should be disabled.");
		Reporter.log(
				"19. Enter valid data in all fields and then check Submit button. | Submit button should be Enabled.");
		Reporter.log("20. Check Sheer ID text. | Sheer ID text should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage
				.verifyEachComponentForNationalGuardOrReserveForBusinessUserOnMilitaryVerificationPage();

	}

	/**
	 * US390216 :: Military Verification Page - Page build out (super set with
	 * static data) US399442 :: Military Verification Page - Business user *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyMillitaryVerificationPageForGoldStarWhenBusinessUserLoggedIn(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyMillitaryVerificationPageForGoldStarWhenBusinessUserLoggedIn method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Millitary Verification Page For Gold Star when Business user logged in");
		Reporter.log("Test Data : Only Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check Millitary verification page | It should be displayed correctly.");
		Reporter.log("6. Check Veteran or Retiree radion button. | It should be selected by default.");
		Reporter.log("7. Check Veteran Label, Sublabels. | It should be displayed.");
		Reporter.log(
				"8. Check Relation dropdown. | Dropdown should be displayed and user should be able to select valid value from it.");
		Reporter.log("9. Check Branch of Service dropdown. | Dropdown should not be displayed");
		Reporter.log("10. Check Discharge Date dropdown. | Dropdown should not be displayed.");
		Reporter.log("11. Check First name of military member. | It should be editable.");
		Reporter.log("12. Check Last name of military member. | It should be editable.");
		Reporter.log(
				"13. Check Month dropdown and select valid month from it. | Dropdown should be displayed and user should be able to select valid month from dropdown.");
		Reporter.log(
				"14. Check Day dropdown and select valid Day from it. | Dropdown should be displayed and user should be able to select valid Day from dropdown.");
		Reporter.log(
				"15. Check Year dropdown and select valid Year from it. | Dropdown should be displayed and user should be able to select valid Year from dropdown.");
		Reporter.log("16. Check Email Address field. | It should be displayed and should be editable.");
		Reporter.log(
				"17. Check Confirm Email Address field. | It should be displayed and should be editable. It should be blank");
		Reporter.log("18. Check Phone number field. | Phone number should be displayed and should be disabled.");
		Reporter.log("19. Check Submit button. | Submit button should be disabled.");
		Reporter.log(
				"20. Enter valid data in all fields and then check Submit button. | Submit button should be Enabled.");
		Reporter.log("21. Check Sheer ID text. | Sheer ID text should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifyEachComponentForGoldStartForBusinessUserOnMilitaryVerificationPage();

	}

	/**
	 * US413544 :: Military Verification Page - Remove legal copy
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyRemovalOfLegalCopyOnMilitaryVerificationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyRemovalOfLegalCopyOnMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify that Legal copy should not be displayed on Millitary Verification Page");
		Reporter.log("Test Data : Any PAH/Full/ Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check Legal Copy for Veteran or Retiree user. | It should not be displayed.");
		Reporter.log("6. Check Active Duty radio button. | User should be able to select it.");
		Reporter.log("7. Check Legal Copy for Active Duty. | It should not be displayed.");
		Reporter.log("8. Check National Guard and Reserve radio button. | User should be able to select it.");
		Reporter.log("9. Check Legal Copy for National Guard and Reserve user. | It should not be displayed.");
		Reporter.log("10. Check Gold Star radio button. | User should be able to select it.");
		Reporter.log("11. Check Legal Copy for Gold Star user. | It should not be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifyLegalCopyForRegularUserOnMilitaryVerificationPage();
	}

	/**
	 * US413544 :: Military Verification Page - Remove legal copy
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyRemovalOfLegalCopyForBusinessUserOnMilitaryVerificationPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyRemovalOfLegalCopyForBusinessUserOnMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log(
				"Test Case  Name : Verify that Legal copy should not be displayed on Millitary Verification Page for Business User");
		Reporter.log("Test Data : Any Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check Legal Copy for Veteran or Retiree user. | It should not be displayed.");
		Reporter.log("6. Check Active Duty radio button. | User should be able to select it.");
		Reporter.log("7. Check Legal Copy for Active Duty. | It should not be displayed.");
		Reporter.log("8. Check National Guard and Reserve radio button. | User should be able to select it.");
		Reporter.log("9. Check Legal Copy for National Guard and Reserve user. | It should not be displayed.");
		Reporter.log("10. Check Gold Star radio button. | User should be able to select it.");
		Reporter.log("11. Check Legal Copy for Gold Star user. | It should not be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifyLegalCopyForBusinessUserOnMilitaryVerificationPage();
	}

	/**
	 * US407904 :: Military Verification - Contact PAH message
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyContactPAHMessageForStandardRestrictedAndNonBCUSersOnMilitaryVerificationPage(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyContactPAHMessageForStandardRestrictedAndNonBCUSersOnMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log(
				"Test Case  Name : Verify contact PAH message for Standard/Restricted/Non master Business User on Millitary Verification Page for Business User");
		Reporter.log("Test Data : Any Standard/Restricted/Non master Business Master User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log(
				"5. Check Message | Message You do not have permission to perform this verification. Please contact your Primary Account holder should be displayed.");
		Reporter.log("6. Check Active Duty radio button. | User should be able to select it.");
		Reporter.log("7. Check icon. | Icon should be displayed.");
		Reporter.log("8. Check OK CTA. | OK CTA should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage
				.checkContactToPAHMessageOnMilitaryVerificationPageWhenStandardOrRestrictedOrNonMasterLoggedIn();

	}

	/**
	 * US400866 :: Military verification - Upload received and under review
	 * status
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "notsuitableforregression")
	public void verifyUploadReceivedAndUnderReviewStatusScreenOnMilitaryVerificationPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyUploadReceivedAndUnderReviewStatusScreenOnMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log(
				"Test Case  Name : Verify upload received and under review status message on Millitary Verification Page for Business User");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master user who has submitted their verification and waiting for approval");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check title | Title Military verification should be displayed.");
		Reporter.log("6. Check black clock icon. | Icon should be displayed.");
		Reporter.log("7. Check subtitle | Subtitle Upload received and under review should be displayed.");
		Reporter.log(
				"8. Check Message | Message We received your documentation. You will receive an email from verify@sheerid.com within 24 hours regarding the status of your verification request. should be displayed.");
		Reporter.log(
				"9. Check Message | Message Note: if you don't receive an email, check your junk/spam folder.  For additional help, visit our FAQs or dial 611 from your T-Mobile phone. should be displayed.");
		Reporter.log(
				"10. Click on FAQ link. | It should be redirected to https://support.t-mobile.com/docs/DOC-37061#FAQs in new tab.");
		Reporter.log("11. Check name. | Users first and last name should be displayed.");
		Reporter.log("12. Check Email address. | Email address should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkUploadReceivedAndUnderReviewMessageOnMilitaryVerificationPage();

	}

	/**
	 * US400832 :: Military verification - Verified status
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyVerifiedStatusPageForMilitaryVerificationWhenUserHasMilitaryPlan(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("verifyVerifiedStatusPageForMilitaryVerification method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Verified Success page when user on Military page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master user who are on military plan and has submitted their verification and and it's successful.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check checkmark icon. | Icon should be displayed.");
		Reporter.log("6. Check title | Title Military verification should be displayed.");
		Reporter.log("7. Check subtitle | Subtitle Verified should be displayed.");
		Reporter.log(
				"8. Check Message | Message Congratulations, you have been verified as a military.  Thank you for your service. should be displayed.");
		Reporter.log("9. Check name. | Users first and last name should be displayed.");
		Reporter.log("10. Check Email address. | Approval date shoudl be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkVerifiedStatusPageWhenUserOnMilitaryPlan();

	}

	/**
	 * US400832 :: Military verification - Verified status
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyVerifiedStatusPageForMilitaryVerificationWhenUserNotOnAMilitaryPlan(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyVerifiedStatusPageForMilitaryVerificationWhenUserNotOnAMilitaryPlan method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Verified Success page when user not on Military plan");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master user not on military plan and has submitted their verification and and it's successful.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check checkmark icon. | Icon should be displayed.");
		Reporter.log("6. Check title | Title Military verification should be displayed.");
		Reporter.log("7. Check subtitle | Subtitle Verified should be displayed.");
		Reporter.log(
				"8. Check Message | Message Congratulations, you have been verified as a military.  Thank you for your service. should be displayed.");
		Reporter.log("9. Check name. | Users first and last name should be displayed.");
		Reporter.log("10. Check Email address. | Approval date shoudl be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkVerifiedStatusPageWhenUserNotOnMilitaryPlan();
	}

	/**
	 * US400926 :: Military verification - Too many failed attempts
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyTooManyFailedAttemptsForMilitaryVerificationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyTooManyFailedAttemptsForMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Too many failed attempts scenario on Military plan page");
		Reporter.log("Test Data : Any PAH/Full/Business Master who has submitted Verification 3 times and has failed.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check checkmark icon. | Icon should be displayed.");
		Reporter.log("6. Check title | Title Military verification should be displayed.");
		Reporter.log("7. Check subtitle | Subtitle Verified should be displayed.");
		Reporter.log(
				"8. Check Message | Message Congratulations, you have been verified as a military.  Thank you for your service. should be displayed.");
		Reporter.log("9. Check name. | Users first and last name should be displayed.");
		Reporter.log("10. Check Email address. | Approval date shoudl be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkTooManyFailedAttemptsPage();

	}

	/**
	 * US402323 : Military Verification - Document upload: first attempt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyHeaderAndMessageOnUploadDocumentPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyHeaderAndMessageOnUploadDocumentFirstAttemptPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Document Upload of First Attempt page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master who has submitted Verification for first time and need documents.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check title | Title Military verification should be displayed.");
		Reporter.log("6. Check subtitle | Subtitle Upload documentation should be displayed.");
		Reporter.log(
				"7. Check Message1 | Message We need documented proof of your current military status to verify eligibility. Confirm the information below is correct and then upload a document that confirms your current military status. should be displayed.");
		Reporter.log(
				"8. Check Message2 | Message If the information is incorrect or doesn't match what's on your documentation, please go back and edit your verification form or contact Customer Care for assistance. should be displayed.");
		Reporter.log("9. Check Edit verification form link | Link should be displayed.");
		Reporter.log("10. Check Contact Customer Care link | Link should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkHeaderAndMessageOnUploadDocumentPage();

	}

	/**
	 * US402323 : Military Verification - Document upload: first attempt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyConfirmYourInformationSectionOnDocumentUploadPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyConfirmYourInformationSectionOnDocumentUploadFirstAttemptPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Confirm your information section on Upload Document First Attempt page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master who has submitted Verification for first time and need documents.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");

		Reporter.log("5. Check Confirm your Information text. | Text should be displayed.");
		Reporter.log("6. Check Label for Name. | Label should be displayed.");
		Reporter.log("7. Check whether First name and last name dispalyed or not. | Names should be displayed.");
		Reporter.log(
				"8. Check Label DOB and whether DOB is dispalyed or not. | Label DOB and date should be displayed.");
		Reporter.log("9. Check whether label Status and it's value displayed or not. | Status should be displayed.");
		Reporter.log(
				"10. Check whether label Branch of service and it's value displayed or not. | Branch of service should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkConfirmYourInformationSectionOnUploadDocumentPage();

	}

	/**
	 * US402323 : Military Verification - Document upload: first attempt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyDocumentMustIncludeSectionOnDocumentUploadPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyDocumentMustIncludeSectionOnDocumentUploadFirstAttemptPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Document must include section on Upload Document First Attempt page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master who has submitted Verification for first time and need documents.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");

		Reporter.log("5. Check text Documents must include the following:. | Text should be displayed.");
		Reporter.log("6. Check checkmark and First Name. | It should be displayed.");
		Reporter.log("7. Check checkmark and Last Name. | It should be displayed.");
		Reporter.log("8. Check checkmark and DOB. | It should be displayed.");
		Reporter.log("9. Check checkmark and Current Military status. | It should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkDocumentMustIncludeSectionOnUploadDocumentPage();

	}

	/**
	 * US402323 : Military Verification - Document upload: first attempt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyNotificationAndMaximumFileSizeTextOnDocumentUploadPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyNotificationAndMaximumFileSizeTextOnDocumentUploadFirstAttemptPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Document must include section on Upload Document First Attempt page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master who has submitted Verification for first time and need documents.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");

		Reporter.log(
				"5. Check notification. | Notification Please omit or black out any sensitive information such as Social Security number, or Military ID# should be displayed.");
		Reporter.log(
				"6. Check acceptable file text. | Acceptable file formats: .bmp, .gif, .jpg, .png, and .pdf should be displayed.");
		Reporter.log("7. Check Max file Size: 10MB text. | Text should be displayed.");

		Reporter.log("8. Check CTA Choose file to upload. | CTA should be displayed.");
		Reporter.log("9. Check CTA Submit documentation. | CTA should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkNotificationAndFileSizeMessageOnUploadDocumentPage();

	}

	/**
	 * US402323 : Military Verification - Document upload: first attempt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyCTAsAndFAQTextOnDocumentUploadFirstAttemptPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyCTAsAndFAQTextOnDocumentUploadFirstAttemptPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Document must include section on Upload Document First Attempt page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master who has submitted Verification for first time and need documents.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");

		Reporter.log("5. Check CTA Choose file to upload. | CTA should be displayed.");
		Reporter.log("6. Check CTA Submit documentation. | CTA should be displayed.");
		Reporter.log("7. Check FAQ text. | Text should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkCTAsAndFAQTextOnUploadDocumentFirstAttemptPage();

	}

	/**
	 * US402323 : Military Verification - Document upload: first attempt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyRedirectionOfFAQLinkOnDocumentUploadFirstAttemptPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyRedirectionOfFAQLinkOnDocumentUploadFirstAttemptPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify Document must include section on Upload Document First Attempt page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master who has submitted Verification for first time and need documents.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");

		Reporter.log("7. Check FAQ text. | Text should be displayed.");
		Reporter.log("8. Click on FAQ link. | It should redirect to https://support.t-mobile.com/docs/DOC-37061#FAQs.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkFAQTextAndRedirectionOfFAQLinkOnUploadDocumentFirstAttemptPage();

	}

	/**
	 * US413824 :: Military Verification Page - Reset First Name and Last Name
	 * to BRP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyResetFirstNameAndLastNameForMilitaryVerificationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyResetFirstNameAndLastNameForMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify that reset first name and last name is working or not");
		Reporter.log("Test Data : Any PAH/Full/Business Master user and eligible for verification");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Read first name and last name. | User should be able to read it.");
		Reporter.log("6. Click Active duty radio button. | Active duty radio should be checked.");
		Reporter.log("7. Enter different first name and last name. | User able to enter names.");
		Reporter.log(
				"8. Click another radio button like Veteran/National Guard/Gold star | User should be able to click it.");
		Reporter.log("9. Check first name and last name. | New names should not be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkResetFunctionalityForFirstNameAndLastNameForMilitaryVerificationPage();

	}

	/**
	 * US417492 : Military Verification - Upload page: Edit verification form
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyEditVerificationLinkOnDocumentUploadFirstAttempt(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyEditVerificationLinkOnDocumentUploadFirstAttempt method called in MillitaryVerificationPageTest");
		Reporter.log(
				"Test Case  Name : Verify when user clicks on Edit Verification Link then whether it redirects to Verify page or not");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master who has submitted Verification for first time and need documents.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log(
				"5. Click on Edit Verification link on Upload Document page | User should be redirected to Verification page.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkRedirectionOfEditVerificationLinkOnMilitaryVerificationPage();

	}

	/**
	 * US393832 : Military Verification Page - Navigation bar options
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyNavigationOfBackArrowOnMilitaryVerificationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyNavigationFunctionalityOnMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify navigation functionality");
		Reporter.log("Test Data : Any PAH/Full/Business Master who has not submitted Verification");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log(
				"5. On MilitaryVerification page, click Back navigation icon. | User should be redirected to Profile page.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkRedirectionOfBackArrowOnMilitaryVerificationPage();

	}

	/**
	 * US393832 : Military Verification Page - Navigation bar options
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyNavigationOfBackArrowOnMilitaryVerificationVerifiedStatusPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyNavigationOfBackArrowOnMilitaryVerificationVerifiedStatusPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify navigation functionality on Military Verification Verified page");
		Reporter.log("Test Data : Any PAH/Full/Business Master who has submitted Verification and in verified status");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log(
				"5. On Military Verification status page, click Back navigation arrow. | User should be redirected to Profile page.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkRedirectionOfBackArrowOnMilitaryVerificationStatusPage();

	}

	/**
	 * US393832 : Military Verification Page - Navigation bar options
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "notsuitableforregression")
	public void verifyNavigationOfBackArrowOnMilitaryVerificationUnderReviewPendingStatusPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyNavigationOfBackArrowOnMilitaryVerificationUnderReviewPendingStatusPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify navigation functionality on Review pending under Review status page");
		Reporter.log("Test Data : Any PAH/Full/Business Master who has submitted Verification and in verified status");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log(
				"5. On Military Verification status page, click Back navigation arrow. | User should be redirected to Profile page.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);

		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkRedirectionOfBackArrowOnMilitaryVerificationStatusPage();
	}

	/**
	 * US393832 : Military Verification Page - Navigation bar options
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyNavigationOfBackArrowOnMilitaryVerificationMaxFailedAttemptsStatusPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyNavigationOfBackArrowOnMilitaryVerificationMaxFailedAttemptsStatusPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify navigation functionality on Review pending under Review status page");
		Reporter.log("Test Data : Any PAH/Full/Business Master who has submitted Verification and in verified status");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log(
				"5. On Military Verification status page, click Back navigation arrow. | User should be redirected to Profile page.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkRedirectionOfBackArrowOnMilitaryVerificationStatusPage();
	}

	/**
	 * US393832 : Military Verification Page - Navigation bar options
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyNavigationOfBackArrowOnMilitaryVerificationDocumentsNotAcceptedPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyNavigationOfBackArrowOnMilitaryVerificationDocumentsNotAcceptedPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify navigation functionality on Document not accepted page");
		Reporter.log("Test Data : Any PAH/Full/Business Master who's documents are not accepted.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log(
				"5. On Documents not accepted page, click Back navigation arrow. | User should be redirected to Profile page.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkRedirectionOfBackArrowOnMilitaryVerificationStatusPage();

	}

	/**
	 * US393832 : Military Verification Page - Navigation bar options
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyNavigationTMobileLogoOnMilitaryVerificationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyNavigationTMobileLogoOnMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify navigation of Tmobile logo");
		Reporter.log("Test Data : Any PAH/Full/Business Master who has not submitted Verification");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log(
				"5. On Military Verification status page, click T-Mobile logo. | User should be redirected to Home page.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);

		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkRedirectionOfTMobileLogoOnMilitaryVerificationPage();
	}

	/**
	 * US393832 : Military Verification Page - Navigation bar options
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyNavigationOfContactUsIconOnMilitaryVerificationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyNavigationOfContactUsIconOnMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify navigation of Contact Us logo");
		Reporter.log("Test Data : Any PAH/Full/Business Master who has not submitted Verification");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log(
				"5. On Military Verification status page, click Contact-us logo. | User should be redirected to Contact us page.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkRedirectionOfContactUsLogoOnMilitaryVerificationPage();
	}

	/**
	 * US401247 :: Military verification - Document not accepted status
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyDocumentNotAcceptedPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyDocumentNotAcceptedPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify document not accepted page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master who has submitted Verification but document not accepted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check icon. | Icon should be displayed.");
		Reporter.log("6. Check title | Title Military verification should be displayed.");
		Reporter.log("7. Check subtitle | Subtitle Document not accepted should be displayed.");
		Reporter.log(
				"8. Check Message1 | Message Thanks for your submission; unfortunately, it was not accepted as validation of status. To try again, follow the link below to upload a new document. should be displayed.");
		Reporter.log(
				"9. Check Message2 | Message If you feel the military status confirmation failure is due to an error, please contact SheerID for assistance should be displayed.");
		Reporter.log("10. Check Error message. | Error message should be displayed.");
		Reporter.log("11. Check CTA Upload new document. | CTA should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkDocumentNotAcceptedPage();

	}

	/**
	 * US401247 :: Military verification - Document not accepted status
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyInformationPreviouslySubmittedStatusPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyDocumentNotAcceptedPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify document not accepted page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master who has submitted Verification but document not accepted");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Check icon. | Icon should be displayed.");
		Reporter.log("6. Check title | Title Military verification should be displayed.");
		Reporter.log("7. Check subtitle | Subtitle Document not accepted should be displayed.");
		Reporter.log(
				"8. Check Message1 | Message Thanks for your submission; unfortunately, it was not accepted as validation of status. To try again, follow the link below to upload a new document. should be displayed.");
		Reporter.log(
				"9. Check Message2 | Message If you feel the military status confirmation failure is due to an error, please contact SheerID for assistance should be displayed.");
		Reporter.log("10. Check Error message. | Error message should be displayed.");
		Reporter.log("11. Check CTA Upload new document. | CTA should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkDocumentNotAcceptedPage();

	}

	/**
	 * US403947 :: Military Verification Page - Build calendar component
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyValuesFromMonthDropdownOnMilitaryVerificationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyValuesFromDayDropdownOnMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify values from Month Dropdown");
		Reporter.log("Test Data : Any PAH/Full/Business Master who has not submitted Verification");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Click on Month dropdown and check values. | Expected values should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.clickOnMillitaryStatusTabOnProfilePage();
		millitaryVerificationPage.checkAllOptionsFromMonthDropdown();

	}

	/**
	 * US403947 :: Military Verification Page - Build calendar component
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyValuesFromDayDropdownOnMilitaryVerificationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyValuesFromDayDropdownOnMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify values from Day Dropdown");
		Reporter.log("Test Data : Any PAH/Full/Business Master who has not submitted Verification");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Click on Day dropdown and check values. | Expected values should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.clickOnMillitaryStatusTabOnProfilePage();
		millitaryVerificationPage.checkAllOptionsFromDayDropdown();

	}

	/**
	 * US403947 :: Military Verification Page - Build calendar component
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyValuesFromYearDropdownOnMilitaryVerificationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyValuesFromYearDropdownOnMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Verify values from Year Dropdown");
		Reporter.log("Test Data : Any PAH/Full/Business Master who has not submitted Verification");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Click on Year dropdown and check values. | Expected values should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.clickOnMillitaryStatusTabOnProfilePage();
		// millitaryVerificationPage.checkAllOptionsFromYearDropdown();

	}

	/**
	 * US403947 :: Military Verification Page - Build calendar component
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyValuesFromBranchOfServiceDropdownOnMilitaryVerificationPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyValuesFromBranchOfServiceDropdownOnMilitaryVerificationPage method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Check values from Branch of service dropdown");
		Reporter.log("Test Data : Any PAH/Full/Business Master who has not submitted Verification");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Click on Branch of service dropdown and check values. | Expected values should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.clickOnMillitaryStatusTabOnProfilePage();
		millitaryVerificationPage.checkAllOptionsFromBranchOfServiceDropdown();

	}

	/**
	 * US412851 :: [Continued] Military Verification Page - Submit form
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyE2ESubmissionflowForVeteranOrRetireeUserWhenUserNotOnMilitaryPlan(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("verifySubmissionflowForVeteranOrRetireeUser method called in MillitaryVerificationPageTest");
		Reporter.log("Test Case  Name : Submit military verification flow and check details on Verified Success page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master user who are on military plan and has submitted their verification and and it's successful.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Select Branch of service | Able to select it.");
		Reporter.log("6. Select Discharge date | Able to select it.");
		Reporter.log("7. Read First name and last name | Able to read it.");
		Reporter.log("8. Click Submit CTA | Veirified status page should be displayed.");
		Reporter.log("9. Check Icon, messages at top. | Details should be displayed.");
		Reporter.log("10. Check Name | First name and last anme should be matched.");
		Reporter.log("11. Check Email address | Email address should be matched.");
		Reporter.log("12. Check Approval date | Approval date should be matched.");
		Reporter.log("13. Check Done CTA. | CTA should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifySubmissionFlowForVeteranOrRetiree();

	}

	/**
	 * US412851 :: [Continued] Military Verification Page - Submit form
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyE2ESubmissionflowForActiveDutyUserWhenUserNotOnMilitaryPlan(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyE2ESubmissionflowForActiveDutyeUserWhenUserNotOnMilitaryPlan method called in MillitaryVerificationPageTest");
		Reporter.log(
				"Test Case  Name : Submit military verification flow for Active Duty and check details on Verified Success page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master user who are on military plan and has submitted their verification and and it's successful.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Click On Active Duty radio button. | User should be able to select it.");
		Reporter.log("6. Select Branch of service | Able to select it.");
		Reporter.log("7. Read First name and last name | Able to read it.");
		Reporter.log("8. Enter Confirm Email address | Able to enter it.");
		Reporter.log("9. Click Submit CTA | Veirified status page should be displayed.");
		Reporter.log("10. Check Icon, messages at top. | Details should be displayed.");
		Reporter.log("11. Check Name | First name and last anme should be matched.");
		Reporter.log("12. Check Email address | Email address should be matched.");
		Reporter.log("13. Check Approval date | Approval date should be matched.");
		Reporter.log("14. Check Done CTA. | CTA should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifySubmissionFlowForActiveDuty();

	}

	/**
	 * US412851 :: [Continued] Military Verification Page - Submit form
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyE2ESubmissionflowForNationalGuardOrReserveUserWhenUserNotOnMilitaryPlan(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyE2ESubmissionflowForNationalGuardOrReserveUserWhenUserNotOnMilitaryPlan method called in MillitaryVerificationPageTest");
		Reporter.log(
				"Test Case  Name : Submit military verification flow for National Guard Or Reserve and check details on Verified Success page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master user who are on military plan and has submitted their verification and and it's successful.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Click On National Guard or Reserve radio button. | User should be able to select it.");
		Reporter.log("6. Select Branch of service | Able to select it.");
		Reporter.log("7. Read First name and last name | Able to read it.");
		Reporter.log("8. Enter Confirm Email address | Able to enter it.");
		Reporter.log("9. Click Submit CTA | Veirified status page should be displayed.");
		Reporter.log("10. Check Icon, messages at top. | Details should be displayed.");
		Reporter.log("11. Check Name | First name and last name should be matched.");
		Reporter.log("12. Check Email address | Email address should be matched.");
		Reporter.log("13. Check Approval date | Approval date should be matched.");
		Reporter.log("14. Check Done CTA. | CTA should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifySubmissionFlowForNationalGuardOrReserve();

	}

	/**
	 * US412851 :: [Continued] Military Verification Page - Submit form
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyE2ESubmissionflowForGoldStarUserWhenUserNotOnMilitaryPlan(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyE2ESubmissionflowForGoldStarUserWhenUserNotOnMilitaryPlan method called in MillitaryVerificationPageTest");
		Reporter.log(
				"Test Case  Name : Submit military verification flow for National Guard Or Reserve and check details on Verified Success page");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master user who are on military plan and has submitted their verification and and it's successful.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Click On Gold Star. | User should be able to select it.");
		Reporter.log("6. Select Relationship | Able to select it.");
		Reporter.log("7. Read First name and last name | Able to read it.");
		Reporter.log("8. Enter Confirm Email address | Able to enter it.");
		Reporter.log("9. Click Submit CTA | Veirified status page should be displayed.");
		Reporter.log("10. Check Icon, messages at top. | Details should be displayed.");
		Reporter.log("11. Check Name | First name and last name should be matched.");
		Reporter.log("12. Check Email address | Email address should be matched.");
		Reporter.log("13. Check Approval date | Approval date should be matched.");
		Reporter.log("14. Check Done CTA. | CTA should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifySubmissionFlowForGoldStar();

	}

	/**
	 * US402323 : Military Verification - Document upload: first attempt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyE2EFlowUntilUploadPageForVeteranOrRetireeUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyE2EFlowUntilUploadPageForVeteranOrRetireeUser method called in MillitaryVerificationPageTest");
		Reporter.log(
				"Test Case  Name : We submit for military verification flow but it goes to Upload page. We check details on the upload page.");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master user who are on military plan and has not submitted their verification yet.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Select Veteran or Retiree option. | User should be able to select it.");
		Reporter.log("6. Select Branch of service | Able to select it.");
		Reporter.log("7. Select Discharge date | Able to select it.");
		Reporter.log("8. Read First name and last name | Able to read it.");
		Reporter.log("9. Click Submit CTA | Upload page should be displayed.");
		Reporter.log("10. Check all details on Upload page. | Details should be matched.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkE2EFlowForUploadPageForVeteranOrRetiree();

	}

	/**
	 * US402323 : Military Verification - Document upload: first attempt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyE2EFlowUntilUploadPageForActiveDutyUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyE2EFlowUntilUploadPageForActiveDutyUser method called in MillitaryVerificationPageTest");
		Reporter.log(
				"Test Case  Name : We submit for military verification flow but it goes to Upload page. We check details on the upload page.");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master user who are on military plan and has not submitted their verification yet.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Select Active Duty option. | User should be able to select it.");
		Reporter.log("6. Select Branch of service | Able to select it.");
		Reporter.log("7. Select Discharge date | Able to select it.");
		Reporter.log("8. Read First name and last name | Able to read it.");
		Reporter.log("9. Click Submit CTA | Upload page should be displayed.");
		Reporter.log("10. Check all details on Upload page. | Details should be matched.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkE2EFlowForUploadPageForActiveDuty();

	}

	/**
	 * US402323 : Military Verification - Document upload: first attempt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyE2EFlowUntilUploadPageForNationalGuardOrReserveUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyE2EFlowUntilUploadPageForNationalGuardOrReserveUser method called in MillitaryVerificationPageTest");
		Reporter.log(
				"Test Case  Name : We submit for military verification flow but it goes to Upload page. We check details on the upload page.");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master user who are on military plan and has not submitted their verification yet.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Select Active Duty option. | User should be able to select it.");
		Reporter.log("6. Select Branch of service | Able to select it.");
		Reporter.log("7. Select Discharge date | Able to select it.");
		Reporter.log("8. Read First name and last name | Able to read it.");
		Reporter.log("9. Click Submit CTA | Upload page should be displayed.");
		Reporter.log("10. Check all details on Upload page. | Details should be matched.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkE2EFlowForUploadPageForNationalGuardOrReserve();

	}

	/**
	 * US402323 : Military Verification - Document upload: first attempt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyE2EFlowUntilUploadPageForGoldStarUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyE2EFlowUntilUploadPageForGoldStarUser method called in MillitaryVerificationPageTest");
		Reporter.log(
				"Test Case  Name : We submit for military verification flow but it goes to Upload page. We check details on the upload page.");
		Reporter.log(
				"Test Data : Any PAH/Full/Business Master user who are on military plan and has not submitted their verification yet.");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Click on Profile link. | Profile page should be displayed");
		Reporter.log("4. Click On Millitary tab | Millitary verification page should be displayed");
		Reporter.log("5. Select Gold star option. | User should be able to select it.");
		Reporter.log("6. Select Branch of service | Able to select it.");
		Reporter.log("7. Select Discharge date | Able to select it.");
		Reporter.log("8. Read First name and last name | Able to read it.");
		Reporter.log("9. Click Submit CTA | Upload page should be displayed.");
		Reporter.log("10. Check all details on Upload page. | Details should be matched.");
		Reporter.log("================================");

		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		// navigateToHomePage(myTmoData);
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.checkE2EFlowForUploadPageForGoldStar();

	}

}
