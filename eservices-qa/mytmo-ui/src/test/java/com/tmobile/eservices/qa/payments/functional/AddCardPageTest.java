/**
 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.AddBankPage;
import com.tmobile.eservices.qa.pages.payments.AddCardPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayLandingPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.pages.payments.UpdateCardPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class AddCardPageTest extends PaymentCommonLib {

	/**
	 * US283007 GLUE Light Reskin - PCM Add Card (Soft Goods) Spoke Page US262233
	 * Angular + GLUE - PCM Add Card (Soft Goods) (L3) US283627 GLUE Light Reskin -
	 * PCM Add Card (Hard Goods) Spoke Page
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular user eligible for card payments
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testGLRPCMAddCardSoftOrHardGoods(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"US283007, US262233, US283627 GLUE Light Reskin - PCM Add Card (Soft Goods, Hard Goods) Spoke Page");
		Reporter.log("Data Conditions - Regular user eligible for card payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | payment method page should be displayed");
		Reporter.log("Step 6: click on Add Card | Card Information page should be displayed");
		Reporter.log("Step 7: Verify all card page fields | All fields on Card page should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.verifyAddCardAllFieldsSoftOrHardGoods();
	}

	/**
	 *
	 * @param data
	 * @param myTmoData Checking AddCardPage Loading Not needed to be included as a
	 *                  part of regression
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DUPLICATE })
	public void testValidateAddCardPageLoading(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"US283007, US262233, US283627 GLUE Light Reskin - PCM Add Card (Soft Goods, Hard Goods) Spoke Page");
		Reporter.log("Data Conditions - Regular user eligible for card payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | payment method page should be displayed");
		Reporter.log("Step 6: click on Add Card | Card Information page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
	}

	/**
	 * /** //US214819
	 * 
	 * CCE Enhancement - Add Card (Hard Goods)
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular user eligible for card payments
	 *                  Flag based feature. Flag is turned off
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testCCEEnhancementAddCardHardGoodsForNewCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US214819	CCE Enhancement - Add Card (Hard Goods)");
		Reporter.log("Data Conditions - Regular user eligible for card payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Verify Balance amount on Home page | Balance amount should be displayed");
		Reporter.log("Step 4: Click on paybill button | One time payment page should be displayed");
		Reporter.log("Step 5: Click on add payment method  | Paymnet spoke page should be displayed");
		Reporter.log("Step 6: Click on add card icon  | Card spoke page should be displayed");
		Reporter.log(
				"Step 7: Enter vaild details and click on continue button | Agree and submit button should enabled");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.verifySavePaymentMethodCheckBox();
		addCardPage.fillCardInfo(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		// addCardPage.acceptCreditCardOverWriteDialogue();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
	}

	/**
	 * /** //US214819
	 * 
	 * CCE Enhancement - Add Card (Hard Goods) Data Conditions - Regular user
	 * eligible for card payments Flag based feature. Flag is turned off
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testCCEEnhancementAddCardHardGoodsForReplacingCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US214819	CCE Enhancement - Add Card (Hard Goods)");
		Reporter.log("Data Conditions - Regular user eligible for card payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Verify Balance amount on Home page | Balance amount should be displayed");
		Reporter.log("Step 4: Click on paybill button | One time payment page should be displayed");
		Reporter.log("Step 5: Click on add payment method  | Paymnet spoke page should be displayed");
		Reporter.log("Step 6: Click on add card icon  | Card spoke page should be displayed");
		Reporter.log(
				"Step 7: Enter vaild details and click on continue button |Agree and submit button should enabled");
		Reporter.log(
				"Step 8: Verify stored card is replaced with new card |Stored card should be replaced with new card");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.fillCardInfo(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		addCardPage.acceptCreditCardOverWriteDialogue();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(myTmoData.getPayment().getCardNumber());
	}

	/**
	 * 
	 * Verify that Save Payment Method option is present for PAH/Full account
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testAddCardTreatmentforSPMOptionPAH(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Conditions - PAH/FULL account. Eligible for card payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on pay bill  | One time payment page should be displayed");
		Reporter.log("Step 4: Click on Add Card | Add Card spoke page should be displayed");
		Reporter.log("Step 5: Verify save payment checkbox is displayed| Save payment checkbox should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.verifySavePaymentMethodCheckBox();
	}

	/**
	 * 
	 * Verify that Save Payment Method option is NOT present for Standard/Restricted
	 * account
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testAddCardTreatmentforSPMOptionStandard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Conditions - Standard/Restricted account. Eligible for card payments");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on pay bill  | One time payment page should be displayed");
		Reporter.log("Step 4: Click on Add Card | Add Card spoke page should be displayed");
		Reporter.log(
				"Step 5: Verify save payment checkbox is not displayed| Save payment checkbox should not be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.verifyNoSavePaymentMethodCheckBox();
	}

	/**
	 * TC_Regression_Desktop_Payment_invalid Credit card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"retired" })
	public void verifyInvalidCardErrorMessage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify Invalid Card ErrorMessage");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Click on Balance radio button and Credit card radio button | Credit card section should be displayed.");
		Reporter.log(
				"6. Enter Invalid Card Number and Click Next button | Error message for invalid card should be displayed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.fillinvalidCardDetails(myTmoData.getPayment());
		addCardPage.verifyInvalidCCErrorMsg();
	}

	/**
	 * US280693 Angular + GLUE - PCM Negative File / Expired Card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEPCMNegativeFileExpiredCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US280693	Angular + GLUE - PCM Negative File / Expired Card");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 6: verify expired card message | Expired Card error message should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.verifyInvalidCCErrorMsg();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void verifyErrorMsgsForCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : verify Error Messages for Card");

		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Click on Credit card radio button | Credit card section should be displayed.");
		Reporter.log(
				"6. Click Next button under credit card section | Error messages for invalid card details should be displayed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.clickContinueAddCard();
		addCardPage.verifyCardErrorMessage();
		addCardPage.verifyExpirationDateErrorMessage();
		addCardPage.verifyCVVErrorMessage();
		addCardPage.verifyZIPErrorMessage();

	}

	/**
	 * 06_Desktop_MC 2 Series BIN_Stored Payments for Eip balance Payoff using 2
	 * series
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void verifyErrorMessageFOrMCBINCARD(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : verify User Able TO Make Bill Payment Using new MC BIN CARD");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Fill invalid card | Error Message should display");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.fillCardInfo(myTmoData.getPayment());
		addCardPage.verifyMCBINCCErrorMsg();
	}

	/**
	 * frozen-monkeys US154339 [R] Payment Module H&S - Expiration Date Field Error
	 * Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testExpirationDateFieldErrorDisplay(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US154339 - Payment Module H&S - Expiration Date Field Error Display");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on add card | Add card spoke page should be displayed");
		Reporter.log("Step 4: enter expiration date wrong | expiration date Error should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.fillCardInfo(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		addCardPage.verifyExpirationdateErrorMsg("Enter a valid expiration date");
	}

	/**
	 * US355996: UI- ADD card validation
	 * 
	 * Container
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAddCardValidation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US355996: UI- ADD card validation");
		Reporter.log("Data Conditions - PAH or FULL user with invalid card ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill button | One time payment page should be displayed");
		Reporter.log("Step 4: Click on add payment method  | Paymnet spoke page should be displayed");
		Reporter.log(
				"Step 5: Enter Invalid Card Number and Click Next button | Error message for invalid card should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
	}

	/**
	 * US444936: Angular 6 PCM-Add card
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAddCardSpokePageSoftGoodsFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US444936-Angular 6 PCM-Add card");
		Reporter.log("Data Condition | Any PAH /Standard User with saved card ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on Add Card blade| Add Card Spoke Page should be displayed");
		Reporter.log("Step 7: verify 'Card information' Header| 'Card information' Header should be displayed");
		Reporter.log(
				"Step 8: verify Field Label text 'Name on card' ,Credit card number ,Expiration date,CVV ,Zip| Field Label text for all Fields  should be displayed");
		Reporter.log("Step 9: verify 'Continue' button| 'Continue' button should be displayed");
		Reporter.log("Step 10: verify 'Cancel' button| 'Cancel' button should be displayed");
		Reporter.log(
				"Step 11: click on  'What's this?' link under cvv | 'Find my CVV number'  modal should be displayed");
		Reporter.log("Step 12: verify  'Save Payment Method' toggle| 'Save Payment Method' toggle should be displayed");
		Reporter.log(
				"Step 13: verify toggle unchecked  by default when having stored card  | toggle should be unchecked  by default when having stored card");
		Reporter.log(
				"Step 14: select 'Save Payment Method' toggle   | 'You can only have 1 saved Credit/Debit card on file. Proceed if you want to replace your existing stored card with a new card.'  text should be displayed");
		Reporter.log(
				"Step 15: Mouse Hover on  'Save Payment Method' tooltip   | 'Save account (optional). Allow T-Mobile to store your payment method for future use by you and verified users to make a payment.'  tooltip text should be displayed");
		Reporter.log(
				"Step 16: Enter vaild details and click on continue button |  Payment Method blade should be displayed ");
		Reporter.log(
				"Step 17: Verify stored card is replaced with new card and selected by default|Stored card should be replaced with new card and selected by default");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddCardPage addCardPage = navigateToNewAddCardPage(myTmoData);
		SpokePage spokePage = new SpokePage(getDriver());
		addCardPage.addCardpageElements(PaymentConstants.Add_card_Save_Payment_Error_Msg,
				PaymentConstants.Add_card_Tool_Tip_Text);
		addCardPage.fillCardDetails(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		addCardPage.verifyReplaceModelHeader();
		addCardPage.clickReplaceModelContinueButton();
		/*
		 * OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		 * oneTimePaymentPage.verifyPageLoaded();
		 * oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(myTmoData.getPayment()
		 * .getCardNumber()); String
		 * storedPaymentNumber=oneTimePaymentPage.getStoredPaymentMethodNumber();
		 * oneTimePaymentPage.clickPaymentMethodBlade(); spokePage.verifyPageLoaded();
		 * spokePage.verifyDefaultSelectionofCardorBankInspokePage(storedPaymentNumber);
		 */

	}

	/**
	 * US444936: Angular 6 PCM-Add card
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAddCardSpokePageSavePaymentMethodTogglewithoutSavedCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US444936-Angular 6 PCM-Add card");
		Reporter.log("Data Condition | Any PAH /Standard User without saved card ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on Add Card blade| Add Card Spoke Page should be displayed");
		Reporter.log("Step 7: verify 'Card information' Header| 'Card information' Header should be displayed");
		Reporter.log("Step 8: verify  'Save Payment Method' toggle| 'Save Payment Method' toggle should be displayed");
		Reporter.log(
				"Step 9: verify toggle checked by default when no stored card)  |  toggle should be checked by default when no stored card)");

		Reporter.log("Actual Result:");

		AddCardPage addCardPage = navigateToNewAddCardPage(myTmoData);
		addCardPage.verifySavePaymentMethodCheckBox();
		addCardPage.verifySaveThisPaymentMethodCheckBoxIscheck();
	}

	/**
	 * US444936: Angular 6 PCM-Add card US483092: [Unfinished] Angular 6 PCM-Add
	 * card Integration
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAddCardSpokePageHardGoodsFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US444936-Angular 6 PCM-Add card");
		Reporter.log("Data Condition | Any PAH /Standard User when having billing address ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on Add Card blade| Add Card Spoke Page should be displayed");
		Reporter.log("Step 7: verify 'Card information' Header| 'Card information' Header should be displayed");
		Reporter.log(
				"Step 8: verify Field Label text 'Name on card' ,Credit card number ,Expiration date,CVV ,Zip| Field Label text for all Fields  should be displayed");
		Reporter.log("Step 9: verify 'Continue' button| 'Continue' button should be displayed");
		Reporter.log("Step 10: verify 'Cancel' button| 'Cancel' button should be displayed");
		Reporter.log(
				"Step 11: click on  'What's this?' link under cvv | 'Find my CVV number'  modal should be displayed");
		Reporter.log("Step 12: verify  'Save Payment Method' toggle| 'Save Payment Method' toggle should be displayed");
		Reporter.log(
				"Step 13: verify toggle unchecked  by default when having stored card  | toggle should be unchecked  by default when having stored card");
		Reporter.log(
				"Step 14: select 'Save Payment Method' toggle   | 'You can only have 1 saved Credit/Debit card on file. Proceed if you want to replace your existing stored card with a new card.'  text should be displayed");
		Reporter.log(
				"Step 15: Mouse Hover on  'Save Payment Method' tooltip   | 'Save account (optional). Allow T-Mobile to store your payment method for future use by you and verified users to make a payment.'  tooltip text should be displayed");
		Reporter.log(
				"Step 16: verify 'Use this billing address' checkbox |'Use this billing address' checkbox  should be displayed");
		Reporter.log(
				"Step 17: verify 'Use this billing address' selected by default |'Use this billing address' should be selected by default");
		Reporter.log(
				"Step 18: verify Billing address returned by the back-end    |Billing address  should be displayed");
		Reporter.log(
				"Step 19: uncheck  billing address checkbox  |Address line 1,Address line 2,City,State (Drop down list),Zip Fields should be displayed");
		Reporter.log(
				"Step 20: select   billing address checkbox  |All the Billing address fields should be suppressed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddCardPage addCardPage = navigateToNewAddCardPage(myTmoData);
		addCardPage.addCardpageElements(PaymentConstants.Add_card_Save_Payment_Error_Msg,
				PaymentConstants.Add_card_Tool_Tip_Text);
		addCardPage.verifyUseThisBillingAddressCheckbox();
		addCardPage.verifyUseThisBillingAddressCheckboxIsSelected();
		addCardPage.verifyUseThisBillingAddress();
		addCardPage.clickUseThisBillingAddressCheckbox();
		addCardPage.verifyBillingAddressFields();
		addCardPage.clickUseThisBillingAddressCheckbox();
		addCardPage.verifyAddressFieldsAreSuppressed();
	}

	/**
	 * US444936: Angular 6 PCM-Add card
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAddCardSpokePageHardGoodsFlowWithoutSavedBillingAddress(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US444936-Angular 6 PCM-Add card");
		Reporter.log("Data Condition | Any PAH /Standard User without having billing address ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on Add Card blade| Add Card Spoke Page should be displayed");
		Reporter.log("Step 7: verify 'Card information' Header| 'Card information' Header should be displayed");
		Reporter.log(
				"Step 8: verify 'Use this billing address' checkbox  ant text |'Use this billing address' checkbox  ant text  should be suppressed");
		Reporter.log(
				"Step 9: verify fields Address line 1,Address line 2,City,State (Drop down list),Zip Fields  |Address line 1,Address line 2,City,State (Drop down list),Zip Fields should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		AddCardPage addCardPage = navigateToNewAddCardPage(myTmoData);
		addCardPage.verifyUseThisBillingAddressCheckboxIsSuppressed();
		addCardPage.verifyUseThisBillingAddressTextIsSuppressed();
		addCardPage.verifyBillingAddressFields();

	}

	/**
	 * US444948: Angular 6 PCM- Replace Payment Method Modal UI and redirection.
	 * US483097: [Unfinished] Angular 6 PCM- Replace Payment Method Modal UI and
	 * redirection. Integration
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testReplacePaymentMethodModal(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US444948-Angular 6 PCM- Replace Payment Method Modal UI and redirection");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved card.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on Add Card blade| Add Card Spoke Page should be displayed");
		Reporter.log("Step 7: verify 'Card information' Header| 'Card information' Header should be displayed");
		Reporter.log(
				"Step 8: Enter vaild card details ,select save payment method toggle and click on continue CTA |  Replace Payment Method Modal should be displayed ");
		Reporter.log("Step 9: Verify 'Wait!' header on the modal|'Wait!' header on the modal should be displayed");
		Reporter.log(
				"Step 10: Verify 'Are you sure?' sub header on the modal|'Are you sure?' sub header on the modal should be displayed");
		Reporter.log(
				"Step 11: Verify payment method information | overwritten payment method ( ****XXXX is the last 4 of the payment method) on the modal should be displayed");
		Reporter.log(
				"Step 12: Verify text 'this card will replace your current [Card type] card on file.' | 'this card will replace your current [Card type] card on file.' text  should be displayed");
		Reporter.log("Step 13: Verify Cancel ,Continue CTAs  | Cancel ,Continue CTA should be displayed");
		Reporter.log("Step 14: click on Cancel CTA | add card page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		addCardPage.verifyAndSelectSavePaymentCheckbox();
		addCardPage.fillCardDetails(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		addCardPage.verifyReplaceModelElements(myTmoData.getPayment().getCardNumber());
		addCardPage.clickReplaceModelCancelButton();
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
	}

	/**
	 * US483099 [Unfinished] Angular 6 PCM- In session payment method logic
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testInSessionLogicForCardPayments(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US483099	[Unfinished] Angular 6 PCM- In session payment method logic");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved card.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on Add Card blade| Add Card Spoke Page should be displayed");
		Reporter.log("Step 7: verify 'Card information' Header| 'Card information' Header should be displayed");
		Reporter.log(
				"Step 8: Enter vaild card details ,select save payment method toggle and click on continue CTA |  Replace Payment Method Modal should be displayed ");
		Reporter.log("Step 9: Verify 'Wait!' header on the modal|'Wait!' header on the modal should be displayed");
		Reporter.log(
				"Step 10: Verify 'Are you sure?' sub header on the modal|'Are you sure?' sub header on the modal should be displayed");
		Reporter.log(
				"Step 11: Verify payment method information | overwritten payment method ( ****XXXX is the last 4 of the payment method) on the modal should be displayed");
		Reporter.log(
				"Step 12: Verify text 'this card will replace your current [Card type] card on file.' | 'this card will replace your current [Card type] card on file.' text  should be displayed");
		Reporter.log("Step 13: Verify Cancel ,Continue CTAs  | Cancel ,Continue CTA should be displayed");
		Reporter.log("Step 14: click on Cancel CTA | add card page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		SpokePage spokePage = navigateToNewSpokePage(myTmoData);
		spokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.fillCardDetails(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyNewOTPPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		spokePage.verifyNewPageLoaded();
		spokePage.clickInSessionBlade();
		addCardPage.verifyInSessionFieldValues(myTmoData.getPayment());
		addCardPage.enterCardNumber(PaymentConstants.CARD_NUMBER);
		addCardPage.enterCVVCode(myTmoData.getPayment().getCvv());
		addCardPage.clickContinueAddCard();
		oneTimePaymentPage.verifyNewOTPPageLoaded();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(PaymentConstants.CARD_NUMBER);
	}

	/**
	 * 
	 * 
	 * 
	 * Container
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testReplacementAlertforStoredCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condition | Any PAH /Standard User with stored bank account ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on Add Bank blade| Add Bank Spoke Page should be displayed");
		Reporter.log("Step 7: verify 'Bank information' Header| 'Bank information' Header should be displayed");
		Reporter.log(
				"Step 8: select 'Save Payment Method' toggle   | 'You can only have 1 saved checking account on file. Proceed if you want to replace your existing stored checking account with a new checking account.'  text should be displayed");
		Reporter.log(
				"Step 9: vreify replacement alert for stored bank|  replacement alert for stored bank should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AddBankPage addBankPage = navigateToAddBankPage(myTmoData);
		addBankPage.selectSavePaymentOptionCheckBox();
		addBankPage.verifySaveThisPaymentErrorMessage(
				"You can only have 1 saved checking account on file. Proceed if you want to replace your existing stored checking account with a new checking account");
	}

	/**
	 * US546091: 1.5 Add card add default Nickname field
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testNickNameFieldForAddCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condition | Any PAH /Standard/Full/Restricted User with Valid AddCard account ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on MakePayment | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on Add Card blade| Add Card Spoke Page should be displayed");
		Reporter.log("Step 7: verify 'NickName' Field| 'Nick Name'Field should be displayed");
		Reporter.log("Step 8: verify 'NickName' Field Header| 'Nick Name'Field Header should be displayed");
		Reporter.log("Step 9: verify 'NickName' Field toggle| 'Nick Name'Field toggle should be displayed");
		Reporter.log("Step 10: verify 'NickName' Field Surpressed| 'Nick Name'Field  should be surpressed");
		Reporter.log("Step 11: Select 'Save Payment Method'| 'Save Payment Method' should selected");
		Reporter.log("Step 12: verify 'NickName' Field Enabled| 'Nick Name'Field  should be Enabled");
		Reporter.log(
				"Step 13: Enter Card Info along with NickName Field| Card info should be appear in all the fileds ");
		Reporter.log("Step 14: Click on 'Continue' Button| 'Continue' Button is clicked successfully");
		Reporter.log("Step 15: Verify Onetimepayment Screen| One Time Payment Should be displayed");
		Reporter.log("Step 16: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log(
				"Step 17: Verify 'NickName' is updated in SpokePage   | 'NickName' should displayed in added card");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		if (System.getProperty("environment").contains("qata03")) {
			HomePage homepage = navigateToHomePage(myTmoData);
			homepage.verifyHomePage();
			getDriver().get(System.getProperty("environment") + "/onetimepayment");
			OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
			oneTimePaymentPage.verifyPageLoaded();
			oneTimePaymentPage.clickPaymentMethodBlade();
			SpokePage spokePage = new SpokePage(getDriver());
			spokePage.verifyPageLoaded();
			spokePage.verifyAddCardBlade();
			spokePage.clickAddCard();

		} else {
			AddCardPage addCardPage = navigateToAddCardPage(myTmoData);
		}
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyNickNameHeader();
		addCardPage.verifySetAsDefaultCheckBoxLabel();
		addCardPage.verifySetAsDefaultCheckBox();
		addCardPage.verifyAndSelectSavePaymentCheckbox();
		addCardPage.verifyNoNickNameandDefaultCheckBoxDisplay();
		addCardPage.verifyAndSelectSavePaymentCheckbox();
		addCardPage.fillCardDetails(myTmoData.getPayment());
		addCardPage.clickSetAsDefaultCheckBox();
		addCardPage.clickContinueAddCard();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyPageLoaded();
		if (spokePage.verifySavedPaymentMethodNickName(myTmoData.getNickName())) {
			Reporter.log("Saved Payment Method nickName is displayed");
		}
	}

	/**
	 * US546090: 6.0 Add card Nickname and default field.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testAngularVerifyNickNameFieldForAddCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condition | Any PAH /Standard/Full/Restricted User with Valid Addcard account ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on Profile | Profile page should be displayed");
		Reporter.log("Step 4. Click Billing and paymnets|Saved Payment method blade should be displayed");
		Reporter.log("Step 5: click on Saved payment method blade | Payment collection Page should be displayed");
		Reporter.log("Step 6: click on Add card blade| Add card Page should be displayed");
		Reporter.log("Step 7: verify 'NickName' Field| 'Nick Name'Field should be displayed");
		Reporter.log("Step 8: verify 'NickName' Field Header| 'Nick Name'Field Header should be displayed");
		Reporter.log("Step 9: verify 'NickName' Field toggle| 'Nick Name'Field toggle should be displayed");
		Reporter.log("Step 10: verify 'NickName' Field Surpressed| 'Nick Name'Field  should be surpressed");
		Reporter.log("Step 11: Select 'Save Payment Method'| 'Save Payment Method' should selected");
		Reporter.log("Step 12: verify 'NickName' Field Enabled| 'Nick Name'Field  should be Enabled");
		Reporter.log(
				"Step 13: Enter card Info along with NickName Field| card info should be appear in all the fileds ");
		Reporter.log("Step 14: Click on 'Continue' Button| 'Continue' Button is clicked successfully");
		Reporter.log("Step 15: Verify Payment Collection Screen|  Payment Collection screen Should be displayed");
		Reporter.log(
				"Step 17: Verify 'NickName' is updated in Payment Collections Page   | 'NickName' should displayed in added  card");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		if (System.getProperty("environment").contains("qata03")) {
			OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
			oneTimePaymentPage.clickPaymentMethodBlade();
		} else {
			NewAutopayLandingPage newAutopayPage = navigateToNewAutoPayLandingPage(myTmoData);
			newAutopayPage.clickAddPaymentmethod();
		}

		SpokePage paymentsCollectionPage = new SpokePage(getDriver());
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		// **Below two lines needs to be commented once defect fixed
		addCardPage.verifyNickNameSurpess();
		addCardPage.selectSavePaymentOptionCheckBox();
		addCardPage.verifyNickNameFieldElements();
		addCardPage.verifyNickNameSurpess();
		// below two lines needs to uncommented
		/*
		 * addCardPage.selectSavePaymentOptionCheckBox();
		 * addCardPage.verifyNickNameSurpess();
		 */
		addCardPage.fillCardDetails(myTmoData.getPayment());
		addCardPage.clickSetAsDefaultCheckBox();
		addCardPage.clickContinueAddCard();
		paymentsCollectionPage.verifyNewPageLoaded();
		if (paymentsCollectionPage.verifySavedPaymentMethodNickName(myTmoData.getNickName())) {
			Reporter.log("Saved Payment Method nickName is displayed");
		}
	}

	/**
	 * US598462 7.0 Add Card/edit card HTML implementation.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAddOrEditCardHtmlAngular(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US598462 7.0 Add Card/edit card HTML implementation.");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved card.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 7: verify Card details | Card Details should be displayed");
		Reporter.log("Step 8: verify Card number, Cvv | Card should be masked and Cvv should not be displayed");
		Reporter.log("Step 9: Update Card details and click save changes button| Spoke page should be displayed");
		Reporter.log("Step 10: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 11: verify updated Card details | Updated Card Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyNewPageLoaded();
		String storedCardNo = spokePage.verifyAndClickStoredCardAngular();
		UpdateCardPage editCardPage = new UpdateCardPage(getDriver());
		editCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		editCardPage.verifyEditCardPageElements(storedCardNo, "v6");
		String nick = editCardPage.updateCardDetailsAndSave(myTmoData.getPayment(), "v6");
		spokePage.verifyPageLoaded();
		spokePage.clickUpdatedStoredCard(storedCardNo);
		editCardPage.verifyUpdatedCardDetails(myTmoData.getPayment(), nick);
	}

}