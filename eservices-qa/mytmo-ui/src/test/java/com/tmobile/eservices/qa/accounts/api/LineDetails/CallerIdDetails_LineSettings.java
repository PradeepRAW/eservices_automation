package com.tmobile.eservices.qa.accounts.api.LineDetails;

import java.util.HashMap;
import java.util.Map;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;


public class CallerIdDetails_LineSettings extends AccountsApi {
	
	public Map<String, String> tokenMap;

	@Test( dataProvider = "byColumnName", enabled = true, groups = "CDCWW-944")
	public void testCallerIDNameFromFirstOrianAPI(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Tocken API call when Promoid is removed ");
		Reporter.log("Test data : Enter valid ban and phone number to get the response ");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testCallerIDNameFromFirstOrianAPI";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("LineSettings_CallerId.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getCallerIdV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi CallerID=new AccountsApi();
			String alltags[]={"name"};
			for(String tag:alltags)	{
				CallerID.checkjsontagitems(response, tag);
			}
			
			CallerID.checkexpectedvalues(response, "Name", apiTestData.getcallerId());
			
		} else {
			failAndLogResponse(response, operationName);
		}
	}
}
