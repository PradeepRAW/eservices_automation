/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.DeviceIntentPage;
import com.tmobile.eservices.qa.pages.payments.WalletPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class HomePageTest extends PaymentCommonLib{
	
	private final static Logger logger = LoggerFactory.getLogger(HomePageTest.class);
	
	/**
	 * Verify warning message is loaded if due date is within 5 days
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyWarningmessageDisplayed(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyWarningmessageDisplayed method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Verify warning message is loaded if due date is within 5 days");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify Warning message of Due Date | Due date message Should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");
		
		HomePage homePage = navigateToHomePage(myTmoData);
		Reporter.log("Home page is loaded");
		homePage.verifyBilldueDatedisplayed();
	}
	

	/**
	 * Verify the remaining days before AutoPay transaction posts is correct
	 * when disabling AutoPay
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyRemainingdaysIndueDate(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyRemainingdaysIndueDate method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Verify the remaining days before AutoPay transaction posts is correct when disabling AutoPay");
		Reporter.log("Data Conditions - Regular user. Should have past due/remaining days");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify Remaining days of Due date | Remaining days of Due date should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");
		
		HomePage homePage = navigateToHomePage(myTmoData);
		if (!(getDriver() instanceof AppiumDriver)) {
			homePage.verifyRemainingdaysDisplayed();
		}
	}

	/**
	 * US544277 CCS: Home | Quick Links | Payment Methods
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyHomoPagePaymentMethodsQuickLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US544277 CCS: Home | Quick Links | Payment Methods");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Check the Manage payments link visible on home page | Should be able to see the link");
		Reporter.log("5. click on the manage payment methods link | Should be able to click on the link");
		Reporter.log("6. verify whether it navigates to the  TMO-ID wallet pageWallet page |  Should be able to navigates to TMO-ID Wallet page");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");
		
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyManagePaymentsLink();
		homePage.clickManagePaymentsLink();
		WalletPage walletPage = new WalletPage(getDriver());
		walletPage.verifyPageLoaded();
	}
	
	/**
	 * US540247: AAL Eligibility | UI Work
	 * Validation Flow: Verify Add a line link in Home page as a PAH
	 * Validation Flow: Verify Add a line link in Home page when user role as "FULL"
	 * Validation Flow: Verify add a line link in home page for eligible customers
	 * Validation Flow: Verify add a line link in home page for ineligible users/customers
	 * Validation Flow: Verify Add a line link in Home page  when user role standard user/restricted
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void testVerifyAddaLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyWarningmessageDisplayed method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Verify the remaining days before AutoPay transaction posts is correct when disabling AutoPay");
		Reporter.log("Data Conditions - PAH,FULL and Eligible customers");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4.Check add a line link with image| Add a line link and image should be displayed");
		Reporter.log("5.Click add a line link| Device Intent page should be displayed/Not displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");
		
		HomePage homePage = navigateToHomePage(myTmoData);
		Reporter.log("Home page is loaded");
		if(homePage.verifyAddaLineLink()){
			homePage.clickAddaLineLink();
			DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
			deviceIntentPage.verifyPageLoaded();
		}
	}
	
	/**
	 * Verify scheduled AutoPay date shows correctly on home page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyScheduledautoPaydateHomepage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyScheduledautoPaydateHomepage method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Verify scheduled AutoPay date shows correctly on home page");
		Reporter.log("Data Conditions - PAH/FA user. AutoPay is ON");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify AutoPay Payment Schdeuled date | AutoPay Payment Schdeuled date is loaded");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyAutoPayscheduledDateHomepage();
	}

	
}
