package com.tmobile.eservices.qa.shop.npi;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.eos.CatalogApiV4;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.tmng.api.ProductsAPIV3;

import io.restassured.response.Response;

public class DcpApiValidatorTMNG extends ApiCommonLib {

	public Map<String, String> tokenMap;

	String failureReason;
	Map<String, String> actualSKUPromoID;
	Map<String, Map<String, String>> familyPromoIDInfo;
	Map<String, Map<String, String>> familyPromoNameInfo;
	List<DCPSkuVadidatorData> expectedSKUDataList;
	List<DCPSkuVadidatorData> actualSKUDataList;
	Set<String> familyIdSet;
	Map<String, String> actualSKUPromoName;

	@BeforeTest(alwaysRun = true)
	public void PrepareExpectedValues() {

		familyIdSet = new HashSet<String>();
		familyPromoIDInfo = new HashMap<String, Map<String, String>>();
		familyPromoNameInfo = new HashMap<String, Map<String, String>>();
		expectedSKUDataList = new LinkedList<DCPSkuVadidatorData>();
		actualSKUDataList = new LinkedList<DCPSkuVadidatorData>();
		actualSKUPromoID = new HashMap<String, String>();
		actualSKUPromoName = new HashMap<String, String>();

		String excelFileName;
		excelFileName = System.getProperty("user.dir") + "/src/test/resources/npidata/dcpSKUValidatorDataTMNG.xlsx";
		FileInputStream excelFile = null;
		Workbook workbook = null;
		try {
			excelFile = new FileInputStream(new File(excelFileName));
			if (excelFileName.contains("xlsx")) {
				workbook = new XSSFWorkbook(excelFile);
			} else {
				workbook = new HSSFWorkbook(excelFile);
			}
			Sheet dataTypeSheet = workbook.getSheetAt(0);

			Iterator<Row> iterator = dataTypeSheet.iterator();

			while (iterator.hasNext()) {

				Row currentRow = iterator.next();

				//if (currentRow.getCell(0) != null && currentRow.getCell(1) != null) {
				if (currentRow.getCell(0) != null) {

					
					
					
					/*String sku = StringUtils.isNoneEmpty(currentRow.getCell(0).getStringCellValue())?currentRow.getCell(0).getStringCellValue():"";
					String familyId = currentRow.getCell(1).getStringCellValue();
					String modelName = currentRow.getCell(2).getStringCellValue();
					String manufacturer = currentRow.getCell(3).getStringCellValue();
					String memory = currentRow.getCell(4).getStringCellValue();
					String color = currentRow.getCell(5).getStringCellValue();
					String availability = currentRow.getCell(6).getStringCellValue();*/
					
					String sku = getCurrRowCellVal(currentRow.getCell(0));
					String familyId = getCurrRowCellVal(currentRow.getCell(1));
					String modelName = getCurrRowCellVal(currentRow.getCell(2));
					String manufacturer = getCurrRowCellVal(currentRow.getCell(3));
					String memory = getCurrRowCellVal(currentRow.getCell(4));
					String color = getCurrRowCellVal(currentRow.getCell(5));
					String availability = getCurrRowCellVal(currentRow.getCell(6));
					
					if (StringUtils.isNoneEmpty(sku)) {
						if (!sku.equalsIgnoreCase("SKU")) {

							DCPSkuVadidatorData dcpSkuValidator = new DCPSkuVadidatorData();
							dcpSkuValidator.setSkuNum(sku);
							dcpSkuValidator.setFamilyId(familyId);
							dcpSkuValidator.setManfactType(manufacturer);
							dcpSkuValidator.setModelName(modelName);
							dcpSkuValidator.setColor(color);
							dcpSkuValidator.setMemory(memory);
							dcpSkuValidator.setAvailableStatus(availability);
							expectedSKUDataList.add(dcpSkuValidator);
							familyIdSet.add(familyId);

						}
					}
				}
			}

		} catch (Exception e) {

		} finally {
			if (excelFile != null) {
				try {
					excelFile.close();
					workbook.close();
				} catch (IOException io) {
					Reporter.log("Failed to close dcpSKUValidatorDataTMNG.xlsx file");
				}
			}

		}
	}


	public String getCurrRowCellVal(Cell cell){
		String cellVal="";
		if(null!=cell){
			cellVal = StringUtils.isNoneEmpty(cell.getStringCellValue())?cell.getStringCellValue():"";
		}else{
			cellVal="";
		}
        return cellVal.trim();
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "TMNGnpi" })
	public void prepareProductsResponse(ApiTestData apiTestData) throws Exception {

		Map<String, String> tokenMap = new HashMap<String, String>();
		Response response = null;
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("correlationid", getValForCorrelation());

		ProductsAPIV3 prodapiV3 = new ProductsAPIV3();

		response = prodapiV3.retrieveProductsUsingPOST("ALL");

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			//System.out.println("tmng products : "+ response.body().asString());
			//logLargeResponse(response, "TMNG Product Details");
		} else {
			failAndLogResponse(response, "TMNG Product Details");
		}

		prepareActualList(response);

	}

	//@Test(dataProvider = "byColumnName", priority = 1, enabled = true, groups = { "npi" })
	public void prepareProductsPromotionsResponse(ApiTestData apiTestData) throws Exception {

		String requestBody = "";
		Response response = null;

		try {
			String operationName = "Product Promotion Details";
			CatalogApiV4 catalogApi = new CatalogApiV4();
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			for (Object object : familyIdSet) {

				String familyID = (String) object;
				requestBody = "{\r\n  \"familyId\": [ \"" + familyID + "\"  ],  \"promotionType\": \"ALL\"\r\n}";
				//logLargeRequest(requestBody, operationName);

				response = catalogApi.retrievePromotionsAtFamilyLevel(apiTestData, requestBody, tokenMap);

				if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
					//logLargeResponse(response, operationName);
				} else {
					failAndLogResponse(response, operationName);
				}

				prepareActualPromotionsList(response, familyID);

				mapPromoWithFamilyID(familyID);

			}

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}

		prepareActualList(response);

	}

	public void mapPromoWithFamilyID(String familyID) {

		familyPromoIDInfo.put(familyID, actualSKUPromoID);
		familyPromoNameInfo.put(familyID, actualSKUPromoName);

	}

	public void prepareActualPromotionsList(Response response, String familyID) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode productPromoInfoNode = jsonNode.path("productPromoInfo");
			if (!productPromoInfoNode.isMissingNode() && productPromoInfoNode.isArray()) {
				for (int i = 0; i < productPromoInfoNode.size(); i++) {
					JsonNode productPromoInfo = productPromoInfoNode.get(i);
					if (productPromoInfo.has("data")) {
						JsonNode parentNode = productPromoInfo.path("data");
						if (!parentNode.isMissingNode() && parentNode.isArray()) {
							for (int j = 0; j < parentNode.size(); j++) {
								JsonNode node = parentNode.get(j);
								if (node.isObject()) {

									String skuNum = getPathVal(node, "skuNumber");
									if (node.has("PromotionTypes")) {
										JsonNode promoTypeNode = node.path("PromotionTypes");
										if (!promoTypeNode.isMissingNode() && promoTypeNode.isArray()) {
											for (int k = 0; k < promoTypeNode.size(); k++) {
												JsonNode unitNode = promoTypeNode.get(k);

												if (unitNode.isObject()) {
													if (unitNode.has("promotions")) {
														JsonNode promotionsNode = unitNode.path("promotions");
														if (!promotionsNode.isMissingNode()
																&& promotionsNode.isArray()) {
															for (int l = 0; l < promotionsNode.size(); l++) {
																JsonNode unitPromoNode = promotionsNode.get(l);

																if (unitPromoNode.isObject()) {
																	String promoId = getPathVal(unitPromoNode,
																			"promoId");
																	String promoName = getPathVal(unitPromoNode,
																			"promoName");

																	actualSKUPromoID.put(skuNum, promoId);
																	actualSKUPromoName.put(skuNum, promoName);

																}
															}

														}
													}
												}

											}
										}

									}
								}

							}
						}
					}
				}
			}

		} catch (Exception e) {
			Reporter.log("Exception occured" + e);
		}

	}

	public void prepareActualList(Response response) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode() && jsonNode.has("products")) {
				JsonNode parentNode = jsonNode.path("products");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {
						DCPSkuVadidatorData dcpSkuValidator=null;	
						JsonNode node = parentNode.get(i);
						if (node.isObject()) {

							/*dcpSkuValidator.setFamilyId(node.path("familyCode").asText());
							dcpSkuValidator.setManfactType(node.path("manufacturer").asText());*/
														
							JsonNode skuNode = node.path("skus");
							if (!skuNode.isMissingNode() && skuNode.isArray()) {
								for (int j = 0; j < skuNode.size(); j++) {
									dcpSkuValidator = new DCPSkuVadidatorData();
									dcpSkuValidator.setFamilyId(node.path("familyCode").asText());
									dcpSkuValidator.setManfactType(node.path("manufacturer").asText());
									dcpSkuValidator.setSkuNum(skuNode.get(j).path("skuCode").asText());
									dcpSkuValidator.setMemory(skuNode.get(j).path("memory").asText());
									dcpSkuValidator.setColor(skuNode.get(j).path("color").asText());
								  JsonNode availabilityNode = skuNode.get(j).path("availability");
								  if (!availabilityNode.isMissingNode() || availabilityNode.isArray()) {
									  dcpSkuValidator.setAvailableStatus(availabilityNode.path("availabilityStatus").asText());
								  }
								  actualSKUDataList.add(dcpSkuValidator);
								}
							}
						}
						//actualSKUDataList.add(dcpSkuValidator);
					}
				}
			}

		} catch (Exception e) {
			Reporter.log("Exception occured" + e);
		}

	}

	@Test(dataProvider = "byColumnName", dependsOnMethods = { "prepareProductsResponse" }, enabled = true, groups = {
			"TMNGnpi" })
	public void productsResultReport(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Reporter.log("validate Family Name");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the Prodcuts list should get from service response");
		Reporter.log("Step 2: Verify expected modelName should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// String matchCriteria = "Family ID";

		prepareResultReport();

	}

	//@Test(dataProvider = "byColumnName", dependsOnMethods = {"prepareProductsPromotionsResponse" }, enabled = true, groups = { "npi" })
	public void verifyPromotionsForNPISkus(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Reporter.log("validate Product Status for Device Launce Sku's are in Response");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the Products list should get from service response");
		Reporter.log("Step 2: Verify expected SKU's Promotions should be in the response");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		preparePromotionsResultReport(familyPromoIDInfo, familyPromoNameInfo);
	}

	public void prepareResultReport() {

		ListIterator<DCPSkuVadidatorData> excpectedDCPListIter = expectedSKUDataList.listIterator();

		/*Reporter.log(
				"<Table><th>Expected SKU</th><th>Actual SKU</th><th>Expected Family</th><th>Actual Family</th><th>Expected ModelName</th><th>Actual ModelName</th><th>Expected Color</th><th>Actual Color</th><th>Expected Manufacturer</th><th>Actual Manufacturer</th><th>Expected Memory</th><th>Actual Memory</th><th>Expected Availability</th><th>Actual Availability</th>");
*/
		Reporter.log(
				"<Table><th>Expected SKU</th><th>Actual SKU</th><th>Expected Family</th><th>Actual Family</th><th>Expected Color</th><th>Actual Color</th><th>Expected Manufacturer</th><th>Actual Manufacturer</th><th>Expected Memory</th><th>Actual Memory</th><th>Expected Availability</th><th>Actual Availability</th>");

		while (excpectedDCPListIter.hasNext()) {

			DCPSkuVadidatorData expectedDcpValidatorData = excpectedDCPListIter.next();
			ListIterator<DCPSkuVadidatorData> actualDCPListIter = actualSKUDataList.listIterator();
			boolean skuFound = false;
			while (actualDCPListIter.hasNext()) {
				String skuResultMsg;
				String familyResultMsg, familyBgColor;
				String manFactResultMsg, manFactBgColor;
				String memoryResultMsg, memoryBgColor;
				String colorResultMsg, colorBbColor;
				String modelResultMsg, modelBgColor;
				String availResultMsg, availBgColor;

				DCPSkuVadidatorData actualDcpValidatorData = actualDCPListIter.next();
				if (StringUtils.isNoneEmpty(expectedDcpValidatorData.getSkuNum())
						&& expectedDcpValidatorData.getSkuNum().equalsIgnoreCase(actualDcpValidatorData.getSkuNum())) {
					skuFound = true;
					familyBgColor = getResult(expectedDcpValidatorData.getFamilyId(),
							actualDcpValidatorData.getFamilyId());
					manFactBgColor = getResult(expectedDcpValidatorData.getManfactType(),
							actualDcpValidatorData.getManfactType());
					memoryBgColor = getResult(expectedDcpValidatorData.getMemory(), actualDcpValidatorData.getMemory());
					colorBbColor = getResult(expectedDcpValidatorData.getColor(), actualDcpValidatorData.getColor());
					/*modelBgColor = getResult(expectedDcpValidatorData.getModelName(),
							actualDcpValidatorData.getModelName());*/
					availBgColor = getResult(expectedDcpValidatorData.getAvailableStatus(),
							actualDcpValidatorData.getAvailableStatus());

					skuResultMsg = "<td bgcolor=\"#00FF00\">" + actualDcpValidatorData.getSkuNum() + "</td>";
					familyResultMsg = "<td bgcolor=\"" + familyBgColor + "\">" + actualDcpValidatorData.getFamilyId()
							+ "</td>";
					manFactResultMsg = "<td bgcolor=\"" + manFactBgColor + "\">"
							+ actualDcpValidatorData.getManfactType() + "</td>";
					memoryResultMsg = "<td bgcolor=\"" + memoryBgColor + "\">" + actualDcpValidatorData.getMemory()
							+ "</td>";
					colorResultMsg = "<td bgcolor=\"" + colorBbColor + "\">" + actualDcpValidatorData.getColor()
							+ "</td>";
					/*modelResultMsg = "<td bgcolor=\"" + modelBgColor + "\">" + actualDcpValidatorData.getModelName()
							+ "</td>";*/
					availResultMsg = "<td bgcolor=\"" + availBgColor + "\">"
							+ actualDcpValidatorData.getAvailableStatus() + "</td>";

					Reporter.log("<tr><td>" + expectedDcpValidatorData.getSkuNum() + "</td>" + skuResultMsg + "<td>"
							+ expectedDcpValidatorData.getFamilyId() + "</td>" + familyResultMsg + "<td>"							
							+ expectedDcpValidatorData.getColor() + "</td>" + colorResultMsg + "<td>"
							+ expectedDcpValidatorData.getManfactType() + "</td>" + manFactResultMsg + "<td>"
							+ expectedDcpValidatorData.getMemory() + "</td>" + memoryResultMsg + "<td>"
							+ expectedDcpValidatorData.getAvailableStatus() + "</td>" + availResultMsg);
					break;
				}

			}
			if (!skuFound) {
				Reporter.log("<tr><td>" + expectedDcpValidatorData.getSkuNum()
						+ "</td><td colspan=\"13\" bgcolor=\"#FF0000\">" + " Expected SKU Not Found</td>");
			}

		}

		Reporter.log("</Table>");

	}

	private String getResult(String expected, String actual) {
		String bgRed = "#FF0000";
		String bgGreen = "#00FF00";
		if (expected.equalsIgnoreCase(actual)) {
			return bgGreen;

		} else {
			return bgRed;
		}
	}

	public void preparePromotionsResultReport(Map<String, Map<String, String>> familyPromoIDMap,
			Map<String, Map<String, String>> familyPromoNameMap) {

		Iterator<Map.Entry<String, Map<String, String>>> actfamilyPromoIDMapIterator = familyPromoIDMap.entrySet()
				.iterator();
		Iterator<Map.Entry<String, Map<String, String>>> actfamilyPromoNameMapIterator = familyPromoNameMap.entrySet()
				.iterator();
		String familyIdFromPromoIDMap;
		String familyIdFromPromoNameMap;
		Map<String, String> skuPromoIdMap;
		Map<String, String> skuPromoNameMap;

		Reporter.log(
				"<Table><th>Actual Family ID</th><th>Actual SKU(s)</th><th>Actual PromoID(s)</th><th>Actual PromoName(s)</th>");
		while (actfamilyPromoIDMapIterator.hasNext()) {

			Map.Entry<String, Map<String, String>> actualFamilyPromoIDEntry = actfamilyPromoIDMapIterator.next();
			Map.Entry<String, Map<String, String>> actualFamilyPromoNameEntry = actfamilyPromoNameMapIterator.next();

			familyIdFromPromoIDMap = actualFamilyPromoIDEntry.getKey();
			familyIdFromPromoNameMap = actualFamilyPromoNameEntry.getKey();

			skuPromoIdMap = new HashMap<String, String>();
			skuPromoIdMap = actualFamilyPromoIDEntry.getValue();

			if (skuPromoIdMap.isEmpty()) {
				Reporter.log("<tr><td>" + familyIdFromPromoIDMap
						+ "</td><td colspan=\"3\" bgcolor=\"#FF0000\">Promotion Data does not exist for this Family Id</td></tr>");
			}

			skuPromoNameMap = new HashMap<String, String>();
			skuPromoNameMap = actualFamilyPromoNameEntry.getValue();

			if (familyIdFromPromoIDMap.equalsIgnoreCase(familyIdFromPromoNameMap)) {

				Iterator<Map.Entry<String, String>> actSkuPromoIdMapIterator = skuPromoIdMap.entrySet().iterator();
				Iterator<Map.Entry<String, String>> actSkuPromoNameMapIterator = skuPromoNameMap.entrySet().iterator();
				while (actSkuPromoIdMapIterator.hasNext()) {

					Map.Entry<String, String> actPromoIdMapEntry = actSkuPromoIdMapIterator.next();
					Map.Entry<String, String> actPromoNameMapEntry = actSkuPromoNameMapIterator.next();

					if (actPromoIdMapEntry.getKey().equalsIgnoreCase(actPromoNameMapEntry.getKey())) {
						String promoID;
						String promoName;
						if (StringUtils.isNoneEmpty(actPromoIdMapEntry.getValue())) {
							promoID = "<td>" + actPromoIdMapEntry.getValue() + "</td>";
						} else {
							promoID = "<td bgcolor=\"#FF0000\"> Promo ID Not Available </td>";
						}
						if (StringUtils.isNoneEmpty(actPromoNameMapEntry.getValue())) {
							promoName = "<td>" + actPromoNameMapEntry.getValue() + "</td>";
						} else {
							promoName = "<td bgcolor=\"#FF0000\"> Promo Name Not Available </td>";
						}

						Reporter.log("<tr><td>" + familyIdFromPromoIDMap + "</td><td>" + actPromoIdMapEntry.getKey()
								+ "</td>" + promoID + promoName);
					}
				}
			}
		}

		Reporter.log("</Table>");

	}

}
