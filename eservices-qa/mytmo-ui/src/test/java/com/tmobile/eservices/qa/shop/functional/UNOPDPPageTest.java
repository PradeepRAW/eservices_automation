package com.tmobile.eservices.qa.shop.functional;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.shop.*;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class UNOPDPPageTest extends ShopCommonLib {

	/**
	 * CDCSM-131 :US634740 - As an authenticated customer, I want to view the
	 * authenticated version of the PLP/PDP pages, so that I can go through the
	 * authenticated experience. 
	 * TC- 936 : Desktop: Validate that the authenticated user is able to view Banner 
	 * TC- 938 : Desktop: Validate that the authenticated user is able to view the PDP
	 * TC- 941 : Desktop: Validate that the  authenticated user is able to view the legal on PDP
	 * TC- 942 : Desktop: Validate that the  authenticated user is able to view the right PDP URL
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "uno" })
	public void testUserNavigatedTOUNOPDPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on See All Phones on shop page | Application Navigated to UNO PLP page");
		Reporter.log("4. Select device | UNOPDP page should be displayed");
		Reporter.log("5. Verify Legal text under the device | Legal text should be displayed under the device");
		Reporter.log("6. Verify Banner | Banner should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		UNOPDPPage unoPDPPage = navigateToUNOPDPPageBySeeAllPhones(myTmoData);
		unoPDPPage.verifyLegalText();
		unoPDPPage.verifyLegalTextFooter();
		unoPDPPage.verifyDeviceImageAtPDP();
	}
	

	/**
	 * CDCSM-131 :US634740 - As an authenticated customer, I want to view the
	 * authenticated version of the PLP/PDP pages, so that I can go through the
	 * authenticated experience. 
	 * TC- 939 : Desktop: Validate that the authenticated user is able to view the pricing on PDP based on the CR
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "uno" })
	public void testDevicePriceInUNOPDPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on See All Phones on shop page | Application Navigated to UNO PLP page");
		Reporter.log("4. Save first device FRP price in V1 | Device FRP price should be saved in V1");
		Reporter.log("5. Select device | UNOPDP page should be displayed");
		Reporter.log("6. Verify device FRP price | Device FRP price should be displayed");
		Reporter.log("7. Save device FRP price in V2 | Device FRP price should be saved in V2");
		Reporter.log("8. Compare device FRP price | Device FRP price should be same");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		UNOPLPPage unoPLPPage = navigateToUNOPLPPageBySeeAllPhones(myTmoData);
		String v1 = unoPLPPage.getDeviceFRP();
		unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		String v2 = unoPDPPage.getDeviceFRP();
		unoPDPPage.compareTwoStringValues(v1, v2);
	}
	
	/**
	 * CDCSM-131 :US634740 - As an authenticated customer, I want to view the
	 * authenticated version of the PLP/PDP pages, so that I can go through the
	 * authenticated experience. 
	 * TC- 940 : Desktop: Validate that the authenticated user is able to view the promotion on PDP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "uno" })
	public void testPromotionTextForDevices(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on See All Phones on shop page | Application Navigated to UNO PLP page");
		Reporter.log("4. Verify Promotion text for devices | Promotion text should be displayed");
		Reporter.log("5. Select device | UNOPDP page should be displayed");
		Reporter.log("6. Verify promotion text | Promotion text should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		UNOPLPPage unoPLPPage = navigateToUNOPLPPageBySeeAllPhones(myTmoData);
		unoPLPPage.verifyPromotionTextonPLP();
		unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyPromotionText();
	}
	
	/**
	 * CDCSM-131 :US634740 - As an authenticated customer, I want to view the
	 * authenticated version of the PLP/PDP pages, so that I can go through the
	 * authenticated experience. 
	 * TC- 944 : Desktop: Validate that authenticated user is navigated to UNO PDP page from Featured device 	
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "uno" })
	public void testUserNavigatedTOUNOPDPPageFromFeaturedDevice(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Select device from featured devices | UNOPDP page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		UNOPDPPage unoPDPPage = navigateToUNOPDPPageByFeatureDevices(myTmoData, myTmoData.getDeviceName());		
	}
	
	/**
	 * CDCSM-146 :US572786 - MyTMO Release - As a Product Manager, 
	 * I want to redirect uncookied/unauthenticated customers back to the PDP page, 
	 * so that they can view contextual information based on their CRP.
	 * TC- 1002 : Desktop:Validate that an uncookied or unauthenticated customer navigates back to PDP page for phones that are available
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testUserNavigatedTOUNOPDPPageThroughDeepLink(ControlTestData data, MyTmoData myTmoData, TMNGData tMNGData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the TMO application | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Phones link | All phones should be displayed ");
		Reporter.log("3. Select device | UNOPDP page should be displayed");
		Reporter.log("4. Click 'Add to cart' CTA | Select User modal should be displayed");
		Reporter.log("5. Select Existing user option | User should navigate to MyTmo login page");		
		Reporter.log("6. Enter valid User-id,Password and click login button | UnoPdp page should be displayed ");
		Reporter.log("7. Verify UpgradeCTA | Upgrade CTA should be enabled");
		Reporter.log("8. Verify Add a Line CTA | Add a Line CTA should be enabled");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.verifySelectUserModal();
		phonesPdpPage.clickExistingUserOptionInSelectUserModal();	
		
		LoginPage loginPage = new LoginPage(getDriver());
//		loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(), myTmoData.getPassword());
		performLogin(myTmoData, loginPage);
		
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyUpGradeCTAEnabled();
		unoPDPPage.verifyAddaLineCTAEnabled();		
	}
	
	/**
	 * CDCSM-146 :US572786 - MyTMO Release - As a Product Manager, 
	 * I want to redirect uncookied/unauthenticated customers back to the PDP page, 
	 * so that they can view contextual information based on their CRP.
	 * TC- 1013 : Desktop:Validate that an uncookied or unauthenticated customer gets the Upgrade pricing when navigating back to UNO PDP
	 * TC- 1015 : Desktop:Validate that an uncookied or unauthenticated customer gets the Upgrade Legal when navigating back to UNO PDP
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testDevicePriceAndLegalTextInUNOPDPPageThroughDeepLink(ControlTestData data, MyTmoData myTmoData, TMNGData tMNGData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the TMO application | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Phones link | All phones should be displayed ");
		Reporter.log("3. Select device | UNOPDP page should be displayed");
		Reporter.log("4. Click 'Add to cart' CTA | Select User modal should be displayed");
		Reporter.log("5. Select Existing user option | User should navigate to MyTmo login page");		
		Reporter.log("6. Enter valid User-id,Password and click login button | UnoPdp page should be displayed ");
		Reporter.log("7. Verify UpgradeCTA | Upgrade CTA should be enabled");
		Reporter.log("8. Verify Add a Line CTA | Add a Line CTA should be enabled");
		Reporter.log("9. Verify Device price | Device price should be displayed");
		Reporter.log("10. Verify Legaltext | Legal text should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.verifySelectUserModal();
		phonesPdpPage.clickExistingUserOptionInSelectUserModal();
		
		LoginPage loginPage = new LoginPage(getDriver());
		//loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(), myTmoData.getPassword());
		performLogin(myTmoData, loginPage);
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyUpGradeCTAEnabled();
		unoPDPPage.verifyAddaLineCTAEnabled();
		unoPDPPage.verifyDeviceFRPPrice();
		unoPDPPage.verifyLegalText();
	}
	
	/**
	 * CDCSM-146 :US572786 - MyTMO Release - As a Product Manager, 
	 * I want to redirect uncookied/unauthenticated customers back to the PDP page, 
	 * so that they can view contextual information based on their CRP.
	 * TC- 1016 : Desktop:Validate that an uncookied or unauthenticated customer navigates to consolidated charges page to continue to AAL flow
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testUserNavigatedToConsolidatedRatePlanPageThroughDeepLink(ControlTestData data, MyTmoData myTmoData, TMNGData tMNGData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the TMO application | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Phones link | All phones should be displayed ");
		Reporter.log("3. Select Sim Card | UNOPDP page should be displayed");
		Reporter.log("4. Click 'Add to cart' CTA | Select User modal should be displayed");
		Reporter.log("5. Select Existing user option | User should navigate to MyTmo login page");		
		Reporter.log("6. Enter valid User-id,Password and click login button | Consolidated rate plan page should be displayed ");
		Reporter.log("7. Verify Continue button | Continue button should be displayed");
		Reporter.log("8. Click on Continue button | Cart Page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.selectSimCardFromPlp();
		
		PdpPage phonesPdpPage = new PdpPage(getDriver());		
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.verifySelectUserModal();
		phonesPdpPage.clickExistingUserOptionInSelectUserModal();
		
		LoginPage loginPage = new LoginPage(getDriver());
		//loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(), myTmoData.getPassword());
		performLogin(myTmoData, loginPage);
		
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyContinueCTA();
		consolidatedRatePlanPage.clickOnContinueCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
	}
	
	/**
	 * CDCSM-148 :US521900 - myTMO release - As an authenticated user that has specified my intent 
	 * to ADD A LINE, I'd like the only CTA displayed to be ADD TO CART, 
	 * so I am not confused by other options.
	 * TC-1191 : Desktop: Validate that selecting a device on PLP page navigates to PDP page when the entry point was AAL CTA on the Shop landing page
	 * TC-1192 : Desktop:Validate that selecting a device on PLP page navigates to PDP page when the entry point was AAL CTA on the Plan   page
	 * TC-1193 : Desktop: Validate that "Buy a new Phone" CTA on device intent page navigates to PLP page when the entry point was AAL CTA on the mytmo home page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "uno" })
	public void testDeviceFRPPriceThroughQuickLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on 'Add a Line' link in shop page | DeviceIntent Page should be displayed");
		Reporter.log("4. Select 'Buy a new phone' option | UNO PLP Page should be displayed");
		Reporter.log("5. Save first device FRP price in V1 | Device FRP price should be saved in V1");
		Reporter.log("6. Select device | UNOPDP Page should be displayed");
		Reporter.log("7. Save device FRP price in V2 | Device FRP price should be saved in V2");
		Reporter.log("8. Verify UpgradeCTA | Upgrade CTA should be enabled");
		Reporter.log("9. Compare device FRP price | Device FRP price should be same");		
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		UNOPLPPage unoPLPPage = navigateToUNOPLPPageFromAALBuyNewPhoneFlow(myTmoData);
		String v1 = unoPLPPage.getDeviceFRP();
		unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyUpGradeCTAEnabled();
		String v2 = unoPDPPage.getDeviceFRP();
		unoPDPPage.compareTwoStringValues(v1, v2);		
	}
	
	/**
	 * CDCSM-148 :US521900 - myTMO release - As an authenticated user that has specified my intent 
	 * to ADD A LINE, I'd like the only CTA displayed to be ADD TO CART, 
	 * so I am not confused by other options.
	 * TC-1194 : Desktop: Validate that clicking on "Add to Cart" CTA on UNO PDP page (EIP Pricing ) navigates to Consolidated changes page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "uno" })
	public void testDeviceEIPPriceThroughQuickLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on 'Add a Line' link in shop page | DeviceIntent Page should be displayed");
		Reporter.log("4. Select 'Buy a new phone' option | Consolidated Rate Plan Page should be displayed");
		Reporter.log("5. Click on Continue button | UNO PLP Page should be displayed");
		Reporter.log("7. Select device | UNOPDP Page should be displayed");		
		Reporter.log("9. Verify UpgradeCTA | Upgrade CTA should be enabled");
		Reporter.log("10. Verify Device EIP Price | Device EIP price should be displayed");		
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		UNOPDPPage unoPDPPage = navigateToUNOPDPPageFromAALBuyNewPhoneFlow(myTmoData);
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyUpGradeCTAEnabled();
		unoPDPPage.verifyDeviceEIPPrice();		
	}
	
	/**
	 * CDCSM-148 :US521900 - myTMO release - As an authenticated user that has specified my intent 
	 * to ADD A LINE, I'd like the only CTA displayed to be ADD TO CART, 
	 * so I am not confused by other options.
	 * TC-1196 : Desktop: Validate that clicking on Back and forth on UNO PDP page displays the same pricing
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "uno" })
	public void testDevicePriceWhenUserNavigateToUNOPLPPageAndSelectAnotherDeviceThroughQuickLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on 'Add a Line' link in shop page | DeviceIntent Page should be displayed");
		Reporter.log("4. Select 'Buy a new phone' option | Consolidated Rate Plan Page should be displayed");
		Reporter.log("5. Click on Continue button | UNO PLP Page should be displayed");
		Reporter.log("7. Select device | UNOPDP Page should be displayed");		
		Reporter.log("9. Verify UpgradeCTA | Upgrade CTA should be enabled");
		Reporter.log("10. Verify Device FRP Price | Device FRP price should be displayed");	
		Reporter.log("11. Navigate to UNOPLPPage | UNOPLPPage should be displayed");
		Reporter.log("12. Select device | UNOPDPPage should be displayed");
		Reporter.log("13. Verify Device FRP Price | Device FRP Price should be displayed");
		Reporter.log("14. Verify UpgradeCTA | Upgrade CTA should be enabled");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		UNOPDPPage unoPDPPage = navigateToUNOPDPPageFromAALBuyNewPhoneFlow(myTmoData);
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyUpGradeCTAEnabled();
		unoPDPPage.verifyDeviceFRPPrice();		
		unoPDPPage.navigateBack();
		UNOPLPPage unoPLPPage = new UNOPLPPage(getDriver());
		unoPLPPage.verifyPageLoaded();
		unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyDeviceFRPPrice();	
		unoPDPPage.verifyUpGradeCTAEnabled();
	}
	
	/**
	 * CDCSM-146 :US572786 - MyTMO Release - As a Product Manager, 
	 * I want to redirect uncookied/unauthenticated customers back to the PDP page, 
	 * so that they can view contextual information based on their CRP.
	 * TC- 1007 : Desktop:Validate that an uncookied or unauthenticated customer navigates back to PDP page for accessories that are available
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testUserNavigatedTOUNOPDPPageThroughDeepLinkForAccessories(ControlTestData data, MyTmoData myTmoData, TMNGData tMNGData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the TMO application | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Accessories link | All Accessories should be displayed ");
		Reporter.log("3. Select accessory device | UNOPDP page should be displayed");
		Reporter.log("4. Click 'Add to cart' CTA | Select User modal should be displayed");
		Reporter.log("5. Select Existing user option | User should navigate to MyTmo login page");		
		Reporter.log("6. Enter valid User-id,Password and click login button | UnoPdp page should be displayed ");
		Reporter.log("7. Verify UpgradeCTA | Upgrade CTA should be enabled");
		Reporter.log("8. Verify Add a Line CTA | Add a Line CTA should be enabled");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PdpPage phonesPdpPage = navigateToAccessoryMainPDP(tMNGData);
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.verifySelectUserModal();
		phonesPdpPage.clickExistingUserOptionInSelectUserModal();		
		LoginPage loginPage = new LoginPage(getDriver());
		//loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(), myTmoData.getPassword());
		performLogin(myTmoData, loginPage);
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyUpGradeCTAEnabled();		
	}
	
	/**
	 * CDCSM-505 :UNO P2 - Upgrade - Hide product categories 	 
	 * TC- 2258 :Validate that user is seeing all the product categories if intent in unknown
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testUserNavigatedTOUNOPDPPageThroughTMNG(ControlTestData data, MyTmoData myTmoData, TMNGData tMNGData) {
		Reporter.log("================================");
		Reporter.log("TestCase:CDCSM-505 :UNO P2 - Upgrade - Hide product categories");
		Reporter.log("TestCase:TC- 2258 :Validate that user is seeing all the product categories if intent in unknown");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the TMO application | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Phones link | UNOPLP page should be displayed ");
		Reporter.log("3. Select Device device | UNOPDP page should be displayed");
		Reporter.log("4. Click 'Add to cart' CTA | User should prompted for login or continue as guest");
		Reporter.log("5. Select Existing user option | User should navigate to MyTmo login page");		
		Reporter.log("6. Enter valid User-id,Password and click login button | UNOPDP page should be displayed ");
		Reporter.log("7. Verify UpgradeCTA | Upgrade CTA should be enabled");
		Reporter.log("8. Verify Add a Line CTA | Add a Line CTA should be enabled");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PdpPage pdpPage = navigateToPhonesPDP(tMNGData);		
		pdpPage.clickOnAddToCartBtn();
		LoginPage loginPage = new LoginPage(getDriver());
		//loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(), myTmoData.getPassword());
		performLogin(myTmoData, loginPage);
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyUpGradeCTAEnabled();
		unoPDPPage.verifyAddaLineCTAEnabled();
	}

}
