/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;


import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.AddCardPage;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.BillingSummaryPage;
import com.tmobile.eservices.qa.pages.payments.NewAddCardPage;
import com.tmobile.eservices.qa.pages.payments.OTPAmountPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.PAConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.PAInstallmentDetailsPage;
import com.tmobile.eservices.qa.pages.payments.PaymentArrangementPage;
import com.tmobile.eservices.qa.pages.payments.PaymentCollectionPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class PaymentArrangementPageTest extends PaymentCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(PaymentArrangementPageTest.class);

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testVerifyPaymentArrangementSetupOnPaymentHub(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log("Step 4: verify pa setup alert| Payment arrangement page should be dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		
			navigateToBillingPage(myTmoData);
		if (getDriver().getCurrentUrl().contains("/billandpay")) {
			BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
			billAndPaySummaryPage.verifyPageLoaded();
			//billAndPaySummaryPage.verifyPaymentArrangementShortcut();
			billAndPaySummaryPage.clickPaymentArrangementShortcut();
			PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
			paymentArrangementPage.verifyPApageDisplayed();
			
		}else {
			BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
			billingSummaryPage.changeBillCycle();
			billingSummaryPage.verifyPaymentArrangementText();
			billingSummaryPage.verifyPASubAlert();
			billingSummaryPage.clickPAArrow();
			PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
			paymentArrangementPage.verifyPageLoaded();
			paymentArrangementPage.verifyPApageDisplayed();
			
		}

	
		
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SEDONA, Group.PENDING })
	public void testSedonaPASetupAgreenSubmitBtnErrorValidation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 4: click on agree and submit button| no payment error message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		// paymentArrangementPage.clickAgreeNSubmitBtn();
		// Assert.assertTrue(paymentArrangements.verifynopaymentmethodErrorDisplayed(),
		// "no payment error message not displayed");
		Reporter.log("no payment error message is displayed");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testPAAutopayUnrollAlert(ControlTestData data, MyTmoData myTmoData) throws IOException {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 4: verify autopay unroll alert displayed | autopay unroll  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results");

		HomePage homePage = navigateToHomePage(myTmoData);
		Autopayswitch("ON");
		
		/*AutoPayPage autopayLandingPage = navigateToAutoPayPage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		if (autopayLandingPage.verifyAutopayisscheduled()) {
			if (!autopayLandingPage.verifyIfPaymentMethodIsSaved()) {
				autopayLandingPage.addBankToPaymentMethod(myTmoData);
			}
			autopayLandingPage.clickAgreeAndSubmitBtn();
			AutoPayConfirmationPage confirmationPage = new AutoPayConfirmationPage(getDriver());
			confirmationPage.verifyPageLoaded();
			confirmationPage.verifySuccessHeader();
			confirmationPage.clickreturnToHome();
			
			homePage.verifyPageLoaded();
		}
		*/
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		
		oneTimePaymentPage.verifyPaymentArrangementLinkDisplayed();
		oneTimePaymentPage.clickpaymentArrangement();
		paymentArrangementPage.verifyPageLoaded();
		if (!paymentArrangementPage.isPAScheduled()) {
			paymentArrangementPage.verifyAutopayUnrollAlert();
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.SEDONA })
	public void testSedonaPASetupOneInstallementCase(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 5: clcik on installement blade|installement spoke page should be displayed");
		Reporter.log("Step 6: verify installment two option disabled|installement two option should be disabled");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		paymentArrangementPage.verifyInstallmentPageDisplayed();
		paymentArrangementPage.verifyEligibleMessageForOneInstallement();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SEDONA })
	public void testSedonaPASetupMinimunInstallementCase(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log(
				"Step 4: verify minimum installement case error |minimum installemnet case error should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyMinimunInstallementCaseErrorDispalyed(PaymentConstants.PA_MIN_INSTALLMENT_CASE_ERROR_MSG);
	}

	/**
	 * US131892 Payment Module H&S - Payment Method Blade in Payment Arrangement
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testPaymentMethodBladeinPaymentArrangement(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: click on payment method | payment spoke page  should be displayed");
		Reporter.log(
				"Step 6: click on choose payment method later and continue | payment None provided should be displayed on payment method");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickpaymentMethod();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.choosePaymentMethodLater();
		otpSpokePage.clickContinueButton();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyPApageDisplayed();
	}

	/**
	 * Verify completed PAYMENT ARRANGEMENT displays on Homepage
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws IOException 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyPAdisplaysHomepage(ControlTestData data, MyTmoData myTmoData) throws IOException {
		logger.info("verifyPAdisplaysHomepage method called in EservicesPaymnetsTest");
		Reporter.log("Test Case :Verify completed PAYMENT ARRANGEMENT displays on Homepage");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Click on Payment Arrangement | Payment Arrangements page should be displayed.");
		Reporter.log("6. Click Balance radio button and Select New Card button| New Card section should be displayed.");
		Reporter.log("7. Fill Card details and click next | Review payment page should be loaded");
		Reporter.log("8. Verify Card Information | Entered Hybrid card details should be displayed correctly.");
		Reporter.log("9. Click on Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("10. Click Submit Button | Payment Confirmation message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		if (!paymentArrangementPage.isPAScheduled()) {
		paymentArrangementPage.clickinstallmentDetails();
		paymentArrangementPage.clickDatePicker();
		paymentArrangementPage.pickDate();
		paymentArrangementPage.clickUpdateButton();
		}
		paymentArrangementPage.clickpaymentMethod();
		SpokePage spokePage=new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), spokePage);
		//addNewBank(myTmoData.getPayment(), spokePage);
		paymentArrangementPage.verifyAgreeAndSubmitButton();
	}

	/**
	 * Ensure T&C are present on the OTP hub, and the link properly displays the
	 * terms and conditions
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void verifyLinkdisplayedCorrectlyIntermsAndcondition(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyLinkdisplayedCorrectlyIntermsAndcondition method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure T&C are present on the OTP hub, and the link properly displays the terms and conditions");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("4. Click on Terms & conditions link | Terms and Conditions should be displayed");
		Reporter.log("5. Click on Payment Arrangement | Payment Arrangements page should be displayed.");
		Reporter.log(
				"9. Click Link on Terms And Conditions  | Redirects to Payments Arrangement page should be navigated correctly");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickTermsAndConditionsLink();
		paymentArrangementPage.verifytermsconditionspageDisplayed();
		paymentArrangementPage.verifyHyperLinkonTnCpage();
	}

	/**
	 * Verify Cancel SCHEDULED PAYMENT modal displays as expected
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyScheduledpaymentDetails(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyCancelscheduledPayment method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Cancel a SCHEDULED PAYMENT");
		Reporter.log("Data Conditions - Regular/Sedona user with scheduled PA");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Click on Scheduled Payment | Current activity page should be displayed");
		Reporter.log("6. Click Add Payment Method | Edit Payment Method for Scheduled Payment should be displayed");
		Reporter.log("7. Verify Scheduled Payment Details | Scheduled Payment Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		Double initialBalAmount = Double.parseDouble(homePage.getBalanceDueAmount().replace("$", ""));
		Reporter.log("Balance amount on Home page: $" + initialBalAmount);
		String dueDate = homePage.getDueDate();
		Reporter.log("Due Date on Home page: " + dueDate);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage alertsActivitespage = new AccountHistoryPage(getDriver());
		PaymentArrangementPage paymentArrangement = new PaymentArrangementPage(getDriver());
		/* oneTimePaymentPage.clickScheduledpaymentReminder(); */
		alertsActivitespage.verifyPageLoaded();
		alertsActivitespage.clickAddpaymentmethod();
		paymentArrangement.verifyPaymentarrangementPage();
		paymentArrangement.verifyScheduledpaymentDetails();
	}

	/**
	 * Verify user can select all visible dates for PA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void verifyVisibledatesSelectedPA(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyVisibledatesSelectedPA method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Verify user can select all viable dates for PA");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Click on Payment Arrangement | Payment Arrangements page should be displayed.");
		Reporter.log("6. Click Date Picker | Date Picker should be displayed");
		Reporter.log(
				"7. User can able to select all the visible Date | Selected Visible date should be displayed Date picker textbox");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		if (!paymentArrangementPage.isPAScheduled()) {
		paymentArrangementPage.clickinstallmentDetails();
		paymentArrangementPage.clickDatePicker();
		paymentArrangementPage.verifyallVisibleDates();
		Reporter.log(PaymentConstants.VERIFY_VISIBLE_DATES_DISPLAYED);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyUserAbleTOMakePaymentscheduleArrangement(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakePaymentscheduleArrangement method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make Payment scheduleArrangement");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Click on Payment Arrangement | Payment Arrangements page should be displayed.");
		Reporter.log("6. Click on one installement | One installement amount should be displayed.");
		Reporter.log("7. Click on two installement | Two installement amount should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		if (!paymentArrangementPage.isPAScheduled()) {
		paymentArrangementPage.clickinstallemtBlade();
		paymentArrangementPage.verifyInstallmentPageDisplayed();
		paymentArrangementPage.clickDatePicker();
		paymentArrangementPage.pickDate();
		paymentArrangementPage.clickinstallemtOne();
		paymentArrangementPage.validateOneInstallment();
		}
	}

	/**
	 * Setup PAYMENT ARRANGEMENT with CREDIT Card Covered
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifySetupPAcreditCardpayment(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySetupPAcreditCardpayment method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Setup PAYMENT ARRANGEMENT with CREDIT Card Covered");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Click on Payment Arrangement | Payment Arrangements page should be displayed.");
		Reporter.log("6. Click Balance radio button and Select New Card button| New Card section should be displayed.");
		Reporter.log("7. Fill Card details and click next | Review payment page should be loaded");
		Reporter.log("8. Verify Card Information | Entered Hybrid card details should be displayed correctly.");
		Reporter.log("9. Click on Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("10. Click Submit Button | Payment Confirmation message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickpaymentMethod();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.fillCardInfo(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		paymentArrangementPage.verifyAgreeAndSubmitButton();
		paymentArrangementPage.verifyStoredCardNumber(myTmoData.getPayment().getCardNumber());
	}

	/**
	 * US282985 GLUE Light Reskin - PA Setup
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Regular account. PA eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testGLRPASetup(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US282985 GLUE Light Reskin - PA Setup");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 5: click on faqs| faq page should be displayed");
		Reporter.log("Step 6: click on Tnc link| T&C page should be displayed");
		Reporter.log("Step 7: verify payment method blade| Payment method blade should be displayed");
		Reporter.log("Step 8: verify Payment Installments blade| Payment Installments blade should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickSeeFAQS();
		paymentArrangementPage.verifyfaqPageDisplayed();
		paymentArrangementPage.clickFaqBackBtn();
		paymentArrangementPage.verifyPApageDisplayed();
		paymentArrangementPage.clickTermsAndConditionsLink();
		paymentArrangementPage.verifyTnCPageDisplayed();
		paymentArrangementPage.verifyHyperLinkonTnCpage();
		paymentArrangementPage.clickTnCBackBtn();
		paymentArrangementPage.verifyPApageDisplayed();
		paymentArrangementPage.verifyCancelButton();
		paymentArrangementPage.verifyAgreeAndSubmitButton();
		paymentArrangementPage.verifyPaymentMethodBlade();
		paymentArrangementPage.verifyPaymentInstallmentsBlade();
	}

	/**
	 * US282997 GLUE Light Reskin - PA Edit Details (Installments page)
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Regular/Sedona user. With current due and
	 *            past due. PA eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testGLRPAEditDetailsInstallmentsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US282997	GLUE Light Reskin - PA Edit Details (Installments page)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log(
				"Step 5: verify and click on Payment Installments blade| Installment details page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyPageLoaded();
		
		if (!paymentArrangementPage.isPAScheduled()) {
			paymentArrangementPage.clickinstallemtBlade();
			paymentArrangementPage.verifyInstallmentPageDisplayed();
			paymentArrangementPage.verifyAmountIsDisplayed();
			paymentArrangementPage.verifyChooseDate();
			paymentArrangementPage.verifyUpdateCTA();
			paymentArrangementPage.verifyCancelCTA();
		}
		
	}

	/**
	 * US284690 GLUE Light Reskin - PA Edit Details - View Details Breakdown
	 * Modal
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Sedona user. With current due and past due.
	 *            PA eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY, Group.SEDONA })
	public void testGLRPAEditDetailsviewDetailsBreakdownModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US284690	GLUE Light Reskin - PA Edit Details - View Details Breakdown Modal");
		Reporter.log("Data Conditions - Sedona user. With current due and past due. PA eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log(
				"Step 5: verify and click on Payment Installments blade| Installment details page should be displayed");
		Reporter.log("Step 6: verify and click on view details| Breakdown modal should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyPaymentInstallmentsBlade();
		if (!paymentArrangementPage.isPAScheduled()) {
		paymentArrangementPage.clickinstallemtBlade();
		paymentArrangementPage.verifyInstallmentPageDisplayed();
		paymentArrangementPage.verifyViewDetailsLink();
		paymentArrangementPage.clickViewDetails();
		paymentArrangementPage.verifyBreakDownModal();
		paymentArrangementPage.verifyTotalOnBreakDownModal();
		paymentArrangementPage.clickCloseBreakdownModal();
		paymentArrangementPage.verifyInstallmentPageDisplayed();
		}
	}

	/**
	 * US282999 GLUE Light Reskin - PA T&Cs
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Regular account. PA eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "Merged" })
	public void testGLRPATnC(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US282999	GLUE Light Reskin - PA T&Cs");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 5: click on Tnc link| T&C page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickTermsAndConditionsLink();
		paymentArrangementPage.verifyTnCPageDisplayed();
		paymentArrangementPage.verifyHyperLinkonTnCpage();
		paymentArrangementPage.clickTnCBackBtn();
		paymentArrangementPage.verifyPApageDisplayed();
	}

	/**
	 * US282998 GLUE Light Reskin - PA FAQs
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Regular account. PA eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testGLRPAFAQ(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US282998	GLUE Light Reskin - PA FAQs");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 5: click on faqs| faq page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickSeeFAQS();
		paymentArrangementPage.verifyfaqPageDisplayed();
		paymentArrangementPage.clickFaqBackBtn();
		paymentArrangementPage.verifyPApageDisplayed();
	}

	/**
	 * US284689 GLUE Light Reskin - PA Setup - Calendar Modal
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Regular account. PA eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testGLRPASetupCalendarModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on date blade| Date selction page should be displayed");
		Reporter.log("Step 5: verify calendar modal| Calendar should be displayed for active date selection.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		if (!paymentArrangementPage.isPAScheduled()) {
		paymentArrangementPage.clickinstallemtBlade();
		paymentArrangementPage.verifyInstallmentPageDisplayed();
		paymentArrangementPage.clickDatePicker();
		paymentArrangementPage.verifyDatePageHeader();
		paymentArrangementPage.verifycalendar();
		paymentArrangementPage.verifycalendarUpdateBtn();
		paymentArrangementPage.verifycalendarCancelBtn();
		paymentArrangementPage.clickCalendarCancelBtn();
		paymentArrangementPage.verifyInstallmentPageDisplayed();
		}
	}

	/**
	 * US282986 GLUE Light Reskin - PA Modify
	 * 
	 * @param data
	 * @param myTmoData
	 *            Test Data - Regular account. PA should be scheduled scheduled
	 *            on XXXX
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testGLRPAModify(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US282986	GLUE Light Reskin - PA Modify");
		Reporter.log("Data Conditions - Regular account. PA should be scheduled scheduled on XXXX");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 5: click on faqs| faq page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

	
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickSeeFAQS();
		paymentArrangementPage.verifyfaqPageDisplayed();
		paymentArrangementPage.clickFaqBackBtn();
		paymentArrangementPage.verifyPApageDisplayed();
		paymentArrangementPage.clickTermsAndConditionsLink();
		paymentArrangementPage.verifyTnCPageDisplayed();
		paymentArrangementPage.clickTnCBackBtn();
		paymentArrangementPage.verifyPApageDisplayed();
		paymentArrangementPage.verifyAgreeAndSubmitButton();
		paymentArrangementPage.verifyPaymentMethodBlade();
		paymentArrangementPage.verifyPaymentInstallmentsBlade();

	}

	/**
	 * US282988 GLUE Light Reskin - PA Modify
	 * 
	 * @param data
	 * @param myTmoData
	 *            Test Data - Regular account. PA is scheduled with 2
	 *            installements. 1 missed. Very data dependent since there are
	 *            48 hours to see Missed Payment
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PENDING })
	public void testGLRPAMissedPayment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US282988	GLUE Light Reskin - PA Missed Payment");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Account History |  Account history page should be displayed");
		Reporter.log("Step 4: Click on View arrengement details link | Payment arrangement page should be loaded");
		Reporter.log("Step 4.1: Verify header Edit payment arrangement is present");
		Reporter.log("Step 4.2 Verify Missed payment. Immediate action needed! is present");
		Reporter.log("Step 4.3 Verify Payment installments blade is present");
		Reporter.log("Step 4.4 Verify Missed is present on the right of installement");
		Reporter.log(
				"Step 4.5 Verify Scheduled is present on Payment Installments blade right between date and amount");
		Reporter.log("Step 4.6 Verify See FAQ link is present");
		Reporter.log(
				"Step 4.7 Verify Make a payment for XXX CTA is present, where XXX is amount of first missed installement");
		Reporter.log("Step 4.8 Verify Cancel CTA is present");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		AccountHistoryPage alertsActivitespage = new AccountHistoryPage(getDriver());
		alertsActivitespage.clickViewArrangementDetails();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPApageDisplayed();
		paymentArrangementPage.verifyMissedPaymentAlertsDisplayd();
		paymentArrangementPage.verifyFAQLinkIsWorking();
		paymentArrangementPage.verifyCancelButton();
		paymentArrangementPage.verifyMakePaymentButton();
	}

	/**
	 * US284825 GLUE Light Reskin - PA Exit Modal
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Regular account. PA eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS ,"test1126"})
	public void testGLRPAExitOTPModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US284825	GLUE Light Reskin - PA Exit Modal");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 4: click on Cancel button | Cancel(Exit) modal should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickCancelButton();
		paymentArrangementPage.verifyCancelModal();
		paymentArrangementPage.verifyCancelModalButtonsCTA();
		paymentArrangementPage.clickNoCTAOnCancelModal();
		paymentArrangementPage.verifyPApageDisplayed();
		paymentArrangementPage.clickCancelButton();
		paymentArrangementPage.verifyCancelModal();
		paymentArrangementPage.clickYesCTAOnCancelModal();
		HomePage home=new HomePage(getDriver());
		home.verifyPageLoaded();
		//oneTimePaymentPage.verifyPageLoaded();
	}

	/**
	 * US262841 Angular + GLUE - OTP - Landing Page - Entry Point to Payment
	 * Arrangement
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SEDONA })
	public void testAngularGLUEOTPPageEntryToPASedonaUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262841	Angular + GLUE - OTP - Landing Page - Entry Point to Payment Arrangement");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log(
				"Step 4: verify payment arrangement texts and link | Payment arrangement links and text should be displayed");
		Reporter.log(
				"Step 5: click on payment arrangement link and Yes button on modal | Payment arrangement page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPaymentArrangementPage(myTmoData);
	}

	/**
	 * US262841 Angular + GLUE - OTP - Landing Page - Entry Point to Payment
	 * Arrangement
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEOTPPageEntryToPAUserWithDueAmount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262841	Angular + GLUE - OTP - Landing Page - Entry Point to Payment Arrangement");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log(
				"Step 4: verify payment arrangement texts and link | Payment arrangement links and text should be displayed");
		Reporter.log(
				"Step 5: click on payment arrangement link and Yes button on modal | Payment arrangement page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPaymentArrangementPage(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testPAConvinenceFeenAlert(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |payment arrangemnet page should be loaded");
		Reporter.log("Step 4: verify convenience fee alert displayed | convenience fee alert  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		displayBillDueDateAndAmount(homePage);
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		oneTimePaymentPage.verifyPaymentArrangementLinkDisplayed();
		paymentArrangementPage.verifyConvenienceFeeAlertDisplayed();
		/*if (oneTimePaymentPage.verifyReviewPAModalIsDisplayedOrNot()) {
			oneTimePaymentPage.clickReviewPABtnOnModal();
		}else if (oneTimePaymentPage.verifyPaymentArrangementLinkDisplayed()) {
			paymentArrangementPage.verifyConvenienceFeeAlertDisplayed();
		}*//* else {
			oneTimePaymentPage.clickCancelBtn();
			oneTimePaymentPage.clickContinueBtnModal();
			homePage.clickAccountHistoryHeader();
			AccountHistoryPage alertsActivitespage = new AccountHistoryPage(getDriver());
			alertsActivitespage.verifyPageLoaded();
			alertsActivitespage.clickAddpaymentmethod();
			paymentArrangementPage.verifyPaymentArrangementpageDisplayed();
		}*/
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAnotifications(ControlTestData data, MyTmoData myTmoData) {

	}

	/**
	 * 	US356345	PII Masking > Variables On Payment Arrangement Page
	 * 	US408170	PII Masking> Edit PA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testPIIMaskingPaymentArrangementPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US356345	PII Masking > Variables On Payment Arrangement Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log(
				"Step 4: verify payment arrangement texts and link | Payment arrangement links and text should be displayed");
		Reporter.log(
				"Step 5: click on payment arrangement link and Yes button on modal | Payment arrangement page should be displayed");
		Reporter.log("Step 6: verify PII masking | PII masking attributes should be present for all payment information");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyPiiMasking(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		paymentArrangementPage.clickpaymentMethod();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyPIIMasking(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		
	}
	
	/**
	 * US346424 MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-Interrupt modal
	 * US437683 100% PA Landing Page Load Front End Logic
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "testdata"})
	public void testDigitalPASupportPastDueInterruptModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US346424	MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-Interrupt modal");
		Reporter.log("Data Condition | past due balance aged greater than 30 days  ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: verify payment arrangement texts and link | Payment arrangement links and text should be displayed");
		Reporter.log("Step 5: click on payment arrangement link | interrupt modal should be displayed");
		Reporter.log("Step 6: verify header 'one-time payment required' on interrupt modal|  header 'one-time payment required' should be displayed");
		Reporter.log("Step 7:  verify 'Before we can set up a payment arrangement, we’ll need a payment for the amount that’s more than 30 days past due' on the modal| one time payment message should be displayed");
		Reporter.log("Step 8:  verify text '30-days past due amount' | '30-days past due amount'  should be displayed");
		Reporter.log("Step 9: verify  Continue , cancel CTA|  Continue , cancel CTA should be displayed ");
		Reporter.log("Step 10: click on  Continue CTA|  user should be taken to OTP Flow");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyPaymentArrangementLink();
		oneTimePaymentPage.clickPaymentArrangementLink();
		oneTimePaymentPage.verifyPAInterruptModal();
		oneTimePaymentPage.verifyTextOnPAInterruptModal(PaymentConstants.INTERRUPT_MODAL_TEXT,PaymentConstants.NEW_OTP_PAGE_HEADER);
		oneTimePaymentPage.verify30DayPastDueOnPAInterruptModal(PaymentConstants.INTERRUPT_MODAL_30_DAYS_PAST_DUE);
		oneTimePaymentPage.verifyCTAsOnPAInterruptModal();
		oneTimePaymentPage.clickContinueOnPAInterruptModal();
		oneTimePaymentPage.verifyNewOTPPageLoaded();
	}
	

	/**
	 *US346423 MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-entry points- Standard OTP
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "testdata"})
	public void testDigitalPASupportPastDueEntryPointsDigitalPA(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US346423 MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-entry points- Standard OTP");
		Reporter.log("Data Condition | past due balance aged greater than 30 days  ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: verify payment arrangement texts and link | Payment arrangement links and text should be displayed");
		Reporter.log("Step 5: click on payment arrangement link | user should navigate to 100% PA flow");
	
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentArrangementLink();
		oneTimePaymentPage.verifyPAInterruptModal();
		oneTimePaymentPage.clickContinueOnPAInterruptModal();
		oneTimePaymentPage.verifyNewOTPPageLoaded();
	}
	
	/**
	 * US408957	PA : General - FAQ page creation
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { })
	public void testPAFAQpage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 6: click on faqs| faq page should be displayed");
		Reporter.log("Step 7: click on back button | Payment arrangement page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickSeeFAQS();
		paymentArrangementPage.verifyfaqPageDisplayed();
		paymentArrangementPage.clickFaqBackBtn();
		paymentArrangementPage.verifyPApageDisplayed();
	}


	
	
	/**
	 * DE228577
	MyTMO Desktop: Sedona user when clicks PA link on homepage redirects to billing page
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { })
	public void testSetupPALinkonHomePageforSedonaUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on setup Payment Arrangemnet link | Payment Arrangemnet  Page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

	
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		displayBillDueDateAndAmount(homePage);
		homePage.clickSetupPaymentArrangementLink();
		
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();

	}

	/**
	 * DE226414
				Reg9_PA link is  getting displayed in OTP page after sucessful PA set up
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { ""})
	public void testPALinkNotDisplayedonOTPPageAfterPAsetup(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("DE226414	Reg9_PA link is  getting displayed in OTP page after sucessful PA set up");
		Reporter.log("Data Conditions - Regular account. Eligible for PA");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 5: add the payment method| Payment method should be updated");
		Reporter.log("Step 6: Click Agree abd submit | PA confirmation page should be displayed");
		Reporter.log("Step 7: Click return home | Home page should be displayed");
		Reporter.log("Step 8:  Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 9:  verify PA link not displayed|PA link  should not be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickpaymentMethod();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.fillCardInfo(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		paymentArrangementPage.verifyAgreeAndSubmitButton();
		paymentArrangementPage.clickAgreeNSubmitBtn();
		PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
		paConfirmationPage.verifyPageLoaded();
		paConfirmationPage.clickReturnToHome();
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyPaymentArrangementLinkNotDisplayed();
	}


	/**
	 * DE229046
	E1 (PA) - Payment blade is NOT disabled when the Installment is on Blackout period

	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { ""})
	public void testPaymentBladeDisabledInstallmentinBlackoutPeriod(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("DE229046	E1 (PA) - Payment blade is NOT disabled when the Installment is on Blackout period");
		Reporter.log("Data Conditions - Regular account. PPA scheduled insideblackout peridA");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 5: verify payment method chevron not dispalyed| payment method chevron should not dispalyed");
		Reporter.log("Step 6: verify message for payment method for inside blackout period| message for payment method for inside blackout period should be dispalyed");
		
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage alertsActivitespage = new AccountHistoryPage(getDriver());
		alertsActivitespage.verifyPageLoaded();
		alertsActivitespage.clickAddpaymentmethod();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
		//paymentArrangementPage.verifyPaymentMethodChevronNotDispalyed();
		paymentArrangementPage.verifyMsgForPaymentMethodInsideBlackoutPeriod();

	}
	
	/**
	 * DE229057
	E1 (PA) - Installment status is wrong for the installment in Blackout period

	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { ""})
	public void testSecuredInstallementStatusInsideBlackoutPeriod(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("DE229057	E1 (PA) - Installment status is wrong for the installment in Blackout period");
		Reporter.log("Data Conditions - Regular account. PPA scheduled insideblackout period secured");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 5: verify installement status as processed| installement status should be as processed");
		Reporter.log("Step 6: verify message for Secured and Blackout Period| message for Secured and Blackout Period should be dispalyed");
		
		Reporter.log("================================");
		Reporter.log("Actual Results:");

	
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyInstallementStatus("Processing");
		paymentArrangementPage.verifyMsgForSecuredInsideBlackoutPeriod();

	}
	
	/**
	 * DE228285
	REG9_Unable to update  past due amount/Paymnet method from OTP Varinat page(Billing,Home page)

	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { ""})
	public void testUpdatePastDueAmountOTPVariantFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("DE228285	REG9_Unable to update  past due amount/Paymnet method from OTP Varinat page(Billing,Home page)");
		Reporter.log("Data Conditions - Regular account with 30+day past due");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Digital PA modal should be loaded");
		Reporter.log("Step 5: verify digital PA modal header| digital PA modal header should be dispalyed");
		Reporter.log("Step 6: click on continue button| OTP variant page should be dispalyed");
		Reporter.log("Step 7: enter other amount and add payment details and click on continue|Agree and submit button should be enabled");
		
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentArrangementLink();
		oneTimePaymentPage.verifyDigitalPAModal();
		oneTimePaymentPage.clickOnDigitalPAcontinueBtn();
		oneTimePaymentPage.verifyOTPVariantPage();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		
		

	}
	
	/**
	 * US524438 OTP Landing Page - PA Entry Point Enhancement
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void verifyPAEntryPointEnhancementOTPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : US524438 OTP Landing Page - PA Entry Point Enhancement");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Verify PA Entrypoint displayed|  PA Entrypoint should be displayed.");
		Reporter.log("6. Verify PA convinence Fee message displayed|  PA convinence Fee message should be displayed.");
		Reporter.log("================================");

		Reporter.log("Actual Results:");
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyPaymentArrangementEntryPointDispalyed();
		oneTimePaymentPage.verifyPAConvinenceFeeMessage();
		oneTimePaymentPage.clickpaymentArrangement();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
	}
	
	
	/**
	 * DE231970
	E1_ OTP Landing Page - PA Entry Point Entire Banner is not clickable
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testPAEntryPointBanner(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : DE231970 E1_ OTP Landing Page - PA Entry Point Entire Banner is not clickable");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Verify PA Entrypoint displayed|  PA Entrypoint should be displayed.");
		Reporter.log("6. Verify PA convinence Fee message displayed|  PA convinence Fee message should be displayed.");
		Reporter.log("================================");

		Reporter.log("Actual Results:");
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPAEntryPointBanner();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
	}
	
	
	
	/**
	 * DE232242  MyTMO [Payments] Installment details incorrect for IS/IA accounts
	 * 
	 * @param data
	 * @param myTmoData
	 * Data Conditions - 
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testInstallementDateswithinBillDuedate(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: DE232242  MyTMO [Payments] Installment details incorrect for IS/IA accounts");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2. Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3. Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4. click on payment arrangement | payment arrangemnet page should be loaded");
		Reporter.log("Step 5.verify payment installements dates with in bill due date| payment installements dates should be with in bill due date");
	
		Reporter.log("================================");
		Reporter.log(Constants.ACTUAL_TEST_STEPS);

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		String dueDate = paymentArrangementPage.getBilldueDate();
		String installementOneDate = paymentArrangementPage.getInstallementOneDate();
		String installementTwoDate = paymentArrangementPage.getInstallementTwoDate();
		paymentArrangementPage.verifyInstallementsdatesWithinDueDateLimit(dueDate,installementOneDate,installementTwoDate);
		

	}
	
	/**
	 * US545955-CCS: PCM Entrypoint Update  
	 *              : Verify  user can enter PCM thru PaymentArrangement page  
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void testverifyPCMentryPointPaymentArrangementPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPCMentry point  method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Verify PCM entryPoint from PaymentArrangementPage ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Navigate to PaymentArrangement Page | PaymentArrangement Page should be displayed");
		Reporter.log("5. Enter the PCM url  | PCM entry Point page should be displayed");
		Reporter.log("================================");
		
		
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyPageLoaded();
		getDriver().get(System.getProperty("environment") + "ccs/payments/paymentcollection");
		SpokePage paymentsCollectionPage = new SpokePage(getDriver());
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.verifyNewPageUrl();
		
	}
	
	
	/**
	 * US578987 Disallow Payment Arrangement for all Non-Master
	 * @param data
	 * @param myTmoData
	 *            
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPaymentArrangementDisAllowedforNonMasterUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US578987	Disallow Payment Arrangement for all Non-Master");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: navigate to billing page|  billing page should be displayed");
		Reporter.log("Step 4: Click on PA shortcut | PA disallowed modal should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");


		navigateToBillingPage(myTmoData);
	if (getDriver().getCurrentUrl().contains("/billandpay")) {
		BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
		billAndPaySummaryPage.verifyPageLoaded();
		billAndPaySummaryPage.verifyPaymentArrangementShortcut();
		billAndPaySummaryPage.clickPaymentArrangementShortcut();
	
		
		
	}else {
		BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
		billingSummaryPage.changeBillCycle();
		billingSummaryPage.verifyPaymentArrangementText();
		billingSummaryPage.verifyPASubAlert();
		billingSummaryPage.clickPAArrow();
		
		
		
		
	}
	PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
	paymentArrangementPage.verifyPaymentArrangementDisAllowedforNonMasterUser();

	}

	/**
	 * US408883 PA : Setup - Landing page.
	 * 
	 * @param data
	 * @param myTmoData
	 * Data Conditions - Regular account. PA eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "Merged" })
	public void testPASetupLandingPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US408883 PA : Setup - Landing page.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 5: verify page| All page components should be displayed");
		Reporter.log("Step 6: verify T&C link| T&C link and text should be displayed");
		Reporter.log("Step 7: verify FAQ link| FAQ link should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyTnCLink();
		paymentArrangementPage.verifySeeFAQSLink();
	}

	/**
	 * US408878 PA : Setup - Payment Collection Module Integration
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testSetupPaymentCollectionModuleIntegration(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US408878 PA : Setup - Payment Collection Module Integration");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: click on payment method | payment spoke page  should be displayed");
		Reporter.log(
				"Step 6: click on choose payment method later and continue | payment None provided should be displayed on payment method");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickpaymentMethod();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyPageLoaded();
		spokePage.choosePaymentMethodLater();
		spokePage.clickContinueButton();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyPApageDisplayed();
		paymentArrangementPage.verifyPaymentMethodNotProvidedOnPaymentBlade();
		paymentArrangementPage.clickpaymentMethod();
		spokePage.clickSavedCard();
		spokePage.clickContinueButton();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyPaymentMethodIsProvided();
		paymentArrangementPage.clickpaymentMethod();
		spokePage.clickSavedBank();
		spokePage.clickContinueButton();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyPaymentMethodIsProvided();
	}

	/**
	 * US408878 PA : Setup - Payment Collection Module Integration
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testSetupPaymentCollectionModuleIntegrationAddNewBankCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US408878 PA : Setup - Payment Collection Module Integration");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: click on payment method | payment spoke page  should be displayed");
		Reporter.log(
				"Step 6: click on choose payment method later and continue | payment None provided should be displayed on payment method");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickpaymentMethod();
		SpokePage spokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), spokePage);
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyPaymentMethodIsProvided();
		paymentArrangementPage.clickpaymentMethod();
		addNewCard(myTmoData.getPayment(), spokePage);
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyPaymentMethodIsProvided();
		
	}
	
	/**
	 * US584283 [Continued] PA payment method display logic
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void checkPaymentMethodOnPaymentBladeWhenUserChangesItWithNewBankAndCardWhenFirstInstallmentIsPaidAndSecondIsScheduled(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US584283 [Continued] PA payment method display logic");
		Reporter.log("TestData: Need any PA eligible account with 1st installment in Paid status and 2nd in Scheduled");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: click on payment method | payment spoke page  should be displayed");
		Reporter.log(
				"Step 6: Click on AddNewBank option and fill details. Click Continue button | Payment method should be updated.");
		Reporter.log("Step 7: Check Payment mehtod on Payment blade. | New Payment method should be updated.");
		Reporter.log("Step 8: click on payment method | payment spoke page  should be displayed");
		Reporter.log(
				"Step 9: Click on AddNewCard option and fill details. Click Continue button | Payment method should be updated.");
		Reporter.log("Step 10: Check Payment mehtod on Payment blade. | New Payment method should be updated.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.isFirstInstallmentPaidOrNot();
		paymentArrangementPage.getLastFourDigitsOfSavedPaymentMethodFromPaymentBlade();
		paymentArrangementPage.clickpaymentMethod();
		SpokePage spokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), spokePage);
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyPaymentMethodIsProvided();
		paymentArrangementPage.clickpaymentMethod();
		addNewCard(myTmoData.getPayment(), spokePage);
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyPaymentMethodIsProvided();

	}

	/**
	 * US584283 [Continued] PA payment method display logic
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void checkPaymentMethodOnPaymentBladeWhenUserChangesItWithNoPaymentMethodWhenFirstInstallmentIsPaidAndSecondIsScheduled(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US584283 [Continued] PA payment method display logic");
		Reporter.log("TestData: Need any PA eligible account with 1st installment in Paid status and 2nd in Scheduled");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: click on payment method | payment spoke page  should be displayed");
		Reporter.log(
				"Step 6: Click on 'Remove existing payment mentod' option. Click Continue button | Payment method should be updated.");
		Reporter.log(
				"Step 7: Check Payment mehtod on Payment blade. | 'None Provided' should be updated on payment method blade.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.isFirstInstallmentPaidOrNot();
		paymentArrangementPage.getLastFourDigitsOfSavedPaymentMethodFromPaymentBlade();
		paymentArrangementPage.clickpaymentMethod();
		paymentArrangementPage.clickOnRemoveExistingPaymentMethod();
		paymentArrangementPage.clickContinuebtn();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyPaymentMethodIsProvided();
	}

	/**
	 * US408958 PA : General - TNC page creation
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPATermsAndConditionspage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 4: click on terms and conditions| terms and conditions page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickTermsAndConditionsLink();
		paymentArrangementPage.verifyTnCPageDisplayed();
		paymentArrangementPage.clickTnCBackBtn();
		paymentArrangementPage.verifyPApageDisplayed();
	}
	
	/**
	 * US408872 PA : Setup - Minimum Installment Case
	 * CDCDWG2-65 	PA : Setup - Minimum Installment Case
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupMinimumInstallmentCase(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US408872	PA : Setup - Minimum Installment Case");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: click on payment method | payment spoke page  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
		SpokePage spokePage = new SpokePage(getDriver());

		Double totalBal = paymentArrangementPage.getTotalBal();
		paymentArrangementPage.verifyNoOfInstallmentsDisplayed(totalBal);

		if (Double.compare(totalBal, 2.02) == 0) {
			paymentArrangementPage.verifyInstallementAmount("$1.01");
			verifyInstallmentBladeandPaymentMethod(paymentArrangementPage, paInstallmentDetailsPage, spokePage);
		} else if (Double.compare(totalBal, 2.02) < 0) {
			if (Double.compare(totalBal, 1.01) < 0) {
				paymentArrangementPage.verifyMinimunInstallementCaseErrorDispalyed(PaymentConstants.PA_MIN_INSTALLMENT_CASE_ERROR_MSG);
				paymentArrangementPage.clickCancelButton();
				billAndPaySummaryPage.verifyPageLoaded();
			} else {
				verifyInstallmentBladeandPaymentMethod(paymentArrangementPage, paInstallmentDetailsPage, spokePage);
			}
		} else {
			paymentArrangementPage.verifyInstallementAmount("$1.01");
			verifyInstallmentBladeandPaymentMethod(paymentArrangementPage, paInstallmentDetailsPage, spokePage);
		}
	}

	/**
	 * @param paymentArrangementPage
	 * @param paInstallmentDetailsPage
	 * @param spokePage
	 */
	private void verifyInstallmentBladeandPaymentMethod(PaymentArrangementPage paymentArrangementPage,
			PAInstallmentDetailsPage paInstallmentDetailsPage, SpokePage spokePage) {
		paymentArrangementPage.clickinstallemtBlade();
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.clickOnBackBtn();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.clickpaymentMethod();
		spokePage.verifyPageLoaded();
	}	
	
	/**
	 * US408886	PA : Setup - Balance (Non-Sedona)
	 * CDCDWG2-78	PA : Setup - Balance (Non-Sedona)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupBalanceNonSedonaNotInvoluntarilySuspendedNoPAFee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US408886	PA : Setup - Balance (Non-Sedona)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: Check balance at PA landing page. | Balance at top should be total Balance = total of current + past due balance.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		Double totalBal = paymentArrangementPage.getTotalBal();
		//Add verification for Total balance = current + past due amounts
	}

	/**
	 * US408886	PA : Setup - Balance (Non-Sedona)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupBalanceNonSedonaNotInvoluntarilySuspendedHasPAFee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US408886	PA : Setup - Balance (Non-Sedona)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: Check balance at PA landing page. | Balance at top should be total Balance = total of current + past due balance + PA setup fees.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		Double totalBal = paymentArrangementPage.getTotalBal();
		//Add verification for Total balance = current + past due amounts + PA setup fees
	}

	/**
	 * US408886	PA : Setup - Balance (Non-Sedona)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupBalanceNonSedonaInvoluntarilySuspendedNoPAFee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US408886	PA : Setup - Balance (Non-Sedona)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: Check balance at PA landing page. | Balance at top should be total Balance = total of current + past due balance.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		Double totalBal = paymentArrangementPage.getTotalBal();
		//Add verification for Total balance = current + past due amount
	}

	/**
	 * US408886	PA : Setup - Balance (Non-Sedona)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupBalanceNonSedonaInvoluntarilySuspendedHasPAFee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US408886	PA : Setup - Balance (Non-Sedona)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: Check balance at PA landing page. | Balance at top should be total Balance = total of current + past due balance + PA setup fees.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		Double totalBal = paymentArrangementPage.getTotalBal();
		//Add verification for Total balance = current + past due amounts + PA setup fees
	}

	/**
	 * CDCDWG2-68 PA : Setup - Landing page Alerts
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupLandingPageAlertsForSuspendedLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-68	PA : Setup - Landing page Alerts");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log(
				"Step 5: Check alert on PA hub page. | Alert 'Your account currently contains x suspended line. Your account will be immediately restored and a restoration fee of $xx.xx (provided by backend) will be applied to your future bill.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifySuspendedLinesAlert(
				"Your account currently contains x suspended line. Your account will be immediately restored and a restoration fee of $xx.xx (provided by backend) will be applied to your future bill.");
	}

	/**
	 * CDCDWG2-68 PA : Setup - Landing page Alerts
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupLandingPageAlertsForMoreThan25SuspendedLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-68	PA : Setup - Landing page Alerts");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log(
				"Step 5: Check alert on PA hub page. | Alert 'Your account currently contains x suspended lines. Your account will be immediately restored and a restoration fee will be applied to your future bill.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifySuspendedLinesAlert(
				"Your account currently contains x suspended lines. Your account will be immediately restored and a restoration fee will be applied to your future bill.");
	}	
	
	/**
	 * CDCDWG2-68 PA : Setup - Landing page Alerts
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupLandingPageAlertsForPASetupFee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-68	PA : Setup - Landing page Alerts");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log(
				"Step 5: Check alert on PA hub page. | Alert 'Creating this payment arrangement online incurs a $X convenience fee which will be applied to your future bill.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifySuspendedLinesAlert("Creating this payment arrangement online incurs a $X convenience fee which will be applied to your future bill.");
	}		

	/**
	 * CDCDWG2-68 PA : Setup - Landing page Alerts
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupLandingPageAlertsForAutopayUnenrollment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-68	PA : Setup - Landing page Alerts");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log(
				"Step 5: Check alert on PA hub page. | Alert 'You will automatically be un-enrolled from Autopay.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifySuspendedLinesAlert("You will automatically be un-enrolled from Autopay.");
	}
	
	/**
	 * CDCDWG2-68 PA : Setup - Landing page Alerts
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupLandingPageAlertsForChoosePaymentMethodLater(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-68	PA : Setup - Landing page Alerts");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log(
				"Step 5: Check alert on PA hub page. | Alert 'You must add a payment method or make a one-time payment before the installment due date(s).");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifySuspendedLinesAlert("You must add a payment method or make a one-time payment before the installment due date(s).");
	}

	/**
	 * CDCDWG2-42 - Entry point logic.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAEntryPointLogic(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-68	PA : Setup - Landing page Alerts");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		HomePage homePage = new HomePage(getDriver());
		homePage.clickSetupPaymentArrangementLink();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
	}

	/**
	 * CDCDWG2-42 - Entry point logic.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAEntryPointLogicViaBilling(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-68	PA : Setup - Landing page Alerts");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on Bill Link | Bill and Pay page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickBillingLink();
		BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
		billAndPaySummaryPage.verifyPageLoaded();
		billAndPaySummaryPage.clickPaymentArrangementShortcut();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
	}

	/**
	 * CDCDWG2-42 - Entry point logic.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAEntryPointLogicViaOTP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-68	PA : Setup - Landing page Alerts");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on Make a payment | OTP page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPaymentArrangementPage(myTmoData);
	}

	/**
	 * CDCDWG2-42 - Entry point logic.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAEntryPointLogicIneligibleUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-68	PA : Setup - Landing page Alerts");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4: Goto payment arrangement page | User should be redirected to Billing page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);
		getDriver().get(System.getProperty("environment") + "/paymentarrangement");
		BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
		billAndPaySummaryPage.verifyPageLoaded();
	}

	/**
	 * CDCDWG2-42 - Entry point logic.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAEntryPointLogicMoreThan30DaysPastDue(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-68	PA : Setup - Landing page Alerts");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on Payment Arrangement link | Modal should be displayed");
		Reporter.log("Step 4: click on Continue | It should redirect to Onetimepayment page.");
		
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickSetupPaymentArrangementLink();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verify30DayPastDueOnPAInterruptModal(PaymentConstants.INTERRUPT_MODAL_30_DAYS_PAST_DUE);
	}

	/**
	 * CDCDWG2-268 PA: Setup agree and submit CTA.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "sreepa"})
	public void testPAsetupWithoutPaymentMethod(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 5: Remove the payment method| Payment method should be removed");
		Reporter.log("Step 5: Click submit | warning message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
	
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickpaymentMethod();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.Removestoredpaymentmethods();
		otpSpokePage.ClickContinuebutton();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.clickAgreeNSubmitBtn();
		paymentArrangementPage.verifypaymentwarningesclamation();
		paymentArrangementPage.verifyaddpaymentmethodwarning();
	}	

	/**
	 * CDCDWG2-72	PA : Setup - Cancel button
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Regular account. PA eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupCancelButton(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US284825	GLUE Light Reskin - PA Exit Modal");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on payment arrangement |Payment arrangement page should be loaded");
		Reporter.log("Step 4: click on Cancel button | Cancel(Exit) modal should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickCancelButton();
		paymentArrangementPage.verifyCancelModal();
		paymentArrangementPage.verifyCancelModalButtonsCTA();
		paymentArrangementPage.clickNoCTAOnCancelModal();
		paymentArrangementPage.verifyPApageDisplayed();
		paymentArrangementPage.clickCancelButton();
		paymentArrangementPage.verifyCancelModal();
		paymentArrangementPage.clickYesCTAOnCancelModal();
		oneTimePaymentPage.verifyPageLoaded();
	}
	
	/**
	 * CDCDWG2-268 - PA: Setup agree and submit CTA
	 * Sprint - 20
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testThatAgreeAndSubmitIsActiveByDefaultOnPALandingPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-268 - PA: Setup agree and submit CTA");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Verify the Agree and Submit CTA| Agree and Submit should be displayed as Active/Enabled");
		Reporter.log("================================");
		
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyAgreeAndSubmitButton();
		paymentArrangementPage.clickAgreeNSubmitBtn();
		paymentArrangementPage.verifyAgreeAndSubmitBtnIsActive();
	
	}
	
	/**
	 * CDCDWG2-268 - PA: Setup agree and submit CTA
	 * Sprint - 20
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testThatMessageWhenThereIsNOPaymentMethod(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-268 - PA: Setup agree and submit CTA");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Verify the Agree and Submit CTA| Agree and Submit should be displayed as Active/Enabled");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickAgreeNSubmitBtn();
		paymentArrangementPage.verifyAddPaymentMethodMessage();
	
	}
	
	/**
	 * CDCDWG2-268 - PA: Setup agree and submit CTA
	 * Sprint - 20
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testThatUserWillBeAbleToSubmitPASecured(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-268 - PA: Setup agree and submit CTA");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Click on Agree and Submit| Should be able to successfully set up PA");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		if (!paymentArrangementPage.isPASecuredOrNot()) {
			paymentArrangementPage.clickpaymentMethod();
			SpokePage otpSpokePage = new SpokePage(getDriver());
			otpSpokePage.verifyPageLoaded();
			otpSpokePage.clickAddCard();
			AddCardPage addCardPage = new AddCardPage(getDriver());
			addCardPage.fillCardInfo(myTmoData.getPayment());
			otpSpokePage.clickContinueButton();
			paymentArrangementPage.verifyPageLoaded();
		}
		paymentArrangementPage.clickAgreeNSubmitBtn();
		paymentArrangementPage.verifyPaymentArrangementSetup();
	}

	/**
	 * CDCDWG2-268 - PA: Setup agree and submit CTA
	 * Sprint - 20
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testThatUserWillBeAbleToSubmitPAUNSecured(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-268 - PA: Setup agree and submit CTA");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Verify payment method | There should not be any payment method attatched");
		Reporter.log("Step 5 : Click on Agree and Submit| Should be able to successfully set up PA");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		if (paymentArrangementPage.isPASecuredOrNot()) {
			paymentArrangementPage.clickpaymentMethod();
			SpokePage otpSpokePage = new SpokePage(getDriver());
			otpSpokePage.verifyPageLoaded();
			otpSpokePage.choosePaymentMethodLater();
			otpSpokePage.clickContinueButton();
			paymentArrangementPage.verifyPageLoaded();
		}
		paymentArrangementPage.clickAgreeNSubmitBtn();
		paymentArrangementPage.verifyPaymentArrangementSetup();
	}

	
	/**
	 * CDCDWG2 - 96 - PA : Modify - Cancel button+
	 * Sprint - 20
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testTheCancelCTAOnPAModifyPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2 - 96 - PA : Modify - Cancel button+");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Click on Make a payment| 'Review payment arrangement' CTA should be displayed");
		Reporter.log("Step 4 : click 'Review payment arrangement'| PA modify page should be displayed");
		Reporter.log("Step 5 : Verify the CTAs	Cancel button| should be Enabled and the spacing between the Agree & Submit and Cancel buttons and from the bottom of the page is consistent with the attached redline and compA");
		
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyCancelCTA();
	}
	
	/**
	 * CDCDWG2 - 96 - PA : Modify - Cancel button+
	 * Sprint - 20
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void validateTheCancelCTAOnPAModifyPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2 - 96 - PA : Modify - Cancel button+");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Click on Make a payment| 'Review payment arrangement' CTA should be displayed");
		Reporter.log("Step 4 : click 'Review payment arrangement'| PA modify page should be displayed");
		Reporter.log("Step 5 : Verify the CTAs	Cancel button| should be Enabled and the spacing between the Agree & Submit and Cancel buttons and from the bottom of the page is consistent with the attached redline and compA");
		Reporter.log("Step 6 : click on cancel CTA| \"Modal should be displayed with message:Are you sure you want to leave this page? Any changes you have made will not be saved.\"\"\n" + "CTA1: YES\n" +"CTA2: NO\"");
		Reporter.log("Step 6 : click on Yes | It will navigate to previous page one time payment page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyReviewPAModal();
		oneTimePaymentPage.clickReviewPaymentArrangementCTA();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyCancelCTA();
		paymentArrangementPage.verifyCancelYesOrNoPopUp();
		paymentArrangementPage.clickYesCTAOnCancelModal();
		oneTimePaymentPage.verifyPageLoaded();
	}
	
	/**
	 * CDCDWG2-268 - PA: Setup agree and submit CTA
	 * Sprint - 20
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCTAsOnModalForPAModifyPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-268 - PA: Setup agree and submit CTA");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Click on Make a payment| 'Review payment arrangement' CTA should be displayed");
		Reporter.log("Step 4 : click 'Review payment arrangement'| PA modify page should be displayed");
		Reporter.log("Step 5 : Verify the CTAs	Cancel button| should be Enabled and the spacing between the Agree & Submit and Cancel buttons and from the bottom of the page is consistent with the attached redline and compA");
		Reporter.log("Step 6 : click on cancel CTA| \"Modal should be displayed with message:Are you sure you want to leave this page? Any changes you have made will not be saved.\"\"\n" + "CTA1: YES\n" +"CTA2: NO\"");
		Reporter.log("Step 6 : click on No | It will navigate to previous page one time payment page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyReviewPaymentArrangementCTA();
		paymentArrangementPage.clickReviewPaymentArrangementCTA();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyCancelCTA();
		paymentArrangementPage.verifyCancelYesOrNoPopUp();
		paymentArrangementPage.clickNoCTAOnCancelModal();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
	
	}
	
	/**
	 * CDCDWG2-313 -(ATDD) PA : Setup - Installment defaults
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAmountIsDefaultedToTotalBalance(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-313 -(ATDD) PA : Setup - Installment defaults");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Click on Make a payment arrangement | 'Payment arrangement' page should be displayed");
		Reporter.log("Step 5 : Verify the amount on PA page | Amount should default to Total balance");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		PaymentArrangementPage paymentArrangementPage = navigateDirectlyToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyTotalBalisDefaulted();
	}	
	
	/**
	 * CDCDWG2-313 -(ATDD) PA : Setup - Installment defaults
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testInstallmentsAreDefaultedToMaxTotalBalance(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-313 -(ATDD) PA : Setup - Installment defaults");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Click on Make a payment arrangement | 'Payment arrangement' page should be displayed");
		Reporter.log("Step 5 : Verify the amount on PA page | Amount should default to Total balance");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		PaymentArrangementPage paymentArrangementPage = navigateDirectlyToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyTotalBalisDefaulted();
		paymentArrangementPage.verifyInstallmentDatesAreDefaultedToMaxTotalBal();
	}	

	/**
	 * CDCDWG2-89 PA : Modify - Agree & Submit button +
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAModifyAgreeAndSubmitIsInActiveByDefault(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-89 PA : Modify - Agree & Submit button +");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Verify the Agree and Submit CTA| Agree and Submit should be displayed as InActive/Disabled");
		Reporter.log("================================");
		
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.refreshPage();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyAgreeAndSubmitButton();
		paymentArrangementPage.verifyAgreeAndSubmitBtnIsInactive();
	}

	/**
	 * CDCDWG2-89 PA : Modify - Agree & Submit button +
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAModifyAgreeAndSubmitIsActiveByAlteringPaymentMethod(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-89 PA : Modify - Agree & Submit button +");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Verify the Agree and Submit CTA| Agree and Submit should be displayed as InActive/Disabled");
		Reporter.log("================================");
		
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.refreshPage();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyAgreeAndSubmitButton();
		paymentArrangementPage.verifyAgreeAndSubmitBtnIsInactive();
		paymentArrangementPage.clickpaymentMethod();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyPageLoaded();
		spokePage.choosePaymentMethodLater();
		spokePage.clickContinueButton();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyAgreeAndSubmitBtnIsActive();
	}
	
	/**
	 * CDCDWG2-90 PA :Modify - Modify page Alerts
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAModifyPageAlerts(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-90 PA :Modify - Modify page Alerts");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Verify the alert from secured to unsecured PA for suspended customers in spoke page| ");
		Reporter.log("================================");
	
	PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
	paymentArrangementPage.clickpaymentMethod();
	SpokePage spokePage = new SpokePage(getDriver());
	spokePage.verifyPageLoaded();
	paymentArrangementPage.clickOnRemoveExistingPaymentMethod();
	paymentArrangementPage.verifyReSuspendedAlertSpoke("By removing a payment method your account will be immediately resuspended");
	paymentArrangementPage.verifyPageLoaded();
	paymentArrangementPage.verifyReSuspendedAlertLanding("By removing a payment method your account will be immediately resuspended");
	paymentArrangementPage.clickAgreeNSubmitBtn();
	PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
	paConfirmationPage.verifyPageLoaded();
	paymentArrangementPage.verifyReSuspendedAlertConfirmation("Your account has been re-suspended. Please re-attach a payment method to this payment arrangement and call Customer Care [Open “Call Us” page in App or call Care directly] to restore your account.");
	
	}
	
	/**
	 * CDCDWG2-98 PA : Modify - State of Installments +
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAModifyStateOfInstallments(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-98 PA : Modify - State of Installments +");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Verify the State of Installments| Installment state should be displayed");
		Reporter.log("Step 5 : Verify the Installment state: [If 1st Inst Date > Current Date]| 'Scheduled' Installment state should be displayed");
		Reporter.log("Step 6 : Verify the Installment state: [If 1st Inst Date = Current Date]| 'Processing' Installment state should be displayed");
		Reporter.log("Step 7 : Verify the Installment state: [If 1st Inst Date < Current Date]| 'Missed|Paid' Installment state should be displayed");
		
		Reporter.log("================================");
		
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyInstallementStatusByInstDate();
	}

	/**
	 * CDCDWG2-101	PA : General - AP 5 Dollar Alert
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAGeneralAP5DollarAlert(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-101	PA : General - AP 5 Dollar Alert");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Verify the 5$ Autopay pay alert| Autopay Un-enrollment alert should be displayed");
		
		Reporter.log("================================");
		
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyAutopayUnrollAlert();
	}

	/**
	 * CDCDWG2-88 PA : Modify - Blackout Treatment.
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAModifyBlackoutTreatmentSecure(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-88 PA : Modify - Blackout Treatment.");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Verify the PA page for PA in blackout period and 1 installment left| Installment state should be 'Processing'");
		Reporter.log("Step 5 : Verify the Edit link on payment blade| Edit link should not be displayed");;
		Reporter.log("Step 6 : Verify the T & C link | T&C link and verbiage should not be displayed");
		Reporter.log("Step 7 : Verify Agree & Submit CTA | Agree & Submit CTA should be displayed and disabled");
		Reporter.log("================================");
		
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyInstallementStatus("Processing");
		paymentArrangementPage.verifyEditLinkNotDisplayedOnPaymentBlade();
		paymentArrangementPage.verifyTnCLinkNotDisplayed();
		paymentArrangementPage.verifyAgreeAndSubmitBtnIsInactive();
	}

	/**
	 * CDCDWG2-88 PA : Modify - Blackout Treatment.
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAModifyBlackoutTreatmentUnSecure(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-88 PA : Modify - Blackout Treatment.");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Verify the PA page for PA in blackout period and 1 installment left| Installment state should be 'Processing'");
		Reporter.log("Step 5 : Verify the Edit link on payment blade| Edit link should not be displayed");;
		Reporter.log("Step 6 : Verify the T & C link | T&C link and verbiage should not be displayed");
		Reporter.log("Step 7 : Verify Agree & Submit CTA | 'Make a payment' CTA should be displayed");
		Reporter.log("================================");
		
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyInstallementStatus("Processing");
		paymentArrangementPage.verifyEditLinkNotDisplayedOnPaymentBlade();
		paymentArrangementPage.verifyTnCLinkNotDisplayed();
		paymentArrangementPage.verifyMakeAPaymentCTA();
		paymentArrangementPage.clickMakeaPaymentCTA();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
	}


	/**
	 * CDCDWG2-93 PA : Modify - Missed Payment page +
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAModifyMissedPaymentPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-93 PA : Modify - Missed Payment page +");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Verify the PA page for Missed PA | Installment state should be 'Missed'");
		Reporter.log("Step 5 : Verify the Alerts| Missed PA Alerts should be displayed");;
		Reporter.log("Step 6 : Verify the Make a payment CTA | Make a payment for $XX.XX should be displayed");
		Reporter.log("Step 7 : Click Make a payment CTA | Should be redirected to OTP page");
		Reporter.log("Step 8 : Verify amount displayed on amount blade | Missed PA installment amount should be displayed");
		
		Reporter.log("================================");
		
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyInstallementStatus("Missed");
		paymentArrangementPage.verifyAlertForMissedPayment(PaymentConstants.PA_MODIFY_MISSED_ALERT_1);
		paymentArrangementPage.verifyAlertForMissedPayment(PaymentConstants.PA_MODIFY_MISSED_ALERT_2);
		paymentArrangementPage.verifyMakeAPaymentCTA();
	}

	/**
	 * CDCDWG2-751 PA: Modify OTP 1.5 Handoff for Missed installment
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAModifyOTPhandoffForMissedPA(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-751 PA: Modify OTP 1.5 Handoff for Missed installment");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Verify the PA page for Missed PA | Installment state should be 'Missed'");
		Reporter.log("Step 6 : Verify the Make a payment CTA | Make a payment for $XX.XX should be displayed");
		Reporter.log("Step 7 : Click Make a payment CTA | Should be redirected to OTP page");
		Reporter.log("Step 8 : Verify amount displayed on amount blade | Missed PA installment amount should be displayed");
		Reporter.log("================================");
		
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage =navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyInstallementStatus("Missed");
		String missedPAamount = paymentArrangementPage.getAmountForMissedPA();
		paymentArrangementPage.verifyMakeAPaymentCTA();
		paymentArrangementPage.clickMakeaPaymentCTA();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyAmountDueEqualsPastDueOnBlade(missedPAamount);
	}
}