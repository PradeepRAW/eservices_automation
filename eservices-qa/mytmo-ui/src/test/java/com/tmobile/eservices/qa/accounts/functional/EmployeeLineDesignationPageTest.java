package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.EmployeeLineDesignationPage;

public class EmployeeLineDesignationPageTest extends GlobalCommonLib {

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testChangeCurrentLineToEmployee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with employee line designation blade");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on Employee Line Designation link | EmployeeLineDesignation page should be displayed");
		Reporter.log("6. Click on  employee line details link | employee line details should be displayed");
		Reporter.log(
				"7. Click on Employee  dependent radio button and save | verify employee dependent status on EmployeeLineDesignation page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		EmployeeLineDesignationPage employeeLineDesignationPage = navigateToEmployeeLineDesignationPage(myTmoData,
				"Employee Line Designation");
		employeeLineDesignationPage.clickFirstEmployeeLineDetailsLink();
		// verify employee line details
		employeeLineDesignationPage.clickEmployeeDependantRadioBtn();
		employeeLineDesignationPage.verifyEmployeeLineDesignationPage();
		employeeLineDesignationPage.verifyStatusForDependant();

	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testChangeCurrentLineToNonDependent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with employee line designation blade");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on Employee Line Designation link | EmployeeLineDesignation page should be displayed");
		Reporter.log("6. Click on  Employee line details link | employee line details should be displayed");
		Reporter.log(
				"7. Click on Employee NON Dependent radio button and save | verify Employee  NON Dependent status on EmployeeLineDesignation page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		EmployeeLineDesignationPage employeeLineDesignationPage = navigateToEmployeeLineDesignationPage(myTmoData,
				"Employee Line Designation");
		employeeLineDesignationPage.clickFirstEmployeeLineDetailsLink();
		// verify employee line details
		employeeLineDesignationPage.clickNonDependantRadioBtn();
		employeeLineDesignationPage.verifyEmployeeLineDesignationPage();
		employeeLineDesignationPage.verifyStatusForNonDependant();
	}

	/**
	 * Ensure user can edit employee line designation details. Ensure employee line
	 * designation details appear correctly.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyAndEditEmployeeLineDetails(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with employee line designation blade");
		Reporter.log("Test steps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to logged in  Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on Employee Line Designation link | EmployeeLineDesignation page should be displayed");
		Reporter.log(
				"6. Click on  Employee line details link and verify save button enabled | save button should be enabled");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		EmployeeLineDesignationPage employeeLineDesignationPage = navigateToEmployeeLineDesignationPage(myTmoData,
				"Employee Line Designation");
		employeeLineDesignationPage.clickonEmployeeLineDetails();
		employeeLineDesignationPage.verifySaveButtonEnabled();
	}
}
