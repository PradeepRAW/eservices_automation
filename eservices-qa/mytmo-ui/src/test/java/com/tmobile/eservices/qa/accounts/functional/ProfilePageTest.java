package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.global.LanguageSettingsPage;

public class ProfilePageTest extends GlobalCommonLib {

	/**
	 * US489055#MyTMO - Add preferred language toggle to Profile
	 * DE235111:Devb02-Profile-language setting -toggle issue
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyPreferredLanguageToggleInProfile(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPreferredLanguageToggleInProfile method called in Profile Page Test");

		Reporter.log("================================");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on language settings link | Language settings page should be displayed");
		Reporter.log("6. Select english language radio button and click on save | Profile page should be displayed");
		Reporter.log("7. Click on language settings link | Language settings page should be displayed");
		Reporter.log(
				"8. Verify is english language radio button selected | English language radio button should be selected");
		Reporter.log("9. Navigate control to back page | Profile page should be displayed");
		Reporter.log("10.Click on language settings link | Language settings page should be displayed");
		Reporter.log("11.Select spanish language radio button and click on save | Profile page should be displayed");
		Reporter.log("12.Click on language settings link | Language settings page should be displayed");
		Reporter.log(
				"13.Verify is spanish language radio button selected | Spanish language radio button should be selected");

		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink("Language Settings");

		LanguageSettingsPage languageSettingsPage = new LanguageSettingsPage(getDriver());
		languageSettingsPage.verifyLanguageSettingsPage();
		languageSettingsPage.selectEnglishRadiobutton();

		profilePage.verifyProfilePage();
		profilePage.clickProfilePageLink("Language Settings");

		languageSettingsPage = new LanguageSettingsPage(getDriver());
		languageSettingsPage.verifyLanguageSettingsPage();
		languageSettingsPage.isEnglishRadioBtnSelected();
		languageSettingsPage.navigateBack();

		profilePage.verifyProfilePage();
		profilePage.clickProfilePageLink("Language Settings");

		languageSettingsPage = new LanguageSettingsPage(getDriver());
		languageSettingsPage.verifyLanguageSettingsPage();
		languageSettingsPage.selectSpanishRadiobutton();

		profilePage.verifyProfilePage();
		profilePage.clickProfilePageLink("Language Settings");

		languageSettingsPage = new LanguageSettingsPage(getDriver());
		languageSettingsPage.verifyLanguageSettingsPage();
		languageSettingsPage.isSpanishRadioBtnSelected();
	}
}
