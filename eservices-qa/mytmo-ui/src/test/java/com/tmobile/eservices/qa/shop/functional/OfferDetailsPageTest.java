package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.OfferDetailsPage;
import com.tmobile.eservices.qa.pages.shop.PLPPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class OfferDetailsPageTest extends ShopCommonLib {

	/**
	 * US174113 - Curated shop: Offer details page :As a TMO customer, When I
	 * select the CTA on marquee banner I should be navigated to offer details
	 * page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testOfferDetailPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : Verify offer details page");
		Reporter.log("Data Condition | IR STD PAH Customer With Promo Offer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop tab | Shop page should be displayed");
		Reporter.log("5. Click on marquee banner CTA| Offer details page should be displayed");
		Reporter.log(
				"6. Verify frequentily asked questions section| Frequentily asked questions section should be displayed");
		Reporter.log("7. Verify legal text | Legal text should be displayed");
		Reporter.log("8. Verify error message | Error message should be displayed");
		Reporter.log("9. Verify continue without the offer link |Continue without the offer link should be displayed");
		Reporter.log("10. Click on continue without the offer link |Product List page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickMarqueeBannerCTA();
		OfferDetailsPage offerDetailsPage = new OfferDetailsPage(getDriver());
		offerDetailsPage.verifyOfferDetailsPage();
		offerDetailsPage.verifyFrequentilyAskedQuestions();
		offerDetailsPage.clickFrequentilyAskedQuestions();
		offerDetailsPage.verifyLegalText();
		offerDetailsPage.verifyNotInterestedLink();
		offerDetailsPage.clickNotInterestedlink();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();

	}

	/**
	 * US321966 - MyTMO - Additional Terms - Offer Details Page: Promo Product
	 * Info Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testPromoProductsInformationDisplayed(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US321966 - MyTMO - Additional Terms - Offer Details Page: Promo Product Info Display");
		Reporter.log("Data Condition | IR STD PAH Customer With Promo Offer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop tab | Shop page should be displayed");
		Reporter.log("5. Click on marquee banner CTA | Offer details page should be displayed");
		Reporter.log("6. Verify Promo product name | Promo product name should be displayed");
		Reporter.log("7. Verify Promo product FRP price | Promo product FRP price should be displayed");
		Reporter.log("8. Verify Promo product EIP down payment | Promo product EIP down payment should be displayed");
		Reporter.log(
				"9. Verify Promo product EIP loan term length | Promo product EIP loan term length should be displayed");
		Reporter.log(
				"10. Verify Promo product EIP monthly payment amount | Promo product EIP monthly payment amount should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());

		shopPage.clickOnHeroBannerBuyButton();
		OfferDetailsPage offerDetailsPage = new OfferDetailsPage(getDriver());
		offerDetailsPage.verifyOfferDetailsPage();
		offerDetailsPage.verifyProductName(myTmoData.getDeviceName());
		offerDetailsPage.verifyEIPDownPayment();
		offerDetailsPage.verifyDeviceEIPInstallmentTerms();
		offerDetailsPage.verifyEIPMonthlyPayment();
	}

	/**
	 * US320451 - MyTMO - Additional Terms - Offer Details Page: Trade-In
	 * Product Total Promo Value and Monthly Promo Value Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
	public void testTotalPromoValueAndMonthlyPromoValueDisplayed(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : US320451: Verify Total Promo value and Monthly Promo value is Dispalyed for Trade-In products");
		Reporter.log("Data Condition | IR STD PAH Customer With Promo Offer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop tab | Shop page should be displayed");
		Reporter.log("5. Click on marquee banner CTA | Offer details page should be displayed");
		Reporter.log("6. Verify Trade-In device | Trade-In device should be displayed");
		Reporter.log("7. Verify Trade-in product name | Trade-in product name should be displayed");
		Reporter.log(
				"8. Verify Trade-in product Total trade-in value | Trade-in product Total trade-in value should be displayed");
		Reporter.log(
				"9. Verify Trade-in product Monthly trade-in value | Trade-in product Monthly trade-in value should be displayed");
		Reporter.log("10. Verify Trade-in product promo length | Trade-in product promo length should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToOfferDetailsPage(myTmoData);
		OfferDetailsPage offerDetailsPage = new OfferDetailsPage(getDriver());
		offerDetailsPage.verifyTradeInDeviceDetails();
		String details = offerDetailsPage.getTradeInDeviceDetails();
		offerDetailsPage.verifyTradeInDevice(details);
		offerDetailsPage.verifyTradeInProductName(details);
		offerDetailsPage.verifyTradeInValue(details);
		offerDetailsPage.verifyMonthlyTradeInValue(details);
		offerDetailsPage.verifyTradeInPromoLength(details);
	}

	/**
	 * US369710#ODP - CTA's calling ability
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testOfferDetailsPagePrimaryANDSecondaryCTAs(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US369710 - Verify Offer Details Page Primary AND Secondary CTAs");
		Reporter.log("Data Condition | STD PAH Customer in Mobile only ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop tab | Shop page should be displayed");
		Reporter.log("5. Click on marquee banner CTA | Offer details page should be displayed");
		Reporter.log("6. Verify Primary AND Secondary CTAs | Primary AND Secondary CTAs should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		OfferDetailsPage offerDetailsPage = navigateToOfferDetailsPage(myTmoData);
		offerDetailsPage.verifyPrimaryCTA();
		offerDetailsPage.verifyNotInterestedLink();
	}

	/**
	 * US299166:Remove Footer from Offer Details Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testFooterNotDisplayedInOfferdetailsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US299166:Remove Footer from Offer Details Page");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on marquee banner CTA | Offer details page should be displayed");
		Reporter.log(
				"4. Verify Footer elements, i.e Support, Contact Us, Store Locator | Footer elements should not displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickReadyToSwitchButton();
		OfferDetailsPage offerDetailsPage = new OfferDetailsPage(getDriver());
		offerDetailsPage.verifyOfferDetailsPage();
		offerDetailsPage.verifyOfferDetailsPageFooterNotDisplayed();
	}

	/**
	 * US393722 : AAL: Entry points for OFFER DETAILS PAGE CTAs (Upgrade CTA)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL })
	public void testOfferDetailsPageUpgradeCTAForFamilyID(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case#US393722 : AAL: Entry points for OFFER DETAILS PAGE CTAs (ADD A LINE CTA)");
		Reporter.log("Data Condition | offer/deep link is for a family ID");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop tab | Shop page should be displayed");
		Reporter.log("5. Click on marquee banner CTA | Offer details page should be displayed");
		Reporter.log("6. Click on Upgrade button | PLP page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickOnHeroBannerSellButton();
		OfferDetailsPage offerDetailsPage = new OfferDetailsPage(getDriver());
		offerDetailsPage.verifyOfferDetailsPage();
		offerDetailsPage.clickOnUpgradeCTA();

		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
	}

	/**
	 * US531493 -Remove 'Not Interested' Link from Offer Details Page	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testNotInterestedLinkNotDisplayedInOfferDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US531493 -Remove 'Not Interested' Link from Offer Details Page");
		Reporter.log("Data Condition | PAH User and User must offer eligible");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop tab | Shop page should be displayed");
		Reporter.log("5. Click on marquee banner CTA | Offer details page should be displayed");
		Reporter.log("6. Verify NotInterested CTA | Not Interested CTA should not be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickOnHeroBannerBuyButton();
		OfferDetailsPage offerDetailsPage = new OfferDetailsPage(getDriver());
		offerDetailsPage.verifyOfferDetailsPage();
		offerDetailsPage.verifyNotInterestedCTA();
	}
		
}
