/**
 * 
 */
package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.TradeInPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;


/**
 * @author ksrivani
 *
 */
public class TradeInPageTest extends TmngCommonLib {

	/***
	 * US485233 Trade-in| Device Details
	 * US485235 Trade-in | Get Estimate
	 * US485218 Trade-in | Integration of Hero like component
	 * TC ID: TC276732
	 * TC ID: TC285982
	 * TC ID: TC285989
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT,Group.TMNG})
	public void testTradeInFieldValidations(TMNGData tMNGData) {
		Reporter.log("US485233 Trade-in| Device Details");
		Reporter.log("US485235 Trade-in | Get Estimate");
		Reporter.log("US485218 Trade-in | Integration of Hero like component");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify We’ll help you join link| We’ll help you join link should be displayed");
		Reporter.log("4. Click on We’ll help you join link | We’ll help you join page should be displayed");
		Reporter.log("5. Verify and click Trade in program link at the footer | Verified and Clicked on Trade in program link");
		Reporter.log("6. Verify Trade In page is displayed | Trade In page should be displayed");
		Reporter.log("7. Verify the display of Trade In header description | Trade In Header should be displayed successfully.");
		Reporter.log("8. Verify the display of description | Description should be displayed successfully.");
		Reporter.log("9. Verify Device Carrier text box | Device Carrier text box is displayed successfully");
		Reporter.log("10. Enter the value in Device carrier text box | Entered value in the Device carrier text box");
		Reporter.log("11. Verify autocomplete list of results having the matching device carrier | All the results loaded should have the matching criteria");
		Reporter.log("12. Verify Device Carrier type ahead list | Selected the value from the type ahead list");
		Reporter.log("13. Verify Device Manufacturer text box | Device Manufacturer text box is displayed successfully");
		Reporter.log("14. Enter the value in Device manufacturer text box | Entered value in the Device manufacturer text box");
		Reporter.log("15. Verify autocomplete list of results having the matching device manufacturer | All the results loaded should have the matching criteria");
		Reporter.log("16. Verify Device Manufacturer type ahead list | Selected the value from the type ahead list");
		Reporter.log("17. Verify Device Model text box | Device Model text box is displayed successfully");
		Reporter.log("18. Enter the value in Device model text box | Entered value in the Device model text box");
		Reporter.log("19. Verify autocomplete list of results having the matching device model | All the results loaded should have the matching criteria");
		Reporter.log("20. Verify Device Model type ahead list | Selected the value from the type ahead list");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToTradeInPage(tMNGData);
		TradeInPage tradeInPage = new TradeInPage(getDriver());
		tradeInPage.verifyTradeInHeaderDescription();
		tradeInPage.verifyDeviceCarrierTextBox();
		tradeInPage.verifyAutoCompleteSearchResultsForDeviceCarrierSelectorField(tMNGData.getDeviceCarrier().substring(0, 1));
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceCarrier();
	    tradeInPage.verifyDeviceManufacturerTextBox();
		tradeInPage.verifyAutoCompleteSearchResultsForDeviceManufacturerSelectorField(tMNGData.getDeviceManufacturer().substring(0, 1));
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceManufacturer();
		tradeInPage.verifyDeviceModelTextBox();
		tradeInPage.verifyAutoCompleteSearchResultsForDeviceModelSelectorField(tMNGData.getDeviceModel().substring(0, 1));
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceModel();
		}
	
	
	/***
	 * US485233 Trade-in| Device Details
	 * US485235 Trade-in | Get Estimate
	 * US485218 Trade-in | Integration of Hero like component
	 * TC ID: TC285977
	 * TC ID: TC285983
	 * TC ID: TC281691
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT,Group.TMNG})
	public void testHideDescriptionWhenTradeInSuccessful(TMNGData tMNGData) {
		Reporter.log("US485233 Trade-in| Device Details");
		Reporter.log("US485235 Trade-in | Get Estimate");
		Reporter.log("US485218 Trade-in | Integration of Hero like component");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify We’ll help you join link| We’ll help you join link should be displayed");
		Reporter.log("4. Click on We’ll help you join link | We’ll help you join page should be displayed");
		Reporter.log("5. Verify and click Trade in program link at the footer | Verified and Clicked on Trade in program link");
		Reporter.log("6. Verify Trade In page is displayed | Trade In page should be displayed");
		Reporter.log("7. Enter the value in Device carrier text box | Entered value in the Device carrier text box");
		Reporter.log("8. Verify Device Carrier type ahead list | Selected the value from the type ahead list");
		Reporter.log("9. Enter the value in Device manufacturer text box | Entered value in the Device manufacturer text box");
		Reporter.log("10. Verify Device Manufacturer type ahead list | Selected the value from the type ahead list");
		Reporter.log("11. Enter the value in Device model text box | Entered value in the Device model text box");
		Reporter.log("12. Verify Device Model type ahead list | Selected the value from the type ahead list");
		Reporter.log("13. Click on 'Go' CTA | Clicked on Go button");
		Reporter.log("14. Verify the description is hidden in Get Estimate page | Description must be hidden, once clicked on Go button");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToTradeInPage(tMNGData);
		TradeInPage tradeInPage = new TradeInPage(getDriver());
		tradeInPage.verifyDeviceCarrierTextBox();
		tradeInPage.setTextForDeviceCarrierSelectorSearchTextBar(tMNGData.getDeviceCarrier());
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceCarrier();
		tradeInPage.setTextForDeviceManufacturerSelectorSearchTextBar(tMNGData.getDeviceManufacturer());
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceManufacturer();
		tradeInPage.setTextForDeviceModelSelectorSearchTextBar(tMNGData.getDeviceModel());
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceModel();
		tradeInPage.clickGetEstimateButton();
		tradeInPage.verifyTradeInDescriptionNotDisplayed();
	}
	
	/***
	 * US485233 Trade-in| Device Details
	 * US485235 Trade-in | Get Estimate
	 * US485218 Trade-in | Integration of Hero like component
	 * TC ID: TC285979
	 * TC ID: TC285984
	 * TC ID: TC284996
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT,Group.TMNG})
	public void testTMNGTradeInFlowToShopPage(TMNGData tMNGData) {
		Reporter.log("US485233 Trade-in| Device Details");
		Reporter.log("US485235 Trade-in | Get Estimate");
		Reporter.log("US485218 Trade-in | Integration of Hero like component");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify We’ll help you join link| We’ll help you join link should be displayed");
		Reporter.log("4. Click on We’ll help you join link | We’ll help you join page should be displayed");
		Reporter.log("5. Verify and click Trade in program link at the footer | Verified and Clicked on Trade in program link");
		Reporter.log("6. Enter the value in Device carrier text box | Entered value in the Device carrier text box");
		Reporter.log("7. Verify removing one character from entered text | Successfully removed a character from entered Device carrier inorder to view typeahead list");
		Reporter.log("8. Verify Device Carrier type ahead list | Selected the value from the type ahead list");
		Reporter.log("9. Enter the value in Device manufacturer text box | Entered value in the Device manufacturer text box");
		Reporter.log("10. Verify removing one character from entered text | Successfully removed a character from entered Device manufacturer inorder to view typeahead list");
		Reporter.log("11. Verify Device Manufacturer type ahead list | Selected the value from the type ahead list");
		Reporter.log("12. Enter the value in Device model text box | Entered value in the Device model text box");
		Reporter.log("13. Verify removing one character from entered text | Successfully removed a character from entered Device model inorder to view typeahead list");
		Reporter.log("14. Verify Device Model type ahead list | Selected the value from the type ahead list");
		Reporter.log("15. Verify 'Go' CTA | Go button is enabled");
		Reporter.log("16. Click on 'Go' CTA | Clicked on Go button");
		Reporter.log("17. Verify the success header of the estimated offer in Get Estimate page | Success header with the estimated offer must be displayed");
		Reporter.log("18. Verify the success description in Get Estimate page | Success description must be displayed");
		Reporter.log("19. Verify the condition terms presented in Get Estimate page | Conditions terms are displayed, when estimated value is greater than Zero");
		Reporter.log("20. Click 'Shop Now' CTA | Clicked on Shop Now CTA");
		Reporter.log("21. Verify the display of Product listing page, upon clicking 'Shop now' | User should be redirected to Product Listing Page (https://www.t-mobile.com/cell-phones)");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToTradeInPage(tMNGData);
		TradeInPage tradeInPage = new TradeInPage(getDriver());
		tradeInPage.setTextForDeviceCarrierSelectorSearchTextBar(tMNGData.getDeviceCarrier());
		tradeInPage.verifyRemovingACharacterFromDeviceCarrier();
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceCarrier();
		tradeInPage.setTextForDeviceManufacturerSelectorSearchTextBar(tMNGData.getDeviceManufacturer());
		tradeInPage.verifyRemovingACharacterFromDeviceManufacturer();
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceManufacturer();
		tradeInPage.setTextForDeviceModelSelectorSearchTextBar(tMNGData.getDeviceModel());
		tradeInPage.verifyRemovingACharacterFromDeviceModel();
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceModel();
		tradeInPage.verifyGoButton();
		tradeInPage.clickGetEstimateButton();
		tradeInPage.verifyEstimatesInSuccessHeader();
		tradeInPage.verifySuccessDescription();
		tradeInPage.verifyConditionTerms();
		tradeInPage.clickShopNowButton();
		tradeInPage.verifyProductListingPage();
	   	}

	/***
	 * US485233 Trade-in| Device Details
	 * US485235 Trade-in | Get Estimate
	 * US485218 Trade-in | Integration of Hero like component
	 * TC ID: TC285980
	 * TC ID: TC285985
	 * TC ID: TC284997
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT,Group.TMNG})
	public void testTradeInLegalTermsAndConditionContent(TMNGData tMNGData) {
		Reporter.log("US485233 Trade-in| Device Details");
		Reporter.log("US485235 Trade-in | Get Estimate");
		Reporter.log("US485218 Trade-in | Integration of Hero like component");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify We’ll help you join link| We’ll help you join link should be displayed");
		Reporter.log("4. Click on We’ll help you join link | We’ll help you join page should be displayed");
		Reporter.log("5. Verify and click Trade in program link at the footer | Verified and Clicked on Trade in program link");
		Reporter.log("6. Verify Trade In page is displayed | Trade In page should be displayed");
		Reporter.log("7. Click Full Terms link | Clicked on Full Terms links successfully.");
		Reporter.log("8. Check if full legal terms overlay is visible with content | Full Terms overlay loaded with content successfully.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToTradeInPage(tMNGData);
		TradeInPage tradeInPage = new TradeInPage(getDriver());
		tradeInPage.clickSeeFullTermsLink();
		tradeInPage.verifyFullTermsOverlay();
	}

	/***
	 * US485233 Trade-in| Device Details
	 * US485235 Trade-in | Get Estimate
	 * US485218 Trade-in | Integration of Hero like component
	 * TC ID: TC285981
	 * TC ID: TC285986
	 * TC ID: TC285000
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT,Group.TMNG})
	public void testTMNGAddAnotherDeviceTradeInFlowToShopPage(TMNGData tMNGData) {
		Reporter.log("US485233 Trade-in| Device Details");
		Reporter.log("US485235 Trade-in | Get Estimate");
		Reporter.log("US485218 Trade-in | Integration of Hero like component");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify We’ll help you join link| We’ll help you join link should be displayed");
		Reporter.log("4. Click on We’ll help you join link | We’ll help you join page should be displayed");
		Reporter.log("5. Verify and click Trade in program link at the footer | Verified and Clicked on Trade in program link");
		Reporter.log("6. Verify Trade In page is displayed | Trade In page should be displayed");
		Reporter.log("7. Enter the value in Device carrier text box | Entered value in the Device carrier text box");
		Reporter.log("8. Verify removing one character from entered text | Successfully removed a character from entered Device carrier inorder to view typeahead list");
		Reporter.log("9. Verify Device Carrier type ahead list | Selected the value from the type ahead list");
		Reporter.log("10. Enter the value in Device manufacturer text box | Entered value in the Device manufacturer text box");
		Reporter.log("11. Verify removing one character from entered text | Successfully removed a character from entered Device manufacturer inorder to view typeahead list");
		Reporter.log("12. Verify Device Manufacturer type ahead list | Selected the value from the type ahead list");
		Reporter.log("13. Enter the value in Device model text box | Entered value in the Device model text box");
		Reporter.log("14. Verify removing one character from entered text | Successfully removed a character from entered Device model inorder to view typeahead list");
		Reporter.log("15. Verify Device Model type ahead list | Selected the value from the type ahead list");
		Reporter.log("16. Click on 'Go' CTA | Clicked on Go button");
		Reporter.log("17. Click on Check Another Device CTA | Clicked on Check Another Device CTA");
		Reporter.log("18. Verify it navigates back to device details page | Trade In device page is displayed");
		Reporter.log("19. Enter the value in Device carrier text box | Entered value in the Device carrier text box");
		Reporter.log("20. Verify removing one character from entered text | Successfully removed a character from entered Device carrier inorder to view typeahead list");
		Reporter.log("21. Verify Device Carrier type ahead list | Selected the value from the type ahead list");
		Reporter.log("22. Enter the value in Device manufacturer text box | Entered value in the Device manufacturer text box");
		Reporter.log("23. Verify removing one character from entered text | Successfully removed a character from entered Device manufacturer inorder to view typeahead list");
		Reporter.log("24. Verify Device Manufacturer type ahead list | Selected the value from the type ahead list");
		Reporter.log("25. Enter the value in Device model text box | Entered value in the Device model text box");
		Reporter.log("26. Verify removing one character from entered text | Successfully removed a character from entered Device model inorder to view typeahead list");
		Reporter.log("27. Verify Device Model type ahead list | Selected the value from the type ahead list");
		Reporter.log("28. Click on 'Go' CTA | Clicked on Go button");
		Reporter.log("29. Click 'Shop Now' CTA | Clicked on Shop Now CTA");
		Reporter.log("30. Verify the display of Product listing page, upon clicking 'Shop now' | User should be redirected to Product Listing Page (https://www.t-mobile.com/cell-phones)");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToTradeInPage(tMNGData);
		TradeInPage tradeInPage = new TradeInPage(getDriver());
		tradeInPage.setTextForDeviceCarrierSelectorSearchTextBar(tMNGData.getDeviceCarrier());
		tradeInPage.verifyRemovingACharacterFromDeviceCarrier();
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceCarrier();
		tradeInPage.setTextForDeviceManufacturerSelectorSearchTextBar(tMNGData.getDeviceManufacturer());
		tradeInPage.verifyRemovingACharacterFromDeviceManufacturer();
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceManufacturer();
		tradeInPage.setTextForDeviceModelSelectorSearchTextBar(tMNGData.getDeviceModel());
		tradeInPage.verifyRemovingACharacterFromDeviceModel();
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceModel();
		tradeInPage.clickGetEstimateButton();
		tradeInPage.clickCheckAnotherDeviceButton();
		tradeInPage.verifyTradeInDevicePage();
		tradeInPage.setTextForDeviceCarrierSelectorSearchTextBar(tMNGData.getDeviceCarrier());
		tradeInPage.verifyRemovingACharacterFromDeviceCarrier();
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceCarrier();
		tradeInPage.setTextForDeviceManufacturerSelectorSearchTextBar(tMNGData.getDeviceManufacturer());
		tradeInPage.verifyRemovingACharacterFromDeviceManufacturer();
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceManufacturer();
		tradeInPage.setTextForDeviceModelSelectorSearchTextBar(tMNGData.getDeviceModel());
		tradeInPage.verifyRemovingACharacterFromDeviceModel();
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceModel();
		tradeInPage.clickGetEstimateButton();
		tradeInPage.clickShopNowButton();
		tradeInPage.verifyProductListingPage();
	}
	
	/***
	 * US485233 Trade-in| Device Details
	 * US485235 Trade-in | Get Estimate
	 * US485218 Trade-in | Integration of Hero like component
	 * TC ID: TC294283
	 * TC ID: TC294284
	 * TC ID: TC294282
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT,Group.TMNG})
	public void testTMNGTradeInFlowToShopPageWithZeroEstimates(TMNGData tMNGData) {
		Reporter.log("US485233 Trade-in| Device Details");
		Reporter.log("US485235 Trade-in | Get Estimate");
		Reporter.log("US485218 Trade-in | Integration of Hero like component");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify We’ll help you join link| We’ll help you join link should be displayed");
		Reporter.log("4. Click on We’ll help you join link | We’ll help you join page should be displayed");
		Reporter.log("5. Verify and click Trade in program link at the footer | Verified and Clicked on Trade in program link");
		Reporter.log("6. Enter the value in Device carrier text box | Entered value in the Device carrier text box");
		Reporter.log("8. Verify removing one character from entered text | Successfully removed a character from entered Device carrier inorder to view typeahead list");
		Reporter.log("9. Verify Device Carrier type ahead list | Selected the value from the type ahead list");
		Reporter.log("10. Enter the value in Device manufacturer text box | Entered value in the Device manufacturer text box");
		Reporter.log("11. Verify removing one character from entered text | Successfully removed a character from entered Device manufacturer inorder to view typeahead list");
		Reporter.log("12. Verify Device Manufacturer type ahead list | Selected the value from the type ahead list");
		Reporter.log("13. Enter the value in Device model text box | Entered value in the Device model text box");
		Reporter.log("14. Verify removing one character from entered text | Successfully removed a character from entered Device model inorder to view typeahead list");
		Reporter.log("15. Verify Device Model type ahead list | Selected the value from the type ahead list");
		Reporter.log("16. Verify 'Go' CTA | Go button is enabled");
		Reporter.log("17. Click on 'Go' CTA | Clicked on Go button");
		Reporter.log("18. Verify the success header of the estimated offer in Get Estimate page | Success header with the estimated offer must be displayed");
		Reporter.log("19. Verify the success description in Get Estimate page | Success description must be displayed");
		Reporter.log("20. Verify the condition terms are not presented in Get Estimate page | No conditions will be displayed, as the estimated offer is Zero");
		Reporter.log("21. Click 'Shop Now' CTA | Clicked on Shop Now CTA");
		Reporter.log("22. Verify the display of Product listing page, upon clicking 'Shop now' | User should be redirected to Product Listing Page (https://www.t-mobile.com/cell-phones)");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToTradeInPage(tMNGData);
		TradeInPage tradeInPage = new TradeInPage(getDriver());
		tradeInPage.setTextForDeviceCarrierSelectorSearchTextBar(tMNGData.getDeviceCarrier());
		tradeInPage.verifyRemovingACharacterFromDeviceCarrier();
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceCarrier();
		tradeInPage.setTextForDeviceManufacturerSelectorSearchTextBar(tMNGData.getDeviceManufacturer());
		tradeInPage.verifyRemovingACharacterFromDeviceManufacturer();
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceManufacturer();
		tradeInPage.setTextForDeviceModelSelectorSearchTextBar(tMNGData.getDeviceModel());
		tradeInPage.verifyRemovingACharacterFromDeviceModel();
		tradeInPage.verifyandSelectFromTypeAheadListOfDeviceModel();
		tradeInPage.verifyGoButton();
		tradeInPage.clickGetEstimateButton();
		tradeInPage.verifyEstimatesInSuccessHeader();
		tradeInPage.verifySuccessDescription();
		tradeInPage.verifyConditionTerms();
		tradeInPage.clickShopNowButton();
		tradeInPage.verifyProductListingPage();
	   	}
	
	
	/***
	 * US569302 Trade in | Result page append device carrier
	 * TC ID: TC295107
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT})
	public void testNoDeviceCarrierInResultIfCarrierSuffixInDeviceModel(TMNGData tMNGData) {
	Reporter.log("US569302 Trade in | Result page append device carrier");
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
	Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
	Reporter.log("3. Click menu hamburger icon and verify We’ll help you join link| We’ll help you join link should be displayed");
	Reporter.log("4. Click on We’ll help you join link | We’ll help you join page should be displayed");
	Reporter.log("5. Verify and click Trade in program link at the footer | Verified and Clicked on Trade in program link");
	Reporter.log("6. Verify Trade In page is displayed | Trade In page should be displayed");
	Reporter.log("7. Enter the value in Device carrier text box | Entered value in the Device carrier text box");
	Reporter.log("8. Verify removing one character from entered text | Successfully removed a character from entered Device carrier inorder to view typeahead list");
	Reporter.log("9. Verify Device Carrier type ahead list | Selected the value from the type ahead list");
	Reporter.log("10. Enter the value in Device manufacturer text box | Entered value in the Device manufacturer text box");
	Reporter.log("11. Verify removing one character from entered text | Successfully removed a character from entered Device manufacturer inorder to view typeahead list");
	Reporter.log("12. Verify Device Manufacturer type ahead list | Selected the value from the type ahead list");
	Reporter.log("13. Enter the value in Device model text box | Entered value in the Device model text box");
	Reporter.log("14. Verify removing one character from entered text | Successfully removed a character from entered Device model inorder to view typeahead list");
	Reporter.log("15. Verify Device Model type ahead list | Selected the value from the type ahead list");
	Reporter.log("16. Click on 'Go' CTA | Clicked on Go button");
	Reporter.log("17. Verify carrier name is not appended, in the result below the success header | Success Result format is verified successfully");
	Reporter.log("=================================");
	Reporter.log("Actual Output:");

	navigateToTradeInPage(tMNGData);
	TradeInPage tradeInPage = new TradeInPage(getDriver());
	tradeInPage.setTextForDeviceCarrierSelectorSearchTextBar(tMNGData.getDeviceCarrier());
	tradeInPage.verifyRemovingACharacterFromDeviceCarrier();
	tradeInPage.verifyandSelectFromTypeAheadListOfDeviceCarrier();
	String deviceCarrier = tradeInPage.retrieveDeviceCarrier();
	tradeInPage.setTextForDeviceManufacturerSelectorSearchTextBar(tMNGData.getDeviceManufacturer());
	tradeInPage.verifyRemovingACharacterFromDeviceManufacturer();
	tradeInPage.verifyandSelectFromTypeAheadListOfDeviceManufacturer();
	String deviceManufacturer = tradeInPage.retrieveDeviceManufacturer();
	tradeInPage.setTextForDeviceModelSelectorSearchTextBar(tMNGData.getDeviceModel());
	tradeInPage.verifyRemovingACharacterFromDeviceModel();
	tradeInPage.verifyandSelectFromTypeAheadListOfDeviceModel();
	String deviceModel = tradeInPage.retrieveDeviceModel();
	tradeInPage.verifyGoButton();
	tradeInPage.clickGetEstimateButton();
	tradeInPage.verifySuccessDescription();
	tradeInPage.verifySuccessResultFormat(deviceCarrier, deviceManufacturer, deviceModel);
		}
	}