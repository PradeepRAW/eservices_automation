package com.tmobile.eservices.qa.global.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsSelectionPage;
import com.tmobile.eservices.qa.pages.global.NewContactUSPage;
import com.tmobile.eservices.qa.pages.payments.UsageOverviewPage;
import com.tmobile.eservices.qa.pages.shop.MyPhoneDetailsPage;

public class ContactUsPageTest extends GlobalCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(ContactUsPageTest.class);

	/**
	 * Verify Add family allowances in Contact Us page
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyContactUsPageAddFamilyAllowances(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("VerifyContactUsPageAddFamilyAllowances method called in ContactUsPageTest");

		Reporter.log("Test Case : Verify Add family allowances in Contact Us page");
		Reporter.log("Test Data : Any PAH/Standard Msisdn");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on contact us link | Contact us page should be displayed");
		Reporter.log("5. Click on add family allowances link| Manage data and addons page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Output:");

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
		contactUSPage.clickOnAddFamilyAllowances();

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
	}

	/**
	 * Verify View App Purchases in Contact Us page
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyContactUsPageViewAppPurchases(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("VerifyContactUsPageViewAppPurchases method called in ContactUsPageTest");

		Reporter.log("Test Case : Verify View App Purchases in Contact Us page");
		Reporter.log("Test Data : Any PAH/Standard Msisdn");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be launched");
		Reporter.log("2. Login to the application | User should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on contact us link | Contact us page should be displayed");
		Reporter.log("5. Click on View App Purchases | Usage Over view Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Output:");

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
		contactUSPage.clickOnViewAppPurchases();
		contactUSPage.switchToWindow();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		UsageOverviewPage usageOverviewPage = new UsageOverviewPage(getDriver());
		usageOverviewPage.verifyUsageOverviewPage();
	}

	/**
	 * Verify Lost/Stolen device in Contact Us page
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyContactUsPageLostOrStolenDevice(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("VerifyContactUsPageLostOrStolenDevice method called in ContactUsPageTest");

		Reporter.log("Test Case : Verify Lost/Stolen device in Contact Us page");
		Reporter.log("Test Data : Any PAH/Standard Msisdn");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be aunched");
		Reporter.log("2. Login to the application | User should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on contact us link | Contact us page should be displayed");
		Reporter.log("5. Click on Lost/Stolen device | My phone details Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Output:");

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
		contactUSPage.clickOnlostOrStolenDevice();
		contactUSPage.switchToWindow();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.acceptIOSAlert();

		MyPhoneDetailsPage myPhoneDetailsPage = new MyPhoneDetailsPage(getDriver());
		myPhoneDetailsPage.verifyPhoneDetailsPage();
	}

	/**
	 * Verify Trade in value in Contact Us page
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID, })
	public void verifyContactUsTradeInValue(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("VerifyContactUsTradeInValue method called in ContactUsPageTest");

		Reporter.log("Test Case : Verify Trade in value in Contact Us page");
		Reporter.log("Test Data : Any PAH/Standard Msisdn");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be launched");
		Reporter.log("2. Login to the application | User should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on contact us link | Contact us page should be displayed");
		Reporter.log("5. Click on Trade in value | Lite Quote Tool Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Output:");

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
		contactUSPage.clickOntradeInValue();
		contactUSPage.switchToWindow();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		/*
		 * LiteQuoteToolPage liteQuoteToolPage = new LiteQuoteToolPage(getDriver());
		 * liteQuoteToolPage.verifyLiteQuoteToolPage();
		 */
	}

	/**
	 * Verify Device unlock status in Contact Us page
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyContactUsDeviceUnlockStatus(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("VerifyContactUsTradeInValue method called in ContactUsPageTest");

		Reporter.log("Test Case : Verify Device Unlock in Contact Us page");
		Reporter.log("Test Data : Any PAH/Standard Msisdn");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be launched");
		Reporter.log("2. Login to the application | User should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on contact us link | Contact us page should be displayed");
		Reporter.log("5. Click on Device unlock status | My phone details Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Output:");

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
		contactUSPage.clickOnDeviceUnlockStatus();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.acceptIOSAlert();

		MyPhoneDetailsPage myPhoneDetailsPage = new MyPhoneDetailsPage(getDriver());
		myPhoneDetailsPage.verifyPhoneDetailsPage();

	}

	/**
	 * Verify File a damage claim in Contact Us page
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID })
	public void verifyContactUsFileADamageClaim(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("verifyContactUsFileADamageClaim method called in ContactUsPageTest");
		Reporter.log("Test Case : Verify File a damage claim in Contact Us page");
		Reporter.log("Test Data : Any PAH/Standard Msisdn");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be launched");
		Reporter.log("2. Login to the application | User should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on contact us link | Contact us page should be displayed");
		Reporter.log("5. Click on File a damage claim | My php info Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Output:");

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
		contactUSPage.clickOnFileADamageClaim();
		contactUSPage.clickOnAllowPopUp();
		contactUSPage.switchToWindow();
		contactUSPage.verifyMyphpinfoPage();
	}

	/**
	 * Verify Upgrade device in Contact Us page
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyContactUsUpgradeDevice(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("verifyContactUsUpgradeDevice method called in ContactUsPageTest");
		Reporter.log("Test Case : Verify Upgrade device in Contact Us page");
		Reporter.log("Test Data : Any PAH/Standard Msisdn");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on contact us link | Contact us page should be displayed");
		Reporter.log("5. Click on Upgrade device | My phone details Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Output:");

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
		contactUSPage.clickOnUpgradeDevice();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		MyPhoneDetailsPage myPhoneDetailsPage = new MyPhoneDetailsPage(getDriver());
		myPhoneDetailsPage.verifyPhoneDetailsPage();

	}

	/**
	 * Verify Access voicemail in Contact Us page
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyContactUsAccessVoicemail(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("verifyContactUsAccessVoicemail method called in ContactUsPageTest");

		Reporter.log("Test Case : Verify Access voicemail in Contact Us page");
		Reporter.log("Test Data : Any PAH/Standard Msisdn");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on contact us link | Contact us page should be displayed");
		Reporter.log("5. Click on Access voicemail | My phone details Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Output:");

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
		contactUSPage.clickOnAccessVoicemail();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		contactUSPage.verifyVoicemailPage();

	}

	/**
	 * US373870/US373876/US373877:Contact Us | TEX | Angular Migration | Test
	 * Automation for Team of Experts Section (Mystery Machine)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyTeamDescriptionPageOnNewAngularContactUsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US373870:Contact Us | TEX | Availabality | team-description page");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on contact us link | Contact us page should be displayed");
		Reporter.log(
				"5. Verify team of experts description divison | Team of experts description divison should be displayed");
		Reporter.log("6. Verify team picture | Team picture should be displayed");

		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
		contactUSPage.verifyTeamofExpertsDescriptionOnNewAngularPage();
		contactUSPage.verifyTeamPictureOnNewAngularPage();

	}

	/**
	 * US342474:Contact Us | TEX | Angular Migration | Schedule Callback (Mystery
	 * Machine) DE240391:[PROD] [MYTMO] Contact US schedule call update is not
	 * working. CDCDWR-464-MyTMO - Contact Us - Create Appointments Issue - DEFECT
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyScheduleCallUpdate(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("DE240391:[PROD] [MYTMO] Contact US schedule call update is not working.");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on contact us link | Contact us link should be clicked");
		Reporter.log("5. Verify contact us page | Contact us page should be displayed");
		Reporter.log("6. Verify call team button | Call team button should be displayed");
		Reporter.log("7. Click call team button | Call team button should be clicked");
		Reporter.log("8. Verify call set up popup window | Call set up popup window should be displayed");
		Reporter.log("9. Click on schedule a call button | Schedule a call button should be clicked");
		Reporter.log("10.Click on time slot | Time slot should be clicked");
		Reporter.log("11.Click confirm button | Confirm button should be clicked");
		Reporter.log("12.Verify confirmation window | Confirmation window should be displayed");
		Reporter.log("13.Click on confirmation window | Confirmation window should be clicked");
		Reporter.log("14.Verify call team button | Call team button should be displayed");
		Reporter.log("15.Click call team button | Call team button should be clicked");
		Reporter.log("16.Verify make a change button | Make a change button should be displayed");
		Reporter.log("17.Click make a change button | Click on make a change button should be success");
		Reporter.log("18.Click on schedule a call button | Schedule a call button should be clicked");
		Reporter.log("19.Click on time slot | Time slot should be clicked");
		Reporter.log("20.Click confirm button | Confirm button should be clicked");
		Reporter.log("21.Click error modal popup ok button | Click on error modal popup should be success");
		Reporter.log("22.Verify call team button | Call team button should be displayed");
		Reporter.log("23.Click call team button | Call team button should be clicked");
		Reporter.log("24.Verify make a change button | Make a change button should be displayed");

		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		NewContactUSPage contactUSPage = navigateToContactUsPage(myTmoData);
		contactUSPage.verifyCallTeamBtn();
		contactUSPage.clickCallTeamBtn();
		contactUSPage.verifySetUpCallPopUpForNewAngular();
		contactUSPage.clickScheduleACallBtn();
		contactUSPage.selectScheduledTime();
		contactUSPage.clickConfirmBtn();
		contactUSPage.verfiyConfirmationWindow();
		contactUSPage.clickConfirmationWindow();
		contactUSPage.verifyCallTeamBtn();
		contactUSPage.clickCallTeamBtn();
		contactUSPage.verifyMakeAChangeBtn();
		contactUSPage.clickMakeAChangeButton();
		contactUSPage.selectScheduledTime();
		contactUSPage.clickConfirmBtn();
		contactUSPage.verfiyConfirmationWindow();
		contactUSPage.clickConfirmationWindow();
		contactUSPage.verifyCallTeamBtn();
		contactUSPage.clickCallTeamBtn();
		contactUSPage.verifyMakeAChangeBtn();
		contactUSPage.clickCancelCallBack();
	}
}
