package com.tmobile.eservices.qa.payments.functional;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.Payment;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.AddCardPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentAmountPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentCollectionPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentPage;
import com.tmobile.eservices.qa.pages.payments.EIPSelectInstallmentPlanPage;
import com.tmobile.eservices.qa.pages.payments.EipJodPage;
import com.tmobile.eservices.qa.pages.payments.JODEndingSoonOptionsPage;
import com.tmobile.eservices.qa.pages.payments.PaymentEstimatorPage;
import com.tmobile.eservices.qa.pages.payments.PurchaseOptionInstallmentPlanPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.pages.payments.StoreLocator;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

public class EIPJODPageTest extends PaymentCommonLib {

	/*
	 * US404149 EIP/JOD Modernization: Create Shell Page]
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPJODPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US404149 | EIP/JOD Modernization: Create Shell Page");
		Reporter.log("Data Condition | EIP Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

	}

	/*
	 * US405385 EIP/JOD Modernization: Display Documents and Receipts Copy
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testVerifyDocumentsandReceiptsSectiononEIPJODPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US405385 |   EIP/JOD Modernization:  Display Documents and Receipts Copy");
		Reporter.log("Data Condition | EIP Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"6. verify 'Documents & Receipts' section header | 'Documents & Receipts' section header should be displayed");
		Reporter.log(
				"7. verify text 'To view billing, payment history, and electronic agreements to your account, go to account activities' in 'Documents & Receipts' section  | text  should be displayed");
		Reporter.log("8. click on 'account activities' link | 'Account History'page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.verifyDocAndRecieptsSection(PaymentConstants.EIPJOD_DOC_RECEIPT_HEADER,
				PaymentConstants.EIPJOD_DOC_RECEIPT_BODY);
		eipJodPage.clickAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
	}

	/*
	 * US405610 EIP/JOD Modernization: Active/Completed Tab Implementation
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testVerifyActiveandCompletedTabsonEIPJODPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US405610 |  EIP/JOD Modernization: Active/Completed Tab Implementation");
		Reporter.log("Data Condition | EIP Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("6. verify 'Active' tab is selected by default| 'Active' tab should be selected by default");
		Reporter.log(
				"7. verify 'Active' tab have magenta highlight and text in black| magenta highlight and text should be in black ");
		Reporter.log(
				"8. verify 'completed'  tab should have grey text and no highlight | text should be in grey and no highlight should be displayed ");
		Reporter.log("9. click on 'completed' tab | magenta highlight and text should be black for 'completed' tab  ");
		Reporter.log(
				"10. verify text in grey and no highlight for  'Active' tab |  text should be in grey and no highlight for  'Active' tab  ");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.verifyEipSection();
		eipJodPage.clickCompletedPlansTab();
		eipJodPage.verifyCompletedEipTabHighlighted();
		eipJodPage.clickActiveEIPPlansTab();
		eipJodPage.verifyActiveEipTabHighlighted();
	}

	/*
	 * 
	 * US406280 EIP/JOD Modernization: EIPs Header
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testEquipmentInstallmentPlansHeaderonEIPJODPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US406280 | EIP/JOD Modernization: EIPs Header");
		Reporter.log("Data Condition | EIP Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"6. verify header 'Equipment Installment Plans' above EIP blades | 'Equipment Installment Plans' header should be displayed");
		Reporter.log(
				"7. switch between 'completed' & 'Active' tab | 'Equipment Installment Plans' header should be same for both tabs");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		
		eipJodPage.verifyEIPBladeHeader();
		eipJodPage.clickCompletedPlansTab();
		eipJodPage.verifyEIPBladeHeader();
		eipJodPage.clickActiveEIPPlansTab();
		eipJodPage.verifyEIPBladeHeader();

	}

	/*
	 * 
	 * US410205 EIP/JOD Modernization: Roll Up Totals US414612 EIP/JOD
	 * Modernization: Roll Up Totals Display Logic
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testRollUpTotalsforUserswithActiveLeasesandInstallementPlans(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US410205 | EIP/JOD Modernization: Roll Up Totals");
		Reporter.log("US414612 |  EIP/JOD Modernization: Roll Up Totals Display Logic");
		Reporter.log("Data Condition | Users with Active Leases and Installement Plans");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"6. verify header 'Monthly payments' and the Amount 'total monthly payment for all active plans' | 'Monthly payments' and the Amount 'total monthly payment for all active plans' should be displayed");
		Reporter.log(
				"7. verify 'Lease balance' link and the Amount for 'total balance of all active leases' | 'Lease balance' link and the Amount for 'total balance of all active leases' should be displayed");
		Reporter.log(
				"8. verify Number of active leases and Total monthly lease payments | Number of active leases and Total monthly lease payments should be displayed");
		Reporter.log(
				"9. verify 'Plan balance' link and the Amount for 'total balance of all active EIP's' | 'Plan balance' link and the Amount for 'total balance of all active EIP's' should be displayed");
		Reporter.log(
				"10. verify Number of active EIPs and Total monthly EIP payments | Number of active EIPs and Total monthly EIP payments should be displayed");
		Reporter.log("11. click on lease balance | User should scroll to JOD section ");
		Reporter.log("12. click on  Plan balance | User should scroll to EIP section ");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.verifyMonthlyPayments();
		eipJodPage.verifyPlanBalance();
		eipJodPage.clickPlanBalance();
		eipJodPage.verifyEipSection();
	}

	/*
	 * 
	 * US414612 EIP/JOD Modernization: Roll Up Totals Display Logic
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  })
	public void testRollUpTotalsforUserswthActiveLeasesonly(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US414612 |  EIP/JOD Modernization: Roll Up Totals Display Logic");
		Reporter.log("Data Condition | Users with Active Leases");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"6. verify header 'Monthly payments' section |'Monthly payments' section header should be displayed");
		Reporter.log("7. verify Lease Balance Section | Lease Balance Section should be displayed");
		Reporter.log("8. verify Plan Balance section | Plan Balance section should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyMonthlyPayments();
		eipJodPage.verifyLeaseDetails();
		eipJodPage.verifyPlanBalance();
		eipJodPage.clickLeaseDetails();
		// eipJodPage.verifyJODSection(); Not implemented now by DEV
	}

	/*
	 * 
	 * US414612 EIP/JOD Modernization: Roll Up Totals Display Logic
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testRollUpTotalsforUserswthActiveInstallemnetPlansonly(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US414612 |  EIP/JOD Modernization: Roll Up Totals Display Logic");
		Reporter.log("Data Condition | Users with Active Installement Plans");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"6. verify header 'Monthly payments' section |'Monthly payments' section header should be displayed");
		Reporter.log("7. verify Plan Balance section | Plan Balance section should  be displayed");
		Reporter.log("8. verify Lease Balance Section | Lease Balance Section should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyMonthlyPayments();
		eipJodPage.verifyLeaseDetails();
		eipJodPage.verifyPlanBalance();
		eipJodPage.clickLeaseDetails();
		// eipJodPage.verifyJODSection(); Not implemented now by DEV
		eipJodPage.clickPlanBalance();
		eipJodPage.verifyEipSection();
	}

	/*
	 * 
	 * US414612 EIP/JOD Modernization: Roll Up Totals Display Logic
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testRollUpTotalsnotDisplayedUserswthNoActiveInstallemnetPlansandLeases(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US414612 |  EIP/JOD Modernization: Roll Up Totals Display Logic");
		Reporter.log("Data Condition | Users with no Active Installement Plans and leases");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"6. verify Roll Up Totals section not displayed  |Roll Up Totals section should not  be displayed");
		Reporter.log(
				"7. verify 'you didn't currently have an active plan or lease' message | 'you didn't currently have an active plan or lease' message should  be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyMonthlyPayments();
		eipJodPage.verifyLeaseDetails();
		eipJodPage.verifyPlanBalance();
		eipJodPage.clickLeaseDetails();
		// eipJodPage.verifyJODSection(); Not implemented now by DEV
		eipJodPage.clickPlanBalance();
		eipJodPage.verifyEipSection();
	}

	/**
	 * US410444 EIP/JOD Modernization: Progress Bar
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testEIPJODModernizationProgressBar(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US410444- EIP/JOD Modernization: Progress Bar");
		Reporter.log("Data Condition | Any User EIP jod eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"Step 6: Verify progress bar on  EIP  blade either expanded or collapsed |  progress bar should displayed on EIP  blade either expanded or collapsed ");
		Reporter.log(
				"Step 7: Verify progress bar on  JOD blade either expanded or collapsed |  progress bar should displayed on JOD  blade either expanded or collapsed ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.verifyProgressBarExpandedAndCollapsedViews();
	}

	/**
	 * US406363 EIP/JOD Modernization: Active EIP Collapsed View
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testInformationDisplayedonCollapsedEIPblade(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US406363- EIP/JOD Modernization: Active EIP Collapsed View");
		Reporter.log("Data Condition | Any User EIP jod eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: Verify Device image |  Device image  should be displayed on collapsed EIP blade ");
		Reporter.log("Step 7: Verify Friendly name of the line|  Friendly name of the line should be displayed ");
		Reporter.log("Step 8: Verify MSISDN associated with the plan|  MSISDN should be displayed ");
		Reporter.log("Step 9: Verify Device name|  Device name should be displayed ");
		Reporter.log("Step 10: Verify Progress bar|  Progress bar should be displayed ");
		Reporter.log("Step 11: Verify Remaining balance| Remaining balance should be displayed ");
		Reporter.log("Step 12: Verify Monthly payment amount| Monthly payment amount should be displayed ");
		Reporter.log("Step 13: click on gray downward pointing Chevron| EIP blade should be expanded ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");
		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.verifyActiveEIPPlans();
	}

	/**
	 * US406281 EIP/JOD Modernization: View Page EIP Blades
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testIndividualBladesforEachActiveEIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US406281- EIP/JOD Modernization: View Page EIP Blades");
		Reporter.log("Data Condition | Any User EIP jod eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"Step 6: Verify individual Blade for each individual EIP/JOD plan in a BAN|  individual Blade should be displayed for each EIP/JOD plan ");
		Reporter.log(
				"Step 7. verify blades are expanding and collapsing accordinly when we click on Chevron|  blades should be expand and collapse accordinly");
		Reporter.log(
				"Step 8. verify User able to have any combination of multiple expanded and collapsed blades at a time|  User should be able to have any combination of multiple expanded and collapsed blades|");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyActiveEIPPlans();
	}

	/**
	 * US406282 EIP/JOD Modernization: Active EIPs expanded view
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testExpandedEIPbladeForInstallmentPlanTypeEIPDisplay(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US406282- EIP/JOD Modernization: Active EIPs expanded view");
		Reporter.log("Data Condition | Any User EIP  eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: Verify Device image |  Device image  should be displayed on collapsed EIP blade ");
		Reporter.log("Step 7: Verify Friendly name of the line|  Friendly name of the line should be displayed ");
		Reporter.log("Step 8: Verify MSISDN associated with the plan|  MSISDN should be displayed ");
		Reporter.log("Step 9: Verify Device name|  Device name should be displayed ");
		Reporter.log("Step 10: Verify Progress bar|  Progress bar should be displayed ");
		Reporter.log("Step 11: Verify Remaining balance| Remaining balance should be displayed ");
		Reporter.log("Step 12: Verify Monthly payment amount| Monthly payment amount should be displayed ");
		Reporter.log("Step 13: click on gray downward pointing Chevron| EIP blade should be expanded ");
		Reporter.log(
				"Step 14: Verify  Make a payment button|   Make a payment button  should be displayed on expanded EIP blade ");
		Reporter.log(
				"Step 15: Verify text 'Use the estimator to see how making extra payments will affect your plan'|  'Use the estimator to see how making extra payments will affect your plan' should be displayed ");
		Reporter.log(
				"Step 16: Verify 'See the payment estimator' link|  'See the payment estimator' should be displayed ");
		Reporter.log("Step 17: Verify Monthly payment amount| Monthly payment amount should be displayed ");
		Reporter.log("Step 18: Verify Payments remaining|  Payments remaining should be displayed ");
		Reporter.log("Step 19: Verify Original price|Original price should be displayed ");
		Reporter.log("Step 20: Verify Down payment| Down payment should be displayed ");
		Reporter.log("Step 21: Verify  Final payment| Final payment  should be displayed ");
		Reporter.log("Step 22: Verify Plan start date| Plan start date should be displayed ");
		Reporter.log("Step 23: Verify Installment plan type | Installment plan type should be displayed ");
		Reporter.log("Step 24: Verify Plan status| Plan status should be displayed ");
		Reporter.log("Step 25: Verify EIP Plan ID| EIP Plan ID should be displayed ");
		Reporter.log("Step 26: Verify IMEI|IMEI should be displayed ");
		Reporter.log("Step 27: Verify Down payment| Down payment should be displayed ");
		Reporter.log("Step 28: click on upward pointing chevron| EIP blade should be collapsed ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.verifyActiveEIPPlansExpandedView();
	}

	/**
	 * US406282 EIP/JOD Modernization: Active EIPs expanded view
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testExpandedEIPbladeForInstallmentPlanTypePOIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US406282- EIP/JOD Modernization: Active EIPs expanded view");
		Reporter.log("Data Condition | Any User EIP POIP plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: click on gray downward pointing Chevron| EIP blade should be expanded ");
		Reporter.log(
				"Step 7: Verify 'See the payment estimator' link not displayed|  'See the payment estimator' should not be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

	}

	/**
	 * US406282 EIP/JOD Modernization: Active EIPs expanded view
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testExpandedEIPbladewithActivePromotion(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US406282- EIP/JOD Modernization: Active EIPs expanded view");
		Reporter.log("Data Condition | Any User EIP eligible with active promotion");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: click on gray downward pointing Chevron| EIP blade should be expanded ");
		Reporter.log("Step 7: Verify  Promotion|   Promotion  should be displayed on expanded EIP blade ");
		Reporter.log("Step 8: Verify Monthly credits remaining|  Monthly credits remaining should be displayed ");
		Reporter.log("Step 9: Verify Amount of monthly credit|  Amount of monthly credit should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

	}

	/**
	 * US410238 EIP/JOD Modernization: Active EIP Sorting
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testActiveEIPSorting(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US410238 EIP/JOD Modernization: Active EIP Sorting");
		Reporter.log("Data Condition | Any User EIP active data");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"Step 6: verify EIPs are sorted by oldest on top (descending oldest to newest) | EIPs should be  sorted by oldest on top ");
		Reporter.log(
				"Step 7: Verify  First EIP in the list  always be expanded|  First EIP in the list shoule be  always expanded ");
		Reporter.log(
				"Step 8: Verify All EIPs except for the first are collapsed|  All EIPs except for the first should be collapsed ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.verifyActiveEIPPlansSorting();
	}

	/**
	 * US410938 Completed EIPs: Collapsed Completed EIP Blades
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testCollapsedCompletedEIPBlades(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US410938- Completed EIPs: Collapsed Completed EIP Blades");
		Reporter.log("Data Condition | Any User EIP completed installemnets");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 4.  click on 'completed' tab| List of completed EIP's should be displayed");
		Reporter.log("Step 6: Verify Device image |  Device image  should be displayed on collapsed EIP blade ");
		Reporter.log("Step 7: Verify Friendly name of the line|  Friendly name of the line should be displayed ");
		Reporter.log("Step 8: Verify MSISDN associated with the plan|  MSISDN should be displayed ");
		Reporter.log("Step 9: Verify Device name|  Device name should be displayed ");
		Reporter.log(
				"Step 10: Verify 'Completed' label on each EIP |  'Completed' label on each EIP  should be displayed ");

		Reporter.log("Step 11: Verify Progress bar not displayed|  Progress bar should not  be displayed ");

		Reporter.log("Step 12: click on gray downward facing  Chevron| EIP blade should be expanded ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.verifyEIPPage();
		eipJodPage.clickCompletedPlansTab();
		eipJodPage.verifyCompletedEIPPlans();
	}

	/**
	 * US410939 Completed EIPs: Expanded Completed EIP Blades
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testExpandedCompletedEIPBlades(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US410939-Completed EIPs: Expanded Completed EIP Blades");
		Reporter.log("Data Condition | Any User EIP completed installemnets");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6.  click on 'completed' tab| List of completed EIP's should be displayed");
		Reporter.log("Step 7: click on gray downward pointing Chevron| EIP blade should be expanded ");
		Reporter.log("Step 8: Verify Device image |  Device image  should be displayed on collapsed EIP blade ");
		Reporter.log("Step 9: Verify Friendly name of the line|  Friendly name of the line should be displayed ");
		Reporter.log("Step 10: Verify MSISDN associated with the plan|  MSISDN should be displayed ");
		Reporter.log("Step 11: Verify Device name|  Device name should be displayed ");
		Reporter.log(
				"Step 12: Verify 'Completed' label on each EIP |  'Completed' label on each EIP  should be displayed ");
		Reporter.log("Step 13: Verify Progress bar not displayed|  Progress bar should not  be displayed ");
		Reporter.log("Step 14: Verify Payments remaining|  Payments remaining should be displayed ");
		Reporter.log("Step 15: Verify Original price|Original price should be displayed ");
		Reporter.log("Step 16: Verify Down payment| Down payment should be displayed ");
		Reporter.log("Step 17: Verify Plan start date| Plan start date should be displayed ");
		Reporter.log("Step 18: Verify Plan status| Plan status should be displayed ");
		Reporter.log("Step 19: Verify EIP Plan ID| EIP Plan ID should be displayed ");
		Reporter.log("Step 20: Verify IMEI|IMEI should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		
		eipJodPage.clickCompletedPlansTab();
		eipJodPage.verifyCompletedEIPPlansAndBladeElements();

	}

	/**
	 * US413515 EIP/JOD Modernization: Estimator Landing Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	
	public void testEIPJODModernizationEstimatorLandingPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US413515- EIP/JOD Modernization: Estimator Landing Page");
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: click on gray downward pointing Chevron| EIP blade should be expanded ");
		Reporter.log(
				"Step 7: Verify 'See the payment estimator' link |  'See the payment estimator' should  be displayed ");
		Reporter.log(
				"Step 8: click 'See the payment estimator' link |  EIP Payment Estimator landing page  should  be displayed ");
		Reporter.log(
				"Step 9: verify Estimator landing page header | Estimator landing page header  should  be displayed ");
		Reporter.log(
				"Step 10: verify EIP Info Blade( EIP information for the selected installment plan) | EIP Info Blade  should  be displayed ");
		Reporter.log(
				"Step 11: verify Payment Amount Estimation Blade | Payment Amount Estimation Blade should  be displayed ");
		Reporter.log("Step 12: verify Estimation amount field | Estimation amount field should  be displayed ");
		Reporter.log("Step 13: verify Estimate CTA| Estimate CTA should  be displayed ");
		Reporter.log(
				"Step 14: verify Estimator Results Section(Estimator Grid Data)|Estimator Results Section should  be displayed ");
		Reporter.log("Step 15: verify  Pay this Amount CTA|  Pay this Amount CTA should  be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		PaymentEstimatorPage paymentEstimatorPage = new PaymentEstimatorPage(getDriver());
		eipJodPage.verifyEIPJODPageHeader();
		int activePlanSize = eipJodPage.getActiveEIPPlansCount();
		for (int i = 0; i < activePlanSize; i++) {
			if (i != 0) {
				eipJodPage.clickEIPDownwardPointingChevron(i);
			}
			eipJodPage.clickEipPaymentEstimatorlink();
			paymentEstimatorPage.verifyPaymentEstimatorpage(
					new BigDecimal(generateRandomAmount()).setScale(2, RoundingMode.CEILING).toString());
			paymentEstimatorPage.navigateBack();
			eipJodPage.verifyPageLoaded();
			eipJodPage.clickActiveArrowUp();
		}
	}

	/**
	 * 
	 * US414627 EIP/JOD Modernization: No Active items Upsell
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testEIPJODModernizationNoActiveitemsUpsell(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US414627- EIP/JOD Modernization: No Active items Upsell");
		Reporter.log("Data Condition | Any MSDSIN with no active EIPs or JODs");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"Step 6: Verify EIP Upsell message 'You don't currently have an active installment plan or lease. Check out all of the latest phones and accessories' |  EIP Upsell message text should  be displayed ");
		Reporter.log("Step 7: click 'Shop Now' CTA  | shop page  should  be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.verifyNoActiveEIPorJODMessage(PaymentConstants.NO_ACTIVE_EIP);
		eipJodPage.verifyShopNowCTA();
		eipJodPage.clickSHopNowCTA();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyPageLoaded();
	}

	/**
	 * US436752 EIP Payment Cancellation Modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testEIPPaymentCancellationModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US436752-EIP Payment Cancellation Modal");
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: click on gray downward pointing Chevron| EIP blade should be expanded ");

		Reporter.log(
				"Step 7: click 'See the payment estimator' link |  EIP Payment Estimator landing page  should  be displayed ");
		Reporter.log(
				"Step 8: verify Estimator landing page header | Estimator landing page header  should  be displayed ");

		Reporter.log("Step 9: click  Pay this Amount CTA|  EIP Payment landing page should  be displayed ");
		Reporter.log("Step 10: click on 'Cancel' CTA | Cancel payment modal should  be displayed ");

		Reporter.log("Step 11: verify Cancel Payment header | Cancel Payment header should  be displayed ");
		Reporter.log(
				"Step 12: verify 'Are you sure you want to cancel your payment'  text| 'Are you sure you want to cancel your payment' text should  be displayed ");
		Reporter.log("Step 13: verify 'Yes' and 'No' CTA | 'Yes' and 'No' CTA should  be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.verifyEIPPage();
		eipJodPage.clickMakeAPaymentBtn();
		EIPPaymentPage eipPaymentPage = new EIPPaymentPage(getDriver());
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.clickCancelBtn();
		eipPaymentPage.verifyCancelModal();
		eipPaymentPage.clickNoBtnOnCancelModal();
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.clickCancelBtn();
		eipPaymentPage.verifyCancelModal();
		eipPaymentPage.clickYesBtnOnCancelModal();
		eipJodPage.verifyEIPPage();
	}

	/**
	 * US413560 EIP/JOD Modernization: EIP Data Prepopulation
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testEIPDataPrepopulationonEstimatorPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US413560 EIP/JOD Modernization: EIP Data Prepopulation");
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: click on gray downward pointing Chevron| EIP blade should be expanded ");
		Reporter.log(
				"Step 7: Verify 'See the payment estimator' link |  'See the payment estimator' should  be displayed ");
		Reporter.log(
				"Step 8: click 'See the payment estimator' link |  EIP Payment Estimator landing page  should  be displayed ");
		Reporter.log("Step 9: verify payment Estimator  header | payment Estimator header  should  be displayed ");
		Reporter.log(
				"Step 10: verify  Name,MSISDN,EIPID,Device Name,Device Balance fields prepopulated in the EIP blade|Name,MSISDN,EIPID,Device Name,Device Balance fields should be prepopulated in the EIP blade ");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
	
		String name, deviceName, msisdn, eipID, deviceBal;
		name = eipJodPage.getActiveEIPFriendlyName(0);
		deviceName = eipJodPage.getActiveEIPDeviceName(0);
		msisdn = eipJodPage.getActiveEIPMsisdn(0);
		eipID = eipJodPage.getActiveEIPFriendlyName(0);
		deviceBal = eipJodPage.getActiveEIPMonthlyAmount(0);
		eipJodPage.clickEipPaymentEstimatorlink();
		PaymentEstimatorPage paymentEstimatorPage = new PaymentEstimatorPage(getDriver());
		paymentEstimatorPage.verifyPrepopulatedFields(name, msisdn, eipID, deviceName, deviceBal);
	}

	/**
	 * US414641 EIP/JOD Modernization: Estimatior Calculation
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testEIPJODModernizationEstimatiorCalculation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US414641 EIP/JOD Modernization: Estimatior Calculation");
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: click on gray downward pointing Chevron| EIP blade should be expanded ");
		Reporter.log(
				"Step 7: Verify 'See the payment estimator' link |  'See the payment estimator' should  be displayed ");
		Reporter.log(
				"Step 8: click 'See the payment estimator' link |  EIP Payment Estimator landing page  should  be displayed ");
		Reporter.log("Step 9: verify payment Estimator  header | payment Estimator header  should  be displayed ");
		Reporter.log(
				"Step 10:Enter Amount less than the balance due in 'Payment amount input field' and click on Estimate button ");
		Reporter.log(
				"Step 11: verify Calculated estimated plan balance (Current Plan balance- Amount entered in Payment amount input field) |estimated plan balance should be updated");
		Reporter.log(
				"Step 12: verify calculated payments remaining in estimated column |estimated Remaining Payments should be updated");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		
		eipJodPage.clickEipPaymentEstimatorlink();
		PaymentEstimatorPage paymentEstimatorPage = new PaymentEstimatorPage(getDriver());
		paymentEstimatorPage.verifyPageLoaded();
		paymentEstimatorPage.verifyPaymentAmountField();
		paymentEstimatorPage.fillAmountEstimationAmountField(generateRandomAmount().toString());
		paymentEstimatorPage.verifyEstimateAndCrossBtn();
		paymentEstimatorPage.clickEstimateBtn();
		paymentEstimatorPage.verifyEstimatedPlanbalance();
		paymentEstimatorPage.verifyEstimatedRemainingPayments();
		paymentEstimatorPage.verifyPayThisAmountBtn();
	}

	/**
	 * US414836 EIP/JOD Modernization: Payment amount input field
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testEIPJODModernizationPaymentAmountInputField(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US414836 EIP/JOD Modernization: Payment amount input field");
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: click on gray downward pointing Chevron| EIP blade should be expanded ");
		Reporter.log(
				"Step 7: click 'See the payment estimator' link |  EIP Payment Estimator landing page  should  be displayed ");
		Reporter.log("Step 8: verify payment Estimator  header | payment Estimator header  should  be displayed ");
		Reporter.log(
				"Step 9: verify text 'Enter extra payment here' in text field | 'Enter extra payment here'text should be dispalyed in text field ");
		Reporter.log("Step 10: verify Estimate button in disabled state| Estimate button should be in disabled state");
		Reporter.log(
				"Step 11: Enter Amount less than the balance due in 'Payment amount input field' | Estimate button should be enabled");
		Reporter.log("Step 12: verify 'x button' in Payment input field | 'x button'  should be displayed");
		Reporter.log(
				"Step 13: verify text 'Enter extra payment here' above Payment input field | 'Enter extra payment here' text should be displayed above Payment input field");
		Reporter.log(
				"Step 14: verify 'Pay this amount' button CTA | 'Pay this amount' button CTA should be displayed ");
		Reporter.log(
				"Step 15: clear text field and Enter Amount less than $1.01 |' amount must be more than $1.00' text should be displayed ");
		Reporter.log(
				"Step 16: clear text field and Enter Amount more than  balance due|'Amount can not exceed plan balance' text should be displayed ");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.verifyEIPPage();
		eipJodPage.clickEipPaymentEstimatorlink();
		PaymentEstimatorPage paymentEstimatorPage = new PaymentEstimatorPage(getDriver());
		paymentEstimatorPage.verifyPageLoaded();
		paymentEstimatorPage.verifyPaymentAmountField();
		paymentEstimatorPage.fillAmountEstimationAmountField(generateRandomAmount().toString());
		paymentEstimatorPage.verifyEstimateAndCrossBtn();
		paymentEstimatorPage.clickEstimateBtn();
		paymentEstimatorPage.verifyEstimatedPlanbalance();
		paymentEstimatorPage.verifyEstimatedRemainingPayments();
		paymentEstimatorPage.verifyPayThisAmountBtn();
	}

	/**
	 * US434469-EIP Payments: Create Shell and Integrate HTML
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testEIPPaymentShellPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US434469-EIP Payments: Create Shell and Integrate HTML");
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile.com/eippayment' | Payments Shell Page should be displayed");
		Reporter.log(
				"Step 5. verify header and footer on Payments Shell Page | header and footer  should be displayed");
		Reporter.log("Step 6: verify Agree and submit button| Agree and submit button should be displayed ");

		Reporter.log("Step 7: click on 'Cancel' CTA  |  eip/jod view page  should  be displayed ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);

		eipJodPage.clickMakeAPaymentBtn();
		EIPPaymentPage eipPaymentPage = new EIPPaymentPage(getDriver());
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.verifySubmitBtn();
		eipPaymentPage.verifyCancelBtn();
		eipPaymentPage.clickCancelBtn();
		eipPaymentPage.verifyCancelModal();
		eipPaymentPage.clickNoBtnOnCancelModal();
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.clickCancelBtn();
		eipPaymentPage.clickYesBtnOnCancelModal();
		eipJodPage.verifyEIPPage();
	}

	/**
	 * US487959 EIP/JOD Modernization - PII Masking Landing Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testPiiMaskingOnEIPLandingPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US487959 EIP/JOD Modernization - PII Masking Landing Page");
		Reporter.log("Data Condition | Any User EIP  eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: Verify Friendly name of the line|  Friendly name of the line should be displayed ");
		Reporter.log("Step 7: Verify MSISDN associated with the plan|  MSISDN should be displayed ");
		Reporter.log("Step 8: Click on downward pointing chevron| EIP blade should be expanded ");
		Reporter.log("Step 9: Verify IMEI|IMEI should be displayed ");
		Reporter.log("Step 10: click on upward pointing chevron| EIP blade should be collapsed ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.verifyActiveEIPPlansPiiMasking(PaymentConstants.PII_CUSTOMER_CUSTFIRSTNAME_PID,
				PaymentConstants.PII_CUSTOMER_MSISDN_PID, PaymentConstants.PII_CUSTOMER_IMEI_PID);
		eipJodPage.clickCompletedPlansTab();
		eipJodPage.verifyCompletedEIPPlansPiiMasking(PaymentConstants.PII_CUSTOMER_CUSTFIRSTNAME_PID,
				PaymentConstants.PII_CUSTOMER_MSISDN_PID, PaymentConstants.PII_CUSTOMER_IMEI_PID);
	}

	/**
	 * US487978 EIP/JOD Modernization - PII Masking Estimator
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS}) 
	public void testPiiMaskingOnEIPEstimatorPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US487978 EIP/JOD Modernization - PII Masking Estimator");
		Reporter.log("Data Condition | Any User EIP  eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | Page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"Step 6: Click 'See the payment estimator' link | EIP Payment Estimator page should be displayed ");
		Reporter.log("Step 7: Verify Friendly name of the line| Friendly name of the line should be displayed ");
		Reporter.log("Step 8: Verify MSISDN associated with the plan| MSISDN should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		eipJodPage.clickEipPaymentEstimatorlink();
		PaymentEstimatorPage estimatorPage = new PaymentEstimatorPage(getDriver());
		estimatorPage.verifyPageLoaded();
		estimatorPage.verifyPiiMaskingForName(PaymentConstants.PII_CUSTOMER_CUSTFIRSTNAME_PID);
		estimatorPage.verifyPiiMaskingForMSISDN(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
	}

	/**
	 * US503350 EIP Payments: Terms and Conditions
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentsTermsAndConditions(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US503350 EIP Payments: Terms and Conditions");
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: click on gray downward pointing Chevron| EIP blade should be expanded ");

		Reporter.log("Step 7: click 'make a payment' link |  EIP Payment landing page  should  be displayed ");
		Reporter.log("8. click on Terms and Conditions link | T&C's page should be displayed|");
		Reporter.log("9. verify header 'Terms & conditions'| header should be displayed|");
		Reporter.log("10. verify Legal copy text |Legal  text should be displayed|");
		Reporter.log("11. click on Back CTA | EIP Payments page  should be displayed|");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyEIPJODPageHeader();
		eipJodPage.clickMakeAPaymentBtn();
		EIPPaymentPage eipPaymentPage = new EIPPaymentPage(getDriver());
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.verifyTermsAndConditionsLink();
		eipPaymentPage.clickTermsAndConditionsLink();
		eipPaymentPage.verifyTermsandConditionsPage();
		eipPaymentPage.clickTandCbackBtn();
		eipPaymentPage.verifyPageLoaded();
	}

	/**
	 * US473941 EIP/JOD Modernization: Collapsed Completed JOD Blades
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCollapsedCompletedJODBlades(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US473941 EIP/JOD Modernization: Collapsed Completed JOD Blades");
		Reporter.log("Data Condition | Any User JOD completed installemnets");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 4.  click on 'completed' tab| List of completed EIP's should be displayed");
		Reporter.log("Step 6: Verify Device image |  Device image  should be displayed on collapsed EIP blade ");
		Reporter.log("Step 7: Verify Friendly name of the line|  Friendly name of the line should be displayed ");
		Reporter.log("Step 8: Verify MSISDN associated with the plan|  MSISDN should be displayed ");
		Reporter.log("Step 9: Verify Device name|  Device name should be displayed ");
		Reporter.log(
				"Step 10: Verify 'Completed' label on each JOD |  'Completed' label on each JOD  should be displayed ");

		Reporter.log("Step 11: Verify Progress bar not displayed|  Progress bar should not  be displayed ");

		Reporter.log("Step 12: click on gray downward facing  Chevron| JOD blade should be expanded ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyEIPJODPageHeader();
		eipJodPage.clickCompletedPlansTab();
		eipJodPage.verifyCompletedJODPlans();
	}

	/**
	 * US473942 EIP/JOD Modernization:Completed Expanded JOD Blades
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testExpandedCompletedJODBlades(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US473942 EIP/JOD Modernization:Completed Expanded JOD Blades");
		Reporter.log("Data Condition | Any User JOD completed installemnets");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6.  click on 'completed' tab| List of completed JOD's should be displayed");
		Reporter.log("Step 7: click on gray downward pointing Chevron| JOD blade should be expanded ");
		Reporter.log("Step 8: Verify Device image |  Device image  should be displayed on collapsed JOD blade ");
		Reporter.log("Step 9: Verify Friendly name of the line|  Friendly name of the line should be displayed ");
		Reporter.log("Step 10: Verify MSISDN associated with the plan|  MSISDN should be displayed ");
		Reporter.log("Step 11: Verify Device name|  Device name should be displayed ");
		Reporter.log(
				"Step 12: Verify 'Completed' label on each JOD |  'Completed' label on each JOD  should be displayed ");
		Reporter.log("Step 13: Verify Progress bar not displayed|  Progress bar should not  be displayed ");
		Reporter.log("Step 14: Verify Payments remaining|  Payments remaining should be displayed ");
		Reporter.log("Step 15: Verify Original price|Original price should be displayed ");
		Reporter.log("Step 16: Verify Down payment| Down payment should be displayed ");
		Reporter.log("Step 17: Verify Plan start date| Plan start date should be displayed ");
		Reporter.log("Step 18: Verify Plan status| Plan status should be displayed ");
		Reporter.log("Step 19: Verify JOD Plan ID| JOD Plan ID should be displayed ");
		Reporter.log("Step 20: Verify IMEI|IMEI should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyEIPJODPageHeader();
		eipJodPage.clickCompletedPlansTab();
		eipJodPage.verifyCompletedJODPlansAndBladeElements();

	}

	/**
	 * US434672 EIP Payments: Change EIP Page HTML US434679 EIP Payments: Change
	 * EIP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testChangeActiveEIPPlans(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US434679 EIP Payments: Change EIP");
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("7. click on Make a payment link |  EIP payment page should be displayed");
		Reporter.log("8. click on eip blade| Change EIP page should be displayed|");
		Reporter.log("9. verify header 'Select installement plan' | header should be displayed|");
		Reporter.log("10. verify seperate eip blade for each active installment plan |eip blades should be displayed|");

		Reporter.log(
				"11. verify Name ,Msisdn,EIPID,Device Balance for each EIP blade |  Name ,Msisdn,EIPID,Device Balance for each EIP blade should be displayed|");
		Reporter.log(
				"12. select any EIP and click on select CTA |  EIP payment page should be displayed with selected CTA");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyEIPJODPageHeader();
		eipJodPage.clickMakeAPaymentBtn();
		EIPPaymentPage eipPaymentPage = new EIPPaymentPage(getDriver());
		eipPaymentPage.verifyPageLoaded();
		String eipPlanId = eipPaymentPage.getEipPlanId();
		eipPaymentPage.clickEipBlade();
		EIPSelectInstallmentPlanPage eipSelectInstallmentPlanPage = new EIPSelectInstallmentPlanPage(getDriver());
		eipSelectInstallmentPlanPage.verifyPageLoaded();
		eipSelectInstallmentPlanPage.verifyPageElements();
		eipSelectInstallmentPlanPage.selectOtherEipPlan();
		eipSelectInstallmentPlanPage.clickSelectBtn();
		eipPaymentPage.verifyPageLoaded();
		Assert.assertFalse(eipPaymentPage.getEipPlanId().equals(eipPlanId), "Error in validating Change Plan");
	}

	/**
	 * US434488 EIP Payments: EIP Blade Data Population
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPBladeDataPopulation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US434488 EIP Payments: EIP Blade Data Population");
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"Step 6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 7: click on gray downward pointing Chevron| EIP blade should be expanded ");
		Reporter.log("Step 8: click on make a payment button  |  EIP payment page should  be displayed ");
		Reporter.log(
				"Step 9: verify  same EIP data(Name,MSISDN,EIPID,Device Name,Device Balance)  prepopulated in the EIP blade on the EIP payment page| same EIP data should be prepopulated in the EIP blade ");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyEIPJODPageHeader();
		String friendlyName = eipJodPage.getActiveEIPFriendlyName(0);
		String msisdn = eipJodPage.getActiveEIPMsisdn(0);
		String deviceName = eipJodPage.getActiveEIPDeviceName(0);
		String remainingBal = eipJodPage.getRemainingBal(0);
		String eipPlanID = eipJodPage.getActiveEIPPlanID(0);
		eipJodPage.clickMakeAPaymentBtn();
		EIPPaymentPage eipPaymentPage = new EIPPaymentPage(getDriver());
		eipPaymentPage.verifyPageLoaded();
		Assert.assertTrue(eipPaymentPage.getEipPlanId().equals(eipPlanID), "Error in validating EIP Plan ID");
		Assert.assertTrue(eipPaymentPage.getFriendlyName().equals(friendlyName), "Error in validating Friendly Name");
		Assert.assertTrue(eipPaymentPage.getMsisdn().equals(msisdn), "Error in validating Msisdn");
		Assert.assertTrue(eipPaymentPage.getRemainingBal().equals(remainingBal),
				"Error in validating Remaining Balance");
		Assert.assertTrue(eipPaymentPage.getDeviceName().equals(deviceName), "Error in validating Device Name");
	}

	/**
	 * US434489 EIP Payments: Multiple EIPs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testMultipleEIPs(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US434489 EIP Payments: Multiple EIPs");
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"Step 6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 7: click on gray downward pointing Chevron| EIP blade should be expanded ");
		Reporter.log("Step 8: click on make a payment button  |  EIP payment page should  be displayed ");
		Reporter.log(
				"Step 9: verify  EIP blade should have chevron on right if there are multiple EIPs on the account| EIP blade should have chevron on right if there are multiple EIPs on the account ");
		Reporter.log("Step 10: click on EIP blade |  EIP selection page should be displayed ");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyEIPJODPageHeader();
		eipJodPage.clickMakeAPaymentBtn();
		EIPPaymentPage eipPaymentPage = new EIPPaymentPage(getDriver());
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.clickEipBlade();
		EIPSelectInstallmentPlanPage eipSelectInstallmentPlanPage = new EIPSelectInstallmentPlanPage(getDriver());
		eipSelectInstallmentPlanPage.verifyPageLoaded();
	}

	/**
	 * US473328 EIP/JOD Modernization: Active JOD Deeplinking
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testActiveJODDeeplinkingPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US473328 | EIP/JOD Modernization: Active JOD Deeplinking");
		Reporter.log("Data Condition | EIP Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log(
				"3. //Verify Home page | Home page should be displayed// Launch url 'my.t-mobile.com/eip-jod-poip?device=leaseActive_0' | EIPJOD page should be displayed");
		Reporter.log(
				"4. deep link directly to specific Active JOD |  specific JOD blade should be automatically expanded and remaining should be collapsed");
		Reporter.log("5. verify Active tab is selected by default | Active tab should be  selected by default |");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToActiveJODDeeplinkingPage(myTmoData);
		eipJodPage.verifyIfJODBladeExpanded();
		eipJodPage.verifyActiveTabIsSelected();
	}

	/**
	 * US473333 | EIP/JOD Modernization: Active JODs expanded view
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testActiveJODExpandedView(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US473333 | EIP/JOD Modernization: Active JODs expanded view");
		Reporter.log("Data Condition | Any User EIP jod eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"Step 6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 7: Verify Device image |  Device image  should be displayed on collapsed JOD  blade ");
		Reporter.log("Step 8: Verify Friendly name of the line|  Friendly name of the line should be displayed ");
		Reporter.log("Step 9: Verify MSISDN associated with the plan|  MSISDN should be displayed ");
		Reporter.log("Step 10: Verify Device name|  Device name should be displayed ");
		Reporter.log("Step 11: Verify Progress bar|  Progress bar should be displayed ");
		Reporter.log("Step 12: Verify Number of remaining payments| Number of remaining payments should be displayed ");
		Reporter.log("Step 13: Verify Monthly payment amount| Monthly payment amount should be displayed ");
		Reporter.log("Step 14: click on gray downward pointing Chevron| JOD blade should be expanded ");
		Reporter.log(
				"Step 15: Verify Monthly payment amount| Monthly payment amount  should be displayed on expanded JOD  blade ");
		Reporter.log("Step 16: Verify Total lease amount|  Total lease amount should be displayed ");
		Reporter.log("Step 17: Verify Purchase option price|  Purchase option price should be displayed ");
		Reporter.log("Step 18: Verify Lease start date|  Lease start date should be displayed ");
		Reporter.log("Step 19: Verify Lease status|  Lease status should be displayed ");
		Reporter.log("Step 20: Verify Payments remaining| Payments remaining should be displayed ");
		Reporter.log("Step 21: Verify Plan ID| Plan ID should be displayed ");
		Reporter.log("Step 22: Verify IMEI| IMEI should be displayed ");
		Reporter.log("Step 23: Verify 'Understand your lease' Link| 'Understand your lease' Link should be displayed ");
		Reporter.log(
				"Step 24: verify  chevron is pointed up on expanded blade|   chevron should be pointed up on expanded blade");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyActiveJODPlansExpandedView();

	}

	/**
	 * US477843 | EIP/JOD Modernization: Active JODs Collapsed view
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testActiveJODCollapsedView(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US477843 | EIP/JOD Modernization: Active JODs Collapsed view");
		Reporter.log("Data Condition | Any User EIP jod eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"Step 6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 7: Verify Device image |  Device image  should be displayed on collapsed JOD  blade ");
		Reporter.log("Step 8: Verify Friendly name of the line|  Friendly name of the line should be displayed ");
		Reporter.log("Step 9: Verify MSISDN associated with the plan|  MSISDN should be displayed ");
		Reporter.log("Step 10: Verify Device name|  Device name should be displayed ");
		Reporter.log("Step 11: Verify Progress bar|  Progress bar should be displayed ");
		Reporter.log("Step 12: Verify Number of remaining payments| Number of remaining payments should be displayed ");
		Reporter.log("Step 13: Verify Monthly payment amount| Monthly payment amount should be displayed ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyActiveJODPlansCollapsedView();
	}

	/**
	 * US445809 | EIP/JOD Modernization: JOD Progress Bar
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testProgressBarForActiveJOD(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US445809 EIP/JOD Modernization: JOD Progress Bar");
		Reporter.log("Data Condition | Any User  jod eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"Step 6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"Step 7: Verify progress bar on  JOD blade either expanded or collapsed |  progress bar should displayed on JOD  blade either expanded or collapsed ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyActiveJODPlansProgressBars();

	}

	/**
	 * US473887 EIP/JOD Modernization: JOD Notification Ribbons
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPJODNotificationRibbons(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US473887 EIP/JOD Modernization: JOD Notification Ribbons");
		Reporter.log("Data Condition | Any User  Active JOD is ending soon");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"7. verify 'Ending soon' ribbon  display on the lease |'Ending soon' ribbon  should be displayed|");
		Reporter.log(
				"8. verify Lease status updated to 'Ending soon' |  Lease status should be updated to 'Ending soon' ");
		Reporter.log("9. verify 'Take action' CTA|'Take action' CTA  should be displayed");
		Reporter.log("10. click on  'Take action' CTA|POIP Take action page  should be displayed");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyEndingSoonRibbon("Ending Soon");
		eipJodPage.verifyTakeActionCTA();
		eipJodPage.clickTakeActionCTA();
		JODEndingSoonOptionsPage optionsPage = new JODEndingSoonOptionsPage(getDriver());
		optionsPage.verifyPageLoaded();
	}

	/**
	 * US434671: EIP Payments: Edit Amount Page HTML
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEditAmountPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US434671  EIP Payments: Edit Amount Page HTML");
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"Step 6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 7: click on gray downward pointing Chevron| EIP blade should be expanded ");
		Reporter.log("Step 8: click on make a payment button  |  EIP payment page should  be displayed ");
		Reporter.log("Step 9: click on Amount blade|  payment amount page  should be displayed");
		Reporter.log(
				"Step 10: veify Extra device payment radio button is selected by default| Extra device payment radio button should be selected by default");
		Reporter.log(
				"Step 11: enter  amount greater than outstanding eip balance in the Extra device payment text field| 'Amount can not exceed plan balance' text should be displayed");
		Reporter.log(
				"Step 12: enter  amount less than $1.00 in the Extra device payment text field| 'Payment amount must be more than $1.00' text should be displayed");
		Reporter.log(
				"Step 13: select  pay eip in full radio button| Extra device payment  field should be cleared and disabled");
		Reporter.log(
				"Step 14: click on continue button| selected amount should be updated in amount blade on eip payments page ");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyEIPJODPageHeader();
		eipJodPage.clickMakeAPaymentBtn();
		EIPPaymentPage eipPaymentPage = new EIPPaymentPage(getDriver());
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.clickAmountBlade();

		EIPPaymentAmountPage eipAmountPage = new EIPPaymentAmountPage(getDriver());
		eipAmountPage.verifyPageLoaded();
		eipAmountPage.verifyEditAmount();

		eipPaymentPage.verifyPageLoaded();
		Assert.assertTrue(eipPaymentPage.getExtraDeviceAmount().equals("$2.00"),
				"Selected amount is updated in amount blade on eip payments page");

	}

	/**
	 * US474386 POIP Flow Modernization: HTMLs, Headers and Sub-Headers
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEndOfLeaseOptionsPOIPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US474386 POIP Flow Modernization: HTMLs, Headers and Sub-Headers");
		Reporter.log("Data Condition | Any EIP eligible User ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log(
				"Step 3: launch ' End of lease options page' |  ' End of lease options page' page should be displayed");
		Reporter.log(
				"Step 4: Verify 'End of lease options'  Header | 'End of lease options' Header should be displayed");
		Reporter.log(
				"Step 5: verify Option 1 Header: 'Option 1: Pay for your device in installments'| Option 1 Header: should be displayed");
		Reporter.log(
				"Step 6: verify option 1 Sub-header: 'Set up an installment plan to make monthly payments and buy your device'|  'option 1 Sub-header: ' should be displayed");
		Reporter.log(
				"Step 7: verify Option 2 Header: 'Option 2: Turn in or upgrade'| Option 2 Header: should be displayed");
		Reporter.log(
				"Step 8: verify option 2 Sub-header: 'You can turn in your device instead of paying the purchase option price. Visit a retail store to turn in your device or to learn about your upgrade options'|  'option 2 Sub-header: ' should be displayed");
		Reporter.log(
				"Step 9: verify Option 3 Header: 'Option 3: Pay for your device later'| Option 3 Header: should be displayed");
		Reporter.log(
				"Step 10: verify Option 3 Sub-Header: If you prefer, you can pay for your device all at once. Your purchase option price will be on your monthly bill after your final lease payment.|  'option 3 Sub-header: ' should be displayed");
		Reporter.log("================================");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyTakeActionCTA();
		eipJodPage.clickTakeActionCTA();
		JODEndingSoonOptionsPage optionsPage = new JODEndingSoonOptionsPage(getDriver());
		optionsPage.verifyPageLoaded();
		optionsPage.verifyOptionOneHeader(PaymentConstants.POIP_OPTION_ONE);
		optionsPage.verifyOptionOneSubHeader(PaymentConstants.POIP_OPTION_ONE_SUB);
		optionsPage.verifyOptionTwoHeader(PaymentConstants.POIP_OPTION_TWO);
		optionsPage.verifyOptionTwoSubHeader(PaymentConstants.POIP_OPTION_TWO_SUB);
		optionsPage.verifyOptionThreeHeader(PaymentConstants.POIP_OPTION_THREE);
		optionsPage.verifyOptionThreeSubHeader(PaymentConstants.POIP_OPTION_THREE_SUB);

	}

	/**
	 * US474400 POIP Flow Modernization Option 1: MSISDN and Device Info and
	 * Installment Plan
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testMSISDNDeviceInfoAndInstallementPlan(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US474400 EIP/JOD Modernization: JOD Notification Ribbons");
		Reporter.log("Data Condition | Any User  Active JOD is ended ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("7. click on  'Take action' CTA|POIP  action page  should be displayed");
		Reporter.log("8. verify page title 'End of lease options'|page title should be displayed");
		Reporter.log(
				"9. verify Device image,Friendly name,MSISDN,device name,Dollar amount due today  ,Monthly payment amount on option 1 blade | Device image,Friendly name,MSISDN,device name,Dollar amount due today  ,Monthly payment amount should be displayed");
		Reporter.log(
				"10. click on 'Setup installement plan' CTA  on option 1 blade |POIP Payment Page should be displayed");
		Reporter.log("11. verify legal text detailing installment plan |legal text should be displayed");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyTakeActionCTA();
		eipJodPage.clickTakeActionCTA();
		JODEndingSoonOptionsPage optionsPage = new JODEndingSoonOptionsPage(getDriver());
		optionsPage.verifyPageLoaded();
		optionsPage.verifyOption1BladeDetails();
		optionsPage.verifyLegalText();
		optionsPage.clickSetupInstallementPlan();
		PurchaseOptionInstallmentPlanPage poip = new PurchaseOptionInstallmentPlanPage(getDriver());
		poip.verifyPageLoaded();

	}

	/**
	 * US474388 POIP Flow Modernization - Option 2: Store Locator Button
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPOIPFindALocation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US474388 POIP Flow Modernization - Option 2: Store Locator Button");
		Reporter.log("Data Condition | Any User  Active JOD is ended ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("7. click on  'Take action' CTA|POIP  action page  should be displayed");
		Reporter.log("8. verify page title 'End of lease options'|page title should be displayed");
		Reporter.log(
				"9. click on 'Find a location' CTA  on option 2 blade | t-mo store locator page should be displayed");
		Reporter.log(
				"10. verify User zip code automatically searched on the store locator |User zip code should be automatically searched on the store locator");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyTakeActionCTA();
		eipJodPage.clickTakeActionCTA();
		JODEndingSoonOptionsPage optionsPage = new JODEndingSoonOptionsPage(getDriver());
		optionsPage.verifyPageLoaded();
		optionsPage.clickFindALocationButton();
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.switchToWindow();
		StoreLocator storeLocator = new StoreLocator(getDriver());
		storeLocator.verifyPageLoaded();
	}

	/**
	 * US474389 POIP Flow Modernization - Option 3: Pay Later
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPOIPPayLater(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condition | Any User  Active JOD is ended ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("7. click on  'Take action' CTA|POIP  action page  should be displayed");
		Reporter.log("8. verify page title 'End of lease options'|page title should be displayed");
		Reporter.log(
				"9. verify 'Full balance will be on your month bill'  on option 3 blade | 'Full balance will be on your month bill' should be displayed");
		Reporter.log(
				"10. verify remaining plan balance on option 3 blade  |remaining plan balance should be displayed  on option 3 blade");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyTakeActionCTA();
		eipJodPage.clickTakeActionCTA();
		JODEndingSoonOptionsPage optionsPage = new JODEndingSoonOptionsPage(getDriver());
		optionsPage.verifyPageLoaded();
		optionsPage.verifyOption3BladeDetails();
	}

	/**
	 * US487979 EIP/JOD Modernization - PII Masking Payment page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testPiiMaskingOnEIPPaymentPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US487979-EIP/JOD Modernization - PII Masking Payment page");
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log("6. click on make payment button | EIP payment should be displayed");
		Reporter.log("7. verify PII details are masked | PII details should be masked");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPpage(myTmoData);
		//eipJodPage.clickEIPDownwardPointingChevron(0);
		eipJodPage.clickMakeAPaymentBtn();
		EIPPaymentPage paymentPage = new EIPPaymentPage(getDriver());
		paymentPage.verifyPageLoaded();
		paymentPage.verifyNamePiiMasking(PaymentConstants.PII_CUSTOMER_CUSTFIRSTNAME_PID);
		paymentPage.verifyMsisdnPiiMasking(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
		//paymentPage.verifyPaymentMethodPiiMasking(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);

	}

	/**
	 * US476084 Setup Installment Plan - Plan Details
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testSetupInstallmentPLanDetails(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US476084 Setup Installment Plan - Plan Details");
		Reporter.log("Data Condition | Any User  ending JOD ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("7. click on  'Take action' CTA|End of lease options page  should be displayed");
		Reporter.log(
				"8. click on 'setup installement plan'  on option 1 blade | Purchase option installment plan should be displayed");
		Reporter.log(
				"9. verify Device image,Friendly name, MSISDN,Device name,Amount due today,Monthly charge for 9 months on POIP page First Blade| installment plan  information should be verified");
		Reporter.log(
				"10. verify text 'Your card will be charged $X.XX today and you will be charged $X.XX monthly for 9 months.  Your first payment will be billed on MM/DD/YYYY' | text should be verified");
		Reporter.log("11. verify Amount  pre-populated in Amount blade|Amount should be pre-populated in Amount blade");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyTakeActionCTA();
		eipJodPage.clickTakeActionCTA();
		JODEndingSoonOptionsPage optionsPage = new JODEndingSoonOptionsPage(getDriver());
		optionsPage.verifyPageLoaded();
		optionsPage.verifyOption1BladeDetails();
		optionsPage.verifyLegalText();
		optionsPage.clickSetupInstallementPlan();
		PurchaseOptionInstallmentPlanPage poip = new PurchaseOptionInstallmentPlanPage(getDriver());
		poip.verifyPageLoaded();

	}

	/**
	 * US534725: EIP/JOD Modernization User Permissions for Restricted Users
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testEIPJODPageRestrictedUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US534725: EIP/JOD Modernization User Permissions for Restricted Users");
		Reporter.log("Data Condition | EIP Restricted User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. launch url 'my.t-mobile/EIPJOD' | Home page should be displayed");
		Reporter.log("5. Verify Home page | Home page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		HomePage homepage = navigateToHomePage(myTmoData);
		getDriver().get(System.getProperty("environment") + "/eipjod");
		homepage.verifyPageLoaded();
	}

	/**
	 * US534718: EIP/JOD Modernization User Permissions for PAH/Full/Standard
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testEIPJODPagePahFullUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US534718: EIP/JOD Modernization User Permissions for PAH/Full/Standard");
		Reporter.log("Data Condition | EIP Eligible PAH|FULL User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("6. verify plans and leases | Ban specific plans and leases should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyPageLoaded();
		
	}
	
	

	/**
	 * US534718: EIP/JOD Modernization User Permissions for PAH/Full/Standard
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPJODPageStandardUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US534718: EIP/JOD Modernization User Permissions for PAH/Full/Standard");
		Reporter.log("Data Condition | EIP eligible Standard User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("6. verify plans and leases | User specific plans and leases should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyPageLoaded();
		
	}

	/**
	 * US477809: EIP Payments: SUBMIT Payments
	 * US463617: EIP Payment Success: HTMLS
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPJODSubmitPaymentsSuccesspage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US477809-EIP Payments: SUBMIT Payments");
		Reporter.log("Test Case : US463617-EIP Payment Success: HTMLS");
		
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log("6. click on make payment button | EIP payment should be displayed");
		Reporter.log("7. enter valid card details and click on agree&submit | EIP Payment Success  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.clickMakeAPaymentBtn();
		EIPPaymentPage eipPaymentPage = new EIPPaymentPage(getDriver());
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.clickAmountBlade();
		EIPPaymentAmountPage eipAmountPage = new EIPPaymentAmountPage(getDriver());
		eipAmountPage.verifyPageLoaded();
		eipAmountPage.setAmount(generateRandomAmount());
		eipAmountPage.clickContinueBtn();
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), spokePage);
	//	eipPaymentPage.clickAgreeandSubmitBtn();
		
	}
	
	
	/**
	 * US562849 :Future POIP treatment
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPJODNoTakeActionCTAofFutureTreatMent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US562849 Future POIP treatment");
		Reporter.log("Data Condition | Any User  Active JOD is ending soon");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link |  billing page should be displayed");
		Reporter.log("5. click on equipment installation plan| EIP JOD page should be displayed|");
		Reporter.log(
				"6. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log(
				"7. verify 'Ending soon' ribbon  display on the lease |'Ending soon' ribbon  should be displayed|");
		Reporter.log(
				"8. verify Lease status updated to 'Ending soon' |  Lease status should be updated to 'Ending soon' ");
		Reporter.log("9. verify 'Take action' CTA|'Take action' CTA  should Not be displayed");
		

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.clickExpandAllLeaseDetails();
		eipJodPage.verifyFututreOptions("donotuse15081","Ending Soon",PaymentConstants.POIP_TAKEACTION_TEXT);
		
	}
	
	/**
	 *DE239064
	E1_saved payment method should be displayed to save the card in EIP flow
	Details
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { })
	public void testSavePaymentCheckBoxinEIPCardFlow(ControlTestData data, MyTmoData myTmoData) {
		
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log(
				"Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: click on Make payment button| EIP payment page should be expanded ");
		Reporter.log("Step 7: click on paymnet blade| EIP payment collection should be expanded ");

		Reporter.log(
				"Step 8: click on Add a card page |  Add card  page  should  be displayed ");
		Reporter.log(
				"Step 9: verify save payment checkbox| save payment checkbox  should  be displayed ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyEIPJODPageHeader();
		eipJodPage.clickMakeAPaymentBtn();
		EIPPaymentPage eipPaymentPage = new EIPPaymentPage(getDriver());
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.clickPaymentMethodBlade();
		EIPPaymentCollectionPage eippaymentcollection = new EIPPaymentCollectionPage(getDriver());
		eippaymentcollection.verifyPageLoaded();
		eippaymentcollection.clickAddaCardBlade();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyPageHeader(PaymentConstants.Add_card_Header);
		addCardPage.verifySavePaymentMethodCheckBox();

	}
	
	/**
	 *DE239064
	E1_saved payment method should be displayed to save the card in EIP flow
	Details
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { })
	public void testExpireddateinEIPCardFlow(ControlTestData data, MyTmoData myTmoData) {
		
		Reporter.log("Data Condition | Any User EIP  plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");
		Reporter.log("Step 4. launch url 'my.t-mobile/EIPJOD' | EIPJOD page should be displayed");
		Reporter.log("Step 5. verify page title 'Equipment Installment Plans & Leases' | page title 'Equipment Installment Plans & Leases'  should be displayed");
		Reporter.log("Step 6: click on Make payment button| EIP payment page should be expanded ");
		Reporter.log("Step 7: click on paymnet blade| EIP payment collection should be expanded ");
		Reporter.log("Step 8: click on Add a card page |  Add card  page  should  be displayed ");
		Reporter.log("Step 9: enter card details with invalid expired date | Validation error message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyEIPJODPageHeader();
		eipJodPage.clickMakeAPaymentBtn();
		EIPPaymentPage eipPaymentPage = new EIPPaymentPage(getDriver());
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.clickPaymentMethodBlade();
		EIPPaymentCollectionPage eippaymentcollection = new EIPPaymentCollectionPage(getDriver());
		eippaymentcollection.verifyPageLoaded();
		eippaymentcollection.clickAddaCardBlade();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyPageHeader(PaymentConstants.Add_card_Header);
		addCardPage.fillCardInfo(new Payment());
		addCardPage.verifyExpirationdateErrorMsg("Enter a valid expiration date");
		addCardPage.verifyContinueBtnEnabled();

	}

	
}