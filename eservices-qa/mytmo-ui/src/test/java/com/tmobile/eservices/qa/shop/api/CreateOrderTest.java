package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.CartApi;
import com.tmobile.eservices.qa.api.eos.CartApiV4;
import com.tmobile.eservices.qa.api.eos.OrderApi;
import com.tmobile.eservices.qa.api.eos.QuoteApi;
import com.tmobile.eservices.qa.api.eos.QuoteApiV5;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.shop.ShopConstants;

import io.restassured.response.Response;

public class CreateOrderTest extends OrderApi {

	public Map<String, String> tokenMap;
	JsonPath jsonPath;

	/**
	 * UserStory# Description: create Order
	 *
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	// @Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = {
	// "createOrder",Group.SHOP, Group.SPRINT })
	public void testCreateOrder(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: create Order");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 3: Verify Success services response code for Create quote |Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "createCartForQuote.txt");
		CartApi cartApi = new CartApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");
		String operationName = "createCartForQuote";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		QuoteApi quoteApi = new QuoteApi();
		operationName = "createQuote";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "createQuote.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApi.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId", getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		operationName = "UpdatePaymentForCart";
		String updatePaymentRequestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_QUOTE + "UpdatePaymentForCart.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody, tokenMap);
		logRequest(updatePaymentRequest, operationName);
		response = quoteApi.updatePayment(apiTestData, updatePaymentRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		operationName = "CreateOrder";
		String createOrderRequestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_ORDER + "CreateOrder.txt");
		String createOrderRequest = prepareRequestParam(createOrderRequestBody, tokenMap);
		logRequest(createOrderRequest, operationName);
		response = createOrder(apiTestData, createOrderRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);

		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * US455672 #EOS create order API - updatePayment end-point
	 * (/v5/quote/{quoteId}/payment)-UPGRADE + FRP
	 *
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 **/
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "testCreateOrderWithFRP", "US455672", Group.SHOP,
			Group.PROGRESSION, Group.RELEASE })
	public void testCreateOrderWithFRP(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Create Order With FRP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should be success returning cart id.");
		Reporter.log("Step 2: Verify Success services response code for update cart|Response code should be 200 OK.");
		Reporter.log(
				"Step 3: Verify Success services response code for Create quote and update quote|Response code should be 200 OK.");
		Reporter.log(
				"Step 4: Verify Success services response code for update Payment|Response code should be 200 OK.");
		Reporter.log("Step 5: Verify Success services response code for Create Order|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "createCartForPayment.txt");
		CartApiV4 cartApi = new CartApiV4();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");
		String operationName = "createCartForPayment";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");

				Reporter.log("Cart Id is not null");

				Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				Reporter.log("Ban present in request is same as one fetched from excel sheet");

				Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				Reporter.log("MSISDN present in request is same as one fetched from excel sheet");

				Assert.assertEquals(jsonPath.get("cart.lines[0].items.device.sku"), apiTestData.getSku());
				Reporter.log("SKU present in request is same as one fetched from excel sheet");

				Assert.assertEquals(jsonPath.get("cart.lines[0].linePriceSummary.type"), "FULL");
				Reporter.log("Line is FULL");

				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		String requestBodyUpdateCart = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_QUOTE + "UpdateCartForPayment.txt");
		operationName = "Update Cart For Payment";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = cartApi.updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");

				Assert.assertNotNull(jsonPath.get("lines[0].items.accessories[0].sku"), "Device SkuNumber is null");
				Reporter.log("Device SkuNumber is not null");

				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");
				Assert.assertTrue(response.statusCode() == 200);

				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		operationName = "createQuoteforPaymemnt";
		QuoteApiV5 quoteApiV5 = new QuoteApiV5();
		String quoteRequestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_QUOTE + "createQuoteforPayment.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId", getPathVal(jsonNode, "orderId"));
				if (StringUtils.isNoneEmpty(getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"))) {
					tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				} else {
					throw new Exception("Cannot Proceed to Update Quote since shipmentDetailId is Empty");
				}
				if (StringUtils.isNoneEmpty(getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"))) {
					tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
				} else {
					throw new Exception("Cannot Proceed to Update Quote since shippingOptionId is Empty");
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		operationName = "UpdateQuoteForPayment";
		String UpdateQuoteForPaymentRequestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_QUOTE + "UpdateQuoteForPayment.txt");
		String UpdateQuoteForPaymentRequest = prepareRequestParam(UpdateQuoteForPaymentRequestBody, tokenMap);
		logRequest(UpdateQuoteForPaymentRequest, operationName);
		response = quoteApiV5.updateQuote(apiTestData, UpdateQuoteForPaymentRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			jsonPath = new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("status.statusMessage"), "quote updated successfully");
			Reporter.log("quote updated successfully");
			logSuccessResponse(response, operationName);

		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Update Quote Response status code : " + response.getStatusCode());
			Reporter.log("Update Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		// commenting below as we will not place orders in Prod

		/*
		 * operationName="UpdatePaymentCheck"; String updatePaymentRequestBody = new
		 * ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +
		 * "UpdatePaymentCheck.txt"); String updatePaymentRequest =
		 * prepareRequestParam(updatePaymentRequestBody,tokenMap);
		 * logRequest(updatePaymentRequest, operationName); response
		 * =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		 * 
		 * if (response != null &&
		 * "200".equals(Integer.toString(response.getStatusCode()))) {
		 * logSuccessResponse(response, operationName); jsonPath=new
		 * JsonPath(response.asString()); Assert.assertTrue(response.statusCode()==200);
		 * Reporter.log("Status code is 200");
		 * Assert.assertEquals(jsonPath.get("status.statusMessage"),
		 * "payment posted successfully"); Reporter.log("Payment posted successfully");
		 * 
		 * } else { Reporter.log(" <b>Update Payment Exception Response :</b> ");
		 * Reporter.log("Update Payment Response status code : " +
		 * response.getStatusCode()); Reporter.log("Update Payment Response : " +
		 * response.body().asString()); failAndLogResponse(response, operationName); }
		 * operationName="CreateOrder"; String createOrderRequestBody = new
		 * ServiceTest().getRequestFromFile(ShopConstants.SHOP_ORDER +
		 * "CreateOrderFRP.txt"); String createOrderRequest =
		 * prepareRequestParam(createOrderRequestBody,tokenMap);
		 * logRequest(createOrderRequest, operationName); response
		 * =createOrder(apiTestData, createOrderRequest,tokenMap); if (response != null
		 * && "200".equals(Integer.toString(response.getStatusCode()))) {
		 * logSuccessResponse(response, operationName); jsonPath=new
		 * JsonPath(response.asString());
		 * Assert.assertNotNull(jsonPath.get("orderSummary[0].skuCode"),
		 * "sku code is null"); Reporter.log("sku code is not null");
		 * Assert.assertNotNull(jsonPath.get("orderNumber"), "order number is null");
		 * Reporter.log("order number is not null");
		 * Reporter.log("Order Created Successfully");
		 * 
		 * } else { Reporter.log(" <b>Exception Response :</b> ");
		 * Reporter.log("Create Order Response status code : " +
		 * response.getStatusCode()); Reporter.log("Create Order Response : " +
		 * response.body().asString()); failAndLogResponse(response, operationName); }
		 * 
		 */
	}

	/**
	 * US507478 #EOS create order API - createOrder end-point (/v4/orders)-Upgrade+
	 * EIP
	 *
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 **/
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test", Group.SHOP, Group.PROGRESSION,
			Group.RELEASE })
	public void testCreateOrderWithLoanAgreementAndUpgrade(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: test loanAgreement with upgrade ");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Test create cart with UPGRADE| CartId should be created and Response should be 200 OK");
		Reporter.log("Step 2: Test update plan |Plan id should not be null and response should be 200");
		Reporter.log("Step 3: Test Create Quote|Quote Id should not be null and response should be 200");
		Reporter.log("Step 4: Test Update Quote|Quote should be updated successfully and response should be 200");
		Reporter.log("Step 5: Test Loan Agreement |Response should be 200 and verify   loan details");
		Reporter.log("Step 6: Test Create Order |Response should be 200 OK and verify order number created");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "createCartForPaymentEIP.txt");
		CartApiV4 cartApi = new CartApiV4();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");
		String operationName = "createCartForPayment";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");

				Reporter.log("Cart Id is not null");

				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		String requestBodyUpdateCartForPlan = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "UpdatePlanForFraudEIP.txt");
		operationName = "Update plan For EIP";
		String updatedRequestUpdateCartForPlan = prepareRequestParam(requestBodyUpdateCartForPlan, tokenMap);
		logRequest(updatedRequestUpdateCartForPlan, operationName);
		response = cartApi.updateCart(apiTestData, updatedRequestUpdateCartForPlan, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		operationName = "createQuoteforPaymemnt";
		QuoteApiV5 quoteApiV5 = new QuoteApiV5();
		String quoteRequestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_QUOTE + "createQuoteforPayment.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId", getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		operationName = "UpdateQuoteForPayment";
		String UpdateQuoteForPaymentRequestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_QUOTE + "UpdateQuoteForPayment.txt");
		String UpdateQuoteForPaymentRequest = prepareRequestParam(UpdateQuoteForPaymentRequestBody, tokenMap);
		logRequest(UpdateQuoteForPaymentRequest, operationName);
		response = quoteApiV5.updateQuote(apiTestData, UpdateQuoteForPaymentRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			jsonPath = new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("status.statusMessage"), "quote updated successfully");
			Reporter.log("quote updated successfully");
			logSuccessResponse(response, operationName);

		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Update Quote Response status code : " + response.getStatusCode());
			Reporter.log("Update Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		// commenting below as we will not place orders in Prod
		/*
		 * operationName="UpdatePaymentCheck"; String updatePaymentRequestBody = new
		 * ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE
		 * +"UpdatePaymentCheck.txt"); String updatePaymentRequest =
		 * prepareRequestParam(updatePaymentRequestBody,tokenMap);
		 * logRequest(updatePaymentRequest, operationName); response
		 * =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		 * 
		 * if (response != null &&
		 * "200".equals(Integer.toString(response.getStatusCode()))) {
		 * logSuccessResponse(response, operationName); jsonPath=new
		 * JsonPath(response.asString()); Assert.assertTrue(response.statusCode()==200);
		 * Reporter.log("Status code is 200");
		 * Assert.assertEquals(jsonPath.get("status.statusMessage"),
		 * "payment posted successfully"); Reporter.log("Payment posted successfully");
		 * 
		 * } else { Reporter.log(" <b>Update Payment Exception Response :</b> ");
		 * Reporter.log("Update Payment Response status code : " +
		 * response.getStatusCode()); Reporter.log("Update Payment Response : " +
		 * response.body().asString()); failAndLogResponse(response, operationName); }
		 * 
		 * 
		 * 
		 * operationName="loanAgreementOps"; String loanRequestBody = new
		 * ServiceTest().getRequestFromFile(ShopConstants.SHOP_LOANAGREEMENT +
		 * "loanAgreement.txt"); tokenMap.put("languagePreference", "ENGLISH");
		 * LoanAgreementApiV2 loanAgreementApiV2=new LoanAgreementApiV2();
		 * 
		 * String updatedloanRequestBody = prepareRequestParam(loanRequestBody,
		 * tokenMap); logRequest(updatedloanRequestBody, operationName); response
		 * =loanAgreementApiV2.loanAgreementOps(apiTestData,
		 * updatedloanRequestBody,tokenMap); if (response != null &&
		 * "200".equals(Integer.toString(response.getStatusCode()))) {
		 * logSuccessResponse(response, operationName); ObjectMapper mapper = new
		 * ObjectMapper(); JsonNode jsonNode = mapper.readTree(response.asString()); if
		 * (!jsonNode.isMissingNode()) { jsonPath= new JsonPath(response.asString());
		 * 
		 * Assert.assertEquals(jsonPath.get("ban"), apiTestData.getBan()); Reporter.
		 * log("MSISDN present in response is same as one fetched from excel sheet");
		 * 
		 * String statusCode= getPathVal(jsonNode, "serviceStatus.statusCode");
		 * Assert.assertEquals(statusCode,"100","Invalid statusCode");
		 * 
		 * Assert.assertNotNull(jsonPath.get("documentId"), "document Id is null");
		 * Reporter.log("document Id is not null");
		 * 
		 * Assert.assertNotNull(jsonPath.get("documentUrl"), "document URL is null");
		 * Reporter.log("document URL is not null");
		 * 
		 * Assert.assertNotNull(jsonPath.get("planID"), "planID is null");
		 * Reporter.log("planID is not null");
		 * 
		 * 
		 * } } else { failAndLogResponse(response, operationName); }
		 * 
		 * operationName="CreateOrder"; String createOrderRequestBody = new
		 * ServiceTest().getRequestFromFile(ShopConstants.SHOP_ORDER
		 * +"CreateOrderEIP.txt"); String createOrderRequest =
		 * prepareRequestParam(createOrderRequestBody,tokenMap);
		 * logRequest(createOrderRequest, operationName); response
		 * =createOrder(apiTestData, createOrderRequest,tokenMap); if (response != null
		 * && "200".equals(Integer.toString(response.getStatusCode()))) {
		 * logSuccessResponse(response, operationName); jsonPath=new
		 * JsonPath(response.asString());
		 * Assert.assertNotNull(jsonPath.get("orderSummary[0].skuCode"),
		 * "sku code is null"); Reporter.log("sku code is not null");
		 * Assert.assertNotNull(jsonPath.get("orderNumber"), "order number is null");
		 * Reporter.log("order number is not null");
		 * Reporter.log("Order Created Successfully");
		 * 
		 * } else { Reporter.log(" <b>Exception Response :</b> ");
		 * Reporter.log("Create Order Response status code : " +
		 * response.getStatusCode()); Reporter.log("Create Order Response : " +
		 * response.body().asString()); failAndLogResponse(response, operationName); }
		 */
	}

	/**
	 * US507478 #EOS create order API - create order end-point (/v4/orders)-AddAline
	 * + EIP
	 *
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 **/
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test", Group.SHOP, Group.PROGRESSION,
			Group.RELEASE })
	public void testCreateOrderWithLoanAgreementAndADDALINE(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: test loanAgreement with add a line ");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Step 1: Test create cart with add a line| CartId should be created and Response should be 200 OK");
		Reporter.log("Step 2: Test update plan |Plan id should not be null and response should be 200");
		Reporter.log("Step 3: Test Create Quote|Quote Id should not be null and response should be 200");
		Reporter.log("Step 4: Test Update Quote|Quote should be updated successfully and response should be 200");
		Reporter.log("Step 5: Test Loan Agreement |Response should be 200 and verify   loan details");
		Reporter.log("Step 6: Test Create Order |Response should be 200 OK and verify order number created");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "createCartForPaymentEIP.txt");
		CartApiV4 cartApi = new CartApiV4();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");
		String operationName = "createCartForPayment";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");

				Reporter.log("Cart Id is not null");

				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		String requestBodyUpdateCartForPlan = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "UpdatePlanForFraudEIP.txt");
		operationName = "Update plan For EIP";
		String updatedRequestUpdateCartForPlan = prepareRequestParam(requestBodyUpdateCartForPlan, tokenMap);
		logRequest(updatedRequestUpdateCartForPlan, operationName);
		response = cartApi.updateCart(apiTestData, updatedRequestUpdateCartForPlan, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		operationName = "createQuoteforPaymemnt";
		QuoteApiV5 quoteApiV5 = new QuoteApiV5();
		String quoteRequestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_QUOTE + "createQuoteforPayment.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId", getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		operationName = "UpdateQuoteForPayment";
		String UpdateQuoteForPaymentRequestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_QUOTE + "UpdateQuoteForPayment.txt");
		String UpdateQuoteForPaymentRequest = prepareRequestParam(UpdateQuoteForPaymentRequestBody, tokenMap);
		logRequest(UpdateQuoteForPaymentRequest, operationName);
		response = quoteApiV5.updateQuote(apiTestData, UpdateQuoteForPaymentRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			jsonPath = new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("status.statusMessage"), "quote updated successfully");
			Reporter.log("quote updated successfully");
			logSuccessResponse(response, operationName);

		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Update Quote Response status code : " + response.getStatusCode());
			Reporter.log("Update Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		// commenting below as we will not place orders in Prod

		/*
		 * operationName="UpdatePaymentCheck"; String updatePaymentRequestBody = new
		 * ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +
		 * "UpdatePaymentCheck.txt"); String updatePaymentRequest =
		 * prepareRequestParam(updatePaymentRequestBody,tokenMap);
		 * logRequest(updatePaymentRequest, operationName); response
		 * =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		 * 
		 * if (response != null &&
		 * "200".equals(Integer.toString(response.getStatusCode()))) {
		 * logSuccessResponse(response, operationName); jsonPath=new
		 * JsonPath(response.asString()); Assert.assertTrue(response.statusCode()==200);
		 * Reporter.log("Status code is 200");
		 * Assert.assertEquals(jsonPath.get("status.statusMessage"),
		 * "payment posted successfully"); Reporter.log("Payment posted successfully");
		 * 
		 * } else { Reporter.log(" <b>Update Payment Exception Response :</b> ");
		 * Reporter.log("Update Payment Response status code : " +
		 * response.getStatusCode()); Reporter.log("Update Payment Response : " +
		 * response.body().asString()); failAndLogResponse(response, operationName); }
		 * 
		 * 
		 * 
		 * operationName="loanAgreementOps"; String loanRequestBody = new
		 * ServiceTest().getRequestFromFile(ShopConstants.SHOP_LOANAGREEMENT +
		 * "loanAgreement.txt"); tokenMap.put("languagePreference", "ENGLISH");
		 * LoanAgreementApiV2 loanAgreementApiV2=new LoanAgreementApiV2();
		 * 
		 * String updatedloanRequestBody = prepareRequestParam(loanRequestBody,
		 * tokenMap); logRequest(updatedloanRequestBody, operationName); response
		 * =loanAgreementApiV2.loanAgreementOps(apiTestData,
		 * updatedloanRequestBody,tokenMap); if (response != null &&
		 * "200".equals(Integer.toString(response.getStatusCode()))) {
		 * logSuccessResponse(response, operationName); ObjectMapper mapper = new
		 * ObjectMapper(); JsonNode jsonNode = mapper.readTree(response.asString()); if
		 * (!jsonNode.isMissingNode()) { jsonPath= new JsonPath(response.asString());
		 * 
		 * Assert.assertEquals(jsonPath.get("ban"), apiTestData.getBan()); Reporter.
		 * log("MSISDN present in response is same as one fetched from excel sheet");
		 * 
		 * String statusCode= getPathVal(jsonNode, "serviceStatus.statusCode");
		 * Assert.assertEquals(statusCode,"100","Invalid statusCode");
		 * 
		 * Assert.assertNotNull(jsonPath.get("documentId"), "document Id is null");
		 * Reporter.log("document Id is not null");
		 * 
		 * Assert.assertNotNull(jsonPath.get("documentUrl"), "document URL is null");
		 * Reporter.log("document URL is not null");
		 * 
		 * Assert.assertNotNull(jsonPath.get("planID"), "planID is null");
		 * Reporter.log("planID is not null");
		 * 
		 * 
		 * } } else { failAndLogResponse(response, operationName); }
		 * 
		 * operationName="CreateOrder"; String createOrderRequestBody = new
		 * ServiceTest().getRequestFromFile(ShopConstants.SHOP_ORDER +
		 * "CreateOrderEIP.txt"); String createOrderRequest =
		 * prepareRequestParam(createOrderRequestBody,tokenMap);
		 * logRequest(createOrderRequest, operationName); response
		 * =createOrder(apiTestData, createOrderRequest,tokenMap); if (response != null
		 * && "200".equals(Integer.toString(response.getStatusCode()))) {
		 * logSuccessResponse(response, operationName); jsonPath=new
		 * JsonPath(response.asString());
		 * Assert.assertNotNull(jsonPath.get("orderSummary[0].skuCode"),
		 * "sku code is null"); Reporter.log("sku code is not null");
		 * Assert.assertNotNull(jsonPath.get("orderNumber"), "order number is null");
		 * Reporter.log("order number is not null");
		 * Reporter.log("Order Created Successfully");
		 * 
		 * } else { Reporter.log(" <b>Exception Response :</b> ");
		 * Reporter.log("Create Order Response status code : " +
		 * response.getStatusCode()); Reporter.log("Create Order Response : " +
		 * response.body().asString()); failAndLogResponse(response, operationName); }
		 */

	}
}