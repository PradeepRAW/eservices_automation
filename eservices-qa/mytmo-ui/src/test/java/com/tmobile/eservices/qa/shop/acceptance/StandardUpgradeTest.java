/**
 * 
 */
package com.tmobile.eservices.qa.shop.acceptance;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.OfferDetailsPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionConfirmationPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

/**
 * @author charshavardhana
 *
 */
public class StandardUpgradeTest extends ShopCommonLib {

	/**
	 * Verify test FRP flow With Skip Trade-In with One Accessory 
	 * 
	 * @param data
	 *
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testFRPWithSkipTradeInFlowWithOneAccessory(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : Verify test FRP flow With Skip Trade-In");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on a non promo device to buy | Product details page should be displayed");
		Reporter.log("6. Select Full Retail Price as payment selection dropdown | Dropdown item should be selected");
		Reporter.log("7. Click on a non promo device to buy | Product details page should be displayed");
		Reporter.log("8. Click on Add to cart button|Line selection page should be displayed");
		Reporter.log("9. Click on a line that phone is buying for | Phone selection page should be displayed");
		Reporter.log("10. Click on skip trade-in link | Insurance migration page should be displayed");
		Reporter.log(
				"11. Click on 'I don't want insurance' button (if popup displayed to confirm make sure that you choosed Yes button) |Cart page should be Displayed ");
		Reporter.log("12. Select One Accessory And Click Continue |  Cart page should be displayed");
		Reporter.log("13. Click on continue button in wizard | Billing Address section should be displayed");
		Reporter.log("14. Click on Continue Payment button | Payment section should be displayed in cart page");
		Reporter.log(
				"15. Click on Accept&Continue button | Order should be success, success message should displayed on order information page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoryPLPPage accessoryPLPPage = navigateToAccessoryPLPPageByFeaturedDevicesWithSkipTradeIn(myTmoData);

		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		/*
		 * cartPage.clickAcceptAndPlaceOrder();
		 * 
		 * OrderConfirmationPage orderConfirmationPage = new
		 * OrderConfirmationPage(getDriver());
		 * Assert.assertTrue(orderConfirmationPage.verifyOrderConfirmationPage()
		 * , Constants.ORDER_CONFIRMATION_PAGE_ERROR); Reporter.log(
		 * "Order confirmation page is displayed");
		 * 
		 * Assert.assertTrue(orderConfirmationPage.verifyOrderSuccessMessage(),
		 * Constants.ORDER_CONFIRMATION); Reporter.log(
		 * "Order is placed successfully");
		 */
	}

	/**
	 * Verify test EIP flow With Skip Trade-In
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testEIPWithSkipTradeInFlow(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : Verify test EIP flow With Skip Trade-In");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on select device | Product details page should be displayed");
		Reporter.log("6. Select Monthly payments & Click on add to cart |Line Selection page should be displayed");
		Reporter.log("7. Click on any line|Phone selection page should be displayed");
		Reporter.log("8. Click on skip trade-in link |Cart Page should be Displayed");
		Reporter.log("9. Select Monthly payments & Click on add to cart |Cart page should be displayed");
		Reporter.log("10. Enter payment info & click accept & continue | Order confirmation page should be displayed");
		Reporter.log("11. Verify order success message | Order success message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByFeatureDevicesWithSKipTradeIn(myTmoData, myTmoData.getDeviceName());
		// cartPage.selectShippingMethod();

		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.AcceptAndPlaceOrderButtonEnabled();

		// Assert.assertTrue(cartPage.AcceptAndContinueButtonEnabled(),
		// "AcceptAndPlaceOrder Button in Cart Page Not Enabled");
		// Reporter.log("Accept and Place Order button in Cartpage is Enabled");
		/*
		 * cartPage.clickAcceptAndContinue();
		 * 
		 * OrderConfirmationPage orderConfirmationPage = new
		 * OrderConfirmationPage(getDriver());
		 * Assert.assertTrue(orderConfirmationPage.verifyOrderConfirmationPage()
		 * , Constants.ORDER_CONFIRMATION_PAGE_ERROR); Reporter.log(
		 * "Order confirmation page is displayed");
		 * 
		 * Assert.assertTrue(orderConfirmationPage.verifyOrderSuccessMessage(),
		 * Constants.ORDER_CONFIRMATION); Reporter.log(
		 * "Order is placed successfully");
		 */
	}

	/**


	/**
	 * Verify EIP with trade in flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testEIPWithTradeInFlowWithAccessory(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify test EIP with trade in flow");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Phone page should be displayed");
		Reporter.log("5. Click on select device | PLP page should be displayed");
		Reporter.log("6. Click on the device | PDP page should be displayed");
		Reporter.log("7. Clcik on Add to cart | Line slector page should be displayed");
		Reporter.log("8. Click on any line|Phone selection page should be displayed");
		Reporter.log("9. Click on got a different phone|Trade in another device page should be displayed");
		Reporter.log("10. Select trade in info & click continue|Device condition page should be displayed");
		Reporter.log("11. Click its good radio button & click continue|Trade in value page should be displayed");
		Reporter.log("12. click continue|Product list page should be displayed");
		Reporter.log("13. click on device|Product details page should be displayed");
		Reporter.log("12. Select One Accessory And Click Continue |  Cart page should be displayed");
		Reporter.log(
				"15. Enter payment info & click accept & place order | Order confirmation page should be displayed");
		Reporter.log("16. Verify order success message | Order success message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		TradeInDeviceConditionConfirmationPage deviceConditionConfirmationPage = navigateToTradeInDeviceConditionConfirmationPageByFeatureDevicesWithTradeInFlow(
				myTmoData, myTmoData.getDeviceName());
		deviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		
	
		// Assert.assertTrue(cartPage.AcceptAndContinueButtonEnabled(),
		// "AcceptAndPlaceOrder Button in Cart Page Not Enabled");
		// Reporter.log("Accept and Place Order button in Cartpage is Enabled");
		/*
		 * cartPage.clickAcceptAndContinue();
		 * 
		 * OrderConfirmationPage orderConfirmationPage = new
		 * OrderConfirmationPage(getDriver());
		 * Assert.assertTrue(orderConfirmationPage.verifyOrderConfirmationPage()
		 * , Constants.ORDER_CONFIRMATION_PAGE_ERROR); Reporter.log(
		 * "Order confirmation page is displayed");
		 * 
		 * Assert.assertTrue(orderConfirmationPage.verifyOrderSuccessMessage(),
		 * Constants.ORDER_CONFIRMATION); Reporter.log(
		 * "Order is placed successfully");
		 */
	}

	/**
	 * Verify Trade-In Promo Flow
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testPromoFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify Trade-In promo flow");
		Reporter.log("Data Condition | IR STD PAH Customer With Promo Eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | shop page should be displayed");
		Reporter.log("6. Click on order now |Offer details page should be displayed");
		Reporter.log("7. Click on accept & continue |Line selection page should be displayed");
		Reporter.log("8. Click on line |Phone selection page should be displayed");
		Reporter.log("9. Click Skip Trade In |Insurance Migration Page Should be Displayed");
		Reporter.log("10. Click Continue on Insurance Migration Page | Cart Page Should be Dispalyed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickOrderNowButton();
		OfferDetailsPage offerDetailsPage = new OfferDetailsPage(getDriver());
		offerDetailsPage.verifyOfferDetailsPage();
		offerDetailsPage.clickAcceptAndContinue();
		/*
		 * PLPPage plpPage = new PLPPage(getDriver());
		 * plpPage.verifyPageLoaded();
		 * plpPage.clickDeviceByName(myTmoData.getDeviceName()); PDPPage pdpPage
		 * = new PDPPage(getDriver()); pdpPage.verifyPDPPage();
		 * pdpPage.clickAddToCart();
		 */
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.clickOnSkipTradeInLink();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.checkPageIsReady();
		if (deviceProtectionPage.verifyDeviceProtectionURL() == true) {
			deviceProtectionPage.clickContinueButton();
		}
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
	}

	/**
	 * Verify FRP with trade in flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testFRPWithTradeInFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify test FRP with trade in flow");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Phone page should be displayed");
		Reporter.log("5. Click on select device | PLP page should be displayed");
		Reporter.log("6. Click on the device | PDP page should be displayed");
		Reporter.log("7. Clcik on Add to cart | Line slector page should be displayed");
		Reporter.log("8. Click on any line|Phone selection page should be displayed");
		Reporter.log("9. Click on any line|Phone selection page should be displayed");
		Reporter.log("10. Click on got a different phone|Trade in another device page should be displayed");
		Reporter.log("11. Select trade in info & click continue|Device condition page should be displayed");
		Reporter.log("12. Click its good radio button & click continue|Trade in value page should be displayed");
		Reporter.log("13. click continue|Product list page should be displayed");
		Reporter.log("14. click on device|Product details page should be displayed");
		Reporter.log("15. Select Full Retial Price & Click on add to cart |Cart page should be displayed");
		Reporter.log(
				"16. Enter payment info & click accept & place order | Order confirmation page should be displayed");
		Reporter.log("17. Verify order success message | Order success message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByFeaturedDevicesWithTradeInFlow(myTmoData);

		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.AcceptAndPlaceOrderButtonEnabled();
		
		/*
		 * cartPage.clickAcceptAndPlaceOrder();
		 * 
		 * OrderConfirmationPage orderConfirmationPage = new
		 * OrderConfirmationPage(getDriver());
		 * Assert.assertTrue(orderConfirmationPage.verifyOrderConfirmationPage()
		 * , Constants.ORDER_CONFIRMATION_PAGE_ERROR); Reporter.log(
		 * "Order confirmation page is displayed");
		 * 
		 * Assert.assertTrue(orderConfirmationPage.verifyOrderSuccessMessage(),
		 * Constants.ORDER_CONFIRMATION); Reporter.log(
		 * "Order is placed successfully");
		 */
	}

	/**
	 * US323432: MyTMO - Additional Terms - Standard Upgrade Flow: Display Total
	 * Financed Amount in Cart
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testTotalFinancedAmountDisplayedInCartPageWithEIPoption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case US323432: MyTMO - Additional Terms - Standard Upgrade Flow: Display Total Financed Amount in Cart");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select Monthly PaymentOption | Monthly PaymentOption should be selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line |  Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button | Insurance migration page should be displayed");
		Reporter.log("11. Click continue button | Accessory PLP page should be displayed");
		Reporter.log("12. Click Skip Accessorys button | Cart page should be displayed");
		Reporter.log(
				"13. Verify 'Total financed amount label' | 'Total financed amount label' should be displayed in OrderDetails tab");
		Reporter.log(
				"14. Verify 'Total financed amount' | 'Total financed amount' should be displayed in OrderDetails tab");
		Reporter.log("15. Click on Continue to shipping button | Shipping information should be displayed");
		Reporter.log("16. Click Continue to payment button | Payment information form should be displayed");
		Reporter.log(
				"17. Verify 'Total financed amount label' | 'Total financed amount label' should be displayed in Payments tab");
		Reporter.log(
				"18. Verify 'Total financed amount' | 'Total financed amount' should be displayed in Payments tab");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupCloseButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyOrderFinancedAmountLabel();
		cartPage.verifyFullFinancedAmount();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingDetails();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.verifyPaymentFinancedAmountLabel();
		cartPage.verifyPaymentFinancedAmount();
	}
	/**
	 * US323432: MyTMO - Additional Terms - Standard Upgrade Flow: Display Total
	 * Financed Amount in Cart
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS,"cxlabtest" })
	public void testTotalFinancedAmountNotDisplayedInCartPageWithFRPoption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case US323432: MyTMO - Additional Terms - Standard Upgrade Flow: Display Total Financed Amount in Cart");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select Full PaymentOption | Full PaymentOption should be selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line |  Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button | Insurance migration page should be displayed");
		Reporter.log("11. Click continue button | Accessory PLP page should be displayed");
		Reporter.log("12. Click Skip Accessorys button | Cart page should be displayed");
		Reporter.log(
				"13. Verify 'Total financed amount label' | 'Total financed amount label' should not be displayed in OrderDetails tab");
		Reporter.log(
				"14. Verify 'Total financed amount' | 'Total financed amount' should not be displayed in OrderDetails tab");
		Reporter.log("15. Click on Continue to shipping button | Shipping information should be displayed");
		Reporter.log("16. Click Continue to payment button | Payment information form should be displayed");
		Reporter.log(
				"17. Verify 'Total financed amount label' | 'Total financed amount label' should not be displayed in Payments tab");
		Reporter.log(
				"18. Verify 'Total financed amount' | 'Total financed amount' should not be displayed in Payments tab");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL() == true) {
			deviceProtectionPage.clickOnYesProtectMyPhoneButton();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickPayMonthlyPopupCloseButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();

		cartPage.verifyFullFinancedAmountFRPDisplayedInOrderDetails();
		cartPage.verifyFullFinancedAmountLabelFRPDisplayedInOrderDetails();

		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingDetails();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.verifyFullFinancedAmountFRPDisplayedInPayment();
	}

}
