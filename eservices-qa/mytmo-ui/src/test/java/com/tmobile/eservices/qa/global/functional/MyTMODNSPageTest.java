package com.tmobile.eservices.qa.global.functional;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.DNSCommonPage;
import com.tmobile.eservices.qa.pages.UNAVCommonPage;
import com.tmobile.eservices.qa.global.GlobalCommonLib;

public class MyTMODNSPageTest extends GlobalCommonLib {
	//private static final Logger logger = LoggerFactory.getLogger(TMNGNewHeaderPageTest.class);
	/**
	 * CDCDWR2-42 - CCPA WS4 | No Sell Web, Sell/No Sell App | DNS Page | UI
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyDNSPage(ControlTestData data, MyTmoData myTmoData){
		Reporter.log("CDCDWR2-42 - CCPA WS4 | No Sell Web, Sell/No Sell App | DNS Page | UI");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify DNS Link is displayed | DNS Link is displayed");
		Reporter.log("4.Click on DNS Link | Clicked on DNS Link successfully");
		Reporter.log("5.Verify DNS page | DNS page should be displayed");
		Reporter.log("6.Verify DNS Page Title is displayed | DNS Page Title is displayed");
		Reporter.log("7.Verify DNS Legal Content | DNS Legal Content is displayed");
		Reporter.log("8.Verify Learn More | Learn More should be displayed");
		Reporter.log("9.Verify DNS Global Toggle | Verify Global Toggle is displayed");
		Reporter.log("10.Verify Local Toggle | Verify Local Toggle should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.verifyDoNotSellLabel();
		unavPage.clickDoNotSellLink();
		unavPage.verifyDoNotSellPage();
		DNSCommonPage dnsPage = new DNSCommonPage(getDriver());
	    dnsPage.verifyDNSPage();
		dnsPage.verifyDNSTitle();
		dnsPage.verifyDNSLegalContent();
		dnsPage.verifyDNSCopyMessage();
		dnsPage.verifyDNSLearnMoreLink();
		dnsPage.verifyDNSBrandToggle();
		dnsPage.verifyDNSLocalToggle();
		dnsPage.verifyDNSLoginLink();
	}
	/**
	 * CDCDWR2-240 - CCPA | WS2 | MyTMO | Send Parameters to DNS Page
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void VerifyLocalDNSCookiesInDNSPage(ControlTestData data, MyTmoData myTmoData){
		Reporter.log("CDCDWR2-240 - CCPA | WS2 | MyTMO | Send Parameters to DNS Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify DNS Link is displayed | DNS Link is displayed");
		Reporter.log("4.Click on DNS Link | Clicked on DNS Link successfully");
		Reporter.log("5.Verify DNS page | DNS page should be displayed");
		Reporter.log("6.Verify DNS Cookie values for Autenticated Customer | DNS Cookie Values should be set");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.verifyDoNotSellLabel();
		unavPage.clickDoNotSellLink();
		//unavPage.verifyDoNotSellPage();
		DNSCommonPage dnsPage = new DNSCommonPage(getDriver());
	    dnsPage.verifyDNSPage();
	    dnsPage.getBrandValueFromDNSCookie();
	    dnsPage.getDNSSettingValueFromDNSCookie();
	    dnsPage.getSiteValueFromDNSCookie();
	    dnsPage.getAuthorizationValueFromDNSCookie();
	    dnsPage.getIDTypeValueFromDNSCookie();
	    dnsPage.getUserIDValueFromDNSCookie();
	    dnsPage.getOriginURLValueFromDNSCookie();
	}
}