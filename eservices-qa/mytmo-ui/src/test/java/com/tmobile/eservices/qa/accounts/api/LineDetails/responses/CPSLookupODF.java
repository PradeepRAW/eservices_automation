package com.tmobile.eservices.qa.accounts.api.LineDetails.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CPSLookupODF {
    @JsonProperty
    private AllCurrentServiceDetails[] allCurrentServiceDetails;

    @JsonProperty
    private AllCurrentServiceDetails[] sharedServicesList;

    @JsonProperty
    private GroupedServices[] groupedServicesList;

    public AllCurrentServiceDetails[] getAllCurrentServiceDetails() {
        return allCurrentServiceDetails;
    }

    public void setAllCurrentServiceDetails(AllCurrentServiceDetails[] allCurrentServiceDetails) {
        this.allCurrentServiceDetails = allCurrentServiceDetails;
    }

    public AllCurrentServiceDetails[] getSharedServicesList() {
        return sharedServicesList;
    }

    public void setSharedServicesList(AllCurrentServiceDetails[] sharedServicesList) {
        this.sharedServicesList = sharedServicesList;
    }

    public GroupedServices[] getGroupedServicesList() {
        return groupedServicesList;
    }

    public void setGroupedServicesList(GroupedServices[] groupedServicesList) {
        this.groupedServicesList = groupedServicesList;
    }

    public AllCurrentServiceDetails findCurrentServiceDetails(String soc){
        AllCurrentServiceDetails[] serviceDetails = this.allCurrentServiceDetails;
        return findServiceDetails(soc, serviceDetails);
    }

    public AllCurrentServiceDetails findSharedServiceDetails(String soc){
        AllCurrentServiceDetails[] serviceDetails = this.sharedServicesList;
        return findServiceDetails(soc, serviceDetails);
    }

    public GroupedServices findGroupServiceDetails(String groupCategory) {
        for (GroupedServices groupedServices : this.groupedServicesList) {
            if(groupedServices.getGroupCategory().equals(groupCategory)) {
                return groupedServices;
            }
        }
        throw new IllegalArgumentException("No details found for soc " + groupCategory);
    }

    public static AllCurrentServiceDetails findServiceDetails(String soc, AllCurrentServiceDetails[] serviceDetails) {
        for (AllCurrentServiceDetails detail : serviceDetails) {
            if (detail.getSoc().equals(soc)) {
                return detail;
            }
        }
        throw new IllegalArgumentException("No details found for soc " + soc);
    }

    @Override
    public String toString() {
        return "CPSLookupODF{" +
                "allCurrentServiceDetails=" + Arrays.toString(allCurrentServiceDetails) +
                '}';
    }
}
