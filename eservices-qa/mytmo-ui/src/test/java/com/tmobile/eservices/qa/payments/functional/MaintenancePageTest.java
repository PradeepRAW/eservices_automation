package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.MaintenancePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class MaintenancePageTest extends PaymentCommonLib {
	
	private static final Logger logger = LoggerFactory.getLogger(MaintenancePageTest.class);
	/**
	 * CDCDWG2-141 Billing & Usage: Update Maintenance page for UI Fixes and new view for Restricted Biz & GOV users
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS,"billing"})
	public void verifyPayBillCTAinMaintenancePageRestrictedBizAndGovUsers(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPayBillCTAinMaintenancePageRestrictedBizAndGovUsers method in MaintenancePageTest");
		Reporter.log("Test case:CDCDWG2-141 Billing & Usage: Update Maintenance page for UI Fixes and new view for Restricted Biz & GOV users");
		Reporter.log("Data Condition | Restricted Biz and GOV users");
		Reporter.log("================================");
		
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | BillandPay page should be displayed");
		Reporter.log("5: navigate to Maintenance page | Maintenance page loaded with Pay bill CTA");
		Reporter.log("6: Verify Background color of 'Pay bill' CTA | 'Pay bill' CTA should be displayed in xxxx color");
	
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		MaintenancePage maintenancePage  = navigateToMaintenancePage(myTmoData);
		maintenancePage.verifyPageLoaded();
		maintenancePage.verifyPayNowButtonBackgroundColor();

  }
}
