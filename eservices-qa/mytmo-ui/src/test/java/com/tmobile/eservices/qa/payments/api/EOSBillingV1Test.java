package com.tmobile.eservices.qa.payments.api;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.EOSCommonMethods;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EOSBillingFilters;

import io.restassured.response.Response;

public class EOSBillingV1Test extends EOSCommonMethods {

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEosbillingV1Dataset(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSBillingFilters billing = new EOSBillingFilters();
		// EOSCommonMethods ecm=new EOSCommonMethods();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response datasetresponse = billing.getResponsedataset(getjwt);
			checkexpectedvalues(datasetresponse, "statusCode", "100");
			checkexpectedvalues(datasetresponse, "statusMessage", "Success");

			checkexpectedvalues(datasetresponse, "loggedInUserInfo.userRole", getjwt[4]);
			checkexpectedvalues(datasetresponse, "loggedInUserInfo.emailAddress", getjwt[5]);
			checkexpectedvalues(datasetresponse, "accountNumber", getjwt[3]);
			checkexpectedvalues(datasetresponse, "msisdn", getjwt[0]);

			String alltags[] = { "currentBillCharges.currentBillDueAmount", "autoPay.easyPayStatus",
					"eipDetailsSection.isEIPDetailsExists", "jodDetailsSection.isJumpLeaseDetailsExists",
					"paymentArrangementDetails.paymentArrangementIndicator", "loggedInUserInfo", "accountLinesInfo",
					"pastdueAmountDetails.pastdueAmount", "paperlessBilling" };
			for (String tag : alltags) {
				checkjsontagitems(datasetresponse, tag);
			}
		}

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEosbillingV1getBillList(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetBillList");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the billlist of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSBillingFilters billing = new EOSBillingFilters();
		// EOSCommonMethods ecm=new EOSCommonMethods();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response billlistresponse = billing.getResponsebillList(getjwt);
			checkexpectedvalues(billlistresponse, "statusCode", "100");
			checkexpectedvalues(billlistresponse, "statusMessage", "Bill list found");

			String alltags[] = { "BillList.documentId", "BillList.currentCharges", "BillList.startDate",
					"BillList.endDate", "BillList.amountDue" };
			for (String tag : alltags) {
				checkjsontagitemsList(billlistresponse, tag);
			}

		}

	}

}