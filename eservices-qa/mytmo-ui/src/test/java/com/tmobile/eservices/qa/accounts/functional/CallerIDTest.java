package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.CallerIDPage;
import com.tmobile.eservices.qa.pages.accounts.LineSettingsPage;

public class CallerIDTest extends GlobalCommonLib {

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1259NotDone")
	public void verifyCallerIDNamePageAuthorableTexts(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name Authorable texts");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.verifyAuthorabletext("Caller ID name");
		callerIdPage.verifyAuthorabletext("This is the name people will see on their phone when you call them.");
		callerIdPage.verifyAuthorabletext(
				"NOTE: Please enter letters and numbers only. It may take up to 72 hours for Caller ID updates to take effect.");
		callerIdPage.verifyAuthorabletext(
				"We want the people you call to know who's actually calling them. When you update your Caller ID name, you're confirming that the new name is your legal name, a commonly-accepted variation of your legal name, or the name everyone calls you. If you change your Caller ID name to something we believe is profane, inappropriate, designed to trick people, or infringes on someone's intellectual property rights, we reserve the right to change it, with or without notice.");

		// VerifyCallerId name page
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-944")
	public void verifyCallerIdNameShowsInLineSettings(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.getCallerIdName();

	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-944NotDone")
	public void verifyCallerIdNameShowsInLineSettingsForDifferentLines(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.getCallerIdName();
		lineSettingsPage.selectLineFromDropDown();
		lineSettingsPage.getCallerIdName();

	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-951")
	public void verifyOnClickingCallerIDNameBladeOnLineSettingsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();

	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-952Notdone")
	public void verifyCallerIDNamePageForPAHUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		// VerifyCallerId name page

	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-952NotDone")
	public void verifyCallerIDNamePageForFullUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		// VerifyCallerId name page

	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-952NotDone")
	public void verifyCallerIDNamePageForRestrictedUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		// VerifyCallerId name page

	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-952NotDone")
	public void verifyCallerIDNamePageForStandardUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();

		// VerifyCallerId name page

	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1257NotDone")
	public void verifyCallerIDErrorMessageBasedonCharactersEnteredforFirstNLastName(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.enterCallerIdDetails(myTmoData);
		callerIdPage.verifyAuthorabletext("Only letters and spaces are allowed");
		callerIdPage.verifyAuthorabletext("Only letters and spaces are allowed");

	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1257NotDone")
	public void verifyCallerIDErrorMessageWhenCharactersEnteredforFirstNLastNameExceed50(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.enterCallerIdDetails(myTmoData);
		callerIdPage.verifyAuthorabletext("Please limit your name to 32 characters or less");
		callerIdPage.verifyAuthorabletext("Please limit your name to 32 characters or less");

	}
	
	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1726")
	public void verifyCallerIDErrorMessageWhenFirstNLastNameHaveOnlySpaces(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.enterCallerIdDetails(myTmoData);
		callerIdPage.verifyAuthorabletext("First name can't be only spaces");
		callerIdPage.verifyAuthorabletext("Last name can't be only spaces");

	}
	
	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1726")
	public void verifyCallerIDAgreeNSubmintBtnDisableWhenFirstNameisNotEntered(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.enterCallerIdDetails(myTmoData);
		callerIdPage.verifyAuthorabletext("First name is required");
		callerIdPage.isAgreeNSubmitDisabled();

	}
	
	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1726")
	public void verifyCallerIDAgreeNSubmintBtnDisableWhenLastNameisNotEntered(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.enterCallerIdDetails(myTmoData);
		callerIdPage.verifyAuthorabletext("Last name is required");
		callerIdPage.isAgreeNSubmitDisabled();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1267")
	public void verifyCallerIDNamePageAuthorableTextsForSTDUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name Authorable texts");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.verifyAuthorabletext("Caller ID name");
		callerIdPage.verifyAuthorabletext("This is the name people will see on their phone when you call them.");
		callerIdPage.verifyAuthorabletext("Please contact your primary account holder to change your Caller ID");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1261_s1")
	public void verifyCallerIDNameAuthorableLablesNNamesForSTDUSer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Restricted or standard");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name Authorable texts");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.verifyCalledIdNameAuthorableLables("FirstName");
		callerIdPage.verifyCalledIdNameAuthorableLables("LastName");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1261_s2")
	public void verifyCallerIDNameAuthorableLablesNNamesForPAHUSer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name Authorable texts");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.verifyCalledIdNameAuthorableLables("FirstName");
		callerIdPage.verifyCalledIdNameAuthorableLables("LastName");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1264")
	public void verifyCallerIDNameSuccessPageVerifyAuthorableTexts(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. click on caller ID");
		Reporter.log("7. enter caller ID details first name last name and click on agree and submit");
		Reporter.log("8. Verify caller ID Name ConfirmationPage Authorable texts");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.enterNAgreeCallerId(myTmoData);
		callerIdPage.verifyCallerIdNamePage();
		callerIdPage.verifyAuthorabletext("Thank you! We've received your new Caller ID submission");
		callerIdPage.verifyAuthorabletext(
				" Your new Caller ID name will be updated within 72 hours. Please check back later.");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1264")
	public void verifyCallerIDNameSuccessPageVerifyLineSeclector(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. click on caller ID");
		Reporter.log("7. enter caller ID details first name last name and click on agree and submit");
		Reporter.log("8. Select other line in caller id name page");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.enterNAgreeCallerId(myTmoData);
		callerIdPage.verifyCallerIdNamePage();
		lineSettingsPage.selectLineFromDropDownList();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1264")
	public void verifyCallerIDNameSuccessPageBackToLineDetails(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name Authorable texts");
		Reporter.log("6. click on caller ID");
		Reporter.log("7. enter caller ID details first name last name and click on agree and submit");
		Reporter.log("8. verify Back to plan details");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.enterNAgreeCallerId(myTmoData);
		callerIdPage.verifyCallerIdNamePage();
		callerIdPage.verifyCallerIdNamePageBackToLineSettings("Back to line settings");
		lineSettingsPage.verifyLineSettingsPage();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1454")
	public void verifyCallerIDNameFailurePageVerifyAuthorableTexts(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. click on caller ID");
		Reporter.log("7. enter caller ID details first name last name a bad word and click on agree and submit");
		Reporter.log("8. Verify caller ID Name failurePage Authorable texts");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.enterNAgreeCallerId(myTmoData);
		callerIdPage.verifyCallerIdNamePage();
		callerIdPage.verifyAuthorabletext("Oops. We're unable to change your Caller ID at the moment.");
		callerIdPage.verifyAuthorabletext(
				"Let's connect you with one of our award-winning customer care reps to assist you with this request.");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1454")
	public void verifyCallerIDNameFailurePageVerifyLineSeclector(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. click on caller ID");
		Reporter.log("7. enter caller ID details first name last name bad word and click on agree and submit");
		Reporter.log("8. Select other line in caller id name page");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.enterNAgreeCallerId(myTmoData);
		callerIdPage.verifyCallerIdNamePage();
		lineSettingsPage.selectLineFromDropDownList();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-1454")
	public void verifyCallerIDNameFailurePageContactUs(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify Caller id Name Authorable texts");
		Reporter.log("6. click on caller ID");
		Reporter.log("7. enter caller ID details first name last name and click on agree and submit");
		Reporter.log("8. verify Back to plan details");
		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		CallerIDPage callerIdPage = new CallerIDPage(getDriver());
		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyNclickLineSettingsAuthorableLinks("Caller ID");
		lineSettingsPage.verifyCallerIDNamePage();
		callerIdPage.enterNAgreeCallerId(myTmoData);
		callerIdPage.verifyCallerIdNamePage();
		callerIdPage.verifyCallerIdNamePageBackToLineSettings("Contact us");
	}
}
