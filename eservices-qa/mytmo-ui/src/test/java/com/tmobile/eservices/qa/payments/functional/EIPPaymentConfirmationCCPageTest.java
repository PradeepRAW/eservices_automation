/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.AddCardPage;
import com.tmobile.eservices.qa.pages.payments.BackToShopTransitionPage;
import com.tmobile.eservices.qa.pages.payments.BillingSummaryPage;
import com.tmobile.eservices.qa.pages.payments.EIPDetailsPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentConfirmationCCPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentPage;
import com.tmobile.eservices.qa.pages.payments.EipJodPaymentConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

/**
 * @author charshavardhana
 *
 */
public class EIPPaymentConfirmationCCPageTest extends PaymentCommonLib{


	/**
	 * US357159 EIP Payment Return to Shop > Supress Return to Billing Home Link
	 * Flow: JUMP EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentReturntoShopSupressReturnToBillingHomeLinkForJUMPEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US357159 EIP Payment Return to Shop > Supress Return to Billing Home Link");
		Reporter.log("Data Condition | JUMP2.0 customer who has not paid 50% of eip amount");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on any Featured Phone | Product details page should be displayed");
		Reporter.log("6. Click on Add to cart | line selecto rpage should be displayed");
		Reporter.log("6. Click on Pay now for EIP Payment | EIP payment page should be displayed");
		Reporter.log("7. Enter payment details and submit | Payment Success page should be displayed");
		Reporter.log("8. Verify 'Return to Billing Home' link | Link should not be displayed");

		EIPPaymentConfirmationCCPage confirmationCCPage = navigatefromShoptoEIPpaymentConfirmationPage(myTmoData);
		confirmationCCPage.verifyReturnToBillingHomeLinkIsSupressed();
	}

	/**
	 * US357159 EIP Payment Return to Shop > Supress Return to Billing Home Link
	 * Flow: Regular EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentDisplayReturnToBillingHomeLinkForRegularEIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US357159 EIP Payment Return to Shop > Supress Return to Billing Home Link");
		Reporter.log("Data Condition | EIP eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log(
				"5.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("6. Click on Installment Plan blade | EIP details page should be displayed");
		Reporter.log("7. Click on make payment button | EIP payment page should be displayed");
		Reporter.log("8. Enter payment details and submit | Payment Success page should be displayed");
		Reporter.log("9. Verify 'Return to Billing Home' link and click on it| Billing page should be displayed");

		EIPPaymentConfirmationCCPage paymentConfirmationCCPage = navigateToEIPPaymentConfirmationCCPage(myTmoData,
				"Card");
		paymentConfirmationCCPage.verifyReturnToBillingHomeLink();
		paymentConfirmationCCPage.clickReturnToBillingHomeLink();
		BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
		billingSummaryPage.verifyPageLoaded();
	}

	/**
	 * US357161 EIP Payment Return to Shop > Supress payment history link Flow:
	 * JUMP EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentReturnToShopSupressPaymentHistoryLinkForJUMPEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US357161 EIP Payment Return to Shop > Supress payment history link");
		Reporter.log("Data Condition | JUMP2.0 customer who has not paid 50% of eip amount");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on any Featured Phone | Product details page should be displayed");
		Reporter.log("6. Click on Add to cart | line selecto rpage should be displayed");
		Reporter.log("7. Click on Pay Now link on the selected line | EIP payment forward page should be displayed");
		Reporter.log(
				"9. Enter the payment details for the 50% amount sent by shop | Should be able to add the payment details");
		Reporter.log("10.Click on Make payment | Should be able to see Payment Confirmation Page");
		Reporter.log("11. Verify 'Payment History' link | Link should not be displayed");

		EIPPaymentConfirmationCCPage confirmationCCPage = navigatefromShoptoEIPpaymentConfirmationPage(myTmoData);
		confirmationCCPage.verifyPaymentHistoryLinkIsSupressed();
	}

	/**
	 * US357161 EIP Payment Return to Shop > Supress payment history link Flow:
	 * Regular EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentDisplayPaymentHistoryLinkForRegularEIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US357161 EIP Payment Return to Shop > Supress payment history link");
		Reporter.log("Data Condition | EIP eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log(
				"5.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("6. Click on Installment Plan blade | EIP details page should be displayed");
		Reporter.log("7. Click on make payment button | EIP payment page should be displayed");
		Reporter.log("8. Enter payment details and submit | Payment Success page should be displayed");
		Reporter.log("9. Verify 'Payment History' link and click on it | Account History page should be displayed");

		EIPPaymentConfirmationCCPage paymentConfirmationCCPage = navigateToEIPPaymentConfirmationCCPage(myTmoData,
				"Card");
		paymentConfirmationCCPage.verifyPaymentHistoryLink();
		paymentConfirmationCCPage.clickPaymentHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
	}

	/**
	 * US357163 EIP Payment Return to Shop > Supress View EIP Details Link Flow:
	 * JUMP EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testSuppressViewEIPDetailsLinkForJUMPEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US357163 EIP Payment Return to Shop > Supress View EIP Details Link");
		Reporter.log("Data Condition | JUMP2.0 customer who has not paid 50% of eip amount");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select the Device of jump upgradable | Product details page should be displayed");
		Reporter.log("5. Click on any Featured Phone | Product details page should be displayed");
		Reporter.log("6. Click on Add to cart | line selecto rpage should be displayed");
		Reporter.log("7. Click on Pay Now link on the selected line | EIP payment forward page should be displayed");
		Reporter.log(
				"8. Enter the payment details for the 50% amount sent by shop | Should be able to add the payment details");
		Reporter.log("9.Click on Make payment | Should be able to see Payment Confirmation Page");
		Reporter.log("10. Verify 'View EIP details' link | Link should not be displayed");
		
		EIPPaymentConfirmationCCPage confirmationCCPage = navigatefromShoptoEIPpaymentConfirmationPage(myTmoData);
		confirmationCCPage.verifyViewEIPDetailsLinkIsSupressed();

	}

	/**
	 * US357163 EIP Payment Return to Shop > Supress View EIP Details Link
	 * US357066 EIP Details Entry Point > Incomplete Data From Shop
	 * 
	 * Flow: Regular EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testViewEIPDetailsLinkForRegularEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US357163 EIP Payment Return to Shop > Supress View EIP Details Link");
		Reporter.log("Data Condition | EIP eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log(
				"5.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("6. Click on Installment Plan blade | EIP details page should be displayed");
		Reporter.log("7. Click on make payment button | EIP payment page should be displayed");
		Reporter.log("7. Enter payment details and submit | Payment Success page should be displayed");
		Reporter.log("8. Verify 'View EIP details' link | Link should be displayed");

		EIPPaymentConfirmationCCPage paymentConfirmationCCPage = navigateToEIPPaymentConfirmationCCPage(myTmoData,
				"Card");
		paymentConfirmationCCPage.verifyViewEIPDetailsLink();
		paymentConfirmationCCPage.clickViewEIPDetailsLink();
		EIPDetailsPage eipDetailsPage = new EIPDetailsPage(getDriver());
		eipDetailsPage.verifyPageLoaded();
	}

	
	/**
	 * US357156 EIP Payment Success Page > Display/Hide Button Logic
	 * Validation Flow: JUMP EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentPageDisplayOrHideButtonForJUMPEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US357156 EIP Payment Success Page > Display/Hide Button Logic");
		Reporter.log("Data Condition | JUMP2.0 customer who has not paid 50% of eip amount");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select the Device of jump upgradable | Product details page should be displayed");
		Reporter.log("6. Click on Add to cart | line selecto rpage should be displayed");
		Reporter.log("7. Click on Pay now for EIP Payment | EIP payment page should be displayed");		Reporter.log("7. Click on Pay Now link on the selected line | EIP payment forward page should be displayed");
		Reporter.log(
				"8. Enter the payment details for the 50% amount sent by shop | Should be able to add the payment details");
		Reporter.log("9.Click on Make payment | Should be able to see Payment Confirmation Page");
		Reporter.log("10. Verify 'Continue with my upgrade' Button | 'Continue with my upgrade' should be displayed");

		EIPPaymentConfirmationCCPage confirmationCCPage = navigatefromShoptoEIPpaymentConfirmationPage(myTmoData);
		confirmationCCPage.verifyContinuewithmyupgradebutton();
		
	}

	/**
	 * US357156 EIP Payment Success Page > Display/Hide Button Logic
	 * Validation Flow: Regular EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentPageDisplayOrHideButtonForRegularEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US357156 EIP Payment Success Page > Display/Hide Button Logic");
		Reporter.log("Data Condition | EIP eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log(
				"5.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("6. Click on Installment Plan blade | EIP details page should be displayed");
		Reporter.log("7. Click on make payment button | EIP payment page should be displayed");
		Reporter.log("7. Enter payment details and submit | Payment Success page should be displayed");
		Reporter.log("8. Verify 'Continue with my upgrade' button | 'Continue with my upgrade' should not be displayed");

		EIPPaymentConfirmationCCPage paymentConfirmationCCPage = navigateToEIPPaymentConfirmationCCPage(myTmoData,
				"Card");
		paymentConfirmationCCPage.verifyContinuewithmyupgradebutton();
		
		//
	}
	
	
	/** inProgress
	 * US357157 EIP Payment Success Page > Create Redirect to Transition PageButton
	 * Validation Flow: JUMP EIP
	 * US370259  Shop Redirect Page: Create Shell Page
	 * 	US367924  Shop Redirect Page: Bind URL to Shop flow CTA
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentPageTransitionPageButtonForJUMPEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US357157 EIP Payment Success Page > Create Redirect to Transition PageButton");
		Reporter.log("Data Condition | JUMP2.0 customer who has not paid 50% of eip amount");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select the Device of jump upgradable | Product details page should be displayed");
		Reporter.log("5. Click on any Featured Phone | Product details page should be displayed");
		Reporter.log("6. Click on Add to cart | line selecto rpage should be displayed");
		Reporter.log("7. Click on Pay now for EIP Payment | EIP payment page should be displayed");		Reporter.log("7. Click on Pay Now link on the selected line | EIP payment forward page should be displayed");
		Reporter.log(
				"8. Enter the payment details for the 50% amount sent by shop | Should be able to add the payment details");
		Reporter.log("9.Click on Make payment | Should be able to see Payment Confirmation Page");
		Reporter.log("10. Verify 'Continue with my upgrade' Button | 'Continue with my upgrade' should be displayed");
		Reporter.log("11. Click on 'Continue With my upgrdae' Button | Transistion Page Should be displayed");
         
		
		EIPPaymentConfirmationCCPage confirmationCCPage = navigatefromShoptoEIPpaymentConfirmationPage(myTmoData);
		confirmationCCPage.clickContinueWithMyUpgradeButton();
		BackToShopTransitionPage transitionPage = new BackToShopTransitionPage(getDriver());
		transitionPage.verifyPageLoaded();
		transitionPage.clickContinueButton();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
	}

	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentdateconfirmation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Check Today's Date on payment confirmation page ");
		Reporter.log("Data Condition | EIP eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log(
				"5.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("6. Click on Installment Plan blade | EIP details page should be displayed");
		Reporter.log("7. Click on make payment button | EIP payment page should be displayed");
		Reporter.log("8. Enter payment details and submit | Payment Success page should be displayed");
		Reporter.log("9. Check the paymentdate| Payment date should be today");
		
		EIPPaymentConfirmationCCPage paymentConfirmationCCPage = navigateToEIPPaymentConfirmationCCPage(myTmoData,"Card");
		/*launchAndPerformLogin(myTmoData);
		getDriver().get("https://deva0b.eservice.tmobile.com/billing/eipdetails.html");
		EIPDetailsPage eipDetailsPage=new EIPDetailsPage(getDriver());
		eipDetailsPage.clickMakePayment();
		EIPPaymentForwardPage eipPaymentForwardPage = new EIPPaymentForwardPage(getDriver());
		eipPaymentForwardPage.verifyPageLoaded();
		String amount=eipPaymentForwardPage.enterPaymentamount();
		if (!eipPaymentForwardPage.verifySavedCCisChecked()) {
			eipPaymentForwardPage.fillPaymentInfo(myTmoData.getPayment());
		}
		eipPaymentForwardPage.clickNextButton();
		EIPPaymentReviewCCPage reviewCCPage = new EIPPaymentReviewCCPage(getDriver());
		reviewCCPage.verifyPageLoaded();
		
		reviewCCPage.clickCCTermsAndConditions();
		reviewCCPage.clickCreditCardSubmitButton();
		EIPPaymentConfirmationCCPage eIPPaymentConfirmationCCPage = new EIPPaymentConfirmationCCPage(getDriver());
		eIPPaymentConfirmationCCPage.verifyPageLoaded();
		eIPPaymentConfirmationCCPage.verifyPaymentConfirmationDetails(amount,myTmoData.getPayment().getZipCode(),myTmoData.getPayment().getCardNumber());
		eIPPaymentConfirmationCCPage.Validatepaymentdate();
		eIPPaymentConfirmationCCPage.verifyPrintConfirmationLink();*/
	
	}
	
	
	
	/**
	 * US408140 Missing PII masking for few elements on EIP Confirmation Page

	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPIIMaskingonEIPConfirmationPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US408140 Missing PII masking for few elements on EIP Confirmation Page");
		Reporter.log("Data Condition | EIP eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log(
				"5.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("6. Click on Installment Plan blade | EIP details page should be displayed");
		Reporter.log("7. Click on make payment link | EIP payment page should be displayed");
		Reporter.log("8. Enter payment details and click on next | EIP Review page should be displayed");
		Reporter.log("9. accept terms and conditions and click on submit| Eip confirmation page should be displayed");
		Reporter.log("10. verify Customer Name is masked| Customer Name should be masked");
		Reporter.log("11. verify Payment card information is masked| Payment card information should be masked");
		Reporter.log("12. verify BAN/account number is masked| BAN/account number should be masked");
		Reporter.log("13. verify zipcode is masked| zipcode should be masked");
		Reporter.log("14. verify Mobile number is masked| Mobile number should be masked");
		Reporter.log("15. verify IMEI is masked| IMEI should be masked");
		Reporter.log("16. verify Instances of the same type of PII  numbered| Instances of the same type of PII should be numbered");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		
	}
	
	
	/**
	 * US445739 After navigating back button  from back to shop page payment history/eip deatil/billing links atr still displaying
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testVerifySupressLinksonEipConfirmationPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US445739 After navigating back button  from back to shop page payment history/eip deatil/billing links atr still displaying");
		Reporter.log("Data Condition | any PAH/Standard customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on any Featured Phone | Product details page should be displayed");
		Reporter.log("6. Click on Add to cart | line selector page should be displayed");
		Reporter.log("7. Click on Pay now for EIP Payment | EIP payment page should be displayed");
		Reporter.log("8. Enter payment details and submit | Payment Success page should be displayed");
		Reporter.log("9. Verify 'Return to Billing Home','Payment history','View EIP Details' links | Links should  be suppressed");

		
	}
	 
	
	/**
	 * US498491 ng6 EIP/Shop Paydown: Return to Shop Button
	 * Verify the return to shop button 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"Group.SPRINT"})
	public void testEIPShopPayOffShopButton(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPShopPayOffShopButton method called in BillingSummaryTest");
		Reporter.log("Shop - select required device to upgrade");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop | shop page should be displayed");
		Reporter.log("5. Click on device |product details page should load");
		Reporter.log("6. click on add to cart  | line selector page  should be displayed");
		Reporter.log("7. Click on Make a paymnet| equipment-landing page should be displayed");
		Reporter.log("8.  Add paymnet method | Paymnet method should be displayed");
		Reporter.log("9.  click on Agree and submit | Payment should be sucessful and EIP sucess page should load");
		Reporter.log("10.  Verify shop CTA| shop button should be displayed");
		Reporter.log("================================");
			
		EIPPaymentPage eipPaymentAmountPage = navigatefromShoptoEIPPaymentAmountPage(myTmoData);
		eipPaymentAmountPage.clickPaymentMethodBlade();
		SpokePage sPaymentsCollectionPage = new SpokePage(getDriver());
		sPaymentsCollectionPage.verifyNewPageLoaded();
		sPaymentsCollectionPage.verifyNewPageUrl();
		sPaymentsCollectionPage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.fillCardDetails(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		eipPaymentAmountPage.clickAgreeandSubmitBtn();
		EipJodPaymentConfirmationPage spaymentConfirmationPage = new EipJodPaymentConfirmationPage(getDriver());
		spaymentConfirmationPage.verifyPageLoaded();
		spaymentConfirmationPage.verifyPageUrl();
		spaymentConfirmationPage.clickBacktoShopButton();
		BackToShopTransitionPage sBacktoShopPage = new BackToShopTransitionPage(getDriver());
		sBacktoShopPage.verifyPageLoaded();
		sBacktoShopPage.verifyPageUrl();
		sBacktoShopPage.VerifyContinueButton();

	}

}
