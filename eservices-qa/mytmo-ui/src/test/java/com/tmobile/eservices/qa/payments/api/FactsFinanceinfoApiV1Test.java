package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.FactsFinanceinfoApiV1;

import io.restassured.response.Response;


public class FactsFinanceinfoApiV1Test extends FactsFinanceinfoApiV1 {
	
public Map<String, String> tokenMap;
	
	/**
	 * UserStory# Description: Facts-Finance-Info
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true,  groups = { "FactsFinanceinfoApiV1", "testFactsFinanceInfo", Group.PAYMENTS,Group.APIREG })
	public void testFactsFinanceInfo(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Facts-Finance-Info");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the Facts-Finance-Info of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "Facts-Finance-Info";
		tokenMap = new HashMap<String, String>();
		Response response = FactsFinanceInfo(apiTestData);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertNotNull(getPathVal(jsonNode,"facts.financeInformation.meta.sensitivity"),"sensitivity not loaded");
			Assert.assertNotNull(getPathVal(jsonNode,"facts.financeInformation.subfacts.msisdn"),"msisdn not loaded");
			Assert.assertNotNull(getPathVal(jsonNode,"facts.financeInformation.subfacts.currentStatus"),"currentStatus not loaded");
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
	}

}
