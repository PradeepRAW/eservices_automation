package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AALEligibilityCheckApiV1;
import com.tmobile.eservices.qa.api.eos.CartApiV4;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.shop.ShopConstants;

import io.restassured.response.Response;

public class AALEligibilityCheckApiV1Test extends AALEligibilityCheckApiV1 {
	public Map<String, String> tokenMap;
	JsonPath jsonPath;

	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AccessoriesAPITest", "testAccessoriesList",
			Group.SHOP, Group.APIREG })
	public void testEligibilityCheckAAL(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get Accessaries List");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the accessories|Accessaries list should get from service response");
		Reporter.log("Step 2: Verify expected accessories|Accessaries should be in the response list");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 5: Verify accessories list is not null|Accessaries list should display some number.");
		Reporter.log("Step 6: Verify accessories per page |Accessaries should be 12 per page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_ACCESSORIES + " getAccessoryList.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = eligibilityCheckAAL(apiTestData, updatedRequest, tokenMap);
		String operationName = "getAccessoryList";
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {

			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US545246# Description:Check for Max Out lines
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AccessoriesAPITest", "testAccessoriesList",
			Group.PROGRESSION })
	public void testEligibilityCheckAAL_MaxOutUS545246(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Check error code with Max Out(Zero Lines) Lines for MBB lines ");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Verify All Eligibilty response code|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify All Eligibilty error code|Error code should be 10009");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_AALELIGIBILITYCHECK + "testAALEligibility.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("version", "v1");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = eligibilityCheckAAL(apiTestData, updatedRequest, tokenMap);

		String operationName = "To Check ALL Eligibility for Max Out";
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();

			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				String s1 = jsonPath.get("errorCode");
				Assert.assertEquals(Integer.parseInt(s1), 10009);
				String expectedName = (jsonPath.get("errors[0].details"));
				Assert.assertEquals(expectedName, "Zero Lines: Total no. of eligible Voice & MBB Line is Zero.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US545246# Description:Check for Succes Error code
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALEligibilityCheckApiV1Test",
			"testEligibilityCheckAAL", Group.SPRINT, Group.PENDING })
	public void testEligibilityCheckAAL_Success_US545246(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: Check success error code for MBB lines ");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Verify All Eligibilty response code|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify All Eligibilty error code|Error code should be 100");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_AALELIGIBILITYCHECK + "testAALEligibility.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("version", "v1");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = eligibilityCheckAAL(apiTestData, updatedRequest, tokenMap);

		String operationName = "To Check ALL Eligibility for Success";
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();

			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("errorCode"), "error code is present");

				String s1 = jsonPath.get("errorCode");
				Assert.assertEquals(Integer.parseInt(s1), 100);

			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US460608# Description:AAL eligibility check - Pending Rate Plan
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALEligibilityCheckApiV1Test",
			"testEligibilityCheckAAL", Group.PENDING })
	public void testEligibilityCheckAAL_Success_US460608(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: Check success error code for MBB lines ");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Verify ALLEligibilty response code|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify ALLEligibilty error code|Error code should be 10013");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_AALELIGIBILITYCHECK + "testAALEligibility.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("version", "v1");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = eligibilityCheckAAL(apiTestData, updatedRequest, tokenMap);

		String operationName = "To Check ALL Eligibility for Success";
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();

			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("errorCode"), "error code is present");

				String s1 = jsonPath.get("errorCode");
				Assert.assertEquals(Integer.parseInt(s1), 100);

			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALEligibilityCheckApiV1Test",
			"testEligibilityCheckAAL", Group.PENDING_REGRESSION })
	public void testEligibilityCheckAAL_PRCustomer_US477500(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testEligibilityCheckAAL_PRCustomer_US477500");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Verify ALLEligibilty response code for PR Customer|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify ALLEligibilty error code|Error code should be 10001");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		aalEligibleCommonForUS477500("10001", apiTestData);

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALEligibilityCheckApiV1Test",
			"testEligibilityCheckAAL", Group.PENDING_REGRESSION })
	public void testEligibilityCheckAAL_ZeroLines_US477500(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testEligibilityCheckAAL_ZeroLines_US477500");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Verify ALLEligibilty response code for Zero Lines|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify ALLEligibilty error code|Error code should be 10009");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		aalEligibleCommonForUS477500("10009", apiTestData);

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALEligibilityCheckApiV1Test",
			"testEligibilityCheckAAL", Group.PENDING_REGRESSION })
	public void testEligibilityCheckAAL_notEligibleUserRoles_US477500(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testEligibilityCheckAAL_notEligibleUserRoles_US477500");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Step 1: Verify ALLEligibilty response code for User Role Not Part of Eligible User Roles|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify ALLEligibilty error code|Error code should be 10002");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		aalEligibleCommonForUS477500("10002", apiTestData);

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALEligibilityCheckApiV1Test",
			"testEligibilityCheckAAL", Group.PENDING_REGRESSION })
	public void testEligibilityCheckAAL_suspendedAccount_US477500(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testEligibilityCheckAAL_suspendedAccount_US477500");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Step 1: Verify ALLEligibilty response code for Suspended Account|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify ALLEligibilty error code|Error code should be 10006");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		aalEligibleCommonForUS477500("10006", apiTestData);

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALEligibilityCheckApiV1Test",
			"testEligibilityCheckAAL", Group.PENDING_REGRESSION })
	public void testEligibilityCheckAAL_pastDue_US477500(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testEligibilityCheckAAL_pastDue_US477500");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Step 1: Verify ALLEligibilty response code for Past Due : Past Due Amount Exists|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify ALLEligibilty error code|Error code should be 10003");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		aalEligibleCommonForUS477500("10003", apiTestData);

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALEligibilityCheckApiV1Test",
			"testEligibilityCheckAAL", Group.PENDING_REGRESSION })
	public void testEligibilityCheckAAL_nonMatchingRatePlanSOC_US477500(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testEligibilityCheckAAL_nonMatchingRatePlanSOC_US477500");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Step 1: Verify ALLEligibilty response code for Non Matching rate Plan SOC : Rate Plan SOC of the customer account is not matching with the one configured Rate Plan SOCs|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify ALLEligibilty error code|Error code should be 10010");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		aalEligibleCommonForUS477500("10010", apiTestData);

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALEligibilityCheckApiV1Test",
			"testEligibilityCheckAAL", Group.PENDING_REGRESSION })
	public void testEligibilityCheckAAL_RatePlanMaxLimitExceeded_US477500(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testEligibilityCheckAAL_RatePlanMaxLimitExceeded_US477500");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Step 1: Verify ALLEligibilty response code for Rate Plan : Max Limit Exceeded|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify ALLEligibilty error code|Error code should be 10011");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		aalEligibleCommonForUS477500("10011", apiTestData);

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALEligibilityCheckApiV1Test",
			"testEligibilityCheckAAL", Group.PENDING_REGRESSION })
	public void testEligibilityCheckAAL_voiceLinesMaxedOut_US477500(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testEligibilityCheckAAL_voiceLinesMaxedOut_US477500");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Step 1: Verify ALLEligibilty response code for Voice Lines Maxed Out : No. Of Active Phone lines equals Eligible Voince Lines|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify ALLEligibilty error code|Error code should be 10012");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		aalEligibleCommonForUS477500("10012", apiTestData);

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALEligibilityCheckApiV1Test",
			"testEligibilityCheckAAL", Group.PENDING })
	public void testEligibilityCheckAAL_serviceFailure_US477500(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testEligibilityCheckAAL_serviceFailure_US477500");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Verify ALLEligibilty response code for Service Failure|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify ALLEligibilty error code|Error code should be 10000");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		aalEligibleCommonForUS477500("10000", apiTestData);

	}

	public void aalEligibleCommonForUS477500(String expectedErrorCode, ApiTestData apiTestData) throws Exception {

		tokenMap = new HashMap<String, String>();
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_AALELIGIBILITYCHECK + "testAALEligibility.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("version", "v1");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = eligibilityCheckAAL(apiTestData, updatedRequest, tokenMap);

		String operationName = "To Check ALL Eligibility for error code : " + expectedErrorCode;
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();

			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("errorCode"), "error code is present");

				String s1 = jsonPath.get("errorCode");
				Assert.assertEquals(Integer.parseInt(s1), Integer.parseInt(expectedErrorCode));

			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALEligibilityCheckApiV1Test",
			"testEligibilityCheckAAL", Group.PENDING })
	public void testGetEligibleLines_US622830(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("1. New endpoint in EOS aaleligibility api");
		Reporter.log("2. BAN in header");
		Reporter.log("3. productType and productSubType in query param in request");
		Reporter.log(
				"4. Response to have all Lines for that BAN which are eligible for Wearables (productSubType == WEARABLES)");
		Reporter.log("5. Response to have to have below elements,");
		Reporter.log("EligibleLinesResp.productTypeInfo.productType");
		Reporter.log("EligibleLinesResp.productTypeInfo.productSubTypesInfo.productSubType");
		Reporter.log("EligibleLinesResp.productTypeInfo.productSubTypesInfo.eligibleLines[x].msisdn");
		Reporter.log("EligibleLinesResp.productTypeInfo.productSubTypesInfo.eligibleLines[x].lineType");
		Reporter.log("EligibleLinesResp.productTypeInfo.productSubTypesInfo.eligibleLines[x].pairingCount");
		Reporter.log("EligibleLinesResp.productTypeInfo.productSubTypesInfo.GSMPairingCountLimit");
		Reporter.log("EligibleLinesResp.productTypeInfo.productSubTypesInfo.eligibleLines[x].name");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testEligibilitServicesForWearable_US622830(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testEligibilitServicesForWearable_US622830");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Verify ALLEligibilty response code for Wearables|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify ProductSubType in response |ProductSubType should be WEARABLES");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("addaline", "ADDALINE");

		Response response = getEligibleLinesForWearables(apiTestData, tokenMap);

		String operationName = "To Check ALL Eligibility for Success response";

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();

			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				System.out.println(response.asString());
				JsonPath jsonPath = new JsonPath(response.asString());
				Assert.assertEquals(jsonPath.getString("eligibilityInfo[0].productSubtypeInfo[0].productSubType"),
						"WEARABLES");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US597278#Enhance GetEligiblePlans response [EOS techincal story]
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "1234545645" })
	public void testGetEligiblePlans_US622830(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testEligibilitServicesForWearable_US622830");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Step 1: Verify Get Eligible Plans with Plan type WEARABLE and Default Plan is Flase|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify lineType node in response |lineType should be Duplicate");
		Reporter.log("Step 3: Verify ratePlanSoc node in response |ratePlanSoc should be Present");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		
		tokenMap = new HashMap<String, String>();
		
		String operationName = "createCart";
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "createCartForPaymentEIP.txt");
		CartApiV4 cartApiV4 = new CartApiV4();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		
		logRequest(updatedRequest, operationName);
		Response response = cartApiV4.createCartV5(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		
		AALEligibilityCheckApiV1 aALEligibilityCheckApiV1 = new AALEligibilityCheckApiV1();
		Response response1 = aALEligibilityCheckApiV1.getEligiblePlansForWearables(apiTestData, tokenMap);
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status: "+response1.statusCode());
		
		String operationName1 = "To Check Line Type and SOC value from eligible plan response";
		if (response1 != null && "200".equals(Integer.toString(response1.getStatusCode()))) {
			logSuccessResponse(response1, operationName1);
			ObjectMapper mapper = new ObjectMapper();

			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				System.out.println(response1.asString());
				JsonPath jsonPath = new JsonPath(response1.asString());
				Assert.assertTrue(jsonPath.getString("planInfo[0].ratePlanSoc").contains("DIGITSAW"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		
	}
}