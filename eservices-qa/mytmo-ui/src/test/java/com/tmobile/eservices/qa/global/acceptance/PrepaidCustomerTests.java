package com.tmobile.eservices.qa.global.acceptance;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.global.GlobalConstants;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.RefillPage;

/**
 * @author rnallamilli
 *
 */
public class PrepaidCustomerTests extends GlobalCommonLib {

	/**
	 * US558159:.NET Migration | Prepaid | MBB Splash Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PENDING})
	public void verifyMBBSplashPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US558159:.NET Migration | Prepaid | MBB Splash Page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify MBB Splash page | MBB Splash page should be displayed");
		Reporter.log("5. Verify star image for mbb customer | Star image should be displayed");
		Reporter.log("6. Verify account management header | Account management header should be displayed");
		Reporter.log("7. Verify mobile internet management link | Mobile internet management page link should be displayed");
		Reporter.log("8. Click on mobile internet management link | Mobile internet management site should be displayed");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyCurrentPageURL("MBB Splash Page", "prepaidmbb");
		homePage.verifyStarImage();
		homePage.verifyAccountManagementHeader();
		homePage.verifyMobileInternetManagementLink();
		homePage.clickMobileInternetManagementLink();
		homePage.verifyCurrentPageURL("Mobile Internet Management site", "ericsson");
	}
	
	/**
	 * US562010:MyTMO Home | Prepaid | UNAV | Angular
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PENDING})
	public void verifyUNAVMenuForPrepaidAccount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US562010:MyTMO Home | Prepaid | UNAV | Angular");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify refill unav menu | Refill unav menu should be displayed");
		Reporter.log("5. Verify phone unav menu | Phone unav menu should be displayed");
		Reporter.log("6. Verify account unav menu | Account unav menu should be displayed");
		Reporter.log("7. Verify shop unav menu | Shop unav menu should be displayed");
		Reporter.log("8. Click refill unav menu | Refill page should be displayed");
		Reporter.log("9. Navigate back in browser | Home page should be displayed");
		Reporter.log("10.Click account unav menu | Account page should be displayed");
		Reporter.log("11.Navigate back in browser | Home page should be displayed");
		Reporter.log("12.Click phone unav menu | Phone device page should be displayed");
		Reporter.log("13.Navigate back in browser | Home page should be displayed");
		Reporter.log("14.Click shop unav menu | Shop page should be displayed");
		Reporter.log("15.Navigate back in browser | Home page should be displayed");
		Reporter.log("16.Click profile menu | Profile landing page should be displayed");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyRefillUnavMenu();
		homePage.clickRefillUnavMenu();
		homePage.verifyCurrentPageURL("Refill page", "refilloverview");
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.verifyPhoneUnavMenu();
		homePage.clickPhoneUnavMenu();
		homePage.verifyCurrentPageURL("Phone page", "PhoneDevice");
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.verifyAccountUnavMenu();
		homePage.clickAccountUnavMenu();
		homePage.verifyCurrentPageURL("Account page", "Prepaid");
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.verifyShopUnavMenu();
		homePage.clickShoplink();
		homePage.verifyCurrentPageURL("Shop page", "shop");
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.clickProfileMenu();
		homePage.verifyCurrentPageURL("Profile landing page", "profilelanding");
	}
	
	/**
	 * US562011:MyTMO Home | Prepaid | Footer | Angular
	 * TC-685:Ensure prepaid user able to access home page, header, footer and redirection works as expected
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyFooterLinksForPrepaidAccount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US562011:MyTMO Home | Prepaid | Footer | Angular");
		Reporter.log("TC-685:Ensure prepaid user able to access home page, header, footer and redirection works as expected");
		Reporter.log("Test Data Conditions: Any Prepaid Account");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify footer support link | Footer support link displayed");
		Reporter.log("5. Verify footer contact us link | Footer contact us link displayed");
		Reporter.log("6. Verify footer store locator link | Footer store locator link displayed");
		Reporter.log("7. Verify footer coverage link | Footer coverage link displayed");
		Reporter.log("8. Verify footer tmobile link | Footer tmobile link displayed");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyFooterSupportLink();
		homePage.verifyFooterContactUsLink();
		homePage.verifyFooterStoreLocatorLink();
		homePage.verifyFooterCoverageLink();
		homePage.verifyFooterTMobileLink();
	}
	
	/**
	 * US574991:MyTMO Home | Quick Links | Angular | Part 2
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID})
	public void verifyPrePaidCustomerQuickLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US574991:MyTMO Home | Quick Links | Angular | Part 2");
		Reporter.log("Test Data Conditions: Any PrePaid Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify auto pay setup quicklink | Auto pay setup quicklink should be displayed");
		Reporter.log("5. Click auto pay setup quicklink | Setup autopay page should be displayed");
		Reporter.log("6. Switch to home window | Home page should  be displayed");
		Reporter.log("7. Verify report a lost or stolen device quicklink | Report a lost or stolen device quicklink should be displayed");
		Reporter.log("8. Click report a lost or stolen device quicklink | My phone page should be displayed");
		Reporter.log("9. Switch to home window | Home page should  be displayed");
						
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyAutoPaySetUpQuickLink();
		homePage.clickAutoPaySetUpQuickLink();
		homePage.switchToSecondWindow();
		homePage.verifyCurrentPageURL("Setup AutoPay page", "payment");
		homePage.switchToWindow();
		homePage.verifyHomePage();
		homePage.verifyReportALostOrStolenDeviceQuickLink();
		homePage.clickReportALostOrStolenDeviceQuickLink();
		homePage.switchToSecondWindow();
		homePage.verifyCurrentPageTitle("Report a lost or stolen device page", "Lost or stolen");
		homePage.switchToWindow();
		homePage.verifyHomePage();
	}
	
	
	/**
	 * US564251:MyTMO Home | Prepaid | My Line Component | Angular cont'd
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PENDING})
	public void verifyHomePageMyLineComponentForPrePaidAccount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US564251:MyTMO Home | Prepaid | My Line Component | Angular cont'd");
		Reporter.log("Test Data Conditions: Any PrePaid account");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify my line section | My line section should be displayed");
		Reporter.log("5. Verify linked phone number in my line section | Linked phone number in my line section should match with logged missdn");
		Reporter.log("6. Verify plan blade div in my  line section | Plan blade div in my  line section should be displayed");
		Reporter.log("7. Verify device blade div in my line section | Device blade div in my line section should be displayed");
		Reporter.log("8. Click on device blade div in my line section | Device page should be displayed");
		Reporter.log("9. Navigate back in browser | Home page should be displayed");
		Reporter.log("10.Click on plan blade div in my line section | Plan page should be displayed");
		Reporter.log("11.Navigate back in browser | Home page should be displayed");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyMyLineSection();
		homePage.verifyLoggedMissdnNumber(myTmoData.getLoginEmailOrPhone());
		homePage.verifyDeviceImageInMyLineSection();
		homePage.verifyDeviceBladeDivInMyLineSection();
		homePage.verifyPlanBladeDivInMyLineSection();
		homePage.clickDeviceBladeDivInMyLineSection();
		homePage.verifyCurrentPageURL("Phone device page", "PhoneDevice");
		homePage.navigateBack();
		homePage.verifyHomePage();
		homePage.clickPlanBladeDivInMyLineSection();
		homePage.verifyCurrentPageURL("Plan page", "Prepaid");
		homePage.navigateBack();
		homePage.verifyHomePage();
	}
	
	
	/**
	 * US569360:MyTMO Home | Prepaid | Global Elements
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyHomePageGlobalElementsForPrePaidAccount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US569360:MyTMO Home | Prepaid | Global Elements");
		Reporter.log("Test Data Conditions: Any PrePaid account");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify welcome text | Welcome text should be displayed");
		Reporter.log("5. Verify linked phone number in my line section | Linked phone number in my line section should match with logged missdn");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyWelcomeText();
		homePage.verifyLoggedMissdnNumber(myTmoData.getLoginEmailOrPhone());
	}
	
	
	/**
	 * US564251:MyTMO Home | Prepaid | My Line Component | Angular cont'd
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyHomePageBillingComponentForPrePaidAccount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US564251:MyTMO Home | Prepaid | My Line Component | Angular cont'd");
		Reporter.log("Test Data Conditions: Any PrePaid account");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify account balance div | Account balance div should be displayed");
		Reporter.log("5. Verify refill account cta | Refill account cta should be displayed");
		Reporter.log("6. Verify account status div | Account status div should be displayed");
		Reporter.log("7. Verify next payment amount div | Next payment amount div should be displayed");
		Reporter.log("8. Verify next charge date div | Next charge div date should be displayed");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyAccountBalanceDiv();
		homePage.verifyRefillAccountCTA();
		homePage.verifyAccountStatusDiv();
		homePage.verifyNextPaymentAmountDiv();
		homePage.verifyNextChargeDateDiv();
	}
	
	/**
	 * US569360:MyTMO Home | Prepaid | Global Elements
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PENDING})
	public void testSearchFunctionalityForPrepaid(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US569360:MyTMO Home | Prepaid | Global Elements");
		Reporter.log("Test Data Conditions: Any PrePaid account");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Login to the application | User Should be login successfully");
		Reporter.log("3.Verify Home page | Home page should be displayed");
		Reporter.log("4.Enter text in search field and click on search button | Click on search button should be success");
		Reporter.log("5.Verify search results | Search functionality should fetch results as per search field");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage=navigateToNewHomePage(myTmoData);
		homePage.enterDataInSearchField(GlobalConstants.SEARCH_TEXT_DATA);
		homePage.verifySearchResultsForTMOHomePage(GlobalConstants.SEARCH_TEXT_DATA);
	}
	
	/**
	 * US584906:MyTMO Home | Prepaid | Quick Link Toggles
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyHomePageDataMaximiserComponentForPrePaidAccount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US584906:MyTMO Home | Prepaid | Quick Link Toggles");
		Reporter.log("Test Data Conditions: Any PrePaid account");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify data maximiser div | Data maximiser div should be displayed");
		Reporter.log("5. Verify data maximiser toggle off status state | Data maximiser toggle should be in off state");
		Reporter.log("6. Verify data maximiser toggle on status state | Data maximiser toggle should be in on state");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyDataMaximiserDiv();
		homePage.verifyDataMaximiserToggleOffState();
		homePage.verifyDataMaximiserToggleOnState();
	}

	
	
	/**
	 * US562017:MyTMO Home | Prepaid | Re-direct traffic from .NET to Cloud
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.FLEX_SPRINT})
	public void verifyRedirectToCloudFromLegacyPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US562017:MyTMO Home | Prepaid | Re-direct traffic from .NET to Cloud");
		Reporter.log("Test Data Conditions: Any Prepaid Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify refill unav menu | Refill unav menu should be displayed");
		Reporter.log("5. Verify phone unav menu | Phone unav menu should be displayed");
		Reporter.log("6. Verify account unav menu | Account unav menu should be displayed");
		Reporter.log("7. Verify shop unav menu | Shop unav menu should be displayed");
		Reporter.log("8. Click refill unav menu | Refill page should be displayed");
		Reporter.log("9. Click mytmobile logo in legacy page | Home page should be displayed");
		Reporter.log("10.Click account unav menu | Account page should be displayed");
		Reporter.log("11.Click mytmobile logo in legacy page | Home page should be displayed");
		Reporter.log("12.Click phone unav menu | Phone device page should be displayed");
		Reporter.log("13.Click mytmobile logo in legacy page | Home page should be displayed");
		Reporter.log("14.Click shop unav menu | Shop page should be displayed");
		Reporter.log("15.Click mytmobile logo in legacy page | Home page should be displayed");
		Reporter.log("16.Click profile menu | Profile landing page should be displayed");
		Reporter.log("17.Click mytmobile logo in legacy page | Home page should be displayed");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyRefillUnavMenu();
		homePage.verifyPhoneUnavMenu();
		homePage.verifyAccountUnavMenu();
		homePage.verifyShopUnavMenu();
		homePage.clickRefillUnavMenu();
		homePage.verifyCurrentPageURL("Refill page", "refilloverview");
		homePage.clickLeagacyPageMyTMobileLogo();
		homePage.verifyHomePage();
		homePage.clickAccountUnavMenu();
		homePage.verifyCurrentPageURL("Account page", "Prepaid");
		homePage.clickLeagacyPageMyTMobileLogo();
		homePage.verifyHomePage();
		homePage.clickPhoneUnavMenu();
		homePage.verifyCurrentPageURL("Phone page", "PhoneDevice");
		homePage.clickLeagacyPageMyTMobileLogo();
		homePage.verifyHomePage();
		homePage.clickShoplink();
		homePage.verifyCurrentPageURL("Shop page", "shop");
		homePage.clickLeagacyPageMyTMobileLogo();
		homePage.verifyHomePage();
		homePage.clickProfileMenu();
		homePage.verifyCurrentPageURL("Profile landing page", "profilelanding");
		homePage.clickLeagacyPageMyTMobileLogo();
		homePage.verifyHomePage();
	}
	
	/**
	 * CDCDWR-329:.NET Migration | Prepaid | CPS Landing Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.FLEX_SPRINT})
	public void verifyVIPCustomersSplashPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-329:.NET Migration | Prepaid | CPS Landing Page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify plan blade div in my line section | Plan blade div in my line section should be displayed");
		Reporter.log("5. Click plan blade div | VIPCustomers splash page should be displayed");
		Reporter.log("6. Verify star image | Star image should be displayed");
		Reporter.log("7. Verify prepaid welcome text | Prepaid welcome text should be displayed");
		Reporter.log("8. Verify contact customer care text | Contact customer care text should be displayed");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyPlanBladeDivInMyLineSection();
		homePage.clickPlanBladeDivInMyLineSection();
		homePage.verifyCurrentPageURL("VIPCustomers splash page", "prepaidinfo/home");
		homePage.verifyStarImage();
		homePage.verifyWelcomePrePaidCustomersText();
		homePage.verifyContactCustomerCareText();
	}
	
	/**
	 * CDCDWR-830:MyTMO - Develop Prepaid UI pages - cont 2 Release Testing
	 * TC-1537:Ensure prepaid user click on refill should be able to acess the new refill page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyRefillMenu(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-830:MyTMO - Develop Prepaid UI pages - cont 2 Release Testing");
		Reporter.log("TC-1537:Ensure prepaid user click on refill should be able to acess the new refill page");
		Reporter.log("Test Data Conditions: Any PrePaid account");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify refill button in my account section | Refill button in my account section should be displayed");
		Reporter.log("5. Click refill button in my account section | Refill page should be displayed");
		Reporter.log("6. Navigate back in browser | Home page should be displayed");
		Reporter.log("7. Verify Home page | Home page should be displayed");
		Reporter.log("8. Verify refill unav menu | Refill unav menu should be displayed");
		Reporter.log("9. Click refill unav menu | Refill page should be displayed");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyRefillButtonInMyAccountSection();
        homePage.clickRefillButtonInMyLineSection();
        
        RefillPage refillPage=new RefillPage(getDriver());
		refillPage.verifyRefillPage();
		refillPage.navigateBack();
		
		homePage.verifyHomePage();
		homePage.verifyRefillUnavMenu();
		homePage.clickRefillUnavMenu();
		
		refillPage=new RefillPage(getDriver());
		refillPage.verifyRefillPage();
	}
	
	/**
	 * TC-686:Ensure prepaid user able to access splash page upon clicking Account
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyAccountSplashPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("TC-686:Ensure prepaid user able to access splash page upon clicking Account");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify account unav menu | Account unav menu should be displayed");
		Reporter.log("5. Click account unav menu | Account splash page should be displayed");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyAccountUnavMenu();
		homePage.clickAccountUnavMenu();
		homePage.verifyCurrentPageURL("Account Splash Page", "prepaid");
		homePage.verifyStarImage();
	}
	
}
