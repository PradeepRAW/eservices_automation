package com.tmobile.eservices.qa.tmng.functional;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.MagentaInsiderPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

/**
 * @author sputti
 *
 */
public class MagentaInsiderPageTest extends TmngCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(MagentaInsiderPageTest.class);	
	/***
	 * TMO - GLOBAL - MI Register End to End Testing 
	 * @param tMNGData
	 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
		public void verifyMIRegisterPage(TMNGData tMNGData) {
		logger.info("End to end Functional Testing of MI Register Page");
		Reporter.log("======================================================================");
		Reporter.log("Test Steps | Expected Test Results:");
		Reporter.log("1. Verify MI Register Page | MI Register Page should be loaded.");
		Reporter.log("2. Verify I agree uncheck on page load | I Agree check box is unchecked state.");
		Reporter.log("3. Verify Submit button disabled on page load | Submit button is in disabled state.");
		Reporter.log("4. Enter required fields in page | Fields should be entered.");
		Reporter.log("5. Verify Success Page load by submitting of MI Register Page | Success Page Should be loaded by clicking on Submit and OK Buttons.");
		Reporter.log("------------------------Actual Test Results:--------------------------");
		loadTmngURL(tMNGData);
		//getDriver().get("https://dmo.digital.t-mobile.com/content/t-mobile/consumer/brand/insider-register.html");
		getDriver().get("https://qat-tmo-publisher.corporate.t-mobile.com/content/t-mobile/consumer/brand/insider-register.html");
		MagentaInsiderPage megentainsiderPage = new MagentaInsiderPage(getDriver());
		megentainsiderPage.verifyMIRegisterPageLoaded();
		getDriver().manage().timeouts().setScriptTimeout(10000, TimeUnit.MINUTES);
		megentainsiderPage.setFirstName();
		megentainsiderPage.validateLastName1();
		megentainsiderPage.validateLastName2();
		megentainsiderPage.setLastName();
		megentainsiderPage.verifyAddress();
		megentainsiderPage.setCity();
		megentainsiderPage.setState();
		megentainsiderPage.setZip();
		megentainsiderPage.setPhoneNumber();
		megentainsiderPage.submitButtonDisable();
		megentainsiderPage.setEmail();
		megentainsiderPage.setSocialChannel();
		megentainsiderPage.setAgree();
		megentainsiderPage.clickOnSubmitBtn();
		megentainsiderPage.ClickOK();
		Reporter.log("======================================================================");
		getDriver().close();
	}
		/***
		 * FLEx - US497142 MI: State Field Validation & US499736 MI: Last Name must contain 2 characters
		 * @param tMNGData
		 */
			@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
			public void verifyLastNameandState(TMNGData tMNGData) {
			Reporter.log("US497142 MI: State Field Validation & US499736 MI: Last Name must contain 2 characters");
			logger.info("Verify State Field Validations in MI Register Page");
			Reporter.log("======================================================================");
			Reporter.log("Test Steps | Expected Test Results:");
			Reporter.log("1. Verify MI Register Page | MI Register Page should be loaded.");
			Reporter.log("2. Verify Field-level error message on tab | Field-level error message displays when user tabs out of field.");
			Reporter.log("3. Verify Field-level error message on click | Field-level error message displays when user clicks in and out of the field without making a State selection.");
			Reporter.log("4. Enter required fields in page | Fields should be entered.");
			Reporter.log("5. Verify Success Page load by submitting of MI Register Page | Success Page Should be loaded by clicking on Submit and OK Buttons.");
			Reporter.log("------------------------Actual Test Results:--------------------------");
			loadTmngURL(tMNGData);
			getDriver().get("https://dmo.digital.t-mobile.com/content/t-mobile/consumer/brand/insider-register.html");
			MagentaInsiderPage megentainsiderPage = new MagentaInsiderPage(getDriver());
			megentainsiderPage.verifyMIRegisterPageLoaded();
			getDriver().manage().timeouts().setScriptTimeout(10000, TimeUnit.MINUTES);
			megentainsiderPage.setFirstName();
			megentainsiderPage.validateLastName1();
			megentainsiderPage.validateLastName2();
			megentainsiderPage.setLastName();
			megentainsiderPage.verifyAddress();
			megentainsiderPage.setCity();
			megentainsiderPage.stateValidation();
			megentainsiderPage.setState();
			megentainsiderPage.setZip();
			megentainsiderPage.setPhoneNumber();
			megentainsiderPage.setEmail();
			megentainsiderPage.setSocialChannel();
			megentainsiderPage.clickOnSubmitBtn();
			megentainsiderPage.ClickOK();
			Reporter.log("======================================================================");
			getDriver().close();
		}
			/***
			 * FLEx - US498554 MI: Field Validations On Blur 
			 * @param tMNGData
			 */
				@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
				public void verifyFieldBlur(TMNGData tMNGData) {
				Reporter.log("US498554 MI: Fields Validations On Blur");
				logger.info("Verify Fields Validations on Blur in MI Register Page");
				Reporter.log("======================================================================");
				Reporter.log("Test Steps | Expected Test Results:");
				Reporter.log("1. Verify MI Register Page | MI Register Page should be loaded.");
				Reporter.log("2. Verify Field-level error message on blur | Field-level error message displays when user blur on field.");
				Reporter.log("3. Enter required fields in page | Fields should be entered.");
				Reporter.log("4. Verify Success Page load by submitting of MI Register Page | Success Page Should be loaded by clicking on Submit and OK Buttons.");
				Reporter.log("------------------------Actual Test Results:--------------------------");
				loadTmngURL(tMNGData);
				getDriver().get("http://qat-tmo-publisher.corporate.t-mobile.com/content/t-mobile/consumer/brand/insider-register.html");
				MagentaInsiderPage megentainsiderPage = new MagentaInsiderPage(getDriver());
				megentainsiderPage.verifyMIRegisterPageLoaded();
				getDriver().manage().timeouts().setScriptTimeout(10000, TimeUnit.MINUTES);
				megentainsiderPage.VerifyFieldBlur();
				megentainsiderPage.setFields();	
				Reporter.log("======================================================================");
				getDriver().close();
			}
				/***
				 * FLEx - US486736 MI: Social Handle field
				 * @param tMNGData
				 */
					@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
					public void verifySocialHandle(TMNGData tMNGData) {
					Reporter.log("US486736 MI: Social Handle field");
					logger.info("Verify Social Handle Field Validations in MI Register Page");
					Reporter.log("======================================================================");
					Reporter.log("Test Steps | Expected Test Results:");
					Reporter.log("1. Verify MI Register Page | MI Register Page should be loaded.");
					Reporter.log("2. Verify Social Handle Field visibility | Social Handle Field should be displays when user select Social Channel value.");
					Reporter.log("3. Verify Social Handle Field value | MI Register Form should be submit with Socual Handle field value.");
					Reporter.log("------------------------Actual Test Results:--------------------------");
					loadTmngURL(tMNGData);
					getDriver().get("https://dmo.digital.t-mobile.com/content/t-mobile/consumer/brand/insider-register.html");
					MagentaInsiderPage megentainsiderPage = new MagentaInsiderPage(getDriver());
					megentainsiderPage.verifyMIRegisterPageLoaded();
					getDriver().manage().timeouts().setScriptTimeout(10000, TimeUnit.MINUTES);
					megentainsiderPage.setFirstName();
					megentainsiderPage.setLastName();
					megentainsiderPage.verifyAddress();
					megentainsiderPage.setCity();
					megentainsiderPage.setState();
					megentainsiderPage.setZip();
					megentainsiderPage.setPhoneNumber();
					megentainsiderPage.setEmail();
					megentainsiderPage.setSocialChannel();
					megentainsiderPage.clickOnSubmitBtn();
					megentainsiderPage.ClickOK();
					Reporter.log("======================================================================");
					getDriver().close();
				}
}