package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.ActivitiesApiV1;

import io.restassured.response.Response;

public class ActivitiesApiV1Test extends ActivitiesApiV1 {
	
	public Map<String, String> tokenMap;
	
	/**
	 * UserStory# Description: Get Account Summary
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ActivitiesApiV1", "testGetAccountSummary", Group.PAYMENTS,Group.PENDING })
	public void testGetAccountSummary(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetAccountSummary");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the AccountSummary of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "getAccountSummary";
		tokenMap = new HashMap<String, String>();
		Response response = getAccountSummary(apiTestData,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertEquals("100", getPathVal(jsonNode,"statusCode"));
			Assert.assertNotNull(getPathVal(jsonNode,"accountActivity.billStatementId"),"billStatementId not loaded");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory# Description: getPdfDocument
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true,  groups = { "ActivitiesApiV1", "testgetPdfDocument", Group.PAYMENTS,Group.APIREG })
	public void testGetPdfDocument(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetPdfDocument");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the getPdfDocument of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "getPdfDocument";
		tokenMap = new HashMap<String, String>();
		Response response = getPdfDocument(apiTestData,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertEquals("100", getPathVal(jsonNode,"statusCode"));
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory# Description: GetEIPHistory
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ActivitiesApiV1", "testGetEIPHistory", Group.PAYMENTS,Group.APIREG })
	public void testGetEIPHistory(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGetEIPHistory");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the testGetEIPHistory of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testGetEIPHistory";
		tokenMap = new HashMap<String, String>();
		Response response = getEipHistory(apiTestData,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertEquals("100", getPathVal(jsonNode,"statusCode"));
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	

}
