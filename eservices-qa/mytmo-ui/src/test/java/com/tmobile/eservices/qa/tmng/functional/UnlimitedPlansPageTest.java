package com.tmobile.eservices.qa.tmng.functional;

import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class UnlimitedPlansPageTest extends TmngCommonLib{
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TMNG})
	public void testUnlimitedPlansPage(TMNGData tMNGData){
		navigateToUnlimitedPlansPage(tMNGData);
		
	}

}
