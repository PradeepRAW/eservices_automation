package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.CartPage;

import com.tmobile.eservices.qa.pages.tmng.functional.SLPPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.SDPPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class SLPPageTest extends TmngCommonLib{
	
	/**
	 * US330127 TMO - IV - Store list Cart US462733 - TMO - IV Accessibility - SLP
	 * from Cart US471183 - TMO - IV Accessibility - SLP from Multi Cart
	 *  US330128 TMO - IV - Filter Cart
	 *  US352090 TMO - IV - StoreList Cart Map
	 *  
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testStoreListCart(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US330127 TMO - IV - Store list Cart");
		Reporter.log("Test Case : US462733 - TMO - IV Accessibility - SLP from Cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device |PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Click on 'Add a Phone' button | Device image should be displayed");
		Reporter.log(
				"6. Click on '+' button in device image and Add a new Phone button | Mini PLP page should be displayed");
		Reporter.log("7. Select any device | Mini PDP page should be displayed");
		Reporter.log("8. Click on Add to Cart | Cart page  should be displayed");
		Reporter.log("9. Click on  'change store locator' CTA |Store selector page  should be displayed");
		Reporter.log(
				"10. Verify Stores List modal (sorted by the relative distance)|Stores List modal  should be displayed");
		Reporter.log("11. Verify  Store Locator map| Store Locator map should be displayed next to Stores List");
		Reporter.log(
				"12. Verify  Default or selected store is highlighted | Default or selected store should be highlighted");

		Reporter.log(
				"13. Verify  Blades in the list are expanded and contains 3 CTAs, Appointment, Get in line, Call us, Save Cart  | Blades in the list should be expanded and contains 3 CTAs, Appointment, Get in line, Call us, Save Cart");
		Reporter.log("14. Verify Inventory status | Inventory status should be displayed");
		Reporter.log(
				"15. Verify 'No stores nearby with inventory. Please try adjusting your search area' message if no store within 50 miles have inventory | 'No stores nearby with inventory. Please try adjusting your search area' message should be displayed if no store within 50 miles have inventory");

		Reporter.log("================================");
		Reporter.log("Actual Output:");


		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.verifyInventoryStatusOnCartPage();
		cartPage.verifyStoreName();
		cartPage.verifyDistanceToStore();
		cartPage.verifyChangeStoreLocatorCTAOnCartPage();
		String storeName = cartPage.getStoreName();
		cartPage.clickChangeStoreLocation();
		SLPPage slpPage = new SLPPage(getDriver());
		slpPage.verifyGoBack();
		slpPage.clickZoomInButtonOnMap();
		slpPage.verifyCheckBoxDescriptionInStoreLocatorPageIsdisplayed();
		slpPage.verifyMessageUnderCheckBoxMessageDetailsInStoreLocatorPage();
		slpPage.verifyDefaultStateOfCheckBox();
		slpPage.verifyStoreLocatorListPage();
		slpPage.verifyStoreMapPage();
		slpPage.verifySelectedStore(storeName);
		slpPage.verifyAppointmentCTA();
		slpPage.verifyGetInLineCTA();
		slpPage.verifycallUsCta();
		slpPage.verifySaveCartCTAonSLP();
		slpPage.verifyShowHideCTADisplayedOnSLPCartPage();
		slpPage.clickShowHideItemsOnSLP();
		slpPage.verifyItemListonClickingShowItemsCTAOnStoreListPage();
		slpPage.verifyShowHideCTADisplayedOnSLPCartPage();
		slpPage.clickShowHideItemsOnSLP();
		slpPage.verifyStoreListOnSLP();
		slpPage.selectCheckBoxInStoreLocatorPage();
		slpPage.verifyStoreListOnSLP();
		slpPage.verifyInventoryStatusInStoreLocatorListPage();
		
		String newstoreName = slpPage.selectAnyStore();
		cartPage.verifyCartPageLoaded();
		cartPage.verifySelectedStoreLocatorName(newstoreName);
		cartPage.verifyInventoryStatusOnCartPage();

		cartPage.clickDuplicateCTA();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyAllProductsStoreName();
		cartPage.clickStoreLocatorName();
		SDPPage sdpPage = new SDPPage(getDriver());
		sdpPage.verifyStoredetailsPage();
		//slpPage.searchForALocationWithNoInventoryAndVerifyMessage(tMNGData.getZipcode());
		//slpPage.verifyDefaultMsgForNoInventory();

	}

	/**
	 * TA1655288: (Prod) :Desktop: Click Find nearby stores: store list page does
	 * not populate, a blank SLP is displayed.
	 *
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyStoreListPageOnSaveCartConfirmationPage(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : TA1655288: (Prod) :Desktop: Click Find nearby stores: store list page does not populate, a blank SLP is displayed.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | Cart page Should be Launched");
		Reporter.log("2. click on Add a phone tile |Cart  page should be displayed");
		Reporter.log("3. Click Device Plus button | Add a new Phone and BYOD should be displayed");
		Reporter.log(
				"4. Click on Add a new phone and click done on credit class pop up| Mini PLP page should be displayed");
		Reporter.log("5. Select any phone from Mini PLP| Mini PDP page should be displayed");
		Reporter.log("6. Click Add to cart on Mini PDP| Cart page should be displayed");
		Reporter.log("7. Click on Save Cart  | Save Cart pop up should display");
		Reporter.log("8. Verify Cart icon on top of the page| Cart icon should be displayed");
		Reporter.log("9. Verify Page title under icon | Page title shold be displayed");
		Reporter.log("10. Verify message under the title | message under the title should be displayed");
		Reporter.log(
				"11. Verify input email in a field under the message | input email in a field under the message should be displayed");
		Reporter.log("12. Click on save | Conformation Page should be displayed");
		Reporter.log("13. Verify Find stores CTA | Find stores CTA should be displayed");
		Reporter.log("14. Click Find stores CTA | Store List Page should be displayed");
		Reporter.log("15. Verify stores in SLP | Store List should be displayed on SLP");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnSaveCart();
		cartPage.verifyCartIconOnSaveCartModel();
		cartPage.verifySaveCartHeader();
		cartPage.verifySaveCartSubHeader();
		String email = cartPage.getEmailWithDate(tMNGData);
		cartPage.enterEmailWithDateInSaveCart(email);
		cartPage.clickSaveButtonOnSaveCartModel();
		cartPage.verifySaveCartReplaceModalAndClickSaveForWebSaveCart();
		cartPage.verifyConfirmationMessageonSaveCartModel();
		cartPage.verifyStoresInSaveCartModalPopup();
		cartPage.clickFindNearByStoresOnSaveCartModal();
		SLPPage slpPage = new SLPPage(getDriver());
		slpPage.verifyStoreLocatorListPage();
	}
	
	
	/**
	 * US481739	TMO - IV Accessories Accessibility - SLP from Cart
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP})
	public void testIVAccessoriesAccessibilitySLPFromCart(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US481739	TMO - IV Accessories Accessibility - SLP from Cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessories should be displayed");
		Reporter.log("4. Select any Accessory | Accessory PDP Page should be selected");
		Reporter.log("5. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("6. Verify 'Find nearby stores' CTA | 'Find nearby stores' CTA should be selected");
		Reporter.log("7. Click on  'Find nearby stores' CTA |Store selector page  should be displayed");
		Reporter.log("8. Verify Stores List modal (sorted by the relative distance)|Stores List modal  should be displayed");
		Reporter.log("9. Verify  Blades in the list are expanded and contains 4 CTAs, Appointment, Get in line, Call us and Save Cart| Blades in the list should be expanded and contains 4 CTAs, Appointment, Get in line, Call us and Save Cart");
		Reporter.log("10. Verify Inventory status on SLP |Product Inventory Hurry, Only a few left or  In Srock or  Contact store for availability should be displayed");
		Reporter.log("11. Verify 'Sorry, we can’t hold products or guarantee availability – even with an appointment' | 'Sorry, we can’t hold products or guarantee availability – even with an appointment' message should be displayed.");
		Reporter.log("12. Verify 'Only show stores with item in stock' check box | 'Only show stores with item in stock' check box should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		PdpPage accessoryDetailsPage = navigateToAccessoryMainPDP(tMNGData);
        accessoryDetailsPage.clickOnAddToCartBtn();
        accessoryDetailsPage.selectContinueAsGuest();
        CartPage cartPage = new CartPage(getDriver());
        cartPage.verifyCartPageLoaded();
        cartPage.verifyChangeStoreLocatorCTAOnCartPage();
        cartPage.clickChangeStoreLocationForAccessory();
        
        SLPPage slpPage = new SLPPage(getDriver());
        slpPage.verifyStoreLocatorListPage();
        slpPage.verifyStoreMapPage();
        slpPage.verifyAppointmentCTA();
        slpPage.verifycallUsCta();
        slpPage.verifyGetInLineCTA();
        slpPage.verifyInventoryStatusInStoreLocatorListPage();
		slpPage.verifyCheckBoxDescriptionInStoreLocatorPageIsdisplayed();
		slpPage.verifyMessageUnderCheckBoxMessageDetailsInStoreLocatorPage();
    
	}
	
	/**
	 * US481740	TMO - IV Accessories Accessibility - SLP from Multi Cart
	 * US481739	TMO - IV Accessories Accessibility - SLP from Cart
	 *  US431430: TMO - IV Fast Followers  - StoreList for Accessories
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP})
	public void testIVAccessoriesAccessibilitySLPFromMultiCart(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US481740	TMO - IV Accessories Accessibility - SLP from Multi Cart");
		Reporter.log("Test Case : US481739	TMO - IV Accessories Accessibility - SLP from Cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessories should be displayed");
		Reporter.log("4. Select any Accessory | Accessory PDP Page should be selected");
		Reporter.log("5. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("6. Click on 'ADD AN ACCESSORY' | Accessory Mini PLP Page should be displayed");
		Reporter.log("7. Select any Accessory from Mini PLP | Accessory MiniPDP Page should be displayed");
		Reporter.log("8. Click on Add to Cart button on Mini PDP | Cart page should be displayed");
		Reporter.log("9. Verify 'Find nearby stores' CTA | 'Find nearby stores' CTA should be selected");
		Reporter.log("10. Click on  'Find nearby stores' CTA |Store selector page  should be displayed");
		Reporter.log("11. Verify Stores List modal (sorted by the relative distance)|Stores List modal  should be displayed");
		Reporter.log("12. Verify  Blades in the list are expanded and contains 4 CTAs, Appointment, Get in line, Call us and Save cart| Blades in the list should be expanded and contains 3 CTAs, Appointment, Get in line, Call us and Save cart");
		Reporter.log("13. Verify Inventory status on SLP |Product Inventory Hurry, Only a few left or  In Srock or  Contact store for availability should be displayed");
		Reporter.log("14. Verify 'Sorry, we can’t hold products or guarantee availability – even with an appointment' | 'Sorry, we can’t hold products or guarantee availability – even with an appointment' message should be displayed.");
		Reporter.log("15. Verify 'Only show stores with item in stock' check box | 'Only show stores with item in stock' check box should be displayed");
		Reporter.log("16. Click Show items CTA  | List of items avalable on selected store and inventory staus should be displayed");
		Reporter.log("17. Verify Hide items CTA  | Hide item CTA should be displayed");
		Reporter.log("18. Click Hide items CTA  | List of items should be collapsed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
	
        CartPage cartPage = navigateToCartPageBySelectingAccessory(tMNGData);
        cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
        cartPage.verifyFindNearbyStoresForAccessory();
        cartPage.clickChangeStoreLocationForAccessory();
        SLPPage slpPage = new SLPPage(getDriver());
        slpPage.verifyStoreLocatorListPage();
        slpPage.verifyStoreMapPage();
        slpPage.verifyAppointmentCTA();
        slpPage.verifycallUsCta();
        slpPage.verifyGetInLineCTA();
        slpPage.verifyShowHideCTADisplayedOnSLPCartPage();
        slpPage.clickShowHideItemsOnSLP();
        slpPage.verifyItemListonClickingShowItemsCTAOnStoreListPage();
        slpPage.verifyShowHideCTADisplayedOnSLPCartPage();
        slpPage.clickShowHideItemsOnSLP();
        slpPage.verifyInventoryStatusInStoreLocatorListPage();
        slpPage.verifyCheckBoxDescriptionInStoreLocatorPageIsdisplayed();
        slpPage.verifyCheckBoxMessageInStoreLocatorPageisDisplayed();
        slpPage.verifyMessageUnderCheckBoxMessageDetailsInStoreLocatorPage();
	}

}
