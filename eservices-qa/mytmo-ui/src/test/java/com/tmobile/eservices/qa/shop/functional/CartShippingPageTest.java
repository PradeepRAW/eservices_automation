package com.tmobile.eservices.qa.shop.functional;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import com.tmobile.eservices.qa.shop.ShopConstants;

public class CartShippingPageTest extends ShopCommonLib {

	
	/**
	 * US151822 - Q2 - Cart: Editable shipping information page - As a mytmo user in
	 * cart page, I should be able to change the ship-to address from their billing
	 * address.
	 * US229251 :New Cart : Shipping - Edit Ship To Address
	 * US220526 :New Cart : Device Order Details - Due today cost
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testEditableShippingAddress(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify shipping address edit flow");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log(
				"1. Log in to the application with valid FA Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See all phones on shop page |PLP Page should be diplayed");
		Reporter.log("4. Click on the Device on PLP page | Device PDP Page should be displayed ");
		Reporter.log("5. Select Payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select Standard line on LS page |Phone Selection Page should be displayed");
		Reporter.log("7. Click on Skip TradeIn on Phone selection page | PHP Page should be displayed ");
		Reporter.log("8. Click on Continue on PHP page| Cart Page should be displayed");
		Reporter.log(
				"9. Verify that Order details and click on Continue to shipping button | Shipping information should be displayed");
		Reporter.log(
				"13. Click Ship to different address button and verify shipping labels | Shipping Address labels should be displayed");
		Reporter.log("14. Verify Update address button | Update address button should be displayed");
		Reporter.log("15. Verify Cancel button | Cancel button should be displayed");
		Reporter.log(
				"12. Fill shipping info & click continue to payment button | Payment information form should be displayed");
		Reporter.log("13. Verify edited shipping address | Edited shipping address should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByFeatureDevicesWithSKipTradeIn(myTmoData, myTmoData.getDeviceName());
		cartPage.clickContinueToShippingButton();
		cartPage.clickShipToDifferentAddress();
		cartPage.verifyShippingDetailsComponent();
		cartPage.verifyAddressLineLabel();
		//cartPage.verifyAddressLineText();
		cartPage.verifyCityLabel();
		//cartPage.verifyCityLabelText();
		cartPage.verifyStateLabel();
		//cartPage.verifyStateLabelText();
		cartPage.verifyZIPcodeLabel();
		//cartPage.verifyZIPcodeLabelText();
		cartPage.verifyCancelButton();
		cartPage.fillShippingAddressInfo();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.verifyEditedShippingAddress();
	}

	/**
	 * US229276 :New Cart : Edit Shipping Address - Field level validations
	 * 
	 * US231307: New Cart:Shipping - Title and Address - Desktop
	 * 
	 * US214807: New Cart:Shipping - Title and Address - Mobile
	 * 
	 * US259561 :New Cart: Tick Mark on the Order and Shipment Tab.
	 * 
	 * US228202: New Cart: Shipping - Available Methods
	 * C410998: TA1910103
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testNewCartEditShippingAddressMandatoryFiedls(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US229276 :New Cart : Edit Shipping Address - Field level validations");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("4. Click on shop | Shop page should be displayed");
		Reporter.log("5. Click on Check it out button on Holidays banner | Offer detailsil page should be displayed");
		Reporter.log("5. Click on upgrade button | Line selection page should be displayed");
		Reporter.log("6. Click on any device| Phone selection page should be displayed");
		Reporter.log("7. Click on the device on Device PLP page |Device PDP page should be displayed ");
		Reporter.log("8. Select payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("9. Select Standared line on LS page | Trade in Phone Selection page should be displayed");
		Reporter.log(
				"10. Select device phone selection page and click on continue | Trade in confirmation page should be displayed");
		Reporter.log("11. Click on Continue buton on confirmation page |  PHP page should be displayed");
		Reporter.log("12. Click on Continue button on PHP page | Cart page should be displayed");
		Reporter.log("13. Click Continue to shipping button | Shipping details should be displayed");
		Reporter.log("14. Verify Shipping method options | Shipping options methods should be displayed");

		Reporter.log("15. Click Ship to different address button | New address label should be displayed");
		Reporter.log("16. Clear address line one | Address line notification error should be displayed");
		Reporter.log("17. Clear City name | Address line City notification error should be displayed");
		Reporter.log("18. Clear zip code | Address line Zip code notification error should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickContinueToShippingButton();
		cartPage.verifyUPSGroungShippingMethodOption();
		cartPage.verifyOrderDetailsTickMarkedDisplayed();
		cartPage.verifyShippingHeader();
		cartPage.verifyShippingDetails();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickShipToDifferentAddress();
		cartPage.clearAddressLineOne(ShopConstants.INVALID_ADDRESS);
		cartPage.verifyAddressInvalidErrorMessage();
		/*cartPage.clearCity(ShopConstants.INVALID_ADDRESS);
		cartPage.verifyAddressLineCityErrorMessage();
		cartPage.clearZipCode(ShopConstants.INVALID_ADDRESS);
		cartPage.verifyAddressLineZipCodeErrorMessage();*/
	}

	/**
	 * US228302 :New Cart : Shipping - Update 'Ship To Address' OR 'Cancel' CTA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testNewCartShippingUpdateToShipToAddressORcancel(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US228302 :New Cart: Shipping - Update 'Ship To Address' OR 'Cancel' CTA");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log(
				"1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See all phones on shop page |PLP Page should be diplayed");
		Reporter.log("4. Click on the device on Device PLP page | Device PDP Page should be displayed ");
		Reporter.log("5. Select payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select Standared line on LS page |Phone Selection Page should be displayed");
		Reporter.log("7. Click on Skip Trade in Hyper link on Phone selection page | PHP Page should be displayed ");
		Reporter.log("8. Click on Continue on PHP page| Cart Page should be displayed");
		Reporter.log(
				"9. Verified UpdateAddress button and Cancel button | Update address button and Cancel button displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickContinueToShippingButton();
		cartPage.clickShipToDifferentAddress();
		String presentAddress = cartPage.getAddressLineOne();
		cartPage.verifyCancelButton();
		cartPage.clearAddressLineOne(ShopConstants.INVALID_ADDRESS);
		cartPage.clickShippingAddressCancelButton();
		cartPage.clickShipToDifferentAddress();
		String previousAddress = cartPage.getAddressLineOne();
		Assert.assertEquals(presentAddress, previousAddress, "Address not updated");
		Reporter.log("Updated address is displayed");
	}

	/**
	 * US488856 - AAL - shipping tab error messages in line(shipping/e911/ppu)
	 * US414601: PPU Address - Continue CTA US444210 - [Continued] e911 - Edit e911
	 * Address (update/cancel) US414545 E911 Address - editing US355453: AAL - Order
	 * Details: Continue CTA US413928: Shipping - shipping method section US444210 -
	 * [Continued] e911 - Edit e911 Address (update/cancel)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void verifyErrorMessagesForShippingE911PpuAddressForAALBYODFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case US488856 - AAL - shipping tab error messages in line(shipping/e911/ppu)");
		Reporter.log("Data Condition | IR PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on 'Add A LINE' button | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Bring my own device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click 'Continue' button on Rate Plan page | Device Protection page should be displayed");
		Reporter.log("8. Click 'Continue' button on device protection page | Cart page should be displayed");
		Reporter.log("9. Click 'Continue to shipping' button | Shipping details should be displayed");
		Reporter.log(
				"10. Firstly - Shipping address, enter invalid address and click on 'Continue' button | Respective error message should be displayed above 'Cancel' CTA");
		Reporter.log("11. Click 'Cancel' CTA | Edit form should be closed off");
		Reporter.log(
				"12. Next e911 address, enter invalid address and click on 'Continue' button | Respective error message should be displayed above 'Cancel' CTA");
		Reporter.log("13. Click 'Cancel' CTA | Edit form should be closed off");
		Reporter.log(
				"14. Next ppu address, enter invalid address and click on 'Continue' button | Respective error message should be displayed above 'Cancel' CTA");
		Reporter.log("15. Click 'Cancel' CTA | Edit form should be closed off");
		Reporter.log(
				"16. Edit all 3 addresses and provide incorrect address and click Continue | Ensure that error message is shown under Shipping address");
		Reporter.log("17. Provide correct address under Shipping address and click continue | Ensure that error "
				+ "message is shown under 911 address((Incorrect address is already provided in above steps)");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageFromAALBYODFlow(myTmoData);
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();

		cartPage.verifyShippingAddressTitle();
		cartPage.verifyE911AddressTitle();
		cartPage.verifyPPUAddressTitle();

		cartPage.clickEditShippingAddress();
		cartPage.fillInvalidShippingAddressInfo();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyErrorMessageOnInvalidShippingAddress();
		cartPage.clickCancelCTAOnShippingEdit();
		cartPage.verifyShippingEditCollapseOnCancel();

		cartPage.clickPPUAddressEditButton();
		cartPage.fillInvalidPPUAddressInfo();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyErrorMessageOnInvalidPPUAddress();
		cartPage.clickPPUAddressCancelCTA();
		cartPage.verifyPPUAddressFieldsCollapse();

		cartPage.clickEditE911Address();
		cartPage.fillInvalidE911AddressInfo();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyErrorMessageOnInvalidE911Address();
		cartPage.clickE911AddressCancel();
		cartPage.verifyE911AddressFieldsCollapse();

		cartPage.clickEditShippingAddress();
		cartPage.clickPPUAddressEditButton();
		cartPage.clickEditE911Address();
		cartPage.fillInvalidShippingAddressInfo();
		cartPage.fillInvalidPPUAddressInfo();
		cartPage.fillInvalidE911AddressInfo();

		cartPage.clickContinueToPaymentButton();
		cartPage.verifyErrorMessageOnInvalidShippingAddress();
		cartPage.verifyErrorMessageOnInvalidPPUAddress();
		cartPage.verifyErrorMessageOnInvalidE911Address();

		cartPage.fillShippingAddressInfo();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyErrorMessageOnInvalidShippingAddressNotDisplayed();
		cartPage.verifyErrorMessageOnInvalidE911Address();
		cartPage.verifyErrorMessageOnInvalidPPUAddress();
	}

	/**
	 * US488856 - AAL - shipping tab error messages in line(shipping/e911/ppu)
	 * US414601: PPU Address - Continue CTA US444210 - [Continued] e911 - Edit e911
	 * Address (update/cancel) US414513 -[Continued] E911 Address US414561-
	 * [Continued] E911 Address - Whats US414598: PPU Address - editing US414545
	 * E911 Address - editing AAL Flow US414596: Primary Place of Use Address
	 * US444220: PPU - Edit PPU Address (update/cancel) US414597: PPU Address -
	 * Whats this Authorable Text - PPU Address Title & Text US414601: PPU Address -
	 * Continue CTA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION})
	public void verifyErrorMessagesForShippingE911PpuAddressForAALNONBYODFlow(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case: US488856 - AAL - shipping tab error messages in line(shipping/e911/ppu)");
		Reporter.log("Data Condition | STD PAH Customer with E911 address and AAL Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line button on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | PLP page should be displayed");
		Reporter.log("7. Select a Device in PLP page | PDP page should be displayed");
		Reporter.log("8. Click on 'Add to Cart' button | Rate plan page should be displayed");
		Reporter.log("9. Click Continue button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log("10. Click 'Yes protect my Phone' button | Cart Page should be displayed");
		Reporter.log("11. Click 'Continue to shipping' button | Shipping details should be displayed");
		Reporter.log(
				"12. Firstly - Shipping address, enter invalid address and click on 'Continue' button | Respective error message should be displayed above 'Cancel' CTA");
		Reporter.log(
				"13. Verify text below PPU Address Title | Text below Title - This is the address where your device will be used most often. We also use it to calculate monthly service taxes - Text should be displayed");

		Reporter.log("14. Click 'Cancel' CTA | Edit form should be closed off");
		Reporter.log(
				"15. Next e911 address, enter invalid address and click on 'Continue' button | Respective error message should be displayed above 'Cancel' CTA");
		Reporter.log("15. Click 'Cancel' CTA | Edit form should be closed off");
		Reporter.log(
				"16. Next ppu address, enter invalid address and click on 'Continue' button | Respective error message should be displayed above 'Cancel' CTA");
		Reporter.log("17. Click 'Cancel' CTA | Edit form should be closed off");
		Reporter.log(
				"17. Edit all 3 addresses and provide incorrect address and click Continue | Ensure that error message is shown under Shipping address");
		Reporter.log("19. Provide correct address under Shipping address and click continue | Ensure that error "
				+ "message is shown under 911 address((Incorrect address is already provided in above steps)");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageFromAALBuyNewPhoneFlowUsingInterstitialTradeIn(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();

		cartPage.verifyShippingAddressTitle();
		cartPage.verifyE911AddressTitle();
		cartPage.verifyPPUAddressTitle();
		cartPage.verifyPPUAddressLegalText();

		cartPage.clickEditShippingAddress();
		cartPage.fillInvalidShippingAddressInfo();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyErrorMessageOnInvalidShippingAddress();
		cartPage.clickCancelCTAOnShippingEdit();
		cartPage.verifyShippingEditCollapseOnCancel();
		
		cartPage.verifyPPUAddressEditButton();
		cartPage.clickPPUAddressEditButton();

		cartPage.fillInvalidPPUAddressInfo();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyErrorMessageOnInvalidPPUAddress();
		cartPage.clickPPUAddressCancelCTA();
		cartPage.verifyPPUAddressFieldsCollapse();

		cartPage.clickEditE911Address();
		cartPage.fillInvalidE911AddressInfo();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyErrorMessageOnInvalidE911Address();
		cartPage.clickE911AddressCancel();
		cartPage.verifyE911AddressFieldsCollapse();

		cartPage.clickEditShippingAddress();
		cartPage.fillInvalidShippingAddressInfo();
		cartPage.clickPPUAddressEditButton();
		cartPage.fillInvalidPPUAddressInfo();
		cartPage.clickEditE911Address();
		cartPage.fillInvalidE911AddressInfo();		

		cartPage.clickContinueToPaymentButton();
		cartPage.verifyErrorMessageOnInvalidShippingAddress();
		cartPage.verifyErrorMessageOnInvalidPPUAddress();
		cartPage.verifyErrorMessageOnInvalidE911Address();

		cartPage.fillShippingAddressInfo();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyErrorMessageOnInvalidShippingAddressNotDisplayed();
		cartPage.verifyErrorMessageOnInvalidE911Address();
		cartPage.verifyErrorMessageOnInvalidPPUAddress();

	}
	/**
	 * US413952: Shipping - shipping address section US413963: Shipping - Edit
	 * Shipping Address US413977: Shipping - Edit Shipping Address (update/cancel)
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testShippingAddressSectionStandardFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case US413952: Shipping - shipping address section" + "US413963: Shipping - Edit Shipping Address"
						+ "US413977: Shipping - Edit Shipping Address (update/cancel)");
		Reporter.log("Data Condition | Std PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select PaymentOption | PaymentOption should be selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select a line |  Line Selector Details page should be displayed");
		Reporter.log("9. Click skip trade in button | Device Protection page should be displayed");
		Reporter.log("10. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("11. Click OK button on Dont brake the bank modal | Accessory PLP page should be displayed");
		Reporter.log("12. Select Skip Accessories | Cart page should be displayed");
		Reporter.log("13. Click Continue to Shipping CTA | Shipping page should be displayed");
		Reporter.log(
				"14. Verify Shipping address title | 'SHIPPING ADDRESS' section title should be displayed on the Shipping page");
		Reporter.log(
				"15. Verify default shipping address | Default address i.e. Street name, city, State, Postal code should be displayed");
		Reporter.log("16. Verify Edit icon presence beside shipping address | Edit icon should be displayed");
		Reporter.log(
				"17. Click edit icon beside shipping address | Shipping address should expand and Address line1, Address line2, City, State, Zipcode fields should be displayed");
		Reporter.log("16. Verify Cancel CTA presence | Cancel CTA should be displayed");
		Reporter.log(
				"18. Click Cancel CTA | Shipping address should collapse and Address line1, Address line2, City, State, Zipcode fields should not be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingAddressTitle();
		cartPage.verifyDefaultShippingAddressInShippingTab();
		cartPage.verifyEditShippingCTADisplayed();
		cartPage.clickEditShippingAddress();
		cartPage.verifyAddressLineOneAuthorableText();
		cartPage.verifyAddressLineTwoAuthorableText();
		cartPage.verifyCityLabel();
		cartPage.verifyStateLabel();
		cartPage.verifyZIPcodeLabel();
		cartPage.verifyCancelButton();
		cartPage.clickCancelCTAOnShippingEdit();
	}

}
