package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.SyncupPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class SyncupPageTest extends TmngCommonLib {
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testDigitsPageTest(TMNGData tMNGData) {
		navigateToDigitsPage(tMNGData);
	}

	/**
	 * DE236505 - TMO - PROD: Sync up drive not displaying correct message for
	 * unknown cars.
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void verifyUnknownCarsMessage(TMNGData tMNGData) {

		Reporter.log("DE236505 - TMO - PROD: Sync up drive not displaying correct message for unknown cars.");
		Reporter.log("====================================================================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify SyncUp page is displayed | Syncup page should be displayed");
		Reporter.log("3. Verify vehicle year and select details | Selected vehicle year from the dropdown.");
		Reporter.log("4. Verify vehicle make and select details | Selected the vehicle make value from the list");
		Reporter.log("5. Verify vehicle model and select details | Selected the vehicle model from the list");
		Reporter.log("6. Verify vehicle engine and select details | Selected the vehicle engine from the list");
		Reporter.log(
				"8. Validate Compatibility checker message for All type of cars | Message is displayed successfully for All type of cars");
		Reporter.log(
				"====================================================================================================");
		Reporter.log("Actual Output:");
		loadTmngURL(tMNGData);
		getDriver().get("https://dmo.digital.t-mobile.com/content/t-mobile/consumer/offers/syncup.html");
		SyncupPage syncupPage = new SyncupPage(getDriver());
		syncupPage.verifyPageLoaded();
		syncupPage.selectVehicle1();
		syncupPage.verifyMessage1();
		syncupPage.selectVehicle2();
		syncupPage.verifyMessage2();
		// syncupPage.selectVehicle3();
		// syncupPage.verifyMessage3();
		syncupPage.selectVehicle4();
		syncupPage.verifyMessage4();
		// syncupPage.selectVehicle5();
		// syncupPage.verifyMessage5();
		getDriver().get("https://dmo.digital.t-mobile.com/content/t-mobile/consumer/offers/syncup.html");
	}
}
