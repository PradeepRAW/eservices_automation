/**
 * 
 */
package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.CommonLibrary;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.CartPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.SDPPage;
import com.tmobile.eservices.qa.pages.tmng.functional.StoreLocatorPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

/**
 * @author phani
 *
 */
public class StoreLocatorTest extends TmngCommonLib {

	/***
	 * US422723 TMO - GLOBAL - Store types on Search Blade TC ID: TC244933
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testStoreTypesOnSearchBladeForCity(TMNGData tMNGData) {
		Reporter.log("US422723 TMO - GLOBAL - Store types on Search Blade");
		Reporter.log("Data Condition | Any Query Parameter (City Name) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 5: Verify City Search result section is loading | City Search result section loaded successfully");
		Reporter.log(
				"Step 6: Verify text \"Showing T-mobile,and Authorized Stores and Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("Step 7: Verify Filter link is clickable | Filter link is working as expected");
		Reporter.log(
				"Step 8: Click Filter link and Verify Third Party Retailer is visible | Third Party Retailer option is visible on filter section.");
		Reporter.log(
				"Step 9: Check mark only Third Party Retailer and verify Apply CTA and click Apply CTA | Third Party Retailer is check marked and click Apply CTA was successful.");
		Reporter.log(
				"Step 10: Verify City Search result section is loading | City Search result section loaded successfully");
		Reporter.log(
				"Step 11: Verify URL has filterBy value as \"type3\" | URL filterBy parameter has been  successfully verified");
		Reporter.log(
				"Step 12: Verify text \"Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log(
				"Step 13: Verify items in store blade are having store type text as \"Third Party Retailer\" | Store type text is successfully verified.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyLoadedSearchSectionIsDisplayed();
		storeLocatorPage.clickFilterButton();
		storeLocatorPage.verifyThirdPartyFilterStore();
		storeLocatorPage.checkOrUncheckTMobileStoreFilterOption("false");
		storeLocatorPage.checkOrUncheckAuthorizedDealerFilterOption("false");
		storeLocatorPage.clickApplyButton();
		storeLocatorPage.verifyType3InUrl();
		storeLocatorPage.verifyLoadedThirdPartyRetailersSearchSectionIsDisplayed();
		storeLocatorPage.verifyThirdPartyRetailersTagInSearchResultBlades();
	}

	/***
	 * US422723 TMO - GLOBAL - Store types on Search Blade TC ID: TC245242
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testStoreTypesOnSearchBladeForZipCode(TMNGData tMNGData) {
		Reporter.log("US422723 TMO - GLOBAL - Store types on Search Blade");
		Reporter.log("Data Condition | Any Query Parameter (Zip Code) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter Zip Code in Search Text Box and click search icon | Zip Code as Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 5: Verify Zip Code Search result section is loading | Zip Code Search result section loaded successfully");
		Reporter.log(
				"Step 6: Verify text \"Showing T-mobile,and Authorized Stores and Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("Step 7: Verify Filter link is clickable | Filter link is working as expected");
		Reporter.log(
				"Step 8: Click Filter link and Verify Third Party Retailer is visible | Third Party Retailer option is visible on filter section.");
		Reporter.log(
				"Step 9: Check mark only Third Party Retailer and verify Apply CTA and click Apply CTA | Third Party Retailer is check marked and click Apply CTA was successful.");
		Reporter.log(
				"Step 10: Verify Zip Code Search result section for Third Party Retailer is loading | Zip Code Search result section for Third Party Retailer loaded successfully");
		Reporter.log(
				"Step 11: Verify URL has filterBy value as \"type3\" | URL filterBy parameter has been  successfully verified");
		Reporter.log(
				"Step 12: Verify text \"Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log(
				"Step 13: Verify items in store blade are having store type text as \"Third Party Retailer\" | Store type text is successfully verified.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getZipcode());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyLoadedSearchSectionIsDisplayed();
		storeLocatorPage.clickFilterButton();
		storeLocatorPage.verifyThirdPartyFilterStore();
		storeLocatorPage.checkOrUncheckTMobileStoreFilterOption("false");
		storeLocatorPage.checkOrUncheckAuthorizedDealerFilterOption("false");
		storeLocatorPage.clickApplyButton();
		storeLocatorPage.verifyType3InUrl();
		storeLocatorPage.verifyLoadedThirdPartyRetailersSearchSectionIsDisplayed();
		storeLocatorPage.verifyThirdPartyRetailersTagInSearchResultBlades();
	}

	/***
	 * US422725 TMO - GLOBAL - Store types on Store Detail Page TC ID - TC245301
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testStoreTypesOnStoreDetailPageForCity(TMNGData tMNGData) {
		Reporter.log("US422725 TMO - GLOBAL - Store types on Store Detail Page");
		Reporter.log("Data Condition | Any Query Parameter (City Name) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log("Step 5: Verify City search blade is loading | City search blade loaded successfully");
		Reporter.log(
				"Step 6: Verify text \"Showing T-mobile,and Authorized Stores and Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("Step 7: Verify Filter link is clickable | Filter link is working as expected");
		Reporter.log(
				"Step 8: Click Filter link and Verify Third Party Retailer is visible | Third Party Retailer option is visible on filter section.");
		Reporter.log(
				"Step 9: Check mark only Third Party Retailer and verify Apply CTA and click Apply CTA | Third Party Retailer is check marked and click Apply CTA was successful.");
		Reporter.log("Step 10: Verify City search blade is loading | City search blade section loaded successfully");
		Reporter.log(
				"Step 11: Verify URL has filterBy value as \"type3\" | URL filterBy parameter has been  successfully verified");
		Reporter.log(
				"Step 12: Verify text \"Third Party Retailers\" for loaded search blade| Text for search blade has been successfully verified.");
		Reporter.log(
				"Step 13: Click on the first store on the search blade| Successfully clicked first store on the search blade");
		Reporter.log(
				"Step 14: Verify \"Third Party Retailer\" text  is visible | Verified \"Third Party Retailer\" link is visible.");
		Reporter.log("Step 15: Verify \"Go Back\" link is clickable | Verified \"Go Back\" link is clickable.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyLoadedSearchSectionIsDisplayed();
		storeLocatorPage.clickFilterButton();
		storeLocatorPage.verifyThirdPartyFilterStore();
		storeLocatorPage.checkOrUncheckTMobileStoreFilterOption("false");
		storeLocatorPage.checkOrUncheckAuthorizedDealerFilterOption("false");
		storeLocatorPage.clickApplyButton();
		storeLocatorPage.verifyType3InUrl();
		storeLocatorPage.verifyLoadedThirdPartyRetailersSearchSectionIsDisplayed();
		storeLocatorPage.verifyThirdPartyRetailersTagInSearchResultBlades();
		storeLocatorPage.clickOnTprSearchBlade();
		storeLocatorPage.verifyStoreTypeOnStoreDetailPage();
		storeLocatorPage.clickOnGoBackButton();
	}

	/***
	 * US422725 TMO - GLOBAL - Store types on Store Detail Page TC ID - TC245302
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testStoreTypesOnStoreDetailPageForZipCode(TMNGData tMNGData) {
		Reporter.log("US422725 TMO - GLOBAL - Store types on Store Detail Page");
		Reporter.log("Data Condition | Any Query Parameter (Zip Code) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter Zip Code in Search Text Box and click search icon | Zip Code as Search Query parameter is entered and clicked on search icon");
		Reporter.log("Step 5: Verify Zip Code search blade is loading | Zip Code search blade loaded successfully");
		Reporter.log(
				"Step 6: Verify text \"Showing T-mobile,and Authorized Stores and Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("Step 7: Verify Filter link is clickable | Filter link is working as expected");
		Reporter.log(
				"Step 8: Click Filter link and Verify Third Party Retailer is visible | Third Party Retailer option is visible on filter section.");
		Reporter.log(
				"Step 9: Check mark only Third Party Retailer and verify Apply CTA and click Apply CTA | Third Party Retailer is check marked and click Apply CTA was successful.");
		Reporter.log(
				"Step 10: Verify Zip Code search blade is loading | Zip Code search blade section loaded successfully");
		Reporter.log(
				"Step 11: Verify URL has filterBy value as \"type3\" | URL filterBy parameter has been  successfully verified");
		Reporter.log(
				"Step 12: Verify text \"Third Party Retailers\" for loaded search blade| Text for search blade has been successfully verified.");
		Reporter.log(
				"Step 13: Click on the first store on the search blade| Successfully clicked first store on the search blade");
		Reporter.log(
				"Step 14: Verify \"Third Party Retailer\" text  is visible | Verified \"Third Party Retailer\" link is visible.");
		Reporter.log("Step 15: Verify \"Go Back\" link is clickable | Verified \"Go Back\" link is clickable.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getZipcode());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyLoadedSearchSectionIsDisplayed();
		storeLocatorPage.clickFilterButton();
		storeLocatorPage.verifyThirdPartyFilterStore();
		storeLocatorPage.checkOrUncheckTMobileStoreFilterOption("false");
		storeLocatorPage.checkOrUncheckAuthorizedDealerFilterOption("false");
		storeLocatorPage.clickApplyButton();
		storeLocatorPage.verifyType3InUrl();
		storeLocatorPage.verifyLoadedThirdPartyRetailersSearchSectionIsDisplayed();
		storeLocatorPage.verifyThirdPartyRetailersTagInSearchResultBlades();
		storeLocatorPage.clickOnTprSearchBlade();
		storeLocatorPage.verifyStoreTypeOnStoreDetailPage();
		storeLocatorPage.clickOnGoBackButton();
	}

	/***
	 * US422725 TMO - GLOBAL - Store types on Store Detail Page TC ID - TC245303
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testStoreTypesOnStoreDetailPageForState(TMNGData tMNGData) {
		Reporter.log("US422725 TMO - GLOBAL - Store types on Store Detail Page");
		Reporter.log("Data Condition | Any Query Parameter (State) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter State in Search Text Box and click search icon | State as Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 5: Verify State search blade is loading, dispalying the cities |  search blade loaded successfully");
		Reporter.log(
				"Step 6: Click on the city from the search blade| Successfully clicked on the city search result of the search blade");
		Reporter.log(
				"Step 7: Verify city Search result section is loading | City Search result section loaded successfully");
		Reporter.log(
				"Step 8: Verify items in store blade are having store type text as \"Third Party Retailer\" | Store type text is successfully verified.");
		Reporter.log("Step 9: Click on the TPR search blade| Successfully clicked on the TPR search blade");
		Reporter.log(
				"Step 10: Verify \"Third Party Retailer\" text  is visible | Verified \"Third Party Retailer\" link is visible.");
		Reporter.log("Step 11: Verify \"Go Back\" link is clickable | Verified \"Go Back\" link is clickable.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getState());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.clickOnCitySearchBlade(tMNGData.getCity());
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyThirdPartyRetailersTagInCitySearchResultBlades();
		storeLocatorPage.clickOnTprSearchBlade();
		storeLocatorPage.verifyStoreTypeOnStoreDetailPage();
		storeLocatorPage.clickOnGoBackButton();
	}

	/***
	 * US422727: TPR Filter TC ID: TC247517
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.TMNG })
	public void testVerifyTPRFilter(TMNGData tMNGData) {
		Reporter.log("US422727: TPR Filter");
		Reporter.log("Data Condition | Any Query Parameter (City Name) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 5: Verify City Search result section is loading | City Search result section loaded successfully");
		Reporter.log(
				"Step 6: Verify text \"Showing T-mobile,and Authorized Stores and Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("Step 7: Verify Filter link is clickable | Filter link is working as expected");
		Reporter.log(
				"Step 8: Click Filter link and Verify Third Party Retailer is visible | Third Party Retailer option is visible on filter section.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyLoadedSearchSectionIsDisplayed();
		storeLocatorPage.clickFilterButton();
		storeLocatorPage.verifyThirdPartyFilterStore();
	}

	/**
	 * US439902: Filters confirmation message TC ID:TC247514
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testFiltersconfirmationMessage(TMNGData tMNGData) {
		Reporter.log("US439902: Filters confirmation message");
		Reporter.log("Data Condition | Any Query Parameter (City Name) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log("Step 5: Verify City search blade is loading | City search blade loaded successfully");
		Reporter.log(
				"Step 6: Verify text 'Showing T-mobile,and Authorized Stores and Third Party Retailers' for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("Step 7: Verify Filter link is clickable | Filter link is working as expected");
		Reporter.log(
				"Step 8: Click Filter link and Verify Third Party Retailer is visible | Third Party Retailer option is visible on filter section.");
		Reporter.log(
				"Step 9: Check mark only Third Party Retailer and verify Apply CTA and click Apply CTA | Third Party Retailer is check marked and click Apply CTA was successful.");
		Reporter.log("Step 10: Verify City search blade is loading | City search blade section loaded successfully");
		Reporter.log(
				"Step 11: Verify text 'Third Party Retailers' for loaded search blade| Text for search blade has been successfully verified.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyLoadedSearchSectionIsDisplayed();
		storeLocatorPage.clickFilterButton();
		storeLocatorPage.verifyThirdPartyFilterStore();
		storeLocatorPage.checkOrUncheckTMobileStoreFilterOption("false");
		storeLocatorPage.checkOrUncheckAuthorizedDealerFilterOption("false");
		storeLocatorPage.clickApplyButton();
		storeLocatorPage.verifyLoadedThirdPartyRetailersSearchSectionIsDisplayed();
	}

	/**
	 *
	 * US439902: Filters confirmation message TC ID:TC247525
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.TMNG })
	public void testFiltersConfirmationMessageWithNoTPR(TMNGData tMNGData) {

		Reporter.log("US439902: Filters confirmation message");
		Reporter.log("Data Condition | Any Query Parameter (City Name) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 5: Verify City Search result section is loading | City Search result section loaded successfully");
		Reporter.log(
				"Step 6: Verify text 'Showing T-mobile,and Authorized Stores and Third Party Retailers' for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("Step 7: Verify Filter link is clickable | Filter link is working as expected");
		Reporter.log(
				"Step 8: Click Filter link and Verify Third Party Retailer is visible | Third Party Retailer option is visible on filter section.");
		Reporter.log(
				"Step 9: Uncheck mark only Third Party Retailer, verify Apply CTA and click Apply CTA | Third Party Retailer unchecked and click Apply CTA was successful.");
		Reporter.log(
				"Step 10: Verify Third Party Retailer text is not visible below search box. | Third Party retailer is not visible below search box.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyLoadedSearchSectionIsDisplayed();
		storeLocatorPage.clickFilterButton();
		storeLocatorPage.verifyThirdPartyFilterStore();
		storeLocatorPage.checkOrUncheckThirdPartyRetailerFilterOption("false");
		storeLocatorPage.clickApplyButton();
		storeLocatorPage.verifyThirdPartyRetailerTextNotDisplayed();
	}

	/***
	 * US422724 TMO - GLOBAL - Store types on Mobile Carousel TC ID - TC247478
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG, Group.ANDROID,
			Group.IOS })
	public void testStoretypesonMobileCarouselForCity(TMNGData tMNGData) {
		Reporter.log("US422724: Store types on Mobile Carousel");
		Reporter.log("Data Condition | Any Query Parameter (City Name) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 5: Verify City Search result section is loading | City Search result section loaded successfully");
		Reporter.log(
				"Step 6: Verify text \"Showing T-mobile,and Authorized Stores and Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("Step 7: Verify Filter link is clickable | Filter link is working as expected");
		Reporter.log(
				"Step 8: Click Filter link and Verify Third Party Retailer is visible | Third Party Retailer option is visible on filter section.");
		Reporter.log(
				"Step 9: Check mark only Third Party Retailer and verify Apply CTA and click Apply CTA | Third Party Retailer is check marked and click Apply CTA was successful.");
		Reporter.log(
				"Step 10: Verify City Search result section is loading | City Search result section loaded successfully");
		Reporter.log(
				"Step 11: Verify URL has filterBy value as \"type3\" | URL filterBy parameter has been  successfully verified");
		Reporter.log(
				"Step 12: Verify text \"Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log(
				"Step 13: Verify items in store blade are having store type text as \"Third Party Retailer\" | Store type text is successfully verified.");
		Reporter.log(
				"Step 14: Verify back arrow before State Name is clickable  | Back arrow before State name is clickable.");
		Reporter.log(
				"Step 15: Verify carousel items at bottom of page are having store type text as \"Third Party Retailer\" | Store type text is successfully verified.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		////navigateToStoreLocatorPageFromOverlayMenu(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyLoadedSearchSectionIsDisplayed();
		storeLocatorPage.clickFilterButton();
		storeLocatorPage.verifyThirdPartyFilterStore();
		storeLocatorPage.checkOrUncheckTMobileStoreFilterOption("false");
		storeLocatorPage.checkOrUncheckAuthorizedDealerFilterOption("false");
		storeLocatorPage.clickApplyButton();
		storeLocatorPage.verifyType3InUrl();
		storeLocatorPage.verifyLoadedThirdPartyRetailersSearchSectionIsDisplayed();
		storeLocatorPage.verifyThirdPartyRetailersTagInSearchResultBlades();
		storeLocatorPage.verifyTotalCountOfTprSearchBlades();
		storeLocatorPage.clickOnCloseButton();
		storeLocatorPage.verifyStoreTypeOnMobileCarousel();
	}

	/***
	 * US422724 TMO - GLOBAL - Store types on Mobile Carousel TC ID - TC247482
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG, Group.ANDROID,
			Group.IOS })
	public void testStoretypesonMobileCarouselForZipCode(TMNGData tMNGData) {
		Reporter.log("US422724: Store types on Mobile Carousel");
		Reporter.log("Data Condition | Any Query Parameter (Zip Code) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter Zip Code in Search Text Box and click search icon | Zip Code as Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 5: Verify Zip Code Search result section is loading | Zip Code Search result section loaded successfully");
		Reporter.log(
				"Step 6: Verify text \"Showing T-mobile,and Authorized Stores and Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("Step 7: Verify Filter link is clickable | Filter link is working as expected");
		Reporter.log(
				"Step 8: Click Filter link and Verify Third Party Retailer is visible | Third Party Retailer option is visible on filter section.");
		Reporter.log(
				"Step 9: Check mark only Third Party Retailer and verify Apply CTA and click Apply CTA | Third Party Retailer is check marked and click Apply CTA was successful.");
		Reporter.log(
				"Step 10: Verify Zip Code Search result section is loading | Zip Code Search result section loaded successfully");
		Reporter.log(
				"Step 11: Verify URL has filterBy value as \"type3\" | URL filterBy parameter has been  successfully verified");
		Reporter.log(
				"Step 12: Verify text \"Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log(
				"Step 13: Verify items in store blade are having store type text as \"Third Party Retailer\" | Store type text is successfully verified.");
		Reporter.log(
				"Step 14: Verify back arrow before State Name is clickable  | Back arrow before State name is clickable.");
		Reporter.log(
				"Step 15: Verify carousel items at bottom of page are having store type text as \"Third Party Retailer\" | Store type text is successfully verified.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		//navigateToStoreLocatorPageFromOverlayMenu(tMNGData);

		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getZipcode());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyLoadedSearchSectionIsDisplayed();
		storeLocatorPage.clickFilterButton();
		storeLocatorPage.verifyThirdPartyFilterStore();
		storeLocatorPage.checkOrUncheckTMobileStoreFilterOption("false");
		storeLocatorPage.checkOrUncheckAuthorizedDealerFilterOption("false");
		storeLocatorPage.clickApplyButton();
		storeLocatorPage.verifyType3InUrl();
		storeLocatorPage.verifyLoadedThirdPartyRetailersSearchSectionIsDisplayed();
		storeLocatorPage.verifyThirdPartyRetailersTagInSearchResultBlades();
		storeLocatorPage.verifyTotalCountOfTprSearchBlades();
		storeLocatorPage.clickOnCloseButton();
		storeLocatorPage.verifyStoreTypeOnMobileCarousel();
	}

	/***
	 * US422724 TMO - GLOBAL - Store types on Mobile Carousel TC ID - TC247484
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG, Group.ANDROID,
			Group.IOS })
	public void testStoretypesonMobileCarouselForState(TMNGData tMNGData) {
		Reporter.log("US422724 TMO - GLOBAL - Store types on Mobile Carousel");
		Reporter.log("Data Condition | Any Query Parameter (State) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter State in Search Text Box and click search icon | State as  Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 5: Verify State Search result section is loading | State Search result section loaded successfully");
		Reporter.log(
				"Step 6: Verify text \"Showing T-mobile,and Authorized Stores and Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("Step 7: Verify Filter link is clickable | Filter link is working as expected");
		Reporter.log(
				"Step 8: Click Filter link and Verify Third Party Retailer is visible | Third Party Retailer option is visible on filter section.");
		Reporter.log(
				"Step 9: Check mark only Third Party Retailer and verify Apply CTA and click Apply CTA | Third Party Retailer is check marked and click Apply CTA was successful.");
		Reporter.log(
				"Step 10: Verify State Search result section for Third Party Retailer is loading with city names | State Search result section for Third Party Retailer loaded with city names successfully");
		Reporter.log(
				"Step 11: Verify URL has filterBy value as \"type3\" | URL filterBy parameter has been  successfully verified");
		Reporter.log(
				"Step 12: Click on the first city blade | City Search result section for Third Party Retailer loaded successfully");
		Reporter.log(
				"Step 13: Verify text \"Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log(
				"Step 14: Verify items in store blade are having store type text as \"Third Party Retailer\" | Store type text is successfully verified.");
		Reporter.log(
				"Step 15: Verify back arrow before State Name is clickable  | Back arrow before State name is clickable.");
		Reporter.log(
				"Step 16: Verify carousel items at bottom of page are having store type text as \"Third Party Retailer\" | Store type text is successfully verified.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		//navigateToStoreLocatorPageFromOverlayMenu(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getState());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
	}

	/***
	 * US422723 TMO - GLOBAL - Store types on Search Blade TC ID - TC250122
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.TMNG })
	public void testErrorMessageForSearchNoResults(TMNGData tMNGData) {
		Reporter.log("US422723 TMO - GLOBAL - Store types on Search Blade");
		Reporter.log("Data Condition | Any Query Parameter (Zip Code) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter Zip Code in Search Text Box and click search icon | Zip Code as Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 5: Verify Zip Code Search result section is loading | Zip Code Search result section loaded successfully");
		Reporter.log(
				"Step 6: Verify text Showing T-mobile,and Authorized Stores and Third Party Retailers for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("Step 7: Verify Filter link is clickable | Filter link is working as expected");
		Reporter.log(
				"Step 8: Click Filter link and Verify Third Party Retailer is visible | Third Party Retailer option is visible on filter section.");
		Reporter.log(
				"Step 9: Check mark only Third Party Retailer and verify Apply CTA and click Apply CTA | Third Party Retailer is check marked and click Apply CTA was successful.");
		Reporter.log(
				"Step 10: Verify error message We couldn’t find a T-Mobile location in that area. Try widening your search and give it another shot. is displayed | Error message for search no results has been successfully verified");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getZipcode());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyLoadedSearchSectionIsDisplayed();
		storeLocatorPage.clickFilterButton();
		storeLocatorPage.verifyThirdPartyFilterStore();
		storeLocatorPage.checkOrUncheckTMobileStoreFilterOption("false");
		storeLocatorPage.checkOrUncheckAuthorizedDealerFilterOption("false");
		storeLocatorPage.clickApplyButton();
		storeLocatorPage.verifyErrorMessageDisplayForNoResults();
	}

	/***
	 * US445230 TMO IV - Fast Follower: Bread crumbs displayed on the SDP should be
	 * suppressed/hidden
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.NON_MOBILE })
	public void testHiddenBreadcrumbCartPage(TMNGData tMNGData) {
		Reporter.log("US445230 TMO IV - Fast Follower: Bread crumbs displayed on the SDP should be suppressed/hidden");
		Reporter.log("Data Condition | Any Store Name/ZipCode for SDP");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. Select any device |PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Click on Store name in Store Container | Store Details page should be displayed in modal");
		Reporter.log("6. Verify the breadcrumb at the bottom of the page | Breadcrumb should be hidden/suppressed");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySelectingPhone(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickStoreLocatorName();
		SDPPage sdpPage = new SDPPage(getDriver());
		sdpPage.verifyStoredetailsPage();
		sdpPage.verifyInventoryTextOnSDPPage();
		sdpPage.verifyBreadCrumbHidden();
	}

	/***
	 * US491169 TMO - GLOBAL - Yext SDP Photo: Display Photo UI
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreImageOnStoreDetailPageForCity(TMNGData tMNGData) {
		Reporter.log("US491169 TMO - GLOBAL - Yext SDP Photo: Display Photo UI");
		Reporter.log("======================================================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"2. Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("3. Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"4. Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log("5. Verify City search blade is loading | City search blade loaded successfully");
		Reporter.log("6. Verify Thumnail Image is loading | Thumnail Image is getting displayed successfully");
		Reporter.log("======================================================================");
		Reporter.log("Actual Output:");
		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyLoadedSearchSectionIsDisplayed();
		storeLocatorPage.clickTMobileStore();
	}

	/***
	 * DE156336 TMO - GLOBAL - SL Invalid deeplink doesn't resolve to /store-locator
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testDeeplinkStoreLocator(TMNGData tMNGData) {
		Reporter.log("DE156336 TMO - GLOBAL - SL Invalid deeplink doesn't resolve to /store-locator");
		Reporter.log("======================================================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"2. Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("3. Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"4. Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log("5. Verify City search blade is loading | City search blade loaded successfully");
		Reporter.log(
				"6. Verify Store Locator is loading for Invalid DeepLink | Store Locator is getting displayed successfully");
		Reporter.log("======================================================================");
		Reporter.log("Actual Output:");
		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.verifystorelocatorUrl();
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
	}

	/***
	 * US561344 SL New Designs|Remove Store types Filters from DeepLinks TC295961
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testRemoveStoreTypeFilterFromDeepLinks(TMNGData tMNGData) {

		Reporter.log("US561344: SL New Designs|Remove Store types Filters from Deeplinks");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. click on store locators link|Store locator page should be launched");
		Reporter.log("3.Enter the city in search box  | city should be entered successfully");
		Reporter.log("4.click on search box | clicked search box successfully");
		Reporter.log("5.validate filter button | It should not be available");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

	}

	/***
	 * US563898 SL New Designs|Remove Store types Filters from UI TC295961
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testRemoveStoreTypeFilter(TMNGData tMNGData) {

		Reporter.log("US563898: SL New Designs|Remove Store types Filters from UI");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. click on store locators link|Store locator page should be launched");
		Reporter.log("3.Enter the city in search box  | city should be entered successfully");
		Reporter.log("4.click on search box | clicked search box successfully");
		Reporter.log("5.validate filter button | It should not be available");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*navigateToStoreLocatorPageDirectlyThroughUrl();
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		// storeLocatorPage.clickStoreLocaterLink();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifyFilterNotVisible();*/

	}

	/***
	 * US561346 SL New Designs|Add Sorting by Wait times & distance TC294310
	 * 
	 * @param tMNGData
	 *//*
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreLocatorCitySortingByWaitTimes(TMNGData tMNGData) {

		Reporter.log("US561346:SL New Designs|Add Sorting by Wait times & distance");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("3.Enter the city in search box  | city should be entered successfully");
		Reporter.log("4.click on search box | clicked search box successfully");
		Reporter.log(
				"Step 5: Verify Zip Code Search result section is loading | Zip Code Search result section loaded successfully");
		Reporter.log("5: Click on drop down menu |  clicked on drop down menu successfully");
		Reporter.log("6: Select distance from drop down menu | selected distance from drop down menu successfully");
		Reporter.log("7: verify the search results | search results are in sorted order");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.clickOnSortDropDown();
		storeLocatorPage.selectWaitTimeFromDropDown();
		storeLocatorPage.verifySearchResultSortedByWaitTime();

	}*/

	/***
	 * US561346 SL New Designs|Add Sorting by Wait times & distance TC294311
	 * US610513 DW3 - SL LP: distance formatting
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreLocatorCitySortingByDistanceAndDistancesAreDisplayedInCorrectFormat(TMNGData tMNGData) {
		Reporter.log("US561346:SL New Designs|Add Sorting by Wait times & distance");
		Reporter.log("US610513 DW3 - SL LP: distance formatting");
		Reporter.log("================================");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log("Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3. Enter the city in search box  | City should be entered successfully");
		Reporter.log("Step 4. Click on search box | Clicked search box successfully");
		Reporter.log("Step 5: Click on sort drop down menu |  Clicked on sort drop down menu successfully");
		Reporter.log("Step 6: Select distance from drop down menu | Selected distance from drop down menu successfully");
		Reporter.log("Step 7: verify the search results | Search results are in sorted order");
		Reporter.log("Step 8 Verify distances are displayed upto 2 decimal places only | Distances are displayed upto 2 decimal places only ");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*navigateToStoreLocatorPageDirectlyThroughUrl();
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.clickOnSortDropDown();
		storeLocatorPage.selectDistanceFromDropDown();
		storeLocatorPage.verifySearchResultSortedByDistance();
		storeLocatorPage.verifyDistancesAreDisplayedUpto2DecimalPlacesOnly();*/
	}

	/***
	 * US500611 Retail Appt Setting| Store locator | Get-in Line | Form Fields,
	 * Language Preference & Notify Unchecked US571486 Get in Line | Properties
	 * update to V2.2 API TC294302 TC300650
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreGetInLineViaPhoneMyCartSaveCartFlowWithNotifyUnChecked(TMNGData tMNGData) {
		Reporter.log(
				"US500611: Retail Appt Setting| Store locator | Get-in Line | Form Fields, Language Preference & Notify Unchecked");
		Reporter.log("US571486: Get in Line | Properties update to V2.2 API");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch T-Mobile.com | T-Mobile landing page should be  displayed");
		Reporter.log("Step 2: Click on phones link | Cell phones page should be displayed");
		Reporter.log("Step 3: Select any device | PDP page should be displayed");
		Reporter.log("Step 4: Click on add to cart | Cart page should be displayed");
		Reporter.log("Step 5: Click save to your cart CTA | Save to your cart CTA clicked successfully");
		Reporter.log("Step 6: Add valid email address in textbox | Email added successfully ");
		Reporter.log("Step 7: Click save CTA | Save CTA clicked successfully");
		Reporter.log("Step 8: Verify and validate assigned store name | Selected store is displaying");
		Reporter.log("Step 9: Click on Get In line link| Get In line link clicked successfully.");
		Reporter.log(
				"Step 10: Get In Line form modal header should have assigned Store name | Verified same Store name successfully.");
		Reporter.log(
				"Step 11: Verify the display of text fields - First Name, Last Name, Reason for your visit, Additional reason for your visit | Verified text fields successfully in Get In Line form.");
		Reporter.log(
				"Step 12: Enter user information for mandatory fields-First Name, Last Name, Reason for your visit | User information entered successfully.");
		Reporter.log(
				"Step 13: Enter additional reason for your visit, which is optional | Additional reason for your visit entered successfully.");
		Reporter.log(
				"Step 14: Verify by default Notify checkbox is unchecked | Notify checkbox is unchecked by default");
		Reporter.log("Step 15: Validate content below Notify | Validated notify content successfully.");
		Reporter.log("Step 17: User language preference is English| Successfully changed user language preference.");
		Reporter.log(
				"Step 19: User can see additional text for required field and rates | Validated required field and text messaging content successfully.");
		Reporter.log(
				"Step 20: User click on 'Get in line' CTA | 'Get in line' CTA was highlighted and clicked successfully.");
		Reporter.log(
				"Step 21: Verify 'Get in line' success modal with its header | 'Get in line' success modal should be displayed successfully.");
		Reporter.log(
				"Step 22: Verify assigned store name in Get in line success modal | Store name value successfully verified");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		String storeName = cartPage.getStoreName();
		cartPage.clickOnSaveCart();
		cartPage.enterEmail(tMNGData);
		cartPage.clickSaveButtonOnSaveCartModel();
		cartPage.verifyStoresInSaveCartModalPopup();
		cartPage.verifySelectedStore(storeName);
		cartPage.verifyAndclickOnGetInLineCTA();
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreNameInGetInLineForm(storeName);
		storeLocatorPage.verifyFieldNamesInGetInLine();
		storeLocatorPage.enterFirstNameInGetInLine(tMNGData);
		storeLocatorPage.enterLastNameInGetInLine(tMNGData);
		String firstName = storeLocatorPage.retrieveFirstName();
		storeLocatorPage.selectReasonForYourVisit();
		storeLocatorPage.selectAdditionalReasonForYourVisit();
		storeLocatorPage.verifyNotifyCheckboxDefaultState();
		storeLocatorPage.validateContentBelowNotifyCheckbox();
		storeLocatorPage.clickOnEnglishRadioButton();
		storeLocatorPage.validateRequiredFieldAndTextMessagingContent();
		storeLocatorPage.verifyAndclickOnGetInLineCTA();
		storeLocatorPage.verifyGetInLineSuccessModal(firstName);
		storeLocatorPage.verifyStoreNameInGetInLineSuccessModal("storeName");*/
	}

	/***
	 * US500611 Retail Appt Setting| Store locator | Get-in Line | Form Fields,
	 * Language Preference & Notify Unchecked US571486 Get in Line | Properties
	 * update to V2.2 API TC294304 TC300651
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreGetInLineViaPhoneMyCartNearbyStoresFlowWithNotifyUnChecked(TMNGData tMNGData) {
		Reporter.log(
				"US500611: Retail Appt Setting| Store locator | Get-in Line | Form Fields, Language Preference & Notify Unchecked");
		Reporter.log("US571486: Get in Line | Properties update to V2.2 API");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch T-Mobile.com | T-Mobile landing page should be  displayed");
		Reporter.log("Step 2: Click on phones link | Cell phones page should be displayed");
		Reporter.log("Step 3: Select any device | PDP page should be displayed");
		Reporter.log("Step 4: Click on add to cart | Cart page should be displayed");
		Reporter.log("Step 5: Click save to your cart CTA | Save to your cart CTA clicked successfully");
		Reporter.log("Step 6: Add valid email address in textbox | Email added successfully ");
		Reporter.log("Step 7: Click save CTA | Save CTA clicked successfully");
		Reporter.log("Step 8: Verify and validate assigned store name | Selected store is displaying");
		Reporter.log("Step 9: Click on Find Nearby Stores link| Find Nearby Stores link clicked successfully.");
		Reporter.log("Step 10: Click on Item in stock checkbox| Items in stock checkbox clicked successfully.");
		Reporter.log("Step 11: Select second store from the search list| Store selected successfully.");
		Reporter.log("Step 12: Verify and validate assigned store name | Store name value successfully verified");
		Reporter.log("Step 13: Click on Get In line link| Get In line link clicked successfully.");
		Reporter.log(
				"Step 14: Get In Line form modal header should have assigned Store name | Verified same Store name successfully.");
		Reporter.log(
				"Step 15: Verify the display of fields - First Name, Last Name, Reason for your visit, Additional reason for your visit | Verified Field names successfully.");
		Reporter.log(
				"Step 16: Enter user information for mandatory fields-First Name, Last Name, Reason for your visit | User information entered successfully.");
		Reporter.log(
				"Step 17: Enter additional reason for your visit, which is optional | Additional reason for your visit entered successfully.");
		Reporter.log(
				"Step 18: Verify by default Notify checkbox is unchecked | Notify checkbox is unchecked by default");
		Reporter.log("Step 19: Validate content below Notify | Validated notify content successfully.");
		Reporter.log("Step 20: User language preference is English| Successfully changed user language preference.");
		Reporter.log(
				"Step 21: User can see additional text for required field and rates | Validated required field and text messaging content successfully.");
		Reporter.log(
				"Step 22: User click on 'Get in line' CTA | 'Get in line' CTA was highlighted and clicked successfully.");
		Reporter.log(
				"Step 23: Verify 'Get in line' success modal with its header | 'Get in line' success modal should be displayed successfully.");
		Reporter.log(
				"Step 24: Verify assigned store name in Get in line success modal | Store name in Get in line success modal is successfully verified");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		String storeName = cartPage.getStoreName();
		cartPage.clickOnSaveCart();
		cartPage.enterEmail(tMNGData);
		cartPage.clickSaveButtonOnSaveCartModel();
		cartPage.verifyStoresInSaveCartModalPopup();
		cartPage.verifySelectedStore(storeName);
		cartPage.clickFindNearByStoresOnSaveCartModal();
		cartPage.selectCheckbox();
		cartPage.selectSecondFilteredStore();
		cartPage.verifySelectedStoreInFilteredSearch(storeName);
		cartPage.verifyAndclickOnGetInLineCTA();*/
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreNameInGetInLineForm("storeName");
		storeLocatorPage.verifyFieldNamesInGetInLine();
		storeLocatorPage.enterFirstNameInGetInLine(tMNGData);
		storeLocatorPage.enterLastNameInGetInLine(tMNGData);
		String firstName = storeLocatorPage.retrieveFirstName();
		storeLocatorPage.selectReasonForYourVisit();
		storeLocatorPage.selectAdditionalReasonForYourVisit();
		storeLocatorPage.verifyNotifyCheckboxDefaultState();
		storeLocatorPage.validateContentBelowNotifyCheckbox();
		storeLocatorPage.clickOnEnglishRadioButton();
		storeLocatorPage.validateRequiredFieldAndTextMessagingContent();
		storeLocatorPage.verifyAndclickOnGetInLineCTA();
		storeLocatorPage.verifyGetInLineSuccessModal(firstName);
		storeLocatorPage.verifyStoreNameInGetInLineSuccessModal("storeName");
	}

	/***
	 * US500611 Retail Appt Setting| Store locator | Get-in Line | Form Fields,
	 * Language Preference & Notify Unchecked US571486 Get in Line | Properties
	 * update to V2.2 API TC294306 TC300665
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreGetInLineViaPDPMyCartNearbyStoresFlowWithNotifyUnChecked(TMNGData tMNGData) {
		Reporter.log(
				"US500611: Retail Appt Setting| Store locator | Get-in Line | Form Fields, Language Preference & Notify Unchecked");
		Reporter.log("US571486: Get in Line | Properties update to V2.2 API");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch T-Mobile.com | T-Mobile landing page should be displayed");
		Reporter.log("Step 2: Click on phones link | Cell phones page should be displayed");
		Reporter.log("Step 3: Select any device | PDP page should be displayed");
		Reporter.log("Step 4: Click on add to cart | Cart page should be displayed");
		Reporter.log("Step 5: Click on Find Nearby Stores link| Find Nearby Stores link clicked successfully.");
		Reporter.log("Step 6: Click on Item in stock checkbox| Items in stock checkbox clicked successfully.");
		Reporter.log("Step 7: Select second store from the search list| Store selected successfully.");
		Reporter.log("Step 8: Verify and validate assigned store name | Store name value successfully verified");
		Reporter.log("Step 9: Click on Get In line link| Get In line link clicked successfully.");
		Reporter.log(
				"Step 10: Get In Line form modal header should have assigned Store name | Verified same Store name successfully.");
		Reporter.log(
				"Step 11: Enter user information for mandatory fields-First Name, Last Name, Reason for your visit | User information entered successfully.");
		Reporter.log(
				"Step 12: Enter additional reason for your visit, which is optional | Additional reason for your visit entered successfully.");
		Reporter.log(
				"Step 13: Verify by default Notify checkbox is unchecked | Notify checkbox is unchecked by default");
		Reporter.log("Step 14: Validate notify content | Validated notify content successfully.");
		Reporter.log("Step 15: User language preference is English| Successfully changed user language preference.");
		Reporter.log(
				"Step 16: User can see additional text for required field and rates | Validated required field and text messaging content successfully.");
		Reporter.log(
				"Step 17: User click on 'Get in line' CTA | 'Get in line' CTA was highlighted and clicked successfully.");
		Reporter.log(
				"Step 18: Verify 'Get in line' success modal with its header | 'Get in line' success modal should be displayed successfully.");
		Reporter.log(
				"Step 19: Verify assigned store name in Get in line success modal | Store name value successfully verified");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		String storeName = cartPage.getStoreName();
		cartPage.clickFindNearbyStoresForDevice();
		cartPage.selectCheckbox();
		cartPage.selectSecondFilteredStore();
		cartPage.verifySelectedStoreInFilteredSearch(storeName);
		cartPage.verifyAndclickOnGetInLineCTA();*/
		/*StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreNameInGetInLineForm(storeName);
		storeLocatorPage.verifyFieldNamesInGetInLine();
		storeLocatorPage.enterFirstNameInGetInLine(tMNGData);
		storeLocatorPage.enterLastNameInGetInLine(tMNGData);
		String firstName = storeLocatorPage.retrieveFirstName();
		storeLocatorPage.selectReasonForYourVisit();
		storeLocatorPage.selectAdditionalReasonForYourVisit();
		storeLocatorPage.verifyNotifyCheckboxDefaultState();
		storeLocatorPage.validateContentBelowNotifyCheckbox();
		storeLocatorPage.clickOnEnglishRadioButton();
		storeLocatorPage.validateRequiredFieldAndTextMessagingContent();
		storeLocatorPage.verifyAndclickOnGetInLineCTA();
		storeLocatorPage.verifyGetInLineSuccessModal(firstName);
		storeLocatorPage.verifyStoreNameInGetInLineSuccessModal(storeName);*/
	}

	/***
	 * US509437 Retail Appt Setting| Store locator | Get-in Line | Notify checked
	 * US571486 Get in Line | Properties update to V2.2 API TC295061 TC300677
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreGetInLineViaPhoneMyCartNearbyStoresFlowWithNotifyChecked(TMNGData tMNGData) {
		Reporter.log("US509437: Retail Appt Setting| Store locator | Get-in Line | Notify checked");
		Reporter.log("US571486: Get in Line | Properties update to V2.2 API");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch T-Mobile.com | T-Mobile landing page should be  displayed");
		Reporter.log("Step 2: Click on phones link | Cell phones page should be displayed");
		Reporter.log("Step 3: Select any device | PDP page should be displayed");
		Reporter.log("Step 4: Click on add to cart | Cart page should be displayed");
		Reporter.log("Step 5: Click save to your cart CTA | Save to your cart CTA clicked successfully");
		Reporter.log("Step 6: Add valid email address in textbox | Email added successfully ");
		Reporter.log("Step 7: Click save CTA | Save CTA clicked successfully");
		Reporter.log("Step 8: Verify and validate assigned store name | Store name value successfully verified");
		Reporter.log("Step 9: Click on Find Nearby Stores link| Find Nearby Stores link clicked successfully.");
		Reporter.log("Step 10: Click on Item in stock checkbox| Items in stock checkbox clicked successfully.");
		Reporter.log("Step 11: Select second store from the search list| Store selected successfully.");
		Reporter.log("Step 12: Verify and validate assigned store name | Store name value successfully verified");
		Reporter.log("Step 13: Click on Get In line link| Get In line link clicked successfully.");
		Reporter.log(
				"Step 14: Get In Line form modal header should have assigned Store name | Verified same Store name successfully.");
		Reporter.log(
				"Step 15: Verify the display of fields - First Name, Last Name, Reason for your visit, Additional reason for your visit | Verified text fields successfully in Get In Line form.");
		Reporter.log("Step 16: Enter First Name in Get in line form | Entered first name in Get in line form");
		Reporter.log("Step 17: Enter Last Name in Get in line form | Entered last name in Get in line form");
		Reporter.log(
				"Step 18: Enter Reason for your visit in Get in line form | Selected Reason for your visit in Get in line form");
		Reporter.log(
				"Step 19: Enter additional reason for your visit, which is optional | Selected Additional reason for your visit in Get in line form");
		Reporter.log("Step 20: Click on Notify checkbox and check it | Notify checkbox is checked");
		Reporter.log(
				"Step 21: Verify the Text messages value in drop down selector | Text messages is displayed in the drop down");
		Reporter.log(
				"Step 22: Select 'Text messages' from drop down selector | Selected 'Text message' from drop down selector");
		Reporter.log("Step 23: Enter the 'phone number' in the text box| Entered phone number in the text box");
		Reporter.log("Step 24: User language preference is English| Successfully changed user language preference.");
		Reporter.log(
				"Step 25: User click on 'Get in line' CTA | 'Get in line' CTA was highlighted and clicked successfully.");
		Reporter.log(
				"Step 26: Verify 'Get in line' success modal with its header | 'Get in line' success modal should be displayed successfully.");
		Reporter.log(
				"Step 27: Verify assigned store name in Get in line success modal | Store name value successfully verified");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		String storeName = cartPage.getStoreName();
		cartPage.clickOnSaveCart();
		cartPage.enterEmail(tMNGData);
		cartPage.clickSaveButtonOnSaveCartModel();
		cartPage.verifyStoresInSaveCartModalPopup();
		cartPage.verifySelectedStore(storeName);
		cartPage.clickFindNearByStoresOnSaveCartModal();
		cartPage.selectCheckbox();
		cartPage.selectSecondFilteredStore();
		cartPage.verifySelectedStoreInFilteredSearch(storeName);
		cartPage.verifyAndclickOnGetInLineCTA();
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreNameInGetInLineForm(storeName);
		storeLocatorPage.verifyFieldNamesInGetInLine();
		storeLocatorPage.enterFirstNameInGetInLine(tMNGData);
		storeLocatorPage.enterLastNameInGetInLine(tMNGData);
		String firstName = storeLocatorPage.retrieveFirstName();
		storeLocatorPage.selectReasonForYourVisit();
		storeLocatorPage.selectAdditionalReasonForYourVisit();
		storeLocatorPage.clickNotifyCheckbox();
		storeLocatorPage.selectTextMessageInNotifyDropDown();
		storeLocatorPage.setPhoneNumber(tMNGData);
		storeLocatorPage.clickOnEnglishRadioButton();
		storeLocatorPage.verifyAndclickOnGetInLineCTA();
		storeLocatorPage.verifyGetInLineSuccessModal(firstName);
		storeLocatorPage.verifyStoreNameInGetInLineSuccessModal(storeName);*/
	}

	/***
	 * US509437 Retail Appt Setting| Store locator | Get-in Line | Notify checked
	 * US571486 Get in Line | Properties update to V2.2 API TC295441 TC300678
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreGetInLineViaPhoneMyCartCompleteInStoreFlowWithNotifyChecked(TMNGData tMNGData) {
		Reporter.log("US509437: Retail Appt Setting| Store locator | Get-in Line | Notify checked");
		Reporter.log("US571486: Get in Line | Properties update to V2.2 API");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch T-Mobile.com | T-Mobile landing page should be displayed");
		Reporter.log("Step 2: Click on phones link | Cell phones page should be displayed");
		Reporter.log("Step 3: Select any device | PDP page should be displayed");
		Reporter.log("Step 4: Click on add to cart | Cart page should be displayed");
		Reporter.log("Step 5: Click Complete in store link | Complete in store link clicked successfully");
		Reporter.log(
				"Step 6: Enter user information on the form -First Name, Last Name, Phone Number | Form loaded User information entered successfully.");
		{
		}
		Reporter.log("Step 7: User click on Required checkbox | Required checkbox clicked successfully.");
		Reporter.log("Step 8: Click 'Save my cart' CTA | 'Save my cart' CTA clicked successfully");
		Reporter.log(
				"Step 9: User click on Find a Store CTA | Find a Store CTA was highlighted and clicked successfully.");
		Reporter.log("Step 10: User redirects to Store locator page | Store Locator page is displayed");
		Reporter.log("Step 11: Verify Search text box is loading | Search text box is displayed on store locator page");
		Reporter.log("Step 12: Enter Zip Code in Search Text Box | Entered value in the search text box");
		Reporter.log("Step 12: Click search icon | Clicked on Search icon button");
		Reporter.log(
				"Step 13: Verify Zip Code Search result section is loading | Zip Code Search result section loaded successfully");
		Reporter.log("Step 14: Select second store from the search list| Store selected successfully.");
		Reporter.log("Step 15: Verify and validate assigned store name | Store name value successfully verified");
		Reporter.log("Step 16: Click on Get In line link| Get In line link clicked successfully.");
		Reporter.log(
				"Step 17: Get In Line form modal header should have assigned Store name | Verified same Store name successfully.");
		Reporter.log(
				"Step 18: Verify the display of fields - First Name, Last Name, Reason for your visit, Additional reason for your visit | Verified text fields successfully in Get In Line form.");
		Reporter.log("Step 19: Enter First Name in Get in line form | Entered first name in Get in line form");
		Reporter.log("Step 20: Enter Last Name in Get in line form | Entered last name in Get in line form");
		Reporter.log(
				"Step 21: Enter Reason for your visit in Get in line form | Selected Reason for your visit in Get in line form");
		Reporter.log(
				"Step 22: Enter additional reason for your visit, which is optional | Selected Additional reason for your visit in Get in line form");
		Reporter.log("Step 23: Click on Notify checkbox and check it | Notify checkbox is checked");
		Reporter.log(
				"Step 24: Verify the Text messages value in drop down selector | Text messages is displayed in the drop down");
		Reporter.log(
				"Step 25: Select 'Text messages' from drop down selector | Selected 'Text message' from drop down selector");
		Reporter.log("Step 26: Enter the 'phone number' in the text box| Entered phone number in the text box");
		Reporter.log("Step 27: User language preference is English| Successfully changed user language preference.");
		Reporter.log(
				"Step 28: User click on 'Get in line' CTA | 'Get in line' CTA was highlighted and clicked successfully.");
		Reporter.log(
				"Step 29: Verify 'Get in line' success modal with its header | 'Get in line' success modal should be displayed successfully.");
		Reporter.log(
				"Step 30: Verify assigned store name in Get in line success modal | Store name value successfully verified");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		String storeName = cartPage.getStoreName();
		cartPage.clickCompleteInStoreLink();
		cartPage.enterEmailInmodalWindow(tMNGData);
		cartPage.clickSaveMyCartOnlineCtaInModelWindow();
		cartPage.clickFindAStoreOnRetailSaveCartModal();

		// navigateTo//StoreLocatorPageInQAT(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreLocatorPage();
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.selectSecondStoreInSearchResults();
		storeLocatorPage.verifyStoreNameInStoreDetails();
		storeLocatorPage.verifyAndclickOnGetInLineCTAInStoreDetails();
		storeLocatorPage.verifyStoreNameInGetInLineForm(storeName);
		storeLocatorPage.verifyFieldNamesInGetInLine();
		storeLocatorPage.enterFirstNameInGetInLine(tMNGData);
		storeLocatorPage.enterLastNameInGetInLine(tMNGData);
		String firstName = storeLocatorPage.retrieveFirstName();
		storeLocatorPage.selectReasonForYourVisit();
		storeLocatorPage.selectAdditionalReasonForYourVisit();
		storeLocatorPage.clickNotifyCheckbox();
		storeLocatorPage.selectTextMessageInNotifyDropDown();
		storeLocatorPage.setPhoneNumber(tMNGData);
		storeLocatorPage.clickOnEnglishRadioButton();
		storeLocatorPage.verifyAndclickOnGetInLineCTA();
		storeLocatorPage.verifyGetInLineSuccessModal(firstName);
		storeLocatorPage.verifyStoreNameInGetInLineSuccessModal(storeName);*/
	}

	/***
	 * US509437 Retail Appt Setting| Store locator | Get-in Line | Notify checked
	 * US571486 Get in Line | Properties update to V2.2 API TC295442 TC300679
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreGetInLineViaPDPMyCartNearbyStoresFlowWithNotifyChecked(TMNGData tMNGData) {
		Reporter.log("US509437: Retail Appt Setting| Store locator | Get-in Line | Notify checked");
		Reporter.log("US571486: Get in Line | Properties update to V2.2 API");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch T-Mobile.com | T-Mobile landing page should be displayed");
		Reporter.log("Step 2: Click on phones link | Cell phones page should be displayed");
		Reporter.log("Step 3: Select any device | PDP page should be displayed");
		Reporter.log("Step 4: Click on add to cart | Cart page should be displayed");
		Reporter.log("Step 5: Click on Find Nearby Stores link| Find Nearby Stores link clicked successfully.");
		Reporter.log("Step 6: Click on Item in stock checkbox| Items in stock checkbox clicked successfully.");
		Reporter.log("Step 7: Select second store from the search list| Store selected successfully.");
		Reporter.log("Step 8: Verify and validate assigned store name | Store name value successfully verified");
		Reporter.log("Step 9: Click on Get In line link| Get In line link clicked successfully.");
		Reporter.log(
				"Step 10: Get In Line form modal header should have assigned Store name | Verified same Store name successfully.");
		Reporter.log(
				"Step 11: Verify the display of fields - First Name, Last Name, Reason for your visit, Additional reason for your visit | Verified text fields successfully in Get In Line form.");
		Reporter.log("Step 12: Enter First Name in Get in line form | Entered first name in Get in line form");
		Reporter.log("Step 13: Enter Last Name in Get in line form | Entered last name in Get in line form");
		Reporter.log(
				"Step 14: Enter Reason for your visit in Get in line form | Selected Reason for your visit in Get in line form");
		Reporter.log(
				"Step 15: Enter additional reason for your visit, which is optional | Selected Additional reason for your visit in Get in line form");
		Reporter.log("Step 16: Click on Notify checkbox and check it | Notify checkbox is checked");
		Reporter.log(
				"Step 17: Verify the Text messages value in drop down selector | Text messages is displayed in the drop down");
		Reporter.log(
				"Step 18: Select 'Text messages' from drop down selector | Selected 'Text message' from drop down selector");
		Reporter.log("Step 19: Enter the 'phone number' in the text box| Entered phone number in the text box");
		Reporter.log("Step 20: User language preference is English| Successfully changed user language preference.");
		Reporter.log(
				"Step 21: User click on 'Get in line' CTA | 'Get in line' CTA was highlighted and clicked successfully.");
		Reporter.log(
				"Step 22: Verify 'Get in line' success modal with its header | 'Get in line' success modal should be displayed successfully.");
		Reporter.log(
				"Step 23: Verify assigned store name in Get in line success modal | Store name value successfully verified");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		String storeName = cartPage.getStoreName();
		cartPage.clickFindNearbyStoresForDevice();
		cartPage.selectCheckbox();
		cartPage.selectSecondFilteredStore();
		cartPage.verifySelectedStoreInFilteredSearch(storeName);
		cartPage.verifyAndclickOnGetInLineCTA();
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreNameInGetInLineForm(storeName);
		storeLocatorPage.verifyFieldNamesInGetInLine();
		storeLocatorPage.enterFirstNameInGetInLine(tMNGData);
		storeLocatorPage.enterLastNameInGetInLine(tMNGData);
		String firstName = storeLocatorPage.retrieveFirstName();
		storeLocatorPage.selectReasonForYourVisit();
		storeLocatorPage.selectAdditionalReasonForYourVisit();
		storeLocatorPage.clickNotifyCheckbox();
		storeLocatorPage.selectTextMessageInNotifyDropDown();
		storeLocatorPage.setPhoneNumber(tMNGData);
		storeLocatorPage.clickOnEnglishRadioButton();
		storeLocatorPage.verifyAndclickOnGetInLineCTA();
		storeLocatorPage.verifyGetInLineSuccessModal(firstName);
		storeLocatorPage.verifyStoreNameInGetInLineSuccessModal(storeName);*/
	}

	/***
	 * US500611 Retail Appt Setting| Store locator | Get-in Line | Form Fields,
	 * Language Preference & Notify Unchecked US571486 Get in Line | Properties
	 * update to V2.2 API TC294305 TC300652
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreGetInLineWithNotifyUnChecked(TMNGData tMNGData) {
		Reporter.log(
				"US500611: Retail Appt Setting| Store locator | Get-in Line | Form Fields, Language Preference & Notify Unchecked");
		Reporter.log("US571486: Get in Line | Properties update to V2.2 API");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch T-Mobile.com | T-Mobile landing page should be displayed");
		Reporter.log("Step 2: Click on phones link | Cell phones page should be displayed");
		Reporter.log("Step 3: Select any device | PDP page should be displayed");
		Reporter.log("Step 4: Click on add to cart | Cart page should be displayed");
		Reporter.log("Step 5: Click Complete in store link | Complete in store link clicked successfully");
		Reporter.log(
				"Step 6: Enter user information on the form -First Name, Last Name, Phone Number | Form loaded User information entered successfully.");
		{
		}
		Reporter.log("Step 7: User click on Required checkbox | Required checkbox clicked successfully.");
		Reporter.log("Step 8: Click 'Save my cart' CTA | 'Save my cart' CTA clicked successfully");
		Reporter.log(
				"Step 9: User click on Find a Store CTA | Find a Store CTA was highlighted and clicked successfully.");
		Reporter.log("Step 10: User redirects to Store locator page | Store Locator page is displayed");
		Reporter.log("Step 11: Verify Search text box is loading | Search text box is displayed on store locator page");
		Reporter.log("Step 12: Enter Zip Code in Search Text Box | Entered value in the search text box");
		Reporter.log("Step 12: Click search icon | Clicked on Search icon button");
		Reporter.log(
				"Step 13: Verify Zip Code Search result section is loading | Zip Code Search result section loaded successfully");
		Reporter.log("Step 14: Select second store from the search list| Store selected successfully.");
		Reporter.log("Step 15: Verify and validate assigned store name | Store name value successfully verified");
		Reporter.log("Step 16: Click on Get In line link| Get In line link clicked successfully.");
		Reporter.log(
				"Step 17: Get In Line form modal header should have assigned Store name | Verified same Store name successfully.");
		Reporter.log(
				"Step 18: Enter user information for mandatory fields-First Name, Last Name, Reason for your visit | User information entered successfully.");
		Reporter.log(
				"Step 19: Enter additional reason for your visit, which is optional | Additional reason for your visit entered successfully.");
		Reporter.log(
				"Step 20: Verify by default Notify checkbox is unchecked | Notify checkbox is unchecked by default");
		Reporter.log("Step 21: Validate Notify content | Validated notify content successfully.");
		Reporter.log("Step 22: User language preference is English| Successfully changed user language preference.");
		Reporter.log(
				"Step 23: User can see additional text for required field and rates | Validated required field and text messaging content successfully.");
		Reporter.log(
				"Step 24: User click on 'Get in line' CTA | 'Get in line' CTA was highlighted and clicked successfully.");
		Reporter.log(
				"Step 25: Verify 'Get in line' success modal with its header | 'Get in line' success modal should be displayed successfully.");
		Reporter.log(
				"Step 26: Verify assigned store name in Get in line success modal | Store name value successfully verified");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*
		 * CartPage cartPage = navigateToCartPage(tMNGData); String storeName =
		 * cartPage.getStoreName(); cartPage.clickOnCompleteInStore();
		 * cartPage.enterEmailInmodalWindow(tMNGData); cartPage.clickCheckBox();
		 * cartPage.clickSaveCartCtaInModelWindow(); cartPage.clickFindAStore();
		 */

		//navigateToStoreLocatorPageInQAT(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreLocatorPage();
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.selectSecondStoreInSearchResults();
		storeLocatorPage.verifyStoreNameInStoreDetails();
		String storeName = storeLocatorPage.getStoreNameInStoreDetailsPage();
		storeLocatorPage.verifyAndclickOnGetInLineCTAInStoreDetails();
		storeLocatorPage.verifyStoreNameInGetInLineForm(storeName);
		storeLocatorPage.verifyFieldNamesInGetInLine();
		storeLocatorPage.enterFirstNameInGetInLine(tMNGData);
		storeLocatorPage.enterLastNameInGetInLine(tMNGData);
		String firstName = storeLocatorPage.retrieveFirstName();
		storeLocatorPage.selectReasonForYourVisit();
		storeLocatorPage.selectAdditionalReasonForYourVisit();
		storeLocatorPage.verifyNotifyCheckboxDefaultState();
		storeLocatorPage.validateContentBelowNotifyCheckbox();
		storeLocatorPage.clickOnEnglishRadioButton();
		storeLocatorPage.validateRequiredFieldAndTextMessagingContent();
		storeLocatorPage.verifyAndclickOnGetInLineCTA();
		storeLocatorPage.verifyGetInLineSuccessModal(firstName);
		storeLocatorPage.verifyStoreNameInGetInLineSuccessModal(storeName);
	}

	/***
	 * US500611 Retail Appt Setting| Store locator | Get-in Line | Form Fields,
	 * Language Preference & Notify Unchecked TC295433
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testErrorMessageInStoreGetInLineWhenNotifyUnChecked(TMNGData tMNGData) {
		Reporter.log(
				"US500611: Retail Appt Setting| Store locator | Get-in Line | Form Fields, Language Preference & Notify Unchecked");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch T-Mobile.com | T-Mobile landing page should be  displayed");
		Reporter.log("Step 2: Click on phones link | Cell phones page should be displayed");
		Reporter.log("Step 3: Select any device | PDP page should be displayed");
		Reporter.log("Step 4: Click on add to cart | Cart page should be displayed");
		Reporter.log("Step 5: Click save to your cart CTA | Save to your cart CTA clicked successfully");
		Reporter.log("Step 6: Add valid email address in textbox | Email added successfully ");
		Reporter.log("Step 7: Click save CTA | Save CTA clicked successfully");
		Reporter.log("Step 8: Verify and validate assigned store name | Selected store is displaying");
		Reporter.log("Step 9: Click on Get In line link| Get In line link clicked successfully.");
		Reporter.log(
				"Step 10: Get In Line form modal header should have assigned Store name | Verified same Store name successfully.");
		Reporter.log(
				"Step 11: Verify the display of fields - First Name, Last Name, Phone Number | Verified Field names successfully.");
		Reporter.log(
				"Step 12: Do not enter user information-First Name, leave it blank | 'Enter your first name' error message should be displayed");
		Reporter.log(
				"Step 13: Do not enter user information-Last Name | 'Enter your last name' error message should be displayed");
		Reporter.log(
				"Step 14: Do not enter user information-Reason for your visit | 'Please select a reason for your visit' error message should be displayed");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*
		 * CartPage cartPage = navigateToCartPage(tMNGData); String storeName =
		 * cartPage.getStoreName(); cartPage.clickOnSaveCart();
		 * cartPage.enterEmailinSaveCart(tMNGData);
		 * cartPage.clickSaveButtonOnSaveCartModel();
		 * cartPage.verifyStoresInSaveCartModalPopup();
		 * cartPage.verifySelectedStore(storeName);
		 */

		//navigateToStoreLocatorPageInQAT(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreLocatorPage();
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.selectSecondStoreInSearchResults();
		storeLocatorPage.verifyStoreNameInStoreDetails();
		String storeName = storeLocatorPage.getStoreNameInStoreDetailsPage();
		storeLocatorPage.verifyAndclickOnGetInLineCTAInStoreDetails();
		storeLocatorPage.verifyStoreNameInGetInLineForm(storeName);
		storeLocatorPage.verifyFieldNamesInGetInLine();
		storeLocatorPage.verifyErrorMessageForFirstName();
		storeLocatorPage.verifyErrorMessageForLastName();
		storeLocatorPage.verifyErrorMessageForReasonForVisit();
	}

	/***
	 * US509437 Retail Appt Setting| Store locator | Get-in Line | Notify checked
	 * US571486 Get in Line | Properties update to V2.2 API TC294312 TC300676
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreGetInLineWithNotifyCheckedViaTextMessage(TMNGData tMNGData) {
		Reporter.log("US509437: Retail Appt Setting| Store locator | Get-in Line | Notify checked");
		Reporter.log("US571486: Get in Line | Properties update to V2.2 API");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch T-Mobile.com | T-Mobile landing page should be displayed");
		Reporter.log("Step 2: Click on phones link | Cell phones page should be displayed");
		Reporter.log("Step 3: Select any device | PDP page should be displayed");
		Reporter.log("Step 4: Click on add to cart | Cart page should be displayed");
		Reporter.log("Step 5: Click save to your cart CTA | Save to your cart CTA clicked successfully");
		Reporter.log("Step 6: Add valid email address in textbox | Email added successfully ");
		Reporter.log("Step 7: Click save CTA | Save CTA clicked successfully");
		Reporter.log("Step 8: Verify and validate assigned store name | Store name value successfully verified");
		Reporter.log("Step 9: Click on Get In line link| Get In line link clicked successfully.");
		Reporter.log(
				"Step 10: Get In Line form modal header should have assigned Store name | Verified same Store name successfully.");
		Reporter.log(
				"Step 11: Verify the display of fields - First Name, Last Name, Reason for your visit, Additional reason for your visit | Verified text fields successfully in Get In Line form.");
		Reporter.log("Step 12: Enter First Name in Get in line form | Entered first name in Get in line form");
		Reporter.log("Step 13: Enter Last Name in Get in line form | Entered last name in Get in line form");
		Reporter.log(
				"Step 14: Enter Reason for your visit in Get in line form | Selected Reason for your visit in Get in line form");
		Reporter.log(
				"Step 15: Enter additional reason for your visit, which is optional | Selected Additional reason for your visit in Get in line form");
		Reporter.log(
				"Step 16: Verify by default Notify checkbox is unchecked | Notify checkbox is unchecked by default");
		Reporter.log("Step 17: Validate content below Notify | Validated notify content successfully.");
		Reporter.log("Step 18: Click on Notify checkbox and check it | Notify checkbox is checked");
		Reporter.log("Step 19: Verify the display of a drop down selector | Drop down selector must be displayed");
		Reporter.log(
				"Step 20: Verify the default value in drop down selector | Drop down selector is defaulted to Text message");
		Reporter.log(
				"Step 21: Verify the Text messages value in drop down selector | Text messages is displayed in the drop down");
		Reporter.log(
				"Step 22: Select 'Text messages' from drop down selector | Selected 'Text message' from drop down selector");
		Reporter.log("Step 23: Verify the display of notify text box | Text box must be displayed");
		Reporter.log("Step 24: Enter the 'phone number' in the text box| Entered phone number in the text box");
		Reporter.log("Step 25: User language preference is English| Successfully changed user language preference.");
		Reporter.log(
				"Step 26: User can see additional text for required field and rates | Validated required field and text messaging content successfully.");
		Reporter.log(
				"Step 27: User click on 'Get in line' CTA | 'Get in line' CTA was highlighted and clicked successfully.");
		Reporter.log(
				"Step 28: Verify 'Get in line' success modal with its header | 'Get in line' success modal should be displayed successfully.");
		Reporter.log(
				"Step 29: Verify assigned store name in Get in line success modal | Store name value successfully verified");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*
		 * CartPage cartPage = navigateToCartPage(tMNGData); String storeName =
		 * cartPage.getStoreName(); cartPage.clickOnSaveCart();
		 * cartPage.enterEmailinSaveCart(tMNGData);
		 * cartPage.clickSaveButtonOnSaveCartModel();
		 * cartPage.verifyStoresInSaveCartModalPopup();
		 * cartPage.verifySelectedStore(storeName);
		 * cartPage.verifyAndclickOnGetInLineCTA();
		 * cartPage.verifyStoreNameInModalWindow();
		 */

		//navigateToStoreLocatorPageInQAT(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreLocatorPage();
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.selectSecondStoreInSearchResults();
		storeLocatorPage.verifyStoreNameInStoreDetails();
		String storeName = storeLocatorPage.getStoreNameInStoreDetailsPage();
		storeLocatorPage.verifyAndclickOnGetInLineCTAInStoreDetails();
		storeLocatorPage.verifyStoreNameInGetInLineForm(storeName);
		storeLocatorPage.verifyFieldNamesInGetInLine();
		storeLocatorPage.enterFirstNameInGetInLine(tMNGData);
		storeLocatorPage.enterLastNameInGetInLine(tMNGData);
		String firstName = storeLocatorPage.retrieveFirstName();
		storeLocatorPage.selectReasonForYourVisit();
		storeLocatorPage.selectAdditionalReasonForYourVisit();
		storeLocatorPage.verifyNotifyCheckboxDefaultState();
		storeLocatorPage.validateContentBelowNotifyCheckbox();
		storeLocatorPage.clickNotifyCheckbox();
		storeLocatorPage.verifyNotifyDropDown();
		storeLocatorPage.clickNotifyDropDown();
		storeLocatorPage.verifyNotifyDropDownDefaultValue();
		storeLocatorPage.selectTextMessageInNotifyDropDown();
		storeLocatorPage.verifyPhoneNumberTextBox();
		storeLocatorPage.setPhoneNumber(tMNGData);
		storeLocatorPage.clickOnEnglishRadioButton();
		storeLocatorPage.validateRequiredFieldAndTextMessagingContent();
		storeLocatorPage.verifyAndclickOnGetInLineCTA();
		storeLocatorPage.verifyGetInLineSuccessModal(firstName);
		storeLocatorPage.verifyStoreNameInGetInLineSuccessModal(storeName);
	}

	/***
	 * US509437 Retail Appt Setting| Store locator | Get-in Line | Notify checked
	 * US571486 Get in Line | Properties update to V2.2 API TC296493 TC300680
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreGetInLineWithNotifyCheckedViaEmail(TMNGData tMNGData) {
		Reporter.log("US509437: Retail Appt Setting| Store locator | Get-in Line | Notify checked");
		Reporter.log("US571486: Get in Line | Properties update to V2.2 API");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch T-Mobile.com | T-Mobile landing page should be displayed");
		Reporter.log("Step 2: Click on phones link | Cell phones page should be displayed");
		Reporter.log("Step 3: Select any device | PDP page should be displayed");
		Reporter.log("Step 4: Click on add to cart | Cart page should be displayed");
		Reporter.log("Step 5: Click save to your cart CTA | Save to your cart CTA clicked successfully");
		Reporter.log("Step 6: Add valid email address in textbox | Email added successfully ");
		Reporter.log("Step 7: Click save CTA | Save CTA clicked successfully");
		Reporter.log("Step 8: Verify and validate assigned store name | Store name value successfully verified");
		Reporter.log("Step 9: Click on Get In line link| Get In line link clicked successfully.");
		Reporter.log(
				"Step 10: Get In Line form modal header should have assigned Store name | Verified same Store name successfully.");
		Reporter.log(
				"Step 11: Verify the display of fields - First Name, Last Name, Reason for your visit, Additional reason for your visit | Verified Field names successfully.");
		Reporter.log(
				"Step 12: Enter user information for mandatory fields-First Name, Last Name, Reason for your visit | User information entered successfully.");
		Reporter.log(
				"Step 13: Enter additional reason for your visit, which is optional | Additional reason for your visit entered successfully.");
		Reporter.log(
				"Step 14: Verify by default Notify checkbox is unchecked | Notify checkbox is unchecked by default");
		Reporter.log("Step 15: Validate content below Notify | Validated notify content successfully.");
		Reporter.log("Step 16: Click on Notify checkbox and check it | Notify checkbox is checked");
		Reporter.log("Step 17: Verify the display of a drop down selector | Drop down selector must be displayed");
		Reporter.log(
				"Step 18: Verify the default value in drop down selector | Drop down selector is defaulted to Text message");
		Reporter.log("Step 19: Verify the 'Email' value in drop down selector | Email is displayed in the drop down");
		Reporter.log("Step 20: Select 'Email' from drop down selector | Selected 'Email' from drop down selector");
		Reporter.log("Step 21: Verify the display of notify text box | Text box must be displayed");
		Reporter.log("Step 22: Enter 'email address' in the text box| Entered email address in the text box");
		Reporter.log("Step 23: Validate content below Notify | Validated notify content successfully.");
		Reporter.log("Step 24: User language preference is English| Successfully changed user language preference.");
		Reporter.log(
				"Step 25: User can see additional text for required field and rates | Validated required field and text messaging content successfully.");
		Reporter.log(
				"Step 26: User click on 'Get in line' CTA | 'Get in line' CTA was highlighted and clicked successfully.");
		Reporter.log(
				"Step 27: Verify 'Get in line' success modal with its header | 'Get in line' success modal should be displayed successfully.");
		Reporter.log(
				"Step 28: Verify assigned store name in Get in line success modal | Store name value successfully verified");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*
		 * CartPage cartPage = navigateToCartPage(tMNGData); String storeName =
		 * cartPage.getStoreName(); cartPage.clickOnSaveCart();
		 * cartPage.enterEmailinSaveCart(tMNGData);
		 * cartPage.clickSaveButtonOnSaveCartModel();
		 * cartPage.verifyStoresInSaveCartModalPopup();
		 * cartPage.verifySelectedStore(storeName);
		 * cartPage.verifyAndclickOnGetInLineCTA();
		 */

		//navigateToStoreLocatorPageInQAT(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreLocatorPage();
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.selectSecondStoreInSearchResults();
		storeLocatorPage.verifyStoreNameInStoreDetails();
		String storeName = storeLocatorPage.getStoreNameInStoreDetailsPage();
		storeLocatorPage.verifyAndclickOnGetInLineCTAInStoreDetails();
		storeLocatorPage.verifyStoreNameInGetInLineForm(storeName);
		storeLocatorPage.verifyFieldNamesInGetInLine();
		storeLocatorPage.enterFirstNameInGetInLine(tMNGData);
		storeLocatorPage.enterLastNameInGetInLine(tMNGData);
		String firstName = storeLocatorPage.retrieveFirstName();
		storeLocatorPage.selectReasonForYourVisit();
		storeLocatorPage.selectAdditionalReasonForYourVisit();
		storeLocatorPage.verifyNotifyCheckboxDefaultState();
		storeLocatorPage.validateContentBelowNotifyCheckbox();
		storeLocatorPage.clickNotifyCheckbox();
		storeLocatorPage.verifyNotifyDropDown();
		storeLocatorPage.verifyNotifyDropDownDefaultValue();
		storeLocatorPage.selectEmailInNotifyDropDown();
		storeLocatorPage.verifyEmailTextBox();
		storeLocatorPage.setEmail(tMNGData);
		storeLocatorPage.clickOnEnglishRadioButton();
		storeLocatorPage.validateRequiredFieldAndTextMessagingContent();
		storeLocatorPage.verifyAndclickOnGetInLineCTA();
		storeLocatorPage.verifyGetInLineSuccessModal(firstName);
		storeLocatorPage.verifyStoreNameInGetInLineSuccessModal(storeName);
	}

	/***
	 * US509437 Retail Appt Setting| Store locator | Get-in Line | Notify checked
	 * TC295443
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testErrorMessageInStoreGetInLineWhenNotifyChecked(TMNGData tMNGData) {
		Reporter.log("US509437: Retail Appt Setting| Store locator | Get-in Line | Notify checked");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch T-Mobile.com | T-Mobile landing page should be  displayed");
		Reporter.log("Step 2: Click on phones link | Cell phones page should be displayed");
		Reporter.log("Step 3: Select any device | PDP page should be displayed");
		Reporter.log("Step 4: Click on add to cart | Cart page should be displayed");
		Reporter.log("Step 5: Click save to your cart CTA | Save to your cart CTA clicked successfully");
		Reporter.log("Step 6: Add valid email address in textbox | Email added successfully ");
		Reporter.log("Step 7: Click save CTA | Save CTA clicked successfully");
		Reporter.log("Step 8: Verify and validate assigned store name | Store name value successfully verified");
		Reporter.log("Step 9: Click on Get In line link| Get In line link clicked successfully.");
		Reporter.log(
				"Step 10: Get In Line form modal header should have assigned Store name | Verified same Store name successfully.");
		Reporter.log("Step 11: Click on Notify checkbox and check it | Notify checkbox is checked");
		Reporter.log(
				"Step 15: Verify the Text messages value in drop down selector | Text messages is displayed in the drop down");
		Reporter.log(
				"Step 12: Select 'Text Messages' from drop down selector | Selected 'Text messages' from drop down");
		Reporter.log(
				"Step 13: Do not enter the phone number in the text box, clear the data by leaving it blank | 'Enter your phone number' error message should be displayed");
		Reporter.log("Step 15: Verify the Email value in drop down selector | Email is displayed in the drop down");
		Reporter.log("Step 14: Select 'Email' from drop down selector | Selected 'Email' from drop down");
		Reporter.log(
				"Step 15: Do not enter Email address in the text box, clear the data by leaving it blank | 'Enter your email address' error message should be displayed");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		/*
		 * CartPage cartPage = navigateToCartPage(tMNGData); String storeName =
		 * cartPage.getStoreName(); cartPage.clickOnSaveCart();
		 * cartPage.enterEmailinSaveCart(tMNGData);
		 * cartPage.clickSaveButtonOnSaveCartModel();
		 * cartPage.verifyStoresInSaveCartModalPopup();
		 * cartPage.verifySelectedStore(storeName);
		 * cartPage.verifyAndclickOnGetInLineCTA()
		 */

		//navigateTo//StoreLocatorPageInQAT(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreLocatorPage();
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.selectSecondStoreInSearchResults();
		storeLocatorPage.verifyStoreNameInStoreDetails();
		String storeName = storeLocatorPage.getStoreNameInStoreDetailsPage();
		storeLocatorPage.verifyAndclickOnGetInLineCTAInStoreDetails();
		storeLocatorPage.verifyStoreNameInGetInLineForm(storeName);
		storeLocatorPage.verifyFieldNamesInGetInLine();
		storeLocatorPage.clickNotifyCheckbox();
		storeLocatorPage.selectTextMessageInNotifyDropDown();
		storeLocatorPage.verifyErrorMessageForPhoneNumber();
		storeLocatorPage.verifyErrorMessageForInvalidPhoneNumber();
		storeLocatorPage.selectEmailInNotifyDropDown();
		storeLocatorPage.verifyErrorMessageForEmail();
		storeLocatorPage.verifyErrorMessageForInvalidEmail();
	}

	/***
	 * US571493:Add Sprint Repair Centers to Store Details US561347:SL New Designs|
	 * Add sorting by Sprint Repair Centers US561348:Add Sprint Repair Centers to
	 * Store list TC298763 TC298761 TC298762
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void verifyMessageForStoreLocatorsAuthorisedWithSprintRepairCenter(TMNGData tMNGData) {
		Reporter.log("US571493:Add Sprint Repair Centers to Store Details");
		Reporter.log("US561347:SL New Designs| Add sorting by Sprint Repair Centers");
		Reporter.log("US561348:Add Sprint Repair Centers to Store list");
		Reporter.log("================================");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter Zip Code in Search Text Box and click search icon | Zip Code as Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 5: Verify Zip Code Search result section is loading | Zip Code Search result section loaded successfully");
		Reporter.log(
				"Step 6: Verify search result list store locators |  Search result section should load stores with closest distance");
		Reporter.log("Step 7: Click on Sort Link to see options | Sort Link  clicked successfully");
		Reporter.log(
				"Step 8: Verify Product Repair Center in the drop down options | Product Repair center is displayed successfully in the drop down");
		Reporter.log(
				"Step 9: Select option By Product Repair Centers (TBD) | Selected Product Repair Center from the drop down");
		Reporter.log(
				"Step 10: Verify search result list once selected sprint repair sort |  Search results are loaded for the respective sort");
		Reporter.log(
				"Step 11: Verify 'Product Repair Center Inside' text in search result blades(should be visible in the list , if any) | 'Product Repair Center Inside' text should be displayed in the search blades");
		Reporter.log(
				"Step 12: Verify followed search results which does not have Product Repair Center should be sorted based on closest distance |  Following Search blades are sorted by distance");
		Reporter.log("Step 13: Verify Repair icon | Repair icon should be displayed");
		Reporter.log(
				"Step 14: Click on any of the search blade authorized with repair center  | Store detail page should be displayed");
		Reporter.log(
				"Step 15: Verify Product repair center text displayed below the store name in details page | 'Product Repair center Inside' text should be displayed in Store details page");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifySearchResultSortedByDistance();
		storeLocatorPage.clickOnSortDropDown();
		storeLocatorPage.verifyProductRepairCenterInSortDropDown();
		storeLocatorPage.selectProductRepairCenterFromDropDown();
		storeLocatorPage.verifyProductRepairCenterInSearchBlades();
		storeLocatorPage.verifyOtherSearchBladesSortedByDistance();
		storeLocatorPage.verifyRepairIcon();
		storeLocatorPage.clickOnSearchBladeWithPRC();
		storeLocatorPage.verifyProductRepairCenterInStoreDetails();
	}

	/***
	 * DE230355 - TMO Prod: Store Locator: Go back Link in SDP is not working
	 * properly after pagination of store list page
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void verifyGoBackLink(TMNGData tMNGData) {
		Reporter.log(
				"DE230355 - TMO Prod: Store Locator: Go back Link in SDP is not working properly after pagination of store list page");
		Reporter.log("======================================================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"2. Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("3. Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"4. Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log("5. Verify City search blade is loading | City search blade loaded successfully");
		Reporter.log("6. Click on Store | Store Details page displayed successfully");
		Reporter.log("7. Click on GoBack Link | Go Back to the City search page successfully");
		Reporter.log("8. Click on Next Link | Go to Next page of City search page successfully");
		Reporter.log("9. Click on Store | Store Details page displayed successfully");
		Reporter.log("10. Click on GoBack Link | Go Back to the City search page successfully");
		Reporter.log("======================================================================");
		Reporter.log("Actual Output:");
		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyLoadedSearchSectionIsDisplayed();
		storeLocatorPage.clickTMobileStore();
		storeLocatorPage.clickOnGoBackButton();
		storeLocatorPage.clickTMobileStore();
		storeLocatorPage.clickOnGoBackButton();
	}

	/**
	 * US568881 - SL | Photo Carousel | Get photos and thumbnails for store 
	 * US588839 - SL | Photo Carousel | Images Selected State
	 * 
	 * @param tMNGData
	 *//*
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testPhotoCarouselGetPhotosAndThumbnailsForStore(TMNGData tMNGData) {
		Reporter.log("US568881 - SL | Photo Carousel | Get photos and thumbnails for store");
		Reporter.log("======================================================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 2: Click on Store locator link  | Store locator page should be displayed");
		Reporter.log("Step 3: Click on Log in  | Log in successful and Create special hours page should be displayed");
		Reporter.log("Step 4: Enter a particular store which has Store exterior or interior photos | Store details entered");
		Reporter.log("Step 5: Select the Store by click on the store address | Store details page displayed");
		Reporter.log("Step 6: Enter valid City/Zip code/State details |  valid City/Zip code/State details entered");
		Reporter.log("Step 7: Validate Photos displayed | Photo should be displayed");
		Reporter.log("Step 8: Click on the preview photo | preview photo should be displayed, photo src is coming from cdn");
		Reporter.log("Step 9: Validate in case of more than 1 photo | Left caret link and Right Caret link should be displayed");
		Reporter.log("Step 10: Validate Click on right Caret  navigate to next image | next Image should be displayed, photo source is from cdn");
		Reporter.log("Step 11: validate Click on left caret navigate to previous Image | preview photo should be displayed, photo src is coming from cdn");
		Reporter.log("Step 12: Validate number of '.' (dots) equals to number of photos | number of dot's displayed should be equal to number of images");
		Reporter.log("======================================================================");
		Reporter.log("Actual Output:");
		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		int numberOfImages=storeLocatorPage.verifyImageIsDisplayed();
		storeLocatorPage.verifyImagesForCdn();
		storeLocatorPage.clickphotoPreview();
		storeLocatorPage.verifyDotsEqualsImages(numberOfImages);
		storeLocatorPage.verifyRightNavigateToNextImage();
		storeLocatorPage.verifyLeftNavigateToNextImage();
		

	}*/
	
	
	/**
	 * US481920 TMO - IV Accessories Accessibility - SDP from Main PDP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.NON_MOBILE })
	public void testStoreLocatorFromAccessoryPdp(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US481920	TMO - IV Accessories Accessibility - SDP from Main PDP");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessories should be displayed");
		Reporter.log("4. Select any Accessory | Accessory PDP Page should be selected");
		Reporter.log("5. Verify Store name on Inventory container | Store Name  should be displayed");
		Reporter.log("6. Click on store name | Store Locator page should be displayed");
		Reporter.log("7. Verify 'Directions' CTA is displayed | 'Directions' CTA should be displayed");
		Reporter.log("8. Verify 'Get In Line' CTA is displayed | 'Get In Line' CTA should be displayed");
		Reporter.log("9. Verify 'Appointment' CTA is displayed | 'Appointment' CTA should be displayed");
		Reporter.log("10. Verify 'About The store' description | 'About The store' description should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage accessoryDetailsPage = navigateToAccessoryMainPDP(tMNGData);
		accessoryDetailsPage.verifyStoreLocatorName();
		accessoryDetailsPage.clickStoreLocatorName();
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreLocatorPage();
		storeLocatorPage.verifyDirectionsCTA();
		storeLocatorPage.verifyGetInLineCTA();
		storeLocatorPage.verifyAppointmentCTA();
		storeLocatorPage.verifyAboutTheStoreDescription();
	}

	/**
	 * US352086 TMO - IV - SDP Map US306677: TMO - IV - SDP from PDP/mini US454309
	 * :TMO - Search this area button on SDP Store Map should be
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.NON_MOBILE })
	public void testStoreLocatorMapFromPhones(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US352086 - TMO - IV - SDP Map");
		Reporter.log("Test Case : US306677: TMO - IV - SDP from PDP/mini");
		Reporter.log("Test Case : US454309 :TMO - Search this area button on SDP Store Map should be");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. Select any device | PDP page should be displayed");
		Reporter.log("4. Click on store name | Store locator page should be displayed");
		Reporter.log("5. Verify map and zoom button |Map and zoom in CTA should be displayed");
		Reporter.log("6. Select a store location | Selected store location should be displayed in the container");
		Reporter.log("7. Verify 'Directions' CTA is displayed | 'Directions' CTA should be displayed");
		Reporter.log("8. Verify 'Get In Line' CTA is displayed | 'Get In Line' CTA should be displayed");
		Reporter.log("9. Verify 'Appointment' CTA is displayed | 'Appointment' CTA should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.verifyStoreLocatorName();
		phonesPdpPage.clickStoreLocatorName();
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreLocatorPage();
		storeLocatorPage.verifyDirectionsCTA();
		storeLocatorPage.verifyGetInLineCTA();
		storeLocatorPage.verifyAppointmentCTA();
		storeLocatorPage.verifyMapDisplayed();
		storeLocatorPage.clickZoomInButtonOnMap();
		String storeName = storeLocatorPage.selectFirstStore();
		storeLocatorPage.verifySelectedStoreLocatorName(storeName);
	}
	
	
	
	/**
	 * US616212: SL search result - sorting - no SRC found message
	 * TC319635
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.PENDING, Group.TMNG })
	public void testsearchResultSortingNoSRCFoundMessage(ControlTestData data, TMNGData tMNGData) {
		
		Reporter.log("US616212: SL search result - sorting - no SRC found message" );
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to Store locator page | Store locator page should be displayed");
		Reporter.log("Step 2: Enter text into search box | Entering value in the search text box should be success");
		Reporter.log("Step 3: Click on search icon | Click on search icon should be success");
		Reporter.log("Step 4: Click on sort drop down | Click on sort drop down should be success");
		Reporter.log("Step 6: Select product repair center menu from sort dropdown | Selecting product repair center menu from sort dropdown should be success");
		Reporter.log("Step 7: Verify 'no product repair center text' | 'No product repair center' text should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		StoreLocatorPage storeLocatorPage = navigateToStoreLocatorPage(tMNGData);
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.clickOnSortDropDown();
		storeLocatorPage.selectProductRepairCenterFromDropDown();
		storeLocatorPage.verifyMessageForNoProductSprintRepairCentre();
		
	}
	
	
	
	/***
	 * US551522: DW3 - SL LP: search input field (default): 
	 * TC244933
	 * US602255: DW3 - SL LP: search input field (type ahead; execute; clear)
	 * TC326888
	 * US602257: DW3 - SL LP: search results - store blades
	 * TC327614
	 * US602302: DW3 - SL LP: search results - sort
	 * TC327615
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testSearchResultsStoreBladesAlongWithSorting(TMNGData tMNGData) { 
		Reporter.log("US551522 DW3 - SL LP: search input field (default)"); 
		Reporter.log("US602255 DW3 - SL LP: search input field (type ahead; execute; clear)"); 
		Reporter.log("US602257 DW3 - SL LP: search results - store blades"); 
		Reporter.log("US602302 DW3 - SL LP: search results - sort"); 
		Reporter.log("Data Condition | Any Query Parameter (City Name) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log("Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log("Step 4: Verify Search input hint text | Search input hint text should be displayed");
		Reporter.log("Step 5: Verify Search input hint text after focus | Search input hint text after focus should be displayed");
		Reporter.log("Step 6: Verify search icon | Search icon should be displayed");
		Reporter.log("Step 7: Enter City in Search Text Box | City as Search Query parameter is entered");
		Reporter.log("Step 8: Verify search icon enabled | Search icon should be enabled, once entered a value in saerch text box");
		Reporter.log("Step 9: Click on Close button | Clicked on Close button");
		Reporter.log("Step 10: Enter City in Search Text Box | City as Search Query parameter is entered");
		Reporter.log("Step 11: Click on search icon | Clicked on Search icon button");
		Reporter.log("Step 12: Verify City Search result section is loading | City Search result section loaded successfully");
		Reporter.log("Step 13: Verify FPR blades in loaded search results | FPR search result blades are displayed successfully in search results");
		Reporter.log("Step 14: Verify all the elements under FPR store blade | Successfully verified Store name data, Distance of store, Store address data, Today's hours, In-Store wait, Appointments available data for FPR search results");
		Reporter.log("Step 15: Verify TPR blades in loaded search results | TPR search result blades are displayed successfully in search results, with tag Third Party Retailers");
		Reporter.log("Step 16: Verify all the elements under TPR store blade | Successfully verified Store name data, Distance of store, Store address data, Today's hours, In-Store wait, Appointments available data for TPR search results");
		Reporter.log("Step 17: Verify Authorized dealer blades in loaded search results | Authorized dealer search result blades are displayed successfully in search results");
		Reporter.log("Step 18: Verify all the elements under Authorized dealer store blade | Successfully verified Store name data, Distance of store, Store address data, Today's hours(if exists) data for Authorized dealer search results");
		Reporter.log("Step 19: Click on drop down menu |  Clicked on drop down menu successfully");
		Reporter.log("Step 20: Select distance from drop down menu | Selected distance from drop down menu successfully");
		Reporter.log("Step 21: Verify the search results sorting order | Search results are sorted by distance");
		Reporter.log("Step 22: Click on drop down menu |  Clicked on drop down menu successfully");
		Reporter.log("Step 23: Select wait time from drop down menu | Selected wait time from drop down menu successfully");
		Reporter.log("Step 24: Verify the search results sorting order | Search results are sorted by wait time");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchInputHintText();
		storeLocatorPage.verifySearchInputTextAfterFocus();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.verifySearchIconIsEnabled();
		storeLocatorPage.clickOnCloseButton();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();	
		storeLocatorPage.verifyFPRSearchResultBlades();
		storeLocatorPage.verifyTPRSearchResultBlades();
		storeLocatorPage.verifyAuthorizedDealerSearchResultBlades();	
		storeLocatorPage.clickOnSortDropDown();
		storeLocatorPage.selectDistanceFromDropDown();
		storeLocatorPage.verifySearchResultSortedByDistance();	
		storeLocatorPage.clickOnSortDropDown();
		storeLocatorPage.selectWaitTimeFromDropDown();
		storeLocatorPage.verifySearchResultSortedByWaitTime();
	}
	
	/***
	 * US610486 DW3 - SL LP: search results - pagination
	 * TA2031438
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyUserCanSeePaginationAndCanNavigateForwardAndBackwardOnStoreLocatorSearchResultsPage(TMNGData tMNGData) {
		Reporter.log("US610486 DW3 - SL LP: search results - pagination");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com Store locator page | T-Mobile store locator page is displayed");
		Reporter.log("Step 2: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log("Step 3: Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
	    Reporter.log("Step 4: Verify city Search result section is loading | City Search result section loaded successfully");
		Reporter.log("Step 5: Verify search result list store locators | Search result section should load stores with closest distance");
		Reporter.log("Step 6: Verify pagination is displayed in proper format | Pagination is displayed in proper format");
		Reporter.log("Step 7: Verify previous page button is not displayed | Previous page button is not displayed ");
		Reporter.log("Step 8: Verify next page button is displayed | Next page button is displayed ");
		Reporter.log("Step 9: click on next button and verify previous page button is displayed | Next page button is clicked and previous page button is displayed ");		
		Reporter.log("=================================");
		Reporter.log("Actual Output:");
		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyPaginationFormatDisplayedCorrectly();
		storeLocatorPage.verifyPreviousStoresPageButtonIsDisabledOrNot("Disabled");
		storeLocatorPage.verifyNextStoresPageButtonIsDisplayedOrNot("Enabled");
		storeLocatorPage.goToNextStoresPage();
		storeLocatorPage.verifyPreviousStoresPageButtonIsDisabledOrNot("Enabled");
	}
	
	/***
	 * US610486 DW3 - SL LP: search results - pagination
	 * TA2031438
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyUserCanSeePaginationAndCannotNavigateForwardAndBackwardOnStoreLocatorSearchResultsPageWithLessThan10Records(TMNGData tMNGData) {
		Reporter.log("US610486 DW3 - SL LP: search results - pagination");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com Store locator page | T-Mobile store locator page is displayed");
		Reporter.log("Step 2: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log("Step 3: Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log("Step 4: Verify city Search result section is loading | City Search result section loaded successfully");
		Reporter.log("Step 5: Verify search result list store locators | Search result section should load stores with closest distance");
		Reporter.log("Step 6: Verify pagination is displayed in proper format | Pagination is displayed in proper format");
		Reporter.log("Step 7: Verify previous page button is not displayed | Previous page button is not displayed ");
		Reporter.log("Step 8: Verify next page button is not displayed | Next page button is not displayed ");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");
		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyPaginationFormatDisplayedCorrectly();
		storeLocatorPage.verifyPreviousStoresPageButtonIsDisabledOrNot("Disabled");
		storeLocatorPage.verifyNextStoresPageButtonIsDisplayedOrNot("Disabled");
	}
	
	/***
	 * CDCDWR2-444 Defects- Mustangs - S21 - EP-18480 - Prod: Store locator: Appointment: Error message for "Phone Number" field in appointment form is not showing up in some situation
	 * 
	 * @param tMNGData
	 *//*
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testVerifyPhoneNumberFieldValidationOnAppointmentFlow(TMNGData tMNGData) {
		Reporter.log("CDCDWR2-444 Defects- Mustangs - S21 - EP-18480 - Prod: Store locator: Appointment: Error message for Phone Number field in appointment form is not showing up in some situation");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com storelocator page |  Store Locator page should be displayed");
		Reporter.log(
				"Step 2: Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 3: Verify City Search result section is loading | City Search result section loaded successfully");
		Reporter.log(
				"Step 4: Select city which have appoinments available | SDP page should be displayed");
		Reporter.log("Step 5: Click Appoinment CTA | Time and data selection table should be displayed");
		Reporter.log(
				"Step 6: Select available time and click Naxt CTA | Personal info table should be displayed");
		Reporter.log(
				"Step 7: Validate error messgae for phone number fileds with no data | Error message should be displayed for Phone number with empty data");
		Reporter.log(
				"Step 8: Enter valid phone number in phone number filed | Error message should not be displayed");
		Reporter.log(
				"Step 9: Clear the phone number field | Error message should be displayed again for phone number");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getZipcode());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.selectStoreWhichHasAppointments();
		storeLocatorPage.verifyStoreNameInStoreDetails();
		storeLocatorPage.clickAppointmentCTA();
		storeLocatorPage.verifyTimeAndDateTable();
		storeLocatorPage.selectTimeSlotforAppointment();
		storeLocatorPage.clickNextCTAOnTimeAndDateTable();
		storeLocatorPage.verifyPhoneNumberTextBox();
		storeLocatorPage.verifyErrorMessageForPhoneNumber();
		storeLocatorPage.setPhoneNumber(tMNGData);
		storeLocatorPage.verifyErrorMessageForPhoneNumberNotDisplayedForValidData();
		storeLocatorPage.verifyErrorMessageForPhoneNumber();
	}
	
	*//***
	 * CDCDWR2-719:SL UI | SLP | Get in line & Wait times 
	 * TC-1852:Validate sort order when using 'wait time' sort option
	 * @param tMNGData
	 *//*
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT})
	public void verifyStoreLocatorSortingByWaitTimes(TMNGData tMNGData) {

		Reporter.log("TC-1852:Validate sort order when using 'wait time' sort option");
		Reporter.log("================================");
		
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Navigate to store locator page | Store locator page should be displayed");
		Reporter.log("2. Verify search text box | Search text box on store locator page should be displayed");
		Reporter.log("3. Enter the city in search box  | city should be entered successfully");
		Reporter.log("4. Cick on search box | clicked search box successfully");
		Reporter.log("5. Click on drop down menu |  clicked on drop down menu successfully");
		Reporter.log("6. Select wait time from drop down menu | selected wait time from drop down menu successfully");
		Reporter.log("7. verify the search results | search results are in sorted order");
		
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.clickOnSortDropDown();
		storeLocatorPage.selectWaitTimeFromDropDown();
		storeLocatorPage.verifySearchResultSortedByWaitTime();
	}*/
	
	/***
	 * US617030 GIL | Name validation and text wrap
	 * TC319576
	 * 
	 * @param tMNGData
	 *//*
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TMNG, Group.NON_MOBILE })
	public void verifyNameValidationInGetInLineFormAndItsSuccessModal(TMNGData tMNGData) {
		Reporter.log("US617030: GIL | Name validation and text wrap");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log("Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log("Step 4: Verify search icon and enter Zip Code (which doesnot have Sprint repair centers ) in Search Text Box and click search icon | Zip Code as Search Query parameter is entered and clicked on search icon");
		Reporter.log("Step 5: Verify Zip Code Search result section is loading | Zip Code Search result section loaded successfully");
		Reporter.log("Step 6: Verify search result list store locators | Search result section should load stores with closest distance");
		Reporter.log("Step 7: Click on any TMO store from the store blades | store details page should be displayed");
		Reporter.log("Step 8: Click on Get In Line link | Get In Line form should be displayed");
		Reporter.log("Step 9: Enter more than 40 characters in first name field | First name first should not allow more than 40 characters and it should not display any characters after 40 characters");
		Reporter.log("Step 10: Enter more than 40 characters in Last Name field | Last Name first should not allow more than 40 characters and it should not display any characters after 40 characters");
		Reporter.log("Step 12: Select Reason for your visit | Selected Reason for your visit in Get in line form");
		Reporter.log("Step 13: Verify by default Notify checkbox is unchecked | Notify checkbox is unchecked by default");
		Reporter.log("Step 14: User language preference is English| Successfully changed user language preference.");
		Reporter.log("Step 15: User click on 'Get in line' CTA | 'Get in line' CTA was highlighted and clicked successfully.");
		Reporter.log("Step 16: Verify 'Get in line' success modal with its header | 'Get in line' success modal should be displayed successfully.");
		Reporter.log("Step 17: Verify the maximum length of the name in Get in line success modal | Verified successfully the length of the Name displayed in Success modal");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");
		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.selectSecondStoreInSearchResults();
		storeLocatorPage.verifyAndclickOnGetInLineCTAInStoreDetails();
		storeLocatorPage.enterFirstNameInGetInLine(tMNGData);
		storeLocatorPage.enterLastNameInGetInLine(tMNGData);
		String firstName = storeLocatorPage.retrieveFirstName();
		storeLocatorPage.verifyFirstNameMaxLimit();
		storeLocatorPage.verifyLastNameMaxLimit();	
		storeLocatorPage.selectReasonForYourVisit();
		storeLocatorPage.verifyNotifyCheckboxDefaultState();
		storeLocatorPage.clickOnEnglishRadioButton();
		storeLocatorPage.verifyAndclickOnGetInLineCTA();
		storeLocatorPage.verifyGetInLineSuccessModal(firstName);
		storeLocatorPage.verifyFirstNameLengthInSuccessModal();
	}*/
	
	/**
	 * US614863: SDP | Add Sprint Repair Center blurb
	 * TC319661
	 * 
	 * @param data
	 * @param tMNGData
	 *//*
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.TMNG })
	public void testAddSprintRepairCenterBlurb(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("US614863: SDP | Add Sprint Repair Center blurb" );
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log("Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 4: Enter City name in search box | Value Entered in the text box successfully");
		Reporter.log("Step 5: click on search icon |  clicked on search icon successfully");
		Reporter.log("Step 6: Verify search results | Search result section loaded successfully");
		Reporter.log("Step 7: Click on a any non  product repair center store from the store blades | clicked PRC successfully");
		Reporter.log("Step 8: For store doesn't support Sprint related transactions: verify 'This store doesn’t currently support Sprint account management or transactions..' authorable text displayed in the second paragraph | Authorable text 'This store doesn’t currentlsupport Sprint account management or transactions.' should be displayed in the second paregraph");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifySprintRepairCenterBlurb();
		
	}*/
	
	/**
	 * US614864: SDP | Add no support for Sprint blurb
	 * TC319660
	 * 
	 * @param data
	 * @param tMNGData
	 *//*
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.TMNG })
	public void testAddNoSupportForSprintBlurb(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("US614864: SDP | Add no support for Sprint blurb" );
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log("Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 4: Enter City name in search box | Value Entered in the text box successfully");
		Reporter.log("Step 5: click on search icon |  clicked on search icon successfully");
		Reporter.log("Step 6: Verify search results | Search result section loaded successfully");
		Reporter.log("Step 7: Click on a any product repair center store from the store blades | clicked PRC successfully");
		Reporter.log("Step 8: For store doesn't support Sprint related transactions: verify 'This store doesn’t currently support Sprint account management or transactions..' authorable text displayed in the second paragraph | Authorable text 'This store doesn’t currentlsupport Sprint account management or transactions.' should be displayed in the second paregraph");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyAddNoSupportForSprintBlurb();
		
	}*/
	
	/***
	 * US422723 TMO - GLOBAL - Store types on Search Blade TC ID - TC245243
	 * 
	 * @param tMNGData
	 *//*
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testStoreTypesOnSearchBladeForState(TMNGData tMNGData) {
		Reporter.log("US422723 TMO - GLOBAL - Store types on Search Blade");
		Reporter.log("Data Condition | Any Query Parameter (State) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter State in Search Text Box and click search icon | State as  Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 5: Verify State Search result section is loading | State Search result section loaded successfully");
		Reporter.log(
				"Step 6: Click on the Store containing a TPR and verify text \"Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getState());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.clickOnCitySearchBlade(tMNGData.getCity());
		storeLocatorPage.verifyThirdPartyRetailersTagInCitySearchResultBlades();
	}*/

	
}