package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.JumpCartPage;
import com.tmobile.eservices.qa.pages.shop.JumpConditionPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionValuePage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class JumpCartPageTest extends ShopCommonLib {

	/**
	 * US354607 MyTMO - AT - JUMP Flow Cart: Pull out FRP from Legal Text and
	 * Display as Standalone Fields
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testFRPfromLegalTextandDisplayasStandaloneFieldsJumpFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US354607: MyTMO - AT - JUMP Flow Cart: Pull out FRP from Legal Text and Display as Standalone Fields");
		Reporter.log("Data Condition | Jump Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on a non promo device to buy with FRP | Product details page should be displayed");
		Reporter.log("6. Click on Add To Cart CTA| Line Selector page should be displayed");
		Reporter.log("7. Click On The Jump Line |Device Protection Page should be displayed ");
		Reporter.log("8. Click on Continue| Jump Condition Page should be displayed");
		Reporter.log("9. Choose Good Options | Jump Value Page should be displayed");
		Reporter.log("10. Click Continue | Jump Cart Page is Displayed");
		Reporter.log(
				"11. verify Full Retail Price label for the product  separated from the legal text| Full Retail Price label for the product should be separated from the legal text");
	
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyPDPPage();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyJumpProtection()) {
			deviceProtectionPage.clickContinueButton();
		}
		JumpConditionPage newJumpConditionPage = new JumpConditionPage(getDriver());
		newJumpConditionPage.verifyNewJumpConditionPage();
		newJumpConditionPage.selectGoodComponents();
		newJumpConditionPage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		// deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		deviceConditionsPage.clickAcceptAndContinueButton();
		JumpCartPage jumpCartPage = new JumpCartPage(getDriver());
		jumpCartPage.verifyJumpCartPage();
		jumpCartPage.verifyFRPseperatedfromLegalText();
		jumpCartPage.verifyFullRetailPrice();
	}

}
