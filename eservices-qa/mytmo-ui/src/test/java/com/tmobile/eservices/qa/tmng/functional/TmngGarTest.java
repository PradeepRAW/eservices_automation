package com.tmobile.eservices.qa.tmng.functional;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.AssertJUnit;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TmngGarTest {

	static List<String> miniPdpURLs;
	static List<String> colourSwatchespageURLs;
	static List<String> imageURLs;
	static HashMap<String, List<String>> tmoCacheResult;
	static JavascriptExecutor javaScript;

	static WebDriver driver;
	// applicationUrl=https://qat.digital.t-mobile.com/cell-phones
	static String applicationUrl = System.getProperty("environment");

	public static List<String> getPagesWithCDNForColourSwatches() {
		colourSwatchespageURLs = new ArrayList<String>();
		List<WebElement> colourswatchesURL = driver.findElements(By.xpath("//img[contains(@alt,'color')]"));
		for (WebElement url : colourswatchesURL) {
			colourSwatchespageURLs.add(url.getAttribute("src"));
		}
		return colourSwatchespageURLs;

	}

	/**
	 * It will return the list of colour swatches pointed to CDN url
	 * 
	 * @return miniPdpURLs
	 */
	public static List<String> getPagesWithCDNForMiniPdp() {
		miniPdpURLs = new ArrayList<String>();
		List<WebElement> colourswatchesURL = driver.findElements(By.xpath("//img[contains(@class,'color')]"));
		for (WebElement url : colourswatchesURL) {
			miniPdpURLs.add(url.getAttribute("src"));
		}
		return miniPdpURLs;

	}

	/**
	 * 
	 * This will instantiate the driver and integarte with the sauce labs
	 * 
	 * @param local
	 */
	@SuppressWarnings("deprecation")
	public static void instantiateDriver(boolean local) {
		if (local) {
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\ntalakokkula\\Desktop\\driver\\chromedriver.exe");
			DesiredCapabilities caps = DesiredCapabilities.chrome();
			LoggingPreferences logPrefs = new LoggingPreferences();
			logPrefs.enable(LogType.PERFORMANCE, Level.INFO);
			caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
			driver = new ChromeDriver(caps);

		} else {
			try {
				LoggingPreferences logPrefs = new LoggingPreferences();
				logPrefs.enable(LogType.PERFORMANCE, Level.INFO);

				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setBrowserName("chrome");
				capabilities.setCapability("platform", "Windows 10");
				capabilities.setCapability("version", "73.0");
				capabilities.setCapability("name", "GAR_Code_Validation");
				capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
				capabilities.setCapability("parentTunnel", "CTFrameworkPlatform");
				capabilities.setCapability("tunnelIdentifier", "CTFrameworkPlatform");
				driver = new RemoteWebDriver(new URL(
						"http://tmo_eServices:a138d0ba-09c7-4a2b-9565-67e15a5887c3@ondemand.saucelabs.com:80/wd/hub"),
						capabilities);
				SessionId sessionId = ((RemoteWebDriver) driver).getSessionId();
				Reporter.log("Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/"
						+ sessionId + "\">SauceLabs</a>");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Test
	public void runGARTestWithCDNForAccessories() {
		try {
			tmoCacheResult = new HashMap<String, List<String>>();
			instantiateDriver(false);
			driver.get(applicationUrl.concat("/cell-phones"));
			Thread.sleep(10000);
			checkPageIsReady();
			driver.findElement(By.xpath("//span[contains(text(),'Tablets')]")).click();
			Thread.sleep(5000);
			checkPageIsReady();
			driver.findElement(By.xpath("(//img[contains(@class,'img')])[1]")).click();
			Thread.sleep(5000);
			checkPageIsReady();
			tmoCacheResult.put(applicationUrl, getPagesWithCDNForColourSwatches());

			verifyCdnResponse();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			driver.close();
		}
	}

	@Test
	public void runGARTestWithCDNForAccessoriesThroughCartPage() {
		try {
			tmoCacheResult = new HashMap<String, List<String>>();
			instantiateDriver(false);
			driver.get(applicationUrl.concat("/cart"));
			Thread.sleep(10000);
			checkPageIsReady();
			driver.findElement(By.xpath("//span[contains(text(),'Add an accessory')]")).click();
			Thread.sleep(5000);
			checkPageIsReady();
			/*
			 * driver.findElement(By.xpath("//button[contains(text(),'Done')]")).click();
			 * Thread.sleep(5000); checkPageIsReady();
			 */
			driver.findElement(By.xpath("(//img[contains(@alt,'image')])[2]")).click();
			Thread.sleep(5000);
			checkPageIsReady();
			tmoCacheResult.put(applicationUrl, getPagesWithCDNForMiniPdp());

			verifyCdnResponse();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			driver.close();
		}
	}

	@Test
	public void runGARTestWithCDNForMiniPdp() {
		try {
			tmoCacheResult = new HashMap<String, List<String>>();
			instantiateDriver(false);
			driver.get(applicationUrl.concat("/cart"));
			Thread.sleep(10000);
			checkPageIsReady();
			driver.findElement(By.xpath("//span[contains(text(),'Add a phone')]")).click();
			Thread.sleep(5000);
			checkPageIsReady();
			driver.findElement(By.xpath("//button[contains(text(),'Done')]")).click();
			Thread.sleep(5000);
			checkPageIsReady();
			driver.findElement(By.xpath("(//img[contains(@alt,'DeviceImage')])[1]")).click();
			Thread.sleep(5000);
			checkPageIsReady();
			tmoCacheResult.put(applicationUrl, getPagesWithCDNForMiniPdp());

			verifyCdnResponse();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			driver.close();
		}
	}

	@Test
	public void runGARTestWithCDNForColourSwatches() {
		try {
			tmoCacheResult = new HashMap<String, List<String>>();
			instantiateDriver(false);
			driver.get(applicationUrl.concat("/cell-phones"));
			Thread.sleep(15000);
			checkPageIsReady();
			driver.findElement(By.xpath("(//img[contains(@alt,'Image')])[1]")).click();
			Thread.sleep(5000);
			checkPageIsReady();

			tmoCacheResult.put(applicationUrl, getPagesWithCDNForColourSwatches());

			verifyCdnResponse();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			driver.close();
		}
	}

	private void verifyCdnResponse() {
		int imageNotFoundCounter = 0;
		List<String> imageNotFoundList = new ArrayList<String>();
		for (Map.Entry<String, List<String>> entry : tmoCacheResult.entrySet()) {
			System.out.println("url is " + entry.getKey() + " and its associated images are " + entry.getValue());
			String page = entry.getKey().replace(applicationUrl, "");
			if (!entry.getValue().isEmpty()) {
				Reporter.log("The page " + "'" + page + "'" + " has all images pointed to cdn.tmobile.com");
				Reporter.log("");
				for (String imageUrl : entry.getValue()) {
					try {
						if (!imageUrl.isEmpty()) {
							URL url = new URL(imageUrl.replace("\"", ""));
							HttpURLConnection connection = (HttpURLConnection) url.openConnection();
							connection.setRequestMethod("GET");
							connection.connect();
							int statusCode = connection.getResponseCode();
							if (statusCode == 200) {
								Reporter.log("Image Url:  " + imageUrl + "  status code: " + statusCode + " - found");
							} else if (statusCode == 404) {
								imageNotFoundCounter++;
								imageNotFoundList.add(
										"Image Url:  " + imageUrl + "  status code: " + statusCode + " - not found");
							} else if (statusCode >= 500) {
								imageNotFoundCounter++;
								imageNotFoundList.add(
										"Image Url:  " + imageUrl + "  status code: " + statusCode + " - not found");
							}
						}
					} catch (Exception e) {

					}
				}
				Reporter.log("");

			} else {
				Reporter.log("The page " + "'" + page + "'" + " has no images pointed to cdn.tmobile.com");
			}
		}
		if (imageNotFoundList.size() > 0) {
			Reporter.log(imageNotFoundCounter + " images have  error");
			for (String image : imageNotFoundList) {

				Reporter.log(image);
			}
			AssertJUnit.fail(imageNotFoundCounter + " images have  error");

		}
	}

	public static void checkPageIsReady() {
		javaScript = (JavascriptExecutor) driver;
		try {
			for (int i = 0; i < 30; i++) {

				Thread.sleep(1000);
				if ("complete".equalsIgnoreCase(javaScript.executeScript("return document.readyState").toString())) {
					Thread.sleep(3000);
					break;
				}
			}
		} catch (Exception ex) {
			Reporter.log(driver.getCurrentUrl() + "Page is not loaded properly");
		}

	}

}
