/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.AddOnDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.LineDetailsPage;

/**
 * 
 * @author Sudheer Reddy Chilukuri
 *
 */
public class AddOnDetailsPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(AddOnDetailsPageTest.class);

	/*
	 * NavigatetoAddonPagefromPlanDetailsPae US419644 : Add-ons Detail Page:
	 * Navigating to Add-ons detail Page
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void navigatetoAddonPagefromPlanDetailsPae(ControlTestData data, MyTmoData myTmoData) {
		logger.info("NavigatetoAddonPagefromPlanDetailsPae");
		Reporter.log("Test Case : Navigate to Addon Page from Plan Details page");
		Reporter.log("Test Data : Any PAH/Standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log("5.Veify that SharedAddon Component | Shared Addon should be displayed on plandetailsPage ");
		Reporter.log(
				"6.Verify that SharedAddon Associated Line |SharedAddon AssociatedLine should be displayed on plandetailsPage ");
		Reporter.log("7.Click On SharedAddon Associated Line | Application should be navigated to Addon Page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToLineDetailsPage(myTmoData);

		AddOnDetailsPage addonDetailsPage = new AddOnDetailsPage(getDriver());
		addonDetailsPage.clickOnSharedAddonAssociatedline();
		addonDetailsPage.verifyAddonPage();
	}

	/*
	 * VerifySharedAddonPriceDetailsinAddonPage US419653 : Add-ons Detail: Display
	 * Price
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifySharedAddonPriceDetailsinAddonPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifySharedAddonPriceDetailsinAddonPage");
		Reporter.log("Test Case : Verify Shared Add on Price Details in AddonPage ");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log("5.Veify that SharedAddon Component | Shared Addon should be displayed on plandetailsPage ");
		Reporter.log(
				"6.Verify that SharedAddon AssociatedLine |SharedAddon AssociatedLine should be displayed on plandetailsPage ");
		Reporter.log("7.Click On SharedAddon AssociatedLine | Application should be navigated to Addon Page");
		// Reporter.log("8.Verify the Price for SharedAddon | Price should be
		// avialble for sharedAddon in Addon Page");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToLineDetailsPage(myTmoData);

		AddOnDetailsPage addonDetailsPage = new AddOnDetailsPage(getDriver());
		addonDetailsPage.clickOnSharedAddonAssociatedline();
		addonDetailsPage.verifyAddonPage();
		addonDetailsPage.verifySharedAddonPrice();
		// addonDetailsPage.verifyPlusTaxandFee();
	}

	/*
	 * VerifyActiveAddonPriceDetailsinAddonPage US419653 : Add-ons Detail: Display
	 * Price
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyActiveAddonPriceDetailsinAddonPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyActiveAddonPriceDetailsinAddonPage");
		Reporter.log("Test Case : Verify Active Add on Price Details in Addon Page");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Line | User should be navigated to LineDetails Page ");
		Reporter.log("5.Veify that ActiveAddon Component | ActiveAddon should be displayed on LineDetailsPage ");
		Reporter.log(
				"6.Verify that ActiveAddon AssociatedLine |ActiveAddon AssociatedLine should be displayed on LineDetailsPage ");
		Reporter.log("7.Click On Active AssociatedLine | Application should be navigated to Addon Page");
		Reporter.log("8.Verify the Price for ActiveAddon | Price should be avialble for ActiveAddon in Addon Page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToLineDetailsPage(myTmoData);
		AddOnDetailsPage addonDetailsPage = new AddOnDetailsPage(getDriver());

		addonDetailsPage.verifyActiveAddonComponent();
		addonDetailsPage.clickOnActiveAddonAssociatedline();
		addonDetailsPage.verifyAddonPage();
		addonDetailsPage.verifySharedAddonPrice();
		// addonDetailsPage.verifyPlusTaxandFee();
	}

	/*
	 * VerifyDiscountandDescriptionforSharedAddon US419656 : Add-ons: Text
	 * Description for Add-ons US429218 : Add-on Details Page: Applied Discounts
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyDiscountandDescriptionforSharedAddon(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyDiscountandDescriptionforSharedAddon");
		Reporter.log("Test Case : Verify Discount and Description for Shared Addon ");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log("5.Veify that SharedAddon Component | Shared Addon should be displayed on plandetailsPage ");
		Reporter.log(
				"6.Verify that SharedAddon AssociatedLine |SharedAddon AssociatedLine should be displayed on plandetailsPage ");
		Reporter.log("7.Click On SharedAddon AssociatedLine | Application should be navigated to Addon Page");
		Reporter.log(
				"8.Verify the Discount and Description for SharedAddon | Discount and Description should be avialble for sharedAddon in Addon Page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToLineDetailsPage(myTmoData);

		AddOnDetailsPage addonDetailsPage = new AddOnDetailsPage(getDriver());
		addonDetailsPage.verifyActiveAddonComponent();
		addonDetailsPage.clickOnActiveAddOnByName("Family Allowances");

		addonDetailsPage.verifyAddonPage();
		addonDetailsPage.verifyAddonDescription("Discount");
	}

	/*
	 * US429194 : Add-ons Detail Page: Manage Add-ons CTA US419647 : US419645 :
	 * Add-ons Detail Page: Back CTA to Previous Page US419644 : Add-ons Detail
	 * Page: Navigating to Add-ons detail Page
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyManageAddonCTAforActiveAddon(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyManageAddonCTAforActiveAddon");
		Reporter.log("Test Case : Verify Manage Addon CTA for Active Addon");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Line | User should be navigated to LineDetails Page ");
		Reporter.log("5.Veify that ActiveAddon Component | ActiveAddon should be displayed on LineDetailsPage ");
		Reporter.log(
				"6.Verify that ActiveAddon AssociatedLine |ActiveAddon AssociatedLine should be displayed on LineDetailsPage ");
		Reporter.log("7.Click On Active AssociatedLine | Application should be navigated to Addon Page");
		Reporter.log(
				"8.Verify the Manage CTA for SharedAddon | Manage CTA should be avialble for sharedAddon in Addon Page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		AddOnDetailsPage addonDetailsPage = new AddOnDetailsPage(getDriver());

		addonDetailsPage.verifyActiveAddonComponent();
		addonDetailsPage.clickOnActiveAddonAssociatedline();
		addonDetailsPage.verifyAddonPage();
		addonDetailsPage.verifyCTAName("Manage");
		addonDetailsPage.verifyAddonHeading();
		// addonDetailsPage.verifySharedAddonName();
		addonDetailsPage.clickOnBackCTA();

		linedetailspage.verifyLineDetailsPage();
	}

	/*
	 * US419658 : [ACM] Acct Overview: Add Ons Detail Page: Data Plan CTA logic
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyDataPlanCTALogiconAddonPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyDataPlanCTALogiconAddonPage");
		Reporter.log("Test Case : Verify Manage Add on CTA for Active Addon");
		Reporter.log("Test Data : Any PAH/standard customer with Data Plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Line | User should be navigated to LineDetails Page ");
		Reporter.log("5.Veify that ActiveAddon Component | ActiveAddon should be displayed on LineDetailsPage ");
		Reporter.log(
				"6.Verify that ActiveAddon AssociatedLine |ActiveAddon AssociatedLine should be displayed on LineDetailsPage ");
		Reporter.log("7.Click On Active AssociatedLine | Application should be navigated to Addon Page");
		Reporter.log("8.Verify the Manage CTA in addonDetailsPage | Manage CTA should be avialble in addonDetailsPage");
		Reporter.log("9.Verify the Return CTA in addonDetailsPage | Return CTA should be avialble in addonDetailsPage");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);

		AddOnDetailsPage addonDetailsPage = new AddOnDetailsPage(getDriver());
		addonDetailsPage.verifyActiveAddonComponent();
		addonDetailsPage.verifyActiveAddonFeature();

		linedetailspage.verifyLineDetailsPage();
	}
}