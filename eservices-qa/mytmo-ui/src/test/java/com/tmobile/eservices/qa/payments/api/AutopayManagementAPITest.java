package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AutopayManagementApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;


public class AutopayManagementAPITest extends AutopayManagementApi {

	public Map<String, String> tokenMap;
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AutopayManagement","autopaysetup",Group.SHOP,Group.SPRINT })
	public void testAutopaySetup(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: setup autopay");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Autopay should setup for account and return the same in service response");
		Reporter.log("Step 2: Verify expected autopay should be set for the account and returned in response");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 6: Verify autopay setup created.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile("autoPaySetup.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("cardNumber", apiTestData.getCardNumber());
		tokenMap.put("cardAlias", apiTestData.getCardAlias());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		System.out.println("updatedRequest=   "+updatedRequest);
		Response response =autopay_Setup(apiTestData, updatedRequest);
		System.out.println("response=   "+response);
		Assert.assertTrue(response.statusCode()==200);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AutopayManagement","deleteAutoPay",Group.SHOP,Group.SPRINT })
	public void testDeleteAutoPay(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: setup autopay");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Autopay should setup for account and return the same in service response");
		Reporter.log("Step 2: Verify expected autopay should be set for the account and returned in response");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 6: Verify autopay setup created.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile("deleteAutoPay.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("cardNumber", apiTestData.getCardNumber());
		tokenMap.put("cardAlias", apiTestData.getCardAlias());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		System.out.println("updatedRequest=   "+updatedRequest);
		Response response =deleteAutopay(apiTestData, updatedRequest);
		Assert.assertTrue(response.statusCode()==200);
		System.out.println("response=   "+response);
	}
	
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AutopayManagement","autopaysetup",Group.SHOP,Group.SPRINT })
	public void testAutopaySetupCreditCard(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: setup autopay Credit card");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Autopay should setup for account and return the same in service response");
		Reporter.log("Step 2: Verify expected autopay should be set for the account and returned in response");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 6: Verify autopay setup created.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile("autoPaySetup.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("cardNumber", apiTestData.getCardNumber());
		tokenMap.put("expirationMonthYear", apiTestData.getExpiryMonth());
		tokenMap.put("cvv", apiTestData.getCVV());
		tokenMap.put("zip", apiTestData.getZipCode());
		
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		System.out.println("updatedRequest#######=   "+updatedRequest);
		Response response =setAutopay(apiTestData, updatedRequest);
		System.out.println("response#########=   "+response);
		Assert.assertTrue(response.statusCode()==200);
	}
	
	/**
     * UserStory# autoPaySearch:
     *
     * @param data
     * @param apiTestData
     * @throws Exception
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = { "AutopayManagement", "autoPaySearch", Group.SHOP,Group.SPRINT })
    public void testAutoPaySearch(ControlTestData data, ApiTestData apiTestData) throws Exception {
        Reporter.log("TestName: Auto Pay Search");
        Reporter.log("Data Conditions:MyTmo registered misdn.");
        Reporter.log("================================");

        String requestBody = new ServiceTest().getRequestFromFile("autoPaySearch.txt");
        tokenMap = new HashMap<String, String>();
        tokenMap.put("ban", apiTestData.getBan());
        String updatedRequest = prepareRequestParam(requestBody, tokenMap);
        Response response = autopaySearch(apiTestData, updatedRequest);
        Assert.assertTrue(response.statusCode() == 200);
        System.out.println("response=   " + response.toString());
    }

}
