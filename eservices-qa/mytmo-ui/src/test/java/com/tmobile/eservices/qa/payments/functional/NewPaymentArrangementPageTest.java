/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import java.io.IOException;
import java.util.Map;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.NewAddBankPage;
import com.tmobile.eservices.qa.pages.payments.NewAddCardPage;
import com.tmobile.eservices.qa.pages.payments.NewPaymentArrangementPage;
import com.tmobile.eservices.qa.pages.payments.PAConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.PaymentCollectionPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author speddis
 *
 */
public class NewPaymentArrangementPageTest extends PaymentCommonLib {

	Map<String, String> cardinfo;
	Map<String, String> bankinfo;

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PA_Angular7" })
	public void testPaymentArrangementSetupNewCard(ControlTestData data, MyTmoData myTmoData) throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log(
				"Step 3: Click on 'Setup Payment arrangement' link | Payment Arrangement page should be displayed");
		Reporter.log("Step 4: Click on Payment method blade | Payment Collection page should be displayed");
		Reporter.log("Step 5: Click on Add a card blade | Add Card page should be displayed");
		Reporter.log(
				"Step 6: Enter card details and click continue | Should validate card details and PA page should be displayed");
		Reporter.log("Step 7: Verify Selected Payment method on PA page | Selected Card number should be displayed");
		Reporter.log("Step 8: Verify Agree and Submit CTA | Agree and Submit CTA should be enabled");
		Reporter.log("Step 9: Click Agree and Submit CTA | PA Confirmation page should be enabled");
		Reporter.log("Step 10: Verify PA Setup Confirmation header | PA Setup Confirmation Header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		cardinfo = getCardInfo("master");

		NewPaymentArrangementPage paymentArrangementPage = navigateToNewPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickPaymentMethodBlade();
		PaymentCollectionPage paymentCollectionPage = new PaymentCollectionPage(getDriver());
		paymentCollectionPage.verifyPageLoaded();
		paymentCollectionPage.clickAddCard();
		NewAddCardPage cardPage = new NewAddCardPage(getDriver());
		cardPage.verifyCardPageLoaded();
		cardPage.addCardInformation(cardinfo);
		paymentArrangementPage.verifyPageLoaded();
		String cardNum = cardinfo.get("cardnumber");
		paymentArrangementPage.verifySelectedPaymentMethodOnBlade(cardNum.substring(cardNum.length() - 4));
		paymentArrangementPage.verifyAgreeAndSubmitCTAisActive();
		paymentArrangementPage.clickAgreeAndSubmitCTA();
		PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
		paConfirmationPage.verifyPageLoaded();
		paConfirmationPage.verifyPAConfirmationPageHeader(PaymentConstants.PA_CONFIRM_PAGE_SETUP);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PA_Angular7" })
	public void testPaymentArrangementSetupNewBank(ControlTestData data, MyTmoData myTmoData) throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log(
				"Step 3: Click on 'Setup Payment arrangement' link | Payment Arrangement page should be displayed");
		Reporter.log("Step 4: Click on Payment method blade | Payment Collection page should be displayed");
		Reporter.log("Step 5: Click on Add a card blade | Add Card page should be displayed");
		Reporter.log(
				"Step 6: Enter Bank details and click continue | Should validate card details and PA page should be displayed");
		Reporter.log("Step 7: Verify Selected Payment method on PA page | Selected bank number should be displayed");
		Reporter.log("Step 8: Verify Agree and Submit CTA | Agree and Submit CTA should be enabled");
		Reporter.log("Step 9: Click Agree and Submit CTA | PA Confirmation page should be enabled");
		Reporter.log("Step 10: Verify PA Setup Confirmation header | PA Setup Confirmation Header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		NewPaymentArrangementPage paymentArrangementPage = navigateToNewPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickPaymentMethodBlade();
		PaymentCollectionPage paymentCollectionPage = new PaymentCollectionPage(getDriver());
		paymentCollectionPage.verifyPageLoaded();
		paymentCollectionPage.clickAddBank();
		NewAddBankPage bankPage = new NewAddBankPage(getDriver());
		bankPage.verifyBankPageLoaded();
		Map<String, String> bankinfo = getBankInfo("bank1");
		String bankNum = bankinfo.get("banknumber");
		paymentArrangementPage.verifySelectedPaymentMethodOnBlade(bankNum.substring(bankNum.length() - 4));
		paymentArrangementPage.clickAgreeAndSubmitCTA();
		PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
		paConfirmationPage.verifyPageLoaded();
		paConfirmationPage.verifyPAConfirmationPageHeader(PaymentConstants.PA_CONFIRM_PAGE_SETUP);

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PA_Angular7" })
	public void testPaymentArrangementSetupNoneProvided(ControlTestData data, MyTmoData myTmoData) throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log(
				"Step 3: Click on 'Setup Payment arrangement' link | Payment Arrangement page should be displayed");
		Reporter.log("Step 4: Click on Payment method blade | Payment Collection page should be displayed");
		Reporter.log(
				"Step 5: Click on 'Choose payment method later' radio button and click 'Select payment method' | PA page should be displayed");
		Reporter.log("Step 7: Verify Selected Payment method on PA page | 'None Provided' should be displayed");
		Reporter.log("Step 8: Verify Agree and Submit CTA | Agree and Submit CTA should be enabled");
		Reporter.log("Step 9: Click Agree and Submit CTA | PA Confirmation page should be enabled");
		Reporter.log("Step 10: Verify PA Setup Confirmation header | PA Setup Confirmation Header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		NewPaymentArrangementPage paymentArrangementPage = navigateToNewPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickPaymentMethodBlade();
		PaymentCollectionPage paymentCollectionPage = new PaymentCollectionPage(getDriver());
		paymentCollectionPage.verifyPageLoaded();
		paymentCollectionPage.clickChoosePaymentMethodLater();
		paymentCollectionPage.clickSelectPaymentMethodCTA();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifySelectedPaymentMethodOnBlade("None Provided");
		paymentArrangementPage.verifyAgreeAndSubmitCTAisActive();
		paymentArrangementPage.clickAgreeAndSubmitCTA();
		PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
		paConfirmationPage.verifyPageLoaded();
		paConfirmationPage.verifyPAConfirmationPageHeader(PaymentConstants.PA_CONFIRM_PAGE_SETUP);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PA_Angular7" })
	public void testPaymentArrangementSetupWithSavedBank(ControlTestData data, MyTmoData myTmoData) throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log(
				"Step 3: Click on 'Setup Payment arrangement' link | Payment Arrangement page should be displayed");
		Reporter.log("Step 4: Click on Payment method blade | Payment Collection page should be displayed");
		Reporter.log("Step 5: Select Saved bank and click 'Select Payment method' CTA | PA page should be displayed");
		Reporter.log("Step 7: Verify Selected Payment method on PA page | Selected Bank number should be displayed");
		Reporter.log("Step 8: Verify Agree and Submit CTA | Agree and Submit CTA should be enabled");
		Reporter.log("Step 9: Click Agree and Submit CTA | PA Confirmation page should be enabled");
		Reporter.log("Step 10: Verify PA Setup Confirmation header | PA Setup Confirmation Header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		NewPaymentArrangementPage paymentArrangementPage = navigateToNewPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickPaymentMethodBlade();
		PaymentCollectionPage paymentCollectionPage = new PaymentCollectionPage(getDriver());
		paymentCollectionPage.verifyPageLoaded();
		String savedBank = paymentCollectionPage.clickStoredBankRadiobutton();
		paymentCollectionPage.clickSelectPaymentMethodCTA();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifySelectedPaymentMethodOnBlade(savedBank);
		paymentArrangementPage.verifyAgreeAndSubmitCTAisActive();
		paymentArrangementPage.clickAgreeAndSubmitCTA();
		PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
		paConfirmationPage.verifyPageLoaded();
		paConfirmationPage.verifyPAConfirmationPageHeader(PaymentConstants.PA_CONFIRM_PAGE_SETUP);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PA_Angular7" })
	public void testPaymentArrangementSetupWithSavedCard(ControlTestData data, MyTmoData myTmoData) throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log(
				"Step 3: Click on 'Setup Payment arrangement' link | Payment Arrangement page should be displayed");
		Reporter.log("Step 4: Click on Payment method blade | Payment Collection page should be displayed");
		Reporter.log("Step 5: Select Saved Card and click 'Select Payment method' CTA | PA page should be displayed");
		Reporter.log("Step 7: Verify Selected Payment method on PA page | Selected Card number should be displayed");
		Reporter.log("Step 8: Verify Agree and Submit CTA | Agree and Submit CTA should be enabled");
		Reporter.log("Step 9: Click Agree and Submit CTA | PA Confirmation page should be enabled");
		Reporter.log("Step 10: Verify PA Setup Confirmation header | PA Setup Confirmation Header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		NewPaymentArrangementPage paymentArrangementPage = navigateToNewPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickPaymentMethodBlade();
		PaymentCollectionPage paymentCollectionPage = new PaymentCollectionPage(getDriver());
		paymentCollectionPage.verifyPageLoaded();
		paymentCollectionPage.clickStoredCardRadiobutton();
		paymentCollectionPage.clickSelectPaymentMethodCTA();
		paymentArrangementPage.verifyPageLoaded();
		String cardNum = cardinfo.get("cardnumber");
		paymentArrangementPage.verifySelectedPaymentMethodOnBlade(cardNum.substring(cardNum.length() - 4));
		paymentArrangementPage.verifyAgreeAndSubmitCTAisActive();
		paymentArrangementPage.clickAgreeAndSubmitCTA();
		PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
		paConfirmationPage.verifyPageLoaded();
		paConfirmationPage.verifyPAConfirmationPageHeader(PaymentConstants.PA_CONFIRM_PAGE_SETUP);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PA_Angular7" })
	public void testPaymentArrangementModifyToUnsecure(ControlTestData data, MyTmoData myTmoData) throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on 'Make payment' link | One time payment page should be displayed");
		Reporter.log("Step 4: Click on 'Payment arrangement' link | Payment Arrangement page should be displayed");
		Reporter.log("Step 5: Click on Payment method blade | Payment Collection page should be displayed");
		Reporter.log("Step 6: Select Saved bank and click 'Select Payment method' CTA | PA page should be displayed");
		Reporter.log("Step 7: Verify Selected Payment method on PA page | Selected Bank number should be displayed");
		Reporter.log("Step 8: Verify Agree and Submit CTA | Agree and Submit CTA should be enabled");
		Reporter.log("Step 9: Click Agree and Submit CTA | PA Confirmation page should be enabled");
		Reporter.log("Step 10: Verify PA Setup Confirmation header | PA Setup Confirmation Header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		NewPaymentArrangementPage paymentArrangementPage = navigateToNewPAPageViaOTP(myTmoData);
		paymentArrangementPage.clickPaymentMethodBlade();
		PaymentCollectionPage paymentCollectionPage = new PaymentCollectionPage(getDriver());
		paymentCollectionPage.verifyPageLoaded();
		paymentCollectionPage.clickChoosePaymentMethodLater();
		paymentCollectionPage.clickSelectPaymentMethodCTA();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifySelectedPaymentMethodOnBlade("Remove Existing");
		paymentArrangementPage.verifyAgreeAndSubmitCTAisActive();
		paymentArrangementPage.clickAgreeAndSubmitCTA();
		PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
		paConfirmationPage.verifyPageLoaded();
		paConfirmationPage.verifyPAConfirmationPageHeader(PaymentConstants.MODIFY_PA_CONFIRM_PAGE_ALERT_PAUPDATED);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PA_Angular7" })
	public void testPaymentArrangementModifyToSecure(ControlTestData data, MyTmoData myTmoData) throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Bill link | Bill and Pay page should be displayed");
		Reporter.log("Step 4: Click on Payment arrangement shortcut | Payment arrangement page should be displayed");
		Reporter.log("Step 5: Click on Payment method blade | Payment Collection page should be displayed");
		Reporter.log(
				"Step 6: Verify & Select Saved bank and click 'Select Payment method' CTA | PA page should be displayed");
		Reporter.log(
				"Step 7: If there are no saved payment methods, then add a new Bank | PA page should be displayed");
		Reporter.log("Step 8: Verify Selected Payment method on PA page | Selected Bank number should be displayed");
		Reporter.log("Step 9: Verify Agree and Submit CTA | Agree and Submit CTA should be enabled");
		Reporter.log("Step 10: Click Agree and Submit CTA | PA Confirmation page should be displayed");
		Reporter.log("Step 11: Verify PA Setup Confirmation header | PA Setup Confirmation Header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		bankinfo = getBankInfo("bank1");

		NewPaymentArrangementPage paymentArrangementPage = navigateToNewPAPageViaBilling(myTmoData);
		paymentArrangementPage.clickPaymentMethodBlade();
		PaymentCollectionPage paymentCollectionPage = new PaymentCollectionPage(getDriver());
		paymentCollectionPage.verifyPageLoaded();
		if (!paymentCollectionPage.verifyAndSelectStoredPayment()) {
			paymentCollectionPage.clickAddBank();
			NewAddBankPage addBankPage = new NewAddBankPage(getDriver());
			addBankPage.verifyBankPageLoaded();
			addBankPage.addNewBankDetails(bankinfo);
			addBankPage.clickContinueCTA();
		}
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifySelectedPaymentMethodOnBlade("****");
		paymentArrangementPage.verifyAgreeAndSubmitCTAisActive();
		paymentArrangementPage.clickAgreeAndSubmitCTA();
		PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
		paConfirmationPage.verifyPageLoaded();
		paConfirmationPage.verifyPAConfirmationPageHeader(PaymentConstants.PA_CONFIRM_PAGE_SETUP);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PA_Angular7" })
	public void testPaymentArrangementModifyToSecureSavedCard(ControlTestData data, MyTmoData myTmoData)
			throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on 'Make payment' link | One Time Payment page should be displayed");
		Reporter.log("Step 4: Click on 'Payment arrangement' link | Payment Arrangement page should be displayed");
		Reporter.log("Step 5: Click on Payment method blade | Payment Collection page should be displayed");
		Reporter.log("Step 6: Select Saved Card and click 'Select Payment method' CTA | PA page should be displayed");
		Reporter.log("Step 7: Verify Selected Payment method on PA page | Selected Bank number should be displayed");
		Reporter.log("Step 8: Verify Agree and Submit CTA | Agree and Submit CTA should be enabled");
		Reporter.log("Step 9: Click Agree and Submit CTA | PA Confirmation page should be enabled");
		Reporter.log("Step 10: Verify PA Setup Confirmation header | PA Setup Confirmation Header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		NewPaymentArrangementPage paymentArrangementPage = navigateToNewPAPageViaBilling(myTmoData);
		paymentArrangementPage.clickPaymentMethodBlade();
		PaymentCollectionPage paymentCollectionPage = new PaymentCollectionPage(getDriver());
		paymentCollectionPage.verifyPageLoaded();
		paymentCollectionPage.clickStoredCardRadiobutton();
		paymentCollectionPage.clickSelectPaymentMethodCTA();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifySelectedPaymentMethodOnBlade("****");
		paymentArrangementPage.verifyAgreeAndSubmitCTAisActive();
		paymentArrangementPage.clickAgreeAndSubmitCTA();
		PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
		paConfirmationPage.verifyPageLoaded();
		paConfirmationPage.verifyPAConfirmationPageHeader(PaymentConstants.PA_CONFIRM_PAGE_SETUP);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PA_Angular7" })
	public void testPaymentArrangementModifyToSecureSavedBank(ControlTestData data, MyTmoData myTmoData)
			throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on 'Make payment' link | One Time Payment page should be displayed");
		Reporter.log("Step 4: Click on 'Payment arrangement' link | Payment Arrangement page should be displayed");
		Reporter.log("Step 5: Click on Payment method blade | Payment Collection page should be displayed");
		Reporter.log("Step 6: Select Saved Bank and click 'Select Payment method' CTA | PA page should be displayed");
		Reporter.log("Step 7: Verify Selected Payment method on PA page | Selected Bank number should be displayed");
		Reporter.log("Step 8: Verify Agree and Submit CTA | Agree and Submit CTA should be enabled");
		Reporter.log("Step 9: Click Agree and Submit CTA | PA Confirmation page should be enabled");
		Reporter.log("Step 10: Verify PA Setup Confirmation header | PA Setup Confirmation Header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		NewPaymentArrangementPage paymentArrangementPage = navigateToNewPAPageViaOTP(myTmoData);
		paymentArrangementPage.clickPaymentMethodBlade();
		PaymentCollectionPage paymentCollectionPage = new PaymentCollectionPage(getDriver());
		paymentCollectionPage.verifyPageLoaded();
		paymentCollectionPage.clickStoredBankRadiobutton();
		paymentCollectionPage.clickSelectPaymentMethodCTA();
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifySelectedPaymentMethodOnBlade("****");
		paymentArrangementPage.verifyAgreeAndSubmitCTAisActive();
		paymentArrangementPage.clickAgreeAndSubmitCTA();
		PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
		paConfirmationPage.verifyPageLoaded();
		paConfirmationPage.verifyPAConfirmationPageHeader(PaymentConstants.PA_CONFIRM_PAGE_SETUP);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PA_Angular7" })
	public void testNavigateToPAFromAccountHistoryScheduledActivity(ControlTestData data, MyTmoData myTmoData)
			throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on 'Account History' link | Account History page should be displayed");
		Reporter.log(
				"Step 4: Click on 'Add payment method' link in Scheduled Activity | Payment arrangement page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.clickAddpaymentmethodUnderScheduledActivity();
		NewPaymentArrangementPage paymentArrangementPage = new NewPaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PA_Angular7" })
	public void testNavigateToPAFromAccountHistoryAlerts(ControlTestData data, MyTmoData myTmoData) throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on 'Account History' link | Account History page should be displayed");
		Reporter.log(
				"Step 4: Click on 'Add payment method' link in Alerts | Payment arrangement page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.clickAddpaymentmethodUnderScheduledActivity();
		NewPaymentArrangementPage paymentArrangementPage = new NewPaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PA_Angular7" })
	public void testPayemntArrangementRecordsInAccountHistory(ControlTestData data, MyTmoData myTmoData)
			throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on 'Account History' link | Account History page should be displayed");
		Reporter.log(
				"Step 4: Verify Payment Arrangement Confirmation on Account history Payments category | Payment arrangement Confirmation record should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.selectCategoryDropdown("Payments");
		accountHistoryPage.verifyPAConfirmationInAccountHistoryActivity("Payment Arrangement Confirmation",
				"Documents");
	}

}