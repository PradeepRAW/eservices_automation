/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.MonthlyTotalBreakdownPage;

/**
 * @author prokarma
 *
 *         US519526 - Detailed review - New monthly total page: New monthly total amount calculation
 */

public class MonthlyTotalBreakdownPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(MonthlyTotalBreakdownPageTest.class);

	@Test(dataProvider = "byColumnName", enabled = true, groups = Group.RELEASE_READY)
	public void verifyHeaderAndMonthlyTotalOnDetailedReviewPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyHeaderAndMonthlyTotalOnDetailedReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Header, Plan name and monthly total on Detailed Review page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA | Plans Review Page Should be displayed");
		Reporter.log("6. Click on New Monthly Total Blade. | Detailed breakdown page should be displayed.");
		Reporter.log("7. Check  header on New Monthly breakdown page. | Header New monthly total charges should be displayed.");
		Reporter.log("8. Check plan name. | Plan name should be displayed.");
		Reporter.log("9. Check New Monthly total. | New Monthly Total should be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		navigateToPlansReviewPage(myTmoData);
		System.out.println("Review page opened");
		MonthlyTotalBreakdownPage monthlyTotalBreakdownPage = new MonthlyTotalBreakdownPage(getDriver());
		monthlyTotalBreakdownPage.verifyNewMonthlyTotalBreakdownPage();
		monthlyTotalBreakdownPage.checkHeaderOfNewMonthlyTotalBreakDownPage();
		monthlyTotalBreakdownPage.checkSubHeaderOfNewMonthlyTotalBreakDownPage();
		monthlyTotalBreakdownPage.checkLabelNewMonthlyTotal();
		
	}

	/**
	 * @author prokarma
	 *
	 * US544120 - Review page - Breakdown pages when MI line initiates plan change
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = Group.RELEASE_READY)
	public void checkAccountAndImpactedAndNonImpactedSectionWhenMILineFromHybridBanStartedChangePlanOnNewTotalBreakdownPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkAccountAndImpactedAndNonImpactedSectionWhenMILineFromHybridBanStartedChangePlanOnNewTotalBreakdownPage method called in ChangePlanPlansBreakdownPageTest");
		Reporter.log("Test Case : Check Account,Impacted and Non impacted section on New monthly total page when MI line from Hybrid BAN started change plan");
		Reporter.log("Test Data : Any MSDSIN PAH/Full on Hybrid TE plan and MI started change plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA | Plans Review Page Should be displayed");
		Reporter.log("6. Click on Plans blade. | Plans Breadkdown Should be displayed");
		Reporter.log("7. Check Account section. | Account section should be displayed with shared plan.");
		Reporter.log("8. Check Impacted section. | Impacted section should be displayed.");
		Reporter.log("9. Check Non Impacted section. | Non Impacted section should be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		navigateToPlansBreakdownPage(myTmoData);
		MonthlyTotalBreakdownPage monthlyTotalBreakdownPage = new MonthlyTotalBreakdownPage(getDriver());
		monthlyTotalBreakdownPage.verifyAccountSection();
		monthlyTotalBreakdownPage.verifyImpactedSection();
		monthlyTotalBreakdownPage.verifyNonImpactedSectionSection();
		//newMonthlyTotalBreakdownPage.verifyCostOfEachItemUnderAccountSectionAndCostAtAccountSection();
	}
	
	
	
	
}

