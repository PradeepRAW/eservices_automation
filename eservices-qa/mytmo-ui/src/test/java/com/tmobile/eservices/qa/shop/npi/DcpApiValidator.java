package com.tmobile.eservices.qa.shop.npi;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.eos.CatalogApiV4;
import com.tmobile.eservices.qa.api.eos.PromotionApiV2;
import com.tmobile.eservices.qa.common.ShopConstants;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class DcpApiValidator extends ApiCommonLib {

	public Map<String, String> tokenMap;

	String failureReason;
	Map<String, String> actualSKUPromoID;
	Map<String, Map<String, String>> familyPromoIDInfo;
	Map<String, Map<String, String>> familyPromoNameInfo;
	List<DCPSkuVadidatorData> expectedSKUDataList;
	List<DCPSkuVadidatorData> actualSKUDataList;
	List<DCPSkuVadidatorData> actualTradeInPromoDataList;
	Set<String> familyIdSet;
	Map<String, String> actualSKUPromoName;
	List<DCPSkuVadidatorData> expectedSKUAccessoryDataList;
	List<DCPSkuVadidatorData> actualSKUAccessoryDataList;
	Map<String, List<DCPSkuVadidatorData>> actualSKUAccessoryMap;

	//@BeforeTest(alwaysRun = true)
	public void PrepareExpectedValues() {

		familyIdSet = new HashSet<String>();
		familyPromoIDInfo = new HashMap<String, Map<String, String>>();
		familyPromoNameInfo = new HashMap<String, Map<String, String>>();
		expectedSKUDataList = new LinkedList<DCPSkuVadidatorData>();
		actualSKUDataList = new LinkedList<DCPSkuVadidatorData>();
		actualTradeInPromoDataList = new LinkedList<DCPSkuVadidatorData>();
		actualSKUPromoID = new HashMap<String, String>();
		actualSKUPromoName = new HashMap<String, String>();
		expectedSKUAccessoryDataList = new LinkedList<DCPSkuVadidatorData>();
		actualSKUAccessoryMap = new HashMap<String, List<DCPSkuVadidatorData>>();

		String excelFileName;
		excelFileName = System.getProperty("user.dir") + "/src/test/resources/npidata/dcpSKUValidatorData.xlsx";
		FileInputStream excelFile = null;
		Workbook workbook = null;
		try {
			excelFile = new FileInputStream(new File(excelFileName));
			if (excelFileName.contains("xlsx")) {
				workbook = new XSSFWorkbook(excelFile);
			} else {
				workbook = new HSSFWorkbook(excelFile);
			}
			Sheet dataTypeProductsSheet = workbook.getSheetAt(0);
			Sheet dataTypeAccessorySheet = workbook.getSheetAt(1);
			
			readFromProductsSheet(dataTypeProductsSheet);
			readFromAccessorySheet(dataTypeAccessorySheet);

		} catch (Exception e) {

		} finally {
			if (excelFile != null) {
				try {
					excelFile.close();
					workbook.close();
				} catch (IOException io) {
					Reporter.log("Failed to close dcpDataValidator.xlsx file");
				}
			}

		}
	}
	
	public void readFromProductsSheet(Sheet dataTypeSheet) {
		Iterator<Row> iterator = dataTypeSheet.iterator();

		while (iterator.hasNext()) {

			Row currentRow = iterator.next();

			//if (currentRow.getCell(0) != null && currentRow.getCell(1) != null) {
			if (null!=currentRow.getCell(0)) {
				
				String sku = getCurrRowCellVal(currentRow.getCell(0));
				String familyId = getCurrRowCellVal(currentRow.getCell(1));
				String modelName = getCurrRowCellVal(currentRow.getCell(2));
				String manufacturer = getCurrRowCellVal(currentRow.getCell(3));
				String memory = getCurrRowCellVal(currentRow.getCell(4));
				String color = getCurrRowCellVal(currentRow.getCell(5));
				String availability = getCurrRowCellVal(currentRow.getCell(6));
				String promoName = getCurrRowCellVal(currentRow.getCell(7));
				String promoCustFaceName = getCurrRowCellVal(currentRow.getCell(8));
				
				if (StringUtils.isNoneEmpty(sku)) {
					if (!sku.equalsIgnoreCase("SKU")) {

						DCPSkuVadidatorData dcpSkuValidator = new DCPSkuVadidatorData();
						dcpSkuValidator.setSkuNum(sku);
						dcpSkuValidator.setFamilyId(familyId);
						dcpSkuValidator.setManfactType(manufacturer);
						dcpSkuValidator.setModelName(modelName);
						dcpSkuValidator.setColor(color);
						dcpSkuValidator.setMemory(memory);
						dcpSkuValidator.setAvailableStatus(availability);
						dcpSkuValidator.setPromoName(promoName);
						dcpSkuValidator.setPromoCustomerFacingName(promoCustFaceName);
						expectedSKUDataList.add(dcpSkuValidator);
						familyIdSet.add(familyId);

					}
				}
			}
		}
		
	}
	
	public void readFromAccessorySheet(Sheet dataTypeSheet) {
		Iterator<Row> iterator = dataTypeSheet.iterator();

		while (iterator.hasNext()) {

			Row currentRow = iterator.next();

			//if (currentRow.getCell(0) != null && currentRow.getCell(1) != null) {
			if (null!=currentRow.getCell(0)) {
				
				String sku = getCurrRowCellVal(currentRow.getCell(0));
				String accessorySku = getCurrRowCellVal(currentRow.getCell(1));
				String familyId = getCurrRowCellVal(currentRow.getCell(2));
				String modelName = getCurrRowCellVal(currentRow.getCell(3));
				String manufacturer = getCurrRowCellVal(currentRow.getCell(4));
				String color = getCurrRowCellVal(currentRow.getCell(5));
				String availability = getCurrRowCellVal(currentRow.getCell(6));
				
				if (StringUtils.isNoneEmpty(sku)) {
					if (!sku.equalsIgnoreCase("SKU")) {

						DCPSkuVadidatorData dcpSkuValidator = new DCPSkuVadidatorData();
						dcpSkuValidator.setSkuNum(sku);
						dcpSkuValidator.setAccessorySkuNum(accessorySku);
						dcpSkuValidator.setFamilyId(familyId);
						dcpSkuValidator.setManfactType(manufacturer);
						dcpSkuValidator.setModelName(modelName);
						dcpSkuValidator.setColor(color);
						dcpSkuValidator.setAvailableStatus(availability);
						expectedSKUAccessoryDataList.add(dcpSkuValidator);						

					}
				}
			}
		}
		
	}

	public String getCurrRowCellVal(Cell cell){
		String cellVal="";
		if(null!=cell){
			cellVal = StringUtils.isNoneEmpty(cell.getStringCellValue())?cell.getStringCellValue():"";
		}else{
			cellVal="";
		}
		return cellVal.trim();
	}
	

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "npi" })
	public void prepareProductsResponse(ApiTestData apiTestData) throws Exception {

		String requestBody = null;
		Map<String, String> tokenMap = new HashMap<String, String>();
		Response response = null;
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("correlationid", getValForCorrelation());

		CatalogApiV4 catalogApi = new CatalogApiV4();
		requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_NPI + "productsCatalog_NPI.txt");

		response = catalogApi.retrieveProductsUsingPOST(apiTestData, requestBody, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			//logLargeResponse(response, "Product Catalog");
		} else {
			failAndLogResponse(response, "Product Catalog");
		}

		prepareActualList(response);

	}
	
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "npi" })
	public void prepareTradeinPromotionResponse(ApiTestData apiTestData) throws Exception {

		//Set<String> uniqueDeviceSkus = new HashSet<String>();
		ListIterator<DCPSkuVadidatorData> excpectedSkuDCPListIter = expectedSKUDataList.listIterator();
		StringBuffer reportStringBuffer = new StringBuffer("<Table><th>SKU</th><th>Expected Promo Name</th><th>Actual Promo Name</th><th>Expected Promo Customer Facing Name</th><th>Actual Promo Customer Facing Name</th>");
		//Reporter.log("<Table><th>Actual SKU(s)</th><th>Actual PromoName(s)</th><th>Actual PromoCustomerFacingName(s)</th>");
		while(excpectedSkuDCPListIter.hasNext()){
			
			DCPSkuVadidatorData expectedDcpValidatorData = excpectedSkuDCPListIter.next();
		//	uniqueDeviceSkus.add(expectedDcpValidatorData.getSkuNum());
		//	}
		//Iterator<String> expectedSkus = uniqueDeviceSkus.iterator();
		//while(expectedSkus.hasNext()){
		String requestBody = null;
		String expSkuVal = expectedDcpValidatorData.getSkuNum();
		Map<String, String> tokenMap = new HashMap<String, String>();
		Response response = null;
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", expSkuVal);
		tokenMap.put("correlationid", getValForCorrelation());

		
		PromotionApiV2 promoApiV2 = new PromotionApiV2();
		//requestBody = "{\r\n  \"shopper\": {\r\n    \"salesChannel\": \"WEB\",\r\n    \"accountType\": \"I\",\r\n    \"accountSubType\": \"R\",\r\n    \"crpid\": \"CRP6\"\r\n  },\r\n  \"promotions\" : {\r\n    \"transactionType\" : \"UPGRADE\"\r\n}\r\n}"
		requestBody = "{ \"shopper\": { \"salesChannel\": \"WEB\", \"accountType\": \"I\", \"accountSubType\": \"R\", \"crpid\": \"CRP6\" }, \"products\": [ { \"productType\": \"DEVICE\", \"sku\": [ \""+expSkuVal+"\" ] } ], \"promotions\": { \"transactionType\": \"UPGRADE\" } }";

		//logLargeRequest(requestBody, "Promotions");
		 response = promoApiV2.search_promotions(apiTestData, requestBody, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			//logLargeResponse(response, "Promotions");
			reportStringBuffer = prepareTradeInPromoReport(response, expSkuVal, reportStringBuffer, expectedDcpValidatorData);
			
		} else {
			failAndLogResponse(response, "Promotions");
		}
		
		//prepareTradeInPromoActualList(response);
		}
		Reporter.log(reportStringBuffer.toString());
	}

	public StringBuffer prepareTradeInPromoReport(Response response, String sku, StringBuffer reportStringBuffer, DCPSkuVadidatorData expectedDcpValidatorData) throws Exception{	
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(response.asString());
		if (!jsonNode.isMissingNode() && jsonNode.has("promotions")) {
			JsonNode parentNode = jsonNode.path("promotions");
			if (!parentNode.isMissingNode() && parentNode.isArray()) {
				for (int i = 0; i < parentNode.size(); i++) {
					JsonNode node = parentNode.get(i);
					if (node.isObject()) {
						String promoName = getPathVal(node, "promoName");
						String promoCustomerFacingName = getPathVal(node, "promoCustomerFacingName");
						
						if (StringUtils.isNoneEmpty(promoName))
							if(promoName.equalsIgnoreCase(expectedDcpValidatorData.getPromoName())) {
							promoName = "<td>"+expectedDcpValidatorData.getPromoName()+"</td><td bgcolor=\"#00FF00\">" + promoName + "</td>";
						} else {
							promoName = "<td>"+expectedDcpValidatorData.getPromoName()+"</td><td bgcolor=\"#FF0000\">" + promoName + "</td>";
						}else{
							promoName = "<td>"+expectedDcpValidatorData.getPromoName()+"</td><td bgcolor=\"#FF0000\"> Promo Name Not Available </td>";
						}
						if (StringUtils.isNoneEmpty(promoCustomerFacingName)) {
							if(promoCustomerFacingName.equalsIgnoreCase(expectedDcpValidatorData.getPromoCustomerFacingName())){
							promoCustomerFacingName = "<td>"+expectedDcpValidatorData.getPromoCustomerFacingName()+"</td><td bgcolor=\"#00FF00\">" + promoCustomerFacingName + "</td>";
						} else {
							promoCustomerFacingName = "<td>"+expectedDcpValidatorData.getPromoCustomerFacingName()+"</td><td bgcolor=\"#FF0000\">" + promoCustomerFacingName + "</td>";
						}
					}else{
						promoCustomerFacingName = "<td>"+expectedDcpValidatorData.getPromoCustomerFacingName()+"</td><td bgcolor=\"#FF0000\"> Promo Customer Facing Name is Not Available </td>";
						}						
						reportStringBuffer.append("<tr><td>" + sku + "</td>" + promoName +  promoCustomerFacingName+"</tr>");
						//Reporter.log("<tr><td>" + sku + "</td>" + promoName +  promoCustomerFacingName+"</tr>");
						break;
					}
				}
			}
		}else if (!jsonNode.isMissingNode() && jsonNode.has("message")){
			String jsonMessage = jsonNode.path("message").asText();
			if(StringUtils.isNoneBlank(jsonMessage)){
				if(jsonMessage.contains("No Promotions found")){
					reportStringBuffer.append("<tr><td>" + sku
							+ "</td><td colspan=\"4\" bgcolor=\"#FF0000\">" + " No Promotions found </td></tr>");
					//Reporter.log("<tr><td>" + sku + "</td><td colspan=\"2\" bgcolor=\"#FF0000\">" + " No Promotions found /td>");
				}else{
					reportStringBuffer.append("<tr><td>" + sku
							+ "</td><td colspan=\"4\" bgcolor=\"#FF0000\">" +jsonMessage +"</td></tr>");
				}
			}else{
				reportStringBuffer.append("<tr><td>" + sku
						+ "</td><td colspan=\"4\" bgcolor=\"#FF0000\">" + " No Promotions found </td></tr>");
			}
		}else{
			reportStringBuffer.append("<tr><td>" + sku
					+ "</td><td colspan=\"4\" bgcolor=\"#FF0000\">" + " No Promotions found </td></tr>");
		}
		
		return reportStringBuffer;
			
	}

	public void prepareTradeInPromoActualList(Response response) {
		try {
			actualTradeInPromoDataList = new LinkedList<DCPSkuVadidatorData>();
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode() && jsonNode.has("promotions")) {
				JsonNode parentNode = jsonNode.path("promotions");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {
						JsonNode node = parentNode.get(i);
						if (node.isObject()) {
							String promoName = getPathVal(node, "promoName");
							String promoCustomerFacingName = getPathVal(node, "promoCustomerFacingName");
							JsonNode itemsInConditionNode = node.path("itemsInCondition");
							if (!itemsInConditionNode.isMissingNode() && itemsInConditionNode.isArray()) {
								for (int j = 0; j < itemsInConditionNode.size(); j++) {
									JsonNode itemNode = itemsInConditionNode.get(j);
									if (!itemNode.path("itemType").isMissingNode()) {
										String itemTypeVal = itemNode.path("itemType").asText();
										if ("SKU".equalsIgnoreCase(itemTypeVal)) {
											JsonNode itemsNode = itemNode.path("items");
											if (!itemsNode.isMissingNode() && itemsNode.isArray()) {
												for (int k = 0; k < itemsNode.size(); k++) {
													String skuNum = itemsNode.get(k).asText();

													DCPSkuVadidatorData dcpSkuValidator = new DCPSkuVadidatorData();
													dcpSkuValidator.setSkuNum(skuNum);
													dcpSkuValidator.setPromoName(promoName);
													dcpSkuValidator.setPromoCustomerFacingName(promoCustomerFacingName);
													actualTradeInPromoDataList.add(dcpSkuValidator);
												}
											}
										} else {
											throw new Exception("Item Type Not found in Item Conditions in Promotions");
										}
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {

		}
	}

	//@Test(dataProvider = "byColumnName", priority = 1, enabled = true, groups = { "npi" })
	public void prepareProductsPromotionsResponse(ApiTestData apiTestData) throws Exception {

		String requestBody = "";
		Response response = null;

		try {
			String operationName = "Product Promotion Details";
			CatalogApiV4 catalogApi = new CatalogApiV4();
			// String familyID = "g-84358CC424B2481AA61480B260031304";
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			for (Object object : familyIdSet) {

				String familyID = (String) object;
				requestBody = "{\r\n  \"familyId\": [ \"" + familyID + "\"  ],  \"promotionType\": \"ALL\"\r\n}";
				//logLargeRequest(requestBody, operationName);

				response = catalogApi.retrievePromotionsAtFamilyLevel(apiTestData, requestBody, tokenMap);

				if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
					//logLargeResponse(response, operationName);
				} else {
					failAndLogResponse(response, operationName);
				}

				prepareActualPromotionsList(response, familyID);

				mapPromoWithFamilyID(familyID);

			}

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}

		prepareActualList(response);

	}

	public void mapPromoWithFamilyID(String familyID) {

		familyPromoIDInfo.put(familyID, actualSKUPromoID);
		familyPromoNameInfo.put(familyID, actualSKUPromoName);

	}

	public void prepareActualPromotionsList(Response response, String familyID) {
		try {
			actualSKUPromoID = new HashMap<String,String>();
			actualSKUPromoName = new HashMap<String,String>();
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode productPromoInfoNode = jsonNode.path("productPromoInfo");
			if (!productPromoInfoNode.isMissingNode() && productPromoInfoNode.isArray()) {
				for (int i = 0; i < productPromoInfoNode.size(); i++) {
					JsonNode productPromoInfo = productPromoInfoNode.get(i);
					if (productPromoInfo.has("data")) {
						JsonNode parentNode = productPromoInfo.path("data");
						if (!parentNode.isMissingNode() && parentNode.isArray()) {
							for (int j = 0; j < parentNode.size(); j++) {
								JsonNode node = parentNode.get(j);
								if (node.isObject()) {

									String skuNum = getPathVal(node, "skuNumber");
									if (node.has("PromotionTypes")) {
										JsonNode promoTypeNode = node.path("PromotionTypes");
										if (!promoTypeNode.isMissingNode() && promoTypeNode.isArray()) {
											for (int k = 0; k < promoTypeNode.size(); k++) {
												JsonNode unitNode = promoTypeNode.get(k);

												if (unitNode.isObject()) {
													if (unitNode.has("promotions")) {
														JsonNode promotionsNode = unitNode.path("promotions");
														if (!promotionsNode.isMissingNode()
																&& promotionsNode.isArray()) {
															for (int l = 0; l < promotionsNode.size(); l++) {
																JsonNode unitPromoNode = promotionsNode.get(l);

																if (unitPromoNode.isObject()) {
																	String promoId = getPathVal(unitPromoNode,
																			"promoId");
																	String promoName = getPathVal(unitPromoNode,
																			"promoName");																	
																	actualSKUPromoID.put(skuNum, promoId);
																	actualSKUPromoName.put(skuNum, promoName);

																}
															}

														}
													}
												}

											}
										}

									}
								}

							}
						}
					}
				}
			}

		} catch (Exception e) {
			Reporter.log("Exception occured" + e);
		}

	}

	public void prepareActualList(Response response) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode() && jsonNode.has("data")) {
				JsonNode parentNode = jsonNode.path("data");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {
						JsonNode node = parentNode.get(i);
						if (node.isObject()) {

							String familyId = node.path("family").asText();
							String manfactType = node.path("manufacturerType").asText();
							String modelName = node.path("modelName").asText();
							String skuNum = node.path("skuNumber").asText();
							String color = node.path("color").asText();
							String memory = node.path("memory").asText();
							String availableStatus = node.path("productStatus").asText();

							DCPSkuVadidatorData dcpSkuValidator = new DCPSkuVadidatorData();
							dcpSkuValidator.setSkuNum(skuNum);
							dcpSkuValidator.setFamilyId(familyId);
							dcpSkuValidator.setManfactType(manfactType);
							dcpSkuValidator.setModelName(modelName);
							dcpSkuValidator.setColor(color);
							dcpSkuValidator.setMemory(memory);
							dcpSkuValidator.setAvailableStatus(availableStatus);
							actualSKUDataList.add(dcpSkuValidator);

						}
					}
				}
			}

		} catch (Exception e) {
			Reporter.log("Exception occured" + e);
		}

	}

	//@Test(dataProvider = "byColumnName", dependsOnMethods = { "prepareTradeinPromotionResponse" }, enabled = true, groups = { "npi" })
	public void tradeInPromoResultReport(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Reporter.log("validate TradeInPromo Information");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the Promotions from v2/promotions/browse, should get from service response");
		Reporter.log("Step 2: Verify expected PromoName and PromoCustomerFacingName should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		prepareTradeInPromoResultReport();

	}
	

	@Test(dataProvider = "byColumnName", dependsOnMethods = { "prepareProductsResponse" }, enabled = true, groups = {"npi" })
		public void productsResultReport(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Reporter.log("validate Family Name");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the Product list should get from service response");
		Reporter.log("Step 2: Verify expected modelName should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		// String matchCriteria = "Family ID";
		
		prepareResultReport();

	}

	//@Test(dataProvider = "byColumnName", dependsOnMethods = {"prepareProductsPromotionsResponse" }, enabled = true, groups = { "npi" })
	public void verifyPromotionsForNPISkus(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Reporter.log("validate Product Status for Device Launce Sku's are in Response");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the Accessory list should get from service response");
		Reporter.log("Step 2: Verify expected SKU's Promotions should be in the response");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		preparePromotionsResultReport(familyPromoIDInfo, familyPromoNameInfo);
	}
	
	public void prepareTradeInPromoResultReport() {
		
		ListIterator<DCPSkuVadidatorData> actualTradeInPromoDataListIter = actualTradeInPromoDataList.listIterator();
		
		Reporter.log(
				"<Table><th>Actual SKU(s)</th><th>Actual PromoName(s)</th><th>Actual PromoCustomerFacingName(s)</th>");
		
		while(actualTradeInPromoDataListIter.hasNext()){
			
			DCPSkuVadidatorData actualDcpValidatorData = actualTradeInPromoDataListIter.next();
			
			String promoName = actualDcpValidatorData.getPromoName();
			String promoCustomerFacingName = actualDcpValidatorData.getPromoCustomerFacingName();
			String skuNum = "";
			if(StringUtils.isNoneBlank(actualDcpValidatorData.getSkuNum()))
			 skuNum = actualDcpValidatorData.getSkuNum();
			
				
			
			if (StringUtils.isNoneEmpty(promoName)) {
				promoName = "<td>" + promoName + "</td>";
			} else {
				promoName = "<td bgcolor=\"#FF0000\"> Promo Name Not Available </td>";
			}
			if (StringUtils.isNoneEmpty(promoCustomerFacingName)) {
				promoCustomerFacingName = "<td>" + promoCustomerFacingName + "</td>";
			} else {
				promoCustomerFacingName = "<td bgcolor=\"#FF0000\"> Promo Customer Facing Name is Not Available </td>";
			}

			Reporter.log("<tr><td>" + skuNum + "</td>" + promoName +  promoCustomerFacingName+"</tr>");
			
		}
		
		
		Reporter.log("</Table>");
		
		
	}

	public void prepareResultReport() {

		ListIterator<DCPSkuVadidatorData> excpectedDCPListIter = expectedSKUDataList.listIterator();

		Reporter.log(
				"<Table><th>Expected SKU</th><th>Actual SKU</th><th>Expected Family</th><th>Actual Family</th><th>Expected ModelName</th><th>Actual ModelName</th><th>Expected Color</th><th>Actual Color</th><th>Expected Manufacturer</th><th>Actual Manufacturer</th><th>Expected Memory</th><th>Actual Memory</th><th>Expected Availability</th><th>Actual Availability</th>");

		while (excpectedDCPListIter.hasNext()) {

			DCPSkuVadidatorData expectedDcpValidatorData = excpectedDCPListIter.next();
			ListIterator<DCPSkuVadidatorData> actualDCPListIter = actualSKUDataList.listIterator();
			boolean skuFound = false;
			while (actualDCPListIter.hasNext()) {
				String skuResultMsg;
				String familyResultMsg, familyBgColor;
				String manFactResultMsg, manFactBgColor;
				String memoryResultMsg, memoryBgColor;
				String colorResultMsg, colorBbColor;
				String modelResultMsg, modelBgColor;
				String availResultMsg, availBgColor;

				DCPSkuVadidatorData actualDcpValidatorData = actualDCPListIter.next();
				if (StringUtils.isNoneEmpty(expectedDcpValidatorData.getSkuNum())
						&& expectedDcpValidatorData.getSkuNum().equalsIgnoreCase(actualDcpValidatorData.getSkuNum())) {
					skuFound = true;
					familyBgColor = getResult(expectedDcpValidatorData.getFamilyId(),
							actualDcpValidatorData.getFamilyId());
					manFactBgColor = getResult(expectedDcpValidatorData.getManfactType(),
							actualDcpValidatorData.getManfactType());
					memoryBgColor = getResult(expectedDcpValidatorData.getMemory(), actualDcpValidatorData.getMemory());
					colorBbColor = getResult(expectedDcpValidatorData.getColor(), actualDcpValidatorData.getColor());
					modelBgColor = getResult(expectedDcpValidatorData.getModelName(),
							actualDcpValidatorData.getModelName());
					availBgColor = getResult(expectedDcpValidatorData.getAvailableStatus(),
							actualDcpValidatorData.getAvailableStatus());

					skuResultMsg = "<td bgcolor=\"#00FF00\">" + actualDcpValidatorData.getSkuNum() + "</td>";
					familyResultMsg = "<td bgcolor=\"" + familyBgColor + "\">" + actualDcpValidatorData.getFamilyId()
							+ "</td>";
					manFactResultMsg = "<td bgcolor=\"" + manFactBgColor + "\">"
							+ actualDcpValidatorData.getManfactType() + "</td>";
					memoryResultMsg = "<td bgcolor=\"" + memoryBgColor + "\">" + actualDcpValidatorData.getMemory()
							+ "</td>";
					colorResultMsg = "<td bgcolor=\"" + colorBbColor + "\">" + actualDcpValidatorData.getColor()
							+ "</td>";
					modelResultMsg = "<td bgcolor=\"" + modelBgColor + "\">" + actualDcpValidatorData.getModelName()
							+ "</td>";
					availResultMsg = "<td bgcolor=\"" + availBgColor + "\">"
							+ actualDcpValidatorData.getAvailableStatus() + "</td>";

					Reporter.log("<tr><td>" + expectedDcpValidatorData.getSkuNum() + "</td>" + skuResultMsg + "<td>"
							+ expectedDcpValidatorData.getFamilyId() + "</td>" + familyResultMsg + "<td>"
							+ expectedDcpValidatorData.getModelName() + "</td>" + modelResultMsg + "<td>"
							+ expectedDcpValidatorData.getColor() + "</td>" + colorResultMsg + "<td>"
							+ expectedDcpValidatorData.getManfactType() + "</td>" + manFactResultMsg + "<td>"
							+ expectedDcpValidatorData.getMemory() + "</td>" + memoryResultMsg + "<td>"
							+ expectedDcpValidatorData.getAvailableStatus() + "</td>" + availResultMsg);
					break;
				}

			}
			if (!skuFound) {
				Reporter.log("<tr><td>" + expectedDcpValidatorData.getSkuNum()
						+ "</td><td colspan=\"13\" bgcolor=\"#FF0000\">" + " Expected SKU Not Found</td>");
			}

		}

		Reporter.log("</Table>");

	}

	private String getResult(String expected, String actual) {
		String bgRed = "#FF0000";
		String bgGreen = "#00FF00";
		if (expected.equalsIgnoreCase(actual)) {
			return bgGreen;

		} else {
			return bgRed;
		}
	}

	public void preparePromotionsResultReport(Map<String, Map<String, String>> familyPromoIDMap,
			Map<String, Map<String, String>> familyPromoNameMap) {

		Iterator<Map.Entry<String, Map<String, String>>> actfamilyPromoIDMapIterator = familyPromoIDMap.entrySet()
				.iterator();
		Iterator<Map.Entry<String, Map<String, String>>> actfamilyPromoNameMapIterator = familyPromoNameMap.entrySet()
				.iterator();
		String familyIdFromPromoIDMap;
		String familyIdFromPromoNameMap;
		Map<String, String> skuPromoIdMap;
		Map<String, String> skuPromoNameMap;

		Reporter.log(
				"<Table><th>Actual Family ID</th><th>Actual SKU(s)</th><th>Actual PromoID(s)</th><th>Actual PromoName(s)</th>");
		while (actfamilyPromoIDMapIterator.hasNext()) {

			Map.Entry<String, Map<String, String>> actualFamilyPromoIDEntry = actfamilyPromoIDMapIterator.next();
			Map.Entry<String, Map<String, String>> actualFamilyPromoNameEntry = actfamilyPromoNameMapIterator.next();

			familyIdFromPromoIDMap = actualFamilyPromoIDEntry.getKey();
			familyIdFromPromoNameMap = actualFamilyPromoNameEntry.getKey();

			skuPromoIdMap = new HashMap<String, String>();
			skuPromoIdMap = actualFamilyPromoIDEntry.getValue();

			if (skuPromoIdMap.isEmpty()) {
				Reporter.log("<tr><td>" + familyIdFromPromoIDMap
						+ "</td><td colspan=\"3\" bgcolor=\"#FF0000\">Promotion Data does not exist for this Family Id</td></tr>");
			}

			skuPromoNameMap = new HashMap<String, String>();
			skuPromoNameMap = actualFamilyPromoNameEntry.getValue();

			if (familyIdFromPromoIDMap.equalsIgnoreCase(familyIdFromPromoNameMap)) {

				Iterator<Map.Entry<String, String>> actSkuPromoIdMapIterator = skuPromoIdMap.entrySet().iterator();
				Iterator<Map.Entry<String, String>> actSkuPromoNameMapIterator = skuPromoNameMap.entrySet().iterator();
				while (actSkuPromoIdMapIterator.hasNext()) {

					Map.Entry<String, String> actPromoIdMapEntry = actSkuPromoIdMapIterator.next();
					Map.Entry<String, String> actPromoNameMapEntry = actSkuPromoNameMapIterator.next();

					if (actPromoIdMapEntry.getKey().equalsIgnoreCase(actPromoNameMapEntry.getKey())) {
						String promoID;
						String promoName;
						if (StringUtils.isNoneEmpty(actPromoIdMapEntry.getValue())) {
							promoID = "<td>" + actPromoIdMapEntry.getValue() + "</td>";
						} else {
							promoID = "<td bgcolor=\"#FF0000\"> Promo ID Not Available </td>";
						}
						if (StringUtils.isNoneEmpty(actPromoNameMapEntry.getValue())) {
							promoName = "<td>" + actPromoNameMapEntry.getValue() + "</td>";
						} else {
							promoName = "<td bgcolor=\"#FF0000\"> Promo Name Not Available </td>";
						}

						Reporter.log("<tr><td>" + familyIdFromPromoIDMap + "</td><td>" + actPromoIdMapEntry.getKey()
								+ "</td>" + promoID + promoName);
					}
				}
			}
		}

		Reporter.log("</Table>");

	}
	
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "npi" })
	public void prepareDeviceAccessoryResponse(ApiTestData apiTestData) throws Exception {

		Set<String> uniqueDeviceSkus = new HashSet<String>();
		ListIterator<DCPSkuVadidatorData> excpectedDCPListIter = expectedSKUAccessoryDataList.listIterator();
		while(excpectedDCPListIter.hasNext()){
			
			DCPSkuVadidatorData expectedDcpValidatorData = excpectedDCPListIter.next();
			uniqueDeviceSkus.add(expectedDcpValidatorData.getSkuNum());
			}
		Iterator<String> expectedAccessorySkus = uniqueDeviceSkus.iterator();
		while(expectedAccessorySkus.hasNext()){
		String requestBody = null;
		String expAccSkuVal = expectedAccessorySkus.next();
		Map<String, String> tokenMap = new HashMap<String, String>();
		Response response = null;
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", expAccSkuVal);
		tokenMap.put("correlationid", getValForCorrelation());

		CatalogApiV4 catalogApi = new CatalogApiV4();
		requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_NPI + "accessoryCatalog_NPI.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response = catalogApi.retrieveProductsUsingPOST(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			//logLargeResponse(response, "Accessories Details");
		} else {
			failAndLogResponse(response, "Accessories Details");
		}

		prepareActualAccessoriesList(response, expAccSkuVal);
		}
		//System.out.println("actualSKUAccessoryMap : "+actualSKUAccessoryMap.toString());
	}
	
	public void prepareActualAccessoriesList(Response response, String expectedSkuNumber) {
		try {
			actualSKUAccessoryDataList = new LinkedList<DCPSkuVadidatorData>();
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode() && jsonNode.has("data")) {
				JsonNode parentNode = jsonNode.path("data");
				if (!parentNode.isMissingNode() && parentNode.isArray()) {
					for (int i = 0; i < parentNode.size(); i++) {
						JsonNode node = parentNode.get(i);
						if (node.isObject()) {

							String familyId = node.path("family").asText();
							String manfactType = node.path("manufacturerType").asText();
							String modelName = node.path("modelName").asText();
							String accessorySkuNum = node.path("skuNumber").asText();
							String color = node.path("color").asText();
							String availableStatus = node.path("productStatus").asText();

							DCPSkuVadidatorData dcpSkuValidator = new DCPSkuVadidatorData();
							dcpSkuValidator.setSkuNum(expectedSkuNumber);
							dcpSkuValidator.setAccessorySkuNum(accessorySkuNum);
							dcpSkuValidator.setFamilyId(familyId);
							dcpSkuValidator.setManfactType(manfactType);
							dcpSkuValidator.setModelName(modelName);
							dcpSkuValidator.setColor(color);
							dcpSkuValidator.setAvailableStatus(availableStatus);
							actualSKUAccessoryDataList.add(dcpSkuValidator);

						}
					}
					actualSKUAccessoryMap.put(expectedSkuNumber, actualSKUAccessoryDataList);
				}
			}else{
				actualSKUAccessoryMap.put(expectedSkuNumber, null);
			}

		} catch (Exception e) {
			Reporter.log("Exception occured" + e);
		}
		
	}
	
	//@Test(dataProvider = "byColumnName", dependsOnMethods = { "prepareDeviceAccessoryResponse" }, enabled = true, groups = {"npi" })
	public void accessoriesResultReport(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
	Reporter.log("validate Family Name");
	Reporter.log("Data Conditions:MyTmo registered misdn.");
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Get all the Product list should get from service response");
	Reporter.log("Step 2: Verify expected modelName should be in the response list");
	Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
	Reporter.log("================================");
	Reporter.log("Actual Result:");
	
	// String matchCriteria = "Family ID";
	
	prepareAccessoriesResultReport();
	
	}
	
	public void prepareAccessoriesResultReport() {

		ListIterator<DCPSkuVadidatorData> excpectedDCPAccListIter = expectedSKUAccessoryDataList.listIterator();
		boolean skuFound = false;
		boolean accessoryfound = false;
		String expectedSku = "";
		Reporter.log(
				"<Table><th>Expected Device SKU</th><th>Actual Device SKU</th><th>Expected Accessory SKU</th><th>Actual Accessory SKU</th><th>Expected Accessory Family</th><th>Actual Accessory Family</th><th>Expected ModelName</th><th>Actual ModelName</th><th>Expected Color</th><th>Actual Color</th><th>Expected Manufacturer</th><th>Actual Manufacturer</th><th>Expected Availability</th><th>Actual Availability</th>");

		while (excpectedDCPAccListIter.hasNext()) {

			DCPSkuVadidatorData expectedDcpValidatorData = excpectedDCPAccListIter.next();
			expectedSku = expectedDcpValidatorData.getSkuNum();

			skuFound = false;

			if (null == actualSKUAccessoryMap.get(expectedSku)) {
				break;
			}
			List<DCPSkuVadidatorData> dcpAccessorySkuVadidatorDataList = actualSKUAccessoryMap.get(expectedSku);
			String actualDeviceSku = expectedSku;

			skuFound = true;

			ListIterator<DCPSkuVadidatorData> actualDCPAccEntryValIter = dcpAccessorySkuVadidatorDataList
					.listIterator();
			while (actualDCPAccEntryValIter.hasNext()) {
				accessoryfound = false;
				DCPSkuVadidatorData actualDcpValidatorData = actualDCPAccEntryValIter.next();
				if (StringUtils.isNoneEmpty(expectedDcpValidatorData.getAccessorySkuNum()) && expectedDcpValidatorData
						.getAccessorySkuNum().equalsIgnoreCase(actualDcpValidatorData.getAccessorySkuNum())) {
					accessoryfound = true;
					String skuResultMsg, accessorySkuResultMsg, accessorySkuBgColor;
					String familyResultMsg, familyBgColor;
					String manFactResultMsg, manFactBgColor;
					// String memoryResultMsg, memoryBgColor;
					String colorResultMsg, colorBbColor;
					String modelResultMsg, modelBgColor;
					String availResultMsg, availBgColor;

					accessorySkuBgColor = getResult(expectedDcpValidatorData.getAccessorySkuNum(),
							actualDcpValidatorData.getAccessorySkuNum());
					familyBgColor = getResult(expectedDcpValidatorData.getFamilyId(),
							actualDcpValidatorData.getFamilyId());
					manFactBgColor = getResult(expectedDcpValidatorData.getManfactType(),
							actualDcpValidatorData.getManfactType());
					// memoryBgColor =
					// getResult(expectedDcpValidatorData.getMemory(),
					// actualDcpValidatorData.getMemory());
					colorBbColor = getResult(expectedDcpValidatorData.getColor(), actualDcpValidatorData.getColor());
					modelBgColor = getResult(expectedDcpValidatorData.getModelName(),
							actualDcpValidatorData.getModelName());
					availBgColor = getResult(expectedDcpValidatorData.getAvailableStatus(),
							actualDcpValidatorData.getAvailableStatus());

					skuResultMsg = "<td bgcolor=\"#00FF00\">" + actualDcpValidatorData.getSkuNum() + "</td>";
					accessorySkuResultMsg = "<td bgcolor=\"" + accessorySkuBgColor + "\">"
							+ actualDcpValidatorData.getAccessorySkuNum() + "</td>";
					familyResultMsg = "<td bgcolor=\"" + familyBgColor + "\">" + actualDcpValidatorData.getFamilyId()
							+ "</td>";
					manFactResultMsg = "<td bgcolor=\"" + manFactBgColor + "\">"
							+ actualDcpValidatorData.getManfactType() + "</td>";
					// memoryResultMsg = "<td bgcolor=\"" + memoryBgColor +
					// "\">" + actualDcpValidatorData.getMemory() + "</td>";
					colorResultMsg = "<td bgcolor=\"" + colorBbColor + "\">" + actualDcpValidatorData.getColor()
							+ "</td>";
					modelResultMsg = "<td bgcolor=\"" + modelBgColor + "\">" + actualDcpValidatorData.getModelName()
							+ "</td>";
					availResultMsg = "<td bgcolor=\"" + availBgColor + "\">"
							+ actualDcpValidatorData.getAvailableStatus() + "</td>";

					Reporter.log("<tr><td>" + expectedDcpValidatorData.getSkuNum() + "</td>" + skuResultMsg + "<td>"
							+ expectedDcpValidatorData.getAccessorySkuNum() + "</td>" + accessorySkuResultMsg + "<td>"
							+ expectedDcpValidatorData.getFamilyId() + "</td>" + familyResultMsg + "<td>"
							+ expectedDcpValidatorData.getModelName() + "</td>" + modelResultMsg + "<td>"
							+ expectedDcpValidatorData.getColor() + "</td>" + colorResultMsg + "<td>"
							+ expectedDcpValidatorData.getManfactType() + "</td>" + manFactResultMsg + "<td>"
							+ expectedDcpValidatorData.getAvailableStatus() + "</td>" + availResultMsg);

					break;
				}
			}

			if (!accessoryfound) {
				String actSkuMsg = "<td bgcolor=\"#00FF00\">" + actualDeviceSku + "</td>";
				Reporter.log("<tr><td>" + expectedDcpValidatorData.getSkuNum() + actSkuMsg + "<td>"
						+ expectedDcpValidatorData.getAccessorySkuNum() + "</td><td colspan=\"11\" bgcolor=\"#FF0000\">"
						+ " Expected Accessory Not Found</td>");
				// break;
			}
			if (!skuFound) {
				Reporter.log("<tr><td>" + expectedDcpValidatorData.getSkuNum()
						+ "</td><td colspan=\"13\" bgcolor=\"#FF0000\">" + " Expected SKU Not Found</td>");
			}
		}
		if (!skuFound) {
			Reporter.log("<tr><td>" + expectedSku + "</td><td colspan=\"13\" bgcolor=\"#FF0000\">"
					+ " Expected SKU Not Found</td>");
		}

		Reporter.log("</Table>");

	}

}
