package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.FamilyAllowanceApiV1;

import io.restassured.response.Response;

public class FamilyAllowanceApiV1Test extends FamilyAllowanceApiV1 {

	public Map<String, String> tokenMap;

	/**
	 * /** UserStory# US596551:Family Allowances - Update subscriber lines for
	 * business and government customers;
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "FamilyAllowanceApiV1Test",
			"testFamilyAllowanceSubNonBusinessandGovtUsers", Group.PAYMENTS, Group.SPRINT })
	public void testFamilyAllowanceSubNonBusinessandGovtUsers(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testFamilyAllowanceSubNonBusinessandGovtUsers");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with SubscriberLines.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testFamilyAllowanceSubNonBusinessandGovtUsers";

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("accounttype", apiTestData.getAccountType());

		Response response = SubScriberNonBusinessandGovtUsers(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "accountNumber"), apiTestData.getBan(),
						"AccountNumber is mismatched");
				Assert.assertEquals(getPathVal(jsonNode, "loggedInMsisdn"), apiTestData.msisdn,
						"Msisdin is mismatched");
				Assert.assertNotNull(jsonNode.get("lines").get(0).get("msisdn"), "Msisdin is not presented");
				Assert.assertNotNull(jsonNode.get("lines").get(0).get("firstName"), "firstName is Not presented");
				Assert.assertNotNull(jsonNode.get("lines").get(0).get("lastName"), "lastName is Not presented");
				Assert.assertNotNull(jsonNode.get("lines").get(0).get("permission"), "permission is Not presented");
			}
		} else {

			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * /** UserStory# US596551:Family Allowances - Update subscriber lines for
	 * business and government customers;
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "FamilyAllowanceApiV1Test",
			"testFamilyAllowanceSubBusinessandGovtUsers", Group.PAYMENTS, Group.SPRINT })
	public void testFamilyAllowanceSubBusinessandGovtUsers(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testFamilyAllowanceSubBusinessandGovtUsers");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with SubscriberLines.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testFamilyAllowanceSubBusinessandGovtUsers";

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("accounttype", apiTestData.getAccountType());

		Response response = SubScriberBusinessandGovtUsers(apiTestData, tokenMap, apiTestData.getAccountType());

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "accountNumber"), apiTestData.getBan(),
						"AccountNumber is mismatched");
				Assert.assertEquals(getPathVal(jsonNode, "loggedInMsisdn"), apiTestData.msisdn,
						"Msisdin is mismatched");
				Assert.assertNotNull(jsonNode.get("lines").get(0).get("msisdn"), "msisdn is Not presented");
				Assert.assertNotNull(jsonNode.get("lines").get(0).get("permission"), "permission is Not presented");
				Assert.assertNotNull(jsonNode.get("lines").get(1).get("msisdn"), "msisdn is Not presented");
				Assert.assertNotNull(jsonNode.get("lines").get(1).get("permission"), "permission is Not presented");
			}
		} else {

			failAndLogResponse(response, operationName);
		}

	}
}
