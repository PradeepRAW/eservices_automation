/**
 * 
 */
package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.ConsolidatedRatePlanPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.InterstitialTradeInPage;
import com.tmobile.eservices.qa.pages.shop.TradeInAnotherDevicePage;
import com.tmobile.eservices.qa.pages.shop.UNOPDPPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

/**
 * @author Suresh
 *
 */
public class InterstitialTest extends ShopCommonLib {
	
	/**
	 * US488169 :Build Interstitial Page for AAL Trade in
	 *  US485303 :Interstitial Page - How it works modal
	 *   US485300 :Interstitial Page - Trade In		
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION})
	public void testAuthorableTitleInInterstitialPageWithWithUnKnowIntent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US488169 :Build Interstitial Page for AAL Trade in");		
		Reporter.log("Data Condition | PAH User with AAL Eligible");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click See all phones button | PLP Page should be displayed");
		Reporter.log("6. Select device | PDP Page should be displayed");		
		Reporter.log("7. Click on Add a Line button | Customer intent page should be displayed");
		Reporter.log("8. Select on 'BYOD device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("9. Click Contune button | Interstitial page should be displayed ");		
		Reporter.log("10. Verify Authorable title 'Want to trade in a device?'| Authorable title 'Want to trade in a device?' should be displayed ");
		Reporter.log("11. Verify Authorable text 'We'll tell you if your trade-in qualifies...' | Authorable text should be displayed");
		Reporter.log("12. Verify Trade-In tile 'Yes, I want to trade in a device' | Trade-In tile should be displayed");
		Reporter.log("13. Verify Skip trade-in tile | Skip trade-in tile should be displayed ");
		Reporter.log("14. Verify 'How trade-in works' link | 'How trade-in works' link should be displayed");
		Reporter.log("15. Click on 'How trade-in works' link  | Trade-in model window should be displayed");
		Reporter.log("16. Verify Trade-in model window header | Trade-in Model window header should be displayed");
		Reporter.log("17. Verify Trade-in model window text | Trade-in Model window text should be displayed");
		Reporter.log("18. Click Trade-in model window 'X' close icon | Trade-in Model window should be closed");
		Reporter.log("19. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");	

		navigateToInterstitialTradeInPageFromSeeAllPhones(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialPageTitle();
		interstitialTradeInPage.verifyAuthorableSubHeaderForInterstitialPage();
		interstitialTradeInPage.verifyYesTradeinTile();
		interstitialTradeInPage.verifySkipTradeinTile();
		interstitialTradeInPage.verifyHowTradeinWorksLink();
		interstitialTradeInPage.clickHowTradeinWorksLink();
		interstitialTradeInPage.verifyHowTradeinWorksModalHeader();
		interstitialTradeInPage.verifyHowTradeinWorksModalText();
		interstitialTradeInPage.clickCloseIcon();
		interstitialTradeInPage.verifyHowTradeinWorksModalNotDisplayed();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
	}
	
	/**
	 * US488169 :Build Interstitial Page for AAL Trade in	
	 * US485303 :Interstitial Page - How it works modal
	 *  US485300 :Interstitial Page - Trade In
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION })
	public void testAuthorableTitleInInterstitialPageWithKnowIntent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US488169 :Build Interstitial Page for AAL Trade in");		
		Reporter.log("Data Condition | PAH User with AAL Eligible");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click Contune button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Click Contune button | Interstitial page should be displayed ");				
		Reporter.log("10. Verify Authorable title 'Want to trade in a device?'| Authorable title 'Want to trade in a device?' should be displayed ");
		Reporter.log("11. Verify Authorable text 'We'll tell you if your trade-in qualifies...' | Authorable text should be displayed");
		Reporter.log("12. Verify Trade-In tile 'Yes, I want to trade in a device' | Trade-In tile should be displayed");
		Reporter.log("13. Verify Skip trade-in tile | Skip trade-in tile should be displayed ");		
		Reporter.log("13. Verify 'How trade-in works' link | 'How trade-in works' link should be displayed");
		Reporter.log("14. Click on 'How trade-in works' link  | Trade-in model window should be displayed");
		Reporter.log("15. Verify Trade-in model window header | Trade-in Model window header should be displayed");
		Reporter.log("16. Verify Trade-in model window text | Trade-in Model window text should be displayed");
		Reporter.log("17. Click Trade-in model window 'X' close icon | Trade-in Model window should be closed");	
		Reporter.log("18. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");


		Reporter.log("================================");
		Reporter.log("Actual Output:");	

		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialPageTitle();
		interstitialTradeInPage.verifyAuthorableSubHeaderForInterstitialPage();
		interstitialTradeInPage.verifyYesTradeinTile();
		interstitialTradeInPage.verifySkipTradeinTile();
		interstitialTradeInPage.verifyHowTradeinWorksLink();
		interstitialTradeInPage.clickHowTradeinWorksLink();
		interstitialTradeInPage.verifyHowTradeinWorksModalHeader();
		interstitialTradeInPage.verifyHowTradeinWorksModalText();
		interstitialTradeInPage.clickCloseIcon();
		interstitialTradeInPage.verifyHowTradeinWorksModalNotDisplayed();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		
	}	
	
	/**
	 * US485302 :Interstitial Page - Decline Trade In
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION })
	public void testSkipTradeInOptionInInterstitialPageWithUnKnowIntent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US485302 :Interstitial Page - Decline Trade In");
		Reporter.log("Data Condition | PAH User with AAL Eligible");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");			
		Reporter.log("4. Click on shop | shop page should be displayed");
		Reporter.log("5. Click on see all phones | PLP page should be displayed");
		Reporter.log("6. Select on Deive | PDP page should be displayed");
		Reporter.log("7. click on 'AAL' CTA | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("9. Click Continue button | Interstitial page should be displayed ");
		Reporter.log("10. Verify Authorable Image | Authorable Image should be displayed ");
		Reporter.log("11. Verify Skip trade-in tile | Skip trade-in tile should be displayed ");		
		Reporter.log("12. Verify Authorabe body text 'No thanks, skip trade-in' | Authorabe body text should be displayed ");			
		Reporter.log("13. Select on 'No thanks, skip trade-in' | PHP page should be displayed");
					
		Reporter.log("================================");
		Reporter.log("Actual Output:");	

		navigateToInterstitialTradeInPageFromSeeAllPhones(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyAuthorableImageSkipTradein();
		interstitialTradeInPage.verifySkipTradeinTile();
		interstitialTradeInPage.authorableTextSkipTradeInTile();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
	}
	
	/**
	 * CDCSM-148 :US521900 - myTMO release - As an authenticated user that has specified my intent 
	 * to ADD A LINE, I'd like the only CTA displayed to be ADD TO CART, 
	 * so I am not confused by other options.
	 * TC-1197 : Desktop: Validate that clicking Continue on consolidated charges page navigates user to Trade in pages
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "uno" })
	public void testUserNavigatedToInterstitialTradeInPageThroughUNOPages(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on 'Add a Line' link in shop page | DeviceIntent Page should be displayed");
		Reporter.log("4. Select 'Buy a new phone' option | Consolidated Rate Plan Page should be displayed");
		Reporter.log("5. Click on Continue button | UNO PLP Page should be displayed");
		Reporter.log("7. Select device | UNOPDP Page should be displayed");		
		Reporter.log("9. Click on Add to Cart CTA | Rate plan page should be displayed");
		Reporter.log("10. Click on Continue button | Interstitial-tradeIn page should be displayed");
		Reporter.log("11. Verify Authorable title 'Want to trade in a device?'| Authorable title 'Want to trade in a device?' should be displayed ");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		UNOPDPPage unoPDPPage = navigateToUNOPDPPageFromAALBuyNewPhoneFlow(myTmoData);
		unoPDPPage.clickOnAddToCartCTA();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.verifyInterstitialPageTitle();
	}

}
