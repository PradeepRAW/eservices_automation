package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAlonePLPPage;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.ESignaturePage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewIntroductionPage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewQuestionPage;
import com.tmobile.eservices.qa.pages.shop.InterstitialTradeInPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.OrderConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.shop.TradeInAnotherDevicePage;
import com.tmobile.eservices.qa.pages.shop.TradeInConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionValuePage;
import com.tmobile.eservices.qa.pages.shop.TradeinDeviceConditionPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import com.tmobile.eservices.qa.shop.ShopConstants;

public class OrderConfirmationTest extends ShopCommonLib {

    /**
     * US472742: REIP - Hyperlink on order Confirmation to Account History (dynamic text EIP/Trade in)
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
    public void verifyAccountHistoryLinkOnOrderconfirmationPageWithEIPAndSkipTradeInDeviceFlow(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US472742: REIP - Hyperlink on order Confirmation to Account History (dynamic text EIP/Trade in)");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on shop link | Shop page should be displayed");
        Reporter.log("5. Click on see all phones | PLP page should be displayed");
        Reporter.log("6. Click on a Device in PLP | PDP page should be displayed");
        Reporter.log("6. Select EIP payment option on PDP | EIP payment option should be selected");
        Reporter.log("7. Click on Add to Cart button on PDP | Line Selector Page should be Displayed");
        Reporter.log("8. Select a line on Line Selector page | Line Selector details Page should be Displayed");
        Reporter.log("9. Click Skip Trade-In button | Device Protection page should be Displayed");
        Reporter.log("10. Click Continue button on Device Protection page  | Accessories page should be Displayed");
        Reporter.log("11. Click Skip Accessories link  | Cart page should be Displayed");
        Reporter.log("12. Click Continue to Shipping CTA on Cart Page | Shipping details info should be Displayed");
        Reporter.log("13. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
        Reporter.log("14. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
        Reporter.log("15. Click on Accept & Continue CTA | Order should be processed and Thank You Order Confirmation page should be displayed");
        Reporter.log("16. Verify 'Account History' link presence on the order Confirmation page | Account History link should be displayed under" +
                " Order Details(For EIP - Skip Trade-In)");
        Reporter.log("17. Click on 'Account History' link | Ensure Account history page is opened on a new tab");

        Reporter.log("================================");
        Reporter.log("Actual Output:");


		navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickSkipTradeInCTA();
		
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
        deviceProtectionPage.verifyDeviceProtectionPage();
        
        AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesLink();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingDetails();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();

        cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
        cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
        cartPage.enterCardNumber(myTmoData.getCardNumber());
        cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
        cartPage.enterCVV(myTmoData.getPayment().getCvv());
        cartPage.clickAcceptAndPlaceOrder();

        ESignaturePage esignaturePage = new ESignaturePage(getDriver());
        esignaturePage.verifyESignaturePage();
        esignaturePage.selectESignatureCheckBox();
        esignaturePage.clickESignatureContinueCTA();
        esignaturePage.clickSignatureImage();
        esignaturePage.clickSelectStyleTab();
        esignaturePage.clickAdoptSignatureCTA();
        esignaturePage.clickSendTheOrderCTA();

        OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
        orderConfirmationPage.verifyOrderConfirmationPage();
        orderConfirmationPage.verifyAccountHistoryLinkPresent();
        orderConfirmationPage.clickAccountHistoryLink();

        AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
        accountHistoryPage.verifyPageUrl();


    }

    /**
     * US472742: REIP - Hyperlink on order Confirmation to Account History (dynamic text EIP/Trade in)
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
    public void verifyAccountHistoryLinkOnOrderconfirmationPageWithEIPAndTradeInDeviceFlow(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US472742: REIP - Hyperlink on order Confirmation to Account History (dynamic text EIP/Trade in)");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on shop link | Shop page should be displayed");
        Reporter.log("5. Click on see all phones | PLP page should be displayed");
        Reporter.log("6. Click on a Device in PLP | PDP page should be displayed");
        Reporter.log("6. Select EIP payment option on PDP | EIP payment option should be selected");
        Reporter.log("7. Click on Add to Cart button on PDP | Line Selector Page should be Displayed");
        Reporter.log("8. Select a line on Line Selector page | Line Selector details Page should be Displayed");
        Reporter.log("9. Click 'Trade-In this phone' button | Device Condition page should be Displayed");
        Reporter.log("10. Select device condition | Trade In confirmation page should be Displayed");
        Reporter.log("11. Select Continue button | Device protection page should be Displayed");
        Reporter.log("12. Select Continue button on Device protection page | Accessories page should be Displayed");
        Reporter.log("13. Click Skip Accessories link  | Cart page should be Displayed");
        Reporter.log("14. Click Continue to Shipping CTA on Cart Page | Shipping details info should be Displayed");
        Reporter.log("15. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
        Reporter.log("16. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
        Reporter.log("17. Click on Accept & Continue CTA | Order should be processed and Thank You Order Confirmation page should be displayed");
        Reporter.log("18. Verify 'Account History' link presence on the order Confirmation page | Account History link should be displayed under" +
                " after the DRP agreement text(For EIP - Trade-In)");
        Reporter.log("19. Click on 'Account History' link | Ensure Account history page is opened in new tab");

        Reporter.log("================================");
        Reporter.log("Actual Output:");
        
        navigateToTradeInDeviceConditionValuePageBySeeAllPhones(myTmoData);

        TradeinDeviceConditionPage tradeinDeviceConditionPage = new TradeinDeviceConditionPage(getDriver());
        tradeinDeviceConditionPage.verifyDeviceConditionAsItsGood();
        tradeinDeviceConditionPage.clickOnGoodCondition();
        TradeInConfirmationPage tradeInConfirmationPage = new TradeInConfirmationPage(getDriver());
        tradeInConfirmationPage.verifyTradeInConfirmationPage();

        tradeInConfirmationPage.verifyEligibilityMessage();
        tradeInConfirmationPage.clickOnTradeInThisPhoneButton();

        DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
        deviceProtectionPage.verifyDeviceProtectionPage();

        deviceProtectionPage.clickOnYesProtectMyPhoneButton();
        AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
        accessoryPLPPage.clickSkipAccessoriesLink();

        CartPage cartPage = new CartPage(getDriver());
        cartPage.clickContinueToShippingButton();
        cartPage.clickContinueToPaymentButton();
        cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
        cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
        cartPage.enterCardNumber(myTmoData.getCardNumber());
        cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
        cartPage.enterCVV(myTmoData.getPayment().getCvv());
        cartPage.clickAcceptAndPlaceOrder();

        ESignaturePage esignaturePage = new ESignaturePage(getDriver());
        esignaturePage.verifyESignaturePage();
        esignaturePage.selectESignatureCheckBox();
        esignaturePage.clickESignatureContinueCTA();
        esignaturePage.clickSignatureImage();
        esignaturePage.clickSelectStyleTab();
        esignaturePage.clickAdoptSignatureCTA();
        esignaturePage.clickSendTheOrderCTA();

        OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
        orderConfirmationPage.verifyOrderConfirmationPage();
        orderConfirmationPage.verifyAccountHistoryLinkPresent();

        orderConfirmationPage.clickAccountHistoryLink();

        AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
        accountHistoryPage.verifyPageUrl();

    }

    /**
     * US472742: REIP - Hyperlink on order Confirmation to Account History (dynamic text EIP/Trade in)
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
    public void verifyAccountHistoryLinkPresenceOnOrderconfirmationPageWithFRP(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US472742: REIP - Hyperlink on order Confirmation to Account History (dynamic text EIP/Trade in)");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on shop link | Shop page should be displayed");
        Reporter.log("5. Click on see all phones | PLP page should be displayed");
        Reporter.log("6. Click on a Device in PLP | PDP page should be displayed");
        Reporter.log("7. Select FRP payment option on PDP | FRP payment option should be selected");
        Reporter.log("8. Click on Add to Cart button on PDP | Line Selector Page should be Displayed");
        Reporter.log("9. Select a line on Line Selector page | Line Selector details Page should be Displayed");
        Reporter.log("10. Click Skip Trade-In button | Device Protection page should be Displayed");
        Reporter.log("11. Click Continue button on Device Protection page  | Accessories page should be Displayed");
        Reporter.log("12. Click Skip Accessories link  | Cart page should be Displayed");
        Reporter.log("13. Click Continue to Shipping CTA on Cart Page | Shipping details info should be Displayed");
        Reporter.log("14. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
        Reporter.log("15. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
        Reporter.log("16. Click on Accept & Continue CTA | Order should be processed and Thank You Order Confirmation page should be displayed");
        Reporter.log("17. Verify 'Account History' link presence on the order Confirmation page | Account History link should not be displayed on order Confirmation page");

        Reporter.log("================================");
        Reporter.log("Actual Output:");

        CartPage cartPage = navigateToPaymentTabBySeeAllPhonesWithSkipTradeIn(myTmoData);
        cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
        cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
        cartPage.enterCardNumber(myTmoData.getCardNumber());
        cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
        cartPage.enterCVV(myTmoData.getPayment().getCvv());
        cartPage.clickAcceptAndPlaceOrderForFRPFlow();

        OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
        orderConfirmationPage.verifyOrderConfirmationPage();
        orderConfirmationPage.verifyAccountHistoryLinkNotPresent();
    }

    /**
     * US415053: Order Confirmation Page
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
    public void verifyShopForAccessoriesCTAOnOrderConfirmationPageUsingAALBYOD(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US415053: Order Confirmation Page");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on shop link | Shop page should be displayed");
        Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
        Reporter.log("6. Click on 'Use my own phone' option | Rate page should be displayed");
        Reporter.log("7. Click 'Continue' button on Rate plan page | Cart Page should be displayed");
        Reporter.log("8. Click on 'Continue to shipping' CTA on cart page | Shipping details info should be displayed");
        Reporter.log("9. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
        Reporter.log("10. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
        Reporter.log("11. Click on Accept & Continue CTA | Order should be Placed and Order Confirmation page should be displayed");
        Reporter.log("12. Verify 'THANK YOU!' text along with Authorable image presence | Authorable image and text should be displayed");
        Reporter.log("13. Verify Order details section & Transfer number section presence | Order details section & Transfer number section's should be displayed");
        Reporter.log("14. Verify 'Shop for accessories' & 'Continue Shopping' CTA's presence | 'Shop for accessories' & 'Continue Shopping' CTA's should be displayed");
        Reporter.log("15. Click 'Shop for accessories'CTA | User should be redirected to Accessories stand alone page");

        Reporter.log("================================");
        Reporter.log("Actual Output:");

        OrderConfirmationPage orderConfirmationPage = navigateToOrderConformationAALBYOD(myTmoData);
        orderConfirmationPage.verifyThankYouText();
        orderConfirmationPage.verifyThankYouImage();
        orderConfirmationPage.verifyOrderDetailsSection();
        orderConfirmationPage.verifyTarnsferNumberSection();
        orderConfirmationPage.verifyShopForAccessory();
        orderConfirmationPage.verifyContinueToShopping();
        orderConfirmationPage.clickShopAccessoriesCTA();
        AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
        accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();

    }

    /**
     * US415053: Order Confirmation Page
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
    public void verifyContinueShoppingCTAOnOrderConfirmationPageUsingAALBYOD(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US415053: Order Confirmation Page");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on shop link | Shop page should be displayed");
        Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
        Reporter.log("6. Click on 'Use my own phone' option | Rate page should be displayed");
        Reporter.log("7. Click 'Continue' button on Rate plan page | Cart Page should be displayed");
        Reporter.log("8. Click on 'Continue to shipping' CTA on cart page | Shipping details info should be displayed");
        Reporter.log("9. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
        Reporter.log("10. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
        Reporter.log("11. Click on Accept & Continue CTA | Order should be Placed and Order Confirmation page should be displayed");
        Reporter.log("12. Verify 'THANK YOU!' text along with Authorable image presence | Authorable image and text should be displayed");
        Reporter.log("13. Verify Order details section & Transfer number section presence | Order details section & Transfer number section's should be displayed");
        Reporter.log("14. Verify 'Shop for accessories' & 'Continue Shopping' CTA's presence | 'Shop for accessories' & 'Continue Shopping' CTA's should be displayed");
        Reporter.log("15. Click 'Continue Shopping' CTA | User should be redirected to Shop page");

        Reporter.log("================================");
        Reporter.log("Actual Output:");

        OrderConfirmationPage orderConfirmationPage = navigateToOrderConformationAALBYOD(myTmoData);
        orderConfirmationPage.verifyThankYouText();
        orderConfirmationPage.verifyThankYouImage();
        orderConfirmationPage.verifyOrderDetailsSection();
        orderConfirmationPage.verifyTarnsferNumberSection();
        orderConfirmationPage.verifyShopForAccessory();
        orderConfirmationPage.verifyContinueToShopping();
        orderConfirmationPage.clickContinueToShopping();
        ShopPage shopPage = new ShopPage(getDriver());
        shopPage.verifyPageUrl();

    }

    /**
     * US415053: Order Confirmation Page
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
    public void verifyShopForAccessoriesCTAOnOrderConfirmationPageUsingAALBuyNewPhoneWithEIP(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US415053: Order Confirmation Page");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on shop link | Shop page should be displayed");
        Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
        Reporter.log("6. Click on 'Buy New Phone' option | PLP page should be displayed");
        Reporter.log("7. Select a Product from PLP page | PDP page should be displayed");
        Reporter.log("8. Select EIP and Click 'Continue' button on PDP page | Rate Plan page should be displayed");
        Reporter.log("9. Click 'Continue' button on Rate plan page | DeviceProtection Page should be displayed");
        Reporter.log("10. Click 'Continue' button  | Cart Page should be displayed");
        Reporter.log("11. Click Continue to Shipping CTA on Cart Page | Shipping details info should be Displayed");
        Reporter.log("12. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
        Reporter.log("13. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
        Reporter.log("14. Click on Accept & Continue CTA | Order should be processed and Signing document page should be displayed(If Applicable)");
        Reporter.log("15. Complete Signing document process(If Applicable) | Order Confirmation page should be displayed");
        Reporter.log("16. Verify 'THANK YOU!' text along with Authorable image presence | Authorable image and text should be displayed");
        Reporter.log("17. Verify Order details section & Transfer number section presence | Order details section & Transfer number section's should be displayed");
        Reporter.log("18. Verify 'Shop for accessories' & 'Continue Shopping' CTA's presence | 'Shop for accessories' & 'Continue Shopping' CTA's should be displayed");
        Reporter.log("19. Click 'Shop for accessories'CTA | User should be redirected to Accessories stand alone page");

        Reporter.log("================================");
        Reporter.log("Actual Output:");

        OrderConfirmationPage orderConfirmationPage = navigateToOrderConformationAALNonBYOD(myTmoData);
        orderConfirmationPage.verifyOrderConfirmationPage();
        orderConfirmationPage.verifyThankYouText();
        orderConfirmationPage.verifyThankYouImage();
        orderConfirmationPage.verifyOrderDetailsSection();
        orderConfirmationPage.verifyTarnsferNumberSection();
        orderConfirmationPage.verifyShopForAccessory();
        orderConfirmationPage.verifyContinueToShopping();
        orderConfirmationPage.clickShopAccessoriesCTA();
        AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
        accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();

    }

    /**
     * US415053: Order Confirmation Page
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
    public void verifyContinueShoppingCTAOnOrderConfirmationPageUsingAALBuyNewPhoneWithFRP(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US415053: Order Confirmation Page");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on shop link | Shop page should be displayed");
        Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
        Reporter.log("6. Click on 'Buy New Phone' option | PLP page should be displayed");
        Reporter.log("7. Select a Product from PLP page | PDP page should be displayed");
        Reporter.log("8. Select FRP and click 'Continue' button on PDP page | Rate Plan page should be displayed");
        Reporter.log("9. Click 'Continue' button on Rate plan page | DeviceProtection Page should be displayed");
        Reporter.log("10. Click 'Continue' button  | Cart Page should be displayed");
        Reporter.log("11. Click Continue to Shipping CTA on Cart Page | Shipping details info should be Displayed");
        Reporter.log("12. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
        Reporter.log("13. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
        Reporter.log("14. Click on Accept & Continue CTA | Order should be processed and Fraud Check pages should be displayed(If Applicable)");
        Reporter.log("15. Complete Fraud check process(If Applicable) | Order Confirmation page should be displayed");
        Reporter.log("16. Verify 'THANK YOU!' text along with Authorable image presence | Authorable image and text should be displayed");
        Reporter.log("17. Verify Order details section & Transfer number section presence | Order details section & Transfer number section's should be displayed");
        Reporter.log("18. Verify 'Shop for accessories' & 'Continue Shopping' CTA's presence | 'Shop for accessories' & 'Continue Shopping' CTA's should be displayed");
        Reporter.log("19. Click 'Continue Shopping' CTA | User should be redirected to Shop page");

        Reporter.log("================================");
        Reporter.log("Actual Output:");

        OrderConfirmationPage orderConfirmationPage = navigateToOrderConformationAALNonBYOD(myTmoData);
        orderConfirmationPage.verifyThankYouText();
        orderConfirmationPage.verifyThankYouImage();
        orderConfirmationPage.verifyOrderDetailsSection();
        orderConfirmationPage.verifyTarnsferNumberSection();
        orderConfirmationPage.verifyShopForAccessory();
        orderConfirmationPage.verifyContinueToShopping();
        orderConfirmationPage.clickContinueToShopping();
        ShopPage shopPage = new ShopPage(getDriver());
        shopPage.verifyPageUrl();

    }

    /**
     * US415064: Order Details Page - order details section
     * US415068: Order Details Page - Transfer Number instructions
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
    public void verifyOrderDetailsAndTransferNumberInstructionsOnOrderConfirmationPageUsingAALBYOD(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US415064: Order Details Page - order details section &" +
                " US415068: Order Details Page - Transfer Number instructions");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on shop link | Shop page should be displayed");
        Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
        Reporter.log("6. Click on 'Use my own phone' option | Rate page should be displayed");
        Reporter.log("7. Click 'Continue' button on Rate plan page | Cart Page should be displayed");
        Reporter.log("8. Click on 'Continue to shipping' CTA on cart page | Shipping details info should be displayed");
        Reporter.log("9. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
        Reporter.log("10. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
        Reporter.log("11. Click on Accept & Continue CTA | Fraud check page(If applicable) is displayed else Order should be Placed and Order Confirmation page should be displayed");
        Reporter.log("12. Verify 'Order details' section | Order Details section should be displayed");
        Reporter.log("13. Verify Order Number presence | Order Number details should be displayed");
        Reporter.log("14. Verify 'Account email' details  | Email confirmation details should be displayed");
        Reporter.log("15. Verify 'Transfer number' section | Transfer your number authorable image, header, text should be displayed");

        Reporter.log("================================");
        Reporter.log("Actual Output:");
        
        OrderConfirmationPage orderConfirmationPage = navigateToOrderConformationAALBYOD(myTmoData);
        orderConfirmationPage.verifyOrderDetailsSection();
        orderConfirmationPage.verifyOrderNumber();
        orderConfirmationPage.verifyOrderEmail();
        orderConfirmationPage.verifyTarnsferNumberSection();
        orderConfirmationPage.verifyTransferImage();
        orderConfirmationPage.verifyTransferText();

    }


    /**
     *  US415064: Order Details Page - order details section
     *  US415068: Order Details Page - Transfer Number instructions
     *
     *  @param data
     *  @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
    public void verifyOrderDetailsAndTransferNumberInstructionsOnOrderConfirmationPageUsingAALBuyNewPhone(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US415064: Order Details Page - order details section &" +
                " US415068: Order Details Page - Transfer Number instructions");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on shop link | Shop page should be displayed");
        Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
        Reporter.log("6. Click on 'Buy New Phone' option | PLP page should be displayed");
        Reporter.log("7. Select a Product from PLP page | PDP page should be displayed");
        Reporter.log("8. Click 'Continue' button on PDP page | Rate Plan page should be displayed");
        Reporter.log("9. Click 'Continue' button on Rate plan page | DeviceProtection Page should be displayed");
        Reporter.log("10. Click 'Continue' button  | Cart Page should be displayed");
        Reporter.log("11. Click Continue to Shipping CTA on Cart Page | Shipping details info should be Displayed");
        Reporter.log("12. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
        Reporter.log("13. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
        Reporter.log("14. Click on Accept & Continue CTA | Order should be processed and Fraud Check pages should be displayed(If Applicable)");
        Reporter.log("15. Complete Fraud check process(If Applicable) | Order Confirmation page should be displayed");
        Reporter.log("12. Verify 'Order details' section | Order Details section should be displayed");
        Reporter.log("13. Verify Order Number | Order Number details should be displayed");
        Reporter.log("14. Verify 'Estimated Ship date' presence | Estimated Ship date details should be displayed");
        Reporter.log("15. Verify 'Account email' details  | Email confirmation details should be displayed");
        Reporter.log("16. Verify 'Transfer number' section | Transfer your number authorable image, header, text should be displayed");

        Reporter.log("================================");
        Reporter.log("Actual Output:");
        
        OrderConfirmationPage orderConfirmationPage = navigateToOrderConformationAALNonBYOD(myTmoData);
        orderConfirmationPage.verifyOrderDetailsSection();
        orderConfirmationPage.verifyOrderNumber();
        orderConfirmationPage.verifyOrderEmail();
        orderConfirmationPage.verifyTarnsferNumberSection();
        orderConfirmationPage.verifyTransferImage();
        orderConfirmationPage.verifyTransferText();
        orderConfirmationPage.verifyShippingDate();

    }
    
    /**
     *  US511120: AAL: Order Details Page - Show shipping date for BYOD 
     *  @param data
     *  @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
    public void verifyEstimatedShippingDateForSimCardInOrderDetailsSection(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test US511120: AAL: Order Details Page - Show shipping date for BYOD");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button | Device intent page should be displayed");			
		Reporter.log("6. Select on 'Use my own phone' option | Consolidated Rate Plan Page  should be displayed");		
		Reporter.log("7. Click Contune button | Cart Page should be displayed");		
        Reporter.log("8. Click Continue to Shipping CTA on Cart Page | Shipping details info should be Displayed");
        Reporter.log("9. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
        Reporter.log("10. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
        Reporter.log("11. Click on Accept & Continue CTA | Order should be processed and Fraud Check pages should be displayed(If Applicable)");
        Reporter.log("12. Complete Fraud check process(If Applicable) | Order Confirmation page should be displayed");
        Reporter.log("13. Verify 'Order details' section | Order Details section should be displayed");
        Reporter.log("14. Verify Order Number | Order Number details should be displayed");
        Reporter.log("15. Verify 'Estimated Ship date' presence | Estimated Ship date details should be displayed");
        
        Reporter.log("================================");
        Reporter.log("Actual Output:");
        
        OrderConfirmationPage orderConfirmationPage = navigateToOrderConformationAALBYOD(myTmoData);
        orderConfirmationPage.verifyOrderDetailsSection();
        orderConfirmationPage.verifyOrderNumber();
        orderConfirmationPage.verifyShippingDate();

    }

    /**
     * US487389 - Trade In - Order Confirmation display Next Steps (android)
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
    public void testOrderConfirmationDisplayTradeInNextStepsInAndroid(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test US487389 - Trade In - Order Confirmation display Next Steps (android)");
        Reporter.log("Data Condition | PAH User With AAL, Execution For Android only");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User should login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Shop link | Shop page should be displayed");
        Reporter.log("5. Click on Add a Line button from quicklinks | Customer intent page should be displayed");
        Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page should be displayed");
        Reporter.log("7. Click Contune button | PLP page should be displayed ");
        Reporter.log("8. Select device | PDP page should be displayed ");
        Reporter.log("9. Click Continue button | Interstitial page should be displayed ");
        Reporter.log("10. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
        Reporter.log("11. Verify Authorable title | Authorable title should be displayed");
        Reporter.log("12. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
        Reporter.log("13. Click on Continue button | Device condition page should be displayed");
        Reporter.log("14. Verify good condition authorable title | Good condition authorable title should be displayed");
        Reporter.log("15. Select good condition option | Device value page should be displayed");
        Reporter.log("16. Verify Trade-In this Phone CTA | Trade-In this Phone CTA should be displayed");
        Reporter.log("17. Click on Trade-In this Phone CTA | Device protection page should be displayed");
        Reporter.log("18. Click on Continue button | Accessories Plp page should be displayed");
        Reporter.log("19. Click on Skip Accessories button | Cart page should be displayed");
        Reporter.log("20. Click on 'Continue to Shipping' button | Shipping details should be displayed");
        Reporter.log("21. Click on 'Continue to Payment' button | Payment information form should be displayed");
        Reporter.log("22. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
        Reporter.log("23. Verify 'DRP Customer Agreement' & 'DRP Terms & Conditions' links avaialbility(If Applicable) | 'DRP Customer Agreement' & 'DRP Terms & Conditions' links should" +
                " be displayed(If Applicable) ");
        Reporter.log("24. Click on Accept & Continue CTA | Order should be processed and Fraud Check pages should be displayed(If Applicable)");
        Reporter.log("25. Complete Fraud check process(If Applicable) | Order Confirmation page should be displayed");
        Reporter.log("26. Verify 'Order details' section | Order Details section should be displayed");
        Reporter.log("27. Verify Order Number | Order Number details should be displayed");
        Reporter.log("28. Verify Trade-In text i.e'TRADE-IN, NEXT STEPS TO GET YOUR CREDITS' | Trade-In text should be displayed should be displayed");
        Reporter.log("29. Verify Receive E-Mail option  | Receive E-Mail option should be displayed");
        Reporter.log("30. Verify 'Remember to send back your old phone' option  | 'Remember to send back your old phone' option should be displayed");

        Reporter.log("================================");
        Reporter.log("Actual Output:");
        
        navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifySkipTradeinTile();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.verifyTradeInAnotherDeviceTitle();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage TradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(getDriver());
		TradeInDeviceConditionValuePage.verifyGoodTitleTextOnDeviceConditionAAL();
		TradeInDeviceConditionValuePage.clickOnGoodCondition();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickContinueTradeInButton();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		if(identityReviewIntroductionPage.verifyPageUrl()) {
			identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
			identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
			IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
			identityReviewQuestionPage.verifyReviewQuestionPage();
			identityReviewQuestionPage.clickFirstOption();
			identityReviewQuestionPage.clickSubmit();
		}
		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
		orderConfirmationPage.verifyOrderConfirmationPage();
		orderConfirmationPage.verifyOrderDetailsSection();
	    orderConfirmationPage.verifyOrderNumber();
	    orderConfirmationPage.verifyTradeInText();
	    orderConfirmationPage.verifyReceiveEmailOption();
	    orderConfirmationPage.verifyRememberToSend();
	
    }

    /**
     * US487390 - Trade In - Order Confirmation display Next Steps (apple)
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING_REGRESSION})
    public void testOrderConfirmationDisplayTradeInNextStepsInIOS(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test US487390 - Trade In - Order Confirmation display Next Steps (apple)");
        Reporter.log("Data Condition | PAH User With AAL, Execution For IOS only");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User should login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Shop link | Shop page should be displayed");
        Reporter.log("5. Click on Add a Line button from quicklinks | Customer intent page should be displayed");
        Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page should be displayed");
        Reporter.log("7. Click Contune button | PLP page should be displayed ");
        Reporter.log("8. Select device | PDP page should be displayed ");
        Reporter.log("9. Click Continue button | Interstitial page should be displayed ");
        Reporter.log("10. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
        Reporter.log("11. Verify Authorable title | Authorable title should be displayed");
        Reporter.log("12. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
        Reporter.log("13. Click on Continue button | Device condition page should be displayed");
        Reporter.log("14. Verify good condition authorable title | Good condition authorable title should be displayed");
        Reporter.log("15. Select good condition option | Device value page should be displayed");
        Reporter.log("16. Verify Trade-In this Phone CTA | Trade-In this Phone CTA should be displayed");
        Reporter.log("17. Click on Trade-In this Phone CTA | Device protection page should be displayed");
        Reporter.log("18. Click on Continue button | Accessories Plp page should be displayed");
        Reporter.log("19. Click on Skip Accessories button | Cart page should be displayed");
        Reporter.log("20. Click on 'Continue to Shipping' button | Shipping details should be displayed");
        Reporter.log("21. Click on 'Continue to Payment' button | Payment information form should be displayed");
        Reporter.log("22. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
        Reporter.log("23. Verify 'DRP Customer Agreement' & 'DRP Terms & Conditions' links avaialbility(If Applicable) | 'DRP Customer Agreement' & 'DRP Terms & Conditions' links should" +
                " be displayed(If Applicable) ");
        Reporter.log("24. Click on Accept & Continue CTA | Order should be processed and Fraud Check pages should be displayed(If Applicable)");
        Reporter.log("25. Complete Fraud check process(If Applicable) | Order Confirmation page should be displayed");
        Reporter.log("26. Verify 'Order details' section | Order Details section should be displayed");
        Reporter.log("27. Verify Order Number | Order Number details should be displayed");
        Reporter.log("28. Verify Trade-In text i.e'TRADE-IN, NEXT STEPS TO GET YOUR CREDITS' | Trade-In text should be displayed should be displayed");
        Reporter.log("29. Verify Receive E-Mail option  | Receive E-Mail option should be displayed");
        Reporter.log("30. Verify 'Remember to send back your old phone' option  | 'Remember to send back your old phone' option should be displayed");
        Reporter.log("31. Verify 'And don't forget to turn off 'find my iphone' option | 'And don't forget to turn off 'find my iphone' option should be displayed ");

        Reporter.log("================================");
        Reporter.log("Actual Output:");
        
        navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifySkipTradeinTile();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.verifyTradeInAnotherDeviceTitle();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage TradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(getDriver());
		TradeInDeviceConditionValuePage.verifyGoodTitleTextOnDeviceConditionAAL();
		TradeInDeviceConditionValuePage.clickOnGoodCondition();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickContinueTradeInButton();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		if(identityReviewIntroductionPage.verifyPageUrl()) {
			identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
			identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
			IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
			identityReviewQuestionPage.verifyReviewQuestionPage();
			identityReviewQuestionPage.clickFirstOption();
			identityReviewQuestionPage.clickSubmit();
		}
		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
		orderConfirmationPage.verifyOrderConfirmationPage();
		orderConfirmationPage.verifyOrderDetailsSection();
	    orderConfirmationPage.verifyOrderNumber();
	    orderConfirmationPage.verifyTradeInText();
	    orderConfirmationPage.verifyReceiveEmailOption();
	    orderConfirmationPage.verifyRememberToSend();
	    orderConfirmationPage.verifyforgetToTurnOffOption();
    }
    
    /**
	 * US570031 Tier6 - AAL Enrollment - Test only	
	 * 		C519163: 01_AAL_NonNY_GSM_Enrollment_With Tier 6 Protection soc_End to End	
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TIER_SIX})
	public void testTireSixProtectionSocNonNYGsmUserNavigatedToOrderConformationPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("TestCase: US570031 Tier6 - AAL Enrollment - Test only");
		Reporter.log("Data Condition | PAH user and User should have TM01 plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");		
		Reporter.log("4. Click on 'Add A LINE' button | Device intent page should be displayed");
		Reporter.log("5. Click on buy a new phone option | RatePlan page should be displayed");		
		Reporter.log("6. Click on Continue button | PLP page should be displayed");		
		Reporter.log("7. Select on Deive which is compatible for Tier 6 soc  | PDP Page is Displayed");
		Reporter.log("8. Select EIP from payment option | EIP Payment option should be selected");		
		Reporter.log("9. Click on Add to cart button | Interstitial-tradein page should be displayed");		
		Reporter.log("10. Click on Skip trade-in option | Device protection page should be displayed");
		Reporter.log("11. Verify Tier-6 Soc | Tire-6 soc should be displayed");
		Reporter.log("12. Select Tier-6 soc and Click Continue button | CartPage should be displayed");
		Reporter.log("13. Click Continue to Shipping CTA | Shipping Tab should be displayed");
		Reporter.log("14. Click Continue to Payment | Payment Information form page should be displayed");
		Reporter.log("15. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
		Reporter.log("16. Click on Accept & Continue CTA | Identity Review Introduction page should be enabled");
		Reporter.log("17. Click on Continue CTA | Identity Review Question page should be enabled");
		Reporter.log("18. Select questions and submit button | Order conformation page should be enabled");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOtherProtectionOption();
		deviceProtectionpage.verifyTier6SOCNonNYPresent();
		deviceProtectionpage.selectTier6SocNonNY();
		deviceProtectionpage.clickContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
//		cartPage.clickAcceptAndPlaceOrder();
//		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
//		if(identityReviewIntroductionPage.verifyPageUrl()) {
//			identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
//			identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
//			IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
//			identityReviewQuestionPage.verifyReviewQuestionPage();
//			identityReviewQuestionPage.clickFirstOption();
//			identityReviewQuestionPage.clickSubmit();
//		}
//		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
//		orderConfirmationPage.verifyOrderConfirmationPage();
	}
	
	 	/**
		 * US570031 Tier6 - AAL Enrollment - Test only	
		 * 		C519177: 02_AAL_NonNY_GSM_Enrollment_With Tier 6 Basic soc_End to End
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TIER_SIX})
		public void testTireSixBasicSocNonNYGsmUserNavigatedToOrderConformationPage(ControlTestData data, MyTmoData myTmoData) {
			Reporter.log("TestCase: US570031 Tier6 - AAL Enrollment - Test only");
			Reporter.log("Data Condition | PAH user and User should have TM01 plan");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | Home page should be displayed");
			Reporter.log("3. Click on shop | shop page should be displayed");		
			Reporter.log("4. Click on 'Add A LINE' button | Device intent page should be displayed");
			Reporter.log("5. Click on buy a new phone option | RatePlan page should be displayed");		
			Reporter.log("6. Click on Continue button | PLP page should be displayed");		
			Reporter.log("7. Select on Deive which is compatible for Tier 6 soc  | PDP Page is Displayed");
			Reporter.log("8. Select EIP from payment option | EIP Payment option should be selected");		
			Reporter.log("9. Click on Add to cart button | Interstitial-tradein page should be displayed");		
			Reporter.log("10. Click on Skip trade-in option | Device protection page should be displayed");
			Reporter.log("11. Verify Tier-6 Basic Soc | Tire-6 basic soc should be displayed");
			Reporter.log("12. Select Tier-6 basic soc and Click Continue button | CartPage should be displayed");
			Reporter.log("13. Click Continue to Shipping CTA | Shipping Tab should be displayed");
			Reporter.log("14. Click Continue to Payment | Payment Information form page should be displayed");
			Reporter.log("15. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
			Reporter.log("16. Click on Accept & Continue CTA | Identity Review Introduction page should be enabled");
			Reporter.log("17. Click on Continue CTA | Identity Review Question page should be enabled");
			Reporter.log("18. Select questions and submit button | Order conformation page should be enabled");
			
			Reporter.log("================================");
			Reporter.log("Actual Output:");
			
			navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
			InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
			interstitialTradeInPage.clickOnSkipTradeInCTA();
			DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
			deviceProtectionpage.verifyDeviceProtectionPage();
			deviceProtectionpage.clickOtherProtectionOption();
			deviceProtectionpage.verifyTier6SOCNonNYPresent();
			deviceProtectionpage.selectTier6SocNonNYBasic();
			deviceProtectionpage.clickContinueButton();
			CartPage cartPage = new CartPage(getDriver());
			cartPage.verifyCartPage();
			cartPage.clickContinueToShippingButton();
			cartPage.clickContinueToPaymentButton();
			cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterCardNumber(myTmoData.getCardNumber());
			cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
			cartPage.enterCVV(myTmoData.getPayment().getCvv());
			cartPage.clickServiceCustomerAgreementCheckBox();
//			cartPage.clickAcceptAndPlaceOrder();
//			IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
//			if(identityReviewIntroductionPage.verifyPageUrl()) {
//				identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
//				identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
//				IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
//				identityReviewQuestionPage.verifyReviewQuestionPage();
//				identityReviewQuestionPage.clickFirstOption();
//				identityReviewQuestionPage.clickSubmit();
//			}
//			OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
//			orderConfirmationPage.verifyOrderConfirmationPage();
		}
		
		/**
		 * US570031 Tier6 - AAL Enrollment - Test only	
		 * 		C519178: 03_AAL_NonNY_GSM_Enrollment_With Tier 6 no I dont want protection_End to End
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TIER_SIX})
		public void testNonNYGsmUserNavigatedToOrderConformationPageWithOutInsuranceProtectionOption(ControlTestData data, MyTmoData myTmoData) {
			Reporter.log("TestCase: US570031 Tier6 - AAL Enrollment - Test only");
			Reporter.log("Data Condition | PAH user and User should have TM01 plan");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | Home page should be displayed");
			Reporter.log("3. Click on shop | shop page should be displayed");		
			Reporter.log("4. Click on 'Add A LINE' button | Device intent page should be displayed");
			Reporter.log("5. Click on buy a new phone option | RatePlan page should be displayed");		
			Reporter.log("6. Click on Continue button | PLP page should be displayed");		
			Reporter.log("7. Select on Deive which is compatible for Tier 6 soc  | PDP Page is Displayed");
			Reporter.log("8. Select EIP from payment option | EIP Payment option should be selected");		
			Reporter.log("9. Click on Add to cart button | Interstitial-tradein page should be displayed");		
			Reporter.log("10. Click on Skip trade-in option | Device protection page should be displayed");
			Reporter.log("11. Click on Other protection option link | 'I don'tneed protection for my phone' option should be displayed");
			Reporter.log("12. Select 'I don't need protection for my phone' option and Click on Continue button | Warning message model should be displayed");
			Reporter.log("13. Click on 'Yes, I'm sure' button | Cart page should be displayed");
			Reporter.log("14. Verify Insurance soc | Insurance soc should not be displayed in cart page");
			Reporter.log("15. Click Continue to Shipping CTA | Shipping Tab should be displayed");
			Reporter.log("16. Click Continue to Payment | Payment Information form page should be displayed");
			Reporter.log("17. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
			Reporter.log("18. Click on Accept & Continue CTA | Identity Review Introduction page should be enabled");
			Reporter.log("19. Click on Continue CTA | Identity Review Question page should be enabled");
			Reporter.log("20. Select questions and submit button | Order conformation page should be enabled");
			
			Reporter.log("================================");
			Reporter.log("Actual Output:");
			
			navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
			InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
			interstitialTradeInPage.clickOnSkipTradeInCTA();
			DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
			deviceProtectionpage.verifyDeviceProtectionPage();
			deviceProtectionpage.clickOtherProtectionOption();
			deviceProtectionpage.clickDontNeedProtectionRadioButton();
			deviceProtectionpage.clickContinueButton();
			deviceProtectionpage.clickYesIamSureButton();
			CartPage cartPage = new CartPage(getDriver());
			cartPage.verifyCartPage();
			cartPage.verifyNoDeviceProtection();
			cartPage.clickContinueToShippingButton();
			cartPage.clickContinueToPaymentButton();
			cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterCardNumber(myTmoData.getCardNumber());
			cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
			cartPage.enterCVV(myTmoData.getPayment().getCvv());
			cartPage.clickServiceCustomerAgreementCheckBox();
//			cartPage.clickAcceptAndPlaceOrder();
//			IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
//			if(identityReviewIntroductionPage.verifyPageUrl()) {
//				identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
//				identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
//				IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
//				identityReviewQuestionPage.verifyReviewQuestionPage();
//				identityReviewQuestionPage.clickFirstOption();
//				identityReviewQuestionPage.clickSubmit();
//			}
//			OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
//			orderConfirmationPage.verifyOrderConfirmationPage();
		}
		
		/**
		 * US570032 Tier 6 : NY Standalone Socs - AAL - Test Only	
		 * 		C519210: 01_AAL_NY_GSM_Enrollment_With Tier 6 Protection soc_End to End	
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION , Group.TIER_SIX })
		public void testTireSixProtectionSocNYGsmUserNavigatedToOrderConformationPage(ControlTestData data, MyTmoData myTmoData) {
			Reporter.log("TestCase: US570032 Tier 6 : NY Standalone Socs - AAL - Test Only");
			Reporter.log("Data Condition | NY PAH user and User should have TM01 plan");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | Home page should be displayed");
			Reporter.log("3. Click on shop | shop page should be displayed");		
			Reporter.log("4. Click on 'Add A LINE' button | Device intent page should be displayed");
			Reporter.log("5. Click on buy a new phone option | RatePlan page should be displayed");		
			Reporter.log("6. Click on Continue button | PLP page should be displayed");		
			Reporter.log("7. Select on Deive which is compatible for Tier 6 soc  | PDP Page is Displayed");
			Reporter.log("8. Select EIP from payment option | EIP Payment option should be selected");		
			Reporter.log("9. Click on Add to cart button | Interstitial-tradein page should be displayed");		
			Reporter.log("10. Click on Skip trade-in option | Device protection page should be displayed");
			Reporter.log("11. Verify Tier-6 Soc | Tire-6 soc should be displayed");
			Reporter.log("12. Select Tier-6 soc and Click Continue button | CartPage should be displayed");
			Reporter.log("13. Verify Insurance soc | Insurance soc should not be displayed in cart page");
			Reporter.log("14. Click Continue to Shipping CTA | Shipping Tab should be displayed");
			Reporter.log("15. Click Continue to Payment | Payment Information form page should be displayed");
			Reporter.log("16. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
			Reporter.log("17. Click on Accept & Continue CTA | Identity Review Introduction page should be enabled");
			Reporter.log("18. Click on Continue CTA | Identity Review Question page should be enabled");
			Reporter.log("19. Select questions and submit button | Order conformation page should be enabled");
			
			Reporter.log("================================");
			Reporter.log("Actual Output:");
			
			navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
			InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
			interstitialTradeInPage.clickOnSkipTradeInCTA();
			DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
			deviceProtectionpage.verifyDeviceProtectionPage();
			deviceProtectionpage.verifyAllNYSOCPresent();
			deviceProtectionpage.selectTier6SocNY();
			deviceProtectionpage.clickContinueButton();
			CartPage cartPage = new CartPage(getDriver());
			cartPage.verifyCartPage();
			cartPage.clickContinueToShippingButton();
			cartPage.clickContinueToPaymentButton();
			cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterCardNumber(myTmoData.getCardNumber());
			cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
			cartPage.enterCVV(myTmoData.getPayment().getCvv());
			cartPage.clickServiceCustomerAgreementCheckBox();
			cartPage.clickAcceptAndPlaceOrder();
//			IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
//			if(identityReviewIntroductionPage.verifyPageUrl()) {
//				identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
//				identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
//				IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
//				identityReviewQuestionPage.verifyReviewQuestionPage();
//				identityReviewQuestionPage.clickFirstOption();
//				identityReviewQuestionPage.clickSubmit();
//			}
//			OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
//			orderConfirmationPage.verifyOrderConfirmationPage();
		}
		
		/**
		 * US570032 Tier 6 : NY Standalone Socs - AAL - Test Only	
		 * 		C519211: 02_AAL_NY_GSM_Enrollment_With Tier 6 Basic soc_End to End
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TIER_SIX })
		public void testTireSixBasicSocNYGsmUserNavigatedToOrderConformationPage(ControlTestData data, MyTmoData myTmoData) {
			Reporter.log("TestCase: US570032 Tier 6 : NY Standalone Socs - AAL - Test Only");
			Reporter.log("Data Condition | NY PAH user and User should have TM01 plan");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | Home page should be displayed");
			Reporter.log("3. Click on shop | shop page should be displayed");		
			Reporter.log("4. Click on 'Add A LINE' button | Device intent page should be displayed");
			Reporter.log("5. Click on buy a new phone option | RatePlan page should be displayed");		
			Reporter.log("6. Click on Continue button | PLP page should be displayed");		
			Reporter.log("7. Select on Deive which is compatible for Tier 6 soc  | PDP Page is Displayed");
			Reporter.log("8. Select EIP from payment option | EIP Payment option should be selected");		
			Reporter.log("9. Click on Add to cart button | Interstitial-tradein page should be displayed");		
			Reporter.log("10. Click on Skip trade-in option | Device protection page should be displayed");
			Reporter.log("11. Verify Tier-6 Basic Soc | Tire-6 basic soc should be displayed");
			Reporter.log("12. Select Tier-6 basic soc and Click Continue button | CartPage should be displayed");
			Reporter.log("13. Verify Insurance soc | Insurance soc should be displayed in cart page");
			Reporter.log("14. Click Continue to Shipping CTA | Shipping Tab should be displayed");
			Reporter.log("15. Click Continue to Payment | Payment Information form page should be displayed");
			Reporter.log("16. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
			Reporter.log("17. Click on Accept & Continue CTA | Identity Review Introduction page should be enabled");
			Reporter.log("18. Click on Continue CTA | Identity Review Question page should be enabled");
			Reporter.log("19. Select questions and submit button | Order conformation page should be enabled");
			
			Reporter.log("================================");
			Reporter.log("Actual Output:");
			
			navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
			InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
			interstitialTradeInPage.clickOnSkipTradeInCTA();
			DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
			deviceProtectionpage.verifyDeviceProtectionPage();
			deviceProtectionpage.verifyAllNYSOCPresent();
			deviceProtectionpage.selectTier6SocNYBasic();
			deviceProtectionpage.clickContinueButton();
			CartPage cartPage = new CartPage(getDriver());
			cartPage.verifyCartPage();
			cartPage.clickContinueToShippingButton();
			cartPage.clickContinueToPaymentButton();
			cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterCardNumber(myTmoData.getCardNumber());
			cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
			cartPage.enterCVV(myTmoData.getPayment().getCvv());
			cartPage.clickServiceCustomerAgreementCheckBox();
//			cartPage.clickAcceptAndPlaceOrder();
//			IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
//			if(identityReviewIntroductionPage.verifyPageUrl()) {
//				identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
//				identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
//				IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
//				identityReviewQuestionPage.verifyReviewQuestionPage();
//				identityReviewQuestionPage.clickFirstOption();
//				identityReviewQuestionPage.clickSubmit();
//			}
//			OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
//			orderConfirmationPage.verifyOrderConfirmationPage();
		}
		
		/**
		 * US570032 Tier 6 : NY Standalone Socs - AAL - Test Only	
		 * 		C519178: 03_AAL_NonNY_GSM_Enrollment_With Tier 6 no I dont want protection_End to End
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION , Group.TIER_SIX })
		public void testNYGsmUserNavigatedToOrderConformationPageWithOutInsuranceProtectionOption(ControlTestData data, MyTmoData myTmoData) {
			Reporter.log("TestCase: US570032 Tier 6 : NY Standalone Socs - AAL - Test Only");
			Reporter.log("Data Condition | NY PAH user and User should have TM01 plan");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | Home page should be displayed");
			Reporter.log("3. Click on shop | shop page should be displayed");		
			Reporter.log("4. Click on 'Add A LINE' button | Device intent page should be displayed");
			Reporter.log("5. Click on buy a new phone option | RatePlan page should be displayed");		
			Reporter.log("6. Click on Continue button | PLP page should be displayed");		
			Reporter.log("7. Select on Deive which is compatible for Tier 6 soc  | PDP Page is Displayed");
			Reporter.log("8. Select EIP from payment option | EIP Payment option should be selected");		
			Reporter.log("9. Click on Add to cart button | Interstitial-tradein page should be displayed");		
			Reporter.log("10. Click on Skip trade-in option | Device protection page should be displayed");
			Reporter.log("11. Click on Other protection option link | 'I don'tneed protection for my phone' option should be displayed");
			Reporter.log("12. Select 'I don't need protection for my phone' option and Click on Continue button | Warning message model should be displayed");
			Reporter.log("13. Click on 'Yes, I'm sure' button | Cart page should be displayed");
			Reporter.log("14. Verify Insurance soc | Insurance soc should not be displayed in cart page");
			Reporter.log("15. Click Continue to Shipping CTA | Shipping Tab should be displayed");
			Reporter.log("16. Click Continue to Payment | Payment Information form page should be displayed");
			Reporter.log("17. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
			Reporter.log("18. Click on Accept & Continue CTA | Identity Review Introduction page should be enabled");
			Reporter.log("19. Click on Continue CTA | Identity Review Question page should be enabled");
			Reporter.log("20. Select questions and submit button | Order conformation page should be enabled");
			
			Reporter.log("================================");
			Reporter.log("Actual Output:");
			
			navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
			InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
			interstitialTradeInPage.clickOnSkipTradeInCTA();
			DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
			deviceProtectionpage.verifyDeviceProtectionPage();
			deviceProtectionpage.clickDontNeedProtectionRadioButton();
			deviceProtectionpage.clickContinueButton();
			deviceProtectionpage.clickYesIamSureButton();
			CartPage cartPage = new CartPage(getDriver());
			cartPage.verifyCartPage();
			cartPage.verifyNoDeviceProtection();
			cartPage.clickContinueToShippingButton();
			cartPage.clickContinueToPaymentButton();
			cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterCardNumber(myTmoData.getCardNumber());
			cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
			cartPage.enterCVV(myTmoData.getPayment().getCvv());
			cartPage.clickServiceCustomerAgreementCheckBox();
			cartPage.clickAcceptAndPlaceOrder();
//			IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
//			if(identityReviewIntroductionPage.verifyPageUrl()) {
//				identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
//				identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
//				IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
//				identityReviewQuestionPage.verifyReviewQuestionPage();
//				identityReviewQuestionPage.clickFirstOption();
//				identityReviewQuestionPage.clickSubmit();
//			}
//			OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
//			orderConfirmationPage.verifyOrderConfirmationPage();
		}
		
		
		/**
		 * CDCSM-160-PII Masking : Automation
		 * CDCSM-55-PII Masking : Order Confirmation page Upgrade 
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
		public void testVerifyMaskCustomerSensitiveInformationInOrderConfirmationPageForPAHUser(ControlTestData data, MyTmoData myTmoData) {
			Reporter.log("Test Case :CDCSM-PII Masking : Order Confirmation page Upgrade");		
			Reporter.log("Data Condition | PAH User");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("================================");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | User Should be login successfully");
			Reporter.log("3. Verify Home page | Home page should be displayed");
			Reporter.log("4. Click on Shop link | Shop page should be displayed");		
			Reporter.log("5. Click on See all phone link | PLP page should be displayed");
			Reporter.log("6. Select device | PDP page should be displayed");
			Reporter.log("7. Click on Add to cart button | LineSelector  page should be displayed");
			Reporter.log("8. Mask customer sensitive information(Customer name and Custoemrs MSISDIN) in line selector page"
					+ " using 3rd party tools 'click-tale' | customer sensitive information should be masked ");
			Reporter.log("9. Select a line | LineSelector details page should be displayed");
			Reporter.log("10. Mask customer sensitive information(Customer name and Custoemrs MSISDIN) in line selector details"
					+ " page using 3rd party tools 'click-tale' | customer sensitive information should be masked ");
			Reporter.log("11. Click on Skip TradeIn in line selector details page | DeviceProtection Page should be displayed ");
			Reporter.log("12. Click on Continue on DeviceProtection page | Accessory PLP page should be displayed");
			Reporter.log("13. Click on Skip Accessory button | Cart page should be displayed");
			Reporter.log("14. Click Continue to shipping button | shipping page should be displayed");
			Reporter.log("15. Click Continue to Payment button | Payment information form should be displayed");
			Reporter.log("16. Fill the Payment Information details and check accept and continue CTA | accept and continue CTA should be enabled");
			Reporter.log("17. Click on accept and continue CTA | Order conformation details should be displayed");
			Reporter.log("18. Mask customer sensitive information(Customers email address) in order conformation page using 3rd party tools 'click-tale' | customer sensitive information should be masked ");
		
			Reporter.log("================================");
			Reporter.log("Actual Output:");	
		
			
			
		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageByFeatureDevices(myTmoData);
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.verifyPIIMaskingForMsisdn(ShopConstants.PII_CUSTOMER_MSISDN_PID);
		lineSelectorPage.verifyPIIMaskingForName(ShopConstants.PII_CUSTOMER_NAME_PID);
		
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		
		lineSelectorDetailsPage.verifyPIIMaskingForMsisdn(ShopConstants.PII_CUSTOMER_MSISDN_PID);
		lineSelectorDetailsPage.verifyPIIMaskingForName(ShopConstants.PII_CUSTOMER_NAME_PID);
		
		lineSelectorDetailsPage.clickSkipTradeInCTA();
		//DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		//deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickAcceptAndPlaceOrderForFRPFlow();

		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
		orderConfirmationPage.verifyOrderConfirmationPage();
		orderConfirmationPage.verifyPIIMaskingForEMail(ShopConstants.PII_CUSTOMER_MAIL_PID);
		}
		
		/**
		 * CDCSM-160-PII Masking : Automation
		 * CDCSM-55-PII Masking : Order Confirmation page AAL user 
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
		public void testVerifyMaskCustomerSensitiveInformationInOrderConfirmationPageForAALUser(ControlTestData data, MyTmoData myTmoData) {
			Reporter.log("Test Case :CDCSM-PII Masking : Order Confirmation page AAL user");		
			Reporter.log("Data Condition | PAH User");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("================================");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | User Should be login successfully");
			Reporter.log("3. Verify Home page | Home page should be displayed");
			Reporter.log("4. Click on Shop link | Shop page should be displayed");		
			Reporter.log("5. Click on 'Add A LINE' button | Device intent page should be displayed");
			Reporter.log("6. Click on buy a new phone option | RatePlan page should be displayed");		
			Reporter.log("7. Click on Continue button | PLP page should be displayed");
			Reporter.log("8. Click on continue button  | interstitial-tradein should be displayed");
			Reporter.log("9. Click on Skip TradeIn | DeviceProtection Page should be displayed ");
			Reporter.log("10. Click on Continue on DeviceProtection page | Cart page should be displayed");
			Reporter.log("11. Click Continue to shipping button | shipping page should be displayed");
			Reporter.log("12. Click Continue to Payment button | Payment information form should be displayed");
			Reporter.log("13. Fill the Payment Information details and check accept and continue CTA | accept and continue CTA should be enabled");
			Reporter.log("14. Click on accept and continue CTA | Order conformation details should be displayed");
			Reporter.log("15. Mask customer sensitive information(Customers email address) in order conformation page using 3rd party tools 'click-tale' | customer sensitive information should be masked ");
			
			Reporter.log("================================");
			Reporter.log("Actual Output:");	
		
			CartPage cartPage = navigateToCartPageFromAALBuyNewPhoneFlowUsingInterstitialTradeIn(myTmoData);
			cartPage.clickContinueToShippingButton();
			cartPage.clickContinueToPaymentButton();
			cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterCardNumber(myTmoData.getCardNumber());
			cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
			cartPage.enterCVV(myTmoData.getPayment().getCvv());
			cartPage.clickServiceCustomerAgreementCheckBox();
			cartPage.clickAcceptAndPlaceOrder();
			
			ESignaturePage esignaturePage = new ESignaturePage(getDriver());
		    esignaturePage.verifyESignaturePage();
		    esignaturePage.selectESignatureCheckBox();
		    esignaturePage.clickESignatureContinueCTA();
		    esignaturePage.clickSignatureImage();
		    esignaturePage.clickSelectStyleTab();
		    esignaturePage.clickAdoptSignatureCTA();
		    esignaturePage.clickSendTheOrderCTA();

			
			OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
		    orderConfirmationPage.verifyOrderConfirmationPage();
		    orderConfirmationPage.verifyPIIMaskingForEMail(ShopConstants.PII_CUSTOMER_MAIL_PID);
		    
		}
		
}
