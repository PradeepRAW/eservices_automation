/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AutoPayPage;
import com.tmobile.eservices.qa.pages.payments.FAQPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

/**
 * @author Sam Iurkov
 *
 */
public class FAQPageTest extends PaymentCommonLib{
	
	/**
	 * US283056 GLUE Light Reskin - AutoPay Faq Page
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Regular account. Autopay eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED})
	public void testGLRAutoPayFAQPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US283056 GLUE Light Reskin - AutoPay Faq Page");
		Reporter.log("Data Conditions - Regular account. Autopay eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to Auto Pay landing page  | Auto Pay landing Page should be displayed");
		Reporter.log("Step 3: Click on FAQ link | Auto Pay FAQ Page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		FAQPage fAQPage = navigateToAutoPayFAQPage(myTmoData);
		fAQPage.validateFAQPage();
		fAQPage.clickFAQBackButton();
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyPageLoaded();
	}

}
