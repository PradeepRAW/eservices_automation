package com.tmobile.eservices.qa.shop;

public class ShopConstants {

	//SHOP PAGE OPTIONS
	public static final String SHOP_PAGE_PROMO_FLOW = "promoflow";
	public static final String SHOP_PAGE_SEEALLDEVICE_FLOW = "seeAllPhones";
	public static final String SHOP_PAGE_SELECTDEVICE_FLOW = "selectDevice";

	//OFFER DETAILS OPTIONS
	public static final String CONTINUE_WITH_OFFER = "continuewithoffer";
	public static final String CONTINUE_WITH_OUT_OFFER = "continuewithoutoffer";

	//PDP PAGE OPTIONS
	public static final String PAYMENT_OPTION_EIP = "Monthly payments";
	public static final String PAYMENT_OPTION_FRP = "Full Retail Price";

	//LINE SELECTOR OPTIONS
	public static final String lINE_SELECTOR_FIRSTLINE = "FirstLine";
	public static final String LINE_SELECTOR_SECONDLINE = "SecondLine";
	public static final String LINE_SELECTOR_JUMPLINE = "JumpLine";
	public static final String LINE_SELECTOR_ADDALINE = "AddALine";

	//PHONE SELECTION OPTIONS
	public static final String PHONE_SELECTION_SKIPTRADEIN = "SkipTradeIn";
	public static final String PHONE_SELECTION_SELECTDEVICE = "SelectDevice";
	public static final String PHONE_SELECTION_DIFFERENTPHONE = "DifferentPhone";

	//INSURANCE MIGRATION PAGE
	public static final String INSURANCE_MIGRATION_CONTINUE = "ContinueWithInsurance";
	public static final String INSURANCE_MIGRATION_REMOVE = "RemoveInsurance";

	//CART PAGE
	public static final String CART_EDIT_DEVICE = "Edit";
	public static final String CART_REMOVE_DEVICE = "RemoveDevice";
	public static final String CART_CONTINUE_TO_SHIPPING = "ContinueToShipping";

	public static final String DEVICE_MEMORY_SIZE = "64 GB";
	public static final String LINESELECTOR_PAGE_ERROR = "Line selector page is not displayed";
	public static final String CART_PAGE_ERROR = "Cart page is not displayed";
	public static final String PDP_PAGE_ERROR = "PDP page is not displayed";
	public static final String PDPPAGE_MEMORY_HEADER_ERROR = "Memory header is not displayed";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_HEADER_ERROR = "Down payment model header is not displayed";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_TEXT_ERROR = "Down payment model text is not displayed";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_NOTE_ERROR = "Down payment model note is not displayed";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_HEADER = "Verify down payment model";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_TEXT = "Verify down payment text";
	public static final String PDPPAGE_DOWNPAYMENT_MODEL_NOTE = "Verify down payment note";

	public static final String VERIFY_DEFEAULT_SORT_OPTION = "Verify defeault option";
	public static final String VERIFY_PRICES_LOWTOHIGH = "Prices are not in the low to high order";
	public static final String VERIFY_PRICES_HIGHTOLOW = "Prices are not in the high low order";
	public static final String VERIFY_FEATURED = "Verify featured devices";
	public static final String VERIFY_HIGH_RATING = "High rating is not in order";
	public static final String HIGH_RATING = "Highest rating";
	public static final String PRICE_LOW_TO_HIGH = "Price low to high";
	public static final String PRICE_HIGH_TO_LOW = "Price high to low";
	public static final String FEATURED = "Featured";

	public static final String PLPPAGE_NO_ITEMS_FOUND_ERROR = "No items found is not displayed";
	public static final String PLPPAGE_NO_ITEMS_FOUND_MESSAGE_ERROR = "For more accurate results try removing some of your filters is displayed";

	public static final String PAGINATION_ERROR = "Pagination not displayed on PLP";
	public static final String ELLIPSIS_ERROR = "Ellipsis are not displayed on PLP";
	public static final String ROWS_ERROR = "3 rows not displayed on PLP";
	public static final String DEVICES_ERROR = "4 devices not displayed per row on PLP";

	public static final String VERIFY_HOME_PAGE_DISPLAYED = "Home page is Displayed";
	public static final String VERIFY_PHONE_PAGE = "Phone page is displayed";
	public static final String VERIFY_PAGINATION = "Verify pagination";
	public static final String VERIFY_ELLIPSIS = "Verify ellipsis";
	public static final String VERIFY_ROWS = "Verify 3 rows displayed on PLP";
	public static final String VERIFY_DEVICES = "Verify 4 devices displayed per row on PLP";
	public static final String IPHONE7_PLP_PAGE_ERROR = "iPhone 7 is not displayed in product list page";
	public static final String ACCESSIBILITY_PAGE_ERROR = "Accessibility Page is not displayed";

	public static final String PDPPAGE_SEEMORE_LINK_ERROR = "See more link is not displayed";
	public static final String PDPPAGE_SEELESS_LINK_ERROR = "See less link is not displayed";
	public static final String PDPPAGE_PREORDER_ERROR = "Pre-ordered is not displayed";
	public static final String PDPPAGE_BACKORDER_ERROR = "Back-ordered is not displayed";
	public static final String PDPPAGE_NO_REVIEWS_ERROR = "There are no reviews for this phone text is nod displayed";
	public static final String PDPPAGE_CONTINUE_BUTTON_ERROR = "Continue button is not displayed";
	public static final String PDPPAGE_ADDTOCART_ERROR = "Add to cart button is not displayed";
	public static final String LEGAL_TEXT_PDPPAGE = "If you cancel wireless service, remaining balance on device's full retail price";
	public static final String PLPPAGE_LEGALTEXT_ERROR = "Legal text is not displayed";
	public static final String PDPPAGE_LEGALTEXT_ERROR = "Legal text is not displayed";
	public static final String PDP_FRP_PRICING_ERROR = "PDP FRP price is not matched with PLP FRP price";

	public static final String STATE = "NY";
	public static final String SHIPPING_ADDRESS_ERROR = "Shipping address is not changed";
	public static final String SHIPPING_METHOD = "3-Day shipping";

	public static final String PDP_PAGE_PAYMENT_OPTION_ERROR = "Payment option is not selected";
	public static final String PDP_PAGE_PRICINGOPTION_HEADER_ERROR = "Pricing option header is not displayed";
	public static final String CVV_TOOPTIP_HEADER_ERROR = "CVV tooltip model header is not selected";
	public static final String CVV_TOOPTIP_IMAGE_ERROR = "CVV tooltip model image is not selected";
	public static final String SHOP_PAGE_LEGAL_TEXT_ERROR = "Legal text is not displayed";
	public static final String SHOP_PAGE_SELECTDEVICE_BUTTON_ERROR = "Select device button is not displayed";
	public static final String ACCEPT_AND_CONTINUE_ERROR = "Accept & continue button is enabled";

	public static final String MAKE_DROPDOWN_OPTION_ERROR = "Make dropdown options are not populated";
	public static final String MODEL_DROPDOWN_OPTION_ERROR = "Model dropdown options are not populated";

	public static final String MAKE_DROPDOWN_DEFAULTOPTION_ERROR = "Make dropdown default option is not populated";
	public static final String MODEL_DROPDOWN_DEFAULTOPTION_ERROR = "Model dropdown default option is not populated";
	public static final String CARRIER_DROPDOWN_ERROR = "Carrier dropdon is not displayed";
	public static final String MAKE_DROPDOWN_ERROR = "Make dropdon is not displayed";
	public static final String MODEL_DROPDOWN_ERROR = "Model dropdon is not displayed";
	public static final String CARRIER_DROPDOWN_OPTION = "ATT";
	public static final String CARRIER_DROPDOWN_OPTION_TMOBILE = "T-Mobile";
	public static final String MAKE_DROPDOWN_OPTION = "Apple";
	public static final String MODEL_DROPDOWN_OPTION = "iPad 2 16GB White - ATT";
	public static final String SAMSUNG_MAKE_DROPDOWN_OPTION = "Samsung";
	public static final String SAMSUNG_MODEL_DROPDOWN_OPTION = "SM-J337T J3 16GB Gold - T-Mobile";
	public static final String DEFAULT_DROPDOWN_OPTION = "Select";
	public static final String TRADEIN_ANOTHER_DEVICEPAGE_ERROR = "Trade in another device page is not displayed";
	public static final String INVALID_IMEI_1 = "111111111111111";
	public static final String EIP_IMEI = "013351006591947";
	public static final String INVALID_IMEI_ERROR_MESSAGE = "IMEI invalid number message is not displayed";

	public static final String DEVICE_CONDITIONPAGE_ERROR = "Device conditions page is not displayed";
	public static final String ITS_GOOD_SECTION_ERROR = "It's good section is not displayed";
	public static final String ITS_GOT_ISSUES_SECTION_ERROR = "It's got issues section is not displayed";
	public static final String ITS_GOOD_SECTION_DEVICE_POWERS_ON_ERROR = "Device powers on point is not displayed";
	public static final String ITS_GOOD_SECTION_NO_LIQUID_DAMAGE_ERROR = "No liquid damage point is not displayed";
	public static final String ITS_GOOD_SECTION_NO_SCREEN_DAMAGE_ERROR = "No screen damage point is not displayed";
	public static final String ITS_GOT_ISSUES_SECTION_HAS_LIQUID_DAMAGE_ERROR = "Has liquid damage point is not displayed";
	public static final String ITS_GOT_ISSUES_SECTION_DOES_NOT_POWER_ON_ERROR = "Doesn't power on point is not displayed";
	public static final String ITS_GOT_ISSUES_SECTION_HAS_SCREEN_DAMAGE_ERROR = "Has screen damage point is not displayed";

	public static final String WHATISTHISMODEL_TITLE_ERROR = "What is this model title is not displayed";
	public static final String WHATISTHISMODEL_HEADER_ERROR = "What is this model header is not displayed";
	public static final String WHATISTHISMODEL_BODY_ERROR = "What is this model body is not displayed";
	public static final String WHATISTHISMODEL_DIANIN_NUMBER_ERROR = "What is this model dial in number is not displayed";
	public static final String DIALTONUMBER_IMEI = "*#06#";

	public static final String FIND_MY_IPHONE = "Find my iPhone";
	public static final String ANTI_THEFT_FEATURE_ANDROID_DEVICE = "Have you disabled the anti-theft feature?";
	public static final String DEVICE_CONDITION_RADIO_BUTTONS_ERROR = "Device Codition selection radio buttons are not displayed.";
	public static final String DEVICE_POWERS_ON = "Device powers on";
	public static final String NO_LIQUID_DAMAGE = "No liquid damage";
	public static final String NO_SCREEN_DAMAGE = "No screen damage";
	public static final String DOESNT_POWER_ON = "Doesn't power on";
	public static final String HAS_LIQUID_DAMAGE = "Has liquid damage";
	public static final String HAS_SCREEN_DAMAGE = "Has screen damage";
	public static final String GOOD_POINT_NOT_DISPLAYED = " point is not displayed under It's Good - Device Condition page";
	public static final String BAD_POINT_NOT_DISPLAYED = " point is not displayed under It's Good - Device Condition page";
	public static final String SELECTED_LOCATE_DEVICES = "Successfully selected Locate Device";
	public static final String VERIFY_LANDING_PAGE = " Page Displayed";
	public static final String NO_PHONE_PAGE = "not navigated to phone page";
	public static final String VERIFY_OPTIONS_DISPLAY_IN_PHONE = "Verify Changes Sim button,Stolenreportbutton,DeviceLockstatus button and Temporary Suspension button";

	public static final String VERIFY_DEVICE_IMAGE = "Verify Device Image Displayed";
	public static final String DISPLAY_DEVICE_IMAGE = "Device Image is displayed";
	public static final String VERIFY_REPORT_STOLEN_PAGE = "Verify Report Device as Lost or stolen Page";
	public static final String CHOOSE_LOCATE_DEVICE = "Select Locate device";
	public static final String COMPLETED_STOLEN_DEVICE_PROCESS = "Successfully completed stolen device process";
	public static final String VERIFY_STOLEN_DEVICE_FUNCTION = "Verify Stolen device functionality";
	public static final String VERIFY_SUSPEND_LINE = "verify Suspend line functionality";
	public static final String COMPLETED_SUSPEND_LINE = "Successfully completed suspend line process";
	public static final String VERIFY_ACCEPT_TC = "Verify terms and conditions functionality";
	public static final String COMPLETED_ACCEPTANCE_TC = "Completed- agree terms and conditions";
	public static final String SUBMIT_REPORT_STOLEN = "Click submit for Report stolen";
	public static final String VERIFY_REPORT_STOLEN_DEVICE = "Verify the Stolen device reported";
	public static final String REPORT_STOLEN_PAGE = "Navigating to Device has been reported as stolen";
	public static final String GO_BACK_PHONE_PAGE = "Choose go back to home page";
	public static final String VERIFY_LINE_SUSPENDED_TEXT = "Verify the Line is suspended";
	public static final String CLICK_FOUND_DEVICE_LINK = "click found device link";
	public static final String DISPLAY_LINE_SUSPEND_TEXT = "This line is suspended text should be verified";
	public static final String CHOOSEN_FOUND_DEVICE_LINK = "Successfully selected found device link";
	public static final String VERIFY_HOME_PAGE_ERROR = "Home page is not displayed";
	public static final String DISPLAY_PHONE_PAGE = "Navigated to Phone page";
	public static final String VERIFY_FILE_CLAIM_JUMP = "Verify file claim Jump option in phone page";
	public static final String DISPLAY_FILE_CLAIM_JUMP_OPTION = "File claim for Jump-option selected";
	public static final String CHANGESIM_PAGE_ERROR = "Change sim page is not displayed";
	public static final String CHANGESIM_ERROR_MESSAGE = "To activate your new SIM card, please contact Customer Care by dialing 611 from your T-Mobile phone or toll free at 1 (800) 937-8997";
	public static final String VERIFY_CONTINUE_CANCEL_INFO_MODAL_PAGE = "Verify Continue and Cancel buttons";
	public static final String DISPLAY_CONTINUE_CANCEL_INFO_MODAL_PAGE = "Successfully displayed continue and cancel buttons";

	public static final String OFFER_SEARCH_PAGE_ERROR = "Offer search page is not displayed";
	public static final String VERIFY_OFFER_SEARCH_PAGE = "Offer search page is displayed";
	public static final String PHONE_PAGE_ERROR = "Phone page is not displayed";
	public static final String YOUR_DEVICE = "Selected device is displayed";
	public static final String PHONEPAGE_TRADEIN_LINK_ERROR = "Trade-in link is displayed";
	public static final String PHONEPAGE_TRADEINSTATUS_LINK_ERROR = "Trade-in status link is displayed";
	public static final String PHONEPAGE_TRADEIN_LINK = "Verify Trade-in link ";
	public static final String PHONEPAGE_TRADEINSTATUS_LINK = "Verify Trade-in status link";

	public static final String SHOPPAGE_ERROR_MSG = "Shop Page is not displaying";
	public static final String ACCESSORIES_PAGE_ERROR = "Accessories page is not displayed";
	public static final String VERIFY_ACCESSORIES_PAGE = "Accessories page is displayed";
	public static final String PHONE_PAGE_VERIFY = "User Is Not Navigated To Phone Page.";
	public static final String VERIFY_SHOP_PAGE = "Shop page is loaded";

	public static final String CARDNAME_ERROR_MESSAGE = "Enter a valid name.";
	public static final String CC_ERROR_MESSAGE = "Enter a valid credit card number.";
	public static final String EXPIRYDATE_ERROR_MESSAGE = "Enter a valid expiration date.";
	public static final String CVV_ERROR_MESSAGE = "Enter a valid CVV number.";
	public static final String ADDALINE_VERIFY = "Add a Line Eligibility/Non-Elegibility Text is not displayed in Shop Page.";
	public static final String VERIFY_NOTELIGIBLE_ADDLINE_MATCHED="Add a line not Eligible text is matched";
	public static final String ADDRESSLINE1 = "1 RAVINIA DR STE 1000";
	public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE_JUMP = "I will be charged a monthly Regulatory Programs & Telco Recovery Fee (not a government-required tax or charge) of up to $3.18 (subject to change without notice; plus tax) per line of service. This fee may not apply to certain data devices/services. International rates and roaming charges may apply. Certain rates are subject to change at any time. If I have purchased a device under EIP, I will refer to my EIP agreement for the specific terms and conditions of that program.";
	public static final String ALTERNATE_STEPS = "You may be able to file an insurance claim after you purchase your new device.";
	public static final String GENERAL_LEGAL_TEXT = "You can trade in another device or skip trade-in for now. You will also be able to file an insurance claim toward this device at the end of purchase transaction.";
	public static final String DEVICE_SUSPENDED = "Device suspended";
	public static final String SUSPEND_TEXT = "Suspend text not appeared";
	public static final String FOUND_DEVICE = "I found my device";
	public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE_AAL = "For plans where taxes and fees are not included in the monthly recurring charge: Monthly Regulatory Programs (RPF) & Telco Recovery Fee (TRF) totaling $3.18 per voice line ($0.60 for RPF & $2.58 for TRF) and $1.16 per data only line ($0.15 for RPF & $1.01 for TRF) applies. Taxes approx. 6-28% of bill";
	public static final String VERIFY_MY_CLAIM = "My Claim";
	public static final String $75_PLAN_RATE="75";
	public static final String TMOBILE_ONE_TABLET_PLAN_NAME="T-Mobile ONE Tablet";
	public static final String CUSTOMER_ACCEPTANCE_AGREEMENT_MODEL_TEXT = "By clicking Continue trade-in, you are accepting the offer price and have read and agreed to Customer Acceptance Agreement. Your device's final value will be based upon full valuation of Trade-in device upon receipt.";
	public static final String LANGUAGE_SPANISH = "Spanish";
	public static final String ADDED_15_SOC = "15";
	public static final String SHOP_PLANS_PAGE_ERROR_MSG = "Shope Plans page is not displaying";
	public static final String REGULATORY_PROGRAMS_TELCO_RECOVERY_FEE_ERROR = "AAL - TRF and Total fees details are not updated in legal desclaimer text.";
	public static final String PUERTO_RICO_CUSTOMER_ELIGIBILITY_MESSAGE_ERROR = "Puerto rico customer eligibility message is not displayed";
	public static final String OFFER_DETAILS_PAGE_ERROR = "Offer details page is not displayed";
	public static final String CONTINUE_WITHOUT_THE_OFFER_ERROR = "Continue without the offer link is displayed";
	public static final String LEGAL_TEXT = "On all T-mobile Plans,if congested,top 3% of data users text is not displaying";
	public static final String FREQUENTILY_ASKED_QUESTIONS_ERROR = "Frequently asked questions section is not displayed";
	public static final String ESTIMATED_SHIP_DATE_FORMAT_ERROR = "Estimated ship date is not displayed with forward slashes";
	public static final String PREMIUM_DEVICE_PROTECTION_PLUS_SYMBOL_ERROR = "Plus symbol is displayed for premium device protection null amount";
	public static final String TRADEINVALUEPAGE_ERROR = "Trade-In value page is not displayed";
	public static final String DEVICE_HAS_ISSUE_AGREECONTINUE_ERROR = "Agree and continue is not displayed in device has issue page";
	public static final String REMOVE_MODEL_ERROR = "Remove model is not displayed in cart page";
	public static final String EMPTY_CART_ERROR = "Empty cart text not displayed";
	public static final String CLICKED_ORDER_STATUS_LINK = "Clicked on Order status link";
	public static final String CHECK_ORDER_STATUS_LINK = "Check and Click Order Status link on phone page";
	public static final String ERROR_ORDER_STATUS_PAGE = "Order status page not loaded";
	public static final String DEVICE_HAS_ISSUE_PAGE_ERROR = "Device has issue page is not displayed";
	public static final String DDEVICEHASISSUESPAGE_BACK_ARROW_ERROR = "Back arrow is not displayed";
	public static final String DEVICEHASISSUESPAGE_MAKEANDMODEL_ERROR = "Make & Model options are not displayed";
	public static final String DEVICEHASISSUESPAGE_IMEI_ERROR = "IMEI is not displayed";
	public static final String DEVICEHASISSUESPAGE_TRADEINVALUE_ERROR = "Trade-In value is not displayed";
	public static final String TRADEIN_PAGES_SEDONA_HEADER_DISPLAYED = "Sedona header is displayed in Trade in pages.";
	public static final String NOT_ELIGIBLE_ADDLINE="You are not eligible to add a line.";
	public static final String LEGAL_COPY_ERROR = "Legal copy is not displayed";
	public static final String VALID_IMEI = "026726404663643";
	public static final String PHONE_SELECTION_PAGE_ERROR = "Phone selection page is not displayed";
	public static final String NO_EIP_DETAILS_PAGE = "User Is Not Navigated EIP Details Page";
	public static final String NO_EIP_DEVICE_PAGE = "User Is Not Navigated To EIP Device Payment Page.";
	public static final String NO_REVIEW_PAYMENT_PAGE = "User Is Not Navigated To Review Payment Page";
	public static final String CARD = "Card";
	public static final String REMOVE_MODEL_BODY_TEXT_ERROR = "Remove model body description is not displayed";
	public static final String BILLING_HEADER_ERROR = "Billing header is not displayed";
	public static final String SHIPPINGDETAILS_PAGE_ERROR = "Shipping details page is not displayed";
	public static final String PAYMENT_INFORMATION_FORM_ERROR = "Payment information form is not displayed";
	public static final String PLACE_ORDER_BUTTON_ERROR = "Place order button is not displayed";
	public static final String INVALID_ADDRESS = "Test";
	public static final String DEVICE_IMAGE_ERROR = "Device image is not displayed.";

	public static final String INSURANCE_NAME_ERROR = "Insurance name is not displayed";
	public static final String INSURANCE_COST_PER_MONTH_ERROR = "Insurance cost is not displayed";
	public static final String INSURANCE_EDIT_BUTTON_ERROR = "Insurance edit button is not displayed";
	public static final String INSURANCE_REMOVE_BUTTON_ERROR = "Insurance remove button is not displayed";
	public static final String MONTHLY_CHARGES_BREAK_DOWN_LINK_ERROR = "Monthly charges break down link is not displayed";
	public static final String INSURANCE_MIGRATION_PAGE_ERROR = "Insurance migration page is not displayed";
	public static final String JUMP_LOGO_ERROR = "JUMP logo error";
	public static final String DUE_TODAY_VERBIAGE_ERROR = "Due today verbiage is not displayed";
	public static final String DOLLER_SYMBOL_ERROR = "Doller Symbol is not displayed";
	public static final String CANCEL_BUTTON = "Cancel button is not displaying";
	public static final String NEW_ADDRESS_LABEL_ERROR = "New address label is not displayed";
	public static final String ADDRESS_LINE_NOTIFICATION_ERROR = "Address line error message is not displayed";
	public static final String CANCEL_BUTTON_ERROR = "Cancel button is not displayed";
	public static final String NICK_NAME_ERROR = "Nick name is not displayed in cart page";
	public static final String DEVICE_NAME_ERROR = "Device name is not displayed in cart page";
	public static final String DEVICE_COLOR_ERROR = "Device color is not displayed";
	public static final String DEVICE_MEMORY_ERROR = "Device memory is not displayed";
	public static final String ERROR_PAGE_NOT_LOADED = "Page not loaded";
	public static final String MONTHLY_CHARGES_HEADER_ERROR = "Monthly charges header not displayed";
	public static final String PRICE_STRIKED_ERROR = "Price is not striked";
	public static final String PROMO_CODE_VALUE = "SALE25";
	public static final String ZERO_PRICE_PROMO_CODE = "12345";
	public static final String PROMO_DISCOUNT_PRICE_SIGN_ERROR = "Promo discount price sign is not displayed";
	public static final String PROMO_APPLY_BUTTON_DISABLE_ERROR = "Promo apply button is not disable mode";
	public static final String DEVICE_SHIPPING_DATE_ERROR = "Device shipping date is not displayed";
	public static final String ACCESSORY_SHIPPING_DATE_ERROR = "Accessory shipping date is not displayed";

	public static final String AAL_INELIGIBLE_WINDOW_ERROR = "AAL ineligible window not displayed";
	public static final String LEGAL_TERMS_LINK_ERROR = "Legal terms link is not displayed";
	public static final String LEGAL_TERMS_MODAL_WINDOW_ERROR= "Legal terms modal window not displayed";

	public static final String PDP_PAGE_PROMO_BADGE_ERROR = "Promo badge not displayed";
	public static final String PDP_PAGE_DEVICE_FRP_ERROR = "Device FRP not displayed";
	public static final String PDP_PAGE_PROMO_LINK_ERROR= "Promo link not displayed";

	public static final String SHOP_PAGE = "Page";
	public static final String SHOP_PAGE_ERROR = "Shop page not displayed";
	public static final String APPLE_IPHONE_6S = "Apple Iphone 6s not displayed";
	public static final String WEA_LINK_ERROR = "Wea Capability not displyed";
	public static final Object WEA_LINK_PAGE_ERROR = "WEA page not displayed";
	public static final String PLP_PAGE_ERROR = "PLP page not displayed";

	public static final String ORDER_DETAILS_TAB_ENABLE_MODE_ERROR = "Order details tab is not enable mode";
	public static final String SHIPPING_TAB_DISABLE_MODE_ERROR = "Shipping tab is not disable mode";
	public static final String SHIPPING_TAB_ENABLE_MODE_ERROR = "Shipping tab is not enable mode";	
	public static final String PAYMENTS_TAB_DISABLE_MODE_ERROR = "Payments tab is not disable mode";
	public static final String PAYMENTS_TAB_ENABLE_MODE_ERROR = "Payments tab is not enable mode";

	public static final String REMOVE_MODEL_HEADER_ERROR = "Remove Model header is not displayed";
	public static final String REMOVE_MODEL_BODY_MESSAGE_ERROR = "Remove Model body message is not displayed";
	public static final String REMOVE_MODEL_BODY_MESSAGE = "Are you sure you want to remove insurance? If so, your device may not be protected from loss, damage, or other issues the warranty doesn?t cover.";

	public static final String ORDER_DETAILS_TICK_MARK_ERROR = "Order Details Tick Mark is not displayed";
	public static final String PAYMENT_DETAILS_TICK_MARK_ERROR = "PaymenT Details Tick mark is not displayed";
	public static final String IPHONEX_CUSTOMERS_DEVICE_PROTECTION_SOC_ERROR = "Device protection soc is visable for Iphonex customers";

	public static final String SHIPPING_ADDRESS_CHECKBOX_MARKED_ERROR = "Shipping address check box is not marked";
	public static final String BILLING_ADDRESS_HEADER_ERROR = "Billing address header is not displayed";
	public static final String PROMO_MESSAGE_ERROR = "Customer Promo applied message error";
	public static final String REMOVE_INSURANCE_YES_BUTTON_ERROR = "Remove Insurance Yes button is not displayed";
	public static final String REMOVE_INSURANCE_NO_BUTTON_ERROR = "Remove Insurance No button is not displayed";
	public static final String PROMO_CODE_NOT_REQUIRED_MESSAGE_ERROR = "Promo code not required message is not displayed";


	public static final String AAL_LINK_ERROR = "AAL link is not displayed";
	public static final String AAL_INELIGIBILITY_MODEL_ERROR = "AAL ineligibility model is not displayed";
	public static final String AAL_PAGE_ERROR = "AAL page is not displayed";

	public static final String UPS_GROUND_SHIPPING_METHOD_ERROR = "UPS ground shipping method is not displayed";
	public static final String SHIPPING_VIA_UPS_EXPRESS_METHOD_ERROR = "Shipping via ups express method is not displayed";
	public static final String UPS_EXPEDITED_NEXT_DAY_AIR_SHIPPING_METHOD_ERROR = "UPS expedited next day air shipping method is not displayed";

	public static final String UPDATED_PDP_FEATURE_SECTION_ERROR = "Updated feature section is not displayed";
	public static final String ACCESSORY_DEVICE_ERROR = "Accessory device is not displayed";
	public static final String ACCESSORY_DEVICE_LEGAL_TEXT_ERROR = "Accessory device legal text is not displayed";
	public static final String ACCESSORY_DEVICE_REMOVE_BUTTON_ERROR = "Accessory device remove button is not displayed";
	public static final String ACCESSORY_DEVICE_EDIT_BUTTON_ERROR = "Accessory device edit button is not displayed";
	public static final String ADD_ACCESSORY_LINK_ERROR = "Add Accessory link is not displayed in cart page";
	public static final String REMOVE_INSURANCE_POP_UP_WINDOW_ERROR = "Remove insurance pop up window not displayed";

	public static final String ACCESSORY_DEVICE_NAME_ERROR = "Accessory device name is not displayed";
	public static final String PAY_MONTHLY_VERBIAGE_ERROR = "Accessory device pay monthly verbiage is not displayed";
	public static final String INSTALMENT_MONTH_ERROR = "Accessory device instalment moths is not displayed";
	public static final String DUE_TODAY_PRICE_ERROR = "Accessory device due today price is not displayed";
	public static final String PAY_IN_FULL_VERBIAGE_ERROR = "Accessory device pay in full verbiage is not displayed";
	public static final String DUE_TODAY_AND_TAX_LABEL_ERROR = "Accessory device due today and tax label is not displayed";
	public static final String FRP_CHARGES_ERROR = "Accessory device FRP charges is not displayed";
	public static final String EIP_LEGAL_DISCLAIMER_ERROR = "Accessory device EIP legal disclaimer is not displayed";

	public static final String PROMO_CODE_SUCCESS_MESSAGE_ERROR = "Promo code success is not displayed";
	public static final String PROMO_CODE_APPLIED_MESSAGE_ERROR = "Promo code applied message is not displayed";
	public static final String NEW_DEVICE_PAYMENT_LABEL_ERROR = "New device payment label is not displayed";
	public static final String LG_PROMO_CODE_VALUE = "SALE25";
	public static final String FIRST_TIME_PROMO_CODE_SUCCESS_MESSAGE = "Successful:";

	public static final String INSTALMENT_MONTH_PRICE_DOLLER_SYMBOL_ERROR = "Instalment price with doller symbol is not displayed";
	public static final String PROMO_DISCOUNT_VERBAGE_ERROR = "Promo discount verbage is not displayed";
	public static final String INSTALMENT_MONTHS_ERROR = "Instalment months is not displayed";
	public static final String PAY_IN_FULL_HEADER_ERROR = "Pay in full header is not displayed";
	public static final String PAY_IN_FULL_PRICE_AND_DOLLER_SYMBOL_ERROR = "Pay in full price and doller symbol is not displayed";
	public static final String TOTAL_PRICE_STRICKED_ERROR = "Total price is not stricked";
	public static final String PROMO_DISCOUNT_PRICE_ERROR = "Promo discount price is not displayed";
	public static final String PROMOTION_AMOUNT_MINUS_SIGN_ERROR = "Promotion amount minus sign is not displayed";

	public static final String DEVICE_COST_ZERO_ERROR = "Device cost zero is not displayed";
	public static final String DEVICE_COST_ZERO_MESSAGE_ERROR = "Device cost zero message is not displayed";
	public static final String UPS_GROUND_SHIPPING_METHOD_PRICE_ERROR = "UPS ground shipping method price is not zero";
	public static final String UPS_EXPRESS_SHIPPING_METHOD_PRICE_ERROR = "UPS express shipping method price is not zero";
	public static final String UPS_EXPEDITED_SHIPPING_METHOD_PRICE_ERROR = "UPS expedited shipping method price is not zero";

	// Accessories page
	public static final String ACCESSORIES_PAGE_ERROR_MSG = "Accessories page is not displayed";
	public static final String VERIFY_ACCESSORIES_PRODUCT_DETAILS_PAGE_ERROR = "Accessories product details page is not displayed";
	public static final String ACCESSORY_CART_PAGE_ERROR = "Accessories cart page is not displayed";

	public static final String VERIFY_ACCESSORIES = "Verify accessories page";
	public static final String VERIFY_COMPARE_ACCESSORIES = "Verify compare accessories";
	public static final String COMPARE_ACCESSORIES_ERROR_MSG = "Compare Accessories are not displaying";
	public static final String DONE_ADDING_ACCESSORIES_ERROR_MSG = "Done Adding Accessories button is not displaying";

	// Review Cart Page
	public static final String REVIEW_CART_PAGE_ERROR_MSG = "Review Cart page is not displaying";
	public static final String VERIFY_REVIEW_CART_PAGE = "Verify Review Cart page";
	public static final String VERIFY_ADDED_DEVICE = "Verify Added Device";
	public static final String ADDED_DEVICE_ERROR_MSG = "Added Device is not displaying";
	public static final String VERIFY_ADDED_ACCESSORIES = "Verify Added Accessories";
	public static final String ADDED_ACCESSORIES_ERROR_MSG = "Added Accessories are not displaying";
	public static final String VERIFY_ADDED_SERVICES = "Verify Added Services";
	public static final String ADDED_SERVICES_ERROR_MSG = "Added Services are not displaying";
	public static final String VERIFY_ADDED_PLAN = "Verify Added Plan";
	public static final String ADDED_PLAN_ERROR_MSG = "Verify Added Plan is not displaying";
	public static final String VERIFY_CLEAR_CART = "Verify clear cart";
	public static final String CLEAR_CART_ERROR_MSG = "Clear cart is not displaying";
	public static final String PRINT_CART_ERROR_MSG = "Print Cart is not displaying";
	public static final String VERIFY_PRINT_CART = "Print Cart is displaying";

	// Pay and Review page
	public static final String PAY_REVIEW_PAGE_ERROR_MSG = "Pay and Review is not displaying";
	public static final String VERIFY_PAY_REVIEW = "Verify Pay and Review";
	public static final String VERIFY_TERMSANDCONDITIONS = "Verify Terms and Conditions";
	public static final String TERMSANDCONDITIONS_ERROR_MSG = "Terms and Conditions are not displaying";

	// Customer Information View Page
	public static final String CUSTOMER_INFORMATION_PAGE_ERROR_MSG = "Customer Information Page is not displaying";
	public static final String VERIFY_CUSTOMER_INFORMATION_PAGE = "Verify Customer Information Page";
	public static final String VERIFY_911_YES_RADIO_BUTTON = "Verify 911 Yes Radio button";
	public static final String VERIFY_911_NO_RADIO_BUTTON = "Verify 911 No Radio button";
	public static final String STREET_ADDRESS_ERROR_MSG = "Street address is not displaying";
	public static final String CITY_ERROR_MSG = "City is not displaying";
	public static final String STATE_ERROR_MSG = "State is not displaying";
	public static final String EMAIL_ERROR_MSG = "Email is not displaying";
	public static final String OVERNIGHTSHIPPING_ERROR_MSG = "Overnight Shipping is not displaying";
	public static final String VERIFY_PAYANDREVIEW = " Verify Continue To PayAndReview";
	public static final String PAYANDREVIEW_ERROR_MSG = "PayAndReview button is not displaying";
	public static final String BACKTOCART_ERROR_MSG = " Back To Add Cart Btn  is not displaying";
	public static final String CANCELOREDR_ERROR_MSG = " Verify Cancel Order Btn  is not displaying";
	public static final String VERIFY_911_YES_ADDRESS_ERROR_MSG = "911 Address Yes Option Radio Button is not displaying";
	public static final String VERIFY_911_NO_ADDRESS_ERROR_MSG = "911 Address No Radio Button is not displaying";
	public static final String VERIFY_RETURN_TO_SHOP_ERROR_MSG = "Return To Shop Button is not displaying";

	// Billing page
	public static final String BILLING_PAGE_HEADER_PAGE_ERROR_MSG = "Bill summary Page is not displaying";
	public static final String BILLING_PAGE_HEADER = "Bill summary";
	public static final String CURRENT_CHARGES_HEADER = "Current charges";
	public static final String VIEW_BILL_DETAILS = "View bill details";
	public static final String PRINT_BILL = "Print bill (PDF)";
	public static final String VIEW_USAGE_SUMMARY = "View usage summary";
	public static final String TABLE_HEADER_1 = "AMOUNT";
	public static final String TABLE_HEADER_2 = "CHANGE FROM LAST MONTH";
	public static final int GRAPH_SIZE = 4;

	public static final String ACC_HISTORY_HEADER = "ACCOUNT HISTORY";
	public static final String VIEW_ACC_HIST_LINK = "View Account History";
	public static final String UNAV_HEADER_TEXT = "Please select which account to manage:";
	public static final String DEVICE_LIST = "Device list is not displaying";
	public static final String JUMP_TXT_VERIFY = "JUMP header is not displayed on Devices list page.";
	public static final String PRODUCT_DETAILS_VERIFY = "Product Details page is not displayed";

	public static final String BILLING_LINK = "Billing";
	public static final String USAGE_LINK = "Usage";
	public static final String PLAN_LINK = "Plan";
	public static final String PHONE_LINK = "Phone";
	public static final String SHOP_LINK = "Shop";

	// SearchBox

	public static final String SEARCH_TEXT_DATA_1 = "Apple";
	public static final String SEARCH_TEXT_DATA_2 = "Samsung";
	public static final String SEARCH_TEXT_INVALID = "@$%^()";
	public static final String NO_SEARCH_FOUND = "No results found.";

	public static final String REVIEW_CART_HEADER = "Review cart";
	public static final String CHECK_OUT_HEADER = "Checkout";

	// CardDetails
	public static final String CARD_NAME_FIELD = "CardHolder Name";
	public static final String CARD_CVV = "999";
	public static final String ZIP_CODE = "999999";
	public static final String CARD_NUMBER = "4916485957805904";

	// OneTimePayment
	public static final String PAYMENT_AMOUNT_TEXT = "Payment amount";
	public static final String PAYMENT_DETAILS_TEXT = "Enter payment information";
	public static final String PAYMENT_TYPE_TEXT = "Payment type";

	public static final String SHOP_PAGE_TITLE = "My T-Mobile | Shop";

	public static final String JUMP_VALIDATION_PAGE_TITLE = "My T-Mobile | JUMP! | JUMP! Device Verification";

	// Easy Pay Sign up details
	public static final String EASYPAY_CARD_NAME = "Test Name";
	public static final String EASYPAY_CARD_NUMBER = "4024007103742713";
	public static final String EASYPAY_CARD_CVV = "999";
	public static final String EASYPAY_CARD_ZIPCODE = "11201";

	public static final String EASYPAY_EDIT_CARD_NAME = "Testing Name";
	public static final String EASYPAY_EDIT_CARD_NUMBER = "4532400840682927";
	public static final String EASYPAY_EDIT_CARD_ZIPCODE = "30346";

	// ===================MyTmo : ADHOC & PAY
	// TestCases=============================
	// Your Profile Page Constants
	public static final String YOUR_PROFILE_HEADER = "Your profile";
	public static final String BILLING_ADDR_2 = "TEST";
	public static final String MANAGE_PAYMENT_OPTION_TXT = "Manage Payment Options";
	public static final String ROUTING_NUMBER = "321370707";
	public static final String ACCOUNT_NUMBER = "09507199";

	// E911 Address
	public static final String ZIP_CODE_E911 = "11201";
	public static final String STREET_ADDR = "250 Joralemon St";
	public static final String EMPTY_ADDRESS_LINE = " ";
	public static final String EMPTY_CITY = " ";
	public static final String EMPTY_ZIP_CODE = " ";
	public static final String ADDRESS_LINE1 = "1234 Main St.";
	public static final String CITY = "Brooklyn";
	public static final String WRONG_STREET_ADDR = "@@@@@";

	// BillDeliveryOptions
	public static final String EDIT_BILL_DELIVERY_OPTION = "Your bill delivery method is now paperless billing.";

	// MARKETING COMMUNICATIONS
	public static final String MARKETTING_COMM = "MARKETING COMMUNICATIONS";

	// profile Page
	public static final String EMAIL_ID = "4042001123@yopmail.com";
	public static final String TEST_EMAIL_ID = "ALEXANDER@test.com";
	public static final String PROFILE_PAGE_ERROR_MSG = "Profile page is not displaying";
	public static final String EXISTING_EMAIL_ADDRESS_MSG = "Email Address is not displaying";
	public static final String VERIFY_EMAIL_ERROR_MSG = "Email Error message is not displaying";

	// securityAnswers
	public static final String FIRST_ANSWER = "firstAnswer";
	public static final String SECOND_ANSWER = "secondAnswer";
	public static final String THIRD_ANSWER = "thirdAnswer";

	// AAL_STANDARD SUITE

	public static final String DATA_6_GB = "6 GB";
	public static final String DATA_10_GB = "10GB";
	public static final String DATA_UNLIMITED_GB = "Unlimited";
	public static final String DEVICE_PAGE_TITLE = "My T-Mobile | Add a Line | Select Device";

	// Upgrade
	public static final String DATA_2_GB = "2 GB";

	public static final String PROFILE_ACCOUNTSETTINGS = "Account Settings";
	public static final String PAYMENTHISTORY_PASTCALLRECORDS = "See past call records";

	public static final String USAGEPAGE_TABS_VOICE = "Voice";
	public static final String USAGEPAGE_TABS_MESSAGING = "Messaging";
	public static final String USAGEPAGE_TABS_DATA = "Data";
	public static final String USAGEPAGE_TABS_TMOBILE = "T-Mobile purchases";
	public static final String USAGEPAGE_TABS_THIRD_PARTY = "Third-party purchases";

	public static final String USAGEPAGE_OVERVIEW_TABS_LINES = "Lines";
	public static final String USAGEPAGE_OVERVIEW_TABS_MINUTES = "Minutes";
	public static final String USAGEPAGE_TABS_MESSAGES = "Messages";
	public static final String USAGEPAGE_TABS_MOBILE_VOICE = "Voice: All";
	public static final String USAGEPAGE_TABS_MOBILE_MESSAGING = "Messaging: All";
	public static final String USAGEPAGE_TABS_MOBILE_DATA = "Data: All";
	public static final String USAGEPAGE_TABS_MOBILE_TMOBILE = "T-Mobile purchases: All";
	public static final String USAGEPAGE_TABS_MOBILE_THIRD_PARTY = "3rd party purchases All";
	public static final String CARD_NUMBER_SUBMIT_ORDER = "4444444444444448";
	public static final String EXPIRY_MONTH_SUBMIT_ORDER = "01";
	public static final String EXPIRY_YEAR_SUBMIT_ORDER = "2017";
	public static final String CARD_CVV_SUBMIT_ORDER = "444";
	public static final String LINE_APPLY_CHANGE_MESSAGE = "Changes apply to all lines on your account";

	// Plans And Services

	public static final String PLAN_PAGE_HEADER = "Plans and services";
	public static final String CHANGE_DATA_HEADING = "Personalize your data";
	public static final String CHANGE_DATA_FOOTER_LINK = "http://www.t-mobile.com/OpenInternet";
	public static final String FREE_DATA_PLAN_PRICES = "free";
	public static final String THANK_YOU_MESSAGE = "Thank you! Your changes have been submitted successfully.";
	public static final String FAMILY_ALLOWANCES = "Family Allowances";
	public static final String DATACALCULATOR_HEADING = "Recommended Data Plan";
	public static final String PLANS_SERVICES = "Plans and Services page should be displayed.";

	public static final String CHANGE_PLANS_CONFIRMATION_MESSAGE = "Your changes have been submitted";
	public static final String PLANS_NICKNAME_MSISDN = "Nickname&MSISDN are not displaying";
	public static final String CHANGE_DATA_4GB_PLAN = "4GB data plan is not displaying";
	public static final String CHANGE_DATA_6GB_PLAN = "6GB data plan is not displaying";
	public static final String CHANGE_DATA_10GB_PLAN = "10GB data plan is not displaying";
	public static final String CHANGE_DATA_NODATA_PLAN = "No data plan is not displaying";
	public static final String CHANGE_DATA_UNLIMITED_PLAN = "Unlimited plan is  displaying";
	public static final String VIEW_PLAN_BTN = "VIEW PLAN";

	// Incorrect Login Text
	public static final String INCORRECT_LOGIN_TEXT = "The login information you provided is incorrect. Please try again.";
	public static final String T_MOBILE_ID = "T-Mobile ID";
	public static final String MEDIA_SETTINGS = "Media Settings";

	public static final String PROFILE_NAME = "Profile";

	// Device Page
	public static final String SELECT_DEVICE_TEXT = "Upgrade: Select Device";

	public static final String SUCCESS_PAGE = "Thank you!";
	public static final String BILLPAYMENT_SUCCESS_MESSAGE = "Thank you";
	public static final String ERR_SUCCESS_PAGE = "Success Page is not displayed";

	public static final String ERR_BILLING_PAYMENTS_PAGE = "User Is Not Navigated To Billing and Payments Page";
	public static final String PDFFILE_DOWNLOAD_PATH = "/Downloads" + "/PastBills.pdf";

	public static final String ORDER_COMPLETED = "order is complete!";
	public static final String ORDER_CONFIRMATION = "Confirmation order not displayed";
	public static final String ORDER_CONFIRMATION_PAGE_ERROR = "Order Confirmation page is not displayed";

	public static final String JUMP_SOC_SERVICES = "JUMP!";
	public static final String JUMP_DEVCE_NAME = "Samsung Galaxy Note7 -64 GB";
	public static final String PLAN_LEGALESE_DATAPASS = "Limited time offer; subject to change. Taxes and fees additional. Compatible device required; not all plans or features available on all devices. Data Passes: Does not include voice or messaging. Post-paid only; pass charges will appear on bill statement for effective date of the pass. Limit of 2 active passes per account in a particular pass category; device pass use dates may not overlap. May purchase new pass in same category prior to expiration of active pass if data allotment has been reached; unless pass includes use at reduced speeds. Partial megabytes rounded up. Full speeds available up to specified data allotment, then slowed to up to 2G speeds. No domestic or international roaming on Domestic Pass, unless Pass is specifically for roaming. International Pass use requires qualifying plan; usage does not impact Mobile Internet plan data allotment. Service available for time/usage amount provided by pass. For time period, a day is 12:00 a.m. to 11:59 p.m. Pacific Time (PT). Credit approval and qualifying agreement required; deposit may apply. Smartphone Mobile Hotspot(Tethering/Wi-Fi Sharing): Plan data allotment applies. No Roaming, unless Pass is specifically for roaming. Use of connected device subject to T-Mobile Terms and Conditions. Must use device manufacturer or T-Mobile feature. Network Management: Service may be slowed, suspended, terminated, or restricted for misuse, abnormal use, interference with our network or ability to provide quality service to other users, or significant roaming. Customers who use 28GB of data in a bill cycle will have their data usage de-prioritized compared to other customers for that bill cycle at locations and times when competing network demands occur, resulting in relatively slower speeds. See T-Mobile.com/OpenInternet for details. LTE is a trademark of ETSI.";
	public static final String VIRTUAL_LINE_MESSAGE = "Virtual Line is not displaying";

	public static final String VIRTUAL_LINE_MINUTES_MESSAGE = "Number of Minutes is not displaying";
	public static final String VIRTUAL_LINE_MESSAGES = "Number of Messages is not displaying";
	public static final String VIRTUAL_LINE_DATA_MESSAGE = "Number of Gigabytes Used is not displaying";

	public static final String PHONE_PAGE_UPGRADE_ERROR = "When you're ready to upgrade, simply visit";

	public static final String ORDER_STATUS = "Order Status";

	public static final String EQUIPMENT_INSTALLMENT_PLAN = "Equipment Installment Plan (EIP)";
	public static final String EQUIPMENT_DOCUMENTS_RECEIPTS = "Documents & Receipts";
	public static final String DOCUMENTS = "Documents";
	public static final String BILLING_SUMMARY_PAGE = "User is not navigated to billing summary page";
	public static final String BILL_DETAILS_TEXT = "Bill details";
	public static final String JUMP_ELIGIBLE = "You're eligible for JUMP! upgrade.";
	public static final String ADDLINE_ELIGIBLE = "You are eligible to add";
	public static final String MP_NAME = "ESOA Test,";
	public static final String SBB_SOC_SERVICES = "Block Charged International Data Roaming - Single Line";
	public static final String EASY_PAY_CONFIRMATION = "successfully signed up for AutoPay";
	public static final String PAYMENT_WITH_TEXT = "Payment with:";

	public static final String ADDLINE_ELIGIBLE_DEFAULT = "You are eligible to add  voice and  mobile internet lines to your account.";
	public static final String ADDLINE_ELIGIBLE_ADDED_VOICE_LINES = "Eligible add voice are not displaying";
	public static final String ADDLINE_ELIGIBLE_ADDED_MOBILE_LINES = "Eligible add Mobile internet are not displaying";

	public static final String VIRTUALLINE_NOTELIGIBLE = "You are logged in with a DIGITS™ virtual line which is not eligible for a device upgrade.";
	public static final String VIRTUALLINE_NONVIRTUALLINE = "Please select a non-DIGITS™ virtual line to upgrade a device.";

	public static final String SIGNUP_HEADER_TEXT = "Sign up for T-Mobile ID";
	public static final String FORGOT_PSWD_HEADER_TEXT = "Reset Your Password";
	public static final String VERIFICATION_METHOD_HEADER_TEXT = "Choose verification method";

	public static final String PHONE_JUMPELIGILE_TXT = "You are a JUMP! customer.";

	public static final String UN_LOCK_ACCOUNT_TXT = "The login information you provided is incorrect. For your security, you have one more attempt at logging in until we lock your account.";

	public static final String UN_LOCK_ACCOUNT_24_TXT = "To protect your security, we have temporarily locked your account. To access your account, wait 24 hours or";

	public static final String FORGOT_PSWD_TXT = "The login information you provided is incorrect. Please try again.";

	public static final String LINE_SUSPENDED = "This line is Suspended";
	public static final String TEMPORARY_SUSPENSION = "Temporary suspension";

	public static final String JOD_WARNINGMESSAGE = "To upgrade your device today please Call us at 1 (800) 937-8997, dial 611 from your T-Mobile phone, or visit your local T-Mobile retail store.";

	public static final String CART_ACCESSORIES_ERROR_MSG = "Added Accessories not displaying";
	public static final String CART_SERVICES_ERROR_MSG = "Added Services are not displaying";
	public static final String CART_PAGE_ERROR_MSG = "Cart page is not displaying";
	public static final String UPGRADE_SERVICE_PAGE = "Upgrade service page is not displaying";
	public static final String UPGRADE_PLAN_PAGE = "Upgrade plan page is not displaying";
	public static final String CUSTOMER_INFO_PAGE_CONTINUE = "Customer information page is not displaying";

	public static final String TEMP_LOCKACCT = "Sorry, we’ve temporarily locked your account to protect your information. Please call Customer Care at 1-800-937-8997.";
	public static final String REPORTED_DEVICE_STOLEN = "You have reported this device as stolen";

	public static final String PROFILE_NAME_TEXT = "Make this the nickname for all lines linked to your T-Mobile ID.The system shall defecult the checkbox to checked state.";

	public static final String HOME_PAGE_VERIFY = "Home page is not displayed";
	public static final String HOME_PAGE_HEADER = "Home page is displayed.";
	public static final String PROFILE_PAGE_VERIFY = "User Is Not Navigated To Profile Page.";
	public static final String PROFILE_PAGE_HEADER = "Navigated To Profile Page.";
	public static final String ACCOUNT_SETTING_HEADER = "Account settings details should be displayed";
	public static final String PROFILE_VALIDATE = "Validate User is able set e911 address and usage address for all the lines using below options";
	public static final String PROFILE_VALIDATE_SAVE = "Save E911 addres is clicked";
	public static final String PROFILE_VERIFY = "Make this the e911 address for all linesMake this the usage address for all lines";
	public static final String PROFILE_EDIT_ADDRESS = "E911 address should be edited";
	public static final String BINGE_ON_TEXT_ERROR = "Binge on text is displaying";

	public static final String PROFILE_SAVE = "the system shall copy the new user level name to the line level for all MSISDNs linked to the current TMO ID";
	public static final String PROFILE_SAVE_ERROR_MESSAGE_TEXT = "User should be able to see the error message STATING  The number you provided is already registered to a T-Mobile ID. If this is your number, confirm your password to manage everything from a single T-Mobile ID.Confirm password AND the label Confirm password is a link";
	public static final String PROFILE_CANCEL = "User should see a screen prompting the user to enter the password associated with the new MSISDN";

	public static final String CHOOSE_ACCOUNT_TEXT = "Successfully Navigated to switch account";
	public static final String CAPCHA_POPUP = "captcha Screen not displayed";
	public static final String FORGOT_PSWD = "Successfuly Navigated to ForgotPassword Screen";
	public static final String RESETS_SQURITY_QUESTIONS = "Successfully displayed the ResetSequrityQuestions";
	public static final String HOME_HEADER_TEXT = "Home page should be displayed";
	public static final String PROFILE_HEADER_TEXT = "Profile 'Page should be displayed";
	public static final String EMAIL_NOTIFICATION_MSG_TEXT = "User should get email notification";
	public static final String EMAIL_NOTIFICATION_TEXT = "User should get welcome email Notification";
	public static final String CHANGE_PSWD_NAVIGATEDTO_HOME = "Successfully change the password and navigated to Home page";
	public static final String CONFIRMATION_TEXT = "Successfuly Redirect  to Confirm Screen";
	public static final String SEQ_QUESTION_HEADER_TEXT = "Successfuly Navigated to Missing Sequrity Questions Screen";
	public static final String FORGOT_PSWD_TEXT = "Successfully change the password and navigated to Home page";
	public static final String BAN_ERROR_MSG = "Ban is not displaying";
	public static final String VERIFY_UPGRADE = "Click Upgrade Button";
	public static final String VERIFY_DEVICE_PAGE = "verify Device Page header";
	public static final String VERIFY_DEVICE_SELECTION = "Verify Select first Device";
	public static final String VERIFY_CONTINUE_PLAN = "Select Continue in ";
	public static final String VERIFY_UPGRADE_TO_2GB_DATA = "Select 2GB Plan Page";
	public static final String SELECTED_2GB_DATA_SERVICE = "2GB data plan is selected";
	public static final String VERIFY_CONTINUE_CHANGE_DATA = "click continue to change data Services";

	public static final String ACCESSORY_PLP_PAGE_ERROR = "Accessory PLP Page is not displayed.";
	public static final String ACCESSORY_PLP_PAGE_PROMO_TEXT_ERROR = "Accessory promo text is not displayed.";
	public static final String ACCESSORY_PLP_PAGE_DISCOUNT_ERROR = "Accessory product list page doscount is not displayed.";
	public static final String ACCESSORY_PLP_PAGE_SEE_DETAILS_LINK_ERROR = "See details link is not displayed.";

	public static final String ACCESSORY_PLP_PAGE_LEGAL_TEXT_ERROR = "Accessory PLP Page legal text is not displayed.";

	public static final String PAY_IN_MONTHLY_SECTION_ERROR = "Pay in monthlyu section is not displayed.";
	public static final String PAY_IN_FULL_SECTION_ERROR = "Pay in full section is not displayed.";
	public static final String WHY_CANT_I_SEE_THIS_LINK_ERROR = "Why cant i see this link is not displayed.";
	public static final String SKIP_ACCESSORIES_ERROR = "Skip accessoreis CTA is not displayed.";
	public static final String ACCESSORIES_BLANK_PAGE_ERROR = "There are no accessories for device text is not displayed.";
	public static final String ACCESSORY_PLP_PAGE_SEDONA_HEADER_ERROR = "Sedona header is not displayed.";
	public static final String WHY_CANT_I_SEE_THIS_MODEL_ERROR = "Why cant i see this model is not displayed.";

	public static final String PRICE_RANGE_ERROR = "PriceRange is not displayed";
	public static final String ADD_PROMO_CODE_ERROR = "Add PromoCode is not displayed";
	public static final String EIP_PAYOFF_HEADER_ERROR = "EIP PayOff header is not displayed";

	public static final String ACCESSORY_PLP_TILES_ERROR = "Max 15 tiles are not displayed";
	public static final String EST_SHIPPING_HEADER_ERROR = "Est shipping header is not displayed";
	public static final String EST_SHIPPING_AMOUNT_ERROR = "Est shipping Amount is not displayed";
	public static final String DUE_TODAY_TOTAL_LABEL_ERROR = "Due today total label is not displayed";
	public static final String EST_DUE_TODAY_TOTAL_ERROR = "Est due today total is not displayed";
	public static final String DUE_MONTHLY_TOTAL_LABEL_ERROR = "Due Monthly total label is not displayed";

	public static final String DUETODAYSUBTOTAL_HEADER_ERROR = "Due today sub total header is not displayed";
	public static final String DUETODAYSUBTOTAL_PRICE_ERROR = "Due today sub total price is not displayed";
	public static final String SALESTAX_HEADER_ERROR = "Sales Tax header is not displayed";
	public static final String SALESTAX_PRICE_ERROR = "Sales Tax price is not displayed";
	public static final String DUETODAYTOTAL_HEADER_ERROR = "Due today total header is not displayed";
	public static final String DUEMONTHLY_TOTAL_ERROR = "Due monthly total is not displayed";

	public static final String SHIPPING_HEADER_ERROR = "Shipping header is not displayed";
	public static final String SHIP_TO_DIFFERENT_ADDRESS_LINK_ERROR = "ShipToDiferentAddress Link is not displayed";
	public static final String EDIT_OR_REMOVE_OPTION_ERROR = "Edit or Remove option is not displayed";
	public static final String TICK_MARK_ERROR = "Headers Tick mark is not displayed";

	public static final String BILLING_ADDRESS_LABEL_ERROR = "Billing Address label is not displayed";
	public static final String BILLING_ADDRESS_TEXT_ERROR = "Billing Address label text is not displayed";
	public static final String BILLING_CITY_LABEL_ERROR = "Billing city label is not displayed";
	public static final String BILLING_CITY_LABEL_TEXT_ERROR = "Billing city label text is not displayed";
	public static final String BILLING_STATE_LABEL_ERROR = "Billing State label is not displayed";
	public static final String BILLING_STATE_LABEL_TEXT_ERROR = "Billing State label text is not displayed";
	public static final String BILLING_ZIPCODE_LABEL_ERROR = "Billing zip code label is not displayed";
	public static final String BILLING_ZIPCODE_LABEL_TEXT_ERROR = "Billing zip code label text is not displayed";

	public static final String ADD_A_LINE_SECTION_IMAGE_ERROR = "Add a line section image is not displayed";
	public static final String PAY_MONTHLY_HEADER_ERROR = "Pay monthly header is not displayed";
	public static final String HYPER_LINK_ERROR = "Hyper link is not displayed in Legar Disclaimer";
	public static final String UPSELL_SOC_OPTIONS_ERROR = "Up sell Soc options are not displayed";

	public static final String APPLY_BUTTON_DISABLE_ERROR = "Apply button is not disable mode";
	public static final String APPLY_BUTTON_ENABLED_ERROR = "Apply button is not enable mode";
	public static final String PROMO_CODE_TEXT_ERROR = "Promo code text is not displayed";

	public static final String CREDITCARD_NUMBER_LABEL_ERROR = "Credit card name label is not displayed";
	public static final String CVV_LABEL_ERROR = "Cvv label is not displayed";
	public static final String WHAT_IS_THIS_TAG_ERROR = "What is this tag is not displayed";
	public static final String CVV_NUMBER_IMAGE_ERROR = "Cvv number image is not displayed";
	public static final String CHECK_BOX_SELECTION_ERROR = "Check box is not selected by default";

	public static final String JUMP_UPGRADE_INELIGIBLE_MESSAGE = "Jump Upgrade Ineligible Message is not displayed";
	public static final String VERFIY_PAY_NOW_LINK = "Pay Now Link is not displayed";
	public static final String HOW_IS_IT_CALCULATED_TAG_ERROR = "How is it calculated tag is not displayed";
	public static final String GLOBAL_NAVIGATION_HEADER_ERROR = "Global navigation header is not displayed";

	public static final String TRADEIN_MESSAGE_ERROR = "TradeIn Original message 'Please ship your old device within 30 days of receiving the new one' is displayed";
	public static final String TRADEIN_STATUS_OFFERS_ERROR = "TradeIn Status offer is not displayed";
	public static final String PRINT_SHIPPING_LABEL_ERROR = "Print Shipping label is not displayed";
	public static final String EIP_OFFER_AMOUNT_ERROR = "EP Offer Amount is not displayed";
	public static final String ACCESSORIES_DEVICE_IMAGE_ERROR = "Accessory device image is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_SPECFICATIONS_HEADER_ERROR = "Specification header is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_WARRANTY_HEADER_ERROR = "Warranty header is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_WEIGHT_HEADER_ERROR = "Weight header is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_DIMENSIONS_HEADER_ERROR = "Dimensions header is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_INTHEBOX_HEADER_ERROR = "In the box header is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_WARRANTY_TEXT_ERROR = "Warranty text is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_WEIGHT_TEXT_ERROR = "Weight text is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_DIMENSIONS_TEXT_ERROR = "Dimensions text is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_INTHEBOX_TEXT_ERROR = "In the box text is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_LEGALTEXT_ERROR = "Legal text is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_OVERVIEW_HEADER_ERROR = "Overview header is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_OVERVIEW_DETAILS_TEXT_ERROR = "Overview details text is not displayed";
	public static final String ACCESSORY_CHECKBOX_STATUS = "Checkbox is not selected";
	public static final String DEVICE_PROTECTION_SOC_ERROR = "Device protection soc not displayed in Insurance migration page";
	public static final String ACCESSORIES_PDP_PAGE_MONTHLY_SECTION = "Monthly section is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_DUE_SECTION = "Due section is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_INSTANT_DISCOUNT_SECTION = "Instant discount section is not displayed";
	public static final String ACCESSORIES_PDP_PAGE_FRP_SECTION = "FRP section is not displayed";
	public static final String DIGITS_LINES_ERROR = "Digits lines are displayed in phone selection page";
	public static final String HOLIDAY_BANNER = "Holiday Banner";
	public static final String LATEST_DEALS_SECTION_ERROR = "Latest Deals Section is not displayed";
	public static final String HERO_DEVICE_BANNER = "Hero Device Banner";
	public static final String CART_PAGE_DEVICE_ERROR = "Selected device is not displayed in cart page";
	public static final String REMOVE_FA_DESCRIPTION = "By removing Netflix Premium, you'll lose your access to Netflix Premium benefits. If you continue, be sure to add Netflix Basic (a $10.99 value) to avoid losing your Netflix access";
	public static final String REMOVE_FA_ADDING_DESCRIPTION="By adding Netflix Prem. $13.99 with Fam Allowance ($19 value) to this line you are also adding it to other lines on your account. This service will be added to the following lines:";
	public static final String REMOVE_FA_ADDING_CONFLCIT_DESCRIPTION="";
	public static final String ADDRESS_VALID_MESSAGE = "Enter a valid street address. Your shipping address cannot be a PO Box";
	public static final String INVALID_ADDRESS_MESSAGE_ERROR = "Invalid address error message is not displayed";
	public static final String JUMP_PLP_PAGE_ERROR = "Jump Product list page is not displayed";
	public static final String PDP_PAGE_PROMO_TEXT_ERROR = "Promo text is not displayed";
	public static final String JUMP_PDP_PAGE_ERROR = "Jump Product detail page is not displayed";

	public static final String MODAL_POPUP_ERROR = "ModalPopUp is not displayed";
	public static final String MODAL_POPUP_TEXT_ERROR = "ModalPopUp text is not displayed";
	public static final String MONTHLY_CHARGES_ERROR = "Monthly charges are not displayed";
	public static final String TOTAL_PRICE_ERROR = "Total price is not displayed";
	public static final String SKIP_ACCESORRIES_LINK_ERROR = "Skip Accessorries link is not displayed";
	public static final String PROMOBANNER_ERROR = "Promo banner is not displayed";
	public static final String LOGGED_IN_LINE_DISPLAY_FIRST_ERROR = "Logged In Line is not displayed in first";
	public static final String REVIEW_SEARCH_BOX_ERROR = "Review Seach Box is displayed";
	public static final String EIP_PAID_OFF_LINE_ERROR = "EIP Paid off line error";
	public static final String JUMP_INELIGIBLE_LINE_ERROR = "JUMP Ineligible line error";
	public static final String ACCESSORIES_CONTINUE_BUTTON = "Continue button is not disabled";

	public static final String ACCESSORY_MODEL_WINDOW_ERROR = "Accessory model window is not displayed";
	public static final String CONTINUE_SHOPPING_BUTTON_ERROR = "Continue shopping button is not displayed";
	public static final String ACCESSORY_MODEL_WINDOW_HEADER_ERROR = "Accessory model window header is not displayed";
	public static final String ACCESSORY_MODEL_WINDOW_TEXT_MESSAGE_ERROR = "Accessory model window text message is not displayed";
	public static final String ACCESSORY_CHECK_OUT_BUTTON_ERROR = "Accessory check out button is not displayed";
	public static final String SELECT_DEVICE_FROM_PLP = "Samsung Galaxy S7";
	public static final String PDP_PROMO_WINDOW_ERROR= "PDP promo window not displayed";

	public static final String HOW_IT_CALCULATED_TAX_LINK_ERROR = "how it calculated tax link not displayed";
	public static final String TAX_BREAK_DOWN_WINDOW_HEADER_ERROR = "Tax break down window header not displayed";
	public static final String TAX_BREAK_DOWN_WINDOW_SUB_TITLE_ERROR = "Tax break down window sub title not displayed";
	public static final String TAX_BREAK_DOWN_WINDOW_DEVICE_NAME_ERROR = "Tax break down window device not displayed";
	public static final String TAX_BREAK_DOWN_WINDOW_DEVICE_PRICE_ERROR = "Tax break down window device price not displayed";
	public static final String TAX_BREAK_DOWN_WINDOW_SUB_TOTAL_LABEL_ERROR = "Tax break down window sub total label not displayed";
	public static final String TAX_BREAK_DOWN_WINDOW_SUB_TOTAL_PRICE_ERROR = "Tax break down window sub total price not displayed";
	public static final String TAX_BREAK_DOWN_WINDOW_SALES_TAX_LABEL_ERROR = "Tax break down window sales tax label not displayed";
	public static final String TAX_BREAK_DOWN_WINDOW_SALES_TAX_PRICE_ERROR = "Tax break down window sales tax price not displayed";
	public static final String TAX_BREAK_DOWN_WINDOW_SALES_TAX_LEGAL_TEXT_ERROR = "Tax break down window sales tax leagal copy not displayed";
	public static final String TAX_BREAK_DOWN_WINDOW_GOTIT_BUTTON_ERROR = "Tax break down window got it button not displayed";

	public static final String APPLE_IPHONE7 = "Apple iPhone 7";
	public static final String APPLE_IPHONE8 = "Apple iPhone 8";
	public static final String E5_TRADEINDEVICE_URL_DEEPLINK = "https://e5.my.t-mobile.com/purchase/shop/g-9DFB47B72B1448DDB97105779F22DCF3/false/190198062277";
	public static final String QATA01_URL_DEEPLINK= "https://qata01.eservice.t-mobile.com/purchase/shop/g-54F380D279E845B48519B925CE14FAA8 /false/610214656056";


	/**
	 * SHOP API Payload locations
	 */
	public static final String SHOP_AALELIGIBILITYCHECK = "shop/aalEligibilityCheck/";
	public static final String SHOP_ACCESSORIES = "shop/accessories/";
	public static final String SHOP_BENEFITSCL="shop/benefitsCL/";
	public static final String SHOP_CART="shop/cart/";
	public static final String SHOP_CATALOG="shop/catalog/";
	public static final String SHOP_DEVICETRADEIN="shop/deviceTradeIn/";
	public static final String SHOP_LOANAGREEMENT="shop/loanAgreement/";
	public static final String SHOP_ORDER="shop/order/";
	public static final String SHOP_QUOTE="shop/quote/";
	public static final String SHOP_SERVICE="shop/service/";
	public static final String SHOP_MONITORS="shop/monitors/";
	public static final String TMNG_CREDITCHECK="tmng/creditCheck/";

	public static final String PII_CUSTOMER_MSISDN_PID = "cust_msisdn";
	public static final String PII_CUSTOMER_NAME_PID = "cust_name";
	public static final String PII_CUSTOMER_MAIL_PID = "cust_mail";
}
