package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.global.ChangePasswordPage;
import com.tmobile.eservices.qa.pages.global.ChooseMethodOptionsPage;
import com.tmobile.eservices.qa.pages.global.ForgotPasswordPage;
import com.tmobile.eservices.qa.pages.global.LoginPage;

public class ChangePasswordPageTest extends GlobalCommonLib{
	
	
	/**
	 * user is able to change password
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void forgotPasswordByEmail(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("forgotPasswordByEmail method called in MyTMORegistrationTest");
		String optionType = myTmoData.getOptionType();
		deleteMails(myTmoData);
		ChooseMethodOptionsPage methodOptionsPage = loginAndNavigateToMethodOptions(myTmoData);
		ChangePasswordPage changePasswordPage = new ChangePasswordPage(getDriver());
		if (optionType.equalsIgnoreCase(ChooseMethodOptionsPage.EMAIL_MESSAGE_TYPE)) {
			methodOptionsPage.chooseVerificationMethod(optionType);
			changePasswordPage.submitNextButton();
			changePasswordPage.checkMail();
			String url = changePasswordPage.getYopMailURL(myTmoData);
			getDriver().get(url);
			changePasswordPage.performChangePassword(myTmoData.getNewPassword());
		}
	}
	
	/**
	 * user is able to change password
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void forgotPasswordByTextMsg(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("forgotPasswordByTextMsg method called in MyTMORegistrationTest");
		getTMOURL(myTmoData);
		unlockAccount(myTmoData);
		LoginPage loginPage = new LoginPage(getDriver());
		ForgotPasswordPage forgotPasswordPage = new ForgotPasswordPage(getDriver());
		loginPage.clickForgotPassword();
		Reporter.log("Successfuly Navigated to ForgotPassword Screen");
		forgotPasswordPage.getResetPasswordText();
		forgotPasswordPage.enterEmailOrPhone(myTmoData.getLoginEmailOrPhone());
		forgotPasswordPage.clickNextButton();
		forgotPasswordPage.enterZipCode(myTmoData.getPayment().getZipCode());
		forgotPasswordPage.clickNextButton();
		ChooseMethodOptionsPage methodOptionsPage = new ChooseMethodOptionsPage(getDriver());
		Reporter.log("Successfuly Redirect  to Choose Method options Screen");
		methodOptionsPage.getChooseMethodText();
		String optionType = myTmoData.getOptionType();

		ChangePasswordPage changePasswordPage = new ChangePasswordPage(getDriver());
		if (optionType.equalsIgnoreCase(ChooseMethodOptionsPage.TEXT_MESSGE_TYPE)) {
			methodOptionsPage.chooseVerificationMethod(optionType);
			forgotPasswordPage.clickNextButton();
			String tempPassword = getTempPassword(myTmoData);
			if (tempPassword.isEmpty()) {
				forgotPasswordPage.clickResendPassword();
				tempPassword = getTempPassword(myTmoData);
			}
			changePasswordPage.enterConfirmCode(tempPassword);
			changePasswordPage.submitNextButton();
		}

		changePasswordPage.performChangePassword(myTmoData.getNewPassword());
		Reporter.log("Successfully change the password and navigated to Home page");
	}

}
