/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AddBankPage;
import com.tmobile.eservices.qa.pages.payments.AddCardPage;
import com.tmobile.eservices.qa.pages.payments.ClaimPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author Bhavya
 * 
 */
public class NewSpokePageTest extends PaymentCommonLib {

	/**
	 * US444963:Angular 6 PCM- Spoke page.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testNewAngularSpokePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US444963-Angular 6 PCM- Spoke page.");
		Reporter.log("Data Condition | Any PAH /Standard User with saved card and saved bank");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment  Spoke Page should be displayed");
		Reporter.log("Step 6: verify 'Edit payment method' Header| 'Edit payment method' Header should be displayed");
		Reporter.log(
				"Step 7: verify saved payment method  blades (In the order In session payment method , saved bank ,saved card) |  saved payment method  blades should be displayed ");
		Reporter.log("Step 8: Verify Add a bank blade|Add a bank blade should be displayed");
		Reporter.log("Step 9: click  Add a bank blade|Add bank Spoke Page should be displayed");
		Reporter.log("Step 10: click  cancel button | Payment Spoke Page Page should be displayed");
		Reporter.log("Step 11: Verify Add a card blade | Add a card blade should be displayed");
		Reporter.log("Step 12: click  Add a card blade | Add card Spoke Page should be displayed");
		Reporter.log("Step 13: Verify Back ,Continue CTAs  | Back ,Continue CTA should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		SpokePage otpSpokePage = navigateToNewSpokePage(myTmoData);
		AddBankPage addBankPage = new AddBankPage(getDriver());
		AddCardPage addCardPage = new AddCardPage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.verifyPaymentMethodBladeElements();
		otpSpokePage.verifyAddBankBlade();
		otpSpokePage.clickAddBank();
		addBankPage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);
		addBankPage.clickBackButton();
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.verifyAddCardBlade();
		otpSpokePage.clickAddCard();
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.clickBackBtn();
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.verifyBackCTA();
		otpSpokePage.verifyContinueCTA();
	}

	/**
	 * US445095-Angualar 6 PCM Delete payment method
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ccs1" })
	public void testverifyDeleteSavedPaymentMethodCard(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US445095-Angualar 6 PCM Delete payment method");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved card .");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment  Spoke Page should be displayed");
		Reporter.log("Step 6: click on  delete Ion of Saved Card | Delete payment method modal should be displayed");
		Reporter.log("Step 7: verify 'Delete this card?' Header|'Delete this card?' Header should be displayed");
		Reporter.log(
				"Step 8: Verify 'Are you sure you want to delete:' sub header on the modal|'Are you sure you want to delete:' sub header on the modal should be displayed");
		Reporter.log(
				"Step 9: Verify payment method information | overwritten payment method ( ****XXXX is the last 4 of the payment method) on the modal should be displayed");
		Reporter.log(
				"Step 10: Verify No thanks and Yes, delete CTAs  | No thanks and Yes, delete CTAs should be displayed");
		Reporter.log("Step 11: click on Yes, delete CTA |  payment method should be deleted");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		SpokePage otpSpokePage = navigateToNewSpokePage(myTmoData);
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickDeletLinkAndVerifyCardDeleteModel(PaymentConstants.Card_Delete_Model_Header,
				PaymentConstants.Delete_Model_Sub_Header);

	}

	/**
	 * US445095-Angualar 6 PCM Delete payment method
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testverifyDeleteSavedPaymentMethodBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US445095-Angualar 6 PCM Delete payment method");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved Bank .");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment  Spoke Page should be displayed");
		Reporter.log("Step 6: click on  delete Ion of Saved Bank | Delete payment method modal should be displayed");
		Reporter.log(
				"Step 7: verify 'Delete this bank account?' Header|'Delete this bank account?' Header should be displayed");
		Reporter.log(
				"Step 8: Verify 'Are you sure you want to delete:' sub header on the modal|'Are you sure you want to delete:' sub header on the modal should be displayed");
		Reporter.log(
				"Step 9: Verify payment method information | overwritten payment method ( ****XXXX is the last 4 of the payment method) on the modal should be displayed");
		Reporter.log(
				"Step 10: Verify No thanks and Yes, delete CTAs  | No thanks and Yes, delete CTAs should be displayed");
		Reporter.log("Step 11: click on Yes, delete CTA |  payment method should be deleted");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		SpokePage otpSpokePage = navigateToNewSpokePage(myTmoData);
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickDeletLinkAndVerifyBankDeleteModel(PaymentConstants.Bank_Delete_Model_Header,
				PaymentConstants.Delete_Model_Sub_Header);
	}

	/**
	 * US444963:Angular 6 PCM- Spoke page contents.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testNewAngularSpokePageContents(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US444963-Angular 6 PCM- Spoke page contents");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved Bank .");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment  Spoke Page should be displayed");

		Reporter.log("Step 6: click on  delete Ion of Saved Bank | Delete payment method modal should be displayed");

		OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
		SpokePage otpSpokePage = new SpokePage(getDriver());
		String storedPaymentNumber = oneTimePaymentPage.getStoredPaymentMethodNumber();
		oneTimePaymentPage.clickPaymentMethodBlade();
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.verifyDefaultSelectionofCardorBankInspokePage(storedPaymentNumber);
		otpSpokePage.verifyOneradioBtnIsSelected();
		otpSpokePage.clickDeleteLinkAndVerifyModel(PaymentConstants.Bank_Delete_Model_Header,
				PaymentConstants.Delete_Model_Sub_Header);
	}

	/**
	 * US484470 Angular 6.0 PCM- Negative file messaging payment Type/BAN
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testPCMNegativeFileOnBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US484470 Angular 6.0 PCM- Negative file messaging payment Type/BAN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 6: verify negative file Ineligible Bank | Error message should be displayed.");
		Reporter.log("================================");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyInvalidBankErrorMsg();
		otpSpokePage.verifyAddBankDisabled();
	}

	/**
	 * US484470 Angular 6.0 PCM- Negative file messaging payment Type/BAN
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testPCMNegativeFileOnCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US484470 Angular 6.0 PCM- Negative file messaging payment Type/BAN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 6: verify negative file Ineligible Card | Error message should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyInvalidCardErrorMsg();
		otpSpokePage.verifyAddCardDisabled();
	}

	/**
	 * US484470 Angular 6.0 PCM- Negative file messaging payment Type/BAN
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testPCMNegativeFileOnBAN(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US484470 Angular 6.0 PCM- Negative file messaging payment Type/BAN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Spoke page should be displayed");
		Reporter.log("Step 6: verify negative file on Ban | Error message should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyInvalidErrorMsgOnBAN();
		otpSpokePage.verifyAddCardDisabled();
		otpSpokePage.verifyAddBankDisabled();
	}

	/**
	 * US544509 Migration treatment Lock stored payment method until verified.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyMigrationTreatmentLockStoredPaymentMethodUntilVerified(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : US544509 Migration treatment Lock stored payment method until verified.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log(
				"4. Go to the stored payment and check the blade whether disable or not |  The blade should be greyed out");
		Reporter.log(
				"5. Check for the message 'you must verify this payment method' | Should be able to see the message");
		Reporter.log("6. Check for the Chevron | Should be able to see the Chevron");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyStoredPaymentMethodDisabled();
		spokePage.verifyPaymentVerificationmessage();
		spokePage.verifyChevronForDisabledStoredPaymentMethod();
	}

	/**
	 * US544495 Modify stored payment method UI blade to display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyModifiedStoredPaymentMethodUI(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US544495 Modify stored payment method UI blade to display");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the Spoke page |  Spoke page should be displayed");
		Reporter.log("5. Check for the 'Name' in stored payment method blade | Should be able to see the 'Name'");
		Reporter.log("6. Check for the 'Default' in stored payment method blade | Should be able to see the 'Default'");
		Reporter.log(
				"7. Check for the 'Expiration date' in stored payment method blade | Should be able to see the 'Expiration date'");
		Reporter.log(
				"8. Check for the Chevron | Should be able to see the Chevron for the stored payment method blade");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyModifiedStoredPaymentMethodBlade();
	}

	/**
	 * US545249 6.0 Migration treatment Lock stored payment method until verified.
	 * -+
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyAnguar6MigrationTreatmentLockStoredPaymentMethodUntilValidated(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : US545249 6.0 Migration treatment Lock stored payment method until verified. -+");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log(
				"4. Go to the stored payment and check the blade whether disable or not |  The blade should be greyed out");
		Reporter.log(
				"5. Check for the message 'you must verify this payment method' | Should be able to see the message");
		Reporter.log("6. Check for the Chevron | Should be able to see the Chevron");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToNewSpokePage(myTmoData);
		spokePage.verifyStoredPaymentMethodDisabledAngular();
		spokePage.verifyPaymentVerificationmessageAngular();
		spokePage.verifyChevronForDisabledStoredPaymentMethodAngular();
	}

	/**
	 * US545248 6.0 Modify stored payment method UI blade to display +
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyAngular6ModifiedStoredPaymentMethodUI(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US545248 6.0 Modify stored payment method UI blade to display +");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the New Spoke page | New Spoke page should be displayed");
		Reporter.log("5. Check for the 'Name' in stored payment method blade | Should be able to see the 'Name'");
		Reporter.log("6. Check for the 'Default' in stored payment method blade | Should be able to see the 'Default'");
		Reporter.log(
				"7. Check for the 'Expiration date' in stored payment method blade | Should be able to see the 'Expiration date'");
		Reporter.log(
				"8. Check for the Chevron | Should be able to see the Chevron for the stored payment method blade");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToNewSpokePage(myTmoData);
		spokePage.verifyModifiedStoredPaymentMethodBladeAngular();
	}

	/**
	 * US524296 Angular 1.5 PCM Spoke Page-page Nudge
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifySpokePageNudge(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US524296 Angular 1.5 PCM Spoke Page-page Nudge");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the Spoke page | Spoke page should be displayed");
		Reporter.log("5. Check for the 'Nudge' | Should be able to verify Nudge");
		Reporter.log("6. verify messaging on the Nudge | Should be able to see the messaging");
		Reporter.log("7. Verify the link in the Nudge | Should be able to see the link");
		Reporter.log("8. Click on the link | Should be able to redirect the user to a modal popup");
		Reporter.log("9. Verify the text in the popup | Should be able to see the text");
		Reporter.log("10. Check for the Close CTA | Should be able to verify the Close CTA at the bottom of the page");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyNudge();
		spokePage.verifyMsgOnNudge();
		spokePage.verifyandClickLinkOnNudge();
		spokePage.verifyNudgeModalPopUp();
		spokePage.verifyCloseCTAOnNudgePopUp();
	}

	/**
	 * US555108 Angular 6.0 payment method page nudge
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyPaymentCollectionPageNudge(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US555108 Angular 6.0 payment method page nudge");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the New Spoke page | New Spoke page should be displayed");
		Reporter.log("5. Check for the 'Nudge' | Should be able to verify Nudge");
		Reporter.log("6. verify messaging on the Nudge | Should be able to see the messaging");
		Reporter.log("7. Verify the link in the Nudge | Should be able to see the link");
		Reporter.log("8. Click on the link | Should be able to redirect the user to a modal popup");
		Reporter.log("9. Verify the text in the popup | Should be able to see the text");
		Reporter.log("10. Check for the Close CTA | Should be able to verify the Close CTA at the bottom of the page");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToNewSpokePage(myTmoData);
		spokePage.verifyNudge();
		spokePage.verifyMsgOnNudge();
		spokePage.verifyandClickLinkOnNudge();
		spokePage.verifyNudgeModalPopUp();
		spokePage.verifyCloseCTAOnNudgePopUp();
	}

	/**
	 * US317885: 6.0 Landing Page- Display default
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyAngular6LandingPageDefaultMethod(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US317885: 6.0 Landing Page- Display default");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the New Spoke page | New Spoke page should be displayed");
		Reporter.log("5. Check for the 'Default' in stored payment method blade | Should be able to see the 'Default'");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToNewSpokePage(myTmoData);
		spokePage.verifyDefaultPaymentMethodAngular();
	}

	/**
	 * US524277: Angular 1.5 PCM Spoke Page-In blade treatment
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifySpokePageInBaldeTreatment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US524277: Angular 1.5 PCM Spoke Page-In blade treatment");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log(
				"4. Go to the stored payment and check the blade whether disable or not |  The blade should be greyed out");
		Reporter.log(
				"5. Check for the message 'you must verify this payment method' | Should be able to see the message");
		Reporter.log("6. Check for the Chevron | Should be able to see the Chevron");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyStoredPaymentMethodDisabled();
		spokePage.verifyPaymentVerificationmessage();
		spokePage.verifyChevronForDisabledStoredPaymentMethod();
		spokePage.clickLinkOnGreyedOutBlade();

	}

	/**
	 * US524291: Angular 6 PCM Spoke Page-In blade treatment
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyAngular6PCMSpokePageInBaldeTreatment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US524291: Angular 6 PCM Spoke Page-In blade treatment");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log(
				"4. Go to the stored payment and check the blade whether disable or not |  The blade should be greyed out");
		Reporter.log(
				"5. Check for the message 'you must verify this payment method' | Should be able to see the message");
		Reporter.log("6. Check for the Chevron | Should be able to see the Chevron");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToNewSpokePage(myTmoData);
		spokePage.verifyStoredPaymentMethodDisabledAngular();
		// spokePage.verifyPaymentVerificationmessageAngular();
		spokePage.verifyChevronForDisabledStoredPaymentMethodAngular();
		// Will be handled inn another html story
		/* spokePage.clickLinkOnGreyedOutBlade(); */

	}

	/**
	 * US565359: 6.0 implement Wallet Full functionality
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyAngular6WalletFullFunctionality(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US565359: 6.0 implement Wallet Full functionality");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the New Spoke page | New Spoke page should be displayed");
		Reporter.log("5. Check for the 'Wallet Full' message | Should be able to see the 'Wallet Full' message");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToNewSpokePage(myTmoData);
		spokePage.verifyAngular6WalletFullMsg(PaymentConstants.WALLET_FULL_MESSAGE);
	}

	/**
	 * US570468: 1.5 implement Wallet Full functionality
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyWalletFullFunctionality(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US570468: 1.5 implement Wallet Full functionality");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the Spoke page | Spoke page should be displayed");
		Reporter.log("5. Check for the 'Wallet Full' message | Should be able to see the 'Wallet Full' message");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyWalletFullMsg(PaymentConstants.WALLET_FULL_MESSAGE);
		spokePage.verifyAddBankDisabled();
		spokePage.verifyAddCardDisabled();
	}

	/**
	 * US579700 PII Masking update
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPIIMaskingOnOTPpages(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US356344	PII Masking > Variables on OTP Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: verify PII masking | PII masking attribute should be present for payment method blade");
		Reporter.log("Step 5: click on amount Icon| amount spoke page should be displayed");
		Reporter.log("Step 6: setup other amount and update| amount should be updated");
		Reporter.log("Step 7: click add payment method| payment spoke page should be displayed");
		Reporter.log("Step 8: verify PII masking | PII masking attribute should be present for stored payment details");
		Reporter.log("Step 9: click on add Bank| Bank information page should be displayed");
		Reporter.log("Step 10: Fill all bank details and click continue button| OTP page should be displayed");
		Reporter.log("Step 11: click submit button| OTP confirmation page should be displayed");
		Reporter.log("Step 12: verify all bank payment details| Bank payment details should be displayed correct.");
		Reporter.log(
				"Step 13: verify PII masking | PII masking attributes should be present for all payment information");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyPIIMasking(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		spokePage.clickAddBank();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		addBankPage.verifyPiiMasking(PaymentConstants.PII_CUSTOMER_CUSTOMERNAME_PID,
				PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		addBankPage.clickBackButton();
		spokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyPiiMasking(PaymentConstants.PII_CUSTOMER_CUSTOMERNAME_PID,
				PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID, PaymentConstants.PII_CUSTOMER_ZIPCODE_PID);
		getDriver().navigate().to(System.getProperty("environment") + "/claimflow");
		ClaimPage claimPage = new ClaimPage(getDriver());
		claimPage.verifyPiiMasking(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
	}

	/**
	 * US579700 PII Masking update (6.0 angular pages)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPIIMaskingOnOTPpagesAngular(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US356344	PII Masking > Variables on OTP Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: verify PII masking | PII masking attribute should be present for payment method blade");
		Reporter.log("Step 5: click on amount Icon| amount spoke page should be displayed");
		Reporter.log("Step 6: setup other amount and update| amount should be updated");
		Reporter.log("Step 7: click add payment method| payment spoke page should be displayed");
		Reporter.log("Step 8: verify PII masking | PII masking attribute should be present for stored payment details");
		Reporter.log("Step 9: click on add Bank| Bank information page should be displayed");
		Reporter.log("Step 10: Fill all bank details and click continue button| OTP page should be displayed");
		Reporter.log("Step 11: click submit button| OTP confirmation page should be displayed");
		Reporter.log("Step 12: verify all bank payment details| Bank payment details should be displayed correct.");
		Reporter.log(
				"Step 13: verify PII masking | PII masking attributes should be present for all payment information");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyNewPageLoaded();
		spokePage.verifyPIIMasking(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		spokePage.clickAddBank();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		addBankPage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);
		addBankPage.verifyPiiMasking(PaymentConstants.PII_CUSTOMER_CUSTOMERNAME_PID,
				PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		addBankPage.clickBackButton();
		spokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.verifyPiiMasking(PaymentConstants.PII_CUSTOMER_CUSTOMERNAME_PID,
				PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID, PaymentConstants.PII_CUSTOMER_ZIPCODE_PID);
		getDriver().navigate().to(System.getProperty("environment") + "payments/claimPaymentMethod");
		ClaimPage claimPage = new ClaimPage(getDriver());
		claimPage.verifyPiiMasking(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
	}
}