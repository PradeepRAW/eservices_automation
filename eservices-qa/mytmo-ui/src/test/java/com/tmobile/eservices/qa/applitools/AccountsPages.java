package com.tmobile.eservices.qa.applitools;

import java.time.LocalDateTime;

import org.testng.annotations.Test;

import com.applitools.eyes.BatchInfo;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.data.MyTmoData;

public class AccountsPages extends AccountsCommonLib {
	ApplitoolsLib applitoolsLib = new ApplitoolsLib();
	BatchInfo batchInfo = new BatchInfo("accounts " +  LocalDateTime.now());

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPlansComparisionPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"mytmo","PlanPage",batchInfo);
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanFeaturePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPlanFeaturesPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"mytmo", "PlanFeaturePage",batchInfo);
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanComparisionPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPlanComparisionPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"mytmo", "PlanComparisionPage",batchInfo);
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanConfigurePage(ControlTestData data, MyTmoData myTmoData) {
		//navigateToPlanConfigurationPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"mytmo", "PlanConfigurePage",batchInfo);
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	// @Test(dataProvider = "byColumnName", enabled = true,groups =
	// {"accounts"})
	public void testPlanConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		//navigateToPlanConfirmationPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"mytmo", "PlanConfirmationPage",batchInfo);
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanListPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPlanListPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"mytmo", "PlanListPage",batchInfo);
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanReviewPage(ControlTestData data, MyTmoData myTmoData) {
		//navigateToPlanReviewPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"mytmo", "PlanReviewPage",batchInfo);
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testManageAddonsSelectionPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToManageAddOnsSelectionPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"mytmo", "ManageAddonsSelectionPage",batchInfo);
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testManageAddonsReviewPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToODFFreeRadioButtonToReviewPage(myTmoData, "Service");
		applitoolsLib.doVisualTest(getDriver(),"mytmo", "ManageAddonsReviewPage",batchInfo);
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	// @Test(dataProvider = "byColumnName", enabled = true,groups =
	// {"accounts"})
	public void testManageAddonsConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToManageAddOnsConfirmationPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"mytmo", "ManageAddonsConfirmationPage",batchInfo);
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPlanBenefitsPage(ControlTestData data, MyTmoData myTmoData) {
	//	navigateToPlanBenefitsPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"mytmo", "PlanBenefitsPage",batchInfo);
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "accounts" })
	public void testPromotionsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToMyPromotionsPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"mytmo", "PromotionsPage",batchInfo);
	}

}
