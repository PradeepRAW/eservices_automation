/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.CostDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.LineDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.PlanDetailsPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class CostDetailsPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(CostDetailsPageTest.class);

	/*
	 * US415140 : Cost Details: Navigating the User from Acct Overview Page US415144
	 * : Cost Details: Page Heading US415164 : Cost Details: PAH/FULL Access Users
	 * Only US415154 : Cost Details: Plan Component US449026 : Acct Overview: Shared
	 * Add on Blade Functionality US461235 : [Cost Details] Display Discount Sum
	 * within Line Component
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyCostDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyCostDetailsPageLoadsfromAccountOverviewPage");
		Reporter.log("Test Case : Verify Cost Details Page Loads from Account Overview Page");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on monthly total| User should be navigated to CostDetails Page ");
		Reporter.log("5.Verify that heading on cost details page | CostDetails heading should be available");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		CostDetailsPage costdetailspage = navigateToCostDetailsPage(myTmoData);
		costdetailspage.verifyPlanNameBreadCrump();
		costdetailspage.verifyDisountandPromotionAmount();
		costdetailspage.verifyPlanComponent();
		costdetailspage.verifySharedAddOn();
		costdetailspage.verifyMonthlyTotalDetails();

		costdetailspage.verifyPlanName();
		costdetailspage.verifyListOfLineNames();
		costdetailspage.verifyListOfLineDetails();

		costdetailspage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		costdetailspage.clickOnBreadCrumbLink(1);

		costdetailspage.clickOnLineDetailsLinks(0);

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyCostDetailsPageBackButtonScenarios(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyCostDetailsPageBackButtonScenarios");
		Reporter.log("Test Case : Verify Cost Details Page Loads from Account Overview Page");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on monthly total| User should be navigated to CostDetails Page ");
		Reporter.log("5.Verify that heading on cost details page | CostDetails heading should be available");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		CostDetailsPage costdetailspage = navigateToCostDetailsPage(myTmoData);
		costdetailspage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnbreadcrumbLInks(1);

		costdetailspage.clickOnLineDetailsLinks(0);

		LineDetailsPage lineDetailsPage = new LineDetailsPage(getDriver());
		lineDetailsPage.verifyLineDetailsPage();

		lineDetailsPage.clickOnbreadcrumbLInks(1);
		costdetailspage.verifyCostDetailsPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testCostDetailsPageUpsellURL(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testCostDetailsPageUpsellURL");
		Reporter.log("Test Case : test Cost Details Pag Upsell URL");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking to Cost Details page | Cost Details page should be displayed ");
		Reporter.log("4.Verify Cost Details page header| header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToFutureURLFromHome(myTmoData, "/account/cost-details");
		CostDetailsPage costdetailspage = new CostDetailsPage(getDriver());
		costdetailspage.verifyCostDetailsPage();
	}
}