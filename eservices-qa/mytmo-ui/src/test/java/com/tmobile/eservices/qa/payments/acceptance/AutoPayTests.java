package com.tmobile.eservices.qa.payments.acceptance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.payments.AutoPayConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.AutoPayPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

public class AutoPayTests extends PaymentCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(AutoPayTests.class);

	/**
	 * Ensure user can sign up for AutoPay via One-Time Payment Pitch page using
	 * new checking
	 * 
	 * @param data
	 * @param myTmoData
	 * Data Conditions - Regular user. Eligible for AutoPay
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RETIRED})
	public void verifySignupAutopayOTPcheckingAcct(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySignupAutopayOTPcheckingAcct method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new checking	");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Bank details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. Click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. Click on Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on AutoPay ON link | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Cancel AutoPay link | Cancel AutoPay modal should be displayed");
		Reporter.log("12. Click on Yes button | Cancel Autopay confirmation page should be displayed");
		Reporter.log("13. Click Return Home button | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToAutoPayPage(myTmoData);
		AutoPayPage autopayLandingPage = new AutoPayPage(getDriver());
		autopayLandingPage.verifyPageLoaded();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		autopayLandingPage.clickAddPaymentMethod();
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		autopayLandingPage.verifyAutoPayLandingPageFields();
		autopayLandingPage.clickAgreeAndSubmitBtn();
		AutoPayConfirmationPage confirmationPage = new AutoPayConfirmationPage(getDriver());
		confirmationPage.verifyPageLoaded();
		confirmationPage.verifySuccessHeader();
		confirmationPage.clickreturnToHome();
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		cancelAutoPay(myTmoData);
	}


	/**
	 * Enable AutoPay - Debit/Credit
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RETIRED})
	public void verifyEnableAutoPayOnAndOffnewCard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEnableAutoPayOnAndOffnewCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Enable AutoPay - Debit/Credit");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Card details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. click ON Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Stop easy pay | Manage Autopay Page should be loaded");
		Reporter.log("12. Click on confirm button | Autopay confirmation Page should be displayed");
		Reporter.log("13. Verify Home page | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

	
		HomePage homePage = navigateToHomePage(myTmoData);
		//cancelAutoPay(myTmoData);
		AutoPayPage autoPayPage = navigateToAutoPayPage(myTmoData);
		SpokePage otpSpokePage = new SpokePage(getDriver());
		autoPayPage.clickAddPaymentMethod();
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		autoPayPage.verifyAutoPayLandingPageFields();
		Reporter.log("Card information is added successfully.");
		autoPayPage.verifyAgreeAndSubmitBtnEnabled();
		/*
		 * Note: commented submission code for PPD
		 * autopayLandingPage.agreeAndSubmitBtn(); AutoPayConfirmationPage
		 * confirmationPage = new AutoPayConfirmationPage(getDriver());
		 * Assert.assertTrue(confirmationPage.verifyPageLoaded(),
		 * "Auto page confirmation page is not displayed.");
		 * Assert.assertTrue(confirmationPage.verifySuccessHeader(),
		 * "Auto pay Success message is displayed.");
		 * confirmationPage.clickreturnToHome(); verifyPage("Home Page",
		 * "Home"); cancelAutoPay(homePage);
		 */
	}

	/**
	 * 01_Desktop_MC 2 Series BIN_AutoPay_Set Autopay information_Validate
	 * system accepts new MC BIN series and identifies card brand_Validate
	 * Charge
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RETIRED})
	public void verifySystemAbletoAcceptnewMCBINSeriesCardsForAutopay(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySystemAbletoAcceptnewMCBINSeriesCardsForAutopay method called in EservicesPaymnetsTest");

		Reporter.log("Test Case : verify System Able to Accept new MC BIN Series Cards For Autopay");

		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Card details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. click ON Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Stop easy pay | Manage Autopay Page should be loaded");
		Reporter.log("12. Click on confirm button | Autopay confirmation Page should be displayed");
		Reporter.log("13. Verify Home page | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");

		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		
		AutoPayPage autoPayPage  = navigateToAutoPayPage(myTmoData);
	//	cancelAutoPay(myTmoData);
		// navigateToAutoPayPagefromHomePage(myTmoData);
		SpokePage otpSpokePage = new SpokePage(getDriver());
		autoPayPage.clickAddPaymentMethod();
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		autoPayPage.verifyNewAutoPayLandingPage();
		Reporter.log("Card information is added successfully.");
		autoPayPage.verifyAgreeAndSubmitBtnEnabled();
	}
	
	/**
	 * Verify Auto Pay
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RETIRED})
	public void verifyUserAbleTOAutoPayOnAndOff(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOAutoPayOnAndOff method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO AutoPayOnAndOff");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("If the AUTOPAY is already ON, stop AutoPay to bring to Initial Condition.");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Checking Account details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on Checking Account Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7.click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8.click ON Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10.Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Stop easy pay | Manage Autopay Page should be loaded");
		Reporter.log("12. Click on confirm button | Autopay confirmation Page should be displayed");
		Reporter.log("13. Verify Home page | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");

		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		navigateToAutoPayPage(myTmoData);
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyAutoPayLandingPageFields();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		autoPayPage.clickAddPaymentMethod();
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		//Commenting Auto Pay Submission As We Dont Have Credit Card Details
		/*autopayLandingPage.agreeAndSubmitBtn();
		AutoPayConfirmationPage confirmationPage = new AutoPayConfirmationPage(getDriver());
		Assert.assertTrue(confirmationPage.verifyPageLoaded(),
				"Auto page confirmation page is not displayed.");
		Assert.assertTrue(confirmationPage.verifySuccessHeader(), "Auto pay Success message is displayed.");
		confirmationPage.clickreturnToHome();
		verifyPage("Home Page", "Home");
		cancelAutoPay(homePage);*/
	}


	/**
	 * TC_274839 MYTMO_Cloud_Profile_Verify by editing the easy pay settings
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RETIRED})
	public void editEasypay(ControlTestData data, MyTmoData myTmoData) {
		logger.info("editEasypay - MYTMO_Cloud_Profile_Verify by editing the easy pay settings");
		Reporter.log("Test Case : editEasypay - MYTMO_Cloud_Profile_Verify by editing the easy pay settings");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Verify Auto pay Off | Auto Pay should be OFf");
		Reporter.log("5: Click on Profile link | Profile Page should be displayed");
		Reporter.log("6: Click on Auto Pay Edit /Edit EasyPay link | Manage auto pay Page should be displayed");
		Reporter.log(
				"7: Fill bank account details and click on next button | Auto Pay Review Page should be displayed");
		Reporter.log(
				"8: Click on Terms and Conditions check box and Submit button | Auto pay ON SuccessFul message should display");
		Reporter.log(
				"9: Click on T-Mobile global Icon | User should be redirected to home page and able to see Auto Pay On");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);


		HomePage homePage = navigateToHomePage(myTmoData);
		cancelAutoPay(myTmoData);
		homePage.clickUserProfileLinks(PaymentConstants.HOME_PAGE_RIGHT_HEADERS_PROFILE);
		Reporter.log("Auto Pay is in OFF position");
		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();

		/*profilePage.clickBillingPayments();
		profilePage.clickEasyPay();*/
		AutoPayPage autopayLandingPage = new AutoPayPage(getDriver());
		autopayLandingPage.verifyAutoPayLandingPageFields();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		autopayLandingPage.clickAddPaymentMethod();
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		autopayLandingPage.verifyAutoPayLandingPageFields();
		//Commenting Auto Pay Submission As We Dont Have Credit Card Details
		/*autopayLandingPage.agreeAndSubmitBtn();
		AutoPayConfirmationPage confirmationPage = new AutoPayConfirmationPage(getDriver());
		Assert.assertTrue(confirmationPage.verifyPageLoaded(),
				"Auto page confirmation page is not displayed.");
		Assert.assertTrue(confirmationPage.verifySuccessHeader(), "Auto pay Success message is displayed.");
		confirmationPage.clickreturnToHome();
		homePage.verifyHomePage();
		cancelAutoPay(homePage);*/
	}
	
	/**
	 * MYTMO_TC003_EZPAY_Single Line  user  signs up for Credit Card Easy Pay -
	 * from Navigation
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RETIRED})
	public void signupForEasyPay(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"signupForEasyPay - MYTMO_TC003_EZPAY_Single Line  user  signs up for Credit Card Easy Pay - from Navigation");
		Reporter.log(
				"Test Case : signupForEasyPay - MYTMO_TC003_EZPAY_Single Line  user  signs up for Credit Card Easy Pay - from Navigation");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Auto Pay Edit /Edit EasyPay link | Manage auto pay Page should be displayed");
		Reporter.log("5: Select card radio button | Card type radio button should be selected");
		Reporter.log("6: Fill card details and click on next button | Auto Pay Review Page should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		cancelAutoPay(myTmoData);
		Reporter.log("Auto Pay is in OFF position");
		homePage.clickEasyPay();
		AutoPayPage autopayLandingPage = new AutoPayPage(getDriver());
		autopayLandingPage.verifyAutoPayLandingPageFields();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		autopayLandingPage.clickAddPaymentMethod();
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		autopayLandingPage.verifyAutoPayLandingPageFields();
		Reporter.log("Card information is added successfully.");
		//Commenting Auto Pay Submission As We Dont Have Credit Card Details
		/*autopayLandingPage.agreeAndSubmitBtn();
		AutoPayConfirmationPage confirmationPage = new AutoPayConfirmationPage(getDriver());
		Assert.assertTrue(confirmationPage.verifyPageLoaded(),
				"Auto page confirmation page is not displayed.");
		Assert.assertTrue(confirmationPage.verifySuccessHeader(), "Auto pay Success message is displayed.");
		confirmationPage.clickreturnToHome();
		verifyPage("Home Page", "Home");
		cancelAutoPay(homePage);*/
	}
	
	/**
	 * TC_274864 TC10_GSM_Easy pay with credit from home page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RETIRED})
	public void verifyGsmEasyPayCreditHomePage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyGsmEasyPayCreditHomePage - GsmEasy Pay Credit from Home Page ");
		Reporter.log("Test Case : verifyGsmEasyPayCreditHomePage - GsmEasy Pay Credit from Home Page ");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click Auto Pay Edit link | Auto pay page should display");
		Reporter.log("5: Enter card detils and clik on next | Review payment page should display");
		Reporter.log("6: Click on Accept Terms and conditions check box | Submit button should be enabled");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);


		HomePage homePage = navigateToHomePage(myTmoData);
		cancelAutoPay(myTmoData);
		Reporter.log("Auto Pay is in OFF position");
		homePage.clickEasyPay();
		AutoPayPage autopayLandingPage = new AutoPayPage(getDriver());
		autopayLandingPage.verifyAutoPayLandingPageFields();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		autopayLandingPage.clickAddPaymentMethod();
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		autopayLandingPage.verifyAutoPayLandingPageFields();
		Reporter.log("Card information is added successfully.");
        autopayLandingPage.verifyAgreeAndSubmitBtnEnabled();
		/*
		 * Note: commented submission code for PPD
		 * autopayLandingPage.agreeAndSubmitBtn(); AutoPayConfirmationPage
		 * confirmationPage = new AutoPayConfirmationPage(getDriver());
		 * Assert.assertTrue(confirmationPage.verifyPageLoaded(),
		 * "Auto page confirmation page is not displayed.");
		 * Assert.assertTrue(confirmationPage.verifySuccessHeader(),
		 * "Auto pay Success message is displayed.");
		 * confirmationPage.clickreturnToHome(); verifyPage("Home Page",
		 * "Home"); cancelAutoPay(homePage);
		 */
	}

}
