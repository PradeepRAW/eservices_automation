package com.tmobile.eservices.qa.payments.functional;

import java.io.IOException;
import java.util.Map;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.payments.BillingSummaryPage;
import com.tmobile.eservices.qa.pages.payments.NewAddCardPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayLandingPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayPage;
//import com.tmobile.eservices.qa.pages.payments.NewSpokePage;
import com.tmobile.eservices.qa.pages.payments.OTPConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.PaymentCollectionPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class NewAutoPayPageTest extends PaymentCommonLib {

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutopayPage(ControlTestData data, MyTmoData myTmoData) {

		NewAutopayPage newAutopayPage = navigateToNewAutoPayPage(myTmoData);
		newAutopayPage.verifyAutoPayLandingPageFields();

	}

	/**
	 * US542383 [AutoPay 7] Create T&C and FAQ page Detail
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutopayTermaandConditions(ControlTestData data, MyTmoData myTmoData) {

		NewAutopayPage newAutopayPage = navigateToNewAutoPayPage(myTmoData);
		newAutopayPage.clickAutopayTermsnConditionsLink();
		newAutopayPage.validateAutopayTnCPage();
		newAutopayPage.clickBackButton();

		newAutopayPage.verifyPageLoaded();
	}

	/**
	 * US542383 [AutoPay 7] Create T&C and FAQ page Detail
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutopayFaqPage(ControlTestData data, MyTmoData myTmoData) {

		NewAutopayPage newAutopayPage = navigateToNewAutoPayPage(myTmoData);
		newAutopayPage.clickAutopayFAQLink();
		newAutopayPage.validateAutopayFAQPage();
		newAutopayPage.clickBackButton();

		newAutopayPage.verifyPageLoaded();
	}

	/**
	 * US542387 [AutoPay 7] Create modal for autopay cancellation Details US542386
	 * [AutoPay 7] Implement CTAs for submit/cancel autopay Details
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutopayCancelModal(ControlTestData data, MyTmoData myTmoData) {

		NewAutopayPage newAutopayPage = navigateToNewAutoPayPage(myTmoData);
		// newAutopayPage.ToggleOnAutoPay();
		newAutopayPage.clickCancelButton();
		newAutopayPage.validateAutopayCancelModal();
		newAutopayPage.clickCancelpopupyesButton();
		newAutopayPage.validateAutopayCancelConfirmationpage();
		newAutopayPage.clickReturnHome();
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

	}

	/**
	 * US546201 [AutoPay 7] PCM for AutoPay Angular 7
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutopayPCMBlade(ControlTestData data, MyTmoData myTmoData) {

		NewAutopayPage newAutopayPage = navigateToNewAutoPayPage(myTmoData);
		newAutopayPage.clickPCMBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyNewPageLoaded();

	}

	/**
	 * US542384 [AutoPay 7] Create Confirmation Page and CTAs Details US542386
	 * [AutoPay 7] Implement CTAs for submit/cancel autopay Details
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutopayConfirmationPageElements(ControlTestData data, MyTmoData myTmoData) {

		NewAutopayPage newAutopayPage = navigateToNewAutoPayPage(myTmoData);
//		String paymentType = newAutopayPage.verifyStoredPaymentMethodOrAddBank(myTmoData);
//		newAutopayPage.verifyAutoPayConfirmationPageElements(paymentType);	

	}

	/**
	 * verify Autopay page navigated from home page quick links
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutopayPagefromHomePageQuicklinks(ControlTestData data, MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		homePage.clickonAutopayLink();
		NewAutopayPage newautoPayPage = new NewAutopayPage(getDriver());
		newautoPayPage.verifyPageLoaded();

	}

	/**
	 * verify Autopay page navigated from Billing Page (Payment Settings)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutopayPageNavigatedfromBillingPage(ControlTestData data, MyTmoData myTmoData) {
		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.clickEasyPayArrow();
		NewAutopayPage newautoPayPage = new NewAutopayPage(getDriver());
		newautoPayPage.verifyPageLoaded();

	}

	/**
	 * verify Autopay page navigated from OTP Pitch page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutopayPageNavigatedfromOTPPitchPage(ControlTestData data, MyTmoData myTmoData) {
		OTPConfirmationPage onetimepaymentconfirmationpage = navigateToOTPConfirmationPage(myTmoData);
		onetimepaymentconfirmationpage.clickonEnrollAutopay();
		NewAutopayPage newautoPayPage = new NewAutopayPage(getDriver());
		newautoPayPage.verifyPageLoaded();

	}

	/**
	 * Autopay not eligible for Govt—non master users and business non master users
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, "autopay7" })
	public void testGovernamentNonMasterUsersRedirectToProfilePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US222984	DE94687 testAutoPayRestrictedAndStandardUsersRedirectToProfilePage");
		Reporter.log("Data Conditions - Standard or Restricted user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. click on billing link| Billing summary page should be displayed");
		Reporter.log("4. Click on Auto pay  link | Profile page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		/*
		 * BillingSummaryPage billingSummaryPage =
		 * navigateToBillingSummaryPage(myTmoData);
		 * billingSummaryPage.clickEasyPayArrow();
		 */
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

		getDriver().get(System.getProperty("environment") + "/autopay");
		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyAutopayredirectedtoProfilePageforGovernamentNonMasterUsers(myTmoData);
	}

	/**
	 * Autopay not eligible for Individual standard users
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void testAutopayDisAllowedforstandardusers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US222984	DE94687 testAutoPayRestrictedAndStandardUsersRedirectToProfilePage");
		Reporter.log("Data Conditions - individual Standard user");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. type autopay in url| Pageshoud be navigated to profile");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

		getDriver().get(System.getProperty("environment") + "/autopay");
		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}

	/**
	 * Autopay not eligible for Individual restricted users
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void testAutopayDisAllowedforrestrictedusers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US222984	DE94687 testAutoPayRestrictedAndStandardUsersRedirectToProfilePage");
		Reporter.log("Data Conditions - individual Standard user");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. type autopay in url| Pageshoud be navigated to profile");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

		getDriver().get(System.getProperty("environment") + "/autopay");
		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}

	/**
	 * US578985 Disallow AutoPay for Governament_User_NonMaster_permissoned
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void testAutopayDisAllowedforGovernament_NM_NP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US578985	Disallow AutoPay for Test autopay for Governament_User_NonMaster_NonPAH");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Set-Auto-Pay | Auto pay disallowed modal should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

		getDriver().get(System.getProperty("environment") + "/autopay");
		NewAutopayPage newautoPayPage = new NewAutopayPage(getDriver());
		newautoPayPage.verifyGovLargebusinessModel();
		newautoPayPage.clickGotoHome();
		homePage.verifyPageLoaded();

	}

	/**
	 * US578985 Disallow AutoPay for Governament_User_NonMaster_permissoned
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void testAutopayDisAllowedforGovernament_NM_P(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US578985	Disallow AutoPay for Test autopay for Governament_User_NonMaster_NonPAH");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Set-Auto-Pay | Auto pay disallowed modal should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

		getDriver().get(System.getProperty("environment") + "/autopay");
		NewAutopayPage newautoPayPage = new NewAutopayPage(getDriver());
		newautoPayPage.verifyGovLargebusinessModel();
		newautoPayPage.clickGotoHome();
		homePage.verifyPageLoaded();

	}

	/**
	 * US578985 Disallow AutoPay for Governament_User_NonMaster_permissoned
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void testAutopayDisAllowedforLargeBusiness_NM_NP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US578985	Disallow AutoPay for Test autopay for Governament_User_NonMaster_NonPAH");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Set-Auto-Pay | Auto pay disallowed modal should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

		getDriver().get(System.getProperty("environment") + "/autopay");
		NewAutopayPage newautoPayPage = new NewAutopayPage(getDriver());
		newautoPayPage.verifyGovLargebusinessModel();
		newautoPayPage.clickGotoHome();
		homePage.verifyPageLoaded();

	}

	/**
	 * US578985 Disallow AutoPay for Governament_User_NonMaster_permissoned
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void testAutopayDisAllowedforLargeBusiness_NM_P(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US578985	Disallow AutoPay for Test autopay for Governament_User_NonMaster_NonPAH");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Set-Auto-Pay | Auto pay disallowed modal should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

		getDriver().get(System.getProperty("environment") + "/autopay");
		NewAutopayPage newautoPayPage = new NewAutopayPage(getDriver());
		newautoPayPage.verifyGovLargebusinessModel();
		newautoPayPage.clickGotoHome();
		homePage.verifyPageLoaded();

	}

	/**
	 * US578985 Disallow AutoPay for Governament_User_NonMaster_permissoned
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void testAutopayDisAllowedforactivePA(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US578985	Disallow AutoPay for Test autopay for Governament_User_NonMaster_NonPAH");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Set-Auto-Pay | Auto pay disallowed modal should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

		getDriver().get(System.getProperty("environment") + "/autopay");
		NewAutopayPage newautoPayPage = new NewAutopayPage(getDriver());
		newautoPayPage.verifyActivePAModel();
		newautoPayPage.clickGotoHome();
		homePage.verifyPageLoaded();

	}

	/**
	 * DE234371 Reg9(Autopay) - Sprite name and card number are not displayed.
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular account. Autopay ON
	 * @throws IOException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void testVerifySpriteNameandCardNumber(ControlTestData data, MyTmoData myTmoData) throws IOException {
		Reporter.log("Test case: DE234371 Reg9(Autopay) - Sprite name and card number are not displayed.");
		Reporter.log("Data Conditions - Regular account. Autopay ON");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3. Naviagte to Autopay page| Verify autopay page is displayed");
		Reporter.log("Step 4:enter new card details | card details should be updated");
		Reporter.log("Step 5:verify Sprite Icon is dispalyed | Sprite Icon should be updated");
		Reporter.log("Step 6:verify stored card replaced with new card |stored card should be replaced with new card");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		NewAutopayLandingPage newAutopayPage = navigateToNewAutoPayLandingPage(myTmoData);
		newAutopayPage.clickAddPaymentmethod();
		PaymentCollectionPage pcp = new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickAddCard();
		NewAddCardPage addcard = new NewAddCardPage(getDriver());
		Map<String, String> cardinfo = getCardInfo("master");
		addcard.addCardInformation(cardinfo);
		newAutopayPage.verifyPageLoaded();
		newAutopayPage.CheckSpriteiconisDisplayed();
		newAutopayPage.Checklastfourdigitsofpaymentmethod(cardinfo.get("last4digits"));

	}

}
