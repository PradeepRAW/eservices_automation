package com.tmobile.eservices.qa.webanalytics;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.data.MyTmoData;

public class AccountsFlow extends AccountsCommonLib {
	
	AnalyticsLib al = new AnalyticsLib();
	/***
	 * Test analytics tags in profile page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "analytics1" })
	public void testProfilePageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		navigateToProfilePage(myTmoData);
		Assert.assertTrue(al.verifyPDLTags(getDriver(),"v5", "my/billing/summary.html"));
		al.commonVerifications(getDriver());
	}

}
