/**
 * 
 */
package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.CommonLibrary;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.CartPage;
import com.tmobile.eservices.qa.pages.tmng.functional.SDPPage;
import com.tmobile.eservices.qa.pages.tmng.functional.StoreLocatorPage;

/**
 * @author rnallamilli
 *
 */
public class NewStoreLocatorTest extends CommonLibrary {
	
	/**
	 * US614864: SDP | Add no support for Sprint blurb
	 * TC319660
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.TMNG })
	public void testAddNoSupportForSprintBlurb(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("US614864: SDP | Add no support for Sprint blurb" );
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log("Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 4: Enter City name in search box | Value Entered in the text box successfully");
		Reporter.log("Step 5: click on search icon |  clicked on search icon successfully");
		Reporter.log("Step 6: Verify search results | Search result section loaded successfully");
		Reporter.log("Step 7: Click on a any product repair center store from the store blades | clicked PRC successfully");
		Reporter.log("Step 8: For store doesn't support Sprint related transactions: verify 'This store doesn’t currently support Sprint account management or transactions..' authorable text displayed in the second paragraph | Authorable text 'This store doesn’t currentlsupport Sprint account management or transactions.' should be displayed in the second paregraph");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifyAddNoSupportForSprintBlurb();
		
	}
	
	/**
	 * US614863: SDP | Add Sprint Repair Center blurb
	 * TC319661
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.TMNG })
	public void testAddSprintRepairCenterBlurb(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("US614863: SDP | Add Sprint Repair Center blurb" );
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log("Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 4: Enter City name in search box | Value Entered in the text box successfully");
		Reporter.log("Step 5: click on search icon |  clicked on search icon successfully");
		Reporter.log("Step 6: Verify search results | Search result section loaded successfully");
		Reporter.log("Step 7: Click on a any non  product repair center store from the store blades | clicked PRC successfully");
		Reporter.log("Step 8: For store doesn't support Sprint related transactions: verify 'This store doesn’t currently support Sprint account management or transactions..' authorable text displayed in the second paragraph | Authorable text 'This store doesn’t currentlsupport Sprint account management or transactions.' should be displayed in the second paregraph");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.verifySprintRepairCenterBlurb();
		
	}
	
	/***
	 * US617030 GIL | Name validation and text wrap
	 * TC319576
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TMNG, Group.NON_MOBILE })
	public void verifyNameValidationInGetInLineFormAndItsSuccessModal(TMNGData tMNGData) {
		Reporter.log("US617030: GIL | Name validation and text wrap");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log("Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log("Step 4: Verify search icon and enter Zip Code (which doesnot have Sprint repair centers ) in Search Text Box and click search icon | Zip Code as Search Query parameter is entered and clicked on search icon");
		Reporter.log("Step 5: Verify Zip Code Search result section is loading | Zip Code Search result section loaded successfully");
		Reporter.log("Step 6: Verify search result list store locators | Search result section should load stores with closest distance");
		Reporter.log("Step 7: Click on any TMO store from the store blades | store details page should be displayed");
		Reporter.log("Step 8: Click on Get In Line link | Get In Line form should be displayed");
		Reporter.log("Step 9: Enter more than 40 characters in first name field | First name first should not allow more than 40 characters and it should not display any characters after 40 characters");
		Reporter.log("Step 10: Enter more than 40 characters in Last Name field | Last Name first should not allow more than 40 characters and it should not display any characters after 40 characters");
		Reporter.log("Step 12: Select Reason for your visit | Selected Reason for your visit in Get in line form");
		Reporter.log("Step 13: Verify by default Notify checkbox is unchecked | Notify checkbox is unchecked by default");
		Reporter.log("Step 14: User language preference is English| Successfully changed user language preference.");
		Reporter.log("Step 15: User click on 'Get in line' CTA | 'Get in line' CTA was highlighted and clicked successfully.");
		Reporter.log("Step 16: Verify 'Get in line' success modal with its header | 'Get in line' success modal should be displayed successfully.");
		Reporter.log("Step 17: Verify the maximum length of the name in Get in line success modal | Verified successfully the length of the Name displayed in Success modal");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");
		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.selectSecondStoreInSearchResults();
		storeLocatorPage.verifyAndclickOnGetInLineCTAInStoreDetails();
		storeLocatorPage.enterFirstNameInGetInLine(tMNGData);
		storeLocatorPage.enterLastNameInGetInLine(tMNGData);
		String firstName = storeLocatorPage.retrieveFirstName();
		storeLocatorPage.verifyFirstNameMaxLimit();
		storeLocatorPage.verifyLastNameMaxLimit();	
		storeLocatorPage.selectReasonForYourVisit();
		storeLocatorPage.verifyNotifyCheckboxDefaultState();
		storeLocatorPage.clickOnEnglishRadioButton();
		storeLocatorPage.verifyAndclickOnGetInLineCTA();
		storeLocatorPage.verifyGetInLineSuccessModal(firstName);
		storeLocatorPage.verifyFirstNameLengthInSuccessModal();
	}
	
	/***
	 * US422723 TMO - GLOBAL - Store types on Search Blade TC ID - TC245243
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testStoreTypesOnSearchBladeForState(TMNGData tMNGData) {
		Reporter.log("US422723 TMO - GLOBAL - Store types on Search Blade");
		Reporter.log("Data Condition | Any Query Parameter (State) for Search Bar ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"Step 2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("Step 3: Verify Search text box is loading | Search Text Box loaded successfully");
		Reporter.log(
				"Step 4: Verify search icon and enter State in Search Text Box and click search icon | State as  Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 5: Verify State Search result section is loading | State Search result section loaded successfully");
		Reporter.log(
				"Step 6: Click on the Store containing a TPR and verify text \"Third Party Retailers\" for loaded search section | Text for search section has been successfully verified.");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.verifySearchIconIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getState());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.clickOnCitySearchBlade(tMNGData.getCity());
		storeLocatorPage.verifyThirdPartyRetailersTagInCitySearchResultBlades();
	}
	
	/***
	 * US561346 SL New Designs|Add Sorting by Wait times & distance TC294310
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testStoreLocatorCitySortingByWaitTimes(TMNGData tMNGData) {

		Reporter.log("US561346:SL New Designs|Add Sorting by Wait times & distance");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to T-Mobile.com | T-Mobile landing page is displayed");
		Reporter.log(
				"2: Click in Store Locator link on Top Right corner of landing page | Store Locator page should be displayed");
		Reporter.log("3.Enter the city in search box  | city should be entered successfully");
		Reporter.log("4.click on search box | clicked search box successfully");
		Reporter.log(
				"Step 5: Verify Zip Code Search result section is loading | Zip Code Search result section loaded successfully");
		Reporter.log("5: Click on drop down menu |  clicked on drop down menu successfully");
		Reporter.log("6: Select distance from drop down menu | selected distance from drop down menu successfully");
		Reporter.log("7: verify the search results | search results are in sorted order");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.clickOnSortDropDown();
		storeLocatorPage.selectWaitTimeFromDropDown();
		storeLocatorPage.verifySearchResultSortedByWaitTime();
	}
	
	/***
	 * CDCDWR2-444 Defects- Mustangs - S21 - EP-18480 - Prod: Store locator: Appointment: Error message for "Phone Number" field in appointment form is not showing up in some situation
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testVerifyPhoneNumberFieldValidationOnAppointmentFlow(TMNGData tMNGData) {
		Reporter.log("CDCDWR2-444 Defects- Mustangs - S21 - EP-18480 - Prod: Store locator: Appointment: Error message for Phone Number field in appointment form is not showing up in some situation");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com storelocator page |  Store Locator page should be displayed");
		Reporter.log(
				"Step 2: Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log(
				"Step 3: Verify City Search result section is loading | City Search result section loaded successfully");
		Reporter.log(
				"Step 4: Select city which have appoinments available | SDP page should be displayed");
		Reporter.log("Step 5: Click Appoinment CTA | Time and data selection table should be displayed");
		Reporter.log(
				"Step 6: Select available time and click Naxt CTA | Personal info table should be displayed");
		Reporter.log(
				"Step 7: Validate error messgae for phone number fileds with no data | Error message should be displayed for Phone number with empty data");
		Reporter.log(
				"Step 8: Enter valid phone number in phone number filed | Error message should not be displayed");
		Reporter.log(
				"Step 9: Clear the phone number field | Error message should be displayed again for phone number");
		Reporter.log("=================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getZipcode());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.selectStoreWhichHasAppointments();
		storeLocatorPage.verifyStoreNameInStoreDetails();
		storeLocatorPage.clickAppointmentCTA();
		storeLocatorPage.verifyTimeAndDateTable();
		storeLocatorPage.selectTimeSlotforAppointment();
		storeLocatorPage.clickNextCTAOnTimeAndDateTable();
		storeLocatorPage.verifyPhoneNumberTextBox();
		storeLocatorPage.verifyErrorMessageForPhoneNumber();
		storeLocatorPage.setPhoneNumber(tMNGData);
		storeLocatorPage.verifyErrorMessageForPhoneNumberNotDisplayedForValidData();
		storeLocatorPage.verifyErrorMessageForPhoneNumber();
	}

	
	/**
	 * CDCDWR2-1028: SL UI | SL Detail Page static maps on mobile | remaining work
	 * TC319660
	 * 
	 * @param data
	 * @param tMNGData
	 *//*
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.TMNG })
	public void testStaticMapRenderingForMobiles(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("CDCDWR2-1028: SL UI | SL Detail Page static maps on mobile | remaining work" );
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to T-Mobile.com storelocator page |  Store Locator page should be displayed");
		Reporter.log("Step 2: Verify search icon and enter City in Search Text Box and click search icon | City as Search Query parameter is entered and clicked on search icon");
		Reporter.log("Step 3: Verify City Search result section is loading | City Search result section loaded successfully");
		Reporter.log("Step 4: Select city which have appoinments available | SDP page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifySearchTextBoxIsDisplayed();
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getZipcode());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		storeLocatorPage.selectFirstStore();
		storeLocatorPage.verifyStoreDetailsPage();
	}*/
	
	/**
	 * US568881 - SL | Photo Carousel | Get photos and thumbnails for store 
	 * US588839 - SL | Photo Carousel | Images Selected State
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void testPhotoCarouselGetPhotosAndThumbnailsForStore(TMNGData tMNGData) {
		Reporter.log("US568881 - SL | Photo Carousel | Get photos and thumbnails for store");
		Reporter.log("======================================================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 2: Click on Store locator link  | Store locator page should be displayed");
		Reporter.log("Step 3: Click on Log in  | Log in successful and Create special hours page should be displayed");
		Reporter.log("Step 4: Enter a particular store which has Store exterior or interior photos | Store details entered");
		Reporter.log("Step 5: Select the Store by click on the store address | Store details page displayed");
		Reporter.log("Step 6: Enter valid City/Zip code/State details |  valid City/Zip code/State details entered");
		Reporter.log("Step 7: Validate Photos displayed | Photo should be displayed");
		Reporter.log("Step 8: Click on the preview photo | preview photo should be displayed, photo src is coming from cdn");
		Reporter.log("Step 9: Validate in case of more than 1 photo | Left caret link and Right Caret link should be displayed");
		Reporter.log("Step 10: Validate Click on right Caret  navigate to next image | next Image should be displayed, photo source is from cdn");
		Reporter.log("Step 11: validate Click on left caret navigate to previous Image | preview photo should be displayed, photo src is coming from cdn");
		Reporter.log("Step 12: Validate number of '.' (dots) equals to number of photos | number of dot's displayed should be equal to number of images");
		Reporter.log("======================================================================");
		Reporter.log("Actual Output:");
		navigateToStoreLocatorPage(tMNGData);
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.setTextIntoTheSearchTextBox(tMNGData.getCity());
		storeLocatorPage.clickSearchIcon();
		storeLocatorPage.verifySearchResultSectionIsDisplayed();
		int numberOfImages=storeLocatorPage.verifyImageIsDisplayed();
		storeLocatorPage.verifyImagesForCdn();
		storeLocatorPage.clickphotoPreview();
		storeLocatorPage.verifyDotsEqualsImages(numberOfImages);
		storeLocatorPage.verifyRightNavigateToNextImage();
		storeLocatorPage.verifyLeftNavigateToNextImage();
		

	}
}