package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.accounts.TMobileIDPage;

public class TMobileIDPageTest extends GlobalCommonLib {

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void testNameChange(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on TMobileId link | TMobileId page should be displayed");
		Reporter.log("6: Verify T-Mobile ID crumb active status| T-Mobile ID bread crumb should be active");
		Reporter.log(
				"7: Click on profile bread crumb and verify profile bread crumb active status | Profile bread crumb should be active");
		Reporter.log("8: Verify profile page | Profile page should be displayed");
		Reporter.log("9. Click on TMobileId link | TMobileId page should be displayed");
		Reporter.log(
				"10.Click on edit link for name and enter data| Edit link should be clicked and data should be entered");
		Reporter.log("11.Click on save button | Data should be saved");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TMobileIDPage tmobileIDPage = navigateToTMobileIDPage(myTmoData, "T-Mobile ID");
		tmobileIDPage.verifyBreadCrumbActiveStatus("T-Mobile ID");
		tmobileIDPage.clickProfileHomeBreadCrumb();
		tmobileIDPage.verifyBreadCrumbActiveStatus("Profile");

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
		profilePage.clickProfilePageLink("T-Mobile ID");

		tmobileIDPage.verifyTMobileIDPage();
		tmobileIDPage.clickNameEditBtn();
		tmobileIDPage.updateNameAndClickSave();
		tmobileIDPage.verifyTMobileIDPage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void testEmailChange(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on TMobileId link | TMobileId page should be displayed");
		Reporter.log(
				"6. Click on edit link beside email and enter data| Edit link should be clicked and data should be entered");
		Reporter.log("7. Click on save button | Save button should be clicked");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TMobileIDPage tmobileIDPage = navigateToTMobileIDPage(myTmoData, "T-Mobile ID");
		tmobileIDPage.clickEmailEditBtn();
		tmobileIDPage.updateEmail();
		tmobileIDPage.verifyEmailErrorMesage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void testSecurityQuestionChange(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on TMobileId link | TMobileId page should be displayed");
		Reporter.log("6. Click on Edit security questions |Security questions should be displayed");
		Reporter.log("7. Enter answers for security questions and click on save | TMobileId page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TMobileIDPage tmobileIDPage = navigateToTMobileIDPage(myTmoData, "T-Mobile ID");
		tmobileIDPage.clickSecurityQstnsEditBtn();
		tmobileIDPage.updateSecurityQstnsAndClickSave();
		tmobileIDPage.verifyTMobileIDPage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyLinkedPhoneNumbers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on TMobileId link | TMobileId page should be displayed");
		Reporter.log(
				"6. Click on phonenumber edit btn and verify linked phone number | Linked phone number should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TMobileIDPage tmobileIDPage = navigateToTMobileIDPage(myTmoData, "T-Mobile ID");
		tmobileIDPage.clickPhoneNumberEditBtn();
		tmobileIDPage.verifyLinkedPhoneNumber();
	}

	/**
	 * 'TC003_MyTMO_Desktop - Verify checkbox text when user selects edit name in
	 * profile management page Test ID:3273876
	 * 
	 * @param controlTestData
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyNickNameCheckOrUncheck(ControlTestData controlTestData, MyTmoData myTmoData) {
		logger.info("verifyCheckNickName method called in TMobileIDTest");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("Test Case:Desktop-Verify Nick Name Check Or Uncheck in profile page");
		Reporter.log("================================");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Profile Link | Profile page should display");
		Reporter.log("5. Click on Tmobile ID Link |T_mobile id details should display");
		Reporter.log(
				"6. Click on Name edit link | Make this the nickname for all lines linked to your T-Mobile ID checkbox should be checked");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TMobileIDPage tmobileIDPage = navigateToTMobileIDPage(myTmoData, "T-Mobile ID");
		tmobileIDPage.clickNameEditBtn();
		tmobileIDPage.verifyNicknameCheckboxIsChecked();
	}

	/**
	 * US327336/US454528:Profile | Re-Launch Enhancements | Rename Home |
	 * Breadcrumbs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyTMobileIDPageBreadCrumbsLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on TMobileId link | TMobileId page should be displayed");
		Reporter.log("6: Verify T-Mobile ID crumb active status| T-Mobile ID bread crumb should be active");
		Reporter.log(
				"7: Click on profile bread crumb and verify profile bread crumb active status | Profile bread crumb should be active");
		Reporter.log("8: Verify profile page | Profile page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		TMobileIDPage tmobileIDPage = navigateToTMobileIDPage(myTmoData, "T-Mobile ID");
		tmobileIDPage.verifyBreadCrumbActiveStatus("T-Mobile ID");
		tmobileIDPage.clickProfileHomeBreadCrumb();
		tmobileIDPage.verifyBreadCrumbActiveStatus("Profile");

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}
}
