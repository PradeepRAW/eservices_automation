package com.tmobile.eservices.qa.global.api;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.CustomerAccountApiV1;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class CustomerAccountApiV1Test extends CustomerAccountApiV1 {
	public Map<String, String> tokenMap;
	
	@BeforeMethod(alwaysRun = true)
	public void clearTokenMapCommon(){
		tokenMapCommon = null;
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CustomerAccountApi", "getCustomerAccountDetails", Group.GLOBAL,Group.APIREG })
	public void testGetCustomerAccountDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: CustomerAccountApi getCustomerAccountDetails ");
		Reporter.log("Data Conditions:MyTmo registered misdn. ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: getCustomerAccountDetails for the given ban and msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		
		String operationName="CustomerAccountApi-getCustomerAccountDetails";
		Response rs = getCustomerAccountDetails(apiTestData,tokenMap);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				/*
				 * String accountNumber= getPathVal(jsonNode, "accountNumber");
				 * Assert.assertEquals(accountNumber,tokenMap.get("ban"),"Invalid ban.");
				 */
				String accountType= getPathVal(jsonNode, "accountType");
				Assert.assertNotEquals(accountType,"","Invalid accountType.");
				String accountSubType= getPathVal(jsonNode, "accountSubType");
				Assert.assertNotEquals(accountSubType,"","Invalid accountSubType.");
			}
		}else{
			failAndLogResponse(rs, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CustomerAccountApi", "getSubscriberLines", Group.GLOBAL,Group.APIREG })
	public void testGetSubscriberLines(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: CustomerAccountApi getSubscriberLines ");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: getSubscriberLines for the given ban and msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		String operationName="CustomerAccountApi-getSubscriberLines";
		Response rs = getSubscriberLines(apiTestData,tokenMap);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				/*
				 * String accountNumber= getPathVal(jsonNode, "accountNumber"); String
				 * loggedInMsisdn= getPathVal(jsonNode, "loggedInMsisdn");
				 */
				String lines= getPathVal(jsonNode, "lines.msisdn");
				/*
				 * Assert.assertEquals(accountNumber,tokenMap.get("ban"),"Invalid ban");
				 * Assert.assertEquals(loggedInMsisdn,tokenMap.get("msisdn"),"Invalid Msisdn")
				 */;
				Assert.assertNotEquals(lines,"","Invalid Lines");
			}
		}else{
			failAndLogResponse(rs, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CustomerAccountApi", "getCustomerAccountWithImeiDetails", Group.GLOBAL,Group.PENDING })
	public void testGetCustomerAccountWithImeiDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: CustomerAccountApi getCustomerAccountDetails with imei ");
		Reporter.log("Data Conditions:MyTmo registered misdn. ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: getCustomerAccountDetails for the given ban and msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		
		String operationName="CustomerAccountApi-getCustomerAccountDetails";
		Response rs = getCustomerAccountDetails(apiTestData,tokenMap);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				/*
				 * String accountNumber= getPathVal(jsonNode, "accountNumber");
				 * Assert.assertEquals(accountNumber,tokenMap.get("ban"),"Invalid ban.");
				 */
				String accountType= getPathVal(jsonNode, "accountType");
				Assert.assertNotEquals(accountType,"","Invalid accountType.");
				String accountSubType= getPathVal(jsonNode, "accountSubType");
				Assert.assertNotEquals(accountSubType,"","Invalid accountSubType.");
				String imei= getPathVal(jsonNode, "myLine.imei");
				Assert.assertNotEquals(imei,"","Invalid imei.");
			}
		}else{
			failAndLogResponse(rs, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CustomerAccountApi", "testGetCustomerABC", Group.GLOBAL,Group.SPRINT })
	public void testGetCustomerABC(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: CustomerAccountApi testGetCustomerABC ");
		Reporter.log("Data Conditions:MyTmo registered misdn. ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: testGetCustomerABC for the given ban and msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		
		String operationName="CustomerAccountApi-getCustomerABC";
		Response rs = getCustomerAccountDetails(apiTestData,tokenMap);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				String authUrl= getPathVal(jsonNode, "authUrl");
				Assert.assertNotEquals(authUrl,"","Invalid authUrl.");
			}
		}else{
			failAndLogResponse(rs, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CustomerAccountApi", "getPrepaidCustomerAccountDetails", Group.GLOBAL,Group.APIREG })
	public void testGetPrepaidCustomerAccountDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: CustomerAccountApi getPrepaidCustomerAccountDetails ");
		Reporter.log("Data Conditions:MyTmo registered misdn. ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: getPrepaidCustomerAccountDetails for the given ban and msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		
		String operationName="CustomerAccountApi-getPrepaidCustomerAccountDetails";
		Response rs = getPrepaidAccount(apiTestData, tokenMap);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				String accountType= getPathVal(jsonNode, "accountType");
				Assert.assertNotEquals(accountType,"","Invalid accountType.");
				String accountSubType= getPathVal(jsonNode, "accountSubType");
				Assert.assertNotEquals(accountSubType,"","Invalid accountSubType.");
			}
		}else{
			failAndLogResponse(rs, operationName);
		}
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CustomerAccountApi1", "testGetCustomerDNSDetails", Group.GLOBAL,Group.SPRINT })
	public void testGetCustomerDNSDetails_TMOID(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: CustomerAccountApi testGetCustomerDNSDetails ");
		Reporter.log("Data Conditions:MyTmo registered misdn. ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: testGetCustomerDNSDetails for the given ban and msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("Msisdn", apiTestData.getMsisdn());
		tokenMap.put("idType", "TMOID");
		tokenMap.put("brand", "Magenta");
		tokenMap.put("id", "U-eabfee19-b366-4889-ad9b-bb50045944aa");
		tokenMap.put("dnsRulesFlag", "Yes");
		tokenMap.put("localDnsValue", "Sell");
		tokenMap.put("dnsType", "Global");
		
		String requestBody = new ServiceTest().getRequestFromFile("getCustomerDNSDetails.txt");
		String operationName="CustomerAccountApi-getCustomerDNSDetails";
		Response rs = getCustomerDNSDetails(apiTestData,requestBody,tokenMap);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				String localDnsValue= getPathVal(jsonNode, "localDnsValue");
				String globalDnsValue= getPathVal(jsonNode, "globalDnsValue");
				Assert.assertNotEquals(globalDnsValue,"","Invalid globalDnsValue.");
				Assert.assertNotEquals(localDnsValue,"","Invalid localDnsValue.");
			}
		}else{
			failAndLogResponse(rs, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CustomerAccountApi1", "testGetCustomerDNSDetails", Group.GLOBAL,Group.SPRINT })
	public void testGetCustomerDNSDetails_AccountID(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: CustomerAccountApi testGetCustomerDNSDetails ");
		Reporter.log("Data Conditions:MyTmo registered misdn. ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: testGetCustomerDNSDetails for the given ban and msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("Msisdn", apiTestData.getMsisdn());
		tokenMap.put("idType", "AccountID");
		tokenMap.put("brand", "TVision");
		tokenMap.put("id", apiTestData.getMsisdn());
		tokenMap.put("dnsRulesFlag", "Yes");
		tokenMap.put("localDnsValue", "Sell");
		tokenMap.put("dnsType", "Global");
		
		String requestBody = new ServiceTest().getRequestFromFile("getCustomerDNSDetails.txt");
		String operationName="CustomerAccountApi-getCustomerDNSDetails";
		Response rs = getCustomerDNSDetails(apiTestData,requestBody,tokenMap);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				String localDnsValue= getPathVal(jsonNode, "localDnsValue");
				String globalDnsValue= getPathVal(jsonNode, "globalDnsValue");
				Assert.assertNotEquals(globalDnsValue,"","Invalid globalDnsValue.");
				Assert.assertNotEquals(localDnsValue,"","Invalid localDnsValue.");
			}
		}else{
			failAndLogResponse(rs, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CustomerAccountApi1", "testGetCustomerDNSDetails", Group.GLOBAL,Group.SPRINT })
	public void testGetCustomerDNSDetails_SubscriberID(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: CustomerAccountApi testGetCustomerDNSDetails ");
		Reporter.log("Data Conditions:MyTmo registered misdn. ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: testGetCustomerDNSDetails for the given ban and msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("Msisdn", apiTestData.getMsisdn());
		tokenMap.put("idType", "SubscriberID");
		tokenMap.put("brand", "MbyT");
		tokenMap.put("id", "U-eabfee19-b366-4889-ad9b-bb50045944aa");
		tokenMap.put("dnsRulesFlag", "Yes");
		tokenMap.put("localDnsValue", "Sell");
		tokenMap.put("dnsType", "Global");
		
		String requestBody = new ServiceTest().getRequestFromFile("getCustomerDNSDetails.txt");
		String operationName="CustomerAccountApi-getCustomerDNSDetails";
		Response rs = getCustomerDNSDetails(apiTestData,requestBody,tokenMap);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				String localDnsValue= getPathVal(jsonNode, "localDnsValue");
				String globalDnsValue= getPathVal(jsonNode, "globalDnsValue");
				Assert.assertNotEquals(globalDnsValue,"","Invalid globalDnsValue.");
				Assert.assertNotEquals(localDnsValue,"","Invalid localDnsValue.");
			}
		}else{
			failAndLogResponse(rs, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CustomerAccountApi1", "testGetCustomerDNSDetails", Group.GLOBAL,Group.SPRINT })
	public void testGetCustomerDNSDetails_SubscriberIDTFB(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: CustomerAccountApi testGetCustomerDNSDetails ");
		Reporter.log("Data Conditions:MyTmo registered misdn. ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: testGetCustomerDNSDetails for the given ban and msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("Msisdn", apiTestData.getMsisdn());
		tokenMap.put("idType", "SubscriberID");
		tokenMap.put("brand", "TFB");
		tokenMap.put("id", "U-eabfee19-b366-4889-ad9b-bb50045944aa");
		tokenMap.put("dnsRulesFlag", "Yes");
		tokenMap.put("localDnsValue", "Sell");
		tokenMap.put("dnsType", "Global");
		
		String requestBody = new ServiceTest().getRequestFromFile("getCustomerDNSDetails.txt");
		String operationName="CustomerAccountApi-getCustomerDNSDetails";
		Response rs = getCustomerDNSDetails(apiTestData,requestBody,tokenMap);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				String localDnsValue= getPathVal(jsonNode, "localDnsValue");
				String globalDnsValue= getPathVal(jsonNode, "globalDnsValue");
				Assert.assertNotEquals(globalDnsValue,"","Invalid globalDnsValue.");
				Assert.assertNotEquals(localDnsValue,"","Invalid localDnsValue.");
			}
		}else{
			failAndLogResponse(rs, operationName);
		}
	}
}
