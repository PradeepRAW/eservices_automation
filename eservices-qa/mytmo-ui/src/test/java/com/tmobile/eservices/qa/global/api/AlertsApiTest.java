package com.tmobile.eservices.qa.global.api;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.AlertsApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;


public class AlertsApiTest extends AlertsApi {

	
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AlertsApiTest",Group.GLOBAL,Group.APIREG })
	public void testFetchCustomerAlerts_Critical(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: fetchCustomerAlertsTest");
		Reporter.log("Data Conditions:MyTmo registered misdn and Alert Level.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Alerts should be reurned for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
				
		String requestBody="{\r\n  \"alertLevel\" : \"CRITICAL\",\r\n  \"weight\" : \"900000\",\r\n  \"maximumAlerts\" : \"30\"\r\n}";	
		String operationName="FetchCustomerAlerts_Critical";
		logRequest(requestBody, operationName);
		Response response = fetchCustomerAlerts(apiTestData, requestBody);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if(!jsonNode.get("alerts").isMissingNode() && jsonNode.get("alerts").isArray()){					
					for (int nodeIndex = 0; nodeIndex < jsonNode.size(); nodeIndex++) {						
						if (!jsonNode.get("alerts").get(nodeIndex).isMissingNode() && jsonNode.get("alerts").get(nodeIndex).isObject()) {
							Assert.assertTrue("critical".equalsIgnoreCase(jsonNode.get("alerts").get(nodeIndex).get("level").asText()));
						}
					}				
				}								
			}
		} else {
			failAndLogResponse(response, operationName);
		}		
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AlertsApiTest",Group.GLOBAL,Group.APIREG })
	public void testFetchCustomerAlerts_Error(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: fetchCustomerAlertsTest");
		Reporter.log("Data Conditions:MyTmo registered misdn and Alert Level.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Alerts should be reurned for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="FetchCustomerAlerts_Error";
		String requestBody="{\r\n  \"alertLevel\" : \"ERROR\",\r\n  \"weight\" : \"900000\",\r\n  \"maximumAlerts\" : \"30\"\r\n}";		
		logRequest(requestBody, operationName);
		Response response =fetchCustomerAlerts(apiTestData, requestBody);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if(!jsonNode.get("alerts").isMissingNode() && jsonNode.get("alerts").isArray()){					
					for (int nodeIndex = 0; nodeIndex < jsonNode.size(); nodeIndex++) {						
						if (!jsonNode.get("alerts").get(nodeIndex).isMissingNode() && jsonNode.get("alerts").get(nodeIndex).isObject()) {
							if("Error".equalsIgnoreCase(jsonNode.get("alerts").get(nodeIndex).get("level").asText())){
							Assert.assertTrue("Error".equalsIgnoreCase(jsonNode.get("alerts").get(nodeIndex).get("level").asText()));
							}
						}
					}				
				}								
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AlertsApiTest",Group.GLOBAL,Group.APIREG })
	public void testFetchCustomerAlerts_Warn(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: fetchCustomerAlertsTest");
		Reporter.log("Data Conditions:MyTmo registered misdn and Alert Level.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Alerts should be reurned for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="FetchCustomerAlerts_Warn";
		String requestBody="{\r\n  \"alertLevel\" : \"WARN\",\r\n  \"weight\" : \"900000\",\r\n  \"maximumAlerts\" : \"30\"\r\n}";
		logRequest(requestBody, operationName);
		Response response =fetchCustomerAlerts(apiTestData, requestBody);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if(!jsonNode.get("alerts").isMissingNode() && jsonNode.get("alerts").isArray()){					
					for (int nodeIndex = 0; nodeIndex < jsonNode.size(); nodeIndex++) {						
						if (!jsonNode.get("alerts").get(nodeIndex).isMissingNode() && jsonNode.get("alerts").get(nodeIndex).isObject()) {
							if("warn".contains(jsonNode.get("alerts").get(nodeIndex).get("level").asText())){
								Assert.assertTrue("warn".contains(jsonNode.get("alerts").get(nodeIndex).get("level").asText()));
								}						
						}
					}				
				}								
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AlertsApiTest",Group.GLOBAL,Group.APIREG })
	public void testFetchCustomerAlerts_Info(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: fetchCustomerAlertsTest");
		Reporter.log("Data Conditions:MyTmo registered misdn and Alert Level.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Alerts should be reurned for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="FetchCustomerAlerts_Info";
		String requestBody="{\r\n  \"alertLevel\" : \"INFO\",\r\n  \"weight\" : \"900000\",\r\n  \"maximumAlerts\" : \"30\"\r\n}";
		logRequest(requestBody, operationName);
		Response response =fetchCustomerAlerts(apiTestData, requestBody);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if(!jsonNode.get("alerts").isMissingNode() && jsonNode.get("alerts").isArray()){					
					for (int nodeIndex = 0; nodeIndex < jsonNode.size(); nodeIndex++) {						
						if (!jsonNode.get("alerts").get(nodeIndex).isMissingNode() && jsonNode.get("alerts").get(nodeIndex).isObject()) {
							if("Info".equalsIgnoreCase(jsonNode.get("alerts").get(nodeIndex).get("level").asText())){
								Assert.assertTrue("Info".equalsIgnoreCase(jsonNode.get("alerts").get(nodeIndex).get("level").asText()));
								}
						}
					}				
				}								
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AlertsApiTest",Group.GLOBAL,Group.APIREG })
	public void testFetchCustomerAlerts_Okay(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: fetchCustomerAlertsTest");
		Reporter.log("Data Conditions:MyTmo registered misdn and Alert Level.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Alerts should be reurned for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="FetchCustomerAlerts_Okay";
		String requestBody="{\r\n  \"alertLevel\" : \"OKAY\",\r\n  \"weight\" : \"900000\",\r\n  \"maximumAlerts\" : \"30\"\r\n}";
		logRequest(requestBody, operationName);
		Response response =fetchCustomerAlerts(apiTestData, requestBody);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if(!jsonNode.get("alerts").isMissingNode() && jsonNode.get("alerts").isArray()){					
					for (int nodeIndex = 0; nodeIndex < jsonNode.size(); nodeIndex++) {						
						if (!jsonNode.get("alerts").get(nodeIndex).isMissingNode() && jsonNode.get("alerts").get(nodeIndex).isObject()) {
							if("Okay".equalsIgnoreCase(jsonNode.get("alerts").get(nodeIndex).get("level").asText())){
								Assert.assertTrue("Okay".equalsIgnoreCase(jsonNode.get("alerts").get(nodeIndex).get("level").asText()));
								}
						}
					}				
				}								
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AlertsApiTest",Group.GLOBAL,Group.APIREG })
	public void testFetchCustomerAlerts_Promo(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: fetchCustomerAlertsTest");
		Reporter.log("Data Conditions:MyTmo registered misdn and Alert Level.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Alerts should be reurned for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="FetchCustomerAlerts_Promo";
		String requestBody="{\r\n  \"alertLevel\" : \"PROMO\",\r\n  \"weight\" : \"900000\",\r\n  \"maximumAlerts\" : \"30\"\r\n}";		
		logRequest(requestBody, operationName);
		Response response =fetchCustomerAlerts(apiTestData, requestBody);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if(!jsonNode.get("alerts").isMissingNode() && jsonNode.get("alerts").isArray()){					
					for (int nodeIndex = 0; nodeIndex < jsonNode.size(); nodeIndex++) {						
						if (!jsonNode.get("alerts").get(nodeIndex).isMissingNode() && jsonNode.get("alerts").get(nodeIndex).isObject()) {
							if("promo".equalsIgnoreCase(jsonNode.get("alerts").get(nodeIndex).get("level").asText())){
								Assert.assertTrue("promo".equalsIgnoreCase(jsonNode.get("alerts").get(nodeIndex).get("level").asText()));
								}
						}
					}				
				}								
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
}
