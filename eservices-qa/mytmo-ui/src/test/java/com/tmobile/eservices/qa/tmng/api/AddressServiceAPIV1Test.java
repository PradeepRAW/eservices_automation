package com.tmobile.eservices.qa.tmng.api;

import java.util.Map;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.tmng.api.AddressCredirProfileID;
import com.tmobile.eservices.qa.pages.tmng.api.AddressProfileV1;
import com.tmobile.eservices.qa.pages.tmng.api.AddressServiceAPIV1;
import com.tmobile.eservices.qa.shop.ShopConstants;

import io.restassured.response.Response;

public class AddressServiceAPIV1Test extends AddressServiceAPIV1 {

	/**
	 * UserStory# Description: US633800 - TPP R2 - EOS Technical - Integrate Address
	 * With Valid payload to verify if response is success.
	 * 
	 * @param data
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void eosAddressServiceValidationWithValidPayload(ControlTestData data, ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {

		Reporter.log("TestName: tmng address service validation");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log("Step 2: Verify Address service response with valid pay load| Response should be 200 OK");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmobileProfileID.txt");
		AddressProfileV1 addressProfileV1 = new AddressProfileV1();
		String operationName = "get profile ID";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = addressProfileV1.getProfileIdForCreditCheck(apiTestData, updatedRequest, tokenMap);
		if (response != null && "201".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("profileId", getPathVal(jsonNode, "profileId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		System.out.println("Profile ID completed");
		operationName = "get Credit profile ID";
		String creditrequestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmobileCreditProfileID.txt");
		AddressCredirProfileID addressCredirProfileID = new AddressCredirProfileID();
		String latestRequest = prepareRequestParam(creditrequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = addressCredirProfileID.getCreditProfileIdForCreditCheck(apiTestData, latestRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("creditProfileID", getPathVal(jsonNode, "creditProfile.creditProfileId"));

			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Credit profile id Response status code : " + response.getStatusCode());
			Reporter.log("Credit profile id Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		System.out.println("Credit Profile ID completed");
		operationName = "Address validation";
		String addressValidation = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "IntegradeAddressValidateAPI.txt");
		String createAddressValidation = prepareRequestParam(addressValidation, tokenMap);
		logRequest(createAddressValidation, operationName);
		response = createAddressValidation(apiTestData, createAddressValidation, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			JsonPath jsonPath = new JsonPath(response.asString());
			logSuccessResponse(response, operationName);

		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * UserStory# Description: US633800 - TPP R2 - EOS Technical - Integrate Address
	 * With invalid payload to verify for a failure response.
	 * 
	 * @param data
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void eosAddressServiceValidationWithInvalidPayload(ControlTestData data, ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {

		Reporter.log("TestName: tmng address service validation");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log(
				"Step 2: Verify Address service response with invalid pay load| Response should be 404 for invalid address");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmobileProfileID.txt");
		AddressProfileV1 addressProfileV1 = new AddressProfileV1();
		String operationName = "get profile ID";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = addressProfileV1.getProfileIdForCreditCheck(apiTestData, updatedRequest, tokenMap);
		if (response != null && "201".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("profileId", getPathVal(jsonNode, "profileId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		System.out.println("Profile ID completed");
		operationName = "get Credit profile ID";
		String creditrequestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmobileCreditProfileID.txt");
		AddressCredirProfileID addressCredirProfileID = new AddressCredirProfileID();
		String latestRequest = prepareRequestParam(creditrequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = addressCredirProfileID.getCreditProfileIdForCreditCheck(apiTestData, latestRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("creditProfileID", getPathVal(jsonNode, "creditProfile.creditProfileId"));

			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Credit profile id Response status code : " + response.getStatusCode());
			Reporter.log("Credit profile id Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		System.out.println("Credit Profile ID completed");
		operationName = "Address validation";
		String addressValidation = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "IntegradeAddressValidateAPI2InValid.txt");
		String createAddressValidation = prepareRequestParam(addressValidation, tokenMap);
		logRequest(createAddressValidation, operationName);
		response = createAddressValidation(apiTestData, createAddressValidation, tokenMap);
		System.out.println(response.toString());
		if (response != null && "400".equals(Integer.toString(response.getStatusCode()))) {
			JsonPath jsonPath = new JsonPath(response.asString());
			logSuccessResponse(response, operationName);

		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * UserStory# Description: US633800 - TPP R2 - EOS Technical - Integrate Address
	 * With Valid payload to verify if response is success for
	 * RetrieveCustomerCreditAPI.
	 * 
	 * @param data
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void eosAddressServiceValidationWithValidPayloadFromRetrieveCustomerCreditAPI(ControlTestData data,
			ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {

		Reporter.log("TestName: tmng address service validation");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log(
				"Step 2: Verify createHardCreditCheck service response with valid pay load| Response should be 200 OK");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmobileProfileID.txt");
		AddressProfileV1 addressProfileV1 = new AddressProfileV1();
		String operationName = "get profile ID";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = addressProfileV1.getProfileIdForCreditCheck(apiTestData, updatedRequest, tokenMap);
		if (response != null && "201".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("profileId", getPathVal(jsonNode, "profileId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		operationName = "get Credit profile ID";
		String creditrequestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmobileCreditProfileID.txt");
		AddressCredirProfileID addressCredirProfileID = new AddressCredirProfileID();
		String latestRequest = prepareRequestParam(creditrequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = addressCredirProfileID.getCreditProfileIdForCreditCheck(apiTestData, latestRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("creditProfileID", getPathVal(jsonNode, "creditProfile.creditProfileId"));

			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Credit profile id Response status code : " + response.getStatusCode());
			Reporter.log("Credit profile id Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		operationName = "Retrieve Customer";
		String retrieveCustomer = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "IntegradeAddressValidateAPI.txt");
		String createRetrieveCustomer = prepareRequestParam(retrieveCustomer, tokenMap);
		logRequest(createRetrieveCustomer, operationName);
		response = createAddressValidation(apiTestData, createRetrieveCustomer, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			JsonPath jsonPath = new JsonPath(response.asString());
			logSuccessResponse(response, operationName);

		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * UserStory# Description: US633800 - TPP R2 - EOS Technical - Integrate Address
	 * With invalid payload to verify for a failure response for
	 * RetrieveCustomerCreditAPI.
	 * 
	 * @param data
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void eosAddressServiceValidationWithInvalidPayloadFromRetrieveCustomerCreditAPI(ControlTestData data,
			ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {

		Reporter.log("TestName: tmng address service validation");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log(
				"Step 2: Verify createHardCreditCheck service response with invalid pay load| Response should be 404 for invalid address");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmobileProfileID.txt");
		AddressProfileV1 addressProfileV1 = new AddressProfileV1();
		String operationName = "get profile ID";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = addressProfileV1.getProfileIdForCreditCheck(apiTestData, updatedRequest, tokenMap);
		if (response != null && "201".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("profileId", getPathVal(jsonNode, "profileId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		System.out.println("Profile ID completed");
		operationName = "get Credit profile ID";
		String creditrequestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmobileCreditProfileID.txt");
		AddressCredirProfileID addressCredirProfileID = new AddressCredirProfileID();
		String latestRequest = prepareRequestParam(creditrequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = addressCredirProfileID.getCreditProfileIdForCreditCheck(apiTestData, latestRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("creditProfileID", getPathVal(jsonNode, "creditProfile.creditProfileId"));

			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Credit profile id Response status code : " + response.getStatusCode());
			Reporter.log("Credit profile id Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		System.out.println("Credit Profile ID completed");

		operationName = "Retrieve Customer";
		String retrieveCustomer = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "IntegradeAddressValidateAPI2InValid.txt");
		String createRetrieveCustomer = prepareRequestParam(retrieveCustomer, tokenMap);
		logRequest(createRetrieveCustomer, operationName);
		response = createAddressValidation(apiTestData, createRetrieveCustomer, tokenMap);
		System.out.println(response.toString());
		if (response != null && "400".equals(Integer.toString(response.getStatusCode()))) {
			JsonPath jsonPath = new JsonPath(response.asString());
			logSuccessResponse(response, operationName);

		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			failAndLogResponse(response, operationName);
		}

	}
}
