package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.global.ContactUSPage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.PLPPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;


public class LineSelectorPageTest extends ShopCommonLib {

	/**
	 * US493907:AAL - Line Selector: Hide AAL CTA when UPGRADE selected from PDP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION })
	public void testAALLinkNotDisplayedForUpgradeFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test  US493907:AAL - Line Selector: Hide AAL CTA when UPGRADE selected from PDP");
		Reporter.log("Data Condition | PAH User and Customer not eligible for AAL");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones | Product list  page should be displayed");
		Reporter.log("6. Click on a Device in PLP | Product details page should be displayed");
		Reporter.log("7. Verify Upgrade button | Upgrade button should be displayed");
		Reporter.log("8. Verify AAL button | AAL button should be displayed");
		Reporter.log("9. Click upgrade button | LineSelector page should be displayed");
		Reporter.log("10. Verify AAL link | AAL link should not be displayed in LineSelector page");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyAddaLineCTADisplayed();
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.verifyAddaLineLinkisDisabled();
	}
	
	/**
	 * 
	 * US306523: Line Selector first - Modal for past due customer and
	 * delinquent customers
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS,  Group.AAL_REGRESSION})
	public void testsEligibilityMessageForPastDueCustomers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case: US306523, US352790: Line Selector first - Modal for past due customer and delinquent customers");
		Reporter.log("Data Condition | PAH Customer with due balance");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on any Featured device | Line selection page should be displayed. No popup");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPDPPageByFeatureDevices(myTmoData, myTmoData.getDeviceName());
		getDriver().get("https://my.t-mobile.com/purchase/lineselector");
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.verifyModalPastDueCustomer();
		lineSelectorPage.verifyModalPastDueTitleText();
		lineSelectorPage.verifyModalPastDueDescriptionText();
		lineSelectorPage.verifyModalPastDueMakePaymentCTA();
		lineSelectorPage.clickCloseCTAPastDueModal();
		lineSelectorPage.verifyStickBannerHeader();
		lineSelectorPage.verifyStickBannerBody();
		lineSelectorPage.clickStickBannerNextIcon();

		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageUrl();
	}

	/**
	 * US390699 myTMO > MBB lines > Error modal
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testMBBLinesErrorModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case#US390699 -myTMO > MBB lines > Error modal");
		Reporter.log("Data Condition | PAH User only have MBB lines on account");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones | Product list  page should be displayed");
		Reporter.log("6. Click on a Device in PLP | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selectior page should be displayed");
		Reporter.log(
				"8. verify 'No lines to upgrade' error modal | 'No lines to upgrade' error modal should be displayed");
		Reporter.log("9. close the error modal | user should navigate to Shop home page.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickAddToCart();

		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.verifyModalNoLinesToUpgrade();
		lineSelectorPage.verifyModalNoLinesToUpgradeModelCloseCTA();
		lineSelectorPage.clickNoLinesToUpgradeModelCloseCTA();

		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyNewShoppage();
	}

	/**
	 * US341010 : AAL: Entry points for Line Selector (known intent) - error
	 * messages messages
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION})
	public void testAALEligibilityCheckForNonPAHUserAtLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case#US341010 : AAL: Entry points for Line Selector (known intent) - error messages");
		Reporter.log("Data Condition | User should full or single line user and role is not among PAH ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones | Product list  page should be displayed");
		Reporter.log("6. Click on a Device in PLP | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selector page should be displayed");
		Reporter.log(
				"8. Click add a line button |Authorable ineligibility message for user role not among pah should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyAddaLineCTADisplayed();
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.verifyAALIneligibleMessageForUserRoleNotAmongPAH();
	}

	/**
	 * US341010 : AAL: Entry points for Line Selector (known intent) - error
	 * messages messages
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAALEligibilityCheckForSuspendedAtLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case#US341010 : AAL: Entry points for Line Selector (known intent) - error messages");
		Reporter.log("Data Condition | Suspended account");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones | Product list  page should be displayed");
		Reporter.log("6. Click on a Device in PLP | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selectior page should be displayed");

		Reporter.log(
				"8. Click add a line button | Authorable ineligibility message for suspended account should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyAddaLineCTADisplayed();
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyAALIneligibleMessageForSuspendedAccount();
	}

	/**
	 * US341010 : AAL: Entry points for Line Selector (known intent) - error
	 * messages messages
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAALEligibilityCheckForAccountTenuredaysLessThan60AtLineSelectorPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case#US341010 : AAL: Entry points for Line Selector (known intent) - error messages");
		Reporter.log("Data Condition | Account tenure days less than 60");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones | Product list  page should be displayed");
		Reporter.log("6. Click on a Device in PLP | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selectior page should be displayed");
		Reporter.log(
				"8. Click add a line button |Authorable ineligibility message for account tenure days less than 60 should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyAddaLineCTADisplayed();
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.verifyAALIneligibleMessageForAccountTenureDaysLessThan60();
	}

	/**
	 * US341010 : AAL: Entry points for Line Selector (known intent) - error
	 * messages messages
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAALEligibilityCheckForPastDueOrDelinquentAccAtLineSelectorPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case#US341010 : AAL: Entry points for Line Selector (known intent) - error messages");
		Reporter.log("Data Condition | Account should be pastdue or delinquent");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones | Product list  page should be displayed");
		Reporter.log("6. Click on a Device in PLP | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selectior page should be displayed");

		Reporter.log(
				"8. Click add a line button | Authorable ineligibility message for delinquent account should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyAddaLineCTADisplayed();
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.verifyAALIneligibleMessageForDelinquentAcc();
	}

	/**
	 * US374727 AAL - unKnown intent Blocked path - lead Eligible (max soc) plan
	 * customers to Modal(LINE SELECTOR)
	 * 
	 * US388897 - [Continued] AAL - UnKnown intent Blocked path - lead max
	 * voice/non max MBB to Modal (LINE SELECTOR)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAALDigitalFlowStoppedforCustomerwithMaxSocLimitInLineSelectorPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test US374727: AAL - unKnown intent Blocked path - lead Eligible (max soc) plan customers to Modal(LINE SELECTOR)");
		Reporter.log("Data Condition | PAH or FULL customer who is on TMO one plan but SOC limit is maxed out ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones | Product list  page should be displayed");
		Reporter.log("6. Click on a Device in PLP | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selectior page should be displayed");
		Reporter.log("8. click on 'AAL CTA' | Max Soc Plan Customer Modal should be displayed ");
		Reporter.log("9. Verify 'Let's talk' title on the modal| 'Let's talk' title should be displayed");
		Reporter.log(
				"10. Verify 'Sorry - we're not able to add a line to your account online.' text on the modal| 'Sorry - we're not able to add a line to your account online.' text should be displayed");
		Reporter.log(
				"11. Verify 'To add a line, please call Customer Care at 1-800-937-8997, or by dialing 611 from your T-Mobile phone.' text on the modal|  'To add a line, please call Customer Care at 1-800-937-8997, or by dialing 611 from your T-Mobile phone.' text should be displayed");
		Reporter.log("12. click on 'CONTACT US' CTA | Contact Us page should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		lineSelectorPage.clickOnAALCTA();
		lineSelectorPage.verifyLetsTalkModel();
		lineSelectorPage.verifyLetsTalkModelWarningMessage();
		lineSelectorPage.clickOnContactUsCTA();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
	}

	/**
	 * US455108:[TEST ONLY] AAL - Deeplink without cookies (through to cart)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION})
	public void testAALDeeplinkFlowWithOutCookiesAndWithPastDueCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US455108:[TEST ONLY] AAL - Deeplink without cookies (through to cart)");
		Reporter.log("Data Condition | PAH User With Past-Due customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select Device | Device PDP page should be displayed");
		Reporter.log("4. Click on 'Login to My T-Mobile' link | MyTmo login page should be displayed");
		Reporter.log("5. Enter valid user-id and Password | LineSelector page should be displayed");
		Reporter.log("6. Verify popup modal is displayed | Popup modal is displayed");
		Reporter.log(
				"7. Verify Title | 'Your account is past due. To add a line, please make a payment' message should be displayed");
		Reporter.log(
				"8. Verify body | Body should be displayed and should contain This account is not eligible for upgrade");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		launchTmngDMO(myTmoData);
		PlpPage phonesPage = new PlpPage(getDriver());
		phonesPage.clickDeviceWithAvailability(myTmoData.getDeviceName());
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.verifyPhonePdpPageLoaded();
		phoneDetailsPage.selectLogIn();
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPage();
		loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(), myTmoData.getLoginPassword());
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.verifyModalPastDueCustomer();
		lineSelectorPage.verifyModalPastDueTitleText();
		lineSelectorPage.verifyModalPastDueDescriptionText();
	}
	

	
	/**
	 * US493907:AAL - Line Selector: Hide AAL CTA when UPGRADE selected from PDP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PROGRESSION})
	public void testCookiedCustomersAALLinkNotDisplayed(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test  US493907:AAL - Line Selector: Hide AAL CTA when UPGRADE selected from PDP");		
		Reporter.log("Data Condition | PAH User and Customer not eligible for AAL");		
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch MyTmo application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");		
		Reporter.log("4. Launch TMO application | TMO Application Should be Launched");
		Reporter.log("5. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("6. Select Device | Device PDP page should be displayed");		
		Reporter.log("7. Verify Upgrade button | Upgrade button should be displayed");
		Reporter.log("8. Verify AAL button | AAL button should be displayed");		
		Reporter.log("9. Click upgrade button | User should navigate to My-Tmo LineSelector page");
		Reporter.log("10. Verify AAL link | AAL link should not be displayed in LineSelector page");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToShopPage(myTmoData);
		launchTmngDMO(myTmoData);
		PlpPage phonesPage = new PlpPage(getDriver());
		phonesPage.checkPageIsReady();
		phonesPage.verifyPhonesPlpPageLoaded();
		phonesPage.clickDeviceWithAvailability(myTmoData.getDeviceName());
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.verifyPhonePdpPageLoaded();
		phoneDetailsPage.clickOnAddToCartBtn();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.verifyAddaLineLinkisDisabled();
		
	}

	/**
	 * US493907:AAL - Line Selector: Hide AAL CTA when UPGRADE selected from PDP
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION})
	public void testUnCookiedCustomersAALLinkDisplayed(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test  US493907:AAL - Line Selector: Hide AAL CTA when UPGRADE selected from PDP");
		Reporter.log("Data Condition | PAH User and Customer not eligible for AAL");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO application | TMO Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on 'Login to My T-Mobile' link under 'Add to cart' CTA | MYTMO login page should be displayed");
		Reporter.log("5. Login to the application | LineSelector page should be displayed");
		Reporter.log("6. Verify AAL link presence | AAL link should be available on LineSelector page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		launchTmngDMO(myTmoData);
		PlpPage phonesPage = new PlpPage(getDriver());
		phonesPage.checkPageIsReady();
		phonesPage.verifyPhonesPlpPageLoaded();
		phonesPage.clickDeviceWithAvailability(myTmoData.getDeviceName());
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.verifyPhonePdpPageLoaded();
		phoneDetailsPage.selectLogIn();
		LoginPage loginPage = new LoginPage(getDriver()); 
		loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(),myTmoData.getLoginPassword());
		//loginPage.accountVerification();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.waitForLineSelectorPage();
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.verifyAddaLineLink();

	}
	
	/**
	 * US552602 :Galaxy Fold : Jump
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testInEligiblityMessageForJumpUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US552602 :Galaxy Fold : Jump");		
		Reporter.log("Data Condition | Jump User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");		
		Reporter.log("5. Click on See all phone link | PLP page should be displayed");
		Reporter.log("6. Select on Galaxy Fold device | PDP page should be displayed");
		Reporter.log("7. Click on Add to cart button | LineSelector  page should be displayed");
		Reporter.log("8. Select jump line | Selected Line not eligible popup should be displayed");
		Reporter.log("9. Click on 'Try again' CTA | PLP page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.selectJumpLine();
		lineSelectorPage.verifyJumpInEligiblityMessage();
		lineSelectorPage.clickInEligiblityMessageModel();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		
	}
		
}
