package com.tmobile.eservices.qa.payments.acceptance;

import java.time.LocalDate;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.NewAddBankPage;
import com.tmobile.eservices.qa.pages.payments.NewAddCardPage;
import com.tmobile.eservices.qa.pages.payments.NewAutoPayConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayCancelConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayLandingPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.PaymentCollectionPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class NewAutoPayTests extends PaymentCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(NewAutoPayTests.class);

	/**
	 * Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new
	 * checking
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular user. Eligible for AutoPay
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void verifySignupautopaywithnewCard(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("verifySignupAutopayOTPcheckingAcct method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new checking	");

		Reporter.log("Data Conditions - Usertype-individual-PAH-billdudatepast-balance:true-discount:eligible");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Bank details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. Click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. Click on Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on AutoPay ON link | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Cancel AutoPay link | Cancel AutoPay modal should be displayed");
		Reporter.log("12. Click on Yes button | Cancel Autopay confirmation page should be displayed");
		Reporter.log("13. Click Return Home button | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// NewAutopayLandingPage newaplandingPage =
		// navigateToAutoPayPagefromBilling(myTmoData);
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);

		if (newaplandingPage.checkispageAutopayON())
			cancelAutopay();
		Map<String, String> hmap = getDueDate(myTmoData);
		String duedate = hmap.get("duedate");
		String balance = "false";
		String duedatetype = "insideblackoutperiod";
		String firstday = getrequireddates(duedatetype, duedate, balance);
		newaplandingPage.ValidateLandingpagedatesonAutopayOFF(firstday, duedate);
		newaplandingPage.clickAddPaymentmethod();
		PaymentCollectionPage pcp=new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickAddCard();
		NewAddCardPage nac = new NewAddCardPage(getDriver());
		nac.verifyCardPageLoaded();
		 Map<String,String>cardinfo=getCardInfo("master");
		nac.addCardInformation(cardinfo);
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.ClickAgreeandSubmit();
		NewAutoPayConfirmationPage apconfPage = new NewAutoPayConfirmationPage(getDriver());
		apconfPage.verifyPageLoaded();
		apconfPage.checkdates(firstday, duedate);
		cardinfo.put("nickname", hmap.get("nickname"));
		cardinfo.put("accountnumber", hmap.get("accountnumber"));
		apconfPage.NewCreditcardconfirmationpageValidations(cardinfo);
		apconfPage.ClickBackToHomebutton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		// home.clickonAutopayLink();
		home.URLredirecttoAutopay();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.checkFirstautopaytextAutopayON(firstday);
		newaplandingPage.checkBilldueAutopayON(duedate);
		// newaplandingPage.checkmontlyPaymentDateAutopayON(firstday);
		newaplandingPage.clickCancelAutopayLink();
		newaplandingPage.checkcancelautopaytextinModel();
		newaplandingPage.clickyescancelautopayonModel();
		NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
		cancelconfPage.verifyPageLoaded();
		cancelconfPage.clickBacktoHomeButton();

	}

	/**
	 * Enable AutoPay - Debit/Credit
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void verifySignupautopaywithnewBank(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("verifySignupAutopayOTPcheckingAcct method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new checking	");

		Reporter.log("Data Conditions - Usertype-individual-PAH-billdudatepast-balance:true-discount:eligible");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Bank details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. Click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. Click on Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on AutoPay ON link | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Cancel AutoPay link | Cancel AutoPay modal should be displayed");
		Reporter.log("12. Click on Yes button | Cancel Autopay confirmation page should be displayed");
		Reporter.log("13. Click Return Home button | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// NewAutopayLandingPage newaplandingPage =
		// navigateToAutoPayPagefromBilling(myTmoData);
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);
		if (newaplandingPage.checkispageAutopayON())
			cancelAutopay();
		Map<String, String> hmap = getDueDate(myTmoData);
		String duedate1 = hmap.get("duedate");
		String balance = "true";
		String duedatetype = "duedatepast";
		String firstday = getrequireddates(duedatetype, duedate1, balance);
		String duedate = LocalDate.parse(duedate1).plusMonths(1).toString();
		newaplandingPage.checkautopayOFFtext();
		newaplandingPage.ValidateLandingpagedatesonAutopayOFF(firstday, duedate);
		newaplandingPage.clickAddPaymentmethod();
		PaymentCollectionPage pcp=new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickAddBank();
		NewAddBankPage nab = new NewAddBankPage(getDriver());
		nab.verifyBankPageLoaded();
		Map<String,String>getBankinfo=getBankInfo("bank1");
		nab.addNewBankDetails(getBankinfo);
		nab.clickContinueCTA();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.ClickAgreeandSubmit();
		NewAutoPayConfirmationPage apconfPage = new NewAutoPayConfirmationPage(getDriver());
		apconfPage.verifyPageLoaded();
		apconfPage.checkdates(firstday, duedate);
		getBankinfo.put("nickname", hmap.get("nickname"));
		getBankinfo.put("accountnumber", hmap.get("accountnumber"));
		getBankinfo.put("paymenttype", "new");
		apconfPage.NewBankconfirmationpageValidations(getBankinfo);
		apconfPage.ClickBackToHomebutton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		// home.clickonAutopayLink();
		home.URLredirecttoAutopay();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.checkFirstautopaytextAutopayON(firstday);
		newaplandingPage.checkBilldueAutopayON(duedate);
		// newaplandingPage.checkmontlyPaymentDateAutopayON(firstday);
		newaplandingPage.clickCancelAutopayLink();
		newaplandingPage.checkcancelautopaytextinModel();
		newaplandingPage.clickyescancelautopayonModel();
		NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
		cancelconfPage.verifyPageLoaded();
		cancelconfPage.clickBacktoHomeButton();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void verifySignupautopaywithstoredbank(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("verifySignupAutopayOTPcheckingAcct method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new checking	");

		Reporter.log("Data Conditions - Usertype-individual-PAH-billdudatepast-balance:true-discount:eligible");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Bank details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. Click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. Click on Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on AutoPay ON link | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Cancel AutoPay link | Cancel AutoPay modal should be displayed");
		Reporter.log("12. Click on Yes button | Cancel Autopay confirmation page should be displayed");
		Reporter.log("13. Click Return Home button | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// NewAutopayLandingPage newaplandingPage =
		// navigateToAutoPayPagefromBilling(myTmoData);
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);
		Map<String,String>getBankinfo=getBankInfo("bank1");
		setupautopaywithBank(getBankinfo);
		cancelAutopay();
		Map<String, String> hmap = getDueDate(myTmoData);
		String duedate = hmap.get("duedate");
		String balance = "false";
		String duedatetype = "outsideblackoutperiod";
		String firstday = getrequireddates(duedatetype, duedate, balance);
		newaplandingPage.checkautopayOFFtext();
		newaplandingPage.ValidateLandingpagedatesonAutopayOFF(firstday, duedate);
		newaplandingPage.Submitpaymentwhenbankisdefaultmethod();
		NewAutoPayConfirmationPage apconfPage = new NewAutoPayConfirmationPage(getDriver());
		apconfPage.verifyPageLoaded();
		apconfPage.checkdates(firstday, duedate);
		getBankinfo.put("nickname", hmap.get("nickname"));
		getBankinfo.put("accountnumber", hmap.get("accountnumber"));
		getBankinfo.put("paymenttype", "stored");
		apconfPage.NewBankconfirmationpageValidations(getBankinfo);
		apconfPage.ClickBackToHomebutton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		// home.clickonAutopayLink();
		home.URLredirecttoAutopay();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.checkFirstautopaytextAutopayON(firstday);
		newaplandingPage.checkBilldueAutopayON(duedate);
		// newaplandingPage.checkmontlyPaymentDateAutopayON(firstday);
		newaplandingPage.clickCancelAutopayLink();
		newaplandingPage.checkcancelautopaytextinModel();
		newaplandingPage.clickyescancelautopayonModel();
		NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
		cancelconfPage.verifyPageLoaded();
		cancelconfPage.clickBacktoHomeButton();

	}

	/**
	 * Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new
	 * checking
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular user. Eligible for AutoPay
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void verifySignupautopaywithStoredCard(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("verifySignupAutopayOTPcheckingAcct method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new checking	");

		Reporter.log("Data Conditions - Usertype-individual-PAH-billdudatepast-balance:true-discount:eligible");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Bank details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. Click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. Click on Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on AutoPay ON link | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Cancel AutoPay link | Cancel AutoPay modal should be displayed");
		Reporter.log("12. Click on Yes button | Cancel Autopay confirmation page should be displayed");
		Reporter.log("13. Click Return Home button | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// NewAutopayLandingPage newaplandingPage =
		// navigateToAutoPayPagefromBilling(myTmoData);
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);
		Map<String,String>cardinfo=getCardInfo("master");
		setupautopaywithCard(cardinfo);
		cancelAutopay();

		Map<String, String> hmap = getDueDate(myTmoData);
		String duedate = hmap.get("duedate");
		String balance = "false";
		String duedatetype = "outsideblackoutperiod";
		String firstday = getrequireddates(duedatetype, duedate, balance);
		newaplandingPage.checkautopayOFFtext();
		newaplandingPage.ValidateLandingpagedatesonAutopayOFF(firstday, duedate);
		if (!newaplandingPage.Verifydefaultiscard()) {
			newaplandingPage.clickAddPaymentmethod();
			PaymentCollectionPage pcp=new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickStoredCardRadiobutton();
		pcp.clickSelectPaymentMethodCTA();
		}
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.ClickAgreeandSubmit();
		NewAutoPayConfirmationPage apconfPage = new NewAutoPayConfirmationPage(getDriver());
		apconfPage.verifyPageLoaded();

		cardinfo.put("nickname", hmap.get("nickname"));
		cardinfo.put("accountnumber", hmap.get("accountnumber"));

		apconfPage.NewCreditcardconfirmationpageValidations(cardinfo);
		apconfPage.ClickBackToHomebutton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		// home.clickonAutopayLink();
		home.URLredirecttoAutopay();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.checkFirstautopaytextAutopayON(firstday);
		newaplandingPage.checkBilldueAutopayON(duedate);
		// newaplandingPage.checkmontlyPaymentDateAutopayON(firstday);
		newaplandingPage.clickCancelAutopayLink();
		newaplandingPage.checkcancelautopaytextinModel();
		newaplandingPage.clickyescancelautopayonModel();
		NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
		cancelconfPage.verifyPageLoaded();
		cancelconfPage.clickBacktoHomeButton();

	}

	/**
	 * Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new
	 * checking
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular user. Eligible for AutoPay
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void verifyCancelAutopaywhenbalancedueisTrue(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("verifySignupAutopayOTPcheckingAcct method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new checking	");

		Reporter.log("Data Conditions - Usertype-individual-PAH-billdudatepast-balance:true-discount:eligible");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Bank details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. Click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. Click on Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on AutoPay ON link | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Cancel AutoPay link | Cancel AutoPay modal should be displayed");
		Reporter.log("12. Click on Yes button | Cancel Autopay confirmation page should be displayed");
		Reporter.log("13. Click Return Home button | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// NewAutopayLandingPage newaplandingPage =
		// navigateToAutoPayPagefromBilling(myTmoData);
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);
		if(newaplandingPage.checkispageAutopayOFF()) {
		if(!newaplandingPage.checkstroredpaymentmethods()) {
			newaplandingPage.clickAddPaymentmethod();
			PaymentCollectionPage pcp=new PaymentCollectionPage(getDriver());
			pcp.verifyPageLoaded();
			pcp.clickAddCard();
			NewAddCardPage nac=new NewAddCardPage(getDriver());
			nac.verifyCardPageLoaded();
			Map<String,String>cardinfo=getCardInfo("master");
		    nac.addCardInformation(cardinfo);
			newaplandingPage.verifyPageLoaded();
			}
			newaplandingPage.ClickAgreeandSubmit();
			NewAutoPayConfirmationPage apconfPage = new NewAutoPayConfirmationPage(getDriver());
			apconfPage.verifyPageLoaded();
			apconfPage.ClickBackToHomebutton();
			HomePage home = new HomePage(getDriver());
			home.verifyPageLoaded();
			// home.clickonAutopayLink();
			home.URLredirecttoAutopay();
			newaplandingPage.verifyPageLoaded();
		}

		newaplandingPage.clickCancelAutopayLink();
		newaplandingPage.checkcancelautopaytextinModel();
		newaplandingPage.clickyescancelautopayonModel();
		NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
		cancelconfPage.verifyPageLoaded();
		cancelconfPage.VerifyOTPtextDisplayed();
		cancelconfPage.clickOnmakeOTP();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
	}

	/**
	 * Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new
	 * checking
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular user. Eligible for AutoPay
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void verifyupdateautopayfrombanktonewcard(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("verifySignupAutopayOTPcheckingAcct method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new checking	");

		Reporter.log("Data Conditions - Usertype-individual-PAH-billdudatepast-balance:true-discount:eligible");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Bank details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. Click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. Click on Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on AutoPay ON link | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Cancel AutoPay link | Cancel AutoPay modal should be displayed");
		Reporter.log("12. Click on Yes button | Cancel Autopay confirmation page should be displayed");
		Reporter.log("13. Click Return Home button | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// NewAutopayLandingPage newaplandingPage =
		// navigateToAutoPayPagefromBilling(myTmoData);
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);
		Map<String,String>getBankinfo=getBankInfo("bank1");
		setupautopaywithBank(getBankinfo);
		Map<String, String> hmap = getDueDate(myTmoData);
		String duedate = hmap.get("duedate");
		newaplandingPage.clickAddPaymentmethod();
		PaymentCollectionPage pcp=new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickAddCard();
		NewAddCardPage nac = new NewAddCardPage(getDriver());
		nac.verifyCardPageLoaded();
		 Map<String,String>cardinfo=getCardInfo("master");
		nac.addCardInformation(cardinfo);
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.ClickAgreeandSubmit();
		NewAutoPayConfirmationPage apconfPage = new NewAutoPayConfirmationPage(getDriver());
		apconfPage.verifyPageLoaded();
		cardinfo.put("nickname", hmap.get("nickname"));
		cardinfo.put("accountnumber", hmap.get("accountnumber"));
		apconfPage.verifySuccessIcon();
		apconfPage.verifyAutopayUpdated();
		apconfPage.NewCreditcardconfirmationpageValidations(cardinfo);
		apconfPage.ClickBackToHomebutton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		// home.clickonAutopayLink();
		home.URLredirecttoAutopay();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.clickCancelAutopayLink();
		newaplandingPage.checkcancelautopaytextinModel();
		newaplandingPage.clickyescancelautopayonModel();
		NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
		cancelconfPage.verifyPageLoaded();
		cancelconfPage.clickBacktoHomeButton();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7", "apbank" })
	public void verifyupdateautopayfromcardtonewBank(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("verifySignupAutopayOTPcheckingAcct method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new checking	");

		Reporter.log("Data Conditions - Usertype-individual-PAH-billdudatepast-balance:true-discount:eligible");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Bank details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. Click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. Click on Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on AutoPay ON link | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Cancel AutoPay link | Cancel AutoPay modal should be displayed");
		Reporter.log("12. Click on Yes button | Cancel Autopay confirmation page should be displayed");
		Reporter.log("13. Click Return Home button | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// NewAutopayLandingPage newaplandingPage =
		// navigateToAutoPayPagefromBilling(myTmoData);
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);
		Map<String,String>cardinfo=getCardInfo("master");
		setupautopaywithCard(cardinfo);
		Map<String, String> hmap = getDueDate(myTmoData);
		String duedate = hmap.get("duedate");

		newaplandingPage.clickAddPaymentmethod();
		PaymentCollectionPage pcp=new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickAddBank();
		NewAddBankPage nab = new NewAddBankPage(getDriver());
		nab.verifyBankPageLoaded();
		Map<String,String>getBankinfo=getBankInfo("bank1");
		nab.addNewBankDetails(getBankinfo);
		nab.clickContinueCTA();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.ClickAgreeandSubmit();
		NewAutoPayConfirmationPage apconfPage = new NewAutoPayConfirmationPage(getDriver());
		apconfPage.verifyPageLoaded();

		getBankinfo.put("nickname", hmap.get("nickname"));
		getBankinfo.put("accountnumber", hmap.get("accountnumber"));
		getBankinfo.put("paymenttype", "new");

		apconfPage.verifySuccessIcon();
		apconfPage.verifyAutopayUpdated();
		apconfPage.NewBankconfirmationpageValidations(getBankinfo);
		apconfPage.ClickBackToHomebutton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		// home.clickonAutopayLink();
		home.URLredirecttoAutopay();
		newaplandingPage.verifyPageLoaded();

		// newaplandingPage.checkBilldueAutopayON(duedate);

		newaplandingPage.clickCancelAutopayLink();
		newaplandingPage.checkcancelautopaytextinModel();
		newaplandingPage.clickyescancelautopayonModel();
		NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
		cancelconfPage.verifyPageLoaded();
		cancelconfPage.clickBacktoHomeButton();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void verifyupdateautopayfromBanktonewBank(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("verifySignupAutopayOTPcheckingAcct method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new checking	");

		Reporter.log("Data Conditions - Usertype-individual-PAH-billdudatepast-balance:true-discount:eligible");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Bank details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. Click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. Click on Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on AutoPay ON link | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Cancel AutoPay link | Cancel AutoPay modal should be displayed");
		Reporter.log("12. Click on Yes button | Cancel Autopay confirmation page should be displayed");
		Reporter.log("13. Click Return Home button | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// NewAutopayLandingPage newaplandingPage =
		// navigateToAutoPayPagefromBilling(myTmoData);
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);
		Map<String,String>getBankinfo1=getBankInfo("bank2");
		setupautopaywithBank(getBankinfo1);
		Map<String, String> hmap = getDueDate(myTmoData);
		String duedate = hmap.get("duedate");

		newaplandingPage.clickAddPaymentmethod();
		PaymentCollectionPage pcp=new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickAddBank();
		NewAddBankPage nab = new NewAddBankPage(getDriver());
		nab.verifyBankPageLoaded();
		Map<String,String>getBankinfo=getBankInfo("bank1");
		nab.addNewBankDetails(getBankinfo);
		nab.clickContinueCTA();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.ClickAgreeandSubmit();
		NewAutoPayConfirmationPage apconfPage = new NewAutoPayConfirmationPage(getDriver());
		apconfPage.verifyPageLoaded();

		getBankinfo.put("nickname", hmap.get("nickname"));
		getBankinfo.put("accountnumber", hmap.get("accountnumber"));
		getBankinfo.put("paymenttype", "new");

		apconfPage.verifySuccessIcon();
		apconfPage.verifyAutopayUpdated();
		apconfPage.NewBankconfirmationpageValidations(getBankinfo);
		apconfPage.ClickBackToHomebutton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		// home.clickonAutopayLink();
		home.URLredirecttoAutopay();
		newaplandingPage.verifyPageLoaded();

		// newaplandingPage.checkBilldueAutopayON(duedate);

		newaplandingPage.clickCancelAutopayLink();
		newaplandingPage.checkcancelautopaytextinModel();
		newaplandingPage.clickyescancelautopayonModel();
		NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
		cancelconfPage.verifyPageLoaded();
		cancelconfPage.clickBacktoHomeButton();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "Nodata" })
	public void verifyupdateautopayfromcardtonewcard(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("verifySignupAutopayOTPcheckingAcct method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new checking	");

		Reporter.log("Data Conditions - Usertype-individual-PAH-billdudatepast-balance:true-discount:eligible");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Bank details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. Click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. Click on Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on AutoPay ON link | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Cancel AutoPay link | Cancel AutoPay modal should be displayed");
		Reporter.log("12. Click on Yes button | Cancel Autopay confirmation page should be displayed");
		Reporter.log("13. Click Return Home button | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// NewAutopayLandingPage newaplandingPage =
		// navigateToAutoPayPagefromBilling(myTmoData);
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);
		Map<String,String>cardinfo1=getCardInfo("visa");
		setupautopaywithCard(cardinfo1);
		Map<String, String> hmap = getDueDate(myTmoData);
		String duedate = hmap.get("duedate");
		newaplandingPage.clickAddPaymentmethod();
		PaymentCollectionPage pcp=new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickAddCard();
		NewAddCardPage nac = new NewAddCardPage(getDriver());
		nac.verifyCardPageLoaded();
		 Map<String,String>cardinfo=getCardInfo("master");
		nac.addCardInformation(cardinfo);
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.ClickAgreeandSubmit();
		NewAutoPayConfirmationPage apconfPage = new NewAutoPayConfirmationPage(getDriver());
		apconfPage.verifyPageLoaded();
		cardinfo.put("nickname", hmap.get("nickname"));
		cardinfo.put("accountnumber", hmap.get("accountnumber"));
		apconfPage.verifySuccessIcon();
		apconfPage.verifyAutopayUpdated();
		apconfPage.NewCreditcardconfirmationpageValidations(cardinfo);
		apconfPage.ClickBackToHomebutton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		// home.clickonAutopayLink();
		home.URLredirecttoAutopay();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.clickCancelAutopayLink();
		newaplandingPage.checkcancelautopaytextinModel();
		newaplandingPage.clickyescancelautopayonModel();
		NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
		cancelconfPage.verifyPageLoaded();
		cancelconfPage.clickBacktoHomeButton();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void verifyUpdateautopaywithstoredbank(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("verifySignupAutopayOTPcheckingAcct method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new checking	");

		Reporter.log("Data Conditions - Usertype-individual-PAH-billdudatepast-balance:true-discount:eligible");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Bank details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. Click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. Click on Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on AutoPay ON link | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Cancel AutoPay link | Cancel AutoPay modal should be displayed");
		Reporter.log("12. Click on Yes button | Cancel Autopay confirmation page should be displayed");
		Reporter.log("13. Click Return Home button | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// NewAutopayLandingPage newaplandingPage =
		// navigateToAutoPayPagefromBilling(myTmoData);
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);
		Map<String,String>getBankinfo=getBankInfo("bank1");
		setupautopaywithBank(getBankinfo);
		Map<String,String>cardinfo1=getCardInfo("master");
		setupautopaywithCard(cardinfo1);

		Map<String, String> hmap = getDueDate(myTmoData);
		String duedate = hmap.get("duedate");

		if (!newaplandingPage.Verifydefaultisbank()) {
			newaplandingPage.clickAddPaymentmethod();
			PaymentCollectionPage pcp=new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickStoredBankRadiobutton();
		pcp.clickSelectPaymentMethodCTA();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.ClickAgreeandSubmit();
		}else {Assert.fail("default method should be card");}
		NewAutoPayConfirmationPage apconfPage=new NewAutoPayConfirmationPage(getDriver());

		getBankinfo.put("nickname", hmap.get("nickname"));
		getBankinfo.put("accountnumber", hmap.get("accountnumber"));
		getBankinfo.put("paymenttype", "stored");
		apconfPage.verifySuccessIcon();
		apconfPage.verifyAutopayUpdated();
		apconfPage.NewBankconfirmationpageValidations(getBankinfo);
		apconfPage.ClickBackToHomebutton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		// home.clickonAutopayLink();
		home.URLredirecttoAutopay();
		newaplandingPage.verifyPageLoaded();

		newaplandingPage.clickCancelAutopayLink();
		newaplandingPage.checkcancelautopaytextinModel();
		newaplandingPage.clickyescancelautopayonModel();
		NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
		cancelconfPage.verifyPageLoaded();
		cancelconfPage.clickBacktoHomeButton();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void verifyUpdateautopaywithstoredCard(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("verifySignupAutopayOTPcheckingAcct method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Ensure user can sign up for AutoPay via One-Time Payment Pitch page using new checking	");

		Reporter.log("Data Conditions - Usertype-individual-PAH-billdudatepast-balance:true-discount:eligible");
		Reporter.log("Data Conditions - Regular user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill Bank details and click next | Review payment page should be displayed");
		Reporter.log("6. Click on  Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("7. Click on Submit Button| Autopay confirmation Page should be displayed");
		Reporter.log("8. Click on Home page Icon  | Home page should be displayed");
		Reporter.log("9. Verify Autopay ON on Home page | Auto Pay  should turn into ON");
		Reporter.log("10. Click on AutoPay ON link | Manage Autopay Page should be loaded");
		Reporter.log("11. Click Cancel AutoPay link | Cancel AutoPay modal should be displayed");
		Reporter.log("12. Click on Yes button | Cancel Autopay confirmation page should be displayed");
		Reporter.log("13. Click Return Home button | Home page should be displayed");
		Reporter.log("14. Verify Autopay OFF on Home page | Auto Pay  should turn into OFF");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// NewAutopayLandingPage newaplandingPage =
		// navigateToAutoPayPagefromBilling(myTmoData);
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);
		Map<String,String>cardinfo=getCardInfo("master");
		setupautopaywithCard(cardinfo);
		Map<String,String>getBankinfo=getBankInfo("bank1");
		setupautopaywithBank(getBankinfo);
		
		Map<String,String> hmap=getDueDate(myTmoData);
		String duedate= hmap.get("duedate");
		String balance="false";
		
		if(!newaplandingPage.Verifydefaultiscard()) {
		newaplandingPage.clickAddPaymentmethod();
		PaymentCollectionPage pcp=new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickStoredCardRadiobutton();
		pcp.clickSelectPaymentMethodCTA();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.ClickAgreeandSubmit();
		}else {Assert.fail("default paymnet should be bank");}
		
		NewAutoPayConfirmationPage apconfPage=new NewAutoPayConfirmationPage(getDriver());
		apconfPage.verifyPageLoaded();

		cardinfo.put("nickname", hmap.get("nickname"));
		cardinfo.put("accountnumber", hmap.get("accountnumber"));
		apconfPage.verifySuccessIcon();
		apconfPage.verifyAutopayUpdated();
		apconfPage.NewCreditcardconfirmationpageValidations(cardinfo);
		apconfPage.ClickBackToHomebutton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		// home.clickonAutopayLink();
		home.URLredirecttoAutopay();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.clickCancelAutopayLink();
		newaplandingPage.checkcancelautopaytextinModel();
		newaplandingPage.clickyescancelautopayonModel();
		NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
		cancelconfPage.verifyPageLoaded();
		cancelconfPage.clickBacktoHomeButton();

	}

}
