package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.accounts.FamilyControlsPage;
import com.tmobile.eservices.qa.pages.global.FamilyAllowanceManagePage;

public class FamilyAllowanceManagePageTest extends GlobalCommonLib{
	
	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.RETIRED})
	public void verifyFamilyAllowancesManageLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with family allowance manage link");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on family controls link | Family controls page should be displayed");
		Reporter.log("6. Click manage link | Family allowance manage page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,"Family Controls");
		familyControlsPage.clickFamilyAllowances();
		familyControlsPage.clickManageLink();
		familyControlsPage.switchToWindow();
		
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();
		
		FamilyAllowanceManagePage familyAllowanceManagePage=new FamilyAllowanceManagePage(getDriver());
		familyAllowanceManagePage.verifyFamilyAllowanceManagePage();
	}
	

}
