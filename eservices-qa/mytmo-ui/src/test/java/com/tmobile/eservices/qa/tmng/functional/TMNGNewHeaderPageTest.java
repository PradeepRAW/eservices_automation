package com.tmobile.eservices.qa.tmng.functional;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;
import com.tmobile.eservices.qa.pages.tmng.functional.TMNGNewHeaderPage;

public class TMNGNewHeaderPageTest extends TmngCommonLib {
	//private static final Logger logger = LoggerFactory.getLogger(TMNGNewHeaderPageTest.class);
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Plans Tab Redirects To Plans Page
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyPlansLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Plans Tab is displayed | Plans Tab is displayed");
		Reporter.log("4.Click on Plans Tab | Clicked on Plans Tab successfully");
		Reporter.log("5.Verify Plans page | Plans page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
	    unavPage.clickMenuExpandButton();
		unavPage.verifyHeaderPlansLabel();
		unavPage.clickHeaderPlansLink();
		unavPage.verifyHeaderPlansPage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Phones Tab Redirects To Phones Page
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyPhonesLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Phones Tab is displayed | Phones Tab is displayed");
		Reporter.log("4.Click on Phones Tab | Clicked on Phones Tab successfully");
		Reporter.log("5.Verify Phones page | Phones page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyPhonesLabel();
		unavPage.clickPhonesLink();
		unavPage.verifyPhonesPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Deals Tab Redirects To Deals Page
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyDealsLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Deals Tab is displayed | Deals Tab is displayed");
		Reporter.log("4.Click on Deals Tab | Clicked on Deals Tab successfully");
		Reporter.log("5.Verify Deals page | Deals page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyDealsLabel();
		unavPage.clickDealsLink();
		unavPage.verifyDealsPage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Coverage Tab Redirects To Coverage Page
	 * @param data
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyCoverageLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Coverage Tab is displayed | Coverage Tab is displayed");
		Reporter.log("4.Click on Coverage Tab | Clicked on Coverage Tab successfully");
		Reporter.log("5.Verify Coverage page | Coverage page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyCoverageLabel();
		unavPage.clickCoverageLink();
		unavPage.verifyCoveragePage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Benefits & More Tab Redirects To Benefits & More Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyBenefitsAndMoreLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify BenefitsAndMore Tab is displayed | BenefitsAndMore Tab is displayed");
		Reporter.log("4.Click on BenefitsAndMore Tab | Clicked on BenefitsAndMore Tab successfully");
		Reporter.log("5.Verify BenefitsAndMore page | BenefitsAndMore page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyBenefitsAndMoreLabel();
		unavPage.clickBenefitsAndMoreLink();
		unavPage.verifyBenefitsAndMorePage();
	}

	// Verify Plans Menu Options
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Magenta Redirects To Magenta Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyMagentaLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Plans Tab | Move to the Plans Tab");
		Reporter.log("4.Verify Magenta Label is displayed | Magenta Label is displayed");
		Reporter.log("5.Click on Magenta | Clicked on Magenta successfully");
		Reporter.log("6.Verify Magenta page | Magenta page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickPlansPlusExpandButton();
		unavPage.verifyMagentaLabel();
		unavPage.clickMagentalink();
		unavPage.verifyMagentaPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Magenta Plus Redirects To Magenta Plus Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyMagentaPlusLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Plans Tab | Move to the Plans Tab");
		Reporter.log("4.Verify Magenta Plus Label is displayed | Magenta Plus Label is displayed");
		Reporter.log("5.Click on Magenta Plus | Clicked on Magenta Plus successfully");
		Reporter.log("6.Verify Magenta Plus page | Magenta Plus page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickPlansPlusExpandButton();
		unavPage.verifyMagentaPlusLabel();
		unavPage.clickMagentaPluslink();
		unavPage.verifyMagentaPlusPage();
	}	

	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Essentials Redirects To Essentials Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyEssentialsLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Plans Tab | Move to the Plans Tab");
		Reporter.log("4.Verify Essentials Label is displayed | Essentials Label is displayed");
		Reporter.log("5.Click on Essentials | Clicked on Essentials successfully");
		Reporter.log("6.Verify Essentials page | Essentials page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickPlansPlusExpandButton();
		unavPage.verifyEssentialsLabel();
		unavPage.clickEssentialslink();
		unavPage.verifyEssentialsPage();
	}	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Unlimited55 Redirects To Unlimited55 Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyUnlimited55Link(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Plans Tab | Move to the Plans Tab");
		Reporter.log("4.Verify Unlimited55 Label is displayed | Unlimited55 Label is displayed");
		Reporter.log("5.Click on Unlimited55 | Clicked on Unlimited55 successfully");
		Reporter.log("6.Verify Unlimited55 page | Unlimited55 page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickPlansPlusExpandButton();
		unavPage.verifyUnlimited55Label();
		unavPage.clickUnlimited55link();
		unavPage.verifyUnlimited55Page();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Military Redirects To Military Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyMilitaryLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Plans Tab | Move to the Plans Tab");
		Reporter.log("4.Verify Military Label is displayed | Military Label is displayed");
		Reporter.log("5.Click on Military | Clicked on Military successfully");
		Reporter.log("6.Verify Military page | Military page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickPlansPlusExpandButton();
		unavPage.verifyHeaderMilitaryLabel();
		unavPage.clickHeaderMilitarylink();
		unavPage.verifyHeaderMilitaryPage();
	}

	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Cell Phones Redirects To Cell Phones Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyCellPhonesLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Phones Tab | Move to the Phones Tab");
		Reporter.log("4.Verify Cell Phones Label is displayed | Cell Phones Label is displayed");
		Reporter.log("5.Click on Cell Phones | Clicked on Cell Phones successfully");
		Reporter.log("6.Verify Cell Phones page | Cell Phones page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickPhonesPlusExpandButton();
		unavPage.verifyCellPhonesLabel();
		unavPage.clickCellPhoneslink();
		unavPage.verifyCellPhonesPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Tablets Redirects To Tablets Page
	 * @param data
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyTabletsLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Phones Tab | Move to the Phones Tab");
		Reporter.log("4.Verify Tablets Label is displayed | Tablets Label is displayed");
		Reporter.log("5.Click on Tablets | Clicked on Tablets successfully");
		Reporter.log("6.Verify Tablets page | Tablets page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickPhonesPlusExpandButton();
		unavPage.verifyHeaderTabletsLabel();
		unavPage.clickHeaderTabletslink();
		unavPage.verifyHeaderTabletsPage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify SmartWatches Redirects To SmartWatches Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifySmartWatchesLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Phones Tab | Move to the Phones Tab");
		Reporter.log("4.Verify SmartWatches Label is displayed | SmartWatches Label is displayed");
		Reporter.log("5.Click on SmartWatches | Clicked on SmartWatches successfully");
		Reporter.log("6.Verify SmartWatches page | SmartWatches page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickPhonesPlusExpandButton();
		unavPage.verifyHeaderSmartWatchesLabel();
		unavPage.clickHeaderSmartWatcheslink();
		unavPage.verifyHeaderSmartWatchesPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Accessories Redirects To Accessories Page
	 * @param data
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyAccessoriesLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Phones Tab | Move to the Phones Tab");
		Reporter.log("4.Verify Accessories Label is displayed | Accessories Label is displayed");
		Reporter.log("5.Click on Accessories | Clicked on Accessories successfully");
		Reporter.log("6.Verify Accessories page | Accessories page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickPhonesPlusExpandButton();
		unavPage.verifyHeaderAccessoriesLabel();
		unavPage.clickHeaderAccessorieslink();
		unavPage.verifyHeaderAccessoriesPage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Bring Your Own Device Redirects To Bring Your Own Device Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyBringYourDeviceLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Phones Tab | Move to the Phones Tab");
		Reporter.log("4.Verify Bring Your Own Device Label is displayed | Bring Your Own Device Label is displayed");
		Reporter.log("5.Click on Bring Your Own Device | Clicked on Bring Your Own Device successfully");
		Reporter.log("6.Verify Bring Your Own Device page | Bring Your Own Device page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickPhonesPlusExpandButton();
		unavPage.verifyBringYourDeviceLabel();
		unavPage.clickBringYourDevicelink();
		unavPage.verifyBringYourDevicePage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Apple Redirects To Apple Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID })
	public void verifyAppleLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Deals Tab | Move to the Deals Tab");
		Reporter.log("4.Verify Apple Label is displayed | Apple Label is displayed");
		Reporter.log("5.Click on Apple Device | Clicked on Apple successfully");
		Reporter.log("6.Verify Apple page | Apple page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickDealsPlusExpandButton();
		unavPage.verifyAppleLabel();
		unavPage.clickApplelink();
		unavPage.verifyApplePage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Samsung Redirects To Samsung Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifySamsungLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Deals Tab | Move to the Deals Tab");
		Reporter.log("4.Verify Samsung Label is displayed | Samsung Label is displayed");
		Reporter.log("5.Click on Samsung Device | Clicked on Samsung successfully");
		Reporter.log("6.Verify Samsung page | Samsung page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickDealsPlusExpandButton();
		unavPage.verifySamsungLabel();
		unavPage.clickSamsunglink();
		unavPage.verifySamsungPage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Google Redirects To Google Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyGoogleLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Deals Tab | Move to the Deals Tab");
		Reporter.log("4.Verify Google Label is displayed | Google Label is displayed");
		Reporter.log("5.Click on Google Device | Clicked on Google successfully");
		Reporter.log("6.Verify Google page | Google page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickDealsPlusExpandButton();
		unavPage.verifyGoogleLabel();
		unavPage.clickGooglelink();
		unavPage.verifyGooglePage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify LG Redirects To LG Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyLGLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Deals Tab | Move to the Deals Tab");
		Reporter.log("4.Verify LG Label is displayed | LG Label is displayed");
		Reporter.log("5.Click on LG Device | Clicked on LG successfully");
		Reporter.log("6.Verify LG page | LG page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickDealsPlusExpandButton();
		unavPage.verifyLGLabel();
		unavPage.clickLGlink();
		unavPage.verifyLGPage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Benefits Redirects To Benefits Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyBenefitsLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Benefits and More Tab | Move to the Benefits and More Tab");
		Reporter.log("4.Verify Benefits Label is displayed | Benefits Label is displayed");
		Reporter.log("5.Click on Benefits Device | Clicked on Benefits successfully");
		Reporter.log("6.Verify Benefits page | Benefits page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickBenefitsPlusExpandButton();
		unavPage.verifyHeaderBenefitsLabel();
		unavPage.clickHeaderBenefitslink();
		unavPage.verifyHeaderBenefitsPage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Why T-Mobile Redirects To Why T-Mobile Vision Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verifyWhyTMobileLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Benefits and More Tab | Move to the Benefits and More Tab");
		Reporter.log("4.Verify WhyT-Mobile Label is displayed | WhyT-Mobile Label is displayed");
		Reporter.log("5.Click on WhyT-Mobile Device | Clicked on WhyT-Mobile successfully");
		Reporter.log("6.Verify WhyT-Mobile page | WhyT-Mobile page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickBenefitsPlusExpandButton();
		unavPage.verifyWhyTMobileLabel();
		unavPage.clickWhyTMobilelink();
		unavPage.verifyWhyTMobilePage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify 5G Vision Redirects To 5G Vision Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.ANDROID, Group.IOS })
	public void verify5GVisionLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Move to Benefits and More Tab | Move to the Benefits and More Tab");
		Reporter.log("4.Verify 5GVision Label is displayed | 5GVision Label is displayed");
		Reporter.log("5.Click on 5GVision Device | Clicked on 5GVision successfully");
		Reporter.log("6.Verify 5GVision page | 5GVision page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.clickBenefitsPlusExpandButton();
		//unavPage.verify5GVisionLabel();
		//unavPage.click5GVisionlink();
		//unavPage.verify5GVisionPage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify FindAStore Redirects To Store Locator Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.ANDROID, Group.IOS})
	public void verifyFindAStoreLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Find A Store Label is displayed |  Find A Store Label is displayed");
		Reporter.log("4.Click on  Find A Store Device | Clicked on  Find A Store successfully");
		Reporter.log("5.Verify  Find A Store page |  Store page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		//unavPage.verifyFindAStoreLabel();
		unavPage.clickFindAStorelink();
		unavPage.verifyFindAStorePage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Let's Talk and Cart Redirects To Cart Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.ANDROID, Group.IOS })
	public void verifyCartLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Let's Talk Option is displayed |  Let's Talk Option Label is displayed");
		Reporter.log("4.Verify Find A Store Label is displayed |  Find A Store Label is displayed");
		Reporter.log("5.Click on Cart Device | Clicked on Cart successfully");
		Reporter.log("6.Verify Cart page | Cart page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyLetsTalkLabel();
		unavPage.verifyCartLabel();
		unavPage.clickCartlink();
		unavPage.verifyCartPage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * US617889 - UNAV | My Account with text| Mobile View
	 * Verify My Account and Login Redirects To MyTMO Login Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void verifyLoginLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US617889 - UNAV | My Account with text| Mobile View");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify My Account Icon is displayed |  My Account Icon is displayed");
		Reporter.log("4.Verify My Account Drop Down Option is displayed |  My Account Drop Down Option is displayed");
		Reporter.log("5.Click on My Account | Clicked on My Account successfully");
		Reporter.log("6.Verify Login Option is displayed |  Login is displayed");
		Reporter.log("7.Click on Login | Clicked on Login successfully");
		Reporter.log("8.Verify Login page | Login page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifyMyAccountLabel();
		unavPage.clickMyAccountlink();
		unavPage.verifyLoginLabel();
		unavPage.clickLoginlink();
		unavPage.verifyLoginPage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify My Account and Bill Pay Redirects To Bill Pay Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void verifyBillPayLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify My Account Drop Down Option is displayed |  My Account Drop Down Option is displayed");
		Reporter.log("4.Click on My Account | Clicked on My Account successfully");
		Reporter.log("5.Verify Bill Pay Option is displayed |  Bill Pay is displayed");
		Reporter.log("6.Click on Bill Pay | Clicked on Bill Pay successfully");
		Reporter.log("7.Verify Bill Pay page | Bill Pay page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifyMyAccountLabel();
		unavPage.clickMyAccountlink();
		unavPage.verifyBillPayLabel();
		unavPage.clickBillPaylink();
		//unavPage.verifyBillPayPage();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify My Account and Check Order Redirects To Check Order Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void verifyCheckOrderLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify My Account Drop Down Option is displayed |  My Account Drop Down Option is displayed");
		Reporter.log("4.Click on My Account | Clicked on My Account successfully");
		Reporter.log("5.Verify Upgrade Option is displayed |  Upgrade is displayed");
		Reporter.log("6.Verify Add A Device Option is displayed |  Add A Device is displayed");
		Reporter.log("7.Verify CheckOrder Option is displayed |  CheckOrder is displayed");
		Reporter.log("8.Click on CheckOrder | Clicked on CheckOrder successfully");
		Reporter.log("9.Verify CheckOrder page | CheckOrder page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifyMyAccountLabel();
		unavPage.clickMyAccountlink();
		unavPage.verifyUpgradeLabel();
		unavPage.verifyAddDeviceLabel();
		unavPage.verifyCheckOrderLabel();
		unavPage.clickCheckOrderlink();
		//unavPage.verifyCheckOrderPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify My Account and Support Redirects To Support Page
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void verifySupportLink(TMNGData tMNGData){
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify My Account Drop Down Option is displayed |  My Account Drop Down Option is displayed");
		Reporter.log("4.Click on My Account | Clicked on My Account successfully");
		Reporter.log("5.Verify Support Option is displayed |  Support is displayed");
		Reporter.log("6.Click on Support | Clicked on Support successfully");
		Reporter.log("7.Verify Support page | Support page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifyMyAccountLabel();
		unavPage.clickMyAccountlink();
		unavPage.verifySupportLabel();
		unavPage.clickSupportlink();
		unavPage.verifySupportPage();
	}
	
	/**
	 * CDCDWR2-171: UNAV: Support Live Person integration
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyLivePersonIntegrationOnTMOHome(TMNGData tMNGData){
		Reporter.log("CDCDWR2-171: UNAV: Support Live Person integration");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Let's Talk Option is displayed |  Let's Talk Option Label is displayed");
		Reporter.log("4.Click Message US inside Let's talk |  LP chat box should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.moveToletsTalkMenu();
		unavPage.clickMessageUsLink();
		unavPage.verifyLPChatBox();
	}
	
	/**
	 * CDCDWR2-408: UNAV | Tmo & MyTmo | Magenta Page Indicator - pt 2 S22
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void verifyUNAVMagentaPageIndicator(TMNGData tMNGData){
		Reporter.log("CDCDWR2-408: UNAV | Tmo & MyTmo | Magenta Page Indicator - pt 2 S22");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Click any menu item on UNAV |  Selected menu should be hilighted");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToPhonesPlpPage(tMNGData);
		unavPage.verifySelectedMenuisHighlighted("Phones & devices");
		navigateToPlansPage(tMNGData);
		unavPage.verifySelectedMenuisHighlighted("Plans");
	}
}