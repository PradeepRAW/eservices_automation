package com.tmobile.eservices.qa.accounts.api;

import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.global.LoginPage;

import io.restassured.response.Response;

public class Registermsisdn extends AccountsCommonLib {

	static Response response = null;

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab1(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab2(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab3(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab4(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab5(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab6(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab7(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab8(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab9(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab10(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab11(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab12(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab13(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab14(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab15(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab16(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab17(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab18(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab19(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab20(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab21(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab22(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab23(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab24(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab25(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab26(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab27(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab28(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab29(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab30(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab31(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab32(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab33(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab34(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab35(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab36(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab37(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab38(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab39(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab40(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab41(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab42(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab43(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab44(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab45(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab46(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab47(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab48(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab49(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab50(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab51(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab52(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab53(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab54(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab55(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab56(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab57(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab58(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab59(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab60(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab61(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab62(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab63(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab64(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab65(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab66(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab67(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab68(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab69(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab70(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab71(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab72(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab73(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab74(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab75(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab76(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab77(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab78(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab79(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab80(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab81(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab82(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab83(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab84(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab85(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab86(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab87(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab88(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab89(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab90(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab91(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab92(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab93(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab94(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab95(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab96(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab97(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab98(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab99(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab100(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab101(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab102(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab103(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab104(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab105(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab106(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab107(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab108(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab109(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab110(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab111(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab112(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab113(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab114(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab115(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab116(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab117(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab118(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab119(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab120(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab121(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab122(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab123(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab124(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab125(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab126(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab127(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab128(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab129(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab130(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab131(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab132(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab133(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab134(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab135(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab136(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab137(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab138(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab139(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab140(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab141(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab142(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab143(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab144(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab145(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab146(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab147(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab148(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab149(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab150(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab151(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab152(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab153(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab154(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab155(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab156(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab157(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab158(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab159(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab160(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab161(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab162(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab163(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab164(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab165(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab166(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab167(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab168(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab169(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab170(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab171(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab172(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab173(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab174(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab175(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab176(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab177(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab178(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab179(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab180(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab181(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab182(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab183(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab184(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab185(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab186(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab187(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab188(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab189(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab190(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab191(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab192(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab193(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab194(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab195(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab196(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab197(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab198(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab199(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab200(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab201(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab202(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab203(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab204(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab205(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab206(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab207(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab208(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab209(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab210(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab211(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab212(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab213(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab214(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab215(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab216(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab217(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab218(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab219(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab220(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab221(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab222(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab223(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab224(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab225(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab226(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab227(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab228(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab229(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab230(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab231(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab232(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab233(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab234(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab235(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab236(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab237(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab238(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab239(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab240(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab241(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab242(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab243(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab244(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab245(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab246(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab247(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab248(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab249(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab250(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab251(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab252(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab253(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab254(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab255(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab256(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab257(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab258(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab259(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab260(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab261(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab262(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab263(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab264(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab265(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab266(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab267(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab268(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab269(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab270(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab271(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab272(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab273(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab274(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab275(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab276(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab277(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab278(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab279(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab280(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab281(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab282(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab283(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab284(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab285(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab286(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab287(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab288(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab289(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab290(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab291(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab292(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab293(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab294(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab295(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab296(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab297(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab298(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab299(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab300(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab301(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab302(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab303(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab304(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab305(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab306(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab307(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab308(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab309(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab310(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab311(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab312(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab313(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab314(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab315(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab316(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab317(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab318(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab319(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab320(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab321(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab322(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab323(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab324(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab325(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab326(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab327(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab328(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab329(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab330(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab331(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab332(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab333(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab334(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab335(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab336(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab337(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab338(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab339(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab340(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab341(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab342(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab343(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab344(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab345(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab346(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab347(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab348(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab349(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab350(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab351(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab352(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab353(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab354(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab355(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab356(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab357(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab358(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab359(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab360(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab361(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab362(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab363(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab364(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab365(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab366(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab367(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab368(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab369(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab370(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab371(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab372(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab373(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab374(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab375(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab376(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab377(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab378(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab379(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab380(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab381(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab382(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab383(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab384(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab385(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab386(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab387(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab388(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab389(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab390(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab391(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab392(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab393(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab394(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab395(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab396(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab397(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab398(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab399(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab400(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab401(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab402(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab403(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab404(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab405(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab406(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab407(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab408(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab409(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab410(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab411(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab412(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab413(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab414(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab415(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab416(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab417(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab418(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab419(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab420(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab421(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab422(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab423(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab424(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab425(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab426(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab427(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab428(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab429(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab430(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab431(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab432(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab433(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab434(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab435(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab436(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab437(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab438(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab439(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab440(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab441(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab442(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab443(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab444(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab445(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab446(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab447(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab448(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab449(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab450(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab451(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab452(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab453(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab454(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab455(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab456(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab457(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab458(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab459(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab460(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab461(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab462(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab463(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab464(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab465(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab466(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab467(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab468(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab469(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab470(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab471(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab472(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab473(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab474(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab475(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab476(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab477(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab478(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab479(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab480(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab481(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab482(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab483(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab484(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab485(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab486(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab487(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab488(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab489(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab490(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab491(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab492(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab493(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab494(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab495(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab496(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab497(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab498(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab499(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "performance")
	public void MsisdnRegistrationQlab500(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	// private void methodOne(MyTmoData myTmoData){
	// RestAssured.baseURI = "https://stage.eos.corporate.t-mobile.com";
	//
	// RequestSpecification request = RestAssured.given();
	//
	// request.header("Authorization", "01.USR.0Ox5rozfztHjBOTmr");
	// request.header("X-B3-TraceId", "123456787");
	// request.header("X-B3-SpanId", "123456787");
	// request.header("channel_id", "DESKTOP");
	// request.header("ban", myTmoData.getLoginPassword());
	// request.header("msisdn", myTmoData.getLoginEmailOrPhone());
	// request.contentType("application/json");
	//
	// JSONObject requestParams = new JSONObject();
	// requestParams.put("accountNumber", myTmoData.getLoginPassword());
	// requestParams.put("msisdn", myTmoData.getLoginEmailOrPhone());
	// requestParams.put("dataServiceSoc", "ALL");
	// requestParams.put("optionalServiceSocs", "ALL");
	// requestParams.put("dataPassSoc", "ALL");
	// requestParams.put("userRole", "PAH");
	//
	//// System.out.println(requestParams.toString());
	// request.body(requestParams.toString());
	//
	// request.trustStore(System.getProperty("user.dir")+"/src/test/resources/cacerts","changeit");
	// long startTime = System.currentTimeMillis();
	// response = request.post("/v1/cpslookup/ondevicefulfillment");
	// long EndTime = System.currentTimeMillis();
	// System.out.println(EndTime-startTime);
	// Reporter.log("Msisdn " + myTmoData.getLoginEmailOrPhone());
	// Reporter.log("Ban " + myTmoData.getLoginPassword());
	// if (response.body().asString() != null && response.getStatusCode() ==
	// 200) {
	//
	// Reporter.log("Success");
	// }
	// else {
	// Reporter.log(response.asString());
	// Assert.fail("invalid response");
	// }
	// }

}
