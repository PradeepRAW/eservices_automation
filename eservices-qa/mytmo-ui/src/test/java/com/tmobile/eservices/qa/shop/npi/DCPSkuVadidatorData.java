package com.tmobile.eservices.qa.shop.npi;

public class DCPSkuVadidatorData {
	
	public String familyId;
	public String manfactType;
	public String modelName;
	public String skuNum;
	public String color;
	public String memory;
	public String availableStatus;
	public String accessorySkuNum;
	public String promoName;
	public String promoCustomerFacingName;
	
	public String getFamilyId() {
		return familyId;
	}
	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}
	public String getManfactType() {
		return manfactType;
	}
	public void setManfactType(String manfactType) {
		this.manfactType = manfactType;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getSkuNum() {
		return skuNum;
	}
	public void setSkuNum(String skuNum) {
		this.skuNum = skuNum;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getMemory() {
		return memory;
	}
	public void setMemory(String memory) {
		this.memory = memory;
	}
	public String getAvailableStatus() {
		return availableStatus;
	}
	public void setAvailableStatus(String availableStatus) {
		this.availableStatus = availableStatus;
	}
	public String getAccessorySkuNum() {
		return accessorySkuNum;
	}
	public void setAccessorySkuNum(String accessorySkuNum) {
		this.accessorySkuNum = accessorySkuNum;
	}
	public String getPromoName() {
		return promoName;
	}
	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}
	public String getPromoCustomerFacingName() {
		return promoCustomerFacingName;
	}
	public void setPromoCustomerFacingName(String promoCustomerFacingName) {
		this.promoCustomerFacingName = promoCustomerFacingName;
	}

}
