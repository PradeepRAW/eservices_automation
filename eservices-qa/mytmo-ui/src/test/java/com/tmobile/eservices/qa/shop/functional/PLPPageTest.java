package com.tmobile.eservices.qa.shop.functional;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.shop.*;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import com.tmobile.eservices.qa.shop.ShopConstants;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class PLPPageTest extends ShopCommonLib {

	/**
	 * Gap Analysis:Validate PLP Filter for Apple Devices
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testAppleDevicesOnManufatureHeaderInFilterDropDown(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condition | PAH User");
		Reporter.log(
				"1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on See All Phones on shop page | Application Navigated to Device PLP page");
		Reporter.log("4. Click on Filter DropDown  | Filter Drop Down should be Displayed");
		Reporter.log("5. Click on Apple and Close the Drop Down | PLP Page With Apple Devices Should Be Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PLPPage plpPage = navigateToPLPPageBySeeAllPhones(myTmoData);
		plpPage.clickFilteDropdown();
		plpPage.selectApple();
		plpPage.clickOnCloseFilter();
		plpPage.verifyFilterDevicesInPLPPage("Apple");
	}

	/**
	 * US140890 - PLP Due Today modal As a mytmo.com user on the PLP, I want to be
	 * able to click on the "Due Today" amount and see an explanation of what this
	 * number includes, so that I better understand what I'm paying for. TA1651837 :
	 * PLP Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testDueTodayModelInPLPPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : Verify due today model in PLP pager");
		Reporter.log("Data Condtision | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log(
				"5. Verify EIP Pricing modal available for all the devices on PLP  | EIP Pricing modal should be displayed ");
		Reporter.log("6. Click on today button |Down payment model should be displayed");
		Reporter.log("7. Verify down payment model text | Down payment model text should be displayed");
		Reporter.log("8. Verify down payment model note | Down payment model note should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PLPPage plpPage = navigateToPLPPageBySeeAllPhones(myTmoData);
		plpPage.verifyEIPPricingModal();
		plpPage.clickTodayButton();
		plpPage.verifyDownPaymentModelHeader();
		plpPage.verifyDownPaymentModelText();
		plpPage.verifyDownPaymentModelNote();
	}

	/**
	 * US137096 - Pagination (desktop) As a mytmo user, I want to be able to
	 * navigate the PLP through pages at the bottom of the screen, so that I can
	 * easily and quickly browse devices.
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testPaginationOnPLPPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : Verify pagination in PLP page");
		Reporter.log("Data Condtision | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Vefify pagination | Pagination should be displayed");
		Reporter.log("6. Vefify number of rows  | 3 rows should be displayed");
		Reporter.log("7. Vefify number of devices per row  | 4 devices should be displayed");
		Reporter.log("8. Vefify Legal Text of devices per row  | Legal Text of devices per row  should be displayed");
		Reporter.log("9. Vefify Legal Text Bottom Of the Page  | Legal Text Bottom Of the Page  should be displayed");
		Reporter.log("9. Vefify Footer Is Not Displayed  | Footer should Not Displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PLPPage plpPage = navigateToPLPPageBySeeAllPhones(myTmoData);
		plpPage.verifyRows();
		plpPage.verifyDevicesPerRow();
		plpPage.verifyDeviceComponentInPlpPage();
		plpPage.verifyLegalTextComponentInPLP();
		plpPage.verifyPLPPageFooterNotDisplayed();

	}

	/**
	 * Gap Analysis:Validate PLP Filter for Samsung Devices
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testSamsungDevicesOnManufatureHeaderInFilterDropDown(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condition | PAH User");
		Reporter.log(
				"1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on See All Phones on shop page | Application Navigated to Device PLP page");
		Reporter.log("4. Click on Filter DropDown  | Filter Drop Down should be Displayed");
		Reporter.log("5. Click on Samsung and Close the Drop Down | PLP Page With Samsung Devices Should Be Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PLPPage plpPage = navigateToPLPPageBySeeAllPhones(myTmoData);
		plpPage.clickFilteDropdown();
		plpPage.selectSamsung();
		plpPage.clickOnCloseFilter();
		plpPage.verifyFilterDevicesInPLPPage("Samsung");
	}

	/**
	 * TA1651837 : PLP Page
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void verifyLatestDealsFromQuicklinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: TA1651837 : PLP Page");
		Reporter.log("Data Condition | IR PAH Customer");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on Shop tab on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on Latest deals from quick links | Devices with offers Should be Displayed ");

		Reporter.log("================================");

		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("Latest deals");
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.verifySpecialOfferBadgesForDevices();
	}

	/**
	 * US132813 Sorting (Low to High / High to Low) : As a mytmo.com customer, I
	 * want to be able to sort products on the PLP, so that I can more easily view
	 * the products I want to shop for. TA1910109, C411004 C411005
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testSortingInPLPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify sorting functionality in PLP page");
		Reporter.log("Data Condtision | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Verify default sorting option as Featured | Default sorting option should be displayed");
		Reporter.log("6. Select sorting option as High Rating | High rating devices should be displayed");
		Reporter.log("7. Select sorting option as Price Low To High | Prices low to high devices should be displayed");
		Reporter.log("8. Select sorting option as Price High To Low | Price High To Low devices should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PLPPage plpPage = navigateToPLPPageBySeeAllPhones(myTmoData);
		plpPage.getSelectedOptionFromSoryBy();
		plpPage.selectSortBy(ShopConstants.PRICE_LOW_TO_HIGH);
		plpPage.verifyPriceLowToHigh();
		plpPage.selectSortBy(ShopConstants.PRICE_HIGH_TO_LOW);

	}

	/**
	 * TA1651837 : PLP Page C411004 C411005
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void verifyResultsCountAfterApplyingSorting(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: TA1651837 : PLP Page");
		Reporter.log("Data Condition | IR PAH Customer");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on Shop tab on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on Latest deals from quick links | Devices with offers Should be Displayed ");

		Reporter.log("================================");

		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickSeeAllPhones();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		int devicesCount = plpPage.getDevicesCountFromPLP();
		plpPage.selectSortBy(ShopConstants.PRICE_HIGH_TO_LOW);
		int deviceCountAfterSorting = plpPage.getDevicesCountFromPLP();
		plpPage.verifyDevicesCountAfterApplyingSorting(devicesCount, deviceCountAfterSorting);
	}

	/**
	 * TA1651837 : PLP Page
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void verifyResultsCountAfterApplyingFilters(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: TA1651837 : PLP Page");
		Reporter.log("Data Condition | IR PAH Customer");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on Shop tab on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See All Phones | PLP Should be displayed");
		Reporter.log("4. Verify Devices Count on PLP | Total Results count Should be displayed");
		Reporter.log(
				"5. Apply any of the filter on PLP | PLP Should be updated and results count should be updated accordongly");
		Reporter.log("6. Verify Devices count before and After applying the filters | Results count should not match");

		Reporter.log("================================");

		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickSeeAllPhones();

		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		int devicesCount = plpPage.getDevicesCountFromPLP();
		plpPage.clickFilteDropdown();
		plpPage.selectApple();
		plpPage.clickOnCloseFilter();
		int deviceCountAfterFilter = plpPage.getDevicesCountFromPLP();
		plpPage.verifyDevicesCountAfterApplyingFilter(devicesCount, deviceCountAfterFilter);
	}

	/**
	 * Gap Analysis:Validate PLP Filter for T-Mobile TA1910109, C411004
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testTMobileOnManufatureHeaderInFilterDropDown(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condtision | IR STD PAH Customer");
		Reporter.log(
				"1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on See All Phones on shop page | Application Navigated to Device PLP page");
		Reporter.log("4. Click on Filter DropDown  | Filter Drop Down should be Displayed");
		Reporter.log("5. Click on T-Mobile and Close the Drop Down | PLP Page With All Devices Should Be Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickSeeAllPhones();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickFilteDropdown();
		plpPage.verifyFilterDropDownHeaders();
		plpPage.selectTMobileManufacturer();
		plpPage.clickOnCloseFilter();
		plpPage.verifyFilterDevicesInPLPPage("T-Mobile");
	}

	/**
	 * US492483 : V3 Gaps - plp/pdp Device, plp/pdp accessories, shop.
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void testDeviceNamesonPLPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US492483 : V3 Gaps - plp/pdp Device, plp/pdp accessories, shop.");
		Reporter.log("Data Condition | IR PAH User");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on Shop tab on home page | User should be navigated to Shop Page ");
		Reporter.log("3. Click on the 'All Phones' link | PLP should be displayed");
		Reporter.log("4. Validate Devcie Names on PLP Page | Device Names should be displayed with Manufacture Name");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPLPPageBySeeAllPhones(myTmoData);
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyAllDeviceManufacturerName();
		plpPage.verifyAllDeviceName();

	}

	/**
	 * Gap Analysis:Validate PLP Filter for Promotion Devices
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testOfferDevicesOnDealsHeaderInFilterDropDown(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log(
				"1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on See All Phones on shop page | Application Navigated to Device PLP page");
		Reporter.log("4. Click on Filter DropDown  | Filter Drop Down should be Displayed");
		Reporter.log(
				"5. Click on Offers on Deals Header and Close the Drop Down | PLP Page With Offer Devices Should Be Displayed");
		Reporter.log("6. Verify Promotion Badge on Devices | PLP Page With Promotion Badges Should Be Displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickSeeAllPhones();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickFilteDropdown();
		plpPage.selectSpecialOfferOnDeals();
		plpPage.clickOnCloseFilter();
		plpPage.verifyPageLoaded();
	}

	/**
	 * US505782:-DEFECT:Prod [MyTMO|Shop] Standard Upgrade: T-Mobile Filter fails to
	 * show Revvl or Revvl+
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testTmobleManufacturerDevicesDisplayedInPlpPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"US505782:-DEFECT:Prod [MyTMO|Shop] Standard Upgrade: T-Mobile Filter fails to show Revvl or Revvl+");
		Reporter.log("Data Condition | STD PAH User");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on Shop tab on home page | User should be navigated to Shop Page ");
		Reporter.log("3. Click on See All Phones on shop page | Application Navigated to Device PLP page");
		Reporter.log("4. Click on Filter DropDown  | Filter Drop Down should be Displayed");
		Reporter.log(
				"5. Click on T-Moble Manufacturer device and Close the filter Drop Down | T-Mobile Devices should be displayed in plp page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPLPPageBySeeAllPhones(myTmoData);
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.clickFilteDropdown();
		plpPage.selectTMobileManufacturer();
		plpPage.clickOnCloseFilter();
		plpPage.verifyFilterDevicesInPLPPage(myTmoData.getMake());

	}

	/**
	 * Gap analysis task: Navigated to device PLP page from home page
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPlpPageFromHomePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Gap analysis task:Navigated to device PLP page from home page");
		Reporter.log("Data Condition | STD PAH User");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on upgrade link | User should be navigated to PLP page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickUpgradeLink();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
	}

}
