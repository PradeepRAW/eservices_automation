/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.payments.AutoPayCancelConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.AutoPayConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.AutoPayPage;
import com.tmobile.eservices.qa.pages.payments.BillingSummaryPage;
import com.tmobile.eservices.qa.pages.payments.FAQPage;
import com.tmobile.eservices.qa.pages.payments.OTPConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.pages.payments.TnCPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class AutoPayPageTest extends PaymentCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(AutoPayPageTest.class);

	// US203322 Payment Hub (Desktop) - AutoPay Blade (Eligible)

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void testVerifyAutopayPageLoad(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US203322	Payment Hub (Desktop) - AutoPay Blade (Eligible)");
		Reporter.log("Data Conditions - Regular user. AutoPay Off");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Set-Auto-Pay | Auto pay page should be displayed");
		Reporter.log("Step 4: Verify autopay page loading | Autopay page should be loaded successfully");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToAutoPayPage(myTmoData);

	}

	/**
	 * US203322 Payment Hub (Desktop) - AutoPay Blade (Eligible)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testVerifyAutopayOnPAHubWhenAutopayOff(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US203322	Payment Hub (Desktop) - AutoPay Blade (Eligible)");
		Reporter.log("Data Conditions - Regular user. AutoPay Off");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on billing link | Billing summary page should be displayed");
		Reporter.log("Step 4: Verify autopay off alert | Autopay off alert should be dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		;
		billingSummaryPage.verifyAutopayAlertwhenOFf();
		billingSummaryPage.verifyAutopaySubAlertwhenOFf();
		billingSummaryPage.clickEasyPayArrow();
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyAutoPayLandingPageFields();

	}

	/**
	 * US222985 DE94748 Offline Stack: NC accounts shows up Auto Pay This defect has
	 * been Indefinitely Deferred by the Business
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testNCAccountDoesnNotShowsUpAutoPay(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testNCAccountDoesnNotShowsUpAutoPay - US222985	DE94748 Offline Stack: NC accounts shows up Auto Pay");
		Reporter.log("Test Case : US222985	DE94748 Offline Stack: NC accounts shows up Auto Pay");
		Reporter.log("Data Conditions - NC account. Account with low credit score");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Verify Setup Auto Pay button | Auto pay button should not be displayed for NC accounts");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		HomePage homePage = navigateToHomePage(myTmoData);
		;
		homePage.verifyAutoPayMatch("OFF");
		homePage.verifySetupAutopayBtn();

	}

	/**
	 * US222984 DE94687 Offline Stack_Account with Standard or Restricted roles is
	 * seeing “Set- up Autopay” CTA on Home page and upon clicking navigated to the
	 * profile page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testAutoPayRestrictedAndStandardUsersRedirectToProfilePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US222984	DE94687 testAutoPayRestrictedAndStandardUsersRedirectToProfilePage");
		Reporter.log("Data Conditions - Standard or Restricted user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Auto pay  link | Profile page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickonAutopayLink();
		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}

	/**
	 * Verify restricted users cannot enable AutoPay
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void verifyRestricteduserAutopayON(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyRestricteduserAutopayOFF method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Verify restricted users cannot disable AutoPay");
		Reporter.log("Data Conditions - Restricted user. Eligible for AutoPay");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify AutoPay ON link | Autopay link should be disabled");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		HomePage homePage = navigateToHomePage(myTmoData);
		;
		homePage.verifyAutopayONLinkDisabled();
	}

	/**
	 * Verify restricted users cannot disable AutoPay
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void verifyRestrictedStandardUserAutopayGreyedOut(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyRestricteduserAutopayOFF method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Verify restricted users cannot disable AutoPay");
		Reporter.log("Data Conditions - Standart/Restricted user. AutoPay is ON/OFF");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify AutoPay OFF link | Autopay OFF link should be disabled");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyAutopayLinkGreyedOut();
	}

	/**
	 * US262983 GLUE Light Reskin - Exit Autopay Modal
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular account. Autopay OFF/ON
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testGLRExitAutoPayFlowModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US262983 GLUE Light Reskin - Exit Autopay Modal");
		Reporter.log("Data Conditions - Regular account. Autopay OFF/ON");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to Auto Pay landing page | Auto Pay landing Page should be displayed");
		Reporter.log("Step 4. Click on Cancel CTA | Verify Exit Autopay popup:");
		Reporter.log("Step 4.1 Verify header is present: ARE YOU SURE YOU WANT TO EXIT AUTOPAY?");
		Reporter.log("Step 4.2 Verify Any changes you have made will not be saved. is present");
		Reporter.log("Step 4.3 Verify Yes CTA is displayed");
		Reporter.log("Step 4.4 Verify No CTA is displayed");
		Reporter.log("Step 5. Click No CTA | Verify autopay page is displayed");
		Reporter.log("Step 6. Click on Cancel CTA | Verify Exit Autopay popup appears");
		Reporter.log("Step 7. Click Yes CTA | Verify Home page is displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AutoPayPage autoPayPage = navigateToAutoPayPage(myTmoData);
		autoPayPage.clickCancelAutoPayButton();
		autoPayPage.verifycancelAutoPayModal();
		autoPayPage.clickNoExitApModal();
		autoPayPage.verifyAutoPayLandingPageFields();
		autoPayPage.clickCancelAutoPayButton();
		autoPayPage.verifycancelAutoPayModal();
		autoPayPage.clickAutopayExitModelYesButton();
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
	}

	/**
	 * US262984 GLUE Light Reskin - AutoPay Update landing page
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular account. Autopay ON
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testGLRAutoPayUpdateLandingPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("US283072	GLUE Light Reskin - AutoPay Update landing page");
		Reporter.log("Data Conditions - Regular account. Autopay ON");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3. Click on AUTOPAY | Verify autopay page is displayed");
		Reporter.log("Step 3.1 Verify button Agree and Submit is present");
		Reporter.log("Step 3.2 Verify button Cancel is present");
		Reporter.log("Step 3.3 Verify link See FAQs is present");
		Reporter.log("Step 3.4 Verify link terms and conditions is present");
		Reporter.log("Step 3.5 Verify Payment will process on the XXth of each month. is present");
		Reporter.log("Step 3.6 Verify Payment method blade is present");
		Reporter.log("Step 3.7 Verify Edit Payment method link is present");
		Reporter.log("Step 3.8 Verify Next Autopay payment .... Bill due: ... is present");
		Reporter.log("Step 3.9 Verify header Autopay Settings is present");
		Reporter.log("Step 3.10 Verify Cancel Autopay link is present");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		if (!homePage.verifyAutoPay("ON")) {
			setupAutopay(myTmoData);
		}
		homePage.clickEasyPay();
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyPageLoaded();
		autoPayPage.verifyAutoPayLandingPageFields();
	}

	/**
	 * US262980 GLUE Light Reskin - AutoPay Setup landing page
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular account. Autopay OFF
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void testGLRAutoPaySetupLandingPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("US262980	GLUE Light Reskin - AutoPay Setup landing page");
		Reporter.log("Test Case : US262980	GLUE Light Reskin - AutoPay Setup landing page");
		Reporter.log("Data Conditions - Regular account. Autopay OFF");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3. Click on SET UP AUTOPAY | Verify autopay page is displayed");
		Reporter.log("Step 3.1 Verify button Agree and Submit is present");
		Reporter.log("Step 3.2 Verify button Cancel is present");
		Reporter.log("Step 3.3 Verify link See FAQs is present");
		Reporter.log("Step 3.4 Verify link terms and conditions is present");
		Reporter.log("Step 3.5 Verify Payment will process on is present");
		Reporter.log("Step 3.6 Verify Payment method blade is present");
		Reporter.log("Step 3.7 Verify Edit Payment method link is present");
		Reporter.log("Step 3.8 Verify First Autopay payment .... Bill due: ... is present");
		Reporter.log("Step 3.9 Verify header Autopay Settings is present");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AutoPayPage autoPayPage = navigateToAutoPayPage(myTmoData);
		autoPayPage.verifyAutoPayLandingPageFields();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testPIIMaskingForAutoPayPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US356346 | US356346 PII Masking > Variables on AutoPay Page");
		Reporter.log("Data Conditions - Regular user. AutoPay Off");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Autopay ON | Auto pay page should be displayed");
		Reporter.log("Step 4: Verify autopay page loading | Autopay page should be loaded successfully");
		Reporter.log(
				"6. Verify PII Masking on autopay Page | First Name, Last Name,Nicknam,Billing,address,Payment card information (Credit card or check number or routing number)\n"
						+ "BAN/account number,City,Zipcode,Mobile number");
	}

	/**
	 * US242115:PA Messaging Enhancements - ALL PAs - OTP Confirmation Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testALLPAsMessageonOTPConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US242115:PA Messaging Enhancements - ALL PAs - OTP Confirmation Page");
		Reporter.log("Data Condition | one-time payment of an amount = total PA amount");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("1. Navigate to my t-mobile.com | Login page should be displayed");
		Reporter.log("2. Login as a mytmo customer | Home page should be displayed");
		Reporter.log("3. Click on paybill | One time payment page should be displayed");
		Reporter.log("4. Click on agree and	submit button | One time payment confirmation page should be displayed");
		/* COD SDET modifications */
		Reporter.log(
				"5. Verify PA completion notification | 'Your Payment Arrangement is now complete! Save time & avoid missing payments in the future by enrolling in AutoPay.");
		Reporter.log("6. Verify 'enrolling in AutoPay.' | Autopay link should be displayed");
		/*-------*/
		Reporter.log("6. Click on autopay hyperlink | Autopay landing page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), spokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.verifyPANotification(PaymentConstants.PA_OTP_CONF_TOTAL_AMOUNT_OUTSIDE_BLACKOUT);
		otpConfirmationPage.clickonEnrollAutopay();
		AutoPayPage autopaylandingpage = new AutoPayPage(getDriver());
		autopaylandingpage.verifyPageLoaded();
	}

	/**
	 * US516533 Payments - AutoPay Restriction during pending PA via Modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testAutopaySetupNotEligibleforPAscheduledUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US516533	Payments - AutoPay Restriction during pending PA via Modal");
		Reporter.log("Data Conditions - PA scheduled User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Set-Auto-Pay | Auto pay page should be displayed");
		Reporter.log("Step 4: Verify autopay not eligible popup | autopay not eligible popup should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToAutoPayPage(myTmoData);
		AutoPayPage autopaylandingpage = new AutoPayPage(getDriver());
		autopaylandingpage.verifyAutopaynotEligibleModalforPAScheduledUser();
		autopaylandingpage.clickGoToHome();

		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();

	}

	/**
	 * US578985 Disallow AutoPay for Business_User_NonMaster_NonPAH
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testAutopayDisAllowedforBusinessUserNonMasterNonPAH(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US578985	Disallow AutoPay for Test autopay for Business_User_NonMaster_NonPAH");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Set-Auto-Pay | Auto pay disallowed modal should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

		getDriver().get(System.getProperty("environment") + "/autopay");
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyAutopayDisAllowedforNonMasterUser(myTmoData);
		autoPayPage.clickGotoHomeButton();
		homePage.verifyPageLoaded();

	}

	/**
	 * US578985 Disallow AutoPay for Business_User_NonMaster_PAH
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testAutopayDisAllowedforBusinessUserNonMasterPAH(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US578985	Disallow AutoPay for Test autopay for Business_User_NonMaster_PAH");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Set-Auto-Pay | Auto pay disallowed modal should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

		getDriver().get(System.getProperty("environment") + "/autopay");
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyAutopayDisAllowedforNonMasterUser(myTmoData);
		autoPayPage.clickGotoHomeButton();
		homePage.verifyPageLoaded();

	}

	/**
	 * US578985 Disallow AutoPay for Governament_User_NonMaster_NonPAH
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testAutopayDisAllowedforGovernamentUserNonMasterNonPAH(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US578985	Disallow AutoPay for Test autopay for Governament_User_NonMaster_NonPAH");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Set-Auto-Pay | Auto pay disallowed modal should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

		getDriver().get(System.getProperty("environment") + "/autopay");
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyAutopayDisAllowedforNonMasterUser(myTmoData);
		autoPayPage.clickGotoHomeButton();
		homePage.verifyPageLoaded();

	}

	/**
	 * US578985 Disallow AutoPay for Governament_User_NonMaster_PAH
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testAutopayDisAllowedforGovernamentUserNonMasterPAH(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US578985	Disallow AutoPay for Test autopay for Governament_User_NonMaster_PAH");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Set-Auto-Pay | Auto pay disallowed modal should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();

		getDriver().get(System.getProperty("environment") + "/autopay");
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyAutopayDisAllowedforNonMasterUser(myTmoData);
		autoPayPage.clickGotoHomeButton();
		homePage.verifyPageLoaded();

	}

	// Qlab03 tests

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "qlab03" })
	public void testAutopayConfirmationPagewithnewBankQlab(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on autopay link | autopay page should be displayed");
		Reporter.log(
				"4. enter bank details and click on Agree and submit | autopay confirmation page should be displayed");
		Reporter.log(
				"5. verify elements on autopay confirmation page | All elements on autopay confirmation page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		getDriver().get(System.getProperty("environment") + "/autopay");

		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyPageLoaded();

		autoPayPage.addBankToPaymentMethod(myTmoData);
		autoPayPage.clickAgreeAndSubmitBtn();
		AutoPayConfirmationPage autoPayConfirmationPage = new AutoPayConfirmationPage(getDriver());

		autoPayConfirmationPage.verifyAutoPayConfirmationPageElements(autoPayConfirmationPage, "Bank");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "qlab03" })
	public void testAutopayConfirmationPagewithnewCardQlab(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on autopay link | autopay page should be displayed");
		Reporter.log(
				"4. enter Card details and click on Agree and submit | autopay confirmation page should be displayed");
		Reporter.log(
				"5. verify elements on autopay confirmation page | All elements on autopay confirmation page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		getDriver().get(System.getProperty("environment") + "/autopay");

		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyPageLoaded();
		// String paymentType =
		// autoPayPage.verifyStoredPaymentMethodOrAddBank(myTmoData);
		autoPayPage.addCardToPaymentMethod(myTmoData);
		autoPayPage.clickAgreeAndSubmitBtn();
		AutoPayConfirmationPage autoPayConfirmationPage = new AutoPayConfirmationPage(getDriver());
		// autoPayPage.addCardToPaymentMethod(myTmoData);

		autoPayConfirmationPage.verifyAutoPayConfirmationPageElements(autoPayConfirmationPage, "Card");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "qlab03" })
	public void testAutopayConfirmationPagewithSavedCardQlab(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on autopay link | autopay page should be displayed");
		Reporter.log("4. click on Agree and submit | autopay confirmation page should be displayed");
		Reporter.log(
				"5. verify elements on autopay confirmation page | All elements on autopay confirmation page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		getDriver().get(System.getProperty("environment") + "/autopay");

		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyPageLoaded();
		String paymentType = autoPayPage.verifyStoredPaymentMethodOrAddCard(myTmoData);
		autoPayPage.addCardToPaymentMethod(myTmoData);
		autoPayPage.clickAgreeAndSubmitBtn();
		AutoPayConfirmationPage autoPayConfirmationPage = new AutoPayConfirmationPage(getDriver());

		autoPayConfirmationPage.verifyAutoPayConfirmationPageElements(autoPayConfirmationPage, "Card");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "qlab03" })
	public void testAutopayConfirmationPagewithSavedBankQlab(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on autopay link | autopay page should be displayed");
		Reporter.log("4. click on Agree and submit | autopay confirmation page should be displayed");
		Reporter.log(
				"5. verify elements on autopay confirmation page | All elements on autopay confirmation page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		getDriver().get(System.getProperty("environment") + "/autopay");

		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyPageLoaded();
		String paymentType = autoPayPage.verifyStoredPaymentMethodOrAddBank(myTmoData);
		autoPayPage.addCardToPaymentMethod(myTmoData);
		autoPayPage.clickAgreeAndSubmitBtn();
		AutoPayConfirmationPage autoPayConfirmationPage = new AutoPayConfirmationPage(getDriver());

		autoPayConfirmationPage.verifyAutoPayConfirmationPageElements(autoPayConfirmationPage, "Bank");

	}

	/**
	 * 
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular account. Autopay ON
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "qlab03" })
	public void testCancelAutopay(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Data Conditions - Regular account. Autopay ON");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to Auto Pay landing page  | Auto Pay landing Page should be displayed");
		Reporter.log(
				"Step 4. Click Cancel Autopay | Verify Are you sure you want to cancell autopay? pop-up is present");
		Reporter.log("Step 5. Click Yes, Cancel Autopay | Verify cancelConfirmation page is displayed");
		Reporter.log("Step 5.1 Verify Autopay cancelled! is dispplayed");
		Reporter.log("Step 5.2 Verify information line is present: You will no longer recieve AutoPay discount");
		Reporter.log("Step 5.3 Verify Return to home CTA is present");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		getDriver().get(System.getProperty("environment") + "/autopay");

		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyPageLoaded();

		if (!homePage.verifyAutopayisscheduled()) {
			setupAutopay(myTmoData);
		}
		cancelonlyAutoPay(homePage, myTmoData);
		AutoPayCancelConfirmationPage autoPayCancelConfirmationPage = new AutoPayCancelConfirmationPage(getDriver());
		autoPayCancelConfirmationPage.verifyPageLoaded();

	}

	/**
	 * US408958 PA : General - TNC page creation
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular account. Autopay Eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutoPayTnCPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US408958 PA : General - TNC page creation");
		Reporter.log("Data Conditions - Regular account. Autopay eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to Auto Pay landing page  | Auto Pay landing Page should be displayed");
		Reporter.log("Step 4: Click link \"terms and conditions\" | Verify termsAndConditions page is loaded");
		Reporter.log("Step 4.1 Verify header Terms and Conditions is present");
		Reporter.log("Step 4.2 Verify Back CTA is present");
		Reporter.log("Step 5. Click Back CTA | Verify Autopay page is displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AutoPayPage autoPayPage = navigateToAutoPayPage(myTmoData);
		autoPayPage.clicktermsNconditions();
		TnCPage tncPage = new TnCPage(getDriver());
		tncPage.verifyPageLoaded();
		tncPage.validateTnCPage();
		tncPage.clickBackButton();
		autoPayPage.verifyPageLoaded();
	}

	/**
	 * US40895 PA : General - FAQ page creation
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular account. Autopay Eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutoPayFAQPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US40895 PA : General - FAQ page creation");
		Reporter.log("Data Conditions - Regular account. Autopay eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to Auto Pay landing page  | Auto Pay landing Page should be displayed");
		Reporter.log("Step 4: Click link \"FAQ\" | Verify termsAndConditions page is loaded");
		Reporter.log("Step 4.1 Verify header Frequenlty Asked Questions is present");
		Reporter.log("Step 4.2 Verify Back CTA is present");
		Reporter.log("Step 5. Click Back CTA | Verify Autopay page is displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AutoPayPage autoPayPage = navigateToAutoPayPage(myTmoData);
		autoPayPage.clickAutoPayFAQsLink();
		FAQPage faqPage = new FAQPage(getDriver());
		faqPage.verifyPageLoaded();
		faqPage.validateFAQPage();
		faqPage.clickOnBackBtn();
		autoPayPage.verifyPageLoaded();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCCSPCMAutopayInUseForNewCard(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. Click on autopay link | autopay page should be displayed");
		Reporter.log("4. Click on Agree and submit | autopay confirmation page should be displayed");
		Reporter.log(
				"5. verify elements on autopay confirmation page | All elements on autopay confirmation page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		/*
		 * HomePage homePage = navigateToHomePage(myTmoData);
		 * homePage.verifyPageLoaded();
		 * getDriver().get(System.getProperty("environment") + "payments/autopay");
		 */

		AutoPayPage autoPayPage = navigateToAutoPayPage(myTmoData);
		autoPayPage.addCardToPaymentMethod(myTmoData);
		autoPayPage.clickAddPaymentMethod();
		autoPayPage.verifyAutoPaySpokePage();
		autoPayPage.verifyInUsePaymentMethod();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCCSPCMAutopayInUseForNewBank(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. Click on autopay link | autopay page should be displayed");
		Reporter.log("4. Click on Agree and submit | autopay confirmation page should be displayed");
		Reporter.log(
				"5. verify elements on autopay confirmation page | All elements on autopay confirmation page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		/*
		 * HomePage homePage = navigateToHomePage(myTmoData);
		 * homePage.verifyPageLoaded();
		 * getDriver().get(System.getProperty("environment") + "payments/autopay");
		 */

		AutoPayPage autoPayPage = navigateToAutoPayPage(myTmoData);
		autoPayPage.verifyPageLoaded();
		autoPayPage.addBankToPaymentMethod(myTmoData);
		autoPayPage.clickAddPaymentMethod();
		autoPayPage.verifyAutoPaySpokePage();
		autoPayPage.verifyInUsePaymentMethod();
	}

}
