package com.tmobile.eservices.qa.accounts.api.cpslookup;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class GetSocParameterTest extends AccountsApi {

	public Map<String, String> tokenMap;

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetSocParameter(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating getSocParameter  ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("socArray", "TMO1PLSG3");
		

		String requestBody = new ServiceTest().getRequestFromFile("getSocParameter.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getSocParameter(apiTestData, updatedRequest);

		String operationName = "get Soc Parameter";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			Assert.assertTrue(response.jsonPath().getString("isServiceFailure").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("socData.socChangeLevel").contains("UPGRADE"));
			Reporter.log(" 200 ok Success");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetSocParameterNODATA(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating getSocParameter  ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("socArray", "1PLUS");

		String requestBody = new ServiceTest().getRequestFromFile("getSocParameter.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getSocParameter(apiTestData, updatedRequest);

		String operationName = "test Get Soc Parameter NO DATA";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.jsonPath().getString("isServiceFailure").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("socData.socChangeLevel").contains("UPGRADE"));
			Reporter.log(" 200 ok Success");

		} else {
			failAndLogResponse(response, operationName);
		}
	}
}