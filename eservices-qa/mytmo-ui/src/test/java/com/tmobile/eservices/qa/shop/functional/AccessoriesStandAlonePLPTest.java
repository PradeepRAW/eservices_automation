package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAlonePLPPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

/**
 * @author rnallamilli
 *
 */
public class AccessoriesStandAlonePLPTest extends ShopCommonLib {


	/**
	 * TA1651842 : Accessories PLP
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS  })
	public void testCurrentCategoryForSpecificAccessory(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("TestCase TA1651842 : Accessories PLP");
		Reporter.log("Data Condition | IR PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Select any category from 'Current Category' dropdown | Respective category should be highlighted in the field");
		Reporter.log("7. Provide device name in the for field | Respective accessories should be filtered out and listed below");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("Accessories");
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		accessoriesStandAlonePLPPage.selectCurrentCategoryFromDropdown("Cases");
		accessoriesStandAlonePLPPage.chooseDeviceForCategory();
		accessoriesStandAlonePLPPage.verifyAccessoryWithCaseNames();
	}

	/**
	 * TA1651842 : Accessories PLP
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS  })
	public void verifyAccessoryResultsCountWhenFilterIsApplied(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("TestCase TA1651842 : Accessories PLP");
		Reporter.log("Data Condition | IR PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Verify Accessories count available on PLP | Accessories count should be displayed");
		Reporter.log("7. Apply any of the filter | Ensure that respective filter is applied and accessories are displayed accordingly");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("Accessories");
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		int accessoriesCount = accessoriesStandAlonePLPPage.getAccessoriesCount();
		accessoriesStandAlonePLPPage.applyManufacturerFilter();
		int resultAccessoriesCount = accessoriesStandAlonePLPPage.getAccessoriesCount();
		accessoriesStandAlonePLPPage.verifyAccessoriesCountAfterApplyingFilter(accessoriesCount,resultAccessoriesCount);

	}

	/**
	 * TA1651842 : Accessories PLP
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP,Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyAccessoryResultsCountWhenSortingIsApplied(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("TestCase TA1651842 : Accessories PLP");
		Reporter.log("Data Condition | IR PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Verify Accessories count available on PLP | Accessories count should be displayed");
		Reporter.log("7. Apply any of the Sorting | Ensure that respective sorting is applied and accessories are displayed accordingly");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("Accessories");
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		int accessoriesCount = accessoriesStandAlonePLPPage.getAccessoriesCount();
		accessoriesStandAlonePLPPage.applySorting();
		int resultAccessoriesCount = accessoriesStandAlonePLPPage.getAccessoriesCount();
		accessoriesStandAlonePLPPage.verifyAccessoriesCountAfterApplyingSorting(accessoriesCount,resultAccessoriesCount);
	}

	/**
	 * TA1651842 : Accessories PLP
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP,Group.DESKTOP, Group.ANDROID,
			Group.IOS  })
	public void testDevicesListCountDropDown(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("TestCase TA1651842 : Accessories PLP");
		Reporter.log("Data Condition | IR PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Verify Accessories count available on PLP | Accessories count should be displayed");
		Reporter.log("7. Change results per page value | Ensure that the results are shown as per the value selected from dropdown");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("Accessories");
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		int resultsPerPage = accessoriesStandAlonePLPPage.getAccessoriesResultsCountPerPage();
		accessoriesStandAlonePLPPage.changeAccessoriesCountPerPage();
		int updatedResultsPerPage = accessoriesStandAlonePLPPage.getAccessoriesResultsCountPerPage();
		accessoriesStandAlonePLPPage.verifyResultsCountPerPage(resultsPerPage, updatedResultsPerPage);
	}
	
	/**
	 * US502246:DEFECT: [MyTMO|Shop] standalone accessories filter doesn't get applied after choosing phone - must click tab or outside of filter
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testAccessoriesFiltersInAccessorysPlpPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US502246:DEFECT: [MyTMO|Shop] standalone accessories filter doesn't get applied after choosing phone - must click tab or outside of filter");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");		
		Reporter.log("6. Select cases under Category and Enter Iphone in model box and Select any model device | Applied filter device should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("Accessories");
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		accessoriesStandAlonePLPPage.selectCurrentCategoryFromDropdown("Cases");
		accessoriesStandAlonePLPPage.enterGenericDeviceName();
		accessoriesStandAlonePLPPage.verifyAccessoryWithCaseNames();
	}
	
	/**
	 * US505783:DEFECT: [MyTMO|Shop] Accessories - Clear Filter doesn't clear "current category" drop box
	 * 
	 * @param data
	 * @param myTmoData
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testCleaFiltersIsEnabledWhenUserSelectAnyProductFromCurrentCategory(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US505783:DEFECT: [MyTMO|Shop] Accessories - Clear Filter doesn't clear 'current category' drop box");
		Reporter.log("Data Condition | PAH User ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");		
		Reporter.log("6. Click on 'Current category' dropdown, And select any product | User should be selected product from Current category section");
		Reporter.log("7. Verify Clear Filters is enabled | Clear Filters should be enabled");		
		Reporter.log("8. Select the 'for' drop down and choose a device, then click away | Clear Filters should be enabled");
		Reporter.log("9. Click Clear Filters | Filters should be reset from Current category and For section");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("Accessories");
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.selectCurrentCategoryFromDropdown("Cases");
		accessoriesStandAlonePLPPage.verifyClearFilterCTA();
		accessoriesStandAlonePLPPage.chooseDeviceForCategory();		
		accessoriesStandAlonePLPPage.clickClearFilterCTA();
		accessoriesStandAlonePLPPage.verifyEmptycatagoryName();
		accessoriesStandAlonePLPPage.verifyEmptyForTextField();
	}

	/**
	 * US505779 : DEFECT: [MyTMO|Shop] Accessories PLP: First/Previous page nav is enabled on accessory PLP load - server error
	 * 	@param data
	 * 	@param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyPageNavigationButtonsOnAccessoriesPLP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US505779 : DEFECT: [MyTMO|Shop] Accessories PLP: First/Previous page nav is enabled on accessory PLP load - server error");
		Reporter.log("Data Condition | PAH User ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on shop link | Shop page should be displayed");
        Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
        Reporter.log("6. Scroll to the Page navigation buttons | Ensure that First, Previous, Next, Last & Respective page numbers section is displayed");
        Reporter.log("7. Verify First & Previous navigation buttons | Ensure that First, Previous buttons are greyed out");
        Reporter.log("8. Click on 'Last' navigation button | Ensure that Next & Last navigation buttons are greyed out");
        
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
        shopPage.clickQuickLinks("Accessories");
        AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
        accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
        accessoriesStandAlonePLPPage.verifyPageNationButton();
        accessoriesStandAlonePLPPage.verifyPageNavigationButtonAreGreyed("First","Previous");
        accessoriesStandAlonePLPPage.clickPageNationLastButton();
        accessoriesStandAlonePLPPage.verifyPageNavigationButtonAreGreyed("Next", "Last");
		 
		}
	
	/**
	 * US610775 : [MyTMO|Shop|Buy] Accessories - filters for phones with special characters fail - UI
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAccessoriesFiltersForPhonesWithSpecialCharacters(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test cASE: US610775 : [MyTMO|Shop|Buy] Accessories - filters for phones with special characters fail - UI");
		Reporter.log("Data Condition | Standard User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");		
		Reporter.log("6. Select REVVL phones under Current category filter | Accessories supporting REVVL phones must be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
        shopPage.clickQuickLinks("Accessories");
        AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
        accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
        accessoriesStandAlonePLPPage.chooseDeviceForCategory();	
        accessoriesStandAlonePLPPage.verifyFirstStandAloneAccessoryDevice();        
	}
		
}
