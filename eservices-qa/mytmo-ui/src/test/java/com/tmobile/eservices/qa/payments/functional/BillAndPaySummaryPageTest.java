package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.AutoPayPage;
import com.tmobile.eservices.qa.pages.payments.BenefitsStatementPage;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.BillDetailsPage;
import com.tmobile.eservices.qa.pages.payments.BillingSummaryPage;
import com.tmobile.eservices.qa.pages.payments.ChargeAccountPage;
import com.tmobile.eservices.qa.pages.payments.ChargeEquipmentPage;
import com.tmobile.eservices.qa.pages.payments.ChargeFeaturesPage;
import com.tmobile.eservices.qa.pages.payments.ChargeOneTimePage;
import com.tmobile.eservices.qa.pages.payments.ChargePlanPage;
import com.tmobile.eservices.qa.pages.payments.EipJodPage;
import com.tmobile.eservices.qa.pages.payments.LeaseDetailsPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayLandingPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.PaperlessBillingPage;
import com.tmobile.eservices.qa.pages.payments.PaymentArrangementPage;
import com.tmobile.eservices.qa.pages.payments.PreviousChargePage;
import com.tmobile.eservices.qa.pages.payments.UsagePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

public class BillAndPaySummaryPageTest extends PaymentCommonLib {

	/**
	 * Bill Summary "PAY NOW" CTA should redirect to (NEW) OTP page from BBill page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testNavigationToOTPPageFromBBillPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test 'Make a payment' navigation to OTP page from new Angular UI page.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on BILLING tab | Billing Summary page should be displayed");
		Reporter.log(
				"Step 4: Click on 'Make a payment' button | User should be navigated to One Time Payment (OTP) page");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickMakeAPaymentLinkOnBBPage();
		OneTimePaymentPage otpPage = new OneTimePaymentPage(getDriver());
		otpPage.verifyPageLoaded();
	}

	/**
	 * Bill Summary "PAY NOW" CTA should redirect to (NEW) OTP page from BBill page
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testBillAndPayPageLoaded(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test 'Make a payment' navigation to OTP page from new Angular UI page.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on BILLING tab | Billing Summary page should be displayed");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		navigateToBillAndPaySummaryPage(myTmoData);

	}

	/**
	 * Bill Summary "Usage" hyperlink/changes CTA is working as expected
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testUsageSectionAndCTARedirection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Billing page functionality per new Angular UI pages");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log(
				"Step 3: Click on Bill and Bill and Pay summary Page should be loaded | Billing page data should be displayed for both Brite Bill and EBill");
		Reporter.log("Step 4: Click on Usage link on BillandPaySummary Page | Usage Overview Page should be loaded");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyUsageSection();
		billAndPaySummaryPage.clickSeeAllUsageLink();
		UsagePage usage = new UsagePage(getDriver());
		usage.verifyPageLoaded();

	}

	/**
	 * Bill Summary "Autopay" Shortcut CTA is working as expectedand redirected to
	 * Autopay Landing page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testAutopayShortcutAndCTARedirection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Billing page functionality per new Angular UI pages");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Billing link | BillAndPay Summary page should be displayed");
		Reporter.log("Step 4: Click on Auto pay shortcut | Auto pay page should be loaded");
		Reporter.log("Step 5: Verify Autop pay landing page | Auto pay landing page header should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyAutoPayShortcut();
		billAndPaySummaryPage.clickAutopayShortcut();
		/*
		 * AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		 * autoPayPage.verifyNewAutoPayLandingPage();
		 */
		NewAutopayLandingPage nautopay = new NewAutopayLandingPage(getDriver());
		nautopay.verifyPageLoaded();
	}

	/**
	 * Bill Summary "Payment Arrangements" Shortcut CTA is working as expected and
	 * on click redirected to PA page
	 * 
	 * @param data      Account should pAH
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testPAShortcutAndCTARedirection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Billing page functionality per new Angular UI pages");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Bill and Pay | Bill and Pay sumary Page should be displayed");
		Reporter.log("Step 4: Click on Payment Arrangement shortcut |Payment Arrangement page should be loaded");
		Reporter.log("Step 5: Check the Payment Arrangement Header | Payment Arragement Header should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage briteBillPage = navigateToBillAndPaySummaryPage(myTmoData);
		briteBillPage.verifyPaymentArrangementShortcut();
		briteBillPage.clickPaymentArrangementShortcut();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPApageDisplayed();
	}

	/**
	 * Bill Summary "JUMP" Shortcut CTA is working as expected and redirection on
	 * click
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testJUMPShortcutAndCTARedirection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Billing page functionality per new Angular UI pages");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Bill and Pay | Bill and Pay summary Page should be displayed");
		Reporter.log("Step 4: Click on Jump on Demand shortcut | Jump Lease Details Page should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyJumpOnDemandLeaseShortcut();
		billAndPaySummaryPage.clickJumpOnDemandShortcut();
		LeaseDetailsPage leaseDetailsPage = new LeaseDetailsPage(getDriver());
		leaseDetailsPage.verifyPageLoaded();
	}

	/**
	 * Bill Summary "Account History" Shortcut CTA is working as expected and
	 * redirected to AH page as expected
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testAccountHistoryShortcutAndCTARedirection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Billing page functionality per new Angular UI pages");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Bill and Pay | Bill and Pay summary Page should be displayed");
		Reporter.log("Step 4: Click on Account History Short cut | Account History Page should be displayed");
		Reporter.log(
				"Step 5: Verify Account History Page | Account History Page should be displayed and Header should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage briteBillPage = navigateToBillAndPaySummaryPage(myTmoData);
		briteBillPage.verifyAccountHistoryShortcut();
		briteBillPage.clickAccountHistoryShortcut();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
	}

	/**
	 * Bill Summary "Equipment installment plans" Shortcut CTA is working as
	 * expected and redirected to EIP page as expected
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testEquipmentInstallmentPlansShortcutAndCTARedirection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Billing page functionality per new Angular UI pages");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Bill and Pay | Bill and Pay summary Page should be displayed");
		Reporter.log("Step 4: Click on Equipment Installment Plan shortcut | EIP Details page should be displayed");
		Reporter.log(
				"Step 5: Verify EIP details page | EIP details page should be loaded and header should be verified");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyEquipmentInstallmentPlansShortcut();
		billAndPaySummaryPage.clickEquipmentInstallmentPlansShortcut();
		EipJodPage eipJodPage = new EipJodPage(getDriver());
		eipJodPage.verifyEIPPage();

	}

	/**
	 * Bill Details "View details" CTA on Plan section is correctly redirecting to
	 * Plan tab
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testViewDetailsCTAAndRedirectionToPlansPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("testViewDetailsCTAAndRedirectionToPlansPage");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("5: Click on any category under charges | Bill details page should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickChargeCategory("Plans");
		ChargePlanPage chargePlanPage = new ChargePlanPage(getDriver());
		chargePlanPage.verifyPageLoaded();
		// commented below code as it is not consistent.
		/*
		 * chargePlanPage.clickPlansViewDetailsLink(); PlanPage planPage = new
		 * PlanPage(getDriver()); planPage.verifyPageLoad();
		 */
	}

	/**
	 * Brite Bill "Download PDF" CTA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testDownloadPDFBriteBill(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("testDownloadPDFBriteBill");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("5: Click on download PDF button | Modal should be displayed with bill types");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickDownloadPDF();
		billAndPaySummaryPage.verifyDownloadPdfModal();
		// billAndPaySummaryPage.clickCloseDownloadPdfModal();
	}

	/**
	 * US306179 - UMB : Fix Bill Details to Bill Summary Link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testBackToBillSummaryLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify print all payments link for brite bill in alert and activites page");
		Reporter.log("Data Condition - Regular account");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log(
				"5: Click on any other bill cycle (not the current) | Verify that Summary page is displayed for that period");
		Reporter.log("6: Click on Plans blade | Verify Charge Plan page is displayed");
		Reporter.log("7: Click on Back to summary link | Verify relevant Summary page is displayed (from Step 5)");
		Reporter.log(
				"8: Click on any other bill cycle (eBill) | Verify that Summary page is displayed for that period");
		Reporter.log("9: Click on any line | Verify details page is displayed for selected period");
		Reporter.log("10: Click on Back to summary link | Verify relevant Summary page is displayed (from Step 8)");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickPreviousBillCycle("Feb", "2018");
		billAndPaySummaryPage.verifyPreviousBillSummary();

		billAndPaySummaryPage.clickChargeCategory("Plans");
		ChargePlanPage chargePlanPage = new ChargePlanPage(getDriver());
		chargePlanPage.verifyPageLoaded();
		chargePlanPage.clickBackToSummaryLink();
		billAndPaySummaryPage.verifyPageLoaded();

		billAndPaySummaryPage.clickPreviousBillCycle("Dec", "2017");
		BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
		billingSummaryPage.verifyPageLoaded();
		billingSummaryPage.clickCurrentChargesRow();
		BillDetailsPage billDetailsPage = new BillDetailsPage(getDriver());
		billDetailsPage.verifyPageLoaded();
		billDetailsPage.clickOnBackToSummaryLink();
		billingSummaryPage.verifyPageLoaded();
	}

	/**
	 * US296554 - UMB: Update real time feed with active/suspend status for each
	 * line US296565 - UMB: Update real time feed immediate charges to include
	 * immediate sedona charges US300380 - UMB: Update real time feed to return a
	 * total sedona charges value
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.SEDONA })
	public void testSummaryPageWithSedonaCharges(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : US296554, US296565, US300380 Verify status, total and restore fees are present for Sedona account");
		Reporter.log("Data Condition - Regular account");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("5: Verify that AR balance is displayed");
		Reporter.log("6: Verify Sedona charges");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
	}

	/**
	 * US275165 - UMB - Add all names on Account to Brite:Bill dataSet US320447: Add
	 * all names on Account to Brite-Bill data Set US320447: [Continued] UMB - Add
	 * all names on Account to Brite:Bill dataSet
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testUMBAddAllNamesOnAccountLinesBriteBill(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US275165 - UMB - Add all names on Account to Brite:Bill dataSet");
		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("5: click 'view by line selector' | All account & Lines section should be displayed");
		Reporter.log("6: Verify Names for account and lines | Names should be displayed for all lines.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);

		billAndPaySummaryPage.clickViewByLineSelector();
		billAndPaySummaryPage.verifyNamesOnAccountLines();
	}

	/**
	 * verify charge category amounts sum equal to total math
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testBriteBillChargesAmountSumEqualsTotalMath(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : verify charge category amounts sum equal to total math");
		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("5: click 'view by line selector' | All account & Lines section should be displayed");
		Reporter.log("6: Verify Names for account and lines | Names should be displayed for all lines.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyAmountTotalInCurrentCharges();
	}

	/**
	 * verify Line category amounts sum equal to total math
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testBriteBillLinesAmountSumEqualsTotalMath(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : verify Line category amounts sum equal to total math");
		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("5: click 'view by line selector' | All account & Lines section should be displayed");
		Reporter.log("6: Verify Names for account and lines | Names should be displayed for all lines.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyAmountTotalInCurrentCharges();
		billAndPaySummaryPage.clickViewByLineSelector();
		billAndPaySummaryPage.verifyNamesOnAccountLines();
		billAndPaySummaryPage.verifyAmountTotalInCurrentCharges();
	}

	/**
	 * Bill Summary User can successfully navigate from Bill Summary to Bill Details
	 * page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, "billing" })
	public void testNavigationToBillDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("testNavigationToBillDetailsPage");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("5: Click on any category under charges | Bill details page should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		/*
		 * billAndPaySummaryPage.clickBalanceCategory(); PreviousChargePage
		 * previousChargePage = new PreviousChargePage(getDriver());
		 * previousChargePage.verifyPageLoaded();
		 * previousChargePage.clickBackToSummaryLink();
		 */

		billAndPaySummaryPage.verifyPageLoaded();
		billAndPaySummaryPage.clickChargeCategory("Plans");
		ChargePlanPage chargePlanPage = new ChargePlanPage(getDriver());
		chargePlanPage.verifyPageLoaded();
		chargePlanPage.clickBackToSummaryLink();

		billAndPaySummaryPage.verifyPageLoaded();
		billAndPaySummaryPage.clickChargeCategory("Equipment");
		ChargeEquipmentPage chargeEquipmentPage = new ChargeEquipmentPage(getDriver());
		chargeEquipmentPage.verifyPageLoaded();
		chargeEquipmentPage.clickBackToSummaryLink();

		billAndPaySummaryPage.verifyPageLoaded();
		billAndPaySummaryPage.clickChargeCategory("Services");
		ChargeFeaturesPage chargeFeaturesPage = new ChargeFeaturesPage(getDriver());
		chargeFeaturesPage.verifyPageLoaded();
		chargeFeaturesPage.clickBackToSummaryLink();

		billAndPaySummaryPage.verifyPageLoaded();
		billAndPaySummaryPage.clickChargeCategory("One-time charges");
		ChargeOneTimePage chargeOneTimePage = new ChargeOneTimePage(getDriver());
		chargeOneTimePage.verifyPageLoaded();
		chargeOneTimePage.clickBackToSummaryLink();

	}

	/**
	 * Test Brite Bill Charge Category Show More Details
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testBriteBillChargeCategoryShowMoreDetails(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Test Brite Bill Charge Category Show More Details");
		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("5: click 'view by line selector' | All account & Lines section should be displayed");
		Reporter.log("6: Verify Names for account and lines | Names should be displayed for all lines.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		// billAndPaySummaryPage.clickShowMoreDetails();
		billAndPaySummaryPage.verifyChargeCategoryDetails();
		billAndPaySummaryPage.clickShowMoreDetails();
		billAndPaySummaryPage.verifyChargeCategoryDetailsCollapsed();
	}

	/**
	 * Test Brite Bill bill history
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testBriteBillHistoricalBills(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Test Brite Bill Charge Category Show More Details");
		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("5: click 'View historical bills' | Historical bills widget should be displayed");
		Reporter.log("6: Select history bill date | Should be able to select the previous old date.");
		Reporter.log("7: Select old history bill date | old Bill summary page should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickPreviousBillCycle("Dec", "2017");
		BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
		billingSummaryPage.verifyPageLoaded();
	}

	/**
	 * Test Brite Bill bill view by line options
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testBriteBillViewByLineOptions(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Test Brite Bill Charge Category Show More Details");
		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("5: click 'View By line' | Should be able to select the View by line");
		Reporter.log("6: Click on Balance option | Previous charge page should be loaded.");
		Reporter.log("7: Click on Account | Charge account page should be loaded.");
		Reporter.log("8. Click on special test account|Charge special account should be loaded");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);

		billAndPaySummaryPage.clickViewByLineSelector();
		billAndPaySummaryPage.clickChargeCategory("Balance");
		PreviousChargePage previousChargePage = new PreviousChargePage(getDriver());
		previousChargePage.verifyPageLoaded();
		previousChargePage.clickBackToSummaryLink();

		billAndPaySummaryPage.verifyPageLoaded();
		billAndPaySummaryPage.clickViewByLineSelector();
		billAndPaySummaryPage.clickChargeCategory("Account");
		ChargeAccountPage chargeAccountPage = new ChargeAccountPage(getDriver());
		chargeAccountPage.verifyPageLoaded();
		chargeAccountPage.clickBackToSummaryLink();

	}

	/**
	 * Test Brite Bill bill PaperlessBilling
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testBriteBillPaperlessBilling(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Test Brite Bill Charge Category Show More Details");
		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("6: Click on Paperless Billing option | Paperless Billing page should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickPaperLessBillingShortCut();
		PaperlessBillingPage paperlessBillingPage = new PaperlessBillingPage(getDriver());
		paperlessBillingPage.verifyPageLoaded();
	}

	/**
	 * DE211533 MyTMO [Billing] Billing failure when multi-BAN TMOID exists
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testBillingPagewithSwitchAccountFunctionality(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :test Billing Page with Switch Account Functionality");
		Reporter.log("Data Condition - TMOID with lines from more than one BAN - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("5: click on switchaccount | choose-account page should be displayed");
		Reporter.log(
				"6: verify bill and pay page loaded for all accounts|bill and pay page should be loaded for all accounts");

		Reporter.log("================================");

		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		// billAndPaySummaryPage.clickSwitchAccount();
		billAndPaySummaryPage.verifyBillandpayPageLoadedforMultiAccounts();

	}

	/**
	 * Test Brite Bill bill PaperlessBilling
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, "billing" })
	public void testBriteBillBenefitsStatemnet(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Test Brite Bill Benefits statement");
		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("6: Click on Paperless Billing option | Paperless Billing page should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickBenefitsstatementShortCut();
		BenefitsStatementPage benefitsStatementPage = new BenefitsStatementPage(getDriver());
		benefitsStatementPage.verifyPageLoaded();
	}

	/**
	 * Test Brite Bill bill PaperlessBilling
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, "billing" })
	public void testMorepaymentOptionsonBillandPaypage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Test Brite Bill Benefits statement");
		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("6: Click on more payment options link | payment options should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickMorePaymentOptionsLink();
		billAndPaySummaryPage.verifyPaymentOptionsDispalyed();

	}

	/**
	 * DE236578-E1_Britebill Summary page_no space under the message and look is not
	 * matching the mock image*
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testBrightBillSummaryPageforNewUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on BILLING tab | Billing Summary page should be displayed");
		Reporter.log("Step 4: verify BB page for new user |BB page for new user should be verified");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		HomePage homePage = navigateToHomePage(myTmoData);
		// String billdueDate= homePage.getBilldueDate().replace("Due ", "").trim();
		homePage.clickBillingLink();
		BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
		billAndPaySummaryPage.verifyPageLoaded();
		long diffinDays = billAndPaySummaryPage.getDiffBetweendueDateandCurrentDate();
		billAndPaySummaryPage.verifyNoBillGeneratedMessageforNewUser(diffinDays + 5);

	}

	/**
	 * test Good To Know Sections on Bright BillPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testGoodToKnowSectionsonBrightBillPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :test Billing Page with Switch Account Functionality");
		Reporter.log("Data Condition - TMOID with lines from more than one BAN - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log(
				"5: verify good to know sections for autopay,usage,benefits | choose-account page should be displayed");

		Reporter.log("================================");

		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyGoodToKnowSectionsForUsage();
		billAndPaySummaryPage.verifyGoodToKnowSectionsForAutoPay();
		billAndPaySummaryPage.verifyGoodToKnowSectionsForBenefits();

	}

	/**
	 * Validate that informative/generic messaging (i.e. "Here is your March
	 * Bill...") should be displayed just beside the amount due section
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testHistoricalSummaryIntroduction(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on BILLING tab | Billing Summary page should be displayed");
		Reporter.log(
				"Step 4: verify historical summary introduction | historical summary introduction should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyHistoricalSummaryIntroduction();

	}

	/**
	 * Validate that "What changed this month?" section with related informational
	 * details are shown just below the View historical bills link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testWhatChangedinThisMonthSectiononBBpage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on BILLING tab | Billing Summary page should be displayed");
		Reporter.log(
				"Step 4: verify what changed in this month section | what changed in this month section  should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyWhatChangedinThisMonthSection();

	}

	/**
	 * Validate "Messaging/Notifications" live area that correct contents mesaging
	 * is shown when <accounts is new with AutoPay OFF and FIRST bill is generated>
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testLiveAreamessagingWhenAutoPayOFF(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on BILLING tab | Billing Summary page should be displayed");
		Reporter.log(
				"Step 4: verify live Area Messaging when Autopay is OFF |  live Area Messaging when Autopay is OFF  should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyLiveAreaMessagingwhenAutopayOFF();
		billAndPaySummaryPage.clickAutopaySignupLinkonLiveAreaMessaging();
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyPageLoaded();

	}

	/**
	 * Validate on BriteBill Interactive Summary Page Options (Show more/less
	 * details below) magenta link that "View by category" button is preselected by
	 * default
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testViewBycategorySelectedByDefault(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("5: verify View by category selected by default | View by category should be selected by default");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyViewbycategorySelectedBydefault();
	}

	/**
	 * Validate that associated line/s on the account is shown/displayed when "View
	 * by line" button is clicked on the Interactive Summary Page Options section
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testAssociatedLineDisplayedinInteractiveSummaryOptions(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : test Associated Line Displayed in View By Line");
		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log("6: Click on view by line link| view by line section should be displayed.");
		Reporter.log(
				"7: verify Associated line is dispalyed on view by line section| Associated line should be dispalyed on view by line section.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickViewByLineSelector();
		billAndPaySummaryPage.verifyAssociatedLineDisplayedonInteractiveSummaryOptions(myTmoData);

	}

	/**
	 * Validate "Make a payment" CTA button have the correct contents when <the
	 * total balance is more than $0.00, the primary style button (magenta) will
	 * display>
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testMakePaymentCTAActiveforPastDueCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : test Associated Line Displayed in View By Line");
		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log(
				"6: verify make payment button active for pastdue customer| make payment button should be active for pastdue customer");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyMakePaymentButtonActiveforPastDueCustomer();

	}

	/**
	 * Validate "Make a payment" button <when the total balance is $0.00 or less or
	 * an AutoPay is scheduled, the secondary style button (white) will display>
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testMakePaymentCTAInActiveforCustomerWithzeroBalance(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : test Associated Line Displayed in View By Line");
		Reporter.log("Data Condition - Regular account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log(
				"6: verify make payment button inactive for customer with zero balance| make payment button should be inactive for customer with zero balance");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyMakePaymentButtonInActiveforZeroBalanceCustomer();

	}

	/**
	 * Validate "Messaging/Notifications" live area that correct contents mesaging
	 * is shown when <accounts is new with AutoPay ON and FIRST bill is generated>
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testliveAreMessagewhenAutopayOnandFirstBillGenerated(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on BILLING tab | Billing Summary page should be displayed");
		Reporter.log(
				"Step 4: verify live Area Message when Autopay On and First Bill Generated |live Area Message when Autopay On and First Bill Generated   should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyLiveAreaMessagingwhenAutopayON();

	}

	/**
	 * Validate "Total balance" section have the correct contents and messaging
	 * details when <total balance is less than $0.00>
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testTotalBalanceSectionwhenBalanceLessthanZero(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on BILLING tab | Billing Summary page should be displayed");
		Reporter.log(
				"Step 4: verify Total Balance Section when Balance Less than Zero|Total Balance Section when Balance Less than Zero   should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyTotalBalanceSectionwhenBalanceLessthanZero();

	}

	/**
	 * Validate "Due date" section have the correct contents and messaging details
	 * when <the ‘total balance’ is more than $0.00 and there is an amount due
	 * immediately>
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testDueDateSectionwhenBalanceMorethanZeroDueimmediately(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on BILLING tab | Billing Summary page should be displayed");
		Reporter.log(
				"Step 4: verify \"Due date\" section when Balance more than Zero due immediately|Due date section  should be verified");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyDueDateSectionwhenBalanceMorethanZeroDueimmediately();

	}

	/**
	 * Validate "AutoPay" section have the correct contents and messaging details
	 * when <AutoPay is scheduled>
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testVerifyAutopaySectionwhenAutopayScheduled(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log("Step 4: verify autopay on alert | autopay on alert   should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.verifyAutopaySectionwhenAutopayscheduled();

	}
}