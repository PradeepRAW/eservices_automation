package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.accounts.CoverageDevicePage;
import com.tmobile.eservices.qa.pages.global.SignalBoosterLinkPage;

public class SignalBoosterLinkPageTest extends GlobalCommonLib{
	
	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PENDING})
	public void verify4GLTESignalBoosterLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with coveragedevice blade");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on Coverage Device link| Coverage Device should be displayed");
		Reporter.log("6. Click on 4GLTESignalBooster link| SignalBoosterLink page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		CoverageDevicePage coverageDevicePage=navigateToCoverageDevicePage(myTmoData,"Coverage Device");
		coverageDevicePage.click4GLTESignalBoosterLink();
		coverageDevicePage.switchToWindow();
		
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();
		
		SignalBoosterLinkPage signalBoosterLinkPage = new SignalBoosterLinkPage(getDriver());
		signalBoosterLinkPage.verifySignalBoosterLinkPage();
	}

}
