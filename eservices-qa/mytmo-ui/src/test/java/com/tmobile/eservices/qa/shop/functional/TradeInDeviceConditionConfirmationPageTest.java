/**
 * 
 */
package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.InterstitialTradeInPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.PhoneSelectionPage;
import com.tmobile.eservices.qa.pages.shop.TradeInAnotherDevicePage;
import com.tmobile.eservices.qa.pages.shop.TradeInConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionValuePage;
import com.tmobile.eservices.qa.pages.shop.TradeinDeviceConditionPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import com.tmobile.eservices.qa.shop.ShopConstants;

/**
 * @author charshavardhana
 *
 */
public class TradeInDeviceConditionConfirmationPageTest extends ShopCommonLib {

	/**
	 * US158299 - [MOCK] Standard Trade-in - 'Agree & Continue' CTA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testAgreeAndContinueButtonInStandardTradeInFlow(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : Verify Trade-In value page");
		Reporter.log("Data Condition | IR PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
		Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
		Reporter.log("4. Click on the device on Device PLP page |Device PDP page should be displayed ");
		Reporter.log("5. Select EIP from payment option and click on Add to cart after verify the installment due amount  | Line Selector page should be displayed");
		Reporter.log("6. Select additional user device on LS page | Trade in Phone Selection page should be displayed");
		Reporter.log("7. Click on Trade in another device link | Trade in another device page should be displayed");
		Reporter.log("8. Select trade-in device info & Click on continue button | Device condition page should be displayed");
		Reporter.log("9. Select it's good radio button & click continue |Trade-in value page should be displayed");
		Reporter.log("10. Verify agree & continue button | Agree & continue button should be displayed");
		Reporter.log("11. Click agree & continue button | Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage=navigateToTradeInDeviceConditionConfirmationPageBySeeAllPhones(myTmoData);		
		tradeInDeviceConditionConfirmationPage.verifyAgreeAndContinueButton();
		tradeInDeviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		
	}	
		
	/**
	 * US552115 :Device Condition - Update conditions questions to toggles (std
	 * upgrade)
	 *  US552116 :Device Condition - allow Toggle (yes/no)
	 *  	 * US569445: TEST ONLY -  Device Value page	
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION})
	public void testCoditionsQuestionsForAALFlowInTradeInConditionPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US552115 :Device Condition - Update conditions questions to toggles (std upgrade)");
		Reporter.log("Data Condition | PAH User with AAL Eligible");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click Contune button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("8. Select Payment option as EIP | Payment option should be selected ");
		Reporter.log("9. Click Add to cart button | Interstitial page should be displayed ");
		Reporter.log("10. Verify Trade-In tile 'Yes, I want to trade in a device' | Trade-In tile should be displayed");
		Reporter.log(
				"11. Select Carrier, make, model,enter valid IMEI number & click continue | TradeInDevice condition Value page should be displayed");
		Reporter.log(
				"12. Verify Authorable title as 'Tell us about the condition of your old device:' | Authorable title should be displayed");
		Reporter.log(
				"13. Verify assured questions No and Yes boxes | Assured questions No and Yes boxes should be displayed");
		Reporter.log("14. Verify 'Not sure what to select' CTA | 'Not sure what to select' CTA should be displayed");
		Reporter.log("15. Verify Battery question | Battery question should be displayed");
		Reporter.log("16. Verify Water drop question | Water drop question should be displayed");
		Reporter.log("17. Verify Cracked screen question | Cracked screen question should be displayed");
		Reporter.log("18. Verify Location question | Location question should be displayed");
		Reporter.log("19. Verify Continue button | Continue button should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialPageTitle();
		interstitialTradeInPage.verifyAuthorableTradeInImage();
		interstitialTradeInPage.verifyYesTradeinTile();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		deviceConditionsPage.titleTextDisplayed();
		deviceConditionsPage.verifyAssuredQuestionsDisplayed();
		deviceConditionsPage.verifyAssuredQuestionsYesNoDisplayed();
		deviceConditionsPage.verifyNotSureWhatToSelectHyperLink();
		deviceConditionsPage.verifyBatteryQuestionDisplayed();
		deviceConditionsPage.verifyWaterDropQuestionDisplayed();
		deviceConditionsPage.verifyCrackedScreenQuestionDisplay();
		deviceConditionsPage.verifyLocationQuestionDisplay();
		deviceConditionsPage.verifyNewContinueButton();
		deviceConditionsPage.clickFindMyIphoneYesButton();
		deviceConditionsPage.clickAcceptableLCDYesButton();
		deviceConditionsPage.clickWaterDamageNoButton();
		deviceConditionsPage.clickDevicePowerONYesButton();
		deviceConditionsPage.clickContinueCTANew();				
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInDeviceConditionConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();
		TradeInConfirmationPage tradeInConfirmationPage = new TradeInConfirmationPage(getDriver());
		tradeInConfirmationPage.verifyEligibilityMessage();
	}

	/**
	 * US552115 :Device Condition - Update conditions questions to toggles (std
	 * upgrade)
	 *  US552116 :Device Condition - allow Toggle (yes/no)
	 *  US552119 :Device Condition - Send selections to value page via Continue CTA
	 * (std upgrade)
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testCoditionsQuestionsForStandardUpgradeFlowInTradeInConditionPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case :US552115 :Device Condition - Update conditions questions to toggles (std upgrade)");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones link | PLP page should be displayed");
		Reporter.log("6. Select Device | PDP Page  should be displayed");
		Reporter.log("7. Select Payment option as EIP | Payment option should be selected ");
		Reporter.log("8. Click Add to cart button | LineSelector page should be displayed ");
		Reporter.log("9. Select a line | LineSelector details page should be displayed");
		Reporter.log("10. Select 'Want to trade in a different phone?' | Phoneslection page should be displayed");
		Reporter.log("11. Select device | TradeInDeviceCondition page should be displayed");
		Reporter.log("12. Select Good device | TradeInDeviceCondition value confirmation page should be displayed");
		Reporter.log("13. Verify Authorable title as 'Tell us about the condition of your old device:' | Authorable title should be displayed");
		Reporter.log("14. Verify assured questions No and Yes boxes | Assured questions No and Yes boxes should be displayed");
		Reporter.log("15. Verify 'Not sure what to select' CTA | 'Not sure what to select' CTA should be displayed");
		Reporter.log("16. Verify Battery question | Battery question should be displayed");
		Reporter.log("17. Verify Water drop question | Water drop question should be displayed");
		Reporter.log("18. Verify Cracked screen question | Cracked screen question should be displayed");
		Reporter.log("19. Verify Location question | Location question should be displayed");
		Reporter.log("20. Verify Continue button | Continue button should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PhoneSelectionPage phoneSelectionPage=navigateToWantToTradeDifferentPhoneLinkBySeeAllPhones(myTmoData);
		phoneSelectionPage.selectDevice();		
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(getDriver());
		tradeInDeviceConditionValuePage.titleTextDisplayed();
		tradeInDeviceConditionValuePage.verifyAssuredQuestionsDisplayed();
		tradeInDeviceConditionValuePage.verifyAssuredQuestionsYesNoDisplayed();
		tradeInDeviceConditionValuePage.verifyNotSureWhatToSelectHyperLink();
		tradeInDeviceConditionValuePage.verifyNewContinueButton();
		tradeInDeviceConditionValuePage.verifyBatteryQuestionDisplayed();
		tradeInDeviceConditionValuePage.verifyWaterDropQuestionDisplayed();
		tradeInDeviceConditionValuePage.verifyCrackedScreenQuestionDisplay();
		tradeInDeviceConditionValuePage.verifyLocationQuestionDisplay();
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDNoButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInDeviceConditionConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();		
		TradeInConfirmationPage tradeInConfirmationPage = new TradeInConfirmationPage(getDriver());		
		tradeInConfirmationPage.verifyInEligibilityMessage();
	}
	/**
	 * US327662: SSU MyTMO > Trade in Promo > All phonetestBadConditionMessageInTradeInDeviceConditionValueConfirmationPageOneNegativeConditions
	 * s > Trade in Value Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPromoCustomerTradeInPromoPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US327662: Customer Qualifies For A Trade In value Promo");
		Reporter.log("Data Condition | STD PAH Customer, device should be eligible for trade in promo");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones | Product list page should be displayed");
		Reporter.log("6. Select Device which has Trade in Promo | Product details page should be displayed");
		Reporter.log("7. Click on Add to cart button |line Selection page should be displayed");
		Reporter.log("8. In line Selection page click on first line | Phone selection page should be displayed");
		Reporter.log(
				"9. In Phone selection page click on Got a diffrent phone | Trade In Another Device page should be displayed");
		Reporter.log(
				"10. In Trade In Another Device page Select Carrier,Make,Model And enter IMEI | Details should be entered");
		Reporter.log(
				"11. Click on contiune button in Trade In Another Device page | Trade In Device Condition should be displayed");
		Reporter.log(
				"12. In Trade In Device Condition page select Its good radio button and click on contiune button | Trade in value page should be displayed");
		Reporter.log("13. Verify Device is qualified for Trade In promo | Device is qualified for Trade In promo");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TradeInAnotherDevicePage tradeInAnotherDevicePage = navigateToTradeInAnotherDevicePageBySeeAllPhones(myTmoData);
		tradeInAnotherDevicePage.selectCarrier(ShopConstants.CARRIER_DROPDOWN_OPTION_TMOBILE);
		tradeInAnotherDevicePage.selectMake(ShopConstants.SAMSUNG_MAKE_DROPDOWN_OPTION);
		tradeInAnotherDevicePage.selectModel(ShopConstants.SAMSUNG_MODEL_DROPDOWN_OPTION);
		tradeInAnotherDevicePage.setIMEINumber(ShopConstants.EIP_IMEI);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		deviceConditionsPage.clickItsGoodRadioBtn();
		deviceConditionsPage.clickOnItsGoodCondition();
		TradeInDeviceConditionConfirmationPage tradeInValuePage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInValuePage.verifyTradeInDeviceConditionConfirmationPage();
		tradeInValuePage.verifyTickMarkForTradeInpromoDeviceQualified();
		// approved by sam

	}

	/**
	 * US327697: SSU My TMO > Trade In Promo > Featured phones > Trade in value page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testFeaturedDeviceInCustomerTradeInPromoPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US327697: Verify Featured Device In Customer Trade In Page");
		Reporter.log("Data Condition | STD PAH Customer, trade-in device is eligible for promotion");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Featured device  | Product details page should be displayed");
		Reporter.log("6. Click on Add to cart button |line Selection page should be displayed");
		Reporter.log("7. In line Selection page click on first line | Phone selection page should be displayed");
		Reporter.log(
				"8. In Phone selection page click on Got a diffrent phone | Trade In Another Device page should be displayed");
		Reporter.log(
				"9. In Trade In Another Device page Select Carrier,Make,Model And enter IMEI | Details should be entered");
		Reporter.log(
				"10. Click on contiune button in Trade In Another Device page | Trade In Device Condition should be displayed");
		Reporter.log(
				"11. In Trade In Device Condition page select Its good radio button and click on contiune button | Trade in value page should be displayed");
		Reporter.log("12. Verify Device is qualified for Trade In promo | Device is qualified for Trade In promo");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TradeInAnotherDevicePage tradeInAnotherDevicePage = navigateToTradeInAnotherDevicePageByFeaturedDevicesFLow(
				myTmoData, "T-Mobile® REVVL® PLUS");
		tradeInAnotherDevicePage.selectCarrier(ShopConstants.CARRIER_DROPDOWN_OPTION_TMOBILE);
		tradeInAnotherDevicePage.selectMake(ShopConstants.SAMSUNG_MAKE_DROPDOWN_OPTION);
		tradeInAnotherDevicePage.selectModel(ShopConstants.SAMSUNG_MODEL_DROPDOWN_OPTION);
		tradeInAnotherDevicePage.setIMEINumber(ShopConstants.EIP_IMEI);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		deviceConditionsPage.clickItsGoodRadioBtn();
		deviceConditionsPage.clickOnItsGoodCondition();
		TradeInDeviceConditionConfirmationPage tradeInValuePage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInValuePage.verifyTradeInDeviceConditionConfirmationPage();
		tradeInValuePage.verifyTickMarkForTradeInpromoDeviceQualified();
		// approved by sam
	}

	/**
	 * US327708: SSU MyTMO > Trade In Promo > Promo Banner > Trade in value page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPromoBannerInCustomerTradeInValuePromoPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US327708: Verify Promo Banner In Customer Trade In Value Page");
		Reporter.log("Data Condition | PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Promo Banner device  | Verify Product details page should be displayed");
		Reporter.log("6. Click on Add to cart button |line Selection page should be displayed");
		Reporter.log("7. In line Selection page click on first line | Phone selection page should be displayed");
		Reporter.log(
				"8. In Phone selection page click on Got a diffrent phone | Trade In Another Device page should be displayed");
		Reporter.log(
				"9. In Trade In Another Device page Select Carrier,Make,Model And enter IMEI | Details should be entered");
		Reporter.log(
				"10. Click on contiune button in Trade In Another Device page | Trade In Device Condition should be displayed");
		Reporter.log(
				"11. In Trade In Device Condition page select Its good radio button and click on contiune button | Trade in value page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TradeInDeviceConditionConfirmationPage tradeInValuePage = navigateToTradeInValuePageThroughPromoBanner(
				myTmoData);
		tradeInValuePage.verifyTickMarkForTradeInpromoDeviceQualified();
		// approved by sam
	}

	/**
	 * US354204 MyTMO > Trade - in Redesign > Device condition > Great condition
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testReDesignDeviceconditionGoodCondition(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US354204 MyTMO > Trade - in Redesign > Device condition > Great condition");
		Reporter.log("Data Condition |STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select Monthly payments | Monthly payments is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Click on First Device | Phone selection page should be displayed");
		Reporter.log("10. Click on Got A Different Phone | Trade-In another device page should be displayed");
		Reporter.log("11. Select trade-in info & click continue |  Device condition page should be displayed");
		Reporter.log("12. select 'Good condition' |  Trade-In value page should be displayed");
		Reporter.log(
				"13. verify 'Good condition' image on Trade-In value page|  'Good condition' image should be displayed on Trade-In value page");
		Reporter.log(
				"14. verify  trade in value message with bill credit amount ('Yes!! You're qualified for the $X bill credit for your old phone'). |  trade in value message with bill credit amount('Yes!! You're qualified for the $X bill credit for your old phone') should be displayed");
		Reporter.log(
				"15. verify  trade in value description 'This value will how up as bill credits for 24 months. How do you want to proceed?' | trade in value description 'This value will how up as bill credits for 24 months. How do you want to proceed?' should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = navigateToTradeInDeviceConditionConfirmationPageBySeeAllPhones(
				myTmoData);
		tradeInDeviceConditionConfirmationPage.verifyDeviceConditionImage();
		tradeInDeviceConditionConfirmationPage.verifyEligibilityMessage();
		tradeInDeviceConditionConfirmationPage.verifyMessageDescription();
	}


	/**
	 * US372226: myTMO > Trade in Condition page > Display Promo
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testTradeInValuePagePromo(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case US372226: test Trade in Condition page Promo");
		Reporter.log("Data Condition |STD PAH Eligible Promo Customer");
		Reporter.log("================================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on shop link | Shop page should be displayed");
		Reporter.log("4. Click on see all phones | Product list page should be displayed");
		Reporter.log("5. Select device | Product details page should be displayed");
		Reporter.log("6. Select Monthly payments | Monthly payments is selected");
		Reporter.log("7. Click add to cart button | Line selection page should be displayed");
		Reporter.log("8. Click on First Device | Phone selection page should be displayed");
		Reporter.log("9. Click on Got A Different Phone | Trade-In another device page should be displayed");
		Reporter.log("10. Select Great Condition | Trade-In Value page should be displayed");
		Reporter.log(
				"11. Verify Eligible message Promo message | 'Thanks. You're eligible for a $400.00 trade in promo' should be displayed");
		Reporter.log(
				"12. Verify message description | 'This value will show up as bill credits for 24 months How do you want to proceed' should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = navigateToTradeInDeviceConditionConfirmationPageBySeeAllPhones(
				myTmoData);
		tradeInDeviceConditionConfirmationPage.verifyEligibilityMessage();
		tradeInDeviceConditionConfirmationPage.verifyMessageDescription();
	}

	/**
	 * US407217 Trade in Redesign > Standard Upgrade > No Promo > No EIP Balance
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION})
	public void testTradeInDescriptionForNoPromoDeviceWithNoEIPBalanceInTradeInValuePage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test case US407217:Trade in Redesign > Standard Upgrade >No Promo >No EIP Balance");
		Reporter.log("Data Condition |Standard Upgrade Customer With No Promo and EIP Balance");
		Reporter.log("================================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on shop link | Shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select device | PDP page should be displayed");
		Reporter.log("6. Click add to cart button | Line selection page should be displayed");
		Reporter.log("7. Select a line | Line selector details page should be displayed");
		Reporter.log("8. Verify line details message header | Line details message header should be displayed");
		Reporter.log("9. Click trade in this phone cta | Device condition page should be displayed");
		Reporter.log("10.Select good condition divison | Trade in value confirmation page should be displayed");
		Reporter.log(
				"11.Verify trade in value description header | Trade in value description header should be displayed");
		Reporter.log(
				"12.Verify trade in value bulletin format divison | Trade in value bulletin format divison should be displayed");
		Reporter.log(
				"13.Evaluate trade in value from line selector details page and trade in value confirmation page | Trade in value should be same in both pages");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		String lineSelectorDetailsPageTradeInValue = lineSelectorDetailsPage.getLineDetailsMessageHeader();
		lineSelectorDetailsPage.clickTradeInThisPhoneCTA();
		TradeinDeviceConditionPage tradeinDeviceConditionPage = new TradeinDeviceConditionPage(getDriver());
		tradeinDeviceConditionPage.verifyTradeInDeviceCondition();
		tradeinDeviceConditionPage.clickOnGoodCondition();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.verifyTradeInValueInDescription();
		tradeInDeviceConditionValuePage.verifyTradeInValueBulletinFormatDiv();
		tradeInDeviceConditionValuePage.verifyTradeInValue(lineSelectorDetailsPageTradeInValue);
	}

	/**
	 * US407218 Trade in Redesign > Standard Upgrade > No Promo > EIP Balance
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testTradeInDescriptionForNoPromoDeviceWithEIPBalanceInTradeInValuePage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test case US407218:Trade in Redesign > Standard Upgrade >No Promo >EIP Balance");
		Reporter.log("Data Condition |STD PAH Customer With EIP Balance and No Promo");
		Reporter.log("================================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on shop link | Shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select device | PDP page should be displayed");
		Reporter.log("6. Click add to cart button | Line selection page should be displayed");
		Reporter.log("7. Select a line With EIP Balance | Line Details page should be displayed");
		Reporter.log(
				"8. Verify Selected line which is displays trade in value in the description | Selected Line with trade in value details should be displayed");
		Reporter.log("9. Verify Bulleted format description | bullet format description should be displayed");
		Reporter.log("10. Click 'Yes, I Want to trade in'  button |  Device condition page should be displayed");
		Reporter.log("11. Select 'Good condition' |  Trade-In value page should be displayed");
		Reporter.log("12. Verify EIP balance in description | EIP balance in description should be displayed");
		Reporter.log("13.Verify trade in value in description | Trade in value in description should be displayed");
		Reporter.log("14. Verify Bulleted format description | bullet format description should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorPage newLineSelectorPage = navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		newLineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());

		LineSelectorDetailsPage newLineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		newLineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		newLineSelectorDetailsPage.verifyTradeInValueWithDollarSymbol();
		newLineSelectorDetailsPage.verifyTradeInDescription();
		newLineSelectorDetailsPage.verifyTradeINButtonText();
		newLineSelectorDetailsPage.clickTradeInThisPhoneCTA();

		TradeinDeviceConditionPage tradeinDeviceConditionPage = new TradeinDeviceConditionPage(getDriver());
		tradeinDeviceConditionPage.verifyTradeInDeviceCondition();
		tradeinDeviceConditionPage.clickOnGoodCondition();

		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.verifyEIPBalance();
		tradeInDeviceConditionValuePage.verifyTradeInValueInDescription();
		tradeInDeviceConditionValuePage.verifyTradeInValueBulletinFormatDiv();
	}


	/**
	 * US488235 :AAL: Trade In Value - Build Base Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_REGRESSION })
	public void testDeviceGoodConditionPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US488235 :AAL: Trade In Value - Build Base Page");
		Reporter.log("Data Condition | PAH User with AAL Eligible");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click Contune button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Click Contune button | Interstitial page should be displayed ");
		Reporter.log("10. Verify Authorable Image | Authorable Image should be displayed ");
		Reporter.log("11. Verify Trade-In tile 'Yes, I want to trade in a device' | Trade-In tile should be displayed");
		Reporter.log(
				"12. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
		Reporter.log("13. Verify Authorable title | Authorable title  should be displayed");
		Reporter.log("14. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
		Reporter.log("15. Click on Continue button | Device condition page should be displayed");
		Reporter.log(
				"16. Verify good condition authorable title | Good condition authorable title should be displayed");
		Reporter.log("17. Select Good condition option | Device condition value page should be displayed");
		Reporter.log("18. Verify 'Skip trade-in' button | 'Skip trade-in' button should be displayed");
		Reporter.log("19. Verify 'Trade in this phone' button | 'Trade in this phone' should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialPageTitle();
		interstitialTradeInPage.verifyAuthorableTradeInImage();
		interstitialTradeInPage.verifyYesTradeinTile();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();

		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();

		TradeInDeviceConditionConfirmationPage tradeInValuePageConfirmation = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInValuePageConfirmation.verifyTradeInDeviceConditionConfirmationPage();
		tradeInValuePageConfirmation.verifySkipTradeInButton();
		tradeInValuePageConfirmation.verifyAgreeAndContinueButton();
	}

	/**
	 * US488235 :AAL: Trade In Value - Build Base Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_REGRESSION })
	public void testDeviceBadConditionPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US488235 :AAL: Trade In Value - Build Base Page");
		Reporter.log("Data Condition | PAH User with AAL Eligible");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click Contune button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Click Contune button | Interstitial page should be displayed ");
		Reporter.log("10. Verify Authorable Image | Authorable Image should be displayed ");
		Reporter.log("11. Verify Trade-In tile 'Yes, I want to trade in a device' | Trade-In tile should be displayed");
		Reporter.log(
				"12. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
		Reporter.log("13. Verify Authorable title | Authorable title  should be displayed");
		Reporter.log("14. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
		Reporter.log("15. Click on Continue button | Device condition page should be displayed");
		Reporter.log("16. Verify bad condition authorable title | Bad condition authorable title should be displayed");
		Reporter.log("17. Select Bad condition option | Device condition value page should be displayed");
		Reporter.log("18. Verify 'Skip trade-in' button | 'Skip trade-in' button should be displayed");
		Reporter.log("19. Verify 'Recycle with us' button | 'Recycle with us' should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialPageTitle();
		interstitialTradeInPage.verifyAuthorableTradeInImage();
		interstitialTradeInPage.verifyYesTradeinTile();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		deviceConditionsPage.clickDeviceConditionAsItsBad();
		deviceConditionsPage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInValuePageConfirmation = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInValuePageConfirmation.verifyTradeInDeviceConditionConfirmationPage();
		tradeInValuePageConfirmation.verifySkipTradeInButton();
		tradeInValuePageConfirmation.verifyRecycleWithUsCTADisplayed();
	}


	/**
	 * US575868 Device Condition - Trade in Value Page w/ Promo
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testDeviceConditionPageWithOutTradeInValuePromoForAALFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US575868 Device Condition - Trade in Value Page w/ Promo");
		Reporter.log("Data Condition | PAH User with no trade in value promo");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line link from quicklinks| Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click Continue button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Click Add to cart button | Interstitial page should be displayed ");
		Reporter.log("10. Click on Trade-In tile 'Yes, I want to trade in a device' | Trade-In another device page should be displayed");
		Reporter.log("11. Select Carrier, make, model,enter valid IMEI number & click continue | Trade-In condition page should be displayed");
		Reporter.log("12. Answer any 3 questions as 'Good' and remaining one as 'Bad' and click Continue CTA | Trade-in device condition value confirmation page should be displayed ");
		Reporter.log("13. Verify Trade In message | User eligible for Trade In price = $0 should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialPageTitle();
		interstitialTradeInPage.verifyAuthorableTradeInImage();
		interstitialTradeInPage.verifyYesTradeinTile();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		deviceConditionsPage.clickFindMyIphoneNoButton();
		deviceConditionsPage.clickAcceptableLCDNoButton();
		deviceConditionsPage.clickWaterDamageYesButton();
		deviceConditionsPage.clickDevicePowerONNoButton();
		deviceConditionsPage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInDeviceConditionConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();	
		TradeInConfirmationPage tradeInConfirmationPage = new TradeInConfirmationPage(getDriver());		
		tradeInConfirmationPage.verifyDollarZeroTradeinMessage();

	}
	
	/**
	 * US575868 Device Condition - Trade in Value Page w/ Promo
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testDeviceConditionPageWithFMVTradeInValuePromoForUpgradeFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US575868 Device Condition - Trade in Value Page w/ Promo");
		Reporter.log("Data Condition | PAH User with FMV Trade in Value promo");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on See AllPhones cta | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Click on Upgrade button | Line Selector page should be displayed ");
		Reporter.log("10. Select a Line | Line Selector details page should be displayed ");
		Reporter.log("11. Click on Trade in this phone cta | Trade In Device Condition value page should be displayed ");
		Reporter.log("12. Answer any 3 questions as 'Good' and remaining one as 'Bad' and click Continue CTA | Trade-in device condition value confirmation page should be displayed ");
		Reporter.log("13. Verify Trade In Success message | User eligible for FMV Trade In price > $0 should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = navigateToTradeInDeviceConditionValuePageBySeeAllPhones(
				myTmoData);
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDNoButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();		
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInDeviceConditionConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();		
		TradeInConfirmationPage tradeInConfirmationPage = new TradeInConfirmationPage(getDriver());		
		tradeInConfirmationPage.verifyPartialEligibilityMessage();

	}

}
