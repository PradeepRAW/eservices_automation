package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.global.GlobalConstants;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.StoreLocatorPage;

public class StoreLocatorPageTest extends GlobalCommonLib{
	
	/**
	 * Ensure user can select a result from the search results, and is directed to the appropriate page. 
	 * Regression_Test_R144
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = "Search")
	public void searchAndRedirectToPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("searchAndRedirectToPage");
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.enterSearchData(GlobalConstants.SEARCH_TEXT_DATA_1);

		homePage.getSupportTab();
		homePage.getYourSelectionTextSupport(GlobalConstants.SEARCH_TEXT_DATA_1);
		homePage.getSupportPagination();

		homePage.clickDevicesTab();
		homePage.getDevicesTab();
		homePage.selectAnyDevice();
		homePage.switchToWindow();
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreLocatorPage();
	}
	
	/**
	 * TC006_MYTMO_Cloud_Footer_Verify the store locator link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID})
	public void verifyStoreLocator(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyStoreLocator method");
		Reporter.log("Test Case : MYTMO_Cloud_Footer_Verify the store locator link");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on footer store locator link | store locator link should be clicked");
		Reporter.log("5. Verify store locator page | store locator page should be displayed");		
		
		Reporter.log("=======================================");
		Reporter.log("Actual Result:");
		
		NewHomePage homePage=navigateToNewHomePage(myTmoData);
		homePage.clickFooterStoreLocatorlink();		
		homePage.switchToWindow();
		
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();
		
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreLocatorPage();
	}

}
