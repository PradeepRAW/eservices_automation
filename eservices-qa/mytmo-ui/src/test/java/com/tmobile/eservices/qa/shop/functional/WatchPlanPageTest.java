package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.DeviceIntentPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.shop.UNOWatchPDPPage;
import com.tmobile.eservices.qa.pages.shop.UNOWatchPLPPage;
import com.tmobile.eservices.qa.pages.shop.WatchPlanPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class WatchPlanPageTest extends ShopCommonLib {

	/**
	 * US544744: Display Line Selector title and lines below cost details CDCSM-2 :
	 * Display Line Selector title and lines below cost details TC-1:
	 * CDCSM-2:Validate the Line selector title on consolidated charge page should
	 * display under rate plan tile TC-3: CDCSM-2:Verify that the authorable text on
	 * consolidated charge page display as per the copy deck attached in the user
	 * story TC-4: CDCSM-2 Verify the display of radio button selections, Line Name,
	 * and Number on consolidated charge page TC-5: CDCSM-2: Verify the Full logged
	 * in customer is by default selected on consolidated charge page TC-7: CDCSM-2:
	 * Verify the user name is the nick name of the user which is displaying under
	 * line selection TC-9: CDCSM-2: Verify the PAH logged in customer is by default
	 * selected on consolidated charge page TC-18: CDCSM-4: Validate the line
	 * selector section on wearable version of consolidated charge page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testLineSelectorSectionAuthorableTextInRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US544744: Display Line Selector title and lines below cost details");
		Reporter.log("Data Condition | User should have AAL eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Click on Wearable tile | Wearable PLP page should be displayed");
		Reporter.log("5. Slelect wearable device | Wearable PDP page should be displayed");
		Reporter.log("6. Click on Continue button | Consolidated rate plan should be displayed");
		Reporter.log(
				"7. Verify LineSection section for wearables in Rate plan page | LineSection section for wearables should be displayed in rate plan page");
		Reporter.log(
				"8. Verify The text on Consolidated charge page | LineSection section authorable text 'Select the phone number you'd like to use on your watch.' should be displayed in rate plan page");
		Reporter.log(
				"9. Verify LineSection section Radio buttons, LineName and PhoneNumbe on rate plan page | LineSection section Radio buttons, LineName and PhoneNumbe should be displayed in rate plan page");
		Reporter.log("10. Verify Radio button for logged user | By default radio button should be selected");
		Reporter.log("11. Verify user name | User name should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		String name = shopPage.getUserName();
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyAWatch();
		UNOWatchPLPPage unoWatchPLPPage = new UNOWatchPLPPage(getDriver());
		unoWatchPLPPage.verifyWatchesPageLoaded();
		unoWatchPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOWatchPDPPage unoWatchPDPPage = new UNOWatchPDPPage(getDriver());
		unoWatchPDPPage.verifyWatchesPDPPageLoaded();
		unoWatchPDPPage.clickOnAddToCartCTA();
		WatchPlanPage watchPlanPage = new WatchPlanPage(getDriver());
		watchPlanPage.verifyWatchPlanPage();
		watchPlanPage.verifyLineSectionForWearables();
		watchPlanPage.verifyLineSectionAuthorableText();
		watchPlanPage.verifyLineSectionRadioBtnNameAndPhoneNumber();
		watchPlanPage.verifyRadioButtonForLoggerUserSelected(myTmoData.getLoginEmailOrPhone());
		watchPlanPage.verifyUserNameForLoggedUser(name);

	}

	/**
	 * US545337: Implement new HTML: Image, Header and Description and costs and
	 * line selector CDCSM-4 : Implement new HTML: Image, Header and Description and
	 * costs and line selector TC-16: CDCSM-4: Validate the device image section on
	 * wearable version of consolidated charge page TC-17: CDCSM-4: Validate the
	 * Rate plan section on wearable version of consolidated charge page TC-19:
	 * CDCSM-4: Validate the "Add plan and link number" CTA on wearable version of
	 * consolidated charge page TC-44: CDCSM-4: Validate the deposit section on
	 * wearable version of consolidated charge page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testLineSelectorSectionDeviceImageAndRatePlanSectionInRatePlanPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case: US544744: Display Line Selector title and lines below cost details");
		Reporter.log("Data Condition | User should have AAL eligible and user should have deposit amount");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Click on Wearable tile | Wearable PLP page should be displayed");
		Reporter.log("5. Select wearable device | Wearable PDP page should be displayed");
		Reporter.log("6. Click on Continue button | Consolidated rate plan should be displayed");
		Reporter.log("7. Verify the image | Image should be displayed");
		Reporter.log("8. Verify the Rate plan section | Rate plan section should be displayed");
		Reporter.log("9. Verify 'DUE MONTHLY' text  | 'DUE MONTHLY' text should be displayed");
		Reporter.log("10. Verify 'DUE MONTHLY' price  | 'DUE MONTHLY' price should be displayed");
		Reporter.log(
				"11. Verify If there is a deposit, 'DUE TODAY' text and price  | If there is a deposit, 'DUE TODAY' text and price should be displayed");
		Reporter.log("12. Verify 'Add plan and link number' CTA  | 'Add plan and link number' CTA should be displayed");
		// Reporter.log("13. Click on 'Add plan and link number' CTA | Trade-in page
		// should be displayed");
		Reporter.log("14. Verify deposit section | Deposit section should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToDeviceIntentPageFromAALQuickLink(myTmoData);
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.clickBuyAWatch();
		UNOWatchPLPPage unoWatchPLPPage = new UNOWatchPLPPage(getDriver());
		unoWatchPLPPage.verifyWatchesPageLoaded();
		unoWatchPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOWatchPDPPage unoWatchPDPPage = new UNOWatchPDPPage(getDriver());
		unoWatchPDPPage.verifyWatchesPDPPageLoaded();
		unoWatchPDPPage.clickOnAddToCartCTA();
		WatchPlanPage watchPlanPage = new WatchPlanPage(getDriver());
		watchPlanPage.verifyWatchPlanPage();
		watchPlanPage.verifyWatchPlanImgDisplayed();
		watchPlanPage.verifyWatchPlanDueMonthlyTextDisplayed();
		watchPlanPage.verifyWatchPlanDepositTextDisplayed();
		watchPlanPage.verifyWatchPlanDueTodayTextDisplayed();
		watchPlanPage.verifyWatchPlanDueTodayPriceDisplayed();
		watchPlanPage.verifyAddPlanLinkNumberCTA();
		watchPlanPage.verifyWatchPlanRatePlanSection();
	}

	/**
	 * US617150: Eligibility check and error messaging for 5 max eligibility check
	 * CDCSM-11 : Eligibility check and error messaging for 5 max eligibility check
	 * TC-33: CDCSM-11: Validate the line selector section for accounts having 5
	 * digit lines not specific to user
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEligibilityCheckForWearables(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US544744: Display Line Selector title and lines below cost details");
		Reporter.log(
				"Data Condition | User should have AAL eligible and User should have 5 digit lines specific to user");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Click on Wearable tile | Wearable PLP page should be displayed");
		Reporter.log("5. Slelect wearable device | Wearable PDP page should be displayed");
		Reporter.log("6. Click on Continue button | Consolidated rate plan should be displayed");
		Reporter.log(
				"7. Verify the line selector section of consolidated charge page | All lines should be able to add wearable to the lines and continue to trade in pages");
		Reporter.log("8. Click on 'Add plan and link number' CTA  | Trade-in page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US617150: Eligibility check and error messaging for 5 max eligibility check
	 * CDCSM-11 : Eligibility check and error messaging for 5 max eligibility check
	 * TC-34: CDCSM-11: Validate the line selector section for user having 5 digit
	 * lines specific to user TC-35: CDCSM-11: Validate the error message section
	 * for 5 digit lines Tc-36: CDCSM-11: Validate the "Add plan and link number"
	 * CTA is disabled if PAH has 5 digit lines
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEligibilityErrorMessageForWearables(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US544744: Display Line Selector title and lines below cost details");
		Reporter.log(
				"Data Condition | User should have AAL eligible and User should have 5 digit lines specific to user");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Click on Wearable tile | Wearable PLP page should be displayed");
		Reporter.log("5. Slelect wearable device | Wearable PDP page should be displayed");
		Reporter.log("6. Click on Continue button | Consolidated rate plan should be displayed");
		Reporter.log(
				"7. Verify the line selector section of consolidated charge page | Authorable error message should be displayed under the line");
		Reporter.log("8. Verify the Radio button | Radio button should be disabled");
		Reporter.log("9. Verify the Line name | Line name should be grayed out");
		Reporter.log("10. Verify 'Add plan and link number' CTA |'Add plan and link number' CTA should be disabled");
		Reporter.log("11. Click on error message | User should navigate Contact Us page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

}
