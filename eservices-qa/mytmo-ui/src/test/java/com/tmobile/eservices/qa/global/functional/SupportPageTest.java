package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.SupportPage;

public class SupportPageTest extends GlobalCommonLib{

	/**
	 * Verify support link redirects to support page
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName",groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID})
	public void verifySupportIcon(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySupportIcon data");
		Reporter.log("Test Case : Verify support link redirects to support page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Support Link | Support page should be displayed");
		Reporter.log("==============================");
		Reporter.log("Actual Results:");

		NewHomePage homePage=navigateToNewHomePage(myTmoData);
		homePage.clickFooterSupportlink();
		homePage.switchToWindow();
		
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();
		
		SupportPage supportPage = new SupportPage(getDriver());
		supportPage.verifySupportPage();
	}
}
