/**
 * Copyright (C) 2015 Deque Systems Inc.,
 *
 * Your use of this Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * This entire copyright notice must appear in every copy of this file you
 * distribute or in any file that contains substantial portions of this source
 * code.
 */

package com.tmobile.eservices.qa.accessibility;

import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.csvreader.CsvWriter;
import com.tmobile.eservices.qa.accessibility.scanner.AXE;
import com.tmobile.eservices.qa.accessibility.scanner.AccessibilityScanner;
import com.tmobile.eservices.qa.accessibility.scanner.Result;

public class AccessibilityCommonLib {
	@Rule
	public TestName testName = new TestName();
	private WebDriver driver;
	private static final URL scriptUrl = AccessibilityCommonLib.class.getResource("/axe.min.js");

	/**
	 * Basic test
	 */
	public void testAccessibility(WebDriver webDriver, String pageName) {
		try {
			JSONObject responseJSON = new AXE.Builder(webDriver, scriptUrl).skipFrames().analyze();
			JSONArray violations = responseJSON.getJSONArray("violations");
			if (violations.length() == 0) {
				Assert.assertTrue(true, "No violations found");
			} else {
				AXE.writeResults(pageName, responseJSON);
				reportWithNumbers(violations);
				createCsvReportByRun(pageName, violations);
				createCsvReportByPage(pageName, violations);
				Assert.assertTrue(false, AXE.report(violations));
			}
		} catch (Exception e) {
			Assert.fail("Failed to run accessibility test for: " + pageName);
		}
	}
	
	
	/**
	 * Create CSV file with page validation
	 */
	public void createCsvReportByRun(String pageName, JSONArray array) {
		String path = "test-output/Accessibility-Suite/csv/";
		String fileName = "accessibilityRun";
		new File(path).mkdirs();
		File directory = new File(path);
		// create CSV folder if doesnt exist
		if (!directory.exists()) {
			directory.mkdirs();
		}
		// creating date
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat dateFormatForCsv = new SimpleDateFormat("yyyy-MM-dd");
		String outputFile = path + fileName + dateFormatForCsv.format(date) + ".csv";
		boolean alreadyExists = new File(outputFile).exists();
		try {
			CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');
			if (!alreadyExists) {
				// if file is not exist - this block will create it and write results to it
				csvOutput.write("Date");
				csvOutput.write("Page");
				csvOutput.write("Error ID");
				csvOutput.write("Description");
				csvOutput.write("Count");
				csvOutput.write("Location of element(s)");
				csvOutput.endRecord();
			}
			for(int i = 0; i < array.length(); i ++) {
				csvOutput.write(dateFormat.format(date));
				csvOutput.write(pageName);
				JSONArray tags = array.getJSONObject(i).getJSONArray("tags");
				csvOutput.write(tags.toString().replaceAll("[^a-zA-Z0-9,]+",""));
				String description = array.getJSONObject(i).getString("help");
				csvOutput.write(description);
				JSONArray nodes = array.getJSONObject(i).getJSONArray("nodes");
				csvOutput.write(Integer.toString(nodes.length()));
				if (nodes.length() > 0) {
					String elements = "";
					for (int j = 0; j < nodes.length(); j++) {
						String element = nodes.getJSONObject(j).getString("html");
						if(elements.isEmpty()) {
							elements = element;
						}else {
							elements = elements + "\n" + element;
						}
					}
					csvOutput.write(elements);
				}
				csvOutput.endRecord();
			}
			csvOutput.close();
		} catch (Exception e) {
			Assert.fail("Failed to vcreate CSV report for '" + pageName + "' page");
		}
	}

	/**
	 * Create CSV file with page validation
	 */
	public void createCsvReportByPage(String pageName, JSONArray array) {
		String path = "test-output/Accessibility-Suite/csv/";
		new File(path).mkdirs();
		File directory = new File(path);
		//create CSV folder if doesnt exist
		if(!directory.exists()) {
			directory.mkdirs();
		}
		String outputFile = path + pageName + ".csv";
		boolean alreadyExists = new File(outputFile).exists();
		try {
		//	creating array of violations
			List<String> violationNames = new ArrayList<String>();
			for (int i = 0; i < array.length(); i++) {
				String nodes = array.getJSONObject(i).getString("help");
				violationNames.add(nodes);
			}
		//	creating date
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			DateFormat dateFormatTitle = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
			Date date = new Date();

			CsvWriter csvOutput = new CsvWriter(new FileWriter(outputFile, true), ',');
			if (!alreadyExists) {
			//	if file is not exist - this block will create it and write results to it
				csvOutput.write("Date");
				for (int i = 0; i < array.length(); i++) {
					csvOutput.write(violationNames.get(i));
				}
				csvOutput.endRecord();
				csvOutput.write(dateFormat.format(date));
				for (int i = 0; i < array.length(); i++) {
					JSONArray nodes = array.getJSONObject(i).getJSONArray("nodes");
					csvOutput.write(Integer.toString(nodes.length()));
				}
				csvOutput.endRecord();
			} else {
				String line = "";
				BufferedReader br = new BufferedReader(new FileReader(outputFile));
				line = br.readLine();
				String listString = "Date," + String.join(",", violationNames);
				if (line.equals(listString)) {
				//	if file exist and violations are same then we will add one line with results
					csvOutput.write(dateFormat.format(date));
					for (int i = 0; i < array.length(); i++) {
						JSONArray nodes = array.getJSONObject(i).getJSONArray("nodes");
						csvOutput.write(Integer.toString(nodes.length()));
					}
					csvOutput.endRecord();
					csvOutput.close();
				} else {
				//	if file is exist, but dont contain headers - create new file and include all
				//	headers there
					String outputFileNew = path + pageName + "-"
							+ dateFormatTitle.format(date) + ".csv";
					CsvWriter csvOutputNew = new CsvWriter(new FileWriter(outputFileNew, true), ',');
					csvOutputNew.write("Date");
					for (int i = 0; i < array.length(); i++) {
						csvOutputNew.write(violationNames.get(i));
					}
					csvOutputNew.endRecord();
					csvOutputNew.write(dateFormat.format(date));
					for (int i = 0; i < array.length(); i++) {
						JSONArray nodes = array.getJSONObject(i).getJSONArray("nodes");
						csvOutputNew.write(Integer.toString(nodes.length()));
					}
					csvOutputNew.endRecord();
					csvOutputNew.close();
					br.close();
				}
			}
			csvOutput.close();
		} catch (Exception e) {
			Assert.fail("Failed to vcreate CSV report for '" + pageName + "' page");
		}
	}

	/**
	 * Report number of violations and defects
	 */
	public void reportWithNumbers(JSONArray array) {
		Reporter.log("Number of violations is: " + array.length());
		int counter = 0;
		for (int i = 0; i < array.length(); i++) {
			JSONArray nodes = array.getJSONObject(i).getJSONArray("nodes");
			counter = counter + nodes.length();
		}
		Reporter.log("Number of violated nodes is: " + Integer.toString(counter));
	}

	/**
	 * Test with skip frames
	 */
	public void testAccessibilityWithSkipFrames() {
		JSONObject responseJSON = new AXE.Builder(driver, scriptUrl).skipFrames().analyze();

		JSONArray violations = responseJSON.getJSONArray("violations");

		if (violations.length() == 0) {
			assertTrue("No violations found", true);
		} else {
			AXE.writeResults(testName.getMethodName(), responseJSON);

			assertTrue(AXE.report(violations), false);
		}
	}

	/**
	 * Test with options
	 */
	public void testAccessibilityWithOptions() {
		JSONObject responseJSON = new AXE.Builder(driver, scriptUrl)
				.options("{ rules: { 'accesskeys': { enabled: false } } }").analyze();

		JSONArray violations = responseJSON.getJSONArray("violations");

		if (violations.length() == 0) {
			assertTrue("No violations found", true);
		} else {
			AXE.writeResults(testName.getMethodName(), responseJSON);

			assertTrue(AXE.report(violations), false);
		}
	}

	/**
	 * Test a specific selector or selectors
	 */
	public void testAccessibilityWithSelector() {
		JSONObject responseJSON = new AXE.Builder(driver, scriptUrl).include("title").include("p").analyze();

		JSONArray violations = responseJSON.getJSONArray("violations");

		if (violations.length() == 0) {
			assertTrue("No violations found", true);
		} else {
			AXE.writeResults(testName.getMethodName(), responseJSON);

			assertTrue(AXE.report(violations), false);
		}
	}

	/**
	 * Test includes and excludes
	 */
	// @Test
	public void testAccessibilityWithIncludesAndExcludes() {
		JSONObject responseJSON = new AXE.Builder(driver, scriptUrl).include("div").exclude("h1").analyze();

		JSONArray violations = responseJSON.getJSONArray("violations");

		if (violations.length() == 0) {
			assertTrue("No violations found", true);
		} else {
			AXE.writeResults(testName.getMethodName(), responseJSON);

			assertTrue(AXE.report(violations), false);
		}
	}

	/**
	 * Test a WebElement
	 */
	// @Test
	public void testAccessibilityWithWebElement() {
		JSONObject responseJSON = new AXE.Builder(driver, scriptUrl).analyze(driver.findElement(By.tagName("p")));

		JSONArray violations = responseJSON.getJSONArray("violations");

		if (violations.length() == 0) {
			assertTrue("No violations found", true);
		} else {
			AXE.writeResults(testName.getMethodName(), responseJSON);

			assertTrue(AXE.report(violations), false);
		}
	}

	/**
	 * This method uses ChromeDeveloperTools library to generate accessbility
	 * report
	 */
	public void checkAccessibility(String pageName, WebDriver driver) throws IOException {
		AccessibilityScanner scanner = new AccessibilityScanner(driver);
		Map<String, Object> audit_report = scanner.runAccessibilityAudit();
		// Logger log = Logger.getLogger(LoginPageScan.class);
		if (audit_report.containsKey("plain_report")) {
			// Reporter.log("Warning:"+audit_report.get("plain_report").toString());
		}

		if (audit_report.containsKey("error")) {
			@SuppressWarnings("unchecked")
			List<Result> errors = (List<Result>) audit_report.get("error");
			Reporter.log("");
			Reporter.log("PAGE URL:" + driver.getCurrentUrl());
			Reporter.log("ERRORS:");
			Reporter.log("--------------------------------------------------------");
			for (Result error : errors) {

				Reporter.log("Rule: " + error.getRule());
				for (String element : error.getElements())
					Reporter.log("Element ==>:" + element);
				Reporter.log("");

			}
		}

		if (audit_report.containsKey("warning")) {
			@SuppressWarnings("unchecked")
			List<Result> warnings = (List<Result>) audit_report.get("warning");
			Reporter.log("");

			Reporter.log("WARNINGS:");
			Reporter.log("--------------------------------------------------------");
			for (Result warning : warnings) {

				Reporter.log("Rule: " + warning.getRule());
				for (String element : warning.getElements())
					Reporter.log("Element ==>:" + element);
				Reporter.log("");
			}
		}

		if (audit_report.containsKey("screenshot")) {
			final byte[] screenshot = (byte[]) audit_report.get("screenshot");

			BufferedImage img = ImageIO.read(new ByteArrayInputStream(screenshot));
			ImageIO.write(img, "png", new File("src/test/resources/screenshots/" + pageName + ".png"));
		}
	}

	public void collectPageServiceCalls(WebDriver webdriver, String pageName) {
		String FILENAME = "logs/" + pageName + ".txt";
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))) {
			Map<String, Object> logType = new HashMap<>();
			logType.put("type", "sauce:network");
			@SuppressWarnings("unchecked")
			List<Map<String, Object>> logEntries = (List<Map<String, Object>>) ((JavascriptExecutor) webdriver)
					.executeScript("sauce:log", logType);
			ArrayList<String> smetricsCalls = new ArrayList<>();
			for (Map<String, Object> logEntry : logEntries) {
				String url = (String) logEntry.get("url");
				// if(logEntry.keySet().contains("requestHeaders"))
				// System.out.println(logEntry.get("requestHeaders"));
				// if (url.contains("mytmobile/eservice/servlet/")) {
				if (url.contains("apps/mytmobile/")) {
					System.out.println(pageName + ":" + url);
					bw.write(url);
					bw.newLine();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// private void writeToFile(String uri, String pgname){
	// try (BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME))) {
	// bw.append(pgname+":"+uri);
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

}
