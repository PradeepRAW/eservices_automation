package com.tmobile.eservices.qa.webanalytics;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import com.tmobile.eservices.qa.pages.WebAnalyticsPage;

public class AnalyticsLib {

	@FindBy(css = "head > script:nth-child(n)")
	public List<WebElement> pageAnalytics;
	SoftAssert softAssert = new SoftAssert();

	/***
	 * 
	 * @param element
	 * @param pageName
	 */
	public void verifySmetrics(List<WebElement> element, String pageName) {
		ArrayList<String> smetricsCount = new ArrayList<>();
		try {
			if (element != null) {
				for (WebElement smetrics : element) {
					System.out.println(pageName + ":" + smetrics.getAttribute("src"));
					if (smetrics.getAttribute("src").contains("smetrics")) {
						parseURI(smetrics.getAttribute("src"));
						System.out.println(pageName + ":" + smetrics.getAttribute("src"));
						smetricsCount.add(smetrics.getAttribute("src"));
						Reporter.log("There are " + smetricsCount.size() + " calls in " + pageName);
					}
				}
			} else
				Assert.fail("No Analytics call in " + pageName);

		} catch (Exception e) {
			Assert.fail("Analytics locator may not be correct");
		}
		if (smetricsCount.size() <= 0)
			Assert.fail("No Analytics call in " + pageName);

	}

	/**
	 * @param args
	 * @throws URISyntaxException
	 */
	@SuppressWarnings("deprecation")
	public static void parseURI(String url) {

		try {
			List<NameValuePair> params = URLEncodedUtils.parse(new URI(url), "UTF-8");
			Reporter.log("<table><tr><td><strong>KEY</strong></td><td><strong>VALUE</strong></td></tr>");
			for (NameValuePair param : params) {
				Reporter.log("<tr><td>" + param.getName() + "</td><td>" + param.getValue() + "</td></tr>");
			}
			// Reporter.log("</tbody></table>");
			Reporter.log("</table>");
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Verify by src element from the web page.
	 * 
	 * @param webdriver
	 * @param pageTagList
	 * @param notNullList
	 */
	public void validateAnalyticTagsByScriptElement(WebDriver webdriver, HashMap<String, String> pageTagList,
			List<String> notNullList) {

		ArrayList<String> smetricsList = new ArrayList<>();
		WebAnalyticsPage analyticsPage = new WebAnalyticsPage(webdriver);
		List<WebElement> element = analyticsPage.pageAnalytics;
		if (element != null) {
			for (WebElement smetrics : element) {
				// System.out.println(smetrics.getAttribute("src"));
				if (smetrics.getAttribute("src").contains("smetrics")) {
					System.out.println("Page " + webdriver.getTitle() + ": " + smetrics.getAttribute("src"));
					smetricsList.add(smetrics.getAttribute("src"));
				}
			}
		}

		Reporter.log("");
		Reporter.log("------------------------");
		Reporter.log("There are '" + smetricsList.size() + "' Smetric Calls in this page");
		Reporter.log("------------------------");
		if (smetricsList.size() > 0) {
			for (String smetric : smetricsList) {
				Reporter.log("");
				verifyPageTagList(smetric, pageTagList, notNullList);
				verifyNotNullTagList(smetric, commonPageTags());
			}
		}
		softAssert.assertAll();
	}

	@SuppressWarnings("unchecked")
	public boolean verifyPDLTags(WebDriver webdriver, String key, String value) {

		Map<String, Object> logType = new HashMap<>();
		logType.put("type", "sauce:network");
		List<Map<String, Object>> logEntries = (List<Map<String, Object>>) ((JavascriptExecutor) webdriver)
				.executeScript("sauce:log", logType);
		for (Map<String, Object> logEntry : logEntries) {
			String url = (String) logEntry.get("url");
			// System.out.println(url);
			if (url.contains("smetrics")) {
				// parseURI(url);
				List<NameValuePair> urlValues = URLEncodedUtils.parse(url, Charset.defaultCharset());
				if (urlValues != null) {
					if (urlValues.toString().contains(key))
						for (NameValuePair urlValue : urlValues) {
							if (key.equals(urlValue.getName())) {
								if (urlValue.getValue().toString().contains(value)) {
									Reporter.log("PASS: " + key + "=" + urlValue.getValue());
									return true;
								} else {
									Reporter.log("FAIL: " + key + "=" + urlValue.getValue());
									return false;
								}
							}
						}
					else
						Reporter.log("FAIL: " + key + " Doesn't exist");
				} else
					Reporter.log("No analytics in this page");
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public boolean verifyCommonPDLTags(WebDriver webdriver, String key) {

		Map<String, Object> logType = new HashMap<>();
		logType.put("type", "sauce:network");
		List<Map<String, Object>> logEntries = (List<Map<String, Object>>) ((JavascriptExecutor) webdriver)
				.executeScript("sauce:log", logType);
		for (Map<String, Object> logEntry : logEntries) {
			String url = (String) logEntry.get("url");
			if (url.contains("smetrics")) {
				List<NameValuePair> urlValues = URLEncodedUtils.parse(url, Charset.defaultCharset());
				if (urlValues != null)
					if (urlValues.toString().contains(key))
						for (NameValuePair urlValue : urlValues) {
							if (key.equals(urlValue.getName())) {
								if (!urlValue.getValue().isEmpty()) {
									Reporter.log("PASS: " + key + "=" + urlValue.getValue());
									return true;
								} else {
									Reporter.log("FAIL: " + key + "=" + urlValue.getValue());
									return false;
								}
							}
						}
					else
						Reporter.log("FAIL: " + key + " Doesn't exist");
				else
					Reporter.log("No analytics in this page");
			}
		}
		return false;
	}

	public void commonVerifications(WebDriver webdriver) {
		Reporter.log(" ");
		Reporter.log(" ");
		Reporter.log("COMMON[NOT NULL] VERIFICATIONS:");
		Reporter.log("---------------------");
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "v103"));
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "pageUrl"));
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "v6"));
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "v60"));
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "v121"));
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "v122"));
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "v1"));
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "v4"));
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "v18"));
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "c74"));
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "v79"));
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "v86"));
		softAssert.assertNotNull(verifyCommonPDLTags(webdriver, "v7"));
		softAssert.assertAll();
	}

	/***
	 * prepare list for common verifications for all pages
	 * 
	 * @return
	 */

	public List<String> commonPageTags() {
		List<String> commonKeyList = new ArrayList<String>();
		commonKeyList.add("v1");
		commonKeyList.add("v4");
		commonKeyList.add("v6");
		commonKeyList.add("v103");
		// commonKeyList.add("pageUrl");
		commonKeyList.add("v60");
		commonKeyList.add("v121");
		commonKeyList.add("v122");
		commonKeyList.add("v18");
		commonKeyList.add("c74");
		commonKeyList.add("v79");
		commonKeyList.add("v86");
		commonKeyList.add("v7");
		return commonKeyList;
	}

	/***
	 * Filter smetric calls by stand alone means outside of saucelabs (future
	 * requirement)
	 * 
	 * @param webdriver
	 * @param pageName
	 */
	public void verifyPageTagsStandAlone(WebDriver webdriver, String pageName) {
		List<LogEntry> entries = webdriver.manage().logs().get(LogType.PERFORMANCE).getAll();
		System.out.println(entries.size() + " " + LogType.PERFORMANCE + " log entries found");
		for (LogEntry entry : entries) {
			JSONObject obj;
			try {
				obj = new JSONObject(entry.getMessage());
				// System.out.println(obj);
				obj = getObjectParam(obj, "message");
				obj = getObjectParam(obj, "params");
				@SuppressWarnings("unused")
				Map<String, String> reqUrlParams = new HashMap<>();
				if (obj.has("request")) {
					obj = getObjectParam(obj, "request");
					String url = (String) obj.get("url");
					if (url.contains("smetrics.t-mobile.com")) {
						// URL urlVal = new URL(url);
						List<NameValuePair> urlValues = URLEncodedUtils.parse(url, Charset.defaultCharset());
						for (NameValuePair urlValue : urlValues) {
							if ("v5".equals(urlValue.getName())) {
								System.out.println(urlValue.getValue());
							}
						}
					}
				}
				/*
				 * obj = getObjectParam(obj, "params"); //System.out.println(obj);
				 * if(obj.has("response")) { obj = getObjectParam(obj, "response");
				 * if(obj.has("headers")) { obj = getObjectParam(obj, "headers");
				 * if(obj.has("v5")) { System.out.println(obj.get("v5")); } else {
				 * System.out.println(obj); } } System.out.println(obj); }
				 */
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/***
	 * get params from JSON object
	 * 
	 * @param obj
	 * @param key
	 * @return
	 * @throws JSONException
	 */
	private JSONObject getObjectParam(JSONObject obj, String key) throws JSONException {
		Object paramObj = obj.get(key);
		if (paramObj instanceof JSONObject) {
			return (JSONObject) paramObj;
		}
		return null;
	}

	/**********
	 * print description for page verifications
	 */
	public void writePageDescription() {
		Reporter.log("THIS PAGE PDL VALIDATIONS:");
		// Reporter.log("--------------------------");
	}

	/***
	 * print description for commom verifications
	 */
	public void writeCommonPageDescription() {
		Reporter.log(" ");
		Reporter.log(" ");
		Reporter.log("COMMON[NOT NULL] VERIFICATIONS:");
		// Reporter.log("---------------------");
	}

	/***
	 * Collect smetrics calls in given page
	 * 
	 * @param webdriver
	 * @return
	 */
	public ArrayList<String> collectCalls(WebDriver webdriver) {
		Map<String, Object> logType = new HashMap<>();
		ArrayList<String> smetricsCalls = new ArrayList<>();
		try {
			logType.put("type", "sauce:network");
			@SuppressWarnings("unchecked")
			List<Map<String, Object>> logEntries = (List<Map<String, Object>>) ((JavascriptExecutor) webdriver)
					.executeScript("sauce:log", logType);

			for (Map<String, Object> logEntry : logEntries) {
				// System.out.println("VERIFY:"+logEntry);
				String url = (String) logEntry.get("url");
				// System.out.println("VERIFY:"+url);
				if (logEntry.toString().contains("smetrics")) {
					// System.out.println("SMETRIC CALLS:"+url);
					smetricsCalls.add(url);
				}
			}
		} catch (Exception e) {
			System.out.println("SAUCE DEBUGGING ERROR:" + e);
		}
		return smetricsCalls;
	}

	/***
	 * page specific validations
	 * 
	 * @param smetricUrl
	 * @param pageTagMap
	 */
	public void verifyPageTagList(String smetricUrl, HashMap<String, String> pageTagMap,
			List<String> pageSpecificNotNullList) {
		// List<String> reporterLogs = null;
		ArrayList<String> reporterLogs = new ArrayList<>();
		List<NameValuePair> urlValues = URLEncodedUtils.parse(smetricUrl, Charset.defaultCharset());
		if (urlValues != null) {
			if (pageTagMap.size() > 0) {
				// @SuppressWarnings("rawtypes")
				Set<?> set = pageTagMap.entrySet();
				// @SuppressWarnings("rawtypes")
				Iterator<?> iterator = set.iterator();
				while (iterator.hasNext()) {
					// @SuppressWarnings("rawtypes")
					@SuppressWarnings("rawtypes")
					Map.Entry mentry = (Map.Entry) iterator.next();
					String key = (String) mentry.getKey();
					String value = (String) mentry.getValue();
					if (urlValues.toString().contains(key))
						for (NameValuePair urlValue : urlValues) {
							if (key.equals(urlValue.getName())) {
								if (urlValue.getValue().toString().contains(value)) {
									reporterLogs.add("PASS: " + key + "=" + urlValue.getValue());
								} else {
									reporterLogs.add("FAIL: " + key + "=" + urlValue.getValue());
								}
							}
						}
					else
						reporterLogs.add("FAIL: '" + key + "' Doesn't Exist");
				}
			}
			if (pageSpecificNotNullList != null)
				for (String notNullKey : pageSpecificNotNullList)
					if (urlValues.toString().contains(notNullKey))
						for (NameValuePair urlValueNotNull : urlValues) {
							if (notNullKey.equals(urlValueNotNull.getName())) {
								if (urlValueNotNull.getValue() != null) {
									reporterLogs.add("PASS: " + notNullKey + "=" + urlValueNotNull.getValue());
								} else {
									reporterLogs.add("FAIL: " + notNullKey + "=" + urlValueNotNull.getValue());
								}
							}
						}
					else
						reporterLogs.add("FAIL: '" + notNullKey + "' Doesn't Exist");
		} else
			Reporter.log("No headers in smetric call");

		boolean flag = false;
		// Set<String> reporterLogsWithUniqValues = new HashSet<>(reporterLogs);
		printPageVerifications(reporterLogs, flag);
	}

	/***
	 * Common page validations
	 * 
	 * @param smetricUrl
	 * @param commonTagList
	 */
	public void verifyNotNullTagList(String smetricUrl, List<String> commonTagList) {
		// List<String> reporterLogs = null;
		// List<String> reporterLogsWithUniqValues = new ArrayList<>(new
		// HashSet<>(reporterLogs));
		ArrayList<String> reporterLogs = new ArrayList<>();
		for (String key : commonTagList) {
			List<NameValuePair> urlValues = URLEncodedUtils.parse(smetricUrl, Charset.defaultCharset());
			if (urlValues != null)
				if (urlValues.toString().contains(key))
					for (NameValuePair urlValue : urlValues) {
						if (key.equals(urlValue.getName())) {
							if (urlValue.getValue() != null) {
								reporterLogs.add("PASS: " + key + "=" + urlValue.getValue());
							} else {
								reporterLogs.add("FAIL: " + key + "=" + urlValue.getValue());
							}
						}
					}
				else
					reporterLogs.add("FAIL: '" + key + "' Doesn't Exist");
			else
				reporterLogs.add("No headers in smetric call");
		}
		boolean flag = true;
		// Set<String> reporterLogsWithUniqValues = new HashSet<>(reporterLogs);
		printPageVerifications(reporterLogs, flag);
	}

	/***
	 * print reporter logs after validation
	 * 
	 * @param reporterLogs
	 * @param flag
	 */
	public void printPageVerifications(List<String> reporterLogs, boolean flag) {
		if (flag)
			writeCommonPageDescription();
		else
			writePageDescription();
		if (reporterLogs != null)
			for (String reporter : reporterLogs)
				Reporter.log(reporter);
		if (reporterLogs.toString().contains("FAIL") && flag == true)
			softAssert.fail("=> There are one or more COMMON tags failed, see test messages for details");
		if (reporterLogs.toString().contains("FAIL") && flag == false)
			softAssert.fail("=> There are one or more PAGE SPECIFIC tags failed, see test messages for details");
	}

	/***
	 * Collect smetrics calls and verify required tags both page and common tags.
	 * 
	 * @param webdriver
	 * @param pageTagList
	 */
	public void verifyPagePdlTags(WebDriver webdriver, HashMap<String, String> pageTagList, List<String> notNullList) {
		ArrayList<String> smetricsList = collectCalls(webdriver);
		Reporter.log("");
		Reporter.log("------------------------");
		Reporter.log("There are '" + smetricsList.size() + "' Smetric Calls in this page");
		Reporter.log("------------------------");
		int index = 0;
		if (smetricsList.size() > 0)
			for (String smetric : smetricsList) {
				Reporter.log("");
				Reporter.log("Call: " + ++index + " Verifications");
				Reporter.log("Click Event");
				Reporter.log("===========================");
				verifyPageTagList(smetric, pageTagList, notNullList);
				verifyNotNullTagList(smetric, commonPageTags());
			}
		else
			softAssert.fail("No smetric call in this page");
		softAssert.assertAll();
	}

	// -------------------------------------
	public void verifyPagePdl(WebDriver webdriver, HashMap<String, String> pageTagList, List<String> notNullList) {
		String smetricsList = returnLastCall(webdriver);
		Reporter.log("");
		if (smetricsList != null) {
			Reporter.log("");
			Reporter.log("Click Event");
			Reporter.log("===========================");
			verifyPageTagList(smetricsList, pageTagList, notNullList);
			verifyNotNullTagList(smetricsList, commonPageTags());
		} else
			softAssert.fail("No smetric call in this page");
		softAssert.assertAll();
	}

	public String returnLastCall(WebDriver webdriver) {
		Map<String, Object> logType = new HashMap<>();
		logType.put("type", "sauce:network");
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> logEntries = (List<Map<String, Object>>) ((JavascriptExecutor) webdriver)
				.executeScript("sauce:log", logType);
		ArrayList<String> smetricsCalls = new ArrayList<>();
		for (Map<String, Object> logEntry : logEntries) {
			String url = (String) logEntry.get("url");
			// System.out.println(url);
			if (url.contains("smetrics")) {
				smetricsCalls.add(url);
				System.out.println(url);
			}
		}
		if (smetricsCalls.size() > 2)
			return smetricsCalls.get(smetricsCalls.size() - 2);
		else
			return smetricsCalls.get(smetricsCalls.size() - 1);

	}
}
