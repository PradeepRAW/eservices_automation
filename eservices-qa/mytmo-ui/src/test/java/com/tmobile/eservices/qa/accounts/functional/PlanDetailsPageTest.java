/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.accounts.LineDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.PlanDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.PlansListPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class PlanDetailsPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(PlanDetailsPageTest.class);

	// Regression

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testNonIOTPlanBehaviorInPlanListPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case Name  : Verify Non IOT Plan Behavior In Plan List Page");
		Reporter.log("Test Data : Precondition this msisdn Should have non-IOT plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click On plan icon | Plan Page should be displayed");
		Reporter.log("4. Click on Change Plan btn | PlanComparision page should be verified");
		Reporter.log("5. Verify 3 tabs on the top of the page is not present");
		Reporter.log("6. Verify text Looks like you’re already on our best plan. We like the way you think");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		LineDetailsPage lineDetailsPage = navigateToLineDetailsPageDirectly(myTmoData);
		lineDetailsPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifyDisabledChangePlanCta();
	}

	/*
	 * VerifyTermsandConditionModalinPlanDetailsPageinLineLevel
	 * 
	 * US399360 : Plan Details Page: Terms and Conditions Modal
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyTermsandConditionModalinPlanDetailsPageinLineLevel(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyTermsandConditionModalinPlanDetailsPageinLineLevel");
		Reporter.log("Test Case : VerifyTermsandConditionModalinPlanDetailsPageinLineLevel");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Line | User should be navigated to LineDetails Page ");
		Reporter.log("5.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log(
				"6.Veify that TermsAndCondition description | TermsAndCondition is displyed on below changePlancta");
		Reporter.log("7.Clcik on SeeFullDetails link | SeeFullDetails link is available");
		Reporter.log("8.Verify that termsandcondition page | Terms and Condition page loaded");
		Reporter.log("9.Click on BackCta | Application should navigate back to plan details page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PlanDetailsPage plandetailspage = navigateToPlanDetailsPage(myTmoData);

		plandetailspage.verifyTermsandcondition();
		plandetailspage.clickSeeFullDetails();
		plandetailspage.verifyTermsandconditionPage();
		plandetailspage.clickOnButtonByName("Back");
		plandetailspage.verifyPlanDetailsPage();
	}

	/*
	 * 
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyPlanDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPlanDetailsPage");
		Reporter.log("Test Case : Verify Plan Details Page");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PlanDetailsPage plandetailspage = navigateToPlanDetailsPage(myTmoData);

		plandetailspage.verifyPlanNameBreadCrump();
		plandetailspage.verifyPlanDetailsPageHeaders();

		plandetailspage.verifyIncludedPlansList();
		plandetailspage.verifyLegalText();
		plandetailspage.clickOnSeeFullDetailsLink();
		plandetailspage.clickOnButtonByName("Back");

		/*
		 * plandetailspage.clickOnButtonByName("Manage Add-Ons");
		 * plandetailspage.clickOnButtonByName("Change plan");
		 * 
		 * verifyManageAddOnsPage(); plandetailspage.verifyAssociatedLinesList();
		 * 
		 * LineDetailsPage lineDetailsPage = new LineDetailsPage(getDriver());
		 * lineDetailsPage.verifyLineDetailsPage();
		 */ }

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testPlanDetailsforBusinessLine(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testPlanDetailsforBusinessLine is called in PlanDetailsPageTest");
		Reporter.log("Test Case : Test Plan Details for Business Line");
		Reporter.log("Test Data : Business customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Launch home page | Home page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifyIncludedInThePlanNamesList(plandetailspage.verifyBusniessLineIncludedInThePlanNamesList(),
				"");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testPlanDetailsforUnlimited55Plus(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testPlanDetailsforUnlimited55Plus is called in PlanDetailsPageTest");
		Reporter.log("Test Case : Test Plan Details for Unlimited 55 Plus");
		Reporter.log("Test Data : Unlimited 55 Plus");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Plan | User should be navigated to Plan details page ");
		Reporter.log("5.Verify Plan details page plans list | Plan details plans list should be display");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PlanDetailsPage plandetailspage = navigateToPlanDetailsPage(myTmoData);
		plandetailspage.verifyIncludedInThePlanNamesList(
				plandetailspage.verifyUnlimited55plusIncludedInThePlanNamesList(), "T-Mobile ONE Unlimited 55");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testPlanDetailsforMilitaryPlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testPlanDetailsforMilitaryPlan is called in PlanDetailsPageTest");
		Reporter.log("Test Case : Test Plan Details for Military Plan");
		Reporter.log("Test Data : Military Plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Plan | User should be navigated to Plan details page ");
		Reporter.log("5.Verify Plan details page plans list | Plan details plans list should be display");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.clickOnPlanName();

		PlanDetailsPage planDetailsPage = new PlanDetailsPage(getDriver());
		planDetailsPage.verifyIncludedInThePlanNamesList(planDetailsPage.verifymilitaryPlanIncludedInThePlanNamesList(),
				"T-Mobile ONE Military");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testPlanDetailsforMBBLine(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testPlanDetailsforMBBLine is called in PlanDetailsPageTest");
		Reporter.log("Test Case : Test Plan Details for MBB Line");
		Reporter.log("Test Data : MBB");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on Account link | Account overview page should be displayed");
		Reporter.log("3.In Account Overview page click on Plan Name | Plan details page should be displayed");
		Reporter.log("4.Verify Plan details page plans list | Plan details plans list should be display");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.clickonISPline("Mobile Internet");

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testPlanDetailsforSimpleChoicenonPooled(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testPlanDetailsforSimpleChoicenonPooled is called in PlanDetailsPageTest");
		Reporter.log("Test Case : Verify Plan Details for Simple Choice non Pooled");
		Reporter.log("Test Data : PAH user with Simple Choice non Pooled plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on Account link | Account overview page should be displayed");
		Reporter.log("3.In Account Overview page click on Plan Name | Plan details page should be displayed");
		Reporter.log("4.Verify Plan details page plans list | Plan details plans list should be display");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToLineDetailsPage(myTmoData);

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.clickonISPline("Simple Choice Unlimited Talk & Text");

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifyIncludedInThePlanNamesList(plandetailspage.verifySCNONPOOLEDIncludedInThePlanNamesList(),
				"Simple Choice Unlimited Talk & Text");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testPlanDetailsforTMOONETI(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testPlanDetailsforTMOONETI is called in PlanDetailsPageTest");
		Reporter.log("Test Case : Test Plan Details for TMO ONE TI");
		Reporter.log("Test Data : PAH user TMO ONE TI Plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on Account link | Account overview page should be displayed");
		Reporter.log("3.In Account Overview page click on Plan Name | Plan details page should be displayed");
		Reporter.log("4.Verify Plan details page plans list | Plan details plans list should be display");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanDetailsPage(myTmoData);

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifyIncludedInThePlanNamesList(plandetailspage.verifyTMOONETIIncludedInThePlanNamesList(),
				"T-Mobile ONE");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testPlanDetailsforTMOONE(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testPlanDetailsforTMOONE is called in PlanDetailsPageTest");
		Reporter.log("Test Case : Test Plan Details for TMO ONE");
		Reporter.log("Test Data : PAH with TMO ONE plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Launch home page | Home page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Plan Name | Plan Details Page should displayed ");
		Reporter.log("5.Verify the Plan Name | Plan Name Should be displayed");
		Reporter.log("6.Verify Included In The Plan Names List | T-Mobile ONE with ONE Should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanDetailsPage(myTmoData);

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifyPlanDetails();
		plandetailspage.verifyIncludedInThePlanNamesList(plandetailspage.verifyTMOONENCIncludedInThePlanNamesList(),
				"T-Mobile ONE");
	}

	// Regression End
	// ==============================================================

	/*
	 * VerifysharedAddonDiscountinPlandetailspage US429672 : Plan Details Page:
	 * Applied Discounts on Shared Add Ons
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifysharedAddonDiscountinPlandetailspage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerfifySharedAddondetailsFromAccountOverviewPage");
		Reporter.log("Test Case : VerfifysharedAddonDiscountonPlanDetailsPage");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log("5.Veify that SharedAddon Component | Shared Addon should be displayed on plandetailsPage ");
		Reporter.log(
				"6.Verify that SharedAddon associatedLine | SharedAddon associatedLine should be displayed on plandetailsPage ");
		Reporter.log("7.Verify that Discount for SharedAddon's | SharedAddon's discount on PlanDetailsPage");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		// plandetailspage.sharedAddonComponent();
		plandetailspage.associatedlineSharedAddon();
		plandetailspage.sharedAddonDiscount();
	}

	/*
	 * US507933 : [Plan Details Page] Manage Add ons CTA for Non Pooled PAH user
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyManageAddonsCTAforNonPooledPAHuser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPlanDetailsPage");
		Reporter.log("Test Case : testPlanDetailsPage");
		Reporter.log("Test Data : Any NonPooled PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on NonPooled line | User should be navigated to LineDetails Page ");
		Reporter.log("5.Click on Plan Nmae | User should be navigated to PlanDetails Page ");
		Reporter.log("6.verify Shared Addon Component | Shared Addon Componenet should be available ");
		Reporter.log("7.verify Shared Addon Details | Shared Addon Details should be available ");
		Reporter.log("8.verify Manage Addon CTA | Manage Addon CTA should be available ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.verifyAccountOverviewPage();
		accountOverviewPage.clickOnNPoolLine();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.clikOnPlanName();
		plandetailspage.verifyManageAddonCta();
	}

	/*
	 * US516481 : Blocking Business customers from changing plan
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyChangePlanCtaforBusinessCustomer(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyChangePlanButton is called in PlanDetailsPageTest");
		Reporter.log("Test Case : VerifyChangePlanButton");
		Reporter.log("Test Data : Business customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Line | User should be navigated to LineDetails Page ");
		Reporter.log("5.Clcik on Plan Name | User should be navigated to Plan Details Page");
		Reporter.log("6.Veirify that Change Plan CTA | Change Plan CTA should be disabled for Bussines Customer ");
		Reporter.log("7.Verify that text message below the Change Plan CTA |Notification Message Sould be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnLineName();

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.clcikonBusinessPlan();
		plandetailspage.verifyPageHeading();
		plandetailspage.verifyDisabledChangePlanCta();
		plandetailspage.verifyNotificationText();
		plandetailspage.verifyContactUsPage();

	}

	/*
	 * US516847 : [ACM] Associated Lines Copy Update
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyRatePlanNameInsteadofAssociatedLinesinPlanDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyChangePlanButton is called in PlanDetailsPageTest");
		Reporter.log("Test Case : VerifyChangePlanButton");
		Reporter.log("Test Data : Business customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Launch home page | Home page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Plan Name | Plan Details Page should displayed ");
		Reporter.log("5.Verify the Plan Name | Plan Name Should be displayed");
		Reporter.log(
				"6.Veify Plan Name Component for associated line | Plan Name Componenet should be displayed instead of Associated line heading ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifyPageHeading();
		plandetailspage.verifyAssociatedPlanName();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testPlanDetailsforSimpleChoiceNC(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyChangePlanButton is called in PlanDetailsPageTest");
		Reporter.log("Test Case : VerifyChangePlanButton");
		Reporter.log("Test Data : Business customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Launch home page | Home page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.verifyPlanNameText("Simple Choice");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void testPlanDetailsforPappyTI(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyChangePlanButton is called in PlanDetailsPageTest");
		Reporter.log("Test Case : VerifyChangePlanButton");
		Reporter.log("Test Data : Business customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Launch home page | Home page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PlanDetailsPage plandetailspage = navigateToPlanDetailsPage(myTmoData);
		plandetailspage.verifyIncludedInThePlanNamesList(plandetailspage.verifyPappyTIIncludedInThePlanNamesList(),
				"T-Mobile ONE");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testPlanDetailsforPappyEssential(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyChangePlanButton is called in PlanDetailsPageTest");
		Reporter.log("Test Case : VerifyChangePlanButton");
		Reporter.log("Test Data : Business customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Launch home page | Home page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifyIncludedInThePlanNamesList(
				plandetailspage.verifyPappyEssentialIncludedInThePlanNamesList(), "T-Mobile Essentials");
	}

	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.RETIRED })
	public void testPriceforNetflixPremiumAddons(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testPriceforNetflixPremiumAddons in Plan Details Page");
		Reporter.log("Test Case : VerifyChangePlanButton");
		Reporter.log("Test Data : Business customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Launch home page | Home page should be displayed");
		Reporter.log("3.Launch Account Overview Page | Accouont Overview Page should be displayed");
		Reporter.log("3.Click on the plan name | Plan Details Page should be displayed");
		Reporter.log("3.Verify Shared Addons Component | Shared Addons Component should be available");
		Reporter.log(
				"3.VErify the netflix Premium AddOn | Netflix Premium Adon should be availble along with expected price");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanDetailsPage(myTmoData);

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.verifySharedAddonComponent();
		plandetailspage.verifyforNetflixPremiumAddons();
		plandetailspage.verifyforNetflixPremiumAddonsPrice();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void testIOTPlanBehaviorInPlanListPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("TestIOTPlanBehaviorInPlanListPage method called in PlansListPageTest");

		Reporter.log("Test Case Name : IOT Plan Behavior In Plan List Page");
		Reporter.log("Test Data : Precondition this msisdn Should have IOT plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click On plan icon | Plan Page should be displayed");
		Reporter.log("4. Click on Change Plan btn | PlanComparision page should be verified");
		Reporter.log("5. Verify 3 tabs on the top of the page is not present");
		Reporter.log("6. Verify text Looks like IOT plans are unavailable on web. Please contact care");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToPlanDetailsPage(myTmoData);

		PlansListPage plansListPage = new PlansListPage(getDriver());
		plansListPage.verifyPlansListPage();
		plansListPage.verifyIOTPlanMessage("Looks like IOT plans are unavailable on web. Please contact care");
	}
	
	/*
	 * Check whether user is getting Pending rate plan message or not when user has Pending rate plan
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void checkPendingRatePlanMessageWhenUserHasPendingRatePlanAndClicksOnManageMyPlanCTA(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkPendingRatePlanMessageWhenUserHasPendingRatePlanAndClicksOnManageMyPlanCTA");
		Reporter.log("Test Case : Check when user has Pending Rate Plan, and user clicks on Manage My Plan CTA, user should get message.");
		Reporter.log("Test Data : Any PAH/Full customer with pending rate plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Account link. | Account Overview page should be displayed.");
		Reporter.log("5. Click on Plan name | Plan Details page should be loaded.");
		Reporter.log("6. Click on Manage My Profile | User should get Pending Plan/Service message.");
		Reporter.log("Actual Results:");

		LineDetailsPage lineDetailsPage = navigateToLineDetailsPageDirectly(myTmoData);
		lineDetailsPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();
		
		
	}
	
}