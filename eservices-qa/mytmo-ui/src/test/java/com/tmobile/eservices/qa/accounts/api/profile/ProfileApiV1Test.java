package com.tmobile.eservices.qa.accounts.api.profile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.pages.accounts.api.ProfileApiV1;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class ProfileApiV1Test extends ProfileApiV1 {

	public Map<String, String> tokenMap;

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV1", "getProfile", Group.ACCOUNTS,
			Group.APIREG })
	public void testGetProfile(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getProfile test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with profile details.");
		Reporter.log("Step 2: Verify profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		Response response = getProfile(apiTestData, tokenMap);
		String operationName = "getProfile";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonNode lines = jsonNode.path("lines");
				if (!lines.isMissingNode() && lines.isArray()) {
					JsonNode jsonNode1 = lines.get(0);
					String msisdn = jsonNode1.path("msisdn").textValue();
					String lastName = jsonNode1.path("lastName").textValue();
					String firstName = jsonNode1.path("firstName").textValue();
					Assert.assertEquals(msisdn, apiTestData.getMsisdn(), "msisdn invalid.");
					Assert.assertNotEquals(lastName, "", "lastName is Null or Empty.");
					Assert.assertNotEquals(firstName, "", "firstName is Null or Empty.");
				}
			} else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV1", "updateProfile", Group.ACCOUNTS,
			Group.APIREG })
	public void testUpdateProfile(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: updateProfile test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with updated profile details.");
		Reporter.log("Step 2: Verify updated profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		
		Response response = getProfile(apiTestData, tokenMap);
		String operationName = "getProfile";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonNode lines = jsonNode.path("lines");
				if (!lines.isMissingNode() && lines.isArray()) {
					JsonNode jsonNode1 = lines.get(0);
					String lastName = jsonNode1.path("lastName").textValue();
					String firstName = jsonNode1.path("firstName").textValue();
					tokenMap.put("firstName", firstName);
					tokenMap.put("lastName", lastName);
				}
			} 
		} 
		
		operationName = "Profile_updateProofile";
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProofile.txt");
		response = updateProfile(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode lines = jsonNode.path("lines");
			if (!lines.isMissingNode() && lines.isArray()) {
				JsonNode line = lines.get(0);
				String msisdn = line.path("msisdn").asText();
				String firstName = line.path("firstName").asText();
				String lastName = line.path("lastName").asText();
				Assert.assertNotNull(lastName,"Invalid lastName.");
				Assert.assertNotNull(firstName, "Invalid firstName.");
				Assert.assertEquals(msisdn, apiTestData.getMsisdn(), "Invalid msisdn.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV1", "updateProfileLanguage",
			Group.ACCOUNTS, Group.PENDING })
	public void testUpdateProfileLanguage(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: updateProfile test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with updated profile details.");
		Reporter.log("Step 2: Verify updated profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		String customerCareLanguage = "English";
		Response response = getProfile(apiTestData, tokenMap);
		String operationName = "getProfile";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonNode lines = jsonNode.path("lines");
				if (!lines.isMissingNode() && lines.isArray()) {
					JsonNode jsonNode1 = lines.get(0);
					customerCareLanguage = jsonNode1.path("customerCareLanguage").textValue();
					Assert.assertNotEquals(customerCareLanguage, "", "customerCareLanguage is Null or Empty.");
				} else {
					failAndLogResponse(response, operationName);
				}
			} else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		if (customerCareLanguage != null) {
			if ("English".contentEquals(customerCareLanguage)) {
				customerCareLanguage = "Spanish";
			} else {
				customerCareLanguage = "English";
			}
		} else {
			System.out.println("customerCareLanguage element not found in response.");
			failAndLogResponse(response, operationName);
		}

		tokenMap.put("customerCareLanguage", customerCareLanguage);
		operationName = "Profile_updateProofile_language";
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProofile_language.txt");
		response = updateProfile(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode lines = jsonNode.path("lines");
			if (!lines.isMissingNode() && lines.isArray()) {
				JsonNode line = lines.get(0);
				String customerCareLanguage1 = line.path("customerCareLanguage").asText();
				Assert.assertEquals(customerCareLanguage1, customerCareLanguage, "Invalid customerCareLanguage.");
			} else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		response = getProfile(apiTestData, tokenMap);
		operationName = "getProfile-reset";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonNode lines = jsonNode.path("lines");
				if (!lines.isMissingNode() && lines.isArray()) {
					JsonNode jsonNode1 = lines.get(0);
					customerCareLanguage = jsonNode1.path("customerCareLanguage").textValue();
					Assert.assertNotEquals(customerCareLanguage, "", "customerCareLanguage is Null or Empty.");
				} else {
					failAndLogResponse(response, operationName);
				}
			} else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		if ("English".contentEquals(customerCareLanguage)) {
			customerCareLanguage = "Spanish";
		} else {
			customerCareLanguage = "English";
		}
		tokenMap.put("customerCareLanguage", customerCareLanguage);
		operationName = "Profile_updateProofile_language";
		requestBody = new ServiceTest().getRequestFromFile("Profile_updateProofile_language.txt");
		response = updateProfile(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode lines = jsonNode.path("lines");
			if (!lines.isMissingNode() && lines.isArray()) {
				JsonNode line = lines.get(0);
				String customerCareLanguage1 = line.path("customerCareLanguage").asText();
				Assert.assertEquals(customerCareLanguage1, customerCareLanguage, "Invalid customerCareLanguage.");
			} else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV1", "getProfileFeatures", Group.ACCOUNTS,
			Group.APIREG })
	public void testGetProfileFeatures(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getProfileFeatures test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with profile Features details.");
		Reporter.log("Step 2: Verify profile Features details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("category", "");
		String requestBody = "{ \"msisdns\": [ msisdn_frkey ]}";
		Response response = getProfileFeatures(apiTestData, requestBody, tokenMap);
		String operationName = "getProfileFeatures";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String ratePlanSocCode = getPathVal(jsonNode, "familySocs.ratePlan.socCode");
				//String familyAllowanceSocCode = getPathVal(jsonNode, "familySocs.familyAllowance.socCode");
				Assert.assertNotEquals(ratePlanSocCode, "", "ratePlan SocCode is Null or Empty.");
				//Assert.assertNotEquals(familyAllowanceSocCode, "", "familyAllowance SocCode is Null or Empty.");
			} else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV1", "getProfileFeaturesBlocked",
			Group.ACCOUNTS, Group.APIREG })
	public void testGetProfileFeaturesBlocked(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getProfileFeatures Blocked test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with profile  Blocked Features details.");
		Reporter.log("Step 2: Verify profile Blocked Features details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("category", "blk");
		String requestBody = "{ \"msisdns\": [ msisdn_frkey ]}";
		Response response = getProfileFeatures(apiTestData, requestBody, tokenMap);
		String operationName = "getProfileFeatures";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonNode lineSocs = jsonNode.path("lineSocs");
				if (!lineSocs.isMissingNode() && lineSocs.isArray()) {
					JsonNode blockingSocs = lineSocs.get(0);
					String blockingSoc = blockingSocs.path("blocking").get(0).path("socCode").asText();
					Assert.assertNotNull(blockingSoc, "blockingSoc is Null or Empty.");
				}
			} else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV1", "updateProfileFeatures",
			Group.ACCOUNTS, Group.APIREG })
	public void testUpdateProfileFeatures(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: updateProfileFeatures test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with updated profile Features details.");
		Reporter.log("Step 2: Verify updated profile Features details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result1:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("category", "");
		String requestBody = "{ \"msisdns\": [ msisdn_frkey ]}";
		Response response = getProfileFeatures(apiTestData, requestBody, tokenMap);
		String operationName = "getProfileFeatures";
		String sequenceNumber = null;
		String effectiveDate = null;
		String socCode = null;
		String name = null;
		String description = null;
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode lineSocs = jsonNode.path("lineSocs");
			if (!lineSocs.isMissingNode() && lineSocs.isArray()) {
				for (int j = 0; j < lineSocs.size(); j++) {
					JsonNode dataNode = lineSocs.get(j);
					if (dataNode.isObject()) {
						JsonNode webGuards = dataNode.path("webGuards");
						if (!webGuards.isMissingNode() && webGuards.isArray()) {
							for (int k = 0; k < webGuards.size(); k++) {
								JsonNode webGuard = webGuards.get(k);
								if (webGuard.isObject()) {
									JsonNode enabled = webGuard.path("enabled");
									if (enabled.booleanValue()) {
										sequenceNumber = webGuard.path("sequenceNumber").textValue();
										effectiveDate = webGuard.path("effectiveDate").textValue();
										socCode = webGuard.path("socCode").textValue();
										name = webGuard.path("name").textValue();
										description = webGuard.path("description").textValue();
										tokenMap.put("sequenceNumber", sequenceNumber);
										tokenMap.put("effectiveDate", effectiveDate);
										tokenMap.put("socCode", socCode);
										tokenMap.put("name", name);
										tokenMap.put("description", description);
										break;
									} else {
										socCode = webGuard.path("socCode").textValue();
										name = webGuard.path("name").textValue();
										description = webGuard.path("description").textValue();
										tokenMap.put("socCode", socCode);
										tokenMap.put("name", name);
										tokenMap.put("description", description);
										break;
									}

								}
							}

						}

					}
				}
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		Assert.assertNotEquals(sequenceNumber, "", "sequenceNumber is Null or Empty.");
		Assert.assertNotEquals(effectiveDate, "", "effectiveDate is Null or Empty.");
		if (sequenceNumber != null && !sequenceNumber.trim().equals("")) {
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfileFeatures.txt");
			response = updateProfileFeatures(apiTestData, requestBody, tokenMap);
			operationName = "updateProfileFeatures";
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {
					JsonNode lineSocs = jsonNode.path("lineSocs");
					if (!lineSocs.isMissingNode() && lineSocs.isArray()) {
						JsonNode allSocs = lineSocs.get(0);
						String webGuardsSoc = allSocs.path("webGuards").get(0).path("socCode").asText();
						Assert.assertNotNull(webGuardsSoc, "webGuardsSoc is Null or Empty.");
					}
				}
			} else {
				failAndLogResponse(response, operationName);
			}
		}
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfileFeatures_reset.txt");
		response = updateProfileFeatures(apiTestData, requestBody, tokenMap);
		operationName = "updateProfileFeatures";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode lineSocs = jsonNode.path("lineSocs");
			if (!lineSocs.isMissingNode() && lineSocs.isArray()) {
				JsonNode allSocs = lineSocs.get(0);
				String webGuardsSoc = allSocs.path("webGuards").get(0).path("socCode").asText();
				Assert.assertNotNull(webGuardsSoc, "webGuardsSoc is Null or Empty.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV1", "getProfilePreferences",
			Group.ACCOUNTS, Group.APIREG })
	public void testGetProfilePreferences(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getProfilePreferences test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with profile Preferences details.");
		Reporter.log("Step 2: Verify profile Preferences details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		Response response = getProfilePreferences(apiTestData, tokenMap);
		String operationName = "getProfilePreferences";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String userID = getPathVal(jsonNode, "userID");
				String isTmoIdExists = getPathVal(jsonNode, "isTmoIdExists");
				String marketingPhone = getPathVal(jsonNode, "marketing.phone");
				String marketingEmail = getPathVal(jsonNode, "marketing.email");
				String marketingMail = getPathVal(jsonNode, "marketing.mail");
				String marketingMonthlyNewsLetter = getPathVal(jsonNode, "marketing.monthlyNewsLetter");
				String notification = getPathVal(jsonNode, "notification");
				String advertising = getPathVal(jsonNode, "advertising");
				Assert.assertNotNull(userID, "userID is Null or Empty.");
				Assert.assertNotNull(isTmoIdExists, "isTmoIdExists is Null or Empty.");
				Assert.assertNotNull(marketingPhone, "marketingPhone is Null or Empty.");
				Assert.assertNotNull(marketingEmail, "marketingEmail is Null or Empty.");
				Assert.assertNotNull(marketingMail, "marketingMail is Null or Empty.");
				Assert.assertNotNull(marketingMonthlyNewsLetter, "marketingMonthlyNewsLetter is Null or Empty.");
				Assert.assertNotNull(notification, "notification is Null or Empty.");
				Assert.assertNotNull(advertising, "advertising is Null or Empty.");
			} else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV14", "updateProfilePreferences_phone",
			Group.ACCOUNTS, Group.APIREG })
	public void testUpdateProfilePreferences_phone(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: updateProfilePreferences- update phone test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with updated profile Preferences details.");
		Reporter.log("Step 2: Verify updated profile Preferences details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		String marketingPhone = null;
		List<String> keys = new ArrayList<String>();
		keys.add("userID");
		keys.add("marketing.phone");
		Map<String, String> values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertNotNull(values.get("marketing.phone"), "marketingPhone is Null or Empty.");

		String marketingPhoneUpdated = invertBooleanValue(values.get("marketing.phone"));
		tokenMap.put("phone", marketingPhoneUpdated);
		tokenMap.put("userID", values.get("userID"));
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfilePreferences.txt");
		values = findElementsUpdateProfilePreferneces(apiTestData, keys, requestBody,tokenMap);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("marketing.phone"), marketingPhoneUpdated, "marketingPhone is invalid.");

		values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("marketing.phone"), marketingPhoneUpdated, "marketingPhone is invalid.");

		marketingPhoneUpdated = invertBooleanValue(marketingPhone);

		tokenMap.put("phone", marketingPhoneUpdated);

		values = findElementsUpdateProfilePreferneces(apiTestData, keys, requestBody,tokenMap);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("marketing.phone"), marketingPhoneUpdated, "marketingPhone is invalid.");

		values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("marketing.phone"), marketingPhoneUpdated, "marketingPhone is invalid.");
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV14", "updateProfilePreferences_email",
			Group.ACCOUNTS, Group.APIREG })
	public void testUpdateProfilePreferences_email(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: updateProfilePreferences- update email test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with updated profile Preferences details.");
		Reporter.log("Step 2: Verify updated profile Preferences details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		String marketingEmail = null;
		List<String> keys = new ArrayList<String>();
		keys.add("userID");
		keys.add("marketing.email");
		Map<String, String> values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertNotNull(values.get("marketing.email"), "marketingEmail is Null or Empty.");

		String marketingEmailUpdated = invertBooleanValue(values.get("marketing.email"));
		tokenMap.put("email", marketingEmailUpdated);
		tokenMap.put("userID", values.get("userID"));
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfilePreferences_email.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		values = findElementsUpdateProfilePreferneces(apiTestData, keys, requestBody,tokenMap);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("marketing.email"), marketingEmailUpdated, "marketingEmail is invalid.");

		values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("marketing.email"), marketingEmailUpdated, "marketingEmail is invalid.");

		marketingEmailUpdated = invertBooleanValue(marketingEmail);

		tokenMap.put("email", marketingEmailUpdated);
		//updatedRequest = prepareRequestParam(requestBody, tokenMap);

		values = findElementsUpdateProfilePreferneces(apiTestData, keys, requestBody,tokenMap);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("marketing.email"), marketingEmailUpdated, "marketingEmail is invalid.");

		values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("marketing.email"), marketingEmailUpdated, "marketingEmail is invalid.");
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV14",
			"updateProfilePreferences_otherLinesAlerts", Group.ACCOUNTS, Group.APIREG })
	public void testUpdateProfilePreferences_otherLinesAlerts(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: updateProfilePreferences- update notification.otherLinesAlerts test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with updated profile Preferences details.");
		Reporter.log("Step 2: Verify updated profile Preferences details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		String notificationOtherLinesAlerts = null;
		List<String> keys = new ArrayList<String>();
		keys.add("userID");
		keys.add("notification.otherLinesAlerts");
		Map<String, String> values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertNotNull(values.get("notification.otherLinesAlerts"),
				"notification.otherLinesAlerts is Null or Empty.");

		String notificationOtherLinesAlertsUpdated = invertBooleanValue(values.get("notification.otherLinesAlerts"));
		tokenMap.put("otherLinesAlerts", notificationOtherLinesAlertsUpdated);
		tokenMap.put("userID", values.get("userID"));
		String requestBody = new ServiceTest()
				.getRequestFromFile("Profile_updateProfilePreferences_otherLinesAlerts.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		values = findElementsUpdateProfilePreferneces(apiTestData, keys, requestBody,tokenMap);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("notification.otherLinesAlerts"), notificationOtherLinesAlertsUpdated,
				"notification.otherLinesAlerts is invalid.");

		values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("notification.otherLinesAlerts"), notificationOtherLinesAlertsUpdated,
				"notification.otherLinesAlerts is invalid.");

		notificationOtherLinesAlertsUpdated = invertBooleanValue(notificationOtherLinesAlerts);

		tokenMap.put("otherLinesAlerts", notificationOtherLinesAlertsUpdated);
		//updatedRequest = prepareRequestParam(requestBody, tokenMap);

		values = findElementsUpdateProfilePreferneces(apiTestData, keys, requestBody,tokenMap);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("notification.otherLinesAlerts"), notificationOtherLinesAlertsUpdated,
				"notification.otherLinesAlerts is invalid.");

		values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("notification.otherLinesAlerts"), notificationOtherLinesAlertsUpdated,
				"notification.otherLinesAlerts is invalid.");
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV14", "updateProfilePreferences_iba",
			Group.ACCOUNTS, Group.APIREG })
	public void testUpdateProfilePreferences_iba(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: updateProfilePreferences- update advertising.iba test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with updated profile Preferences details.");
		Reporter.log("Step 2: Verify updated profile Preferences details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		List<String> keys = new ArrayList<String>();
		keys.add("optOutIBA");
		Map<String, String> values = findElementsGetProfile(apiTestData, keys);
		Assert.assertNotNull(values.get("optOutIBA"), "optOutIBA is Null or Empty.");

		String advertisingIba = null;
		String advertisingDeviceLocation = null;
		String advertisingWebBrowsingInfo = null;

		keys = new ArrayList<String>();
		keys.add("userID");
		keys.add("advertising.deviceLocation");
		keys.add("advertising.iba");
		keys.add("advertising.webBrowsingInfo");
		values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertNotNull(values.get("advertising.iba"), "advertising.iba is Null or Empty.");
		advertisingIba = values.get("advertising.iba");
		advertisingDeviceLocation = values.get("advertising.deviceLocation");
		advertisingWebBrowsingInfo = values.get("advertising.webBrowsingInfo");

		String advertisingIbaUpdated = "false";// invertBooleanValue(values.get("advertising.iba"));
		String advertisingDeviceLocationUpdated = "false";// invertBooleanValue(values.get("advertising.deviceLocation"));
		String advertisingWebBrowsingInfoUpdated = "false";// invertBooleanValue(values.get("advertising.webBrowsingInfo"));
		tokenMap.put("iba", advertisingIbaUpdated);
		tokenMap.put("deviceLocation", advertisingDeviceLocationUpdated);
		tokenMap.put("webBrowsingInfo", advertisingWebBrowsingInfoUpdated);
		tokenMap.put("userID", values.get("userID"));
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfilePreferences_iba.txt");
		//String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		values = findElementsUpdateProfilePreferneces(apiTestData, keys, requestBody,tokenMap);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(advertisingIba, advertisingIbaUpdated, "advertising.iba not updated.");

		keys.add("optOutIBA");
		values = findElementsGetProfile(apiTestData, keys);
		Assert.assertNotNull(values.get("optOutIBA"), "optOutIBA is Null or Empty.");

		keys.add("userID");
		keys.add("advertising.iba");
		values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertNotNull(values.get("advertising.iba"), "advertising.iba is Null or Empty.");
		advertisingIba = values.get("advertising.iba");

		/****/

		values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("advertising.iba"), advertisingIbaUpdated, "advertising.iba is invalid.");

		advertisingIbaUpdated = invertBooleanValue(advertisingIba);

		tokenMap.put("iba", advertisingIbaUpdated);
		//updatedRequest = prepareRequestParam(requestBody, tokenMap);

		requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfilePreferences_iba_reset.txt");
		keys = new ArrayList<String>();
		keys.add("advertising.iba");
		keys.add("userID");
		values = findElementsUpdateProfilePreferneces(apiTestData, keys, requestBody,tokenMap);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		Assert.assertEquals(values.get("advertising.iba"), advertisingIbaUpdated, "advertising.iba is invalid.");

		values = findElementInGetProfilePreferences(apiTestData, keys);
		Assert.assertNotNull(values.get("userID"), "userID is Null or Empty.");
		// Assert.assertEquals(values.get("advertising.iba"),advertisingIbaUpdated,"advertising.iba
		// is invalid.");

		keys.add("optOutIBA");
		values = findElementsGetProfile(apiTestData, keys);
		Assert.assertNotNull(values.get("optOutIBA"), "optOutIBA is Null or Empty.");
	}

	public Map<String, String> findElementsGetProfile(ApiTestData apiTestData, List<String> elements) throws Exception {
		Response response = getProfile(apiTestData, tokenMap);
		String operationName = "getProfile";
		Map<String, String> elementsMap = new HashMap<String, String>();
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode parentNode = jsonNode.path("lines");
			if (!parentNode.isMissingNode() && parentNode.isArray()) {
				for (int i = 0; i < parentNode.size(); i++) {
					JsonNode dataNode = parentNode.get(i);
					if (dataNode.isObject()) {
						String msisdn = dataNode.path("msisdn").textValue();
						if (msisdn.equals(apiTestData.getMsisdn())) {
							String optOutIBA = dataNode.path("optOutIBA").textValue();
							elementsMap.put("optOutIBA", optOutIBA);
							break;
						}
					}
				}
			}
		}
		return elementsMap;
	}

	public Map<String, String> findElementsUpdateProfilePreferneces(ApiTestData apiTestData, List<String> elements,
			String request,Map<String,String> tokenMap) throws Exception {
		Response response = updateProfilePreferences(apiTestData, request, tokenMap);
		String operationName = "updateProfilePreferences";
		Map<String, String> elementsMap = new HashMap<String, String>();
		logRequest(request, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Iterator<String> itr = elements.iterator();
				while (itr.hasNext()) {
					String elementName = itr.next().toString();
					elementsMap.put(elementName, getPathVal(jsonNode, elementName));
				}
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return elementsMap;
	}

	public Map<String, String> findElementInGetProfilePreferences(ApiTestData apiTestData, List<String> elements)
			throws Exception, IOException {
		Response response = getProfilePreferences(apiTestData, tokenMap);
		String operationName = "getProfilePreferences";
		Map<String, String> elementsMap = new HashMap<String, String>();
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Iterator<String> itr = elements.iterator();
				while (itr.hasNext()) {
					String elementName = itr.next().toString();
					elementsMap.put(elementName, getPathVal(jsonNode, elementName));
				}
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return elementsMap;
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV1", "getProfilePaperlessBillingDetails",
			Group.ACCOUNTS, Group.APIREG })
	public void testGetProfilePaperlessBillingDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: PaperlessBillingDetails test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with profile Preferences details.");
		Reporter.log("Step 2: Verify profile Preferences details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("pahMsisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());

		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfilePaperlessBillingDetails_disable.txt");
		
		Response response = updateProfilePreferences(apiTestData, requestBody, tokenMap);
		String operationName = "updateProfilePaperlessBillingDetails";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String autoPay = getPathVal(jsonNode, "autoPay.enabled");
				Assert.assertNotNull(autoPay, "autoPay is Null or Empty.");

				String deliveryMethod = getPathVal(jsonNode, "billFormat.deliveryMethod");
				Assert.assertNotNull(deliveryMethod, "deliveryMethod is Null or Empty.");

				String paperlessErolled = getPathVal(jsonNode, "billFormat.paperlessErolled");
				Assert.assertNotNull(paperlessErolled, "paperlessErolled is Null or Empty.");

				String emailOptIn = getPathVal(jsonNode, "billFormat.paperless.emailOptIn");
				Assert.assertNotNull(emailOptIn, "emailOptIn is Null or Empty.");

				String textOptIn = getPathVal(jsonNode, "billFormat.paperless.textOptIn");
				Assert.assertNotNull(textOptIn, "textOptIn is Null or Empty.");

				String email = getPathVal(jsonNode, "billFormat.paperless.email");
				Assert.assertNotNull(email, "email is Null or Empty.");

				String billDetailLevel = getPathVal(jsonNode, "billFormat.paper.billDetailLevel");
				Assert.assertNotNull(billDetailLevel, "billDetailLevel is Null or Empty.");

				String pahMsisdn = getPathVal(jsonNode, "billFormat.pahMsisdn");
				Assert.assertNotNull(pahMsisdn, "pahMsisdn is Null or Empty.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfilePaperlessBillingDetails.txt");
		response = updateProfilePreferences(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String autoPay = getPathVal(jsonNode, "autoPay.enabled");
				Assert.assertNotNull(autoPay, "autoPay is Null or Empty.");

				String deliveryMethod = getPathVal(jsonNode, "billFormat.deliveryMethod");
				Assert.assertNotNull(deliveryMethod, "deliveryMethod is Null or Empty.");

				String paperlessErolled = getPathVal(jsonNode, "billFormat.paperlessErolled");
				Assert.assertNotNull(paperlessErolled, "paperlessErolled is Null or Empty.");

				String emailOptIn = getPathVal(jsonNode, "billFormat.paperless.emailOptIn");
				Assert.assertNotNull(emailOptIn, "emailOptIn is Null or Empty.");

				String textOptIn = getPathVal(jsonNode, "billFormat.paperless.textOptIn");
				Assert.assertNotNull(textOptIn, "textOptIn is Null or Empty.");

				String email = getPathVal(jsonNode, "billFormat.paperless.email");
				Assert.assertNotNull(email, "email is Null or Empty.");

				String billDetailLevel = getPathVal(jsonNode, "billFormat.paper.billDetailLevel");
				Assert.assertNotNull(billDetailLevel, "billDetailLevel is Null or Empty.");

				String pahMsisdn = getPathVal(jsonNode, "billFormat.pahMsisdn");
				Assert.assertNotNull(pahMsisdn, "pahMsisdn is Null or Empty.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		response = getProfilePaperlessBillingDetails(apiTestData, tokenMap);
		operationName = "getProfilePaperlessBillingDetails";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String autoPay = getPathVal(jsonNode, "autoPay.enabled");
				Assert.assertNotNull(autoPay, "autoPay is Null or Empty.");

				String deliveryMethod = getPathVal(jsonNode, "billFormat.deliveryMethod");
				Assert.assertNotNull(deliveryMethod, "deliveryMethod is Null or Empty.");

				String paperlessErolled = getPathVal(jsonNode, "billFormat.paperlessErolled");
				Assert.assertNotNull(paperlessErolled, "paperlessErolled is Null or Empty.");

				String emailOptIn = getPathVal(jsonNode, "billFormat.paperless.emailOptIn");
				Assert.assertNotNull(emailOptIn, "emailOptIn is Null or Empty.");

				String textOptIn = getPathVal(jsonNode, "billFormat.paperless.textOptIn");
				Assert.assertNotNull(textOptIn, "textOptIn is Null or Empty.");

				String email = getPathVal(jsonNode, "billFormat.paperless.email");
				Assert.assertNotNull(email, "email is Null or Empty.");

				/*
				 * String paper= getPathVal(jsonNode, "billFormat.paper");
				 * Assert.assertNotNull(paper,"paper is Null or Empty.");
				 */

			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV1",
			"updateProfilePaperlessBillingDetails", Group.ACCOUNTS, Group.APIREG })
	public void testUpdateProfilePaperlessBillingDetails(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: updateProfilePaperlessBillingDetails test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with updated profile PaperlessBillingDetails Preferences details.");
		Reporter.log("Step 2: Verify updated profile PaperlessBillingDetails returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("pahMsisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());

		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfilePaperlessBillingDetails_disable.txt");
		Response response = updateProfilePreferences(apiTestData, requestBody, tokenMap);
		String operationName = "updateProfilePaperlessBillingDetails";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String autoPay = getPathVal(jsonNode, "autoPay.enabled");
				Assert.assertNotNull(autoPay, "autoPay is Null or Empty.");

				String deliveryMethod = getPathVal(jsonNode, "billFormat.deliveryMethod");
				Assert.assertNotNull(deliveryMethod, "deliveryMethod is Null or Empty.");

				String paperlessErolled = getPathVal(jsonNode, "billFormat.paperlessErolled");
				Assert.assertNotNull(paperlessErolled, "paperlessErolled is Null or Empty.");

				String emailOptIn = getPathVal(jsonNode, "billFormat.paperless.emailOptIn");
				Assert.assertNotNull(emailOptIn, "emailOptIn is Null or Empty.");

				String textOptIn = getPathVal(jsonNode, "billFormat.paperless.textOptIn");
				Assert.assertNotNull(textOptIn, "textOptIn is Null or Empty.");

				String email = getPathVal(jsonNode, "billFormat.paperless.email");
				Assert.assertNotNull(email, "email is Null or Empty.");

				String billDetailLevel = getPathVal(jsonNode, "billFormat.paper.billDetailLevel");
				Assert.assertNotNull(billDetailLevel, "billDetailLevel is Null or Empty.");

				String pahMsisdn = getPathVal(jsonNode, "billFormat.pahMsisdn");
				Assert.assertNotNull(pahMsisdn, "pahMsisdn is Null or Empty.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfilePaperlessBillingDetails.txt");
		response = updateProfilePreferences(apiTestData, requestBody, tokenMap);
		operationName = "updateProfilePaperlessBillingDetails";
		logRequest(requestBody, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String autoPay = getPathVal(jsonNode, "autoPay.enabled");
				Assert.assertNotNull(autoPay, "autoPay is Null or Empty.");

				String deliveryMethod = getPathVal(jsonNode, "billFormat.deliveryMethod");
				Assert.assertNotNull(deliveryMethod, "deliveryMethod is Null or Empty.");

				String paperlessErolled = getPathVal(jsonNode, "billFormat.paperlessErolled");
				Assert.assertNotNull(paperlessErolled, "paperlessErolled is Null or Empty.");

				String emailOptIn = getPathVal(jsonNode, "billFormat.paperless.emailOptIn");
				Assert.assertNotNull(emailOptIn, "emailOptIn is Null or Empty.");

				String textOptIn = getPathVal(jsonNode, "billFormat.paperless.textOptIn");
				Assert.assertNotNull(textOptIn, "textOptIn is Null or Empty.");

				String email = getPathVal(jsonNode, "billFormat.paperless.email");
				Assert.assertNotNull(email, "email is Null or Empty.");

				String billDetailLevel = getPathVal(jsonNode, "billFormat.paper.billDetailLevel");
				Assert.assertNotNull(billDetailLevel, "billDetailLevel is Null or Empty.");

				String pahMsisdn = getPathVal(jsonNode, "billFormat.pahMsisdn");
				Assert.assertNotNull(pahMsisdn, "pahMsisdn is Null or Empty.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV1", "getProfileDigits", Group.ACCOUNTS,
			Group.APIREG })
	public void testGetProfileDigits(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getProfileDigits test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with profile digits details.");
		Reporter.log("Step 2: Verify profile digits details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		Response response = getProfileDigits(apiTestData, tokenMap);
		String operationName = "getProfileDigits";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {

				String count = jsonNode.path("count").asText();
				Assert.assertEquals(count, "0", "digit count is Zero.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV123", "updateProfileFullPermission",
			Group.ACCOUNTS, Group.APIREG })
	public void testUpdateProfileFullPermission(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: updateProfile test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with updated profile details.");
		Reporter.log("Step 2: Verify updated profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("nonprimary", apiTestData.getPrimaryMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("permission", "FULL");
		Map<String, String> resultMap = null;
		List<String> elements = new ArrayList<String>();
		resultMap = getProfileRespElementsMap(tokenMap, apiTestData, elements);
		String operationName = "Profile_updateProofile";
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProofile_Permission.txt");
		Response response = updateProfile(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode lines = jsonNode.path("lines");
			if (!lines.isMissingNode() && lines.isArray()) {
				JsonNode line = lines.get(0);
				String msisdn = line.path("msisdn").asText();
				String permission = line.path("permission").asText();
				Assert.assertEquals(permission, "FULL", "Invalid permission.");
				Assert.assertEquals(msisdn, apiTestData.getPrimaryMsisdn(), "Invalid msisdn.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		
		resetPermission(apiTestData,tokenMap,resultMap,operationName);
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV123",
			"testUpdateProfileStandardPermission", Group.ACCOUNTS, Group.APIREG })
	public void testUpdateProfileStandardPermission(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: updateProfile test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with updated profile details.");
		Reporter.log("Step 2: Verify updated profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("nonprimary", apiTestData.getPrimaryMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("permission", "STANDARD");
		Map<String, String> resultMap = null;
		List<String> elements = new ArrayList<String>();
		resultMap = getProfileRespElementsMap(tokenMap, apiTestData, elements);
		String operationName = "Profile_updateProofile";
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProofile_Permission.txt");
		Response response = updateProfile(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode lines = jsonNode.path("lines");
			if (!lines.isMissingNode() && lines.isArray()) {
				JsonNode line = lines.get(0);
				String msisdn = line.path("msisdn").asText();
				String permission = line.path("permission").asText();
				Assert.assertEquals(permission, "STANDARD", "Invalid permission.");
				Assert.assertEquals(msisdn, apiTestData.getPrimaryMsisdn(), "Invalid msisdn.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		
		resetPermission(apiTestData,tokenMap,resultMap,operationName);
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV123",
			"testUpdateProfileRestrictedPermission", Group.ACCOUNTS, Group.APIREG })
	public void testUpdateProfileRestrictedPermission(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: updateProfile test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with updated profile details.");
		Reporter.log("Step 2: Verify updated profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("nonprimary", apiTestData.getPrimaryMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("permission", "RESTRICTED");
		Map<String, String> resultMap = null;
		List<String> elements = new ArrayList<String>();
		resultMap = getProfileRespElementsMap(tokenMap, apiTestData, elements);
		String operationName = "Profile_updateProofile";
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProofile_Permission.txt");
		Response response = updateProfile(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode lines = jsonNode.path("lines");
			if (!lines.isMissingNode() && lines.isArray()) {
				JsonNode line = lines.get(0);
				String msisdn = line.path("msisdn").asText();
				String permission = line.path("permission").asText();
				Assert.assertEquals(permission, "RESTRICTED", "Invalid permission.");
				Assert.assertEquals(msisdn, apiTestData.getPrimaryMsisdn(), "Invalid msisdn.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		resetPermission(apiTestData,tokenMap,resultMap,operationName);
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV123",
			"testUpdateProfileNoAccessPermission", Group.ACCOUNTS, Group.APIREG })
	public void testUpdateProfileNoAccessPermission(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: updateProfile test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with updated profile details.");
		Reporter.log("Step 2: Verify updated profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("nonprimary", apiTestData.getPrimaryMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("permission", "NOACCESS");
		Map<String, String> resultMap = null;
		List<String> elements = new ArrayList<String>();
		resultMap = getProfileRespElementsMap(tokenMap, apiTestData, elements);
		String operationName = "Profile_updateProofile";	
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProofile_Permission.txt");
		Response response = updateProfile(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode lines = jsonNode.path("lines");
			if (!lines.isMissingNode() && lines.isArray()) {
				JsonNode line = lines.get(0);
				String msisdn = line.path("msisdn").asText();
				String permission = line.path("permission").asText();
				Assert.assertEquals(permission, "NOACCESS", "Invalid permission.");
				Assert.assertEquals(msisdn, apiTestData.getPrimaryMsisdn(), "Invalid msisdn.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		
		resetPermission(apiTestData,tokenMap,resultMap,operationName);
	}
	
	public void resetPermission(ApiTestData apiTestData,Map<String, String> tokenMap,Map<String, String> resultMap,String operationName)throws Exception {
		/*****Reset-permission*****/
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProofile_Permission.txt");
		tokenMap.put("permission", resultMap.get(apiTestData.getPrimaryMsisdn()));
		Response response = updateProfile(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			JsonNode lines = jsonNode.path("lines");
			if (!lines.isMissingNode() && lines.isArray()) {
				JsonNode line = lines.get(0);
				String msisdn = line.path("msisdn").asText();
				String permission = line.path("permission").asText();
				Assert.assertEquals(permission, resultMap.get(apiTestData.getPrimaryMsisdn()), "Invalid permission.");
				Assert.assertEquals(msisdn, apiTestData.getPrimaryMsisdn(), "Invalid msisdn.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV1", "govAcNonMasterPermission",
			Group.ACCOUNTS, Group.SPRINT })
	public void testGetProfile_GM_NM_NP_To_GM_NM_P(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getProfile government customer test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with government customer profile details.");
		Reporter.log("Step 2: Verify government customer profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		List<String> elements = new ArrayList<String>();
		Map<String, String> resultMap = null;

		/********************** View Master **************************/
		resultMap = getProfileRespElementsMap(tokenMap, apiTestData, elements);
		Assert.assertEquals(resultMap.get("govAcNonMasterPermission"), "GMP_NM_NP",
				"govAcNonMasterPermission isInvalid.");

		updateMlCustTypeRespElementsMap(tokenMap, apiTestData, elements, "GMP_NM_P");
		Assert.assertEquals(resultMap.get("updateSuccessful"), "true", "updateSuccessful is Invalid.");

		resultMap = getProfileRespElementsMap(tokenMap, apiTestData, elements);
		Assert.assertEquals(resultMap.get("govAcNonMasterPermission"), "GMP_NM_P",
				"govAcNonMasterPermission is Invalid.");

		/********************** View Non-Master **************************/
		tokenMap.put("msisdn", apiTestData.getPrimaryMsisdn());
		resultMap = getProfileRespElementsMap(tokenMap, apiTestData, elements);
		Assert.assertEquals(resultMap.get("permission"), "NONMASTER_PERMISSIONED", "Permission Invalid msisdn.");
		/********************** View Non-Master **************************/

		tokenMap.put("msisdn", apiTestData.getMsisdn());
		updateMlCustTypeRespElementsMap(tokenMap, apiTestData, elements, "GMP_NM_P");
		Assert.assertEquals(resultMap.get("updateSuccessful"), "true", "updateSuccessful is Invalid.");

		resultMap = getProfileRespElementsMap(tokenMap, apiTestData, elements);
		Assert.assertEquals(resultMap.get("govAcNonMasterPermission"), "GMP_NM_P", "Permission Invalid msisdn.");
	}

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ProfileApiV12", "govMasterPermission",
			Group.ACCOUNTS, Group.SPRINT })
	public void testGetProfile_GM_NM_P_To_GM_NM_NP(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getProfile government customer test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with government customer profile details.");
		Reporter.log("Step 2: Verify government customer profile details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		//tokenMap.put("ban", apiTestData.getBan());
		//tokenMap.put("msisdn", apiTestData.getMsisdn());
		//tokenMap.put("password", apiTestData.getPassword());
		List<String> elements = new ArrayList<String>();
		Map<String, String> resultMap = null;

		/********************** View Master **************************/
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		resultMap = getProfileRespElementsMap(tokenMap, apiTestData, elements);
		Assert.assertEquals(resultMap.get("govAcNonMasterPermission"), "GMP_NM_P",
				"govAcNonMasterPermission isInvalid.");

		tokenMap.put("msisdn", apiTestData.getMsisdn());
		resultMap = updateMlCustTypeRespElementsMap(tokenMap, apiTestData, elements, "GMP_NM_NP");
		Assert.assertEquals(resultMap.get("updateSuccessful"), "true", "updateSuccessful is Invalid.");

		tokenMap.put("msisdn", apiTestData.getMsisdn());
		resultMap = getProfileRespElementsMap(tokenMap, apiTestData, elements);
		Assert.assertEquals(resultMap.get("govAcNonMasterPermission"), "GMP_NM_NP",
				"govAcNonMasterPermission is Invalid.");

		/********************** View Non-Master **************************/
		tokenMap.put("msisdn", apiTestData.getPrimaryMsisdn());
		resultMap = getProfileRespElementsMap(tokenMap, apiTestData, elements);
		Assert.assertEquals(resultMap.get("permission"), "NONMASTER_RESTRICTED", "Permission Invalid msisdn.");
		/********************** View Non-Master **************************/

		tokenMap.put("msisdn", apiTestData.getMsisdn());
		updateMlCustTypeRespElementsMap(tokenMap, apiTestData, elements, "GMP_NM_P");
		Assert.assertEquals(resultMap.get("updateSuccessful"), "true", "updateSuccessful is Invalid.");

		resultMap = getProfileRespElementsMap(tokenMap, apiTestData, elements);
		Assert.assertEquals(resultMap.get("govAcNonMasterPermission"), "GMP_NM_P", "Permission Invalid msisdn.");
	}

}
