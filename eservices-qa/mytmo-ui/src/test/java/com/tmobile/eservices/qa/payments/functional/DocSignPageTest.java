package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.DocuSignPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class DocSignPageTest extends PaymentCommonLib {

	/*
	 * US481195 :Setup Installment Plan - Page Load DocuSign Call
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPJODDocuSignPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US481195 | Setup Installment Plan - Page Load DocuSign Call");
		Reporter.log("Data Condition | EIP-JOD Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Navigate to EIP/JOD page' | EIPJOD page should be displayed");
		Reporter.log("5. Click Take Action on Active EIP' | EIPJOD lease ending page should be displayed");
		Reporter.log("6. Click Setup Installments Button' | POIP payment page should be displayed");
		Reporter.log(
				"7. Add Payment Method and Click on Agree Submit Button' | DocuSign page with EIP lease details  should be displayed");
		Reporter.log("8. Verify EIP lease deatils' |  EIP lease details  are verified");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		DocuSignPage sDocSignPage = navigateToPoipDocUsignPage(myTmoData);
		sDocSignPage.clickDisclosureCheckBox();
		sDocSignPage.clickContinueButton();

	}

}