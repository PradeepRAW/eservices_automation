package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.ConsolidatedRatePlanPage;
import com.tmobile.eservices.qa.pages.shop.DeviceIntentPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.shop.UNOWatchPDPPage;
import com.tmobile.eservices.qa.pages.shop.UNOWatchPLPPage;
import com.tmobile.eservices.qa.pages.shop.WatchPlanPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class RatePlanTest extends ShopCommonLib {

	/**
	 * US378701 - AAL - Consolidated Charges Page: Monthly Costs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testTIplanCustomersMonthlyCostsInRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US378701 - AAL - Consolidated Charges  Page: Monthly Costs");
		Reporter.log("Data Condition |  PAH Customer with TI plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Deive | PDP page should be displayed");
		Reporter.log("6. click on 'AAL' CTA | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log("7. Verify ' DUE MONTHLY' text | ' DUE MONTHLY' text should be displayed");
		Reporter.log(
				"8. Verify ' DUE MONTHLY' amount with dollar sign($) | ' DUE MONTHLY' amount with doller sign($) should be displayed");
		Reporter.log("9. Verify 'Friendly' plan name | 'Friendly' plan name should be displayed");
		Reporter.log("10. Click 'Friendly' plan name | 'Friendly' plan Header should be displayed");
		Reporter.log("11. Verify Plan model description | Plan model description should be displayed");
		Reporter.log("12. Click close(X) icon | Plan model window header should not be displayed");
		Reporter.log("13. Verify Auto pay text| Auto pay text should be displayed");
		Reporter.log(
				"14. Verify 'TI plan Taxes and fees included' text | Taxes and fees text should be displayed for TI plan Customers");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyDueMonthlyText();
		consolidatedRatePlanPage.verifyDueMonthlyAmount();
		consolidatedRatePlanPage.verifyFriendlyPlanName();
		consolidatedRatePlanPage.clickFriendlyPlanName();
		consolidatedRatePlanPage.verifyModelHeader();
		consolidatedRatePlanPage.verifyModelDescription();
		consolidatedRatePlanPage.clickModelCloseIcon();
		consolidatedRatePlanPage.verifyPlanModelWindowHeaderNotDisplayed();
		// consolidatedRatePlanPage.verifyAutoPayAuthorableText();
		consolidatedRatePlanPage.verifyTaxesAndFeesIncludedTextForTIcustomers();

	}

	/**
	 * US378701 - AAL - Consolidated Charges Page: Monthly Costs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testTEplanCustomersMonthlyCostsInRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US378701 - AAL - Consolidated Charges  Page: Monthly Costs");
		Reporter.log("Data Condition |  PAH Customer with TE plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Deive | PDP page should be displayed");
		Reporter.log("6. click on 'AAL' CTA | Consolidated Rate Plan Page  should be displayed  ");
		Reporter.log("7. Verify ' DUE MONTHLY' text | ' DUE MONTHLY' text should be displayed");
		Reporter.log(
				"8. Verify ' DUE MONTHLY' amount with doller sign($) | ' DUE MONTHLY' amount with doller sign($) should be displayed");
		Reporter.log("9. Verify 'Friendly' plan name | 'Friendly' plan name should be displayed");
		Reporter.log("10. Click 'Friendly' plan name | 'Friendly' plan Header should be displayed");
		Reporter.log("11. Verify Plan model description | Plan model description should be displayed");
		Reporter.log("12. Click close(X) icon | Plan model window header should not be displayed");
		Reporter.log("13. Verify Auto pay text| Auto pay text should be displayed");
		Reporter.log(
				"14. Verify 'If customer has a TE plan Taxes and fees not included' | Taxes and fees should not be displayed for TE plan Customers");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyDueMonthlyText();
		consolidatedRatePlanPage.verifyDueMonthlyAmount();
		consolidatedRatePlanPage.verifyFriendlyPlanName();
		consolidatedRatePlanPage.clickFriendlyPlanName();
		consolidatedRatePlanPage.verifyModelHeader();
		consolidatedRatePlanPage.verifyModelDescription();
		consolidatedRatePlanPage.clickModelCloseIcon();
		consolidatedRatePlanPage.verifyPlanModelWindowHeaderNotDisplayed();
		// consolidatedRatePlanPage.verifyAutoPayAuthorableText();
		consolidatedRatePlanPage.verifyTaxesAndFeesIncludedTextForTEcustomers();

	}

	/**
	 * US406141 : AAL - Consolidated Charges Page: Due Today costs BYOD (Deposit AND
	 * SIM) 1 * US400209 : AAL - Consolidated Charges Page: Due Today costs (SIM
	 * ONLY and DEPOSIT with SIM) - SIM KIT MODAL 2 US400209 : AAL - Consolidated
	 * Charges Page: Due Today costs (SIM ONLY and DEPOSIT with SIM) - SIM KIT MODAL
	 * 2 * US406143 : AAL - Consolidated Charges Page: Due Today costs BYOD (SIM
	 * only AND Deposit with Sim Kit) - DETAILS MODAL 3
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testPlanDetailsInRatePlanPageNavigatedFromDeviceIntentForMyOwnDevice(ControlTestData data,
			MyTmoData myTmoData) {

		Reporter.log("Test : US406141 - AAL - Consolidated Charges  Page: Due Today costs BYOD  (Deposit AND SIM) 1");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on add a line cta from quick links | Device intent page should be displayed");
		Reporter.log("5. Click on buy a new phone | Rate plan page should be displayed");
		Reporter.log("6. Verify 'DUE TODAY' text | 'DUE TODAY' text should be displayed");
		Reporter.log("7. Verify 'DUE TODAY' Amount | 'DUE TODAY' Amount should be displayed");
		Reporter.log("8.Verify Authorable deposit amount | Authorable deposit amount should be displayed");
		Reporter.log("9. click on details CTA | Details model should be displayed ");
		Reporter.log("10. Verify header 'Deposit' | Header 'Deposit' should be displayed ");
		Reporter.log("11. Verify Deposit description | Deposit description should be displayed ");
		Reporter.log("12. Verify  'Due Today' Amount   | 'Due Today' Amount  should be  Displayed");
		Reporter.log("13. Verify  'Due Monthly' Amount  | 'Due Monthly' Amount  should be  Displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyDueTodayText();
		consolidatedRatePlanPage.verifyDueTodayAmount();
		consolidatedRatePlanPage.verifyDueMonthlyAmount();
		consolidatedRatePlanPage.verifyDueMonthlyText();

	}

	/**
	 * US378700 : AAL - Consolidated Charges Page: Due Today costs (Deposit AND SIM)
	 * 1
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testNewPlanDetailsInRatePlanPageNavigatedFromDeviceIntent(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test : US378700 - AAL - Consolidated Charges Page: Due Today costs (Deposit AND SIM) 1");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on add a line cta from quick links | Device intent page should be displayed");
		Reporter.log("5. Click on buy a new phone | Rate plan page should be displayed");
		Reporter.log("6. Verify 'DUE TODAY' text | 'DUE TODAY' text should be displayed");
		Reporter.log("7. Verify 'DUE TODAY' Amount | 'DUE TODAY' Amount should be displayed");
		Reporter.log("8.Verify Authorable deposit amount | Authorable deposit amount should be displayed");
		Reporter.log("9. Verify Sim StarterKit text | Sim StarterKit text should be displayed");
		Reporter.log(
				"10. Verify Sim StarterKit Strike-out Amount | Sim StarterKit Strike-out Amount should be displayed");
		Reporter.log(
				"11. Verify Sim StarterKit After discount Amount | Sim StarterKit After discount Amount should be displayed");
		Reporter.log(
				"12. Verify  'Due Today' Amount   | 'Due Today' Amount  should be  Deposit + Sim Starter Kit cost minus any instant discounts on SIM");
		Reporter.log("13. Verify Promotion Icon and text | Promotion Icon and  text should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();

		consolidatedRatePlanPage.verifyDueTodayText();
		consolidatedRatePlanPage.verifyDueTodayAmount();
		Long dueTodayPrice = consolidatedRatePlanPage.getDueTodayPrice();
		consolidatedRatePlanPage.verifyAuthorableDepositAmount();
		Double depositAmount = consolidatedRatePlanPage.getDepositAmountUnderDueTodayPrice();
		consolidatedRatePlanPage.verifySIMStarterKitCTA();
		consolidatedRatePlanPage.verifySimStarterKitStrikePrice();
		consolidatedRatePlanPage.verifySimStarterKitAfterDiscountPrice();
		Double SimstaterKitAfterDiscountPrice = consolidatedRatePlanPage
				.getSimstaterKitAfterDiscountPriceUnderDueTodayPrice();

		Double dueTodaySum = depositAmount + SimstaterKitAfterDiscountPrice;
		consolidatedRatePlanPage.compareDueTodayAmount(dueTodayPrice, dueTodaySum.intValue());
		consolidatedRatePlanPage.verifyPromotionIcon();
		consolidatedRatePlanPage.verifyPromotionalText();

	}

	/**
	 * US398897 - AAL - Consolidated Charges Page: Monthly Costs - plan modal
	 * US378703 - AAL - Consolidated Charges Page: Hyperlink above CTA (legal)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testPlanModelWhenSelectPlanLinkInRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US398897 - AAL - Consolidated Charges Page: Monthly Costs - plan modal");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Deive | PDP page should be displayed");
		Reporter.log("6. click on 'AAL' CTA | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Verify 'DueMonthly' text | 'DueMonthly' text should be displayed");
		Reporter.log("8. Verify 'Friendly' plan name | 'Friendly' plan name should be displayed");
		Reporter.log("9. Click 'Friendly' plan name | 'Friendly' plan Header should be displayed");
		Reporter.log("10. Verify Plan model description | Plan model description should be displayed");
		Reporter.log("11. Click close(X) icon | Plan model window header should not be displayed");
		Reporter.log("12. Click on 'AAL' CTA | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log(
				"13. Verify 'See full plan terms' link in legal text | 'See full plan terms' link in legal text should be displayed ");
		Reporter.log("14. Click 'See full plan terms' link | NewWindow should be opened");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyDueMonthlyText();
		consolidatedRatePlanPage.verifyFriendlyPlanName();
		consolidatedRatePlanPage.clickFriendlyPlanName();
		consolidatedRatePlanPage.verifyModelHeader();
		consolidatedRatePlanPage.verifyModelDescription();
		consolidatedRatePlanPage.clickModelCloseIcon();
		consolidatedRatePlanPage.verifyPlanModelWindowHeaderNotDisplayed();
		consolidatedRatePlanPage.verifySeeFullPlanTermsLink();
		consolidatedRatePlanPage.clickSeeFullPlanTermsLink();
		consolidatedRatePlanPage.switchToWindow();
	}

	/**
	 * US485067:AAL - Deeplinking from TMO to MYTMO (phone/byod) - load blank
	 * "consolidated charges page"
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testAALDeeplinkFlowWithCookiesAndUserNavigateToRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US485067:AAL - Deeplinking from TMO to MYTMO (phone/byod) - load blank consolidated charges page");
		Reporter.log("Data Condition | PAH User With AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("5. Select Device | Device PDP page should be displayed");
		Reporter.log("6. Click on 'Add a Line' button | MyTmo login page should be displayed");
		Reporter.log("7. Enter valid user-id and Password | User should be navigated to RatePlan page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		launchTmngDMO(myTmoData);
		PlpPage phonesPage = new PlpPage(getDriver());
		phonesPage.checkPageIsReady();
		phonesPage.clickDeviceWithAvailability(myTmoData.getDeviceName());
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.verifyPhonePdpPageLoaded();
		phoneDetailsPage.clickOnAddALineBtn();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
	}

	/**
	 * US507072:Deeplinking from TMO to MYTMO (phone/byod) - load blank
	 * "consolidated charges page" - modal behavior
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPahCustomersNavigateRatePlanPageUsingDeepLinking(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test case: US507072:Deeplinking from TMO to MYTMO (phone/byod) - load blank 'consolidated charges page' - modal behavior");
		Reporter.log("Data Condition | PAH User and Customer eligible for AAL");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch MyTmo application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Launch TMO application | TMO Application Should be Launched");
		Reporter.log("5. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("6. Select Device | Device PDP page should be displayed");
		Reporter.log("7. Verify AAL button | AAL button should be displayed");
		Reporter.log("8. Click AAL button | Consolidated Rate Plan Page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		launchTmngDMO(myTmoData);
		PlpPage phonesPage = new PlpPage(getDriver());
		phonesPage.checkPageIsReady();
		phonesPage.clickDeviceWithAvailability(myTmoData.getDeviceName());
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.verifyPhonePdpPageLoaded();
		phoneDetailsPage.clickOnAddALineBtn();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
	}

	/**
	 * US507072:Deeplinking from TMO to MYTMO (phone/byod) - load blank
	 * "consolidated charges page" - modal behavior
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testNonPahCustomersEligibilityCheckInRatePlanPageUsingDeepLinking(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test case: US507072:Deeplinking from TMO to MYTMO (phone/byod) - load blank 'consolidated charges page' - modal behavior");
		Reporter.log("Data Condition | NON-PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch MyTmo application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Launch TMO application | TMO Application Should be Launched");
		Reporter.log("5. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("6. Select Device | Device PDP page should be displayed");
		Reporter.log("7. Verify AAL button | AAL button should be displayed");
		Reporter.log("8. Click AAL button | Error model shoiuld be displayed");
		Reporter.log(
				"9. Click on Error model close 'X' icon | Error model should be closed and user should navigate to shop page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		launchTmngDMO(myTmoData);
		PlpPage phonesPage = new PlpPage(getDriver());
		phonesPage.checkPageIsReady();
		phonesPage.clickDeviceWithAvailability(myTmoData.getDeviceName());
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.verifyPhonePdpPageLoaded();
		phoneDetailsPage.clickOnAddALineBtn();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyPageUrl();
		consolidatedRatePlanPage.verifyAalIneligibilityModalDisplayed();
		consolidatedRatePlanPage.clickOnAALIneligibilityModalCloseIcon();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyPageLoaded();
	}

	/**
	 * US400782 - AAL - Consolidated Charges Page: Due Today costs (SIM ONLY) 1
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testDueTodaySectionInRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US400782 - AAL - Consolidated Charges  Page: Due Today costs (SIM ONLY) 1");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on 'Add A LINE' button | Customer intent page should be displayed");
		Reporter.log("5. Select on 'Buy my own device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("6. Verify Contune button | Continue button should be displayed ");
		Reporter.log("7. Verify 'Due Today' Amount | 'Due Today' Amount should be displayed ");
		Reporter.log("8. Get 'Due Today' Amount | 'Due Today' Amount should be stored in V1");
		Reporter.log("9. Get 'Deposite' Amount | 'Deposite' Amount should be stored in V2");
		Reporter.log(
				"10. Verify Sim StarterKit Strike-out Amount | Sim StarterKit Strike-out Amount should be displayed");
		Reporter.log(
				"11. Verify Sim StarterKit After discount Amount | Sim StarterKit After discount Amount should be displayed");
		Reporter.log(
				"12. Get 'Sim StaterKit After Discount Amount' | 'Sim StaterKit After Discount Amount' should be stored in V3");
		Reporter.log("13. Verify Sim StarterKit text | Sim StarterKit text should be displayed");
		Reporter.log("14. Verify Promotion text | Promotion text should be displayed");
		Reporter.log(
				"15. Add Deposit amount and SimstarterKit amount(V2 + V3) | Deposit amount and SimstarterKit amount(V2 + V3) should be stored in V4");
		Reporter.log("16. Compare V1 and V4 amounts | Both amounts should be equal");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyContinueCTA();
		consolidatedRatePlanPage.verifyDueTodayAmount();
		Long dueTodayPrice = consolidatedRatePlanPage.getDueTodayPrice();
		Double depositAmount = consolidatedRatePlanPage.getDepositAmountUnderDueTodayPrice();
		consolidatedRatePlanPage.verifySimStarterKitStrikePrice();
		consolidatedRatePlanPage.verifySimStarterKitAfterDiscountPrice();
		Double SimstaterKitAfterDiscountPrice = consolidatedRatePlanPage
				.getSimstaterKitAfterDiscountPriceUnderDueTodayPrice();
		consolidatedRatePlanPage.verifySIMStarterKitCTA();
		consolidatedRatePlanPage.verifyPromotionalText();
		Double dueTodaySum = depositAmount + SimstaterKitAfterDiscountPrice;
		consolidatedRatePlanPage.compareDueTodayAmount(dueTodayPrice, dueTodaySum.intValue());

	}

	/**
	 * US406137 - AAL - Consolidated Charges Page: Due Today costs BYOD (SIM ONLY) 1
	 * US507509 :Consolidated Charges Page - remove footer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testDueTodaySectionWithUseMyOwnPhoneFlowInRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US406137 - AAL - Consolidated Charges Page: Due Today costs BYOD (SIM ONLY) 1");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Deive | PDP page should be displayed");
		Reporter.log("6. click on 'AAL' CTA | Customer intent page should be displayed ");
		Reporter.log("7. click on Use my own phone | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log("8. Veify Due Today section | Due Today section should be displayed ");
		Reporter.log("9. Veify Due Today amount | Due Today amount should be displayed ");
		Reporter.log("10. Veify SIM starter kit CTA | SIM starter kit should be displayed ");
		Reporter.log(
				"11. Veify strike through price for SIM starter kit CTA | Strike through price should be displayed for SIM starter kit when having instant discount");
		Reporter.log("12. Veify promotional text | Promotional text should be displayed ");
		Reporter.log("13. Verify Footers | Footers should be removed in Consolidated Rate Plan Page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyDueTodayAmount();
		consolidatedRatePlanPage.verifySIMStarterKitCTA();
		consolidatedRatePlanPage.verifyRatePlanPageFooterNotDisplayed();
		// consolidatedRatePlanPage.verifySimStarterKitStrikePrice(); StrikeThrough
		// Pricing is not available
	}

	/**
	 * US406140 : AAL - Consolidated Charges Page: Due Today costs BYOD (SIM ONLY)
	 * promotion text modal US406138 : AAL - Consolidated Charges Page: Due Today
	 * costs BYOD (SIM ONLY) 2
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPromotionalDetailsWhenHavingSIMInRatePlanPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log(
				"US406140 : AAL - Consolidated Charges  Page: Due Today costs BYOD (SIM ONLY) promotion text modal");
		Reporter.log("Data Condition |  PAH Customer with no deposit and ONLY a SIM KIT fee");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on add a line cta from quick links | Device intent page should be displayed");
		Reporter.log("5. Click on Use My Own Phone link |  Rate plan page should be displayed");
		Reporter.log("6. Veify CART promotion text for SIM CARD(If promotion) |Promotional text should be displayed ");
		Reporter.log(
				"7. Veify Promotion icon & Promotion details| Promotion icon & Promotion details should be displayed ");
		Reporter.log("8. Click promotional link |Promotional model should be displayed ");
		Reporter.log("9. Click promotional close button |Promotional model should be closed ");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");

		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();

		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyPromotionIcon();
		consolidatedRatePlanPage.verifyPromotionalText();
		consolidatedRatePlanPage.clickPromotionalLink();
		consolidatedRatePlanPage.verifyPromotionalModel();
		consolidatedRatePlanPage.clickPromotionalModelCloseButton();
	}

	/**
	 * AAL - Consolidated Charges Page: Due Today costs BYOD (Deposit and Sim Kit)
	 * promotion text modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPromotionalDetailsWhenHavingSIMandDepositInRatePlanPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log(
				"US406140 : AAL - Consolidated Charges  Page: Due Today costs BYOD (SIM ONLY) promotion text modal");
		Reporter.log("Data Condition |  PAH Customer with no deposit and ONLY a SIM KIT fee");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on add a line cta from quick links | Device intent page should be displayed");
		Reporter.log("5. Click on Use My Own Phone link |  Rate plan page should be displayed");
		Reporter.log(
				"6. Veify CART promotion text for SIM CARD and Deposit (If promotion) |Promotional text should be displayed ");
		Reporter.log(
				"7. Veify Promotion icon & Promotion details| Promotion icon & Promotion details should be displayed ");
		Reporter.log("8. Click promotional link |Promotional model should be displayed ");
		Reporter.log("9. Click promotional close button |Promotional model should be closed ");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");

		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();

		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyPromotionalText();
		consolidatedRatePlanPage.clickPromotionalLink();
		consolidatedRatePlanPage.verifyPromotionalModel();
		consolidatedRatePlanPage.clickPromotionalModelCloseButton();
	}

	/**
	 * US414860 - Trade In Messaging (Consolidated Charges Page) Trade in Device
	 * drop down/ sticky on Consolidated Charges Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testTradeInDetailsWhileAALOnRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US414860 - Trade In Messaging (Consolidated Charges Page)");
		Reporter.log(
				"Data Condition |  mytmo customer who has NOT been directed to the Customer Device Intent Page (deeplink/see all phones/ featured device flow)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Device | PDP page should be displayed");
		Reporter.log("6. click on 'AAL' CTA | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log(
				"7. Verify TradeIn details dropdown/sticky | components like Icon/Image, Authorizable text, Hyperlink should be displayed");
		Reporter.log("8. Click on Call CTA | Dial number popup should be displayed");
		Reporter.log("9. CloseDial number popup | Dial number popup should be closed");
		Reporter.log("10. Click on X | Dial number popup should be closed");
		Reporter.log(
				"11. Verify the page by performing refresh/selecting back button on the page | Sticky should be displayed always on the page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ConsolidatedRatePlanPage consolidatedRatePlanPage = navigateToCustomerIntentPageThroughAddALine(myTmoData);

		consolidatedRatePlanPage.verifyTradeInDetailsDropDownOrSticky();
		consolidatedRatePlanPage.verifyOnCallCta();
		consolidatedRatePlanPage.clickModalWindowCloseBtn();
		consolidatedRatePlanPage.refreshPage();
		consolidatedRatePlanPage.verifyTradeInDetailsDropDownOrSticky();
	}

	/**
	 * US406142 : AAL - Consolidated Charges Page: Due Today costs BYOD (Deposit and
	 * Sim Kit) 2
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testNewPlanDetailsInRatePlanPageNavigatedFromDeviceIntentForPromo(ControlTestData data,
			MyTmoData myTmoData) {

		Reporter.log(
				"Test : US406142 - AAL - Consolidated Charges  Page: Due Today costs BYOD (Deposit and Sim Kit) 2");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on add a line cta from quick links | Device intent page should be displayed");
		Reporter.log("5. Click on buy a new phone | Rate plan page should be displayed");
		Reporter.log("6. Verify 'DUE TODAY' text | 'DUE TODAY' text should be displayed");
		Reporter.log("7. Verify 'DUE TODAY' Amount | 'DUE TODAY' Amount should be displayed");
		Reporter.log("8.Verify Authorable deposit amount | Authorable deposit amount should be displayed");
		Reporter.log("9. Verify Sim StarterKit text | Sim StarterKit text should be displayed");
		Reporter.log(
				"10. Verify Sim StarterKit Strike-out Amount | Sim StarterKit Strike-out Amount should be displayed");
		Reporter.log(
				"11. Verify Sim StarterKit After discount Amount | Sim StarterKit After discount Amount should be displayed");
		Reporter.log(
				"12. Verify  'Due Today' Amount   | 'Due Today' Amount  should be  Deposit + Sim Starter Kit cost minus any instant discounts on SIM");
		Reporter.log("13. Verify Promotion Icon and text | Promotion Icon and  text should be displayed");
		Reporter.log("14.Verify details cta | Details cta should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();

		consolidatedRatePlanPage.verifyDueTodayText();
		consolidatedRatePlanPage.verifyDueTodayAmount();
		Long dueTodayPrice = consolidatedRatePlanPage.getDueTodayPrice();
		consolidatedRatePlanPage.verifyAuthorableDepositAmount();
		Double depositAmount = consolidatedRatePlanPage.getDepositAmountUnderDueTodayPrice();
		consolidatedRatePlanPage.verifySIMStarterKitCTA();
		consolidatedRatePlanPage.verifySimStarterKitStrikePrice();
		consolidatedRatePlanPage.verifySimStarterKitAfterDiscountPrice();
		Double SimstaterKitAfterDiscountPrice = consolidatedRatePlanPage
				.getSimstaterKitAfterDiscountPriceUnderDueTodayPrice();

		Double dueTodaySum = depositAmount + SimstaterKitAfterDiscountPrice;
		consolidatedRatePlanPage.compareDueTodayAmount(dueTodayPrice, dueTodaySum.intValue());
		consolidatedRatePlanPage.verifyPromotionIcon();
		consolidatedRatePlanPage.verifyPromotionalText();
		consolidatedRatePlanPage.verifyPlanDetailsCTA();
	}

	/**
	 * US378700 : AAL - Consolidated Charges Page: Due Today costs (Deposit AND SIM)
	 * 1
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testNewPlanDetailsInRatePlanPageNavigatedFromPDP(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test : US378700 - AAL - Consolidated Charges Page: Due Today costs (Deposit AND SIM) 1");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select a device | PDP page should be displayed");
		Reporter.log("6. click on 'AAL' CTA | Rate plan page should be displayed ");
		Reporter.log("7. Verify 'DUE TODAY' text | 'DUE TODAY' text should be displayed");
		Reporter.log("8. Verify 'DUE TODAY' Amount | 'DUE TODAY' Amount should be displayed");
		Reporter.log("9.Verify Authorable deposit amount | Authorable deposit amount should be displayed");
		Reporter.log("10. Verify Sim StarterKit text | Sim StarterKit text should be displayed");
		Reporter.log(
				"11. Verify Sim StarterKit After discount Amount | Sim StarterKit After discount Amount should be displayed");
		Reporter.log(
				"12. Verify  'Due Today' Amount   | 'Due Today' Amount  should be  Deposit + Sim Starter Kit cost minus any instant discounts on SIM");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.clickOnAddALineButton();

		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();

		consolidatedRatePlanPage.verifyDueTodayText();
		consolidatedRatePlanPage.verifyDueTodayAmount();
		Long dueTodayPrice = consolidatedRatePlanPage.getDueTodayPrice();
		consolidatedRatePlanPage.verifyAuthorableDepositAmount();
		Double depositAmount = consolidatedRatePlanPage.getDepositAmountUnderDueTodayPrice();
		consolidatedRatePlanPage.verifySIMStarterKitCTA();
		consolidatedRatePlanPage.verifySimStarterKitAfterDiscountPrice();
		Double SimstaterKitAfterDiscountPrice = consolidatedRatePlanPage
				.getSimstaterKitAfterDiscountPriceUnderDueTodayPrice();

		Double dueTodaySum = depositAmount + SimstaterKitAfterDiscountPrice;
		consolidatedRatePlanPage.compareDueTodayAmount(dueTodayPrice, dueTodaySum.intValue());

	}

	/**
	 * US461789 -[SDET] AAL: Consolidated Charge Phone purchase flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testConsolidatedChargePhonePurchaseFlowUsingFeatureDevices(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US461789 -[SDET] AAL: Consolidated Charge Phone purchase flow");
		Reporter.log("Data Condition |  PAH Customer with AAL eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Select device from Featured device | PDP page should be displayed");
		Reporter.log("5. Select EIP payment option | EIP payment option should be selected");
		Reporter.log("6. Click Add a line button | Consolidated Rate Plan Page should be displayed");
		Reporter.log(
				"7. Verify 'DueToday' price with doller symbol | 'DueToday' price with doller symbol should be displayed ");
		Reporter.log(
				"8. Verify 'Sim cost' price with doller symbol | 'Sim cost' price with doller symbol should be displayed ");
		Reporter.log("9. Verify 'Sim Starter Kit' | 'Sim Starter Kit' should be displayed ");
		Reporter.log("10. Verify 'Promo text' with offer price | 'Promo text' with offer price should be displayed ");
		Reporter.log("11. Verify 'Due Monthly' text | 'Due Monthly' text should be displayed");
		Reporter.log("12. Verify 'Due Monthly' amount | 'Due Monthly' amount should be displayed");
		Reporter.log("13. Verify Plan name, ex: 'T-Mobile ONE plan' | 'Plan name should be displayed");
		Reporter.log("14. Click Plan name, ex: 'T-Mobile ONE plan' | Plan details model should be displayed");
		Reporter.log("15. Verify Plan model description | Plan model description should be displayed");
		Reporter.log("16. Click close(X) icon | Plan model window header should not be displayed");
		Reporter.log("17. Verify Auto pay text| Auto pay text should be displayed");
		Reporter.log(
				"18. Verify 'TI plan Taxes and fees included' text | Taxes and fees text should be displayed for TI plan Customers");
		Reporter.log("19. Click Continue button | Device protection page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPDPPageByFeatureDevices(myTmoData, myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickOnContinueAALButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyDueTodayText();
		consolidatedRatePlanPage.verifyDueTodayAmount();
		// consolidatedRatePlanPage.verifySimPrice();
		// consolidatedRatePlanPage.verifyPromotionalText();
		consolidatedRatePlanPage.verifyDueMonthlyText();
		consolidatedRatePlanPage.verifyDueMonthlyAmount();
		consolidatedRatePlanPage.verifyFriendlyPlanName();
		consolidatedRatePlanPage.clickFriendlyPlanName();
		consolidatedRatePlanPage.verifyModelHeader();
		consolidatedRatePlanPage.verifyModelDescription();
		consolidatedRatePlanPage.clickModelCloseIcon();
		consolidatedRatePlanPage.verifyPlanModelWindowHeaderNotDisplayed();
		consolidatedRatePlanPage.verifyAutoPayAuthorableText();
		consolidatedRatePlanPage.verifyTaxesAndFeesIncludedTextForTIcustomers();
		consolidatedRatePlanPage.clickOnContinueCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionURL();
	}

	/**
	 * US461803 -[SDET] AAL: Consolidated Charge BYOD flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testConsolidatedChargePhonePurchaseFlowUsingQuckLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US461803 -[SDET] AAL: Consolidated Charge BYOD flow");
		Reporter.log("Data Condition |  PAH Customer with AAL eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on 'Add A LINE' button | Customer intent page should be displayed");
		Reporter.log("5. Select on 'Buy my own device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log(
				"6. Verify 'DueToday' price with doller symbol | 'DueToday' price with doller symbol should be displayed ");
		Reporter.log(
				"7. Verify 'Sim cost' price with doller symbol | 'Sim cost' price with doller symbol should be displayed ");
		Reporter.log(
				"8. Verify 'Deposit' price with doller symbol | 'Deposit' price with doller symbol should be displayed ");
		Reporter.log("9. Verify Discount Strike-out Amount | Discount Strike-out Amount should be displayed");
		Reporter.log(
				"10. Verify 'See full plan terms' link in legal text | 'See full plan terms' link in legal text should be displayed ");
		Reporter.log("11. Click 'See full plan terms' link | NewWindow should be opened");
		Reporter.log("12. Click close(X) icon | Plan model window header should not be displayed");
		Reporter.log("13. Verify Auto pay text | Auto pay text should be displayed");
		Reporter.log(
				"14. Verify 'If customer has a TE plan Taxes and fees not included' text | 'Taxes and fees not included' text should not be displayed for TE plan Customers");
		Reporter.log("19. Click Continue button | Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyDueTodayText();
		consolidatedRatePlanPage.verifyDueTodayAmount();
		consolidatedRatePlanPage.verifySimPrice();
		consolidatedRatePlanPage.verifyAuthorableDepositAmount();
		consolidatedRatePlanPage.verifySimStarterKitAfterDiscountPrice();
		consolidatedRatePlanPage.verifySeeFullPlanTermsLink();
		consolidatedRatePlanPage.clickSeeFullPlanTermsLink();
		consolidatedRatePlanPage.verifyModelHeader();
		consolidatedRatePlanPage.clickModelCloseIcon();
		consolidatedRatePlanPage.verifyPlanModelWindowHeaderNotDisplayed();
		consolidatedRatePlanPage.verifyAutoPayAuthorableText();
		consolidatedRatePlanPage.verifyTaxesAndFeesIncludedTextForTEcustomers();
		consolidatedRatePlanPage.clickOnContinueCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
	}

	/**
	 * US461804 -[SDET] AAL: Cart Page Phone purchase (EIP) flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testNavigatedToCartPageUsingFeatureDevicesWithAALFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US461804 -[SDET] AAL: Cart Page Phone purchase (EIP) flow");
		Reporter.log("Data Condition |  NY Customer with AAL eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Select device from Featured device | PDP page should be displayed");
		Reporter.log("5. Select EIP payment option | EIP payment option should be selected");
		Reporter.log("6. Click Add a line button | Consolidated Rate Plan Page should be displayed");
		Reporter.log(
				"7. Verify 'DueToday' price with doller symbol | 'DueToday' price with doller symbol should be displayed ");
		Reporter.log(
				"8. Verify 'Sim cost' price with doller symbol | 'Sim cost' price with doller symbol should be displayed ");
		Reporter.log("9. Verify 'Sim Starter Kit' | 'Sim Starter Kit' should be displayed ");
		Reporter.log("10. Verify 'Promo text' with offer price | 'Promo text' with offer price should be displayed ");
		Reporter.log("11. Verify 'Due Monthly' text | 'Due Monthly' text should be displayed");
		Reporter.log("12. Verify 'Due Monthly' amount | 'Due Monthly' amount should be displayed");
		Reporter.log("13. Verify Plan name, ex: 'T-Mobile ONE plan' | 'Plan name should be displayed");
		Reporter.log("14. Click Plan name, ex: 'T-Mobile ONE plan' | Plan details model should be displayed");
		Reporter.log("15. Verify Plan model description | Plan model description should be displayed");
		Reporter.log("16. Click close(X) icon | Plan model window header should not be displayed");
		Reporter.log("17. Verify Auto pay text| Auto pay text should be displayed");
		Reporter.log(
				"18. Verify 'TI plan Taxes and fees included' text | Taxes and fees text should be displayed for TI plan Customers");
		Reporter.log("19. Click Continue button | Device protection page should be displayed");
		Reporter.log(
				"20. Verified $15 per month option defaultly selected option | $15 per month option should be defaultly selected");
		Reporter.log("21. Click Continue button | Cart Page should be displayed");
		Reporter.log("22. Verify Customer name | Customer name should be displayed");
		Reporter.log("23. Verify Ship date | Ship date should be displayed");
		Reporter.log("24. Verify Device full retail price | Device full retail price should be displayed");
		Reporter.log(
				"25. Verify EIP Price(Device Due Today + Due Monthly price) | EIP Price(Device Due Today + Due Monthly price) should be displayed");
		Reporter.log("26. Verify EIP Legal text | EIP Legal text should be displayed");
		Reporter.log("27. Verify SIM cart Sku and image | SIM cart Sku and image should be displayed");
		Reporter.log("28. Verify Device Edit button | Device Edit should be displayed");
		Reporter.log("29. Verify Device Remove butoon | Device Remove should be displayed");
		Reporter.log(
				"30. Verify Device Protection $15 per month | Device Protection $15 per month should be displayed in Cart Page");
		Reporter.log(
				"31. Verify Remove button for Device protection | Remove button for Device protection should be displayed");
		Reporter.log(
				"32. Verify Edit button for Device protection | Edit button for Device protection should be displayed");
		Reporter.log(
				"33. Verify Additional Voice line section, $25(ex) a month, taxes and fees included | Additional Voice line section, 25 dollars a month, taxes and fees should be included");
		Reporter.log("34. Verify Accessory tile hidden | Accessory tile should be hidden");
		Reporter.log("35. Verify DueToday price | DueToday price should be displayed");
		Reporter.log("36. Verify Shipping price | Shipping price should be displayed");
		Reporter.log("37. Verify Sales tax price | Sales tax price should be displayed");
		Reporter.log(
				"38. Verify total due-today total(DueToday + Shipping + Sales tax) | total due-today price should be matched");
		Reporter.log("39. Verify Total due-monthly price | Total due-monthly price should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPDPPageByFeatureDevices(myTmoData, myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyDueTodayText();
		consolidatedRatePlanPage.verifyDueTodayAmount();
		// consolidatedRatePlanPage.verifyAuhtorableSimAmount();
		// consolidatedRatePlanPage.verifySimStarterKitStrikePrice();
		// consolidatedRatePlanPage.verifyPromotionalText();
		// consolidatedRatePlanPage.verifyInstantDiscountPriceDisplayed();
		consolidatedRatePlanPage.verifyDueMonthlyText();
		consolidatedRatePlanPage.verifyDueMonthlyAmount();
		consolidatedRatePlanPage.verifyFriendlyPlanName();
		consolidatedRatePlanPage.clickFriendlyPlanName();
		consolidatedRatePlanPage.verifyModelHeader();
		consolidatedRatePlanPage.verifyModelDescription();
		consolidatedRatePlanPage.clickModelCloseIcon();
		consolidatedRatePlanPage.verifyPlanModelWindowHeaderNotDisplayed();
		consolidatedRatePlanPage.verifyAutoPayAuthorableText();
		consolidatedRatePlanPage.verifyTaxesAndFeesIncludedTextForTIcustomers();
		consolidatedRatePlanPage.clickOnContinueCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionURL();
		deviceProtectionPage.verifyProtectionBasicSOCPrice();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		// cartPage.verifyNickName();
		cartPage.verifyShippingDateForDevice();
		cartPage.verifyFullRetailPrice();
		cartPage.verifyEIPAmountAndDuration();
		cartPage.verifyEIPLegalText();
		cartPage.verifyEditButtonIsDisplayed();
		// cartPage.verifyRemoveButtonIsDisplayed();
		cartPage.verifyInsuranceCostPerMonth();
		// cartPage.verifySKUandImage();
		cartPage.verifyInsuranceRemoveButton();
		cartPage.verifyInsuranceEditButton();
		cartPage.verifyAdditionalVoiceLineMonthlyPriceDetails();
		cartPage.verifyDueTodayVerbiageDollerSymbol();
		cartPage.verifyEstShippingIsDisplayedInOrderDetailPage();
		// cartPage.verifySalesTaxPrice();

	}

	/**
	 * US485315 :Essentials plan customers enabled to go through flow (Consolidated
	 * Charges Page)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testTMobileEssentialPlanNameInRatePlanPageWithKnowIntent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :US485315 :Essentials plan customers enabled to go through flow (Consolidated Charges Page)");
		Reporter.log("Data Condition | PAH User with AAL Eligible and User must have T-Mobile Essentials");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button | Device intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log(
				"7. Verify Friendly plan name as 'T-Mobile Essential' | Friendly plan name as 'T-Mobile Essential' should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyANewPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyFriendlyPlanNameEssentials();

	}

	/**
	 * US485315 :Essentials plan customers enabled to go through flow (Consolidated
	 * Charges Page)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testTMobileEssentialPlanNameInRatePlanPageWithUnKnowIntent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :US485315 :Essentials plan customers enabled to go through flow (Consolidated Charges Page)");
		Reporter.log("Data Condition | PAH User with AAL Eligible and User must have T-Mobile Essentials");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones button | PLP page should be displayed");
		Reporter.log("6. Select a device | PDP page should be displayed");
		Reporter.log("7. Click Add a line button  | Consolidated Rate Plan Page  should be displayed");
		Reporter.log(
				"8. Verify Friendly plan name as 'T-Mobile Essential' | Friendly plan name as 'T-Mobile Essential' should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyFriendlyPlanNameEssentials();
	}

	/**
	 * US485318 :Simple Choice customers enabled to go through flow (Consolidated
	 * Charges Page) - UI layer US523384 :Simple Choice customers enabled to go
	 * through flow (Consolidated Charges Page) - EOS layer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testSimpleChoicePlanNameInRatePlanPageWithKnownIntent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :US485318 :Simple Choice customers enabled to go through flow (Consolidated Charges Page) - UI layer");
		Reporter.log("Data Condition | PAH User with AAL Eligible and User must have Simple Choice plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line from quick links | Device intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page should be displayed");
		Reporter.log(
				"7. Verify Friendly plan name as 'Simple Choice' | Friendly plan name as 'Simple Choice' should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyANewPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyFriendlySimpleChoicePlanName();
	}

	/**
	 * US485318 :Simple Choice customers enabled to go through flow (Consolidated
	 * Charges Page) - UI layer US523384 :Simple Choice customers enabled to go
	 * through flow (Consolidated Charges Page) - EOS layer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testSimpleChoicePlanNameInRatePlanPageWithUnknownIntent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :US485318 :Simple Choice customers enabled to go through flow (Consolidated Charges Page) - UI layer");
		Reporter.log("Data Condition | PAH User with AAL Eligible and User must have Simple Choice plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones button | PLP page should be displayed");
		Reporter.log("6. Select a device | PDP page should be displayed");
		Reporter.log("7. Click Add a line button  | Consolidated Rate Plan Page should be displayed");
		Reporter.log(
				"8. Verify Friendly plan name as 'Simple Choice' | Friendly plan name as 'Simple Choice' should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyFriendlySimpleChoicePlanName();
	}

	/**
	 * US577984 :Day 1 Fee: Consolidated Charges Page when there is no SSK fee for a
	 * user adding a line when buying a new device T1414082: Validate if deposit CTA
	 * is clickable for low credit customer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testLowCreditCustomerClickableForDepositCTA(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :US577984 :Day 1 Fee: Consolidated Charges Page when there is no SSK fee for a user adding a line when buying a new device");
		Reporter.log("Data Condition | PAH User and User should have LowCredit and user should have deposit amount");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line from quick links | Device intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page should be displayed");
		Reporter.log("7. Verify Deposit amount CTA | Deposit amount CTA should be displayed");
		Reporter.log("8. Click on Deposit amount CTA  | Deposit model should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyANewPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyPlanDetailsCTA();
		consolidatedRatePlanPage.clickSIMStarterKitCTA();
		consolidatedRatePlanPage.verifyDepositModal();
		consolidatedRatePlanPage.verifyDepositHeaderonPlanDetailsModel();

	}

	/**
	 * US577988 :BYOD: Display SIM Card SKU for a user bringing their own device
	 * when adding a new line on consolidated charges and cart page US616176: [SDET
	 * ONLY] US577988: BYOD: Display SIM Card SKU for a user bringing their own
	 * device when adding a new line on consolidated charges and cart page
	 * 
	 * T1414085: Validate if the user is charged for SIM Card for BYOD devices
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testDueTodayAndDueMonthlySectionsInRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :US577988 :BYOD: Display SIM Card SKU for a user bringing their own device when adding a new line on consolidated charges and cart page");
		Reporter.log("Data Condition | PAH User and User should have AAL Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a line button | Device-Intent page should be displayed");
		Reporter.log("6. Select Use my own phone option | RatePlan page should be displayed");
		Reporter.log("7. Verify Due Today section | Due Today section should be displayed");
		Reporter.log("8. Verify Due Today price | Due Today price should be displayed");
		Reporter.log("9. Verify Due Monthly Section | Due Monthly section should be displayed");
		Reporter.log("10. Verify Due Monthly price | Due Monthly price should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.clickUseMyPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyDueTodayText();
		consolidatedRatePlanPage.verifyDueTodayAmount();
		consolidatedRatePlanPage.verifyDueMonthlyText();
		consolidatedRatePlanPage.verifyDueMonthlyAmount();

	}

	/**
	 * US577988 :BYOD: Display SIM Card SKU for a user bringing their own device
	 * when adding a new line on consolidated charges and cart page US616176: [SDET
	 * ONLY] US577988: BYOD: Display SIM Card SKU for a user bringing their own
	 * device when adding a new line on consolidated charges and cart page
	 * 
	 * T1414087: Validate if SIM Card CTA is clickable for BYOD devices
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testSimStarterKitDepositModelPopUpInRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :US577988 :BYOD: Display SIM Card SKU for a user bringing their own device when adding a new line on consolidated charges and cart page");
		Reporter.log("Data Condition | PAH User and User should have AAL Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a line button | Device-Intent page should be displayed");
		Reporter.log("6. Select Use my own phone option | RatePlan page should be displayed");
		Reporter.log("7. Verify Sim StarterKit link CTA | Sim StarterKit link CTA should be displayed");
		Reporter.log("8. Click on Sim StarterKit link CTA | Sim StarterKit deposit model popup should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifySIMStarterKitCTA();
		consolidatedRatePlanPage.clickSIMStarterKitCTA();
		consolidatedRatePlanPage.verifyDepositModal();
	}

	/**
	 * US607084: (COPY FOR 7/22 RELEASE) Simple Choice customers enabled to go
	 * through flow (Consolidated Charges Page) - EOS layer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testSimpleChoicePlanCustomersNavigatedToRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case: US607084: (COPY FOR 7/22 RELEASE) Simple Choice customers enabled to go through flow (Consolidated Charges Page) - EOS layer");
		Reporter.log("Data Condition | AAL eligible and User should have Simple Choice plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Select Use my own phone option | RatePlan page should be displayed");
		Reporter.log("5. Verify Plan name link | Plan name link should be displayed in RatePlan page");
		Reporter.log("6. Click on Plan name link | Smiple choice plan should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}
	
	/**
	 * CDCSM-14: (Eligibility check and error messaging for DIGITS product type eligibility check
	 * TC-37 :CDCSM-14: Validate the line selector section for user having talk and text  digit lines specific to user
	 * TC-38 :CDCSM-14: Validate the error message section for lines which has talk and text
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAuthorableErrorMessageInRatePlanPageForWearables(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: CDCSM-14: (Eligibility check and error messaging for DIGITS product type eligibility check");
		Reporter.log("Test Case: TC-37 :CDCSM-14: Validate the line selector section for user having talk and text  digit lines specific to user");
		Reporter.log("Test Case: TC-38 :CDCSM-14: Validate the error message section for lines which has talk and text");
		Reporter.log("Data Condition | AAL eligible user");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Select Wearable tile option | Watches plp page should be displayed");
		Reporter.log("5. Select wearable device | UNOPDP page should be displayed");
		Reporter.log("6. Click on Continue CTA | Consolidated rate plan page should be displayed");
		Reporter.log("7. Select the line which has Talk and Text and Click Continue CTA | Modal window should be displayed");
		Reporter.log("8. Verify Authorable header text | Authorable header text should be displayed");
		Reporter.log("9. Verify Authorable body text | Authorable body text should be displayed");
		Reporter.log("10. Verify Authorable OK CTA | Authorable OK CTA should be displayed");
		Reporter.log("11. Click on error message | User should be navigated to Contact us page");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}
	
	/**
	 * CDCSM-14: (Eligibility check and error messaging for DIGITS product type eligibility check
	 * TC-39 :CDCSM-14: Validate the error modal for user who has talk and text digit line	
	 * TC-40 :CDCSM-14: Verify the consolidated charge page for user who have talk and text digit line and he refresh the page/navigate out from this page
	 * TC-41 :CDCSM-14: Verify the consolidated charge page for user who have talk and text digit line and he refresh the page/navigate out from this page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testRadioButtonIsGrayedOutInRatePlanPageForWearables(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: CDCSM-14: (Eligibility check and error messaging for DIGITS product type eligibility check");
		Reporter.log("Test Case: TC-39 :CDCSM-14: Validate the error modal for user who has talk and text digit line");
		Reporter.log("Test Case: TC-40 :CDCSM-14: Verify the consolidated charge page for user who have talk and text digit line and he refresh the page/navigate out from this page");
		Reporter.log("Test Case: TC-41 :CDCSM-14: Verify the consolidated charge page for user who have talk and text digit line and he refresh the page/navigate out from this page");
		Reporter.log("Data Condition | AAL eligible user");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Select Wearable tile option | Watches plp page should be displayed");
		Reporter.log("5. Select wearable device | UNOPDP page should be displayed");
		Reporter.log("6. Click on Continue CTA | Consolidated rate plan page should be displayed");
		Reporter.log("7. Select the line which has Talk and Text and Click Continue CTA | Modal window should be displayed");
		Reporter.log("8. Verify Authorable header text | Authorable header text should be displayed");
		Reporter.log("9. Verify Authorable body text | Authorable body text should be displayed");
		Reporter.log("10. Verify Authorable OK CTA | Authorable OK CTA should be displayed");
		Reporter.log("11. Click on OK CTA | Consolidated rate plan page should be displayed");
		Reporter.log("12. Verify error message under ineligible line | Error message should be displayed under ineligible line");
		Reporter.log("13. Verify Radio button for ineligible line | Radio button and Line name should be grayed out for ineligible line");
		Reporter.log("14. Navigate back | UNOPDP page should be displayed");
		Reporter.log("15. Click on Continue CTA | Consolidated rate plan page should be displayed");
		Reporter.log("16. Click on Continue CTA | Error message should be displayed under ineligible line");
		Reporter.log("17. Verify Radio button for ineligible line | Radio button and Line name should be grayed out for ineligible line");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}
	
	/**
	 * CDCSM-5: Display Due Monthly info (no deposit scenario)
	 * TC-45 :CDCSM-5: Validate the rate plan section on consolidated charge page for DIGITSAW
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutoSectionPriceForAppleWearableDevicesInRateplanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: CDCSM-5: Display Due Monthly info (no deposit scenario)");
		Reporter.log("Test Case: TC-45 :CDCSM-5: Validate the rate plan section on consolidated charge page for DIGITSAW");
		Reporter.log("Data Condition | AAL eligible user");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Select Wearable tile option | Watches plp page should be displayed");
		Reporter.log("5. Click on Brands filter | Brands section should be expanded");
		Reporter.log("6. Select brand as 'Apple' | Apple brand devices only displayed in PLP Page");
		Reporter.log("7. Select wearable device | UNOPDP page should be displayed");
		Reporter.log("8. Click on Continue CTA | Consolidated rate plan page should be displayed");
		Reporter.log("9. Verify Rate plan section for monthly section | Rate plan section should be displayed DIGITS watch price and Autopay price");
		Reporter.log("10 Verify auto pay section down to monthly cost | Monthly cost should be displayed");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyAWatch();
		UNOWatchPLPPage unoWatchPLPPage = new UNOWatchPLPPage(getDriver());
		unoWatchPLPPage.verifyWatchesPageLoaded();
		unoWatchPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOWatchPDPPage unoWatchPDPPage = new UNOWatchPDPPage(getDriver());
		unoWatchPDPPage.verifyWatchesPDPPageLoaded();
		unoWatchPDPPage.clickOnAddToCartCTA();
		WatchPlanPage watchPlanPage = new WatchPlanPage(getDriver());
		watchPlanPage.verifyWatchPlanPage();
		watchPlanPage.verifyWatchPlanDueMonthlyPriceDisplayed();
		watchPlanPage.verifyAutoPayTextBelowDueMonthlyDisplayed();
	}
	
	/**
	 * CDCSM-5: Display Due Monthly info (no deposit scenario)
	 * TC-47 :CDCSM-5: Validate the rate plan section on consolidated charge page for DPDIGITS
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutoSectionPriceForSamsungWearableDevicesInRateplanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: CDCSM-5: Display Due Monthly info (no deposit scenario)");
		Reporter.log("Test Case: TC-47 :CDCSM-5: Validate the rate plan section on consolidated charge page for DPDIGITS");
		Reporter.log("Data Condition | AAL eligible user and eligible for DPDIGITS");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Select Wearable tile option | Watches plp page should be displayed");
		Reporter.log("5. Click on Brands filter | Brands section should be expanded");
		Reporter.log("6. Select brand as 'Samsung' | Samsung brand devices only displayed in PLP Page");
		Reporter.log("7. Select wearable device | UNOPDP page should be displayed");
		Reporter.log("8. Click on Continue CTA | Consolidated rate plan page should be displayed");
		Reporter.log("9. Verify Rate plan section for monthly section | Rate plan section should be displayed DIGITS watch price and Autopay price");
		Reporter.log("10 Verify auto pay section down to monthly cost | Monthly cost should be displayed");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyAWatch();
		UNOWatchPLPPage unoWatchPLPPage = new UNOWatchPLPPage(getDriver());
		unoWatchPLPPage.verifyWatchesPageLoaded();
		unoWatchPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOWatchPDPPage unoWatchPDPPage = new UNOWatchPDPPage(getDriver());
		unoWatchPDPPage.verifyWatchesPDPPageLoaded();
		unoWatchPDPPage.clickOnAddToCartCTA();
		WatchPlanPage watchPlanPage = new WatchPlanPage(getDriver());
		watchPlanPage.verifyWatchPlanPage();
		watchPlanPage.verifyWatchPlanDueMonthlyPriceDisplayed();
		watchPlanPage.verifyAutoPayTextBelowDueMonthlyDisplayed();
	}
	
	/**
	 * CDCSM-5: Display Due Monthly info (no deposit scenario)
	 * TC-48 :CDCSM-5: Validate the rate plan section on consolidated charge page for DPDIGITTE
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testTEPlanCustomesAutoSectionPriceForSamsungWearableDevicesInRateplanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: CDCSM-5: Display Due Monthly info (no deposit scenario)");
		Reporter.log("Test Case: TC-48 :CDCSM-5: Validate the rate plan section on consolidated charge page for DPDIGITTE");
		Reporter.log("Data Condition | TE Plan users eligible for AAL and eligible for DPDIGITTE");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Select Wearable tile option | Watches plp page should be displayed");
		Reporter.log("5. Click on Brands filter | Brands section should be expanded");
		Reporter.log("6. Select brand as 'Samsung' | Samsung brand devices only displayed in PLP Page");
		Reporter.log("7. Select wearable device | UNOPDP page should be displayed");
		Reporter.log("8. Click on Continue CTA | Consolidated rate plan page should be displayed");
		Reporter.log("9. Verify Rate plan section for monthly section | Rate plan section should be displayed DIGITS watch price");
		Reporter.log("10 Verify auto pay section down to monthly cost | Monthly cost should not be displayed");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyAWatch();
		UNOWatchPLPPage unoWatchPLPPage = new UNOWatchPLPPage(getDriver());
		unoWatchPLPPage.verifyWatchesPageLoaded();
		unoWatchPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOWatchPDPPage unoWatchPDPPage = new UNOWatchPDPPage(getDriver());
		unoWatchPDPPage.verifyWatchesPDPPageLoaded();
		unoWatchPDPPage.clickOnAddToCartCTA();
		WatchPlanPage watchPlanPage = new WatchPlanPage(getDriver());
		watchPlanPage.verifyWatchPlanPage();
		watchPlanPage.verifyWatchPlanDueMonthlyPriceDisplayed();
		watchPlanPage.verifyAutoPayTextBelowDueMonthlyDisplayed();
	}
	
	/**
	 * CDCSM-6: Display Deposit section with deposit modal (If deposit applies)
	 * TC-27 :CDCSM-6: Validate the deposit amount on consolidated charge page for users with deposit
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testValidateDepositPriceInRateplanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: CDCSM-6: Display Deposit section with deposit modal (If deposit applies)");
		Reporter.log("Test Case: TC-27 :CDCSM-6: Validate the deposit amount on consolidated charge page for users with deposit");
		Reporter.log("Data Condition | AAL eligible users and user should have deposit price (Low credit customers)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Select Wearable tile option | Watches plp page should be displayed");		
		Reporter.log("5. Select wearable device | UNOPDP page should be displayed");
		Reporter.log("6. Click on Continue CTA | Consolidated rate plan page should be displayed");
		Reporter.log("7. Verify Deposit section on consolidated charges page | Deposit section should be displayed");
		Reporter.log("8. Verify Due Monthly section | Due monthly section should be displayed");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		

		navigateToDeviceIntentPageFromAALQuickLink(myTmoData);
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.clickBuyAWatch();
		UNOWatchPLPPage unoWatchPLPPage = new UNOWatchPLPPage(getDriver());
		unoWatchPLPPage.verifyWatchesPageLoaded();
		unoWatchPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOWatchPDPPage unoWatchPDPPage = new UNOWatchPDPPage(getDriver());
		unoWatchPDPPage.verifyWatchesPDPPageLoaded();
		unoWatchPDPPage.clickOnAddToCartCTA();
		WatchPlanPage watchPlanPage = new WatchPlanPage(getDriver());
		watchPlanPage.verifyWatchPlanPage();
		
		watchPlanPage.verifyWatchPlanDepositTextDisplayed();
		watchPlanPage.verifyWatchPlanDueTodayTextDisplayed();
		watchPlanPage.verifyWatchPlanDueTodayPriceDisplayed();
		
		watchPlanPage.verifyWatchPlanDueMonthlyTextDisplayed();
		watchPlanPage.verifyWatchPlanDueMonthlyPriceDisplayed();

	}
	
	/**
	 * CDCSM-6: Display Deposit section with deposit modal (If deposit applies)
	 * TC-29 :CDCSM-6: Validate the  Deposit hyperlink
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testValidateDepositHyperLinkInRateplanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: CDCSM-6: Display Deposit section with deposit modal (If deposit applies)");
		Reporter.log("Test Case: TC-29 :CDCSM-6: Validate the  Deposit hyperlink");
		Reporter.log("Data Condition | AAL eligible users and user should have deposit price (Low credit customers)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Device-intent page should be displayed");
		Reporter.log("4. Select Wearable tile option | Watches plp page should be displayed");		
		Reporter.log("5. Select wearable device | UNOPDP page should be displayed");
		Reporter.log("6. Click on Continue CTA | Consolidated rate plan page should be displayed");
		Reporter.log("7. Verify Deposit price | Deposit price should be displayed");
		Reporter.log("8. Verify Deposit hyper link | Deposit hyper link should be displayed");
		Reporter.log("9. Click on Deposit hyper link | Deposit authorable modal should be displayed");
		Reporter.log("10. Verify Deposit modal header | Deposit modal header should be displayed");
		Reporter.log("11. Verify Deposit modal body text | Deposit modal body text should be displayed");
		Reporter.log("12. Verify Deposit modal close 'X' icon | Deposit modal close 'X' icon should be displayed");
		Reporter.log("13. Click on close 'X' icon | Deposit modal should be closed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToDeviceIntentPageFromAALQuickLink(myTmoData);
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.clickBuyAWatch();
		UNOWatchPLPPage unoWatchPLPPage = new UNOWatchPLPPage(getDriver());
		unoWatchPLPPage.verifyWatchesPageLoaded();
		unoWatchPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOWatchPDPPage unoWatchPDPPage = new UNOWatchPDPPage(getDriver());
		unoWatchPDPPage.verifyWatchesPDPPageLoaded();
		unoWatchPDPPage.clickOnAddToCartCTA();
		WatchPlanPage watchPlanPage = new WatchPlanPage(getDriver());
		watchPlanPage.verifyWatchPlanPage();
		
		watchPlanPage.verifyWatchPlanDueTodayTextDisplayed();
		watchPlanPage.verifyWatchPlanDueTodayPriceDisplayed();
		watchPlanPage.verifyWatchPlanDepositTextDisplayed();
		
		watchPlanPage.clickDepositHyperlink();
		watchPlanPage.verifyDepositModal();
		
		watchPlanPage.verifyDepositModalHeaderText();
		watchPlanPage.verifyDepositModalBodyText();
		watchPlanPage.verifyDepositModalCloseButton();
		watchPlanPage.clickModalWindowCloseBtn();
		watchPlanPage.verifyDepositModalNotDisplayed();
		watchPlanPage.verifyWatchPlanPage();
		
	}

}
