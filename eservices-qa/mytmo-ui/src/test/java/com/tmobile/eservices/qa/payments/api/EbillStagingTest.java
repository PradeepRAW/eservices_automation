package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EbillStagingApi;

import io.restassured.response.Response;

public class EbillStagingTest extends EbillStagingApi {
	public Map<String, String> tokenMap;

	/**
	 * UserStory# Description: Billing getListbills Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = { "getListbills", "testGetListBills" })
	public void testGetListBills(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGetListBills");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the ListBills of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "getListbills";
		tokenMap = new HashMap<String, String>();
		Response response = getListbills(apiTestData);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertNotNull(getPathVal(jsonNode, "amountDue"), "amount due not dispalyed");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# Description: Billing getPrintBillSummary Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = { "getPrintBillSummary",
			"testPrintBillSummary" })
	public void testPrintBillSummary(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testPrintBillSummary");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the print bill summary of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "getPrintBillSummary";
		tokenMap = new HashMap<String, String>();
		Response response = getPrintBillSummary(apiTestData);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertEquals(getPathVal(jsonNode, "title"), "Monthly Statement");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# Description: Billing testPrintBill Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = { "getPrintBill", "testgetPrintBill" })
	public void testGetPrintBill(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testgetPrintBill");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the print bill summary of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "getPrintBill";
		tokenMap = new HashMap<String, String>();
		Response response = getPrintBill(apiTestData);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertEquals(getPathVal(jsonNode, "title"), "Monthly Statement");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
}