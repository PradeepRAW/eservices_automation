package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;


/**
 * @author charshavardhana
 *
 */
public class AccessoriesPLPTest extends ShopCommonLib {


	/**
	 * US327808: MyTMO - Additional Terms - Upgrade with Accessories - Accessory
	 * List Page - Remove Loan Term Length from Sticky Banner US351460:
	 * [Unfinished] MyTMO - Additional Terms - Upgrade with Accessories -
	 * Accessory List Page - Remove Loan Term Length from Sticky Banner
	 * * US320261: MyTMO - Additional Terms - Upgrade with Accessories - Accessory
	 * List Page Pricing Display US351473: [Unfinished] MyTMO - Additional Terms
	 * - Upgrade with Accessories - Accessory List Page Pricing Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS })
	public void testUpgradeWithAccessoriesPLPStickyBanner(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US327808, US351460: Upgrade with Accessories - Accessory List Page - Remove 24 month Loan term");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selection page should be displayed");
		Reporter.log("8. Select line |  Phone selection page should be displayed");
		Reporter.log("9. Click skip trade in button |  Insurance migration page should be displayed");
		Reporter.log("10. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("11. Select any accessory by checking checkbox |  Verify sticky banner is displayed");
		Reporter.log("12.1. Verify that each accessory contains EIP down payment | EIP down payment should be present");
		Reporter.log("12.2. Verify that each accessory contains loan term length | Loan term length should be present. And should be 9, 12, 24 or 36 month");
		Reporter.log("13.3. Verify that each accessory contains monthly payment | Monthly payment should be present");
		Reporter.log("14.4. Verify that each accessory contains FRP | FRP should be present");
		Reporter.log("15. Verify Loan Term Length of 24 months in EIP should not be displayed on sticky banner | Loan Term Length of 24 months in EIP should not be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage  lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();		
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.verifyStickyBanner();
		accessoryPLPPage.verifyTotalPriceDollarSign();		
		accessoryPLPPage.verifyFRPPrice();
		accessoryPLPPage.verifyEIPPrice();
		accessoryPLPPage.verifyEIPMonthTerms();
		accessoryPLPPage.verifyDownPaymentPriceForAllAccessoriesInPLP();		
		accessoryPLPPage.verifyPayMonthlyMonthsTermsNotDisplayed();		
	}

}

/*
 * Retired US In this Page: US356624,US407128
 */
