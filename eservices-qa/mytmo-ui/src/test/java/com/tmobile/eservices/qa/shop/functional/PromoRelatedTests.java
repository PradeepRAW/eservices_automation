package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.shop.AccessoryPDPPage;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.ConsolidatedRatePlanPage;
import com.tmobile.eservices.qa.pages.shop.DeviceIntentPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.shop.TradeInConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionValuePage;
import com.tmobile.eservices.qa.pages.shop.TradeinDeviceConditionPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import com.tmobile.eservices.qa.shop.ShopConstants;

public class PromoRelatedTests extends ShopCommonLib{
	/**
	 * US230389:NewCart:Promo details in cart
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPromoDetailsInCartPageForEIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:NewCart:Promo details in cart-EIP");
		Reporter.log("Data Condition | IR STD PAH Customer | Cart Promo Code");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log(
				"1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See all phones on shop page |PLP Page should be diplayed");
		Reporter.log("4. Click on the device on Device PLP page | Device PDP Page should be displayed ");
		Reporter.log("5. Select payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select Standared line on LS page |Phone Selection Page should be displayed");
		Reporter.log("7. Click on Skip Trade in Hyper link on Phone selection page | PHP Page should be displayed ");
		Reporter.log("8. Click on Continue on PHP page| Cart Page should be displayed");
		Reporter.log("9. Verify Pay Monthly header | Pay Monthly header should be Displayed");
		Reporter.log("10. Verify Monthly price striked | Monthly price should be striked");
		Reporter.log("11. Verify -$ Price | -$ Price should be Displayed");
		Reporter.log("12. Verify Promo Discounts Verbage | Promo Discounts Verbage should be Displayed");
		Reporter.log("13. Verify Instalment months | Instalment months should be Displayed");
		Reporter.log("14. Verify Due today +tax label | Due today +tax label should be Displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.verifyCartPage();
		cartPage.veifyPromoDetailsForEip();
	}

	/**
	 * US230389:NewCart:Promo details in cart
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPromoDetailsInCartPageForFRP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:NewCart:Promo details in cart-FRP");
		Reporter.log("Data Condition | IR STD PAH Customer | Cart Promo Code");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log(
				"1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See all phones on shop page |PLP Page should be diplayed");
		Reporter.log("4. Click on the device on Device PLP page | Device PDP Page should be displayed ");
		Reporter.log("5. Select payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select Standared line on LS page |Phone Selection Page should be displayed");
		Reporter.log("7. Click on Skip Trade in Hyper link on Phone selection page | PHP Page should be displayed ");
		Reporter.log("8. Click on Continue on PHP page| Cart Page should be displayed");
		Reporter.log("9. Verify Pay In Full header | Pay In Full header should be Displayed");
		Reporter.log("10. Verify price striked | price should be striked");
		Reporter.log("11. Verify $ and Price | $ and Price should be Displayed");
		Reporter.log("12. Verify Promo Discounts Verbage | Promo Discounts Verbage should be Displayed");
		Reporter.log("13. Verify Due today +tax label | Due today +tax label should be Displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.verifyCartPage();
		cartPage.verifyPayInFullHeader();
		cartPage.verifyPriceStriked();
		cartPage.verifyPayInFullPriceWithDollerSymbol();
		cartPage.verifyPromoDiscountVerbage();
		cartPage.verifyDueTodayAndTaxLabel();
	}

	/**
	 * US277880:New Cart:Promo code in cart - With Slash price treatment
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPriceDetailsAfterApplyPromoCodeInCartPageForEIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:New Cart:Promo code in cart - With Slash price treatment-EIP");
		Reporter.log("Data Condition | IR STD PAH Customer | Cart Promo Code");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log(
				"1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See all phones on shop page |PLP Page should be diplayed");
		Reporter.log("4. Click on the device on Device PLP page | Device PDP Page should be displayed ");
		Reporter.log("5. Select payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select Standared line on LS page |Phone Selection Page should be displayed");
		Reporter.log("7. Click on Skip Trade in Hyper link on Phone selection page | PHP Page should be displayed ");
		Reporter.log("8. Click on Continue on PHP page| Cart Page should be displayed");
		Reporter.log("9. Enter Promo code and click apply button | Promo sucess message should be displayed");
		Reporter.log("10. Verify Total Price is stricked | Total price should be stricked");
		Reporter.log("11. Verify Promo discount price sign | Promo discount price sign should be Displayed");
		Reporter.log("12. Verify Promo discount price | Promo discount price should be Displayed");
		Reporter.log("13. Verify Promo Discounts Verbage | Promo Discounts Verbage should be Displayed");
		Reporter.log(
				"14. Verify Installments months price doller symbol | Installments months price doller symbol should be Displayed");
		Reporter.log("15. Verify Instalment months | Instalment months should be Displayed");
		Reporter.log("16. Verify Due today +tax label | Due today +tax label should be Displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.verifyCartPage();
		cartPage.clickAddPromoCodeButton();
		cartPage.enterPromoCodeValue(ShopConstants.PROMO_CODE_VALUE);
		cartPage.clickApplyButton();
		cartPage.verifyPromoCodeSuccessGreenMark();
		cartPage.verifyPriceStriked();
		cartPage.verifyPromoDiscountPriceSign();
		cartPage.verifyPromoDiscountPrice();
		cartPage.verifyPromoDiscountVerbage();
		cartPage.verifyInstallmentsMonthsPriceWithDollerSymbol();
		cartPage.verifyInstalmentMonths();
		cartPage.verifyDueTodayAndTaxLabel();
	}

	/**
	 * US277880:New Cart:Promo code in cart - With Slash price treatment
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPriceDetailsAfterApplyPromoCodeInCartPageForFRP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:New Cart:Promo code in cart - With Slash price treatment-FRP");
		Reporter.log("Data Condition | IR STD PAH Customer | Cart Promo Code");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log(
				"1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See all phones on shop page |PLP Page should be diplayed");
		Reporter.log("4. Click on the device on Device PLP page | Device PDP Page should be displayed ");
		Reporter.log("5. Select payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select Standared line on LS page |Phone Selection Page should be displayed");
		Reporter.log("7. Click on Skip Trade in Hyper link on Phone selection page | PHP Page should be displayed ");
		Reporter.log("8. Click on Continue on PHP page| Cart Page should be displayed");
		Reporter.log("11. Enter Promo code and click apply button | Promo sucess message should be displayed");
		Reporter.log("9. Verify Total Price is stricked | Total price should be stricked");
		Reporter.log("11. Verify Promo discount price | Promo discount price should be Displayed");
		Reporter.log("12. Verify Promo Discounts Verbage | Promo Discounts Verbage should be Displayed");
		Reporter.log("13. Verify Promotion amount minus sign | Promotion amount minus sign should be Displayed");
		Reporter.log("14. Verify Full price doller symbol | Full price doller symbol should be Displayed");
		Reporter.log("15. Verify Due today +tax label | Due today +tax label should be Displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.verifyCartPage();
		cartPage.verifyPayInFullHeader();
		cartPage.clickAddPromoCodeButton();
		cartPage.enterPromoCodeValue(ShopConstants.PROMO_CODE_VALUE);
		cartPage.clickApplyButton();
		cartPage.verifyPromoCodeSuccessGreenMark();
		cartPage.verifyPriceStriked();
		cartPage.verifyPromoDiscountPrice();
		cartPage.verifyPromoDiscountVerbage();
		cartPage.verifyPromoDiscountPriceSign();
		cartPage.verifyPayInFullPriceWithDollerSymbol();
		cartPage.verifyDueTodayAndTaxLabel();
		}

	/**
	 * US227551: Cart:Device Promo Code - Successful
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testCartDevicePromoCodeSuccessful(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: Cart:Device Promo  Code - Successful");
		Reporter.log("Data Condition | IR STD PAH Customer | Cart Promo Code");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log(
				"1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See all phones on shop page |PLP Page should be diplayed");
		Reporter.log("4. Click on the device on Device PLP page | Device PDP Page should be displayed ");
		Reporter.log("5. Select payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select Standared line on LS page |Phone Selection Page should be displayed");
		Reporter.log("7. Click on Skip Trade in Hyper link on Phone selection page | PHP Page should be displayed ");
		Reporter.log("8. Click on Continue on PHP page| Cart Page should be displayed");
		Reporter.log(
				"9. Click Promo code button and enter valid promo code value, Click apply button| Promo code Sucess message should be displayed");
		Reporter.log("10. Click Promo code cancel button | Promo apply button should be disabled mode");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickAddPromoCodeButton();
		cartPage.enterPromoCodeValue(ShopConstants.PROMO_CODE_VALUE);
		cartPage.clickApplyButton();
		cartPage.verifyPromoCodeSuccessfulMessage();
		cartPage.clickAddPromoCodeCancelButton();
		cartPage.verifyPromoApplyButton();
	}
	
	/**
	 * US294037:New Cart : Remove stricke out pricing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testRemoveStrikeOutPrice(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:New Cart :  Remove stricke out pricing");
		Reporter.log("Data Condition | IR STD PAH Customer - Cart Promo Code");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | PLP page should be displayed");
		Reporter.log("6. Select Device | PDP page should be displayed");
		Reporter.log("7. Click Add to cart button | Line selector page should be displayed");
		Reporter.log("8. Select Device Line | Phone Selection page should be displayed");
		Reporter.log("9. Click Skip Trade In button | Cart page should be displayed");
		Reporter.log("10. Enter Promo code and click apply button | Promo sucess message should be displayed");
		Reporter.log("11. Verify Total Price is stricked | Total price should not be stricked");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupCloseButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesLink();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickAddPromoCodeButton();
		cartPage.enterPromoCodeValue(ShopConstants.PROMO_CODE_VALUE);
		cartPage.clickApplyButton();
		cartPage.verifyPromoCodeSuccessGreenMark();
		cartPage.verifyStrikeOutPriceing();
	}
	/**
	 * US265728: New Cart : Trade-in promo covers entire device cost. US272324: New
	 * Cart : Trade-in promo covers entire device cost.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testNewCartTradeinPromoCoverEntireDeviceCost(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:New Cart:Trade-in promo covers entire device cost.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selection page should be displayed");
		Reporter.log("8. Select line |  Phone selection page should be displayed");
		Reporter.log("9. Click skip trade in button |  Insurance migration page should be displayed");
		Reporter.log("9. Click Continue button | Cart page should be displayed");
		Reporter.log("10. Enter PromoCode | Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickAddPromoCodeButton();
		cartPage.enterPromoCodeValue(ShopConstants.PROMO_CODE_VALUE);
		cartPage.clickApplyButton();
		cartPage.verifyPromoCodeNotRequiredMessage();
	}

	/**
	 * US272323:New Cart : Trade-in promo covers entire device cost.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testNewCartTradeInPromoCoversEntireDeviceCost(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:New Cart : Trade-in promo covers entire device cost");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | PLP page should be displayed");
		Reporter.log("6. Select Device | PDP page should be displayed");
		Reporter.log("7. Click Add to cart button | Line selector page should be displayed");
		Reporter.log("8. Select Device Line | Phone Selection page should be displayed");
		Reporter.log("9. Click Skip Trade In button | Cart page should be displayed");
		Reporter.log(
				"10. Enter Zero price Promo code and click apply button | Promo sucess message should be displayed");
		Reporter.log("11. Verify device cost is zero | Device cost is zero should be displayed");
		Reporter.log(
				"12. Enter Zero price Promo code and click apply button | Promo error message should be displayed");
		Reporter.log("13. Verify Device cost is zero message | Device cost is zero message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.enterPromoCodeValue(ShopConstants.ZERO_PRICE_PROMO_CODE);
		cartPage.clickAddPromoCodeButton();
		cartPage.verifyPromoCodeSuccessMessage();
		cartPage.verifyDeviceCostIsZero();
		cartPage.enterPromoCodeValue(ShopConstants.ZERO_PRICE_PROMO_CODE);
		cartPage.clickAddPromoCodeButton();
		cartPage.verifyPromoCodeUnSucessfulMessage();
		cartPage.verifyYourDeviceCostIsZeroTodayMessage();
	}

	/**
	 * US265741:New Cart:Stacked DCP cart promo-Trade-in promo & Promo Code_Promo
	 * Code covers device cost
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testDCPpromoCodeCoversDeviceCostForEIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case:New Cart:Stacked DCP cart promo-Trade-in promo & Promo Code_Promo Code covers device cost-EIP");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selection page should be displayed");
		Reporter.log("8. Select line |  Phone selection page should be displayed");
		Reporter.log("9. Click on SkipTradeIn button |Insurance migration page should be displayed");
		Reporter.log("10. Continue button | Cart page should be displayed");
		Reporter.log(
				"11. Enter Zero price Promo code and click apply button | Promo sucess message should be displayed");
		Reporter.log("12. Verify device cost is zero | Device cost is zero should be displayed");
		Reporter.log("13. Verify Due today +tax label | Due today +tax label should be Displayed");
		Reporter.log("14. Click Monthly charges brean down link | Monthly charges model window should be Displayed");
		Reporter.log("15. Verify device cost is zero | Device cost should be zero is Displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickAddPromoCodeButton();
		cartPage.enterPromoCodeValue(ShopConstants.ZERO_PRICE_PROMO_CODE);
		cartPage.clickApplyButton();
		cartPage.verifyPromoCodeSuccessMessage();
		cartPage.verifyDeviceCostIsZero();
		cartPage.verifyDueTodayAndTaxLabel();
		cartPage.clickMonthlyChargesBreakDownLink();
		cartPage.verifyMonthlyChargesWindow();
		cartPage.verifyDeviceCostIsZero();
	}

	/**
	 * US230399: New Cart : Previously Applied Promo Code in cart - Abandon Cart
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testNewCartAppliedPromoCodeInCart(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:Previously Applied Promo Code in cart - Abandon Cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selection page should be displayed");
		Reporter.log("8. Select line |  Phone selection page should be displayed");
		Reporter.log("9. Click on SkipTradeIn button |  Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickAddPromoCodeButton();
		cartPage.enterPromoCodeValue(ShopConstants.LG_PROMO_CODE_VALUE);
		cartPage.clickApplyButton();
		cartPage.verifyPromoCodeFirstTimeSucessMessage();
		cartPage.verifyPromoCodeAppliedMessage();
		cartPage.verifyApplyButtonIsDisable();
	}
	/**
	 * US261726 :New Cart : Device Promo Code- Successful
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testNewCartDevicePromoCodeSuccessful(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US240371: New Cart :  Device Promo  Code- Successful");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on See All Phones button | Line selector page should be displayed");
		Reporter.log("6. Select a Line | PhoneSelection page should be displayed");
		Reporter.log("7. Click on SkipTradeIn button | Product list page should be displayed");
		Reporter.log("8. Select Device | Product details page should be displayed");
		Reporter.log("9. Click on add to cart button| Cart page should be displayed");
		Reporter.log("10. Click on AddPromoCode button| Promo code should be displayed");
		Reporter.log(
				"11. Enter AddPromoCode Value and Click on Apply button | Promo Code Sucess message should be displayed");
		Reporter.log("12. Verify Apply button is disabled | Apply button should be disabled");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickAddPromoCodeButton();
		cartPage.enterPromoCodeValue(ShopConstants.PROMO_CODE_VALUE);
		cartPage.clickApplyButton();
		cartPage.verifyPromoCodeSuccessfulMessage();
		cartPage.verifyApplyButtonIsDisable();
	}

	/**
	 * US227553 :New Cart : Device Promo Code - Unsuccessful
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testNewCartDevicePromoCodeUnsuccessful(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case ::New Cart: Device Promo  Code - Unsuccessful");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones button | LineSelector page should be displayed");
		Reporter.log("6. Select a line | Phone Selection page should be displayed");
		Reporter.log("7. Click on SkipTradeIn button | Product list page should be displayed");
		Reporter.log("8. Select Device | Product details page should be displayed");
		Reporter.log("9. Click add to cart button | Cart page should be displayed");
		Reporter.log("10. Click on AddPromoCode button| Promo code should be displayed");
		Reporter.log(
				"11. Enter AddPromoCode Value and Click on Apply button| Invalid Promo code error message should be displayed");
		Reporter.log("12. verify Apply button is disabled| Apply button should be disabled");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickAddPromoCodeButton();
		cartPage.enterPromoCodeValue(ShopConstants.PROMO_CODE_VALUE);
		cartPage.clickApplyButton();
		cartPage.verifyPromoCodeUnSucessfulMessage();
		cartPage.verifyApplyButtonIsDisable();
	}
	/**
	 * US348488 : SSU MyTMO > Trade In Promo > Promo Banner > Trade in > Cart promo
	 * pricing
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPromoValueAndEIPPricingInCart(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US348488 : SSU MyTMO > Trade In Promo > Promo Banner > Trade in > Cart");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on Banner | Product details  page should be displayed");
		Reporter.log("6. Select Monthly Payments & Click on add cart | Line selector page should be displayed");
		Reporter.log("7. Select line |Phone selection page should be displayed");
		Reporter.log("8. Click on Got A Different Phone | Trade-In another device page should be displayed");
		Reporter.log("9. Select trade-in info & click continue |  Device condition page should be displayed");
		Reporter.log("10. Click on continue |  Trade-In value page should be displayed");
		Reporter.log("11. Click Accept and Continue |  Insurance migration page should be displayed");
		Reporter.log("12. Click on Continue |  Accessories page should be displayed");
		Reporter.log("13. Click on Skip accessories |  Cart page should be displayed");
		Reporter.log(
				"14. Verify EIP pricing | Promo value and EIP pricing should be displayed. Final EIP price should be difference between regular EIP and promo prices");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = navigateToTradeInValuePageThroughPromoBannerSell(
				myTmoData);
		tradeInDeviceConditionValuePage.clickContinueTradeInButton();
		/*
		 * InsuranceMigrationPage insuranceMigrationPage = new
		 * InsuranceMigrationPage(getDriver());
		 * insuranceMigrationPage.insurenceMigrationcontinue();
		 */
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();

		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyMonthlyEIPTradeInPrice();
		cartPage.verifyeipMonthlyAmtCartPage();

		Double eipMonthlyTotal = cartPage.geteipMonthlyAmtTotalIntegerCartPage();
		Double eipMonthly = cartPage.geteipMonthlyIntegerCartPage();
		Double promoAmt = cartPage.geteipPromoAmtIntegerCartPage();
		Double totalEipPromoAmt = eipMonthly - promoAmt;
		cartPage.comparePromoValueEipPricing(totalEipPromoAmt, eipMonthlyTotal);

	}

	/**
	 * US328821: SSU MyTMO > Trade in Promo > Modularity flag on >Trade in value and
	 * cart promo pricing US348501: SSU MyTMO > Trade in Promo > Modularity flag on
	 * >Trade in value and cart
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPageSequenceWhenModularityIsTrunedOff(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US328821 : SSU MyTMO > Trade in Promo > Modularity flag on >Trade in value and cart");
		Reporter.log("Test Case: US348501: SSU MyTMO > Trade in Promo > Modularity flag on >Trade in value and cart");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on Banner | Product details  page should be displayed");
		Reporter.log("6. Select Monthly Payments & Click on add cart | Line selector page should be displayed");
		Reporter.log("7. Select line |Phone selection page should be displayed");
		Reporter.log("8. Click on Got A Different Phone | Trade-In another device page should be displayed");
		Reporter.log("9. Select trade-in info & click continue |  Device condition page should be displayed");
		Reporter.log("10. Click on continue |  Trade-In value page should be displayed");
		Reporter.log("11. Click Accept and Continue |  Insurance migration page should be displayed");
		Reporter.log("12. Click Continue |  Cart page should be displayed");
		Reporter.log(
				"13. Verify EIP pricing | Promo value and EIP pricing should be displayed. Final EIP price should be difference between regular EIP and promo prices");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByPromoBannerSellWithTradeInFlow(myTmoData);
		cartPage.verifyeipMonthlyAmtCartPage();
		Double eipMonthlyTotal = cartPage.geteipMonthlyAmtTotalIntegerCartPage();
		Double eipMonthly = cartPage.geteipMonthlyIntegerCartPage();
		Double promoAmt = cartPage.geteipPromoAmtIntegerCartPage();
		Double totalEipPromoAmt = eipMonthly - promoAmt;
		cartPage.comparePromoValueEipPricing(totalEipPromoAmt, eipMonthlyTotal);

	}
	/**
	 * US348476: SSU MyTMO > Trade in Promo > All phones > Trade in > Cart promo
	 * pricing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPromoCustomerCartPromoPricingDetails(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US348476: Verify Promo customer cart promo pricing details");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones | Product list page should be displayed");
		Reporter.log("6. Select Device which has Trade in Promo| Product details page should be displayed");
		Reporter.log("7. Click on Add to cart button | New Line Selection page should be displayed");
		Reporter.log("8. In line Selection page click on first line | Phone selection page should be displayed");
		Reporter.log(
				"9. In Phone selection page click on Got a diffrent phone | Trade In Another Device page should be displayed");
		Reporter.log(
				"10. In Trade In Another Device page Select Carrier,Make,Model And enter IMEI | Details should be entered");
		Reporter.log(
				"11. Click on contiune button in Trade In Another Device page | Trade In Device Condition should be displayed");
		Reporter.log(
				"12. In Trade In Device Condition page select Its good radio button and click on contiune button | Trade in value page should be displayed");
		Reporter.log(
				"13. In Trade in value page click on Continue button | Insurance migration page should be displayed");
		Reporter.log("14. In Insurance migration page click on Continue button | Cart should be displayed");
		Reporter.log("15. Verify Promo value  | Promo value should be displayed");
		Reporter.log(
				"16. Verify EIP pricing | Promo value and EIP pricing should be displayed. Final EIP price should be difference between regular EIP and promo prices");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithTradeInFlow(myTmoData);
		cartPage.verifyMonthlyEIPTradeInPrice();

		Double eipMonthlyTotal = cartPage.geteipMonthlyAmtTotalIntegerCartPage();
		Double eipMonthly = cartPage.geteipMonthlyIntegerCartPage();
		Double promoAmt = cartPage.geteipPromoAmtIntegerCartPage();
		Double totalEipPromoAmt = eipMonthly - promoAmt;
		cartPage.comparePromoValueEipPricing(totalEipPromoAmt, eipMonthlyTotal);
	}

	/**
	 * US348483: SSU My TMO > Trade In Promo > Featured phones > Trade in > Cart
	 * Promo Pricing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testFeaturedDeviceInCartPromoPricingDetails(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US348483: Verify Featured Device In Cart Promo Pricing Details");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Featured device  | Product details page should be displayed");
		Reporter.log("6. Click on Add to cart button |line Selection page should be displayed");
		Reporter.log("7. In line Selection page click on first line | Phone selection page should be displayed");
		Reporter.log(
				"8. In Phone selection page click on Got a diffrent phone | Trade In Another Device page should be displayed");
		Reporter.log(
				"9. In Trade In Another Device page Select Carrier,Make,Model And enter IMEI | Details should be entered");
		Reporter.log(
				"10. Click on contiune button in Trade In Another Device page | Trade In Device Condition should be displayed");
		Reporter.log(
				"11. In Trade In Device Condition page select Its good radio button and click on contiune button | Trade in value page should be displayed");
		Reporter.log(
				"12. In Trade in value page click on Continue button | Insurance migration page should be displayed");
		Reporter.log("13. In Insurance migration page click on Continue button | Cart should be displayed");
		Reporter.log("14. Verify Promo value  | Promo value should be displayed");
		Reporter.log(
				"15. Verify EIP pricing | Promo value and EIP pricing should be displayed. Final EIP price should be difference between regular EIP and promo prices");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageByFeaturedDevicesWithTradeInFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyeipPricingBeforepromo();
		cartPage.verifyTradeInPromoTextIsDisplayed();
		cartPage.verifyTradeInPromoValueIsDisplayed();
		cartPage.verifyeipPricingAfterpromo();
		cartPage.VerifyFinalEip();

	}

	/**
	 * US348494 #SSU MyTMO > Trade in Promo > Deep link - TMO.com > Trade in > Cart
	 * Promo pricing US328820 SSU MyTMO > Trade in Promo > Deep link - TMO.com >
	 * Trade in value page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testDeeplinkFromTMOcomAndTradeInValueAndCartPage(ControlTestData data, TMNGData tMNGData,
			MyTmoData myTmoData) {
		Reporter.log("Test case: US348494: Verify Deep link - TMO.com to Trade In value Promo and  Cart Promo pricing");
		Reporter.log("Test case: US328820: SSU MyTMO > Trade in Promo > Deep link - TMO.com > Trade in value page");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Get t-mobile.com | t-mobile home page should display");
		Reporter.log("2. Click on Phones link | Phones page should display");
		Reporter.log("3. Click on device  | Product details page should display");
		Reporter.log("4. Click on Upgrade button | https://account.t-mobile.com login page should display");
		Reporter.log("5. Login to the application | User Should be login successfully");
		Reporter.log("6. Verify Line selector page | Line selector page should be displayed");
		Reporter.log("7. In line Selection page click on first line | Phone selection page should be displayed");
		Reporter.log(
				"8. In Phone selection page click on Got a diffrent phone | Trade In Another Device page should be displayed");
		Reporter.log(
				"9. In Trade In Another Device page Select Carrier,Make,Model And enter IMEI | Details should be entered");
		Reporter.log(
				"10. Click on contiune button in Trade In Another Device page | Trade In Device Condition should be displayed");
		Reporter.log(
				"11. In Trade In Device Condition page select Its good radio button and click on contiune button | Trade in value page should be displayed");
		Reporter.log(
				"12. In Trade in value page click on Continue button | Insurance migration page should be displayed");
		Reporter.log("14. In Insurance migration page click on Continue button | Cart should be displayed");
		Reporter.log(
				"13. Verify EIP pricing And applied Promo | Promo value and EIP pricing should be displayed. Final EIP price should be difference between regular EIP and promo prices");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = launchTMNGAndNavigateToMyTmoCartPage(tMNGData, myTmoData);
		cartPage.verifyMonthlyEIPTradeInPrice();
		Double eipMonthlyTotal = cartPage.geteipMonthlyAmtTotalIntegerCartPage();
		Double eipMonthly = cartPage.geteipMonthlyIntegerCartPage();
		Double promoAmt = cartPage.geteipPromoAmtIntegerCartPage();
		Double totalEipPromoAmt = eipMonthly - promoAmt;
		cartPage.comparePromoValueEipPricing(totalEipPromoAmt, eipMonthlyTotal);
	}
	/**
	 * US330051: TEST ONLY: MyTMO - Additional Terms - Device with Catalog Promotion
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testCatalogPromotionDevicePriceDisplayedCorrectlyInCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US330051:TEST ONLY: MyTMO - Additional Terms - Device with Catalog Promotion");
		Reporter.log("Data Condition | IR STD PAH Customer with Catalog Promotion");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Find Catalog Promotion device | Promotion device should have strike-through price");
		Reporter.log("6. Verify FRP after promo | Save FRP for that device as PLPFRP");
		Reporter.log("7. Verify down payment | Save down payment for that device as PLPDP");
		Reporter.log("8. Verify EIP price | Save EIP for that device as PLPEIP");
		Reporter.log("9. Verify EIP term | Save EIP term for that device as PLPEIPTERM");
		Reporter.log("10. Verify new FRP | new PLPFRP = PLPDP + PLPEIP X PLPEIPTERM");
		Reporter.log("11. Select Catalog Promotion device | Product details page should be displayed");
		Reporter.log("12. Verify FRP after promo | Save FRP for that device as PDPFRP");
		Reporter.log("13. Compare PDPFRP with PLPFRP | PDPFRP should be equal to PLPFRP");
		Reporter.log("14. Verify down payment | Save down payment for that device as PDPDP");
		Reporter.log("15. Compare PDPDP with PLPDP | PDPDP should be equal to PLPDP");
		Reporter.log("16. Verify EIP price | Save EIP for that device as PDPEIP");
		Reporter.log("17. Compare PDPEIP with PLPEIP | PDPEIP should be equal to PLPEIP");
		Reporter.log("18. Verify EIP term | Save EIP term for that device as PDPEIPTERM");
		Reporter.log("17. Compare PDPEIPTERM with PLPEIPTERM | PDPEIPTERM should be equal to PLPEIPTERM");
		Reporter.log("18. Verify new FRP | new PDPFRP = PDPDP + PDPEIP X PDPEIPTERM");
		Reporter.log("19. Select EIP Payment | EIP payments is selected");
		Reporter.log("20. Click add to cart button | Line selection page should be displayed");
		Reporter.log("21. Select line | Phone selection page should be displayed");
		Reporter.log("22. Click skip trade in button | Insurance migration page should be displayed");
		Reporter.log("23. Click continue button | Accessory PLP page should be displayed");
		Reporter.log("24. Click Skip Accessories button | Cart page should be displayed");
		Reporter.log(
				"25. Verify Strike-through price = original FRP minus the catalog promo amount)  | Strike-through price should be present");
		Reporter.log("26. Verify FRP after promo | Save FRP for that device as CFRP");
		Reporter.log("27. Compare CFRP with PDPFRP | CFRP should be equal to PDPFRP");
		Reporter.log("28. Verify down payment | Save down payment for that device as CDP");
		Reporter.log("29. Compare CDP with PDPDP | CDP should be equal to PDPDP");
		Reporter.log("30. Verify EIP price | Save EIP for that device as CEIP");
		Reporter.log("31. Compare CEIP with PDPEIP | CEIP should be equal to PDPEIP");
		Reporter.log("32. Verify EIP term | Save EIP term for that device as CEIPTERM");
		Reporter.log("33. Compare CEIPTERM with PDPEIPTERM | CEIPTERM should be equal to PDPEIPTERM");
		Reporter.log("34. Verify new FRP | new CFRP = CDP + CEIP X CEIPTERM");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyShoppage();

		shopPage.verifyCatelogPromoDevice();
		Double PLPFRP = Math.ceil(shopPage.newFRPCatelogPromoDevice());
		Double PLPDP = shopPage.downPaymentCatelogPromoDevice();
		Double PLPEIP = shopPage.monthlyPaymentCatelogPromoDevice();
		shopPage.verifyEipTermCatelogPromoDevice();
		Double PLPEIPTerm = shopPage.getEipTermCatelogPromoDevice();
		shopPage.compareFRPwithTotal(PLPFRP, (PLPDP + (PLPEIP * PLPEIPTerm)));
		shopPage.clickOnCatalogPromoDevice();
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		Double PDPFRP = Math.ceil(pdpPage.getNewFRPPDPPage());
		pdpPage.compareFRPonPLPwithPDP(PLPFRP, PDPFRP);
		Double PDPDP = pdpPage.getNewDownPaymentPDPPage();
		pdpPage.compareDownPriceonPLPwithPDP(PLPDP, PDPDP);
		Double PDPEIP = Double.parseDouble(pdpPage.getDeviceMonthlyPaymentInPDP());
		pdpPage.compareEIPPriceonPLPwithPDP(PLPEIP, PDPEIP);
		Double PDPEIPTerm = pdpPage.getEIPTermPDPPage();
		pdpPage.compareEIPTermonPLPwithPDP(PLPEIPTerm, PDPEIPTerm);
		pdpPage.compareFRPwithTotal(PDPFRP, (PDPDP + (PDPEIP * PDPEIPTerm)));
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		lineSelectorDetailsPage.clickOnSkipTradeInLink();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();

		// accessoryPLPPage.clickSkipAccessoriesLink();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyStrikeThroughPrice();
		Double CFRP = Math.ceil(cartPage.getNewFRPAfterPromoCart());
		cartPage.compareFRPonPDPwithCart(PDPFRP, CFRP);
		Double CDP = cartPage.getDownPaymentCart();
		cartPage.compareDPonPDPwithCart(CDP, PDPDP);
		Double CEIPTERM = cartPage.getEipTermCDevice();
		cartPage.compareEIPTermonPDPwithCart(PDPEIPTerm, CEIPTERM);
		Double CEIP = cartPage.getDeviceMonthlyIntegerCartPage();
		cartPage.compareEIPonPDPwithCart(PDPEIP, CEIP);
		cartPage.compareFRPwithTotal(CFRP, (CDP + (CEIP * CEIPTERM)));
	}

	/**
	 * US330053: TEST ONLY: MyTMO - Additional Terms - Accessory with Catalog
	 * Promotion (Standard Upgrade with Accessories attached flow)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testCatalogPromotionAccessoryPriceDisplayedCorrectlyForStandardUpgradeFlow(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test Case: US330053:TEST ONLY: MyTMO - Additional Terms - Accessory with Catalog Promotion (Standard Upgrade with Accessories attached flow) ");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select EIP Payment | EIP payments is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line |  Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button |  Insurance migration page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("12. Verify Promotion Accessory is present | Promotion Accessory is present");
		Reporter.log("13. Verify FRP after promo | Save FRP for that accessory as PLPFRP");
		Reporter.log("14. Verify down payment | Save down payment for that accessory as PLPDP");
		Reporter.log("15. Verify EIP price | Save EIP for that accessory as PLPEIP");
		Reporter.log("16. Verify EIP term | Save EIP term for that accessory as PLPEIPTERM");
		Reporter.log("17. Verify new FRP | new PLPFRP = PLPDP + PLPEIP X PLPEIPTERM");
		Reporter.log("18. Click  on Promotin Accessory | Accessory pdp page should be displayed");
		Reporter.log("19. Verify FRP after promo | Save FRP for that accessory as PDPFRP");
		Reporter.log("20. Compare PDPFRP with PLPFRP | PDPFRP should be equal to PLPFRP");
		Reporter.log("21. Verify down payment | Save down payment for that accessory as PDPDP");
		Reporter.log("22. Compare PDPDP with PLPDP | PDPDP should be equal to PLPDP");
		Reporter.log("23. Verify EIP price | Save EIP for that accessory as PDPEIP");
		Reporter.log("24. Compare PDPEIP with PLPEIP | PDPEIP should be equal to PLPEIP");
		Reporter.log("25. Verify EIP term | Save EIP term for that accessory as PDPEIPTERM");
		Reporter.log("26. Compare PDPEIPTERM with PLPEIPTERM | PDPEIPTERM should be equal to PLPEIPTERM");
		Reporter.log("27. Verify new FRP | new PDPFRP = PDPDP + PDPEIP X PDPEIPTERM");
		Reporter.log(
				"28. Verify Strike-through price = original FRP minus the catalog promo amount | Strike-through price should be present");
		Reporter.log("29. Click Add to Order CTA | Cart page should be displayed");
		Reporter.log(
				"30. Verify Strike-through price = original FRP minus the catalog promo amount | Strike-through price should be present");
		Reporter.log("31. Verify FRP after promo | Save FRP for that accessory as CFRP");
		Reporter.log("32. Compare CFRP with PDPFRP | CFRP should be equal to PDPFRP");
		Reporter.log("33. Verify down payment | Save down payment for that accessory as CDP");
		Reporter.log("34. Compare CDP with PDPDP | CDP should be equal to PDPDP");
		Reporter.log("35. Verify EIP price | Save EIP for that accessory as CEIP");
		Reporter.log("36. Compare CEIP with PDPEIP | CEIP should be equal to PDPEIP");
		Reporter.log("37. Verify EIP term | Save EIP term for that accessory as CEIPTERM");
		Reporter.log("38. Compare CEIPTERM with PDPEIPTERM | CEIPTERM should be equal to PDPEIPTERM");
		Reporter.log("39. Verify new FRP | new CFRP = CDP + CEIP X CEIPTERM");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickContinueButton();

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();

		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.verifyAccessoryWithCatalogPromotion();
		accessoryPLPPage.verifyFRPOfAccessoryWithCatalogPromotion();
		Double PLPFRP = accessoryPLPPage.getFRPOfAccessoryWithCatalogPromotion();
		accessoryPLPPage.verifyDownPaymentOfAccessoryWithCatalogPromotion();
		Double PLPDP = accessoryPLPPage.getDownPaymentOfAccessoryWithCatalogPromotion();
		accessoryPLPPage.verifyEIPPaymentOfAccessoryWithCatalogPromotion();
		Double PLPEIP = accessoryPLPPage.getEIPPaymentOfAccessoryWithCatalogPromotion();
		accessoryPLPPage.verifyEIPTermOfAccessoryWithCatalogPromotion();
		Double PLPEIPTERM = accessoryPLPPage.getEIPTermOfAccessoryWithCatalogPromotion();
		Double CalcEIP = (PLPFRP - PLPDP) / PLPEIPTERM;
		accessoryPLPPage.compareAccessoryDueMonthlyAmounts(PLPEIP, CalcEIP);
		accessoryPLPPage.clickFirstPromoAccessory();
		AccessoryPDPPage accessoryPDPPage = new AccessoryPDPPage(getDriver());
		accessoryPDPPage.verifyAccessoryProductDetailsPage();
		accessoryPDPPage.verifyFRPAfterPromo();
		Double PDPFRP = accessoryPDPPage.getFRPOfAccessoryWithCatalogPromotion();
		accessoryPDPPage.compareAccessoryPromoFRPAmounts(PLPFRP, PDPFRP);
		accessoryPDPPage.verifyEIPDownPaymentPrice();
		Double PDPDP = accessoryPDPPage.getDownPaymentOfAccessoryWithCatalogPromotion();
		accessoryPDPPage.compareAccessoryPromoDownPayment(PLPDP, PDPDP);
		accessoryPDPPage.verifyEIPMonthlyPrice();
		Double PDPEIP = accessoryPDPPage.getEIPPaymentOfAccessoryWithCatalogPromotion();
		accessoryPDPPage.compareAccessoryPromoEIP(PLPEIP, PDPEIP);
		accessoryPDPPage.verifyEIPInstallmentsMonths();
		Double PDPEIPTERM = accessoryPDPPage.getEIPTermOfAccessoryWithCatalogPromotion();
		accessoryPDPPage.compareAccessoryPromoEIPTerm(PLPEIPTERM, PDPEIPTERM);
		Double CalcEIPPDP = (PDPFRP - PDPDP) / PDPEIPTERM;
		accessoryPDPPage.compareAccessoryDueMonthlyAmounts(PDPEIP, CalcEIPPDP);
		accessoryPDPPage.verifyStrikedThroughValue();
		accessoryPDPPage.clickAddToCartButton2();
		accessoryPLPPage.verifyAccessoryPLPPage();

		// accessoryPLPPage.clickAccessoryContinueButton();
		accessoryPLPPage.clickContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyAccessoryFRPisDisplayed();
		Double CFRP = cartPage.getAccessoryFRP();
		cartPage.compareAccessoryFRP(PDPFRP, CFRP);
		cartPage.verifyAccessoryDownPaymentisDisplayed();
		Double CDP = cartPage.getAccessoryDownPayment();
		cartPage.compareAccessoryDownPayment(PDPDP, CDP);
		cartPage.verifyMonthlyPayment(1);
		Double CEIP = cartPage.getAccessoryMonthlyIntegerCartPage(myTmoData.getNumberOfAccessories());
		cartPage.compareAccessoryMonthlyAmounts(PDPEIP.toString(), CEIP.toString());
		cartPage.verifyEIPTermForDeviceAndAccessories();
		Double CEIPTERM = cartPage.getAccessoryEIPTerm();
		cartPage.compareAccessoryEIPTerm(PDPEIPTERM, CEIPTERM);
		Double CalcEIPC = (CFRP - CDP) / CEIPTERM;
		cartPage.compareAccessoryDueMonthlyAmounts(CEIP, CalcEIPC);

	}
	/**
	 * US342387 myTMO > PDP Simplification > Standard Upgrade > Migration > NY
	 * customers > Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testDeviceMigrationNYCustomersDisplayAndCartFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test  US342387: myTMO > PDP Simplification > Standard Upgrade > Migration > NY customers > Display");
		Reporter.log("Data Condition | IR STD PAH Customer, NY customers, with device protection");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select a more expensive device (different tier) | Product details page should be displayed");
		Reporter.log("7. Select EIP | EIP is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button | Insurance migration page should be displayed");
		Reporter.log(
				"11. Verify header 'Your new phone requires higher level of protection. Let's keep it protected.' | 'Your new phone requires higher level of protection. Let's keep it protected.'  should be displayed");
		Reporter.log(
				"12. Verify current SOC status as 'Previous'| 'current SOC status as 'Previous' should be displayed");
		Reporter.log("13. Verify the Previous SOC name| the Previous SOC name should be displayed");
		Reporter.log(
				"14. Verify  Strikethrough treatment for pricing | Strikethrough treatment for pricing should be displayed");
		Reporter.log(
				"15. Verify 'Protection 360' as a recommended SOC with pricing and description| 'Protection 360' as a recommended SOC with pricing and description should be displayed");
		Reporter.log(
				"16. Verify  radiobuttons for all recommended SOCs | radiobutton for radiobuttons for all recommended SOCs should be displayed");
		Reporter.log(
				"17. Verify 'I don't need protection for my phone' radio button |  'I don't need protection for my phone' radio button should be displayed");
		Reporter.log("18. Verify legal text for  NY customers | Legal text for  NY customers should be displayed");
		Reporter.log("19. Select any SOC  and click on continue button | Accessories PLP  should be displayed");
		Reporter.log("20. Click on skip accessories | Cart page  should be displayed");
		Reporter.log("21. Verify selected SOC added to  cart | Selected SOC should be added to cart");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();

		deviceProtectionPage.verifyHighLevelProtectionTitle();
		deviceProtectionPage.verifyPrevoiusSOCStatus();
		deviceProtectionPage.verifyPrevoiusSOCName();
		deviceProtectionPage.verifyPrevoiusSOCPrice();
		deviceProtectionPage.verify360SOCName();
		deviceProtectionPage.verify360SOCPrice();
		deviceProtectionPage.verify360SOCDescription();
		deviceProtectionPage.verifyRadioButtonsPresent();
		deviceProtectionPage.verifyLegalTextForNYCustomers();
		deviceProtectionPage.clickContinueButton();

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();

		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyInsuranceName();
	}

	/**
	 * US347235 MyTMO > Line Selector > Trade in > Promo > With EIP Balance
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testLineSelectorTradeInPromoWithEIPBalance(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US347235 : MyTMO > Line Selector > Trade in > Promo > With EIP Balance");
		Reporter.log("Data Condition | STD PAH Customer eligible for  trade in promo");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on a trade in promo Device in PLP| PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button on PDP| Line Selector Page should be Displayed");
		Reporter.log(
				"7. Select line in line selector page and expand the selected line | I should see ramaining lines should not be displayed");
		Reporter.log("8. Select the line have EIP balance");
		Reporter.log(
				"9. Verify eligibility message with trade in promo value 'Good news! This phone is eligible for our $X trade-in promotion'| eligibility message with trade in promo value 'Good news! This phone is eligible for our $X trade-in promotion' should be Displayed");
		Reporter.log(
				"10. Verify eligibility description with Remaining EIP balance. 'You'll have to pay off first, we will add the remaining balance of $x you owe to your order'| Eligibility description with remaining EIP balance. 'You'll have to pay off first, we will add the remaining balance of $x you owe to your order' should be Displayed");
		Reporter.log("11. Click 'Continue trade in this phone' button | TradeInAnotherDevice page should be displayed");
		Reporter.log(
				"12. Select all trade in values and Click Continue button | TradeInDevice condition page should be displayed");
		Reporter.log("13. Click Continue button | TradeInDevice condition page should be displayed");
		Reporter.log("14. Select it's good radio button & click continue |Trade-in value page should be displayed");
		Reporter.log("15. Click agree & continue button | Cart page should be displayed");
		Reporter.log(
				"16. Verify 'trade in promo value' in cart page  | 'Trade in promo value' in cart page should be displayed");
		Reporter.log("17. Verify EIP amount in cart page  | EIP amount in cart page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.verifyEligibilityMessageIsDisplayed();
		lineSelectorDetailsPage.verifyEligibilityDescriptionIsDisplayed();
		lineSelectorDetailsPage.verifyTradeInEligibilityMessageDollarSymbol();
		lineSelectorDetailsPage.clickTradeInCTA();

		TradeinDeviceConditionPage tradeinDeviceConditionPage = new TradeinDeviceConditionPage(getDriver());
		tradeinDeviceConditionPage.verifyTradeInDeviceCondition();
		tradeinDeviceConditionPage.verifyDeviceConditionAsItsGood();
		tradeinDeviceConditionPage.clickOnGoodCondition();

		TradeInConfirmationPage tradeInConfirmationPage = new TradeInConfirmationPage(getDriver());
		tradeInConfirmationPage.verifyTradeInConfirmationPage();
		tradeInConfirmationPage.clickContinueTradeInButton();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();

		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyMonthlyEIPTradeInPrice();
		cartPage.verifyeipMonthlyAmtCartPage();

	}
	

	/**
	 * US426845: Cart promo banner : Promotional copy in cart
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testCartPromoBanner(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case US426845: Cart promo banner : Promotional copy in cart");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device  with a catalog promotion | Product details page should be displayed");
		Reporter.log("7. Select Monthly Payment Option | Monthly Payment Option should be selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Verify Line Selector Details page is displayed");
		Reporter.log("10. Click skip trade in button | Premium Device Protection page should be displayed");
		Reporter.log("11. Click continue button | Accessory PLP page should be displayed");
		Reporter.log("12. Click 'Skip Accessory' button | Cart page should be displayed");
		Reporter.log(
				"13. Verify Banner with appropriate promotional text in cart page | Banner with appropriate promotional text should be present");
		Reporter.log("14. Verify link present on banner | Link should be displayed");
		Reporter.log("15. Click on link | Modal with details should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.verifyPromoBannerIsDisplayed();
		cartPage.verifyPromoBannerText();
		cartPage.verifyPromoDetilsLink();
		cartPage.clickPromoDetailsLink();
		cartPage.verifyPromoModalOfferDetailsWindow();
		cartPage.verifyPromotionOfferDetails();
		cartPage.clickClosePromoModal();
		cartPage.verifyPromoBannerIsDisplayed();

	}
	/**
	 * US426845: Cart promo banner : Promotional copy in cart
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testCartPromoBannerNoPromo(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case US426845: Cart promo banner : Promotional copy in cart");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device  without a catalog promotion | Product details page should be displayed");
		Reporter.log("7. Select Monthly Payment Option | Monthly Payment Option should be selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Verify Line Selector Details page is displayed");
		Reporter.log("10. Click skip trade in button | Premium Device Protection page should be displayed");
		Reporter.log("11. Click continue button | Accessory PLP page should be displayed");
		Reporter.log("12. Click 'Skip Accessory' button | Cart page should be displayed");
		Reporter.log("13. Verify Banner is not present in cart page | Banner should not be present");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickOnYesProtectMyPhoneButton();
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();

		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		// accessoryPLPPage.clickContinueButtonForFRPAccessory();
		accessoryPLPPage.clickContinueButton();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyPromoBannerIsNotDisplayed();
	}

	/**
	 * US426846: Cart promo banner : Clickable links on offer modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testCartPageLinkInPromoBanner(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case US426846: Cart promo banner : Clickable links on offer modal");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device  without a catalog promotion | Product details page should be displayed");
		Reporter.log("7. Select Monthly Payment Option | Monthly Payment Option should be selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Verify Line Selector Details page is displayed");
		Reporter.log("10. Click skip trade in button | Premium Device Protection page should be displayed");
		Reporter.log("11. Click continue button | Accessory PLP page should be displayed");
		Reporter.log("12. Click 'Skip Accessory' button | Cart page should be displayed");
		Reporter.log(
				"13. Verify Banner with appropriate promotional text in cart page | Banner with appropriate promotional text should be present");
		Reporter.log("14. Verify clickable link present on banner | Link should be displayed");
		Reporter.log("15. Click on link | New browser tab should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByFeatureDevicesWithSKipTradeIn(myTmoData, myTmoData.getDeviceName());
		cartPage.verifyPromoBannerIsDisplayed();
		cartPage.clickPromoDetailsLink();
		cartPage.verifyPromoModalOfferDetailsWindow();
		cartPage.verifyGetMoreDetailsLinkInModal();
		cartPage.verifyLinkInModalIsFunctional();

	}

	/**
	 * DE187518: [MyTMO|Shop] Cart is incorrectly accepting PO Boxes for delivery
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testShippingAddressDontAcceptPOboxes(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: DE187518: [MyTMO|Shop] Cart is incorrectly accepting PO Boxes for delivery");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select FRP Payment | FRP payments is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button | Insurance migration page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("12. Click OK button on Dont brake the bank modal | Accessory PLP page should be displayed");
		Reporter.log("13. Select Skip Accessories | Cart page should be displayed");
		Reporter.log("14. Click Continue to Shipping CTA | Shipping tab should be displayed");
		Reporter.log(
				"15. Verify Shipping Address dont accepts PO box addresses | Shipping Address should not accepts PO box addresses");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByFeatureDevicesWithSKipTradeIn(myTmoData, myTmoData.getDeviceName());
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickShipToDifferentAddress();
		cartPage.provideShippingAddressWithPO();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyErrorMessageAddressWithPOBox();
	}

	/**
	 * US412269: CART Pricing - Promo Code Pricing and instant discount BYOD
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testFRPpriceThroughAALFlowAndPromoDiscountForSimStarterKitUsingQuickLink(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test : US412269: CART Pricing - Promo Code Pricing and instant discount BYOD");
		Reporter.log("Data Condition | STD PAH Customer with AAL eligiblity and Promo discount for Sim Starter Kit");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on 'Add A LINE' button | Customer intent page should be displayed");
		Reporter.log("5. Select on 'Buy my own device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("6. Verify SimStarter Kit | SimStarter Kit should be displaye");
		Reporter.log("7. Verify SimStarter Kit price | SimStarter Kit price should be displayed");
		Reporter.log("8. Click Continue button | Device Protection page should be displayed");
		Reporter.log("9. Click Continue button | Cart page should be displayed");
		Reporter.log("10. Enter valid Promo value | Promo discount price should be displayed");
		Reporter.log("11. Verify SimStarterKit text | SimStarterKit text should be displayed");
		Reporter.log(
				"12. Verify SimStarterKit strike through price | SimStarterKit strike through price should be displayed");
		Reporter.log(
				"13. Store SimStarterKit strike through price in V1 | SimStarterKit strike through price should be stored in V1");
		Reporter.log("14. Store SimStarterKit New price in V2 | SimStarterKit New price should be stored in V2");
		Reporter.log("15. Verify V2 = (V1 - Promo Discount)| SimStarterKit New price should matched");
		Reporter.log("16. Verify Promo success message | Promo success message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifySIMStarterKitCTA();
		consolidatedRatePlanPage.verifySimStarterKitStrikePrice();
		consolidatedRatePlanPage.clickOnContinueCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupCloseButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickAddPromoCodeButton();
		cartPage.enterPromoCodeValue(ShopConstants.PROMO_CODE_VALUE);
		cartPage.clickApplyButton();
		cartPage.verifyPromoCodeSuccessGreenMark();
		cartPage.verifyPriceStriked();
		cartPage.verifyPromoDiscountPriceSign();
		cartPage.verifyPromoDiscountPrice();
		cartPage.verifyPromoDiscountVerbage();
		Double V1 = cartPage.getSIMKitPrice();

	}

	/**
	 * Verify EIP Device with Promotion Accessories device
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPromotionAccessoriesDeviceWithEIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Add Promotion Accessories device in Standard upgrade flow");
		Reporter.log("Data Condition | IR STD PAH Customer With Promo Accessories device");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select EIP | EIP is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line |  Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button |  Insurance migration page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("12. Select Promo accessory and click continue button | CartPage should be displayed");
		Reporter.log(
				"13. Fill payment information & click accept & continue | Order confirmation page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		//accessoryPLPPage.verifyDontBreakTheBankModalAndClickOk();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickAccessoriesContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();

		/*
		 * cartPage.verifyAcceptAndContinue();
		 * cartPage.clickAcceptAndPlaceOrder(); OrderConfirmationPage
		 * orderConfirmationPage = new OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}
	
	
	
}
