/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.NewAddCardPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayLandingPage;
import com.tmobile.eservices.qa.pages.payments.PaymentCollectionPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

/**
 * @author charshavardhana
 *
 */
public class NewAddCardPageTest extends PaymentCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(NewAddCardPageTest.class);

	/**
	 * 05_Desktop_MC 2 Series BIN_AutoPay_Set Incorrect Autopay information_Validate
	 * system displays error
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyErrorMessageMCBINSeriesCardsForAutopay(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyErrorMessageMCBINSeriesCardsForAutopay method called in EservicesPaymnetsTest");

		Reporter.log("Test Case : verify Error Message MC BIN Series Cards For Autopay");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill invalid card | Error Message should display");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		NewAutopayLandingPage autopay = navigateToAutoPayPagefromBilling(myTmoData);
		autopay.clickAddPaymentmethod();
		PaymentCollectionPage pcp = new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickAddCard();
		NewAddCardPage addcard = new NewAddCardPage(getDriver());
		addcard.verifyCardPageLoaded();
		addcard.enterCardNumber("222100-272099");
		addcard.Entervalidcardnumber();

	}

	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifycreditcardpage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyErrorMessageMCBINSeriesCardsForAutopay method called in EservicesPaymnetsTest");

		Reporter.log("Test Case : verify Error Message MC BIN Series Cards For Autopay");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill invalid card | Error ;"
				+ "Message should display");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		NewAddCardPage addcard = navigateToNewAddCArdPage(myTmoData);
		addcard.CheckNameonCardPalceholder();
		addcard.CheckCardNumberPalceholder();
		addcard.CheckExpirationDatePalceholder();
		addcard.CheckCVVNumberPalceholder();
		addcard.CheckZipPalceholder();
		addcard.CheckSavePaymentmethodtext(true);
		addcard.Checktooltipforsaveoptional(true);
		addcard.CheckAlreadyhavepaymentmethodtext(true);
		addcard.CheckCVVHelpLink();
		//addcard.CheckCvvHelppopupDsiplayed(true);
	}

	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifycreditcardpagewithnostoredcard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyErrorMessageMCBINSeriesCardsForAutopay method called in EservicesPaymnetsTest");

		Reporter.log("Test Case : verify Error Message MC BIN Series Cards For Autopay");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill invalid card | Error ;"
				+ "Message should display");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		NewAddCardPage addcard = navigateToNewAddCArdPage(myTmoData);
		addcard.CheckNameonCardPalceholder();
		addcard.CheckCardNumberPalceholder();
		addcard.CheckExpirationDatePalceholder();
		addcard.CheckCVVNumberPalceholder();
		addcard.CheckZipPalceholder();
		addcard.CheckSavePaymentmethodtext(true);
		addcard.Checktooltipforsaveoptional(true);
		addcard.CheckAlreadyhavepaymentmethodtext(false);
		addcard.clickBackBtn();
		PaymentCollectionPage OTPpcpPage = new PaymentCollectionPage(getDriver());
	    OTPpcpPage.verifyPageLoaded();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifycreditcardpagestdrestrictedUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyErrorMessageMCBINSeriesCardsForAutopay method called in EservicesPaymnetsTest");

		Reporter.log("Test Case : verify Error Message MC BIN Series Cards For Autopay");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill invalid card | Error ;"
				+ "Message should display");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		NewAddCardPage addcard = navigateToNewAddCArdPage(myTmoData);
		addcard.CheckNameonCardPalceholder();
		addcard.CheckCardNumberPalceholder();
		addcard.CheckExpirationDatePalceholder();
		addcard.CheckCVVNumberPalceholder();
		addcard.CheckZipPalceholder();
		addcard.CheckSavePaymentmethodtext(false);
		addcard.Checktooltipforsaveoptional(false);
		addcard.CheckAlreadyhavepaymentmethodtext(false);
		addcard.clickBackBtn();
		PaymentCollectionPage OTPpcpPage = new PaymentCollectionPage(getDriver());
	    OTPpcpPage.verifyPageLoaded();
	}

	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void verifycreditcardpagevalidations(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyErrorMessageMCBINSeriesCardsForAutopay method called in EservicesPaymnetsTest");

		Reporter.log("Test Case : verify Error Message MC BIN Series Cards For Autopay");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Easy pay button | Manage Autopay Page should be loaded");
		Reporter.log("5. Fill invalid card | Error ;"
				+ "Message should display");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		NewAddCardPage addcard = navigateToNewAddCArdPage(myTmoData);
		addcard.validateAddBankAllFields();
	
	}

	
	
	
}