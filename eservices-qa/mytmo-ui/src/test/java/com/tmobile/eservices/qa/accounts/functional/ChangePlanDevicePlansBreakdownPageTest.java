/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.ChangePlanDeviceBreakdownPage;
import com.tmobile.eservices.qa.pages.accounts.ChangePlansReviewPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */

/**
 *
 * US515750 - Plans detail breakdown page - Multi-line pooled: Plan name
 */

public class ChangePlanDevicePlansBreakdownPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(ChangePlanDevicePlansBreakdownPageTest.class);

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyDeviceBreakdownPagePageWhenAccountHasMultipleEIPOnLines(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("verifyDeviceBreakdownPagePage method called in ChangePlanDevicePlansBreakdownPageTest");
		Reporter.log(
				"Test Case : Verify Device name which is on EIP, cost of it and other details on Device breakdown page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Plans page | Plans Page Should be displayed");
		Reporter.log("5. Verify Featured Plans page | Featured Plans Page Should be displayed");
		Reporter.log("6. Verify Plans Comparison page | Plans Comparison Page Should be displayed");
		Reporter.log("7. Verify details of EIP device on Account overview Page |Should be able to read all details");
		Reporter.log("8. Click on Change Plan CTA |Plans Configure Page Should be displayed");
		Reporter.log("9. Click on Device blade. | Device details Breadkdown Should be displayed");
		Reporter.log("10. Check Header. | Header Your devices should be displayed.");
		Reporter.log("11. Check below subtitle. | Subtitle should be Here are the device details for your account.");
		Reporter.log("12. Check monthly cost. | Total monthly device cost should be displayed.");
		Reporter.log("13. Check line name and msisdn. | Line name and msisdn should be displayed");
		Reporter.log(
				"14. Check Equipment section with device name and cost. | Equipment section with Device on EIP and cost should be displayed");
		Reporter.log(
				"15. Check Back to summary CTA and click on it. | Back to Summary CTa should be displayed and it redirects to Review page");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();
		changePlansReviewPage.clickOnDevicesBlade();

		ChangePlanDeviceBreakdownPage changePlanDevicePlansBreakdownPageTest = new ChangePlanDeviceBreakdownPage(
				getDriver());

		changePlanDevicePlansBreakdownPageTest.checkHeaderOfPlansBreakDownPage();
		changePlanDevicePlansBreakdownPageTest.checkSubHeaderOfPlansBreakDownPage();
		changePlanDevicePlansBreakdownPageTest.checkTotalMonthlyDeviceCostText();
		changePlanDevicePlansBreakdownPageTest.verifyAndCheckTotalMonthlyCost();
		changePlanDevicePlansBreakdownPageTest.verifyNameAndMSISDNOfEachLine();
		changePlanDevicePlansBreakdownPageTest.clickOnBackToSummaryCTA();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyDeviceBreakdownPageWhenUserHasOnlyOneDeviceOnEIP(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyDeviceBreakdownPageWhenUserHasOnlyOneDeviceOnEIP method called in ChangePlanDevicePlansBreakdownPageTest");
		Reporter.log(
				"Test Case : Verify Device name which is on EIP, cost of it and other details on Device breakdown page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Plans page | Plans Page Should be displayed");
		Reporter.log("5. Verify Featured Plans page | Featured Plans Page Should be displayed");
		Reporter.log("6. Verify Plans Comparison page | Plans Comparison Page Should be displayed");
		Reporter.log("7. Verify details of EIP device on Account overview Page |Should be able to read all details");
		Reporter.log("8. Click on Change Plan CTA |Plans Configure Page Should be displayed");
		Reporter.log("9. Click on Device blade. | Device details Breadkdown Should be displayed");
		Reporter.log("10. Check Header. | Header Your devices should be displayed.");
		Reporter.log("11. Check below subtitle. | Subtitle should be Here are the device details for your account.");
		Reporter.log("12. Check monthly cost. | Total monthly device cost should be displayed.");
		Reporter.log("13. Check line name and msisdn. | Line name and msisdn should be displayed");
		Reporter.log(
				"14. Check Equipment section with device name and cost. | Equipment section with Device on EIP and cost should be displayed");
		Reporter.log(
				"15. Check Back to summary CTA and click on it. | Back to Summary CTa should be displayed and it redirects to Review page");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();
		changePlansReviewPage.clickOnDevicesBlade();

		ChangePlanDeviceBreakdownPage changePlanDevicePlansBreakdownPageTest = new ChangePlanDeviceBreakdownPage(
				getDriver());

		changePlanDevicePlansBreakdownPageTest.checkHeaderOfPlansBreakDownPage();
		changePlanDevicePlansBreakdownPageTest.checkSubHeaderOfPlansBreakDownPage();
		changePlanDevicePlansBreakdownPageTest.checkTotalMonthlyDeviceCostText();
		changePlanDevicePlansBreakdownPageTest.verifyAndCheckTotalMonthlyCostWhenAccountHasOnlyOneDeviceOnEIP();
		changePlanDevicePlansBreakdownPageTest.verifyNameAndMSISDNOfEachLine();
		changePlanDevicePlansBreakdownPageTest.clickOnBackToSummaryCTA();
	}
}
