package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.PaymentsApiV3;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class PaymentsApiV3Test extends PaymentsApiV3 {

	public Map<String, String> tokenMap;

	/**
	 * /** Create New EOS for Payments;
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsApiV3Test", "testOTP_NewCard_Amex",Group.PAYMENTS, Group.SPRINT })
	public void testOTP_NewCard_Amex(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testOTP_NewCard_Amex");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with testOTP_NewCard_Amex API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testOTP_NewCard_Amex";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("PaymentsApiV3_otp_newcard_amex.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = oneTimePayment_OTP(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "status"), "SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode, "reasonDescription"), "Created Succesfully",
						"Reason Description is Mismatched");

			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsApiV3Test", "testOTP_NewCard_Discover",Group.PAYMENTS, Group.SPRINT })
	public void testOTP_NewCard_Discover(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testOTP_NewCard_Discover");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with testOTP_NewCard_Discover API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testOTP_NewCard_Discover";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("PaymentsApiV3_otp_newcard_discover.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = oneTimePayment_OTP(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "status"), "SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode, "reasonDescription"), "Created Succesfully",
						"Reason Description is Mismatched");

			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsApiV3Test", "testOTP_NewCard_Master",Group.PAYMENTS, Group.SPRINT })
	public void testOTP_NewCard_Master(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testOTP_NewCard_Master");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with testOTP_NewCard_Master API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testOTP_NewCard_Master";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("PaymentsApiV3_otp_newcard_master.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = oneTimePayment_OTP(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "status"), "SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode, "reasonDescription"), "Created Succesfully",
						"Reason Description is Mismatched");

			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsApiV3Test", "testOTP_NewCard",Group.PAYMENTS, Group.SPRINT })
	public void testOTP_NewCard(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testOTP_NewCard");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with testOTP_NewCard API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testOTP_NewCard";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("PaymentsApiV3_otp_newcard.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = oneTimePayment_OTP(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "status"), "SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode, "reasonDescription"), "Created Succesfully",
						"Reason Description is Mismatched");

			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsApiV3Test", "testOTP_StoredBank",Group.PAYMENTS, Group.SPRINT })
	public void testOTP_StoredBank(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testOTP_StoredBank");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with testOTP_StoredBank API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testOTP_StoredBank";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("PaymentsApiV3_otp_storedbank.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = oneTimePayment_OTP(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "status"), "SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode, "reasonDescription"), "Created Succesfully",
						"Reason Description is Mismatched");

			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsApiV3Test", "testOTP_StoredCard_Amex",Group.PAYMENTS, Group.SPRINT })
	public void testOTP_StoredCard_Amex(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testOTP_StoredCard_Amex");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with testOTP_StoredCard_Amex API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testOTP_StoredCard_Amex";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("PaymentsApiV3_otp_storedcard_amex.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = oneTimePayment_OTP(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "status"), "SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode, "reasonDescription"), "Created Succesfully",
						"Reason Description is Mismatched");

			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsApiV3Test", "testOTP_StoredCard_Master",Group.PAYMENTS, Group.SPRINT })
	public void testOTP_StoredCard_Master(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testOTP_StoredCard_Master");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with testOTP_StoredCard_Master API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testOTP_StoredCard_Master";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("PaymentsApiV3_otp_storedcard_master.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = oneTimePayment_OTP(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "status"), "SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode, "reasonDescription"), "Created Succesfully",
						"Reason Description is Mismatched");

			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsApiV3Test", "testOTP_StoredCard",Group.PAYMENTS, Group.SPRINT })
	public void testOTP_StoredCard(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testOTP_StoredCard");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with testOTP_StoredCard API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testOTP_StoredCard";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("PaymentsApiV3_otp_storedcard.txt");

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = oneTimePayment_OTP(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "status"), "SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode, "reasonDescription"), "Created Succesfully",
						"Reason Description is Mismatched");

			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}
}
