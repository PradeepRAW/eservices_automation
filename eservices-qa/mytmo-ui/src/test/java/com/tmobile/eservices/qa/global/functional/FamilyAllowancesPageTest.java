package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.FamilyControlsPage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.global.FamilyAllowancesPage;

public class FamilyAllowancesPageTest extends GlobalCommonLib {

	/***
	 * US474743 Family Allowances | Downloads | HTML Integration (1/3) US474744
	 * Family Allowances | Downloads | HTML Integration (2/3) US474746 Family
	 * Allowances | Downloads | HTML Integration (3/3) TC ID - 263046
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
	public void testFieldValidationsWithAllowanceAppliedForDownloads(MyTmoData myTmoData) {
		Reporter.log("Data Condition | Any Msisdn with Family Allowance SOC enabled.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log(
				"6. Select a line from the drop down in the Family Control page | Line should be selected from the drop down");
		Reporter.log(
				"7. Click on family allowances link and verify Family allowances page| Family allowances page should be displayed");
		Reporter.log(
				"8. Verify Downloads blade is visible, displaying the header | Downloads blade header is displayed successfully.");
		Reporter.log("9. Verify toggle down arrow is displayed | Toggle down arrow is displayed successfully.");
		Reporter.log("10. Click on Downloads blade | Clicked on Downloads blade, and is expanded succesfully");
		Reporter.log(
				"11. Verify the content below Downloads Blades is visible, when blade is expanded | Content below Downloads blade header is visible.");
		Reporter.log(
				"12. Verify 'No Allowance' checkbox is clickable | No Allowance checkbox is visible and clickable.");
		Reporter.log(
				"13. Verify the display of tool tip icon beside the check box | Tooltip icon must be displayed beside the check box");
		Reporter.log(
				"14. Verify the display of tool tip message beside the check box upon hovering | Tool tip message 'Setting No Allowance will allow this line to download without restriction. This could result in overage charges on your bill. You can also restrict calling on this line by using the Schedule and Never Allowed options.' must be displayed successfully");
		Reporter.log("15. Verify 'No Allowance' checkbox is unchecked | No Allowance checkbox should be unchecked.");
		Reporter.log(
				"16. Verify the display of Current allowance, Used and Remaining fields in the Downloads blade | Respective fields should be displayed");
		Reporter.log(
				"17. Verify Current allowance is editable, and allows only numeric value greater than 0 | Current allowance should be editable and must accept only numeric value greater than or equal to 0");
		Reporter.log(
				"18. Verify the display of field validation error message, when Current allowance field is blank | 'Please enter limit' message should be displayed");
		Reporter.log(
				"19. Verify Current allowance field accepts maximum of 6 digits | Current allowance field must accept maximum of 6 digits");
		Reporter.log(
				"20. Verify the display of Save button, and is clickable | Save button should be displayed, and is clickable");
		Reporter.log(
				"21. Verify clicking on Save button, applies the allowance for a line| Allowance should be applied to the line");
		Reporter.log(
				"22. Verify the display of Saved allowance in the current allowance field for the specified line | Saves allowanced should be displayed for the specified line");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,
				"Family Controls");
		familyControlsPage.clickFamilyAllowances();
		FamilyAllowancesPage familyAllowancesPage = new FamilyAllowancesPage(getDriver());
		familyAllowancesPage.verifyFamilyAllowancesPage();
		familyAllowancesPage.verifyDownloadsBladeHeader();
		familyAllowancesPage.verifyDownloadsToggleArrow();
		familyAllowancesPage.clickOnDownloadsBlade();
		familyAllowancesPage.verifyDownloadsBladeDescription();
		familyAllowancesPage.verifyDownloadsNoAllowanceCheckBox();
		familyAllowancesPage.verifyDownloadsNoAllowanceTooltipIcon();
		familyAllowancesPage.verifyDownloadsNoAllowanceTooltipMessage();
		familyAllowancesPage.checkOrUncheckDownloadsNoAllowanceCheckbox("false");
		familyAllowancesPage.verifyDownloadsFieldLabelsDisplay();
		familyAllowancesPage.verifyCurrentAllowanceFieldValidationErrorMessageForDownloads();
		familyAllowancesPage.verifyDownloadsRemainingFieldDefaultValueWhenCurrentAllowanceIsBlank();
		familyAllowancesPage.verifyMinAndMaxLengthOfDownloadsCurrentAllowance();
		familyAllowancesPage.enterDataIntoDownloadsCurrentAllowance(myTmoData.getAllowance());
		familyAllowancesPage.verifyCalculatedRemainingValueForDownloads();
		familyAllowancesPage.verifySavedCurrentAllowanceDisplayedUnderDownloads();
	}

	/***
	 * US474743 Family Allowances | Downloads | HTML Integration (1/3) US474744
	 * Family Allowances | Downloads | HTML Integration (2/3) US474746 Family
	 * Allowances | Downloads | HTML Integration (3/3) TC ID - 263453
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS ,Group.PROFILE})
	public void testErrorMessageWithAllowanceForDownloads(MyTmoData myTmoData) {
		Reporter.log("Data Condition | Any Msisdn with Family Allowance SOC enabled.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log(
				"6. Select a line from the drop down in the Family Control page | Line should be selected from the drop down");
		Reporter.log(
				"7. Click on family allowances link and verify Family allowances page| Family allowances page should be displayed");
		Reporter.log("8. Click on Downloads blade | Clicked on Downloads blade, and is expanded succesfully");
		Reporter.log(
				"9. Verify 'No Allowance' checkbox is clickable | No Allowance checkbox is visible and clickable.");
		Reporter.log("10. Verify 'No Allowance' checkbox is unchecked | No Allowance checkbox should be unchecked.");
		Reporter.log(
				"11. Verify the display of Current allowance, Used and Remaining fields in the Downloads blade | Respective fields should be displayed");
		Reporter.log("12. Clear the data in Current allowance field | Current allowance should be cleared off");
		Reporter.log(
				"13. Verify the display of field validation error message, when Current allowance field is blank | 'Please enter limit' message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,
				"Family Controls");
		familyControlsPage.clickFamilyAllowances();
		FamilyAllowancesPage familyAllowancesPage = new FamilyAllowancesPage(getDriver());
		familyAllowancesPage.verifyFamilyAllowancesPage();
		familyAllowancesPage.verifyDownloadsBladeHeader();
		familyAllowancesPage.verifyDownloadsToggleArrow();
		familyAllowancesPage.clickOnDownloadsBlade();
		familyAllowancesPage.verifyDownloadsNoAllowanceCheckBox();
		familyAllowancesPage.checkOrUncheckDownloadsNoAllowanceCheckbox("false");
		familyAllowancesPage.verifyDownloadsFieldLabelsDisplay();
		familyAllowancesPage.verifyCurrentAllowanceFieldValidationErrorMessageForDownloads();
	}

	/***
	 * US474743 Family Allowances | Downloads | HTML Integration (1/3) US474744
	 * Family Allowances | Downloads | HTML Integration (2/3) US474746 Family
	 * Allowances | Downloads | HTML Integration (3/3) TC ID - 263518
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS,Group.PROFILE })
	public void testDiscardChangesWithAllowanceForDownloads(MyTmoData myTmoData) {
		Reporter.log("Data Condition | Any Msisdn with Family Allowance SOC enabled.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log(
				"6. Select a line from the drop down in the Family Control page | Line should be selected from the drop down");
		Reporter.log(
				"7. Click on family allowances link and verify Family allowances page| Family allowances page should be displayed");
		Reporter.log("8. Click on Downloads blade | Clicked on Downloads blade, and is expanded succesfully");
		Reporter.log(
				"9. Verify 'No Allowance' checkbox is clickable | No Allowance checkbox is visible and clickable.");
		Reporter.log("10. Verify 'No Allowance' checkbox is unchecked | No Allowance checkbox should be unchecked.");
		Reporter.log(
				"11. Verify the display of Current allowance, Used and Remaining fields in the Downloads blade | Respective fields should be displayed");
		Reporter.log(
				"12. Verify Current allowance is editable, and enter numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
		Reporter.log(
				"13. Verify the display of Discard Changes button, and is clickable | Discard Changes button should be displayed, and is clickable");
		Reporter.log(
				"14. Verify clicking on Discard Changes button, should not apply the allowance for a line| Allowance entered should not be applied to the line");
		Reporter.log("15. Click on Downloads blade | Clicked on Downloads blade, and is expanded succesfully");
		Reporter.log(
				"16. Verify that the updated value in the Current allowance field will not be displayed | Updated current allowances should be not displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,
				"Family Controls");
		familyControlsPage.clickFamilyAllowances();
		FamilyAllowancesPage familyAllowancesPage = new FamilyAllowancesPage(getDriver());
		familyAllowancesPage.verifyFamilyAllowancesPage();
		familyAllowancesPage.verifyDownloadsBladeHeader();
		familyAllowancesPage.verifyDownloadsToggleArrow();
		familyAllowancesPage.clickOnDownloadsBlade();
		familyAllowancesPage.verifyDownloadsNoAllowanceCheckBox();
		familyAllowancesPage.checkOrUncheckDownloadsNoAllowanceCheckbox("false");
		familyAllowancesPage.verifyDownloadsFieldLabelsDisplay();
		familyAllowancesPage.verifyCurrentAllowanceWithDiscardChangesForDownloads(myTmoData.getAllowance());
	}

	/***
	 * US474743 Family Allowances | Downloads | HTML Integration (1/3) US474744
	 * Family Allowances | Downloads | HTML Integration (2/3) US474746 Family
	 * Allowances | Downloads | HTML Integration (3/3) TC ID - 263519
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS ,Group.PROFILE})
	public void testAllowanceWhenCheckedAndUncheckedAllowanceForDownloads(MyTmoData myTmoData) {
		Reporter.log("Data Condition | Any Msisdn with Family Allowance SOC enabled.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log(
				"6. Select a line from the drop down in the Family Control page | Line should be selected from the drop down");
		Reporter.log(
				"7. Click on family allowances link and verify Family allowances page| Family allowances page should be displayed");
		Reporter.log("8. Click on Downloads blade | Clicked on Downloads blade, and is expanded succesfully");
		Reporter.log(
				"9. Verify 'No Allowance' checkbox is clickable | No Allowance checkbox is visible and clickable.");
		Reporter.log("10. Verify 'No Allowance' checkbox is unchecked | No Allowance checkbox should be unchecked.");
		Reporter.log(
				"11. Verify the display of Current allowance, Used and Remaining fields in the Downloads blade | Respective fields should be displayed");
		Reporter.log(
				"12. Verify Current allowance is editable, and enter numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
		Reporter.log(
				"13. Now check and uncheck again the No allowance check box | No allowance check box should be unchecked and checked");
		Reporter.log(
				"14. Verify that Current allowance field is refreshed and previously saved value is displayed for the specified line | Previously saved current allowances should be displayed for the line");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,
				"Family Controls");
		familyControlsPage.clickFamilyAllowances();
		FamilyAllowancesPage familyAllowancesPage = new FamilyAllowancesPage(getDriver());
		familyAllowancesPage.verifyFamilyAllowancesPage();
		familyAllowancesPage.verifyDownloadsBladeHeader();
		familyAllowancesPage.verifyDownloadsToggleArrow();
		familyAllowancesPage.clickOnDownloadsBlade();
		familyAllowancesPage.verifyDownloadsNoAllowanceCheckBox();
		familyAllowancesPage.checkOrUncheckDownloadsNoAllowanceCheckbox("false");
		familyAllowancesPage.verifyDownloadsFieldLabelsDisplay();
		familyAllowancesPage.verifyCurrentAllowanceWithCheckAndUncheckNoAllowanceUnderDownloads(myTmoData.getAllowance());
	}

	/**
	 * US412124 - Family Allowances | Apply Allowance Changes to All Lines (1/2)
	 * TC ID - TC
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
	public void testAllComponentsDiscardChangesWithApplyAllowanceToAllLines(MyTmoData myTmoData) {
		Reporter.log("Data Condition | Any Msisdn with Family Allowance SOC enabled.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on Minutes blade | Clicked on Minutes blade, and is expanded succesfully");
		Reporter.log(
				"3. Verify the state of 'No Allowance' checkbox for Minutes | No Allowance checkbox state is verified successfully.");
		Reporter.log(
				"4. If No allowance checkbox is unchecked, Verify Current allowance is editable, and enter numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
		Reporter.log(
				"5. If No allowance checkbox is already checked, Verify Current allowance is editable, and update numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
		Reporter.log("6. Click on Messages blade | Clicked on Messages blade, and is expanded succesfully");
		Reporter.log(
				"7. Verify the state of 'No Allowance' checkbox for Messages | No Allowance checkbox state is verified successfully.");
		Reporter.log(
				"8. If No allowance checkbox is unchecked, Verify Current allowance is editable, and enter numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
		Reporter.log(
				"9. If No allowance checkbox is already checked, Verify Current allowance is editable, and update numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
		Reporter.log("10. Click on Downloads blade | Clicked on Downloads blade, and is expanded succesfully");
		Reporter.log(
				"11. Verify the state of 'No Allowance' checkbox for Downloads | No Allowance checkbox state is verified successfully.");
		Reporter.log(
				"12. If No allowance checkbox is unchecked, Verify Current allowance is editable, and enter numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
		Reporter.log(
				"13. If No allowance checkbox is already checked, Verify Current allowance is editable, and update numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
		Reporter.log("14. Verify state of Schedule accordion| schedule blade should be collapsed successfully");
		Reporter.log("15. Click on Schedule Blade | Schedule Blade should be clicked");
		Reporter.log("16. Expand Schedule page | Expanded successfully");
		Reporter.log("17. Check or uncheck timing field|  It should be checked or unchecked based on input");
		Reporter.log("18. Click on Allowed numbers | Clicked on Allowed numbers blade and is expanded successfully.");
		Reporter.log("19. Verify toggle down arrow is clickable | toggle down arrow verified successfully.");
		Reporter.log("20. Verify Always allowed text is visible | Always allowed is visible.");
		Reporter.log(
				"21. Enter a valid number in the Add new text field |Text field should accept maximum of 10 digits number");
		Reporter.log("22. Click on Add option | Add option must be clickable");
		Reporter.log(
				"23. Verify adding a number under Always allowed | I must see the entered number gets added to the Always allowed table");
		Reporter.log("24. Verify Never allowed text is visible | Never allowed is visible.");
		Reporter.log(
				"25. Enter a valid number in the Add new text field | Text field should accept maximum of 10 digits number");
		Reporter.log("26. Click on Add option | Add option must be clickable");
		Reporter.log(
				"27. Verify adding a number under Never allowed | I must see the entered number gets added to the Never allowed table");
		Reporter.log(
				"28. Click on 'Apply allowance to all lines' checkbox  | 'Apply allowance to all lines' checkbox should be clickable");
		Reporter.log("29. Click on Discard Changes button | Save button must be clickable");
		Reporter.log(
				"30. Now change the line from the dashboard line selector to some other line | Must be able to change the line from the dashboard");
		Reporter.log(
				"31. Verify the Minutes current allowance value for the other line selected | Verified entered Minutes allowance does not apply to all lines");
		Reporter.log(
				"32. Verify the Messages current allowance value for the other line selected | Verified entered Messages allowance does not apply to all lines");
		Reporter.log(
				"33. Verify the Downloads current allowance value for the other line selected | Verified entered Downloads allowance does not apply to all lines");
		Reporter.log(
				"34. Verify the schedule changes |  Fields should not be saved, thus checkbox should not be checked, if it is checked before Discard changes");
		Reporter.log(
				"35. Verify the new number is not added to Always allowed for the other line selected | New number should not be added and saved to the Always allowed number");
		Reporter.log(
				"36. Verify the new number is not added to Never allowed for the other line selected | New number should not be added and saved to the Never allowed number");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,
				"Family Controls");
		familyControlsPage.clickFamilyAllowances();
		FamilyAllowancesPage familyAllowancesPage = new FamilyAllowancesPage(getDriver());
		familyAllowancesPage.verifyFamilyAllowancesPage();
		familyAllowancesPage.clickOnMinutesBlade();
		familyAllowancesPage.verifyMinutesNoAllowanceCheckBox();
		familyAllowancesPage.checkOrUncheckMinutesNoAllowanceCheckbox("false");
		familyAllowancesPage.enterDataIntoMinutesCurrentAllowance(myTmoData.getAllowance());
		familyAllowancesPage.verifyCalculatedRemainingValueForMinutes();
		String valueBeforeDiscardUnderMinutes = familyAllowancesPage.getRemainingValueForMinutes();
		familyAllowancesPage.clickOnMessagesBlade();
		familyAllowancesPage.verifyMessagesNoAllowanceCheckBox();
		familyAllowancesPage.checkOrUncheckMessagesNoAllowanceCheckbox("false");
		familyAllowancesPage.enterDataIntoMessagesCurrentAllowance(myTmoData.getAllowance());
		familyAllowancesPage.verifyCalculatedRemainingValueForMessages();
		String valueBeforeDiscardUnderMessages = familyAllowancesPage.getRemainingValueForMessages();
		familyAllowancesPage.clickOnDownloadsBlade();
		familyAllowancesPage.verifyDownloadsNoAllowanceCheckBox();
		familyAllowancesPage.checkOrUncheckDownloadsNoAllowanceCheckbox("false");
		familyAllowancesPage.enterDataIntoDownloadsCurrentAllowance(myTmoData.getAllowance());
		familyAllowancesPage.verifyCalculatedRemainingValueForDownloads();
		String valueBeforeDiscardUnderDownloads = familyAllowancesPage.getRemainingValueForDownloads();
		familyAllowancesPage.verifyScheduleAccordianStatusCollapsed();
		familyAllowancesPage.clickScheduleBlade();
		familyAllowancesPage.changeScheduleFirstCheckboxStatus();
		String valueBeforeDiscardUnderSchedule = familyAllowancesPage.getScheduleState();
		familyAllowancesPage.clickOnAllowedNumbers();
		familyAllowancesPage.verifyAlwaysAllowed();
		familyAllowancesPage.enterDataIntoAddNewUnderAlwaysAllowed(myTmoData.getPhoneNumber());
		familyAllowancesPage.clickAddButtonForAlwaysAllowed();
		String valueBeforeDiscardUnderAlwaysAllowed = familyAllowancesPage.getNumberUnderAlwaysAllowed();
		familyAllowancesPage.verifyNeverAllowed();
		familyAllowancesPage.enterDataIntoAddNewUnderNeverAllowed(myTmoData.getPhoneNumber2());
		familyAllowancesPage.clickAddButtonForNeverAllowed();
		String valueBeforeDiscardUnderNeverAllowed = familyAllowancesPage.getNumberUnderNeverAllowed();
		familyAllowancesPage.verifyAndClickOnApplyAllowanceChangesToAllLinesCheckbox();
		familyAllowancesPage.verifyAndClickOnDiscardChangesButton();
		familyAllowancesPage.verifyAndEditSelectOtherLine();
		familyAllowancesPage.clickOnMinutesBlade();
		String valueAfterDiscardUnderMinutes = familyAllowancesPage.getRemainingValueForMinutes();
		familyAllowancesPage.validateValueAfterDiscardChangesForOtherMISDIN(valueBeforeDiscardUnderMinutes,
				valueAfterDiscardUnderMinutes);
		familyAllowancesPage.clickOnMessagesBlade();
		String valueAfterDiscardUnderMessages = familyAllowancesPage.getRemainingValueForMessages();
		familyAllowancesPage.validateValueAfterDiscardChangesForOtherMISDIN(valueBeforeDiscardUnderMessages,
				valueAfterDiscardUnderMessages);
		familyAllowancesPage.clickOnDownloadsBlade();
		String valueAfterDiscardUnderDownloads = familyAllowancesPage.getRemainingValueForDownloads();
		familyAllowancesPage.validateValueAfterDiscardChangesForOtherMISDIN(valueBeforeDiscardUnderDownloads,
				valueAfterDiscardUnderDownloads);
		familyAllowancesPage.clickScheduleBlade();
		String valueAfterDiscardUnderSchedule = familyAllowancesPage.getScheduleState();
		familyAllowancesPage.validateValueAfterDiscardChangesForOtherMISDIN(valueBeforeDiscardUnderSchedule,
				valueAfterDiscardUnderSchedule);
		familyAllowancesPage.clickOnAllowedNumbers();
		familyAllowancesPage.verifyNumberDiscardedNotSavedUnderAlwaysAllowed(valueBeforeDiscardUnderAlwaysAllowed);
		familyAllowancesPage.verifyNumberDiscardedNotSavedUnderNeverAllowed(valueBeforeDiscardUnderNeverAllowed);
	}

		/**
		 * US535261
		 * TC288641
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
		public void testDiscardChangesForFamilyAllowances(ControlTestData data, MyTmoData myTmoData) {
			
			Reporter.log("================================");
			Reporter.log("Data Condition | Any Msisdn with Family Allowance SOC enabled.");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | User Should be login successfully");
			Reporter.log("3. Verify home page | Home page should be displayed");
			Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
			Reporter.log("5. Click on family controls link and verify Family Control page| Family controls page should be displayed");
			Reporter.log("7. Click on family allowances link and verify Family allowances page| Family allowances page should be displayed");
			Reporter.log("8. Verify Messages blade is visible, displaying the header | Messages blade header is displayed successfully.");
			Reporter.log("9. Click on Messages blade | Clicked on Messages blade, and is expanded succesfully");
			Reporter.log("10. Enter the data into Current allowance | Current allowance must accept only numeric value greater than or equal to 0");
			Reporter.log("11. Verify Remaining field value calculated dynamically using formula Current allowance - Used = Remaining in web page, upon editing the Current allowance field | Remaining field value should change based on the below calculations, and must be always greater than 0");
			Reporter.log("12. click on discard button  | discard button clicked successfully");
			Reporter.log("13. verify minutes accordian status  | minutes accordian blade should be collapsed successfully");
			Reporter.log("14. verify messages accordian status  | messages accordian blade should be collapsed successfully");
			Reporter.log("15. verify downloads accordian status  | downloads accordian blade should be collapsed successfully");
			Reporter.log("16. verify schedule accordian status  | schedule accordian blade should be collapsed successfully");
			Reporter.log("17. verify change history accordian status  | change history accordian blade should be collapsed successfully");
			Reporter.log("================================");
			Reporter.log("Actual Output:");

			FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,"Family Controls");
			familyControlsPage.clickFamilyAllowances();
			FamilyAllowancesPage familyAllowancesPage = new FamilyAllowancesPage(getDriver());
			familyAllowancesPage.verifyFamilyAllowancesPage();
			familyAllowancesPage.verifyMessagesBladeHeader();
			familyAllowancesPage.clickOnMessagesBlade();
			familyAllowancesPage.enterDataIntoMessagesCurrentAllowance(myTmoData.getAllowance());
			familyAllowancesPage.verifyCalculatedRemainingValueForMessages();
			familyAllowancesPage.verifyAndClickOnDiscardChangesButton();
			
			
			
			
			
			familyAllowancesPage.verifyMinutesAccordianStatusCollapsed();
			familyAllowancesPage.verifyMessagesAccordianStatusCollapsed();
			familyAllowancesPage.verifyDownloadsAccordianStatusCollapsed();
			familyAllowancesPage.verifyScheduleAccordianStatusCollapsed();
			familyAllowancesPage.verifychangeHistoryAccordianStatusCollapsed();
		}
		 
		/**
		 * US535271
		 * TC288640
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.PROFILE})
		public void testFailureModalReDirectToDashboard(ControlTestData data, MyTmoData myTmoData) {
			
			Reporter.log("================================");
			Reporter.log("Data Condition | Any Msisdn with Family Allowance SOC enabled.");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | User Should be login successfully");
			Reporter.log("3. Verify home page | Home page should be displayed");
			Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
			Reporter.log("5. Click on family controls link and verify Family Control page| Family controls page should be displayed");
			Reporter.log("6. Click on family allowances link and verify Family allowances page| Family allowances page should be displayed");
			Reporter.log("8. Verify modal message | Modal message should not be displayed");
			Reporter.log("9. Click on Allowed numbers | Clicked on Allowed numbers blade and is expanded successfully.");
			Reporter.log("10. Enter a special number in the text field |entered the number successfully");
			Reporter.log("11. Click on Add button | add button for always allowed field is clickable ");
			Reporter.log("12.Click on save  button  | Error message 'We're having some trouble' should be displayed");
			Reporter.log("13.Click on continue button| family Allowamces page should be displayed successfully");
			Reporter.log("================================");
			Reporter.log("Actual Output:");

			FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,"Family Controls");
			familyControlsPage.clickFamilyAllowances();
			FamilyAllowancesPage familyAllowancesPage = new FamilyAllowancesPage(getDriver());
			familyAllowancesPage.verifyFamilyAllowancesPage();
			familyAllowancesPage.clickOnAllowedNumbers();
			familyAllowancesPage.enterDataIntoAddNewUnderAlwaysAllowed(myTmoData.getPhoneNumber());
			familyAllowancesPage.clickAddButtonForAlwaysAllowed();
			familyAllowancesPage.verifyAndClickOnSaveButton();
			familyAllowancesPage.integralServerErrorModalOnFailureOfSave();
			familyAllowancesPage.verifyFamilyAllowancesPage();

		}
		
		/**
		 * US516247
		 * TC288639
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS,Group.PROFILE})
		public void testPreventAddingDefaultNumbers(ControlTestData data, MyTmoData myTmoData) {
		
			Reporter.log("================================");
			Reporter.log("Data Condition | Any Msisdn with Family Allowance SOC enabled.");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | User Should be login successfully");
			Reporter.log("3. Verify home page | Home page should be displayed");
			Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
			Reporter.log("5. Click on family controls link and verify Family Control page| Family controls page should be displayed");
			Reporter.log("6. Click on family allowances link and verify Family allowances page| Family allowances page should be displayed");
			Reporter.log("7. Click on Allowed numbers | Clicked on Allowed numbers blade and is expanded successfully.");
			Reporter.log("8. Verify Always allowed text is visible | Always allowed is visible.");
			Reporter.log("9. Enter a valid number in the text field |Text field should accept maximum of 10 digits number");
			Reporter.log("10. Click on Add option | Add option must be clickable");
			Reporter.log("11. Verify adding a number under Always allowed | I must see the entered number gets added to the Always allowed table");
			Reporter.log("12. Enter a same valid number in the text field again |Error message ' number alreday exists in always allowed field' should be displayed successfully");
			Reporter.log("================================");
			Reporter.log("Actual Output:");

			FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,"Family Controls");
			familyControlsPage.clickFamilyAllowances();
			FamilyAllowancesPage familyAllowancesPage = new FamilyAllowancesPage(getDriver());
			familyAllowancesPage.verifyFamilyAllowancesPage();
			familyAllowancesPage.clickOnAllowedNumbers();
			familyAllowancesPage.enterDataIntoAddNewUnderAlwaysAllowed(myTmoData.getPhoneNumber());
			familyAllowancesPage.clickAddButtonForAlwaysAllowed();
			familyAllowancesPage.verifyAddedNumberDisplayedUnderAlwaysAllowed();
			familyAllowancesPage.enterDataIntoAddNewUnderNeverAllowed(myTmoData.getPhoneNumber2());
			familyAllowancesPage.verifyErrormessageInAllowedNumbers();
			


		}
		/**
		 * US519794:Family Allowances | Breadcrumbs
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
		public void verifyFamilyAllowancesPageBreadCrumbLinks(ControlTestData data, MyTmoData myTmoData) {
			Reporter.log("Test Data Conditions: Any Msisdn");
			Reporter.log("Test Steps|Expected Result");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | User Should be logged in  successfully");
			Reporter.log("3. Verify Home page | Home page should be displayed");
			Reporter.log("4. Click Profile Link | Profile page should be displayed");
			Reporter.log("5. Click on family control link | Family control page should be displayed");
			Reporter.log("6. Click on View list of all Lines CTA | View list of all lines should be loaded" );
			Reporter.log("7. Verify all them missdns in view list of lines contain attribute 'PID' | All the missds in view list of lines page contains PID attribute");
			Reporter.log("8. Verify the value of 'PID' atribute in all missdns| All the missdns has PID value as 'cust_msisdn'" );
			Reporter.log("9. Verify the value of 'PID' atribute in all missdns should be in sorted order | All the missdns has PID value as 'cust_msisdn' in sorted order" );
			Reporter.log("10.Click on View list of all Lines CTA | View list of all lines page should be displayed successfully" );
			Reporter.log("11.Click in the first msisdn provided in the list and verify Family Allowance page is loading | Successfully clicked first msisdn and Family Allowance page is loaded.");
			Reporter.log("12.Click on View list of all Lines CTA | View list of all lines page should be displayed successfully" );
			Reporter.log("13.Select one missdn|It should navigate to family allowances page" );
			Reporter.log("14.Click on family controls breadcrumb|It should navigate to family controls page" );
			Reporter.log("15:Verify Missdn in Family Controls Page | Missdn should be same as the missdn selected in view list of lines page");
			Reporter.log("16:Click on Profile BreadCrumb| Profile page should be displayed");
			Reporter.log("17:Verify Missdn in Profile Page | Missdn should be same as the missdn selected in view list of lines page");
			Reporter.log("18.Verify family allowances bread crumb status | Family allowances bread crumb should be active");
			Reporter.log("19.Click on change history blade | Click on change history blade should be success");
			Reporter.log("20.Click on history of all lines link and verify bread crumb status | History of all lines breadcrumb should be active");
			Reporter.log("21.Click on family allowances bread crumb | Family allowances page should be displayed");
			Reporter.log("22.Verify family allowances bread crumb status | Family allowances bread crumb should be active");
			Reporter.log("23.Click on list of all lines link and verify bread crumb status | List of all lines breadcrumb should be active");
			Reporter.log("24.Click on family allowances bread crumb | Family allowances page should be displayed");
			Reporter.log("25.Verify family allowances bread crumb status | Family allowances bread crumb should be active");
			Reporter.log("26.Click on family control bread crumb | Family control page should be displayed");
			Reporter.log("27.Verify family controls bread crumb status | Family controls bread crumb should be active");
			Reporter.log("28.Click on profile bread crumb | Profile page should be displayed");
			Reporter.log("29.Verify profile bread crumb status | Profile bread crumb should be active");

			Reporter.log("================================");
			Reporter.log("Actual Output:");

			FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,
					"Family Controls");
			familyControlsPage.verifyAndClickViewListOfLinesLinkInFamilyControlsPage();
			
			FamilyAllowancesPage familyAllowancesPage = new FamilyAllowancesPage(getDriver());
			familyAllowancesPage.verifyViewListOfLinesPage();
			familyAllowancesPage.verifyPIDAttributeinMissdnWebElementinViewListOfLinesPage();
			familyAllowancesPage.verifyPIDAttributeValueOrderInMissdnWebElement();
			
			familyControlsPage.clickFirstMissdninViewListOfLines();
			familyControlsPage.clickFamilyAllowances();
			
			familyAllowancesPage.verifyFamilyAllowancesPage();
			familyAllowancesPage.clickViewListOfLinesCTA();
			String selectedMissdn=familyControlsPage.selectAndRetrieveMissdninViewListOfLine();
			familyAllowancesPage.verifyFamilyAllowancesPage();
			familyAllowancesPage.clickFamilyControlsBreadCrumb();
			
			familyControlsPage.verifyFamilyControlsPage();
			
			familyAllowancesPage.verifyMissdnNumberInFamilyControlsPage(selectedMissdn);
			familyAllowancesPage.clickProfileHomeBreadCrumb();
			
			familyControlsPage.verifyProfilePage();
			
			familyAllowancesPage.verifyMissdnNumberInProfilePage(selectedMissdn);	
			familyAllowancesPage.navigateBack();
			
			familyControlsPage.clickFamilyAllowances();
			
			familyAllowancesPage.verifyFamilyAllowancesPage();
			familyAllowancesPage.verifyBreadCrumbActiveStatus("Family Allowances");
			familyAllowancesPage.clickChangeHistoryBlade();
			familyAllowancesPage.ClickChangeHistoryViewListOfLinesCTA();
			familyAllowancesPage.verifyBreadCrumbActiveStatus("History of All Lines");
			familyAllowancesPage.clickFamilyAllowancesBreadCrumb();
			familyAllowancesPage.verifyFamilyAllowancesPage();
			familyAllowancesPage.verifyBreadCrumbActiveStatus("Family Allowances");
			familyAllowancesPage.clickViewListOfLinesCTA();
			familyAllowancesPage.verifyBreadCrumbActiveStatus("List of All Lines");
			familyAllowancesPage.clickFamilyAllowancesBreadCrumb();
			familyAllowancesPage.verifyFamilyAllowancesPage();
			familyAllowancesPage.verifyBreadCrumbActiveStatus("Family Allowances");
			familyAllowancesPage.clickFamilyControlsBreadCrumb();

			familyControlsPage = new FamilyControlsPage(getDriver());
			familyControlsPage.verifyFamilyControlsPage();
			familyControlsPage.verifyBreadCrumbActiveStatus("Family Controls");
			familyControlsPage.clickProfileHomeBreadCrumb();

			ProfilePage profilePage = new ProfilePage(getDriver());
			profilePage.verifyProfilePage();
			profilePage.verifyBreadCrumbActiveStatus("Profile");
		}
		
		/**
		 * US412124 - Family Allowances | Apply Allowance Changes to All Lines (1/2)
		 * TC ID - TC
		 * 
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
		public void testAllComponentsSavedWithApplyAllowanceToAllLines(MyTmoData myTmoData) {
			Reporter.log("Data Condition | Any Msisdn with Family Allowance SOC enabled.");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Click on Minutes blade | Clicked on Minutes blade, and is expanded succesfully");
			Reporter.log(
					"3. Verify the state of 'No Allowance' checkbox for Minutes | No Allowance checkbox state is verified successfully.");
			Reporter.log(
					"4. If No allowance checkbox is unchecked, Verify Current allowance is editable, and enter numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
			Reporter.log(
					"5. If No allowance checkbox is already checked, Verify Current allowance is editable, and update numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
			Reporter.log("6. Click on Messages blade | Clicked on Messages blade, and is expanded succesfully");
			Reporter.log(
					"7. Verify the state of 'No Allowance' checkbox for Messages | No Allowance checkbox state is verified successfully.");
			Reporter.log(
					"8. If No allowance checkbox is unchecked, Verify Current allowance is editable, and enter numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
			Reporter.log(
					"9. If No allowance checkbox is already checked, Verify Current allowance is editable, and update numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
			Reporter.log("10. Click on Downloads blade | Clicked on Downloads blade, and is expanded succesfully");
			Reporter.log(
					"11. Verify the state of 'No Allowance' checkbox for Downloads | No Allowance checkbox state is verified successfully.");
			Reporter.log(
					"12. If No allowance checkbox is unchecked, Verify Current allowance is editable, and enter numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
			Reporter.log(
					"13. If No allowance checkbox is already checked, Verify Current allowance is editable, and update numeric value greater than 0 | Current allowance should be editable and must accept numeric value greater than or equal to 0");
			Reporter.log("14. Verify state of Schedule accordion| schedule blade should be collapsed successfully");
			Reporter.log("15. Click on Schedule Blade | Schedule Blade should be clicked");
			Reporter.log("16. Expand Schedule page | Expanded successfully");
			Reporter.log("17. Check or uncheck timing field|  It should be checked or unchecked based on input");
			Reporter.log("18. Click on Allowed numbers | Clicked on Allowed numbers blade and is expanded successfully.");
			Reporter.log("19. Verify Always allowed text is visible | Always allowed is visible.");
			Reporter.log(
					"20. Enter a valid number in the Add new text field |Text field should accept maximum of 10 digits number");
			Reporter.log("21. Click on Add option | Add option must be clickable");
			Reporter.log(
					"22. Verify adding a number under Always allowed | I must see the entered number gets added to the Always allowed table");
			Reporter.log("23. Verify Never allowed text is visible | Never allowed is visible.");
			Reporter.log(
					"24. Enter a valid number in the Add new text field | Text field should accept maximum of 10 digits number");
			Reporter.log("25. Click on Add option | Add option must be clickable");
			Reporter.log(
					"26. Verify adding a number under Never allowed | I must see the entered number gets added to the Never allowed table");
			Reporter.log(
					"27. Click on 'Apply allowance to all lines' checkbox  | 'Apply allowance to all lines' checkbox should be clickable");
			Reporter.log("28. Click on Save button | Save button must be clickable");
			Reporter.log(
					"29. Now change the line from the dashboard line selector to some other line | Must be able to change the line from the dashboard");
			Reporter.log(
					"30. Verify the Minutes current allowance value for the other line selected | Verified entered Minutes allowance applies to all lines");
			Reporter.log(
					"31. Verify the Messages current allowance value for the other line selected | Verified entered Messages allowance applies to all lines");
			Reporter.log(
					"32. Verify the Downloads current allowance value for the other line selected | Verified entered Downloads allowance applies to all lines");
			Reporter.log(
					"33. Verify the schedule changes |  Fields should be saved, thus checkbox must be checked, if it is checked and saved");
			Reporter.log(
					"34. Verify the newly added number under Always allowed | New number must be saved to the Always allowed number");
			Reporter.log(
					"35. Verify the newly added number under Never allowed | New number must be saved to the Never allowed number");
			Reporter.log("================================");
			Reporter.log("Actual Output:");

			FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,
					"Family Controls");
			familyControlsPage.clickFamilyAllowances();
			FamilyAllowancesPage familyAllowancesPage = new FamilyAllowancesPage(getDriver());
			familyAllowancesPage.verifyFamilyAllowancesPage();
			familyAllowancesPage.verifyMinutesToggleArrow();
			familyAllowancesPage.clickOnMinutesBlade();
			familyAllowancesPage.verifyMinutesNoAllowanceCheckBox();
			familyAllowancesPage.checkOrUncheckMinutesNoAllowanceCheckbox("false");
			familyAllowancesPage.enterDataIntoMinutesCurrentAllowance(myTmoData.getAllowance());
			familyAllowancesPage.verifyCalculatedRemainingValueForMinutes();
			String valueBeforeSaveUnderMinutes = familyAllowancesPage.getRemainingValueForMinutes();
			familyAllowancesPage.verifyMessagesToggleArrow();
			familyAllowancesPage.clickOnMessagesBlade();
			familyAllowancesPage.verifyMessagesNoAllowanceCheckBox();
			familyAllowancesPage.checkOrUncheckMessagesNoAllowanceCheckbox("false");
			familyAllowancesPage.enterDataIntoMessagesCurrentAllowance(myTmoData.getAllowance());
			familyAllowancesPage.verifyCalculatedRemainingValueForMessages();
			String valueBeforeSaveUnderMessages = familyAllowancesPage.getRemainingValueForMessages();
			familyAllowancesPage.verifyDownloadsToggleArrow();
			familyAllowancesPage.clickOnDownloadsBlade();
			familyAllowancesPage.verifyDownloadsNoAllowanceCheckBox();
			familyAllowancesPage.checkOrUncheckDownloadsNoAllowanceCheckbox("false");
			familyAllowancesPage.enterDataIntoDownloadsCurrentAllowance(myTmoData.getAllowance());
			familyAllowancesPage.verifyCalculatedRemainingValueForDownloads();
			String valueBeforeSaveUnderDownloads = familyAllowancesPage.getRemainingValueForDownloads();
			familyAllowancesPage.verifyScheduleAccordianStatusCollapsed();
			familyAllowancesPage.clickScheduleBlade();
			familyAllowancesPage.changeScheduleFirstCheckboxStatus();
			String valueBeforeSaveUnderSchedule = familyAllowancesPage.getScheduleState();
			familyAllowancesPage.clickOnAllowedNumbers();
			familyAllowancesPage.verifyAlwaysAllowed();
			familyAllowancesPage.enterDataIntoAddNewUnderAlwaysAllowed(myTmoData.getPhoneNumber());
			familyAllowancesPage.clickAddButtonForAlwaysAllowed();
			String valueBeforeSaveUnderAlwaysAllowed = familyAllowancesPage.getNumberUnderAlwaysAllowed();
			familyAllowancesPage.verifyNeverAllowed();
			familyAllowancesPage.enterDataIntoAddNewUnderNeverAllowed(myTmoData.getPhoneNumber2());
			familyAllowancesPage.clickAddButtonForNeverAllowed();
			String valueBeforeSaveUnderNeverAllowed = familyAllowancesPage.getNumberUnderNeverAllowed();
			familyAllowancesPage.verifyAndClickOnApplyAllowanceChangesToAllLinesCheckbox();
			familyAllowancesPage.verifyAndClickOnSaveButton();
			familyAllowancesPage.verifyAndEditSelectOtherLine();

			familyAllowancesPage.clickOnMinutesBlade();
			String valueAfterSaveUnderMinutes = familyAllowancesPage.getRemainingValueForMinutes();
			familyAllowancesPage.validateSavedValueDisplayedForOtherMISDIN(valueBeforeSaveUnderMinutes,
					valueAfterSaveUnderMinutes);
			familyAllowancesPage.clickOnMessagesBlade();
			String valueAfterSaveUnderMessages = familyAllowancesPage.getRemainingValueForMessages();
			familyAllowancesPage.validateSavedValueDisplayedForOtherMISDIN(valueBeforeSaveUnderMessages,
					valueAfterSaveUnderMessages);
			familyAllowancesPage.clickOnDownloadsBlade();
			String valueAfterSaveUnderDownloads = familyAllowancesPage.getRemainingValueForDownloads();
			familyAllowancesPage.validateSavedValueDisplayedForOtherMISDIN(valueBeforeSaveUnderDownloads,
					valueAfterSaveUnderDownloads);
			familyAllowancesPage.clickScheduleBlade();
			String valueAfterSaveUnderSchedule = familyAllowancesPage.getScheduleState();
			familyAllowancesPage.validateSavedValueDisplayedForOtherMISDIN(valueBeforeSaveUnderSchedule,
					valueAfterSaveUnderSchedule);
			familyAllowancesPage.clickOnAllowedNumbers();
			String valueAfterSaveUnderAlwaysAllowed = familyAllowancesPage.getNumberUnderAlwaysAllowed();
			familyAllowancesPage.validateSavedValueDisplayedForOtherMISDIN(valueBeforeSaveUnderAlwaysAllowed,
					valueAfterSaveUnderAlwaysAllowed);
			String valueAfterSaveUnderNeverAllowed = familyAllowancesPage.getNumberUnderNeverAllowed();
			familyAllowancesPage.validateSavedValueDisplayedForOtherMISDIN(valueBeforeSaveUnderNeverAllowed,
					valueAfterSaveUnderNeverAllowed);
		}
		/***
		 * CDCDWR-599 EP-18058 | Family Allowance - Desktop - User receives "We're having some trouble" error when he enters 5+ digits download allowance amount and save
		 * 
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT})
		public void testMaxFieldValidationsWithAllowanceAppliedForDownloads(MyTmoData myTmoData) {
			Reporter.log("Data Condition | Any Msisdn with Family Allowance SOC enabled.");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | User Should be login successfully");
			Reporter.log("3. Verify home page | Home page should be displayed");
			Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
			Reporter.log(
					"5. Click on family controls link and verify Family Control page| Family controls page should be displayed");
			Reporter.log(
					"6. Select a line from the drop down in the Family Control page | Line should be selected from the drop down");
			Reporter.log(
					"7. Click on family allowances link and verify Family allowances page| Family allowances page should be displayed");
			Reporter.log(
					"8. Verify Downloads blade is visible, displaying the header | Downloads blade header is displayed successfully.");
			Reporter.log("9. Verify toggle down arrow is displayed | Toggle down arrow is displayed successfully.");
			Reporter.log("10. Click on Downloads blade | Clicked on Downloads blade, and is expanded succesfully");
			Reporter.log(
					"11. Verify the content below Downloads Blades is visible, when blade is expanded | Content below Downloads blade header is visible.");
			Reporter.log(
					"12. Verify 'No Allowance' checkbox is clickable | No Allowance checkbox is visible and clickable.");
			Reporter.log("13. Verify 'No Allowance' checkbox is unchecked | No Allowance checkbox should be unchecked.");
			Reporter.log(
					"14. Verify the display of Current allowance, Used and Remaining fields in the Downloads blade | Respective fields should be displayed");
			Reporter.log(
					"15. Verify Current allowance is editable, and allows only numeric value greater than 0 | Current allowance should be editable and must accept only numeric value greater than or equal to 0");
			Reporter.log(
					"16. Verify the display of field validation error message, when Current allowance field is blank | 'Please enter limit' message should be displayed");
			Reporter.log(
					"17. Verify Current allowance field accepts maximum of 6 digits | Current allowance field must accept maximum of 6 digits");
			Reporter.log(
					"18. Verify the display of Save button, and is clickable | Save button should be displayed, and is clickable");
			Reporter.log(
					"19. Verify clicking on Save button, applies the allowance for a line| Allowance should be applied to the line");
			Reporter.log(
					"20. Verify the display of Saved allowance in the current allowance field for the specified line | Saves allowanced should be displayed for the specified line");
			Reporter.log("================================");
			Reporter.log("Actual Output:");

			FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,
					"Family Controls");
			familyControlsPage.clickFamilyAllowances();
			FamilyAllowancesPage familyAllowancesPage = new FamilyAllowancesPage(getDriver());
			familyAllowancesPage.verifyFamilyAllowancesPage();
			familyAllowancesPage.verifyDownloadsBladeHeader();
			familyAllowancesPage.verifyDownloadsToggleArrow();
			familyAllowancesPage.clickOnDownloadsBlade();
			familyAllowancesPage.verifyDownloadsBladeDescription();
			familyAllowancesPage.verifyDownloadsNoAllowanceCheckBox();
			familyAllowancesPage.checkOrUncheckDownloadsNoAllowanceCheckbox("false");
			familyAllowancesPage.verifyDownloadsFieldLabelsDisplay();
			familyAllowancesPage.verifyCurrentAllowanceFieldValidationErrorMessageForDownloads();
			familyAllowancesPage.verifyDownloadsRemainingFieldDefaultValueWhenCurrentAllowanceIsBlank();
			familyAllowancesPage.verifyMinAndMaxLengthOfDownloadsCurrentAllowance();
			familyAllowancesPage.enterDataIntoDownloadsCurrentAllowance(myTmoData.getAllowance());
			familyAllowancesPage.verifyCalculatedRemainingValueForDownloads();
			familyAllowancesPage.verifyAndClickOnSaveButton();
			familyAllowancesPage.verifyFamilyAllowancesPage();
			familyAllowancesPage.clickOnDownloadsBlade();
			familyAllowancesPage.verifyCalculatedRemainingValueForDownloads();
		}
		
		/**
		 *US584903- Family Allowances | Download History $ Display
         * TC306329
         * @param data
        * @param myTmoData
        */	          
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyDownloadHistoryDollarDisplay(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile Link | Profile page should be displayed");
		Reporter.log("5. Click on family control link | Family control page should be displayed");
		Reporter.log("6. Click on family allowance link | Family allowance page should be displayed");
		Reporter.log("7. Click on chnage history tab | blade should be expanded successfully ");
		Reporter.log("8. verify dollar symbol before downloads value | It should be displayed successfully ");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData,
				"Family Controls");
		familyControlsPage.clickFamilyAllowances();
		FamilyAllowancesPage familyAllowancesPage = new FamilyAllowancesPage(getDriver());
		familyAllowancesPage.verifyFamilyAllowancesPage();
		familyAllowancesPage.clickChangeHistoryBlade();
		familyAllowancesPage.verifyDollarSymbolinChangeHistory();
	}

		}