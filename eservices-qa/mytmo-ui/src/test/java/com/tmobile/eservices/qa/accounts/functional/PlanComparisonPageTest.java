/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.accounts.ChangePlansReviewPage;
import com.tmobile.eservices.qa.pages.accounts.ExplorePlanPage;
import com.tmobile.eservices.qa.pages.accounts.MilitaryVerificationPage;
import com.tmobile.eservices.qa.pages.accounts.PlanComparisonPage;
import com.tmobile.eservices.qa.pages.accounts.PlanDetailsPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class PlanComparisonPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(PlanComparisonPageTest.class);

	// Regression

	/*
	 * US512564 : [Explore Plans Page] Display Legal Text
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifytheLegalTextonPlanComparionPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifytheLegalTextonPlanComparionPage");
		Reporter.log("Test Case : Verify the Legal Text on Plan Comparion Page");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on feature plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that LegalText |LealText should be available");
		Reporter.log("8. Click on SeeFullDetailLink |Navigated to LegalInformation");
		Reporter.log("9. Clcik on BACK cts | Navigated to Previous Page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanComparisonPageTestDirectly(myTmoData, planName);

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifyLegalTextOnExplore();
		newplancomparisionpage.seeFullDetailsLinkClick();
		newplancomparisionpage.verifyThatLegalPage();
		newplancomparisionpage.verifyBackCta();
		newplancomparisionpage.verifyComparePlan();
	}

	/*
	 * US523878 : [Plan Comparison] Military Plan availability for Change Plan
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyMilitrayPlanChangesforVerifiedCustomer(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyMilitrayPlanChangesforVerifiedCustomer");
		Reporter.log("Test Case : Verify Militray Plan Changes for Verified Customer");
		Reporter.log("Test Data : MilitaryVerified PAH/Full customer with verified military plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on military plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that SelectPlan CTA |SelectPlan should be available under the feature plan");
		Reporter.log(
				"8. Select military plan from currnet plan |SelectPlan should be available under the current plan");
		Reporter.log(
				"9. Select military plan from feature plan |SelectPlan should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanComparisonPageTestDirectly(myTmoData, "Magenta Military");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.clickOnSelectPlanCta();
		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();
		changePlansReviewPage.verifyNewPlanName("Magenta Military");
	}

	/*
	 * US523878 : [Plan Comparison] Military Plan availability for Change Plan
	 * verify get verify button for non military verified customers
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyGetVerifyButtonForNonMilitrayVerifiedCustomer(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyGetVerifyButtonForNonMilitrayVerifiedCustomer");
		Reporter.log("Test Case : Verify Get Verify Button For Non Militray Verified Customer");
		Reporter.log("Test Data : Non Military Verified PAH/Full customer with verified military plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on military plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that SelectPlan CTA |SelectPlan should be available under the feature plan");
		Reporter.log(
				"8. Select military plan from currnet plan |Verify plan should be available under the current plan");
		Reporter.log(
				"9. Select military plan from feature plan |Verify plan should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanComparisonPageTestDirectly(myTmoData, "Magenta Military");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyFuturePlanGetVerifiedBtn();
		MilitaryVerificationPage millitaryVerificationPage = new MilitaryVerificationPage(getDriver());
		millitaryVerificationPage.verifyMilitaryVerificationPage();
	}

	/*
	 * Get Eligible Plan for - TestGetEligiblePlansforTMobileEssentials
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testGetEligiblePlansforTMobileEssentials(ControlTestData data, MyTmoData myTmoData) {
		logger.info("TestGetEligiblePlansforTMobileEssentials");
		Reporter.log("Test Case : TestGetEligiblePlansforTMobileEssentials");
		Reporter.log("Test Data : Any PAH/Full customer with TMobileEssentials");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on available plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log(
				"8. Verify Available plan from currnet plan |Eligble palns should be available along with current plan under the current plan");
		Reporter.log(
				"9. Verify Available plan from Choose this plan |Eligble plans should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanComparisonPageTestDirectly(myTmoData, "T-Mobile Essentials");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.checkEligibleplansfromCurrentPlaSectionforTMobileEssentials();
	}

	/*
	 * Get Eligible Plan for - TestGetEligiblePlansforTMobileONEMilitary
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testGetEligiblePlansforTMobileONEMilitary(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testGetEligiblePlansforTMobileONEMilitary");
		Reporter.log("Test Case : Test Get Eligible Plans for TMobile ONE Military");
		Reporter.log("Test Data : Any PAH/Full customer with TMobileONEMilitary");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on available plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log(
				"8. Verify Available plan from currnet plan |Eligble palns should be available along with current plan under the current plan");
		Reporter.log(
				"9. Verify Available plan from Choose this plan |Eligble plans should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanComparisonPageTestDirectly(myTmoData, "");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.checkEligibleplansfromCurrentPlaSectionforTMobileONEMilitary();
	}

	/*
	 * Get Eligible Plan for - testGetEligiblePlansforLegacyPlan_TalkandText5
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testGetEligiblePlansforLegacyPlanTalkandText(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testGetEligiblePlansforLegacyPlan_TalkandText");
		Reporter.log("Test Case : testGetEligiblePlansforLegacyPlan_TalkandText");
		Reporter.log("Test Data : Any PAH/Full customer with TalkandText");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on available plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log(
				"8. Verify Available plan from currnet plan |Eligble palns should be available along with current plan under the current plan");
		Reporter.log(
				"9. Verify Available plan from Choose this plan |Eligble plans should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanComparisonPageTestDirectly(myTmoData, planName);

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.checkEligibleplansfromCurrentPlaSectionforTalkandText();
	}

	/*
	 * US570542 : [Plan Comparison Page] Error Page for Std/Restricted user for Deep
	 * linking
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testDeeplinkingPlanComparisonPageforRestrictedUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testDeeplinkingPlanComparisonPageforRestrictedUser");
		Reporter.log("Test Case : testDeeplinkingPlanComparisonPageforRestrictedUser");
		Reporter.log("Test Data : Restricted User / Standard User");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. DeepLinking planComparion Plan Page | Plan Comparison Page should be displayed");
		Reporter.log("5. Verify that Notification Message| Notification Message should be available");
		Reporter.log("Actual Results:");

		navigateToFutureURLFromHome(myTmoData, "change-plan/plan-comparison/#UNLMTT");
		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyOOPsError();
		exploreplanpage.verifyNotificationErrorMessage();
		exploreplanpage.verifyBacktcta();
	}

	/*
	 * Get Eligible Plan for - testGetEligiblePlansfor_DigitsLine
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testGetEligiblePlansforDigitsLine(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testGetEligiblePlansfor_DigitsLine");
		Reporter.log("Test Case : Test Get Eligible Plans for Digits Line");
		Reporter.log("Test Data : 4253898178/Auto12345 is pah 4th line is digits (425) 443-1421, ban - 966610177");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | Account Overview Page should be displayed");
		Reporter.log("5. Click on Digits line | LineDetails Page should be displayed");
		Reporter.log("6. Click on Plan Name | Plan Details Page should be displayed");
		Reporter.log("7. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("8. Click on available plan |Navigated to plan Comparison Page");
		Reporter.log("9. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log(
				"10. Verify Available plan from currnet plan |Eligble palns should be available along with current plan under the current plan");
		Reporter.log(
				"11. Verify Available plan from Choose this plan |Eligble plans should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		ExplorePlanPage exploreplanpage = navigateToExplorePlansPage(myTmoData);

		exploreplanpage.verifyErrorMessageforBestPlan();
		exploreplanpage.verifyManageAddonCta();
		exploreplanpage.verifyBackCtaonExplorePage();
	}

	/*
	 * TaxMigration - testTaxMigrationChangePlanfromTItoTEforNonPooledCustomer
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testTaxMigrationChangePlanfromTItoTEforNonPooledCustomer(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testTaxMigrationChangePlanfromTItoTEforNonPooledCustomer");
		Reporter.log("Test Case : Test Tax Migration Change Plan from TI to TE for Non Pooled Customer");
		Reporter.log("Test Data : Any NonPooled PAH Customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | Account Overview Page should be displayed");
		Reporter.log("5. Click on available line | LineDetails Page should be displayed");
		Reporter.log("6. Click on Plan Name | Plan Details Page should be displayed");
		Reporter.log("7. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("8. Click on any TE plan |Navigated to plan Comparison Page");
		Reporter.log("9. Verify that Contact US CTA on Choose Plan Section |Contact cta should be available");
		Reporter.log("10. Clcik on ContactUs cta |ContactUs page should displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanComparisonPageTestDirectly(myTmoData, "T-Mobile Essentials");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyContactCtabeowChoosePlan();
	}

	/*
	 * US512787 : [Plan Comparison] More Details
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMoreDetailsOnnewPlanComparisionPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyMoreDetailsonnewPlanComparisionPage");
		Reporter.log("Test Case : verifyMoreDetailsonnewPlanComparisionPage");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on feature plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that MoreDetails |MoreDetails Componenet  should be available");
		Reporter.log("8. Click on the Feature |ModalWindow open with description");
		Reporter.log("9. Clcik on X | ModalWindos closed and landed in Comparision page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanComparisonPageTestDirectly(myTmoData, planName);

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		// newplancomparisionpage.verifyListOfMoreDetailsHeaders();
		newplancomparisionpage.clickOnChooseThisPlanSection();
		newplancomparisionpage.clickOnChooseThisPlanByName("Magenta®");
		newplancomparisionpage.verifyPlanByPrice("$140.00");
		newplancomparisionpage.clickOnChooseThisPlanByName("Magenta® Plus Military");
		newplancomparisionpage.verifyPlanByPrice("$120.00");
		newplancomparisionpage.clickOnChooseThisPlanByName("T-Mobile Essentials");
		newplancomparisionpage.verifyPlanByPrice("$105.00");
		newplancomparisionpage.clickOnChooseThisPlanByName("Magenta® Plus");
		newplancomparisionpage.verifyPlanByPrice("$170.00");
		newplancomparisionpage.clickOnChooseThisPlanByName("Magenta® Military");
		newplancomparisionpage.verifyPlanByPrice("$90.00");
	}

	/*
	 * US523878 : [Plan Comparison] Military Plan availability for Change Plan
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void veiffy55PlusplanSelectionforEligibleCustomer(ControlTestData data, MyTmoData myTmoData) {
		logger.info("veiffy55PlusplanSelectionforEligibleCustomer");
		Reporter.log("Test Case : veiffy55PlusplanSelectionforEligibleCustomer");
		Reporter.log("Test Data : Any PAH/Full customer with 55Plusplan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on 55plus plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA |selectPlan should be available");
		Reporter.log("8. Select 55plus plan from currnet plan |selectPlan should be available under the current plan");
		Reporter.log("9. Select 55plus plan from feature plan |selectPlan should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanReviewPageTest(myTmoData, "T-Mobile ONE Unlimited 55");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifySelectPlanCta(1);
		newplancomparisionpage.select55PlusPlanfromCurrentPlanDropDown();
		newplancomparisionpage.verifySelectPlanCta(0);
	}

	/*
	 * Get Eligible Plan for - T-Mobile ONE with ONE Plus No Credit Check
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testGetEligiblePlansforTMobileONEwithONEPlusNoCreditCheck(ControlTestData data, MyTmoData myTmoData) {
		logger.info("TestGetEligiblePlansforTMobileONEwithONEPlusNoCreditCheck");
		Reporter.log("Test Case : TestGetEligiblePlansforTMobileONEwithONEPlusNoCreditCheck");
		Reporter.log("Test Data : Any PAH/Full customer with TMobileONEwithONEPlusNoCreditCheck");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on available plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log(
				"8. Verify Available plan from currnet plan |Eligble palns should be available along with current plan under the current plan");
		Reporter.log(
				"9. Verify Available plan from Choose this plan |Eligble plans should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanComparisonPageTestDirectly(myTmoData, "planName");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.checkEligibleplansfromCurrentPlanSection();
	}

	/*
	 * Get Eligible Plan for -
	 * TestGetEligiblePlansforSimpleChoiceNorthAmericaNoCreditCheck
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testGetEligiblePlansforSimpleChoiceNorthAmericaNoCreditCheck(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("TestGetEligiblePlansforSimpleChoiceNorthAmericaNoCreditCheck");
		Reporter.log("Test Case : TestGetEligiblePlansforSimpleChoiceNorthAmericaNoCreditCheck");
		Reporter.log("Test Data : Any PAH/Full customer with SimpleChoiceNorthAmericaNoCreditCheck");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on available plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log(
				"8. Verify Available plan from currnet plan |Eligble palns should be available along with current plan under the current plan");
		Reporter.log(
				"9. Verify Available plan from Choose this plan |Eligble plans should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanComparisonPageTestDirectly(myTmoData, "planName");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.checkEligibleplansfromCurrentPlanSectionforSCNANCC();

	}

	/*
	 * Get Eligible Plan for - TestGetEligiblePlansforSimpleChoiceNorthAmerica
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testGetEligiblePlansforSimpleChoiceNorthAmerica(ControlTestData data, MyTmoData myTmoData) {
		logger.info("TestGetEligiblePlansforSimpleChoiceNorthAmerica");
		Reporter.log("Test Case : TestGetEligiblePlansforSimpleChoiceNorthAmerica");
		Reporter.log("Test Data : Any PAH/Full customer with SimpleChoiceNorthAmerica");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on available plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log(
				"8. Verify Available plan from currnet plan |Eligble palns should be available along with current plan under the current plan");
		Reporter.log(
				"9. Verify Available plan from Choose this plan |Eligble plans should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanComparisonPageTestDirectly(myTmoData, "Simple Choice North America");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.checkEligibleplansfromCurrentPlanSectionforSCNA();

	}

	/*
	 * Get Eligible Plan for - TestGetEligiblePlansforTMobileONE
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testGetEligiblePlansforTMobileONE(ControlTestData data, MyTmoData myTmoData) {
		logger.info("TestGetEligiblePlansforTMobileONE");
		Reporter.log("Test Case : TestGetEligiblePlansforTMobileONE");
		Reporter.log("Test Data : Any PAH/Full customer with TMobileONE");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on available plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log(
				"8. Verify Available plan from currnet plan |Eligble palns should be available along with current plan under the current plan");
		Reporter.log(
				"9. Verify Available plan from Choose this plan |Eligble plans should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanComparisonPageTestDirectly(myTmoData, "planName");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.checkEligibleplansfromCurrentPlanSectionforTMobileONE();
	}

	/*
	 * Get Eligible Plan for - TestGetEligiblePlansforTMobileONEwithONEPlus
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testGetEligiblePlansforTMobileONEwithONEPlus(ControlTestData data, MyTmoData myTmoData) {
		logger.info("TestGetEligiblePlansforTMobileONEwithONEPlus");
		Reporter.log("Test Case : TestGetEligiblePlansforTMobileONEwithONEPlus");
		Reporter.log("Test Data : Any PAH/Full customer with TMobileONEwithONEPlus");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on available plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log(
				"8. Verify Available plan from currnet plan |Eligble palns should be available along with current plan under the current plan");
		Reporter.log(
				"9. Verify Available plan from Choose this plan |Eligble plans should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanComparisonPageTestDirectly(myTmoData, "planName");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.checkEligibleplansfromCurrentPlaSectionforTMobileONEPlus();
	}

	/*
	 * Get Eligible Plan for - TestGetEligiblePlansforTMobileONEUnlimited55
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void TestGetEligiblePlansforTMobileONEUnlimited55(ControlTestData data, MyTmoData myTmoData) {
		logger.info("TestGetEligiblePlansforTMobileONEUnlimited55");
		Reporter.log("Test Case : TestGetEligiblePlansforTMobileONEUnlimited55");
		Reporter.log("Test Data : Any PAH/Full customer with TMobileONEUnlimited55");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Click on available plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log(
				"8. Verify Available plan from currnet plan |Eligble palns should be available along with current plan under the current plan");
		Reporter.log(
				"9. Verify Available plan from Choose this plan |Eligble plans should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanComparisonPageTestDirectly(myTmoData, "planName");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.checkEligibleplansfromCurrentPlaSectionforTMobileONEUnlimited55();
	}

	/*
	 * Get Eligible Plan for - testGetEligiblePlansfor_HomeInternetLine
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testGetEligiblePlansforHomeInternetLine(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testGetEligiblePlansfor_HomeInternetLine");
		Reporter.log("Test Case : testGetEligiblePlansfor_HomeInternetLine");
		Reporter.log("Test Data : 4708196827/Auto12345 is pah 2nd line is Home Internet  4253658114");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | Account Overview Page should be displayed");
		Reporter.log("5. Click on HomeInternet line | LineDetails Page should be displayed");
		Reporter.log("6. Click on Plan Name | Plan Details Page should be displayed");
		Reporter.log("7. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("8. Click on available plan |Navigated to plan Comparison Page");
		Reporter.log("9. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log(
				"10. Verify Available plan from currnet plan |Eligble palns should be available along with current plan under the current plan");
		Reporter.log(
				"11. Verify Available plan from Choose this plan |Eligble plans should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		ExplorePlanPage explorePlanPage = navigateToExplorePlansPage(myTmoData);

		explorePlanPage.verifyExplorePageforHomeInternet();
		explorePlanPage.verifyErrorMessageforBestPlan();
		explorePlanPage.verifyManageAddonCta();
		explorePlanPage.verifyBackCtaonExplorePage();
	}

	/*
	 * Get Eligible Plan for - testGetEligiblePlansfor_WearablePlan
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testGetEligiblePlansforWearablePlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testGetEligiblePlansfor_WearablePlan");
		Reporter.log("Test Case : testGetEligiblePlansfor_WearablePlan");
		Reporter.log("Test Data : 4156297432/Auto12345 is pah 2nd line is wearable   (425) 373-6706, ban - 964805244”");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | Account Overview Page should be displayed");
		Reporter.log("5. Click on Wearable line | LineDetails Page should be displayed");
		Reporter.log("6. Click on Plan Name | Plan Details Page should be displayed");
		Reporter.log("7. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("8. Click on available plan |Navigated to plan Comparison Page");
		Reporter.log("9. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log(
				"10. Verify Available plan from currnet plan |Eligble palns should be available along with current plan under the current plan");
		Reporter.log(
				"11. Verify Available plan from Choose this plan |Eligble plans should be available under the feature plan");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnWearablePlan();
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
		exploreplanpage.verifyAvailablePlansinExplorePageforWearable();
		exploreplanpage.slectNewPlan();

		plandetailspage.verifyExplorePlanDetailsPageLoad();
		// plandetailspage.verifyAvailableCtaonExplorePlanDetailsPage();
		plandetailspage.clickOnBackButton();

	}

	/*
	 * TaxMigration - testChangePlanforMorethan12linesCustomer
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testChangePlanforMorethan12linesCustomer(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testChangePlanforMorethan12linesCustomer");
		Reporter.log("Test Case : testChangePlanforMorethan12linesCustomer");
		Reporter.log("Test Data : Any PAH Customer with more than 12 lines");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | Account Overview Page should be displayed");
		Reporter.log("5. Click on available line | LineDetails Page should be displayed");
		Reporter.log("6. Click on Plan Name | Plan Details Page should be displayed");
		Reporter.log("7. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("8. Click on any available plan |Navigated to plan Comparison Page");
		Reporter.log("9. Verify that Contact US CTA on Choose Plan Section |Contact cta should be available");
		Reporter.log("10. Clcik on ContactUs cta |ContactUs page should displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToPlanComparisonPageTestDirectly(myTmoData, "planName");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifyContactCtabeowChoosePlan();
	}

	/*
	 * US578753 : Display Message for Plan Comparison Deep Linking Logic
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testErrorMessageNewPlanCompariosnDeepLinkingforUnAvailablePlan(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("testNewPlanCompariosnDeepLinking");
		Reporter.log("Test Case : testNewPlanCompariosnDeepLinking");
		Reporter.log("Test Data : PAH User");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log(
				"4. DeepLinking planComparion Plan Page with Un-Available planSoc | Plan Comparison Page should be displayed");
		Reporter.log("5. Verify that Notification Message| Notification Message should be available");
		Reporter.log("Actual Results:");

		navigateToFutureURLFromHome(myTmoData, "change-plan/plan-comparison/#UNLMTT");

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.errorMessageforUnAvaiablePlan();
	}

	/*
	 * US582121 : [Best Plan logic] Navigation to Explore PLans page for Plan
	 * comparison deep link
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testErrorMessageNewPlanCompariosnDeepLinkingforBestPlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testNewPlanCompariosnDeepLinking");
		Reporter.log("Test Case : testNewPlanCompariosnDeepLinking");
		Reporter.log("Test Data : PAH User");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log(
				"4. DeepLinking planComparion Plan Page with Un-Available planSoc | Plan Comparison Page should be displayed");
		Reporter.log("5. Verify that Notification Message| Notification Message should be available");
		Reporter.log("Actual Results:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.deeplinkNewPlanComparisonPageforBestPlan();

		ExplorePlanPage explorePlanPage = new ExplorePlanPage(getDriver());
		explorePlanPage.verifyErrorMessageforBestPlan();
		explorePlanPage.verifyManageAddonCta();
		explorePlanPage.verifyManageAddonPage();
	}

	/*
	 * CDCWW-963 : Integration] [MBB Line] Implement deep-linking to plan comparison
	 * page (SOC&MSISDN)
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testDeeplinkingPlanComparisonPageforMBBUserwithSOCandMSISDN(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testDeeplinkingPlanComparisonPageforRestrictedUser");
		Reporter.log("Test Case : testDeeplinkingPlanComparisonPageforRestrictedUser");
		Reporter.log("Test Data : MBB User - 7134981280 /TM0Test12");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. DeepLinking planComparion Plan Page | Plan Comparison Page should be displayed");
		Reporter.log("5. Verify that Explore Page | Explore Page should be available");
		Reporter.log("Actual Results:");

		navigateToFutureURLFromHome(myTmoData, "change-plan/plan-comparison/959091261/NAMI22GB");

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
	}
}