/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.accounts.ExplorePlanPage;
import com.tmobile.eservices.qa.pages.accounts.LineDetailsPage;

/**
 * @author prokarma
 *
 */
public class ExplorePlanPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(ExplorePlanPageTest.class);

	// Regression

	/*
	 * US512564 : [Explore Plans Page] Display Legal Text
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifLegalTextonExplorePage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifLegalTextonExplorePage");
		Reporter.log("Test Case : verifLegalTextonExplorePage");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Launch AccountOverview Page | Accout Oveview page should be displayed");
		Reporter.log("5. Clcik on the plan name | Plan Details Page loaded Successfully");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Verify that LegalText |LealText should be available");
		Reporter.log("7. Click on See Full Detail Link |Navigated to LegalInformation");
		Reporter.log("8. Clcik on BACK cts | Navigated to Previous Page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		ExplorePlanPage explorePlanPage = navigateToExplorePlansPageDirectly(myTmoData);

		explorePlanPage.verifyExplorePage();
		explorePlanPage.verifyLegalTextOnExplore();
		explorePlanPage.seeFullDetailsLinkClick();
		explorePlanPage.verifyThatLegalPage();
		explorePlanPage.verifyBackCta();
		explorePlanPage.verifyExplorePage();
	}

	/*
	 * US591339 : [Change Plan- Fit and Finish] Update the Explore Plans Page URL
	 * link to accept the Hashed MSISDN
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testDeeplinkingHashedMissdinforValidMissdin(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testDeeplinkingHashedMissdinforValidMissdin");
		Reporter.log("Test Case : SuccessAddnewlineScenario");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Launch Explore Page | ExplorePage should be displayed along with Expected Component");

		Reporter.log("Actual Results:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		navigateToFutureURLFromHome(myTmoData, "change-plan/explore-plans#966829839");
		exploreplanpage.verifyExplorePage();
		exploreplanpage.verifyCurrentandChoosePlanComponent();
	}

	/*
	 * US591339 : [Change Plan- Fit and Finish] Update the Explore Plans Page URL
	 * link to accept the Hashed MSISDN
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testDeeplinkingHashedMissdinforInValidMissdin(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testDeeplinkingHashedMissdinforInValidMissdin");
		Reporter.log("Test Case : test Deeplinking Hashed Missd in for In Valid Missdin");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Launch Explore Page | ExplorePage should be displayed along with Expected Component");

		Reporter.log("Actual Results:");

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		navigateToFutureURLFromHome(myTmoData, "change-plan/explore-plans#966829839");

		exploreplanpage.verifyExplorePage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testExplorePlansPageUpsellURL(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testExplorePlansPageUpsellURL");
		Reporter.log("Test Case : test Deeplinking To Explore Plans Page");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Get Explore Page upsell URL | ExplorePage should be displayed");
		Reporter.log("Actual Results:");

		navigateToFutureURLFromHome(myTmoData, "/account/change-plan/explore-plans");
		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
	}

	/*
	 * US556955 :[Change Plan - Fit and Finish] Updates to Explore Plans Landing
	 * Page
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyExplorePlansLandingPageDetails(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyExplorePlansLandingPageDetails");
		Reporter.log("Test Case : VerifyExplorePlansLandingPage");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Launch AccountOverview Page | Accout Oveview page should be displayed");
		Reporter.log("5. Clcik on the plan name | Plan Details Page loaded Successfully");
		Reporter.log("5. Click on ChangePlan CTA |LineAffected component should available");
		Reporter.log("6. Verify that affectedline details |LealText should be available");
		Reporter.log("7. Verify that Current Plan Component |Current Component should be available");
		Reporter.log("7. Verify that Choose a Plan Component |Choose a Plan Component should be available");
		Reporter.log("Actual Results:");

		ExplorePlanPage exploreplanpage = navigateToExplorePlansPageDirectly(myTmoData);
		exploreplanpage.clickOnSeeMorePlansLink();

		exploreplanpage.verifyTMobileOnePlanDetailsSection();
		exploreplanpage.verifyMagentaPlusPlanDetailsSection();
		exploreplanpage.verifyMagentPlanDetailsSection();
		exploreplanpage.verifyTMobileEssentialsPlanDetailsSection();
		exploreplanpage.verifyMagentaPlusMilitaryPlanDetailsSection();
		exploreplanpage.verifyMagentaTMMilitaryPlanDetailsSection();
	}

	// Regression End

	/*
	 * US519640 : [Change (Explore) Plan] Auto-pay error message
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyAutoPayErrorMessageonExplorePage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAutoPayErrorMessageonExplorePage");
		Reporter.log("Test Case : Verify Auto Pay Error Message on Explore Page ");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Launch AccountOverview Page | Accout Oveview page should be displayed");
		Reporter.log("5. Clcik on the plan name | Plan Details Page loaded Successfully");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Verify autoPay error message |AutoPay Error Message should be available");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		ExplorePlanPage exploreplanpage = navigateToExplorePlansPageDirectly(myTmoData);
		exploreplanpage.verifyAutoPayErrorMessage();
	}

	/*
	 * US534678 :[Change Plan- Explore and Compare page] Best Plan Scenario
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyErrorMessageforBestPlanCustomer(ControlTestData data, MyTmoData myTmoData) {
		logger.info("SuccessAddnewlineScenario");
		Reporter.log("Test Case : SuccessAddnewlineScenario");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Launch AccountOverview Page | Accout Oveview page should be displayed");
		Reporter.log("5. Clcik on the plan name | Plan Details Page loaded Successfully");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Verify BestPlan Error |BestPlan Error should be available");
		Reporter.log("Actual Results:");

		ExplorePlanPage exploreplanpage = navigateToExplorePlansPageDirectly(myTmoData);
		exploreplanpage.verifyExplorePageforHomeInternet();
		exploreplanpage.verifyErrorMessageforBestPlan();
		exploreplanpage.verifyManageAddonCta();
		exploreplanpage.verifyBackCtaonExplorePage();
	}

	/*
	 * US573310 :[Change Plan- Pending Rate Plan Change deep linking for Explore
	 * Plan page
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testPendingPlanErrorMessageforDeeplinkingExplorePlanPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testPendingPlanErrorMessageforDeeplinkingExplorePlanPage");
		Reporter.log("Test Case : testPendingPlanErrorMessageforDeeplinkingExplorePlanPage");
		Reporter.log("Test Data : Any PAH/Full customer with Pending Rate Plan ");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. DeepLinking Explore Plan Page | Explore Plan Page should be displayed");
		Reporter.log("5. Verify the Pending Plan Error Message| Peding Plan Error Message should be available");
		Reporter.log("Actual Results:");

		ExplorePlanPage exploreplanpage = navigateToExplorePlansPageDirectly(myTmoData);
		exploreplanpage.verifyChangePlanHeading();
		exploreplanpage.verifyPendingErrorMessage();
		exploreplanpage.verifyContactUsforPendingPlan();
		exploreplanpage.navigateBack();
		exploreplanpage.verifyBackCtaforPendingPlan();

		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		accountOverviewPage.verifyAccountOverviewPage();
	}
	/*
	 * US570540 : [Explore Plans Page] Error Page for Std/Restricted user for Deep
	 * linking
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testDeeplinkingExplorePageforRestrictedUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testDeeplinkingExplorePageforRestrictedUser");
		Reporter.log("Test Case : Test Deep linking Explore Page for Restricted User");
		Reporter.log("Test Data : Restricted User / Standard User");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. DeepLinking Explore Plan Page | Explore Plan Page should be displayed");
		Reporter.log("5. Verify that Notification Message| Notification Message should be available");
		Reporter.log("Actual Results:");

		navigateToFutureURLFromHome(myTmoData, "/account/change-plan/explore-plans");

		ExplorePlanPage explorePlanPage = new ExplorePlanPage(getDriver());
		explorePlanPage.verifyOOPsError();
		explorePlanPage.verifyNotificationErrorMessage();
		explorePlanPage.verifyBacktcta();

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
	}

	/*
	 * US591339 : [Change Plan- Fit and Finish] Update the Explore Plans Page URL
	 * link to accept the Hashed MSISDN
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testDeeplinkingHashedMissdinforPendingPlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testDeeplinkingHashedMissdinforPendingPlan");
		Reporter.log("Test Case : testDeeplinkingHashedMissdinforPendingPlan");
		Reporter.log("Test Data : Any PAH/Full customer with Pending Rate Plan ");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. DeepLinking HassedMissdin Explore Plan Page | Explore Plan Page should be displayed");
		Reporter.log("5. Verify the Pending Plan Error Message| Peding Plan Error Message should be available");
		Reporter.log("Actual Results:");

		navigateToFutureURLFromHome(myTmoData, "/change-plan/explore-plans#7862706415");
		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());

		exploreplanpage.verifyChangePlanHeading();
		exploreplanpage.verifyPendingErrorMessage();
		exploreplanpage.verifyBackCtaforPendingPlan();

		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		accountOverviewPage.verifyAccountOverviewPage();
		exploreplanpage.verifyContactUsforPendingPlan();
	}

	/*
	 * US591339 : [Change Plan- Fit and Finish] Update the Explore Plans Page URL
	 * link to accept the Hashed MSISDN
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testDeeplinkingHashedMissdinforBestPlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testDeeplinkingHashedMissdinforBestPlan");
		Reporter.log("Test Case : testDeeplinkingHashedMissdinforBestPlan");
		Reporter.log("Test Data : Any PAH/Full customer with Best Plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Launch AccountOverview Page | Accout Oveview page should be displayed");
		Reporter.log("5. Clcik on the plan name | Plan Details Page loaded Successfully");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage shold be loaded");
		Reporter.log("6. Verify BestPlan Error |BestPlan Error should be available");
		Reporter.log("Actual Results:");

		navigateToFutureURLFromHome(myTmoData, "/change-plan/explore-plans#3366531082");

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyErrorMessageforBestPlan();
		exploreplanpage.verifyManageAddonCta();
		exploreplanpage.verifyBackCtaonExplorePage();
	}

	/*
	 * Check whether Explore page is loading or not when user clicks Change Plan
	 * link on Home page
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void checkExplorePageDeeplinking(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkExplorePageDeeplinking");
		Reporter.log(
				"Test Case : Check whether Explore page loads or not when user clicks Change plan link on Home page");
		Reporter.log("Test Data : Any PAH/Full customer without pending rate plan changes");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Change Plan link on I want to section | Explore Plan page should be displayed");
		Reporter.log("Actual Results:");

		navigateToExplorePlansPageDirectly(myTmoData);

	}

	/*
	 * Check whether Comparison page is loading or not when user directly hit
	 * Comparison page url
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void checkPlansComparisonPageWhenUserHitDeeplinking(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkPlansComparisonPageWhenUserHitDeeplinking");
		Reporter.log(
				"Test Case : Check whether Comparison page loads or not when user hits direct url of Comparison page");
		Reporter.log("Test Data : Any PAH/Full customer without pending rate plan changes");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Hit direct url of Plans Comparison page | Plans Comparison page should be displayed");
		Reporter.log("Actual Results:");

		navigateToPlansComparisonPageDirectly(myTmoData);

	}

}