/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.PrepaidProfileLandingPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class PrepaidProfileLandingPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(PrepaidProfileLandingPageTest.class);

	/*
	 * CDCWW-1484 [Prepaid Profile Page .NET Migration] Profile Landing Page
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyAllTabsOnPrepaidProfileLandingPageAndTheirDescription(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAllTabsOnPrepaidProfileLandingPageAndTheirDescription");
		Reporter.log("Test Case : Check all tabs on Prepaid Profile page and description on all tabs.");
		Reporter.log("Test Data : Any valid Prepaid customer");
		Reporter.log("==================================");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Verify URL, Header | It should be displayed");
		Reporter.log(
				"6. Check Billing and Payments tab and description under it | Billing and payments tab should be displayed with description.");
		Reporter.log(
				"7. Check T-Mobile ID tab and description under it | T-Mobile ID tab should be displayed with description.");
		Reporter.log(
				"8. Check Line Settings tab and description under it | Line Settings tab should be displayed with description.");
		Reporter.log(
				"9. Check Privacy & Notifications tab and description under it | Privacy & Notifications tab should be displayed with description.");
		Reporter.log(
				"10. Check Family Controls tab and description under it | Family Controls tab should be displayed with description.");
		Reporter.log(
				"11. Check Multiple Devices tab and description under it | Multiple Devices tab should be displayed with description.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);

		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.directURLForPrepaidProfilePage();
		prepaidProfilePage.verifyPrePaidProfilePage();
		
		prepaidProfilePage.checkBillingAndPaymentsTabAndDescriptionUnderIt();

		prepaidProfilePage.checkTMobileIDTabAndDescriptionUnderIt();

		prepaidProfilePage.checkLineSettingsTabAndDescriptionUnderIt();

		prepaidProfilePage.checkPrivacyAndNotificationsTabAndDescriptionUnderIt();

		prepaidProfilePage.checkFamilyControlTabAndDescriptionUnderIt();

		prepaidProfilePage.checkMultipleDevicesTabAndDescriptionUnderIt();

	}

}