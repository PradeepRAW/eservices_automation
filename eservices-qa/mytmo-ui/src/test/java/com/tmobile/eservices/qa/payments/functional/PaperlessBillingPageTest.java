package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class PaperlessBillingPageTest extends PaymentCommonLib {

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testVerifyPaperlessBillingPageLoading(ControlTestData data, MyTmoData myTmoData) throws Exception {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log("Step 4: click on paperless bill| Paper less billing page should be dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		navigateToPaperlessBillingPage(myTmoData);
	}
}
