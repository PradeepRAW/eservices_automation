package com.tmobile.eservices.qa.shop.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.EOSCommonLib;
import com.tmobile.eservices.qa.pages.accounts.api.EOSODF;
import com.tmobile.eservices.qa.pages.accounts.api.EOSPartnerBenefits;
import com.tmobile.eservices.qa.pages.global.api.EOSProfile;
import com.tmobile.eservices.qa.pages.payments.api.EOSAALEligibilityCheckApiV1;
import com.tmobile.eservices.qa.pages.payments.api.EOSAccounts;
import com.tmobile.eservices.qa.pages.payments.api.EOSBillingFilters;
import com.tmobile.eservices.qa.pages.payments.api.EOSPAFilter;
import com.tmobile.eservices.qa.pages.payments.api.EOSPaymentmanager;
import com.tmobile.eservices.qa.pages.payments.api.EOSservicequotefilter;
import com.tmobile.eservices.qa.pages.payments.api.EOSusageFilter;
import com.tmobile.eservices.qa.payments.api.invalidtestdataidentifier;

import io.restassured.response.Response;

public class TestDataFilterShopQlab02 {

	private static final Logger logger = LoggerFactory.getLogger(TestDataFilterShopQlab02.class);

	EOSCommonLib ecl = new EOSCommonLib();
	EOSBillingFilters billing = new EOSBillingFilters();
	EOSAccounts accounts = new EOSAccounts();
	EOSProfile profile = new EOSProfile();
	EOSODF odf = new EOSODF();
	EOSPartnerBenefits partnerbenefits = new EOSPartnerBenefits();
	EOSPaymentmanager paymentmanger = new EOSPaymentmanager();
	EOSPAFilter eospa = new EOSPAFilter();
	EOSusageFilter eosusage = new EOSusageFilter();
	EOSservicequotefilter eossquote = new EOSservicequotefilter();
	EOSAALEligibilityCheckApiV1 eosAALCheck = new EOSAALEligibilityCheckApiV1();

	@DataProvider(name = "testdata", parallel = true)
	public Iterator<Object[]> prepareBooleans() {
		String FILE_NAME;
		FILE_NAME = System.getProperty("user.dir") + "/src/test/resources/testdata/testdatavalidatorQLAB02.xlsx";
		Set<Object[]> misdinpawords = getallmisdins(FILE_NAME);
		return misdinpawords.iterator();
	}

	invalidtestdataidentifier gaps = new invalidtestdataidentifier();

	@Test(dependsOnMethods = { "Testdataevaluation" })

	public void identifygaps() throws IOException {
		gaps.tesdatagapidentifier();
	}

	@Test(dataProvider = "testdata")
	public void Testdataevaluation(String misdpwd) {

		try {
			boolean itest = true;

			String misdin = misdpwd.split("/")[0];
			String pwd = misdpwd.split("/")[1];
			if (misdin.trim().length() > 0 && pwd.trim().length() > 0) {

				// Reporter.log("<tr>");
				JSONObject adddata = new JSONObject();
				adddata.put("id", misdin);
				adddata.put("misdin", misdpwd);
				String[] getjwt = ecl.getauthorizationandregisterinapigee(misdin, pwd);
				if (getjwt != null) {

					String accesscode = getjwt[1];
					String jwttoken = getjwt[2];
					String ban = getjwt[3];
					String usertype = getjwt[4];
					String mailid = getjwt[5];
					String firstname = getjwt[6];
					String lastname = getjwt[7];
					String acctype = getjwt[8];
					String status = getjwt[9];
					String lines = getjwt[10];
					getjwt[11] = misdin;
					adddata.put("misdin", misdpwd);
					adddata.put("ban", ban);
					adddata.put("usrtype", usertype);
					adddata.put("acctype", acctype);
					adddata.put("status", status);
					adddata.put("lines", lines);

					Response sedonaquote = eossquote.getResponsesedonaquote(getjwt);
					String issedona = eossquote.checkissedona(sedonaquote);
					if (issedona != null) {
						adddata.put("issedona", issedona);
					}

					Map<String, String> tokenMap = new HashMap<String, String>();
					String requestBody = new ServiceTest().getRequestFromFile("testAALEligibility.txt");
					tokenMap.put("ban", ban);
					tokenMap.put("msisdn", misdin);
					String updatedRequest = prepareRequestParam(requestBody, tokenMap);
					Response eosAALCheckResponse = eosAALCheck.eligibilityCheckAAL(getjwt, updatedRequest);
					String isAALEligible = eosAALCheck.isAALEligible(eosAALCheckResponse);
					if (isAALEligible != null) {
						adddata.put("isAALEligible", isAALEligible);
					}
					// Billing-getDataset
					Response datasetrespons = billing.getResponsedataset(getjwt);
					/*
					 * String ispa=billing.getpaeligibility(datasetrespons); if(ispa!=null)
					 * {adddata.put("pa", ispa);}
					 */
					String iseip = billing.geteipdetails(datasetrespons);
					if (iseip != null) {
						adddata.put("eip", iseip);
					}
					String isauto = billing.getautopaystatus(datasetrespons);
					if (isauto != null) {
						adddata.put("auto", isauto);
					}
					String haspastdue = billing.getIspastdueAmount(datasetrespons);
					if (haspastdue != null) {
						adddata.put("haspastdue", haspastdue);
					}
					String hasarbalance = billing.getARbalance(datasetrespons);

					// Billing-getBillList
					Response billlistrespons = billing.getResponsebillList(getjwt);
					String billtype = billing.getbilltype(billlistrespons);
					if (billtype != null) {
						adddata.put("bill", billtype);
					}
					String previousbills = billing.checkpreviousbills(billlistrespons);
					if (previousbills != null) {
						adddata.put("priviousbill", previousbills);
					}

					Response getpaaymentmethods = accounts.getResponsepaymentMethods(getjwt);
					String bpeligible = accounts.geteligibilityforbankpayment(getpaaymentmethods);
					if (bpeligible != null) {
						adddata.put("bankpaymenteligible", bpeligible);
					}

					String[] getstoredinfo = accounts.getstoreddata(getpaaymentmethods);
					if (getstoredinfo[0] != null)
						adddata.put("storedbank", getstoredinfo[0]);
					if (getstoredinfo[1] != null)
						adddata.put("storecreditcard", getstoredinfo[1]);
					if (getstoredinfo[2] != null)
						adddata.put("storedebitcard", getstoredinfo[1]);

					// profile
					Response getprofile = profile.getProfileresponse(getjwt);
					String billstate = profile.getbillingState(getprofile);
					if (billstate != null) {
						adddata.put("billstate", billstate);
					}

					// payment manager
					Response getpainfo = paymentmanger.getPAinformationresponse(getjwt);
					String pasetup = paymentmanger.isPAAlreadysetup(getpainfo);
					if (pasetup != null) {
						adddata.put("paalreadysetup", pasetup);
					}
					String paval = paymentmanger.ispaeligible(getpainfo);
					if (paval != null) {
						if (paval.equalsIgnoreCase("true") && hasarbalance.equalsIgnoreCase("true"))
							adddata.put("pa", "true");
						else
							adddata.put("pa", "false");
					}

					Response subscriberinfo = paymentmanger.getsubscriberinforesponse(getjwt);
					String autopayeligible = paymentmanger.isautopayeligible(subscriberinfo);
					if (autopayeligible != null) {
						adddata.put("autopayeligible", autopayeligible);
					}
					String fdpeligible = paymentmanger.isfdpeligible(subscriberinfo);
					if (fdpeligible != null) {
						adddata.put("fdpeligible", fdpeligible);
					}

					// ODF
					Response odfinfo = odf.getResponseODF(getjwt);
					List<String> allcurrenservicedetails = odf.getallCurrentServiceDetails(odfinfo);
					if (allcurrenservicedetails != null) {
						adddata.put("allcurrenservicedetails", allcurrenservicedetails);
					}
					String currentdataservicedetailssoc = odf.getcurrentdataservicedetailssoc(odfinfo);
					if (currentdataservicedetailssoc != null) {
						adddata.put("currentdataservicedetailssoc", currentdataservicedetailssoc);
					}
					String plansoc = odf.getplansoc(odfinfo);
					if (plansoc != null) {
						adddata.put("plansoc", plansoc);
					}

					// partner benefits
					Response responsepartnerbenefits = partnerbenefits.getResponsepartnerbenefits(getjwt);
					String isregistred = partnerbenefits.getisregistered(responsepartnerbenefits);
					if (isregistred != null) {
						adddata.put("isregistred", isregistred);
					}

					// eospa

					Response respa = eospa.getResponsePA(getjwt);
					String aginghistory = eospa.getaginghistory(respa);
					if (aginghistory != null) {
						adddata.put("aginghistory", aginghistory);
					}

					// eos-usage
					Response usagesummary = eosusage.getResponseusagesummary(getjwt);
					String useddata = eosusage.getuseddata(usagesummary);
					if (useddata != null) {
						adddata.put("useddata", useddata);
					}

					String usedmessages = eosusage.getusedmessages(usagesummary);
					if (usedmessages != null) {
						adddata.put("usedmessages", usedmessages);
					}

					String usedcalls = eosusage.getusedcalls(usagesummary);
					if (usedcalls != null) {
						adddata.put("usedcalls", usedcalls);
					}

					String thirdparypurchases = eosusage.getthirdpartypurchases(usagesummary);
					if (thirdparypurchases != null) {
						adddata.put("thirdparypurchases", thirdparypurchases);
					}

					String tmobpurchases = eosusage.gettmobilepurchases(usagesummary);
					if (tmobpurchases != null) {
						adddata.put("tmobpurchases", tmobpurchases);
					}

				}

				else {

					System.out.println(misdin);
					adddata.put("login", "invalid");

				}

				// jtestdata.put(adddata);
				/*
				 * System.out.println(adddata.toString(0)); if
				 * (ecl.getjsondataformisdin(misdin).equalsIgnoreCase("true")) {
				 * ecl.putjsondataformisdin(misdin, adddata.toString(0)); } else {
				 * ecl.postjsondataformisdin(misdin, adddata.toString(0)); }
				 */

				System.out.println(adddata.toString(0));
				if (ecl.getjsondataformisdinShop(misdin).equalsIgnoreCase("true")) {
					ecl.putjsondataformisdinShop(misdin, adddata.toString(0));
				} else {
					ecl.postjsondataformisdinShop(misdin, adddata.toString(0));
				}

			}

			// rootval.put("testdata",jtestdata);
			// System.out.println(adddata.toString(0));

		} catch (Exception eq11) {
			eq11.printStackTrace();

		}
	}

	public Set<Object[]> getallmisdins(String excel) {
		Set<Object[]> misdinpassword = new HashSet<Object[]>();
		// Set<String> misdinpassword = new HashSet<>();
		try {

			Workbook workbook;
			FileInputStream excelFile = new FileInputStream(new File(excel));
			if (excel.contains("xlsx")) {
				workbook = new XSSFWorkbook(excelFile);
			} else {
				workbook = new HSSFWorkbook(excelFile);
			}

			Sheet datatypeSheet = workbook.getSheetAt(0);

			Iterator<Row> iterator = datatypeSheet.iterator();

			while (iterator.hasNext()) {

				Row currentRow = iterator.next();

				if (currentRow.getCell(0) != null && currentRow.getCell(1) != null) {

					String misdin = currentRow.getCell(0).getStringCellValue();
					String pwd = currentRow.getCell(1).getStringCellValue();

					if (!misdin.trim().equals("")) {
						if (!misdin.equalsIgnoreCase("loginEmailOrPhone")) {

							misdinpassword.add(new Object[] { misdin.trim() + "/" + pwd.trim() });

						}
					}
				}
			}

			excelFile.close();
			workbook.close();
		} catch (Exception e) {

		}
		return misdinpassword;
	}

	@SuppressWarnings({ "rawtypes" })
	public String prepareRequestParam(String requestParam, Map<String, String> tokenMap) {
		String replacedRequestParam = requestParam;
		if (requestParam != null) {
			if (tokenMap != null) {
				Iterator it = tokenMap.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();
					String key = pair.getKey().toString();
					String depVal = pair.getValue().toString();
					depVal = StringEscapeUtils.escapeJava(depVal);
					String frameworkKey = key + "_frkey";
					if (depVal.startsWith("{")) {
					} else {
						depVal = "\"" + depVal + "\"";
					}
					// if(requestParam.contains(key+"_frkey")) {
					if (replacedRequestParam.contains(frameworkKey)) {
						replacedRequestParam = replacedRequestParam.replace(frameworkKey, depVal);

					}
				}
			}
		}
		return replacedRequestParam;
	}

}
