package com.tmobile.eservices.qa.payments.api;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.accounts.functional.ManageAddOnsSelectionpagetest;
import com.tmobile.eservices.qa.api.EOSCommonLib;
import com.tmobile.eservices.qa.pages.accounts.api.EOSODF;
import com.tmobile.eservices.qa.pages.accounts.api.EOSPartnerBenefits;
import com.tmobile.eservices.qa.pages.global.api.EOSProfile;
import com.tmobile.eservices.qa.pages.payments.api.EOSAccounts;
import com.tmobile.eservices.qa.pages.payments.api.EOSBillingFilters;
import com.tmobile.eservices.qa.pages.payments.api.EOSPaymentmanager;

import io.restassured.response.Response;
public class Misdinjsoncreator {
	
	private static final Logger logger = LoggerFactory.getLogger(ManageAddOnsSelectionpagetest.class);

	EOSCommonLib ecl=new EOSCommonLib();
	EOSBillingFilters billing=new EOSBillingFilters();
	EOSAccounts accounts= new EOSAccounts();
	EOSProfile profile=new EOSProfile();
	EOSODF odf=new EOSODF();
	EOSPartnerBenefits partnerbenefits=new EOSPartnerBenefits();
	EOSPaymentmanager paymentmanger=new EOSPaymentmanager();
	invalidtestdataidentifier gaps= new invalidtestdataidentifier();
	@Test(dependsOnMethods = { "Testdataevaluation" })

	public void identifygaps() throws IOException{
		gaps.tesdatagapidentifier();
	}
	
	@Test
	public void Testdataevaluation(){
		String FILE_NAME;
		FILE_NAME = System.getProperty("user.dir")+"/src/test/resources/testdata/testdatavalidator.xlsx";
	
		 
		        try {
                    boolean itest=true;
		        	//Reporter.log("<table><tbody><tr><td>Misdin</td><td>Password</td><td>Is Valid</td><td>Ban</td><td>UserType</td><td>MailID</td><td>FirstName</td><td>LastName</td></tr>");
		        	Set <String>misdinpawords=getallmisdins(FILE_NAME);
		        	 Iterator<String> iterator = misdinpawords.iterator();
		        	 JSONObject rootval = new JSONObject();
		        	 JSONArray jtestdata = new JSONArray();
		        	
		        	 while (iterator.hasNext()) {	
		                	String misdpwd=iterator.next();
		              String misdin=misdpwd.split("/")[0];
		             String pwd=misdpwd.split("/")[1];
		            
		          //  String misdin="4256238342";
		          // String pwd="Auto12345";
		                if(misdin.trim().length()>0 && pwd.trim().length()>0) {	
		                
		               // Reporter.log("<tr>");
		                	 JSONObject adddata = new JSONObject();  
		                	adddata.put("misdin", misdpwd);
		                	String[] getjwt=ecl.getauthorizationandregisterinapigee(misdin,pwd);
		                	if(getjwt!=null) {
			                 
		                		
		                		
		                		String accesscode=getjwt[1];
			                	String jwttoken=getjwt[2];
			                	String ban=getjwt[3];
			                	String usertype=getjwt[4];
			                	String mailid=getjwt[5];
			                	String firstname=getjwt[6];
			                	String lastname=getjwt[7];
			                	String acctype=getjwt[8];
			                	String status=getjwt[9];
			                	String lines=getjwt[10];
			                	adddata.put("misdin", misdpwd);
			                	adddata.put("usrtype", usertype);
			                	adddata.put("acctype", acctype);
			                	adddata.put("status", status);
			                	adddata.put("lines", lines);
			                	
			                	
			           //Billing-getDataset     	
			              Response datasetrespons=billing.getResponsedataset(getjwt);	
			              /* String ispa=billing.getpaeligibility(datasetrespons); 	
			               if(ispa!=null) {adddata.put("pa", ispa);}*/
			               String iseip=billing.geteipdetails(datasetrespons);
			               if(iseip!=null) {adddata.put("eip", iseip);}
			               String isauto=billing.getautopaystatus(datasetrespons);
			               if(isauto!=null) {adddata.put("auto", isauto);}
			               String haspastdue=billing.getIspastdueAmount(datasetrespons);
			               if(haspastdue!=null) {adddata.put("haspastdue", haspastdue);}
			               String hasarbalance=billing.getARbalance(datasetrespons);
			               
			            //Billing-getBillList  
			               Response billlistrespons=billing.getResponsebillList(getjwt);	
			               String billtype=billing.getbilltype(billlistrespons); 
			               if(billtype!=null) {adddata.put("bill",billtype );}
			               String previousbills=billing.checkpreviousbills(billlistrespons); 
			               if(previousbills!=null) {adddata.put("priviousbill",previousbills );}
			               
			              Response getpaaymentmethods=accounts.getResponsepaymentMethods(getjwt);
			              String bpeligible=accounts.geteligibilityforbankpayment(getpaaymentmethods);
			              if(bpeligible!=null) {adddata.put("bankpaymenteligible", bpeligible);}

			              String[] getstoredinfo=accounts.getstoreddata(getpaaymentmethods);
			              if(getstoredinfo[0]!=null)adddata.put("storedbank", getstoredinfo[0]);
			              if(getstoredinfo[1]!=null)adddata.put("storecreditcard", getstoredinfo[1]);
			              if(getstoredinfo[2]!=null)adddata.put("storedebitcard", getstoredinfo[1]);
			              
			              
			              
			              
			              Response getprofile=profile.getProfileresponse(getjwt);
			              String billstate=profile.getbillingState(getprofile);
			              if(billstate!=null) {adddata.put("billstate", billstate);}

			              
			              Response getpainfo=paymentmanger.getPAinformationresponse(getjwt);
			              String pasetup=paymentmanger.isPAAlreadysetup(getpainfo);
			              if(pasetup!=null) {adddata.put("paalreadysetup", pasetup);}
			              String paval=paymentmanger.ispaeligible(getpainfo);
			              if(paval!=null) {
			            	  if(paval.equalsIgnoreCase("true")&&hasarbalance.equalsIgnoreCase("true"))
			            		  adddata.put("pa", "true");
			            	  else
			            		  adddata.put("pa", "false");
			            	  }
			             
			              Response subscriberinfo=paymentmanger.getsubscriberinforesponse(getjwt);
			              String autopayeligible=paymentmanger.isautopayeligible(subscriberinfo);
			              if(autopayeligible!=null) {adddata.put("autopayeligible", autopayeligible);}
			              String fdpeligible=paymentmanger.isfdpeligible(subscriberinfo);
			              if(fdpeligible!=null) {adddata.put("fdpeligible", fdpeligible);}
			              
			                   
			              
			              //ODF
			              Response odfinfo=odf.getResponseODF(getjwt);
			              List<String> allcurrenservicedetails=odf.getallCurrentServiceDetails(odfinfo);
			              if(allcurrenservicedetails!=null) {adddata.put("allcurrenservicedetails", allcurrenservicedetails);}
			              String currentdataservicedetailssoc=odf.getcurrentdataservicedetailssoc(odfinfo);
			              if(currentdataservicedetailssoc!=null) {adddata.put("currentdataservicedetailssoc", currentdataservicedetailssoc);}
			              String plansoc=odf.getplansoc(odfinfo);
			              if(plansoc!=null) {adddata.put("plansoc", plansoc);}
			              
			              //partner benefits
			              Response responsepartnerbenefits=partnerbenefits.getResponsepartnerbenefits(getjwt);
			              String isregistred=partnerbenefits.getisregistered(responsepartnerbenefits);
			              if(isregistred!=null) {adddata.put("isregistred", isregistred);}
			              
			 
		              	}

		                	else {
		              		
		                	System.out.println(misdin);
		                	adddata.put("login", "invalid");
		              		
				                	
		                }
		              
		                	   jtestdata.put(adddata);	
		                	
		                	
		                	
		                	
		                }
		           
		            //    Reporter.log("</tr>");
		            }
		        	 rootval.put("testdata",jtestdata);
		        	 System.out.println(rootval.toString(0));
		        	 FileWriter file = new FileWriter( System.getProperty("user.dir")+"/src/test/resources/testdata/db.json");
		        	
		     		
		     			file.write(rootval.toString(1));
		     			file.close();
		        	 
		        	// Reporter.log("</tbody></table>");
		            if(!itest) {
			        	  Assert.fail();
			          }
		        
		        } catch (Exception eq11) {
		            eq11.printStackTrace();
		    
		     
		        }
		    }
	
		 
		 
	
	
	public Set<String> getallmisdins(String excel){
		  Set<String> misdinpassword = new HashSet<>();
		 try {
	
			 Workbook workbook;	 
			 FileInputStream excelFile = new FileInputStream(new File(excel));
	         if(excel.contains("xlsx")) {
	        	workbook = new XSSFWorkbook (excelFile); 
	         }else {
	        	 workbook = new HSSFWorkbook (excelFile); 
	         }
			
          Sheet datatypeSheet = workbook.getSheetAt(0);
         
          Iterator<Row> iterator = datatypeSheet.iterator();
       
          while (iterator.hasNext()) {
        
              Row currentRow = iterator.next();
             
            	 
              if(currentRow.getCell(0)!=null&&currentRow.getCell(1)!=null) {
          
              String misdin= currentRow.getCell(0).getStringCellValue();
              String pwd=currentRow.getCell(1).getStringCellValue();
             
              if(!misdin.trim().equals("")) {
              if(!misdin.equalsIgnoreCase("loginEmailOrPhone")) {
              
              	misdinpassword.add(misdin.trim()+"/"+pwd.trim());
        
              }}
              }
          }
        
          excelFile.close();
          workbook.close();
		 
		 }catch(Exception e) {
        	  
          }
		  return misdinpassword;
	}
	
}	
	
	