package com.tmobile.eservices.qa.accounts.api.cpslookup;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class GetCurrentPassesTest extends AccountsApi {

	public Map<String, String> tokenMap;

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetCurrentPasses(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Current Passes");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("GetCurrentPasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getCurrentPasses(apiTestData, updatedRequest);

		String operationName = "Get Current Passes";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			if (response.jsonPath().getString("statusDetails.statusMessage")
					.contains("getCurrentPasses: no passes returned from middleware system")) {

				Assert.assertTrue(response.body().asString() != null && response.getStatusCode() == 200);
				Reporter.log(" 200 ok Success");

				Reporter.log("No Active passes from MW is Returned");

			} else {

				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.planServiceStatus")
						.contains("ACTIVE"));
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.serviceSubtype")
						.contains("DATA PASS"));
				Assert.assertTrue(
						response.jsonPath().getString("listLineDetails.planServices.dataPassDescription").length() > 4);
				Assert.assertTrue(
						response.jsonPath().getString("listLineDetails.planServices.dataPassGroup").length() > 4);
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.name").length() > 4);
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.dataPassEffDateTimestamp")
						.length() > 4);
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.dataPassExpDateTimestamp")
						.length() > 4);

				Reporter.log(" Active passes from MW is Returned");
			}

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetCurrentPasses1(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Current Passes");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("GetCurrentPasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getCurrentPasses(apiTestData, updatedRequest);

		String operationName = "Get Current Passes";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			if (response.jsonPath().getString("statusDetails.statusMessage")
					.contains("getCurrentPasses: no passes returned from middleware system")) {

				Assert.assertTrue(response.body().asString() != null && response.getStatusCode() == 200);
				Reporter.log(" 200 ok Success");

				Reporter.log("No Active passes from MW is Returned");

			} else {
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.planServiceStatus")
						.contains("ACTIVE"));
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.serviceSubtype")
						.contains("DATA PASS"));
				Assert.assertTrue(
						response.jsonPath().getString("listLineDetails.planServices.dataPassDescription").length() > 4);
				Assert.assertTrue(
						response.jsonPath().getString("listLineDetails.planServices.dataPassGroup").length() > 4);
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.name").length() > 4);
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.dataPassEffDateTimestamp")
						.length() > 4);
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.dataPassExpDateTimestamp")
						.length() > 4);
				Reporter.log(" Active passes from MW is Returned");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetCurrentPasses2(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Current Passes");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("GetCurrentPasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getCurrentPasses(apiTestData, updatedRequest);

		String operationName = "Get Current Passes";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			if (response.jsonPath().getString("statusDetails.statusMessage")
					.contains("getCurrentPasses: no passes returned from middleware system")) {

				Assert.assertTrue(response.body().asString() != null && response.getStatusCode() == 200);
				Reporter.log(" 200 ok Success");

				Reporter.log("No Active passes from MW is Returned");

			} else {

				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.planServiceStatus")
						.contains("ACTIVE"));
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.serviceSubtype")
						.contains("DATA PASS"));
				Assert.assertTrue(
						response.jsonPath().getString("listLineDetails.planServices.dataPassDescription").length() > 4);
				Assert.assertTrue(
						response.jsonPath().getString("listLineDetails.planServices.dataPassGroup").length() > 4);
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.name").length() > 4);
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.dataPassEffDateTimestamp")
						.length() > 4);
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.dataPassExpDateTimestamp")
						.length() > 4);

				Reporter.log(" Active passes from MW is Returned");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetCurrentPasses3(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Current Passes");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");
		;

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("GetCurrentPasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getCurrentPasses(apiTestData, updatedRequest);

		String operationName = "Get Current Passes";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			if (response.jsonPath().getString("statusDetails.statusMessage")
					.contains("getCurrentPasses: no passes returned from middleware system")) {

				Assert.assertTrue(response.body().asString() != null && response.getStatusCode() == 200);
				Reporter.log(" 200 ok Success");

				Reporter.log("No Active passes from MW is Returned");

			} else {

				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.planServiceStatus")
						.contains("ACTIVE"));
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.serviceSubtype")
						.contains("DATA PASS"));
				Assert.assertTrue(
						response.jsonPath().getString("listLineDetails.planServices.dataPassDescription").length() > 4);
				Assert.assertTrue(
						response.jsonPath().getString("listLineDetails.planServices.dataPassGroup").length() > 4);
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.name").length() > 4);
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.dataPassEffDateTimestamp")
						.length() > 4);
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.dataPassExpDateTimestamp")
						.length() > 4);

				Reporter.log(" Active passes from MW is Returned");

			}
		} else {
			failAndLogResponse(response, operationName);

		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetCurrentPasses4(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Current Passes");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("GetCurrentPasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getCurrentPasses(apiTestData, updatedRequest);

		String operationName = "Get Current Passes";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			if (response.jsonPath().getString("statusDetails.statusMessage")
					.contains("getCurrentPasses: no passes returned from middleware system")) {

				Assert.assertTrue(response.body().asString() != null && response.getStatusCode() == 200);
				Reporter.log(" 200 ok Success");

				Reporter.log("No Active passes from MW is Returned");

			} else {
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.planServiceStatus")
						.contains("ACTIVE"));
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.serviceSubtype")
						.contains("DATA PASS"));

				JSONObject obj = new JSONObject(response.getBody().asString());
				JSONArray arrlstLineDetail = obj.getJSONArray("listLineDetails");

				for (int i = 0; i < arrlstLineDetail.length(); i++) {

					JSONObject lstLineDetails = (JSONObject) arrlstLineDetail.opt(i);

					JSONArray arrPlanServices = lstLineDetails.getJSONArray("planServices");

					for (int j = 0; j < arrPlanServices.length(); j++) {

						JSONObject arrPlanService = arrPlanServices.getJSONObject(j);

						Assert.assertTrue(StringUtils.isNoneEmpty(arrPlanService.getString("dataPassDescription")));
						Assert.assertTrue(StringUtils.isNoneEmpty(arrPlanService.getString("dataPassGroup")));
						Assert.assertTrue(StringUtils.isNoneEmpty(arrPlanService.getString("name")));
						Assert.assertTrue(
								StringUtils.isNoneEmpty(arrPlanService.getString("dataPassEffDateTimestamp")));
						Assert.assertTrue(
								StringUtils.isNoneEmpty(arrPlanService.getString("dataPassExpDateTimestamp")));

					}
				}

				Reporter.log(" Active passes from MW is Returned");

			}
		} else {

			failAndLogResponse(response, operationName);

		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetCurrentPasses5(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Current Passes");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("GetCurrentPasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getCurrentPasses(apiTestData, updatedRequest);

		String operationName = "Get Current Passes";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			if (response.jsonPath().getString("statusDetails.statusMessage")
					.contains("getCurrentPasses: no passes returned from middleware system")) {

				Assert.assertTrue(response.body().asString() != null && response.getStatusCode() == 200);
				Reporter.log(" 200 ok Success");

				Reporter.log("No Active passes from MW is Returned");

			} else {

				Assert.assertTrue((response.jsonPath().getString("listLineDetails.planServices.planServiceStatus")
						.contains("ACTIVE"))
						|| (response.jsonPath().getString("listLineDetails.planServices.planServiceStatus")
								.contains("FUTURE_DATED")));
				Assert.assertTrue(response.jsonPath().getString("listLineDetails.planServices.serviceSubtype")
						.contains("DATA PASS"));

				JSONObject obj = new JSONObject(response.getBody().asString());
				JSONArray arrlstLineDetail = obj.getJSONArray("listLineDetails");

				for (int i = 0; i < arrlstLineDetail.length(); i++) {

					JSONObject lstLineDetails = (JSONObject) arrlstLineDetail.opt(i);

					JSONArray arrPlanServices = lstLineDetails.getJSONArray("planServices");

					for (int j = 0; j < arrPlanServices.length(); j++) {

						JSONObject arrPlanService = arrPlanServices.getJSONObject(j);

						Assert.assertTrue(StringUtils.isNoneEmpty(arrPlanService.getString("dataPassDescription")));
						Assert.assertTrue(StringUtils.isNoneEmpty(arrPlanService.getString("dataPassGroup")));
						Assert.assertTrue(StringUtils.isNoneEmpty(arrPlanService.getString("name")));
						Assert.assertTrue(
								StringUtils.isNoneEmpty(arrPlanService.getString("dataPassEffDateTimestamp")));
						Assert.assertTrue(
								StringUtils.isNoneEmpty(arrPlanService.getString("dataPassExpDateTimestamp")));

					}
				}

			}

			Reporter.log(" Active passes from MW is Returned");
		} else {

			failAndLogResponse(response, operationName);

		}
	}
}
