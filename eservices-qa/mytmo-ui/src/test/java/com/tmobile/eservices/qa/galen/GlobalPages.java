package com.tmobile.eservices.qa.galen;

import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;

public class GlobalPages extends GlobalCommonLib{
	GalenLib galenLib = new GalenLib();
	
	/**
	 * Verify Login Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void galenTestLoginPage(ControlTestData data, MyTmoData myTmoData) {
		getTMOURL(myTmoData);
		galenLib.doVisualTest(getDriver(), "LoginPage","src/test/resources/specs/loginPage.gspec","desktop");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global2" })
	public void galenTestHomePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		galenLib.doVisualTest(getDriver(), "HomePage","src/test/resources/specs/homePage.gspec","desktop");
	}

}
