package com.tmobile.eservices.qa.shop;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.commonlib.CommonLibrary;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAlonePDPPage;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAlonePLPPage;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAloneReviewCartPage;
import com.tmobile.eservices.qa.pages.shop.AccessoryPDPPage;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.ChangeSimPage;
import com.tmobile.eservices.qa.pages.shop.CheckOrderPage;
import com.tmobile.eservices.qa.pages.shop.ConsolidatedRatePlanPage;
import com.tmobile.eservices.qa.pages.shop.DeviceIntentPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewIntroductionPage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewQuestionPage;
import com.tmobile.eservices.qa.pages.shop.InterstitialTradeInPage;
import com.tmobile.eservices.qa.pages.shop.JumpCartPage;
import com.tmobile.eservices.qa.pages.shop.JumpConditionPage;
import com.tmobile.eservices.qa.pages.shop.JumpIMEIPage;
import com.tmobile.eservices.qa.pages.shop.JumpTradeInConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.JumpTradeInPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.OfferDetailsPage;
import com.tmobile.eservices.qa.pages.shop.OrderConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.PLPPage;
import com.tmobile.eservices.qa.pages.shop.PhonePages;
import com.tmobile.eservices.qa.pages.shop.PhoneSelectionPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.shop.TMobileProd_HomePage;
import com.tmobile.eservices.qa.pages.shop.TradeInAnotherDevicePage;
import com.tmobile.eservices.qa.pages.shop.TradeInConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionValuePage;
import com.tmobile.eservices.qa.pages.shop.UNOPDPPage;
import com.tmobile.eservices.qa.pages.shop.UNOPLPPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;

public class ShopCommonLib extends CommonLibrary {

	public static final String DMO_URL_LINESELECTION = "https://dmo.digital.t-mobile.com/cell-phones";
	public static final String TMNG_URL_LINESELECTION = "https://www.t-mobile.com/cell-phones";
	public static final String QAT_URL_LINESELECTION = "https://qat.digital.t-mobile.com/cell-phones";
	public static final String E5_LINESELECTOR_URL_DEEPLINK = "https://e5.my.t-mobile.com/purchase/shop/i-8A0A8516050D4AEC9E04C319931E0E45/true/610214654465";
	public static final String QAT_LINESELECTOR_URL_DEEPLINK = "https://qata0c.eservice.t-mobile.com/purchase/shop/g-BF514EB8E4E847619F07E2BBAF6B060B/false/190198453464";
	public static final String QAT_LINESELECTOR_URL = "https://qata0c.eservice.t-mobile.com/purchase/lineSelection";
	public static final String QAT_CHECKORDERSTATUS_URL = "https://qata0c.eservice.t-mobile.com/myphone/checkorder.html";
	private final String phonesPageUrl = "cell-phones";
	public static final String TMO_URL = "app.tmo.url";
	public static final String STAGING_URL_LINESELECTION = "https://staging.t-mobile.com/cell-phones";

	/**
	 * Navigate To New Shop Page
	 *
	 * @param myTmoData
	 * @return
	 */
	public ShopPage navigateToShopPage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickShoplink();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyPageLoaded();
		return shopPage;
	}

	/**
	 * Navigate To Phone Page
	 *
	 * @param myTmoData
	 * @return
	 */
	public PhonePages navigateToPhonePage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickPhoneLink();
		PhonePages phonePage = new PhonePages(getDriver());
		phonePage.verifyPhonesPage();
		return phonePage;
	}

	/**
	 * Navigate TO PLP Page By See All Phones
	 *
	 * @param myTmoData
	 * @return
	 */
	public PLPPage navigateToPLPPageBySeeAllPhones(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickSeeAllPhones();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		return plpPage;
	}

	/**
	 * Navigate TO PDP Page By See All Phones
	 *
	 * @param myTmoData
	 * @return
	 */
	public PDPPage navigateToPDPPageBySeeAllPhones(MyTmoData myTmoData) {
		PLPPage plpPage = navigateToPLPPageBySeeAllPhones(myTmoData);
		plpPage.clickDeviceByName(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		return pdpPage;
	}

	/**
	 * Navigate To Line Selector Page By See All Phones
	 *
	 * @param myTmoData
	 * @return
	 */
	public LineSelectorPage navigateToLineSelectorPageBySeeAllPhones(MyTmoData myTmoData) {
		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		return lineSelectorPage;
	}

	/**
	 * Navigate To Line Selector Details Page By See All Phones
	 * 
	 * @param myTmoData
	 * @return
	 */
	public LineSelectorDetailsPage navigateToLineSelectorDetailsPageBySeeAllPhones(MyTmoData myTmoData) {
		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		lineSelectorPage.clickDevice();
		// lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		return lineSelectorDetailsPage;
	}

	/**
	 * Navigate To Device Protection Page By See All Phones Through Skip TradeIn
	 *
	 * @param myTmoData
	 * @return
	 */
	public DeviceProtectionPage navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(MyTmoData myTmoData) {
		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionURL();
		return deviceProtectionPage;
	}

	/**
	 * Navigate To Accessory PLP Page By See All Phones With Skip Trade In
	 *
	 * @param myTmoData
	 * @return
	 */
	public AccessoryPLPPage navigateToAccessoryPLPPageBySeeAllPhonesWithSkipTradeIn(MyTmoData myTmoData) {
		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(
				myTmoData);
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		return accessoryPLPPage;
	}

	/**
	 * Navigate To Accessory PDP with upgrade flow
	 *
	 * @param myTmoData
	 * @return
	 */
	public AccessoryPDPPage navigateToAccessoryPDPPageBySeeAllPhonesWithSkipTradeIn(MyTmoData myTmoData) {
		navigateToAccessoryPLPPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickFirstAccesory();
		AccessoryPDPPage accessoryPDPPage = new AccessoryPDPPage(getDriver());
		accessoryPDPPage.verifyPageUrl();
		return accessoryPDPPage;
	}

	/**
	 * Navigate To Cart Page By See All Phones With Skip Trade In Flow
	 *
	 * @param myTmoData
	 * @return CartPage
	 */
	public CartPage navigateToCartPageBySeeAllPhonesWithSkipTradeIn(MyTmoData myTmoData) {
		AccessoryPLPPage accessoryPLPPage = navigateToAccessoryPLPPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		return cartPage;
	}

	/**
	 * Navigate To Shipping Tab By See All Phones With Skip Trade In
	 *
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToShippingTabBySeeAllPhonesWithSkipTradeIn(MyTmoData myTmoData) {
		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingDetails();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickShipToDifferentAddress();
		cartPage.verifyShippingInformation();
		return cartPage;
	}

	/**
	 * Navigate To PaymentTab tab By See All Phones With Skip Trade In Flow
	 *
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToPaymentTabBySeeAllPhonesWithSkipTradeIn(MyTmoData myTmoData) {
		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingDetails();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		return cartPage;
	}
	/**
	 * Navigate To Want to trade different phone link By See All Phones
	 *
	 * @param myTmoData
	 * @return
	 */
	public PhoneSelectionPage navigateToWantToTradeDifferentPhoneLinkBySeeAllPhones(MyTmoData myTmoData) {
		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		// lineSelectorDetailsPage.clickOnCheckDeviceValueButton();
		lineSelectorDetailsPage.clickWantToTradeDifferentPhoneLink();

		PhoneSelectionPage phoneSelectionPage = new PhoneSelectionPage(getDriver());
		phoneSelectionPage.verifyPhoneSelectionPage();
		return phoneSelectionPage;
	}

	/**
	 * Navigate To Trade In Another Page By See All Phones
	 *
	 * @param myTmoData
	 * @return
	 */
	public TradeInAnotherDevicePage navigateToTradeInAnotherDevicePageBySeeAllPhones(MyTmoData myTmoData) {
		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		// lineSelectorDetailsPage.clickOnCheckDeviceValueButton();
		lineSelectorDetailsPage.clickWantToTradeDifferentPhoneLink();

		PhoneSelectionPage phoneSelectionPage = new PhoneSelectionPage(getDriver());
		phoneSelectionPage.verifyPhoneSelectionPage();
		phoneSelectionPage.clickGotADifferentPhone();

		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		return tradeInAnotherDevicePage;
	}

	/**
	 * Navigate To Trade In Another Device Condition Value Page By See All Phones
	 *
	 * @param myTmoData
	 * @return
	 */
	public TradeInDeviceConditionValuePage navigateToTradeInDeviceConditionValuePageBySeeAllPhones(
			MyTmoData myTmoData) {
		TradeInAnotherDevicePage tradeInAnotherDevicePage = navigateToTradeInAnotherDevicePageBySeeAllPhones(myTmoData);
		/*
		 * tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice( myTmoData);
		 */
		tradeInAnotherDevicePage.selectCarrier(myTmoData.getCarrier());
		tradeInAnotherDevicePage.selectMake(myTmoData.getMake());
		tradeInAnotherDevicePage.selectModel(myTmoData.getModel());
		tradeInAnotherDevicePage.setIMEINumber(myTmoData.getiMEINumber());
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		return deviceConditionsPage;
	}

	/**
	 * Navigate To Trade In Device Condition Confirmation Page By See All Phones
	 *
	 * @param myTmoData
	 * @return
	 */
	public TradeInDeviceConditionConfirmationPage navigateToTradeInDeviceConditionConfirmationPageBySeeAllPhones(
			MyTmoData myTmoData) {
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = navigateToTradeInDeviceConditionValuePageBySeeAllPhones(
				myTmoData);
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInConfirmationPage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();
		return tradeInConfirmationPage;
	}

	/**
	 * Navigate to Device Protection Page By See All Phones With TradeIn Flow
	 *
	 * @param myTmoData
	 * @return
	 */

	public DeviceProtectionPage navigateToDeviceProtectionPageBySeeAllPhonesWithTradeInFlow(MyTmoData myTmoData) {
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = navigateToTradeInDeviceConditionConfirmationPageBySeeAllPhones(
				myTmoData);
		// tradeInDeviceConditionConfirmationPage.clickContinueTradeInButton();
		tradeInDeviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		return deviceProtectionPage;
	}

	/**
	 * Navigate To Accessory PLP
	 *
	 * @param myTmoData
	 * @return
	 */
	public AccessoryPLPPage navigateToAccessoryPLPPageBySeeAllPhonesWithTradeInFlow(MyTmoData myTmoData) {
		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageBySeeAllPhonesWithTradeInFlow(
				myTmoData);
		deviceProtectionPage.checkPageIsReady();
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		return accessoryPLPPage;
	}

	/**
	 * Navigate To Cart Page By See All Phones With TradeIn Flow
	 *
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToCartPageBySeeAllPhonesWithTradeInFlow(MyTmoData myTmoData) {
		TradeInDeviceConditionConfirmationPage tradeInConfirmationPage = navigateToTradeInDeviceConditionConfirmationPageBySeeAllPhones(
				myTmoData);
		tradeInConfirmationPage.clickContinueTradeInButton();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickCloseModalDialogWindow();
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		return cartPage;
	}

	/**
	 * Navigate To PDP Page By Feature Devices
	 *
	 * @param myTmoData
	 * @return
	 */
	public PDPPage navigateToPDPPageByFeatureDevices(MyTmoData myTmoData, String featureDeviceName) {
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickOnFeaturedDevices(featureDeviceName);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		return pdpPage;
	}

	/**
	 * Navigate To Line Selector Page By Feature Devices
	 *
	 * @param myTmoData
	 * @return
	 */
	public LineSelectorPage navigateToLineSelectorPageByFeatureDevices(MyTmoData myTmoData) {
		PDPPage pdpPage = navigateToPDPPageByFeatureDevices(myTmoData, myTmoData.getDeviceName());
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		return lineSelectorPage;
	}

	/**
	 * Navigate To Line Selection Details Page By Featured Devices Flow
	 *
	 * @param myTmoData
	 * @param featureDeviceName
	 * @return
	 */
	public LineSelectorDetailsPage navigateToLineSelectorDetailsPageByFeatureDevices(MyTmoData myTmoData,
			String featureDeviceName) {
		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageByFeatureDevices(myTmoData);
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		return lineSelectorDetailsPage;
	}

	/**
	 * Navigate To Device Protection Page By Featured Devices With Skip TradeIn
	 * 
	 * @param myTmoData
	 * @return
	 */
	public DeviceProtectionPage navigateToDeviceProtectionPageByFeatureDevicesWithSkipTradeIn(MyTmoData myTmoData) {
		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageByFeatureDevices(myTmoData,
				myTmoData.getDeviceName());
		lineSelectorDetailsPage.clickSkipTradeInCTA();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		return deviceProtectionPage;

	}

	/**
	 * Navigate To Accessory PLP Page By Featured Devices With Skip TradeIn
	 * 
	 * @param myTmoData
	 * @return
	 */
	public AccessoryPLPPage navigateToAccessoryPLPPageByFeaturedDevicesWithSkipTradeIn(MyTmoData myTmoData) {
		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageByFeatureDevicesWithSkipTradeIn(
				myTmoData);
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupCloseButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		return accessoryPLPPage;
	}

	/**
	 * Navigate To Cart Page By Feature Devices With Skip Trade In
	 *
	 * @param myTmoData
	 * @param featureDeviceName
	 * @return
	 */
	public CartPage navigateToCartPageByFeatureDevicesWithSKipTradeIn(MyTmoData myTmoData, String featureDeviceName) {
		AccessoryPLPPage accessoryPLPPage = navigateToAccessoryPLPPageByFeaturedDevicesWithSkipTradeIn(myTmoData);
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		return cartPage;
	}

	/**
	 * Navigate To Trade In Another Device Page By Featured Devices Flow
	 *
	 * @param myTmoData
	 * @param featureDeviceName
	 * @return
	 */
	public TradeInAnotherDevicePage navigateToTradeInAnotherDevicePageByFeaturedDevicesFLow(MyTmoData myTmoData,
			String featureDeviceName) {
		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageByFeatureDevices(myTmoData,
				featureDeviceName);
		lineSelectorDetailsPage.clickOnWantToTradeInDifferentPhone();

		PhoneSelectionPage phoneSelectionPage = new PhoneSelectionPage(getDriver());
		phoneSelectionPage.verifyPhoneSelectionPage();
		phoneSelectionPage.clickGotADifferentPhone();

		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		return tradeInAnotherDevicePage;
	}

	/**
	 * Navigate To Device Condition Value Page By Featured Devices Flow With TradeIn
	 * Flow
	 *
	 * @param myTmoData
	 * @param featureDeviceName
	 * @return
	 */
	public TradeInDeviceConditionValuePage navigateToDeviceConditionValuePageByFeaturedDevicesWithTradeInFlow(
			MyTmoData myTmoData, String featureDeviceName) {
		TradeInAnotherDevicePage tradeInAnotherDevicePage = navigateToTradeInAnotherDevicePageByFeaturedDevicesFLow(
				myTmoData, featureDeviceName);
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectCarrier(myTmoData.getCarrier());
		tradeInAnotherDevicePage.selectMake(myTmoData.getMake());
		tradeInAnotherDevicePage.selectModel(myTmoData.getModel());
		tradeInAnotherDevicePage.setIMEINumber(myTmoData.getiMEINumber());
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		return deviceConditionsPage;
	}

	/**
	 * Navigate To Trade In Device Condition Confirmation Page By Featured Devices
	 * With TradeIn Flow
	 *
	 * @param myTmoData
	 * @param featureDeviceName
	 * @return
	 */
	public TradeInDeviceConditionConfirmationPage navigateToTradeInDeviceConditionConfirmationPageByFeatureDevicesWithTradeInFlow(
			MyTmoData myTmoData, String featureDeviceName) {
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = navigateToDeviceConditionValuePageByFeaturedDevicesWithTradeInFlow(
				myTmoData, featureDeviceName);
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInConfirmationPage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();
		return tradeInConfirmationPage;
	}

	/**
	 * Navigate To Cart page By Featured Devices With TradeIn Flow
	 *
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToCartPageByFeaturedDevicesWithTradeInFlow(MyTmoData myTmoData) {
		TradeInDeviceConditionConfirmationPage deviceConditionConfirmationPage = navigateToTradeInDeviceConditionConfirmationPageByFeatureDevicesWithTradeInFlow(
				myTmoData, myTmoData.getDeviceName());
		deviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesLink();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		return cartPage;
	}

	// TODO - Refactoring is in Progress

	/**
	 * Navigate TO Offer Details Page
	 *
	 * @param myTmoData
	 * @return
	 */
	public OfferDetailsPage navigateToOfferDetailsPage(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickOnHeroBannerSellButton();
		OfferDetailsPage offerDetailsPage = new OfferDetailsPage(getDriver());
		offerDetailsPage.verifyOfferDetailsPage();
		return offerDetailsPage;
	}

	/**
	 * Navigate to TradeInValuePage With Got Issues radio button
	 *
	 * @param myTmoData
	 * @return
	 */
	public TradeInDeviceConditionConfirmationPage navigateToTradeInValuePageWithclickItsGotIssuesBtn(
			MyTmoData myTmoData) {
		TradeInDeviceConditionValuePage deviceConditionsPage = navigateToTradeInDeviceConditionValuePageBySeeAllPhones(
				myTmoData);
		deviceConditionsPage.clickItsGotIssuesRadioBtn();
		TradeInDeviceConditionConfirmationPage tradeInValuePage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInValuePage.verifyTradeInDeviceConditionConfirmationPage();
		return tradeInValuePage;
	}

	/**
	 * Navigate To JumpCart Page By Featured Devices Flow
	 *
	 * @param myTmoData
	 * @param featureDeviceName
	 * @return
	 */
	public JumpCartPage navigateToCartPageByFeatureDevicesWithJumpTradeInFlow(MyTmoData myTmoData,
			String featureDeviceName) {
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickOnFeaturedDevices(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyJumpProtection();
		deviceProtectionPage.clickJumpProtection();
		JumpConditionPage newJumpConditionPage = new JumpConditionPage(getDriver());
		newJumpConditionPage.verifyNewJumpConditionPage();
		newJumpConditionPage.DeviceConditionsPageItsGoodComponent();
		newJumpConditionPage.selectGoodComponents();
		newJumpConditionPage.verifyAcceptAndContinueButton();
		newJumpConditionPage.clickAcceptAndContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		deviceConditionsPage.clickAcceptAndContinueButton();
		JumpCartPage jumpCartPage = new JumpCartPage(getDriver());
		return jumpCartPage;
	}

	/**
	 * Navigate To Change SIM Page
	 *
	 * @param myTmoData
	 * @return
	 */
	public ShopCommonLib navigateToChangeSIMPage(MyTmoData myTmoData) {
		HomePage homePage = new HomePage(getDriver());
		launchAndPerformLogin(myTmoData);

		homePage.clickPhoneLink();
		PhonePages phonePage = new PhonePages(getDriver());
		phonePage.verifyPhonesPages();

		phonePage.clickChangesim();
		ChangeSimPage changeSimPage = new ChangeSimPage(getDriver());
		changeSimPage.verifyChangeSimPage();
		return this;
	}

	/**
	 * Navigate To Order Status Page
	 *
	 * @param myTmoData
	 */
	public ShopCommonLib launchAndNavigateToOrderStatusPage(MyTmoData myTmoData) {
		navigateToPhonePage(myTmoData);
		PhonePages phonePage = new PhonePages(getDriver());
		phonePage.refreshPage();
		phonePage.clickCheckOrderStatusLink();
		return this;
	}

	/**
	 * Add voice or MBB line flow
	 *
	 * @param data
	 * @param myTmoData
	 */
	public DeviceIntentPage addVoiceOrMbbLineFlow(ControlTestData data, MyTmoData myTmoData) {

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickShoplink();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyPageLoaded();
		shopPage.verifyPageUrl();
		shopPage.clickQuickLinks("Add a line");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		return deviceIntentPage;
	}

	/**
	 * navigateToCartPageForJumpUser from IMEI Page
	 *
	 * @return
	 */

	public CartPage navigateToCartPageForJumpUserFromIMEIPage() {

		JumpIMEIPage jumpIMEIPage = new JumpIMEIPage(getDriver());
		Assert.assertTrue(jumpIMEIPage.verifyJumpIMEIPage(), Constants.JUMP_IMEI_PAGE_ERROR);
		Reporter.log("Jump IMEI Page is displayed");
		jumpIMEIPage.clickPowerOnCheckBox();
		jumpIMEIPage.clickLCDDisplayCheckBox();
		jumpIMEIPage.clickContinueButton();

		TradeInConfirmationPage tradeInConfirmationPage = new TradeInConfirmationPage(getDriver());
		tradeInConfirmationPage.verifyTradeInConfirmationPage();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL() == true) {
			deviceProtectionPage.clickContinueButton();
		}
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		return cartPage;
	}

	/**
	 * Navigate To MyTmo Login Page
	 *
	 * @return ShopCommonLib
	 */
	public ShopCommonLib navigateToMyTmoLoginPage() {
		getDriver().manage().deleteAllCookies();
		getDriver().get("http://t-mobile.com");
		TMobileProd_HomePage tMobileProdHomePage = new TMobileProd_HomePage(getDriver());
		tMobileProdHomePage.verifyTMobile_HomePage();
		tMobileProdHomePage.clickPhoneTab();
		Reporter.log("Phone tab is Clicked");
		tMobileProdHomePage.clickDevice();
		tMobileProdHomePage.checkPageIsReady();
		tMobileProdHomePage.clickMyTMobileLink();
		return this;
	}

	/**
	 * Navigate to Accessories PLP Page
	 *
	 * @param myTmoData
	 * @return AccessoriesStandAlonePLPPage
	 */
	public AccessoriesStandAlonePLPPage navigateToStandAloneAccessoriesPLP(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyPageLoaded();
		shopPage.verifyPageUrl();
		shopPage.clickQuickLinks("Accessories");
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		return accessoriesStandAlonePLPPage;
	}

	/**
	 * Navigate to Accessories PDP Page
	 *
	 * @param myTmoData
	 * @return
	 */
	public AccessoriesStandAlonePDPPage navigateToStandAloneAccessoriesPDP(MyTmoData myTmoData) {
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = navigateToStandAloneAccessoriesPLP(myTmoData);
		accessoriesStandAlonePLPPage.clickAccesoriesDeviceByName(myTmoData.getAccessoryName());
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		return accessoriesStandAlonePDPPage;
	}

	/**
	 * Navigate to StandAlone Review Cart Page
	 *
	 * @param myTmoData
	 * @return
	 */
	public AccessoriesStandAloneReviewCartPage navigateToStandAloneReviewCartPage(MyTmoData myTmoData) {
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = navigateToStandAloneAccessoriesPDP(myTmoData);
		accessoriesStandAlonePDPPage.clickAddToCartButton();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		return accessoriesStandAloneReviewCartPage;
	}

	/**
	 * Navigate to TradeInValuePage with Promo Banner * @param myTmoData
	 *
	 * @return
	 */

	public TradeInDeviceConditionConfirmationPage navigateToTradeInValuePageThroughPromoBanner(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickOnHeroBannerBuyButton();
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		lineSelectorDetailsPage.clickOnWantToTradeInDifferentPhone();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectCarrier(ShopConstants.CARRIER_DROPDOWN_OPTION_TMOBILE);
		tradeInAnotherDevicePage.selectMake(ShopConstants.SAMSUNG_MAKE_DROPDOWN_OPTION);
		tradeInAnotherDevicePage.selectModel(ShopConstants.SAMSUNG_MODEL_DROPDOWN_OPTION);
		tradeInAnotherDevicePage.setIMEINumber(ShopConstants.EIP_IMEI);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		deviceConditionsPage.clickItsGoodRadioBtn();
		deviceConditionsPage.clickOnItsGoodCondition();
		TradeInDeviceConditionConfirmationPage tradeInValuePage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInValuePage.verifyTradeInDeviceConditionConfirmationPage();
		return tradeInValuePage;
	}

	/**
	 * navigateToLineSelectorPage from Home Page
	 */

	public void navigateToLineseletorPage() {

		getDriver().get(QAT_LINESELECTOR_URL);
	}

	/**
	 * navigateToLineSelectorPage from Home Page
	 */

	public void navigateToLineseletorPageWithDeepLink() {

		getDriver().get(QAT_LINESELECTOR_URL_DEEPLINK);
	}

	/**
	 * navigate To Trade in device PDP from Home Page
	 */
	public void navigateToTradeinDevicePDPPageWithDeepLink() {
		// getDriver().get(ShopConstants.QATA01_URL_DEEPLINK);

		try {
			String[] currentUrl = getDriver().getCurrentUrl().split("home");
			String purchase = currentUrl[0] + "purchase/shop/i-8A0A8516050D4AEC9E04C319931E0E45/false/610214654465";
			getDriver().get(purchase);
			Reporter.log("Productdetails page is displayed");
		} catch (Exception e) {
			Assert.fail("Productdetails page is not displayed");
		}

	}

	/**
	 * navigate to line selector page
	 */
	public void navigateToLineSelectorPageWithDeepLink() {
		getDriver().get(E5_LINESELECTOR_URL_DEEPLINK);
		getDriver().manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	}

	/**
	 * launch TMNG and navigate to MyTmobile login page
	 *
	 * @param myTmoData
	 * @return
	 */
	public void LaunchTMNGAndNavigateToMyTmoLogin(MyTmoData myTmoData) {
		getDriver().get(DMO_URL_LINESELECTION);
		getDriver().manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

		PlpPage phonesPage = new PlpPage(getDriver());
		phonesPage.checkPageIsReady();
		phonesPage.clickDeviceWithAvailability(myTmoData.getDeviceName());

		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.verifyPhonePdpPageLoaded();
		phoneDetailsPage.selectLogIn();
	}

	/**
	 * launch TMNG
	 *
	 * @param myTmoData
	 * @return
	 */
	public void launchTmngDMO(MyTmoData myTmoData) {
		getDriver().get(DMO_URL_LINESELECTION);
		getDriver().manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		Reporter.log("Navigated to: " + DMO_URL_LINESELECTION);

	}

	/**
	 * launch TMNG prod URL
	 *
	 * @param myTmoData
	 * @return
	 */
	public void launchTmngProdURL(MyTmoData myTmoData) {
		getDriver().get(TMNG_URL_LINESELECTION);
		getDriver().manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		Reporter.log("Navigated to: " + TMNG_URL_LINESELECTION);

	}

	/**
	 * launch TMNG QAT
	 *
	 * @param myTmoData
	 * @return
	 */
	public void launchTmngQAT(MyTmoData myTmoData) {
		getDriver().get(QAT_URL_LINESELECTION);
		getDriver().manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

	}

	/**
	 * launch TMNG and Login to My-tmobile navigate to trade In Another Device Page
	 *
	 * @param myTmoData
	 * @return
	 */
	public CartPage launchTMNGAndNavigateToMyTmoCartPage(TMNGData tMNGData, MyTmoData myTmoData) {
		LaunchTMNGAndNavigateToMyTmoLogin(myTmoData);
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(), myTmoData.getLoginPassword());

		/*
		 * HomePage homePage = new HomePage(getDriver()); homePage.verifyPageLoaded();
		 * homePage.clickShoplink(); ShopPage newShopPage = new ShopPage(getDriver());
		 * newShopPage.verifyNewShoppage();
		 *
		 * newShopPage.clickOnFeaturedDevices(futureDeviceTest); PDPPage pdpPage = new
		 * PDPPage(getDriver()); pdpPage.verifyPDPPage();
		 *
		 * pdpPage.clickAddToCart(); pdpPage.clickOnPaymentOption();
		 * pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		 */

		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickWantToTradeDifferentPhoneLink();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectCarrier(myTmoData.getCarrier());
		tradeInAnotherDevicePage.selectMake(myTmoData.getMake());
		tradeInAnotherDevicePage.selectModel(myTmoData.getModel());
		tradeInAnotherDevicePage.setIMEINumber(myTmoData.getiMEINumber());
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceCondition();
		tradeInDeviceConditionValuePage.clickOnGoodCondition();
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickContinueTradeInButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		if (accessoryPLPPage.verifyAccessoryPLPPageURL() == true)
			;
		{
			// accessoryPLPPage.clickOkButton();
			accessoryPLPPage.clickSkipAccessoriesCTA();
		}

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		return cartPage;
	}

	/**
	 * Navigate To Cart Page By Promo banner secondary CTA Flow
	 *
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToCartPageByPromoBannerSellWithTradeInFlow(MyTmoData myTmoData) {
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = navigateToTradeInValuePageThroughPromoBannerSell(
				myTmoData);
		tradeInDeviceConditionValuePage.clickContinueTradeInButton();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		if (accessoryPLPPage.verifyAccessoryPLPPageURL() == true)
			;
		{
			// accessoryPLPPage.clickOkButton();
			accessoryPLPPage.clickSkipAccessoriesCTA();
		}

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		return cartPage;
	}

	/**
	 * Navigate to TradeInValuePage with Promo Banner Sell button * @param myTmoData
	 *
	 * @return
	 */

	public TradeInDeviceConditionValuePage navigateToTradeInValuePageThroughPromoBannerSell(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		// shopPage.clickTradeInPromoBannerForInEligible();
		// shopPage.clickOnBannerWithPromo();
		shopPage.clickOnHeroBannerBuyButton();
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickWantToTradeDifferentPhoneLink();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectCarrier(myTmoData.getCarrier());
		tradeInAnotherDevicePage.selectMake(myTmoData.getMake());
		tradeInAnotherDevicePage.selectModel(myTmoData.getModel());
		tradeInAnotherDevicePage.setIMEINumber(myTmoData.getiMEINumber());
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage TradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		TradeInDeviceConditionValuePage.verifyTradeInDeviceCondition();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();

		return tradeInDeviceConditionValuePage;
	}

	/**
	 * Navigate to TradeInValuePage with Promo Banner Sell button using New Line
	 * selector page* @param myTmoData
	 *
	 * @return
	 */

	public TradeInDeviceConditionValuePage navigateToTradeInValuePageThroughPromoBannerSellUsingLineseltor(
			MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickOnHeroBannerSellButton();
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickWantToTradeDifferentPhoneLink();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage TradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		TradeInDeviceConditionValuePage.clickOnGoodCondition();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		return tradeInDeviceConditionValuePage;
	}

	/**
	 * Navigate to TradeInValuePage with Promo Banner Sell button using New Line
	 * selector page* @param myTmoData
	 *
	 * @return
	 */

	public TradeInDeviceConditionValuePage navigateToTradeInValuePageThroughDeeplinkUsingLineseltor(
			MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		navigateToTradeinDevicePDPPageWithDeepLink();
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickWantToTradeDifferentPhoneLink();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage TradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		TradeInDeviceConditionValuePage.clickOnGoodCondition();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		return tradeInDeviceConditionValuePage;
	}

	/**
	 * Navigate TO Device Condition Page Jump Flow
	 *
	 * @param myTmoData
	 * @return
	 */
	public JumpTradeInPage navigateToDeviceConditionPageJumpFlowThroughlineSelectorPage(MyTmoData myTmoData) {
		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.clickYesProtectMyPlanCTA();
		JumpTradeInPage jumpTradeInPage = new JumpTradeInPage(getDriver());
		jumpTradeInPage.verifyPageUrl();
		return jumpTradeInPage;
	}

	/**
	 * Navigate to Cart page from new line selector details page
	 *
	 * @param myTmoData
	 * @return
	 */

	public CartPage navigateToCartPageFromlineSelectorPage(MyTmoData myTmoData) {
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickTradeInCTA();
		TradeInDeviceConditionValuePage TradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		TradeInDeviceConditionValuePage.verifyTradeInDeviceCondition();
		TradeInDeviceConditionValuePage.clickOnGoodCondition();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickContinueTradeInButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.checkPageIsReady();
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickSkipAccessoriesLink();
		CartPage cartPage = new CartPage(getDriver());
		return cartPage;
	}

	/**
	 * Navigate to Jump Cart page
	 *
	 * @param myTmoData
	 * @return
	 */

	public JumpCartPage navigateToJumpCartPage(MyTmoData myTmoData) {
		JumpTradeInPage jumpTradeInPage = navigateToDeviceConditionPageJumpFlowThroughlineSelectorPage(myTmoData);
		jumpTradeInPage.clickDisableFindMyPhoneCheckBox();
		jumpTradeInPage.clickLCDDisplayCheckBox();
		jumpTradeInPage.clickPowerOnCheckBox();
		jumpTradeInPage.clickContinueButton();
		JumpTradeInConfirmationPage jumpTradeInConfirmationPage = new JumpTradeInConfirmationPage(getDriver());
		jumpTradeInConfirmationPage.verifyPageUrl();
		jumpTradeInConfirmationPage.clickAcceptAndContinueButton();
		JumpCartPage jumpCartPage = new JumpCartPage(getDriver());
		jumpCartPage.verifyJumpCartPage();
		return jumpCartPage;
	}

	/***
	 * Network XHR call for knowing the shop flow or content...etc.
	 *
	 */

	@SuppressWarnings("unchecked")
	public String getContentFlags(String serviceCall) {
		Map<String, Object> logType = new HashMap<>();
		logType.put("type", "sauce:network");
		List<Map<String, Object>> logEntries = (List<Map<String, Object>>) ((JavascriptExecutor) getDriver())
				.executeScript("sauce:log", logType);
		for (Map<String, Object> logEntry : logEntries) {

			String url = (String) logEntry.get("url");
			if (url.contains(serviceCall)) {
				return logEntry.get("body").toString();
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public String getContentFlagsvalue(String expval) {

		/*
		 * HashMap<String, String> pglocators = new HashMap<>();
		 * pglocators.put("bypassAccessoryList", "bypassAccessoryListA");
		 * pglocators.put("byPasspdpPage", "byPasspdpPage");
		 * pglocators.put("byPasslineselectorPage", "byPasslineselectorPage");
		 * pglocators.put("byPassphoneselectionPage", "byPassphoneselectionPage");
		 * pglocators.put("byPasstradeInAnotherDevicePage",
		 * "byPasstradeInAnotherDevicePage");
		 * pglocators.put("byPassdeviceConditionsPage", "byPassdeviceConditionsPage");
		 * pglocators.put("byPasstradeInValuePage", "byPasstradeInValuePage");
		 * pglocators.put("byPassInsuranceMigrationPage",
		 * "byPassInsuranceMigrationPage"); pglocators.put("byPasscartPage",
		 * "byPasscartPage"); pglocators.put("byPassaccessoriesStandAlonePLPPage",
		 * "byPassaccessoriesStandAlonePLPPage");
		 * 
		 * Map<String, Object> logType = new HashMap<>(); logType.put("type",
		 * "sauce:network"); List<Map<String, Object>> logEntries = (List<Map<String,
		 * Object>>) ((JavascriptExecutor) getDriver()) .executeScript("sauce:log",
		 * logType);
		 * 
		 * for (Map<String, Object> logEntry : logEntries) { String url = (String)
		 * logEntry.get("url"); if (url.contains("environmentvalues.partial")) { try {
		 * URL url1 = new URL(url); if (JsonPath.from(url1).get(pglocators.get(expval))
		 * == null)
		 * 
		 * return "false"; else return
		 * JsonPath.from(url1).get(pglocators.get(expval)).toString(); } catch
		 * (Exception e) { return "false"; } } }
		 */
		return "false";
	}

	/**
	 * navigate To Customer Intent Page Through AddALine
	 *
	 * @param myTmoData
	 * @return
	 */
	public ConsolidatedRatePlanPage navigateToCustomerIntentPageThroughAddALine(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.checkPageIsReady();
		return consolidatedRatePlanPage;
	}

	/**
	 * Navigate To Order Conformation Page By See All Phones With Skip Trade In Flow
	 * and FRP Payment Option
	 *
	 * @param myTmoData
	 * @return
	 */
	public OrderConfirmationPage navigateToOrderConformationPageBySeeAllPhonesWithFRPPaymentOption(
			MyTmoData myTmoData) {
		navigateToPaymentTabBySeeAllPhonesWithSkipTradeIn(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyPageUrl();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		cartPage.clickAcceptAndPlaceOrder();
		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
		orderConfirmationPage.verifyOrderConfirmationPage();
		return orderConfirmationPage;
	}

	/**
	 * Navigates user to DeviceIntent Page from AAL QuickLink
	 * 
	 * @param myTmoData
	 * @return
	 */
	public DeviceIntentPage navigateToDeviceIntentPageFromAALQuickLink(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		return deviceIntentPage;
	}

	/**
	 * Navigates user to Rate plan Page using BYOD flow
	 * 
	 * @param myTmoData
	 * @return
	 */
	public ConsolidatedRatePlanPage navigateToRatePlanPageFromAALBYODFlow(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		return consolidatedRatePlanPage;
	}

	/**
	 * Navigates user to Rate plan Page using buy new phone AAL flow
	 * 
	 * @param myTmoData
	 * @return
	 */
	public ConsolidatedRatePlanPage navigateToRatePlanPageFromAALBuyNewPhoneFlow(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("Add a line");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyANewPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		return consolidatedRatePlanPage;
	}

	/**
	 * Navigates user to Shipping section using buy new phone AAL flow
	 * 
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToShippingPageFromAALBuyNewPhoneFlow(MyTmoData myTmoData) {
		ConsolidatedRatePlanPage ratePlanpage = navigateToRatePlanPageFromAALBuyNewPhoneFlow(myTmoData);
		ratePlanpage.clickOnContinueCTA();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.clickDeviceByName(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickOnContinueAALButton();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyAALShippingDetails();
		return cartPage;
	}

	/**
	 * Navigates to Cart page using BYOD AAL flow
	 * 
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToCartPageFromAALBYODFlow(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");

		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();

		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();

		return cartPage;
	}

	/**
	 * Navigates to Cart page using BYOD AAL flow
	 *
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToCartPageFromAALNonBYODFlow(MyTmoData myTmoData) {
		ConsolidatedRatePlanPage ratePlanpage = navigateToRatePlanPageFromAALBuyNewPhoneFlow(myTmoData);
		ratePlanpage.verifyConsolidatedRatePlanPage();
		ratePlanpage.clickOnContinueCTA();

		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.clickDeviceByName(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.clickContinueBtn();

		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.clickOnSkipTradeInCTA();

		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		return cartPage;
	}

	/**
	 * Navigates to Check Order Status page
	 * 
	 * @return
	 */

	public void navigateToCheckOrderStatusPage() {
		getDriver().get(QAT_CHECKORDERSTATUS_URL);
		getDriver().manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

	}

	/**
	 * Navigates to Check Order Status page Non BYOD
	 * 
	 * @param myTmoData
	 * @return
	 */

	public CheckOrderPage navigateToCheckOrderPageNonBYOD(MyTmoData myTmoData) {
		ConsolidatedRatePlanPage ratePlanpage = navigateToRatePlanPageFromAALBuyNewPhoneFlow(myTmoData);
		ratePlanpage.verifyConsolidatedRatePlanPage();
		ratePlanpage.clickOnContinueCTA();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
		orderConfirmationPage.verifyOrderConfirmationPage();
		orderConfirmationPage.clickOrderStatus();
		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyCheckOrderPage();
		return checkOrderPage;
	}

	/**
	 * Navigates to Check Order Status page BYOD
	 * 
	 * @param myTmoData
	 * @return
	 */

	public CheckOrderPage navigateToCheckOrderPageBYOD(MyTmoData myTmoData) {
		CartPage cartPage = new CartPage(getDriver());
		navigateToCartPageFromAALBYODFlow(myTmoData);
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
		orderConfirmationPage.verifyOrderConfirmationPage();
		orderConfirmationPage.clickOrderStatus();
		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyCheckOrderPage();
		return checkOrderPage;
	}

	/**
	 * Navigate To Order Confirmation Page By AAL BYOD
	 * 
	 *
	 * @param myTmoData
	 * @return
	 */
	public OrderConfirmationPage navigateToOrderConformationAALBYOD(MyTmoData myTmoData) {
		CartPage cartPage = new CartPage(getDriver());
		navigateToCartPageFromAALBYODFlow(myTmoData);
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		if (identityReviewIntroductionPage.verifyPageUrl()) {
			identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
			identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
			IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
			identityReviewQuestionPage.verifyReviewQuestionPage();
			identityReviewQuestionPage.clickFirstOption();
			identityReviewQuestionPage.clickSubmit();
		}
		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
		orderConfirmationPage.verifyOrderConfirmationPage();
		return orderConfirmationPage;
	}

	/**
	 * Navigate To Order Confirmation Page By AAL Non BYOD
	 * 
	 *
	 * @param myTmoData
	 * @return
	 */
	public OrderConfirmationPage navigateToOrderConformationAALNonBYOD(MyTmoData myTmoData) {
		ConsolidatedRatePlanPage ratePlanpage = navigateToRatePlanPageFromAALBuyNewPhoneFlow(myTmoData);
		ratePlanpage.verifyConsolidatedRatePlanPage();
		ratePlanpage.clickOnContinueCTA();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		if (identityReviewIntroductionPage.verifyPageUrl()) {
			identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
			identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
			IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
			identityReviewQuestionPage.verifyReviewQuestionPage();
			identityReviewQuestionPage.clickFirstOption();
			identityReviewQuestionPage.clickSubmit();
		}
		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
		orderConfirmationPage.verifyOrderConfirmationPage();
		return orderConfirmationPage;
	}

	/**
	 * Navigates Cart Page using buy new phone AAL flow Interstitial Trade In Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToCartPageFromAALBuyNewPhoneFlowUsingInterstitialTradeIn(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyANewPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.clickDeviceByName(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickOnContinueAALButton();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		return cartPage;
	}

	/**
	 * Navigates Interstitial Trade in Page using buy new phone AAL flow
	 * 
	 * @param myTmoData
	 * @return
	 */
	public InterstitialTradeInPage navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("Add a line");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyANewPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickDeviceByName(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickOnContinueAALButton();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();

		return interstitialTradeInPage;
	}

	/**
	 * Navigates Interstitial Trade in Page using See All Phones - Unknown Intent
	 * 
	 * @param myTmoData
	 * @return
	 */
	public InterstitialTradeInPage navigateToInterstitialTradeInPageFromSeeAllPhones(MyTmoData myTmoData) {
		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();

		return interstitialTradeInPage;
	}

	/**
	 * Navigate To Edit Shipping address By See All Phones With Skip Trade In
	 *
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToEditShippingAddBySeeAllPhonesWithSkipTradeIn(MyTmoData myTmoData) {
		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickContinueToShippingButton();
		cartPage.clickEditShippingAddress();
		cartPage.verifyShippingEditExpand();
		return cartPage;
	}

	/**
	 * Navigate To CartPage Without Device Protection By See All Phones With Skip
	 * Trade-In
	 * 
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToCartPageWithoutDeviceProtectionBySeeAllPhonesWithSkipTradeIn(MyTmoData myTmoData) {
		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.selectMemory(myTmoData.getDeviceMemory());
		pdpPage.clickAddToCart();

		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();

		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickOnNoIWillTakeChancesButton();
		deviceProtectionPage.clickYesIamSureButton();

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupCloseButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();

		return cartPage;
	}

	/**
	 * Navigate to Phones Product details page(Main)
	 *
	 */
	public PdpPage navigateToPhonesPDP(TMNGData tMNGData) {
		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		return phonesPdpPage;
	}

	// common methods

	/**
	 * Navigate to Phones page
	 *
	 */
	public PlpPage navigateToPhonesPlpPage(TMNGData tMNGData) {
		SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
		Reporter.log(
				"Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/" + sessionId
						+ "\">SauceLabs</a>|<a target=\"_blank\" href=\"http://prdplctep000d.unix.gsm1900.org/videos/"
						+ sessionId + ".mp4" + "\"> Sbox</a>");
		loadTmngURL(tMNGData);
		PlpPage phonesPlpPage = new PlpPage(getDriver());
		String env = System.getProperty("environment").trim();
		if (env.contains("dmo") || env.contains("uatent") || env.contains("www2") || env.contains("stag")
				|| env.contains("qat")) {
			getDriver().get(env + phonesPageUrl);

		} else {
			HomePage homePage = new HomePage(getDriver());
			homePage.clickPhonesLink();
		}
		// phonesPlpPage.refreshPage();
		phonesPlpPage.verifyPhonesPlpPageLoaded();
		return phonesPlpPage;
	}

	@SuppressWarnings("deprecation")
	public void loadTmngURL(TMNGData tMNGData) {
		try {
			logger.info("getTMOURL method called ");
			String tmngUrl = null;
			String environmentVariable = null;
			String environment = tMNGData.getEnv();
			if (StringUtils.isEmpty(environment)) {
				environment = System.getProperty("environment");
				tmngUrl = environment;
				environmentVariable = readEnvFromUrl(environment);
				tMNGData.setEnv(environmentVariable);
			}
			if (StringUtils.isNotEmpty(environmentVariable) && StringUtils.isEmpty(environment)) {
				StringBuilder mytmoBuilder = new StringBuilder();
				mytmoBuilder.append(TMO_URL);
				tmngUrl = getTestConfig().getConfig(mytmoBuilder.toString());
			}
			launchTmng(tmngUrl);
			// HomePage homePage = new HomePage(getDriver());
			// homePage.verifyPageLoaded();
		} catch (Exception ex) {
			Reporter.log("caught exception while loading ");
		}
	}

	private void launchTmng(String tmngUrl) {
		getDriver().get(tmngUrl);
		Reporter.log("Url is provided: " + tmngUrl);
	}

	private static String readEnvFromUrl(String environment) {
		String substring = environment.substring(environment.indexOf("//"), environment.indexOf("."));
		return substring.replaceAll("[^A-Za-z0-9 ]", "");
	}

	/**
	 * launch TMNG Staging
	 *
	 * @param myTmoData
	 * @return
	 */
	public void launchTmngStaging(MyTmoData myTmoData) {
		getDriver().get(STAGING_URL_LINESELECTION);
		getDriver().manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		Reporter.log("Navigated to: " + STAGING_URL_LINESELECTION);
	}

	/**
	 * 
	 * @param myTmoData
	 * @param CookieName
	 * @param CookieValue
	 */
	public void deepLink(MyTmoData myTmoData, String CookieName, String CookieValue) {
		SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
		Reporter.log("Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/"
				+ sessionId + "\">SauceLabs</a>");
		LoginPage loginPage = new LoginPage(getDriver());
		launchSiteWithCoockies(myTmoData, CookieName, CookieValue);
		performLogin(myTmoData, loginPage);
		chooseAccount(myTmoData);
	}

	/**
	 * Launch tmo site
	 * 
	 * @param myTmoData
	 * @param CookieName
	 * @param CookieValue
	 */
	public void launchSiteWithCoockies(MyTmoData myTmoData, String CookieName, String CookieValue) {
		try {
			String mytmoURL = "https://my.t-mobile.com/purchase/shop";
			getDriver().get(mytmoURL);

			Cookie ck = new Cookie(CookieName, CookieValue);
			getDriver().manage().addCookie(ck);

			CommonPage commonPage = new CommonPage(getDriver());
			commonPage.verifyLoginPageTitle();
			Reporter.log("Application(T-Mobile URL '" + mytmoURL + "') Loaded");
			unlockAccount(myTmoData);
		} catch (Exception e) {
			logger.error(Constants.UNABLE_TO_UNLOCK + e);
		}
	}

	/**
	 * Navigates to cart page using AAL Unknown Intent Intent
	 * 
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToCartPageAALUnknownIntentSeeAllPhones(MyTmoData myTmoData) {
		navigateToInterstitialTradeInPageFromSeeAllPhones(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		return cartPage;
	}

	/**
	 * Navigates to cart page using AAL Unknown Intent Trade-in flow
	 * 
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToCartPageAALUnknownIntentSeeAllPhonesTradein(MyTmoData myTmoData) {
		navigateToInterstitialTradeInPageFromSeeAllPhones(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.verifyTradeInAnotherDeviceTitle();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInConfirmationPage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();
		tradeInConfirmationPage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		return cartPage;
	}

	/**
	 * Navigates to cart page using AAL Known Intent Trade-in flow
	 * 
	 * @param myTmoData
	 * @return
	 */
	public CartPage navigateToCartPageAALknownIntentSeeAllPhonesTradein(MyTmoData myTmoData) {
		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialPageTitle();
		interstitialTradeInPage.verifyAuthorableTradeInImage();
		interstitialTradeInPage.verifyYesTradeinTile();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		deviceConditionsPage.titleTextDisplayed();
		deviceConditionsPage.verifyAssuredQuestionsDisplayed();
		deviceConditionsPage.verifyAssuredQuestionsYesNoDisplayed();
		deviceConditionsPage.verifyNotSureWhatToSelectHyperLink();
		deviceConditionsPage.verifyBatteryQuestionDisplayed();
		deviceConditionsPage.verifyWaterDropQuestionDisplayed();
		deviceConditionsPage.verifyCrackedScreenQuestionDisplay();
		deviceConditionsPage.verifyLocationQuestionDisplay();
		deviceConditionsPage.verifyNewContinueButton();
		deviceConditionsPage.clickFindMyIphoneYesButton();
		deviceConditionsPage.clickAcceptableLCDYesButton();
		deviceConditionsPage.clickWaterDamageNoButton();
		deviceConditionsPage.clickDevicePowerONYesButton();
		deviceConditionsPage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInDeviceConditionConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();
		TradeInConfirmationPage tradeInConfirmationPage = new TradeInConfirmationPage(getDriver());
		tradeInConfirmationPage.verifyEligibilityMessage();
		tradeInConfirmationPage.clickOnTradeInThisPhone();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		return cartPage;
	}

	/**
	 * navigate to cart page using deeplink
	 *
	 * 
	 */
	public DeviceIntentPage navigateToDeviceIntentUsingDeeplink() {
		String environment = System.getProperty("environment");
		getDriver().get(environment + "/purchase/device-intent");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		return deviceIntentPage;
	}

	/**
	 * Navigate TO UNOPLP Page By See All Phones
	 *
	 * @param myTmoData
	 * @return
	 */
	public UNOPLPPage navigateToUNOPLPPageBySeeAllPhones(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickSeeAllPhones();
		UNOPLPPage unoPlpPage = new UNOPLPPage(getDriver());
		unoPlpPage.verifyPageLoaded();
		return unoPlpPage;
	}

	/**
	 * Navigate TO UNOPDP Page By See All Phones
	 *
	 * @param myTmoData
	 * @return
	 */
	public UNOPDPPage navigateToUNOPDPPageBySeeAllPhones(MyTmoData myTmoData) {
		UNOPLPPage unoPLPPage = navigateToUNOPLPPageBySeeAllPhones(myTmoData);
		unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		return unoPDPPage;
	}

	/**
	 * Navigate To UNOPDP Page By Feature Devices
	 *
	 * @param myTmoData
	 * @return
	 */
	public UNOPDPPage navigateToUNOPDPPageByFeatureDevices(MyTmoData myTmoData, String featureDeviceName) {
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickOnFeaturedDevices(featureDeviceName);
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		return unoPDPPage;
	}

	/**
	 * Navigate TO UNOPLP Page By selecting Upgrade CTA in home page
	 *
	 * @param myTmoData
	 * @return
	 */
	public UNOPLPPage navigateToUNOPLPPageByClickingUpgradeButtonInHomePage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickUpgradeLink();
		UNOPLPPage unoPlpPage = new UNOPLPPage(getDriver());
		unoPlpPage.verifyPageLoaded();
		return unoPlpPage;
	}

	/**
	 * Navigate TO UNOPLP Page through Phones page
	 *
	 * @param myTmoData
	 * @return
	 */
	public UNOPLPPage navigateToUNOPLPPageThroughPhonesPage(MyTmoData myTmoData) {
		PhonePages phonesPage = navigateToPhonePage(myTmoData);
		phonesPage.clickShopNow();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyPageLoaded();
		shopPage.clickSeeAllPhones();
		UNOPLPPage unoPlpPage = new UNOPLPPage(getDriver());
		unoPlpPage.verifyPageLoaded();
		return unoPlpPage;
	}

	/**
	 * Navigate TO UNOPLP Page through quick links
	 *
	 * @param myTmoData
	 * @return
	 */
	public UNOPLPPage navigateToUNOPLPPageThroughQuickLink(MyTmoData myTmoData) {
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("All phones");
		UNOPLPPage unoPlpPage = new UNOPLPPage(getDriver());
		unoPlpPage.verifyPageLoaded();
		return unoPlpPage;
	}

	/**
	 * Navigate TO Stand Alone Accessory UNOPLP Page through quick links
	 *
	 * @param myTmoData
	 * @return
	 */
	public UNOPLPPage navigateToUNOStandAloneAccessoryPLPPageThroughQuickLink(
			MyTmoData myTmoData) {
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("Accessories");
		UNOPLPPage unoPLPPage = new UNOPLPPage(getDriver());
		unoPLPPage.verifyAccessoriesPageLoaded();
		return unoPLPPage;
	}

	/**
	 * Navigate to TMNG PDP page for Accessories.
	 *
	 */
	public PdpPage navigateToAccessoryMainPDP(TMNGData tMNGData) {
		PlpPage accessoriesPlpPage = navigateToPhonesPlpPage(tMNGData);
		accessoriesPlpPage.clickOnAccessoriesMenuLink();
		accessoriesPlpPage.verifyAccessoriesPlpPageLoaded();
		accessoriesPlpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryPdpPage = new PdpPage(getDriver());
		accessoryPdpPage.verifyAccessoriesPdpPageLoaded();
		return accessoryPdpPage;
	}

	/**
	 * Navigates user to UNO PLP Page using buy new phone AAL flow
	 * 
	 * @param myTmoData
	 * @return
	 */
	public UNOPLPPage navigateToUNOPLPPageFromAALBuyNewPhoneFlow(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("Add a line");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyANewPhoneOption();
		UNOPLPPage unoPLPPage = new UNOPLPPage(getDriver());
		unoPLPPage.verifyPageLoaded();
		return unoPLPPage;
	}

	/**
	 * Navigates user to UNO PDP Page using buy new phone AAL flow
	 * 
	 * @param myTmoData
	 * @return
	 */
	public UNOPDPPage navigateToUNOPDPPageFromAALBuyNewPhoneFlow(MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("Add a line");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyANewPhoneOption();
		UNOPLPPage unoPLPPage = new UNOPLPPage(getDriver());
		unoPLPPage.verifyPageLoaded();
		unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		return unoPDPPage;
	}

}
