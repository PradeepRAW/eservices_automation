/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.JodDevicePaymentPage;
import com.tmobile.eservices.qa.pages.payments.LeaseDetailsPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author Sam Iurkov
 *
 */
public class LeaseDetailsPageTest extends PaymentCommonLib {
    private static final Logger logger = LoggerFactory.getLogger(EIPDetailsPageTest.class);

    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING})
    public void testLeaseDetailsPageLoad(ControlTestData data, MyTmoData myTmoData) {
        logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
        Reporter.log("Billing - Verify EIP Bill Summary Details");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("6. Click on Jump On Demand  | Lease Details page should be able to load");
        Reporter.log("================================");

    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING})
    public void testGetInstallmentPlanDetailsOnLeaseDetailsPage(ControlTestData data, MyTmoData myTmoData) {
        logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
        Reporter.log("Billing - Verify EIP Bill Summary Details");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("6. Click on Jump On Demand  | Lease Details page should be able to load");
        Reporter.log("7. Under the current lease click on get installment plans | It should navigate to JOD device payment");
        Reporter.log("================================");

    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING})
    public void testSeeMoreDetailsForleaseDetailsPage(ControlTestData data, MyTmoData myTmoData) {
        logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
        Reporter.log("Billing - Verify EIP Bill Summary Details");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("6. Click on Jump On Demand  | Lease Details page should be able to load");
        Reporter.log("7. Click on see more details| should able to see the popup with more ");
        Reporter.log("================================");


    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING})
    public void testForAllLeasesandLinesDropdown(ControlTestData data, MyTmoData myTmoData) {
        logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
        Reporter.log("Billing - Verify EIP Bill Summary Details");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("6. Click on All leases  | All lease lines should be displayed");
        Reporter.log("7. Click on single lease line| The particular lease line details should be visible ");
        Reporter.log("================================");


    }


    /*
	 * US356350  PII Masking > Variables on JOD Page
	 * 
	 * Flow: Regular EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPIIMaskingJODpages(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US356350 | US356350 PII Masking > Variables on JOD Page");
		Reporter.log("Data Condition | JOD Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("5. Click on JOD View Lease Deatils  | JOD View Lease Details should be displayed");
		Reporter.log(
				"6. Verify PII Masking on JOD Lease Details Page | Mobile Number and IMEI Number PID's presented for Current Lease and Completed Lease");
		Reporter.log("7. Select  Lease Drop Down | PID is presented for Lines in Lease DropDown");
		Reporter.log("8. Click on Get Installment Plan | Purhcase Option Installment Plan Page should be displayed");
		Reporter.log("9. Verify PII Masking on Purhcase Option Installment Plan Page | Billing/Shipping/911  address and Mobile Number PID's presented ");

		LeaseDetailsPage jodLeaseDetailsPage  = navigateToLeaseDetailsPage(myTmoData);
		jodLeaseDetailsPage.verifyPIICustomerName(PaymentConstants.PII_CUSTOMER_CUSTOMERNAME_PID);
		jodLeaseDetailsPage.verifyPIIPhoneNumber(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
		jodLeaseDetailsPage.verifyPIIImeiNumber(PaymentConstants.PII_CUSTOMER_IMEI_PID);
		jodLeaseDetailsPage.clickLeaseDropDown();
		jodLeaseDetailsPage.verifyPIIforLinesInLeaseDropDownList(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
		jodLeaseDetailsPage.verifyPIIforLeaseDropDown(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
		jodLeaseDetailsPage.clickGetInstallMentPlan();
		JodDevicePaymentPage jodDevicePaymentPage  = new JodDevicePaymentPage(getDriver());
		jodDevicePaymentPage.verifyPageLoaded();
		jodDevicePaymentPage.verifyPIIforCustomerFirstName(PaymentConstants.PII_CUSTOMER_CUSTFIRSTNAME_PID);
		jodDevicePaymentPage.verifyPIIforCustomerLastName(PaymentConstants.PII_CUSTOMER_CUSTLASTNAME_PID);
		jodDevicePaymentPage.verifyPIIforPhoneNumber(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
		jodDevicePaymentPage.verifyPIIforBillingAddress(PaymentConstants.PII_CUSTOMER_BILLINGADDRESS_PID);
		
		
	}
	
	/**
	 * US367994: PII: Mask customers personal and credit card information - EOL
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCartPageMaskMsisdnAndBillingAddressEOL(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case US367994: PII: Mask customers personal and credit card information - EOL");
		Reporter.log("Data Condition | JOD Eligible, EOL");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("5. Click on JOD View Lease Deatils  | JOD View Lease Details should be displayed");
		Reporter.log(
				"6. Verify PII Masking on JOD Lease Details Page | Mobile Number and IMEI Number PID's presented for Current Lease and Completed Lease");
		Reporter.log("7. Select  Lease Drop Down | PID is presented for Lines in Lease DropDown");
		Reporter.log("8. Click on Get Installment Plan | Purhcase Option Installment Plan Page should be displayed");
		Reporter.log("9. Verify PII Masking on Purhcase Option Installment Plan Page | Billing/Shipping/911  address and Mobile Number PID's presented ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		String mask = "pii_mask_data";

		LeaseDetailsPage jodLeaseDetailsPage  = navigateToLeaseDetailsPage(myTmoData);
		jodLeaseDetailsPage.clickGetInstallMentPlan();
		JodDevicePaymentPage jodDevicePaymentPage  = new JodDevicePaymentPage(getDriver());
		jodDevicePaymentPage.verifyPageLoaded();
		jodDevicePaymentPage.verifyMsisdnMaskElement(mask);
		jodDevicePaymentPage.verifyMaskedElementForCityStateZip(mask);
		

		
		
	}
}
