package com.tmobile.eservices.qa.commonlib;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.jayway.jsonpath.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.exception.FrameworkException;
import com.tmobile.eqm.testfrwk.ui.core.web.WebTest;
import com.tmobile.eservices.qa.api.RestServiceHelper;
import com.tmobile.eservices.qa.api.unlockdata.EmailServiceHelper;
import com.tmobile.eservices.qa.api.unlockdata.EmailServiceImpl;
import com.tmobile.eservices.qa.api.unlockdata.RequestData;
import com.tmobile.eservices.qa.api.unlockdata.RestService;
import com.tmobile.eservices.qa.api.unlockdata.ServiceException;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.global.ChangePasswordPage;
import com.tmobile.eservices.qa.pages.global.ChooseAccountPage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.tmng.functional.SpecialHoursPage;
import com.tmobile.eservices.qa.pages.tmng.functional.StoreLocatorPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author Thulasiram
 *
 */
public class CommonLibrary extends WebTest {

	JavascriptExecutor javaScript;
	RestService restService;

	protected final static Logger logger = LoggerFactory.getLogger(CommonLibrary.class);
	
	public static final String STORELOCATORURL = "/store-locator";

	/*	*//**
			 * 
			 * @param myTmoData
			 *//*
				 * @SuppressWarnings("deprecation") public void getTMOURL(MyTmoData myTmoData) {
				 * logger.info("getTMOURL method called "); String mytmoURL = null; String
				 * environmentVariable = null; String environment = myTmoData.getEnv(); if
				 * (StringUtils.isEmpty(environment)) { environment =
				 * System.getProperty("environment"); mytmoURL = environment;
				 * environmentVariable = readEnvFromUrl(environment);
				 * myTmoData.setEnv(environmentVariable); } if
				 * (StringUtils.isNotEmpty(environmentVariable) &&
				 * StringUtils.isEmpty(environment)) { StringBuilder mytmoBuilder = new
				 * StringBuilder(); mytmoBuilder.append(Constants.MYTMO_URL); mytmoURL =
				 * getTestConfig().getConfig(mytmoBuilder.toString()); } launchURL(mytmoURL); }
				 */

	/**
	 *
	 * @param myTmoData
	 */
	@SuppressWarnings("deprecation")
	public void getTMOURL(MyTmoData myTmoData) {
		logger.info("getTMOURL method called ");
		String mytmoURL = null;
		String environmentVariable = null;
		String environment = myTmoData.getEnv();

		try {
			if (StringUtils.isEmpty(environment)) {
				environment = System.getProperty("environment");
				mytmoURL = environment;
				environmentVariable = readEnvFromUrl(environment);
				myTmoData.setEnv(environmentVariable);
			}

			if (StringUtils.isNotEmpty(environmentVariable) && StringUtils.isEmpty(environment)) {
				StringBuilder mytmoBuilder = new StringBuilder();
				mytmoBuilder.append(Constants.MYTMO_URL);
				mytmoURL = getTestConfig().getConfig(mytmoBuilder.toString());
			}
		} catch (Exception e) {
			Reporter.log("Unable to launch url");
		}
		launchURL(mytmoURL);
	}

	/**
	 *
	 * @param myTmoData
	 */
	public void getURL(MyTmoData myTmoData) {
		try {
			logger.info("getTMOURL method called ");
			String env = System.getProperty("environmentTMO").trim();
			Reporter.log("TMO env is: " + env);
			if (env.endsWith("/")) {
				env = env.substring(0, env.length() - 1);
			}
			getDriver().get(env + "/cell-phones");
		} catch (Exception e) {
			Reporter.log("Unable to launch TMO url");
		}
	}

	/**
	 * Read Env From URL
	 * 
	 * @param environment
	 * @return
	 */
	private static String readEnvFromUrl(String environment) {
		String substring = environment.substring(environment.indexOf("//"), environment.indexOf("."));
		return substring.replaceAll("[^A-Za-z0-9 ]", "");
	}

	/**
	 * Navigate To Home Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public HomePage navigateToHomePage(MyTmoData myTmoData) {
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.checkPageIsReady();
		/*
		 * if (getDriver().getCurrentUrl().contains("shop")) {
		 * getDriver().get(System.getProperty("environment") + "/home");
		 * homePage.checkPageIsReady(); }
		 */
		homePage.verifyPageLoaded();
		return homePage;
	}

	/**
	 * Navigate To Account History Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public AccountHistoryPage navigateToAccountHistoryPage(MyTmoData myTmoData) {
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		homePage.verifyHomePage();
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		return accountHistoryPage;
	}

	/**
	 * Click on Logout
	 * 
	 * @param myTmoData
	 * @return
	 */
	public CommonLibrary logOut(MyTmoData myTmoData) {
		HomePage homePage = new HomePage(getDriver());
		homePage.clickLogoutButton();
		return this;
	}

	/**
	 * Login To MyTmo
	 * 
	 * @param myTmoData
	 */
	public void launchAndPerformLogin(MyTmoData myTmoData) {
		String misdinpwd = getmisdinpaswordfromfilter(myTmoData);
		if (misdinpwd == null) {
			Assert.fail("No valid data identified");
		}
		String misdin = misdinpwd.split("/")[0];
		String pwd = misdinpwd.split("/")[1];
		myTmoData.setLoginEmailOrPhone(misdin);
		myTmoData.setLoginPassword(pwd);
		System.out.println(myTmoData.getTestName());
		System.out.println(myTmoData.getLoginEmailOrPhone());

		SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
		Reporter.log(
				"Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/" + sessionId
						+ "\">SauceLabs</a>|<a target=\"_blank\" href=\"http://prdplctep000d.unix.gsm1900.org/videos/"
						+ sessionId + ".mp4" + "\"> Sbox</a>");
		LoginPage loginPage = new LoginPage(getDriver());
		launchSite(myTmoData, loginPage);
		performLogin(myTmoData, loginPage);
		chooseAccount(myTmoData);
		loginPermission();
	}

	/**
	 * Perform Login
	 * 
	 * @param myTmoData
	 * @param loginPage
	 */
	protected void performLogin(MyTmoData myTmoData, LoginPage loginPage) {
		try {
			String username = myTmoData.getLoginEmailOrPhone().trim();
			String password = myTmoData.getLoginPassword().trim();
			if (getDriver().getTitle().contains("Login") || getDriver().getTitle().contains("Access Messages")) {
				loginPage.performLoginAction(username, password);
			}
		} catch (Exception e) {
			Reporter.log("Login Unsuccessful");
		}

		Reporter.log(Constants.LOGIN_AS + myTmoData.getLoginEmailOrPhone() + "/" + myTmoData.getLoginPassword());
	}

	/**
	 * Launch MyTmo
	 * 
	 * @param myTmoData
	 * @param loginPage
	 */
	public void launchSite(MyTmoData myTmoData, LoginPage loginPage) {
		try {
			getTMOURL(myTmoData);
			unlockAccount(myTmoData);
		} catch (Exception e) {
			logger.error(Constants.UNABLE_TO_UNLOCK + e);
		}
	}

	/**
	 * This will reset the profile password
	 */

	public boolean changeProfilePassword(MyTmoData myTmoData) {
		boolean changePassword = false;

		RequestData requestData = new RequestData();
		String environment = System.getProperty(Constants.IAM_SERVER_ENVIRONEMT);
		requestData.setMsisdn(myTmoData.getLoginEmailOrPhone());
		// requestData.setEnvironment("PPD");
		requestData.setEnvironment(environment);
		// myTmoData.getLoginEmailOrPhone());
		long startTime = System.currentTimeMillis();
		// logger.info("startTime::::::{}", startTime);

		try {
			changePassword = new RestServiceHelper().changeProfilePassword(requestData);

		} catch (Exception e) {
			// e.printStackTrace();
			// logger.error(Constants.UNABLE_TO_UNLOCK + e);
			// throw new FrameworkException(Constants.UNABLE_TO_UNLOCK, e);
		}
		long stopTime = System.currentTimeMillis();
		// logger.info("stopTime::::::{}", stopTime);
		long elapsedTime = stopTime - startTime;
		// logger.info("elapsedTime::::::{}", elapsedTime);
		return changePassword;
	}

	/**
	 * Call this method when the user is locked
	 * 
	 * @param myTmoData
	 * @throws IOException
	 */
	public boolean unlockAccount(MyTmoData myTmoData) {

		RequestData requestData = new RequestData();
		String environment = System.getProperty(Constants.IAM_SERVER_ENVIRONEMT);
		requestData.setMsisdn(myTmoData.getLoginEmailOrPhone());
		requestData.setEnvironment(environment);
		// myTmoData.getLoginEmailOrPhone());
		long startTime = System.currentTimeMillis();
		logger.info("startTime::::::{}", startTime);
		boolean unlockAccount = false;
		try {
			unlockAccount = new RestServiceHelper().unlockAccount(requestData);
			System.out.println("UNLOCK ACCOUNT##" + unlockAccount);
		} catch (Exception e) {
			System.out.println("Exception while unlocking ac" + e.getMessage());
			e.printStackTrace();
			logger.error(Constants.UNABLE_TO_UNLOCK + e);
			throw new FrameworkException(Constants.UNABLE_TO_UNLOCK, e);
		}
		long stopTime = System.currentTimeMillis();
		logger.info("stopTime::::::{}", stopTime);
		long elapsedTime = stopTime - startTime;
		logger.info("elapsedTime::::::{}", elapsedTime);
		return unlockAccount;
	}

	/**
	 * Launch MyTmo URL
	 * 
	 * @param url
	 */
	private void launchURL(String url) {
		getDriver().manage().deleteAllCookies();
		getDriver().getWindowHandles().clear();
		if (getDriver() instanceof AppiumDriver) {
			try {
				for (int i = 0; i < 2; i++) {
					getDriver().get(url);
					waitForPageLoad(getDriver());
					if (getDriver().getCurrentUrl().contains("account.t-mobile.com"))
						break;
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else {
			getDriver().get(url);
			waitForPageLoad(getDriver());
			CommonPage commonPage = new CommonPage(getDriver());
			try {
				commonPage.verifyLoginPageTitle();
				Reporter.log("Application(T-Mobile URL '" + url + "') Loaded");
			} catch (Exception e) {
				Assert.fail("Login Page not loaded");
			}
		}

	}

	/**
	 * This method is used to get the pin from tmo profile
	 * 
	 * @param myTmoData
	 */
	public String getPinFromTMOProfile(MyTmoData myTmoData) {
		RequestData requestData = new RequestData();
		String environment = System.getProperty(Constants.IAM_SERVER_ENVIRONEMT);
		requestData.setMsisdn(myTmoData.getLoginEmailOrPhone());
		requestData.setEnvironment(environment.toLowerCase());
		return new RestServiceHelper().getPinProfileDetails(requestData);
	}

	/**
	 * This method is used to delete the profile in IAM
	 * 
	 * @param myTmoData
	 * @return true/false
	 */
	public boolean deletIAMProfile(MyTmoData myTmoData) {
		RequestData requestData = new RequestData();
		String environment = System.getProperty(Constants.IAM_SERVER_ENVIRONEMT);
		requestData.setMsisdn(myTmoData.getLoginEmailOrPhone());
		requestData.setEnvironment(environment.toLowerCase());
		return new RestServiceHelper().deleteProfile(requestData);
	}

	/**
	 * 
	 * @param myTmoData
	 * @return
	 */
	public String getTempPassword(MyTmoData myTmoData) {
		RequestData requestData = new RequestData();
		String environment = System.getProperty(Constants.IAM_SERVER_ENVIRONEMT);
		requestData.setMsisdn(myTmoData.getLoginEmailOrPhone());
		requestData.setEnvironment(environment.toLowerCase());
		return new RestServiceHelper().getTempPassword(requestData);
	}

	/**
	 * 
	 * @param myTmoData
	 * @return
	 */
	public boolean deleteEmails(MyTmoData myTmoData) {
		boolean verifyMailDeleted = false;
		try {
			verifyMailDeleted = new EmailServiceImpl().deleteEmails(myTmoData.getEmailId(), myTmoData.getPassword());
		} catch (Exception e) {
			Assert.fail("unable to  delete email in service");
			throw new ServiceException("unable to  delete email in service", e);
		}
		return verifyMailDeleted;
	}

	/**
	 * 
	 * @param myTmoData
	 * @return
	 */
	public String getContentFromEmail(MyTmoData myTmoData) {
		RequestData requestData = new RequestData();
		requestData.setEnvironment(myTmoData.getEnv());
		requestData.setMsisdn(myTmoData.getLoginEmailOrPhone());
		String emailContent = new EmailServiceHelper().getEmailContent(myTmoData.getEmailId(), myTmoData.getPassword());
		String httpSubstring = emailContent.substring(emailContent.indexOf("https"));
		String[] emailLink = httpSubstring.split(" ");
		return emailLink[0];
	}

	public String getFilePath() {
		String runlocal = System.getProperty("runlocal");
		String homeOrSauceLabpath;
		String property;
		if (StringUtils.isNotEmpty(runlocal) && StringUtils.equals(runlocal, "false")) {
			property = System.getProperty("user.home");
			logger.info("fileLocation {}", property);
			homeOrSauceLabpath = "C:/Users/Administrator/Downloads/8817001361.pdf";

		} else {
			property = System.getProperty("user.home");
			logger.info("fileLocation{}", property);
			homeOrSauceLabpath = property + Constants.PDFFILE_DOWNLOAD_PATH;
		}
		return homeOrSauceLabpath;
	}

	/**
	 * @param myTmoData
	 */
	public void chooseAccount(MyTmoData myTmoData) {
		waitForPageLoad(getDriver());
		ChooseAccountPage chooseAccountPage = new ChooseAccountPage(getDriver());
		if (getDriver().getTitle().contains("AccountChooser")) {
			chooseAccountPage.chooseAccount(myTmoData.getLoginEmailOrPhone());
		}
	}

	public void loginPermission() {
		if (getDriver().getCurrentUrl().contains("login/permissions.html")) {
			getDriver().get(System.getProperty("environment") + "/home.html");
		}
	}

	@SuppressWarnings("deprecation")
	public void deleteMails(MyTmoData myTmoData) {
		ChangePasswordPage changePasswordPage = new ChangePasswordPage(getDriver());
		getDriver().get(myTmoData.getUrl());
		changePasswordPage.enterMail(myTmoData.getEmailId());
		changePasswordPage.clickCheckInboxButton();
		changePasswordPage.clickDelete();
		getDriver().get(getTestConfig().getConfig("e3.app.mytmo.url"));
	}

	public static void waitForPageLoad(WebDriver driver) {
		JavascriptExecutor javaScript = (JavascriptExecutor) driver;
		for (int i = 0; i < 30; i++) {
			try {
				if ("complete".equalsIgnoreCase(javaScript.executeScript("return document.readyState").toString())) {
					Thread.sleep(2000);
					break;
				}
			} catch (Exception ex) {
				Reporter.log("caught exception while loading" + ex.getMessage());
			}
		}
	}

	public void verifyPage(String pageName, String titleTextToVerify) {
		try {
			CommonPage commonPage = new CommonPage(getDriver());
			commonPage.checkPageIsReady();
			commonPage.waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("homeUsage_waitcursor")));
			if (getDriver() instanceof AppiumDriver) {
				commonPage.waitFor(ExpectedConditions.visibilityOfElementLocated(By.className("co_billing-main-page")));
			} else {
				commonPage.waitFor(ExpectedConditions.visibilityOfElementLocated(By.className("co_balance")));
			}
			Assert.assertTrue(getDriver().getTitle().contains(titleTextToVerify), pageName + " is not loaded");
			Reporter.log(pageName + " is loaded");

		} catch (Exception e) {
			Reporter.log(pageName + " is not loaded");
			throw new FrameworkException("Home page failed to load/Login failed - Step Failed");
		}
	}

	public void verifyCurrentPageURL(String pageName, String urlTextToVerify) {
		try {
			waitForPageLoad(getDriver());
			Assert.assertTrue(getDriver().getCurrentUrl().contains(urlTextToVerify), pageName + " is not loaded");
			Reporter.log(pageName + " is loaded");

		} catch (Exception e) {
			Reporter.log(e.getMessage().trim());
			Reporter.log(pageName + " is not loaded");
		}
	}

	public void verifyFeaturedPlanPageURL() {
		verifyCurrentPageURL("featured plan Page", "featured-plan");
	}

	public void verifyPlanConfigurePageTitle() {
		verifyCurrentPageURL("Plan Configure Page", "plans-configure");
	}

	public void verifyPlanServiceListPageTitle() {
		verifyCurrentPageURL("odf service list", "odf/Service:%20FAMALL10,FAMNFX9,FAMNFX13");
	}

	public void verifyPlanServiceReviewPageURL() {
		verifyCurrentPageURL("Plan Service Review Page", "services-review");
	}

	@SuppressWarnings("unchecked")
	public List<NameValuePair> contentServiceCall() {
		Map<String, Object> logType = new HashMap<>();
		logType.put("type", "sauce:network");
		List<Map<String, Object>> logEntries = (List<Map<String, Object>>) ((JavascriptExecutor) getDriver())
				.executeScript("sauce:log", logType);
		for (Map<String, Object> logEntry : logEntries) {
			String url = (String) logEntry.get("url");
			if (url.contains("offerdetails.partial")) {
				List<NameValuePair> urlValues = URLEncodedUtils.parse(url, Charset.defaultCharset());
				if (urlValues != null)
					return urlValues;
			}
		}
		return null;
	}

	public void checkOfferDetailsVar() {
		Map<String, Object> logType = new HashMap<>();
		logType.put("type", "sauce:network");
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> logEntries = (List<Map<String, Object>>) ((JavascriptExecutor) getDriver())
				.executeScript("sauce:log", logType);
		for (Map<String, Object> logEntry : logEntries) {
			String url = (String) logEntry.get("url");
			if (url.contains("offerdetails.partial")) {
				JSONObject obj = new JSONObject("offerdetails.partial.json");
				String bannerImageURL = obj.getJSONObject("offerdetails").getString("bannerImageURL");
				Assert.assertFalse(bannerImageURL.isEmpty(), "Banner image is present");
				String bannerSmallTxt = obj.getJSONObject("offerdetails").getString("bannerSmallTxt");
				Assert.assertFalse(bannerSmallTxt.isEmpty(), "Banner small text is present");
				String bannerBigTxt = obj.getJSONObject("offerdetails").getString("bannerBigTxt");
				Assert.assertFalse(bannerBigTxt.isEmpty(), "Banner big text is present");
				String offerDescriptionHeader = obj.getJSONObject("offerdetails").getString("offerDescriptionHeader");
				Assert.assertFalse(offerDescriptionHeader.isEmpty(), "Offer description header is present");
				String offerDescriptionDetails = obj.getJSONObject("offerdetails").getString("offerDescriptionDetails");
				Assert.assertFalse(offerDescriptionDetails.isEmpty(), "Offer description details is present");
				String offerRedemptionSteps = obj.getJSONObject("offerdetails").getString("offerRedemptionSteps");
				Assert.assertFalse(offerRedemptionSteps.isEmpty(), "Offer Redemption steps is present");
				String faq = obj.getJSONObject("offerdetails").getString("faq");
				Assert.assertFalse(faq.isEmpty(), "faq is present");
				String legalTxt = obj.getJSONObject("offerdetails").getString("legalTxt");
				Assert.assertFalse(legalTxt.isEmpty(), "Legal text is present");
				String CTAFirstUrl = obj.getJSONObject("offerdetails").getString("CTAFirstUrl");
				Assert.assertFalse(CTAFirstUrl.isEmpty(), "CTA First url is present");
				String CTASecondUrl = obj.getJSONObject("offerdetails").getString("CTASecondUrl");
				Assert.assertFalse(CTASecondUrl.isEmpty(), "CTA Second url is present");
				String tradeInModels = obj.getJSONObject("offerdetails").getString("tradeInModels");
				Assert.assertFalse(tradeInModels.isEmpty(), "TradeIn Model is present");
			}
		}
	}

	/**
	 * Navigate To Profile Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected ProfilePage navigateToProfilePage(MyTmoData myTmoData) {
		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickProfileMenu();
		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
		return profilePage;
	}

	public void PasswordReset(MyTmoData myTmoData, LoginPage loginPage) throws InterruptedException {
		getDriver().get("http://10.92.28.199:10000/msisdn/" + myTmoData.getLoginEmailOrPhone() + "/deleteprofile");
		myTmoData.setEnv(null);
		getTMOURL(myTmoData);
		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.registerMsisdn(myTmoData);
	}

	/**
	 * Get MSSIDN & Password From Filters
	 * 
	 * @param myTmoData
	 * @return
	 */
	public String getmisdinpaswordfromfilter(MyTmoData myTmoData) {
		String randomElement = null;
		try {
			String fil = myTmoData.getfilter();
			if (fil == null) {
				return myTmoData.getLoginEmailOrPhone() + "/" + myTmoData.getLoginPassword();
			}
			String query = "$.[?( (@.capability!='am')&&" + fil + ")].misdin";
			List<String> tstdata = JsonPath.parse(new URL("http://172.28.49.103:3000/testdata")).read(query);
			if (tstdata.size() > 0) {
				Random rand = new Random();
				randomElement = tstdata.get(rand.nextInt(tstdata.size()));
			}
			Reporter.log("Filter Data condition " + randomElement);
		} catch (Exception e) {
			Assert.fail("Unable to retrive data from data server");
		}
		return randomElement;
	}

	/**
	 * Navigate To Home Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public NewHomePage navigateToNewHomePage(MyTmoData myTmoData) {
		launchAndPerformLogin(myTmoData);
		NewHomePage homePage = new NewHomePage(getDriver());
		try {
			homePage.verifyHomePage();
		} catch (Exception e) {
			Reporter.log("Home page not displayed");
		}
		return homePage;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getCardInfo(String cardType) throws IOException {
		Map<String, String> getcard = new HashMap<String, String>();
		String coquesry = "$.payment[?(@.cardtype=='" + cardType.toLowerCase() + "')].";
		getcard.put("nameoncard",
				((List<String>) JsonPath.parse(new File(Constants.Payments_Path)).read(coquesry + "nameoncard")).get(0)
						.toString());
		getcard.put("cvv", ((List<String>) JsonPath.parse(new File(Constants.Payments_Path)).read(coquesry + "cvv"))
				.get(0).toString());
		getcard.put("last4digits",
				((List<String>) JsonPath.parse(new File(Constants.Payments_Path)).read(coquesry + "last4digits")).get(0)
						.toString());
		getcard.put("zip", ((List<String>) JsonPath.parse(new File(Constants.Payments_Path)).read(coquesry + "zip"))
				.get(0).toString());
		getcard.put("expiry",
				((List<String>) JsonPath.parse(new File(Constants.Payments_Path)).read(coquesry + "expiry")).get(0)
						.toString());
		String parsejsonstr = ((List<String>) JsonPath.parse(new File(Constants.Payments_Path))
				.read(coquesry + "cardnumber")).get(0).toString();
		String encriptcardnum = new String(Base64.getDecoder().decode(parsejsonstr));
		getcard.put("cardnumber", encriptcardnum);
		getcard.put("cardcode",
				((List<String>) JsonPath.parse(new File(Constants.Payments_Path)).read(coquesry + "cardcode")).get(0)
						.toString());

		return getcard;

	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getBankInfo(String bank) throws IOException {
		Map<String, String> getbank = new HashMap<String, String>();
		String coquesry = "$.payment[?(@.banktype=='" + bank.toLowerCase() + "')].";
		getbank.put("nameonbank",
				((List<String>) JsonPath.parse(new File(Constants.Payments_Path)).read(coquesry + "nameonbank")).get(0)
						.toString());

		getbank.put("routingnumber",
				((List<String>) JsonPath.parse(new File(Constants.Payments_Path)).read(coquesry + "routingnumber"))
						.get(0).toString());
		/*
		 * getbank.put("bankaccountnumber", ((List<String>) JsonPath.parse(new
		 * File(Constants.Payments_Path)).read(coquesry + "bankaccountnumber"))
		 * .get(0).toString());
		 */

		String bannum = getrequiredvaluefromexpression(
				((List<String>) JsonPath.parse(new File(Constants.Payments_Path)).read(coquesry + "bankaccountnumber"))
						.get(0).toString());
		getbank.put("bankaccountnumber", bannum);

		getbank.put("paymentmethod",
				((List<String>) JsonPath.parse(new File(Constants.Payments_Path)).read(coquesry + "paymentmethod"))
						.get(0).toString());

		return getbank;
	}

	public String getrequiredvaluefromexpression(String express) {
		LocalDate today = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(express);
		String reqdate = formatter.format(today).toString();
		return reqdate;
	}

	public boolean launchAndPerformLoginWithJenkinsData(MyTmoData myTmoData) {
		String misdin = null;
		String password = null;

		misdin = System.getProperty("misdin");
		password = System.getProperty("Password");

		myTmoData.setLoginEmailOrPhone(misdin);
		myTmoData.setLoginPassword(password);
		System.out.println(myTmoData.getTestName());
		System.out.println(myTmoData.getLoginEmailOrPhone());

		SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
		Reporter.log(
				"Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/" + sessionId
						+ "\">SauceLabs</a>|<a target=\"_blank\" href=\"http://prdplctep000d.unix.gsm1900.org/videos/"
						+ sessionId + ".mp4" + "\"> Sbox</a>");
		LoginPage loginPage = new LoginPage(getDriver());
		launchSite(myTmoData, loginPage);
		// performLogin(myTmoData, loginPage);
		if (getDriver().getTitle().contains("Login") || getDriver().getTitle().contains("Access Messages")) {
			loginPage.performLoginAction(misdin, password);
		}
		chooseAccount(myTmoData);
		loginPermission();
		return false;
	}

	public void getAccessToken(String msidn) {

		RequestData requestData = new RequestData();
		String environment = System.getProperty(Constants.IAM_SERVER_ENVIRONEMT);
		// +System.getProperties().toString());
		requestData.setMsisdn(msidn);
		// requestData.setEnvironment("PPD");
		requestData.setEnvironment(environment);
		requestData.setProfileType("TMO");
		String authCode = new RestServiceHelper().getAuthCode(environment);

	}

	/**
	 * This method is used to Launch Special Hours page
	 *
	 * @param tMNGData
	 */

	public SpecialHoursPage launchSpecialHoursPage(TMNGData tMNGData) {
		SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
		try {
			getDriver().navigate().to(System.getProperty("environment"));
			Reporter.log(
					"URL provided for Special hours application launch: '" + System.getProperty("environment") + "'");
			SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
			Reporter.log("Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/"
					+ sessionId
					+ "\">SauceLabs</a>|<a target=\"_blank\" href=\"http://prdplctep000d.unix.gsm1900.org/videos/"
					+ sessionId + ".mp4" + "\"> Sbox</a>");
		} catch (Exception e) {
			Assert.fail("Caught exception while loading Special Hours page");
		}
		return specialHoursPage;
	}

	/**
	 * Perform Login for cookied customers
	 * 
	 * @param myTmoData
	 * @param loginPage
	 */
	protected void performLoginForCookiedCustomers(MyTmoData myTmoData, LoginPage loginPage) {
		try {
			String username = myTmoData.getLoginEmailOrPhone().trim();
			String password = myTmoData.getLoginPassword().trim();
			loginPage.performLoginActionForCookiedCustomers(username, password);
			chooseAccount(myTmoData);
			loginPermission();
		} catch (Exception e) {
			Reporter.log("Login Unsuccessful for cookied customers");
		}
		Reporter.log(Constants.LOGIN_AS + myTmoData.getLoginEmailOrPhone() + "/" + myTmoData.getLoginPassword());
	}
	
	/**
	 * Navigate to Store Locator Page from Home Page
	 *
	 */
	public StoreLocatorPage navigateToStoreLocatorPage(TMNGData tMNGData) {
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		try {
			getDriver().get(System.getProperty("environment") + STORELOCATORURL);
			SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
			Reporter.log("Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/"
					+ sessionId
					+ "\">SauceLabs</a>|<a target=\"_blank\" href=\"http://prdplctep000d.unix.gsm1900.org/videos/"
					+ sessionId + ".mp4" + "\"> Sbox</a>");
			Reporter.log("Store locator page is displayed");
			setLocation();
			checkPageIsReady();
		} catch (Exception e) {
			Assert.fail("Store locator page is not displayed");
		}
		return storeLocatorPage;
	}
	
	/**
	 * Set Geo Location
	 *
	 */

	public void setLocation() {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript(
				"window.navigator.geolocation.getCurrentPosition = function(success){ var position = {coords : {  latitude : 34.036885,  longitude : -118.144194 }  };  success(position);}");
		Reporter.log("Location is set");
	}
	
	/**
	 * We are using this method to verifying the page load status Check Page Is
	 * Ready
	 */
	public void checkPageIsReady() {
		javaScript = (JavascriptExecutor) getDriver();
		try {
			for (int i = 0; i < 30; i++) {

				Thread.sleep(2000);
				if ("complete".equalsIgnoreCase(javaScript.executeScript("return document.readyState").toString())) {
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception ex) {
			Reporter.log("caught exception while loading");
		}

	}


}