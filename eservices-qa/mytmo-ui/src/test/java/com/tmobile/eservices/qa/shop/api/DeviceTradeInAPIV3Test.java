package com.tmobile.eservices.qa.shop.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.DeviceTradeInApiV3;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.shop.ShopConstants;

import io.restassured.response.Response;

public class DeviceTradeInAPIV3Test extends DeviceTradeInApiV3 {

	public Map<String, String> tokenMap;
	JsonPath jsonPath;

	/**
	 * UserStory# Description: Checks if a device is eligible for tradein
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "deviceTradeIn", Group.SHOP, Group.APIREG })
	public void testDeviceEligibleForTradeIn(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Checks if a device is eligible for tradein");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Checks if a device is eligible for tradein");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_DEVICETRADEIN + "getDeviceEligibilityForTradeinV3API.txt");
		String operationName = "getDeviceEligibilityForTradeinAPI";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = checkDeviceEligibleForTradeIn(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(getPathVal(jsonNode, "isEligible") != null);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# Description: Gets questions regarding the condition of the device
	 * based on carrier/make/model
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "deviceTradeIn", Group.SHOP, Group.APIREG })
	public void testGetQuestionAndAnswersOfDevice(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Gets questions regarding the condition of the device based on carrier/make/model");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Operation will return question and answers for requested device. ");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "getQuestionAndAnswersOfDevice";
		Response response = getQuestionAndAnswersOfDevice(apiTestData, "", tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# Description: Gets Trade in devices
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "deviceTradeIn", Group.SHOP, Group.APIREG })
	public void testGetTradeInDevices(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Gets Trade in devices");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Operation will return Trade in devices list for a msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Response response = getTradeInDevices(apiTestData, "", tokenMap);
		String operationName = "getTradeInDevices";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			JSONObject jsonObject = new JSONObject(getJSONfromResponse(response));
			Assert.assertNotNull(jsonObject.get("devices"));
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# Description: Gets shipping label based on RMA number
	 * 
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "deviceTradeIn", Group.SHOP, Group.APIREG })
	public void testGetShippingLabelBasedOnRMANumber(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Gets shipping lable based on rmaNumber");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Operaion will return shipping lable based on requested RMA Number");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "getShippingLabelBasedOnRMANumber";
		Response response = getShippingLabelBasedOnRMANumber(apiTestData, "", tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# Description: Create Quote TradeIN
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "deviceTradeIn", Group.SHOP, Group.APIREG })
	public void testDevicecreateQuoteTradeIn(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Create Quote TradeIN");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Checks if a device is eligible for create quote tradein");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		createQuoteTradeIn(apiTestData);
	}

	private Map<String, String> createQuoteTradeIn(ApiTestData apiTestData)
			throws Exception, IOException, JsonProcessingException {
		String operationName = "createQuoteTradeIn";
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_QUOTE + "createQuoteTradeInV3.txt");

		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("email", apiTestData.getMsisdn() + "@yoipmail.com");
		// tokenMap.put("lineMsisdn", apiTestData.getMsisdn());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = createQuoteTradeIn(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if ("999".equalsIgnoreCase(getPathVal(jsonNode, "statusCode"))) {
					failAndLogResponse(response, operationName);
				} else {
					tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
					JSONObject jsonObject = new JSONObject(getJSONfromResponse(response));
					Assert.assertNotNull(jsonObject.get("device"));
					Assert.assertNotNull(jsonObject.get("quoteId"));
				}
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		return tokenMap;
	}

	/**
	 * UserStory# Description: Create Quote TradeIN
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "deviceTradeIn", Group.SHOP, Group.APIREG })
	public void testDevicesubmitQuoteTradeIn(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: submit Quote TradeIN");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Checks if a device is eligible for submit quote tradein");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		// tokenMap = new HashMap<String, String>();
		tokenMap = createQuoteTradeIn(apiTestData);

		String operationName = "submitQuoteTradeIn";
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_QUOTE + "submitQuoteTradeInV3.txt");
		// tokenMap.put("lineMsisdn", apiTestData.getMsisdn());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = submitQuoteTradeIn(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if ("999".equalsIgnoreCase(getPathVal(jsonNode, "statusCode"))) {
					failAndLogResponse(response, operationName);
				}
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# Description: This service gets the list of devices associated with
	 * each line on the account that are eligible for a trade-in during a qualifying
	 * transaction.
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "deviceTradeIn", Group.SHOP, Group.PENDING })
	public void testGetAccountDeviceListEligibleForTradeIn(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log(
				"TestName: This service gets the list of devices associated with each line on the account that are eligible for a trade-in during a qualifying transaction.");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Step 1: Will return list of associated devices with each line on the account that are eligible for trade-in.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_DEVICETRADEIN + "getAccountDeviceListForTradeinV3API.txt");
		String operationName = "getAccountDeviceListForTradeinAPI";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getAccountDeviceListEligibleForTradeIn(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			JSONObject jsonObject = new JSONObject(getJSONfromResponse(response));
			Assert.assertNotNull(jsonObject.get("accountDevices"));

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * UserStory# Description: get LookUp Data For TradeInFlow
	 *
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 ***/
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "deviceTradeIn", Group.SHOP, Group.APIREG })
	public void testgetLookUpDataForTradeInFlow(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: get LookUp Data For TradeInFlow");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Verify eligible carrier names in response");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "getLookUpDataForTradeInFlow";
		Response response = getLookUpDataForTradeInFlow(apiTestData, "Carriers", tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			String carrierNames = response.path("codes[0].carriers[0].carrierName");
			Assert.assertNotNull(carrierNames);
		} else {
			failAndLogResponse(response, operationName);
		}

	}
	
	/**
	 * UserStory# Description: CDCSM-389 (https://jirasw.t-mobile.com/browse/CDCSM-389)
	 *
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 ***/
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "deviceTradeIn", Group.SHOP, Group.SPRINT })
	public void testAccountDevicesToVerifyEligibility(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testAccountDevicesToVerifyEligibility");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: /accountDevices response needs to include following attribute under 'device' node in response");
		Reporter.log("\"eligibilty\":{ \"isEligible\": true, \"inEligibiltyReasonCode\": \"string\", \"inEligibiltyMessage\": \"string\" }");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_DEVICETRADEIN + "getAccountDeviceListForTradeinV3API2.txt");
		String operationName = "getAccountDeviceListForTradeinAPI";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		logRequest(requestBody, operationName);
		Response response = getAccountDeviceListEligibleForTradeIn(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			
			jsonPath = new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("accountDevices[0].device.eligibilty.isEligible"), "Is Eligible field is present");
			//Assert.assertNotNull(jsonPath.get("accountDevices[0].statusCode"), "Reason code is present");
			//Assert.assertNotNull(jsonPath.get("accountDevices[0].statusMessage"), "Reason message is present");
			
		} else {
			failAndLogResponse(response, operationName);
		}
	}
		
	/**
	 * UserStory# Description: CDCSM-389 (https://jirasw.t-mobile.com/browse/CDCSM-389)
	 *
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 ***/
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "deviceTradeIn", Group.SHOP, Group.SPRINT })
	public void testAccountDevicesToVerifyInEligibility(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testAccountDevicesToVerifyEligibility");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: /accountDevices response needs to include following attribute under 'device' node in response");
		Reporter.log("\"eligibilty\":{ \"isEligible\": false, \"inEligibiltyReasonCode\": \"string\", \"inEligibiltyMessage\": \"string\" }");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_DEVICETRADEIN +"getAccountDeviceListForTradeinV3APIInEligibility.txt");
		String operationName = "getAccountDeviceListForTradeinAPI";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		logRequest(requestBody, operationName);
		Response response = getAccountDeviceListEligibleForTradeIn(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			
			jsonPath = new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("accountDevices[0].device.eligibilty.isEligible"), "Is Eligible field is present");
			Assert.assertNotNull(jsonPath.get("accountDevices[0].device.eligibilty.inEligibiltyReasonCode"), "In Eligibility Reason code is present");
			Assert.assertNotNull(jsonPath.get("accountDevices[0].device.eligibilty.inEligibiltyMessage"), "In Eligibility Reason message is present");
			
		} else {
			failAndLogResponse(response, operationName);
		}
	}

}
