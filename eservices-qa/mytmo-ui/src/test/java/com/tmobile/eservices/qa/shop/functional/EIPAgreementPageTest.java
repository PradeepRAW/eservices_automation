package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class EIPAgreementPageTest extends ShopCommonLib {

	/**
	 * US218280: Accessories : Device on EIP and Accessories on EIP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testAccessoryEipAgreement(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US218280:Accessories : Device on EIP and Accessories on EIP");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select Monthly payments | Monthly payments is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line |  Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button |  Insurance migration page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("12. Select accessory | Accessory PDP page should be displayed");
		Reporter.log("13. Select monthly payment | Monthly payment is selected");
		Reporter.log("14. Click Continue button | Cart page should be displayed");
		Reporter.log("15. Click Continue to shipping button | Shipping tab should be displayed");
		Reporter.log("16. Select Shipping option and Click Continue to Payments button"
				+ " | Payments tab should be displayed");
		Reporter.log("17. Enter Credit card details and Click Accept and Continue button"
				+ " | EIP Agreement should be displayed");
		Reporter.log("================================");

		AccessoryPLPPage accessoryPLPPage = navigateToAccessoryPLPPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		accessoryPLPPage.verifyAccessoryPLPPage();

		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickOnPayMonthly();
		accessoryPLPPage.clickContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterCardHoldersName(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		// In progress , blocked due to DE169066
	}

	/**
	 * US263100: Accessories : Device on FRP and Accessories on EIP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testDueMonthlyTotalDeviceFRPAccessoryEIPAgreement(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US263100:Accessories : Device on FRP and Accessories on EIP");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select FRP | FRP is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line |  Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button |  Insurance migration page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("12. Select accessory | Accessory PDP page should be displayed");
		Reporter.log("13. Select monthly payment | Monthly payment is selected. Store monthly amount in var A1");
		Reporter.log("14. Click Continue button | Cart page should be displayed");
		Reporter.log("15. Click Continue to shipping button | Shipping tab should be displayed");
		Reporter.log("16. Select Shipping option and Click Continue to Payments button"
				+ " | Payments tab should be displayed");
		Reporter.log("17. Enter Credit card details and Click Accept and Continue button"
				+ " | EIP agreement for accessories  should be displayed");
		Reporter.log("18. verify EIP document captures accessories financed via EIP."
				+ " | EIP document should captures accessories financed via EIP");
		Reporter.log("================================");

		AccessoryPLPPage accessoryPLPPage = navigateToAccessoryPLPPageBySeeAllPhonesWithTradeInFlow(myTmoData);
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.verifyStickyBanner();
		accessoryPLPPage.clickOnPayMonthly();
		accessoryPLPPage.clickAccessoriesContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterCardHoldersName(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		// Pending verification of Agreement due to defect -DE169066

	}

}
