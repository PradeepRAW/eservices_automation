package com.tmobile.eservices.qa.accounts.api.phone;

import java.util.HashMap;
import java.util.Map;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;

import com.tmobile.eservices.qa.api.eos.DeviceUnlockStatus;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;


public class GetDeviceunlockStatus extends DeviceUnlockStatus{
    
	
	public Map<String, String> tokenMap;
	
	/**
	 * 
	 * Verifying Device is temporarily unlocked and eligible for permanent unlock 
	 * 
	 * @param data
	 * @param apiTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,  groups = { Group.ACCOUNTS, Group.APIREG })
	public void getdeviceUnlockStatus_DeviceTemporarilyUnlockedAndEligibleForPermanentUnlock(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Device is temporarily unlocked and eligible for permanent unlock ");
		Reporter.log("Note : DeviceUnlockStatus Service calls genericPhone in UI ");
		Reporter.log("Test data : IMEI");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testgetdeviceUnlockStatus_DeviceTemporarilyUnlockedAndEligibleForPermanentUnlock";
		tokenMap = new HashMap<String, String>();

		tokenMap.put("imei", apiTestData.getiMEINumber());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceUnlockStatus.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getDeviceUnlockResponse(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			DeviceUnlockStatus device=new DeviceUnlockStatus();
			String alltags[]={"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus","deviceUnlockStatusList[0].deviceUnlockAttribute.unlockType"};
			for(String tag:alltags)	{
				device.checkjsontagitems(response, tag);
			}
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible", "true");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType", "PERMANENT");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus", "true");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.unlockType", "TEMPORARY");
		} else {
			failAndLogResponse(response, operationName);
		}	
	}
	

	
	/**
	 * 
	 * Verifying Device is temporarily unlocked and NOT eligible for permanent unlock. 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true,  groups = { Group.ACCOUNTS, Group.APIREG })
	public void getdeviceUnlockStatus_DeviceTemporarilyUnlockedAndNotEligibleForPermanentUnlock(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating Device is temporarily unlocked and NOT eligible for permanent unlock.");
		Reporter.log("Note : DeviceUnlockStatus Service calls genericPhone in UI ");
		Reporter.log("Test data : IMEI");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testgetdeviceUnlockStatus_DeviceTemporarilyUnlockedAndNotEligibleForPermanentUnlock";
		tokenMap = new HashMap<String, String>();

		tokenMap.put("imei", apiTestData.getiMEINumber());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceUnlockStatus.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getDeviceUnlockResponse(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			DeviceUnlockStatus device=new DeviceUnlockStatus();
			String alltags[]={"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus","deviceUnlockStatusList[0].deviceUnlockAttribute.unlockType"};
			for(String tag:alltags)	{
				device.checkjsontagitems(response, tag);
			}
			Map<String,String> expectedTagAndValue=new HashMap<String,String>();
			expectedTagAndValue.put("deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible","false");
			expectedTagAndValue.put("deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType","!PERMANENT");
			device.checkMutipleExpectedvalues(response,expectedTagAndValue);
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus", "true");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.unlockType", "TEMPORARY");
		} else {
			failAndLogResponse(response, operationName);
		}	
	}
	
	/**
	 * 
	 * Verifying Device is Permanently Unlocked. 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true,  groups = { Group.ACCOUNTS, Group.APIREG })
	public void getdeviceUnlockStatus_DevicePermanentlyUnlocked(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating the Device is Permanently Unlocked ");
		Reporter.log("Note : DeviceUnlockStatus Service calls genericPhone in UI ");
		Reporter.log("Test data : IMEI");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testgetdeviceUnlockStatus_DevicePermanentlyUnlocked";
		tokenMap = new HashMap<String, String>();

		tokenMap.put("imei", apiTestData.getiMEINumber());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceUnlockStatus.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getDeviceUnlockResponse(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			DeviceUnlockStatus device=new DeviceUnlockStatus();
			String alltags[]={"deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus","deviceUnlockStatusList[0].deviceUnlockAttribute.unlockType"};
			for(String tag:alltags)	{
				device.checkjsontagitems(response, tag);
			}
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus", "true");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.unlockType", "PERMANENT");
		} else {
			failAndLogResponse(response, operationName);
		}	
	}
	

	
	/**
	 * 
	 * Verifying the device Status Unknown 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true,  groups = { Group.ACCOUNTS, Group.APIREG })
	public void getdeviceUnlockStatus_DeviceStatusUnknown(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating the device Status Unknown");
		Reporter.log("Note : DeviceUnlockStatus Service calls genericPhone in UI ");
		Reporter.log("Test data : IMEI");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testgetdeviceUnlockStatus_DeviceStatusUnknown";
		tokenMap = new HashMap<String, String>();

		tokenMap.put("imei", apiTestData.getiMEINumber());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceUnlockStatus.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getDeviceUnlockResponse(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			DeviceUnlockStatus device=new DeviceUnlockStatus();
			String alltags[]={"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus","deviceUnlockStatusList[0].deviceUnlockAttribute.unlockType"};
			for(String tag:alltags)	{
				device.checkjsontagitems(response, tag);
			}
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus", "false");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.unlockType", "UNKNOWN");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible", "false");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType", "UNKNOWN");
			
		} else {
			failAndLogResponse(response, operationName);
		}	
	}
	
	
	/**
	 * 
	 * Verifying The device is locked and eligible for temporary unlock
	 * 
	 * @param data
	 * @param myTmoData
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true,  groups = { Group.ACCOUNTS, Group.APIREG })
	public void getdeviceUnlockStatus_DeviceLockedAndEligibleForTemporaryUnlock(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating The device is locked and eligible for temporary unlock");
		Reporter.log("Note : DeviceUnlockStatus Service calls genericPhone in UI ");
		Reporter.log("Test data : IMEI");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testgetdeviceUnlockStatus_DeviceLockedAndEligibleForTemporaryUnlock";
		tokenMap = new HashMap<String, String>();

		tokenMap.put("imei", apiTestData.getiMEINumber());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceUnlockStatus.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getDeviceUnlockResponse(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			DeviceUnlockStatus device=new DeviceUnlockStatus();
			String alltags[]={"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus","deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockableRemotely"};
			for(String tag:alltags)	{
				device.checkjsontagitems(response, tag);
			}
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible", "true");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType", "TEMPORARY");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockableRemotely", "true");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus", "false");
		} else {
			failAndLogResponse(response, operationName);
		}	
	}
	

	
	/**
	 * 
	 * Verifying The device is locked and eligible for permanent unlock and can be unlocked remotely
	 * 
	 * @param data
	 * @param myTmoData
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true,  groups = { Group.ACCOUNTS, Group.APIREG })
	public void getdeviceUnlockStatus_DeviceLockedAndEligibleForPermanentUnlockCanUnlockedRemotely(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating The device is locked and eligible for permanent unlock and can be unlocked remotely ");
		Reporter.log("Note : DeviceUnlockStatus Service calls genericPhone in UI ");
		Reporter.log("Test data : IMEI");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testgetdeviceUnlockStatus_DeviceLockedAndEligibleForPermanentUnlockCanUnlockedRemotely";
		tokenMap = new HashMap<String, String>();

		tokenMap.put("imei", apiTestData.getiMEINumber());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceUnlockStatus.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getDeviceUnlockResponse(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			DeviceUnlockStatus device=new DeviceUnlockStatus();
			String alltags[]={"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus","deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockableRemotely"};
			for(String tag:alltags)	{
				device.checkjsontagitems(response, tag);
			}
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible", "true");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType", "PERMANENT");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockableRemotely", "true");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus", "false");
		} else {
			failAndLogResponse(response, operationName);
		}	
	}
	

	/**
	 * 
	 * Verifying The device is locked and eligible for permanent unlock and can NOT be unlocked remotely
	 * 
	 * @param data
	 * @param myTmoData
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true,  groups = { Group.ACCOUNTS, Group.APIREG })
	public void getdeviceUnlockStatus_DeviceLockedAndEligibleForPermanentUnlockCanNOTUnlockedRemotely(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating The device is locked and eligible for permanent unlock and can NOT be unlocked remotely ");
		Reporter.log("Note : DeviceUnlockStatus Service calls genericPhone in UI ");
		Reporter.log("Test data : IMEI");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testgetdeviceUnlockStatus_DeviceLockedAndEligibleForPermanentUnlockCanNOTUnlockedRemotely";
		tokenMap = new HashMap<String, String>();

		tokenMap.put("imei", apiTestData.getiMEINumber());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceUnlockStatus.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getDeviceUnlockResponse(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			DeviceUnlockStatus device=new DeviceUnlockStatus();
			String alltags[]={"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus","deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockableRemotely"};
			for(String tag:alltags)	{
				device.checkjsontagitems(response, tag);
			}
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible", "true");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType", "PERMANENT");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockableRemotely", "false");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus", "false");
		} else {
			failAndLogResponse(response, operationName);
		}	
	}
	
	/**
	 * 
	 * Verifying The device is locked and not eligible for unlock
	 * 
	 * @param data
	 * @param myTmoData
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true,  groups = { Group.ACCOUNTS, Group.APIREG })
	public void getdeviceUnlockStatus_DeviceLockedAndNotEligibleForUnlock(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating The device is locked and not eligible for unlock ");
		Reporter.log("Note : DeviceUnlockStatus Service calls genericPhone in UI ");
		Reporter.log("Test data : IMEI");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testgetdeviceUnlockStatus_DeviceLockedAndNotEligibleForUnlock";
		tokenMap = new HashMap<String, String>();

		tokenMap.put("imei", apiTestData.getiMEINumber());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceUnlockStatus.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getDeviceUnlockResponse(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			DeviceUnlockStatus device=new DeviceUnlockStatus();
			String alltags[]={"deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible",
					"deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus"};
			for(String tag:alltags)	{
				device.checkjsontagitems(response, tag);
			}
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible", "false");
			device.checkexpectedvalues(response, "deviceUnlockStatusList[0].deviceUnlockAttribute.unlockStatus", "false");
		} else {
			failAndLogResponse(response, operationName);
		}	
	}
	
	
	
	
	
	
	
		
}

	/*
	 * // @Test(groups = "phone")
	 * 
	 * @Test(groups = "sssxc1") public void getdeviceUnlockStatus_Locked() {
	 * Response response = null;
	 * 
	 * Service getDeviceUnlockObj=new Service();
	 * response=getDeviceUnlockObj.getDeviceUnlockStatusApi();
	 * 
	 * int statusCode = response.getStatusCode();
	 * System.out.println("The status code recieved: " + statusCode);
	 * System.out.println(response.prettyPrint());
	 * 
	 * Reporter.log("Test Case : Validating DeviceUnlock_Locked Operation Response "
	 * ); Reporter.
	 * log("Note : DeviceUnlockStatus Service Internally calls getDeviceUnlockAttribute "
	 * ); Reporter.log("Test data : IMEI");
	 * 
	 * // System.out.println("Response body: " + response.asString()); if
	 * (response.body().asString() != null && response.getStatusCode() == 200) {
	 * 
	 * System.out.println("output Success");
	 * 
	 * Assert.assertTrue(response.jsonPath().getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.unlockStatus")
	 * .contains("false")); Assert.assertTrue(response.jsonPath().getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.unlockType")
	 * .contains("LOCKED")); Assert.assertTrue(response.jsonPath() .getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType")
	 * .contains("UNKNOWN")); Assert.assertTrue(response.jsonPath() .getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible")
	 * .contains("false")); Assert.assertTrue(response.jsonPath() .getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockableRemotely")
	 * .contains("true"));
	 * 
	 * } else { System.out.println("output Failure");
	 * Assert.fail("invalid response"); } }
	 * 
	 * @Test(groups = "phone") public void getdeviceUnlockStatus_Unknown() {
	 * 
	 * final String Object = null; final List res = null; Response response = null;
	 * 
	 * // public static void main(String args[]) throws Exception {
	 * 
	 * environment = System.getProperty("environment"); RestAssured.baseURI =
	 * environment;
	 * 
	 * RequestSpecification request = RestAssured.given();
	 * 
	 * request.header("Authorization", "xiS7vL6vfy8A8dhiY0TIaMZqoc1X");
	 * request.header("interactionId", "123456787"); request.header("sender_id",
	 * "8888888888"); request.header("application_id", "ESERVICE");
	 * request.header("channel_id", "WEB"); request.contentType("application/json");
	 * 
	 * JSONObject requestParams = new JSONObject(); requestParams.put("imei",
	 * "355431075230159");
	 * 
	 * // System.out.println(requestParams.toString());
	 * 
	 * request.body(requestParams.toString()); response =
	 * request.post("/myPhone/deviceUnlockStatus");
	 * 
	 * int statusCode = response.getStatusCode();
	 * System.out.println("The status code recieved: " + statusCode);
	 * System.out.println(response.prettyPrint());
	 * 
	 * Reporter.log("Test Case : Validating DeviceUnlock_Locked Operation Response "
	 * ); Reporter.
	 * log("Note : DeviceUnlockStatus Service Internally calls getDeviceUnlockAttribute "
	 * ); Reporter.log("Test data : IMEI");
	 * 
	 * // System.out.println("Response body: " + response.asString()); if
	 * (response.body().asString() != null && response.getStatusCode() == 200) {
	 * 
	 * System.out.println("output Success");
	 * 
	 * Assert.assertTrue(response.jsonPath().getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.unlockStatus")
	 * .contains("false")); Assert.assertTrue(response.jsonPath().getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.unlockType")
	 * .contains("UNKNOWN")); Assert.assertTrue(response.jsonPath().getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType")
	 * .contains("UNKNOWN")); Assert.assertTrue(response.jsonPath().getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible")
	 * .contains("false")); Assert.assertTrue(response.jsonPath().getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockableRemotely")
	 * .contains("false")); } else { System.out.println("output Failure");
	 * Assert.fail("invalid response"); } }
	 * 
	 * @Test(groups = "phone") public void getdeviceUnlockStatus_Temporary() {
	 * 
	 * final String Object = null; final List res = null; Response response = null;
	 * 
	 * environment = System.getProperty("environment"); RestAssured.baseURI =
	 * environment;
	 * 
	 * RequestSpecification request = RestAssured.given();
	 * 
	 * request.header("Authorization", "xiS7vL6vfy8A8dhiY0TIaMZqoc1X");
	 * request.header("interactionId", "123456787"); request.header("sender_id",
	 * "8888888888"); request.header("application_id", "ESERVICE");
	 * request.header("channel_id", "WEB"); request.contentType("application/json");
	 * 
	 * JSONObject requestParams = new JSONObject(); requestParams.put("imei",
	 * "865208041427165");
	 * 
	 * // System.out.println(requestParams.toString());
	 * 
	 * request.body(requestParams.toString());
	 * 
	 * request.trustStore(System.getProperty("user.dir") +
	 * "/src/test/resources/cacerts", "changeit"); response =
	 * request.post("/myPhone/deviceUnlockStatus");
	 * 
	 * int statusCode = response.getStatusCode();
	 * System.out.println("The status code recieved: " + statusCode);
	 * System.out.println(response.prettyPrint());
	 * 
	 * Reporter.log("Test Case : Validating DeviceUnlock_Locked Operation Response "
	 * ); Reporter.
	 * log("Note : DeviceUnlockStatus Service Internally calls getDeviceUnlockAttribute "
	 * ); Reporter.log("Test data : IMEI");
	 * 
	 * // System.out.println("Response body: " + response.asString()); if
	 * (response.body().asString() != null && response.getStatusCode() == 200) {
	 * 
	 * System.out.println("output Success");
	 * 
	 * Assert.assertTrue(response.jsonPath().getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.unlockStatus")
	 * .contains("true")); Assert.assertTrue(response.jsonPath().getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.unlockType")
	 * .contains("TEMPORARY")); Assert.assertTrue(response.jsonPath().getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligibleType")
	 * .contains("UNKNOWN")); Assert.assertTrue(response.jsonPath().getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockEligible")
	 * .contains("false")); Assert.assertTrue(response.jsonPath().getString(
	 * "deviceUnlockStatusList.deviceUnlockAttribute.deviceUnlockEligibilityDetails.unlockableRemotely")
	 * .contains("true")); } else { System.out.println("output Failure");
	 * Assert.fail("invalid response"); } }
	 */
	 

