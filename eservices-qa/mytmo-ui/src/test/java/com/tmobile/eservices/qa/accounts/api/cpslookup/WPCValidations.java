package com.tmobile.eservices.qa.accounts.api.cpslookup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;

public class WPCValidations {
	public List<String> getallmisdins(String excel) {
		List<String> misdinpassword = new ArrayList<>();
		try {

			Workbook workbook;
			FileInputStream excelFile = new FileInputStream(new File(excel));
			if (excel.contains("xlsx")) {
				workbook = new XSSFWorkbook(excelFile);
			} else {
				workbook = new HSSFWorkbook(excelFile);
			}

			Sheet datatypeSheet = workbook.getSheetAt(0);

			Iterator<Row> iterator = datatypeSheet.iterator();

			while (iterator.hasNext()) {

				Row currentRow = iterator.next();

				if (currentRow.getCell(0) != null && currentRow.getCell(1) != null && currentRow.getCell(2) != null) {

					String WPCSOC = currentRow.getCell(0).getStringCellValue();
					//String pwd = currentRow.getCell(1).getStringCellValue();
					String WPCvalue = currentRow.getCell(2).getStringCellValue();

					if (!WPCSOC.trim().equals("")) {
						if (!WPCSOC.equalsIgnoreCase("loginEmailOrPhone")) {

							misdinpassword.add(WPCSOC.trim() + "/"  + "/" + WPCvalue.trim());

						}
					}
				}
			}

			excelFile.close();
			workbook.close();
		} catch (Exception e) {

		}
		return misdinpassword;
	}

	@Test( groups = "wpc")
	public void getFile() throws MalformedURLException, IOException {
		String FILE_NAME = System.getProperty("user.dir") + "/src/test/resources/testdata/WPC1.xlsx";
		List<String> misdinpawords = getallmisdins(FILE_NAME);
		Iterator<String> iterator = misdinpawords.iterator();
		while (iterator.hasNext()) {
			
			String misdpwd = iterator.next();
			String misdin = misdpwd.split("/")[0];
			String pwd = misdpwd.split("/")[1];
			String para3 = misdpwd.split("/")[2];
			String url = "http://commerce.eservice.t-mobile.com/wcs/resources/store/10154/tmobile/product/bySearchTerm/"
					+ misdin + "?searchType=ANY&locale=en_US";
			String query1 = "$.CatalogEntryView[*].xcatentry_OptionalServiceConfigurationType";
			//String query2 = "$.CatalogEntryView[*].xcatentry_eServiceOptionalServiceConfigurationType";
			
			
			List<String>	tstdata1 =new ArrayList<String>();
			try{
				tstdata1 = JsonPath.parse(new URL(url)).read(query1);
		//	List<String> tstdata2 = JsonPath.parse(new URL(url)).read(query2);
			
			
			if (tstdata1.contains(para3)) {
				
				System.out.println("pass    "   + misdin +"         "+ tstdata1.toString() +"         " +para3);
				Reporter.log("pass    "   + misdin +"-------------"+ tstdata1.toString() +" ------------------- " +para3);
			} else {
				System.out.println("fail    "   + misdin +"         "+ tstdata1.toString() +"         " +para3);
				Reporter.log("");
				Reporter.log("");
				Reporter.log("fail    "   + misdin +"-------------"+ tstdata1.toString() +"-------------	" +para3);
				
			}
		
			}
			catch(Exception e)
			
			{
				
				Reporter.log("");
				Reporter.log(misdin + "    WCS page not loaded");
			}
		
	}
	
}
	
}