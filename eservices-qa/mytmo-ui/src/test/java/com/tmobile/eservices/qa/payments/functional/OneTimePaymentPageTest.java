package com.tmobile.eservices.qa.payments.functional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.exception.FrameworkException;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.AddBankPage;
import com.tmobile.eservices.qa.pages.payments.AddCardPage;
import com.tmobile.eservices.qa.pages.payments.AutoPayPage;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.OTPAmountPage;
import com.tmobile.eservices.qa.pages.payments.OTPConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.PaymentArrangementPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAlonePLPPage;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAloneReviewCartPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class OneTimePaymentPageTest extends PaymentCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(OTPConfirmationPageTest.class);

	/**
	 * Verify AutoPay information (when AP is enabled) shows correctly as an alert
	 * on the one-time payment page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyAutopayAlertonOneTimepayment(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAlertsonOneTimepayment method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Verify AutoPay information (when AP is enabled) shows correctly as an alert on the one-time payment page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Verify Alert is loaded| Alerts should be displayed.");
		Reporter.log("================================");

		Reporter.log("Actual Results:");
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyOtpPageAlerts();
	}

	/**
	 * champs-payments US142445 Sedona - OTP - BAN on Bank Account and Card
	 * 
	 * US207914 [R] Payment Module H&S - BAN on Bank and Card Modal modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "to be fixed" })
	public void testBanOnBankAccountandCardModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify ban on payment| Payment BAN message should be displayed");
		Reporter.log("Step 4: Click Ok button | Home page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyBanMessage();
		oneTimePaymentPage.ClickOKbutton();
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
	}

	/**
	 * champs-11 US165967 Sedona - OTP - Landing Page - Blade Error States US373729
	 * : [PHASE 2] UI - PCM landing page Error blade "You must add a payment method
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.SEDONA })
	public void testSedonaOTPLandingPageBladeErrorStates(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US165967 Sedona - OTP - Landing Page - Blade Error States");
		Reporter.log("US373729 : [PHASE 2] UI - PCM landing page Error blade \"You must add a payment method");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a TMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to one time payment page | one time payment page should be displayed");
		Reporter.log(
				"Step 4: Click on Agree and submit with out entering payment method | Error message for payment method should be displayed");
		Reporter.log(
				"Step 5: Click on Agree and submit with out entering Amount| Error message for Enter amount should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.agreeAndSubmit();
		oneTimePaymentPage.verifyInlineErrorMsgs("You must add a payment method");
		// oneTimePaymentPage.verifyInlineErrorMsgs("Enter Amount");
	}

	/**
	 * US157269 Champs Sprint 9 - Sedona - OTP - Landing Page - Payment Amount Cache
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.SEDONA })
	public void testSedonaOTPLandingPagePaymentAmountClearCache(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to OTP landing page | OTP landing page should be displayed");
		Reporter.log("Step 4: Click on amount icon | Amount spoke page should be displayed");
		Reporter.log("Step 5: Set other amount and refresh the page| OTP landing page should be displayed");
		Reporter.log("Step 5: Click on amount icon and verify other amount| Other amount should be cleared");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		editAmountPage.clickCancelBtn();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyOtherAmountCacheCleared();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testOTPRefreshPageTreatment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US193341 :: Test Sedona OTP Refresh Page Treatment ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Click on Add Payment -> Add Card field | Card details page should be loaded");
		Reporter.log(
				"Step 5: Hit the F5 (page refresh) key | The user should be navigated back to the OTP landing page 'Pay Your Bill'");
		Reporter.log("Step 6: Click on Add Payment -> Add Bank field | Bank details page should be loaded");
		Reporter.log(
				"Step 7: Hit the F5 (page refresh) key | The user should be navigated back to the OTP landing page 'Pay Your Bill'");
		Reporter.log("================================");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.refreshPage();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		oneTimePaymentPage.refreshPage();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddBank();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		addBankPage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);
		oneTimePaymentPage.refreshPage();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickTermsAndConditions();
		oneTimePaymentPage.verifyTermsAndConditions();
		oneTimePaymentPage.refreshPage();
		oneTimePaymentPage.verifyPageLoaded();
	}

	/**
	 * Ensure payment confirmation page shows correct details
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyPaymentconfirmationPagewithCorrectdetails(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPaymentconfirmationPagewithCorrectdetails method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Ensure payment confirmation page shows correct details");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Select Radio button Stored credit Card | Radio button should be selected ");
		Reporter.log("6. Click next | Review payment page should be loaded");
		Reporter.log("7. Verify Credit Card account number | Credit card number should be displayed correctly");
		Reporter.log("8. Verify Zip code | Zip code will be displayed correctly");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.fillCardInfo(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(myTmoData.getPayment().getCardNumber());
	}

	/**
	 * US283063 GLUE Light Reskin - OTP Landing Page
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Regular user. PA eligible
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testVerifyAllElementsonOTPLandingPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262823	Angular + GLUE - OTP - Landing Page - Payment Date Blade (BAU)");
		Reporter.log(Constants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: verify date blade | Date blade should be displayed");
		Reporter.log("Step 5: verify Amount blade | Amount blade should be displayed");
		Reporter.log("Step 6: verify PaymentMethod blade | PaymentMethod blade should be displayed");
		Reporter.log("Step 7: verify Payment Arrangement link | Payment Arrangement link should be displayed");
		Reporter.log("Step 8: click terms and conditions | terms and conditions should be displayed");
		Reporter.log(
				"Step 9: verify Agree And Submit Button Enabled OTPPage| Agree And Submit Button should be Enabled OTP Page");
		Reporter.log("Step 10: Click on cancel button on one time payment page |cancal model popup should be loaded");
		Reporter.log("Step 11: Click on continue button| home page should be displayed");
		Reporter.log("================================");
		Reporter.log(Constants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyDateBlade();
		oneTimePaymentPage.verifyAmountBlade();
		oneTimePaymentPage.verifyPaymentMethodBlade();
		oneTimePaymentPage.verifyPaymentArrangementLink();
		oneTimePaymentPage.clickTermsAndConditions();
		oneTimePaymentPage.verifyTermsandConditions();
		oneTimePaymentPage.clickTermsAndConditionsBackButton();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.isContinueBtnDisplayedOnModal();
		oneTimePaymentPage.verifyCancelMsg("Are you sure you want to leave this page?");
		oneTimePaymentPage.clickBackBtn();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.clickContinueBtnModal();
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();

	}

	/**
	 * champs-payments US142491 Sedona - OTP - Landing Page - Cancel Button Modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testOTPLandingPageCancelButtonModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US165967 Sedona - OTP - Landing Page - Blade Error States");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a TMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to one time payment page | one time payment page should be displayed");
		Reporter.log("Step 4: Click on cancel button on one time payment page |cancal model popup should be loaded");
		Reporter.log("Step 5: Click on continue button| home page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.isContinueBtnDisplayedOnModal();
		oneTimePaymentPage.verifyCancelMsg("Are you sure you want to cancel this transaction");
		oneTimePaymentPage.clickBackBtn();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.clickContinueBtnModal();
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
	}

	/**
	 * US283068 GLUE Light Reskin - OTP Terms and Conditions Page
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Condition - Regular user with stored payment method.Autopay
	 *            OFF
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testGLROTPTermsConditionsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US283068	GLUE Light Reskin - OTP Terms and Conditions Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Terms and conditions link | Terms and conditions page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickTermsAndConditions();
		oneTimePaymentPage.verifyTermsAndConditions();
		oneTimePaymentPage.clickTermsAndConditionsBackButton();
		oneTimePaymentPage.verifyPageLoaded();
	}

	/**
	 * US284839 GLUE Light Reskin - Exit OTP Modal
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Regular user with stored payment method.Autopay
	 *            OFF
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testGLRExitOTPModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US284839	GLUE Light Reskin - Exit OTP Modal");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Click on Cancel button | Cancel modal should be displayed:");
		Reporter.log(
				"Step 4.1 Verify Are you sure you want to cancel this transaction? Any information on this screen will not be saved. is present");
		Reporter.log("Step 4.2 Verify Continue CTA is displayed");
		Reporter.log("Step 4.3 Verify Back CTA is present");
		Reporter.log("Step 5. Click Back CTA | Verify OTP landing page is displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.verifyCancelOTPModal();
		oneTimePaymentPage.clickbackExitOTPBtn();
		oneTimePaymentPage.verifyPageLoaded();
	}

	// ######################################

	/**
	 * US280305 NG4 Page Loading Spinner
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testVerifySpinnerOnOneTimePaymentPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testVerifySpinnerOnOneTimePaymentPage method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log(
				"3. hit the otp url and verify spinner on one time payment page | spinner should be present until the page loads completely");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifySpinnerpresentuntilOTPPageLoads();

	}

	// chillana's stories sprint 4

	// PA scheduled data with missed payment
	/**
	 * US242105
	 * 
	 * OTP CX Enhancements - Missed PA - OTP Landing Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testOTPCXEnhancementsMissedPAOTPLandingPAge(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testOTPCXEnhancementsMissedPAOTPLandingPAge method called in ErrorHandlingModuleTest");
		Reporter.log("testOTPCXEnhancementsMissedPAOTPLandingPAge method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on amount icon| amount spoke page should be displayed");
		Reporter.log(
				"5. set other amount less than missed payment |alert for amount less than missed payment should be displayed");
		Reporter.log(
				"6. set other amount greater than missed payment |alert for amount greater than missed payment should be displayed");

		Reporter.log("7. click on review payments made link |alert and activities page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);

		editAmountPage.clickAmountRadioButton("Other");
		// Assert.assertTrue(oneTimePaymentPage.verifyotherAmountPopulatedAmountSpokePage(oneTimePaymentPage.getBalanceDueAmount()),
		// "other amount polpulated is not matched with installement due");
		double balance = Double.parseDouble(editAmountPage.getBalanceDueAmount());
		editAmountPage.setOtherAmount("5");
		editAmountPage.clickUpdateAmount();

		oneTimePaymentPage.verifyAlertForAmountLessThanMissedInstallement();
		oneTimePaymentPage.clickamountIcon();
		editAmountPage.verifyPageLoaded();

		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(Double.toString(balance));
		editAmountPage.clickUpdateAmount();

		oneTimePaymentPage.verifyAlertForAmountGreaterThanMissedInstallement();

		oneTimePaymentPage.clickReviewPaymentsMade();
		AccountHistoryPage alertsActivitiesPage = new AccountHistoryPage(getDriver());
		alertsActivitiesPage.verifyPageLoaded();

	}

	// PA scheduled data with unsecured payment
	/**
	 *
	 * US242110
	 * 
	 * OTP CX Enhancements - Unsecured PA - OTP Landing Page (Inside blackout)
	 * 
	 * US272291
	 * 
	 * OTP CX Enhancements - Unsecured PA - OTP Edit Amount Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testOTPCXEnhancementsUnSecuredPAInsideBlackOut(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testOTPCXEnhancementsUnSecuredPAInsideBlackOut method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on amount icon| amount spoke page should be displayed");
		Reporter.log(
				"5. set other amount less than installement due  |alert for amount less than installement due  should be displayed");
		Reporter.log(
				"6. set other amount greater than installement due |alert for amount greater than installement due  should be displayed");

		Reporter.log("7. click on review previous  payments  link |alert and activities page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);
		editAmountPage.clickAmountRadioButton("Other");

		// Assert.assertTrue(oneTimePaymentPage.verifyotherAmountPopulatedAmountSpokePage(oneTimePaymentPage.getBalanceDueAmount()),
		// "other amount polpulated is not matched with installement due");
		double balance = Double.parseDouble(editAmountPage.getBalanceDueAmount());

		editAmountPage.setOtherAmount("3");
		editAmountPage.clickUpdateAmount();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyAlertForAmountLessThanInstallementDueInsideBlackoutPeriod();
		oneTimePaymentPage.clickamountIcon();
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(Double.toString(balance + 1));
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyAlertForAmountGreaterThanInstallementDueInsideBlackoutPeriod();
		oneTimePaymentPage.clickReviewPreviouspayments();
		AccountHistoryPage alertsActivitiesPage = new AccountHistoryPage(getDriver());
		alertsActivitiesPage.verifyPageLoaded();

	}

	/**
	 *
	 * US272292
	 * 
	 * OTP CX Enhancements - Unsecured PA - OTP Confirmation Page
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testOTPCXEnhancementsOTPconfirmationPageOutsideBlackoutPeriod(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"testOTPCXEnhancementsOTPconfirmationPageOutsideBlackoutPeriod method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on amount icon| amount spoke page should be displayed");
		Reporter.log("5. enter amount greater than installement and submit|otp confirmation page should be  updated");
		Reporter.log(
				"6.verify alert for amount greater than installement | alert for amount greater than installement should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);
		editAmountPage.clickAmountRadioButton("Other");

		double balance = Double.parseDouble(editAmountPage.getBalanceDueAmount());

		editAmountPage.setOtherAmount(Double.toString(balance + 1));
		editAmountPage.clickUpdateAmount();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage onetimepaymentconfirmationpage = new OTPConfirmationPage(getDriver());
		onetimepaymentconfirmationpage.verifyPageLoaded();
		oneTimePaymentPage.clickOnShowdetails();
		onetimepaymentconfirmationpage.verifyAlertForAmountGreaterThanInstallement();

	}

	/**
	 *
	 * US272292
	 * 
	 * OTP CX Enhancements - Unsecured PA - OTP Confirmation Page
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testOTPCXEnhancementsOTPconfirmationPageOutsideBlackoutPeriodLessthanInstallement(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"testOTPCXEnhancementsOTPconfirmationPageOutsideBlackoutPeriodLessthanInstallement method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on amount icon| amount spoke page should be displayed");
		Reporter.log("5. enter amount less than installement and submit|otp confirmation page should be  updated");
		Reporter.log(
				"6.verify alert for amount less than installement | alert for amount less than installement should be displayed");
		Reporter.log("7.click on review previous payments | alert and activities page  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);
		editAmountPage.clickAmountRadioButton("Other");
		double balance = Double.parseDouble(editAmountPage.getBalanceDueAmount());
		editAmountPage.setOtherAmount(Double.toString(balance - 1));
		editAmountPage.clickUpdateAmount();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage onetimepaymentconfirmationpage = new OTPConfirmationPage(getDriver());
		onetimepaymentconfirmationpage.verifyPageLoaded();
		oneTimePaymentPage.clickOnShowdetails();
		Reporter.log("otp confirmation page is displayed");
		onetimepaymentconfirmationpage.verifyAlertForAmountLessThanInstallement();
		oneTimePaymentPage.clickReviewPreviouspayments();
		AccountHistoryPage alertsActivitiesPage = new AccountHistoryPage(getDriver());
		alertsActivitiesPage.verifyPageLoaded();
		Reporter.log("alert and activities page  is displayed");

	}

	/**
	 *
	 * US242115
	 * 
	 * OTP CX Enhancements - ALL PAs - OTP Confirmation Page
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testOTPCXEnhancementsAllPaOTPconfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testOTPCXEnhancementsOTPconfirmationPageOutsideBlackoutPeriodLessthanInstallement method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on amount icon| amount spoke page should be displayed");
		Reporter.log("5. select total balance and submit|otp confirmation page should be  updated");
		Reporter.log(
				"6.verify PA closing alert on otp confirmation page |  PA closing alert on otp confirmation page should be displayed");
		Reporter.log("7.click on enroll autopay | autopay landing page  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);
		editAmountPage.clickAmountRadioButton("Other");

		double balance = Double.parseDouble(editAmountPage.getBalanceDueAmount());

		editAmountPage.setOtherAmount(Double.toString(balance));
		editAmountPage.clickUpdateAmount();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage onetimepaymentconfirmationpage = new OTPConfirmationPage(getDriver());
		onetimepaymentconfirmationpage.verifyPageLoaded();
		oneTimePaymentPage.clickOnShowdetails();

		Reporter.log("otp confirmation page is displayed");
		onetimepaymentconfirmationpage.verifyPAClosingAlert();

		onetimepaymentconfirmationpage.clickonEnrollAutopay();
		AutoPayPage autopaylandingpage = new AutoPayPage(getDriver());

		autopaylandingpage.verifyNewAutoPayLandingPage();
		Reporter.log("autopay landing pagepage is displayed");

	}

	// PA scheduled data with unsecured payment
	/**
	 *
	 * US263337
	 * 
	 * OTP CX Enhancements - Unsecured PA - OTP Landing Page (Outside blackout)
	 * 
	 * US272291
	 * 
	 * OTP CX Enhancements - Unsecured PA - OTP Edit Amount Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testOTPCXEnhancementsUnSecuredPAOutsideBlackOut(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testOTPCXEnhancementsUnSecuredPAOutsideBlackOut method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on amount icon| amount spoke page should be displayed");
		Reporter.log(
				"5. set other amount less than installement due  |alert for amount less than installement due  should be displayed");
		Reporter.log("6. click on review previous  payments  link |alert and activities page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.verifyotherAmountIsPopulated(editAmountPage.getBalanceDueAmount());
		double balance = Double.parseDouble(editAmountPage.getBalanceDueAmount());
		editAmountPage.setOtherAmount(Double.toString(balance - 1));
		editAmountPage.clickUpdateAmount();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyAlertForAmountLessThanInstallementDueOutsideBlackoutPeriod();
		oneTimePaymentPage.clickReviewPreviouspayments();
		AccountHistoryPage alertsActivitiesPage = new AccountHistoryPage(getDriver());
		alertsActivitiesPage.verifyPageLoaded();

	}

	/**
	 *
	 * US272292
	 * 
	 * OTP CX Enhancements - Unsecured PA - OTP Confirmation Page
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testOTPCXEnhancementsOTPconfirmationPageInsideBlackoutPeriodGreaterThanInstallement(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testOTPCXEnhancementsOTPconfirmationPageInsideBlackoutPeriodGreaterThanInstallement method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on amount icon| amount spoke page should be displayed");
		Reporter.log("5. enter amount greater than installement and submit|otp confirmation page should be  updated");
		Reporter.log(
				"6.verify alert for amount greater than installement inside blackout period | alert for amount greater than installement inside blackout period  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);
		editAmountPage.clickAmountRadioButton("Other");
		double balance = Double.parseDouble(editAmountPage.getBalanceDueAmount());
		editAmountPage.setOtherAmount(Double.toString(balance + 1));
		editAmountPage.clickUpdateAmount();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage onetimepaymentconfirmationpage = new OTPConfirmationPage(getDriver());
		onetimepaymentconfirmationpage.verifyPageLoaded();
		oneTimePaymentPage.clickOnShowdetails();
		onetimepaymentconfirmationpage.verifyAlertForAmountGreaterThanInstallement();

	}

	/**
	 *
	 * US272292
	 * 
	 * OTP CX Enhancements - Unsecured PA - OTP Confirmation Page
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testOTPCXEnhancementsOTPconfirmationPageInsideBlackoutPeriodLessThanInstallement(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"testOTPCXEnhancementsOTPconfirmationPageInsideBlackoutPeriodLessThanInstallement method called in ErrorHandlingModuleTest");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4. click on amount icon| amount spoke page should be displayed");
		Reporter.log("5. enter amount less than installement and submit|otp confirmation page should be  updated");
		Reporter.log(
				"6.verify alert for amount less than installement inside blackout period | alert for amount less than installement inside blackout period  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OTPAmountPage editAmountPage = navigateToEditAmountPage(myTmoData);
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount("5");
		editAmountPage.clickUpdateAmount();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage onetimepaymentconfirmationpage = new OTPConfirmationPage(getDriver());
		onetimepaymentconfirmationpage.verifyPageLoaded();
		oneTimePaymentPage.clickOnShowdetails();
		onetimepaymentconfirmationpage.verifyAlertForAmountLessThanInstallementInsideBlackoutperiod();

	}

	/**
	 * US262836 Angular + GLUE - OTP - Landing Page - Header (Sedona)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SEDONA })
	public void testAngularGLUEOTPLandingPageHeaderSedona(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262836	Angular + GLUE - OTP - Landing Page - Header (Sedona)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log(
				"Step 4: verify Total Balance in the Header | Total Balance should be displayed in the Header as expected");
		Reporter.log("================================");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyTotalBalanceAmountInHeader();
		// TODO: verify Total Balance in the header as per the AC
	}

	/**
	 * US262836 Angular + GLUE - OTP - Landing Page - Header (Sedona)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEOTPLandingPageHeader(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262836	Angular + GLUE - OTP - Landing Page - Header (Sedona)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log(
				"Step 4: verify Total Balance in the Header | Total Balance should be displayed in the Header as expected");
		Reporter.log("================================");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyTotalBalanceAmountInHeader();
	}

	/**
	 * US262829 Angular + GLUE - OTP - Landing Page - Footer (BAU)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEOTPLandingPageFooter(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262829	Angular + GLUE - OTP - Landing Page - Footer (BAU)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Footer: T&C's link | TnC Link & Page should be displayed");
		Reporter.log("Step 5: Verify OTP Footer: Agree & Submit button | Agree & Submit button should be displayed");
		Reporter.log("Step 6: Verify OTP Footer: Cancel Button | Cancel Button should be displayed");
		Reporter.log("================================");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickTermsAndConditions();
		oneTimePaymentPage.verifyTermsandConditions();
		oneTimePaymentPage.clickTermsAndConditionsBackButton();
		oneTimePaymentPage.verifyAgreeAndSubmitButton();
		oneTimePaymentPage.verifyCancelBtnOtpPage();
	}

	/**
	 * US262829 Angular + GLUE - OTP - Landing Page - Footer (BAU)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SEDONA })
	public void testAngularGLUEOTPLandingPageFooterSedona(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262829	Angular + GLUE - OTP - Landing Page - Footer (BAU)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Footer: T&C's link | TnC Link & Page should be displayed");
		Reporter.log("Step 5: Verify OTP Footer: Agree & Submit button | Agree & Submit button should be displayed");
		Reporter.log("Step 6: Verify OTP Footer: Cancel Button | Cancel Button should be displayed");
		Reporter.log("Step 7: Verify OTP Footer: PA Link | Entry point to Payment Arrangement should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickTermsAndConditions();
		oneTimePaymentPage.verifyTermsandConditions();
		oneTimePaymentPage.clickTermsAndConditionsBackButton();
		oneTimePaymentPage.verifyAgreeAndSubmitButton();
		oneTimePaymentPage.verifyCancelBtnOtpPage();
		oneTimePaymentPage.verifyPaymentArrangementLink();
	}

	/**
	 * US262821 Angular + GLUE - OTP - Landing Page - Header
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEOTPLandingPageHeaderRegularUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262821	Angular + GLUE - OTP - Landing Page - Header");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyPayYourBillHeader();
	}

	/**
	 * US262821 Angular + GLUE - OTP - Landing Page - Header
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEOTPLandingPageHeaderBusinessUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262821	Angular + GLUE - OTP - Landing Page - Header");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyPayYourBillHeader();
	}

	/**
	 * US262833 Angular + GLUE - OTP - Landing Page - Cancel Button Modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEOTPPageCancelModalWithoutPastDueBal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262833	Angular + GLUE - OTP - Landing Page - Cancel Button Modal: With Out Past Due Bal");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on cancel button | Cancel Modal should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.verifyCancelModal();
		oneTimePaymentPage.verifyCancelMsg(PaymentConstants.CANCEL_TEXT_MODAL_NO_PAST_DUE);
		oneTimePaymentPage.clickBackBtnCancelModal();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.verifyCancelModal();
		oneTimePaymentPage.clickContinueBtnCancelModal();
	}

	/**
	 * US262833 Angular + GLUE - OTP - Landing Page - Cancel Button Modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEOTPPageCancelModalWithPastDueBal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262833	Angular + GLUE - OTP - Landing Page - Cancel Button Modal: With Past Due Bal");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on cancel button | Cancel Modal should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.verifyCancelModal();
		oneTimePaymentPage.verifyCancelMsg(PaymentConstants.CANCEL_TEXT_MODAL_PAST_DUE);
		oneTimePaymentPage.clickBackBtnCancelModal();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.verifyCancelModal();
		oneTimePaymentPage.clickContinueBtnCancelModal();
	}

	/**
	 * US262833 Angular + GLUE - OTP - Landing Page - Cancel Button Modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SEDONA })
	public void testAngularGLUEOTPPageCancelModalSedona(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262833	Angular + GLUE - OTP - Landing Page - Cancel Button Modal: Sedona");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on cancel button | Cancel Modal should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.verifyCancelModal();
		oneTimePaymentPage.verifyCancelMsg(PaymentConstants.CANCEL_TEXT_MODAL_SEDONA);
		oneTimePaymentPage.clickBackBtnCancelModal();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.verifyCancelModal();
		oneTimePaymentPage.clickContinueBtnCancelModal();
	}

	/**
	 * US262892 Angular + GLUE - OTP - Terms & Condition Spoke Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEOTPTermsConditionsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262892	Angular + GLUE - OTP - Terms & Condition Spoke Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log(
				"Step 4: verify payment arrangement texts and link | Payment arrangement links and text should be displayed");
		Reporter.log("Step 5: click on Terms & Conditions link | Terms & Conditions page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickTermsAndConditions();
		oneTimePaymentPage.verifyTermsandConditions();
		oneTimePaymentPage.clickTermsAndConditionsBackButton();
	}

	/**
	 * US262219 Angular + GLUE - PCM Blade Implementation (L1) - Static Blade
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEPCMBladeWithSavedPaymentBlade(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262219	Angular + GLUE - PCM Blade Implementation (L1) - Static Blade");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: verify payment method blade | Payment method blade should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyPayYourBillHeader();
		oneTimePaymentPage.verifySavedPaymentMethod();
	}

	/**
	 * US262219 Angular + GLUE - PCM Blade Implementation (L1) - Static Blade
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEPCMBladeWithOutSavedPaymentBlade(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262219	Angular + GLUE - PCM Blade Implementation (L1) - Static Blade");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: verify payment method blade | Payment method blade should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyPayYourBillHeader();
		oneTimePaymentPage.verifyPaymentMethod();
	}

	/**
	 * US217668 OTP CX Enhancements - Missed PA - OTP Landing Page - Display Missed
	 * Installment Amount
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testMissedPAOTPLandingPageDisplayMissedInstallmentAmount(ControlTestData data, MyTmoData myTmoData) {
		logger.info("US217668 OTP CX Enhancements - Missed PA - OTP Landing Page - Display Missed Installment Amount");
		Reporter.log(
				"Test Case : US217668 OTP CX Enhancements - Missed PA - OTP Landing Page - Display Missed Installment Amount");
		Reporter.log("Data Conditions| IR PAH With Missed PA and Upcoming PA");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on amount Icon| amount spoke page should be displayed");
		Reporter.log("Step 5: verify other amount filed is Selected| other amount should be selected");
		Reporter.log("Step 6: setup other amount < PA | Notification should be Displayed");
		Reporter.log("Step 7: verify High Severity symbol| High severity symbol should be Displayed");
		Reporter.log("Step 8: click on review Payments| Account Activity Page should be Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		// oneTimePaymentPage.verifyAmountDueOnBlade();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyAmountRadioButtonIsSelected("Other");
		editAmountPage.verifyWarningTextForLessAmount();
		oneTimePaymentPage.clickReviewPreviouspayments();
		AccountHistoryPage activitiesPage = new AccountHistoryPage(getDriver());
		activitiesPage.verifyPageLoaded();
	}

	/**
	 * US305185 OTP PA Notification - Missed PA - OTP Landing Page - Display
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testOTPSecuredPANotificationPOPUP(ControlTestData data, MyTmoData myTmoData) {
		logger.info("US217668 OTP CX Enhancements - Missed PA - OTP Landing Page - Display Missed Installment Amount");
		Reporter.log(
				"Test Case : US217668 OTP CX Enhancements - Missed PA - OTP Landing Page - Display Missed Installment Amount");
		Reporter.log("Data Conditions| IR PAH With Missed PA and Upcoming PA");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to OTP landing page | OTP landing page should be displayed");
		Reporter.log("Step 4: There should be Open Popup | The Popup should be displayed");
		Reporter.log(
				"Step 5: Should see the PENDING [INSTALLMENT AMOUNT] from [CARD/BANK ICON][INSTALLMENT DATE] for your PA| Should be able to see the details");
		Reporter.log(
				"Step 6: Should see the the links: Review Payment Arrangement and Continue with Payment | Should be able to see the links");
		Reporter.log(
				"Step 7: clicking Review Payment Arrangement will take to the Modify PA page| Should be able to take to OTP page");
		Reporter.log(
				"Step 8: click on Continue with Payment| user continues with the OTP the default amount should be set to the amount due for the current installment");
		Reporter.log(
				"Step 9: click on nywhere outside the modal will close the modal| Should navigate to the OTP page");
		Reporter.log("===============================");
		Reporter.log("Actual Results:");
	}

	/**
	 * US305185 OTP PA Notification - Missed PA - OTP Landing Page - Display
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testOTPNonSecuredPANotificationPOPUP(ControlTestData data, MyTmoData myTmoData) {
		logger.info("US217668 OTP CX Enhancements - Missed PA - OTP Landing Page - Display Missed Installment Amount");
		Reporter.log(
				"Test Case : US217668 OTP CX Enhancements - Missed PA - OTP Landing Page - Display Missed Installment Amount");
		Reporter.log("Data Conditions| IR PAH With Missed PA and Upcoming PA");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to OTP landing page | OTP landing page should be displayed");
		Reporter.log("Step 4: There shouldn't be any Popup message | Should not see any Popup message");
		Reporter.log("===============================");
		Reporter.log("Actual Results:");
	}

	/**
	 * 
	 * US320399 UI OTP landing page modification for upgrade flow US324123 UI OTP
	 * Landing page notification for upgrade flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testJumpOTPpageModificationsAndNotifications(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US320399 UI OTP landing page modification for upgrade flow");
		Reporter.log("US324123 UI OTP Landing page notification for upgrade flow");
		Reporter.log("Data Conditions| IR PAH With past due balance and not suspended");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4. Click on Shop link | Shop page should be displayed");
		Reporter.log("Step 5. Click on See all phones | Product list page should be displayed");
		Reporter.log("Step 6. Click on add to cart button | Line selector page should be displayed");
		Reporter.log("Step 7. Verify payment pop up modal window | Payment pop up modal should not be present");
		Reporter.log("Step 8. Click on X CTA | Modal should not be visible. Line selector page should be greyed out");
		Reporter.log("Step 9.Click on the banner | Billing and payments one time payment page should be displayed");
		Reporter.log(
				"Step 10: Verify the message below the amount| minimum required to upgrade message should be displayed");
		Reporter.log("Step 11: Click on Payment amount blade | Payment Edit amount page should be displayed");
		Reporter.log("Step 12: Verify the default selection|Past Due Balance should be selected");
		Reporter.log("Step 13: Select Past due radio button|past due radio button should be selected");
		Reporter.log("Step 14: Click on Update button|One time Payment page should be displayed");
		Reporter.log(
				"Step 15: Verify the notification above terms and conditions | Once you make this payment, your account will become eligible to upgrade or add a line should be displayed");
		Reporter.log("Step 16: Click on Payment amount blade | Payment Edit amount page should be displayed");
		Reporter.log("Step 17: Enter the custom Amount equal to the past due Amount | Amount should be dispalyed");
		Reporter.log("Step 18: Click on Update button|One time Payment page should be displayed");
		Reporter.log(
				"Step 19: Verify the message below the amount| minimum required to upgrade message should be displayed");
		Reporter.log(
				"Step 20: Verify the notification above terms and conditions | Once you make this payment, your account will become eligible to upgrade or add a line should be displayed");
		Reporter.log("Step 21: Click on Payment amount blade | Payment Edit amount page should be displayed");
		Reporter.log("Step 22: Enter the custom Amount greater than the past due Amount | Amount should be dispalyed");
		Reporter.log("Step 23: Click on Update button|Popup message should be displayed");
		Reporter.log("Step 24: Click YES on the popup|One time Payment page should be displayed");
		Reporter.log(
				"Step 25: Verify the message below the amount| minimum required to upgrade message should not be displayed");
		Reporter.log(
				"Step 26: Verify the notification above terms and conditions | Once you make this payment, your account will become eligible to upgrade or add a line should be displayed");
		Reporter.log("Step 27: Click on Payment amount blade | Payment Edit amount page should be displayed");
		Reporter.log("Step 28: Enter the custom Amount lesser than the past due Amount | Amount should be dispalyed");
		Reporter.log("Step 29: Click on Update button|One time Payment page should be displayed");
		Reporter.log(
				"Step 30: Verify the message below the amount| minimum required to upgrade message should not be displayed");
		Reporter.log(
				"Step 31: Verify the notification above terms and conditions | This payment will not make you eligible to upgrade");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		displayBillDueDateAndAmount(homePage);
		String pastdueAmount = homePage.verifyPastDueAmount();
		if (pastdueAmount.equals("Error")) {
			throw new FrameworkException("Balance amount is not displayed on the home page");
		}
		homePage.clickShoplink();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickOnFeaturedDevices("Apple iPhone 8 Plus");
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		/*
		 * lineSelectorPage.verifyLineSelectorPage();
		 * lineSelectorPage.verifyModalPastDueCustomer();
		 * lineSelectorPage.clickCloseCTAPastDueModal();
		 */
		lineSelectorPage.clickStickBannerNextIcon();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyAmountDueEqualsPastDueOnBlade(pastdueAmount);
		oneTimePaymentPage.verifyInlineErrorMsg("minimum required to upgrade");
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage otpAmountPage = new OTPAmountPage(getDriver());
		otpAmountPage.verifyPageLoaded();
		otpAmountPage.verifyTotalOrPastDueButtonIsSelected();
		otpAmountPage.clickUnselectedRadioButton();
		otpAmountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage
				.verifyOtpPageAlerts("Once the past-due amount is paid, you’ll be eligible to upgrade or add a line.");

		oneTimePaymentPage.clickamountIcon();
		otpAmountPage.verifyPageLoaded();
		otpAmountPage.clickAmountRadioButton("Other");
		otpAmountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage
				.verifyOtpPageAlerts("Once the past-due amount is paid, you’ll be eligible to upgrade or add a line.");

		oneTimePaymentPage.clickamountIcon();
		otpAmountPage.verifyPageLoaded();
		otpAmountPage.clickAmountRadioButton("Other");
		Double pastDueAmt = Double.parseDouble(pastdueAmount.replace("$", "")) + 1;
		otpAmountPage.setOtherAmount(pastDueAmt.toString());
		otpAmountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage
				.verifyOtpPageAlerts("Once the past-due amount is paid, you’ll be eligible to upgrade or add a line.");

		oneTimePaymentPage.clickamountIcon();
		otpAmountPage.verifyPageLoaded();
		otpAmountPage.clickAmountRadioButton("Other");
		pastDueAmt = pastDueAmt - 2;
		otpAmountPage.setOtherAmount(pastDueAmt.toString());
		otpAmountPage.verifyWarningTextsWhenLessAmountEntered();
		otpAmountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyInlineErrorMsgNotDisplayed();
		oneTimePaymentPage.verifyOtpPageAlerts("This payment will not make you eligible to upgrade");

	}

	/**
	 * US327484: UI OTP Cancel modal CTA behavior
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SEDONA })
	public void testJumpOTPpageCancelButtonCTA(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US327484: UI OTP Cancel modal CTA behavior");
		Reporter.log("Data Conditions| IR PAH With past due balance and not suspended");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4. Click on Shop link | Shop page should be displayed");
		Reporter.log("Step 5. Click on See all phones | Product list page should be displayed");
		Reporter.log("Step 6. Click on add to cart button | Line selector page should be displayed");
		Reporter.log("Step 7. Verify payment pop up modal window | Payment pop up modal should not be present");
		Reporter.log("Step 8. Click on X CTA | Modal should not be visible. Line selector page should be greyed out");
		Reporter.log("Step 9.Click on the banner | Billing and payments one time payment page should be displayed");
		Reporter.log("Step 10: Click on Cancel button| Are you sure Modal is presented");
		Reporter.log("Step 11: Click on continue button|Should be redirected to Shop Upgrade page");
		Reporter.log("Step 12: Click on Back button|Are you sure modal should be closed");
		Reporter.log("Actual Result:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickOnFeaturedDevices("Apple iPhone 8 Plus");
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		/*
		 * lineSelectorPage.verifyLineSelectorPage();
		 * lineSelectorPage.verifyModalPastDueCustomer();
		 * lineSelectorPage.clickCloseCTAPastDueModal();
		 */
		lineSelectorPage.clickStickBannerNextIcon();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.verifyCancelModal();
		oneTimePaymentPage.verifyCancelMsg(PaymentConstants.CANCEL_TEXT_MODAL_NO_PAST_DUE);
		oneTimePaymentPage.clickBackBtnCancelModal();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.verifyCancelModal();
		oneTimePaymentPage.clickContinueBtnCancelModal();
		ShopPage returnShopPage = new ShopPage(getDriver());
		returnShopPage.verifyShoppage();
	}

	/**
	 * US327519: Zero Balance on arrival to OTP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testJumpOTPpageZeroBalance(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US327519: Zero Balance on arrival to OTP");
		Reporter.log("Data Conditions| IR PAH With Zero Balance and current balance");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4. Click on Shop link | Shop page should be displayed");
		Reporter.log("Step 5. Click on See all phones | Product list page should be displayed");
		Reporter.log("Step 6. Click on add to cart button | Line selector page should be displayed");
		Reporter.log("Step 7. Verify payment pop up modal window | Payment pop up modal should not be present");
		Reporter.log("Step 8. Click on X CTA | Modal should not be visible. Line selector page should be greyed out");
		Reporter.log("Step 9.Click on the banner | Billing and payments one time payment page should be displayed");
		Reporter.log("Step 10: Verify the notification message above T&C| Notification message should not be shown");
		Reporter.log("Step 11: Verify the Amount blade for Zero balance |Enter Amount should be dispalyed");
		Reporter.log("Step 12: Click on Enter Amount on Amount blade |Edit Amount page should be dispalyed");
		Reporter.log("Step 13: Verify the default selection for zero balance|Other should be selected");
		Reporter.log("Step 14: Verify the Alert message below total balance|Alert message should not be shown");
		Reporter.log("Step 15: Verify for Past due radio button|Past due radio button should be removed");
		Reporter.log("Step 16: Click on Other radio button and verify the Amount |Default Amount should be zero");
		Reporter.log("Step 17: Verify the Alert message below Other button|Alert message should not be shown");
		Reporter.log("Step 18: Verify the notification above cancel CTA| Notification message should not be shown");
		Reporter.log("Step 19: Click on cancel on Edit amount page|one time payment page should be displayed");
		Reporter.log("Step 20: Click on cancel on OTP page|Should be redirected to Shop upgrade page");
		Reporter.log("Actual Result:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickOnFeaturedDevices("Apple iPhone 8 Plus");
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		/*
		 * lineSelectorPage.verifyLineSelectorPage();
		 * lineSelectorPage.verifyModalPastDueCustomer();
		 * lineSelectorPage.clickCloseCTAPastDueModal();
		 */
		lineSelectorPage.clickStickBannerNextIcon();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyAmountBladeText("Enter Amount");
		oneTimePaymentPage.verifyNoAlertsOnOtpPage();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage otpAmountPage = new OTPAmountPage(getDriver());
		otpAmountPage.verifyPageLoaded();
		otpAmountPage.verifyAmountRadioButtonIsSelected("Other");
		otpAmountPage.verifyAmountRadioButton("Past due");
		otpAmountPage.clickCancelBtn();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickCancelBtn();
		ShopPage returnShopPage = new ShopPage(getDriver());
		returnShopPage.verifyShoppage();

	}

	/**
	 * US327519: Zero Balance on arrival to OTP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testJumpOTPpageCurrentBalance(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US327519: Current Balance on arrival to OTP");
		Reporter.log("Data Conditions| IR PAH With current balance");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 4. Click on Shop link | Shop page should be displayed");
		Reporter.log("Step 5. Click on See all phones | Product list page should be displayed");
		Reporter.log("Step 6. Click on add to cart button | Line selector page should be displayed");
		Reporter.log("Step 7. Verify payment pop up modal window | Payment pop up modal should not be present");
		Reporter.log("Step 8. Click on X CTA | Modal should not be visible. Line selector page should be greyed out");
		Reporter.log("Step 9.Click on the banner | Billing and payments one time payment page should be displayed");
		Reporter.log("Step 10: Verify the notification message above T&C| Notification message should not be shown");
		Reporter.log("Step 11: Verify the Amount blade for Current balance |Balance Amount should be dispalyed");
		Reporter.log("Step 12: Click on Enter Amount on Amount blade |Edit Amount page should be dispalyed");
		Reporter.log("Step 13: Verify the default selection for Current balance|Total balance should be selected");
		Reporter.log("Step 14: Verify the Alert message below total balance|Alert message should not be shown");
		Reporter.log("Step 15: Verify for Past due radio button|Past due radio button should be removed");
		Reporter.log("Step 16: Click on Other radio button and verify the Amount |Default Amount should be zero");
		Reporter.log("Step 17: Verify the Alert message below Other button|Alert message should not be shown");
		Reporter.log("Step 18: Verify the notification above cancel CTA| Notification message should not be shown");
		Reporter.log("Step 19: Click on cancel on Edit amount page|one time payment page should be displayed");
		Reporter.log("Step 20: Click on cancel on OTP page|Should be redirected to Shop upgrade page");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		String bal = homePage.getBalanceDueAmount();
		homePage.clickShoplink();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickOnFeaturedDevices("Apple iPhone 8 Plus");
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		/*
		 * lineSelectorPage.verifyLineSelectorPage();
		 * lineSelectorPage.verifyModalPastDueCustomer();
		 * lineSelectorPage.clickCloseCTAPastDueModal();
		 */
		lineSelectorPage.clickStickBannerNextIcon();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyAmountBladeText(bal);
		oneTimePaymentPage.verifyNoAlertsOnOtpPage();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage otpAmountPage = new OTPAmountPage(getDriver());
		otpAmountPage.verifyPageLoaded();
		otpAmountPage.verifyAmountRadioButtonIsSelected("Total");
		otpAmountPage.verifyAmountRadioButton("Past due");
		otpAmountPage.clickCancelBtn();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickCancelBtn();
		ShopPage returnShopPage = new ShopPage(getDriver());
		returnShopPage.verifyShoppage();
	}

	/**
	 * US335247 OTP - PA Notifications - Interim Solution - Cont.
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testOTPPANotificationsForSecuredPaymentArragement(ControlTestData data, MyTmoData myTmoData) {
		logger.info("US335247 OTP - PA Notifications - Interim Solution - Cont.");
		Reporter.log("Test Case : US335247 OTP - PA Notifications - Interim Solution - Cont.");
		Reporter.log("Data Conditions| IR PAH With Scheduled upcoming PA with Stored payment");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to OTP landing page | OTP landing page should be displayed");
		Reporter.log("Step 4: There should be Open Popup | The Popup should be displayed");
		Reporter.log(
				"Step 5: Should see the message 'You've already scheduled a payment of $[PENDING INSTALLMENT AMOUNT] from [CARD/BANK ICON] ****[XXXX] on [INSTALLMENT DATE] for your Payment Arrangement. Making a separate payment is not guaranteed to stop this from processing.' on pop up| Should be able to see the details");
		Reporter.log(
				"Step 6: Should see the : Review Payment Arrangement and Continue with Payment | Should be able to see the options");
		Reporter.log(
				"Step 7: clicking Review Payment Arrangement will take to the Modify PA page| Should be able to take to PA page");
		Reporter.log(
				"Step 8: click on Continue with Payment| user continues with the OTP page and the default amount should be set to the amount due for the current installment");
		Reporter.log(
				"Step 9: click on anywhere outside the modal will close the modal| Should navigate to the OTP page");
		Reporter.log("===============================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyReviewPAModal();
		oneTimePaymentPage.clickContinueWithPaymentCTA();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.clickContinueBtnCancelModal();
		verifyPage("Home page", "Home");
		HomePage homePage = new HomePage(getDriver());
		homePage.clickPayBillbutton();
		oneTimePaymentPage.verifyPageLoaded();
		String reviewPaText = oneTimePaymentPage.verifyReviewPAModal();
		String amount = reviewPaText.substring(reviewPaText.indexOf("$")).substring(0, reviewPaText.indexOf(" "));
		String installementDate = reviewPaText.substring(reviewPaText.indexOf(" on ")).substring(4, 14);
		SimpleDateFormat inputFormat = new SimpleDateFormat("MM/DD/yyyy");
		Date date = null;
		try {
			date = inputFormat.parse(installementDate);
			SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy");
			installementDate = outputFormat.format(date);
		} catch (ParseException e) {
			Assert.fail("Test failed in date formatting");
		}
		oneTimePaymentPage.clickReviewPABtnOnModal();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyInstallementDate(installementDate);
		paymentArrangementPage.verifyInstallementAmount(amount);

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testOTPPANotificationsForUnsecuredPA(ControlTestData data, MyTmoData myTmoData) {
		logger.info("US335247 OTP - PA Notifications - Interim Solution - Cont.");
		Reporter.log("Test Case : US335247 OTP - PA Notifications - Interim Solution - Cont.");
		Reporter.log("Data Conditions| IR PAH With Scheduled upcoming PA with Stored payment");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to OTP landing page | OTP landing page should be displayed");
		Reporter.log(
				"Step 4: There shouldn't be any Popup available | The Popup should not be visible for Unsecured  PA");
		Reporter.log("Actual Results:");
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		Assert.assertFalse(oneTimePaymentPage.verifyReviewPAModalIsDisplayedOrNot(),
				"Review PA Modal on OTP page is found");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testOTPPANotificationsForSecuredPASingleInstallment(ControlTestData data, MyTmoData myTmoData) {
		logger.info("US335247 OTP - PA Notifications - Interim Solution - Cont.");
		Reporter.log("Test Case : US335247 OTP - PA Notifications - Interim Solution - Cont.");
		Reporter.log("Data Conditions| IR PAH With Scheduled upcoming PA with Stored payment");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to OTP landing page | OTP landing page should be displayed");
		Reporter.log("Step 4: There should be Open Popup | The Popup should be displayed");
		Reporter.log(
				"Step 5: Should see the message 'You've already scheduled a payment of $[PENDING INSTALLMENT AMOUNT] from [CARD/BANK ICON] ****[XXXX] on [INSTALLMENT DATE] for your Payment Arrangement. Making a separate payment is not guaranteed to stop this from processing.' on pop up| Should be able to see the details");
		Reporter.log(
				"Step 6: Should see the : Review Payment Arrangement and Continue with Payment | Should be able to see the options");
		Reporter.log(
				"Step 7: clicking Review Payment Arrangement will take to the Modify PA page| Should be able to take to PA page");
		Reporter.log(
				"Step 8: click on Continue with Payment| user continues with the OTP page and the default amount should be set to the amount due for the current installment");
		Reporter.log(
				"Step 9: click on anywhere outside the modal will close the modal| Should navigate to the OTP page");
		Reporter.log("===============================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyReviewPAModal();
		oneTimePaymentPage.clickContinueWithPaymentCTA();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.clickContinueBtnCancelModal();
		verifyPage("Home page", "Home");
		HomePage homePage = new HomePage(getDriver());
		homePage.clickPayBillbutton();
		oneTimePaymentPage.verifyPageLoaded();
		String reviewPaText = oneTimePaymentPage.verifyReviewPAModal();
		String amount = reviewPaText.substring(reviewPaText.indexOf("$")).substring(0, reviewPaText.indexOf(" "));
		String installementDate = reviewPaText.substring(reviewPaText.indexOf(" on ")).substring(4, 14);
		SimpleDateFormat inputFormat = new SimpleDateFormat("MM/DD/yyyy");
		Date date = null;
		try {
			date = inputFormat.parse(installementDate);
			SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy");
			installementDate = outputFormat.format(date);
		} catch (ParseException e) {
			Assert.fail("Test failed in date formatting");
		}
		oneTimePaymentPage.clickReviewPABtnOnModal();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyInstallementDate(installementDate);
		paymentArrangementPage.verifyInstallementAmount(amount);
	}

	/**
	 * US242113:PA Messaging Enhancements- Secured PA - OTP Landing Page (Outside
	 * blackout)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testSecuredPAMessageonOTPOutsideBlackoutPeriod(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US242113:PA Messaging Enhancements- Secured PA - OTP Landing Page");
		Reporter.log("Data Condition | PA secured outside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("1. Navigate to my t-mobile.com | Login page should be displayed");
		Reporter.log("2. Login as a mytmo customer | Home page should be displayed");
		Reporter.log("3. Click on paybill | OTP landing page should be loadded");
		Reporter.log("4: There should be Open Popup | The Popup should be displayed");
		Reporter.log(
				"4. Verify message 'Should see the message 'DON'T FORGET! This payment may NOT cancel your scheduled payment of [$XX.XX] from <ICON> ****<XXXX> on  [MM/DD/YYYY] for your Payment Arrangement.' on popup modal window |  Message should be displayed in popup modal window");
		Reporter.log("5. Verify edit payment cta | Edit payment cta should be displayed");
		Reporter.log(
				"7. Click on edit payment cta | the payment arrangement edit payment method page should be displayed");
		Reporter.log("11: Click 'Cancel' button then on modal click 'Yes' | OTP page should be displayed");
		Reporter.log("11: On OTP landing page should able to see popup | Popup should be displayed");
		Reporter.log("6. Verify continue payment cta |Continue payment cta should be displayed");
		Reporter.log("9. Click continue payment cta | One time payment page should be displayed ");
		Reporter.log("10.Verify amount due | Amount due should be displayed");
		Reporter.log("11.Verify payment method icon | Payment method icon should be displayed");
		Reporter.log(
				"12.Verify last 4 digits of the payment method | Last 4 digits of the payment method should be displayed");
		Reporter.log("13.Verify current installment due date | Current installment due date should be displayed");
		Reporter.log(
				"14.And should Verify 'This payment may NOT cancel your scheduled Payment Arrangement' PA notification | Payment may not cancel PA notification should be displayed");
		Reporter.log("15.Verify payment arrangement hyper link | Payment arrangement hyper link should be displayed");
		Reporter.log(
				"16.Click on payment arrangement hyper link | Payment arrangement landing page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		String paText = PaymentConstants.PAYMENT_ARRANGEMENT_OTP_MODAL.replace("[$XX.XX]", paDetails.get("Amount"))
				.replace("****<XXXX>", paDetails.get("Payment Method")).replace("[MM/DD/YYYY]", paDetails.get("Date"));
		String paTextOnModal = oneTimePaymentPage.verifyReviewPAModal();
		Assert.assertTrue(paText.equals(paTextOnModal));
		oneTimePaymentPage.clickContinueWithPaymentCTA();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyPAAlert(PaymentConstants.PA_ON_OTP);
		oneTimePaymentPage.refreshPage();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickReviewPABtnOnModal();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.verifyInstallementDate(paDetails.get("Date"));
		paymentArrangementPage.verifyInstallementAmount(paDetails.get("Amount"));
	}

	/**
	 * US352129:PA Messaging Enhancements - Missed PA - OTP Landing Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testMissedPAMessageonOTPLandingPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US352129:PA Messaging Enhancements - Missed PA - OTP Landing Page");
		Reporter.log("Data Condition | PA Messaging Enhancements - Missed PA - OTP Landing Page");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("1. Navigate to my t-mobile.com | Login page should be displayed");
		Reporter.log("2. Login as a mytmo customer | Home page should be displayed");
		Reporter.log("3. Click on paybill | One time payment page should be displayed");
		Reporter.log("4. Verify amount due| Amount due should be displayed");
		Reporter.log("5. Click on amount blade | Amount spoke page should be displayed");
		Reporter.log(
				"6. Enter payment amount >= missed installment due, then click on update button and click yes on confirmation window | This payment covers your Payment Arrangement amount due today notification should be displayed");
		Reporter.log(
				"7. Enter payment amount < missed installment due, then click on update button and click yes on confirmation window | You need to have paid $[XX.XX] toward your Payment Arrangement by today to avoid suspension & fees. Increase amount or review payments made notification should be displayed");
		Reporter.log("8. Verify review payments made hyperlink | Review payments made hyperlink should be displayed");
		Reporter.log("9. Click on review payments made hyperlink | Account activity page should be displayed");
		Reporter.log(
				"10.Verify amount due for the missed installment | Amount due for the missed installment should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		// TODO: need to check from where the amount can be captured in case of
		// Missed PA instead of AH.
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyAmountBladeText(paDetails.get("Amount"));
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double balance = Double.parseDouble(paDetails.get("Amount")) + 1;
		editAmountPage.setOtherAmount(balance.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPAAlert(PaymentConstants.PA_OTP_PAGE_MISSED_LESS_AMOUNT);

		oneTimePaymentPage.clickamountIcon();
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		balance = Double.parseDouble(paDetails.get("Amount")) - 2;
		editAmountPage.setOtherAmount(balance.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPAAlert(PaymentConstants.PA_OTP_PAGE_MISSED_MORE_AMOUNT);
		oneTimePaymentPage.clickReviewPaymentsMade();
		AccountHistoryPage alertsActivitiesPage = new AccountHistoryPage(getDriver());
		alertsActivitiesPage.verifyPageLoaded();
	}

	/**
	 * US217668 :PA Messaging Enhancements - Missed PA - OTP Edit Amount Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testMissedPAMessageonOTPEditPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US217668:PA Messaging Enhancements - Missed PA - OTP Edit Amount Page");
		Reporter.log("Data Condition | PA Messaging Enhancements - Missed PA - OTP Edit Amount Page");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("1. Navigate to my t-mobile.com | Login page should be displayed");
		Reporter.log("2. Login as a mytmo customer | Home page should be displayed");
		Reporter.log("3. Click on paybill | One time payment page should be displayed");
		Reporter.log("4. Verify amount due| Amount due should be displayed");
		Reporter.log("5. Click on amount blade |  Amount spoke page should be displayed");
		Reporter.log("6. Verify other radio button state | Other radio button should be selected by default");
		Reporter.log(
				"7. Enter payment amount is changed to < current installment due, then click on update button and click yes on confirmation window | You need to have paid $[XX.XX] toward your Payment notification should be displayed");
		Reporter.log("8. Verify review payments made hyperlink | Review payments made hyperlink should be displayed");
		Reporter.log("9. Click on review payments made hyperlink | Account activity page should be displayed");
		Reporter.log(
				"10.Verify amount due for the missed installment | Amount due for the missed installment should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.verifyAmountRadioButtonIsSelected("Other");
		Double balance = Double.parseDouble(editAmountPage.getBalanceDueAmount()) - 1;
		editAmountPage.setOtherAmount(balance.toString());
		editAmountPage.verifyPAAlert(PaymentConstants.PA_OTP_EDIT_PAGE_MISSED_LESS_AMOUNT);
		editAmountPage.clickReviewPaymentsMade();
		AccountHistoryPage alertsActivitiesPage = new AccountHistoryPage(getDriver());
		alertsActivitiesPage.verifyPageLoaded();
	}

	//
	/**
	 * US229949:PA Messaging Enhancements - Secured PA - OTP Landing Page (Inside
	 * blackout)
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testsecuredPAReviewPaymentArrangementdetailsCTAOTPLandingPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test US229949:  PA Messaging Enhancements - Secured PA - OTP Landing Page (Inside blackout)");
		Reporter.log("Data Condition | PA secured inside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log(
				"Step 4: verify modal with message 'PLEASE NOTE! Your scheduled payment of [$XX.XX] from <ICON> ****<XXXX> is currently being processed for your Payment Arrangement and cannot be cancelled' |modal  should be displayed");
		Reporter.log(
				"Step 5: verify CTA's (Review Payment Arrangement details,Continue making payment)| Review Payment Arrangement details,Continue making payment CTA's should be displayed");
		Reporter.log(
				"Step 6: click on 'Review Payment Arrangement details' CTA|payment arrangement landing  page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		String paText = PaymentConstants.PA_OTP_SECURED_PA_INSIDE_BLACKOUT_MSG
				.replace("[$XX.XX]", paDetails.get("Amount")).replace("****<XXXX>", paDetails.get("Payment Method"));
		String paTextOnModal = oneTimePaymentPage.verifyReviewPAModal();
		Assert.assertTrue(paText.equals(paTextOnModal));
		oneTimePaymentPage.clickContinueWithPaymentCTA();
		oneTimePaymentPage.verifyPageLoaded();

	}

	/**
	 * US229949
	 * 
	 * PA Messaging Enhancements - Secured PA - OTP Landing Page (Inside blackout)
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASecuredAlertOTPLandingPageInsideBlackoutPeriod(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US229949:  PA Messaging Enhancements - Secured PA - OTP Landing Page (Inside blackout)");
		Reporter.log("Data Condition | PA secured inside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log(
				"Step 4: verify modal with  message 'PLEASE NOTE! Your scheduled payment of [$XX.XX] from <ICON> ****<XXXX> is currently being processed for your Payment Arrangement and cannot be cancelled' | modal with PA processed message should be displayed");
		Reporter.log("Step 5: verify [$XX.XX] is current installemnet due|current installemnet due should be verified");
		Reporter.log(
				"Step 6: verify <ICON> is payment method icon of the payment method being used for PA|<ICON> should be verified");
		Reporter.log(
				"Step 7: verify <XXXX> is the last 4 digits of the payment method being used for PA| <XXXX>  last 4 digits should be verified");
		Reporter.log(
				"Step 8: verify CTA's (Review Payment Arrangement details,Continue making payment)| Review Payment Arrangement details,Continue making payment CTA's should be displayed");
		Reporter.log(
				"Step 9: click on 'Continue making payment' CTA| one-time payment landing page  should be displayed");
		Reporter.log(
				"Step 10: verify 'This payment will NOT cancel your scheduled Payment Arrangement. View payment schedule' on otp page| 'This payment will NOT cancel your scheduled Payment Arrangement. View payment schedule'   should be displayed");
		Reporter.log(
				"Step 11: click on 'View payment schedule' link| payment arrangement landing page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		String paText = PaymentConstants.PA_OTP_SECURED_PA_INSIDE_BLACKOUT_MSG
				.replace("[$XX.XX]", paDetails.get("Amount")).replace("****<XXXX>", paDetails.get("Payment Method"));
		String paTextOnModal = oneTimePaymentPage.verifyReviewPAModal();
		Assert.assertTrue(paText.equals(paTextOnModal));
		oneTimePaymentPage.clickContinueWithPaymentCTA();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifySecuredPAMessageNotification(
				PaymentConstants.PA_OTP_SECURED_PA_INSIDE_BLACKOUT_MESSAGE_NOTIFICATION);
		oneTimePaymentPage.verifyViewPaymentScheduleLink();

	}

	/**
	 * US304469 PA Messaging Enhancements - Unsecured PA - OTP Landing Page (Inside
	 * blackout)
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUnsecuredPAMessageonOTPLandingPageAmountGreaterthanInstallmentdueInsideBlackout(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US304469:  PA Messaging Enhancements - Unsecured PA - OTP Landing Page (Inside blackout)");
		Reporter.log("Data Condition | PA unsecured inside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade |  OTP edit amount page should be displayed");
		Reporter.log(
				"Step 5: verify current installment due pre-populated  in the Other Amount field| current installment due should be pre-populated in the Other Amount field");
		Reporter.log("Step 6: enter other amount greater than installment due and update | amount should be updated");
		Reporter.log(
				"Step 7: verify 'This payment covers your payment arrangement amount due today' message |'This payment covers your payment arrangement amount due today' message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();

		editAmountPage.clickAmountRadioButton("Other");
		Double bal = Double.parseDouble(paDetails.get("Amount")) + 5;
		editAmountPage.setOtherAmount(bal.toString());
		editAmountPage.clickUpdateAmount();

		oneTimePaymentPage.verifyPAAlert(PaymentConstants.PA_OTP_UNSECURE_INSIDE_MORE_AMOUNT);

		oneTimePaymentPage.clickamountIcon();
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		bal = bal - 6;
		editAmountPage.setOtherAmount(bal.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPAAlert(PaymentConstants.PA_OTP_UNSECURE_INSIDE_LESS_AMOUNT);

		oneTimePaymentPage.clickReviewPaymentsMade();
		AccountHistoryPage alertsActivitiesPage = new AccountHistoryPage(getDriver());
		alertsActivitiesPage.verifyPageLoaded();
	}

	/**
	 * US304469 PA Messaging Enhancements - Unsecured PA - OTP Landing Page (Inside
	 * blackout) Not required: Already covered in above test
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUnsecuredPAMessageonOTPLandingPageAmountLessthanInstallmentdueInsideBlackout(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test US304469:  PA Messaging Enhancements - Unsecured PA - OTP Landing Page (Inside blackout)");
		Reporter.log("Data Condition | PA unsecured inside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade |  OTP edit amount page should be displayed");
		Reporter.log(
				"Step 5: verify current installment due pre-populated  in the Other Amount field| current installment due should be pre-populated in the Other Amount field");
		Reporter.log("Step 6: enter other amount less than installment due and update | amount should be updated");
		Reporter.log(
				"Step 7: verify 'You need to have paid $[XX.XX] toward your Payment Arrangement by today to avoid suspension & fees. Increase amount or review previous payments.' message | message should be displayed");
		Reporter.log("Step 8: verify [$XX.XX] is current installment due|current installment due should be verified");
		Reporter.log("Step 9: click on 'review previous payments' link|account activity page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");
	}

	/**
	 * US396884 Desktop: Enhance AR Balance Field to Update in Real Time
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testVerifyARBalanceUpdatedUponOTPPayment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US396884:  Desktop: Enhance AR Balance Field to Update in Real Time");
		Reporter.log("Data Condition |Any PAH/Standard with balance due and account is not suspended");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: capture balance due on home page | balance due should be captured");
		Reporter.log("Step 4: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 5: click on Payment amount blade |  OTP  amount page should be displayed");
		Reporter.log(
				"Step 6: select other radio buttton and enter captured balance due from home page | amount should be entered");
		Reporter.log("Step 7: enter payment details and submit | OTP confirmation page should be displayed");
		Reporter.log("Step 8: navigate back to home page | home page should be displayed");
		Reporter.log(
				"Step 9: verify balance due amount is'0' on home page|balance due amount should be '0' on home page");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		displayBillDueDateAndAmount(homePage);
		String balanceAmount = homePage.getBalanceDueAmount();
		if (balanceAmount.equals("Error")) {
			throw new FrameworkException("Balance amount is not displayed on the home page");
		}
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(balanceAmount.replace("$", "").trim());
		String dAmount = editAmountPage.getOtherAmount();
		editAmountPage.clickUpdateAmount();
		if (!oneTimePaymentPage.verifyPaymethodInPaymentBalde()) {
			oneTimePaymentPage.clickPaymentMethodBlade();
			SpokePage otpSpokePage = new SpokePage(getDriver());
			addNewCard(myTmoData.getPayment(), otpSpokePage);
		}
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.clickReturnToHome();
		homePage.verifyPageLoaded();
		String amountAfterPayment = homePage.getBalanceDueAmount();
		verifyHomePageBalanceDue(balanceAmount, dAmount, amountAfterPayment, homePage);
	}

	/**
	 * US396960: Mobile: Enhance AR Balance Field to Update in Real Time
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testVerifyARBalanceUpdatedUponOTPPaymentMobile(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US396960: Mobile: Enhance AR Balance Field to Update in Real Time");
		Reporter.log("Data Condition |Any PAH/Standard with balance due and account is not suspended");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: capture balance due on home page | balance due should be captured");
		Reporter.log("Step 4: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 5: click on Payment amount blade |  OTP  amount page should be displayed");
		Reporter.log(
				"Step 6: select other radio buttton and enter captured balance due from home page | amount should be entered");
		Reporter.log("Step 7: enter payment details and submit | OTP confirmation page should be displayed");
		Reporter.log("Step 8: navigate back to home page | home page should be displayed");
		Reporter.log(
				"Step 9: verify balance due amount is'0' on home page|balance due amount should be '0' on home page");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		displayBillDueDateAndAmount(homePage);
		String balanceAmount = homePage.getBalanceDueAmount();
		if (balanceAmount.equals("Error")) {
			throw new FrameworkException("Balance amount is not displayed on the home page");
		}
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(balanceAmount.replace("$", "").trim());
		String dAmount = editAmountPage.getOtherAmount();
		editAmountPage.clickUpdateAmount();
		if (!oneTimePaymentPage.verifyPaymethodInPaymentBalde()) {
			oneTimePaymentPage.clickPaymentMethodBlade();
			SpokePage otpSpokePage = new SpokePage(getDriver());
			addNewCard(myTmoData.getPayment(), otpSpokePage);
		}
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.clickReturnToHome();
		homePage.verifyPageLoaded();
		String amountAfterPayment = homePage.getBalanceDueAmount();
		verifyHomePageBalanceDue(balanceAmount, dAmount, amountAfterPayment, homePage);
	}

	/**
	 * US396884 Desktop: Enhance AR Balance Field to Update in Real Time US387414
	 * Desktop: Enhance AR Balance > Calculate AR Balance for multiple and excess
	 * payments. US396955 Mobile: Enhance AR Balance > Calculate AR Balance for
	 * multiple and excess payments.
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testVerifyARBalanceUpdatedUponPaymentMoreThanDue(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US396884:  Desktop: Enhance AR Balance Field to Update in Real Time");
		Reporter.log("Data Condition |Any PAH/Standard with balance due and account is not suspended");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: capture balance due on home page | balance due should be captured");
		Reporter.log("Step 4: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 5: click on Payment amount blade |  OTP  amount page should be displayed");
		Reporter.log(
				"Step 6: select other radio buttton and enter amount more than balance due| amount should be entered");
		Reporter.log("Step 7: enter payment details and submit | OTP confirmation page should be displayed");
		Reporter.log("Step 8: navigate back to home page | home page should be displayed");
		Reporter.log(
				"Step 9: verify negative amount due on home page|negative amount due on home page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		displayBillDueDateAndAmount(homePage);
		String balanceAmount = homePage.getBalanceDueAmount();
		if (balanceAmount.equals("Error")) {
			throw new FrameworkException("Balance amount is not displayed on the home page");
		}
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(
				String.valueOf(Double.parseDouble(balanceAmount.replace("$", "").trim()) + generateRandomAmount()));
		String dAmount = editAmountPage.getOtherAmount();
		editAmountPage.clickUpdateAmount();
		if (!oneTimePaymentPage.verifyPaymethodInPaymentBalde()) {
			oneTimePaymentPage.clickPaymentMethodBlade();
			SpokePage otpSpokePage = new SpokePage(getDriver());
			addNewCard(myTmoData.getPayment(), otpSpokePage);
		}
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.clickReturnToHome();
		homePage.verifyPageLoaded();
		String amountAfterPayment = homePage.getBalanceDueAmount();
		verifyHomePageBalanceDue(balanceAmount, dAmount, amountAfterPayment, homePage);
	}

	/**
	 * US396884 Desktop: Enhance AR Balance Field to Update in Real Time US387414
	 * Desktop: Enhance AR Balance > Calculate AR Balance for multiple and excess
	 * payments. US396955 Mobile: Enhance AR Balance > Calculate AR Balance for
	 * multiple and excess payments.
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testVerifyARBalanceUpdatedUponMultiplePayments(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test :  Desktop: Enhance AR Balance Field to Update in Real Time");
		Reporter.log("Data Condition |Any PAh/Standard with balance due and account is not suspended");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: capture balance due on home page | balance due should be captured");
		Reporter.log("Step 4: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 5: click on Payment amount blade |  OTP  amount page should be displayed");
		Reporter.log(
				"Step 6: select other radio buttton and enter amount between '0' to '$2'| amount should be entered");
		Reporter.log("Step 7: enter payment details and submit | OTP confirmation page should be displayed");
		Reporter.log("Step 8: navigate back to home page | home page should be displayed");
		Reporter.log(
				"Step 9: verify updated Balance due(previous capturted balance due - '0' to '$2' = updated balance due)  |  Balance due should be updated ");
		Reporter.log("Step 10: capture updated balance due on home page | balance due should be captured");
		Reporter.log("Step 11: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 12: click on Payment amount blade |  OTP  amount page should be displayed");
		Reporter.log(
				"Step 13: select other radio buttton and enter amount between '0' to '$2'| amount should be entered");
		Reporter.log("Step 14: enter payment details and submit | OTP confirmation page should be displayed");
		Reporter.log("Step 15: navigate back to home page | home page should be displayed");
		Reporter.log(
				"Step 16: again verify updated Balance due(previous capturted balance due - '0' to '$2' = updated balance due)  |  Balance due should be updated ");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		displayBillDueDateAndAmount(homePage);
		String balanceAmount = homePage.getBalanceDueAmount();
		if (balanceAmount.equals("Error")) {
			throw new FrameworkException("Balance amount is not displayed on the home page");
		}
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		String dAmount = editAmountPage.getOtherAmount();
		editAmountPage.clickUpdateAmount();
		if (!oneTimePaymentPage.verifyPaymethodInPaymentBalde()) {
			oneTimePaymentPage.clickPaymentMethodBlade();
			SpokePage otpSpokePage = new SpokePage(getDriver());
			addNewCard(myTmoData.getPayment(), otpSpokePage);
		}

		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.clickReturnToHome();
		homePage.verifyPageLoaded();
		String amountAfterPayment = homePage.getBalanceDueAmount();
		verifyHomePageBalanceDue(balanceAmount, dAmount, amountAfterPayment, homePage);
		// One More Payment
		balanceAmount = homePage.getBalanceDueAmount();
		homePage.clickPayBillbutton();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		dAmount = editAmountPage.getOtherAmount();
		editAmountPage.clickUpdateAmount();
		if (!oneTimePaymentPage.verifyPaymethodInPaymentBalde()) {
			oneTimePaymentPage.clickPaymentMethodBlade();
			SpokePage otpSpokePage = new SpokePage(getDriver());
			addNewCard(myTmoData.getPayment(), otpSpokePage);
		}
		oneTimePaymentPage.agreeAndSubmit();
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.clickReturnToHome();
		homePage.verifyPageLoaded();
		amountAfterPayment = homePage.getBalanceDueAmount();
		verifyHomePageBalanceDue(balanceAmount, dAmount, amountAfterPayment, homePage);
	}

	/**
	 * US304466 PA Messaging Enhancements - Unsecured PA - OTP Landing Page (Outside
	 * blackout)
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUnsecuredPAMessageOutsideBlackoutPeriodOTPLandingPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case:US304466: PA Messaging Enhancements - Unsecured PA - OTP Landing Page (Outside blackout)");
		Reporter.log("Data Condition | PA unsecured outside blackout period");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on Payment amount blade |  OTP  amount page should be displayed");
		Reporter.log(
				"Step 5: verify Other radio button selected by default| Other radio button should be selected by default");
		Reporter.log(
				"Step 6: verify current installment due pre-populated  in the Other Amount field| current installment due should be pre-populated in the Other Amount field");
		Reporter.log("Step 7: change the payment amount < installment due and update | amount should be updated");
		Reporter.log(
				"Step 8: verify 'You need to have paid $[XX.XX] toward your Payment Arrangement by [INSTALLMENT DATE] to avoid suspension & fees. Review payments made' alert|alert should be displayed");
		Reporter.log("Step 9: verify [$XX.XX] is current installemnet due|current installemnet due should be verified");
		Reporter.log("Step 10: click on 'Review payments made' link|account activity page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		HashMap<String, String> paDetails = accountHistoryPage.getScheduledPaDetails();
		accountHistoryPage.clickOnTMobileIcon();
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyAmountBladeText(paDetails.get("Amount"));
		String paText = PaymentConstants.PA_OTP_UNSECURE_OUTSIDE_LESS_AMOUNT
				.replace("$[XX.XX]", paDetails.get("Amount")).replace("[MM/DD/YYYY]", paDetails.get("Date"));
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double bal = Double.parseDouble(paDetails.get("Amount").replace("$", "")) - 1;
		editAmountPage.setOtherAmount(bal.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyPAAlert(paText);
		oneTimePaymentPage.clickReviewPaymentsMade();
		AccountHistoryPage alertsActivitiesPage = new AccountHistoryPage(getDriver());
		alertsActivitiesPage.verifyPageLoaded();

	}

	/**
	 * US408133 PII Masking> OTP/PA Conflict Modal
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPIIMaskingOTPPAConflictModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US408133: PII Masking OTP/PA Conflict Modal");
		Reporter.log("Data Condition | PA  scheduled");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps: | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | OTP/PA Conflict Modal should be displayed");
		Reporter.log(
				"Step 4: verify PII masking | PII masking attribute should be present for payment method blade card/bank");
		Reporter.log("Step 5: verify the PII tags |  The PII tags should be numbered ");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps:");

		OneTimePaymentPage otpPage = navigateToOTPpage(myTmoData);
		if (otpPage.verifyReviewPAModalIsDisplayedOrNot()) {
			otpPage.verifyPIImaskingOnModal(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		}
	}

	/**
	 * US346425 MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-OTP
	 * Variant-L1-Modification.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "testdata" })
	public void testDigitalPASupportPastDueOTPModification(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"US346425 MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-OTP Variant-L1-Modification.");
		Reporter.log("Data Condition | past due balance aged greater than 30 days  ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log(
				"Step 4: verify greater than 30 day past due displayed on amount blade  | greater than 30 day past due should be displayed on amount blade");
		Reporter.log(
				"Step 5: verify  Date blade will display the current date| Date blade should display the current date");
		Reporter.log(
				"Step 6: verify date change functionality will be disabled|  date change functionality should  be disabled");
		Reporter.log(
				"Step 7: verify  AutoPay  messaging  will be suppressed|  AutoPay  messaging  should be suppressed");
		Reporter.log(
				"Step 8:  verify  payment arrangement link will be suppressed |  payment arrangement link should be suppressed");
		Reporter.log(
				"Step 9: verify  $XXXX is the greater than 30 day past due amount|  greater than 30 day past due amount should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentArrangementLink();
		oneTimePaymentPage.verifyPAInterruptModal();
		String pastDue30DaysAmount = oneTimePaymentPage.get30DaysPastDueAmount();
		oneTimePaymentPage.clickContinueOnPAInterruptModal();
		oneTimePaymentPage.verifyNewOTPPageLoaded();
		oneTimePaymentPage.verifyAmountDueEqualsPastDueOnBlade(pastDue30DaysAmount);
		// oneTimePaymentPage.verifyAmountBladeText("");
		oneTimePaymentPage.verifyDateBladeIsDisabled();
		oneTimePaymentPage.verifyDateOnDateBlade();
		oneTimePaymentPage.verifyPALinkIsSuppressed();
	}

	/**
	 * US346426 MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-OTP
	 * Variant-L1-Modification-Messaging
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "testdata" })
	public void testDigitalPASupportPastDueModificationMessaging(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"US346426 MyTMO - PAYMENTS - Digital PA Support for >30 Days Past Due-OTP Variant-L1-Modification-Messaging");
		Reporter.log("Data Condition | past due balance aged greater than 30 days  ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log(
				"Step 4: Change default amount >= 30 day past due amount, but < the total past due amount | Eligible for a payment arrangement notification should be displayed");
		Reporter.log(
				"Step 5: Change default amount <= 30 day past due amount, but < the total past due amount | Not eligible for a payment arrangement notification should be displayed");
		Reporter.log(
				"Step 6: Change default amount >= the total past due amount | Payment will cover your past due balance notification should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentArrangementLink();
		oneTimePaymentPage.verifyPAInterruptModal();
		String pastDue30DAmount = oneTimePaymentPage.get30DaysPastDueAmount().replace("$", "");
		oneTimePaymentPage.clickContinueOnPAInterruptModal();
		oneTimePaymentPage.verifyNewOTPPageLoaded();
		String totalDueAmount = oneTimePaymentPage.getTotalDueAmount().replace("$", "");
		OTPAmountPage amountPage = new OTPAmountPage(getDriver());

		oneTimePaymentPage.clickamountIcon();
		// amountPage.verifyotherAmountIsPopulated(pastDue30DAmount);
		Double updatedAmount = Double.parseDouble(pastDue30DAmount) + 1;
		amountPage.setOtherAmount(updatedAmount.toString());
		amountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyDigitalPAAlert(PaymentConstants.DIGITAL_PA_AMOUNT_BETWEEN_PAST_DUE_AND_TOTAL_DUE);

		// commented below part is removed from user story
		/*
		 * oneTimePaymentPage.clickamountIcon(); updatedAmount =
		 * Double.parseDouble(totalDueAmount)+1;
		 * amountPage.setOtherAmount(updatedAmount.toString());
		 * amountPage.clickUpdateAmount(); oneTimePaymentPage.verifyPageLoaded();
		 * oneTimePaymentPage.verifyDigitalPAAlert(PaymentConstants.
		 * DIGITAL_PA_AMOUNT_GREATER_THAN_TOTAL_DUE);
		 */

		oneTimePaymentPage.clickamountIcon();
		updatedAmount = Double.parseDouble(pastDue30DAmount) - 1;
		amountPage.setOtherAmount(updatedAmount.toString());
		amountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyDigitalPAAlert(PaymentConstants.DIGITAL_PA_AMOUNT_LESS_THAN_PAST_DUE);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void testPCMBladeWithOutStoredPaymentMethods(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US445060-Angular 6 PCM-blade implementation  UI & redirection");
		Reporter.log("Data Condition | Any PAH /Standard User ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");

		Reporter.log(
				"Step 4. launch url 'http://eservicehtml.eservice.tmobile.com/blade0' |Payment method blade should be displayed");
		Reporter.log(
				"Step 5. verify chevron  and 'Add payment method ' on right hand side of blade | chevron  and 'Add payment method ' text should be displayed");
		Reporter.log("Step 6: click on chevron| PCM spoke page should be displayed ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
		SpokePage otpSpokePage = new SpokePage(getDriver());
		oneTimePaymentPage.verifyArrowRight();
		oneTimePaymentPage.verifyPaymentBaldeHeaderWithOutPaymentMethod();
		oneTimePaymentPage.clickArrowRight();
		otpSpokePage.verifyPageLoaded();
	}

	/**
	 * US445060 Angular 6 PCM-blade implementation UI & redirection
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPCMBlade(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US445060-Angular 6 PCM-blade implementation  UI & redirection");
		Reporter.log("Data Condition | Any PAH /Standard User ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Launch the application | Application Should be Launched");
		Reporter.log("Step 2. Login to the application | User Should be login successfully");
		Reporter.log("Step 3. Verify Home page | Home page should be displayed");

		Reporter.log(
				"Step 4. launch url 'http://eservicehtml.eservice.tmobile.com/blade0' |Payment method blade should be displayed");
		Reporter.log(
				"Step 5. verify chevron  and Last 4 digits of card/Account number preceded with '****' on right hand side of blade | chevron  and Last 4 digits of card/Account number should be displayed");

		Reporter.log("Step 8: click on chevron| PCM spoke page should be displayed ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
		SpokePage otpSpokePage = new SpokePage(getDriver());
		oneTimePaymentPage.verifyPaymentMethodBlade();
		oneTimePaymentPage.verifyArrowRight();
		oneTimePaymentPage.verifyPaymentMethodCardOrAccountNumber();
		oneTimePaymentPage.clickArrowRight();
		otpSpokePage.verifyPageLoaded();
	}

	/**
	 * DE224281 E1 (FDP) - Unable to Cancel the payment and seeing error "Operation
	 * Failed"
	 *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testcancelFutureDatePayment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :test Verify Balance on Blling and OTP Page Match with HomePage");
		Reporter.log("Data Condition - suspended account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. capture balance on  Home page | balance should be captured");
		Reporter.log("5: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log(
				"6: verify balance on billing page  match with balance on homepage | balance on billing page should match with balance on homepage");
		Reporter.log("7: Click on make payment link | OTP page should be displayed");
		Reporter.log(
				"8: verify balance on OTP page  match with balance on homepage | balance on OTP page should match with balance on homepage");
		Reporter.log("================================");

		Reporter.log("Actual Result:");
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickDateIcon();
		oneTimePaymentPage.verifyFutureDatePage();
		oneTimePaymentPage.selectDate();
		oneTimePaymentPage.clickUpdateBtn();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.fillCardInfo(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();

		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(myTmoData.getPayment().getCardNumber());
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage oTPConfirmationPage = new OTPConfirmationPage(getDriver());
		oTPConfirmationPage.verifyPageLoaded();
		oTPConfirmationPage.clickReturnToHome();
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();

		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.cancelScheduledFDP();
	}

	/**
	 * DE214913 Home page shows balance and on Billing its showing as $0.00 for
	 * suspended customers
	 *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testBalanceonBllingandOTPPageMatchwithHomePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :test Verify Balance on Blling and OTP Page Match with HomePage");
		Reporter.log("Data Condition - suspended account - Brite Bill");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. capture balance on  Home page | balance should be captured");
		Reporter.log("5: Click on Billing link | Brite Bill page should be displayed");
		Reporter.log(
				"6: verify balance on billing page  match with balance on homepage | balance on billing page should match with balance on homepage");
		Reporter.log("7: Click on make payment link | OTP page should be displayed");
		Reporter.log(
				"8: verify balance on OTP page  match with balance on homepage | balance on OTP page should match with balance on homepage");
		Reporter.log("================================");

		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		String homebal = homePage.getBalanceDueAmount();
		homePage.clickBillingLink();
		BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
		billAndPaySummaryPage.verifyPageLoaded();
		billAndPaySummaryPage.verifyBalanceonBllingPageMatchwithHomePage(homebal);
		billAndPaySummaryPage.clickMakeAPaymentLinkOnBBPage();
		OneTimePaymentPage otpPage = new OneTimePaymentPage(getDriver());
		otpPage.verifyPageLoaded();
		otpPage.verifyBalanceonOTPPageMatchwithHomePage(homebal);
	}

	/**
	 * DE224594 E1(FDP) - Within the same session after setting up the FDP, PA link
	 * is displayed as Enabled.
	 *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testNOPAlinkAfterFDPSetup(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :DE224594 E1(FDP) - Within the same session after setting up the FDP, PA link is displayed as Enabled.");
		Reporter.log("Data Condition - MSDSIN eligible for PA,FDP");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3. click on paybill | one time payment page should be displayed");
		Reporter.log("4: Click on amount icon | OTP amount page should be displayed");
		Reporter.log("5: enter other ammount and update | amount should be updated");
		Reporter.log("6: click date blade  | Date page should be displayed");
		Reporter.log("7: select future date and update  | Date should be updated");
		Reporter.log("8: click on payment method blade  | OTP spoke page should be displayed");
		Reporter.log("9: add card detaild and click on continue | card details should be updated");
		Reporter.log("10: click on agree and submit | OTP confirmation page should be displayed");
		Reporter.log("11: click on return home | home page should be displayed");
		Reporter.log("12: navigate to otp page and verify PA link | PA link should not be displayed");

		Reporter.log("================================");

		Reporter.log("Actual Result:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.selectFutureDate();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), spokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.clickReturnToHome();
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyPaymentArrangementLinkNotDisplayed();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.verifyCancelModal();
		oneTimePaymentPage.clickContinueBtnCancelModal();
		homePage.verifyPageLoaded();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.cancelScheduledFDP();

	}

	/**
	 * DE229519 Reg9 - Bank is NOT displayed when its on Negative file
	 *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testNegativeFileForBankPaymentMethod(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :DE229519 Reg9 - Bank is NOT displayed when its on Negative file");
		Reporter.log("Data Condition - any MSDSIN with negative file on bank");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Navigate to OTP page | OTP page  should be captured");
		Reporter.log("5: Click on payment blade | payment spoke page should be displayed");
		Reporter.log("6: verify negative file for Bank | negative file for Bank should be displayed");

		Reporter.log("================================");

		Reporter.log("Actual Result:");
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyPageLoaded();
		spokePage.verifyInvalidBankErrorMsg();
		spokePage.verifyAddBankDisabled();

	}

	/**
	 * DE230309 Offline stack_ informative text underneath the balance "Total
	 * Balance" on the OTP landing page is displayed when there is only current
	 * balance account
	 *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testTotalbalanceTextUnderneathTheBalance(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :DE229519 Reg9 - Bank is NOT displayed when its on Negative file");
		Reporter.log("Data Condition - any MSDSIN with negative file on bank");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Navigate to OTP page | OTP page  should be captured");
		Reporter.log("5: Click on payment blade | payment spoke page should be displayed");
		Reporter.log("6: verify negative file for Bank | negative file for Bank should be displayed");

		Reporter.log("================================");

		Reporter.log("Actual Result:");
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		if (!editAmountPage.verifyTotalBalanceequaltoPastDueBal()) {
			editAmountPage.clickCancelBtn();
			oneTimePaymentPage.verifyPageLoaded();
			oneTimePaymentPage.verifyTextUnderTotalBal();

		}

	}

	/**
	 * US524276: Angular 1.5 PCM-Landing-blade claim treatment.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyClaimTreatmentNoClaim(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US524276: Angular 1.5 PCM-Landing-blade claim treatment.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the One time payment page | One time payment page should be displayed");
		Reporter.log(
				"5. Check for the 'Add a payment method' for no claims | Should be able to see the 'Add a payment method'");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyPaymentMethod();
	}

	/**
	 * US524276: Angular 1.5 PCM-Landing-blade claim treatment.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyClaimTreatmentWithClaim(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US524276: Angular 1.5 PCM-Landing-blade claim treatment.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the One time payment page | One time payment page should be displayed");
		Reporter.log(
				"5. Check for the claimed payment method on blade | Should be able to see the Claimed payment method displayed on blade");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifySavedPaymentMethod();
	}

	/**
	 * US524290: Angular 6 PCM Landing page-blade claim treatment.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyAngular6ClaimTreatmentNoClaim(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US524290: Angular 6 PCM Landing page-blade claim treatment.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the PCM payment blade page | PCM payment blade page should be displayed");
		Reporter.log(
				"5. Check for the 'Add a payment method' for no claims | Should be able to see the 'Add a payment method'");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
		oneTimePaymentPage.verifyNoSavedPaymentMethod();
	}

	/**
	 * US524290: Angular 6 PCM Landing page-blade claim treatment.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyAngular6ClaimTreatmentWithClaim(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US524290: Angular 6 PCM Landing page-blade claim treatment.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the PCM payment blade page | PCM payment blade page should be displayed");
		Reporter.log(
				"5. Check for the claimed payment method on blade | Should be able to see the Claimed payment method displayed on blade");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
		oneTimePaymentPage.verifySavedPaymentMethod();
	}

	/**
	 * US545276: Past Due in shop--OTP Landing page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyPastDueInShopOtpLandingPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US545276: Past Due in shop--OTP Landing page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop and make sure to have past due | Should be able to navigate to shop page");
		Reporter.log("5. Click on make a payment | Should be able to navigate to OTP page");
		Reporter.log(
				"6. Check for the Date Chevron | The Date Chevron should be disable and should able to make only the one time payment");
		Reporter.log(
				"7. Check for messaging displayed for 'Qualifying amount' | 'Once the past-due amount is paid, you’ll be eligible to continue shopping.' should be displayed");
		Reporter.log(
				"8. Check for messaging displayed for less than 'Qualifying amount' | 'This payment will not make you eligible to continue shopping.' should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("Accessories");
		AccessoriesStandAlonePLPPage standAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		standAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		standAlonePLPPage.clickAddToCart(0);
		AccessoriesStandAloneReviewCartPage standAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		standAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		standAloneReviewCartPage.verifyMakePaymentCTAIsDisplayed();
		standAloneReviewCartPage.clickMakePaymentCTA();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyDateBladeIsDisabled();
		oneTimePaymentPage.verifyNoChevronOnDateBlade();
		oneTimePaymentPage.verifyOtpPageAlerts(PaymentConstants.SHOP_OTP_QUALIFYING_AMOUNT_MSG);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage amountPage = new OTPAmountPage(getDriver());
		amountPage.verifyPageLoaded();
		amountPage.setOtherAmount("1.02");
		amountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.verifyOtpPageAlerts(PaymentConstants.SHOP_OTP_LESS_THAN_QUALIFYING_AMOUNT_MSG);
	}

	/**
	 * US545338: Past Due in shop--Edit amount copy change
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyPastDueInShopOtpEditAmountPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US545338: Past Due in shop--Edit amount copy change");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop and make sure to have past due | Should be able to navigate to shop page");
		Reporter.log("5. Click on make a payment | Should be able to navigate to OTP page");
		Reporter.log(
				"7. Check for messaging displayed for 'Qualifying amount' | 'Once the past-due amount is paid, you’ll be eligible to continue shopping.' should be displayed");
		Reporter.log(
				"8. Check for messaging displayed for less than 'Qualifying amount' | 'This payment will not make you eligible to continue shopping.' should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("Accessories");
		AccessoriesStandAlonePLPPage standAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		standAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		standAlonePLPPage.clickAddToCart(0);
		AccessoriesStandAloneReviewCartPage standAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		standAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		standAloneReviewCartPage.verifyMakePaymentCTAIsDisplayed();
		standAloneReviewCartPage.clickMakePaymentCTA();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage amountPage = new OTPAmountPage(getDriver());
		amountPage.verifyPageLoaded();
		amountPage.verifyTextUnderTotalBal(PaymentConstants.SHOP_OTP_AMOUNT_PAGE_TOTAL_BAL);
		amountPage.verifyTextUnderPastDue(PaymentConstants.SHOP_OTP_AMOUNT_PAGE_PAST_DUE);
		amountPage.verifyTextUnderOther(PaymentConstants.SHOP_OTP_AMOUNT_PAGE_OTHER);

		amountPage.verifyWarningText(PaymentConstants.SHOP_OTP_QUALIFYING_AMOUNT_MSG);
		amountPage.setOtherAmount("1.02");
		amountPage.verifyWarningText(PaymentConstants.SHOP_OTP_LESS_THAN_QUALIFYING_AMOUNT_MSG);
	}

	/**
	 * US545317: Past Due in shop--OTP Confirmation page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyPastDueInShopOtpConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US545317: Past Due in shop--OTP Confirmation page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop and make sure to have past due | Should be able to navigate to shop page");
		Reporter.log("5. Click on make a payment | Should be able to navigate to OTP page");
		Reporter.log(
				"6. Check for the Date Chevron | The Date Chevron should be disable and should able to make only the one time payment");
		Reporter.log(
				"7. Check for messaging displayed for 'Qualifying amount' | 'Once the past-due amount is paid, you’ll be eligible to continue shopping.' should be displayed");
		Reporter.log(
				"8. Check for messaging displayed for less than 'Qualifying amount' | 'This payment will not make you eligible to continue shopping.' should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("Accessories");
		AccessoriesStandAlonePLPPage standAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		standAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		standAlonePLPPage.clickAddToCart(0);
		AccessoriesStandAloneReviewCartPage standAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		standAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		standAloneReviewCartPage.verifyMakePaymentCTAIsDisplayed();
		standAloneReviewCartPage.clickMakePaymentCTA();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), spokePage);
		oneTimePaymentPage.verifyPageLoaded();
		/*oneTimePaymentPage.clickAgreeAndSubmit();
		OTPConfirmationPage confirmationPage = new OTPConfirmationPage(getDriver());
		confirmationPage.verifyPageLoaded();
		confirmationPage.verifyContinueShoppingBtn();
		confirmationPage.clickContinueShoppingBtn();
		standAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();*/
	}

	/**
	 * US578983 OTP Landing Balance Suppression Details *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testOTPBalanceSuppressedforNonMasterUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US578983: OTP Landing Balance Suppression Details");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the One time payment page | One time payment page should be displayed");
		Reporter.log(
				"5. verify balance amount is suppressed in total balance ,last payment text| balance amount should be suppressed in total balance ,last payment text");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyBalanceAmountSuppressedinBalancetextforNonMasterUser(myTmoData);
		oneTimePaymentPage.verifyBalanceAmountSuppressedinLastpaymenttextforNonMasterUser(myTmoData);
	}

	/**
	 * US579620 Update the OTP suppression logic to hide stored payment methods for
	 * non master users Detail US57967 Update the OTP suppression logic to hide the
	 * save payment method option for non master users Detail
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testOTPSuppressionLogicforstoredPayment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : US579620:US57967  OTP suppression logic to hide the save payment method option for non master users Detail");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Go to the One time payment page | One time payment page should be displayed");
		Reporter.log(
				"5. verify  suppression logic to hide stored payment methods| stored payment methods should be suppressed");
		Reporter.log("================================");
		Reporter.log("Actual Steps:");

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyPageLoaded();
		spokePage.verifySavedPaymentsSuppressed(myTmoData);
		spokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyPageHeader(PaymentConstants.Add_card_Header);
		addCardPage.verifyCardPageLoaded("Card information");
		addCardPage.verifySavePaymentMethodCheckBoxSuppressed(myTmoData);
		addCardPage.clickBackBtn();
		spokePage.clickAddBank();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		addBankPage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);
		addBankPage.verifySavePaymentMethodCheckBoxSuppressed(myTmoData);

	}

}
