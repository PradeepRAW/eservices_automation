package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tmobile.eservices.qa.shop.ShopConstants;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.CatalogApiV4;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class Catalogv4ApiTest extends CatalogApiV4{
	
	public Map<String, String> tokenMap;
	/**
	/**
	 * UserStory# Description:US521441 PDP
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 	
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","getAccessoryPDPV4",Group.SHOP, Group.APIREG  })
	public void getAccessoryPDPV4ThroughJWT(ControlTestData data, ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception {
		Reporter.log("TestName: get Accessory List PDP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the Accessory list should get from service response");
		Reporter.log("Step 2: Verify expected Accessory should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CATALOG+"productsCatalog_AccessoryPDPV4_US521441.txt");
		tokenMap.put("sku", apiTestData.getSku());
		String latestRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(latestRequest, "getAccessoryPDPV4ThroughJWT");
		System.out.println("requestBody = "+latestRequest);
		Response response =retrieveProductsUsingPOST(apiTestData, latestRequest,tokenMap);
		
		System.out.println("Response :"+response.body().asString());
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
		Reporter.log("Response :"+response.asString());
		
        JsonPath jsonPath= new JsonPath(response.asString());
		
		//Assert.assertEquals(jsonPath.get("data[0].boxContents.name"), "In the box");
		//Reporter.log("Box contents are displayed");
		
		//Assert.assertNotNull(jsonPath.get("data[0].modelName"), "Model name is absent");
		//Reporter.log("Model name is present");
		
		//Assert.assertNotNull(jsonPath.get("data[0].productAvailability.availabilityStatus"), "Availability status is absent");
		//Reporter.log("Availability status is present");
		
	}
	
	/**
	/**
	 * UserStory# Description:US521441 PLP
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 	
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","getAccessoryPLPV4",Group.SHOP, Group.APIREG  })
		public void getAccessoryPLPV4WithJwt(ControlTestData data, ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception {
			Reporter.log("TestName: get Accessory List PLP");
			Reporter.log("Data Conditions:MyTmo registered misdn.");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("Step 1: Get all the Accessory list should get from service response");
			Reporter.log("Step 2: Verify expected Accessory should be in the response list");
			Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
			Reporter.log("================================");
			Reporter.log("Actual Result:");
			String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CATALOG +"productsCatalog_AccessoryPLPV4_US521441.txt");
			tokenMap.put("sku", apiTestData.getSku());
			String latestRequest = prepareRequestParam(requestBody, tokenMap);
			System.out.println("requestBody = "+latestRequest);
			logRequest(latestRequest, "getAccessoryPLPV4WithJwt");
			Response response =retrieveProductsUsingPOST(apiTestData, latestRequest,tokenMap);
			System.out.println("Response :"+response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Response Status:"+response.statusCode());
			Reporter.log("Response :"+response.asString());
            JsonPath jsonPath= new JsonPath(response.asString());
			
			//Assert.assertEquals(jsonPath.get("data[0].boxContents.name"), "In the box");
			//Reporter.log("Box contents are displayed");
			
			//Assert.assertNotNull(jsonPath.get("data[0].modelName"), "Model name is absent");
			//Reporter.log("Model name is present");
			
			//Assert.assertNotNull(jsonPath.get("data[0].productAvailability.availabilityStatus"), "Availability status is absent");
			//Reporter.log("Availability status is present");
		}
		
	/**
	/**
	 * UserStory# Description:US420782
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 	
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv41","getDevicePLPV4",Group.SHOP,Group.APIREG  })
	public void getDevicePLPV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: get Device List PLP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the Device list should get from service response");
		Reporter.log("Step 2: Verify expected Device should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CATALOG +"productsCatalog-DevicePLP.txt");
		System.out.println("requestBody = "+requestBody);
		Response response =retrieveProductsUsingPOST(apiTestData, requestBody, tokenMap);
		System.out.println("Response :"+response.asString());
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
		Reporter.log("Response :"+response.asString());
		
	}
	
	/**
	/**
	 * UserStory# Description:US420782
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 	
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv41","getDevicePDPV4",Group.SHOP,Group.APIREG  })
	public void getDevicePDPV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: get Device List PDP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the Device list should get from service response");
		Reporter.log("Step 2: Verify expected Device should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CATALOG +"productsCatalog-DevicePDP.txt");
		System.out.println("requestBody = "+requestBody);
		Response response =retrieveProductsUsingPOST(apiTestData, requestBody, tokenMap);
		System.out.println("Response :"+response.asString());
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
		Reporter.log("Response :"+response.asString());
		
	}
	
	/**
	/**
	 * UserStory# Description:US420782
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 	
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","getDevicePDPV4",Group.SHOP,Group.SPRINT  })
	public void getAccessoryPDPV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: get Device List PDP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the Device list should get from service response");
		Reporter.log("Step 2: Verify expected Device should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CATALOG +"productsCatalog-DevicePDP.txt");
		System.out.println("requestBody = "+requestBody);
		Response response =retrieveProductsUsingPOST(apiTestData, requestBody, tokenMap);
		System.out.println("Response :"+response.asString());
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
		Reporter.log("Response :"+response.asString());
		
	}
	
	/**
	/**
	 * UserStory# Description:US418072:Modify EOS /catalog/products API to return SIM Starter Kit details
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 	
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","getSimStarterKitV4",Group.SHOP,Group.SPRINT  })
	public void getSimStarterKitV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: get SimStarterKit List PDP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the SimStarterKit list should get from service response");
		Reporter.log("Step 2: Verify expected SimStarterKit should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CATALOG +"CatalogProducts_SimStarterKit.txt");
		System.out.println("requestBody = "+requestBody);
		Response response =retrieveProductsUsingPOST(apiTestData, requestBody, tokenMap);
		System.out.println("Response :"+response.asString());
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
		Reporter.log("Response :"+response.asString());
		
	}
	
	/**
	/**
	 * UserStory# Description:US420782
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 	
	 */	
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","getFilterOptionsUsingGETV4",Group.SHOP,Group.SPRINT  })
	public void testCatalogFilterPhoneManuFacturerV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get phone manufacture List");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the device manufacturers|Manufacturers list should get from service response");
		Reporter.log("Step 2: Verify expected Manufacturers|Manufacturers should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String, String> tokenMap = new HashMap<String, String>();
		Response response =getFilterOptionsUsingGET(apiTestData, "PHONES", "", tokenMap);
		String operationName="testCatalogFilterPhoneManuFacturer_V4";
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Response Status:"+response.statusCode());
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonNode deviceFamilyInfoPathNode = jsonNode.path("deviceFamilyInfo");
				JsonNode parentNode = deviceFamilyInfoPathNode.path("data");
				Assert.assertNotNull(parentNode);
				//String prodStatus = parentNode.get(0).asText("productStatus");
				//Assert.assertNotNull(prodStatus);
				//tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		
		/*Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
		List<String> phones=response.path("data.values.value[0]");
		Reporter.log(phones.toString());
		Assert.assertEquals(phones.toString(),"[Apple, T-Mobile, Samsung, LG, Motorola, Alcatel]");*/
		
	}
	
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","retrieveGroupedProductsUsingPOSTV4",Group.SHOP,Group.SPRINT  })
	public void testRetrieveGroupedProductsUsingPOSTV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: fet Products Catalog List by family");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the Products Catalog list should get from service response");
		Reporter.log("Step 2: Verify expected Products Catalog list should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="productsCatalog-FamilyFilterV4";
		tokenMap=new HashMap<String,String>();
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CATALOG +"productsCatalog-FamilyFilterV3.txt");
		logRequest(requestBody, operationName);
		Response response =retrieveGroupedProductsUsingPOST(apiTestData,requestBody,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonNode parentNode = jsonNode.path("paginationMetadata");
				Assert.assertNotNull(parentNode);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		
	}

	
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","getFilterOptionsUsingGETV4",Group.SHOP,Group.SPRINT  })
	public void testCatalogFilterTabletsManuFacturerV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get Tablets manufacture List");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the tablet manufacturers|Manufacturers list should get from service response");
		Reporter.log("Step 2: Verify expected Manufacturers|Manufacturers should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String, String> tokenMap = new HashMap<String, String>();
		Response response =getFilterOptionsUsingGET(apiTestData, "TABLETS", "", tokenMap);
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
		List<String> tablets=response.path("data.values.value[0]");
		Reporter.log(tablets.toString());
		Assert.assertEquals(tablets.toString(),"[Apple, Samsung, LG, Alcatel]");
	}
	
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","getFilterOptionsUsingGETV4",Group.SHOP,Group.SPRINT  })
	public void testCatalogFilterWearablesManuFacturerV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get Wearables manufacture List");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the Wearables manufacturers|Manufacturers list should get from service response");
		Reporter.log("Step 2: Verify expected Manufacturers|Manufacturers should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String, String> tokenMap = new HashMap<String, String>();
		Response response =getFilterOptionsUsingGET(apiTestData, "WEARABLES", "", tokenMap);
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
		List<String> wearables=response.path("data.values.value[0]");
		Reporter.log(wearables.toString());
		Assert.assertEquals(wearables.toString(),"[Apple, Samsung]");
	}
	
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","getFilterOptionsUsingGETV4",Group.SHOP,Group.SPRINT })
	public void testCatalogFilterHotspotsManuFacturerV4(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get hotspot manufacture List");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the Hotspots manufacturers|Manufacturers list should get from service response");
		Reporter.log("Step 2: Verify expected Manufacturers|Manufacturers should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String, String> tokenMap = new HashMap<String, String>();
		Response response =getFilterOptionsUsingGET(apiTestData, "HOTSPOTS", "", tokenMap);
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
		List<String> hotspots=response.path("data.values.value[0]");
		Reporter.log(hotspots.toString());
		Assert.assertEquals(hotspots.toString(),"[T-Mobile, Alcatel]");
	}
	
	/**
	/**
	 * UserStory# Description:US524728 PDP
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 	
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","getAccessoryPDPV4",Group.SHOP, Group.APIREG })
	public void getDevicePDPV4ProductSpecification(ControlTestData data, ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception {
		Reporter.log("TestName: get product List PDP device specification");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the product list should get from service response");
		Reporter.log("Step 2: Verify expected product should be in the response list");
		Reporter.log("Step 3: Verify Success services response code and verify each specification|Response code should be 200 OK.");
		Reporter.log("Step 4: Verify SKU level items first|SKU items should be first then product level items");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		String operationName="getDeviceSpecifications";
		tokenMap=new HashMap<String,String>();
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CATALOG +"getDeviceSpecificationDetails.txt");
		logRequest(requestBody, operationName);
		System.out.println("requestBody = "+requestBody);
		Response response =retrieveProductsUsingPOST(apiTestData, requestBody,tokenMap);
		
		System.out.println("Response :"+response.asString());
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
		Reporter.log("Response :"+response.asString());
		
        JsonPath jsonPath= new JsonPath(response.asString());
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[7].name"), "Battery Standby Time");
		Reporter.log("Battery Standby Time are displayed");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[8].name"), "Battery Talk Time");
		Reporter.log("Battery Talk Time is present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[9].name"), "Ports");
		Reporter.log("Battery Talk Time is present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[10].name"), "Connectivity");
		Reporter.log("Connectivity details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[11].name"), "Processor");
		Reporter.log("Processor details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[12].name"), "Operating System");
		Reporter.log("Operating System details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[13].name"), "Ram");
		Reporter.log("RAM details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[14].name"), "Maximum Expandable Memory");
		Reporter.log("Maximum Expandable Memory details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[15].name"), "Wireless Network Technology Generations");
		Reporter.log("Wireless Network Technology details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[16].name"), "Supported Email Platforms");
		Reporter.log("Email Platforms details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[17].name"), "Hearing Aid Compatibility");
		Reporter.log("WEA Capable details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[18].name"), "WEA Capable");
		Reporter.log("Mobile Hotspot Capable details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[19].name"), "Mobile Hotspot Capable");
		Reporter.log("Frequency details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[20].name"), "Cameras");
		Reporter.log("Frequency details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[21].name"), "Frequency");
		Reporter.log("Frequency details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[0].name"), "Display");
		Reporter.log("Display details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[1].name"), "Display resolution");
		Reporter.log(" Display resolution details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[2].name"), "Weight");
		Reporter.log("Weight details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[3].name"), "Length");
		Reporter.log("Length details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[4].name"), "Height");
		Reporter.log("Height details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[5].name"), "Width");
		Reporter.log("Width details are present");
		
		Assert.assertEquals(jsonPath.get("data[13].specifications[6].name"), "Battery Description");
		Reporter.log("Battery Description details are present");
		
	}
	
	   @Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","retrieveGroupedProductsUsingPOSTV4",Group.SHOP,Group.SPRINT  })
		public void US628645testMPMProductcatalogAPI_productSubtype_SKU(ControlTestData data, ApiTestData apiTestData) throws Exception {
			Reporter.log("TestName: fet Products Catalog List by family");
			Reporter.log("Data Conditions:MyTmo registered misdn.");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("Step 1: Get all the Products Catalog list should get from service response");
			Reporter.log("Step 2: Verify expected Products Catalog list should be in the response list");
			Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
			Reporter.log("================================");
			Reporter.log("Actual Result:");
			String operationName="catalogAPI_productSubtype_SKU";
			String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CATALOG +"CatalogAPI_productSubtype_SKU.txt");
			tokenMap=new HashMap<String,String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("password", apiTestData.getPassword());
		//	tokenMap.put("sku", apiTestData.getSku());
		
			logRequest(requestBody, operationName);
		//	Response response =retrieveGroupedProductsUsingPOST(apiTestData,requestBody,tokenMap);
			Response response =retrieveGroupedProductApi(apiTestData,requestBody,tokenMap);
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				 JsonPath jsonPath= new JsonPath(response.asString());
				Assert.assertTrue(response.statusCode()==200);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {
					
					Assert.assertNotNull(jsonPath.get("productType"), "productType is not COMPATIBLESERVICES");
					Reporter.log("productType is COMPATIBLESERVICES");
				}
			} else {
				failAndLogResponse(response, operationName);
			}
			
		}
	   
	   @Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","retrieveGroupedProductsUsingPOSTV4",Group.SHOP,Group.SPRINT  })
		public void US628645testMPMProductcatalogAPI_productSubtype_TAC(ControlTestData data, ApiTestData apiTestData) throws Exception {
			Reporter.log("TestName: fet Products Catalog List by family");
			Reporter.log("Data Conditions:MyTmo registered misdn.");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("Step 1: Get all the Products Catalog list should get from service response");
			Reporter.log("Step 2: Verify expected Products Catalog list should be in the response list");
			Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
			Reporter.log("================================");
			Reporter.log("Actual Result:");
			String operationName="catalogAPI_productSubtype_SKU";
			String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CATALOG +"CatalogAPI_productSubtype_TAC.txt");
			tokenMap=new HashMap<String,String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("password", apiTestData.getPassword());
		//	tokenMap.put("sku", apiTestData.getSku());
		
			logRequest(requestBody, operationName);
		//	Response response =retrieveGroupedProductsUsingPOST(apiTestData,requestBody,tokenMap);
			Response response =retrieveGroupedProductApi(apiTestData,requestBody,tokenMap);
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				 JsonPath jsonPath= new JsonPath(response.asString());
				Assert.assertTrue(response.statusCode()==200);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {
					
					Assert.assertNotNull(jsonPath.get("productType"), "productType is not DEVICE");
					Reporter.log("productType is DEVICE");
				}
			} else {
				failAndLogResponse(response, operationName);
			}
			
		}

	   @Test(dataProvider = "byColumnName", enabled = true, groups = { "catalogapiv4","retrieveGroupedProductsUsingPOSTV4",Group.SHOP,Group.SPRINT  })
		public void US628645testMPMProductcatalogAPI_productSubtype_SOC(ControlTestData data, ApiTestData apiTestData) throws Exception {
			Reporter.log("TestName: fet Products Catalog List by family");
			Reporter.log("Data Conditions:MyTmo registered misdn.");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("Step 1: Get all the Products Catalog list should get from service response");
			Reporter.log("Step 2: Verify expected Products Catalog list should be in the response list");
			Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
			Reporter.log("================================");
			Reporter.log("Actual Result:");
			String operationName="catalogAPI_productSubtype_SKU";
			String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CATALOG +"CatalogAPI_productSubtype_SOC.txt");
			tokenMap=new HashMap<String,String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("password", apiTestData.getPassword());
		//	tokenMap.put("sku", apiTestData.getSku());
		
			logRequest(requestBody, operationName);
		//	Response response =retrieveGroupedProductsUsingPOST(apiTestData,requestBody,tokenMap);
			Response response =retrieveGroupedProductApi(apiTestData,requestBody,tokenMap);
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				 JsonPath jsonPath= new JsonPath(response.asString());
				Assert.assertTrue(response.statusCode()==200);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {
					
					Assert.assertNotNull(jsonPath.get("productType"), "productType is not SERVICE");
					Reporter.log("productType is SERVICE");
				}
			} else {
				failAndLogResponse(response, operationName);
			}
			
		}

}
