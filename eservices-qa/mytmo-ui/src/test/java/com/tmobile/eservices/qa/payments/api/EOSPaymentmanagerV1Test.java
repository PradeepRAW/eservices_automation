package com.tmobile.eservices.qa.payments.api;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.EOSCommonMethods;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EOSPaymentmanager;

import io.restassured.response.Response;

public class EOSPaymentmanagerV1Test extends  EOSCommonMethods{
	
	
	
	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true,groups = { "NewAPI" })
	public void testEosgetsubscriberV1(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		EOSPaymentmanager pm=new EOSPaymentmanager();
	
		
		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);
		
		if (getjwt != null) {
			Response subscriber=pm.getsubscriberinforesponse(getjwt);
			checkexpectedvalues(subscriber, "statusCode", "100");
			checkexpectedvalues(subscriber, "statusMessage", "Success");
			
	
		}
}
	
	
	

	@Test(dataProvider = "byColumnName", enabled = true,groups = { "NewAPI" })
	public void testEosgetPAV1(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetBillList");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the billlist of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		EOSPaymentmanager pm=new EOSPaymentmanager();
		//EOSCommonMethods ecm=new EOSCommonMethods();
		
		String[] getjwt =getjwtfromfiltersforAPI(apiTestData);
		
		if (getjwt != null) {
			Response paresponse=pm.getPAinformationresponse(getjwt);
			checkexpectedvalues(paresponse, "statusCode", "100");
			checkexpectedvalues(paresponse, "statusMessage","Success");
			
		
		
	}
	
}
	


	@Test(dataProvider = "byColumnName", enabled = true,groups = { "NewAPI" })
	public void testEosgetschedulepaymentV1(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetBillList");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the billlist of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		EOSPaymentmanager pm=new EOSPaymentmanager();
		//EOSCommonMethods ecm=new EOSCommonMethods();
		
		String[] getjwt =getjwtfromfiltersforAPI(apiTestData);
		
		if (getjwt != null) {
			Response schedulepayment=pm.getsschedulepaymentresponse(getjwt);
			checkexpectedvalues(schedulepayment, "statusCode", "100");
			checkexpectedvalues(schedulepayment, "statusMessage", "Success");
			
		
		
	}
	
}
	

	
}