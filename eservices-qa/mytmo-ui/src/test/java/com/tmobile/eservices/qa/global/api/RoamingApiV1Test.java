package com.tmobile.eservices.qa.global.api;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.RoamingApiV1;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class RoamingApiV1Test extends RoamingApiV1 {
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "RoamingApi", "testGetDeviceList", Group.GLOBAL,Group.APIREG })
	public void testGetDeviceList(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Response rs = getDeviceList(apiTestData);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			Reporter.log(rs.body().asString());
		}
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "RoamingApi", "testGetCountryList", Group.GLOBAL,Group.APIREG })
	public void testGetCountryList(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Response rs = getDeviceList(apiTestData);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			Reporter.log(rs.body().asString());
		}
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "RoamingApi", "testGetCruiseList", Group.GLOBAL,Group.APIREG })
	public void testGetCruiseList(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Response rs = getDeviceList(apiTestData);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			Reporter.log(rs.body().asString());
		}
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "RoamingApi", "testGetRoamingDetailsWithCountryID", Group.GLOBAL,Group.APIREG })
	public void testGetRoamingDetailsWithCountryID(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Response rs = getDeviceList(apiTestData);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			Reporter.log(rs.body().asString());
		}
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "RoamingApi", "testGetRoamingDetailsWithCruiseID", Group.GLOBAL,Group.APIREG })
	public void testGetRoamingDetailsWithCruiseID(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Response rs = getDeviceList(apiTestData);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			Reporter.log(rs.body().asString());
		}
	}
}
