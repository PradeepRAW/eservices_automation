package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.global.ChangePasswordPage;
import com.tmobile.eservices.qa.pages.global.ChooseMethodOptionsPage;
import com.tmobile.eservices.qa.pages.global.ForgotPasswordPage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.global.ResetSecurityQuestions;

public class ResetSecurityQuestionsTest extends GlobalCommonLib {
	
	/**
	 * Verify user is able to reset the password using Security Questions
	 * 
	 * @param myTmoData
	 * @param controlTestData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void forgotPasswordBySecurityQuestions(MyTmoData myTmoData, ControlTestData controlTestData) {
		logger.info("forgotPasswordBySecurityQuestions method called in MyTMORegistrationTest");
		Reporter.log("Test Case : Verify user is able to reset the password using Security Questions");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Click on forgot password button | Forgot password page should be displayed");
		Reporter.log(
				"4. Enter accout information(Email or Phone) and click next | User should be able to enter Zip code");
		Reporter.log("5. Enter zip code and click next | Choose method options screen should be displayed");
		Reporter.log(
				"6. Choose option type Security Questions and click on next button | User should be able to select questions and answers");
		Reporter.log("7. click next button | Change password page should be displayed");
		Reporter.log(
				"8. Enter new password, confirm password and click on submit | Password should be saved successfully");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		logger.info("forgotPasswordBySequestions method called in MyTMORegistrationTest");
		ChooseMethodOptionsPage methodOptionsPage = loginAndNavigateToMethodOptions(myTmoData);
		ChangePasswordPage changePasswordPage = new ChangePasswordPage(getDriver());
		String optionType = myTmoData.getOptionType();
		methodOptionsPage.chooseVerificationMethod(optionType);
		changePasswordPage.submitNextButton();
		ResetSecurityQuestions resetSecurityQuestions = new ResetSecurityQuestions(getDriver());
		resetSecurityQuestions.submitResetSecurityQuestions(myTmoData);
		Reporter.log("User is able to select questions and answers");
		// changePasswordPage.performChangePassword(myTmoData.getNewPassword());
		Reporter.log("Password is saved successfully");
	}

	/**
	 * TC018_MyTMO_IW_Reset Password_Security Questions TestID:3273895
	 * 
	 * @param controlTestData
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { })
	public void verifyResetpasswordWithSecurityquestions(ControlTestData controlTestData, MyTmoData myTmoData) {
		logger.info("VerifyResetpasswordWithSecurityquestions method called in ForgetPasswordPageTest");
		Reporter.log("Test Case Name : Verify user is able to Reset Password with Security Question");
		Reporter.log("Test Data : Any PAH/Full/Standard User ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be launched");
		Reporter.log("2. Click On forgot Password | Forgot Password Screen should be displayed");
		Reporter.log(
				"3. Enter accout information(Email or Phone) and click next | User should be able to enter Zip code");
		Reporter.log("4. Enter zip code and click next | Choose method options screen should be displayed");
		Reporter.log(
				"5. Choose option type Security Questions and click on next button | User should be able to select questions and enter answers");
		Reporter.log("6. click submit button | Change password page should be displayed");
		Reporter.log(
				"7. Enter new password, confirm password and click on submit | Password should be saved successfully");
		Reporter.log("Actual Output:");

		getTMOURL(myTmoData);
		unlockAccount(myTmoData);
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.clickForgotPassword();
		ForgotPasswordPage forgotPasswordPage = new ForgotPasswordPage(getDriver());
		forgotPasswordPage.getResetPasswordText();
		forgotPasswordPage.enterEmailOrPhone(myTmoData.getLoginEmailOrPhone());
		forgotPasswordPage.clickNextButton();
		forgotPasswordPage.enterZipCode(myTmoData.getSignUpData().getZipCode());
		forgotPasswordPage.clickNextButton();
		ChooseMethodOptionsPage methodOptionsPage = new ChooseMethodOptionsPage(getDriver());
		methodOptionsPage.getChooseMethodText();
		methodOptionsPage.chooseVerificationMethod(ChooseMethodOptionsPage.SECURITY_QUESTIONS_MESSGE_TYPE);
		methodOptionsPage.clickNextBtn();
		ResetSecurityQuestions questions = new ResetSecurityQuestions(getDriver());
		questions.resetSeqText();
		questions.submitResetSecurityQuestions(myTmoData);
	}
}
