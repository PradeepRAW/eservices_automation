/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.NewAutopayLandingPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayTncPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

/**
 * @author Sam Iurkov
 *
 */
public class NewAutopayTncPageTest extends PaymentCommonLib {

	/**
	 * US283055 AutoPay terms and conditions page
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Regular account. Autopay OFF
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "autopay7" })
	public void testnewAutoPayTnCPageDisplayed(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Verify AutoPay terms and conditions page");
		Reporter.log("Data Conditions - Regular account. Autopay eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to Auto Pay landing page  | Auto Pay landing Page should be displayed");
		Reporter.log("Step 4: Click link \"terms and conditions\" | Verify termsAndConditions page is loaded");
		Reporter.log("Step 4.1 Verify header Terms and Conditions is present");
		Reporter.log("Step 4.2 Verify Back CTA is present");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		NewAutopayTncPage tnCPage = navigateNewAutopayTncpage(myTmoData);
		tnCPage.clickBackButton();
		NewAutopayLandingPage nAPl = new NewAutopayLandingPage(getDriver());
		nAPl.verifyPageLoaded();

	}
}
