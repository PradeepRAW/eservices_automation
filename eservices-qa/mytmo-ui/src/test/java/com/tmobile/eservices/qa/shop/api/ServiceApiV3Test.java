package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.shop.ShopConstants;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.CartApiV4;
import com.tmobile.eservices.qa.api.eos.ServicesApiV3;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class ServiceApiV3Test extends ServicesApiV3{
	
	/**
	/**
	 * UserStory# Description:US418059
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "socapiv3","getServicesForSale",Group.SHOP,Group.PROGRESSION })
	public void testGetServicesForSale(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getServicesForSale test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with socs.");
		Reporter.log("Step 2: Verify cart socs returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		Map<String,String> tokenMap = new HashMap<String, String>();
		//Create Cart
		
		String cartRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "createCartAPI.txt");
		String cartOperationName = "Create Cart";
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		CartApiV4 cartV4 = new CartApiV4();
		String cartUpdatedRequest = prepareRequestParam(cartRequestBody, tokenMap);
		Response cartResponse = cartV4.createCart(apiTestData, cartUpdatedRequest, tokenMap);
		logRequest(cartUpdatedRequest, cartOperationName);

		if (cartResponse != null && "200".equals(Integer.toString(cartResponse.getStatusCode()))) {
			logSuccessResponse(cartResponse, cartOperationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(cartResponse.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(cartResponse, cartOperationName);
		}
		
		//getSOC
		
		String operationName = "getServicesForSale_V3";

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_SERVICE + "getSOCforSaleV3.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response =getServicesForSale(apiTestData,updatedRequest, tokenMap);
		Assert.assertTrue(response.statusCode()==200);
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertNotNull(jsonNode.get("socForSale"));			
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}


	/**
	/**
	 * UserStory# Description:US418065,US418062
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "socapiv3","getServicesForPlans",Group.SHOP,Group.PROGRESSION })
	public void testGetServicesForPlans(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getServicesForPlans test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with plans socs.");
		Reporter.log("Step 2: Verify cart plans socs returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		Map<String,String> tokenMap = new HashMap<String, String>();
		
		//Create Cart
		
		String cartRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "createCartAPI.txt");
		String cartOperationName = "Create Cart";
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		CartApiV4 cartV4 = new CartApiV4();
		String cartUpdatedRequest = prepareRequestParam(cartRequestBody, tokenMap);
		Response cartResponse = cartV4.createCart(apiTestData, cartUpdatedRequest, tokenMap);
		logRequest(cartUpdatedRequest, cartOperationName);

		if (cartResponse != null && "200".equals(Integer.toString(cartResponse.getStatusCode()))) {
			logSuccessResponse(cartResponse, cartOperationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(cartResponse.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(cartResponse, cartOperationName);
		}
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "updateCartAPI.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response =getServicesForPlan(apiTestData,updatedRequest, tokenMap);
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "getServicesForPlan");
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			/*if (!jsonNode.isMissingNode()) {
				
			}*/
		} else {
			failAndLogResponse(response, "getServicesForPlan");
		}
		
		
		//Assert.assertTrue(response.statusCode()==200);
		//Reporter.log("Response Status:"+response.statusCode());
	}
	
	/**
	/**
	 * UserStory# Description:US600490
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"testSocServicesApiForV3",Group.SHOP,Group.PROGRESSION, "0801" })
	public void testGetOptionalServicesApiForV3(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getOptionalServices test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with soc Api.");
	    Reporter.log("Step 2: Verify services response code|Response code should be 404 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		Map<String,String> tokenMap = new HashMap<String, String>();

		//Create Cart
		String cartRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "createCartForPayment.txt");
		String cartOperationName = "Create Cart";
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		CartApiV4 cartV4 = new CartApiV4();
		String cartUpdatedRequest = prepareRequestParam(cartRequestBody, tokenMap);
		Response cartResponse = cartV4.createCart(apiTestData, cartUpdatedRequest, tokenMap);
		logRequest(cartUpdatedRequest, cartOperationName);

		if (cartResponse != null && "200".equals(Integer.toString(cartResponse.getStatusCode()))) {
			logSuccessResponse(cartResponse, cartOperationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(cartResponse.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(cartResponse, cartOperationName);
		}
		// Get Service call
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "getOptionalService.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response =getOptionalServicesResponse(apiTestData,updatedRequest, tokenMap);
		
		if (response != null && "404".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "get Optional services response");
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				
				if(null!=response){			
					JSONObject jsonObject = new JSONObject(getJSONfromResponse(response));
					Assert.assertEquals(jsonObject.get("message"), "Optional services not found from DCP.","Error code not verified successfully");				
				}
			}
		} else {
			failAndLogResponse(response, "getServicesForPlan");
		}
		

	}

}
