package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.EOSCommonMethods;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EOSAccountsV3Filters;
import com.tmobile.eservices.qa.pages.payments.api.EOSencription;

import io.restassured.response.Response;

public class EOSAccountsV3Test extends EOSCommonMethods {

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "otpv3" })

	public void testAccountsValidatebank(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSAccountsV3Filters accounts = new EOSAccountsV3Filters();
		EOSencription encri = new EOSencription();

		Map<Object, Object> optpara = new HashMap<Object, Object>();
		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response publickeyresponse = accounts.getResponsePublickKey(getjwt);
			String publickey = accounts.getpublicKey(publickeyresponse);

			String encrypts = encri.RsaEncryption("RSA", "AQAB", publickey, "345678937");

			optpara.put("accountNumber", encrypts);
			optpara.put("routingNumber", "021000021");

			Response validatebankresponse = accounts.getResponsevalidatebank(getjwt, optpara);
			String bankalias = accounts.getbankalias(validatebankresponse);
			optpara.put("bankalias", bankalias);
			Response addbankresponse = accounts.getResponseaddbank(getjwt, optpara);
			String addbankstatus = accounts.addbankstatus(addbankresponse);
			if (addbankstatus.equalsIgnoreCase("success"))
				Reporter.log("Bank is added successfully");
			else
				Assert.fail("Bank is not added successfully");

			Response searchpaymentresponse = accounts.getResponseSearchpayments(getjwt);
			List<String> bankaliaslist = accounts.getstoredbankalias(searchpaymentresponse);
			if (bankaliaslist.contains(bankalias))
				Reporter.log("Bank is searched in search paymentmethod");
			else
				Assert.fail("Bank is not searched in ");

			Response deletebankresponse = accounts.getResponsedeletebank(getjwt, optpara);

			String deletebankstatus = accounts.deletebankstatus(deletebankresponse);
			if (deletebankstatus.equalsIgnoreCase("success"))
				Reporter.log("Bank is deleted successfully");
			else
				Assert.fail("Bank is not deleted successfully");

			searchpaymentresponse = accounts.getResponseSearchpayments(getjwt);
			bankaliaslist = accounts.getstoredbankalias(searchpaymentresponse);
			if (!bankaliaslist.contains(bankalias))
				Reporter.log("Bank is not searched in search paymentmethod");
			else
				Assert.fail("Bank is still searched in search paymentmethod");

		}

	}
}