package com.tmobile.eservices.qa.shop.functional;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.shop.OfferSearchPage;
import com.tmobile.eservices.qa.pages.shop.PhonePages;
import com.tmobile.eservices.qa.pages.shop.ReportLostORStolenPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import com.tmobile.eservices.qa.shop.ShopConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class PhonePageTest extends ShopCommonLib {

    private static final Logger logger = LoggerFactory.getLogger(PhonePageTest.class);

    /*
     * Ensure user can suspend a line via the lost/stolen flow
     * P1_TC_Regression_Desktop_Phone Page for Shared Line
     *
     * @param data
     */
    @Test(dataProvider = "byColumnName", groups = {Group.PENDING})
    public void testSuspendALineWithLostOrStolenFlow(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case : Ensure user can suspend a line via the lost/stolen flow");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Phone link | Phone page should be displayed");
        Reporter.log("5. Click on lost or stolen link | Report lost or stolen page should be displayed");
        Reporter.log(
                "5. Select lost stolen, suspend line radio buttons & click submit | Lost or stolen suspend confirmation page should be displayed");
        Reporter.log("6. Click on go back to phone page link | Phone page should be displayed");
        Reporter.log("==============================");
        Reporter.log("Actual Output:");

        navigateToHomePage(myTmoData);
        HomePage homePage = new HomePage(getDriver());
        homePage.clickPhoneLink();
        PhonePages phonePage = new PhonePages(getDriver());
        phonePage.verifyPhonesPages();
        phonePage.clickLostOrStolenLink();

        ReportLostORStolenPage reportLostORStolenPage = new ReportLostORStolenPage(getDriver());
        reportLostORStolenPage.clickLocateDeviceButton();
        Reporter.log(ShopConstants.CHOOSE_LOCATE_DEVICE);
        //stolenDeviceFuntionality();

        Reporter.log(ShopConstants.VERIFY_STOLEN_DEVICE_FUNCTION);
        //suspendFunctionality(Boolean.TRUE);
        Reporter.log(ShopConstants.VERIFY_SUSPEND_LINE);
        //acceptTnCFunctionality();

        Reporter.log(ShopConstants.VERIFY_ACCEPT_TC);
        reportLostORStolenPage.clickSubmitbutton();

        Reporter.log(ShopConstants.SUBMIT_REPORT_STOLEN);
        reportLostORStolenPage.verifyReportedLoststolenDisplayed();
        phonePage.clickGobackTophonePage();

        Reporter.log(ShopConstants.GO_BACK_PHONE_PAGE);
        // Assert.assertTrue(phonePage.getSuspendedtext(),
        // Constants.SUSPEND_TEXT);
        Reporter.log(ShopConstants.VERIFY_LINE_SUSPENDED_TEXT);

        phonePage.clickFounddeviceLink();
        Reporter.log(ShopConstants.CLICK_FOUND_DEVICE_LINK);
        phonePage.clickUnblockRestoredevice();
        phonePage.clickRestoreOkButton();
    }

    /**
     * Ensure Jump!-eligible lines are directed to the Assurant site when
     * selecting the â€˜file a claimâ€™ link
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", groups = {Group.PENDING})
    public void verifyFileaClaimlink(ControlTestData data, MyTmoData myTmoData) {
        HomePage homePage = new HomePage(getDriver());
        PhonePages phonePage = new PhonePages(getDriver());
        Reporter.log(
                "Ensure Jump!-eligible lines are directed to the Assurant site when selecting the â€˜file a claimâ€™ link");

        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Phone link | Phone page should be displayed");
        Reporter.log("5. Click Assurant Claim and Continue Button | Assurant Claim form should be displayed");
        Reporter.log("==============================");
        Reporter.log("Actual Output:");

        launchAndPerformLogin(myTmoData);

        Reporter.log("Home page is displayed");
        homePage.clickPhoneLink();
        phonePage.verifyPhonesPages();
        phonePage.clickAssurantclaim();
        phonePage.clickContinuebtn();
        phonePage.checkPageIsReady();
        Assert.assertTrue(getDriver().getTitle().contains("Insurance Claim"), "User not redirected to Assurant site");
        Reporter.log("Insurance Claim page is displayed");
    }


    /**
     * Verify Check On Rebate Now
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS})
    public void verifyCheckOnRebateNow(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case : Verify Check On Rebate Now in Phones Page");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on phone tab | Phone Page should be displayed");
        Reporter.log(
                "5. Click on 'check on rebate now' | Offer seach page(T-Mobile Promotions Center) should be displayed");
        Reporter.log("========================");
        Reporter.log("Actual Results");

        navigateToPhonePage(myTmoData);
        PhonePages phonePage = new PhonePages(getDriver());
        phonePage.ClickOnCheckOnRebateNow();
        phonePage.clickOnAllowPopUp();
        phonePage.switchToWindow();
        OfferSearchPage offerSearchPage = new OfferSearchPage(getDriver());
        offerSearchPage.verifyOrderSearchPage();
    }

    /**
     * MyTMO_Verify Cloud-Phones page-Links
     * US177931 - Redirect user in Phone page to Curated shop : As a product
     * owner I want user to be navigated from Phone to curated shop page So he
     * can view Tmo featured devices or promotions
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
            Group.IOS})
    public void verifyPhonePageLinksandRedirectionFromPhonePageToShopPage(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case : verifyPhonePageLinks - MyTMO_Verify Cloud-Phones page-Links");
        Reporter.log("Test Case : Verify phone page upgrade button redires to shop page");
        Reporter.log("Expected Test Steps:");
        Reporter.log("1: Launch Application | Application should be launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4: Click on Phone link | Phone page should be displayed");
        Reporter.log("5: Verify Phone page Cloud links | All Cloud Links should be displayed");
        Reporter.log("6. Click on shop now | Shop page should be displayed");
        Reporter.log("================================");
        Reporter.log("Actual Test Steps:");

        navigateToPhonePage(myTmoData);
        PhonePages phonePage = new PhonePages(getDriver());

        phonePage.clickShopNow();
        ShopPage shopPage = new ShopPage(getDriver());
        shopPage.verifyShoppage();
    }


    @Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
    public void verifyPaymenDetailsOrderStatusPage(ControlTestData data, MyTmoData myTmoData) {
        logger.info("verifyPaymenDetailsOrderStatusPage method");
        Reporter.log("Test case: Verify Paymen Details on Order Status Page");
        Reporter.log("Expected Test Steps");
        Reporter.log("1. Launch application | Application should be launched");
        Reporter.log("2. Login to the application  |  User should be able to login Successfully");
        Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
        Reporter.log("4: Click on phone link | Phone page should be displayed");
        Reporter.log("5: Click on Order status link | Order status page should be displayed");
        Reporter.log(
                "6: Click More Details link on Order status page |Payment status order details should be displayed");
        Reporter.log("================================");
        Reporter.log("Actual Test Steps");

        launchAndNavigateToOrderStatusPage(myTmoData);
        /*
         * CheckOrderPage checkorderPage = new CheckOrderPage(getDriver());
         * Assert.assertTrue(checkorderPage.verifyCheckOrderPage());
         * Reporter.log("Order status page is displayed");
         * checkorderPage.clickMoredetailsLink();
         * Assert.assertTrue(checkorderPage.verifyPaymentDetails());
         * Reporter.log("Payment status order details are displayed");
         */
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
    public void verifyShippingAddressInOrderStatusPage(ControlTestData data, MyTmoData myTmoData) {
        logger.info("verifyShippingAddressInOrderStatusPage method");
        Reporter.log("Test case: Verify Shipping address on Order status page.");
        Reporter.log("Expected Test Steps");
        Reporter.log("1. Launch application | Application should be launched");
        Reporter.log("2. Login to the application  |  User should be able to login Successfully");
        Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
        Reporter.log("4: Click on phone link | Phone page should be displayed");
        Reporter.log("5: Click on Order status link | Order status page should be displayed");
        Reporter.log(
                "6: Click More Details link on Order status page | Shipping status in order details should be displayed");
        Reporter.log("================================");
        Reporter.log("Actual Test Steps");

        launchAndNavigateToOrderStatusPage(myTmoData);
        /*
         * CheckOrderPage checkorderPage = new CheckOrderPage(getDriver());
         * Assert.assertTrue(checkorderPage.verifyCheckOrderPage());
         * Reporter.log("Order status page is displayed");
         * checkorderPage.clickMoredetailsLink();
         *
         * Assert.assertTrue(checkorderPage.verifyShippingAddress());
         * Reporter.log("Shipping status in order details are displayed");
         */
    }

}