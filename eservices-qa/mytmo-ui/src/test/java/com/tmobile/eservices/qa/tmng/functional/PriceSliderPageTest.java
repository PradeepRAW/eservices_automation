package com.tmobile.eservices.qa.tmng.functional;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.PriceSliderPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TMNGNewHeaderPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

/**
 * @author sputti
 *
 */
public class PriceSliderPageTest extends TmngCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(PriceSliderPageTest.class);	
	/***
	 * FLEx - Price Slider End to End Functional Testing 
	 * @param tMNGData
	 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TMNG })
		public void verifyPriceSliderPage(TMNGData tMNGData) {
		logger.info("End to End Automation Testing of PriceSlider Application");
		Reporter.log("======================================================================");
		Reporter.log("Test Steps | Expected Test Results:");
		Reporter.log("Launch TMNG Price Slider | Price Slider Should be Launched");
		Reporter.log("1. Verify Price 5+ Tick | 5+ Tick should be displayed");
		Reporter.log("2. Verify OfferText, Price, DollarSign, BubbleText, PriceDescription and Legal message | OfferText, Price, DollarSign, BubbleText, PriceDescription and Legal messages should be displayed by clicking on 5+ Tick Mark");
		Reporter.log("--------------------------------------------------Actual Test Results:--------------------------------------------------------");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		navigateToTMobilePage(tMNGData);
	    unavPage.clickMenuExpandButton();
		unavPage.verifyHeaderPlansLabel();
		unavPage.clickHeaderPlansLink();
		unavPage.verifyHeaderPlansPage();
		PriceSliderPage priceSliderPage = new PriceSliderPage(getDriver());
		priceSliderPage.clickEssentialsCTA();
		//priceSliderPage.verifyPriceSliderPageLoaded();
		getDriver().manage().timeouts().setScriptTimeout(10000, TimeUnit.MINUTES);
		Reporter.log("======================================================================");
		priceSliderPage.verifyPrice1Tick();
		//priceSliderPage.verifyOfferText(1);
		priceSliderPage.verifyPrice(1);
		priceSliderPage.verifyDollarSign(1);
		priceSliderPage.verifyPriceDescription(1);
		priceSliderPage.verifyLegalText(1);
		priceSliderPage.verifyPrimaryCTA(1);
		Reporter.log("======================================================================");
		priceSliderPage.verifyPrice2Tick();
		//priceSliderPage.verifyOfferText(2);
		priceSliderPage.verifyPrice(2);
		priceSliderPage.verifyDollarSign(2);
		priceSliderPage.verifyBubbleText(2);
		priceSliderPage.verifyPriceDescription(2);
		priceSliderPage.verifyLegalText(2);
		priceSliderPage.verifyPrimaryCTA(2);
		Reporter.log("======================================================================");
		priceSliderPage.verifyPrice3Tick();
		//priceSliderPage.verifyOfferText(3);
		priceSliderPage.verifyPrice(3);
		priceSliderPage.verifyDollarSign(3);
		priceSliderPage.verifyBubbleText(3);
		priceSliderPage.verifyPriceDescription(3);
		priceSliderPage.verifyLegalText(3);
		priceSliderPage.verifyPrimaryCTA(3);
		Reporter.log("======================================================================");
		priceSliderPage.verifyPrice4Tick();
		//priceSliderPage.verifyOfferText(4);
		priceSliderPage.verifyPrice(4);
		priceSliderPage.verifyDollarSign(4);
		priceSliderPage.verifyBubbleText(4);
		priceSliderPage.verifyPriceDescription(4);
		priceSliderPage.verifyLegalText(4);
		priceSliderPage.verifyPrimaryCTA(4);
		Reporter.log("======================================================================");
		priceSliderPage.verifyPrice5PlusTick();
		//priceSliderPage.verifyOfferText(5);
		priceSliderPage.verifyPrice(5);
		priceSliderPage.verifyDollarSign(5);
		priceSliderPage.verifyBubbleText(5);
		priceSliderPage.verifyPriceDescription(5);
		priceSliderPage.verifyLegalText(5);
		priceSliderPage.verifyPrimaryCTA(5);
		Reporter.log("======================================================================");
	}
		/***
		 * DE229344 - Price Slider: Primary CTA text alignment is not proper. Text overflows from CTA in web browser 
		 * @param tMNGData
		 */
			@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
			public void verifyPriceSliderCTATextAign(TMNGData tMNGData) {
			logger.info("DE229344 - Price Slider: Primary CTA text alignment is not proper. Text overflows from CTA in web browser");
			Reporter.log("======================================================================");
			Reporter.log("Test Steps | Expected Test Results:");
			Reporter.log("Launch TMNG Price Slider | Price Slider Should be Launched");
			Reporter.log("Verify Primary CTA text alignment | CTA text should be align center");
			Reporter.log("--------------------------------------------------Actual Test Results:--------------------------------------------------------");
			loadTmngURL(tMNGData);
			getDriver().get("https://dmo-tmo-publisher.corporate.t-mobile.com/content/t-mobile/consumer/no-credit-check.html");
			PriceSliderPage priceSliderPage = new PriceSliderPage(getDriver());
			priceSliderPage.verifyPriceSliderPageLoaded();
			getDriver().manage().timeouts().setScriptTimeout(10000, TimeUnit.MINUTES);
			Reporter.log("======================================================================");
			priceSliderPage.verifyPrimaryCTAAlign();
		}
}