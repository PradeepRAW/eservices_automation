package com.tmobile.eservices.qa.applitools;

import java.time.LocalDateTime;

import org.testng.annotations.Test;

import com.applitools.eyes.BatchInfo;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class ShopPages extends ShopCommonLib {
	ApplitoolsLib applitoolsLib = new ApplitoolsLib();
	BatchInfo batchInfo = new BatchInfo("accounts " + LocalDateTime.now());

	/***
	 * Test Visual Testing for AccessoriesPDP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testAccessoriesPDP(ControlTestData data, MyTmoData myTmoData) {
		// AccessoriesPDP
		applitoolsLib.doVisualTest(getDriver(), "shop", "AccessoriesPDP", batchInfo);
	}

	/***
	 * Test Visual Testing for AccessoriesPLP
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop1" })
	public void testAccessoriesPLP(ControlTestData data, MyTmoData myTmoData) {
		navigateToAccessoryPLPPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "AccessoriesPLP", batchInfo);
	}

	/***
	 * Test Visual Testing for AccessoriesStandAlonePDP
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testAccessoriesStandAlonePDP(ControlTestData data, MyTmoData myTmoData) {
		// AccessoriesStandAlonePDP
		applitoolsLib.doVisualTest(getDriver(), "shop", "AccessoriesStandAlonePDP", batchInfo);
	}

	/***
	 * Test Visual Testing for AccessoriesStandAlonePLP
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testAccessoriesStandAlonePLP(ControlTestData data, MyTmoData myTmoData) {

		// AccessoriesStandAlonePLP
		applitoolsLib.doVisualTest(getDriver(), "shop", "AccessoriesStandAlonePLP", batchInfo);
	}

	/***
	 * Test Visual Testing for AccessoriesStandAloneReviewCartPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testAccessoriesStandAloneReviewCartPage(ControlTestData data, MyTmoData myTmoData) {

		// AccessoriesStandAloneReviewCartPage
		applitoolsLib.doVisualTest(getDriver(), "shop", "AccessoriesStandAloneReviewCartPage", batchInfo);
	}

	/***
	 * Test Visual Testing for AddALinePage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testAddALinePage(ControlTestData data, MyTmoData myTmoData) {

		// AddALinePage
		applitoolsLib.doVisualTest(getDriver(), "shop", "AddALinePage", batchInfo);
	}

	/***
	 * Test Visual Testing for CartPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testCartPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "CartPage", batchInfo);
	}

	/***
	 * Test Visual Testing for Esignature
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testEsignature(ControlTestData data, MyTmoData myTmoData) {

		// Esignature
		applitoolsLib.doVisualTest(getDriver(), "shop", "Esignature", batchInfo);
	}

	/***
	 * Test Visual Testing for InsuranceMigrationPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testInsuranceMigrationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "DeviceProtectioPage", batchInfo);
	}

	/***
	 * Test Visual Testing for JumpCartPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testJumpCartPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToCartPageForJumpUserFromIMEIPage();
		applitoolsLib.doVisualTest(getDriver(), "shop", "JumpCartPage", batchInfo);
	}

	/***
	 * Test Visual Testing for JumpLineSelectorPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testJumpLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		// JumpLineSelectorPage
		applitoolsLib.doVisualTest(getDriver(), "shop", "JumpLineSelectorPage", batchInfo);
	}


	/***
	 * Test Visual Testing for LineSelectorPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "LineSelectorPage", batchInfo);
	}

	/***
	 * Test Visual Testing for NewLineSelectorDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testNewLineSelectorDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "NewLineSelectorDetailsPage", batchInfo);
	}

	/***
	 * Test Visual Testing for NewLineSelectorPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testNewLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "NewLineSelectorPage", batchInfo);
	}

	/***
	 * Test Visual Testing for OfferDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testOfferDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToOfferDetailsPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "OfferDetailsPage", batchInfo);
	}

	/***
	 * Test Visual Testing for OrderConfirmation
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testOrderConfirmation(ControlTestData data, MyTmoData myTmoData) {
		// OrderConfirmation
		applitoolsLib.doVisualTest(getDriver(), "shop", "OrderConfirmation", batchInfo);
	}

	/***
	 * Test Visual Testing for OrderStatusPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testOrderStatusPage(ControlTestData data, MyTmoData myTmoData) {
		launchAndNavigateToOrderStatusPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "OrderStatusPage", batchInfo);
	}

	/***
	 * Test Visual Testing for PDPPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testPDPPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPDPPageBySeeAllPhones(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "PDPPage", batchInfo);
	}

	/***
	 * Test Visual Testing for PhonePage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testPhonePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPhonePage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "PhonePage", batchInfo);
	}

	/***
	 * Test Visual Testing for PhoneSelectionPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testPhoneSelectionPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "PhoneSelectionPage", batchInfo);
	}

	/***
	 * Test Visual Testing for PlansPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testPlansPage(ControlTestData data, MyTmoData myTmoData) {
		AccountsCommonLib accountsCommonLib = new AccountsCommonLib();
		accountsCommonLib.navigateToAccountOverviewPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "PlansPage", batchInfo);
	}

	/***
	 * Test Visual Testing for PLPPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testPLPPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPLPPageBySeeAllPhones(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "PLPPage", batchInfo);
	}

	/***
	 * Test Visual Testing for Returns
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testReturns(ControlTestData data, MyTmoData myTmoData) {
		// Returns
		applitoolsLib.doVisualTest(getDriver(), "shop", "Returns", batchInfo);
	}

	/***
	 * Test Visual Testing for ReviewCartPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testReviewCartPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToStandAloneReviewCartPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "ReviewCartPage", batchInfo);
	}

	/***
	 * Test Visual Testing for SecureCheckOutPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testSecureCheckOutPage(ControlTestData data, MyTmoData myTmoData) {
		// SecureCheckOutPage
		applitoolsLib.doVisualTest(getDriver(), "shop", "SecureCheckOutPage", batchInfo);
	}

	/***
	 * Test Visual Testing for ShopPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testShopPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "ShopPage", batchInfo);
	}

	/***
	 * Test Visual Testing for TradeInAnotherDevicePage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testTradeInAnotherDevicePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTradeInAnotherDevicePageBySeeAllPhones(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "TradeInAnotherDevicePage", batchInfo);
	}

	/***
	 * Test Visual Testing for TradeInConfirmationPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testTradeInConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		// TradeInConfirmationPage
		applitoolsLib.doVisualTest(getDriver(), "shop", "TradeInConfirmationPage", batchInfo);
	}

	/***
	 * Test Visual Testing for TradeInDeviceConditionPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testTradeInDeviceConditionPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTradeInDeviceConditionValuePageBySeeAllPhones(myTmoData);
		applitoolsLib.doVisualTest(getDriver(), "shop", "TradeInDeviceConditionPage", batchInfo);
	}

	/***
	 * Test Visual Testing for TradeInPage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testTradeInPage(ControlTestData data, MyTmoData myTmoData) {
		// TradeInPage
		applitoolsLib.doVisualTest(getDriver(), "shop", "TradeInPage", batchInfo);
	}

	/***
	 * Test Visual Testing for TradeInValuePage
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testTradeInValuePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTradeInDeviceConditionConfirmationPageByFeatureDevicesWithTradeInFlow(myTmoData, "Iphone8");
		applitoolsLib.doVisualTest(getDriver(), "shop", "TradeInValuePage", batchInfo);
	}

}
