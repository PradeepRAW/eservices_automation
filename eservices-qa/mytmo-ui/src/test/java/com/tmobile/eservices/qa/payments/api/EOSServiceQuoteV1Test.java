package com.tmobile.eservices.qa.payments.api;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.EOSCommonMethods;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EOSservicequotefilter;

import io.restassured.response.Response;

public class EOSServiceQuoteV1Test extends EOSCommonMethods {

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEossedonaquote(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSservicequotefilter quote = new EOSservicequotefilter();
		// EOSCommonMethods ecm=new EOSCommonMethods();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response sedonaquoteresponse = quote.getResponsesedonaquote(getjwt);
			String alltags[] = { "quoteId", "totalDue" };
			for (String tag : alltags) {
				checkjsontagitems(sedonaquoteresponse, tag);
			}

		}

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEospaquote(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetBillList");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the billlist of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSservicequotefilter quote = new EOSservicequotefilter();
		// EOSCommonMethods ecm=new EOSCommonMethods();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response paquoteresponse = quote.getResponsepaquote(getjwt);
			checkexpectedvalues(paquoteresponse, "statusCode", "100");
			checkexpectedvalues(paquoteresponse, "statusMessage", "Success");

		}

	}

}