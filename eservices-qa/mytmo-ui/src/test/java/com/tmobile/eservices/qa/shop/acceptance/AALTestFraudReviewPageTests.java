package com.tmobile.eservices.qa.shop.acceptance;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.IdentityFraudRejectPage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewIntroductionPage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewQuestionPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class AALTestFraudReviewPageTests extends ShopCommonLib {

	/**
	 * verifyFraudAcceptScreen
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALFraudCheck" })
	public void verifyFraudAcceptScreen(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : verify Fraud Accept By Placing Order");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
		Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log(
				"10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
		Reporter.log(
				"11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("12. Click on 'Continue' CTA | Payment Information section should be displayed");
		Reporter.log("13. Click Edit on shipping address | Shipping address fields should be displayed");
		Reporter.log(
				"14. Fill in shipping address with First line to be`fraudcheckaccept` and rest provided address| Shipping address fields should be entered");
		Reporter.log("15. Fill Card details and click continue button | Order conformation page should be displayed");
		Reporter.log("16. Verify order header | Order header should be displayed");
		Reporter.log("17. Verify order Number | Order Number should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickEditShippingAddress();
		cartPage.verifyShippingEditExpand();
		cartPage.fillFraudCheckShippingAddressInfo("fraudcheckaccept", "Issaquah");
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		/*
		 * cartPage.clickServiceCustomerAgreementCheckBox();
		 * cartPage.placeOrderCTAEnabled(); cartPage.clickAcceptAndPlaceOrder();
		 * OrderConfirmationPage orderConfirmationPage = new
		 * OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}

	/**
	 * verifyFraudRejectScreen
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALFraudCheck" })
	public void verifyFraudRejectScreenRealData(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : verify Fraud Reject Screen By Placing Order");
		Reporter.log("Data Conditions: MyTmo registered misdn. Customer with email mytmobilereject@gmail.com");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
		Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log(
				"10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
		Reporter.log(
				"11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("12. Click Edit on shipping address | Shipping address fields should be displayed");
		Reporter.log(
				"13. Fill in shipping address with First line to be`fraudcheckreject` and rest provided address| Shipping address fields should be entered");
		Reporter.log("14. Click on 'Continue' CTA | Payment Information section should be displayed");
		Reporter.log("15. Click on Accept and Continue|Fraud review page should be displayed");
		Reporter.log("16. Click on Continue CTA |Continue CTA should be clicked");
		Reporter.log("17. Verify Cancle CTA| Cancle CTA should be displayed and should be enabled");
		Reporter.log("18. Verify Submit CTA| Submit CTA should be displayed and should be disabled");
		Reporter.log("19. Select option from review questions |Option should be select");
		Reporter.log("20. Click on Submit |Order reject screen be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyEditShippingCTADisplayed();
		cartPage.clickEditShippingAddress();
		cartPage.verifyShippingEditExpand();
		cartPage.fillFraudCheckShippingAddressInfo("1421 NE Iris St", "Issaquah");
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());

		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.placeOrderCTAEnabled();

		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionCancelCTA();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();

		IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
		identityReviewQuestionPage.verifyReviewQuestionPage();
		identityReviewQuestionPage.verifySubmitDisabled();
		identityReviewQuestionPage.clickFirstOption();
		identityReviewQuestionPage.verifySubmitEnabled();
		identityReviewQuestionPage.clickSubmit();

		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();

	}

	/**
	 * verify Fraud Review Questions Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALFraudCheck" })
	public void verifyFraudReviewQuestionsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : verify Fraud Review Questions Page");
		Reporter.log("Data Conditions: MyTmo registered misdn. Customer with email mytmobilereject@gmail.com");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
		Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log(
				"10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
		Reporter.log(
				"11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("12. Click Edit on shipping address | Shipping address fields should be displayed");
		Reporter.log(
				"13. Fill in shipping address with First line to be`fraudcheckreject` and rest provided address| Shipping address fields should be entered");
		Reporter.log("14. Click on 'Continue' CTA | Payment Information section should be displayed");
		Reporter.log("15. Click on Accept and Continue|Fraud review page should be displayed");
		Reporter.log("16. Click on Continue CTA |Continue CTA should be clicked");
		Reporter.log("17. Verify Cancle CTA| Cancle CTA should be displayed and should be enabled");
		Reporter.log("18. Verify Submit CTA| Submit CTA should be displayed and should be disabled");
		Reporter.log("19. Select option from review questions |Option should be select");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());

		cartPage.clickEditShippingAddress();
		cartPage.verifyShippingEditExpand();
		cartPage.fillFraudCheckShippingAddressInfo("1421 NE Iris St", "Issaquah");
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.placeOrderCTAEnabled();

		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionCancelCTA();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();

		IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
		identityReviewQuestionPage.verifyReviewQuestionPage();
		identityReviewQuestionPage.verifySubmitDisabled();
		identityReviewQuestionPage.clickFirstOption();
		identityReviewQuestionPage.verifySubmitEnabled();
		identityReviewQuestionPage.clickSubmit();

		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();

	}

	/**
	 * verify Payment Decline Wrong CVV
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALFraudCheck" })
	public void verifyPaymentDeclineWrongCVV(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : verify Payment Decline Wrong CVV");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
		Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log(
				"10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
		Reporter.log(
				"11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("12. Click Edit on shipping address | Shipping address fields should be displayed");
		Reporter.log(
				"13. Fill in shipping address with First line to be `fraudcheckreview` and rest provided address| Shipping address fields should be entered");
		Reporter.log("14. Click on 'Continue' CTA | Payment Information section should be displayed");
		Reporter.log("15. Fill Card details With Wrong CVV and click continue button | Payment Should be declined");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickEditShippingAddress();
		cartPage.verifyShippingEditExpand();
		cartPage.fillFraudCheckShippingAddressInfo("1421 NE Iris St", "Issaquah");
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		/*
		 * cartPage.clickServiceCustomerAgreementCheckBox();
		 * cartPage.placeOrderCTAEnabled(); cartPage.clickAcceptAndPlaceOrder();
		 */
	}

	/**
	 * verify Payment Decline Wrong AVS
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AALFraudCheck" })
	public void verifyPaymentDeclineWrongAVS(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : verify Payment Decline Wrong AVS (incorrect billing address)");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
		Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log(
				"10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
		Reporter.log(
				"11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("12. Click on 'Continue' CTA | Payment Information section should be displayed");
		Reporter.log(
				"13. Fill Card details With Wrong Billing Address and click continue button | Payment Should be declined");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickEditShippingAddress();
		cartPage.verifyShippingEditExpand();
		cartPage.fillFraudCheckShippingAddressInfo("1425 NE Iris St", "Issaquah");
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.placeOrderCTAEnabled();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionCancelCTA();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();

		IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
		identityReviewQuestionPage.verifyReviewQuestionPage();
		identityReviewQuestionPage.verifySubmitDisabled();
		identityReviewQuestionPage.clickFirstOption();
		identityReviewQuestionPage.verifySubmitEnabled();
		identityReviewQuestionPage.clickSubmit();

		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
	}

}
