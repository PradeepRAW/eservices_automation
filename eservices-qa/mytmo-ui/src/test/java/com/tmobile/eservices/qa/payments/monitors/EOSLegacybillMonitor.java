package com.tmobile.eservices.qa.payments.monitors;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.EOSCommonMethods;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EOSLegacybilling;

import io.restassured.response.Response;

/**
 * @author Bhavya
 *
 */

public class EOSLegacybillMonitor extends EOSCommonMethods {

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "ebillmonitor" })
	public void monitorEbillPage(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSLegacybilling lbilling = new EOSLegacybilling();
		// String[] getjwt = getjwtfromfiltersforAPI(apiTestData);
		// Commented above getjwt call as we aren't using any jwt auth token for legacy
		// service calls.
		String ban = apiTestData.getBan();
		if (ban != null) {
			// Created a new method with prefix as sample just to test run and check
			// response.
			Response lbilllistresponse = lbilling.getResponselegacybillListSample(ban);
			// Need to change validation below validations w.r.t legacy xml responses.
			// Either we should parse xml to json or implement a validation for xml
			// responses.
			checkexpectedvalues(lbilllistresponse, "statusCode", "100");
			checkexpectedvalues(lbilllistresponse, "statusMessage", "Bill list found");

			String statementid = lbilling.getbillingstatementid(lbilllistresponse);

			// check Deatiled bill
			// Response
			// printdetailmsisdnresponse=lbilling.getResponseprintdetailbillmsisdn(getjwt,
			// statementid);

			/// check summarybill

			// Response
			// printsummarybanresponse=lbilling.getResponseprintsummarybillban(getjwt,
			// statementid);

		}

	}

}