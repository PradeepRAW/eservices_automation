package com.tmobile.eservices.qa.galen;

import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class PUBPages extends PaymentCommonLib{
	GalenLib galenLib = new GalenLib();

	/**
	 * Do visual testing for AccountHistoryPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testAccountHistoryPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAccountHistoryPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "AccountHistoryPage","","");
	}
	
	/**
	 * Do visual testing for AddBankPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testAddBankPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAddBankPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "AddBankPage","","");
	}
	
	/**
	 * Do visual testing for AddCardPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testAddCardPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAddCardPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "AddCardPage","","");
	}
	
	/**
	 * Do visual testing for AutoPayCancelConfirmationPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testAutopayCancelConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayCancelConfirmationPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "AutoPayCancelConfirmationPage","","");
	}
	
	/**
	 * Do visual testing for AutoPayConfirmationPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testAutopayConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayConfirmationPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "AutoPayConfirmationPage","","");
	}
	
	/**
	 * Do visual testing for AutoPayPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testAutoPayPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "AutoPayPage","","");
	}
	
	/**
	 * Do visual testing for BillAndPaySummaryPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testBillAndPaySummaryPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToBillAndPaySummaryPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "BillAndPaySummaryPage","","");
	}
	
	/**
	 * Do visual testing for BillDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testBillDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToBillDetailsPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "BillDetailsPage","","");
	}
	
	/**
	 * Do visual testing for BillingSummaryPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testBillingSummaryPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToBillingSummaryPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "BillingSummaryPage","","");
	}
	
	/**
	 * Do visual testing for CallDetailRecordsForwardPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testCallDatailRecordsForwardPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToCallDetailRecordsForwardPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "CallDetailRecordsForwardPage","","");
	}
	
	/**
	 * Do visual testing for ChargeEquipementPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testChargeEquipementPage(ControlTestData data, MyTmoData myTmoData) {

		galenLib.doVisualTest(getDriver(), "ChargeEquipementPage","","");
	}
	
	/**
	 * Do visual testing for ChargeFeaturesPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testChargeFeaturesPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToChargeFeaturesPage(myTmoData, "");
		galenLib.doVisualTest(getDriver(), "ChargeFeaturesPage","","");
	}
	
	/**
	 * Do visual testing for ChargeOneTimePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testChargeOnetimePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToChargeOneTimePage(myTmoData);
		galenLib.doVisualTest(getDriver(), "ChargeOneTimePage","","");
	}
	
	/**
	 * Do visual testing for ChargePlanPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testChargePlanPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToChargePlanPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "ChargePlanPage","","");
	}
	
	/**
	 * Do visual testing for EIPDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testEIPDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEIPDetailsPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "EIPDetailsPage","","");
	}
	
	/**
	 * Do visual testing for EIPPaymentConfirmationCCPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testEIPPaymentConfirmationCCPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEIPPaymentConfirmationCCPage(myTmoData, "Card");
		galenLib.doVisualTest(getDriver(), "EIPPaymentConfirmationCCPage","","");
	}
	
	/**
	 * Do visual testing for EIPPaymentForwardPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testEIPPaymentForwardPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEIPPaymentForwardPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "EIPPaymentForwardPage","","");
	}
	
	/**
	 * Do visual testing for EIPPaymentReviewCCPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testEIPPaymentReviewCCPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEIPPaymentReviewCCPage(myTmoData, "Card");
		galenLib.doVisualTest(getDriver(), "EIPPaymentReviewCCPage","","");
	}
	
	/**
	 * Do visual testing for AutoPayFAQPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testFAQPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayFAQPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "AutoPayFAQPage","","");
	}
	
	/**
	 * Do visual testing for LeaseDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testLeaseDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLeaseDetailsPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "LeaseDetailsPage","","");
	}
	
	/**
	 * Do visual testing for ChargeOneTimePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testOneTimePaymentPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToOTPpage(myTmoData);
		galenLib.doVisualTest(getDriver(), "ChargeOneTimePage","","");
	}
	
	/**
	 * Do visual testing for EditAmountPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testOTPAmountPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEditAmountPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "EditAmountPage","","");
	}
	
	/**
	 * Do visual testing for OTPConfirmationPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testOTPConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToOTPConfirmationPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "OTPConfirmationPage","","");
	}
	
	/**
	 * Do visual testing for OTPDatePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testOTPDatePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToOTPDatePage(myTmoData);
		galenLib.doVisualTest(getDriver(), "OTPDatePage","","");
	}
	
	/**
	 * Do visual testing for PAConfirmationPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testPAConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPAConfirmationPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PAConfirmationPage","","");
	}
	
	/**
	 * Do visual testing for PAInstallmentDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testPAInstallmentDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPAInstallmentDetailsPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PAInstallmentDetailsPage","","");
	}
	
	/**
	 * Do visual testing for PaperlessBillingPage
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testPaperlessBillingPage(ControlTestData data, MyTmoData myTmoData) throws Exception {
		navigateToPaperlessBillingPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PaperlessBillingPage","","");
	}
	
	/**
	 * Do visual testing for PaymentArrangementPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testPaymentArrangementPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPaymentArrangementPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PaymentArrangementPage","","");
	}
	
	/**
	 * Do visual testing for PaymentErrorPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testPaymentErrorPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPaymentErrorPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PaymentErrorPage","","");
	}
	
	/**
	 * Do visual testing for ProfilePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testProfilePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToProfilePage(myTmoData);
		galenLib.doVisualTest(getDriver(), "ProfilePage","","");
	}
	
	/**
	 * Do visual testing for SpokePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testSpokePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToSpokePage(myTmoData);
		galenLib.doVisualTest(getDriver(), "SpokePage","","");
	}
	
	/**
	 * Do visual testing for AutoPayTnCPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testTnCPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayTnCPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "AutoPayTnCPage","","");
	}
	
	/**
	 * Do visual testing for UsageDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testUsageDetailsPage(ControlTestData data, MyTmoData myTmoData) {

		galenLib.doVisualTest(getDriver(), "UsageDetailsPage","","");
	}
	
	/**
	 * Do visual testing for UsageOverviewPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testUsageOverviewPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToUsageOverviewPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "UsageOverviewPage","","");
	}
	
	/**
	 * Do visual testing for UsagePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testUsagePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToUsagePage(myTmoData);
		galenLib.doVisualTest(getDriver(), "UsagePage","","");
	}


}
