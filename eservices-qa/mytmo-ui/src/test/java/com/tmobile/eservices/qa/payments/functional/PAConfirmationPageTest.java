/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AddCardPage;
import com.tmobile.eservices.qa.pages.payments.PAConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.PaymentArrangementPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class PAConfirmationPageTest  extends PaymentCommonLib{

    private final static Logger logger = LoggerFactory.getLogger(PaymentArrangementPageTest.class);

    @Test(dataProvider = "byColumnName", enabled = true, groups = {"payments"})
    public void verifyPAConfirmationPageLoad(ControlTestData data, MyTmoData myTmoData) {
        logger.info("verifyPAdisplaysHomepage method called in EservicesPaymnetsTest");
        Reporter.log("Test Case :Verify completed PAYMENT ARRANGEMENT displays on Homepage");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Paybill | One time payment page should be displayed");
        Reporter.log("5. Click on Payment Arrangement | Payment Arrangements page should be displayed.");
        Reporter.log("6. Click Balance radio button and Select New Card button| New Card section should be displayed.");
        Reporter.log("7. Fill Card details and click next | Review payment page should be loaded");
        Reporter.log("8. Verify Card Information | Entered Hybrid card details should be displayed correctly.");
        Reporter.log("9. Click on Terms And Conditions checkbox | Submit button should be enabed.");
        Reporter.log("10. Click Submit Button | Payment Confirmation message should be displayed");

        PAConfirmationPage paConfirmationPage = navigateToPAConfirmationPage(myTmoData);
        paConfirmationPage.verifyPageLoaded();
    }

    /**
     * CDCDWG2-97	PA : Modify - Confirmation page functionality
     * CDCDWG2-100	PA : Modify - Confirmation page Alerts
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {"payments"})
    public void verifyPAModifyConfirmationPageAndAlerts(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case :CDCDWG2-97	PA : Modify - Confirmation page functionality");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Paybill | One time payment page should be displayed");
        Reporter.log("5. Click on Payment Arrangement | Payment Arrangements page should be displayed.");
        Reporter.log("6. Click Payment method blade and Select New Card button| New Card section should be displayed.");
        Reporter.log("7. Fill Card details and click next | Review payment page should be loaded");
        Reporter.log("8. Verify Card Information | Entered Hybrid card details should be displayed correctly.");
        Reporter.log("9. Click Submit Button | Payment Confirmation message should be displayed");
        Reporter.log("10. Verify confirmation page alerts | Alert messages should be displayed");

        PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
        SpokePage otpSpokePage = new SpokePage(getDriver());
    	if (!paymentArrangementPage.isPASecuredOrNot()) {
        paymentArrangementPage.clickpaymentMethod();
        paymentArrangementPage.clickAddCard();
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.fillCardInfo(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
    	}else {
			paymentArrangementPage.clickpaymentMethod();
			otpSpokePage.choosePaymentMethodLater();
		}
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.clickAgreeNSubmitBtn();
		PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
		paConfirmationPage.verifyPageLoaded();
		paConfirmationPage.verifyModifyPaConfirmationAlerts(PaymentConstants.MODIFY_PA_CONFIRM_PAGE_ALERT_PAUPDATED);
		paConfirmationPage.verifyModifyPaConfirmationAlerts(PaymentConstants.MODIFY_PA_CONFIRM_PAGE_ALERT_INSTALLMENT_DATES_AMOUNTS_UPDAETD);
		paConfirmationPage.verifyModifyPaConfirmationAlerts(PaymentConstants.MODIFY_PA_CONFIRM_PAGE_ALERT_PAYMENTS_PROCESSING);
		
    }
}
