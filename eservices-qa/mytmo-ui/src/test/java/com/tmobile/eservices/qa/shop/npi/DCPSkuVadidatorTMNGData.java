package com.tmobile.eservices.qa.shop.npi;

public class DCPSkuVadidatorTMNGData {
	
	public String familyId;
	public String manfactType;
	public String modelName;
	public String skuNum;
	public String color;
	public String memory;
	public String availableStatus;
	
	public String getFamilyId() {
		return familyId;
	}
	public void setFamilyId(String familyId) {
		this.familyId = familyId;
	}
	public String getManfactType() {
		return manfactType;
	}
	public void setManfactType(String manfactType) {
		this.manfactType = manfactType;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getSkuNum() {
		return skuNum;
	}
	public void setSkuNum(String skuNum) {
		this.skuNum = skuNum;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getMemory() {
		return memory;
	}
	public void setMemory(String memory) {
		this.memory = memory;
	}
	public String getAvailableStatus() {
		return availableStatus;
	}
	public void setAvailableStatus(String availableStatus) {
		this.availableStatus = availableStatus;
	}

}
