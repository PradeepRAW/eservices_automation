
package com.tmobile.eservices.qa.accounts.api.phone;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class getDeviceBlockStatus {

	String environment;

	@Test(groups = "phone1")
	public void getDeviceBlockstatus_stolen() {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();

		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("interactionId", "123456787");
		request.header("workflowId", "1234");
		request.header("sender_id", "MYTMO");
		request.header("application_id", "ESERVICE");
		request.header("channel_id", "WEB");
		request.contentType("application/json");

		JSONObject requestParams = new JSONObject();
		JSONArray array = new JSONArray();
		array.put("3463209463");
		requestParams.put("msisdn", array);
		requestParams.put("imei", "354842091791460");
		requestParams.put("accountNumber", "956062160");
		// System.out.println(requestParams.toString());

		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/getDeviceBlockStatus");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating getDeviceBlockStatus Operation Response ");
		Reporter.log("Note : /myPhone/getDeviceBlockStatus Service Internally calls devicedetails ");
		Reporter.log("Test data : Account eligible for lost/stolen/suspend/block");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("blockIndicator").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("reasonDescription")
					.contains("Reported stolen by a T-Mobile customer"));
			Assert.assertTrue(response.jsonPath().getString("isLost").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("isStolen").contains("true"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(groups = "phone1")
	public void getDeviceBlockstatus_lost() {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();

		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("interactionId", "123456787");
		request.header("workflowId", "1234");
		request.header("sender_id", "MYTMO");
		request.header("application_id", "ESERVICE");
		request.header("channel_id", "WEB");
		request.contentType("application/json");

		JSONObject requestParams = new JSONObject();
		JSONArray array = new JSONArray();
		array.put("9737041537");
		requestParams.put("msisdn", array);
		requestParams.put("imei", "012629000228740");
		requestParams.put("accountNumber", "955102500");
		// System.out.println(requestParams.toString());

		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/getDeviceBlockStatus");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating getDeviceBlockStatus Operation Response ");
		Reporter.log("Note : /myPhone/getDeviceBlockStatus Service Internally calls devicedetails ");
		Reporter.log("Test data : Account eligible for lost/stolen/suspend/block");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("blockIndicator").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("reasonDescription")
					.contains("Reported lost by a T-Mobile customer"));
			Assert.assertTrue(response.jsonPath().getString("isLost").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("isStolen").contains("false"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(groups = "phone1")
	public void getDeviceBlockstatus_lost_linetype_duplicate() {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();

		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("interactionId", "123456787");
		request.header("workflowId", "1234");
		request.header("sender_id", "MYTMO");
		request.header("application_id", "ESERVICE");
		request.header("channel_id", "WEB");
		request.contentType("application/json");

		JSONObject requestParams = new JSONObject();
		JSONArray array = new JSONArray();
		array.put("4042000091");
		requestParams.put("msisdn", array);
		requestParams.put("imei", "012629000228740");
		requestParams.put("accountNumber", "939856923");
		// System.out.println(requestParams.toString());

		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/getDeviceBlockStatus");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating getDeviceBlockStatus Operation Response ");
		Reporter.log("Note : /myPhone/getDeviceBlockStatus Service Internally calls devicedetails ");
		Reporter.log("Test data : Account eligible for lost/stolen/suspend/block");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("blockIndicator").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("reasonDescription")
					.contains("Reported lost by a T-Mobile customer"));
			Assert.assertTrue(response.jsonPath().getString("lineType").contains("DUPLICATE"));
			Assert.assertTrue(response.jsonPath().getString("isLost").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("isStolen").contains("false"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}
}
