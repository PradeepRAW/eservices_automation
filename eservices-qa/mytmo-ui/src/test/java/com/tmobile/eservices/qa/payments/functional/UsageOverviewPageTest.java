/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.CallDetailRecordsForwardPage;
import com.tmobile.eservices.qa.pages.payments.UsageOverviewPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class UsageOverviewPageTest extends PaymentCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(UsageOverviewPageTest.class);

//	/**
//	 * TC572_Ensure user can download usage records Usage records download
//	 * 
//	 * @param data
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
//			Group.IOS })
//	public void verifyUserCanDownloadUsageRecords(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("verifyUserCanDownloadUsageRecords method called in DelugeTest");
//
//		Reporter.log("Test Case Name: Verify User Can Download Usage Records");
//		Reporter.log("Test Data: ");
//		Reporter.log("================================");
//		Reporter.log("1.Launch application | Application should be launched");
//		Reporter.log("2.Login to the application  | User should be able to login Successfully");
//		Reporter.log("3.Verify Home page | Home page should be dispalyed");
//		Reporter.log("4.Click on Usage link | Usage Overview Page should be displayed");
//		Reporter.log("5.Click view all usage details | Call Record details page should display");
//		Reporter.log("6.Click Download Usage Records | Records should be downloaded");
//		Reporter.log("================================");
//		Reporter.log("Actual Steps");
//
//		UsageOverviewPage usageOverviewPage = navigateToUsageOverviewPage(myTmoData);
//		usageOverviewPage.clickViewAllUsageDetails();
//		Reporter.log(PaymentConstants.CALL_DETAIL_RECORDS_PAGE_DISPLAYED);
//		usageOverviewPage.downloadUsageRecords();
//		Reporter.log(PaymentConstants.USAGE_RECORDS_DOWNLOADED);
//	}

	/**
	 * Ensure user can view data stash details
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyDataStashDetails(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyDataStashDetails method called in DelugeTest");
		Reporter.log("Test Case Name:Verify Data Stash Details");
		Reporter.log("Test Data: ");
		Reporter.log("================================");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Overview Page should be displayed");
		Reporter.log("5.Verify data Stash | Verify Data Stash link should display");
		Reporter.log("================================");
		Reporter.log("Actual Steps");

		UsageOverviewPage usageOverviewPage = navigateToUsageOverviewPage(myTmoData);
		usageOverviewPage.verifyUsageOverviewPage();
		if (getDriver() instanceof AppiumDriver) {
			usageOverviewPage.verifyDataStashMobile();
		} else {
			usageOverviewPage.verifyDataStash();
		}
	}
	/**
	 * TC006_eServices_MyTMO_Unbilled_Usage Overview_To new columns
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyTabsOnUsageOverviewPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyTabsOnUsageOverviewPage method called in EBillTest");
		Reporter.log("Test Case : verify Tabs On Usage OverviewPage");
		Reporter.log("Test Data: ");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4.Click On Usage link | Usage overview page should be displayed");
		Reporter.log("4.1.Verify Lines tab | Lines Tab should be displayed");
		Reporter.log("5.Verify Minutes tab | Minutes Tab should be displayed");
		Reporter.log("6.Verify Voice tab | Voice Tab should be displayed");
		Reporter.log("7.Verify Messaging tab | Messaging Tab should be displayed");
		Reporter.log("8.Verify T-Mobile purchases tab | T-Mobile purchases Tab should be displayed");
		Reporter.log("9.Verify Third-party purchases tab | Third-party purchases Tab should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps");

		UsageOverviewPage usageOverviewPage = navigateToUsageOverviewPage(myTmoData);
		if (getDriver() instanceof AppiumDriver) {
			usageOverviewPage.verifyMobileTabs(PaymentConstants.USAGEPAGE_OVERVIEW_TABS_MINUTES);
			usageOverviewPage.verifyMobileTabs(PaymentConstants.USAGEPAGE_TABS_MESSAGES);
			usageOverviewPage.verifyMobileTabs(PaymentConstants.USAGEPAGE_TABS_DATA);
			usageOverviewPage.clickViewAllUsageDetails();
			CallDetailRecordsForwardPage callDetailRecordsForwardPage = new CallDetailRecordsForwardPage(getDriver());
			callDetailRecordsForwardPage.verifyPageLoaded();
			callDetailRecordsForwardPage.verifyVoiceMobile();
			callDetailRecordsForwardPage.clickTabToggleSelector();
			callDetailRecordsForwardPage.selectTabMobileDropdown("T-Mobile purchases");
			usageOverviewPage.verifyMobileTabs(PaymentConstants.USAGEPAGE_TABS_MOBILE_TMOBILE);

			callDetailRecordsForwardPage.clickTabToggleSelector();
			callDetailRecordsForwardPage.selectTabMobileDropdown("3rd party purchases");
			usageOverviewPage.verifyMobileTabs(PaymentConstants.TABS_MOBILE_THIRD_PARTY);
		} else {
			usageOverviewPage.verifyTabs(PaymentConstants.USAGEPAGE_OVERVIEW_TABS_LINES);
			usageOverviewPage.verifyTabs(PaymentConstants.USAGEPAGE_OVERVIEW_TABS_MINUTES);
			usageOverviewPage.verifyTabs(PaymentConstants.USAGEPAGE_TABS_MESSAGES);
			usageOverviewPage.verifyTabs(PaymentConstants.USAGEPAGE_TABS_DATA);
			usageOverviewPage.verifyTabs(PaymentConstants.USAGEPAGE_TABS_TMOBILE);
			usageOverviewPage.verifyTabs(PaymentConstants.USAGEPAGE_TABS_THIRD_PARTY);
		}
	}

	/**
	 * 8134545839 / CDwebtest1 Ensure user can view past months' usage
	 * overviews.
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RETIRED })
	public void verifyUserCanViewPastMonthUsageOverView(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserCanViewPastMonthUsageOverView method called in UsageTest");
		Reporter.log("Test Case Name: Ensure user can view past months' usage overviews");
		Reporter.log("Test Data: ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on USAGE Button | Usage over view page should be displayed");
		Reporter.log("5. Verify Usage over time Header | Usage over time Header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps");

		UsageOverviewPage usageOverviewPage = navigateToUsageOverviewPage(myTmoData);
		usageOverviewPage.selectPreviousBillCycle();
		usageOverviewPage.getTextUsageOverTimeHeader("Usage over time");
	}

	/**
	 * Require Test data, removed from Regression as all verification part is
	 * commented from earlier
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyUsagesharedLine(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUsagesharedLine method");
		Reporter.log("Regressionm Usage Share line with Name and MSISDN");
		Reporter.log("Test Data: ");
		Reporter.log("================================");
		Reporter.log("Test steps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Select MSISDN Account | Home Page Should be displayed");
		Reporter.log("4. Click on Usage link | Usage page should be displayed");
		Reporter.log("5. Verify Name is displayed in usage summary | Name should be displayed in usage summary");
		Reporter.log("6. Verify MSISDN is displayed in usage summary | MSISDN should be displayed in usage summary");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToUsageOverviewPage(myTmoData);
		UsageOverviewPage usageOverviewPage = new UsageOverviewPage(getDriver());
//		 Assert.assertTrue(usageOverviewPage.verifyNameusageSummary(),
//		 Constants.VERIFY_NAME_USAGESUMMARY);
//		 logStep(StepStatus.PASS, Constants.VERIFY_NAME_USAGESUMMARY,
//		 Constants.USAGESUMMARY_NAME);
//		 Reporter.log("Name is displayed in usage summary");
//		 Assert.assertTrue(usageOverviewPage.verifyMSISDNusageSummary(),
//		 Constants.VERIFY_MSISDN_USAGESUMMARY);
//		 logStep(StepStatus.PASS, Constants.VERIFY_MSISDN_USAGESUMMARY,
//		 Constants.USAGESUMMARY_MSISDN);
//		 Reporter.log("MSISDN is displayed in usage summary");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED})
	public void testUsageDetailsPageForUsageOverview(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserCanViewPastMonthUsageOverView method called in UsageTest");
		Reporter.log("Test Case Name: Ensure user can view past months' usage overviews");
		Reporter.log("Test Data: ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on USAGE Button | Usage over view page should be displayed");
		Reporter.log("5. Verify Usage over time Header | Usage over time Header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps");

		UsageOverviewPage usageOverviewPage = navigateToUsageOverviewPage(myTmoData);
		usageOverviewPage.verifyUsageOverviewPage();
		usageOverviewPage.clickViewAllUsageDetails();
		CallDetailRecordsForwardPage callDetailRecordsForwardPage = new CallDetailRecordsForwardPage(getDriver());
		callDetailRecordsForwardPage.verifyPageUrl();
		//usageOverviewPage.clickOnlogOutBtn();

	}

	
}
