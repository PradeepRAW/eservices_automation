package com.tmobile.eservices.qa.shop.functional;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import com.tmobile.eservices.qa.shop.ShopConstants;

public class CartPaymentPageTest extends ShopCommonLib {

	/**
	 * US156356 - Cart/Payment method inline errors : As a product owner I want to
	 * ensure in-line validation happens for payment method fields Whenever invalid
	 * data is entered
	 * [TA1910105](https://rally1.rallydev.com/#/detail/task/311847089948?fdp=true): Ensure Estimated Ship date matches on PDP, cart, confirmation pages (offline/prod only for EIP devices)
	 * C411000
	 * Verify estimated ship date format in cart page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.UAT })
	public void testEstimatedShipDateFormatedAndPaymentInfoInlineErrors(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify payment inline errors in cart page");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log(
				"1. Log in to the application with valid FA Standard customer  | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See all phones on shop page |PLP Page should be displayed");
		Reporter.log("4. Click on the Device on PLP page | Device PDP Page should be displayed ");
		Reporter.log("5. Select Payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Save Estimated Ship Date in PDP | Estimated Ship Date in PDP should be displayed and Saved");
		Reporter.log("7. Select Standard line on LS page | Phone Selection Page should be displayed");
		Reporter.log("8. Click on Skip TradeIn on Phone selection page | DeviceProtection Page should be displayed ");
		Reporter.log("9. Click on Continue on DeviceProtection page | Accessory PLP page should be displayed");
		Reporter.log("10. Click on Skip Accessory button | Cart page should be displayed");
		Reporter.log("11. Verify Ship date format and compare PDP Estimated Ship Date | Ship date format and PDP Estimated Ship Date should be displayed");
		Reporter.log("12. Click Continue to shipping button | Ship to different address button should be displayed");
		Reporter.log("13. Click Continue to Payment button | Payment information form should be displayed");
		Reporter.log("14. Verify card name error message| Card name error message should  be displayed");
		Reporter.log("15. Verify card error message| Card error message should be displayed");
		Reporter.log("16. Verify expiry date error message| Expiry date error message should be displayed");
		Reporter.log("17. Verify card CVV error message| Card CVV error message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");		
		
		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyEstimatedShipDate();
		String pdpPageEstimatedShipDate = pdpPage.getEstimatedShipDate();		
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickOnSkipTradeInLink();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage").equals("false")) {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		Assert.assertTrue(cartPage.verifyEstimatedShipDateFormat(), ShopConstants.ESTIMATED_SHIP_DATE_FORMAT_ERROR);
		Reporter.log("Estimated ship date format with forward slashs are displayed");
		String cartPageEstimatedShipDate = cartPage.getEstimatedShipDate();
		cartPage.compareEstimatedShipDate(pdpPageEstimatedShipDate, cartPageEstimatedShipDate);
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.getCCErrorMessage();
		//cartPage.getExpiryDateErrorMessage();
		//cartPage.getCVVErrorMessage();
	}

	/**
	 * US232031 :New Cart : Billing Address
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testNewCartBillingAddress(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US232031 :New Cart : Billing Address");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log(
				"1. Log in to the application with valid FA Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
		Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
		Reporter.log("4. Click on the device on Device PLP page |Device PDP page should be displayed ");
		Reporter.log("5. Select payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select Standared line on LS page | Trade in Phone Selection page should be displayed");
		Reporter.log(
				"7. Select device phone selection page and click on continue | Trade in confirmation page should be displayed");
		Reporter.log("8. Click on Continue buton on confirmation page |  PHP page should be displayed");
		Reporter.log("9. Click on Continue button on PHP page | Cart page should be displayed");
		Reporter.log("10. Click Continue to Shipping button| Shipping details should be displayed");
		Reporter.log("11. Click Continue to Payment button | Payment details form should be displayed");
		Reporter.log("12. Click Billing and Address check box | Billing address should be displayed");
		Reporter.log("13. VerifyBilling address labels | Billing address labels should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByFeatureDevicesWithSKipTradeIn(myTmoData, myTmoData.getDeviceName());

		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingDetails();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.clickBillingAndShippingCheckBox();
		cartPage.verifyBillingHeader();
		cartPage.verifyAddressLineOneLabel();
		cartPage.verifyAddressLineTwoLabel();
		cartPage.verifyPaymentsTabCityLabel();
		cartPage.verifyPaymentsTabStateLabel();
		cartPage.verifyPaymentsTabZIPcodeLabel();
	}

	/**
	 * US282746: New Cart : Formatting on Credit cards
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testNewCartFormattingOnCreditCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:New Cart :  Formatting on Credit cards");
		Reporter.log("Data Condition | IR PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log(
				"1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See all phones on shop page |PLP Page should be diplayed");
		Reporter.log("4. Click on the device on Device PLP page | Device PDP Page should be displayed ");
		Reporter.log("5. Select payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select Standared line on LS page |Phone Selection Page should be displayed");
		Reporter.log("7. Click on Skip Trade in Hyper link on Phone selection page | PHP Page should be displayed ");
		Reporter.log("8. Click on Continue on PHP page| Cart Page should be displayed");
		Reporter.log(
				"9. Click ContinueToShippingButton and Click ContinueToPaymentButton | Payment Information form should be displayed");
		Reporter.log(
				"10. Enter credit card Information and Verify credit card number formate |  Credit card number should be formated with '-'");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.placeOrderCTAEnabled();
	}

	/**
	 * US433516 - Service Agreement - Check Box (NON BYOD scenario) US406776 Payment
	 * capture fields in cart AAL Flow US433531- Service Agreement - Check Box/Place
	 * order CTA (NON BYOD scenario) US433514- Service Agreement - title and text
	 * for agreement (NON BYOD US474210: Service Agreement - Check Box/Text
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testPaymentCaptureFieldsServiceCheckBoxNonByodAALFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case US406776: Payment capture fields in cart");
		Reporter.log("Data Condition | IR PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line button in shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
		Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log(
				"10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be dispayed");
		Reporter.log(
				"11. Click ContinueToShippingButton and Click ContinueToPaymentButton | Payment Information form should be displayed");
		Reporter.log(
				"12. Verify Service customer agreement Check Box and Title | Service customer agreement Check box and Title should be displayed ");
		Reporter.log("13. Verify By default Check box is unchecked | By default check box should be unchecked ");
		Reporter.log("14. Verify Due Today text label | Due Today label should be displayed to the left side.");
		Reporter.log("15. Verify Due Today value | Due Today value should be displayed to the right of label");
		Reporter.log(
				"Verify Shipping Text and type	| Shipping text label and type should be displayed to the left side");
		Reporter.log("16. Verify Shipping value	| Shipping value should be displayed to the right of label");
		Reporter.log(
				"14. Verify Sales Tax & how's it calculated? | Sales Tax label and how's it calculated? should be displayed to the left side");
		Reporter.log("17. Verify Sales Tax pricing | Sales Tax price should be displayed to the right of label");
		Reporter.log("18. Verify Due Today Total text label | Due Today Total label should be displayed on left");
		Reporter.log("19. Verify Due Today Total value | Due Today Total value should be displayed on right of label");
		Reporter.log(
				"20. Verify Total Financed amount beneath due today total | Total Financed amount should be displayed beneath due today total");
		Reporter.log("21. Verify Name on card field	| Name on card field should be displayed, and editable");
		Reporter.log("22. Verify Credit Card Number field | Credit Card Number field should be editable");
		Reporter.log("23. Verify Expiration Date field | Expiration Date field should be editable");
		Reporter.log("24. Verify CVV field | CVV field should be editable");
		Reporter.log("25. Verify what's this? CTA | what's this? CTA should be displayed besides CVV field");
		Reporter.log("26. Click on what's this? CTA |	Modal should be opened for CVV");
		Reporter.log("27. Enter Card Information | Service customer agreement Check box should be displayed ");
		Reporter.log(
				"28. Select the Service customer agreement Check Box | Service customer agreement Check Box should be Selected ");
		Reporter.log("29. Verify 'Place order CTA' | 'Place order CTA' should be Enabled ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageFromAALNonBYODFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());		
		cartPage.getDueMonthlyTotalCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.serviceAgreementTitleDisplayed();
		cartPage.serviceAgreementCheckBoxDisplayed();
		cartPage.verifyServiceAgreementText();
		cartPage.verifyDueTodayLabelAtPayment();
		cartPage.verifyDueTodayAmountAtPayment();
		cartPage.verifyFullFinancedAmountLabelFRPDisplayedInPayment();
		cartPage.verifyPaymentFinancedAmount();
		cartPage.verifyFirstname();
		cartPage.verifyLastname();
		cartPage.verifyCardNumber();
		cartPage.verifyCVVEntered();
		cartPage.verifyCardExpiryDate();		
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.placeOrderCTAEnabled();
	}

	/**
	 * US433531- Service Agreement - Check Box/Place order CTA BYOD scenario)
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void verifyServiceAgreementPlaceOrderCTABYOD(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : US433531 - Service Agreement - Check Box/Place order CTA");
		Reporter.log(
				"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on 'Add A LINE' button | Customer intent page should be displayed");
		Reporter.log("5. Select on 'Bring my own device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("6. Click Continue button on Consolidated Rate Plan Page | Cart Page should be displayed");
		Reporter.log(
				"7. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("8. Click on 'Continue' CTA | Payment Information section should be displayed");
		Reporter.log("9. Verify 'Place order CTA' | 'Place order CTA' should be displayed ");
		Reporter.log(
				"10. Verify By default 'Place order CTA' is Disabled | By default 'Place order CTA' should be Disabled ");
		Reporter.log(
				"11. Verify Service customer agreement Check Box | Service customer agreement Check box should be displayed ");
		Reporter.log(
				"12. Select the Service customer agreement Check Box | Service customer agreement Check Box should be Selected ");
		Reporter.log("13. Verify 'Place order CTA' | 'Place order CTA' should be Enabled ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageFromAALBYODFlow(myTmoData);		
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.placeOrderCTADisabled();
		cartPage.serviceAgreementCheckBoxDisplayed();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.placeOrderCTAEnabled();
		

	}

	/**
	 * US406777 [TEST ONLY] Payment capture fields in cart -Error checks AAL Flow
	 * US433516 - Service Agreement - Check Box (BYOD scenario) US474210: Service
	 * Agreement - Check Box/Text US433514- Service Agreement - title and text for
	 * agreement (BYOD scenario)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testPaymentFieldsCartErrorChecksAALFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case US406777: Payment capture fields in cart -Error checks");
		Reporter.log("Data Condition | IR PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line button in shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Use my own Phone' option | Rate plan page should be displayed");
		Reporter.log("7. Click Continue on Rate Plan Page | Cart Page should be displayed");
		Reporter.log(
				"8. Click ContinueToShippingButton and Click ContinueToPaymentButton | Payment Information form should be displayed");
		Reporter.log(
				"9. Verify Service customer agreement Check Box | Service customer agreement Check box should be displayed ");
		Reporter.log(
				"10. Verify Service customer agreement title | Service customer agreement title should be displayed ");
		Reporter.log("11. Verify By default Check box is unchecked | By default check box should be unchecked ");
		Reporter.log(
				"12. Verify author able text next to check box.  | Verify this text is Displayed : Please check this box to accept");
		Reporter.log("13. Verify card number error message| Card error message should be displayed");
		Reporter.log("14. Verify expiry date error message| Expiry date error message should be displayed");
		Reporter.log("15. Verify card CVV error message| Card CVV error message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageFromAALBYODFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.serviceAgreementCheckBoxDisplayed();
		cartPage.serviceAgreementTitleDisplayed();
		cartPage.verifyServiceAgreementCheckBox();
		cartPage.verifyServiceAgreementText();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterInvalidCardNumber();
		cartPage.verifyInValidCreditCardErrorMessage();

	}
	/**
	 * US392776: Standard Upgrade : Reflect taxes based on shipping address
	 * US401930: Standard Upgrade : Reflect taxes based on shipping address
	 * (SDET Only)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testReflectTaxesBasedOnShippingAddress(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case US392776, US401930: Standard Upgrade : Reflect taxes based on shipping address");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select Monthly PaymentOption | Monthly PaymentOption should be selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line |  Verify Line Selector Details page is displayed");
		Reporter.log("10. Click Yes, lets get started button | Verify Trade In Device Condition Value page displayed");
		Reporter.log("11. Click Good Condition option | Verify Trade In Device Condition Confirmation page displayed");
		Reporter.log("12. Click Trade In This Phone CTA | Verify Insurance Migration page displayed");
		Reporter.log("13. Click continue button | Accessory PLP page should be displayed");
		Reporter.log("14. Click OK button on Dont brake the bank modal | Accessory PLP page should be displayed");
		Reporter.log("15. Select Skip Accessories | Cart page should be displayed");
		Reporter.log("16. Click Continue to Shipping CTA | Shipping page should be displayed");
		Reporter.log("17. Verify Address #1 and click Continue to Payment | Payment page should be displayed");
		Reporter.log(
				"18. Verify 'Sales tax' amount and 'Total Due today' amount. Store them as #1 | 'Sales tax' amount and 'Total Due today' amount should be displayed");
		Reporter.log("19. Click on Shipping tab | Shipping page should be displayed");
		Reporter.log("20. Modify shipping address, to be different from Address #1 | Address #2 should be provided");
		Reporter.log("21. Click Continue to Payment | Payment page should be displayed");
		Reporter.log(
				"22. Verify 'Sales tax' amount and 'Total Due today' amount. Store them #2 | 'Sales tax' amount and 'Total Due today' amount should be displayed");
		Reporter.log("23. Compare 'Sales tax' amounts #1 and #2 | 'Sales tax' amounts should be different");
		Reporter.log("24. Compare 'Total Due today' amounts #1 and #2| 'Total Due today' amounts should be different");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoryPLPPage accessoryPLPPage = navigateToAccessoryPLPPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingDetails();
		// cartPage.clickShipToDifferentAddress();
		// cartPage.provideShippingAddressFirst();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.verifySalesTaxAmountInPaymentsTab();
		cartPage.verifyDueTodayTotalInPaymentsTab();

		String SalesTaxCharges = cartPage.getSalesTaxAmountInPaymetsTab();
		String DueTodayTotalCharges = cartPage.getDueTodayTotalAmountInPaymetsTab();

		cartPage.clickShippingTab();
		cartPage.verifyShippingDetails();
		cartPage.clickShipToDifferentAddress();
		cartPage.provideShippingAddressSecond();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.verifySalesTaxAmountInPaymentsTab();
		cartPage.verifyDueTodayTotalInPaymentsTab();
		String NewSalesTaxCharges = cartPage.getSalesTaxAmountInPaymetsTab();
		String NewDueTodayTotalCharges = cartPage.getDueTodayTotalAmountInPaymetsTab();
		cartPage.compareSalesTaxAmount(SalesTaxCharges, NewSalesTaxCharges);
		cartPage.compareDueTodayTotalAmount(DueTodayTotalCharges, NewDueTodayTotalCharges);
	}
	/**
	 * US367993: PII : Mask Customers email address - Std Upgrade
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testCartPageMaskNameBillingShippingAddressStdUpgrade(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case US367993: PII : Mask Customers email address - Std Upgrade");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select Monthly PaymentOption | Monthly PaymentOption should be selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line |  Verify Line Selector Details page is displayed");
		Reporter.log("10. Click Yes, lets get started button | Verify Trade In Device Condition Value page displayed");
		Reporter.log("11. Click Good Condition option | Verify Trade In Device Condition Confirmation page displayed");
		Reporter.log("12. Click Trade In This Phone CTA | Verify Insurance Migration page displayed");
		Reporter.log("13. Click continue button | Accessory PLP page should be displayed");
		Reporter.log("14. Click OK button on Dont brake the bank modal | Accessory PLP page should be displayed");
		Reporter.log("15. Select Skip Accessories | Cart page should be displayed");
		Reporter.log(
				"16. Verify Customer Name contain masked element in class | Customer Name contain masked element in class");
		Reporter.log("17. Click Continue to Shipping CTA | Shipping page should be displayed");
		Reporter.log(
				"18. Verify 'Ship to:' address element contains common class for masking | 'Ship to:' address should contain class (e.g. pii_mask_data)");
		Reporter.log("19. Click Continue to Payment | Payment page should be displayed");
		Reporter.log(
				"20. Verify 'Billing' address element contains common class for masking | 'Billing' address should contain class (e.g. pii_mask_data)");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		String mask = "pii_mask_data";

		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.verifyMaskedElementForCustomerName(mask);
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingDetails();
		// cartPage.verifyMaskedElementForShipTo(mask);
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		//cartPage.verifyAllBillingAddressSameAsShippingMaskElementsInPaymentsTab(mask);
	}
	
	/**
	 * US588956:[MyTMO | SHOP | BUY]: MCSA-Individual customers are able to Upgrade through MyT-Mobile web
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT})
	public void testUpgradeFlowForMCSAcustomers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US588956:[MyTMO | SHOP | BUY]: MCSA-Individual customers are able to Upgrade through MyT-Mobile web");
		Reporter.log("Data Condition | MCSA-Individual Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");		
		Reporter.log("5. Select featured device | Product details page should be displayed");		
		Reporter.log("6. Click upgrade button | Line selection page should be displayed");
		Reporter.log("7. Select line | Line Selector Details page is displayed");
		Reporter.log("8. Click on Skip-TradeIn button | Device protection page should be displayed");		
		Reporter.log("9. Click continue button | Accessory PLP page should be displayed");		
		Reporter.log("10. Select Skip Accessories | Cart page should be displayed");
		Reporter.log("11. Click Continue to Shipping CTA | Shipping tab should be displayed");		
		Reporter.log("12. Click Continue to Payment | Payment tab should be displayed");
		Reporter.log("13. Fill payment details and select terms and conditions check box | Payments details should be entered");
		Reporter.log("14. Click on Agree and Place order button | Respective error shold be displayed for MCSA-Individual customes ");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

}
