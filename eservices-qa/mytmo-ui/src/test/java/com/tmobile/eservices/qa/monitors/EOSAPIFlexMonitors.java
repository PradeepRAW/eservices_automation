package com.tmobile.eservices.qa.monitors;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.EOSAPIFlexMonitorsHelper;
import com.tmobile.eservices.qa.api.eos.CustomerAccountApiV1;
import com.tmobile.eservices.qa.pages.accounts.api.ProfileApiV1;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class EOSAPIFlexMonitors extends EOSAPIFlexMonitorsHelper {

	@BeforeMethod(alwaysRun = true)
	public void clearTokenMapCommon(){
		tokenMapCommon = null;
	}
	
	/*getProfile--updateProfile--getProfile*/
	
	@Test(dataProvider = "byColumnName", enabled = true)
	public void profileUpdateFlow(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			tokenMap.put("email", apiTestData.getEmailId());
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Profile Update Flow</b> ");

			// 1. Invoke ProfileAPI.updateProfile 
			invokeGetProfile(apiTestData,tokenMap);
			checkEmptyValuesInTokenMap(tokenMap);
			
			// 2.  Invoke updateProfile
			invokeUpdateProfile(apiTestData, tokenMap);
			checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke ProfileAPI.getProfile

			//invokeGetProfile(apiTestData,tokenMap);
			//checkEmptyValuesInTokenMap(tokenMap);

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}
	
/*getProfile--updateProfile--getProfile*/
	
	@Test(dataProvider = "byColumnName", enabled = true)
	public void basicFlow(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			tokenMap.put("email", apiTestData.getEmailId());
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Flex API Flow</b> ");

			// 1. Invoke CustomerAccountApiV1.getCustomerAccountDetails 
			invokeGetCustomerAccountDetails(apiTestData, tokenMap);
			checkEmptyValuesInTokenMap(tokenMap);
			
			// 2. Invoke CustomerAccountApiV1.getSubscriberLines 
			invokeGetSubscriberLines(apiTestData, tokenMap);
			checkEmptyValuesInTokenMap(tokenMap);
			
			
		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}
	

	private void invokeUpdateProfile(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		ProfileApiV1 profile = new ProfileApiV1();
		String operationName = "updateProfile";	
		
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfile.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		
		logRequest(updatedRequest, operationName);
		Response response = profile.updateProfilePreferences(apiTestData, updatedRequest, tokenMap);
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		}else {
			failAndLogResponse(response, operationName);
		}
		
		List<String> elements=new ArrayList<String>();
		getResponseElementsMap(elements, response);
			
	}
	
	private void invokeUpdateProfilePreferences(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		ProfileApiV1 profile = new ProfileApiV1();
		String operationName = "updateProfilePrefernces";	
		
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfilePreferneces.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		
		logRequest(updatedRequest, operationName);
		Response response = profile.updateProfilePreferences(apiTestData, updatedRequest, tokenMap);
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		}else {
			failAndLogResponse(response, operationName);
		}
		
		List<String> elements=new ArrayList<String>();
		getResponseElementsMap(elements, response);
			
	}
	
	private void invokeUpdateProfilePaperlessBilling(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		ProfileApiV1 profile = new ProfileApiV1();
		String operationName = "updateProfilePaperlessBilling";	
		
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfilePaperlessBilling.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		
		logRequest(updatedRequest, operationName);
		Response response = profile.updateProfilePreferences(apiTestData, updatedRequest, tokenMap);
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		}else {
			failAndLogResponse(response, operationName);
		}
		
		List<String> elements=new ArrayList<String>();
		getResponseElementsMap(elements, response);
			
	}
	
	private void invokeUpdateProfileFeatures(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		ProfileApiV1 profile = new ProfileApiV1();
		String operationName = "updateProfileFeatures";	
		
		String requestBody = new ServiceTest().getRequestFromFile("Profile_updateProfileFeatures.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		
		logRequest(updatedRequest, operationName);
		Response response = profile.updateProfilePreferences(apiTestData, updatedRequest, tokenMap);
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		}else {
			failAndLogResponse(response, operationName);
		}
		
		List<String> elements=new ArrayList<String>();
		getResponseElementsMap(elements, response);
			
	}
	
	private void invokeGetProfile(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		ProfileApiV1 profile = new ProfileApiV1();
		List<String> elements=new ArrayList<String>();
		profile.getProfileRespElementsMap1(tokenMap, apiTestData, elements);
	}

	private void invokeGetProfileFeatures(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		ProfileApiV1 profile = new ProfileApiV1();
		String operationName = "getProfileFeatures";	
		
		Response response = profile.getProfileFeatures(apiTestData, "", tokenMap);
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		}else {
			failAndLogResponse(response, operationName);
		}
		
		List<String> elements=new ArrayList<String>();
		getResponseElementsMap(elements, response);
			
	}
	
	private void invokeGetProfilePreferences(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		ProfileApiV1 profile = new ProfileApiV1();
		String operationName = "getProfilePreferences";	
		
		Response response = profile.getProfilePreferences(apiTestData, tokenMap);
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		}else {
			failAndLogResponse(response, operationName);
		}
		
		List<String> elements=new ArrayList<String>();
		getResponseElementsMap(elements, response);
			
	}
	
	private void invokeGetProfilePaperlessBillingDetails(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		ProfileApiV1 profile = new ProfileApiV1();
		String operationName = "getProfilePaperlessBillingDetails";	
		
		Response response = profile.getProfilePaperlessBillingDetails(apiTestData, tokenMap);
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		}else {
			failAndLogResponse(response, operationName);
		}
		
		List<String> elements=new ArrayList<String>();
		getResponseElementsMap(elements, response);
			
	}
	
	private void invokeGetProfileDigits(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		ProfileApiV1 profile = new ProfileApiV1();
		String operationName = "getProfileDigits";	
		
		Response response = profile.getProfileDigits(apiTestData, tokenMap);
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		}else {
			failAndLogResponse(response, operationName);
		}
		
		List<String> elements=new ArrayList<String>();
		getResponseElementsMap(elements, response);
			
	}
	
	public void checkEmptyValuesInTokenMap(Map<String,String> tokenMap) throws Exception
	{
		Iterator<Map.Entry<String,String>> itr = tokenMap.entrySet().iterator();
		StringBuilder exceptionMessage = new StringBuilder();
		exceptionMessage = new StringBuilder();
		if(StringUtils.isNoneEmpty(tokenMap.get("exceptionMessage"))){
			exceptionMessage.append(tokenMap.get("exceptionMessage"));
		}
		while (itr.hasNext())
		{			
			Map.Entry<String, String> curr = itr.next();
			if (StringUtils.isEmpty(curr.getValue())){
				exceptionMessage = exceptionMessage.append(" "+curr.getKey() +" is Empty " +"\n");			
			}			
		}
		if(StringUtils.isNoneEmpty(exceptionMessage)){
		throw new Exception(exceptionMessage.toString());
		}
		
	}
	
	private void invokeGetCustomerAccountDetails(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		CustomerAccountApiV1 customerAccountApiV1 = new CustomerAccountApiV1();
		List<String> elements=new ArrayList<String>();
		elements.add("accountType");
		elements.add("accountSubType");
		Response response=customerAccountApiV1.getCustomerAccountDetails(apiTestData, tokenMap);
		logSuccessResponse(response, "getCustomerAccountDetails");
		Map<String,String> result=getResponseElementsMap(elements, response);
		System.out.println(result.get("accountType"));
		System.out.println(result.get("accountSubType"));
	}
	private void invokeGetSubscriberLines(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		CustomerAccountApiV1 customerAccountApiV1 = new CustomerAccountApiV1();
		List<String> elements=new ArrayList<String>();
		elements.add("lines.msisdn");
		Response response=customerAccountApiV1.getSubscriberLines(apiTestData, tokenMap);
		logSuccessResponse(response, "getSubscriberLines");
		Map<String,String> result=getResponseElementsMap(elements, response);
		System.out.println(result.get("lines.msisdn"));
	}
}
