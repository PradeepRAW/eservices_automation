/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.AutoPayConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.AutoPayPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class AutopayConfirmationPageTest extends PaymentCommonLib {

    /**
     * US283058 GLUE Light Reskin - AutoPay Setup Confirmation Page
     *
     * @param data
     * @param myTmoData Data Conditions - Regular user, eligible for card payments
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RETIRED})
    public void testGLRAutoPaySetupConfirmationpage(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US283058	GLUE Light Reskin - AutoPay Setup Confirmation Page");
        Reporter.log("Data Conditions - Regular user, eligible for card payments");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1: Launch Application | Application should be launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4: Verify Auto pay is Off | Auto Pay should be Off");
        Reporter.log("5: Click on Setup AUTOPAY button | Auto pay landing page should be displayed");
        Reporter.log("6: Click on payment method blade | Spoke page should be displayed");
        Reporter.log("7: Click on Add card blade | CARD information page should be displayed");
        Reporter.log("8: Add Payment details and click 'Continue' button | Auto pay Landing  page should be displayed");
        Reporter.log("9: Click 'Agree and Submit' button | Auto pay Confirmation message should be displayed");
        Reporter.log("10: Verify all Page elements | All expected Auto pay Confirmation elements should be displayed");
        Reporter.log("11. Navigate to Home page | Home page should be displayed");
        Reporter.log("12. Verify Autopay status 'ON' and click on it | Auto pay landing page should be displayed");
        Reporter.log("13. Click 'cancel autopay' | Auto pay Cancellation pop up should be displayed");
        Reporter.log(
                "14. Click Yes on pop up | Auto pay Should be cancelled and Cancel confirmation page should be displayed");
        Reporter.log("================================");
        Reporter.log("Actual Result:");

        AutoPayPage autopayLandingPage = navigateToAutoPayPage(myTmoData);
        String paymentType = autopayLandingPage.verifyStoredPaymentMethodOrAddBank(myTmoData);
        autopayLandingPage.clickAgreeAndSubmitBtn();
        AutoPayConfirmationPage autoPayConfirmationPage = new AutoPayConfirmationPage(getDriver());
        autoPayConfirmationPage.verifyAutoPayConfirmationPageElements(autoPayConfirmationPage, paymentType);
        autoPayConfirmationPage.clickreturnToHome();
        HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		cancelAutoPay(myTmoData);
    }

    /**
     * US262985 GLUE Light Reskin - AutoPay Update Confirmation Page
     *
     * @param data
     * @param myTmoData Data Conditions - Regular account. Autopay ON. Several stored
     *                  payment methods
     */
    @Test(dataProvider = "byColumnName", enabled = true)
    public void testGLRAutoPayUpdateConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US262985	GLUE Light Reskin - AutoPay Update Confirmation Page");
        Reporter.log("Data Conditions - Regular account. Autopay ON. Several stored payment methods");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1: Launch Application | Application should be launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4: Verify Auto pay is ON | Auto Pay should be ON");
        Reporter.log("5: Click on AUTOPAY ON button | Auto pay landing page should be displayed");
        Reporter.log(
                "6: Modify Payment details and click 'Agree and Submit' button | Auto pay Confirmation message should be displayed");
        Reporter.log("7. Navigate to Home page | Home page should be displayed");
        Reporter.log("8. Verify Autopay status 'ON' and click on it | Auto pay landing page should be displayed");
        Reporter.log("9. Click 'cancel autopay' | Auto pay Cancellation pop up should be displayed");
        Reporter.log(
                "10. Click Yes on pop up | Auto pay Should be cancelled and Cancel confirmation page should be displayed");
        Reporter.log("================================");
        Reporter.log("Actual Result:");

        navigateToHomePage(myTmoData);
        HomePage homePage = new HomePage(getDriver());
        if (!homePage.verifyAutoPay("ON")) {
            homePage.clickEasyPay();
            AutoPayPage autopayLandingPage = new AutoPayPage(getDriver());
            autopayLandingPage.verifyAutoPayLandingPageFields();
            autopayLandingPage.addBankToPaymentMethod(myTmoData);
            autopayLandingPage.clickAgreeAndSubmitBtn();
            AutoPayConfirmationPage confirmationPage = new AutoPayConfirmationPage(getDriver());
            confirmationPage.verifyPageLoaded();
            confirmationPage.verifySuccessHeader();
            confirmationPage.clickreturnToHome();
            verifyPage("Home page", "Home");
        }
        AutoPayPage autopayLandingPage = clickSetUpAutoPay();
        String accNum = autopayLandingPage.addBankToPaymentMethod(myTmoData);
        autopayLandingPage.clickAgreeAndSubmitBtn();
        AutoPayConfirmationPage confirmationPage = new AutoPayConfirmationPage(getDriver());
        confirmationPage.verifyAutoPayUpdateConfirmationPageElements(confirmationPage, accNum);
        confirmationPage.clickreturnToHome();
        homePage.verifyHomePage();
        AutoPayPage autoPayPage = clickSetUpAutoPay();
        autoPayPage.verifyNewPaymentDetailsUpdated(accNum, "Bank");
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING, Group.DUPLICATE})
    public void testGLRAutoPayUpdateConfirmationPageLoading(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US262985	GLUE Light Reskin - AutoPay Update Confirmation Page");
        Reporter.log("Data Conditions - Regular account. Autopay ON. Several stored payment methods");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1: Launch Application | Application should be launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4: Verify Auto pay is ON | Auto Pay should be ON");
        Reporter.log("5: Click on AUTOPAY ON button | Auto pay landing page should be displayed");
        Reporter.log(
                "6: Modify Payment details and click 'Agree and Submit' button | Auto pay Confirmation message should be displayed");
        Reporter.log("7. Navigate to Home page | Home page should be displayed");
        Reporter.log("8. Verify Autopay status 'ON' and click on it | Auto pay landing page should be displayed");
        Reporter.log("9. Click 'cancel autopay' | Auto pay Cancellation pop up should be displayed");
        Reporter.log(
                "10. Click Yes on pop up | Auto pay Should be cancelled and Cancel confirmation page should be displayed");
        Reporter.log("================================");
        Reporter.log("Actual Result:");
        navigateToHomePage(myTmoData);
        HomePage homePage = new HomePage(getDriver());
        AutoPayPage autopayLandingPage = clickSetUpAutoPay();
        String accNum = autopayLandingPage.addBankToPaymentMethod(myTmoData);
        autopayLandingPage.clickAgreeAndSubmitBtn();
        AutoPayConfirmationPage confirmationPage = new AutoPayConfirmationPage(getDriver());
            confirmationPage.verifyPageLoaded();


    }

    /**
     * US356346	PII Masking > Variables on Autopay Page
     *
     * @param data
     * @param myTmoData Data Conditions - Regular user, eligible for bank payments
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {"pending"})
    public void testPIIMaskingOnAutopayConfirmationPages(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("US356346	PII Masking > Variables on Autopay Page");
        Reporter.log("Data Conditions - Regular user, eligible for Bank payments");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("Step 1: Launch Application | Application should be launched");
        Reporter.log("Step 2. Login to the application | User Should be login successfully");
        Reporter.log("Step 3. Verify Home page | Home page should be displayed");
        Reporter.log("Step 4: Verify Auto pay is Off | Auto Pay should be Off");
        Reporter.log("Step 5: Click on Setup AUTOPAY button | Auto pay landing page should be displayed");
        Reporter.log("Step 6: verify PII masking | PII masking attribute should be present for payment method blade");
        Reporter.log("Step 7: Click on payment method blade | Spoke page should be displayed");
        Reporter.log("Step 8: verify PII masking | PII masking attribute should be present for stored payment details");
        Reporter.log("Step 9: Click on Add Bank blade | Bank information page should be displayed");
        Reporter.log("Step 10: Add Payment details and click 'Continue' button | Auto pay Landing  page should be displayed");
        Reporter.log("Step 11: Click 'Agree and Submit' button | Auto pay Confirmation message should be displayed");
        Reporter.log("Step 12: Verify all Page elements | All expected Auto pay Confirmation elements should be displayed");
        Reporter.log("Step 13: verify PII masking | PII masking attributes should be present for all payment information");
        Reporter.log("Step 14. Navigate to Home page | Home page should be displayed");
        Reporter.log("Step 15. Verify Autopay status 'ON' and click on it | Auto pay landing page should be displayed");
        Reporter.log("Step 16. Click 'cancel autopay' | Auto pay Cancellation pop up should be displayed");
        Reporter.log(
                "Step 17. Click Yes on pop up | Auto pay Should be cancelled and Cancel confirmation page should be displayed");
        Reporter.log("================================");
        Reporter.log("Actual Result:");

        AutoPayPage autopayLandingPage = navigateToAutoPayPage(myTmoData);
        String paymentType = autopayLandingPage.verifyStoredPaymentMethodOrAddBank(myTmoData);
        autopayLandingPage.verifyPIIMasking(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
        autopayLandingPage.clickAgreeAndSubmitBtn();
        AutoPayConfirmationPage autoPayConfirmationPage = new AutoPayConfirmationPage(getDriver());
        autoPayConfirmationPage.verifyAutoPayConfirmationPageElements(autoPayConfirmationPage, paymentType);
        autoPayConfirmationPage.verifyPIIMasking();
        autoPayConfirmationPage.clickreturnToHome();
        HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		cancelAutoPay(myTmoData);
    }
    
    
    /**
     * DE218554 User with PA pending is being allowed to set up AutoPay
     *
     * @param data
     * @param myTmoData Data Conditions - Regular user, eligible for card payments
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {})
    public void testAutoPaySetupNotAllowedForPAscheduledUser(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("DE218554	 User with PA pending is being allowed to set up AutoPay");
        Reporter.log("Data Conditions - Regular user with PA alredy scheduled");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1: Launch Application | Application should be launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4: Verify Auto pay is Off | Auto Pay should be Off");
        Reporter.log("5: Click on Setup AUTOPAY button | Auto pay landing page should be displayed");
        Reporter.log("6: Click on payment method blade | Spoke page should be displayed");
        Reporter.log("7: Click on Add card blade | CARD information page should be displayed");
        Reporter.log("8: Add Payment details and click 'Continue' button | Auto pay Landing  page should be displayed");
        Reporter.log("9: Click 'Agree and Submit' button |payment Unknown Error should be displayed");
        
        Reporter.log("================================");
        Reporter.log("Actual Result:");

        AutoPayPage autopayLandingPage = navigateToAutoPayPage(myTmoData);
        String paymentType = autopayLandingPage.verifyStoredPaymentMethodOrAddBank(myTmoData);
        autopayLandingPage.clickAgreeAndSubmitBtn();
        AutoPayConfirmationPage autoPayConfirmationPage = new AutoPayConfirmationPage(getDriver());
        autoPayConfirmationPage.verifyAutoPaySignupErrorforPAScheduledUser();

       
    }

}
