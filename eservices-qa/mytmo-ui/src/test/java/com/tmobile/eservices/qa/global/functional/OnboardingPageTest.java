package com.tmobile.eservices.qa.global.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.NewHomePage;

/**
 * 
 * @author rsuresh
 *
 */

public class OnboardingPageTest extends GlobalCommonLib {
	private static final Logger logger = LoggerFactory.getLogger(OnboardingPageTest.class);
	
	/** 4254425709 / Test12345
	 * Verify that Onboarding check list is not visible to Retricted Users
	 */
	@Test(dataProvider = "byColumnName",groups = {Group.PENDING})
	public void verifyOnboardingCheckListNotVisibleRetrictedUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyOnboardingCheckListNotVisibleRetrictedUser method called in OnboardingTest");
		Reporter.log("Test Case : Verify that Onboarding check list is not visible to Retricted Users");
		Reporter.log("Test Data Conditions: Msisdn with Restricted Access - Non PAH");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify Onboarding CheckList Not Visible Retricted User | Onboarding CheckList Not Visible Retricted User");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyOnBoardingFooterCheckList();
	}
		
	/** 4253013673 / Test12345
	 * Ensure onboarding functionality is suppressed, and not accessible to new or current users.
	 */
	@Test(dataProvider = "byColumnName",groups = {Group.PENDING})
	public void verifyOnboardingFunctionalityNotAccessiableToNewUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyOnboardingFunctionalityNotAccessiableToNewUser method called in OnboardingTest");
		Reporter.log("Test Case : Ensure onboarding functionality is suppressed, and not accessible to new or current users");
		Reporter.log("Test Data Conditions: New user Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify Onboarding Functionality Not Visible new user | Onboarding Functionality Not Visible new user");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage =navigateToNewHomePage(myTmoData);
		homePage.verifyOnBoardingFooterCheckList();
	}

}
