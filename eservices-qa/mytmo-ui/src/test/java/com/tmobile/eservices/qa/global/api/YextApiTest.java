package com.tmobile.eservices.qa.global.api;

import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.YextApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class YextApiTest extends YextApi {
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "YextApiTest", "testListStores", Group.GLOBAL,Group.SPRINT })
	public void testListStores(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Response rs = listStores("7807", "1");
		String operationName = "testListStores";
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				/*String phoneNumber= getPathVal(jsonNode, "phoneNumber");
				String teamCode= getPathVal(jsonNode, "teamCode");
				Assert.assertNotEquals(phoneNumber,"","phoneNumber is Null or Empty.");
				Assert.assertNotEquals(teamCode,"","teamCode is Null or Empty.");*/
			}else{
				failAndLogResponse(rs, operationName);
			}
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "YextApiTest", "testEntitiesList",Group.GLOBAL, Group.SPRINT })
	public void testEntitiesList(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Response rs = entitiesList("{\"address.line1\":{\"$eq\":\"27520 Covington Way SE\"}}");
		String operationName = "testEntitiesList";
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				/*String phoneNumber= getPathVal(jsonNode, "phoneNumber");
				String teamCode= getPathVal(jsonNode, "teamCode");
				Assert.assertNotEquals(phoneNumber,"","phoneNumber is Null or Empty.");
				Assert.assertNotEquals(teamCode,"","teamCode is Null or Empty.");*/
			}else{
				failAndLogResponse(rs, operationName);
			}
		}
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "YextApiTest", "testEntitiesList1",Group.GLOBAL, Group.SPRINT })
	public void testEntitiesList1(ControlTestData data, ApiTestData apiTestData) throws Exception {
		String requestBody="{\"address.region\":{\"$eq\":\"SD\"}}";
		String operationName="testEntitiesList_SD";
		logRequest(requestBody, operationName);
		Response rs = entitiesList(requestBody);
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(rs, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(rs.asString());
			if (!jsonNode.isMissingNode()) {
				/*String phoneNumber= getPathVal(jsonNode, "phoneNumber");
				String teamCode= getPathVal(jsonNode, "teamCode");
				Assert.assertNotEquals(phoneNumber,"","phoneNumber is Null or Empty.");
				Assert.assertNotEquals(teamCode,"","teamCode is Null or Empty.");*/
			}else{
				failAndLogResponse(rs, operationName);
			}
		}
	}
}
