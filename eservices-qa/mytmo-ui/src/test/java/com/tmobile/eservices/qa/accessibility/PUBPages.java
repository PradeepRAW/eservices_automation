package com.tmobile.eservices.qa.accessibility;

import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class PUBPages extends PaymentCommonLib {
	AccessibilityCommonLib accessibilityCommonLib = new AccessibilityCommonLib();

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testAccountHistoryPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAccountHistoryPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"AccountHistoryPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "AccountHistoryPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testAddBankPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAddBankPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"AddBankPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "AddBankPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testAddCardPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAddCardPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"AddCardPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "AddCardPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testAutopayCancelConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayCancelConfirmationPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"AutoPayCancelConfirmationPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "AutoPayCancelConfirmationPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testAutopayConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayConfirmationPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"AutoPayConfirmationPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "AutoPayConfirmationPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testAutoPayPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"AutoPayPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "AutoPayPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testBillAndPaySummaryPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToBillAndPaySummaryPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"BillAndPaySummaryPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "BillAndPaySummaryPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testBillDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToBillDetailsPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"BillDetailsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "BillDetailsPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testBillingSummaryPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToBillingSummaryPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"BillingSummaryPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "BillingSummaryPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testCallDatailRecordsForwardPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToCallDetailRecordsForwardPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"CallDetailRecordsForwardPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "CallDetailRecordsForwardPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testChargeEquipementPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToChargeEquipmentPage(myTmoData, "Equipment");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ChargeEquipementPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "ChargeEquipementPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testChargeFeaturesPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToChargeFeaturesPage(myTmoData,"Services");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ChargeFeaturesPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "ChargeFeaturesPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testChargeOnetimePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToChargeOneTimePage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ChargeOneTimePage");
		accessibilityCommonLib.testAccessibility(getDriver(), "ChargeOneTimePage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testChargePlanPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToChargePlanPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ChargePlanPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "ChargePlanPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testEIPDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEIPDetailsPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"EIPDetailsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "EIPDetailsPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testEIPPaymentConfirmationCCPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEIPPaymentConfirmationCCPage(myTmoData, "Card");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"EIPPaymentConfirmationCCPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "EIPPaymentConfirmationCCPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testEIPPaymentForwardPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEIPPaymentForwardPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"EIPPaymentForwardPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "EIPPaymentForwardPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testEIPPaymentReviewCCPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEIPPaymentReviewCCPage(myTmoData, "Card");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"EIPPaymentReviewCCPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "EIPPaymentReviewCCPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testFAQPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayFAQPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"AutoPayFAQPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "AutoPayFAQPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testLeaseDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLeaseDetailsPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"LeaseDetailsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "LeaseDetailsPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testOneTimePaymentPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToOTPpage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ChargeOneTimePage");
		accessibilityCommonLib.testAccessibility(getDriver(), "ChargeOneTimePage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testOTPAmountPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEditAmountPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"EditAmountPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "EditAmountPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testOTPConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToOTPConfirmationPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"OTPConfirmationPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "OTPConfirmationPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testOTPDatePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToOTPDatePage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"OTPDatePage");
		accessibilityCommonLib.testAccessibility(getDriver(), "OTPDatePage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testPAConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPAConfirmationPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PAConfirmationPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "PAConfirmationPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testPAInstallmentDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPAInstallmentDetailsPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PAInstallmentDetailsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "PAInstallmentDetailsPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testPaperlessBillingPage(ControlTestData data, MyTmoData myTmoData) throws Exception {
		navigateToPaperlessBillingPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PaperlessBillingPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "PaperlessBillingPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testPaymentArrangementPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPaymentArrangementPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PaymentArrangementPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "PaymentArrangementPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testPaymentErrorPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPaymentErrorPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PaymentErrorPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "PaymentErrorPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testProfilePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToProfilePage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ProfilePage");
		accessibilityCommonLib.testAccessibility(getDriver(), "ProfilePage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testSpokePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToSpokePage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"SpokePage");
		accessibilityCommonLib.testAccessibility(getDriver(), "SpokePage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testTnCPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayTnCPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"AutoPayTnCPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "AutoPayTnCPage");
	}
	
	
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testHistoricBillsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToHistoricBillsPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"HistoricBillsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "HistoricBillsPage");
	}
	
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testUsageDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		NavigatetoUsagedetailspage(myTmoData,"Data","Data details");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(), "UsageDetailsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "UsageDetailsPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testHomepage(ControlTestData data, MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"HomePage");
		accessibilityCommonLib.testAccessibility(getDriver(), "HomePage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pub" })
	public void testUsagePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToUsagePage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"UsagePage");
		accessibilityCommonLib.testAccessibility(getDriver(), "UsagePage");
	}

}
