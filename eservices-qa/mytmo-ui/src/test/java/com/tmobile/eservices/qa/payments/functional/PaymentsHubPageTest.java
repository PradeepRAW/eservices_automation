/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.PaymentsHubPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

/**
 * @author pshiva
 *
 */
public class PaymentsHubPageTest extends PaymentCommonLib {
	

	
	
	/**
	 * US545936 CCS: Profile | Payment Methods | Payment Method Notifications
	 * US551562 [Continued] Create New Payment entry Hub Page  (SP09)
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void verifyPaymentMethodsNotificationsMigration(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US551562 [Continued] Create New Payment entry Hub Page  (SP09)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Navigate to payment hub page | Should be able to navigate to Payment hub page");
		Reporter.log("5. Check for the OTP blade | Should be able to see OTP blade");
		Reporter.log("6. Check for the Autopay blade | Should be able to see Autopay blade");
		Reporter.log("7. Check for the PA blade | Should be able to see PA blade");
		Reporter.log("8. Check for the EIP blade | Should be able to see the EIP blade");
		Reporter.log("9. Check for the Manage my Payment methods. | Should be able to see it");
		
		Reporter.log("================================");
		HomePage homePage = navigateToHomePage(myTmoData);
		getDriver().get(System.getProperty("environment") + "/paymenthub");
		PaymentsHubPage paymentsHubPage = new PaymentsHubPage(getDriver());
		paymentsHubPage.verifyPageLoaded();
		paymentsHubPage.verifyOTPBlade();
		paymentsHubPage.verifyAutopayBlade();
		paymentsHubPage.verifyPABlade();
		paymentsHubPage.verifyEIPBlade();
		paymentsHubPage.verifyManageMyPaymentMethodsBlade();
		paymentsHubPage.verifyBackCTA();
		paymentsHubPage.clickBackCTA();
		homePage.verifyPageLoaded();
	}
}
