/**
 * 
 */
package com.tmobile.eservices.qa.tmng.functional;


import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.CommonLibrary;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.SpecialHoursPage;

/**
 * @author ksrivani
 *
 */
public class SpecialHoursPageTest extends CommonLibrary {
	/***
	 * US568892:Create special hours
	 * US568894:Store type
	 * US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)
	 * US579391:SH Internal Tool | Loading page
	 * US568897:SH Internal Tool | Store result page
	 * US587265:SH Internal Tool | Add Start & End date (UI)
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifySavingSpecialHoursSearchByCityStateForRetailForLessThanOrEqualTo10Stores(TMNGData tMNGData) {
	Reporter.log("US568892:Create special hours" );
	Reporter.log("US568894:Store type" );
	Reporter.log("US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)" );
	Reporter.log("US579391:SH Internal Tool | Loading page" );
	Reporter.log("US568897:SH Internal Tool | Store result page" );
	Reporter.log("US587265:SH Internal Tool | Add Start & End date (UI)" );
	Reporter.log("================================");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Verify the display of Create special hours title | Create special hours title is displayed");
	Reporter.log("Step 8: Verify the display of radio button labels | Verified successfully the radio button labels under Create special hours");
	Reporter.log("Step 9: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 10: Verify the display of Search for stores text below radio buttons | 'Select how you'd like to search for stores' text is verified");
	Reporter.log("Step 11: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 12: Verify the display of City,State,Zip text box | City,state,Zip text box is displayed successfully");
	Reporter.log("Step 13: Click on the text box and Verify the display of text in City,State,Zip text box | Text shown in City,State,Zip text box is verified");
	Reporter.log("Step 14: Validate the City | Validated successfully City entering has only Alphabets");
	Reporter.log("Step 15: Validate the State | Validated successfully State entering has two characters");
	Reporter.log("Step 16: Enter city,state in search text box | Entered city along with state in the search text box");
	Reporter.log("Step 17: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 18: Validate Results Sub Header| Results Sub Header is displayed");
	Reporter.log("Step 19: Validate All checkbox is pre checked| All checkbox should be pre checked");
	Reporter.log("Step 20: Validate every store has checkbox pre selected| Every store is prechecked, If not selecting 'All' checkbox, automatically selects all stores checkboxes");
	Reporter.log("Step 21: Validate any store checkbox deselected All checkbox deselected automatically| Deselecting any of the store checkbox, automatically deselects 'All' checkbox");
	Reporter.log("Step 22: Validate when all stores checkbox selected All checkbox selected automatically| Selecting all the stores checkboxes, automatically selects 'All' checkbox");
	Reporter.log("Step 23: Validate pagination not displayed when less than or equal to 10 stores in the result| Pagination is not displayed when less than or equal to 10 stores in the result successfully");
	Reporter.log("Step 24: Validate Continue or Cancel CTA displayed | Continue or Cancel CTA’s are displayed ");
	Reporter.log("Step 25: Validate Maximum of 1 store selected to enable continue button| Continue Button enabled when only one Store selected");
	Reporter.log("Step 26: Verify Continue and Cancel CTA's | Continue or Cancel CTA's displayed");
	Reporter.log("Step 27: Verify Continue button enabled when one store selected | Continue Button enabled when only one Store selected");
	Reporter.log("Step 28: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 29: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 30: Verify the display Special hours start date field | 'Special hours start date' text field should be displayed");
	Reporter.log("Step 31: Enter valid Start Date Manually in MM/DD/YYYY format | Entered date in the Start Date ");
	Reporter.log("Step 32: Enter valid end Date Manually in MM/DD/YYYY format | Entered date in the End Date");	
	Reporter.log("Step 33: Click on Closed All day radio button | clicked on Closed All day radio button");
	Reporter.log("Step 34: Click on Continue | Clicked on Continue Button");
	Reporter.log("=========================");
	
	launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
    specialHoursPage.verifyCreateSpecialHoursTitle();
	specialHoursPage.verifyRetailTprRadioButtonLabels();
	specialHoursPage.selectRetailButton();
	specialHoursPage.verifySearchForStoresText();
	specialHoursPage.selectCityStateZipButton();
	specialHoursPage.verifyCityStateZipTextBox();
	specialHoursPage.verifyTextInCityStateZipTextBox();
	specialHoursPage.validateCity(tMNGData.getCity());
	specialHoursPage.validateState(tMNGData.getState());
	specialHoursPage.setCityStateIntoTheSearchTextBox(tMNGData.getCity(), tMNGData.getState());
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySelectedStoresPage();
	specialHoursPage.verifyResultsSubheader();
	specialHoursPage.verifyDefaultAllCheckboxChecked();
	specialHoursPage.verifyEveryStorePrechecked();
	specialHoursPage.verifyDeselectingAStore();
    specialHoursPage.verifySelectingAllStores();
	specialHoursPage.verifyNoPaginationForLessThan10Stores();
	specialHoursPage.verifyContinueAndCancelCTA();
	specialHoursPage.verifyContinueButtonEnabledUponSelectingAStore();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyReasonAndTimeSelectionPage();
	specialHoursPage.verifyStartDateField();
	specialHoursPage.setStartDate(tMNGData.getStartDate());
	specialHoursPage.setEndDate(tMNGData.getEndDate());
	specialHoursPage.clickonClosedAllRadio();
	specialHoursPage.clickOnContinueButton();
	}

	/***
	 * US568892:Create special hours
	 * US568894:Store type
	 * US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)
	 * US568897:SH Internal Tool | Store result page
	 * US587265:SH Internal Tool | Add Start & End date (UI)
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifySavingSpecialHoursSearchBySapIdForRetailForMoreThan50Stores(TMNGData tMNGData) {
	Reporter.log("US568892:Create special hours" );
	Reporter.log("US568894:Store type" );
	Reporter.log("US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)" );
	Reporter.log("US568897:SH Internal Tool | Store result page" );	
	Reporter.log("US587265:SH Internal Tool | Add Start & End date (UI)" );
	Reporter.log("================================");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 8: Select SAP ID radio button | Selected SAP ID radio button");
	Reporter.log("Step 9: Verify the display of SAP ID text box | SAP ID text box is displayed successfully");
	Reporter.log("Step 10: Enter Multiple SAP ID in search text box | Entered multiple SAP ID separated by comma in the search text box");
	Reporter.log("Step 11: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 12: Validate Selected Stores page | Selected Stores page should be displayed with its header");
	Reporter.log("Step 13: Validate Results Sub Header| Results Sub Header is displayed");
	Reporter.log("Step 14: Validate All checkbox is pre checked| All checkbox should be pre checked");
	Reporter.log("Step 15: Validate every store has checkbox pre selected| Every store is prechecked, If not selecting 'All' checkbox, automatically selects all stores checkboxes");
	Reporter.log("Step 16: Validate any store checkbox deselected All checkbox deselected automatically| Deselecting any of the store checkbox, automatically deselects 'All' checkbox");
	Reporter.log("Step 17: Validate when all stores checkbox selected All checkbox selected automatically| Selecting all the stores checkboxes, automatically selects 'All' checkbox");
	Reporter.log("Step 18: Verify pagination exits | Pagination exits, as number of stores are greater than 10");
	Reporter.log("Step 19: Validate whether first page number is selected, if not click on First page | Page number 1 is selected successfully, and thus is disabled");
	Reporter.log("Step 20: Validate at a time 10 stores displayed when more than 10 stores present | At a time only\"+storeCheckbox.size()+\" stores displayed per page successfully");
	Reporter.log("Step 21: Validate Less than caret link disabled when First page selected | Less than caret link is disabled when First page is selected ");
	Reporter.log("Step 22: Select Last page number | Selected Last page number");
	Reporter.log("Step 23: Validate Greater than caret link enabled when first page selected | Greater than caret link is enabled when First page is selected");
	Reporter.log("Step 24: Validate Continue or Cancel CTA displayed | Continue or Cancel CTA’s are displayed ");
	Reporter.log("Step 25: Validate Maximum of 1 store selected to enable continue button| Continue Button enabled when only one Store selected");
	Reporter.log("Step 26: Verify Continue and Cancel CTA's | Continue or Cancel CTA's displayed");
	Reporter.log("Step 27: Verify Continue button enabled when one store selected | Continue Button enabled when only one Store selected");
	Reporter.log("Step 28: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 29: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 30: Verify the display Special hours start date field | 'Special hours start date' text field should be displayed");
	Reporter.log("Step 31: Enter valid Start Date Manually in MM/DD/YYYY format | Entered date in the Start Date ");
	Reporter.log("Step 32: Enter valid end Date Manually in MM/DD/YYYY format | Entered date in the End Date");	
	Reporter.log("Step 33: Click on Closed All day radio button | clicked on Closed All day radio button");
	Reporter.log("Step 34: Click on Continue | Clicked on Continue Button");
	Reporter.log("=========================");
	launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
	specialHoursPage.selectRetailButton();
	specialHoursPage.selectSapIdButton();
	specialHoursPage.verifySapIdTextBox();
	specialHoursPage.setSapIdIntoTheSearchTextBox(tMNGData.getSAPId());
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySelectedStoresPage();
	specialHoursPage.verifyResultsSubheader();
	specialHoursPage.verifyDefaultAllCheckboxChecked();
	specialHoursPage.verifyEveryStorePrechecked();
    specialHoursPage.verifyDeselectingAStore();
    specialHoursPage.verifySelectingAllStores();
    specialHoursPage.verifyPaginationAndStoresDisplayInAPageForMoreThan10Stores();
	specialHoursPage.verifyLessThanCaretLinkDisabledWhenSelectedFirstPage();
	specialHoursPage.verifyGreaterThanCaretLinkIsEnabled();
	specialHoursPage.verifyContinueAndCancelCTA();
	specialHoursPage.verifyContinueButtonEnabledUponSelectingAStore();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyReasonAndTimeSelectionPage();
	specialHoursPage.verifyStartDateField();
	specialHoursPage.setStartDate(tMNGData.getStartDate());
	specialHoursPage.setEndDate(tMNGData.getEndDate());
	specialHoursPage.clickonClosedAllRadio();
	specialHoursPage.clickOnContinueButton();
    }

	
	/***
	 * US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)
	 * TC307909
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT})
	public void verifyErrorMessageForBlankCityStateZipcode(TMNGData tMNGData) {
	Reporter.log("US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)" );
	Reporter.log("================================");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Select Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 8: Select City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 9: Do not enter the value of City, State, Zipcode in search text box and click on Continue button | Clicked on Continue button");
	Reporter.log("Step 10: Verify the error message displayed | Error message 'Please enter City, State or ZIP.' is displayed successfully");
	Reporter.log("=========================");
	launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
    specialHoursPage.selectRetailButton();
    specialHoursPage.selectCityStateZipButton();
    specialHoursPage.clickOnContinueButton();
    specialHoursPage.verifyErrorMessageForBlankCityStateZipcode();
    }
	
	/***
	 * US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)
	 * TC307910
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyErrorMessageForInvalidCityStateZipcode(TMNGData tMNGData) {
	Reporter.log("US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)" );
	Reporter.log("================================");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Select Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 8: Select City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 9: Enter invalid city or state or zipcode in search text box | Entered invalid value in the search text box");
	Reporter.log("Step 10: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 11: Verify the error message displayed | Error message 'Please enter in the format of City, State or ZIP Code.' is displayed successfullyy");
	Reporter.log("=========================");
	launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
    specialHoursPage.selectRetailButton();
    specialHoursPage.selectCityStateZipButton();
    specialHoursPage.setCityStateIntoTheSearchTextBox(tMNGData.getCity(),tMNGData.getState());
    specialHoursPage.clickOnContinueButton();
    specialHoursPage.verifyErrorMessageForInvalidCityStateZipcode();
    }
	
	/***
	 * US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)
	 * TC307911
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyErrorMessageForBlankSapID(TMNGData tMNGData) {
	Reporter.log("US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)" );
	Reporter.log("================================");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Select Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 8: Select SAP ID radio button | Selected SAP ID radio button");
	Reporter.log("Step 9: Do not enter the SAP ID value in search text box and click on Continue button | Clicked on Continue button");
	Reporter.log("Step 10: Verify the error message displayed | Error message 'Please enter SAP ID(s).' is displayed successfully");
	Reporter.log("=========================");
	launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
    specialHoursPage.selectRetailButton();
    specialHoursPage.selectSapIdButton();
    specialHoursPage.clickOnContinueButton();
    specialHoursPage.verifyErrorMessageForBlankSapID();
    }
	
	/***
	 * US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)
	 * TC307913
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyErrorMessageForInvalidSapID(TMNGData tMNGData) {
	Reporter.log("US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)" );
	Reporter.log("================================");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Select Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 8: Select SAP ID radio button | Selected SAP ID radio button");
	Reporter.log("Step 9: Enter invalid SAP ID in search text box | Entered invalid value in the search text box");
	Reporter.log("Step 10: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 11: Verify the error message displayed | Error message 'Please enter the SAP ID(s), separated by commas.' is displayed successfully");
	Reporter.log("=========================");
	launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
    specialHoursPage.selectRetailButton();
    specialHoursPage.selectSapIdButton();
	specialHoursPage.setSapIdIntoTheSearchTextBox(tMNGData.getSAPId());
    specialHoursPage.clickOnContinueButton();
    specialHoursPage.verifyErrorMessageForInvalidSapID();
    }
	
	/***
	 * US587265:SH Internal Tool | Add Start & End date (UI)
	 * US632405:SH Internal Tool | Set end date to 31 days max
	 * TC309736
	 * TC325688
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyErrorMessageForStartDateAndEndDateExceedingAnYear(TMNGData tMNGData) {
	Reporter.log("US568895:SH Internal Tool | Data validation – Client side (City, State, Zip)" );
	Reporter.log("================================");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Verify the display of Create special hours title | Create special hours title is displayed");
	Reporter.log("Step 8: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 9: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 10: Enter city,state in search text box | Entered city along with state in the search text box");
	Reporter.log("Step 11: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 12: Validate Selected Stores page | Selected Stores page should be displayed with its header");
	Reporter.log("Step 13: Click on Continue | Continue button should be clicked");
	Reporter.log("Step 14: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 15: Select Reason | Entered 'Reason' successfully");
	Reporter.log("Step 16: Enter description | Entered 'description' successfully");
	Reporter.log("Step 17: Verify the display Special hours start date field | 'Special hours start date' text field should be displayed");
	Reporter.log("Step 18: Enter date more than an year from current date | Entered date in the Start Date");
	Reporter.log("Step 19: Enter date more than 31 days from current date in End date | Entered date in the End Date");
	Reporter.log("Step 20: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 21: Verify the error message display | Error message 'Please select a start date that is within one year from today.' should be displayed");
	Reporter.log("Step 22: Verify the error message display | Error message 'Please select an end date that is within 31 days from the start date.' should be displayed");
	Reporter.log("=========================");
	launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
    specialHoursPage.verifyCreateSpecialHoursTitle();
    specialHoursPage.selectRetailButton();
    specialHoursPage.setCityStateIntoTheSearchTextBox(tMNGData.getCity(),tMNGData.getState());
    specialHoursPage.clickOnContinueButton();
    specialHoursPage.verifySelectedStoresPage();
    specialHoursPage.clickOnContinueButton();
    specialHoursPage.verifyReasonAndTimeSelectionPage();
    specialHoursPage.setReason(tMNGData.getReason());
	specialHoursPage.setDescription(tMNGData.getDescription());
    specialHoursPage.verifyStartDateField();
    specialHoursPage.setStartDate(tMNGData.getStartDate());
	specialHoursPage.setEndDate(tMNGData.getEndDate());
	specialHoursPage.clickonClosedAllRadio();
    specialHoursPage.clickOnContinueButton();
    specialHoursPage.verifyErrorMessageIfStartDateExceedsOneYearFromCurrentDate(tMNGData.getStartDate());
    specialHoursPage.verifyErrorMessageIfEndDateExceeds31DaysFromStartDate(tMNGData.getEndDate());
    }
	
	
	/**
	 * US591713 - SH Internal Tool | Summary page
	 * TC309621
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void testSHInternalToolSummaryPage(TMNGData tMNGData) {
		Reporter.log("================================");
		Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
		Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
		Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
		Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
		Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
		Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
		Reporter.log("Step 7: Select Retail radio button | Retail radio button selected");
		Reporter.log("Step 8: Select City/Zip code/State radio button |  City/Zip code/State radio button selected");
		Reporter.log("Step 9: Enter valid City/Zip code/State details |  valid City/Zip code/State details entered");
		Reporter.log("Step 10: Click on Continue | Continue button should be clicked");
		Reporter.log("Step 11: Validate Selected Stores page displayed |  Selected Stores page should be displayed");
		Reporter.log("Step 12: Click on Continu	e | Continue button should be clicked");
		Reporter.log("Step 13: Validate Select a reason and times page displayed |  Select a reason and times page displayed");
		Reporter.log("Step 14: Click on Start Date edit field  | Calendar control should be displayed");
		Reporter.log("Step 15: Enter Start Date Manually in MM/DD/YYYY format | date should be entered ");
		Reporter.log("Step 16: Validate Store hours sub header displayed  | Store hours sub header should be displayed");
		Reporter.log("Step 17: Validate Continue button enabled when Closed all Day radio button selected | Continue button should be enabled");
		Reporter.log("Step 18: Click on 'Exact times' radio button  | Exact times' radio button selected");
		Reporter.log("Step 19: validate opening time and closing time dropdowns selected | opening time and closing time dropdowns should be selected");
		Reporter.log("Step 20: Validate opening time and closing time has dropdown values have 12:00 AM (Midnight) to 11:30 PM at an interval of 30 minutes | dropdown values have 12:00 AM (Midnight) to 11:30 PM at an interval of 30 minutes should be displayed");
		Reporter.log("Step 21: Validate opening time and closing time has dropdown values selectable and display the value once selected | dropdown values should be selectable and displayed the value once selected");
		Reporter.log("Step 22: Click on Continue | Selected Stores page should be displayed");
		Reporter.log("Step 23: Click on Continue | Summary page displayed");
		Reporter.log("Step 24: Validate the StoreIDs selected displayed comma separated list |  StoreIDs selected should be displayed in comma separated list");
		Reporter.log("Step 25: Select Reason, enter description and select start date, end date and store hours | Select Reason, enter description and select start date, end date and store hours details entered");
		Reporter.log("Step 26: Click on Continue | confirmation page displayed");
		Reporter.log("Step 27: Validate confirmation header displayed | Confirmation header should be displayed");
		Reporter.log("Step 27: validate 'Store(s) ' selected with store ID value comma separated list displayed |  'Store(s) ' selected with store ID value comma list should be displayed");
		Reporter.log("Step 28: Click on Continue | Special Hours Saved.' confirmation message should be displayed");
		Reporter.log("Step 29: validate when click on cancel in confirmation page navigate to select a reason and times page and retain the previous selected and entered values | select a reason and times page should be displayed and retain the previous selected and entered values");

		launchSpecialHoursPage(tMNGData);
		SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
		specialHoursPage.verifySHLoginPage();
		specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
		specialHoursPage.verifySpecialHoursPage();
		specialHoursPage.selectRetailButton();
		specialHoursPage.selectCityStateZipButton();
		specialHoursPage.setCityStateIntoTheSearchTextBox(tMNGData.getCity(),tMNGData.getState());
		specialHoursPage.clickOnContinueButton();
		specialHoursPage.verifySelectedStoresPage();
		specialHoursPage.clickOnContinueButton();
		specialHoursPage.setReason(tMNGData.getReason());
		specialHoursPage.setDescription(tMNGData.getDescription());
		specialHoursPage.setStartDate(tMNGData.getStartDate());
		specialHoursPage.setEndDate(tMNGData.getEndDate());
		specialHoursPage.clickonExactTimesRadio();
		specialHoursPage.setOpeningTimeinOpeningDropDown(tMNGData.getOpeningTime());
		specialHoursPage.setClosingTimeinclosingDropDown(tMNGData.getClosingTime());
		specialHoursPage.clickOnContinueButton();
		specialHoursPage.verifyStoreSelctedFieldValue();
		specialHoursPage.verifyReasonValue(tMNGData.getReason());
		specialHoursPage.verifyDescriptionValue(tMNGData.getDescription());
		specialHoursPage.verifyStartDateAndEndDateInConfirmationPage(tMNGData.getStartDate(), tMNGData.getEndDate());
		specialHoursPage.verifyOpeningAndClosingTimeInConfirmationPage(tMNGData.getOpeningTime(), tMNGData.getClosingTime());
		specialHoursPage.clickOnCancelButton();
		specialHoursPage.verifyReasonAndTimeSelectionPage();
		specialHoursPage.clickOnContinueButton();
		specialHoursPage.clickOnContinueButton();
		specialHoursPage.verifySpecialHoursSavedMessage();
    }
	
	/**
	 * US591712 - SH Internal Tool | Add Hours
	 * TC309623
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void testSHInternalToolAddHours(TMNGData tMNGData) {
		Reporter.log("================================");
		Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
		Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
		Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
		Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
		Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
		Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
		Reporter.log("Step 7: Select Retail radio button | Retail radio button selected");
		Reporter.log("Step 8: Select City/Zip code/State radio button |  City/Zip code/State radio button selected");
		Reporter.log("Step 9: Enter valid City/Zip code/State details |  valid City/Zip code/State details entered");
		Reporter.log("Step 10: Click on Continue | Continue button should be clicked");
		Reporter.log("Step 11: Validate Selected Stores page displayed |  Selected Stores page should be displayed");
		Reporter.log("Step 12: Click on Continue | Continue button should be clicked");
		Reporter.log("Step 13: Validate Select a reason and times page displayed |  Select a reason and times page displayed");
		Reporter.log("Step 14: Click on Start Date edit field  | Calendar control should be displayed");
		Reporter.log("Step 15: Click on anywhere outside of start date field | Calendar control should not be displayed");
		Reporter.log("Step 16: Click on calendar control edit field  | Calendar control should be displayed");
		Reporter.log("Step 17: Enter Start Date Manually in MM/DD/YYYY format | date should be entered ");
		Reporter.log("Step 18: Enter valid End Date Manually in MM/DD/YYYY format | Entered date in the End Date ");
		Reporter.log("Step 19: Validate Closed all Day, Exact times, Early/Late radio buttons displayed | Closed all Day, Exact times, Early/Late radio buttons should be displayed");
		Reporter.log("Step 20: Validate Store hours sub header displayed  | Store hours sub header should be displayed");
		Reporter.log("Step 21: Click on 'Exact times' radio button  | Exact times' radio button selected");
		Reporter.log("Step 22: validate opening time and closing time dropdowns selected | opening time and closing time dropdowns should be selected");
		Reporter.log("Step 23: Validate opening time and closing time has dropdown values selectable and display the value once selected | dropdown values should be selectable and displayed the value once selected");
		Reporter.log("Step 24: validate when selecting closing time that is before opening time, click on continue |  error message 'Closing time cannot be before opening time' should be displayed");
		Reporter.log("Step 25: select 'Early/Late' radio button | 'Opening early or late' dropdown and the 'Closing early or late dropdown' should be displayed");
		Reporter.log("Step 26: Validate 'Opening early or late' dropdown and the 'Closing early or late' dropdown values | dropdown values 30 min early to 6hr early and 30 min late to 6hrs late with 30 min intervel");
		Reporter.log("Step 27: Validate when closing time hit after midnight  | error message 'Closing time cannot be at the next day.' should be displayed");
		Reporter.log("Step 28: Validate when opening time hit before midnight | error message 'Opening time cannot be at the previous day.' should be displayed");
		launchSpecialHoursPage(tMNGData);
		SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
		specialHoursPage.verifySHLoginPage();
		specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
		specialHoursPage.verifySpecialHoursPage();
	    specialHoursPage.verifyCreateSpecialHoursTitle();
		specialHoursPage.selectRetailButton();
	    specialHoursPage.selectCityStateZipButton();
	    specialHoursPage.setCityStateIntoTheSearchTextBox(tMNGData.getCity(),tMNGData.getState());
	    specialHoursPage.clickOnContinueButton();
	    specialHoursPage.verifySelectedStoresPage();
	    specialHoursPage.clickOnContinueButton();
	    specialHoursPage.verifyReasonAndTimeSelectionPage();
	    specialHoursPage.setReason(tMNGData.getReason());
	    specialHoursPage.setDescription(tMNGData.getDescription());
	    specialHoursPage.clickOnSelectDateField();
	    specialHoursPage.verifyCalendarIsDisplayed();
	    specialHoursPage.clickonCalendarLogo();
	    specialHoursPage.clickonCalendarLogo();
	    specialHoursPage.verifyCalendarIsDisplayed();
	    specialHoursPage.setStartDate(tMNGData.getStartDate());
	    specialHoursPage.validateClosedAllDayRadio();
		specialHoursPage.setEndDate(tMNGData.getEndDate());
	    specialHoursPage.validateExactTimesRadio();
	    //specialHoursPage.validateEarlyLateRadio();
	    specialHoursPage.validateSpecialHoursHeader();
	    specialHoursPage.clickonExactTimesRadio();
	    specialHoursPage.verifyOpeningAndClosingDropDownIsDisplayed();
	    specialHoursPage.selectValueFromOpeningAndClosingDropDown();
	    specialHoursPage.validateclosingTimeBeforeOpeningTime(tMNGData.getOpeningTime(),tMNGData.getClosingTime());
	    //specialHoursPage.clickonEarlyRadio();
	    //specialHoursPage.verifyOpeningAndClosingEarlyDropDownIsDisplayed();
	    //specialHoursPage.selectValueFromOpeningAndClosingEarlyDropDown();
	    //specialHoursPage.validateclosingTimeNextDay(tMNGData.getOpeningEarly(), tMNGData.getClosingEarly());
    }
	
	/***
	 * US568898: SH Internal Tool | Saved Special Hours page
	 * US603338: SH Internal Tool | Login
	 * TC317481
	 * TC317479
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifySavedSpecialHoursPage(TMNGData tMNGData) {
	Reporter.log("US568898: SH Internal Tool | Saved Special Hours page" );
	Reporter.log("US585723: SH Internal Tool | Add Reason & Description" );
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Verify the display of Special hours title | Special hours title is displayed");
	Reporter.log("Step 8: Verify the display of Create special hours title | Create special hours title is displayed");
	Reporter.log("Step 9: Verify the display of radio button labels | Verified successfully the radio button labels under Create special hours");
	Reporter.log("Step 10: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 11: Verify the display of Search for stores text below radio buttons | 'Select how you'd like to search for stores' text is verified");
	Reporter.log("Step 12: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 13: Verify the display of City,State,Zip text box | City,state,Zip text box is displayed successfully");
	Reporter.log("Step 14: Click on the text box and Verify the display of text in City,State,Zip text box | Text shown in City,State,Zip text box is verified");
	Reporter.log("Step 15: Validate the City | Validated successfully City entering has only Alphabets");
	Reporter.log("Step 16: Validate the State | Validated successfully State entering has two characters");
	Reporter.log("Step 17: Enter city,state in search text box | Entered city along with state in the search text box");
	Reporter.log("Step 18: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 19: Verify Store result page displayed with Selected stores| Selected Stores page is displayed with its header");
	Reporter.log("Step 20: Validate Results Sub Header| Results Sub Header is displayed");
	Reporter.log("Step 21: Validate All checkbox is pre checked| All checkbox should be pre checked");
	Reporter.log("Step 22: Validate pagination not displayed when less than or equal to 10 stores in the result| Pagination is not displayed when less than or equal to 10 stores in the result successfully");
	Reporter.log("Step 23: Validate Continue or Cancel CTA displayed | Continue or Cancel CTA’s are displayed ");
	Reporter.log("Step 24: Validate Maximum of 1 store selected to enable continue button| Continue Button enabled when only one Store selected");
	Reporter.log("Step 25: Verify Continue and Cancel CTA's | Continue or Cancel CTA's displayed");
	Reporter.log("Step 26: Verify Continue button enabled when one store selected | Continue Button enabled when only one Store selected");
	Reporter.log("Step 27: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 28: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 29: Select Reason | Entered 'Reason' successfully");
	Reporter.log("Step 30: Verify clearing Reason | Entered 'Reason' successfully");
	Reporter.log("Step 31: Select Reason | Entered 'Reason' successfully");
	Reporter.log("Step 32: Enter description | Entered 'description' successfully");
    Reporter.log("Step 33: Verify the display Special hours start date field | 'Special hours start date' text field should be displayed");
	Reporter.log("Step 34: Enter valid Start Date Manually in MM/DD/YYYY format | Entered date in the Start Date ");
	Reporter.log("Step 35: Enter valid End Date Manually in MM/DD/YYYY format | Entered date in the End Date ");
    Reporter.log("Step 36: Click on Closed All day radio button | clicked on Closed All day radio button");
	Reporter.log("Step 37: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 38: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 39: verify 'Special hours saved' header displayed | 'Special hours saved' header should be displayed");
	Reporter.log("Step 40: verify 'Saved special hours' header displayed | 'Saved special hours' header should be displayed");
	Reporter.log("Step 41: verify special hours event created with the reason on collapsed accordion header | special hours event should be created with the reason on collapsed accordion header");
	Reporter.log("Step 42: click on down caret icon and verify accordion expanded | Accordion should be expanded and SH event details should be displayed");
	Reporter.log("Step 43: verify special hour event date is later than today or future date | Special hour event date is verified in Saved events page");
	Reporter.log("Step 44: Verify Store(s) selected in comma separated list of SAP IDs displayed | Store(s) selected in comma separated list of SAP IDs should be displayed");
	Reporter.log("Step 45: Verify Reason header with reason value displayed | Reason header with reason value should be displayed");
	Reporter.log("Step 46: Verify Description header with Description value displayed | Description header with Description value should be displayed");
	Reporter.log("Step 47: Verify Special Hours header with Special Hours details displayed | Special Hours header with Special Hours details should be displayed");
	Reporter.log("Step 48: Verify Log out button in Saved Special hours page | Log out button is displayed in Saved Special hours page");
	Reporter.log("Step 49: Verify Click on Create special hours icon system takes to the create special hours page |  Click on Create special hours icon system should takes to the create special hours page");
    Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
    specialHoursPage.verifyCreateSpecialHoursTitle();
	specialHoursPage.verifyRetailTprRadioButtonLabels();
	specialHoursPage.selectRetailButton();
	specialHoursPage.verifySearchForStoresText();
	specialHoursPage.selectCityStateZipButton();
	specialHoursPage.verifyCityStateZipTextBox();
	specialHoursPage.verifyTextInCityStateZipTextBox();
	specialHoursPage.validateCity(tMNGData.getCity());
	specialHoursPage.validateState(tMNGData.getState());
	specialHoursPage.setCityStateIntoTheSearchTextBox(tMNGData.getCity(), tMNGData.getState());
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySelectedStoresPage();
	specialHoursPage.verifyResultsSubheader();
	specialHoursPage.verifyDefaultAllCheckboxChecked();
	specialHoursPage.verifyNoPaginationForLessThan10Stores();
	specialHoursPage.verifyContinueAndCancelCTA();
	specialHoursPage.verifyContinueButtonEnabledUponSelectingAStore();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyReasonAndTimeSelectionPage();
	specialHoursPage.setReason(tMNGData.getReason());
	specialHoursPage.verifyClearingReason();
	specialHoursPage.setReason(tMNGData.getReason());
	specialHoursPage.setDescription(tMNGData.getDescription());
	specialHoursPage.verifyStartDateField();
	specialHoursPage.setStartDate(tMNGData.getStartDate());
	specialHoursPage.setEndDate(tMNGData.getEndDate());
	specialHoursPage.clickonClosedAllRadio();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySpecialHoursSaved();
	specialHoursPage.verifySavedSpecialHoursHeader();
	specialHoursPage.verifySpecialHoursEventReasons();
	specialHoursPage.verifyAccordionExpandsUponClick();
	specialHoursPage.verifySpecialHoursEventDate();
	specialHoursPage.verifyStoresWithSapIdInsavedSpecialHours();
	specialHoursPage.verifyReasonHeaderWithReasonValue();
	specialHoursPage.verifyDescriptionHeaderWithDescriptionValue();
	specialHoursPage.verifySpecialHoursHeaderWithEventDate();
	specialHoursPage.verifyLogOutInSavedEvents();
	specialHoursPage.verifyAndClickCreateSpecialHoursIcon();
	}
	
	/***
	 * US597770: SH Internal Tool | Remove saved events
	 * US585723: SH Internal Tool | Add Reason & Description
	 * TC317482
	 * TC318330
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyRemoveSavedEvents(TMNGData tMNGData) {
	Reporter.log("US597770: SH Internal Tool | Remove saved events" );
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Verify the display of Create special hours title | Create special hours title is displayed");
	Reporter.log("Step 8: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 9: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
    Reporter.log("Step 10: Enter city,state in search text box | Entered city along with state in the search text box");
	Reporter.log("Step 11: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 12: Verify Store result page displayed with Selected stores| Selected Stores page is displayed with its header");
    Reporter.log("Step 13: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 14: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 15: Select Reason | Entered 'Reason' successfully");
	Reporter.log("Step 16: Enter description | Entered 'description' successfully");
	Reporter.log("Step 17: Enter valid Start Date Manually in MM/DD/YYYY format | Entered date in the Start Date ");
	Reporter.log("Step 18: Enter valid End Date Manually in MM/DD/YYYY format | Entered date in the End Date ");
	Reporter.log("Step 19: Click on Closed All day radio button | clicked on Closed All day radio button");
	Reporter.log("Step 20: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 21: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 22: verify 'Special hours saved' header displayed | Special hours saved' header should be displayed");
	Reporter.log("Step 23: verify special hours event created with the reason on collapsed accordion header | special hours event should be created with the reason on collapsed accordion header");
	Reporter.log("Step 24: click on down caret icon and verify accordion expanded | Accordion should be expanded and SH event details should be displayed");
	Reporter.log("Step 25: Verify Click Remove Icon displayed a Modal | Clicked on Remove icon, should display a Modal");
	Reporter.log("Step 26: Modal has 'Are you sure that you want to delete this event?' text | Are you sure that you want to delete this event?' text should be displayed");
	Reporter.log("Step 27: verify 2 CTAs 'Cancel' and 'Confirm' displayed | 2 CTAs 'Cancel' and 'Confirm' should be displayed ");
	Reporter.log("Step 28: verify Click on Cancel CTA close the Modal | Modal should be closed when click on cancel CTA");
	Reporter.log("Step 29: Click on Confirm CTA | Clicked on Confirm CTA");
	Reporter.log("Step 30: verify SH event is deleted | SH event should be deleted");
    Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
    specialHoursPage.verifyCreateSpecialHoursTitle();
	specialHoursPage.selectRetailButton();
	
	specialHoursPage.selectSapIdButton();
	specialHoursPage.setSapIdIntoTheSearchTextBox(tMNGData.getSAPId());
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySelectedStoresPage();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyReasonAndTimeSelectionPage();
	specialHoursPage.setReason(tMNGData.getReason());
	specialHoursPage.setDescription(tMNGData.getDescription());
	specialHoursPage.setStartDate(tMNGData.getStartDate());
	specialHoursPage.setEndDate(tMNGData.getEndDate());
	specialHoursPage.clickonClosedAllRadio();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySpecialHoursSaved();
	specialHoursPage.verifySpecialHoursEventReasons();
	int eventsCountBeforeRemoval = specialHoursPage.numberOfReasonEventsBeforeRemove();
	specialHoursPage.verifyAccordionExpandsUponClick();
	specialHoursPage.verifyAndClickRemoveIcon();
	specialHoursPage.verifyRemoveModal();
	specialHoursPage.verifyRemoveModalCTA();
	specialHoursPage.clickOnCancelCTA();
	specialHoursPage.verifyAndClickRemoveIcon();
	specialHoursPage.clickOnConfirmCTA();
	specialHoursPage.verifyShEventDeleted(eventsCountBeforeRemoval);
    }
	
	/***
	 * US618286: SH Internal Tool | Saved event entry point
	 * TC319566
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifySavedEventEntryPoint(TMNGData tMNGData) {
	Reporter.log("US618286: SH Internal Tool | Saved event entry point" );
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Verify 'Saved events' link with test and caret is displayed in the home page | Saved events link along with its caret should be displayed in 'Select store' page");
	Reporter.log("Step 8: Click on 'Saved events' link | Clicked on Saved events link");
	Reporter.log("Step 9: Verify user is navigated to Saved events details page | Navigated to Saved events page");
    Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
	specialHoursPage.verifySavedEventsLink();
	specialHoursPage.clickOnSavedEventslink();
	specialHoursPage.verifySavedEventsPage();
    }
	
	/***
	 * US618287: SH Internal Tool | Logout
	 * TC319442
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyLogoutFromSelectStorePage(TMNGData tMNGData) {
	Reporter.log("US618287: SH Internal Tool | Logout" );
	Reporter.log("================================");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Verify Log out CTA displayed | Logout CTA should be displayed");
	Reporter.log("Step 8: Click on log out CTA | Clicked on Logout button");
	Reporter.log("Step : Verify user is logged out of SH internal tool app, and navigated to Login page | Special hours Login page is displayed");    
	Reporter.log("=========================");
	launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
	specialHoursPage.verifyLogOut();
	specialHoursPage.clickOnLogOut();
	specialHoursPage.verifySHLoginPage();
    }
	
	/***
	 * US618287: SH Internal Tool | Logout
	 * TC319443
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyLogoutFromSelectReasonPage(TMNGData tMNGData) {
	Reporter.log("US618287: SH Internal Tool | Logout" );
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 8: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 9: Enter city,state in search text box | Entered city along with state in the search text box");
	Reporter.log("Step 10: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 11: Verify Store result page displayed with Selected stores| Selected Stores page is displayed with its header");
	Reporter.log("Step 12: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 13: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 14: Verify Log out CTA displayed | Logout CTA should be displayed");
	Reporter.log("Step 15: Click on log out CTA | Clicked on Logout button");
	Reporter.log("Step 16: Verify user is logged out of SH internal tool app, and navigated to Login page | Special hours Login page is displayed");    
	Reporter.log("=========================");
	launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
	specialHoursPage.selectRetailButton();
	specialHoursPage.selectCityStateZipButton();
	specialHoursPage.setCityStateIntoTheSearchTextBox(tMNGData.getCity(), tMNGData.getState());
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySelectedStoresPage();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyReasonAndTimeSelectionPage();
	specialHoursPage.verifyLogOut();
	specialHoursPage.clickOnLogOut();
	specialHoursPage.verifySHLoginPage();
    }
	
	/***
	 * US618287: SH Internal Tool | Logout
	 * TC319526
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyLogoutFromConfirmationPage(TMNGData tMNGData) {
	Reporter.log("US618287: SH Internal Tool | Logout" );
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 8: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 9: Enter city,state in search text box | Entered city along with state in the search text box");
	Reporter.log("Step 10: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 11: Verify Store result page displayed with Selected stores| Selected Stores page is displayed with its header");
	Reporter.log("Step 12: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 13: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 14: Enter valid Start Date Manually in MM/DD/YYYY format | Entered date in the Start Date ");
	Reporter.log("Step 15: Enter valid End Date Manually in MM/DD/YYYY format | Entered date in the End Date ");
	Reporter.log("Step 16: Click on Closed All day radio button | clicked on Closed All day radio button");
	Reporter.log("Step 17: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 18: Verify Log out CTA displayed | Logout CTA should be displayed");
	Reporter.log("Step 19: Click on log out CTA | Clicked on Logout button");
	Reporter.log("Step 20: Verify user is logged out of SH internal tool app, and navigated to Login page | Special hours Login page is displayed");    
	Reporter.log("=========================");
	launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
	specialHoursPage.selectRetailButton();
	specialHoursPage.selectCityStateZipButton();
	specialHoursPage.setCityStateIntoTheSearchTextBox(tMNGData.getCity(), tMNGData.getState());
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySelectedStoresPage();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyReasonAndTimeSelectionPage();
	specialHoursPage.setReason(tMNGData.getReason());
	specialHoursPage.setDescription(tMNGData.getDescription());
	specialHoursPage.setStartDate(tMNGData.getStartDate());
	specialHoursPage.setEndDate(tMNGData.getEndDate());
	specialHoursPage.clickonClosedAllRadio();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyLogOut();
	specialHoursPage.clickOnLogOut();
	specialHoursPage.verifySHLoginPage();
    }
	
	
	/***
	 * US618287: SH Internal Tool | Logout
	 * TC321496
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyLogoutFromSavedEventsPage(TMNGData tMNGData) {
	Reporter.log("US618287: SH Internal Tool | Logout" );
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 8: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 9: Enter city,state in search text box | Entered city along with state in the search text box");
	Reporter.log("Step 10: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 11: Verify Store result page displayed with Selected stores| Selected Stores page is displayed with its header");
	Reporter.log("Step 12: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 13: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 14: Enter valid Start Date Manually in MM/DD/YYYY format | Entered date in the Start Date ");
	Reporter.log("Step 15: Enter valid End Date Manually in MM/DD/YYYY format | Entered date in the End Date ");
	Reporter.log("Step 16: Click on Closed All day radio button | clicked on Closed All day radio button");
	Reporter.log("Step 17: Click on Continue | Clicked on Continue Button");	
	Reporter.log("Step 18: Verify Log out CTA displayed | Logout CTA should be displayed");
	Reporter.log("Step 19: Click on log out CTA | Clicked on Logout button");
	Reporter.log("Step 20: Verify user is logged out of SH internal tool app, and navigated to Login page | Special hours Login page is displayed");    
	Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
	specialHoursPage.selectRetailButton();
	specialHoursPage.selectCityStateZipButton();
	specialHoursPage.setCityStateIntoTheSearchTextBox(tMNGData.getCity(), tMNGData.getState());
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySelectedStoresPage();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyReasonAndTimeSelectionPage();
	specialHoursPage.setReason(tMNGData.getReason());
	specialHoursPage.setDescription(tMNGData.getDescription());
	specialHoursPage.setStartDate(tMNGData.getStartDate());
	specialHoursPage.setEndDate(tMNGData.getEndDate());
	specialHoursPage.clickonClosedAllRadio();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyLogOutInSavedEvents();
	specialHoursPage.clickOnLogOutInSavedEvents();
	specialHoursPage.verifySHLoginPage();
    }
	
	/***
	 * US627029: [Unfinished] SH Internal Tool | Login
	 * TC322113
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifySpecialHoursLogIn(TMNGData tMNGData) {
	Reporter.log("US627029: [Unfinished] SH Internal Tool | Login" );
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed");   
	Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
    }
	
	/***
	 * US627029: [Unfinished] SH Internal Tool | Login
	 * TC322114
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifySpecialHoursLogInWithAlreadyLoggedInUser(TMNGData tMNGData) {
	Reporter.log("US627029: [Unfinished] SH Internal Tool | Login" );
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Click on log out CTA | Clicked on Logout button");
	Reporter.log("Step 8: Verify user is able to login with Logged in email | Selected Logged in email to Login");
	Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
	specialHoursPage.clickOnLogOut();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
    }
	
	/***
	 * US605992: SH internal Tool | Conflicting events on same date, same store
	 * TC319975
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void testConflictingEventsOnSameDateForSameStore(TMNGData tMNGData) {
	
	Reporter.log("US605992: SH internal Tool | Conflicting events on same date, same store" );
	Reporter.log("================================");
	Reporter.log("Pre condition: An event with some date");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours page | Special hours page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 4: Click on Login button | Clicked on Login button");
	Reporter.log("Step 5: Verify the display of Create special hours title | Create special hours title is displayed");
	Reporter.log("Step 6: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 7: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 8: Enter city,state in search text box | Entered city along with state in the search text box");
	Reporter.log("Step 9: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 10: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 11: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 12: Select the Reason | Selected Reason from the drop down ");
	Reporter.log("Step 13: Enter the same Start Date Manually in MM/DD/YYYY format for the same store | Entered date in the Start Date ");
	Reporter.log("Step 18: Enter valid End Date Manually in MM/DD/YYYY format | Entered date in the End Date ");
    Reporter.log("Step 15: Click on Closed All day radio button | clicked on Closed All day radio button");
	Reporter.log("Step 16: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 17: Verify the display of success Modal | 'A special hour event on <effective date> has been created for SAP ID <SAP ID>. Please contact <user email> for further information.' text should be displayed");

	
	launchSpecialHoursPage(tMNGData); 
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
	specialHoursPage.selectSapIdButton();
	specialHoursPage.setSapIdIntoTheSearchTextBox(tMNGData.getSAPId());
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyReasonAndTimeSelectionPage();
	specialHoursPage.ClickOnReasonDropDown();
	specialHoursPage.setReason(tMNGData.getReason());
	specialHoursPage.setDescription(tMNGData.getDescription());
	specialHoursPage.setStartDate(tMNGData.getStartDate());
	specialHoursPage.setEndDate(tMNGData.getEndDate());
	specialHoursPage.clickonClosedAllRadio();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyConfirmationPage();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyPopUpForConflictingEvent();
	
	
	
	}
	/***
	 * US605992: SH internal Tool | Conflicting events on same date, same store
	 * TC320005
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void testEventsOnDifferentDateForSameStore(TMNGData tMNGData) {
	
		Reporter.log("US605992: SH internal Tool | Conflicting events on same date, same store" );
		Reporter.log("================================");
		Reporter.log("Pre condition: An event with some date");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch Special hours page | Special hours page is displayed");
		Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
		Reporter.log("Step 3: Enter ntpassword in text box | Entered ntpassword into the password text box");
		Reporter.log("Step 4: Click on Login button | Clicked on Login button");
		Reporter.log("Step 5: Verify the display of Create special hours title | Create special hours title is displayed");
		Reporter.log("Step 6: Click on Retail radio button | Able to Select Retail radio button");
		Reporter.log("Step 7: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
		Reporter.log("Step 8: Enter city,state in search text box | Entered city along with state in the search text box");
		Reporter.log("Step 9: Click on Continue button| Clicked on Continue button");
		Reporter.log("Step 10: Click on Continue button | Clicked on Continue button");
		Reporter.log("Step 11: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
		Reporter.log("Step 12: Select the Reason | Selected Reason from the drop down ");
		Reporter.log("Step 13: Enter the different Start Date Manually in MM/DD/YYYY format for the same store | Entered date in the Start Date ");
		Reporter.log("Step 18: Enter valid End Date Manually in MM/DD/YYYY format | Entered date in the End Date ");
		Reporter.log("Step 15: Click on Closed All day radio button | clicked on Closed All day radio button");
		Reporter.log("Step 16: Click on Continue | Clicked on Continue Button");
		Reporter.log("Step 17: Click on Continue | Clicked on Continue Button");
		Reporter.log("Step 18: verify 'Special hours saved' header displayed | Special hours saved' header should be displayed");
		
		launchSpecialHoursPage(tMNGData);
		SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
		specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
		specialHoursPage.verifySpecialHoursPage();
		specialHoursPage.selectSapIdButton();
		specialHoursPage.setSapIdIntoTheSearchTextBox(tMNGData.getSAPId());
		specialHoursPage.clickOnContinueButton();
		specialHoursPage.verifySelectedStoresPage();
		specialHoursPage.clickOnContinueButton();
		specialHoursPage.verifyReasonAndTimeSelectionPage();
		specialHoursPage.ClickOnReasonDropDown();
		specialHoursPage.setReason(tMNGData.getReason());
		specialHoursPage.setDescription(tMNGData.getDescription());
		specialHoursPage.setStartDate(tMNGData.getStartDate());
		specialHoursPage.setEndDate(tMNGData.getEndDate());
		specialHoursPage.clickonClosedAllRadio();
		specialHoursPage.clickOnContinueButton();
		specialHoursPage.verifyConfirmationPage();
		specialHoursPage.clickOnContinueButton();
		specialHoursPage.verifySavedSpecialHoursHeader();

	}
	
	/***
	 * US597722: [Continued] SH Internal Tool | User types
	 * TC320025
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyEmergencyUserCreatesAnEvent(TMNGData tMNGData) {
	Reporter.log("US597722: [Continued] SH Internal Tool | User types" );
	Reporter.log("================================");
	Reporter.log("Pre condition: A valid credentials of an Emergency user");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter Emergency user permissioned email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 7: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 8: Enter city, state in search text box | Entered city along with state in the search text box");
	Reporter.log("Step 9: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 10: Verify Store result page displayed with Selected stores| Selected Stores page is displayed with its header");
	Reporter.log("Step 11: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 12: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 13: Click on the Reason drop down | Clicked on Reason drop down ");
	Reporter.log("Step 14: Verify the display of options in Reason drop down | User must be able to see the list of emergency reasons consisting of  'Fire, Flood, Hurricane, Snow ,Smoke ,Storm ,Tornado, Earthquake, Other weather , Other emergency'");
	Reporter.log("Step 15: Select Reason | Selected Reason from the Reason drop down");
	Reporter.log("Step 16: Enter description | Entered 'description' successfully");
	Reporter.log("Step 17: Enter valid Start Date Manually in MM/DD/YYYY format | Entered date in the Start Date ");
	Reporter.log("Step 18: Enter valid End Date Manually in MM/DD/YYYY format | Entered date in the End Date ");
	Reporter.log("Step 19: Click on Closed All day radio button | clicked on Closed All day radio button");
	Reporter.log("Step 20: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 21: Verify the display of Confirmation page | Special hours event confirmation page is displayed");
	Reporter.log("Step 22: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 23: verify special hours event created with the reason on collapsed accordion header | special hours event should be created with the selected reason on collapsed accordion header");  
	Reporter.log("Step 24: click on down caret icon and verify accordion expanded | Accordion should be expanded and SH event details should be displayed");
	Reporter.log("Step 25: verify special hour event date is later than today or future date | Special hour event date is verified in Saved events page");
	Reporter.log("Step 26: Verify Store(s) selected in comma separated list of SAP IDs displayed | Store(s) selected in comma separated list of SAP IDs should be displayed");
	Reporter.log("Step 27: Verify Reason header with reason value displayed | Reason header with reason value should be displayed");
	Reporter.log("Step 28: Verify Description header with Description value displayed | Description header with Description value should be displayed");
	Reporter.log("Step 29: Verify Special Hours header with Special Hours details displayed | Special Hours header with Special Hours details should be displayed");
    Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
    specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.selectRetailButton();
	specialHoursPage.selectSapIdButton();
	specialHoursPage.setSapIdIntoTheSearchTextBox(tMNGData.getSAPId());
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySelectedStoresPage();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyReasonAndTimeSelectionPage();
	specialHoursPage.ClickOnReasonDropDown();
	specialHoursPage.verifyReasonOptionsForEmergencyUser();
	specialHoursPage.setReason(tMNGData.getReason());
	specialHoursPage.setDescription(tMNGData.getDescription());
	specialHoursPage.setStartDate(tMNGData.getStartDate());
	specialHoursPage.setEndDate(tMNGData.getEndDate());	
	specialHoursPage.clickonClosedAllRadio();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyConfirmationPage();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySpecialHoursEventReasons();
	specialHoursPage.verifyAccordionExpandsUponClick();
	specialHoursPage.verifySpecialHoursEventDate();
	specialHoursPage.verifyStoresWithSapIdInsavedSpecialHours();
	specialHoursPage.verifyReasonHeaderWithReasonValue();
	specialHoursPage.verifyDescriptionHeaderWithDescriptionValue();
	specialHoursPage.verifySpecialHoursHeaderWithEventDate();
    }
	
	/***
	 * US597722: [Continued] SH Internal Tool | User types
	 * TC320168
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyHolidayNPIUserCreatesAnEvent(TMNGData tMNGData) {
	Reporter.log("US597722: [Continued] SH Internal Tool | User types" );
	Reporter.log("================================");
	Reporter.log("Pre condition: A valid credentials of a Holiday NPI user");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter Holiday NPI permissioned email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 7: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 8: Enter city,state in search text box | Entered city along with state in the search text box");
	Reporter.log("Step 9: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 10: Verify Store result page displayed with Selected stores| Selected Stores page is displayed with its header");
	Reporter.log("Step 11: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 12: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 13: Click on the Reason drop down | Clicked on Reason drop down ");
	Reporter.log("Step 14: Verify the display of options in Reason drop down | User must be able to see the list of reasons consisting of 'Holiday, New Product Introduction'");
	Reporter.log("Step 15: Select Reason | Selected Reason from the Reason drop down");
	Reporter.log("Step 16: Enter description | Entered 'description' successfully");
	Reporter.log("Step 17: Enter valid Start Date Manually in MM/DD/YYYY format | Entered date in the Start Date ");
	Reporter.log("Step 18: Enter valid End Date Manually in MM/DD/YYYY format | Entered date in the End Date ");
	Reporter.log("Step 19: Click on Closed All day radio button | clicked on Closed All day radio button");
	Reporter.log("Step 20: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 21: Verify the display of Confirmation page | Special hours event confirmation page is displayed");
	Reporter.log("Step 22: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 23: verify special hours event created with the reason on collapsed accordion header | special hours event should be created with the selected reason on collapsed accordion header");  
	Reporter.log("Step 24: click on down caret icon and verify accordion expanded | Accordion should be expanded and SH event details should be displayed");
	Reporter.log("Step 25: verify special hour event date is later than today or future date | Special hour event date is verified in Saved events page");
	Reporter.log("Step 26: Verify Store(s) selected in comma separated list of SAP IDs displayed | Store(s) selected in comma separated list of SAP IDs should be displayed");
	Reporter.log("Step 27: Verify Reason header with reason value displayed | Reason header with reason value should be displayed");
	Reporter.log("Step 28: Verify Description header with Description value displayed | Description header with Description value should be displayed");
	Reporter.log("Step 29: Verify Special Hours header with Special Hours details displayed | Special Hours header with Special Hours details should be displayed");
    Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
    specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.selectRetailButton();
	specialHoursPage.selectSapIdButton();
	specialHoursPage.setSapIdIntoTheSearchTextBox(tMNGData.getSAPId());
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySelectedStoresPage();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyReasonAndTimeSelectionPage();
	specialHoursPage.ClickOnReasonDropDown();
	specialHoursPage.verifyReasonOptionsForHolidayNPIUser();
	specialHoursPage.setReason(tMNGData.getReason());
	specialHoursPage.setDescription(tMNGData.getDescription());
	specialHoursPage.setStartDate(tMNGData.getStartDate());
	specialHoursPage.setEndDate(tMNGData.getEndDate());	
	specialHoursPage.clickonClosedAllRadio();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyConfirmationPage();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySpecialHoursEventReasons();
	specialHoursPage.verifyAccordionExpandsUponClick();
	specialHoursPage.verifySpecialHoursEventDate();
	specialHoursPage.verifyStoresWithSapIdInsavedSpecialHours();
	specialHoursPage.verifyReasonHeaderWithReasonValue();
	specialHoursPage.verifyDescriptionHeaderWithDescriptionValue();
	specialHoursPage.verifySpecialHoursHeaderWithEventDate();
    }
	
	/***
	 * US597722: [Continued] SH Internal Tool | User types
	 * TC320166
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifySuperUserCreatesAnEvent(TMNGData tMNGData) {
	Reporter.log("US597722: [Continued] SH Internal Tool | User types" );
	Reporter.log("================================");
	Reporter.log("Pre condition: A valid credentials of an Super user");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter Super user permissioned email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 7: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 8: Enter city,state in search text box | Entered city along with state in the search text box");
	Reporter.log("Step 9: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 10: Verify Store result page displayed with Selected stores| Selected Stores page is displayed with its header");
	Reporter.log("Step 11: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 12: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 13: Click on the Reason drop down | Clicked on Reason drop down ");
	Reporter.log("Step 14: Verify the display of options in Reason drop down | User should be able to see from a list of reasons consisting of 'Fire, Flood, Hurricane, Snow ,Smoke ,Storm ,Other weather ,Other emergency, Holiday, New Product Introduction'");
	Reporter.log("Step 15: Select Reason | Selected Reason from the Reason drop down");
	Reporter.log("Step 16: Enter description | Entered 'description' successfully");
	Reporter.log("Step 17: Enter valid Start Date Manually in MM/DD/YYYY format | Entered date in the Start Date ");
	Reporter.log("Step 18: Enter valid End Date Manually in MM/DD/YYYY format | Entered date in the End Date ");
	Reporter.log("Step 19: Click on Closed All day radio button | clicked on Closed All day radio button");
	Reporter.log("Step 20: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 21: Verify the display of Confirmation page | Special hours event confirmation page is displayed");
	Reporter.log("Step 22: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 23  Verify special hours event created with the reason on collapsed accordion header | special hours event should be created with the selected reason on collapsed accordion header");  
	Reporter.log("Step 24: click on down caret icon and verify accordion expanded | Accordion should be expanded and SH event details should be displayed");
	Reporter.log("Step 25: verify special hour event date is later than today or future date | Special hour event date is verified in Saved events page");
	Reporter.log("Step 26: Verify Store(s) selected in comma separated list of SAP IDs displayed | Store(s) selected in comma separated list of SAP IDs should be displayed");
	Reporter.log("Step 27: Verify Reason header with reason value displayed | Reason header with reason value should be displayed");
	Reporter.log("Step 28: Verify Description header with Description value displayed | Description header with Description value should be displayed");
	Reporter.log("Step 29: Verify Special Hours header with Special Hours details displayed | Special Hours header with Special Hours details should be displayed");
    Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
    specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.selectRetailButton();
	specialHoursPage.selectSapIdButton();
	specialHoursPage.setSapIdIntoTheSearchTextBox(tMNGData.getSAPId());
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySelectedStoresPage();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyReasonAndTimeSelectionPage();
	specialHoursPage.ClickOnReasonDropDown();
	specialHoursPage.verifyReasonOptionsForSuperUser();
	specialHoursPage.setReason(tMNGData.getReason());
	specialHoursPage.setDescription(tMNGData.getDescription());
	specialHoursPage.setStartDate(tMNGData.getStartDate());
	specialHoursPage.setEndDate(tMNGData.getEndDate());	
	specialHoursPage.clickonClosedAllRadio();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyConfirmationPage();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySpecialHoursEventReasons();
	specialHoursPage.verifyAccordionExpandsUponClick();
	specialHoursPage.verifySpecialHoursEventDate();
	specialHoursPage.verifyStoresWithSapIdInsavedSpecialHours();
	specialHoursPage.verifyReasonHeaderWithReasonValue();
	specialHoursPage.verifyDescriptionHeaderWithDescriptionValue();
	specialHoursPage.verifySpecialHoursHeaderWithEventDate();
    }
	
	/***
	 * US597722: [Continued] SH Internal Tool | User types
	 * TC320182
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyInvalidUserPermissions(TMNGData tMNGData) {
	Reporter.log("US597722: [Continued] SH Internal Tool | User types" );
	Reporter.log("================================");
	Reporter.log("Pre condition: An invalid credentials which do not have any user type permissions");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours page | Special hours page is displayed");
	Reporter.log("Step 2: Enter email of an invalid user in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 4: Click on Login button | Clicked on Login button");
	Reporter.log("Step 5: Verify the error message display | A modal of error message 'Please contact SpecialStoreHoursAdmin@t-mobile.com for access.' should be displayed for invalid user");
	Reporter.log("Step 6: Verify 'Close' CTA display | Close button is displayed in Modal pop up");
	Reporter.log("Step 7: Click on 'Close' CTA ('x') in Modal pop up | Clicked on Close button in Modal pop up");
	Reporter.log("Step 8: Clicked on logged in email to sign out | Selected Logged in email to sign out");
	Reporter.log("Step 9: Verify the display of Special hours login page | Special hours Login page is displayed");
    Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
    specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifyErrorMessageInModalPopUp();
	specialHoursPage.verifycloseButtonInModalPopUp();
	specialHoursPage.clickOncloseButtonInModalPopUp();
	specialHoursPage.verifySHLoginPageAfterLogout();

    }
	
	/***
	 * US597722: [Continued] SH Internal Tool | User types
	 * TC325558
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TMNG })
	public void verifyInvalidUserRedirectsToLandingPage(TMNGData tMNGData) {
	Reporter.log("US597722: [Continued] SH Internal Tool | User types" );
	Reporter.log("================================");
	Reporter.log("Pre condition: An invalid credentials which do not have any user type permissions");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours page | Special hours page is displayed");
	Reporter.log("Step 2: Enter email of an invalid user in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 4: Click on Login button | Clicked on Login button");
	Reporter.log("Step 5: Verify the error message display | A modal of error message 'Please contact SpecialStoreHoursAdmin@t-mobile.com for access.' should be displayed for invalid user");
	Reporter.log("Step 6: Enter Landing page url in the browser | Entered Landing page URL in the browser");
	Reporter.log("Step 7: Verify User is redirected to Sign out confirmation page | User should be redirected to Sign out confirmation page");
	Reporter.log("Step 8: Clicked on logged in email to sign out | Selected Logged in email to sign out");
	Reporter.log("Step 9: Verify the display of Special hours login page | Special hours Login page is displayed");
    Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
    specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifyErrorMessageInModalPopUp();
	specialHoursPage.verifyRedirectingToLandingPage();
	specialHoursPage.verifySHLoginPageAfterLogout();

    }
	
	/***
	 * CDCDWR2-515:SH Internal tool | Enhancements - hint text, spinner, expand all | Test & Support
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT , Group.TMNG })
	public void verifySpecialHoursEnhancements(TMNGData tMNGData) {
	Reporter.log("CDCDWR2-515:SH Internal tool | Enhancements - hint text, spinner, expand all | Test & Support" );
	Reporter.log("================================");
	Reporter.log("Step 1: Launch Special hours application by entering URL | Special hours Login page is displayed");
	Reporter.log("Step 2: Enter email in text box | Entered email into the Email text box");
	Reporter.log("Step 3: Click on Next button | Clicked on Next button in Login page");
	Reporter.log("Step 4: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 5: Click on Sign In button | Clicked on Sign In button");
	Reporter.log("Step 6: Verify Special hours page  | Special hours page is displayed"); 
	Reporter.log("Step 7: Verify the display of Create special hours title | Create special hours title is displayed");
	Reporter.log("Step 8: Verify the display of radio button labels | Verified successfully the radio button labels under Create special hours");
	Reporter.log("Step 9: Click on Retail radio button | Able to Select Retail radio button");
	Reporter.log("Step 10: Verify the display of Search for stores text below radio buttons | 'Select how you'd like to search for stores' text is verified");
	Reporter.log("Step 11: Click on City,State,Zip radio button | Selected City,State,Zip radio button");
	Reporter.log("Step 12: Verify the display of City,State,Zip text box | City,state,Zip text box is displayed successfully");
	Reporter.log("Step 13: Click on the text box and Verify the display of text in City,State,Zip text box | Text shown in City,State,Zip text box is verified");
	Reporter.log("Step 14: Validate the City | Validated successfully City entering has only Alphabets");
	Reporter.log("Step 15: Validate the State | Validated successfully State entering has two characters");
	Reporter.log("Step 16: Enter city,state in search text box | Entered city along with state in the search text box");
	Reporter.log("Step 17: Click on Continue button| Clicked on Continue button");
	Reporter.log("Step 18: Validate Results Sub Header| Results Sub Header is displayed");
	Reporter.log("Step 19: Validate All checkbox is pre checked| All checkbox should be pre checked");
	Reporter.log("Step 20: Validate every store has checkbox pre selected| Every store is prechecked, If not selecting 'All' checkbox, automatically selects all stores checkboxes");
	Reporter.log("Step 21: Validate any store checkbox deselected All checkbox deselected automatically| Deselecting any of the store checkbox, automatically deselects 'All' checkbox");
	Reporter.log("Step 22: Validate when all stores checkbox selected All checkbox selected automatically| Selecting all the stores checkboxes, automatically selects 'All' checkbox");
	Reporter.log("Step 23: Validate pagination not displayed when less than or equal to 10 stores in the result| Pagination is not displayed when less than or equal to 10 stores in the result successfully");
	Reporter.log("Step 24: Validate Continue or Cancel CTA displayed | Continue or Cancel CTA’s are displayed ");
	Reporter.log("Step 26: Verify Continue and Cancel CTA's | Continue or Cancel CTA's displayed");
	Reporter.log("Step 28: Click on Continue button | Clicked on Continue button");
	Reporter.log("Step 29: Validate clicking on continue lands on 'Reason and Time Selection' page | 'Reason and Time Selection' page is displayed successfully");
	Reporter.log("Step 30: Verify the display Special hours start date field | 'Special hours start date' text field should be displayed");
	Reporter.log("Step 31: Enter valid Start Date Manually in MM/DD/YYYY format | Entered date in the Start Date ");
	Reporter.log("Step 32: Enter valid end Date Manually in MM/DD/YYYY format | Entered date in the End Date");	
	Reporter.log("Step 33: Click on Closed All day radio button | clicked on Closed All day radio button");
	Reporter.log("Step 34: Click on Continue | Clicked on Continue Button");
	Reporter.log("Step 35: verify 'Special hours saved' header displayed | Special hours saved' header should be displayed");
	Reporter.log("Step 36: verify special hours event created with the reason on collapsed accordion header | special hours event should be created with the reason on collapsed accordion header");
	Reporter.log("Step 37: click on down caret icon and verify accordion expanded | Accordion should be expanded and SH event details should be displayed");
	Reporter.log("Step 38: Verify Click Remove Icon displayed a Modal | Clicked on Remove icon, should display a Modal");
	Reporter.log("Step 39: Modal has 'Are you sure that you want to delete this event?' text | Are you sure that you want to delete this event?' text should be displayed");
	Reporter.log("Step 40: verify 2 CTAs 'Cancel' and 'Confirm' displayed | 2 CTAs 'Cancel' and 'Confirm' should be displayed ");
	Reporter.log("Step 41: verify Click on Cancel CTA close the Modal | Modal should be closed when click on cancel CTA");
	Reporter.log("Step 42: Click on Confirm CTA | Clicked on Confirm CTA");
	Reporter.log("Step 43: verify SH event is deleted | SH event should be deleted");
	Reporter.log("Step 44: Click on create special hours icon | Special hours page should be displayed");
	
	Reporter.log("=========================");
	
	launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
	specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifySpecialHoursPage();
	specialHoursPage.selectSapIdButton();
	specialHoursPage.verifySapIdTextBox();
	specialHoursPage.setSapIdIntoTheSearchTextBox("9596");
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySelectedStoresPage();
	specialHoursPage.verifyResultsSubheader();
	specialHoursPage.verifyDefaultAllCheckboxChecked();
	specialHoursPage.verifyEveryStorePrechecked();
	specialHoursPage.verifyDeselectingAStore();
    specialHoursPage.verifySelectingAllStores();
	specialHoursPage.verifyNoPaginationForLessThan10Stores();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyReasonAndTimeSelectionPage();
	specialHoursPage.setReason(tMNGData.getReason());
	specialHoursPage.setDescription(tMNGData.getDescription());
	specialHoursPage.setStartDate(tMNGData.getStartDate());
	specialHoursPage.setEndDate(tMNGData.getEndDate());
	specialHoursPage.clickonClosedAllRadio();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyConfirmationPage();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifySpecialHoursSaved();
	specialHoursPage.verifySpecialHoursEventReasons();
	int eventsCountBeforeRemoval = specialHoursPage.numberOfReasonEventsBeforeRemove();
	specialHoursPage.verifyAccordionExpandsUponClick();
	specialHoursPage.verifyAndClickRemoveIcon();
	specialHoursPage.verifyRemoveModal();
	specialHoursPage.verifyRemoveModalCTA();
	specialHoursPage.clickOnCancelCTA();
	specialHoursPage.verifyAndClickRemoveIcon();
	specialHoursPage.clickOnConfirmCTA();
	specialHoursPage.verifyShEventDeleted(eventsCountBeforeRemoval);
	specialHoursPage.verifyAndClickCreateSpecialHoursIcon();
	specialHoursPage.verifySpecialHoursPage();
	}
	
	/***
	 * CDCDWR2-515:SH Internal tool | Enhancements - hint text, spinner, expand all | Test & Support
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT , Group.TMNG })
	public void verifyUnAuthorisedUserAccess(TMNGData tMNGData) {
	Reporter.log("CDCDWR2-515:SH Internal tool | Enhancements - hint text, spinner, expand all | Test & Support" );
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours page | Special hours page is displayed");
	Reporter.log("Step 2: Enter email of an invalid user in text box | Entered email into the Email text box should be success");
	Reporter.log("Step 3: Enter ntpassword in text box | Entered ntpassword into the password text box");
	Reporter.log("Step 4: Click on Login button | Clicked on Login button");
	Reporter.log("Step 5: Verify the error message display | A modal of error message 'Please contact SpecialStoreHoursAdmin@t-mobile.com for access.' should be displayed for invalid user");
	Reporter.log("Step 6: Verify 'Close' CTA display | Close button is displayed in Modal pop up");
	Reporter.log("Step 7: Click on 'Close' CTA ('x') in Modal pop up | Clicked on Close button in Modal pop up");
	Reporter.log("Step 8: Clicked on logged in email to sign out | Selected Logged in email to sign out");
	Reporter.log("Step 9: Verify the display of Special hours login page | Special hours Login page is displayed");
    Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
    specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
	specialHoursPage.verifyErrorMessageInModalPopUp();
	specialHoursPage.verifycloseButtonInModalPopUp();
	specialHoursPage.clickOncloseButtonInModalPopUp();
	specialHoursPage.verifySHLoginPageAfterLogout();
    }
	
	/***
	 * CDCDWR2-118:SL UI | SH Internal Tool | Store Hierarchy selector
	 * @param tMNGData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT , Group.TMNG })
	public void verifyStoreHierarchySelector(TMNGData tMNGData) {
	Reporter.log("CDCDWR2-118:SL UI | SH Internal Tool | Store Hierarchy selector" );
	Reporter.log("================================");
	
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("Step 1: Launch Special hours login page | Special hours login page is displayed");
	Reporter.log("Step 2: Enter email details into the email text box | Entering email details into the email text box should be success");
	Reporter.log("Step 3: Click on next button in login page | Click on next button should be success");
	Reporter.log("Step 4: Enter ntpassword in password text box | Entering ntpassword into the password text box should be success");
	Reporter.log("Step 5: Click on sign in button | Click on sign in button should be success");
	Reporter.log("Step 6: Verify special hours page | Special hours page should be displayed");
	Reporter.log("Step 7: Select store hierarchy radio button and click on continue | 'Please select store area' warning message should be displayed");
	Reporter.log("Step 8: Select 'Central' from area dropdown | Market dropdown should be displayed");
	Reporter.log("Step 9: Select 'Central Markets' from market dropdown | District dropdown should be displayed");
	Reporter.log("Step 10:Select 'Michigan/Indiana' from district dropdown | Territory dropdown should be displayed");
	Reporter.log("Step 11:Select 'West Michigan' from territory dropdown and click on continue | Store selector results should be displayed");
    
	Reporter.log("=========================");
    launchSpecialHoursPage(tMNGData);
	SpecialHoursPage specialHoursPage = new SpecialHoursPage(getDriver());
	specialHoursPage.verifySHLoginPage();
    specialHoursPage.signInWithTMobileEmailAndPassword(tMNGData.getEmail(),tMNGData.getPassword());
    specialHoursPage.verifySpecialHoursPage();
	specialHoursPage.selectStoreHierarchyRadioButton();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyWarningMessageForNonSelection();
	specialHoursPage.selectCentralInAreaDropDown();
	specialHoursPage.selectCentralMarketsInMarketDropDown();
	specialHoursPage.selectMichiganInDistrictDropDown();
	specialHoursPage.clickOnContinueButton();
	specialHoursPage.verifyStoreSelectorResults();
    }
	}
