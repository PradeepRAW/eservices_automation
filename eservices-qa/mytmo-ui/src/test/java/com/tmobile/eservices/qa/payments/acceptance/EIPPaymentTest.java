package com.tmobile.eservices.qa.payments.acceptance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.EIPDetailsPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentForwardPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentReviewCCPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author Srajana
 *
 */
public class EIPPaymentTest extends PaymentCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(EIPPaymentTest.class);

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyUserAbleTOEditEIPPayment(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOEditEIPPayment method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify UserAble TO EditEIPPayment");

		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing Link | Bill Summary page should be displayed");
		Reporter.log("5. Click view link under EIP section | EIP Details page should be displayed.");
		Reporter.log("6. Click on make payment | One time payment page should be displayed.");
		Reporter.log(
				"7. Click on Pay Other Amount Checkbox, Fill Card details and click next| Review Payment Page should be displayed.");
		Reporter.log(
				"8. Click on edit payment link | User should be redirected One time payment page and Edit Credit card information.");
		Reporter.log("9. Re Enter CVV code and click next | Review Payment Page should be displayed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		/*BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.clickViewDetails();

		EIPDetailsPage eipDetailsPage = new EIPDetailsPage(getDriver());*/
		EIPDetailsPage eipDetailsPage = navigateToEIPDetailsPage(myTmoData);
		eipDetailsPage.verifyPageLoaded();
		eipDetailsPage.clickMakePayment();
		EIPPaymentForwardPage eipPaymentPage = new EIPPaymentForwardPage(getDriver());
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.clickPayEIPFullCheckBox();

		eipPaymentPage.clickNewRadioButton();
		eipPaymentPage.fillPaymentInfo(myTmoData.getPayment());
		eipPaymentPage.clickNextButton();
		EIPPaymentReviewCCPage eIPPaymentReviewCCPage = new EIPPaymentReviewCCPage(getDriver());
		eIPPaymentReviewCCPage.verifyPageLoaded();
		eIPPaymentReviewCCPage.clickEditPaymentLink();
		eipDetailsPage.verifyEIPDevicePaymentPage();
		Reporter.log("User is redirected EIP payment page and able to Edit Credit card information.");
		eipPaymentPage.setCVV(myTmoData.getPayment().getCvv());
		eipPaymentPage.clickNextButton();
		eIPPaymentReviewCCPage.verifyPageLoaded();
	}

	/**
	 * TC001_E-Bill_make Eip payments_Close EIP_Full payment_Credit card
	 * 
	 * @param data
	 * @param myTmoData
	 * @param payment
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyUserAbleTOMakeEIPFullPayment(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakeEIPFullPayment method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make EIP Full Payment");

		Reporter.log("Expected Test Steps");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing Link | Bill Summary page should be displayed");
		Reporter.log("5. Click view link under EIP section | EIP Details page should be displayed.");
		Reporter.log("6. Click on make payment | EIP device payment page should be displayed.");
		Reporter.log(
				"7. Click on Pay EIP Full Checkbox, Fill Card details and click next| Review Payment Page should be displayed.");
		Reporter.log("8. Click on Credit Card Terms and Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");

		Reporter.log("Actual Test Steps");

		//EIPDetailsPage eIPDetailsPage = new EIPDetailsPage(getDriver());
		/*
		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.clickViewDetails();*/
		
		EIPDetailsPage eIPDetailsPage = navigateToEIPDetailsPage(myTmoData);
		eIPDetailsPage.verifyPageLoaded();
		eIPDetailsPage.clickMakePayment();
		EIPPaymentForwardPage eipPaymentPage = new EIPPaymentForwardPage(getDriver());
		eipPaymentPage.verifyPageLoaded();
		eipPaymentPage.clickPayEIPFullCheckBox();
		eipPaymentPage.clickNewRadioButton();
		eipPaymentPage.fillPaymentInfo(myTmoData.getPayment());
		eipPaymentPage.clickNextButton();
		EIPPaymentReviewCCPage eIPPaymentReviewCCPage = new EIPPaymentReviewCCPage(getDriver());
		eIPPaymentReviewCCPage.verifyPageLoaded();
		eIPPaymentReviewCCPage.clickCCTermsAndConditions();
	}
}