package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAlonePDPPage;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAlonePLPPage;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAloneReviewCartPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.shop.UNOPDPPage;
import com.tmobile.eservices.qa.pages.shop.UNOPLPPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

/**
 * @author rnallamilli
 *
 */
public class AccessoriesStandAloneReviewCartPageTest extends ShopCommonLib {

	/**
	 * US295813:Accessories Stand Alone - Check Out Button on Cart (for Restricted
	 * Users) US291601:Accessories Stand Alone - PDP Add to Cart CTA behavior
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testCartCheckOutBtnForRestrictedUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US295813:Accessories Stand Alone - Check Out Button on Cart (for Restricted Users)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Click Accessory device | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Cart page should be displayed");
		Reporter.log("8. Verify check out button | Checkout button should be disabled");
		Reporter.log("9. Click check out button message| Text message for disabled cta should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyPageLoaded();
		shopPage.verifyPageUrl();
		shopPage.clickQuickLinks("Accessories");
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		accessoriesStandAlonePLPPage.clickAccesoriesDeviceByName(myTmoData.getAccessoryName());
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		accessoriesStandAlonePDPPage.clickAddToCartButton();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.verifyCheckOutButtonState();
		accessoriesStandAloneReviewCartPage.VerifyCheckOutButtonMessage();
	}

	/**
	 * US306711: MyTMO - Additional Terms - Standalone Accessories Flow - Cart:
	 * Product Line Pricing Display US314374: MyTMO - Additional Terms - Standalone
	 * Accessories Flow - Cart: Total Due Today and Total Due Monthly
	 * 
	 * US389937 : [Continued] MyTMO - Additional Terms - Standalone Accessories Flow
	 * - Cart: Product Line Pricing Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testStandaloneAccessoriesProductLinePricingDisplay(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"TestCase US306711: MyTMO - Additional Terms - Standalone Accessories Flow - Cart: Product Line Pricing Display");
		Reporter.log(
				"TestCase US314374: MyTMO - Additional Terms - Standalone Accessories Flow - Cart: Total Due Today and Total Due Monthly");
		Reporter.log(
				"TestCase US389940: MyTMO - Additional Terms - Standalone Accessories Flow - Cart: Total Due Today and Total Due Monthly");
		Reporter.log(
				"TestCase US389937: MyTMO - Additional Terms - Standalone Accessories Flow -Cart: Product Line Pricing Display");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Click Accessory device | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review Cart page should be displayed");
		Reporter.log(
				"8. Add one more accessory to cart | Few accessories should be in Review Cart page should be displayed");
		Reporter.log("9. Click Monthly payments Radio button | Table should be displayed");
		Reporter.log("10. Verify Details label | Details label should be displayed");
		Reporter.log(
				"11. Verify Upfront payment for each accessory | Accessory Upfront payment should be displayed for each accessory");
		Reporter.log("12. Verify Due Today label | Due Today label should be displayed");
		Reporter.log(
				"13. Verify Due Today amount for each accessory | Due Today amount should be displayed for each accessory");
		Reporter.log("14. Verify Due Today Total amount | Due Today Total amount should be displayed");
		Reporter.log("15. Verify Due Monthly label | Due Monthly label should be displayed");
		Reporter.log(
				"16. Verify Due Monthly amount for each accessory | Due Monthly amount should be displayed for each accessory");
		Reporter.log("17. Verify Due Monthly Total amount| Due Monthly Total amount should be displayed");
		Reporter.log("18. Click One payment Radio button | Accessory FRP should be displayed for each accessory");
		Reporter.log("19. Verify Due Today Total amount| Due Today Total amount should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		Integer NumberOfAccessories = 2;
		String secondAccessory = "Beats Solo3 Wireless On-Ear Headphones - Matte Silver";

		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = navigateToStandAloneReviewCartPage(
				myTmoData);

		accessoriesStandAloneReviewCartPage.clickContinueToShoppingButton();
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.clickAddtoCartBtnByAccessoryName(secondAccessory);
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();

		accessoriesStandAloneReviewCartPage.clickMonthlyRadioButton();
		accessoriesStandAloneReviewCartPage.verifyTable();
		accessoriesStandAloneReviewCartPage.verifyDetailsLabel();
		accessoriesStandAloneReviewCartPage.verifyDueTodayLabel();
		accessoriesStandAloneReviewCartPage.verifyDueMonthlyLabel();
		accessoriesStandAloneReviewCartPage.verifyEipUpfrontPaymentPriceAtAccessoryCartPage();
		accessoriesStandAloneReviewCartPage.verifyEipDueMonthlyForAccesories();
		accessoriesStandAloneReviewCartPage.verifyEipDueTodayForAccesories();

		Double totalDueToday = accessoriesStandAloneReviewCartPage.getDueTodayTotalIntegerCartPage();
		Double accessoriesDueTodaySum = accessoriesStandAloneReviewCartPage
				.getSumOfAccessoriesDueTodayInteger(NumberOfAccessories);
		Double shippingAmt = accessoriesStandAloneReviewCartPage.getshippingIntegerCartPage();
		Double dueTodaySum = accessoriesDueTodaySum + shippingAmt;
		accessoriesStandAloneReviewCartPage.compareAccessoryDueTodayAmounts(totalDueToday, dueTodaySum);

		Double totalDueMonthly = accessoriesStandAloneReviewCartPage.getDueMonthlyTotalIntegerCartPage();
		Double accessoryDueMonthly = accessoriesStandAloneReviewCartPage
				.getAccessoryDueMonthlyInteger(NumberOfAccessories);
		accessoriesStandAloneReviewCartPage.compareAccessoryDueMonthlyAmounts(accessoryDueMonthly, totalDueMonthly);

		accessoriesStandAloneReviewCartPage.clickOnePaymentRadioButton();
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		Double totalDueTodayOnePayment = accessoriesStandAloneReviewCartPage.getDueTodayTotalIntegerCartPage();
		Double accessoryDueTodayOnePayment = accessoriesStandAloneReviewCartPage
				.getSumOfAccessoriesDueTodayInteger(NumberOfAccessories);
		Double shippingAmtOnePayment = accessoriesStandAloneReviewCartPage.getshippingIntegerCartPage();
		Double dueTodaySumOnePayment = accessoryDueTodayOnePayment + shippingAmtOnePayment;
		accessoriesStandAloneReviewCartPage.compareAccessoryDueTodayAmounts(totalDueTodayOnePayment,
				dueTodaySumOnePayment);
	}

	/**
	 * US321295: MyTMO - MyTMO - Additional Terms - Standalone Accessories Flow -
	 * Cart: Display + taxes after Total Due Today dollar amount
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testStandaloneAccessoriesDisplayAddTaxesText(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"TestCase US321295: MyTMO - MyTMO - Additional Terms - Standalone Accessories Flow - Cart: Display + taxes after Total Due Today dollar amount");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Click Accessory device (price > $69.99) | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review Cart page should be displayed");
		Reporter.log("8. Click Monthly payments Radio button | Accessory Upfront amount should be displayed");
		Reporter.log(
				"9. Verify Due Today Total amount with dollar symbol | Due Today Total amount should be displayed with dollar symbol");
		Reporter.log(
				"10. Verify '+ taxes' text in Due Today Total cell| Due today Total cell should contain '+ taxes' text ");
		Reporter.log("11. Click One payment Radio button | Accessory FRP amount should be displayed");
		Reporter.log("12. Verify DUE TODAY label | DUE TODAY label should be displayed");
		Reporter.log(
				"13. Verify Due Today Total amount with dollar symbol | Due Today Total amount should be displayed with dollar symbol");
		Reporter.log(
				"14. Verify '+ taxes' text in Due Today Total cell| Due today Total cell should contain '+ taxes' text ");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = navigateToStandAloneAccessoriesPLP(myTmoData);
		accessoriesStandAlonePLPPage.verifyEIPEligibilityAndAddAccessory();

		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.clickMonthlyRadioButton();
		accessoriesStandAloneReviewCartPage.verifyEipUpfrontPaymentPriceAtAccessoryCartPage();
		accessoriesStandAloneReviewCartPage.verifyEipDueTodayTotalPriceForAccesories();
		accessoriesStandAloneReviewCartPage.verifyTaxesTerm();

		accessoriesStandAloneReviewCartPage.clickOnePaymentRadioButton();
		accessoriesStandAloneReviewCartPage.verifyDueTodayLabel();
		accessoriesStandAloneReviewCartPage.verifyEipDueTodayTotalPriceForAccesories();
		accessoriesStandAloneReviewCartPage.verifyTaxesTerm();
	}

	/**
	 * US321295: MyTMO - MyTMO - Additional Terms - Standalone Accessories Flow -
	 * Cart: Display + taxes after Total Due Today dollar amount
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testStandaloneAccessoriesDisplayAddTaxesTextPRCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"TestCase US321295: MyTMO - MyTMO - Additional Terms - Standalone Accessories Flow - Cart: Display + taxes after Total Due Today dollar amount");
		Reporter.log("Data Condition | Puerto Rico PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Click Accessory device (price > $69.99) | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review Cart page should be displayed");
		Reporter.log("8. Click Monthly payments Radio button | Accessory Upfront amount should be displayed");
		Reporter.log(
				"9. Verify Due Today Total amount with dollar symbol | Due Today Total amount should be displayed with dollar symbol");
		Reporter.log(
				"10. Verify '+ taxes' text in Due Today Total cell| Due today Total cell should contain '+ taxes' text ");
		Reporter.log("11. Click One payment Radio button | Accessory FRP amount should be displayed");
		Reporter.log("12. Verify DUE TODAY label | DUE TODAY label should be displayed");
		Reporter.log(
				"13. Verify Due Today Total amount with dollar symbol | Due Today Total amount should be displayed with dollar symbol");
		Reporter.log(
				"14. Verify '+ taxes' text in Due Today Total cell| Due today Total cell should contain '+ taxes' text ");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = navigateToStandAloneAccessoriesPLP(myTmoData);
		accessoriesStandAlonePLPPage.verifyEIPEligibilityAndAddAccessory();

		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.clickMonthlyRadioButton();
		accessoriesStandAloneReviewCartPage.verifyEipUpfrontPaymentPriceAtAccessoryCartPage();
		accessoriesStandAloneReviewCartPage.verifyEipDueTodayTotalPriceForAccesories();
		accessoriesStandAloneReviewCartPage.verifyTaxesTerm();

		accessoriesStandAloneReviewCartPage.clickOnePaymentRadioButton();
		accessoriesStandAloneReviewCartPage.verifyDueTodayLabel();
		accessoriesStandAloneReviewCartPage.verifyEipDueTodayTotalPriceForAccesories();
		accessoriesStandAloneReviewCartPage.verifyTaxesTerm();
	}

	/**
	 * US321670: MyTMO - Additional Terms - Standalone Accessories Flow - Cart:
	 * Update EIP Legal Text
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testStandaloneAccessoriesPaymentOptionLegalText(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"TestCase US321670: MyTMO - Additional Terms - Standalone Accessories Flow - Cart: Update EIP Legal Text");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Click Accessory device | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review Cart page should be displayed");
		Reporter.log("8. Click Monthly payments Radio button | Accessory Upfront amount should be displayed");
		Reporter.log("9. Verify Due Monthly label | Due Monthly label should be displayed");
		Reporter.log("10. Verify Legal text | Legal text should be displayed");
		Reporter.log("11. Click One payment Radio button | Accessory FRP amount should be displayed");
		Reporter.log("12. Verify DUE TODAY label | DUE TODAY label should be displayed");
		Reporter.log("13. Verify Legal text | Legal text should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = navigateToStandAloneReviewCartPage(
				myTmoData);
		accessoriesStandAloneReviewCartPage.clickMonthlyRadioButton();
		accessoriesStandAloneReviewCartPage.verifyDueMonthlyLabel();
		accessoriesStandAloneReviewCartPage.verifyLegalText();
		accessoriesStandAloneReviewCartPage.clickOnePaymentRadioButton();
		accessoriesStandAloneReviewCartPage.verifyDueTodayLabel();
		accessoriesStandAloneReviewCartPage.verifyDueMonthlyLabelNotDisplay();
		accessoriesStandAloneReviewCartPage.verifyLegalTextNotDisplay();
	}

	/**
	 * US321670: MyTMO - Additional Terms - Standalone Accessories Flow - Cart:
	 * Update EIP Legal Text
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testStandaloneAccessoriesPaymentOptionLegalTextForPRCustomer(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"TestCase US321670: MyTMO - Additional Terms - Standalone Accessories Flow - Cart: Update EIP Legal Text");
		Reporter.log("Data Condition | PR Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Click Accessory device | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review Cart page should be displayed");
		Reporter.log("8. Click Monthly payments Radio button | Accessory Upfront amount should be displayed");
		Reporter.log("9. Verify Due Monthly label | Due Monthly label should be displayed");
		Reporter.log("10. Verify Legal text | Legal text should be displayed");
		Reporter.log("11. Click One payment Radio button | Accessory FRP amount should be displayed");
		Reporter.log("12. Verify DUE TODAY label | DEUTODAY label should be displayed");
		Reporter.log("13. Verify Legal text | Legal text should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = navigateToStandAloneReviewCartPage(
				myTmoData);
		accessoriesStandAloneReviewCartPage.clickMonthlyRadioButton();
		accessoriesStandAloneReviewCartPage.verifyDueMonthlyLabel();
		accessoriesStandAloneReviewCartPage.verifyLegalText();
		accessoriesStandAloneReviewCartPage.clickOnePaymentRadioButton();
		accessoriesStandAloneReviewCartPage.verifyDueTodayLabel();
		accessoriesStandAloneReviewCartPage.verifyDueMonthlyLabelNotDisplay();
		accessoriesStandAloneReviewCartPage.verifyLegalTextNotDisplay();
	}

	/**
	 * US315202: TEST ONLY: MyTMO - Additional Terms - Standalone Accessories -
	 * Checkout: Pricing Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testStandaloneAccessoriesCheckoutPricingDisplayedForPRCustomer(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"TestCase US315202: TEST ONLY: MyTMO - Additional Terms - Standalone Accessories - Checkout: Pricing Display");
		Reporter.log("Data Condition | PR Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Click Accessory device | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review Cart page should be displayed");
		Reporter.log("8. Click Continue Shopping button | Accessory plp page should be displayed");
		Reporter.log("9. Add one more Accessory device | Review Cart page should be displayed");
		Reporter.log("10. Click Monthly payments Radio button | Verify Due Monthly header should be displayed");
		Reporter.log(
				"11. Verify Total Due Monthly Amount | Total Due Monthly Amount should be displayed. It should be summary of all items added to the cart");
		Reporter.log(
				"12. Verify Total Due Today Amount | Total Due Today Amount should be displayedIt should be summary of all accessories added to the cart");
		Reporter.log("13. Click One Payment Radio button | Verify Due Today header should be displayed");
		Reporter.log(
				"14. Verify Total Due Today Amount | Total Due Today Amount should be displayed. It should be summary of all items added to the cart");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		Integer NumberOfAccessories = 2;
		String secondAccessory = "Beats Solo3 Wireless On-Ear Headphones - Matte Silver";

		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = navigateToStandAloneReviewCartPage(
				myTmoData);

		accessoriesStandAloneReviewCartPage.clickContinueToShoppingButton();
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.clickAddtoCartBtnByAccessoryName(secondAccessory);
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();

		accessoriesStandAloneReviewCartPage.clickMonthlyRadioButton();
		accessoriesStandAloneReviewCartPage.verifyDueMonthlyLabel();
		accessoriesStandAloneReviewCartPage.verifyEipDueMonthlyTotalPriceForAccesories();
		Double totalDueMonthly = accessoriesStandAloneReviewCartPage.getDueMonthlyTotalIntegerCartPage();
		Double accessoryDueMonthly = accessoriesStandAloneReviewCartPage
				.getAccessoryDueMonthlyInteger(NumberOfAccessories);
		accessoriesStandAloneReviewCartPage.compareAccessoryDueMonthlyAmounts(accessoryDueMonthly, totalDueMonthly);

		accessoriesStandAloneReviewCartPage.verifyEipDueTodayTotalPriceForAccesories();
		Double totalDueToday = accessoriesStandAloneReviewCartPage.getDueTodayTotalIntegerCartPage();
		Double accessoriesDueTodaySum = accessoriesStandAloneReviewCartPage
				.getSumOfAccessoriesDueTodayInteger(NumberOfAccessories);
		Double shippingAmt = accessoriesStandAloneReviewCartPage.getshippingIntegerCartPage();
		Double dueTodaySum = accessoriesDueTodaySum + shippingAmt;
		accessoriesStandAloneReviewCartPage.compareAccessoryDueTodayAmounts(totalDueToday, dueTodaySum);

		accessoriesStandAloneReviewCartPage.clickOnePaymentRadioButton();
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.verifyDueTodayLabel();
		accessoriesStandAloneReviewCartPage.verifyEipDueTodayForAccesories();
		Double totalDueTodayOnePayment = accessoriesStandAloneReviewCartPage.getDueTodayTotalIntegerCartPage();
		Double accessoryDueTodayOnePayment = accessoriesStandAloneReviewCartPage
				.getSumOfAccessoriesDueTodayInteger(NumberOfAccessories);
		Double shippingAmtOnePayment = accessoriesStandAloneReviewCartPage.getshippingIntegerCartPage();
		Double dueTodaySumOnePayment = accessoryDueTodayOnePayment + shippingAmtOnePayment;
		accessoriesStandAloneReviewCartPage.compareAccessoryDueTodayAmounts(totalDueTodayOnePayment,
				dueTodaySumOnePayment);
	}

	/**
	 * Ensure user can place an accessory order. 
	 * C410983
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyUserCanPlaceAccessoryOrder(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserCanPlaceAccessoryOrder method called in ShopTest");
		Reporter.log("Test Case : Ensure user can place an accessory order.");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop tab | Shop Page should be displayed");
		Reporter.log("5. Click shop accessories link | Accessories Page should be displayed");
		Reporter.log("6. Selkect device and click OrderNow button | Device should be added into cart");
		Reporter.log("7. Click CartIcon | Review Cart Page Should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.verifyPageUrl();
		shopPage.clickQuickLinks("Accessories");

		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		accessoriesStandAlonePLPPage.clickAccesoriesDeviceByName(myTmoData.getAccessoryName());
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		accessoriesStandAlonePDPPage.clickAddToCartButton();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
	}

	/**
	 * US324642:MyTMO - Additional Terms - Standalone Accessories Flow: Display
	 * Total Financed Amount in Cart
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testTotalFinancedAmountIsDisplayedInCartPageWithAccessoryesEIPOption(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test Case US324642:MyTMO - Additional Terms - Standalone Accessories Flow: Display Total Financed Amount in Cart");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button  | Accessory plp page should be displayed");
		Reporter.log("6. Click Accessory device which eligible for EIP | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review Cart page should be displayed");
		Reporter.log(
				"8. Verify that Total Financed Amount from Cart page is matching TFA from PDP | Amounts should be equal");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = navigateToStandAloneAccessoriesPDP(myTmoData);
		Double fRPFromPDP = accessoriesStandAlonePDPPage.getAccessoryFRPPRiceInPDP();

		accessoriesStandAlonePDPPage.clickAddToCartButton();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.verifyTotalFinancedAmountValueisDisplayed();
		Double fRPFromCart = accessoriesStandAloneReviewCartPage.getAccessoryTFAInCart();
		accessoriesStandAloneReviewCartPage.compareAccessoryTFAmounts(fRPFromPDP, fRPFromCart);
		accessoriesStandAloneReviewCartPage.clickOnePaymentRadioButton();
		accessoriesStandAloneReviewCartPage.verifyTotalFinancedAmountValueisNotDisplayed();
		accessoriesStandAloneReviewCartPage.clickMonthlyRadioButton();
		accessoriesStandAloneReviewCartPage.verifyTotalFinancedAmountValueisDisplayed();
	}

	/**
	 * US324642:MyTMO - Additional Terms - Standalone Accessories Flow: Display
	 * Total Financed Amount in Cart
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testTotalFinancedAmountIsNotDisplayedInCartPageWithAccessoryesFRPOption(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test Case US324642:MyTMO - Additional Terms - Standalone Accessories Flow: Display Total Financed Amount in Cart");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button  | Accessory plp page should be displayed");
		Reporter.log(
				"6. Click Accessory device which is not eligible for EIP | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review Cart page should be displayed");
		Reporter.log(
				"8. Verify that Total Financed Amount is not displayed on Cart page | Amounts should not be present");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = navigateToStandAloneAccessoriesPDP(myTmoData);
		accessoriesStandAlonePDPPage.clickAddToCartButton();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.verifyTotalFinancedAmountValueisNotDisplayed();
	}

	/**
	 * US474616:Take Authenticated Users To MyTMO Stand-Alone Accessories Cart (from
	 * TMNG Cart)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testTMNGAuthenticatedUsersToMyTMOStandAloneAccessoriesReviewCartPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test Case: US474616:Take Authenticated Users To MyTMO Stand-Alone Accessories Cart (from TMNG Cart)");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Launch TMO  | Application Should be Launched");
		Reporter.log("5. Click on Phones Link |Cell phones page should be displayed");
		Reporter.log("6. click on Accessories Link | Accessories PLP page should be displayed");
		Reporter.log("7. Select any Accessory | PDP page should be displayed");
		Reporter.log("8. Click on Add to Cart button | MyTmo Cart page should be displayed with right accessory on it");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);

		getDriver().get("https://dmo.digital.t-mobile.com/cell-phones");
		// HomePage homePage = new HomePage(getDriver());
		// homePage.verifyPageLoaded();
		// homePage.clickPhonesLink();
		PlpPage phonesPage = new PlpPage(getDriver());
		phonesPage.verifyPhonesPlpPageLoaded();
		phonesPage.clickOnAccessoriesMenuLink();
		PlpPage accessoriesPage = new PlpPage(getDriver());
		accessoriesPage.verifyAccessoriesPlpPageLoaded();
		accessoriesPage.clickDeviceWithAvailability(myTmoData.getAccessoryName());
		PdpPage accessoryDetailsPage = new PdpPage(getDriver());
		accessoryDetailsPage.verifyAccessoriesPdpPageLoaded();
		accessoryDetailsPage.clickOnAddToCartBtn();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.verifyAccessoryIsPresentOnCartPage(myTmoData);

	}

	/**
	 * US487940: MyTMO Standalone Accessories Flow: Cart: Disable Checkout CTA
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testCheckOutButtonIsDisabled(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US487940: MyTMO Standalone Accessories Flow: Cart: Disable Checkout CTA");
		Reporter.log("Data Condition | PAH User with Past-due customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button  | Accessory plp page should be displayed");
		Reporter.log("6. Select accessory device | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review Cart page should be displayed");
		Reporter.log("8. Click on 'Past due modal pop up' 'X' icon | 'Past due modal pop up' should be closed");
		Reporter.log("9. Verify Past due sticky message | Past due sticky message should be displayed");
		Reporter.log("10. Verify Past due price in sticky message | Past due sticky message should contain price");
		Reporter.log("11. Verify Checkout button is disabled | Checkout button should be disabled");
		Reporter.log("12. Verify Continue Shopping button is enabled | Continue Shopping button should be enabled");
		Reporter.log("13. Click on Past due Sticky Banner | OTP page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = navigateToStandAloneAccessoriesPDP(myTmoData);
		accessoriesStandAlonePDPPage.clickAddToCartButton();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.clickOnPopUpCloseIcon();
		accessoriesStandAloneReviewCartPage.verifyPastDueMessageIsDisplayed();
		accessoriesStandAloneReviewCartPage.verifyPastDuePriceIsDisplayed();
		accessoriesStandAloneReviewCartPage.verifyCheckOutButtonIsDisabled();
		accessoriesStandAloneReviewCartPage.verifyContinueToShoppingEnabled();
		accessoriesStandAloneReviewCartPage.clickOnStickyBanner();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
	}

	/**
	 * US514814: MyTMO Standalone Accessories Flow: Cart: Add a Message and Pop a
	 * Modal
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testPastDueMessagePopUpModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US514814: MyTMO Standalone Accessories Flow: Cart: Add a Message and Pop a Modal");
		Reporter.log("Data Condition | PAH User with Past-due customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button  | Accessory plp page should be displayed");
		Reporter.log("6. Select accessory device | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review Cart page should be displayed");
		Reporter.log("8. Verify Past due sticky message | Past due sticky message should be displayed");
		Reporter.log("9. Verify Past due modal | Past due modal pop up should be displayed ");
		Reporter.log("10. Verify Past due authorable message | Past due authorable message should be displayed");
		Reporter.log("11. Verify 'Make a payment' button | 'Make a payment' should be displayed");
		Reporter.log("12. Click on 'Past due modal pop up' 'X' icon | 'Past due modal pop up' should be closed");
		Reporter.log("13. Refresh page | 'Past due modal pop up' should be displayed");
		Reporter.log("14. Click on 'Make a payment' button | OTP page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = navigateToStandAloneAccessoriesPDP(myTmoData);
		accessoriesStandAlonePDPPage.clickAddToCartButton();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.verifyPopUpModalHeaderIsDisplayed();
		accessoriesStandAloneReviewCartPage.verifyMakePaymentCTAIsDisplayed();
		accessoriesStandAloneReviewCartPage.clickOnPopUpCloseIcon();
		accessoriesStandAloneReviewCartPage.verifyPastDueMessageIsDisplayed();
		accessoriesStandAloneReviewCartPage.refreshPage();
		accessoriesStandAloneReviewCartPage.verifyPopUpModalHeaderIsDisplayed();
		accessoriesStandAloneReviewCartPage.clickMakePaymentCTA();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();

	}

	/**
	 * US524996: DEFECT: [MyTMO|Shop|Buy] [ANALYSIS]TMNG Accessories: Cookied but
	 * not logged in customer goes to MyTMO accessory PLP rather than cart
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testTmoAccessoryNameInMyTmoReviewCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :US524996: DEFECT: [MyTMO|Shop|Buy] [ANALYSIS]TMNG Accessories: Cookied but not logged in customer goes to MyTMO accessory PLP rather than cart");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Logout for My-Tmo | My-Tmo login page should be displayed");
		Reporter.log("5. Enter www.t-mobile.com without clear cache  | Application Should be Launched");
		Reporter.log("6. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("7. Click on Accessories | Accessories PLP should be displayed");
		Reporter.log(
				"8. Select Price as 'Price high to low' | Accessory prices should be displayed as High to low order");
		Reporter.log("9. Select Higher price accessory | Accessory PDP Page should be displayed");
		Reporter.log("10. Save accessory name in V1 | Accessory name should be saved in V1");
		Reporter.log("11. Click Add to cart button | CartPage Page should be displayed");
		Reporter.log("12. Navigate Back | Accessory PDP Page should be displayed");
		Reporter.log("13. Click on MyTmo link  | User should navigate MyTmo Review cartPage");
		Reporter.log("14. Save accessory name in V2 | Accessory name should be saved in V2");
		Reporter.log("15. Compare accessory device names i.e V1 and V2 | Accessory device name should be equal");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US505785:DEFECT: [Shop|Accessories] Accessory added to cart from ‘related
	 * accessories‘ section missing image
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAddAccessoryFromRelatedAccessoriesSection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case:US505785:DEFECT: [Shop|Accessories] Accessory added to cart from ‘related accessories‘ section missing image");
		Reporter.log("Data Condition | PAH User ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Select Accessory device | Accessory pdp page should be displayed");
		Reporter.log(
				"7. Select any accessory from the “Related accessories” section | Accessory pdp page should be displayed");
		Reporter.log("8. Get accessory name and Save in V1 | Accessory name should be saved in V1");
		Reporter.log("9. Click Add to cart button | Review cart page should be displayed");
		Reporter.log("10. Verify Accessory Image | Accessory Image should be displayed");
		Reporter.log(
				"11. Get accessory name and Save in V2 from Review cart page | Accessory name should be saved in V2 from review cart page");
		Reporter.log("12. Compare accessory names (V1 and V2) | Accessory name should be equal");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = navigateToStandAloneAccessoriesPDP(myTmoData);
		accessoriesStandAlonePDPPage.verifyRelatedDevicesSectionOnAccessoriesStandalonePDPPage();
		String relatedAccessory = accessoriesStandAlonePDPPage.selectRelatedAccessoryOnAccessoriesStandalonePage();
		accessoriesStandAlonePDPPage.clickAddToCartButton();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyReviewCartText();
		accessoriesStandAloneReviewCartPage.verifyAddedAccessoryIsAvailableOnReviewCartPage(relatedAccessory);
	}

	/**
	 * US540439:myTMO > Accessories > Create Cart and consume cookie
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCreateCartAndConsumeCookie(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US540439:myTMO > Accessories > Create Cart and consume cookie");
		Reporter.log("Data Condition | PAH User ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessorys UNO PLP page should be displayed");
		Reporter.log("6. Select Accessory device | Accessorys UNO PDP page should be displayed");
		Reporter.log("7. Verify Product family name and Saved in V1 | Product family name should be saved in V1");
		Reporter.log("8. Verify Price options for EIP and FRP | Device price FRP and EIP options should be displayed");
		Reporter.log("9. Click on Add to Cart button | Accessory Cart page should be displayed");
		Reporter.log("10. Verify Product family name | Product family name should be same as V1");
		Reporter.log("11. Verify Price options for EIP | Device price EIP options should be same as on PDP");
		Reporter.log("12. Click on 'One payment' radio button | FRP should be displayed");
		Reporter.log("13. Verify FRP | Device price FRP should be same as on PDP");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US540437:myTMO > Accessories > Filtered/non filtered UNO PLP/PDP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCustomerNavigatedToUNOplpPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US540437:myTMO > Accessories > Filtered/non filtered UNO PLP/PDP");
		Reporter.log("Data Condition | PAH User ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Accessory link | Accessories UNO PLP page should be displayed");
		Reporter.log("5. Select an accessory | Accessories UNO PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("7. Click on Remove Accessory device | Accessories UNO PLP page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US540437:myTMO > Accessories > Filtered/non filtered UNO PLP/PDP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCustomerNavigatedToUNOplpPageFromEmptyCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US540437:myTMO > Accessories > Filtered/non filtered UNO PLP/PDP");
		Reporter.log("Data Condition | PAH User ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on Cart Icon | Empty CartPage should be displayed");
		Reporter.log("6. Click on 'Shop Accessories' button | Accessories UNO PLP page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		// steps needs to be clarified, it is possible that this scenario for TMNG
	}

	/**
	 * US531453: Block PR Customers in MyTMO Standalone Accessory Cart Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testStickyBannerForPRcustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US531453: Block PR Customers in MyTMO Standalone Accessory Cart Page");
		Reporter.log("Data Condition | PR Customers ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button  | Accessory plp page should be displayed");
		Reporter.log("6. Select accessory device | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review Cart page should be displayed");
		Reporter.log("8. Verify 'Checkout CTA' is disabled | 'Checkout CTA' should be disabled");
		Reporter.log(
				"9. Verify sticky banner 'Please visit a retail location for a Puerto Rico account.' | Sticky banner 'Please visit a retail location for a Puerto Rico account.' should be displayed");
		Reporter.log(
				"10. Click sticky banner 'Please visit a retail location for a Puerto Rico account.' | Store locator page should be opened in separate window");
		Reporter.log("11. Verify Sticky banner text | Sticky banner text should be authorable");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = navigateToStandAloneReviewCartPage(
				myTmoData);
		accessoriesStandAloneReviewCartPage.verifyCheckOutButtonIsDisabled();
		accessoriesStandAloneReviewCartPage.verifyPuretoRicoStickyBanner();
		accessoriesStandAloneReviewCartPage.clickPuretoRicoStickyBanner();
		accessoriesStandAloneReviewCartPage.switchToWindow();
		verifyCurrentPageURL("StoreLocatorPage", "store-locator");

	}

	/**
	 * US554905
	 * 
	 * Test Only > Uno MyTMO Handoff for TMNG release
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCookiedUnAuthenticatedUserTMOtoMyTMOUNOAccessories(ControlTestData data, TMNGData tMNGData,
			MyTmoData myTmoData) {
		Reporter.log("Test Case#US554905 -Test Only  >  Uno MyTMO Handoff for TMNG release");
		Reporter.log("Data Condition | AAL Eligible User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Navigate to t-mobile link| Tmo url Should be redirected");
		Reporter.log("2. Click on Accessories | Accessories PLP page should be displayed");
		Reporter.log("3. Select a Accessories| Accessories PDP page should be displayed");
		Reporter.log("6. Click on 'Add To Cart' | User should be navigated to myTmo Login Page");
		Reporter.log("5. Enter Login Informaion | UNO PDP Should be displayed");
		Reporter.log("8. Click on Add to Cart | CartPage should be displayed");
		Reporter.log("9. Click Check Out | Checkout page should be displayed");


		launchTmngStaging(myTmoData);
		PlpPage phonesPlpPage = new PlpPage(getDriver());
		phonesPlpPage.verifyPhonesPlpPageLoaded();
		phonesPlpPage.clickOnAccessoriesMenuLink();
		phonesPlpPage.verifyAccessoriesPlpPageLoaded();
		phonesPlpPage.clickDeviceWithAvailability(myTmoData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectLogIn();
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(), myTmoData.getLoginPassword());
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.clickCheckOutButton();
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneCheckOutPage();


	}

	/**
	 * CDCSM-315 - UI - Accessory deeplinking to Mytmo Standalone accessory cart page
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testAccessoryDeeplinkingFromUNOTMOtoMyTMOAccessoriesPDP(ControlTestData data, TMNGData tMNGData,
																	   MyTmoData myTmoData) {
		Reporter.log("Test Case: CDCSM - 315 - UI - Accessory deeplinking to Mytmo Standalone accessory cart page");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Navigate to t-mobile link| Tmo url Should be redirected");
		Reporter.log("2. Click on Accessories | Accessories PLP page should be displayed");
		Reporter.log("3. Select a Accessories| Accessories PDP page should be displayed");
		Reporter.log("6. Click on 'Add To Cart' | User should be navigated to myTmo Login Page");
		Reporter.log("5. Enter Login Informaion | UNO PDP Should be displayed");
		Reporter.log("8. Click on Add to Cart | CartPage should be displayed");
		Reporter.log("9. Navigate back using browser navigation | User should be redirected to UNO PDP");

		launchTmngStaging(myTmoData);
		PlpPage phonesPlpPage = new PlpPage(getDriver());
		phonesPlpPage.verifyPhonesPlpPageLoaded();
		phonesPlpPage.clickOnAccessoriesMenuLink();
		phonesPlpPage.verifyAccessoriesPlpPageLoaded();
		phonesPlpPage.clickDeviceWithAvailability(myTmoData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectLogIn();
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(), myTmoData.getLoginPassword());
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.navigateToPreviousPage();
		phonesPdpPage.verifyAccessoriesPdpPageLoaded();

	}

	/**
	 * CDCSM-315 - UI - Accessory deeplinking to Mytmo Standalone accessory cart page
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testAccessoryDeeplinkingFromUNOTMOtoMyTMOAccessoriesPLP(ControlTestData data, TMNGData tMNGData,
																	   MyTmoData myTmoData) {
		Reporter.log(" CDCSM - 315 - UI - Accessory deeplinking to Mytmo Standalone accessory cart page");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Navigate to t-mobile link| Tmo url Should be redirected");
		Reporter.log("2. Click on Accessories | Accessories PLP page should be displayed");
		Reporter.log("3. Select a Accessories| Accessories PDP page should be displayed");
		Reporter.log("6. Click on 'Add To Cart' | User should be navigated to myTmo Login Page");
		Reporter.log("5. Enter Login Informaion | UNO PDP Should be displayed");
		Reporter.log("8. Click on Add to Cart | CartPage should be displayed");
		Reporter.log("9. Click ContinueShopping cta | Accessory PLP page should be displayed");

		launchTmngStaging(myTmoData);
		PlpPage phonesPlpPage = new PlpPage(getDriver());
		phonesPlpPage.verifyPhonesPlpPageLoaded();
		phonesPlpPage.clickOnAccessoriesMenuLink();
		phonesPlpPage.verifyAccessoriesPlpPageLoaded();
		phonesPlpPage.clickDeviceWithAvailability(myTmoData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectLogIn();
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(), myTmoData.getLoginPassword());
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.clickContinueShoppingButton();
		phonesPlpPage.verifyAccessoriesPlpPageLoaded();

	}

	/**
	 * US554905
	 * 
	 * Test Only > Uno MyTMO Handoff for TMNG release
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCookiedAuthenticatedUserTMOtoMyTMOUNOAccessories(ControlTestData data, TMNGData tMNGData,
			MyTmoData myTmoData) {
		Reporter.log("Test Case#US554905 -Test Only  >  Uno MyTMO Handoff for TMNG release");
		Reporter.log("Data Condition |  AAL Eligible User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch My T-Mobile link| MyTmo url Should be redirected");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Navigate to t-mobile link| Tmo url Should be redirected");
		Reporter.log("1. Navigate to t-mobile link| Tmo url Should be redirected");
		Reporter.log("2. Click on Accessories | Accessories PLP page should be displayed");
		Reporter.log("3. Select a Accessories| Accessories PDP page should be displayed");
		Reporter.log("6. Click on 'Add To Cart' | Accessories Cart Page");
		Reporter.log("9. Click Check Out | Checkout page should be displayed");

		navigateToHomePage(myTmoData);
		launchTmngStaging(myTmoData);
		PlpPage phonesPlpPage = new PlpPage(getDriver());
		phonesPlpPage.verifyPhonesPlpPageLoaded();
		phonesPlpPage.clickOnAccessoriesMenuLink();
		phonesPlpPage.verifyAccessoriesPlpPageLoaded();
		phonesPlpPage.clickDeviceWithAvailability(myTmoData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.clickOnAddToCartBtn();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.clickCheckOutButton();
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneCheckOutPage();

	}


	/**
	 * US602157: Accessories > Standalone accessories > Configure EIP amount
	 * CDCSM-302 - [MyTMO|Shop] standalone acc: only getting EIP pricing on last item added to cart at $49 threshhold
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP })
	public void testEIPPricingOnCartPageForAccessoryOnStandaloneAccessoryCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US602157:Accessories > Standalone accessories > Configure EIP amount");
		Reporter.log("Data Condition | PAH user, And User should have over $49 accessory device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory link | Accessory plp page should be displayed");
		Reporter.log("6. Select an Accessory device which has Over $49 | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review cart page should be displayed");
		Reporter.log("8. Verify Pricing details | Should be displayed the EIP pricing");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = navigateToStandAloneAccessoriesPLP(myTmoData);
		accessoriesStandAlonePLPPage.clickAccesoriesDeviceByName(myTmoData.getAccessoryName());
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		accessoriesStandAlonePDPPage.verifyAccessoryFRPGreaterThan49();
		accessoriesStandAlonePDPPage.clickAddToCartButton();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.verifyDueMonthlyLabel();
		accessoriesStandAloneReviewCartPage.verifyDueTodayLabel();
		accessoriesStandAloneReviewCartPage.verifyEipDueMonthlyForAccesories();
		accessoriesStandAloneReviewCartPage.verifyEipDueTodayForAccesories();
	}
	
	/**
	 * US602157: Accessories > Standalone accessories > Configure EIP amount
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testFRPPricingOnCartPageForAccessoryOnStandaloneAccessoryCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US602157:Accessories > Standalone accessories > Configure EIP amount");
		Reporter.log("Data Condition | PAH user, And User should have less than $49 accessory device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Select an Accessory device which has less than $49 | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review cart page should be displayed");
		Reporter.log("8. Verify FRP pricing details | FRP price Should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = navigateToStandAloneAccessoriesPLP(myTmoData);
		accessoriesStandAlonePLPPage.applySorting();
		accessoriesStandAlonePLPPage.clickAccesoriesDeviceByName(myTmoData.getAccessoryName());
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		accessoriesStandAlonePDPPage.verifyAccessoryFRPLessThan49();
		accessoriesStandAlonePDPPage.clickAddToCartButton();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.verifyDueMonthlyLabelNotDisplay();
		accessoriesStandAloneReviewCartPage.verifyDueTodayLabel();
		accessoriesStandAloneReviewCartPage.verifyEipDueTodayForAccesories();
	}

	/**
	 * C357353 - Upgrade: Ensure customers can change Accessories from FRP to EIP and that pricing is correct in cart.
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testPricingVariationsOnCartPageForAccessoryOnStandaloneAccessoryCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("C357353 - Upgrade: Ensure customers can change Accessories from FRP to EIP and that pricing is correct in cart.");
		Reporter.log("Data Condition | PAH user");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Select an Accessory | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review cart page should be displayed");
		Reporter.log("8. Verify EIP & FRP pricing details | Respective prices should get reflected");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = navigateToStandAloneAccessoriesPLP(myTmoData);
		accessoriesStandAlonePLPPage.applySorting();
		accessoriesStandAlonePLPPage.clickAccesoriesDeviceByName(myTmoData.getAccessoryName());
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		accessoriesStandAlonePDPPage.verifyAccessoryFRPLessThan49();
		accessoriesStandAlonePDPPage.clickAddToCartButton();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.verifyDueMonthlyLabelNotDisplay();
		accessoriesStandAloneReviewCartPage.verifyDueTodayLabel();
		accessoriesStandAloneReviewCartPage.verifyEipDueTodayForAccesories();
		accessoriesStandAloneReviewCartPage.clickOnePaymentRadioButton();
		accessoriesStandAloneReviewCartPage.verifyDueMonthlyLabelNotDisplay();
		accessoriesStandAloneReviewCartPage.verifyDueTodayLabel();
	}
	
	/**
	 * CDCSM-294: UNO Ph2 - Standalone Accessories - Display only Accessories on UNO PLP 
	 * if MyTMO CART has Accessory only
	 * TC- 1985 : Validate if user is able to continue to navigate to Cart after selecting a new accessory
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUserAddedMultipleAccessoriesToCart(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on Accessories Quick Link on shop page | Application Navigated to UNO Stand Alone Accessory PLP page");
		Reporter.log("4. Select accessory device | UNOPDP page should be displayed");
		Reporter.log("5. Click on Add to Cart CTA | Cart page should be displayed");
		Reporter.log("6. Click on Continue Shopping CTA | UNOAccessory PLP page should be displayed");		
		Reporter.log("7. Select another accessory device | UNOPDP page should be displayed");
		Reporter.log("8. Click on Add to Cart CTA | Cart page should be displayed");
		Reporter.log("9. Verify devices in review cart | Devices should be displayed in review cart page");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		UNOPLPPage unoPLPPage = navigateToUNOStandAloneAccessoryPLPPageThroughQuickLink(myTmoData);	
		unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.ClickOnUpGradeCTA();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.clickContinueShoppingButton();
		unoPLPPage.verifyPageLoaded();
		unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.ClickOnUpGradeCTA();
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		//accessoriesStandAloneReviewCartPage.verifyAccessoryIsPresentOnCartPage(myTmoData)
	}
	
	/**
	 * CDCSM-294: UNO Ph2 - Standalone Accessories - Display only Accessories on UNO PLP 
	 * if MyTMO CART has Accessory only
	 * TC- 1986 : Validate if user is able to see FRP pricing in accessory cart if the pricing is less than 49$
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEipPriceisEnabledWhenUserAddedMultipleDevices(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on Accessories Quick Link on shop page | Application Navigated to UNO Stand Alone Accessory PLP page");
		Reporter.log("4. Select accessory device which is less than $49 | UNOPDP page should be displayed");
		Reporter.log("5. Click on Add to Cart CTA | Cart page should be displayed");
		Reporter.log("6. Verify FRP price | FRP price should be displayed");
		Reporter.log("6. Verify EIP price | EIP price should not be displayed");
		Reporter.log("7. Click on Continue Shopping CTA | UNOAccessory PLP page should be displayed");		
		Reporter.log("8. Select another accessory device which is greater than $49 | UNOPDP page should be displayed");
		Reporter.log("9. Click on Add to Cart CTA | Cart page should be displayed");
		Reporter.log("10. Verify EIP price | EIP should be displayed in review cart page");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		UNOPLPPage unoPLPPage = navigateToUNOStandAloneAccessoryPLPPageThroughQuickLink(myTmoData);
		unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyAccessoryFRPLessThan49();		
		unoPDPPage.ClickOnUpGradeCTA();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.verifyTotalFinancedAmountValueisNotDisplayed();		
		accessoriesStandAloneReviewCartPage.clickContinueShoppingButton();
		unoPLPPage.verifyPageLoaded();
		unoPLPPage.applySorting();
		unoPLPPage.clickDeviceByName(myTmoData.getAccessoryName());	
		unoPLPPage.verifyPageLoaded();
		unoPDPPage.ClickOnUpGradeCTA();
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.verifyEipDueMonthlyTotalPriceForAccesories();		
		
	}
	
	
	
}
/**
 * Retired US In this Page: US295814
 */