package com.tmobile.eservices.qa.webanalytics;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.CommonLibrary;
import com.tmobile.eservices.qa.data.MyTmoData;

public class UiTelemetry extends CommonLibrary {

	AnalyticsLib al = new AnalyticsLib();
	SoftAssert softAssert = new SoftAssert();

	/***
	 * 
	 * @param data
	 * @param myTmoData
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "UITelemetry" })
	public List<NameValuePair> testUiTelemetryFramework(ControlTestData data, MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		JavascriptExecutor js = new JavascriptExecutor() {

			@Override
			public Object executeScript(String arg0, Object... arg1) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object executeAsyncScript(String arg0, Object... arg1) {
				// TODO Auto-generated method stub
				return null;
			}
		};
		Map<String, Object> logType = new HashMap<>();
		logType.put("type", "sauce:network");
		List<Map<String, Object>> logEntries = (List<Map<String, Object>>) ((JavascriptExecutor) getDriver())
				.executeScript("sauce:log", logType);
		for (Map<String, Object> logEntry : logEntries) {
			String url = (String) logEntry.get("url");
			if (url.contains("UILogging")) {
				List<NameValuePair> urlValues = URLEncodedUtils.parse(url, Charset.defaultCharset());
				if (urlValues != null)
					return urlValues;
			}
		}
		return null;
	}

}
