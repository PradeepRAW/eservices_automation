package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.DealsPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class DealsPageTest extends TmngCommonLib{
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TMNG})
	public void testDealsPage(TMNGData tMNGData){
		navigateToDealsPage(tMNGData);
	}
	
	/**
	 * TA1655228: 7/4: Uatent: Email capture component is not throwing error message on submitting with empty fields
	 * Ref      : DE162981
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testEmailErrorMessageInDealsPage(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : TA1655228: 7/4: Uatent: Email capture component is not throwing error message on submitting with empty fields");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Deals Link | Deals page should be displayed");
		Reporter.log("3. Enter FirstName | FirstName should be entered");
		Reporter.log("4. Enter LastName | LastName should be entered");
		Reporter.log("5. Click Submit button | Email error message should be entered");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToDealsPage(tMNGData);
		DealsPage dealspage = new DealsPage(getDriver());
		dealspage.enterFirstName(tMNGData.getFirstName());
		dealspage.enterLastName(tMNGData.getLastName());
		dealspage.clickSubmitButton();
		dealspage.verifyEmailErrorMessage();
	}
		
}
