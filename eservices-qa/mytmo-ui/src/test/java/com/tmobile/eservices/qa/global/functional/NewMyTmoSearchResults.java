package com.tmobile.eservices.qa.global.functional;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.tmng.functional.SearchResults;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class NewMyTmoSearchResults extends GlobalCommonLib {


    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP })
    public void validateSearchResultsDefaultMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : validate default search results UI");
        Reporter.log("OFD | Search | Integration | Search Results UI");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. validate inline search text field displayed |Inline search field should be displayed");
        Reporter.log("3. validate device TAB is getting displayed | Device Tab should be displayed");
        Reporter.log("4. validate accessories TAB is getting displayed | Device Tab should be displayed");
        Reporter.log("5. validate support TAB is getting displayed | Device Tab should be displayed");
        Reporter.log("6. validate newsroom TAB is getting displayed | Device Tab should be displayed");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
//        pageInvoke();
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("smart watch");
        searchResults.isInlineSearchDisplayed();
        searchResults.isDeviceTabDisplayed();
        searchResults.validateResultsTemplateUnderDevicesAccessories();
        searchResults.isAccessoryTabDisplayed();
        searchResults.clickAccessoryTab();
        searchResults.validateResultsTemplateUnderDevicesAccessories();
        searchResults.isSupportTabDisplayed();
        searchResults.isNewsRoomTabDisplayed();
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP })
    public void validateInlineSearchMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : validate Device TAB in search results page");
        Reporter.log("OFD | Search | Integration | Device TAB");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. validate inline search text field displayed |Inline search field should be displayed");
        Reporter.log("3. enter search keyword 'apple'| search keyword 'apple' has been entered into text field");
        Reporter.log("4. verify search suggestion as 5 | search suggestion count should be displayed as 5");
        Reporter.log("5. validate 'apple' key word in search suggestions' | apple keyword should be displayed in suggestions data");
        Reporter.log("6. validate inline search suggestion close button and click on it | search suggestions should be closed ");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
//        pageInvoke();
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("Deals");;
        searchResults.isInlineSearchDisplayed();
        searchResults.enterSearchKeyword("apple");
//        searchResults.verifySearchSuggestionsCount(5);
        searchResults.verifySearchSuggestionsData("apple");
//        searchResults.verifyInlineSearchCloseButton(); //Inline search close button is removed permanently
    }


    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP})
    public void validateDeviceTabMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : validate Device TAB in search results page");
        Reporter.log("OFD | Search | Integration | Device TAB");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. validate inline search text field displayed |Inline search field should be displayed");
        Reporter.log("3. enter search keyword 'apple'| search keyword 'apple' has been entered into text field");
        Reporter.log("4. click on enter | should able to display search results as per search keyword");
        Reporter.log("5. validate device TAB is getting displayed | Device Tab should be displayed");
        Reporter.log("6. validate category name is getting displayed | Category name should get display within search results UI");
        Reporter.log("7. validate search results count under device TAB | Search count has displayed as per the requirement [<=10] under Device Tab");
        Reporter.log("8. Verify filter categories labels under Device tab | Filter category tabs should be displayed under Device Tab");
        Reporter.log("9. Verify mandatory labels labels in search results under Device tab | Mandatory labels  should be displayed under Device category search results");
        Reporter.log("10. verify results label | Results label should be displayed");
        Reporter.log("11. verify sort by label | Sort by label should be displayed");
        Reporter.log("12. verify sort by options  | Sort by options should be displayed");
        Reporter.log("13. verify pagination links | Pagination links should be displayed and clickable");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
//        pageInvoke();
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("iPhone 11");
        searchResults.isInlineSearchDisplayed();
        searchResults.enterSearchKeyword("apple");
        searchResults.hitEnterSearchKeyword();
        searchResults.isDeviceTabDisplayed();
        searchResults.categoryNameInSearchResults("DEVICE");
        searchResults.deviceNAccessoriesTabResultsCountPerPage();
        searchResults.validateResultsTemplateUnderDevicesAccessories();
        searchResults.verifyFilterCategoriesDeviceTab();
        searchResults.validateResultsLabel();
        searchResults.validateSortBy();
        searchResults.validateSortByOptionsDeviceTab();
        searchResults.validatePaginationLinks();
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP})
    public void validateAccessoriesTabMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : validate Accessories TAB in search results page");
        Reporter.log("OFD | Search | Integration | Accessories TAB");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. validate inline search text field displayed |Inline search field should be displayed");
        Reporter.log("3. enter search keyword 'iphone'| search keyword 'iphone' has been entered into text field");
        Reporter.log("4. click on enter | should able to display search results as per search keyword");
        Reporter.log("5. validate Accessories TAB is getting displayed | Accessories Tab should be displayed");
        Reporter.log("6. click on Accessories TAB  | Accessories Tab should be in focus");
        Reporter.log("7. validate category name is getting displayed | Category name should get display within search results UI");
        Reporter.log("8. validate search results count under Accessories TAB | Search count has displayed as per the requirement [<=10] under Accessories Tab");
        Reporter.log("9. Verify mandatory labels labels in search results under Device tab | Mandatory labels  should be displayed under Device category search results");
        Reporter.log("10. Verify filter categories labels under Accessories tab | Filter category tabs should be displayed under Accessories Tab");
        Reporter.log("11. verify results label | Results label should be displayed");
        Reporter.log("12. verify sort by label | Sort by label should be displayed");
        Reporter.log("13. verify sort by options  | Sort by options should be displayed");
        Reporter.log("14. verify pagination links | Pagination links should be displayed and clickable");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
//        pageInvoke();
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("air pods");
        searchResults.isInlineSearchDisplayed();
        searchResults.enterSearchKeyword("iphone");
        searchResults.hitEnterSearchKeyword();
        searchResults.isAccessoryTabDisplayed();
        searchResults.clickAccessoryTab();
//                .isAccessoryTabActive()
        searchResults.categoryNameInSearchResults("ACCESSORY");
        searchResults.deviceNAccessoriesTabResultsCountPerPage();
        searchResults.validateResultsTemplateUnderDevicesAccessories();
        searchResults.verifyFilterCategoriesAccessoriesTab();
        searchResults.validateResultsLabel();
        searchResults.validateSortBy();
        searchResults.validateSortByOptionsAccessoryTab();
        searchResults.validatePaginationLinks();
    }
    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP})
    public void validateSupportTabMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : validate Support TAB in search results page");
        Reporter.log("OFD | Search | Integration | Support TAB");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. validate inline search text field displayed |Inline search field should be displayed");
        Reporter.log("3. enter search keyword 'iphone'| search keyword 'iphone' has been entered into text field");
        Reporter.log("4. click on enter | should able to display search results as per search keyword");
        Reporter.log("5. validate Support TAB is getting displayed | Support Tab should be displayed");
        Reporter.log("6. validate category name is getting displayed | Category name should get display within search results UI");
        Reporter.log("7. verify results label | Results label should be displayed");
        Reporter.log("8. validate search results count under Support TAB | Search count has displayed as per the requirement [<=10] under Support Tab");
        Reporter.log("9. verify pagination links | Pagination links should be displayed and clickable");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
//        pageInvoke();
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("iPhone 11");
        searchResults.isInlineSearchDisplayed();
        searchResults.enterSearchKeyword("iphone");
        searchResults.hitEnterSearchKeyword();
        searchResults.isSupportTabDisplayed();
        searchResults.clickSupportTab();
        searchResults.categoryNameInSearchResults("SUPPORT");
        searchResults.validateResultsLabel();
        searchResults.supportTabResultsCountPerPage();
        searchResults.validatePaginationLinks();
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP})
    public void validateNewsRoomTabMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : validate NewsRoom TAB in search results page");
        Reporter.log("OFD | Search | Integration | NewsRoom TAB");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. validate inline search text field displayed |Inline search field should be displayed");
        Reporter.log("3. enter search keyword 'iphone'| search keyword 'iphone' has been entered into text field");
        Reporter.log("4. click on enter | should able to display search results as per search keyword");
        Reporter.log("5. validate NewsRoom TAB is getting displayed | NewsRoom Tab should be displayed");
        Reporter.log("6. click on NewsRoom TAB  | NewsRoom Tab should be in focus");
        Reporter.log("7. validate category name is getting displayed | Category name should get display within search results UI");
        Reporter.log("8. validate search results count under NewsRoom TAB | Search count has displayed as per the requirement [<=10] under NewsRoom Tab");
        Reporter.log("9. Verify filter categories labels under NewsRoom tab | Filter category tabs should be displayed under NewsRoom Tab");
        Reporter.log("10. verify results label | Results label should be displayed");
        Reporter.log("11. verify sort by label | Sort by label should be displayed");
        Reporter.log("12. verify sort by options  | Sort by options should be displayed");
        Reporter.log("13. verify pagination links | Pagination links should be displayed and clickable");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
//        pageInvoke();
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("john");
        searchResults.isInlineSearchDisplayed();
        searchResults.enterSearchKeyword("iphone");
        searchResults.hitEnterSearchKeyword();
        searchResults.isNewsRoomTabDisplayed();
        searchResults.clickNewsRoomTab();
        searchResults.categoryNameInSearchResults("NEWSROOM • NEWS");
        searchResults.newsroomTabResultsCountPerPage();
        searchResults.verifyFilterCategoriesNewsRoomTab();
        searchResults.validateResultsLabel();
        searchResults.validateSortBy();
        searchResults.validateSortByOptionsNewsroomTab();
        searchResults.validatePaginationLinks();
    }


    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP})
    public void validateFiltersMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : Apply filter in device TAB and verify the results UI");
        Reporter.log("OFD | Search | Integration | Filters");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. validate inline search text field displayed |Inline search field should be displayed");
        Reporter.log("3. enter search keyword 'apple'| search keyword 'apple' has been entered into text field");
        Reporter.log("4. click on enter | should able to display search results as per search keyword");
        Reporter.log("5. validate device TAB is getting displayed | Device Tab should be displayed");
        Reporter.log("6. validate category name is getting displayed | Category name should get display within search results UI");
        Reporter.log("7. Verify filter categories labels under Device tab | Filter category tabs should be displayed under Device Tab");
        Reporter.log("8. verify results label | Results label should be displayed");
        Reporter.log("9. Apply 'Mobile Internet' filter | 'Mobile Internet' filter should be applied");
        Reporter.log("10. verify filter search results count w.r.t results UI count | Filter results count should be matching with Results UI count");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
//        pageInvoke();
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("samsung");
        searchResults.isInlineSearchDisplayed();
        searchResults.enterSearchKeyword("apple");
        searchResults.hitEnterSearchKeyword();
        searchResults.isDeviceTabDisplayed();
//                .isDeviceTabActive()
        searchResults.categoryNameInSearchResults("DEVICE");
        searchResults.verifyFilterCategoriesDeviceTab();
        searchResults.validateResultsLabel();
        searchResults.applyFilter();
        searchResults.validateFilterResultsCount();
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP })
    public void validateInlineSearchClearButtonMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : Apply filter in device TAB and verify the results UI");
        Reporter.log("OFD | Search | Integration | Filters");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. validate inline search text field displayed |Inline search field should be displayed");
        Reporter.log("3. enter search keyword 'iPhone'| search keyword 'iPhone' has been entered into text field");
        Reporter.log("10. click on clear X button| Search keyword should be removed and suggestions should be disappear");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
//        pageInvoke();
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("Galaxy S10");
        searchResults.isInlineSearchDisplayed();
        searchResults.enterSearchKeyword("iphone");
        searchResults.clickInlineSearchClear();
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP})
    public void validateFilterClearButtonMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : Apply filter in device TAB and verify the results UI");
        Reporter.log("OFD | Search | Integration | Filters");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. validate inline search text field displayed |Inline search field should be displayed");
        Reporter.log("3. enter search keyword 'iPhone'| search keyword 'iPhone' has been entered into text field");
        Reporter.log("10. click on clear X button| Search keyword should be removed and suggestions should be disappear");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
//        pageInvoke();
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("cell phones");
        searchResults.enterSearchKeyword("apple");
        searchResults.hitEnterSearchKeyword();
        searchResults.isDeviceTabDisplayed();
        searchResults.applyFilter();
        searchResults.validateFilterResultsCount();
        searchResults.verifyFilterClearButton();
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP})
    public void validateClearAutoPopulationQueryMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : Apply filter in device TAB and verify the results UI");
        Reporter.log("OFD | Search | Inline Search - Clear auto-population query (ATDD)");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. enter search keyword 'apple' in inline search text field |user should be able to enter search keyword");
        Reporter.log("3. delete search keyword by hit backspace'| search keyword has been deleted and auto suggestions should not displayed");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("cell phones");
        searchResults.enterSearchKeyword("apple");
        searchResults.clearAutoPopulation();
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP})
    public void validateInteractiveSearchIconMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : validate interactive search inline search icon");
        Reporter.log("OFD | Search | Inline Search - Interactive icon (ATDD)");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO  | Application Should be Launched");
        Reporter.log("2. enter search keyword 'cell phones' in Unav search text field |user should be able to enter search keyword");
        Reporter.log("3. delete search keyword in inline search field'| search keyword has been deleted and auto suggestions should not displayed");
        Reporter.log("4. click on interactive search icon | default search results should be displaying");
        Reporter.log("5. enter search keyword 'apple' in inline search text field |user should be able to enter search keyword");
        Reporter.log("6. delete search keyword by hit backspace'| search keyword has been deleted and auto suggestions should not displayed");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("cell phones");
        searchResults.interactiveSearchIcon();
        searchResults.enterSearchKeyword("apple");
        searchResults.clearAutoPopulation();

    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP})
    public void validateSearchResults2NDTimeMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : validate default search results UI and enter search keyword for second time and verify the result");
        Reporter.log("OFD | Search | Integration | Search Results UI");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. validate inline search text field displayed |Inline search field should be displayed");
        Reporter.log("3. validate device TAB is getting displayed | Device Tab should be displayed");
        Reporter.log("4. validate accessories TAB is getting displayed | Device Tab should be displayed");
        Reporter.log("5. validate support TAB is getting displayed | support Tab should be displayed");
        Reporter.log("6. validate newsroom TAB is getting displayed | newsroom Tab should be displayed");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
//        pageInvoke();
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("iPhone");
        searchResults.isInlineSearchDisplayed();
        searchResults.enterSearchKeyword("apple");
        searchResults.hitEnterSearchKeyword();
        searchResults.isDeviceTabDisplayed();
        searchResults.isAccessoryTabDisplayed();
        searchResults.isSupportTabDisplayed();
        searchResults.isNewsRoomTabDisplayed();
        searchResults.enterSearchKeyword("samsung");
        searchResults.hitEnterSearchKeyword();
        searchResults.isDeviceTabDisplayed();
        searchResults.isAccessoryTabDisplayed();
        searchResults.isSupportTabDisplayed();
        searchResults.isNewsRoomTabDisplayed();

    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP })
    public void testSortFunctionalityMyTMO(ControlTestData data, MyTmoData myTmoData){
        Reporter.log("Test Case : Apply filter in device TAB and verify the results UI");
        Reporter.log("OFD | Search | Integration | Filters");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. validate inline search text field displayed |Inline search field should be displayed");
        Reporter.log("3. enter search keyword 'apple'| search keyword 'apple' has been entered into text field");
        Reporter.log("4. click on enter | should able to display search results as per search keyword");
        Reporter.log("5. validate device TAB is getting displayed | Device Tab should be displayed");
        Reporter.log("6. validate category name is getting displayed | Category name should get display within search results UI");
        Reporter.log("7. Verify filter categories labels under Device tab | Filter category tabs should be displayed under Device Tab");
        Reporter.log("8. verify results label | Results label should be displayed");
        Reporter.log("9. Apply 'Mobile Internet' filter | 'Mobile Internet' filter should be applied");
        Reporter.log("10. verify filter search results count w.r.t results UI count | Filter results count should be matching with Results UI count");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
//        pageInvoke();
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("samsung");
        searchResults.isInlineSearchDisplayed();
        searchResults.enterSearchKeyword("apple");
        searchResults.hitEnterSearchKeyword();
        searchResults.isDeviceTabDisplayed();
        searchResults.selectSortOption("LowToHigh");
        searchResults.validateSortFunctionality("LowToHigh");
        searchResults.selectSortOption("HighToLow");
        searchResults.validateSortFunctionality("HighToLow");

    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP })
    public void testPDPLinksMyTMO(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case : validate PDP link for devices/internet devices/SIM/Wearables/Tablets");
        Reporter.log("OFD | Search | PDP Links |");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch MyTMO/Search  | Application Should be Launched");
        Reporter.log("2. validate inline search text field displayed |Inline search field should be displayed");
        Reporter.log("3. enter search keyword and click on enter| should able to display search results as per search keyword");
        Reporter.log("5. click on PDP link | user should be able to navigate to TMO pdp page");
        Reporter.log("================================");
        Reporter.log("Actual Output:");
//        pageInvoke();
        SearchResults searchResults = new SearchResults(getDriver());
        navigateToHomePage(myTmoData);
        searchResults.unavSearchMyTmo("iphone 11");
        searchResults.clickPdpLink();
        searchResults.validatePdpLink("MyTMO","https://my.t-mobile.com/purchase");
        searchResults.clickBrowserbackButton();
        searchResults.enterSearchKeyword("alcatel linkzone");
        searchResults.hitEnterSearchKeyword();
        searchResults.clickPdpLink();
        searchResults.validatePdpLink("MyTMO","https://www.t-mobile.com/tablet/alcatel-linkzone");
        searchResults.clickBrowserbackButton();
        searchResults.enterSearchKeyword("smart watch");
        searchResults.hitEnterSearchKeyword();
        searchResults.clickPdpLink();
        searchResults.validatePdpLink("MyTMO","https://www.t-mobile.com/smart-watch/");
        searchResults.clickBrowserbackButton();
        searchResults.enterSearchKeyword("sim");
        searchResults.hitEnterSearchKeyword();
        searchResults.clickPdpLink();
        searchResults.validatePdpLink("MyTMO","https://www.t-mobile.com/");
        searchResults.clickBrowserbackButton();
        searchResults.enterSearchKeyword("apple ipad pro 12.9");
        searchResults.hitEnterSearchKeyword();
        searchResults.clickPdpLink();
        searchResults.validatePdpLink("MyTMO","https://www.t-mobile.com/tablet/");

    }

    /*public void pageInvoke() {
        String env = System.getProperty("environment");
        getDriver().get(env);
        checkPageIsReady();
        searchResults = new SearchResults(getDriver());
    }*/

}
