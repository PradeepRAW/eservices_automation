package com.tmobile.eservices.qa.accounts.api.cpslookup;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class GetEligibleServicesTest extends AccountsApi {

	public Map<String, String> tokenMap;

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligibleServicesTMOONETI(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Eligible Services for plan:TM1TI2  ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ratePlan", "TM1TI2");
		tokenMap.put("isMBB", "false");

		String requestBody = new ServiceTest().getRequestFromFile("GetEligibleServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligibleServices(apiTestData, updatedRequest);

		String operationName = "Get Eligible Services TMO ONE TI";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			assertThat(response.jsonPath().getList("servicesList.soc"),
					hasItems("VM2TEXT", "INTELMCAF", "NIDVM2T", "ZNASIT15", "LISTEN5"));
			Reporter.log("Expected servicesList are Returned in Response");

			assertThat(response.jsonPath().getList("dataServiceList.soc"), hasItems("FRLTDATA", "NODATA", "GLOBAL15","TM1PLUS"));
			Reporter.log("Expected dataServiceList are Returned in Response");

			assertThat(response.jsonPath().getList("sharedServicesList.soc"),
					hasItems("FAMALL10", "FAMNFX9", "FAMNFX13", "FAMMODE"));
			Reporter.log("Expected servicesList are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligibleServicesTMOONETE(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Eligible Services for plan:LTULFM2 ");
		Reporter.log("Test data : Any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ratePlan", "LTULFM2");
		tokenMap.put("isMBB", "false");

		String requestBody = new ServiceTest().getRequestFromFile("GetEligibleServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligibleServices(apiTestData, updatedRequest);

		String operationName = "Get Eligible Services TMO ONE TE";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			assertThat(response.jsonPath().getList("servicesList.soc"),
					hasItems("VM2TEXT", "INTELMCAF", "NIDVM2T", "ZNASIT15", "LISTEN5"));
			Reporter.log("Expected servicesList are Returned in Response");

			assertThat(response.jsonPath().getList("dataServiceList.soc"), hasItems("LTDATA", "NODATA", "TM1PLUS","GLOBAL15"));
			Reporter.log("Expected dataServiceList are Returned in Response");

			assertThat(response.jsonPath().getList("sharedServicesList.soc"), hasItems("FAMALLOW", "FAMMODE"));
			Reporter.log("Expected servicesList are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligibleServicesSC(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Eligible Services for plan:NAUTTF ");
		Reporter.log("Test data : Any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ratePlan", "NAUTTF");
		tokenMap.put("isMBB", "false");

		String requestBody = new ServiceTest().getRequestFromFile("GetEligibleServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligibleServices(apiTestData, updatedRequest);

		String operationName = "Get Eligible Services TMO ONE SC";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			assertThat(response.jsonPath().getList("servicesList.soc"),
					hasItems("INTELMCAF", "NIDVM2T", "LISTEN5", "ZNASIT15", "LISTEN5"));
			Reporter.log("Expected servicesList are Returned in Response");

			assertThat(response.jsonPath().getList("dataServiceList.soc"),
					hasItems("V2GBXDATA", "NODATA", "6GBDSC", "10GBDSC"));
			Reporter.log("Expected dataServiceList are Returned in Response");

			assertThat(response.jsonPath().getList("sharedServicesList.soc"), hasItems("FAMALLOW", "FAMMODE"));
			Reporter.log("Expected servicesList are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligibleServicesMobileInternet(ApiTestData apiTestData) throws Exception {

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ratePlan", "MITI6GB");
		tokenMap.put("isMBB", "true");

		String requestBody = new ServiceTest().getRequestFromFile("GetEligibleServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligibleServices(apiTestData, updatedRequest);

		String operationName = "Get Eligible Services Mobile Internet";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			assertThat(response.jsonPath().getList("servicesList.soc"), hasItems("INTELMCAF", "NIDVM2T", "LISTEN5"));
			Reporter.log("Expected servicesList are Returned in Response");

			assertThat(response.jsonPath().getList("dataServiceList.soc"), hasItems("NODATA"));
			Reporter.log("Expected dataServiceList are Returned in Response");

			Assert.assertTrue(response.jsonPath().getList("sharedServicesList.soc").size() == 0);
			Reporter.log("Expected servicesList are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligibleServicesNCCFamily(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Eligible Services for plan: NCCFAMILY");
		Reporter.log("Test data : Any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ratePlan", "NCCFAMILY");
		tokenMap.put("isMBB", "false");

		String requestBody = new ServiceTest().getRequestFromFile("GetEligibleServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligibleServices(apiTestData, updatedRequest);

		String operationName = "Get Eligible Services NCC Family";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			assertThat(response.jsonPath().getList("servicesList.soc"), hasItems("NIDVM2T", "INTELMCAF", "LISTEN5"));
			Reporter.log("Expected servicesList are Returned in Response");

			assertThat(response.jsonPath().getList("dataServiceList.soc"), hasItems("NODATA"));
			Reporter.log("Expected dataServiceList are Returned in Response");

			assertThat(response.jsonPath().getList("sharedServicesList.soc"), hasItems("FAMMODE"));
			Reporter.log("Expected servicesList are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligibleServicesPappyEssential(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Eligible Services for plan: TMESNTL2 ");
		Reporter.log("Test data : Any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ratePlan", "TMESNTL2");
		tokenMap.put("isMBB", "false");

		String requestBody = new ServiceTest().getRequestFromFile("GetEligibleServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligibleServices(apiTestData, updatedRequest);

		String operationName = "Get Eligible Services Pappy Essential";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			assertThat(response.jsonPath().getList("servicesList.soc"),
					hasItems("ZNASIT15", "VM2TEXT", "INTELMCAF", "NIDVM2T", "LISTEN5", "INTTMOB15"));
			Reporter.log("Expected servicesList are Returned in Response");

			assertThat(response.jsonPath().getList("dataServiceList.soc"), hasItems("ESN10GBDT","ESNDATA", "NODATA"));
			Reporter.log("Expected dataServiceList are Returned in Response");

			assertThat(response.jsonPath().getList("sharedServicesList.soc"),
					hasItems("FAMMODE", "FAMALL10", "FAMALLOW"));
			Reporter.log("Expected servicesList are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligibleServicesPappyTI(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Eligible Services for plan: 1PLSALL ");
		Reporter.log("Test data : Any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ratePlan", "1PLSALL");
		tokenMap.put("isMBB", "false");

		String requestBody = new ServiceTest().getRequestFromFile("GetEligibleServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligibleServices(apiTestData, updatedRequest);

		String operationName = "Get Eligible Services Pappy TI";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			assertThat(response.jsonPath().getList("servicesList.soc"), hasItems("VM2TEXT", "INTELMCAF", "ZNASIT15"));
			Reporter.log("Expected servicesList are Returned in Response");

			assertThat(response.jsonPath().getList("dataServiceList.soc"), hasItems("TM1PLUS0", "NODATA"));
			Reporter.log("Expected dataServiceList are Returned in Response");

			assertThat(response.jsonPath().getList("sharedServicesList.soc"),
					hasItems("FAMMODE", "FAMALL10", "FAMNFX9", "FAMNFX13"));
			Reporter.log("Expected servicesList are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligibleServicesUNlimited55(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Eligible Services for plan: 1PLONE55TISSALL ");
		Reporter.log("Test data : Any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ratePlan", "ONE55TIS");
		tokenMap.put("isMBB", "false");

		String requestBody = new ServiceTest().getRequestFromFile("GetEligibleServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligibleServices(apiTestData, updatedRequest);

		String operationName = "Get Eligible Services UNlimited 55";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			assertThat(response.jsonPath().getList("dataServiceList.soc"), hasItems("FRLTDATA", "NODATA"));
			Reporter.log("Expected dataServiceList are Returned in Response");

			assertThat(response.jsonPath().getList("sharedServicesList.soc"), hasItems("FAMMODE", "FAMALLOW"));
			Assert.assertTrue(response.jsonPath().getList("sharedServicesList.soc").size() == 2);
			Reporter.log("Expected servicesList are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}
}