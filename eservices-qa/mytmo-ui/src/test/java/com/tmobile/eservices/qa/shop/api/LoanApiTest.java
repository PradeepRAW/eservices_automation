package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.LoanApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class LoanApiTest extends LoanApi {

	public Map<String, String> tokenMap;
	/**
	 * UserStory# Description: Get Loan  AttributesFor Payment
	 *  
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "LoanApiTest", Group.SHOP,Group.PENDING_REGRESSION })
	public void testGetCustomerLoan(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get Loan  AttributesFor Payment");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Operation will return customer loan");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("loanId", "02714");
		Response response = getCustomerLoan(apiTestData, "");

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			Reporter.log(" <b>Response :</b> ");
			Reporter.log(response.body().asString());
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}
	/**
	 * UserStory# Description: Get Loan  AttributesFor Payment
	 *  
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "LoanApiTest", Group.SHOP,Group.PENDING_REGRESSION })
	public void testGetDeviceInfoForPaymentId(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get Loan  AttributesFor Payment");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Operation will return loan attributes required by Payment Module.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("loanId", "02714");
		Response response = getDeviceInfoForPaymentId(apiTestData, "");

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			Reporter.log(" <b>Response :</b> ");
			Reporter.log(response.body().asString());
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}
	
	/**
	 * UserStory# Description: Get Loan  AttributesFor Payment
	 *  
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "LoanApiTest", Group.SHOP,Group.PENDING_REGRESSION })
	public void testGetCustomerLoanByLoanId(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get Loan  AttributesFor Payment");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Operation will return customer loan.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("loanId", "02714");
		Response response = getCustomerLoanByLoanId(apiTestData, "");

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			Reporter.log(" <b>Response :</b> ");
			Reporter.log(response.body().asString());
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}
	/**
	 * UserStory# Description: Gets payment history on loan
	 *  
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "LoanApiTest", Group.SHOP,Group.PENDING_REGRESSION })
	public void testGetPaymentHistoryOnLoan(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Gets payment history on loan");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Operation will return Payment ID. ");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("loanId", "02714");
		Response response = getPaymentHistoryOnLoan(apiTestData, "");

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			Reporter.log(" <b>Response :</b> ");
			Reporter.log(response.body().asString());
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}
	
	/**
	 * UserStory# Description: Get Loan  AttributesFor Payment
	 *  
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "LoanApiTest", Group.SHOP,Group.PENDING_REGRESSION })
	public void testGetLoanAttributesForPayment(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get Loan  AttributesFor Payment");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Operation will return loan attributes required by Payment Module to process an EIP Balance Payment. ");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("loanId", "02714");
		Response response = getLoanAttributesForPayment(apiTestData, "");

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			Reporter.log(" <b>Response :</b> ");
			Reporter.log(response.body().asString());
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			logStep(StepStatus.FAIL, "Failure Reason", response.body().asString());
			Assert.fail("Failure Reason : " + response.body().asString());
		}
	}
	
}

