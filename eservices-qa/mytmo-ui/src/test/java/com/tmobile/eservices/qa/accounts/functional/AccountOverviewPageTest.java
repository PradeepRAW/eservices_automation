/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.accounts.CostDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.LineDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.PlanDetailsPage;
import com.tmobile.eservices.qa.pages.shop.AddALinePage;

/**
 * 
 * @author Sudheer Reddy Chilukuri
 *
 */
public class AccountOverviewPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(AccountOverviewPageTest.class);

	/*
	 * US435588 : MyTMO-AAL: Clicking on AAL CTA US435775 : MyTMO-AAL: Eligibility
	 * Check Pass_Success Scenarios
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void successAddnewlineScenario(ControlTestData data, MyTmoData myTmoData) {
		logger.info("SuccessAddnewlineScenario");
		Reporter.log("Test Case : Success Add new line Scenario");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Account link | AccountOver view page should be displayed");
		Reporter.log("5. Verify that Add a new line CTA |Add a new line CTA should be displayed");
		Reporter.log("6. Click on Add a new line CTA |Application should navigated to Customer Intent Page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountoverviewpage = navigateToAccountoverViewPageFlow(myTmoData);
		accountoverviewpage.verifyAccountOverviewPage();
		accountoverviewpage.clickOnAddaLine();

		AddALinePage addALinePage = new AddALinePage(getDriver());
		addALinePage.verifyAddALinePage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verfiyAccountOverviewDetails(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Verify Account Overview page details");
		Reporter.log("Test Case : Verfiy Account Overview Details");
		Reporter.log("Test Data : PAH");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Account link | AccountOver view page should be displayed");
		Reporter.log("5. Verify that Add a new line CTA |Add a new line CTA should be displayed");
		Reporter.log("6. Click on Add a new line CTA |Madal Error window should be displayed with proper message ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountoverviewpage = navigateToAccountoverViewPageFlow(myTmoData);
		accountoverviewpage.verifyAccountDetailsPageHeaders();
		accountoverviewpage.verifyBan();
		accountoverviewpage.verifyMonthlyTotalDetails();
		accountoverviewpage.verifyPlanName();
		accountoverviewpage.verifyLineName();
		accountoverviewpage.verifyAddALine();
	}

	/*
	 * US415163 : Cost Details: Provide Navigation Back to Acct Overview Page
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyAllBackButtonScenariosInAccountOveview(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyAllBackButtonScenariosInAccountOveview");
		Reporter.log("Test Case : Verify All Back Button Scenarios In Account Oveview");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");

		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Account link| Account Overview page should be displayed");
		Reporter.log("5. Click on monthly total| User should be navigated to CostDetails Page ");
		Reporter.log("6. Click on HeaderBackArrow | User should be navigated to AccountOverview Page");
		Reporter.log("7. Click on monthly total| User should be navigated to CostDetails Page ");
		Reporter.log("8. Click on BrowserBackArrow | User should be navigated to AccountOverview Page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountoverviewpage = navigateToAccountoverViewPageFlow(myTmoData);
		accountoverviewpage.clickOnMonthlyTotalDetails();

		CostDetailsPage costdetailspage = new CostDetailsPage(getDriver());
		costdetailspage.verifyCostDetailsPage();
		costdetailspage.clickOnBreadCrumbLink(0);

		accountoverviewpage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnbreadcrumbLInks(0);

		accountoverviewpage.clickOnLineName();

		LineDetailsPage lineDetailsPage = new LineDetailsPage(getDriver());
		lineDetailsPage.verifyLineDetailsPage();

		lineDetailsPage.clickOnbreadcrumbLInks(0);
		accountoverviewpage.verifyAccountOverviewPage();
	}

	/*
	 * US435588 : MyTMO-AAL: Clicking on AAL CTA US435775 : MyTMO-AAL: Eligibility
	 * Check Pass_Success Scenarios
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testAccountOverViewUpsellURL(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testAccountOverViewUpsellURL");
		Reporter.log("Test Case : Test Account OverView Up sell URL");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Get Account over view UpSell URL | Account Overview page should be displayed");
		Reporter.log("5. Verify Account Overview header | Header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToFutureURLFromHome(myTmoData, "/account/account-overview");

		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
	}

	// Regression End

	/*
	 * US545596 -Specify Alerts to Show on the Line Details Page
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifySpecifyAlertsToShowOnTheLineDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySpecifyAlertsToShowOnTheLineDetailsPage");
		Reporter.log("Test Case : Specify Alerts to Show on the Line Details Page");
		Reporter.log("Test Data : Any Suspended customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Verify that Add a new line CTA |Add a new line CTA should be displayed");
		Reporter.log("6. Click on Add a new line CTA |Madal Error window should be displayed with proper message ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountoverviewpage = navigateToAccountoverViewPageFlow(myTmoData);
		// WHEN I click on the line (in "Lines and Devices" section) with a
		// suspended line
		// THEN the Line Details Page shows a message at the top of the page
		// (under the menu) that says the line  is suspended
		accountoverviewpage.verifySuspendedErrorMessage();
	}

	/*
	 * US545598 -Display Top Priority Alert Per Line on the Account Overview Page
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyTopPriorityAlertPerLineDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyTopPriorityAlertPerLineDetailsPage");
		Reporter.log("Test Case : Display Top Priority Alert Per Line on the Account Overview Page");
		Reporter.log("Test Data : Any Suspended customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Verify that Add a new line CTA |Add a new line CTA should be displayed");
		Reporter.log("6. Click on Add a new line CTA |Madal Error window should be displayed with proper message ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		// AccountOverviewPage accountoverviewpage =
		// navigateToAccountoverViewPageFlow(myTmoData);

		// THEN I see only the alert with the highest priority (in all caps like
		// the screenprint attached)
		// AND if two have the same priority for a line, I see the first of
		// these alerts returned
	}

	/*
	 * US545598 -Display Top Priority Alert Per Line on the Account Overview Page
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "sprint")
	public void verifyAlertsOnAccountOverviewPageForPAHUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAlertsOnAccountOverviewPageForPAHUser");
		Reporter.log("Test Case : Display Alerts On Account Overview Page For PAH User on the Account Overview Page");
		Reporter.log("Test Data : Any PAH User with 80% data used");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Account Overview");
		Reporter.log("5. Verify the alert text from the EOS service is shown above the line impacted");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountoverviewpage = navigateToAccountoverViewPageFlow(myTmoData);
		accountoverviewpage.verifyAlertsOnAccountOverviewPage();
	}
}