/**
 *
 */
package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.global.ContactUSPage;
import com.tmobile.eservices.qa.pages.global.StoreLocatorPage;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.PhoneSelectionPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.shop.TradeInAnotherDevicePage;
import com.tmobile.eservices.qa.pages.shop.TradeInConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.TradeinDeviceConditionPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

/**
 * @author charshavardhana
 *
 */
public class LineSelectorDetailsPageTest extends ShopCommonLib {


	/**
	 * US169442: Trade in Phone Selection Page : Trade-in Device Tile - Device
	 * Details
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testPhoneSelectionPageComponent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop tab | Shop Page should be displayed");
		Reporter.log("5. Click on select device | PDP Page Should be Displayed");
		Reporter.log("6. Click Add to Cart | Line Selector Page should be displayed");
		Reporter.log("7. Click on First Line| Phone Selection Page Should be Displayed");
		Reporter.log("8. Verify Header | Header should be displayed");
		Reporter.log("9. Verify Skip Trade In Link | Skip Trade In Link should be displayed");
		Reporter.log("10. Verify Device Component | Device Component should be displayed");
		Reporter.log(
				"11. Verify Got a different Phone Link is Displayed | Got a different Phone Link should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");
		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageByFeatureDevices(myTmoData);
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.verifySkipTradeInLink();
		lineSelectorDetailsPage.veifyWantToTradeInDifferentPhone();
	}

	/**
	 * US407224:Trade in Redesign > Line selector > Unknown Device > Header
	 * US347133 MyTMO > Trade - in > Redesign > Line Selector > Learn more link
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP})
	public void testUnknowDeviceHeaderInLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US407224 : Trade in Redesign > Line selector > Unknown Device > Header");
		Reporter.log("Data Condition | PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on a Device | PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button | Line Selector Page should be Displayed");
		Reporter.log(
				"7. Click on line which has unknown device | Want to trade in this phone? See how much it's worth' header should be Displayed");
		Reporter.log("8. Click on 'Learn how trade in works' link | Trade in works Pop Up should be displayed");
		Reporter.log(
				"9. verify Trade in works Pop Up description| Trade in works Pop Up description should be displayed");
		Reporter.log("10. click on trade in pop up close button | Line Selector Page should be Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		lineSelectorPage.selectUnKnownDevice();

		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.verifyUnknownDeviceHeader();
		lineSelectorDetailsPage.clickTradeInWorksLink();
		lineSelectorDetailsPage.verifyTradeInWorksPopUpHeader();
		lineSelectorDetailsPage.verifyTradeInWorksPopUpDescription();
		lineSelectorDetailsPage.clickCloseTradeInWorks();
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();

	}

	/**
	 * US347125 : MyTmo > Trade - in Redesign > Line selector > Expand line >
	 * Device Information
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testExpandLineDeviceInformationInLineSelectionPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case#US347125 - Verify the device information on Expand Line Selection Page");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on a Device in PLP| PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button on PDP | Line Selector Page should be Displayed");
		Reporter.log(
				"7. Select a line and Expand the Selected line | I should see remaining lines should not be Displayed");
		Reporter.log("8. Verify the device image of line selected | I should see device image");
		Reporter.log("9. Verify the device line name of line selected | I should see device line name");
		Reporter.log("10. Verify the device line number of line selected | I should see device line number");
		Reporter.log("11. Verify the device name of line selected | I should see device name");
		Reporter.log("12. Click on back button on line selector page | I should navigate back to line selector page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		lineSelectorPage.clickOnFirstLine();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.verifyLineDeviceImageIsDisplayed();
		lineSelectorDetailsPage.verifyLineNameIsDisplayed();
		lineSelectorDetailsPage.verifyLineNumberIsDisplayed();
		lineSelectorDetailsPage.verifyDeviceNameIsDisplayed();
		lineSelectorDetailsPage.clickBackButton();
		lineSelectorPage.verifyLineSelectorPage();
		// approved by sam
	}

	/**
	 * US378677 : myTMO > Line Selector > Promo > Expanded > Header and
	 * Description > EIP balance
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPromoDevicesTradeInPromoHeaderAndDescriptionInLineSelectorPage(ControlTestData data,
																				   MyTmoData myTmoData) {
		Reporter.log("Test US378677 : myTMO > Line Selector > Promo > Expanded > Header and Description > EIP balance");
		Reporter.log("Data Condition | STD PAH Customer With Trade-In Promo Offer and EIP Balance");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on a Device(Trade-In Promo) in PLP| PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button on PDP| Line Selector Page should be Displayed");
		Reporter.log("7. Select a line which has EIP balance | Line Selector Details page should be diaplyed");
		Reporter.log(
				"8. Verify Trade-In promo header | Trade in this phone to get our $<**> off promotion! text should be Displayed");
		Reporter.log(
				"9. Verify Trade-In promo description | You'll have to pay off first, we will add the remaining balance of $** you owe to your order description should be Displayed");
		Reporter.log(
				"10. Verify Trade-In promo additional description | Here’s how it works Additional Trade-In Promo description with details should be Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.verifyTradeInMessage();
		lineSelectorDetailsPage.verifyTradeInPromoDescription();
		lineSelectorDetailsPage.verifyHowTradeInWorks();
	}

	/**
	 * US375601 : myTMO > Line Selector > Promo > Expanded > Header and
	 * Description > No EIP balance
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testTradeInPromoHeaderAndDescriptionInLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US375601 : myTMO > Line Selector > Promo > Expanded > Header and Description > No EIP balance");
		Reporter.log("Data Condition | STD PAH Customer With Trade-In Promo Offer with NO EIP balance");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on a Device(Trade-In Promo) in PLP| PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button on PDP| Line Selector Page should be Displayed");
		Reporter.log("7. Select a line which has no EIP balance | Line Selector Details page should be diaplyed");
		Reporter.log(
				"8. Verify Trade-In promo header | Trade in this phone to get our $<***> off promotion! header text should be Displayed");
		Reporter.log(
				"9. Verify Trade-In promo description| Here’s how it works description with details should be Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.verifyTradeInPromoHeader();
		lineSelectorDetailsPage.verifyTradeInPromoHeaderDescription();

	}

	/**
	 * US375599 : myTMO > Line Selector > Promo > Expanded > Promo link
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testTradeInPromoOfferLinkInLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US375599 : myTMO > Line Selector > Promo > Expanded > Promo link");
		Reporter.log("Data Condition | STD PAH Customer WithTrade-In Promo Offer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on a Device(Trade-In Promo) in PLP| PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button on PDP| Line Selector Page should be Displayed");
		Reporter.log("7. Select a line and Expand the Selected line | Line Selector Details page should be diaplyed");
		Reporter.log("8. Verify trade in promo offer link | Trade in Promo offer link should be displayed");
		Reporter.log("9. Click promo offer link | Promo offer details model should be displayed");
		Reporter.log("10. Verify 'How trade in offer works' | How trade in offer works should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.verifyPromoOfferLinkdisplay();
		lineSelectorDetailsPage.clickPromoOfferLink();
		lineSelectorDetailsPage.verifyPromoOfferPopUpHeader();
		lineSelectorDetailsPage.clickCloseTradeInWorks();
		lineSelectorDetailsPage.verifyLearnHowTradeInWorksLink();
	}

	/**
	 * US347236 :MyTMO > Line Selector > Trade in > Non Promo > With Balance
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testRemainingEIPbalanceInLineSelectionPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US347236 :MyTMO > Line Selector > Trade in > Non Promo > With Balance");
		Reporter.log("Data Condition | STD PAH Customer with EIP balance");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on a Device in PLP| PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button on PDP | Line Selector Page should be Displayed");
		Reporter.log(
				"7. Select a line (Line should have EIP balance and eligible for trade in) and Expand the Selected line | "
						+ "I should see remaining lines should not be Displayed");
		Reporter.log(
				"8. Verify TradeIn value eligibility message | Want to trade in this phone? You'll get $*** after it's paid off eligibility message  should be displayed");
		Reporter.log("9. Verify TradeIn value eligibility amount with doller($) symbol | "
				+ "TradeIn value eligibility amount with doller($) symbol should be displayed");
		Reporter.log("10. Verify TradeIn value eligibility message description | "
				+ "TradeIn value eligibility message description should be displayed");
		Reporter.log("11. Verify TradeIn value eligibility message description with remaining balance amount | "
				+ "TradeIn value eligibility message description with remaining balance amount should be displayed");
		Reporter.log(
				"12. Click 'Continue trade in this phone' button | TradeIn Device Condition page should be displayed");
		Reporter.log(
				"13. Select all trade in values and Click Continue button | TradeInDevice condition page should be displayed");
		Reporter.log("14. Click Continue button | TradeInDevice condition page should be displayed");
		Reporter.log("15. Select it's good radio button & click continue |Trade-in value page should be displayed");
		Reporter.log("16. Click agree & continue button | Cart page should be displayed");
		Reporter.log(
				"17.Verify remaining EIP balance in cart page  | Remaining EIP balance in cart page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		lineSelectorPage.selectUnKnownDevice();

		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyTradeInMessage();
		lineSelectorDetailsPage.verifyTradeInValueWithDollarSymbol();
		lineSelectorDetailsPage.verifyTradeInDescription();
		lineSelectorDetailsPage.clickTradeInCTA();

		TradeinDeviceConditionPage tradeinDeviceConditionPage = new TradeinDeviceConditionPage(getDriver());
		tradeinDeviceConditionPage.verifyTradeInDeviceCondition();
		tradeinDeviceConditionPage.verifyDeviceConditionAsItsGood();
		tradeinDeviceConditionPage.clickOnGoodCondition();
		TradeInConfirmationPage tradeInConfirmationPage = new TradeInConfirmationPage(getDriver());
		tradeInConfirmationPage.verifyTradeInConfirmationPage();
		tradeInConfirmationPage.clickContinueTradeInButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyeipMonthlyAmtCartPage();

	}

	/**
	 * US353246 :myTMO > Line Selector > Trade in > Promo > Modularity off
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testLineSelectorPageTradeInPromo(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case US353246 :Test Line Selector Page Modularity off for Tradein Promo");
		Reporter.log("Data Condition | STD PAH Customer,  not eligible for trade in");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on banner | PDP page should be displayed");
		Reporter.log("5. Click on Add to Cart button on PDP| Line Selector Page should be Displayed");
		Reporter.log(
				"6. Select a Line which is not eligible for trade in(Account Level) | This phone is not eligible for trade in message should be displayed");
		Reporter.log("7. Verify Continue button| Continue button should be Disabled");
		Reporter.log("8. Verify Trade in a different phone link | Trade in a different phone link should be hiden");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickOnHeroBannerBuyButton();
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.verifyIneligibilityDescriptionIsDisplayed();
		lineSelectorDetailsPage.verifyYesGetStartedIfItsPresent();
		lineSelectorDetailsPage.verifyTradeInDiffentPhoneLinkInvisibleAndClickSkipTradeInCTA();

	}

	/**
	 * US347133 MyTMO > Trade - in > Redesign > Line Selector > Learn more link
	 * balance Testdata: 4042001801 TM0Test1
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testLearnMoreLinkInLineSelectionPageJumpCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US347133 : Verify Line Selector Learn more link");
		Reporter.log("Data Condition | jump Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on a Device in PLP which does not have promo offer | PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button on PDP | Line Selector Page should be Displayed");
		Reporter.log("7. Click on 'Learn how JUMP works' link | Trade in works Pop Up should be displayed");
		Reporter.log(
				"8. verify Trade in works Pop Up description| Trade in works Pop Up description should be displayed");
		Reporter.log("9. click on trade in pop up close button | Line Selector Page should be Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US507138:Trade In - Unknown Device, update CTA and URL destination
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUnknowDeviceUpdateCTAandURLDestination(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US407224 : Trade in Redesign > Line selector > Unknown Device > Header");
		Reporter.log("Data Condition | PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on a Device | PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button | Line Selector Page should be Displayed");
		Reporter.log("7. Verify Check the phone's value CTA | Check the phone's value CTA should be Displayed");
		Reporter.log("8. Click on Phone's Value CTA | Trade in another device should be Displayed");
		Reporter.log("9. Navigate back to Line Selector Detials Page | Line Selector Details Page should be Displayed");
		Reporter.log("10. Verify Want to trade in a different phone CTA | Want to trade in a different phone CTA should be Displayed");
		Reporter.log("11. Click on Want to trade in a different phone CTA |Phone Selection Page should be Displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.verifyPhoneValueCTA();
		lineSelectorDetailsPage.clickOnPhoneValueCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();

		lineSelectorDetailsPage.verifyWantToTradeDifferentPhoneLinkDisplay();
		lineSelectorDetailsPage.clickWantToTradeDifferentPhoneLink();
		PhoneSelectionPage phoneSelectionPage = new PhoneSelectionPage(getDriver());
		phoneSelectionPage.verifyPhoneSelectionPage();

	}

	/**
	 * US421124 :Trade in Redesign > Line Selector > MBB lines
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUserAbleToViewMBBlineInLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US421124 :Trade in Redesign > Line Selector > MBB lines");
		Reporter.log("Data Condition | STD PAH Customer With MBB line");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on a Device | PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button on PDP | Line Selector Page should be Displayed");
		Reporter.log("7. Select a MBB line | Line Selector Details page should be diaplyed");
		Reporter.log("8. Verify Device Information | Device information should be displayed");
		Reporter.log("9. Verify Complete order message | Complete order message should be displayed");
		Reporter.log("10. Click 'Call us' button | Contact-us page should be displayed");
		Reporter.log("11. Click 'Visit a store' button | Store locator page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		lineSelectorPage.clickMBBDevice();

		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.verifyCompleteOrderMessage();

		lineSelectorDetailsPage.clickOnCallUSButton();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
		contactUSPage.navigateBack();
		lineSelectorPage.clickMBBDevice();
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();

		lineSelectorDetailsPage.clickOnVisitAStoreButton();
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		storeLocatorPage.verifyStoreLocatorPage();
	}

	/**
	 * US421124 :Trade in Redesign > Line Selector > MBB lines
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testJumpUserAbleToViewJumpMBBlineInLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US421124 :Trade in Redesign > Line Selector > MBB lines");
		Reporter.log("Data Condition | Jump Customer With JUMP MBB line");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on a Device | PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button on PDP | Line Selector Page should be Displayed");
		Reporter.log(
				"7. Verify Jump MBB line in lineSelector page | Jump MBB Line should be displayed in line selector page");
		Reporter.log("8. Select a Jump MBB line | Line Selector Details page should be diaplyed");
		Reporter.log("9. Verify Device Information | Device information should be displayed");
		Reporter.log("10. Verify Complete order message | Complete order message should be displayed");
		Reporter.log("11. Verify Primary CTA 'Call us' button | Primary CTA'Call us' button should be displayed");
		Reporter.log("12. Click 'Call us' button | Contact-us page should be displayed");
		Reporter.log(
				"13. Verify Secondary CTA 'Visit a store' button | Secondary CTA 'Visit a store' button should be displayed");
		Reporter.log("14. Click 'Visit a store' button | Store locator page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}
	
	/**
	 * US515546 :Line Selector Page - Remove Names from all scenarios
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testRemoveUserNameInLineSelectorDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US515546 :Line Selector Page - Remove Names from all scenarios");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on a Device | PDP page should be displayed");
		Reporter.log("6. Click on Add to Cart button on PDP | Line Selector Page should be Displayed");
		Reporter.log("7. Select a line which has device name | Line Selector Details page should be diaplyed");		
		Reporter.log("8. Remove User name in front of line header | User name should be diaplyed in front of line header");

		Reporter.log("================================");
		Reporter.log("Actual Output:");		
		
		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		String cname = lineSelectorPage.getCustomerName();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.verifyCustomerNameRemovedFromHeader(cname);
	}
	
	/**
	 * US575086 :EIP payoff not available - Copy
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPpayOffNotAvailableForStandardUpgradeFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US575086 :EIP payoff not available");	
		Reporter.log("Data Condition | PAH User with EIP Balance");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");		
		Reporter.log("4. Click on Shop link | Shop page should be displayed");		
		Reporter.log("5. Click on See all phones link | PLP page should be displayed");
		Reporter.log("6. Select Device | PDP Page  should be displayed");
		Reporter.log("7. Select Payment option as EIP | Payment option should be selected ");
		Reporter.log("8. Click Add to cart button | LineSelector page should be displayed ");				
		Reporter.log("9. Select a line | LineSelector details page should be displayed");
		Reporter.log("10. Verify Device has a remaining balance message as'You still owe $XX.xx on this phone, and need to pay it off before you can trade it in.' | Device balance message should be displayed");
		Reporter.log("11. Verify message as 'Give us a call at 1-800-T-MOBILE and we’d be happy to help you complete your order!' | Message should be displayed");
		Reporter.log("12. Verify 'Yes I want to trade-in' button | 'Yes I want to trade-in' button should be disabled");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
	
		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.verifyRemainingBalanceMessage();
		lineSelectorDetailsPage.verifyGiveUsCallMessage();
		lineSelectorDetailsPage.verifyTradeInThisPhoneCTADisabled();
	}

}
