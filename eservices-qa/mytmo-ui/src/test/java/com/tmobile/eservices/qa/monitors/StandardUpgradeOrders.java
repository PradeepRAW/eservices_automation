package com.tmobile.eservices.qa.monitors;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.ShopAPIMonitorsHelper;
import com.tmobile.eservices.qa.api.eos.CartApiV5;
import com.tmobile.eservices.qa.api.eos.QuoteApi;
import com.tmobile.eservices.qa.data.ApiTestData;

public class StandardUpgradeOrders extends ShopAPIMonitorsHelper {
	
	@BeforeMethod(alwaysRun = true)
	public void clearTokenMapCommon(){
		tokenMapCommon = null;
	}
	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeFRPFlow(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			tokenMap.put("email", apiTestData.getEmailId());
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Standard Upgrade Flow FRP</b> ");

			// 1. Invoke getCRP (QueryCreditCheck)
			invokeQueryCreditCheck(apiTestData, tokenMap);

			// 2. Invoke FilterApi

			// invokeFilterApi(apiTestData,tokenMap);

			// 2. Invoke familyIdProductPostAPI_FRP
			invokeFamilyProducts(apiTestData, tokenMap, "v4");

			//checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke createCartAPI FRP
			requestBody = new ServiceTest().getRequestFromFile("createCartAPI_FRP_Order.txt");
			CartApiV5 cartApi = new CartApiV5();
			invokeCreateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			//checkEmptyValuesInTokenMap(tokenMap);

			// 4. Invoke CreateQuoteAPI FRP
			QuoteApi quoteApi = new QuoteApi();
			invokeCreateQuote(apiTestData, quoteApi, tokenMap, "v5");

			//checkEmptyValuesInTokenMap(tokenMap);

			// 5. Invoke UpdateQuoteAPI FRP
			requestBody = new ServiceTest().getRequestFromFile("UpdateQuote_FRP_req.txt");
			invokeUpdateQuote(apiTestData, requestBody, quoteApi, tokenMap, "v5");

			//checkEmptyValuesInTokenMap(tokenMap);

			// 6. Invoke UpdatePayment FRP
			invokeUpdatePayment(apiTestData, quoteApi, tokenMap, "v5");

			//checkEmptyValuesInTokenMap(tokenMap);

			// 7. Invoke OrderAPI FRP
			requestBody = new
			ServiceTest().getRequestFromFile("order_FRP_req.txt");
			invokeOrder(apiTestData, requestBody, tokenMap);

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeEIPFlow(ApiTestData apiTestData) {
		try {

			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("sku", apiTestData.getSku());
			tokenMap.put("correlationid", getValForCorrelation());
			tokenMap.put("email", apiTestData.getEmailId());
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Standard Upgrade Flow EIP</b> ");

			// 1. Invoke GetCRP (QueryCreditCheck)
			invokeQueryCreditCheck(apiTestData, tokenMap);

			// 2. Invoke familyIdProductPostAPI
			invokeFamilyProducts(apiTestData, tokenMap, "v4");

			// 3. Invoke createCartAPI EIP
			requestBody = new ServiceTest().getRequestFromFile("createCartAPI_EIP_forOrder.txt");
			CartApiV5 cartApi = new CartApiV5();
			invokeCreateCart(apiTestData, requestBody, tokenMap, cartApi, "v5");

			// 4. Invoke CreateQuoteAPI EIP
			QuoteApi quoteApi = new QuoteApi();
			invokeCreateQuote(apiTestData, quoteApi, tokenMap, "v5");

			// 5. Invoke UpdateQuoteAPI EIP
			requestBody = new ServiceTest().getRequestFromFile("UpdateQuote_EIP_req.txt");
			invokeUpdateQuote(apiTestData, requestBody, quoteApi, tokenMap, "v5");

			// 6. Invoke UpdatePayment EIP
			invokeUpdatePayment(apiTestData, quoteApi, tokenMap, "v5");

			// 7. Invoke OrderAPI EIP
			
			 requestBody = new
			 ServiceTest().getRequestFromFile("order_EIP_req.txt");
			 invokeOrder(apiTestData, requestBody, tokenMap);
			 

		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

}
