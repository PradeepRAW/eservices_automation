package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.AccountVerificationPage;
import com.tmobile.eservices.qa.pages.global.ChangeSimPage;
import com.tmobile.eservices.qa.pages.global.PhonePage;

public class ChangeSimPageTest extends GlobalCommonLib {

	/**
	 * US303680:SIM Swap | Digits Customer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testSimSwapWithPairedDigitsDataForPAH(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US303680:SIM Swap | Digits Customer");
		Reporter.log("Test Data : PAH");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone link | Phone page should be displayed");
		Reporter.log("5. Click on change sim link | Change sim page should be displayed");
		Reporter.log("6. Click on next button | Account verification page should be displayed");
		Reporter.log("7. Answer security questions and click on next button | Change sim page should be displayed");
		Reporter.log(
				"8. Select digits line and verify sim swap cannot be performed message | Sim swap cannot be performed message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		phonePage.clickChangeSimMenuLink();

		ChangeSimPage changeSimPage = new ChangeSimPage(getDriver());
		changeSimPage.verifyChangeSimPage();
		changeSimPage.clickNextBtn();

		AccountVerificationPage accountVerificationPage = new AccountVerificationPage(getDriver());
		accountVerificationPage.verifyAccountVerificationPage();
		accountVerificationPage.clickOnNextBtn();
		accountVerificationPage.clickOnSecurityQstnsNextBtn();

		changeSimPage = new ChangeSimPage(getDriver());
		changeSimPage.verifyChangeSimPage();
		changeSimPage.selectDigitsLine();
		changeSimPage.verifySimSwapMessage();
	}

	/**
	 * US283095:SIM Swap | Hidden for Restricted Customer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testSimSwapForPAHCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US283095:SIM Swap | Hidden for Restricted Customer");
		Reporter.log("Test Data : PAH");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone link | Phone page should be displayed");
		Reporter.log("5. Click on change sim link | Change sim page should be displayed");
		Reporter.log("6. Click on next button | Account verification page should be displayed");
		Reporter.log("7. Answer security questions and click on next button | Change sim page should be displayed");
		Reporter.log(
				"8. Select digits line and verify sim swap cannot be performed message | Sim swap cannot be performed message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		phonePage.clickChangeSimMenuLink();

		ChangeSimPage changeSimPage = new ChangeSimPage(getDriver());
		changeSimPage.verifyChangeSimPage();
		changeSimPage.clickNextBtn();

		AccountVerificationPage accountVerificationPage = new AccountVerificationPage(getDriver());
		accountVerificationPage.verifyAccountVerificationPage();
		accountVerificationPage.clickOnNextBtn();
		accountVerificationPage.clickOnSecurityQstnsNextBtn();

		changeSimPage = new ChangeSimPage(getDriver());
		changeSimPage.verifyChangeSimPage();
		changeSimPage.selectDigitsLine();
		changeSimPage.verifySimSwapMessage();
	}

}
