package com.tmobile.eservices.qa.tmng.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.tmng.api.PaymentApiV1;

import io.restassured.response.Response;

public class PaymentApiV1Test extends PaymentApiV1 {

	JsonPath jsonPath;
	public Map<String, String> tokenMap;

	/**
	 * /** UserStory# Description:US546902 - EOS TMNG payment tokenization
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "eosTMNGGetPublicKeyForPayment", "PaymentApiV1Test",
			Group.PROGRESSION })
	public void eosTMNGGetPublicKeyForPayment(ControlTestData data, ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {
		Reporter.log("TestName: EOS public key for TMNG Payment Tokenization");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth  token  ");
		Reporter.log("Step 2: Verify expected get public key response |Response should be 200 OK");
		Reporter.log("Step 3: Verify public key|public key should be there in response and not null");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();

		Response response = getPublicKeyForTMNG(apiTestData, tokenMap);
		String operationName = "Get Public Key";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("publicKey"), "public key is null");
				Reporter.log("public key is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# Description:US582650 - public key for EOS credit check API
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "eosShopGetPublicKeyForCreditCheck",
			"PaymentApiV1Test", Group.PROGRESSION })
	public void eosShopGetPublicKeyForCreditCheck(ControlTestData data, ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {
		Reporter.log("TestName: EOS public key for TMNG Payment Tokenization");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth  token  ");
		Reporter.log("Step 2: Verify expected get public key response with KeyType shop |Response should be 200 OK");
		Reporter.log("Step 3: Verify public key|public key should be there in response and not null");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();

		Response response = getShopPublicKeyForCreditCheck(apiTestData, tokenMap);
		String operationName = "Get  shop Public Key";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("publicKey"), "public key is null");
				Reporter.log("public key is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

}
