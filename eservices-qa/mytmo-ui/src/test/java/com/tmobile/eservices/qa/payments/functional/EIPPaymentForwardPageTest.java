/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.EIPDetailsPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentForwardPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentReviewCCPage;
import com.tmobile.eservices.qa.pages.payments.EquipmentPaymentAmountPage;
import com.tmobile.eservices.qa.pages.payments.ShopLineSectorPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author pshiva
 *
 */
public class EIPPaymentForwardPageTest extends PaymentCommonLib {

	/**
	 * US357075 EIP Payment Page Prepopulation > Receive Estimator Amount Validation
	 * Flow: JUMP EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentPageReceiveEstimatorAmountValidationForJUMPEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US357075 EIP Payment Page Prepopulation > Receive Estimator Amount Validation");
		Reporter.log("Data Condition | JUMP2.0 customer who has not paid 50% of eip amount");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select the Device of jump upgradable | Product details page should be displayed");
		Reporter.log("6. Click on Upgrade | Product lines page should be displayed");
		Reporter.log("7. Click on Pay Now link on the selected line | EIP payment page should be displayed");
		Reporter.log("8. Verify Estimator amount field | Estimator amount field should be Read Only");

		EIPPaymentForwardPage paymentPage = navigatefromShoptoEIPpaymentForwardPage(myTmoData);
		paymentPage.verifyPaymentAmountTxtBox();

	}

	/**
	 * US357075 EIP Payment Page Prepopulation > Receive Estimator Amount Validation
	 * Flow: Regular EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentPageReceiveEstimatorAmountValidationForRegularEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US357075 EIP Payment Page Prepopulation > Receive Estimator Amount Validation from billing");
		Reporter.log("Data Condition | EIP eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log(
				"5.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("6. Click on Installment Plan blade | EIP details page should be displayed");
		Reporter.log("7. Click on make payment button | EIP payment page should be displayed");
		Reporter.log("8. Verify Estimator amount field | Should be able to change Estimator amount");

		EIPPaymentForwardPage eipPaymentForwardPage = navigateToEIPPaymentForwardPage(myTmoData);
		eipPaymentForwardPage.verifyPaymentAmountTxtBox();
		String amount = eipPaymentForwardPage.enterPaymentamount();
		eipPaymentForwardPage.verifyEstimatorAmountField(amount);
	}

	/**
	 * 
	 * US357076 EIP Payment Page Prepopulation > Restriction of page functionality
	 * Flow: JUMP EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentPageRestrictionOfPageFunctionalityForJUMPEIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US357075 EIP Payment Page Prepopulation > Receive Estimator Amount Validation");
		Reporter.log("Data Condition | JUMP2.0 customer who has not paid 50% of eip amount");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select the Device of jump upgradable | Product details page should be displayed");
		Reporter.log("6. Click on Upgrade | Product lines page should be displayed");
		Reporter.log("7. Click on Pay Now link on the selected line | EIP payment page should be displayed");
		Reporter.log("8. Verify Estimator amount field | Estimator amount field should be Read Only");
		Reporter.log("9. Verify 'Pay EIP in Full' checkbox | Checkbox should be Read Only");
		Reporter.log("10. Verify 'change' link under payment towards section | Change link should be Read Only");
		Reporter.log("11. Enter payment details and verify | Payment details should be accepted");

		EIPPaymentForwardPage eipPaymentForwardPage = navigatefromShoptoEIPpaymentForwardPage(myTmoData);
		// eipPaymentForwardPage.verifyChangeButton();
		eipPaymentForwardPage.verifyPaymentAmountTxtBox();
		eipPaymentForwardPage.verifyEIPfullCheckBoxIsDisabled();
		eipPaymentForwardPage.clickNewRadioButton();
		eipPaymentForwardPage.fillPaymentInfo(myTmoData.getPayment());
		eipPaymentForwardPage.clickNextButton();
		EIPPaymentReviewCCPage paymentReviewPage = new EIPPaymentReviewCCPage(getDriver());
		paymentReviewPage.verifyPageLoaded();
	}

	/**
	 * 
	 * US357076 EIP Payment Page Prepopulation > Restriction of page functionality
	 * Flow: Regular EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentPageRestrictionOfPageFunctionalityForRegularEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US357076 EIP Payment Page Prepopulation > Restriction of page functionality");
		Reporter.log("Data Condition | EIP eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log(
				"5.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("6. Click on Installment Plan blade | EIP details page should be displayed");
		Reporter.log("7. Click on make payment button | EIP payment page should be displayed");
		Reporter.log("8. Verify Estimator amount field | Should be able to change Estimator amount");
		Reporter.log("9. Verify 'Pay EIP in Full' checkbox | Checkbox should be enabled to change");
		Reporter.log(
				"10. Verify 'change' link under payment towards section | Change link should be able to change the payment method");
		Reporter.log("11. Enter payment details and verify | Payment details should be accepted");

		EIPPaymentForwardPage eipPaymentForwardPage = navigateToEIPPaymentForwardPage(myTmoData);
		String amount = eipPaymentForwardPage.enterPaymentamount();
		eipPaymentForwardPage.verifyEstimatorAmountField(amount);
		eipPaymentForwardPage.verifyEIPfullCheckBoxIsEnabled();
		eipPaymentForwardPage.verifyPayEIPFullCheckBox();
		if (!eipPaymentForwardPage.verifySavedCCisChecked()) {
			eipPaymentForwardPage.fillPaymentInfo(myTmoData.getPayment());
		}
		eipPaymentForwardPage.clickNextButton();
		EIPPaymentReviewCCPage paymentReviewCCPage = new EIPPaymentReviewCCPage(getDriver());
		paymentReviewCCPage.clickEditPaymentLink();
		eipPaymentForwardPage.verifyPageLoaded();
		boolean isEnabled = eipPaymentForwardPage.verifyChangeButton();
		if (isEnabled) {
			EIPDetailsPage eIPDetailsPage = new EIPDetailsPage(getDriver());
			eIPDetailsPage.verifyPageLoaded();
		}

	}

	/**
	 * US356972 EIP Details Entry Point > Recieve Jump Flag US356974 EIP Details
	 * Entry Point > Receive Shop URL Validation Flow: JUMP EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPDetailsEntryPointReceiveJumpFlagAndShopURLForJUMPEIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US356974 | US356972 EIP Details Entry Point > Recieve Jump Flag");
		Reporter.log("Data Condition | JUMP2.0 customer who has not paid 50% of eip amount");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select the Device of jump upgradable | Product details page should be displayed");
		Reporter.log("6. Click on Upgrade | Product lines page should be displayed");
		Reporter.log("7. Click on Pay Now link on the selected line | EIP payment page should be displayed");
		Reporter.log("8. Verify Jump Flag attribute | Jump attribute should be available on the page as hidden field");
		Reporter.log(
				"8. Verify SHOP URL attribute | Shop URL attribute should be available on the page as hidden field");

		EIPPaymentForwardPage paymentPage = navigatefromShoptoEIPpaymentForwardPage(myTmoData);
		paymentPage.verifyJumpFlag();
		paymentPage.verifyShopURL();
	}

	/*
	 * Do not delete: a sample verificaation for adding page tag
	 * getTMOURL(myTmoData); LoginPage loginPage = new LoginPage(getDriver());
	 * loginPage.addTags(); loginPage.verifyAddedTag();
	 */

	/**
	 * US498489-US567737 ng6 EIP/Shop Paydown: Populate and Restrict Payment Amount
	 * Blade Verify the amount blade restriction
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testEIPShopPayOffAmountBlade(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPShopPayOffAmountBlade method called in BillingSummaryTest");
		Reporter.log("Shop - select required device to upgrade");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop | shop page should be displayed");
		Reporter.log("5. Click on device |product details page should load");
		Reporter.log("6. click on add to cart  | line selector page  should be displayed");
		Reporter.log("7. Click on Make a paymnet| equipment-landing page should be displayed");
		Reporter.log("8. Verify amount  | paymnet blade  should be restricted");
		Reporter.log("================================");

		EIPPaymentPage eipPaymentAmountPage = navigatefromShoptoEIPPaymentAmountPage(myTmoData);
		eipPaymentAmountPage.verifyNoPaymentAmountChevron();
		if (eipPaymentAmountPage.verifyPaymentAmountEnabled()) {
			eipPaymentAmountPage.clickAmountBlade();
			EquipmentPaymentAmountPage equipmentPaymentAmountPage = new EquipmentPaymentAmountPage(getDriver());
			if (equipmentPaymentAmountPage.verifyCurrentUrl("equipment-payment-amount")) {
				equipmentPaymentAmountPage.verifyPageUrl();
			} else {
				Reporter.log("PaymentAmount  is not Cickable");
			}

		} else {
			Reporter.log("PaymentAmount Blade is not Cickable");
		}
	}

	/**
	 * US498479-US567733 ng6 EIP/Shop Paydown: Populate and Restrict EIP Selection
	 * Blade Verify the eipblade restriction
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testEIPShopPayOffEipBlade(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPShopPayOffEipBlade method called in BillingSummaryTest");
		Reporter.log("Shop - select required device to upgrade");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop | shop page should be displayed");
		Reporter.log("5. Click on device |product details page should load");
		Reporter.log("6. click on add to cart  | line selector page  should be displayed");
		Reporter.log("7. Click on Make a paymnet| equipment-landing page should be displayed");
		Reporter.log("8. Verify eipblade  | eipblade should be restricted");
		Reporter.log("================================");

		EIPPaymentPage eipPaymentAmountPage = navigatefromShoptoEIPPaymentAmountPage(myTmoData);
		eipPaymentAmountPage.verifyEipInstallmentPlanclick();
		EquipmentPaymentAmountPage equipmentPaymentAmountPage = new EquipmentPaymentAmountPage(getDriver());
		if (equipmentPaymentAmountPage.verifyCurrentUrl("equipment-payment-amount")) {
			equipmentPaymentAmountPage.verifyPageUrl();
		} else {
			Reporter.log("EIP planid/blade  is not Cickable");
		}
	}

	/**
	 * US498490 ng6 EIP/Shop Paydown: Payment Cancellation Modal Verify the return
	 * to shop button
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void testEIPShopPayOffCancelbutton(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPShopPayOffCancelbutton method called in BillingSummaryTest");
		Reporter.log("Shop - select required device to upgrade");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop | shop page should be displayed");
		Reporter.log("5. Click on device |product details page should load");
		Reporter.log("6. click on add to cart  | line selector page  should be displayed");
		Reporter.log("7. Click on Make a paymnet| equipment-landing page should be displayed");
		Reporter.log("8.  click on cacel button | Modal should be displayed");
		Reporter.log("9.  click on yes | Redirects to shop line selector page ");
		Reporter.log("10.  Click on cancel| Equipment landing page should persists");
		Reporter.log("================================");

		EIPPaymentPage eipPaymentAmountPage = navigatefromShoptoEIPPaymentAmountPage(myTmoData);
		eipPaymentAmountPage.clickCancelBtn();
		eipPaymentAmountPage.verifyCancelModal();
		if (eipPaymentAmountPage.getModelDialogText().contains(PaymentConstants.EIP_CANCEL_MDOELDIALOG_TEXT)) {
			Reporter.log("Model Dialog text Verified as " + eipPaymentAmountPage.getModelDialogText());
			eipPaymentAmountPage.clickNoBtnOnCancelModal();
			eipPaymentAmountPage.verifyPageLoaded();
			eipPaymentAmountPage.verifyPageUrl();
			eipPaymentAmountPage.clickCancelBtn();
			eipPaymentAmountPage.clickYesBtnOnCancelModal();
			ShopLineSectorPage lineselectorPage = new ShopLineSectorPage(getDriver());
			lineselectorPage.verifyPageLoaded();
			lineselectorPage.verifyPageUrl();
		}

	}
}
