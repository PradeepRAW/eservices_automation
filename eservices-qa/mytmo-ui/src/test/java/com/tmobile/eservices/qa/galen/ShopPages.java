package com.tmobile.eservices.qa.galen;

import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class ShopPages extends ShopCommonLib{
	GalenLib galenLib = new GalenLib();

	/***
	 * Test Visual Testing for AccessoriesPDP
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testAccessoriesPDP(ControlTestData data, MyTmoData myTmoData) {
		// AccessoriesPDP
		galenLib.doVisualTest(getDriver(), "AccessoriesPDP","","");
	}
	
	/***
	 * Test Visual Testing for AccessoriesPLP
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop1" })
	public void testAccessoriesPLP(ControlTestData data, MyTmoData myTmoData) {
		navigateToAccessoryPLPPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		galenLib.doVisualTest(getDriver(), "AccessoriesPLP","","");
	}
	
	/***
	 * Test Visual Testing for AccessoriesStandAlonePDP
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testAccessoriesStandAlonePDP(ControlTestData data, MyTmoData myTmoData) {
		// AccessoriesStandAlonePDP
		galenLib.doVisualTest(getDriver(), "AccessoriesStandAlonePDP","","");
	}
	
	/***
	 * Test Visual Testing for AccessoriesStandAlonePLP
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testAccessoriesStandAlonePLP(ControlTestData data, MyTmoData myTmoData) {
		
		// AccessoriesStandAlonePLP
		galenLib.doVisualTest(getDriver(), "AccessoriesStandAlonePLP","","");
	}
	
	/***
	 * Test Visual Testing for AccessoriesStandAloneReviewCartPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testAccessoriesStandAloneReviewCartPage(ControlTestData data, MyTmoData myTmoData) {

		// AccessoriesStandAloneReviewCartPage
		galenLib.doVisualTest(getDriver(), "AccessoriesStandAloneReviewCartPage","","");
	}
	
	/***
	 * Test Visual Testing for AddALinePage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testAddALinePage(ControlTestData data, MyTmoData myTmoData) {
		
		// AddALinePage
		galenLib.doVisualTest(getDriver(), "AddALinePage","","");
	}
	
	/***
	 * Test Visual Testing for CartPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testCartPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		galenLib.doVisualTest(getDriver(), "CartPage","","");
	}
	
	/***
	 * Test Visual Testing for Esignature
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testEsignature(ControlTestData data, MyTmoData myTmoData) {
		
		// Esignature
		galenLib.doVisualTest(getDriver(), "Esignature","","");
	}
	
	/***
	 * Test Visual Testing for InsuranceMigrationPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testInsuranceMigrationPage(ControlTestData data, MyTmoData myTmoData) {
		 navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		 galenLib.doVisualTest(getDriver(), "DeviceProtectioPage","","");
	}
	
	/***
	 * Test Visual Testing for JumpCartPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testJumpCartPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToCartPageForJumpUserFromIMEIPage();
		galenLib.doVisualTest(getDriver(), "JumpCartPage","","");
	}
	
	/***
	 * Test Visual Testing for JumpLineSelectorPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testJumpLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		// JumpLineSelectorPage
		galenLib.doVisualTest(getDriver(), "JumpLineSelectorPage","","");
	}
	
	/***
	 * Test Visual Testing for LineSelectorPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		galenLib.doVisualTest(getDriver(), "LineSelectorPage","","");
	}
	
	/***
	 * Test Visual Testing for NewLineSelectorDetailsPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testNewLineSelectorDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		galenLib.doVisualTest(getDriver(), "NewLineSelectorDetailsPage","","");
	}
	
	/***
	 * Test Visual Testing for NewLineSelectorPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testNewLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		galenLib.doVisualTest(getDriver(), "NewLineSelectorPage","","");
	}
	
	/***
	 * Test Visual Testing for OfferDetailsPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testOfferDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToOfferDetailsPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "OfferDetailsPage","","");
	}
	
	/***
	 * Test Visual Testing for OrderConfirmation
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testOrderConfirmation(ControlTestData data, MyTmoData myTmoData) {
		// OrderConfirmation
		galenLib.doVisualTest(getDriver(), "OrderConfirmation","","");
	}
	
	/***
	 * Test Visual Testing for OrderStatusPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testOrderStatusPage(ControlTestData data, MyTmoData myTmoData) {
		launchAndNavigateToOrderStatusPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "OrderStatusPage","","");
	}
	
	/***
	 * Test Visual Testing for PDPPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testPDPPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPDPPageBySeeAllPhones(myTmoData);
		galenLib.doVisualTest(getDriver(), "PDPPage","","");
	}
	
	/***
	 * Test Visual Testing for PhonePage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testPhonePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPhonePage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PhonePage","","");
	}
	
	/***
	 * Test Visual Testing for PhoneSelectionPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testPhoneSelectionPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		galenLib.doVisualTest(getDriver(), "PhoneSelectionPage","","");
	}
	
	/***
	 * Test Visual Testing for PlansPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testPlansPage(ControlTestData data, MyTmoData myTmoData) {
		AccountsCommonLib accountsCommonLib = new AccountsCommonLib();
		accountsCommonLib.navigateToAccountOverviewPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "PlansPage","","");
	}
	
	/***
	 * Test Visual Testing for PLPPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testPLPPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPLPPageBySeeAllPhones(myTmoData);
		galenLib.doVisualTest(getDriver(), "PLPPage","","");
	}
	
	/***
	 * Test Visual Testing for Returns
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testReturns(ControlTestData data, MyTmoData myTmoData) {
		// Returns
		galenLib.doVisualTest(getDriver(), "Returns","","");
	}
	
	/***
	 * Test Visual Testing for ReviewCartPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testReviewCartPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToStandAloneReviewCartPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "ReviewCartPage","","");
	}
	
	/***
	 * Test Visual Testing for SecureCheckOutPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testSecureCheckOutPage(ControlTestData data, MyTmoData myTmoData) {
		// SecureCheckOutPage
		galenLib.doVisualTest(getDriver(), "SecureCheckOutPage","","");
	}
	
	/***
	 * Test Visual Testing for ShopPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testShopPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		galenLib.doVisualTest(getDriver(), "ShopPage","","");
	}
	
	/***
	 * Test Visual Testing for TradeInAnotherDevicePage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testTradeInAnotherDevicePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTradeInAnotherDevicePageBySeeAllPhones(myTmoData);
		galenLib.doVisualTest(getDriver(), "TradeInAnotherDevicePage","","");
	}
	
	/***
	 * Test Visual Testing for TradeInConfirmationPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testTradeInConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		// TradeInConfirmationPage
		galenLib.doVisualTest(getDriver(), "TradeInConfirmationPage","","");
	}
	
	/***
	 * Test Visual Testing for TradeInDeviceConditionPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testTradeInDeviceConditionPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTradeInDeviceConditionValuePageBySeeAllPhones(myTmoData);
		galenLib.doVisualTest(getDriver(), "TradeInDeviceConditionPage","","");
	}
	
	/***
	 * Test Visual Testing for TradeInPage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testTradeInPage(ControlTestData data, MyTmoData myTmoData) {
		// TradeInPage
		galenLib.doVisualTest(getDriver(), "TradeInPage","","");
	}
	
	/***
	 * Test Visual Testing for TradeInValuePage
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "shop" })
	public void testTradeInValuePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTradeInDeviceConditionConfirmationPageByFeatureDevicesWithTradeInFlow(myTmoData, "Iphone8");
		galenLib.doVisualTest(getDriver(), "TradeInValuePage","","");
	}


}
