package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.ClaimPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayLandingPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

public class ClaimPageTest extends PaymentCommonLib {

	/*
	 * US575971 :Claim page 1.5 US577086 :Validation routing treatment 1.5/6
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testClaimCardforMigratedCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US575971 | Claim page 1.5 ");
		Reporter.log("Data Condition | BAN/MSISDIN should have mutiple Card's and Bank's Paymentmethod");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on MakePayment | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on ConfirmorDelete link(Card/Bank) to validate | ClaimsPage is Displayed");
		Reporter.log("Step 7: Enter Card Details | Card Details Should be Displayed");
		Reporter.log("Step 8: Click on Continue Button | Spoke Page should be displayed and Payment should be updated");
		Reporter.log(
				"Step 9: Select the Payment Method | Click on Continue Button|Onetime Payment page should be displayed");
		Reporter.log("Step 10: Enter the Random Amount | Amount should be updated");
		Reporter.log("Step 11: Click on Agree and submit Button | Agree and Submit Button should be clicked");
		Reporter.log("Step 12: Verify Payment Confirmation screen | Paymentr Confirmation Screen should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		/*
		 * OTPAmountPage otpAmountPage = navigateToEditAmountPage(myTmoData);
		 * otpAmountPage.setOtherAmount(generateRandomAmount().toString());
		 * otpAmountPage.clickUpdateAmount(); OneTimePaymentPage oneTimePaymentPage =
		 * new OneTimePaymentPage(getDriver());
		 * oneTimePaymentPage.clickPaymentMethodBlade();
		 */
		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.ClickConfirmorDeleteLink(myTmoData.getCardNumber());
		ClaimPage claimsPage = new ClaimPage(getDriver());
		claimsPage.verifyPageLoaded();
		claimsPage.verifyPageUrl();
		claimsPage.verifyCliamsPageTitleText(PaymentConstants.CLAIMS_PAGE_HEADER);
		claimsPage.verifyCardLabel();
		claimsPage.enterCardorBankAcctNo(myTmoData.getCardNumber());
		claimsPage.clickContinueButton();
		/*
		 * otpSpokePage.verifySavedPaymentMethodNickName(myTmoData.getNickName());
		 * otpSpokePage.selectStoredCard(); otpSpokePage.clickContinueButton();
		 * OTPConfirmationPage confirmationPage = new OTPConfirmationPage(getDriver());
		 * confirmationPage.verifyPageLoaded();
		 */

	}

	/*
	 * US575971 :Claim page 1.5 US577086 :Validation routing treatment 1.5/6
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testClaimCardforMigratedBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US575971 | Claim page 1.5 ");
		Reporter.log("Data Condition | BAN/MSISDIN should have mutiple Card's and Bank's Paymentmethod");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on MakePayment | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on ConfirmorDelete link(Card/Bank) to validate | ClaimsPage is Displayed");
		Reporter.log("Step 7: Enter Card Details | Card Details Should be Displayed");
		Reporter.log("Step 8: Click on Continue Button | Spoke Page should be displayed and Payment should be updated");
		Reporter.log(
				"Step 9: Select the Payment Method | Click on Continue Button|Onetime Payment page should be displayed");
		Reporter.log("Step 10: Enter the Random Amount | Amount should be updated");
		Reporter.log("Step 11: Click on Agree and submit Button | Agree and Submit Button should be clicked");
		Reporter.log("Step 12: Verify Payment Confirmation screen | Paymentr Confirmation Screen should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		/*
		 * OTPAmountPage otpAmountPage = navigateToEditAmountPage(myTmoData);
		 * otpAmountPage.setOtherAmount(generateRandomAmount().toString());
		 * otpAmountPage.clickUpdateAmount(); OneTimePaymentPage oneTimePaymentPage =
		 * new OneTimePaymentPage(getDriver());
		 * oneTimePaymentPage.clickPaymentMethodBlade();
		 */
		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.ClickConfirmorDeleteLink(myTmoData.getAccNumber());
		ClaimPage claimsPage = new ClaimPage(getDriver());
		claimsPage.verifyPageLoaded();
		claimsPage.verifyPageUrl();
		claimsPage.verifyCliamsPageTitleText(PaymentConstants.CLAIMS_PAGE_HEADER);
		claimsPage.verifyBankLabel();
		claimsPage.enterCardorBankAcctNo(myTmoData.getAccNumber());
		claimsPage.clickContinueButton();
		/*
		 * otpSpokePage.verifySavedPaymentMethodNickName(myTmoData.getNickName());
		 * otpSpokePage.selectStoredCard(); otpSpokePage.clickContinueButton();
		 * OTPConfirmationPage confirmationPage = new OTPConfirmationPage(getDriver());
		 * confirmationPage.verifyPageLoaded();
		 */

	}

	/*
	 * US575971 :Claim page 1.5
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testDeleteCardforMigratedCardorBankfromCliamPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US575971 | Claim page 1.5 ");
		Reporter.log("Data Condition | BAN/MSISDIN should have mutiple Card's and Bank's Paymentmethod");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on MakePayment | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on ConfirmorDelete link(Card/Bank) to validate | ClaimsPage is Displayed");
		Reporter.log("Step 7: Click on Remove Card Button | Remove Card Button should be clicked");
		Reporter.log("Step 8: Click on Alert YesDelete button | YesDelete should be clicked");
		Reporter.log("Step 9: Verify Spoke page | Spoke Page page should be displayed");
		Reporter.log("Step 10: Verify Delted Payment Method | Deleted Payment Method should not be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.ClickConfirmorDeleteLink(myTmoData.getAccNumber());
		ClaimPage claimsPage = new ClaimPage(getDriver());
		claimsPage.verifyPageLoaded();
		claimsPage.verifyPageUrl();
		claimsPage.clickBackButton();
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.ClickConfirmorDeleteLink(myTmoData.getAccNumber());
		claimsPage.verifyPageLoaded();
		claimsPage.clickRemoveFromWalletButton();
		claimsPage.clickCancelButton();
		claimsPage.verifyPageLoaded();
		claimsPage.clickRemoveFromWalletButton();
		claimsPage.verifyAlertTitleText(PaymentConstants.CLAIMS_REMOVE_ALERT);
		claimsPage.clickYesButton();
		otpSpokePage.verifyPageLoaded();
		if (otpSpokePage.verifySavedPaymentMethodAccountNo(myTmoData.getAccNumber()) == false) {
			Reporter.log("Card or Bank deleted");
		}
	}

	/*
	 * US575971 :Claim page 1.5 US577079 : Migration integration front end
	 * integration inline error messaging 1.5 US577086 :Validation routing treatment
	 * 1.5/6
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testErrorforInvalidCardforMigratedCardforClaimPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US575971 | Claim page 1.5 ");
		Reporter.log("Data Condition | BAN/MSISDIN should have mutiple Card's and Bank's Paymentmethod");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on MakePayment | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: click on ConfirmorDelete link(Card/Bank) to validate | ClaimsPage is Displayed");
		Reporter.log("Step 7: Enter Incorrect Card Details |Error Message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.ClickConfirmorDeleteLink(myTmoData.getAccNumber());
		ClaimPage claimsPage = new ClaimPage(getDriver());
		claimsPage.verifyPageLoaded();
		claimsPage.verifyPageUrl();
		claimsPage.enterCardorBankAcctNo(myTmoData.getAccNumber());
		claimsPage.clickContinueButton();
		claimsPage.verifyErrorMessage(PaymentConstants.CLAIM_INLINE_ERROR_MESSAGE);
	}

	/*
	 * US575973 :Claim Page 6.0 US577086 :Validation routing treatment 1.5/6
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testPaymentCollectionClaimCardforMigratedCard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US575973 | Claim page 6.0");
		Reporter.log("Data Condition | BAN/MSISDIN should have mutiple Card's and Bank's Paymentmethod");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on Profile | Profile page should be displayed");
		Reporter.log("Step 4. Click Billing and paymnets|Saved Payment method blade should be displayed");
		Reporter.log("Step 5: click on Saved payment method blade | Payment collection Page should be displayed");
		Reporter.log("Step 6: click on ConfirmorDelete link(Card/Bank) to validate | ClaimsPage is Displayed");
		Reporter.log("Step 7: Enter Card Details | Card Details Should be Displayed");
		Reporter.log("Step 8: Click on Continue Button | Spoke Page should be displayed and Payment should be updated");
		Reporter.log(
				"Step 9: Select the Payment Method | Click on Continue Button|Onetime Payment page should be displayed");
		Reporter.log("Step 10: Enter the Random Amount | Amount should be updated");
		Reporter.log("Step 11: Click on Agree and submit Button | Agree and Submit Button should be clicked");
		Reporter.log("Step 12: Verify Payment Confirmation screen | Paymentr Confirmation Screen should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		if (getDriver().getCurrentUrl().contains("qata03")) {
			OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
			oneTimePaymentPage.clickPaymentMethodBlade();
		} else {
			NewAutopayLandingPage newAutopayPage = navigateToNewAutoPayLandingPage(myTmoData);
			newAutopayPage.clickAddPaymentmethod();
		}

		SpokePage paymentsCollectionPage = new SpokePage(getDriver());
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.verifyNewPageUrl();
		paymentsCollectionPage.ClickConfirmorDeleteLink(myTmoData.getCardNumber());
		ClaimPage claimsPage = new ClaimPage(getDriver());
		claimsPage.verifyPageLoaded();
		claimsPage.verifyPageUrl();
		claimsPage.verifyCliamsPageTitleText(PaymentConstants.CLAIMS_PAGE_HEADER);
		claimsPage.enterCardorBankAcctNo(myTmoData.getCardNumber());
		claimsPage.clickContinueButton();
		/*
		 * paymentsCollectionPage.verifySavedPaymentMethodNickName(myTmoData.getNickName
		 * ()); paymentsCollectionPage.selectStoredCard();
		 * paymentsCollectionPage.clickContinueButton(); //Code needs to update
		 * OTPConfirmationPage confirmationPage = new OTPConfirmationPage(getDriver());
		 * confirmationPage.verifyPageLoaded();
		 */

	}

	/*
	 * US575973 :Claim Page 6.0 US577086 :Validation routing treatment 1.5/6
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testPaymentCollectionClaimBankforMigratedBank(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US575973 | Claim page 6.0");
		Reporter.log("Data Condition | BAN/MSISDIN should have mutiple Card's and Bank's Paymentmethod");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on Profile | Profile page should be displayed");
		Reporter.log("Step 4. Click Billing and paymnets|Saved Payment method blade should be displayed");
		Reporter.log("Step 5: click on Saved payment method blade | Payment collection Page should be displayed");
		Reporter.log("Step 6: click on ConfirmorDelete link(Card/Bank) to validate | ClaimsPage is Displayed");
		Reporter.log("Step 7: Enter Card Details | Card Details Should be Displayed");
		Reporter.log("Step 8: Click on Continue Button | Spoke Page should be displayed and Payment should be updated");
		Reporter.log(
				"Step 9: Select the Payment Method | Click on Continue Button|Onetime Payment page should be displayed");
		Reporter.log("Step 10: Enter the Random Amount | Amount should be updated");
		Reporter.log("Step 11: Click on Agree and submit Button | Agree and Submit Button should be clicked");
		Reporter.log("Step 12: Verify Payment Confirmation screen | Paymentr Confirmation Screen should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		if (getDriver().getCurrentUrl().contains("qata03")) {
			OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
			oneTimePaymentPage.clickPaymentMethodBlade();
		} else {
			NewAutopayLandingPage newAutopayPage = navigateToNewAutoPayLandingPage(myTmoData);
			newAutopayPage.clickAddPaymentmethod();
		}
		SpokePage paymentsCollectionPage = new SpokePage(getDriver());
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.verifyNewPageUrl();
		paymentsCollectionPage.ClickConfirmorDeleteLink(myTmoData.getAccNumber());
		ClaimPage claimsPage = new ClaimPage(getDriver());
		claimsPage.verifyPageLoaded();
		claimsPage.verifyPageUrl();
		claimsPage.verifyCliamsPageTitleText(PaymentConstants.CLAIMS_PAGE_HEADER);
		claimsPage.enterCardorBankAcctNo(myTmoData.getAccNumber());
		claimsPage.clickContinueButton();
		/*
		 * paymentsCollectionPage.verifySavedPaymentMethodNickName(myTmoData.
		 * getAccNumber()); paymentsCollectionPage.selectStoredCard();
		 * paymentsCollectionPage.clickContinueButton(); //Code needs to update
		 * OTPConfirmationPage confirmationPage = new OTPConfirmationPage(getDriver());
		 * confirmationPage.verifyPageLoaded();
		 */

	}

	/*
	 * US575973 :Claim Page 6.0
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testPaymentCollectionDeleteCardorBankforfromCliamPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US575973 | Claim page 6.0");
		Reporter.log("Data Condition | BAN/MSISDIN should have mutiple Card's and Bank's Paymentmethod");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on Profile | Profile page should be displayed");
		Reporter.log("Step 4. Click Billing and paymnets|Saved Payment method blade should be displayed");
		Reporter.log("Step 5: click on Saved payment method blade | Payment collection Page should be displayed");
		Reporter.log("Step 6: click on ConfirmorDelete link(Card/Bank) to validate | ClaimsPage is Displayed");
		Reporter.log("Step 7: Click on Remove Card Button | Remove Card Button should be clicked");
		Reporter.log("Step 8: Click on Alert YesDelete button | YesDelete should be clicked");
		Reporter.log("Step 9: Verify Spoke page | Spoke Page page should be displayed");
		Reporter.log("Step 10: Verify Delted Payment Method | Deleted Payment Method should not be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		if (getDriver().getCurrentUrl().contains("qata03")) {
			OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
			oneTimePaymentPage.clickPaymentMethodBlade();
		} else {
			NewAutopayLandingPage newAutopayPage = navigateToNewAutoPayLandingPage(myTmoData);
			newAutopayPage.clickAddPaymentmethod();
		}
		SpokePage paymentsCollectionPage = new SpokePage(getDriver());
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.verifyNewPageUrl();
		paymentsCollectionPage.ClickConfirmorDeleteLink(myTmoData.getAccNumber());
		ClaimPage claimsPage = new ClaimPage(getDriver());
		claimsPage.verifyPageLoaded();
		claimsPage.verifyPageUrl();
		claimsPage.clickBackButton();
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.ClickConfirmorDeleteLink(myTmoData.getAccNumber());
		claimsPage.verifyPageLoaded();
		claimsPage.clickRemoveFromWalletButton();
		claimsPage.clickCancelButton();
		claimsPage.verifyPageLoaded();
		claimsPage.clickRemoveFromWalletButton();
		claimsPage.verifyAlertTitleText(PaymentConstants.CLAIMS_REMOVE_ALERT);
		claimsPage.clickYesButton();
		paymentsCollectionPage.verifyNewPageLoaded();
		if (paymentsCollectionPage.verifySavedPaymentMethodAccountNo(myTmoData.getAccNumber()) == false) {
			Reporter.log("Card or Bank deleted");
		}
	}

	/*
	 * US575973 :Claim Page 6.0 US577079 :Migration integration front end
	 * integration inline error messaging 6.0 US577086 :Validation routing treatment
	 * 1.5/6
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testPaymentCollectionErrorforInvalidCardforMigratedCardforClaimPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US575973 | Claim page 6.0 ");
		Reporter.log("Data Condition | BAN/MSISDIN should have mutiple Card's and Bank's Paymentmethod");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on Profile | Profile page should be displayed");
		Reporter.log("Step 4. Click Billing and paymnets|Saved Payment method blade should be displayed");
		Reporter.log("Step 5: click on Saved payment method blade | Payment collection Page should be displayed");
		Reporter.log("Step 6: click on ConfirmorDelete link(Card/Bank) to validate | ClaimsPage is Displayed");
		Reporter.log("Step 7: Enter Incorrect Card Details |Error Message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		if (getDriver().getCurrentUrl().contains("qata03")) {
			OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
			oneTimePaymentPage.clickPaymentMethodBlade();
		} else {
			NewAutopayLandingPage newAutopayPage = navigateToNewAutoPayLandingPage(myTmoData);
			newAutopayPage.clickAddPaymentmethod();
		}
		SpokePage paymentsCollectionPage = new SpokePage(getDriver());
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.ClickConfirmorDeleteLink(myTmoData.getAccNumber());
		ClaimPage claimsPage = new ClaimPage(getDriver());
		claimsPage.verifyPageLoaded();
		claimsPage.verifyPageUrl();
		claimsPage.enterCardorBankAcctNo(myTmoData.getAccNumber());
		claimsPage.clickContinueButton();
		claimsPage.verifyErrorMessage(PaymentConstants.CLAIM_INLINE_ERROR_MESSAGE);
	}
}