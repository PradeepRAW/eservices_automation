package com.tmobile.eservices.qa.global.functional;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.AssertJUnit;
//import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

//import org.testng.annotations.Test;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GarTMOTest{
	
	static List<String> pageURLs;
 	static List<String> imageURLs;
 	static HashMap<String, List<String>> tmoCacheResult;
 	
 	static WebDriver driver;
 	//static String applicationUrl=System.getProperty("https://www.t-mobile.com");
 	
 	/***
	 * list all the pages which contain s.tmocache.com 
	 * @return
	 */
	public static void getPagesWithCDN()
	{
		pageURLs = new ArrayList<String>();
		pageURLs.add("https://www.t-mobile.com/");
		pageURLs.add("https://www.t-mobile.com/cell-phones");
		pageURLs.add("https://www.t-mobile.com/cell-phone-plans");
		pageURLs.add("https://www.t-mobile.com/5g");
		pageURLs.add("https://www.t-mobile.com/internet-devices");
		pageURLs.add("https://www.t-mobile.com/offers/deals-hub");
		pageURLs.add("https://www.t-mobile.com/switch-to-t-mobile");
		pageURLs.add("https://www.t-mobile.com/resources/how-to-join-us");
		pageURLs.add("https://www.t-mobile.com/resources/bring-your-own-phone");
		pageURLs.add("https://www.t-mobile.com/coverage/4g-lte-network");
		pageURLs.add("https://www.t-mobile.com/coverage/international-calling");
		pageURLs.add("https://www.t-mobile.com/travel-abroad-with-simple-global");
		pageURLs.add("https://www.t-mobile.com/internet-of-things");
		pageURLs.add("https://www.t-mobile.com/offers/t-mobile-digits");
		pageURLs.add("https://www.t-mobile.com/hub/accessories-hub");
		pageURLs.add("https://www.t-mobile.com/tv");
		pageURLs.add("https://www.t-mobile.com/tv/all-hd-4k-tv");
		pageURLs.add("https://www.t-mobile.com/tv/dvr");
		pageURLs.add("https://www.t-mobile.com/tv/video-on-demand");
		pageURLs.add("https://www.t-mobile.com/tv/technology");
		pageURLs.add("https://www.t-mobile.com/tv/alexa-voice-controlled-tv");
		pageURLs.add("https://www.t-mobile.com/tv/google-assistant-voice-control-tv");
		pageURLs.add("https://www.t-mobile.com/tv/channels");
		pageURLs.add("https://www.t-mobile.com/tv/tvision-5g");
		pageURLs.add("https://www.t-mobile.com/tv/freedom-from-satellite");
		pageURLs.add("https://www.t-mobile.com/tv/all-hd-4k-tv");
		pageURLs.add("https://www.t-mobile.com/responsibility/privacy/privacy-policy");
		pageURLs.add("https://www.t-mobile.com/our-story");
		pageURLs.add("https://www.t-mobile.com/our-story/un-carrier-history");
		pageURLs.add("https://www.t-mobile.com/our-story/network-and-innovation");
		pageURLs.add("https://www.t-mobile.com/our-story/awards");
		pageURLs.add("https://www.t-mobile.com/our-story/working-together");
		pageURLs.add("https://www.t-mobile.com/responsibility");
		pageURLs.add("https://www.t-mobile.com/responsibility/community");
		pageURLs.add("https://www.t-mobile.com/responsibility/sustainability");
		pageURLs.add("https://www.t-mobile.com/responsibility/consumer-info");
		pageURLs.add("https://www.t-mobile.com/responsibility/privacy");
		pageURLs.add("https://www.t-mobile.com/responsibility/legal");
		pageURLs.add("https://www.t-mobile.com/news");
		pageURLs.add("https://www.t-mobile.com/news/archive");
		pageURLs.add("https://www.t-mobile.com/news/media-library");
		pageURLs.add("https://www.t-mobile.com/news/fact-sheets");
		pageURLs.add("https://www.t-mobile.com/news/contact-us");
		pageURLs.add("https://www.t-mobile.com/brand/john-legere-cookbook");
		pageURLs.add("https://www.t-mobile.com/responsibility/consumer-info/safety/9-1-1");
		pageURLs.add("https://www.tmobile.careers/job-search");
		pageURLs.add("https://www.t-mobile.com/responsibility/privacy/privacy-policy");
		pageURLs.add("https://www.t-mobile.com/responsibility/privacy/privacy-choice/ad-options");
		pageURLs.add("https://www.t-mobile.com/responsibility/legal/terms-and-conditions");
		pageURLs.add("https://www.t-mobile.com/responsibility/privacy");
		pageURLs.add("https://www.t-mobile.com/responsibility/consumer-info");
		pageURLs.add("https://www.t-mobile.com/responsibility/consumer-info/accessibility-policy");
		pageURLs.add("https://www.t-mobile.com/responsibility/consumer-info/policies/internet-service");
	}
	/**
	 * 
	 * This will instantiate the driver and integrate with the sauce labs
	 * @param local
	 */
	@SuppressWarnings("deprecation")
	public static void instantiateDriver(boolean local)
	{
		if(local)
		{
		System.setProperty("webdriver.chrome.driver", "C:/chromedriver_win32/chromedriver.exe");
		DesiredCapabilities caps = DesiredCapabilities.chrome();
		LoggingPreferences logPrefs = new LoggingPreferences();
		logPrefs.enable(LogType.PERFORMANCE, Level.INFO);
		caps.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
		driver = new ChromeDriver(caps);

		}
		else
		{
			try
			{
				LoggingPreferences logPrefs = new LoggingPreferences();
				logPrefs.enable(LogType.PERFORMANCE, Level.INFO);
				
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setBrowserName("chrome");
	        capabilities.setCapability("platform", "Windows 10");
	        capabilities.setCapability("version", "73.0");
	        capabilities.setCapability("name", "GAR_Code_Validation");
	        capabilities.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
	        capabilities.setCapability("parentTunnel", "");
	        capabilities.setCapability("tunnelIdentifier", "eServicesTunnels");
	        driver=new RemoteWebDriver(new URL("http://tmo_eServices:a138d0ba-09c7-4a2b-9565-67e15a5887c3@ondemand.saucelabs.com:80/wd/hub"), capabilities);
	        SessionId sessionId = ((RemoteWebDriver) driver).getSessionId();
			Reporter.log("Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/"
					+ sessionId + "\">SauceLabs</a>");
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * This will get all the images in tmobile website pages which are pointed to cdn.tmobile.com
	 * 
	 * @return
	 */
	@SuppressWarnings("finally")
	public static List<String> getAllImageURLsPointedToCdnUrl()
	{
		imageURLs = new ArrayList<String>();
		imageURLs.clear();
		try {
			List<LogEntry> entries = driver.manage().logs().get(LogType.PERFORMANCE).getAll();
			Reporter.log("Entries " + entries);
			for (LogEntry entry : entries) {
				String service_String = entry.getMessage();
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(service_String);
				if (service_String.contains("cdn.tmobile.com/images") && service_String.contains("responseReceived")) {
					System.out.println("response string is " + service_String);
					JsonNode deviceFamilyInfoPathNode1 = jsonNode.path("message").path("params").path("response");
					imageURLs.add(deviceFamilyInfoPathNode1.get("url").toString());
					
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return imageURLs;
		}
	}
	
	/**
	 * This will get all the images in tmobile site which are pointed to s.tmocache.com
	 * @return
	 */
	@SuppressWarnings("finally")
	public static List<String> getAllImageURLsPointedToTmoCache()
	{
		List<String> tmo_imageURLs = new ArrayList<String>();
		try {
			List<LogEntry> entries = driver.manage().logs().get(LogType.PERFORMANCE).getAll();
			
			for (LogEntry entry : entries) {
				String service_String = entry.getMessage();
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(service_String);

				if (service_String.contains("s.tmocache.com") && service_String.contains("responseReceived")) {
					JsonNode deviceFamilyInfoPathNode1 = jsonNode.path("message").path("params").path("response");
					tmo_imageURLs.add(deviceFamilyInfoPathNode1.get("url").toString());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			return tmo_imageURLs;
		}
	}
	
	@Test
	public  void runGARTestWithCDN() 
	{	
		try
		{
			tmoCacheResult = new HashMap<String,List<String>>();
			getPagesWithCDN();
			instantiateDriver(false);
			for(String url1 : pageURLs)
			{
				driver.navigate().to(url1);
				Thread.sleep(15000);
				Reporter.log("=========================================================================");
				Reporter.log("URL " + url1);	
				List<WebElement> links = driver.findElements(By.tagName("img"));
				for(WebElement ele:links) {
					if (ele.getAttribute("src").startsWith("https://cdn.tmobile.com/")) {
						Reporter.log("Image link " + ele.getAttribute("src")+" is pointed to cdn.tmobile.com");		
					}
					else 
						Reporter.log("Image link " +  ele.getAttribute("src")+" is not pointed to cdn.tmobile.com");
				}	
				/*try {						
					Assert.assertTrue(ele.getAttribute("src").startsWith("https://cdn.tmobile.com/"));
						Reporter.log("Image link " + ele.getAttribute("src")+" is pointed to cdn.tmobile.com");		
				} catch (Exception e) {
					Assert.fail("Image link " +  ele.getAttribute("src")+" is not pointed to cdn.tmobile.com");;
				}	*/
				
			int imageNotFoundCounter=0;
			List<String> imageNotFoundList=new ArrayList<String>();
			for (Map.Entry<String, List<String>> entry : tmoCacheResult.entrySet()) 
			{
				Reporter.log("url is "+entry.getKey() + " and its associated images are " + entry.getValue());
				if(entry.getValue().isEmpty())
				{
					for(String imageUrl:entry.getValue()) {
						try {
							if (!imageUrl.isEmpty()) {
								URL url = new URL(imageUrl.replace("\"", ""));
								HttpURLConnection connection = (HttpURLConnection) url.openConnection();
								connection.setRequestMethod("GET");
								connection.connect();
								int statusCode = connection.getResponseCode();
								if(statusCode==200) {
									Reporter.log("Image Url:  "+imageUrl+"  status code: "+statusCode+" - found");
								}else if(statusCode==404) {
									imageNotFoundCounter++;
									imageNotFoundList.add("Image Url:  "+imageUrl+"  status code: "+statusCode+" - not found");
								}else if(statusCode>=500) {
									imageNotFoundCounter++;
									imageNotFoundList.add("Image Url:  "+imageUrl+"  status code: "+statusCode+" - not found");
								}
					    }
						}catch(Exception e) {
							
						}
					}
				}		    
			}
			if (imageNotFoundList.size() > 0) {
				Reporter.log(imageNotFoundCounter + " images have  error");
				for(String image:imageNotFoundList) {
					
					Reporter.log(image);
				}
				AssertJUnit.fail(imageNotFoundCounter + " images have  error");
			}			
			}	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			driver.quit();
		}
	}
	
		
}
