package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.CartApiV5;
import com.tmobile.eservices.qa.api.eos.QuoteApiV5;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.shop.ShopConstants;
import com.tmobile.eservices.qa.shop.api.common.ShopApiCommonLibrary;

import io.restassured.response.Response;

public class QuoteApiV5Test extends ShopApiCommonLibrary{

	JsonPath jsonPath;
	public Map<String, String> tokenMap;
	/**
	/**
	 * UserStory# Description:US420772:Move EOS Address endpoint functionality to EOS CreateQuote microservice
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception 
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updateAddress",Group.SHOP,Group.SPRINT  })
	public void updateAddress(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update address test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response successfully updated address.");
		Reporter.log("Step 2: Verify address updated successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		Response response =quoteApiV5.updateAddress(apiTestData, ShopConstants.SHOP_QUOTE + "updateQuoteAddressV5.txt", tokenMap);
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
	}

	/**
	/**
	 * UserStory# Description:US420774:Integrate EOS UpdateQuote microservice with DCP v3 checkout API
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","createQuote",Group.SHOP,Group.SPRINT  })
	public void testCreateQuote(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: create quote test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response successfully created quote.");
		Reporter.log("Step 2: Verify quote created successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName="createCartForQuote";
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForQuote.txt");
		CartApiV5 cartApiV5 = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApiV5.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}


		QuoteApiV5 quoteApiV5=new QuoteApiV5();

		String requestQuote = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteV5.txt");
		String updatedRequestQuote = prepareRequestParam(requestQuote, tokenMap);

		Response responseQuote =quoteApiV5.updateQuote(apiTestData, updatedRequestQuote, tokenMap);
		Assert.assertTrue(responseQuote.statusCode()==200);
		Reporter.log("Response Status:"+responseQuote.statusCode());
	}

	/**
	/**
	 * UserStory# Description:US446867:EOS createQuote API integration with DCP endpoints- Upgrade Flow(FRP)
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","createQuote",Group.SHOP,Group.APIREG })
	public void testCreateQuoteUpgradeFRPflow(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: create quote test");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Test create cart with FULL terms|cartId should be generated and Response should be 200 OK");
		Reporter.log("Step 2: Test create quote with FULL terms |quote id should be generated");
		Reporter.log("Step 3: Verify Success services response (quote id,order id,shipment detail and option id)|Response should be 200 Ok and should contain shipment details");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName="createCartForQuote";
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForQuoteUpgradeFRPflow4486867.txt");
		CartApiV5 cartApiV5 = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApiV5.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}


		operationName="createQuote";
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteForUpgradeFRPflow.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				Assert.assertTrue(response.statusCode()==200);
				
				JsonPath jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				
				Assert.assertNotNull(jsonPath.get("shippingOptions[0].shipmentDetailId"), "Shipment Detail Id is null");
				Reporter.log("Shipment Detail Id is not null");
				
				Assert.assertNotNull(jsonPath.get("shippingOptions[0].shippingOptionId"), "Shipping Option Id is null");
				Reporter.log("Shipping Option Id is not null");
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

	}
	/**
	/**
	 * UserStory# Description:US446867:EOS createQuote API integration with DCP endpoints- Upgrade Flow(EIP)
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","createQuote",Group.SHOP,Group.APIREG })
	public void testCreateQuoteUpgradeEIPflow(ControlTestData data, ApiTestData apiTestData) throws Exception {
			Reporter.log("TestName: create quote test");
			Reporter.log("Data Conditions:MyTmo registered msisdn.");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("Step 1: Test create cart with EIP terms|cartId should be generated and Response should be 200 OK");
			Reporter.log("Step 2: Test create quote with EIP terms |quote id should be generated");
			Reporter.log("Step 3: Verify Success services response (quote id,order id,shipment detail and option id)|Response should be 200 Ok and should contain shipment details");
			Reporter.log("================================");
			Reporter.log("Actual Result:");

		String operationName="createCartForQuote";
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForQuoteUpgradeEIPflow.txt");
		CartApiV5 cartApiV4 = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApiV4.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuote";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteForUpgradeEIPflow.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				Assert.assertTrue(response.statusCode()==200);
				
				JsonPath jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				
				Assert.assertNotNull(jsonPath.get("shippingOptions[0].shipmentDetailId"), "Shipment Detail Id is null");
				Reporter.log("Shipment Detail Id is not null");
				
				Assert.assertNotNull(jsonPath.get("shippingOptions[0].shippingOptionId"), "Shipping Option Id is null");
				Reporter.log("Shipping Option Id is not null");
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

	}
	/**
	/**
	 * UserStory# Description:US420774:Integrate EOS UpdateQuote microservice with DCP v3 checkout API
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception 
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updateQuote",Group.SHOP,Group.SPRINT  })
	public void updateQuote(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update quote test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response successfully updated quote.");
		Reporter.log("Step 2: Verify quote updated successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String request = new ServiceTest().getRequestFromFile("test.txt");
		tokenMap = new HashMap<String, String>();
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		Response response =quoteApiV5.updateQuote(apiTestData, request, tokenMap);
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
	}

	/**
	/**
	 * UserStory# Description:US420774:Integrate EOS UpdateQuote microservice with DCP v3 checkout API
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.SHOP,Group.SPRINT  })
	public void updatePayment(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update quote payment test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response successfully updated quote payment.");
		Reporter.log("Step 2: Verify quote payment updated successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "updatePayment.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		Response response =quoteApiV5.updatePayment(apiTestData, updatedRequest, tokenMap);
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status:"+response.statusCode());
	}

	/**
	 * US519007  #EOS Update Quote API - updatePayment end-point (/v5/quote/{quoteId}/payment)-UPGRADE + FRP 
	 *
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.PENDING_REGRESSION})
	public void testUpdatePaymentUpGradeWithFRP(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Update Payment");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK");
		Reporter.log("Step 3: Verify Success services response code for Create quote |Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "createCartForPayment.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForPayment";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				
				Reporter.log("Cart Id is not null");
				
				//Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				//Reporter.log("Ban present in request is same as one fetched from excel sheet");
				
				//Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				//Reporter.log("MSISDN present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].items.device.sku"), apiTestData.getSku());
				Reporter.log("SKU present in request is same as one fetched from excel sheet");				
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].linePriceSummary.type"), "FULL");
				Reporter.log("Line is FULL");
				
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
				
				
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		String requestBodyUpdateCart = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateCartForPayment.txt");
		operationName = "Update Cart For Payment";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = cartApi.updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);
		

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			if (!jsonNode.isMissingNode()) 
			{
				jsonPath= new JsonPath(response.asString());
				
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.accessories[0].sku"), "Device SkuNumber is null");
				Reporter.log("Device SkuNumber is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");	
				Assert.assertTrue(response.statusCode()==200);
				
				
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");		
			}
		} else {
			failAndLogResponse(response, operationName);
	}    
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforPaymemnt";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "createQuoteforPayment.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="UpdateQuoteForPayment";
		String UpdateQuoteForPaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "UpdateQuoteForPayment.txt");
		String UpdateQuoteForPaymentRequest =prepareRequestParam(UpdateQuoteForPaymentRequestBody,tokenMap);
		logRequest(UpdateQuoteForPaymentRequest,operationName);
		response=quoteApiV5.updateQuote(apiTestData,UpdateQuoteForPaymentRequest,tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) 
		{
			jsonPath= new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("status.statusMessage"), "quote updated successfully");
			Reporter.log("quote updated successfully");
			logSuccessResponse(response, operationName);
		
		} 
		else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Update Quote Response status code : " + response.getStatusCode());
			Reporter.log("Update Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="UpdatePaymentCheck";
		String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdatePaymentCheck.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
		logRequest(updatePaymentRequest, operationName);
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			Reporter.log("Payment posted successfully");
			
		} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

	}
	
	/**
	/**
	 * UserStory# Description:US447239:EOS updateQuote API integration with DCP v3 endpoints- Upgrade Flow-FRP
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.SHOP,Group.APIREG})
	public void testUpdateQuoteWithFRPUpgradeFlow(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update quote test with FRP");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Test create cart with FULL terms|cartId should be generated and Response should be 200 OK");
		Reporter.log("Step 1: Test update cart |cart should be updated and Response should be 200 OK");
		Reporter.log("Step 2: Test create quote with FULL terms |quote id should be generated");
		Reporter.log("Step 2: Test update quote  |quote id should be updated");
		Reporter.log("Step 3: Verify Success services response |Response should be 200 Ok ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForPayment.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForPayment";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				
				JsonPath jsonPath= new JsonPath(response.asString());
				Assert.assertTrue(response.statusCode()==200);
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		String requestBodyUpdateCart = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateCartForPayment.txt");
		operationName = "Update Cart For Payment";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = cartApi.updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);
		

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode()==200);
				
				JsonPath jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");	
			}
		} else {
			failAndLogResponse(response, operationName);
	}
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforPaymemnt";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteforPayment.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
			
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="UpdateQuoteForPayment";
		String UpdateQuoteForPaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateQuoteForPayment.txt");
		String UpdateQuoteForPaymentRequest =prepareRequestParam(UpdateQuoteForPaymentRequestBody,tokenMap);
		logRequest(UpdateQuoteForPaymentRequest,operationName);
		response=quoteApiV5.updateQuote(apiTestData,UpdateQuoteForPaymentRequest,tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) 
		{
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			jsonPath= new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("status.statusMessage"), "quote updated successfully");
			Reporter.log("quote updated successfully");
		
		} 
		else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Update Quote Response status code : " + response.getStatusCode());
			Reporter.log("Update Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
	}
	/**
	/**
	 * UserStory# Description:US447239:EOS updateQuote API integration with DCP v3 endpoints- Upgrade Flow-EIP
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.SHOP,Group.APIREG })
	public void testUpdateQuoteWithEIPUpgradeFlow(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update quote test with EIP");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Test create cart with EIP terms|cartId should be generated and Response should be 200 OK");
		Reporter.log("Step 1: Test update cart |cart should be updated and Response should be 200 OK");
		Reporter.log("Step 2: Test create quote with EIP terms |quote id should be generated");
		Reporter.log("Step 2: Test update quote  |quote id should be updated");
		Reporter.log("Step 3: Verify Success services response |Response should be 200 Ok ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForPaymentEIP.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForPaymentEIP";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		String requestBodyUpdateCart = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "UpdateCartForPaymentEIP.txt");
		operationName = "Update Cart For Payment EIP";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = cartApi.updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);
		

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode()==200);
				
				JsonPath jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");	
			}
		} else {
			failAndLogResponse(response, operationName);
	}   
		QuoteApiV5 quoteApiV5 =new QuoteApiV5();
		operationName="createQuoteforPaymemntEIP";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "createQuoteforPaymentWithEIP.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="UpdateQuoteForPaymentEIP";
		String UpdateQuoteForPaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateQuoteForPaymentWithEIP.txt");
		String UpdateQuoteForPaymentRequest =prepareRequestParam(UpdateQuoteForPaymentRequestBody,tokenMap);
		logRequest(UpdateQuoteForPaymentRequest,operationName);
		response=quoteApiV5.updateQuote(apiTestData,UpdateQuoteForPaymentRequest,tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath= new JsonPath(response.asString());
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "quote updated successfully");
			Reporter.log("Quote is updated successfully");
			Assert.assertNotNull(jsonPath.get("orderNumber"), "Order Number is null");
			Reporter.log("Order Number is not null");
			Assert.assertNotNull(jsonPath.get("financeOrderTotal.totalFinancedAmount"), "Total Financed Amount is null");
			                                       
			Reporter.log("Total Financed Amount is not null");
			Assert.assertNotNull(jsonPath.get("financeInfo[0].installementPlanId"), "Installement Plan Id is null");
			Reporter.log("Installement Plan Id is not null");
		
		} 
		else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Update Quote Response status code : " + response.getStatusCode());
			Reporter.log("Update Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
	}
	
	/**
	 * US519008  #EOS Update Quote API - updatePayment end-point (/v5/quote/{quoteId}/payment)-UPGRADE + EIP 
	 *
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","US519008",Group.SHOP, Group.SPRINT })
	public void testUpdatePaymentUpGradeWithEIP(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Update Payment");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should be success returning cart id.");
		Reporter.log("Step 2: Verify Success services response code for update cart|Response code should be 200 OK.");
		Reporter.log("Step 3: Verify Success services response code for Create quote and update quote|Response code should be 200 OK.");
		Reporter.log("Step 4: Verify Success services response code for update Payment|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForPaymentEIP.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForPayment";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				
				Reporter.log("Cart Id is not null");
				
				//Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				//Reporter.log("Ban present in request is same as one fetched from excel sheet");
				
				//Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				//Reporter.log("MSISDN present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].items.device.sku"), apiTestData.getSku());
				Reporter.log("SKU present in request is same as one fetched from excel sheet");				
				
				Assert.assertEquals(jsonPath.get("cart.promotions[0].promoType"), "auto_pay");
				Reporter.log("Promotion Type is Auto Pay");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].linePriceSummary.type"), "EIP");
				Reporter.log("Line is EIP");
				
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		String requestBodyUpdateCart = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateCartForPaymentWithEIP.txt");
		operationName = "Update Cart For Payment EIP";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = cartApi.updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);
		

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			if (!jsonNode.isMissingNode()) 
			{
				jsonPath= new JsonPath(response.asString());
				
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.accessories[0].sku"), "Device SkuNumber is null");
				Reporter.log("Device SkuNumber is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");	
					
			}
		} else {
			failAndLogResponse(response, operationName);
	}  QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforPaymemnt";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteforPayment.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest,tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
			/*	Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");*/
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="UpdateQuoteForPayment";
		String UpdateQuoteForPaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateQuoteForPaymentWithEIP.txt");
		String UpdateQuoteForPaymentRequest =prepareRequestParam(UpdateQuoteForPaymentRequestBody,tokenMap);
		logRequest(UpdateQuoteForPaymentRequest,operationName);
		response=quoteApiV5.updateQuote(apiTestData,UpdateQuoteForPaymentRequest,tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) 
		{
			jsonPath= new JsonPath(response.asString());
			logSuccessResponse(response, operationName);
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "quote updated successfully");
			Reporter.log("Quote is updated successfully");
			Assert.assertNotNull(jsonPath.get("orderNumber"), "Order Number is null");
			Reporter.log("Order Number is not null");
			Assert.assertNotNull(jsonPath.get("financeOrderTotal.totalFinancedAmount"), "Total Financed Amount is null");
			                                       
			Reporter.log("Total Financed Amount is not null");
			Assert.assertNotNull(jsonPath.get("financeInfo[0].installementPlanId"), "Installement Plan Id is null");
			Reporter.log("Installement Plan Id is not null");
		} 
		else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Update Quote Response status code : " + response.getStatusCode());
			Reporter.log("Update Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="UpdatePaymentCheck";
		String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdatePaymentCheck.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
		logRequest(updatePaymentRequest, operationName);
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			
			Reporter.log("Payment posted successfully");
			
		} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

	}
	
	/**
	 * US516326  #EOS [Continued] [Technical] EOS createQuote API integration with DCP endpoints- AAL Flow 
	 *
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.SHOP,Group.APIREG})
	public void testCreateQuoteWithAAL_NBYOD_FRP_US516326(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Create Quote");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 3: Verify Success services response code for Create quote |Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
	
		tokenMap = new HashMap<String, String>();
		CartApiV5Tests art=new CartApiV5Tests();
		art.createCartAAL_NBYOD_FRPUS516325(apiTestData, tokenMap);

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "UpdatePlanAccessoriesAPI_NBYOD_FRP.txt");
		String operationName = "Update FRP Plan and Accessories";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response =art.updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode()==200);	
				jsonPath= new JsonPath(response.asString());
				
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				
				Assert.assertEquals(jsonPath.get("lines[0].items.plans[0].planId"),"TM1TI");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		
		
        QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforNBYODFRP";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"testCreateQuoteWithAAL.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
	
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);
		
		System.out.println("response is coming");
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.SHOP,Group.APIREG })
	public void testCreateQuoteWithAAL_NBYOD_EIP_US516326(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update cart of AAL flow with NBYOD EIP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");
		Reporter.log("Step 2: Verify create cart services response code|Response code should be 200 OK.");
		Reporter.log("Step 3: Update Cart should Update specific items to cart request for specific cart id");
		Reporter.log("Step 4: Verify Update cart of plan,accessories and services are updated|Response code should be 200 OK.");
		Reporter.log("step 5: Verify remove accessories and line|accessories and line should be removed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		CartApiV5Tests art=new CartApiV5Tests();
		art.createCartAAL_NBYOD_EIP_US516325(apiTestData, tokenMap);

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"UpdatePlanAccessoriesAPI_NBYOD_EIP.txt");
		String operationName = "Update EIP Plan and Accessories";

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = art.updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode()==200);	
				jsonPath= new JsonPath(response.asString());
				
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				Assert.assertEquals(jsonPath.get("lines[0].items.accessories[0].color"),"Gloss Black");
				Reporter.log("sku is not null");
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"),"Plan id is Null");
				Reporter.log("Plan id is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		
        operationName="createQuoteforNBYODEIP";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"testCreateQuoteWithAAL.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.SHOP,Group.APIREG })
	public void testCreateQuoteWithAAL_BYOD_FRP_US516326(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update cart of AAL flow with BYOD FRP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");
		Reporter.log("Step 2: Verify create cart services response code|Response code should be 200 OK.");
		Reporter.log("Step 3: Update Cart should Update specific items to cart request for specific cart id");
		Reporter.log("Step 4: Verify Update cart of plan and sim are updated|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap = new HashMap<String, String>();
		CartApiV5Tests art=new CartApiV5Tests();
		art.createCartAAL_BYOD_FRP_US516325(apiTestData, tokenMap);

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"UpdatePlanAPI_BYOD_FRP1.txt");
		String operationName = "Update FRP Plan";

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = art.updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		
        
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			
			
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode()==200);
				
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");
				
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforBYODFRP";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"testCreateQuoteWithAAL.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
	    response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
	}
	
	
	/**
	 * US528892  #EOS Update payment API - updatePayment end-point (/v5/quote/{quoteId}/payment)
	 *
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","US528892",Group.SHOP, Group.SPRINT })
	public void testUpdatePaymentFraudRequest(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Update Payment Fraud Check Request");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should be success returning cart id.");
		Reporter.log("Step 2: Verify Success services response code for update cart|Response code should be 200 OK.");
		Reporter.log("Step 3: Verify Success services response code for Create quote and update quote|Response code should be 200 OK.");
		Reporter.log("Step 4: Verify  request code for update Payment|For given request body response code should be 200 OK.");
		Reporter.log("Step 5: UpdatePayment request payload should have below nodes");
		Reporter.log("CustomerProfile.FirstName|CustomerProfile.FamilyName|CustomerProfile.PhoneNumbers[0].PhoneNumber|CustomerProfile.PhoneNumbers[0].PhoneType|CustomerPofile.AddressCommunications[0].AddressLine1|CustomerProfile.AddressCommunications[0].AddressLine2|CustomerProfile.AddressCommunications[0].StateCode|CustomerProfil.AddressCommunications[0].CityName|CustomerProfile.AddressCommunications[0].Zip");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForPayment.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForPayment";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				
				Reporter.log("Cart Id is not null");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				Reporter.log("Ban present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				Reporter.log("MSISDN present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].items.device.sku"), apiTestData.getSku());

				
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		String requestBodyUpdateCart = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateCartForPayment.txt");
		operationName = "Update Cart For Payment EIP";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = cartApi.updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);
		

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			if (!jsonNode.isMissingNode()) 
			{
				jsonPath= new JsonPath(response.asString());
				
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");

			}
		} else {
			failAndLogResponse(response, operationName);
	}
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforPaymemnt";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteforPayment.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest,tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="UpdateQuoteForPayment";
		String UpdateQuoteForPaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateQuoteForPayment.txt");
		String UpdateQuoteForPaymentRequest =prepareRequestParam(UpdateQuoteForPaymentRequestBody,tokenMap);
		logRequest(UpdateQuoteForPaymentRequest,operationName);
		response=quoteApiV5.updateQuote(apiTestData,UpdateQuoteForPaymentRequest,tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) 
		{
			jsonPath= new JsonPath(response.asString());
			logSuccessResponse(response, operationName);
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "quote updated successfully");
			Reporter.log("Quote is updated successfully");
		} 
		else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Update Quote Response status code : " + response.getStatusCode());
			Reporter.log("Update Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="UpdatePaymentCheck";
		String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdatePaymentCheck.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
		logRequest(updatePaymentRequest, operationName);
		System.out.println("verifying response code");
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(updatePaymentRequest);
		System.out.println("Request Body is "+jsonNode);
		
		Assert.assertNotNull(getPathVal(jsonNode,"customerProfile.firstName"));
		Assert.assertNotNull(getPathVal(jsonNode,"customerProfile.familyName"));
		Assert.assertNotNull(getPathVal(jsonNode,"customerProfile.phoneNumbers[0].phoneNumber"));
		Assert.assertNotNull(getPathVal(jsonNode,"customerProfile.phoneNumbers[0].phoneType"));
		Assert.assertNotNull(getPathVal(jsonNode,"customerProfile.addressCommunications[0].addressLine1"));
		Assert.assertNotNull(getPathVal(jsonNode,"customerProfile.addressCommunications[0].addressLine2"));
		Assert.assertNotNull(getPathVal(jsonNode,"customerProfile.addressCommunications[0].cityName"));
		Assert.assertNotNull(getPathVal(jsonNode,"customerProfile.addressCommunications[0].stateCode"));
		Assert.assertNotNull(getPathVal(jsonNode,"customerProfile.addressCommunications[0].zip"));
		
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			
			Reporter.log("Payment posted successfully");
			
		} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.PENDING_REGRESSION})
	public void testFraudCheckWithAAL_BYODFRP_Fail(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Fraud Check Request BYODFRP Fail flow");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("====");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should be success returning cart id.");
		Reporter.log("Step 2: verify update cart with plan and sim|Plan and sim should be updated with response code should be 200 OK.");
		Reporter.log("Step 3: Verify Create quote response|Response code should be 200 OK.");
		Reporter.log("Step 4: Verify update Address response|response code should be 200 OK.");
		Reporter.log("Step 5: Verify update Payment response|response code should be 200 OK.");
		Reporter.log("Step 6: Verify Fraud check response after three unsuccessful attempts for Fail Scenario|response code should be 404 OK.");

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForFraud_BYODFRP_Pass.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForFraud";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				
				Reporter.log("Cart Id is not null");
				
				//Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				//Reporter.log("Ban present in request is same as one fetched from excel sheet");
				
				//Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				//Reporter.log("MSISDN present in request is same as one fetched from excel sheet");			
				
				Assert.assertEquals(jsonPath.get("cart.promotions[0].promoType"), "auto_pay");
				Reporter.log("Promotion Type is Auto Pay");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].linePriceSummary.type"), "FULL");
				Reporter.log("Line is FULL");
				
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"UpdatePlanSimForFraudAPI_BYOD_FRP_Pass.txt");
	    operationName = "Update FRP Plan and Sim";

		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response =cartApi.updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		
        
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			
			
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode()==200);
				
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				
				//Assert.assertEquals(jsonPath.get("lines[0].items.simStarterKit.sku"), "610214641922");
				Reporter.log("SIM starter kit sku is present");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");
				
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		QuoteApiV5 quoteApiV5= new QuoteApiV5();
		operationName="createQuoteforBYODFRPFraud";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteforFraud_Pass.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
				tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="UpdateAddressCheck";
		String updateAdderssRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "UpdateAddressForFraud_Pass.txt");
		String updateAdderssRequest = prepareRequestParam(updateAdderssRequestBody,tokenMap);
		logRequest(updateAdderssRequest, operationName);
		response =quoteApiV5.updateAddress(apiTestData, updateAdderssRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			Assert.assertNotNull(jsonPath.get("address[1].addressType"), "address type is not present");
			Reporter.log("address type is present");
			
		} else {
			Reporter.log(" <b>Update Address Exception Response :</b> ");
			Reporter.log("Update Address Response status code : " + response.getStatusCode());
			Reporter.log("Update Address Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		
		operationName="UpdatePaymentFraud";
		String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdatePaymentForFraud_Pass.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
		logRequest(updatePaymentRequest, operationName);
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			Reporter.log("Payment posted successfully");
			tokenMap.put("transactionId",getPathVal(jsonNode, "fraudCheckPart.transactionId"));
			tokenMap.put("questionSetId",getPathVal(jsonNode, "fraudCheckPart.questionSetId"));
			tokenMap.put("questionId",getPathVal(jsonNode, "fraudCheckPart.questions.questionId"));
			tokenMap.put("choiceId",getPathVal(jsonNode,"fraudCheckPart.questions.choice[5].choiceid"));
			
		} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
     
		operationName="FraudCheck";
		String fraudCheckRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "FraudCheck_Pass.txt");
		String fraudCheckRequest = prepareRequestParam(fraudCheckRequestBody,tokenMap);
		logRequest(fraudCheckRequest, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			
			jsonPath=new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("questions.questionId"), "questionId is null");
			Reporter.log("questionId is not null");
			Assert.assertNotNull(jsonPath.get("questions.choice[5].choiceid"), "choiceid is null");
			Reporter.log("choiceid is not null");
			
			tokenMap.put("questionIdfc",getPathVal(jsonNode, "questions.questionId"));
			tokenMap.put("choiceIdfc",getPathVal(jsonNode,"questions.choice[3].choiceid"));
			
			
			    
			
		} else {
			Reporter.log(" <b>>Fraud Check Exception Response :</b> ");
			Reporter.log("Fraud Check  Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check  Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="FraudCheckFirst";
		String fraudCheckRequestBody1 = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck1_Pass.txt");
		String fraudCheckRequest1 = prepareRequestParam(fraudCheckRequestBody1,tokenMap);
		logRequest(fraudCheckRequest1, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest1, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			
			Assert.assertNotNull(jsonPath.get("questions.questionId"), "questionId is null");
			Reporter.log("questionId is not null");
			Assert.assertNotNull(jsonPath.get("questions.choice[5].choiceid"), "choiceid is null");
			Reporter.log("choiceid is not null");
			
			
			tokenMap.put("questionId",getPathVal(jsonNode, "questions.questionId"));
			tokenMap.put("choiceId",getPathVal(jsonNode,"questions.choice[1].choiceid"));
			
		} else {
			Reporter.log(" <b>Fraud Check 1 Exception Response :</b> ");
			Reporter.log("Fraud Check 1 Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check 1 Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="FraudCheckSecond";
		String fraudCheckRequestBody2 = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck2_Pass.txt");
		String fraudCheckRequest2 = prepareRequestParam(fraudCheckRequestBody2,tokenMap);
		logRequest(fraudCheckRequest2, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest2, tokenMap);

		if (response != null && "404".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());

			Assert.assertEquals(jsonPath.get("code"), "404");
			Reporter.log("Fraud failed successfully");
			
			
		} else {
			Reporter.log(" <b>Fraud Check 2 Exception Response :</b> ");
			Reporter.log("Fraud Check 2 Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check 2 Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.PENDING_REGRESSION})
	public void testFraudCheckWithAAL_BYODFRP_Pass(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Fraud Check Request BYODFRP Pass flow");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("====");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should be success returning cart id.");
		Reporter.log("Step 2: verify update cart with plan and sim|Plan and sim should be updated with response code should be 200 OK.");
		Reporter.log("Step 3: Verify Create quote response|Response code should be 200 OK.");
		Reporter.log("Step 4: Verify update Address response|response code should be 200 OK.");
		Reporter.log("Step 5: Verify update Payment response|response code should be 200 OK.");
		Reporter.log("Step 6: Verify Fraud check response after three successful attempts for Pass Scenario|response code should be 200 OK.");

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForFraud_BYODFRP_Pass.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		
		String operationName="createCartForFraud";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				
				Reporter.log("Cart Id is not null");
				
				//Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				Reporter.log("Ban present in request is same as one fetched from excel sheet");
				
				//Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				Reporter.log("MSISDN present in request is same as one fetched from excel sheet");			
				
				Assert.assertEquals(jsonPath.get("cart.promotions[0].promoType"), "auto_pay");
				Reporter.log("Promotion Type is Auto Pay");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].linePriceSummary.type"), "FULL");
				Reporter.log("Line is FULL");
				
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"UpdatePlanSimForFraudAPI_BYOD_FRP_Pass.txt");
	   
		operationName = "Update FRP Plan and Sim";

		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response =cartApi.updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		
        
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			
			
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode()==200);
				
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				
				//Assert.assertEquals(jsonPath.get("lines[0].items.simStarterKit.sku"), "610214641922");
				Reporter.log("SIM starter kit sku is present");
				
				/*Assert.assertNotNull(jsonPath.get("lines[0].items.accessories[0].sku"), "SkuNumber is null");
				Reporter.log("SkuNumber is not null");*/
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");
				
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforBYODFRPFraud";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteforFraud_Pass.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
				tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		
		operationName="UpdateAddressCheck";
		String updateAdderssRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateAddressForFraud_Pass.txt");
		String updateAdderssRequest = prepareRequestParam(updateAdderssRequestBody,tokenMap);
		logRequest(updateAdderssRequest, operationName);
		response =quoteApiV5.updateAddress(apiTestData, updateAdderssRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			Assert.assertNotNull(jsonPath.get("address[1].addressType"), "address type is not present");
			Reporter.log("address type is present");
			
		} else {
			Reporter.log(" <b>Update Address Exception Response :</b> ");
			Reporter.log("Update Address Response status code : " + response.getStatusCode());
			Reporter.log("Update Address Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		
		operationName="UpdatePaymentFraud";
		String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdatePaymentForFraud_Pass.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
		logRequest(updatePaymentRequest, operationName);
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			Reporter.log("Payment posted successfully");
			tokenMap.put("transactionId",getPathVal(jsonNode, "fraudCheckPart.transactionId"));
			tokenMap.put("questionSetId",getPathVal(jsonNode, "fraudCheckPart.questionSetId"));
			tokenMap.put("questionId",getPathVal(jsonNode, "fraudCheckPart.questions.questionId"));
			tokenMap.put("choiceId",getPathVal(jsonNode,"fraudCheckPart.questions.choice[5].choiceid"));
			
		} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
     
		operationName="FraudCheck";
		String fraudCheckRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck_Pass.txt");
		String fraudCheckRequest = prepareRequestParam(fraudCheckRequestBody,tokenMap);
		logRequest(fraudCheckRequest, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			
			jsonPath=new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("questions.questionId"), "questionId is null");
			Reporter.log("questionId is not null");
			Assert.assertNotNull(jsonPath.get("questions.choice[5].choiceid"), "choiceid is null");
			Reporter.log("choiceid is not null");
			
			tokenMap.put("questionIdfc",getPathVal(jsonNode, "questions.questionId"));
			tokenMap.put("choiceIdfc",getPathVal(jsonNode,"questions.choice[5].choiceid"));
			
			
			    
			
		} else {
			Reporter.log(" <b>>Fraud Check Exception Response :</b> ");
			Reporter.log("Fraud Check  Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check  Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="FraudCheckFirst";
		String fraudCheckRequestBody1 = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck1_Pass.txt");
		String fraudCheckRequest1 = prepareRequestParam(fraudCheckRequestBody1,tokenMap);
		logRequest(fraudCheckRequest1, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest1, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			
			Assert.assertNotNull(jsonPath.get("questions.questionId"), "questionId is null");
			Reporter.log("questionId is not null");
			Assert.assertNotNull(jsonPath.get("questions.choice[5].choiceid"), "choiceid is null");
			Reporter.log("choiceid is not null");
			
			
			tokenMap.put("questionId",getPathVal(jsonNode, "questions.questionId"));
			tokenMap.put("choiceId",getPathVal(jsonNode,"questions.choice[5].choiceid"));
			
		} else {
			Reporter.log(" <b>Fraud Check 1 Exception Response :</b> ");
			Reporter.log("Fraud Check 1 Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check 1 Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="FraudCheckSecond";
		String fraudCheckRequestBody2 = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck2_Pass.txt");
		String fraudCheckRequest2 = prepareRequestParam(fraudCheckRequestBody2,tokenMap);
		logRequest(fraudCheckRequest2, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest2, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());

			Assert.assertEquals(jsonPath.get("result"), "passed");
			Reporter.log("Fraud passed successfully");
			
			
		} else {
			Reporter.log(" <b>Fraud Check 2 Exception Response :</b> ");
			Reporter.log("Fraud Check 2 Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check 2 Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.PENDING_REGRESSION})
	public void testFraudCheckWithAAL_EIP_Fail(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Fraud Check Request For EIP Fail flow");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("====");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should be success returning cart id.");
		Reporter.log("Step 2: verify update cart with plan|Plan should be updated with response code 200 OK.");
		Reporter.log("Step 3: Verify Create quote response|Response code should be 200 OK.");
		Reporter.log("Step 4: Verify update Address response|response code should be 200 OK.");
		Reporter.log("Step 5: Verify update Payment response|response code should be 200 OK.");
		Reporter.log("Step 6: Verify Fraud check response after three unsuccessful attempts for Fail Scenario|response code should be 404 OK.");
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForFraud_EIP_Pass.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForFraud";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				
				Reporter.log("Cart Id is not null");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				Reporter.log("Ban present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				Reporter.log("MSISDN present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].items.device.sku"), apiTestData.getSku());
				Reporter.log("SKU present in request is same as one fetched from excel sheet");				
				
				Assert.assertEquals(jsonPath.get("cart.promotions[0].promoType"), "auto_pay");
				Reporter.log("Promotion Type is Auto Pay");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].linePriceSummary.type"), "FULL");
				Reporter.log("Line is FULL");
				
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		String requestBodyUpdateCart = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"UpdatePlanForFraudEIP.txt");
		operationName = "Update plan For EIP Fraud";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = cartApi.updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);
		

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			if (!jsonNode.isMissingNode()) 
			{
				jsonPath= new JsonPath(response.asString());
				
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.device.sku"), "Device SkuNumber is null");
				Reporter.log("Device SkuNumber is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");	
					
			}
		} else {
			failAndLogResponse(response, operationName);
	}
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforPaymemnt";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteforFraud_Pass.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
				tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="UpdateAddressCheck";
		String updateAdderssRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateAddressForFraud_Pass.txt");
		String updateAdderssRequest = prepareRequestParam(updateAdderssRequestBody,tokenMap);
		logRequest(updateAdderssRequest, operationName);
		response =quoteApiV5.updateAddress(apiTestData, updateAdderssRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			Assert.assertNotNull(jsonPath.get("address[1].addressType"), "address type is not present");
			Reporter.log("address type is present");
			
			
		} else {
			Reporter.log(" <b>Update Address Exception Response :</b> ");
			Reporter.log("Update Address Response status code : " + response.getStatusCode());
			Reporter.log("Update Address Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		
		operationName="UpdatePaymentFraud";
		String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdatePaymentForFraud_Pass.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
		logRequest(updatePaymentRequest, operationName);
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			Reporter.log("Payment posted successfully");
			tokenMap.put("transactionId",getPathVal(jsonNode, "fraudCheckPart.transactionId"));
			tokenMap.put("questionSetId",getPathVal(jsonNode, "fraudCheckPart.questionSetId"));
			tokenMap.put("questionId",getPathVal(jsonNode, "fraudCheckPart.questions.questionId"));
			tokenMap.put("choiceId",getPathVal(jsonNode,"fraudCheckPart.questions.choice[5].choiceid"));
			
		} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
     
		operationName="FraudCheck";
		String fraudCheckRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck_Pass.txt");
		String fraudCheckRequest = prepareRequestParam(fraudCheckRequestBody,tokenMap);
		logRequest(fraudCheckRequest, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			
			jsonPath=new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("questions.questionId"), "questionId is null");
			Reporter.log("questionId is not null");
			Assert.assertNotNull(jsonPath.get("questions.choice[5].choiceid"), "choiceid is null");
			Reporter.log("choiceid is not null");
			
			tokenMap.put("questionIdfc",getPathVal(jsonNode, "questions.questionId"));
			tokenMap.put("choiceIdfc",getPathVal(jsonNode,"questions.choice[3].choiceid"));
			
			 
			
		} else {
			Reporter.log(" <b>>Fraud Check Exception Response :</b> ");
			Reporter.log("Fraud Check  Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check  Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="FraudCheckFirst";
		String fraudCheckRequestBody1 = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "FraudCheck1_Pass.txt");
		String fraudCheckRequest1 = prepareRequestParam(fraudCheckRequestBody1,tokenMap);
		logRequest(fraudCheckRequest1, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest1, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("questions.questionId"), "questionId is null");
			Reporter.log("questionId is not null");
			Assert.assertNotNull(jsonPath.get("questions.choice[5].choiceid"), "choiceid is null");
			Reporter.log("choiceid is not null");
			
			tokenMap.put("questionId",getPathVal(jsonNode, "questions.questionId"));
			tokenMap.put("choiceId",getPathVal(jsonNode,"questions.choice[2].choiceid"));
			
		} else {
			Reporter.log(" <b>Fraud Check 1 Exception Response :</b> ");
			Reporter.log("Fraud Check 1 Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check 1 Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="FraudCheckSecond";
		String fraudCheckRequestBody2 = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck2_Pass.txt");
		String fraudCheckRequest2 = prepareRequestParam(fraudCheckRequestBody2,tokenMap);
		logRequest(fraudCheckRequest2, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest2, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		if (response != null && "404".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());

			Assert.assertEquals(jsonPath.get("code"), "404");
			Reporter.log("Fraud failed successfully");
			
			
		} else {
			Reporter.log(" <b>Fraud Check 2 Exception Response :</b> ");
			Reporter.log("Fraud Check 2 Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check 2 Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.PENDING_REGRESSION})
	public void testFraudCheckWithAAL_EIP_Pass(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Fraud Check Request for EIP Pass flow");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("====");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should be success returning cart id.");
		Reporter.log("Step 2: verify update cart with plan|Plan should be updated with response code should be 200 OK.");
		Reporter.log("Step 3: Verify Create quote response|Response code should be 200 OK.");
		Reporter.log("Step 4: Verify update Address response|response code should be 200 OK.");
		Reporter.log("Step 5: Verify update Payment response|response code should be 200 OK.");
		Reporter.log("Step 6: Verify Fraud check response after three successful attempts for Pass Scenario|response code should be 200 OK.");
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForFraud_EIP_Pass.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForFraud";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				
				Reporter.log("Cart Id is not null");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				Reporter.log("Ban present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				Reporter.log("MSISDN present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].items.device.sku"), apiTestData.getSku());
				Reporter.log("SKU present in request is same as one fetched from excel sheet");				
				
				Assert.assertEquals(jsonPath.get("cart.promotions[0].promoType"), "auto_pay");
				Reporter.log("Promotion Type is Auto Pay");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].linePriceSummary.type"), "FULL");
				Reporter.log("Line is FULL");
				
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		String requestBodyUpdateCart = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"UpdatePlanForFraudEIP.txt");
		operationName = "Update plan For EIP Fraud";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = cartApi.updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);
		

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			if (!jsonNode.isMissingNode()) 
			{
				jsonPath= new JsonPath(response.asString());
				
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.device.sku"), "Device SkuNumber is null");
				Reporter.log("Device SkuNumber is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");	
					
			}
		} else {
			failAndLogResponse(response, operationName);
			
	}
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforPaymemnt";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteforFraud_Pass.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
				tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		operationName="UpdateAddressCheck";
		String updateAdderssRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +" UpdateAddressForFraud_Pass.txt");
		String updateAdderssRequest = prepareRequestParam(updateAdderssRequestBody,tokenMap);
		logRequest(updateAdderssRequest, operationName);
		response =quoteApiV5.updateAddress(apiTestData, updateAdderssRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			
			
			Assert.assertNotNull(jsonPath.get("address[1].addressType"), "address type is null");
			Reporter.log("address type is not null");
			
		} else {
			Reporter.log(" <b>Update Address Exception Response :</b> ");
			Reporter.log("Update Address Response status code : " + response.getStatusCode());
			Reporter.log("Update Address Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		
		operationName="UpdatePaymentFraud";
		String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdatePaymentForFraud_Pass.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
		logRequest(updatePaymentRequest, operationName);
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			Reporter.log("Payment posted successfully");
			tokenMap.put("transactionId",getPathVal(jsonNode, "fraudCheckPart.transactionId"));
			tokenMap.put("questionSetId",getPathVal(jsonNode, "fraudCheckPart.questionSetId"));
			tokenMap.put("questionId",getPathVal(jsonNode, "fraudCheckPart.questions.questionId"));
			tokenMap.put("choiceId",getPathVal(jsonNode,"fraudCheckPart.questions.choice[5].choiceid"));
			
		} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
     
		operationName="FraudCheck";
		String fraudCheckRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck_Pass.txt");
		String fraudCheckRequest = prepareRequestParam(fraudCheckRequestBody,tokenMap);
		logRequest(fraudCheckRequest, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			
			jsonPath=new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("questions.questionId"), "questionId is null");
			Reporter.log("questionId is not null");
			Assert.assertNotNull(jsonPath.get("questions.choice[5].choiceid"), "choiceid is null");
			Reporter.log("choiceid is not null");
			
			tokenMap.put("questionIdfc",getPathVal(jsonNode, "questions.questionId"));
			tokenMap.put("choiceIdfc",getPathVal(jsonNode,"questions.choice[5].choiceid"));
			
			 
			
		} else {
			Reporter.log(" <b>>Fraud Check Exception Response :</b> ");
			Reporter.log("Fraud Check  Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check  Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="FraudCheckFirst";
		String fraudCheckRequestBody1 = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck1_Pass.txt");
		String fraudCheckRequest1 = prepareRequestParam(fraudCheckRequestBody1,tokenMap);
		logRequest(fraudCheckRequest1, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest1, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("questions.questionId"), "questionId is null");
			Reporter.log("questionId is not null");
			Assert.assertNotNull(jsonPath.get("questions.choice[5].choiceid"), "choiceid is null");
			Reporter.log("choiceid is not null");
			
			tokenMap.put("questionId",getPathVal(jsonNode, "questions.questionId"));
			tokenMap.put("choiceId",getPathVal(jsonNode,"questions.choice[5].choiceid"));
			
		} else {
			Reporter.log(" <b>Fraud Check 1 Exception Response :</b> ");
			Reporter.log("Fraud Check 1 Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check 1 Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="FraudCheckSecond";
		String fraudCheckRequestBody2 = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck2_Pass.txt");
		String fraudCheckRequest2 = prepareRequestParam(fraudCheckRequestBody2,tokenMap);
		logRequest(fraudCheckRequest2, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest2, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());

			Assert.assertEquals(jsonPath.get("result"), "passed");
			Reporter.log("Fraud passed successfully");
			
			
		} else {
			Reporter.log(" <b>Fraud Check 2 Exception Response :</b> ");
			Reporter.log("Fraud Check 2 Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check 2 Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.PENDING_REGRESSION})
	public void testFraudCheckWithAAL_FRP_Fail(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Fraud Check for FRP Fail flow");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("====");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should be success returning cart id.");
		Reporter.log("Step 2: verify update cart with plan|Plan should be updated with response code should be 200 OK.");
		Reporter.log("Step 3: Verify Create quote response|Response code should be 200 OK.");
		Reporter.log("Step 4: Verify update Address response|response code should be 200 OK.");
		Reporter.log("Step 5: Verify update Payment response|response code should be 200 OK.");
		Reporter.log("Step 6: Verify Fraud check response after three unsuccessful attempts for Fail Scenario|response code should be 404 OK.");

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForFraud_FRP_Pass.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		
		String operationName="createCartForFraud";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				
				Reporter.log("Cart Id is not null");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				Reporter.log("Ban present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				Reporter.log("MSISDN present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].items.device.sku"), apiTestData.getSku());
				Reporter.log("SKU present in request is same as one fetched from excel sheet");				
				
				Assert.assertEquals(jsonPath.get("cart.promotions[0].promoType"), "auto_pay");
				Reporter.log("Promotion Type is Auto Pay");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].linePriceSummary.type"), "FULL");
				Reporter.log("Line is FULL");
				
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforFraud";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteforFraud_Pass.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
				tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="UpdateAddressCheck";
		String updateAdderssRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateAddressForFraud_Pass.txt");
		String updateAdderssRequest = prepareRequestParam(updateAdderssRequestBody,tokenMap);
		logRequest(updateAdderssRequest, operationName);
		response=quoteApiV5.updateAddress(apiTestData, updateAdderssRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("address[1].addressType"), "address type is not present");
			Reporter.log("address type is present");
			
		} else {
			Reporter.log(" <b>Update Address Exception Response :</b> ");
			Reporter.log("Update Address Response status code : " + response.getStatusCode());
			Reporter.log("Update Address Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		
		operationName="UpdatePaymentFraud";
		String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdatePaymentForFraud_Pass.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
		logRequest(updatePaymentRequest, operationName);
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(response.asString());
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");

			
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			Reporter.log("Payment posted successfully");
			
			Assert.assertNotNull(jsonPath.get("fraudCheckPart.questions.choice[5].choiceText"), "choice text is not available");
			Reporter.log("choice text is available");
			
			tokenMap.put("transactionId",getPathVal(jsonNode, "fraudCheckPart.transactionId"));
			tokenMap.put("questionSetId",getPathVal(jsonNode, "fraudCheckPart.questionSetId"));
			tokenMap.put("questionId",getPathVal(jsonNode, "fraudCheckPart.questions.questionId"));
			tokenMap.put("choiceId",getPathVal(jsonNode,"fraudCheckPart.questions.choice[5].choiceid"));

			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			
			Reporter.log("Payment posted successfully");

			
		} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

     
		operationName="FraudCheck";
		String fraudCheckRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck_Pass.txt");
		String fraudCheckRequest = prepareRequestParam(fraudCheckRequestBody,tokenMap);
		logRequest(fraudCheckRequest, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			
			jsonPath=new JsonPath(response.asString());
			
			Assert.assertNotNull(jsonPath.get("questions.questionId"), "questionId is null");
			Reporter.log("questionId is not null");
			Assert.assertNotNull(jsonPath.get("questions.choice[5].choiceid"), "choiceid is null");
			Reporter.log("choiceid is not null");
			
			
			tokenMap.put("questionIdfc",getPathVal(jsonNode, "questions.questionId"));
			tokenMap.put("choiceIdfc",getPathVal(jsonNode,"questions.choice[3].choiceid"));
			
			 
			
		} else {
			Reporter.log(" <b>>Fraud Check Exception Response :</b> ");
			Reporter.log("Fraud Check  Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check  Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="FraudCheckFirst";
		String fraudCheckRequestBody1 = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck1_Pass.txt");
		String fraudCheckRequest1 = prepareRequestParam(fraudCheckRequestBody1,tokenMap);
		logRequest(fraudCheckRequest1, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest1, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("questions.questionId"), "questionId is null");
			Reporter.log("questionId is not null");
			Assert.assertNotNull(jsonPath.get("questions.choice[5].choiceid"), "choiceid is null");
			Reporter.log("choiceid is not null");
			
			tokenMap.put("questionId",getPathVal(jsonNode, "questions.questionId"));
			tokenMap.put("choiceId",getPathVal(jsonNode,"questions.choice[1].choiceid"));
			
		} else {
			Reporter.log(" <b>Fraud Check 1 Exception Response :</b> ");
			Reporter.log("Fraud Check 1 Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check 1 Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="FraudCheckSecond";
		String fraudCheckRequestBody2 = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck2_Pass.txt");
		String fraudCheckRequest2 = prepareRequestParam(fraudCheckRequestBody2,tokenMap);
		logRequest(fraudCheckRequest2, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest2, tokenMap);

		if (response != null && "404".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			
			Assert.assertEquals(jsonPath.get("code"), "404");
			Reporter.log("Fraud Failed successfully");
			
			
		} else {
			Reporter.log(" <b>Fraud Check 2 Exception Response :</b> ");
			Reporter.log("Fraud Check 2 Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check 2 Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
	}
	
	/**
	 * US476296  #EOS Update Quote API - FraudCheck end-point (/v5/quote/{quoteId}/fraudCheck)-AAL + FRP
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test",Group.AALFRAUDCHECK})
	public void testFraudCheckWithAAL_FRP_Pass(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Fraud Check for FRP Pass flow");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("====");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should be success returning cart id.");
		Reporter.log("Step 2: verify update cart with plan|Plan should be updated with response code should be 200 OK.");
		Reporter.log("Step 3: Verify Create quote response|Response code should be 200 OK.");
		Reporter.log("Step 4: Verify update Address response|response code should be 200 OK.");
		Reporter.log("Step 5: Verify update Payment response|response code should be 200 OK.");
		Reporter.log("Step 6: Verify Fraud check response after three successful attempts for Pass Scenario|response code should be 200 OK.");

		
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartForFraud_FRP_Pass.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());

		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForFraud";

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				
				Reporter.log("Cart Id is not null");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				Reporter.log("Ban present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				Reporter.log("MSISDN present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].items.device.sku"), apiTestData.getSku());

				Reporter.log("SKU present in request is same as one fetched from excel sheet");				
				
				Assert.assertEquals(jsonPath.get("cart.promotions[0].promoType"), "auto_pay");
				Reporter.log("Promotion Type is Auto Pay");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].linePriceSummary.type"), "FULL");
				Reporter.log("Line is FULL");
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		

		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforFraud";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "createQuoteforFraud_Pass.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
				tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		operationName="UpdateAddressCheck";
		String updateAdderssRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateAddressForFraud_Pass.txt");
		String updateAdderssRequest = prepareRequestParam(updateAdderssRequestBody,tokenMap);
		logRequest(updateAdderssRequest, operationName);
		response=quoteApiV5.updateAddress(apiTestData, updateAdderssRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("address[1].addressType"), "address type is not present");
			Reporter.log("address type is present");
			
		} else {
			Reporter.log(" <b>Update Address Exception Response :</b> ");
			Reporter.log("Update Address Response status code : " + response.getStatusCode());
			Reporter.log("Update Address Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		
		operationName="UpdatePaymentFraud";
		String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdatePaymentForFraud_Pass.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
		logRequest(updatePaymentRequest, operationName);
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			Reporter.log("Payment posted successfully");
			
			Assert.assertNotNull(jsonPath.get("fraudCheckPart.questions.choice[5].choiceText"), "choice text is not available");
			Reporter.log("choice text is available");
			
			tokenMap.put("transactionId",getPathVal(jsonNode, "fraudCheckPart.transactionId"));
			tokenMap.put("questionSetId",getPathVal(jsonNode, "fraudCheckPart.questionSetId"));
			tokenMap.put("questionId",getPathVal(jsonNode, "fraudCheckPart.questions.questionId"));
			tokenMap.put("choiceId",getPathVal(jsonNode,"fraudCheckPart.questions.choice[5].choiceid"));
			
		} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
     
		operationName="FraudCheck";
		String fraudCheckRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck_Pass.txt");
		String fraudCheckRequest = prepareRequestParam(fraudCheckRequestBody,tokenMap);
		logRequest(fraudCheckRequest, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			
			jsonPath=new JsonPath(response.asString());
			
			Assert.assertNotNull(jsonPath.get("questions.questionId"), "questionId is null");
			Reporter.log("questionId is not null");
			Assert.assertNotNull(jsonPath.get("questions.choice[5].choiceid"), "choiceid is null");
			Reporter.log("choiceid is not null");
			
			
			tokenMap.put("questionIdfc",getPathVal(jsonNode, "questions.questionId"));
			tokenMap.put("choiceIdfc",getPathVal(jsonNode,"questions.choice[5].choiceid"));
			
			 
			
		} else {
			Reporter.log(" <b>>Fraud Check Exception Response :</b> ");
			Reporter.log("Fraud Check  Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check  Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="FraudCheckFirst";
		String fraudCheckRequestBody1 = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck1_Pass.txt");
		String fraudCheckRequest1 = prepareRequestParam(fraudCheckRequestBody1,tokenMap);
		logRequest(fraudCheckRequest1, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest1, tokenMap);
		mapper = new ObjectMapper();
		jsonNode = mapper.readTree(response.asString());
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("questions.questionId"), "questionId is null");
			Reporter.log("questionId is not null");
			Assert.assertNotNull(jsonPath.get("questions.choice[5].choiceid"), "choiceid is null");
			Reporter.log("choiceid is not null");
			
			tokenMap.put("questionId",getPathVal(jsonNode, "questions.questionId"));
			tokenMap.put("choiceId",getPathVal(jsonNode,"questions.choice[5].choiceid"));
			
		} else {
			Reporter.log(" <b>Fraud Check 1 Exception Response :</b> ");
			Reporter.log("Fraud Check 1 Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check 1 Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="FraudCheckSecond";
		String fraudCheckRequestBody2 = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"FraudCheck2_Pass.txt");
		String fraudCheckRequest2 = prepareRequestParam(fraudCheckRequestBody2,tokenMap);
		logRequest(fraudCheckRequest2, operationName);
		response =quoteApiV5.fraudCheck(apiTestData, fraudCheckRequest2, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			
			Assert.assertEquals(jsonPath.get("result"), "passed");
			Reporter.log("Fraud passed successfully");
			
			
		} else {
			Reporter.log(" <b>Fraud Check 2 Exception Response :</b> ");
			Reporter.log("Fraud Check 2 Response status code : " + response.getStatusCode());
			Reporter.log("Fraud Check 2 Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
	}
	
	/**
	 * US519010  #EOS Update Quote API - Update Payment end-point (/v5/quote/{quoteId}/payment)-AAL + FRP
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test",Group.SHOP, Group.SPRINT })
	public void UpdatePaymentAALWith_NBYOD_FRP_US519010(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Update Payment for FRP flow");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("====");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should be success returning cart id.");
		Reporter.log("Step 2: verify update cart with plan and accessories|Plan and accessories should be updated with response code should be 200 OK.");
		Reporter.log("Step 3: Verify Create quote response|Response code should be 200 OK.");
		Reporter.log("Step 3: Verify Update quote response|Response code should be 200 OK.");
		Reporter.log("Step 4: Verify update Payment response|response code should be 200 OK.");

		
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartAAL_NBYOD_FRP.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
        tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForFraud";

		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				
				Reporter.log("Cart Id is not null");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				Reporter.log("Ban present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				Reporter.log("MSISDN present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].items.device.sku"), apiTestData.getSku());

				Reporter.log("SKU present in request is same as one fetched from excel sheet");				
				
				Assert.assertEquals(jsonPath.get("cart.promotions[0].promoType"), "auto_pay");
				Reporter.log("Promotion Type is Auto Pay");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].linePriceSummary.type"), "FULL");
				Reporter.log("Line is FULL");
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"UpdatePlanAccessoriesAPI_NBYOD_FRP_US516325.txt");
		   
		operationName = "Update FRP Plan and Accessories";

		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response =cartApi.updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		
        
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			
			
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode()==200);
				
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.simStarterKit.sku"), "simStarterKit sku is null");
				Reporter.log("SIM starter kit sku is present");
				
				/*Assert.assertNotNull(jsonPath.get("lines[0].items.accessories[0].sku"), "SkuNumber is null");
				Reporter.log("SkuNumber is not null");*/
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");
				
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		

		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforPayment";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteforAAL_NBYOD_FRP_US516326.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
				tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="updateQuoteforPayment";
		String updatequoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateQuoteForPayment_US519010.txt");
		String updatequotelatestRequest = prepareRequestParam(updatequoteRequestBody, tokenMap);
		logRequest(updatequotelatestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, updatequotelatestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				
				
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		
		operationName="UpdatePayment";
		String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"updatePayment_US519010.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
		logRequest(updatePaymentRequest, operationName);
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			Reporter.log("Payment posted successfully");
		
			
			} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
    }
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test",Group.SHOP, Group.SPRINT })
	public void UpdatePaymentAALWith_NBYOD_EIP_US519010(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Update Payment for EIP flow");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("====");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should be success returning cart id.");
		Reporter.log("Step 2: verify update cart with plan|Plan should be updated with response code should be 200 OK.");
		Reporter.log("Step 3: Verify Create quote response|Response code should be 200 OK.");
		Reporter.log("Step 3: Verify Update quote response|Response code should be 200 OK.");
		Reporter.log("Step 4: Verify update Payment response|response code should be 200 OK.");

		
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartAPI_NBYOD_EIP_US516325.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
        tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForFraud";

		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				
				Reporter.log("Cart Id is not null");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				Reporter.log("Ban present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				Reporter.log("MSISDN present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].items.device.sku"), apiTestData.getSku());

				Reporter.log("SKU present in request is same as one fetched from excel sheet");				
				
				Assert.assertEquals(jsonPath.get("cart.promotions[0].promoType"), "auto_pay");
				Reporter.log("Promotion Type is Auto Pay");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].linePriceSummary.type"), "FULL");
				Reporter.log("Line is FULL");
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"UpdatePlan_US519010.txt");
		   
		operationName = "Update FRP Plan";

		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response =cartApi.updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		
        
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			
			
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode()==200);
				
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");
				
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		

		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforPayment";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteforAAL_NBYOD_FRP_US516326.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
				tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		
		operationName="updateQuoteforPayment";
		String updatequoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateQuoteForPayment_EIP_US519010.txt");
		String updatequotelatestRequest = prepareRequestParam(updatequoteRequestBody, tokenMap);
		logRequest(updatequotelatestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, updatequotelatestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				
				
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		
		operationName="UpdatePayment";
		String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"updatePayment_EIP_US519010.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
		logRequest(updatePaymentRequest, operationName);
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			Reporter.log("Payment posted successfully");
		
			
			} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
     }
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test",Group.SHOP, Group.SPRINT })
	public void UpdatePaymentAALWith_BYOD_FRP_US519010(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Update Payment for BYOD FRP flow");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("====");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should be success returning cart id.");
		Reporter.log("Step 2: verify update cart with plan and accessories|Plan and accessories should be updated with response code should be 200 OK.");
		Reporter.log("Step 3: Verify Create quote response|Response code should be 200 OK.");
		Reporter.log("Step 3: Verify Update quote response|Response code should be 200 OK.");
		Reporter.log("Step 4: Verify update Payment response|response code should be 200 OK.");

		
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"createCartAPI_BYOD_FRP_US516325.txt");
		CartApiV5 cartApi = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
        tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForFraud";

		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				
				Reporter.log("Cart Id is not null");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
				Reporter.log("Ban present in request is same as one fetched from excel sheet");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
				Reporter.log("MSISDN present in request is same as one fetched from excel sheet");			
				
				Assert.assertEquals(jsonPath.get("cart.promotions[0].promoType"), "auto_pay");
				Reporter.log("Promotion Type is Auto Pay");
				
				Assert.assertEquals(jsonPath.get("cart.lines[0].linePriceSummary.type"), "FULL");
				Reporter.log("Line is FULL");
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"UpdatePlanAccessoriesAPI_NBYOD_FRP_US516325.txt");
		   
		operationName = "Update FRP Plan and Sim";

		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response =cartApi.updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		
        
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			
			
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode()==200);
				
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.accessories[0].sku"), "simStarterKit sku is null");
				Reporter.log("SIM starter kit sku is present");
				
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");
				
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		operationName="createQuoteforPayment";
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"createQuoteforAAL_BYOD_FRP_US516326.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
				tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="updateQuoteforPayment";
		String updatequoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateQuoteForPayment_US519010.txt");
		String updatequotelatestRequest = prepareRequestParam(updatequoteRequestBody, tokenMap);
		logRequest(updatequotelatestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, updatequotelatestRequest,tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
				Reporter.log("Order Id is not null");
				Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
				Reporter.log("Line Id is not null");
				
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				
				
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		
		operationName="UpdatePayment";
		String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"updatePayment_BYOD_FRP_US519010.txt");
		String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
		logRequest(updatePaymentRequest, operationName);
		response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			jsonPath=new JsonPath(response.asString());
			Assert.assertTrue(response.statusCode()==200);
			Reporter.log("Status code is 200");
			
			Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
			Reporter.log("Payment posted successfully");
		
			
			} else {
			Reporter.log(" <b>Update Payment Exception Response :</b> ");
			Reporter.log("Update Payment Response status code : " + response.getStatusCode());
			Reporter.log("Update Payment Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
    }
	
	/**
	 * UserStory# Description:US544968:myTMO : Technical Story - Populate quoteItemCollection in createQuote Response for BYOD scenario in AAL transaction
	 * 
	 * @param data:qlab02
	 * @param myTmoDataCOD-17
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test",Group.SHOP, Group.SPRINT })
	public void testquoteItemCollectionWithDeviceInCreateQuote(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: test Quote Item collection with device ");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Test create cart with device in cart| CartId should be created and Response should be 200 OK");
		Reporter.log("Step 2: Test update plan |Plan id should not be null and response should be 200");
		Reporter.log("Step 3: Test Create Quote|Quote Id should not be null and response should be 200");
		Reporter.log("Step 6: Verify Quote Item collection in create quote  |verify device in quote item collection");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
	    Response response=createQuoteThroughFRPWithDeviceForAddALineTransaction(data,apiTestData);
	     if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
          ObjectMapper mapper = new ObjectMapper();
          JsonNode jsonNode = mapper.readTree(response.asString());
          if(!jsonNode.isMissingNode())
          {
                jsonPath= new JsonPath(response.asString());
                Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
                Reporter.log("Quote Id is not null");
                
                Assert.assertEquals(jsonPath.get("quoteItemCollection.quoteItems[0].offer.productType"),"DEVICE","device is not present in quote item collection");
                Reporter.log("device is present in quote item collection");

                     }
		   } else {
		          Reporter.log(" <b>Exception Response :</b> ");
		          Reporter.log("Create Quote Response status code : " + response.getStatusCode());
		          Reporter.log("Create Quote Response : " + response.body().asString());
		      }
         }
	
		@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test",Group.SHOP, Group.SPRINT })
		public void testquoteItemCollectionWithSIMInCreateQuote(ControlTestData data, ApiTestData apiTestData) throws Exception {
			Reporter.log("TestName: test Quote Item collection with device ");
			Reporter.log("Data Conditions:MyTmo registered msisdn.");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("Step 1: Test create cart with sim in cart| CartId should be created and Response should be 200 OK");
			Reporter.log("Step 2: Test update plan |Plan id should not be null and response should be 200");
			Reporter.log("Step 3: Test Create Quote|Quote Id should not be null and response should be 200");
			Reporter.log("Step 6: Verify Quote Item collection in create quote  |verify sim starter kit in quote item collection");
			Reporter.log("================================");
			Reporter.log("Actual Result:");
			
		    Response response=createQuoteWithSimForAddALineTransaction(data,apiTestData);
		     if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
	             ObjectMapper mapper = new ObjectMapper();
	          JsonNode jsonNode = mapper.readTree(response.asString());
	          if(!jsonNode.isMissingNode())
	          {
	                jsonPath= new JsonPath(response.asString());
	                Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
	                Reporter.log("Quote Id is not null");
	                
	                Assert.assertEquals(jsonPath.get("quoteItemCollection.quoteItems[0].quoteItemId"), "SimStarterKit","quote item id is not sim");
	                Reporter.log("sim starter kit is present in quote item collection");
	
	                     }
	   } else {
	          Reporter.log(" <b>Exception Response :</b> ");
	          Reporter.log("Create Quote Response status code : " + response.getStatusCode());
	          Reporter.log("Create Quote Response : " + response.body().asString());
	        
	   }
		}
		
		/**
		 * US516329# Description:Update Quote with NBYOD FRP flow
		 * @param data
		 * @param apiTestData
		 * @throws Exception
		 */
		
		@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.SHOP,Group.APIREG})
		public void testUpdateQuoteWithAAL_NBYOD_FRP_US516329(ControlTestData data, ApiTestData apiTestData) throws Exception {
			Reporter.log("TestName: Update Quote for AAL NBYOD FRP");
			Reporter.log("Data Conditions:MyTmo registered misdn.");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");
			Reporter.log("Step 2: Verify create cart services response code|Response code should be 200 OK.");
			Reporter.log("Step 3: Update Cart should Update specific items to cart request for specific cart id");
			Reporter.log("Step 4: Verify Update cart of plan and sim are updated|Response code should be 200 OK.");
			Reporter.log("Step 4: Verify create Quote response|Response code should be 200 OK.");
			Reporter.log("Step 4: Verify update Quote response|Response code should be 200 OK.");
			Reporter.log("================================");
			Reporter.log("Actual Result:");
			
			tokenMap = new HashMap<String, String>();
			CartApiV5Tests art=new CartApiV5Tests();
			art.createCartAAL_NBYOD_FRPUS516325(apiTestData, tokenMap);
			String operationName = "Update FRP Plan";
			String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "updatePlan.txt");
			String updatedRequest = prepareRequestParam(requestBody, tokenMap);
			Response response =art.updateCart(apiTestData, updatedRequest, tokenMap);
			logRequest(updatedRequest, operationName);
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				
				if (!jsonNode.isMissingNode()) {
					Assert.assertTrue(response.statusCode()==200);	
					jsonPath= new JsonPath(response.asString());
					
					Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
					Reporter.log("Cart Id is not null");
					
					Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"),"Plan id is Null");
					Reporter.log("Plan Id is not null");
				}
			} else {
				failAndLogResponse(response, operationName);
			}
			
			
	        QuoteApiV5 quoteApiV5=new QuoteApiV5();
			operationName="createQuoteforNBYODFRP";
			String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"testCreateQuoteWithAAL.txt");
			String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		
			logRequest(latestRequest, operationName);
			response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);
			
			System.out.println("response is coming");
			
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if(!jsonNode.isMissingNode())
				{
					jsonPath= new JsonPath(response.asString());
					Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
					Reporter.log("Quote Id is not null");
					Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
					Reporter.log("Order Id is not null");
					tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
					tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
					tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
					tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
					
					
				}
			} else {
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Create Quote Response status code : " + response.getStatusCode());
				Reporter.log("Create Quote Response : " + response.body().asString());
				failAndLogResponse(response, operationName);
			}
			
			operationName="UpadateQuoteforNBYODFRP";
		    String UpdatequoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateQuoteforAAL_NBYOD_FRP.txt");
		    String latestRequest1 = prepareRequestParam(UpdatequoteRequestBody, tokenMap);
		
			logRequest(latestRequest1, operationName);
			response = quoteApiV5.updateQuote(apiTestData, latestRequest1, tokenMap);
			
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if(!jsonNode.isMissingNode())
				{
					jsonPath= new JsonPath(response.asString());
					Assert.assertEquals(jsonPath.get("status.statusMessage"), "quote updated successfully");
					
					
			}
			} else {
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Update Quote Response status code : " + response.getStatusCode());
				Reporter.log("Update Quote Response : " + response.body().asString());
				failAndLogResponse(response, operationName);
			}
		}
		
		/**
		 * US516329# Description:Update Quote with NBYOD EIP flow
		 * @param data
		 * @param apiTestData
		 * @throws Exception
		 */
		
		@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.SHOP,Group.APIREG})
		public void testUpdateQuoteWithAAL_NBYOD_EIP_US516329(ControlTestData data, ApiTestData apiTestData) throws Exception {
			Reporter.log("TestName: Update Quote of AAL flow with NBYOD EIP");
			Reporter.log("Data Conditions:MyTmo registered misdn.");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");
			Reporter.log("Step 2: Verify create cart services response code|Response code should be 200 OK.");
			Reporter.log("Step 3: Update Cart should Update specific items to cart request for specific cart id");
			Reporter.log("Step 4: Verify Update cart of plan and sim are updated|Response code should be 200 OK.");
			Reporter.log("Step 4: Verify create Quote response|Response code should be 200 OK.");
			Reporter.log("Step 4: Verify update Quote response|Response code should be 200 OK.");
			Reporter.log("================================");
			Reporter.log("Actual Result:");
			tokenMap = new HashMap<String, String>();
			CartApiV5Tests art=new CartApiV5Tests();
			art.createCartAAL_NBYOD_EIP_US516325(apiTestData, tokenMap);
			
			String operationName = "Update EIP Plan";
			String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"UpdatePlan.txt");
			
			
			String updatedRequest = prepareRequestParam(requestBody, tokenMap);
			Response response = art.updateCart(apiTestData, updatedRequest, tokenMap);
			logRequest(updatedRequest, operationName);
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				
				if (!jsonNode.isMissingNode()) {
					Assert.assertTrue(response.statusCode()==200);	
					jsonPath= new JsonPath(response.asString());
					
					Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
					Reporter.log("Cart Id is not null");
					
		//			Assert.assertEquals(jsonPath.get("lines[0].items.plans[0].planId"),"TM1TI2");
				}
			} else {
				failAndLogResponse(response, operationName);
			}
			
            operationName="createQuoteforNBYODEIP";
			String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"testCreateQuoteWithAAL.txt");
			String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
			logRequest(latestRequest, operationName);
			QuoteApiV5 quoteApiV5=new QuoteApiV5();
			response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if(!jsonNode.isMissingNode())
				{
					jsonPath= new JsonPath(response.asString());
					Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
					Reporter.log("Quote Id is not null");
					Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
					Reporter.log("Order Id is not null");
					tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
					tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
					tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
					tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
					
				}
			} else {
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Create Quote Response status code : " + response.getStatusCode());
				Reporter.log("Create Quote Response : " + response.body().asString());
				failAndLogResponse(response, operationName);
			}
			operationName="UpadateQuoteforNBYODEIP";
		    String UpdatequoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateQuoteforAAL_NBYOD_EIP.txt");
			latestRequest = prepareRequestParam(UpdatequoteRequestBody, tokenMap);
		
			logRequest(latestRequest, operationName);
			response = quoteApiV5.updateQuote(apiTestData, latestRequest, tokenMap);
			
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if(!jsonNode.isMissingNode())
				{
					jsonPath= new JsonPath(response.asString());
					jsonPath= new JsonPath(response.asString());
					Assert.assertEquals(jsonPath.get("status.statusMessage"), "quote updated successfully");
					
					
					
				}
			} else {
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Create Quote Response status code : " + response.getStatusCode());
				Reporter.log("Create Quote Response : " + response.body().asString());
				failAndLogResponse(response, operationName);
			}
		}
		
		/**
		 * US516329# Description:Update Quote with BYOD FRP flow
		 * @param data
		 * @param apiTestData
		 * @throws Exception
		 */
		
		@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.SHOP,Group.APIREG })
		public void testUpdateQuoteWithAAL_BYOD_FRP_US516329(ControlTestData data, ApiTestData apiTestData) throws Exception {
			Reporter.log("TestName: update quote of AAL flow with BYOD FRP");
			Reporter.log("Data Conditions:MyTmo registered misdn.");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");
			Reporter.log("Step 2: Verify create cart services response code|Response code should be 200 OK.");
			Reporter.log("Step 3: Update Cart should Update specific items to cart request for specific cart id");
			Reporter.log("Step 4: Verify Update cart of plan and sim are updated|Response code should be 200 OK.");
			Reporter.log("Step 4: Verify create Quote response|Response code should be 200 OK.");
			Reporter.log("Step 4: Verify update Quote response|Response code should be 200 OK.");
			Reporter.log("================================");
			Reporter.log("Actual Result:");
			
			tokenMap = new HashMap<String, String>();
			CartApiV5Tests art=new CartApiV5Tests();
			art.createCartAAL_BYOD_FRP_US516325(apiTestData, tokenMap);

			String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART +"UpdatePlanSimAPI_BYOD_FRP.txt");
			String operationName = "Update FRP Plan and Sim";

			String updatedRequest = prepareRequestParam(requestBody, tokenMap);
			Response response = art.updateCart(apiTestData, updatedRequest, tokenMap);
			logRequest(updatedRequest, operationName);
			
	        
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				
				
				
				if (!jsonNode.isMissingNode()) {
					Assert.assertTrue(response.statusCode()==200);
					
					jsonPath= new JsonPath(response.asString());
					Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
					Reporter.log("Cart Id is not null");
					
					Assert.assertNotNull(jsonPath.get("lines[0].items.simStarterKit.sku"), "SIM starter kit sku is not present");
					Reporter.log("SIM starter kit sku is present");
					
					Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
					Reporter.log("Plan Id is not null");
					
				}
			} else {
				failAndLogResponse(response, operationName);
			}

			QuoteApiV5 quoteApiV5=new QuoteApiV5();
			operationName="createQuoteforBYODFRP";
			String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"testCreateQuoteWithAAL.txt");
			String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
			logRequest(latestRequest, operationName);
		    response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if(!jsonNode.isMissingNode())
				{
					jsonPath= new JsonPath(response.asString());
					Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
					Reporter.log("Quote Id is not null");
					Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
					Reporter.log("Order Id is not null");
					tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
					tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
					tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
					tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
					
				}
			} else {
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Create Quote Response status code : " + response.getStatusCode());
				Reporter.log("Create Quote Response : " + response.body().asString());
				failAndLogResponse(response, operationName);
			}
			
			operationName="UpadateQuoteforBYODFRP";
		    String UpdatequoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateQuoteforAAL_NBYOD_FRP.txt");
			latestRequest = prepareRequestParam(UpdatequoteRequestBody, tokenMap);
		
			logRequest(latestRequest, operationName);
			response = quoteApiV5.updateQuote(apiTestData, latestRequest, tokenMap);
			
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if(!jsonNode.isMissingNode())
				{
					jsonPath= new JsonPath(response.asString());
					jsonPath= new JsonPath(response.asString());
					Assert.assertEquals(jsonPath.get("status.statusMessage"), "quote updated successfully");
					
					}
			} else {
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Create Quote Response status code : " + response.getStatusCode());
				Reporter.log("Create Quote Response : " + response.body().asString());
				failAndLogResponse(response, operationName);
			}
		}
		/**
		 * US579393# Description:Update Payment for TradeIn EIP Payoff
		 * @param data
		 * @param apiTestData
		 * @throws Exception
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { "quoteApiV5Test","updatePayment",Group.SHOP, Group.PROGRESSION})
		public void testUpdatePaymentForTradeInUpGradeFlow(ControlTestData data, ApiTestData apiTestData) throws Exception {
			Reporter.log("TestName: Update Payment");
			Reporter.log("Data Conditions:MyTmo registered misdn.");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
			Reporter.log("Step 2: Update Cart with TradeIn should return for the specific user based on the Alert Type");
			Reporter.log("Step 3: Verify Create Quote response code|Response code should be 200 OK.");
			Reporter.log("Step 4: Verify Update Quote response code|Response code should be 200 OK.");
			Reporter.log("Step 5: Verify Update Payment response code|Response code should be 200 OK");
			
			Reporter.log("================================");
			Reporter.log("Actual Result:");

			String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "createCartForPaymentEIP.txt");
			CartApiV5 cartApi = new CartApiV5();
			tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("transactiontype","UPGRADE");
			tokenMap.put("sku", apiTestData.getSku());
		    //tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
			String operationName="createCartForEIPPayoffPayment";
			String updatedRequest = prepareRequestParam(requestBody, tokenMap);
			logRequest(updatedRequest, operationName);
			Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);
			
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) 
				{	
					jsonPath= new JsonPath(response.asString());
					Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
					
					Reporter.log("Cart Id is not null");
					
					Assert.assertEquals(jsonPath.get("cart.lines[0].ban"), apiTestData.getBan());
					Reporter.log("Ban present in request is same as one fetched from excel sheet");
					
					Assert.assertEquals(jsonPath.get("cart.lines[0].msisdn"), apiTestData.getMsisdn());
					Reporter.log("MSISDN present in request is same as one fetched from excel sheet");
					
					Assert.assertEquals(jsonPath.get("cart.lines[0].items.device.sku"), apiTestData.getSku());
					Reporter.log("SKU present in request is same as one fetched from excel sheet");				
					
					Assert.assertNotNull(jsonPath.get("cart.lines[0].linePriceSummary.type"), "Line is not FULL");
					Reporter.log("Line is FULL");
					
					tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
					tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
				}
			} else {
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Cart Response status code : " + response.getStatusCode());
				Reporter.log("Cart Response : " + response.body().asString());
				failAndLogResponse(response, operationName);
			}
			
			String requestBodyUpdateCart = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE +"UpdateCartForPaymentWithEIP_Payoff.txt");
			operationName = "Update Cart For Payment";
			String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
			logRequest(updatedRequestUpdateCart, operationName);
			response = cartApi.updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);
			
			
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				
				if (!jsonNode.isMissingNode()) 
				{
					jsonPath= new JsonPath(response.asString());
					
					Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
					Reporter.log("Cart Id is not null");
					
					//Assert.assertNotNull(jsonPath.get("lines[0].items.accessories[0].sku"), "Device SkuNumber is null");
					//Reporter.log("Device SkuNumber is not null");
					
					Assert.assertNotNull(jsonPath.get("lines[0].items.financePayoff"), "finance payoff is null");
					Reporter.log("Finance payoff is not null");
					
					Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
					Reporter.log("Plan Id is not null");	
					
					Assert.assertTrue(response.statusCode()==200);
					
					
					Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
					Reporter.log("Cart Id is not null");		
				}
			} else {
				failAndLogResponse(response, operationName);
		}    
			QuoteApiV5 quoteApiV5=new QuoteApiV5();
			operationName="createQuoteforPaymemnt";
			String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "createQuoteforPayment_EIP_Payoff.txt");
			String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
			logRequest(latestRequest, operationName);
			response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);
			
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if(!jsonNode.isMissingNode())
				{
					jsonPath= new JsonPath(response.asString());
					Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
					Reporter.log("Quote Id is not null");
					Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
					Reporter.log("Order Id is not null");
					Assert.assertNotNull(jsonPath.get("	lines[0].lineId"), "Line Id is null");
					Reporter.log("Line Id is not null");
					tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
					tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
					tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
					tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
				}
			} else {
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Create Quote Response status code : " + response.getStatusCode());
				Reporter.log("Create Quote Response : " + response.body().asString());
				failAndLogResponse(response, operationName);
			}
			
			operationName="UpdateQuoteForPayment";
			String UpdateQuoteForPaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "UpdateQuoteForPayment_EIP_Payoff.txt");
			String UpdateQuoteForPaymentRequest =prepareRequestParam(UpdateQuoteForPaymentRequestBody,tokenMap);
			logRequest(UpdateQuoteForPaymentRequest,operationName);
			response=quoteApiV5.updateQuote(apiTestData,UpdateQuoteForPaymentRequest,tokenMap);
			
			
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) 
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("status.statusMessage"), "quote updated successfully");
				Reporter.log("quote updated successfully");
				logSuccessResponse(response, operationName);
			
			} 
			else {
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Update Quote Response status code : " + response.getStatusCode());
				Reporter.log("Update Quote Response : " + response.body().asString());
				failAndLogResponse(response, operationName);
			}
			
			operationName="UpdatePaymentCheck";
			String updatePaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "updatePayment_EIP_Payoff.txt");
			String updatePaymentRequest = prepareRequestParam(updatePaymentRequestBody,tokenMap);
			logRequest(updatePaymentRequest, operationName);
			response =quoteApiV5.updatePayment(apiTestData, updatePaymentRequest, tokenMap);
		
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				jsonPath=new JsonPath(response.asString());
				Assert.assertTrue(response.statusCode()==200);
				Reporter.log("Status code is 200");
				Assert.assertEquals(jsonPath.get("status.statusMessage"), "payment posted successfully");
				Reporter.log("Payment posted successfully");
				
			} else {
				Reporter.log(" <b>Update Payment Exception Response :</b> ");
				Reporter.log("Update Payment Response status code : " + response.getStatusCode());
				Reporter.log("Update Payment Response : " + response.body().asString());
				failAndLogResponse(response, operationName);
			}

		}

	}