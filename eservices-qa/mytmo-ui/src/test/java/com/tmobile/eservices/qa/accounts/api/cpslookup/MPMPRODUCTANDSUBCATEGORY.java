package com.tmobile.eservices.qa.accounts.api.cpslookup;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;

import io.restassured.response.Response;

public class MPMPRODUCTANDSUBCATEGORY {

	public static List<String> getallratePlans(String excel) {
		List<String> misdinpassword = new ArrayList<>();
		try {

			Workbook workbook;
			FileInputStream excelFile = new FileInputStream(new File(excel));
			if (excel.contains("xlsx")) {
				workbook = new XSSFWorkbook(excelFile);
			} else {
				workbook = new HSSFWorkbook(excelFile);
			}

			org.apache.poi.ss.usermodel.Sheet datatypeSheet = workbook.getSheetAt(0);

			Iterator<Row> iterator = datatypeSheet.iterator();

			while (iterator.hasNext()) {

				Row currentRow = iterator.next();

				if (currentRow.getCell(0) != null && currentRow.getCell(1) != null && currentRow.getCell(2) != null) {

					String WPCSOC = currentRow.getCell(0).getStringCellValue();
					String category = currentRow.getCell(1).getStringCellValue();
					String subcategory = currentRow.getCell(2).getStringCellValue();

					if (!WPCSOC.trim().equals("")) {
						if (!WPCSOC.equalsIgnoreCase("loginEmailOrPhone")) {

							misdinpassword.add(WPCSOC.trim() + "/" + category.trim() + "/" + subcategory.trim());

						}
					}
				}
			}

			excelFile.close();
			workbook.close();
		} catch (Exception e) {

		}
		return misdinpassword;
	}

	@Test(groups = "mpm")
	public void TestMPM() throws Exception {
		String FILE_NAME = System.getProperty("user.dir") + "/src/test/resources/testdata/WPC1.xlsx";

		List<String> misdinpawords = getallratePlans(FILE_NAME);
		Iterator<String> iterator = misdinpawords.iterator();
		Response response;
		List<String> Al = new ArrayList<String>();
		int a = 0;
		while (iterator.hasNext()) {

			String misdpwd = iterator.next();
			String rateplan = misdpwd.split("/")[0];
			String productcategory = misdpwd.split("/")[1];
			String productsubcategory = misdpwd.split("/")[2];

			RestService restService = new RestService("", "https://core.op.api.internal.t-mobile.com:443");

			restService.setContentType("application/json");
			restService.addHeader("Authorization",
					"Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlVGSlBSRVJCVkMweU1ERTUifQ.eyJpYXQiOjE1NzE3ODA2OTYsImV4cCI6MTU3MTgxNjY5NiwiaXNzIjoiaHR0cHM6Ly9icmFzcy5hY2NvdW50LnQtbW9iaWxlLmNvbSIsImF1ZCI6Ik1ZVE1PIiwibm9uY2UiOiIyMDY4MDI1Mjg1IiwiQVQiOiIwMi5VU1IuNTZUZTdWSmlkdU15dmpEbEIiLCJzdWIiOiJVLWRhYjVjZDYxLTA3MDUtNGVlMy05OTc2LWIzNzE3YTU0OTFlZiIsImFjciI6ImxvYTIiLCJhbXIiOlsicGFzc3dvcmQiLCJzZWN1cml0eV9xdWVzdGlvbiJdLCJ1c24iOiI3Nzc3NDMxMzM1MjJmNWIyIiwiZW50X3R5cGUiOiJkaXJlY3QiLCJlbnQiOnsiYWNjdCI6W3siciI6IkFPIiwiaWQiOiI5MTg3MjEyNTYiLCJ0c3QiOiJJUiIsImxpbmVfY291bnQiOjEwLCJsaW5lcyI6W3sicGhudW0iOiIyMDY4MDI1Mjg1IiwiciI6IkQiLCJkaXNhYmxlZCI6dHJ1ZX1dfV19fQ.gpQXbd3IEP6E73qd3WKK7ZyNElrLXrc701bOIkSSJbQEGippRdqTdLtsPe1c3vAtezITSyTvBxwHgVRR-tc2ddl4n9yJbhhQ3Ww1MkXYq0N1lOoG0mSiCw2YLUsOxD-y8giwTBt8ucZ51bIXW--Eh5W0dQamuGuLJP1twsqsbtJ_pAla83_P33dKGAwNIWoyvj7blAUhGGhB557Yy19XGktsXjQNotpvkJC6ERjHrRC2s9blpEhPGe3N1cG4q-hJeGMO_VWE_B5uHnX4tMA1aAGZ36XS-VrNvAkNJOCVLOAsFZp2y_eIc8IyaACRXXeiRmjUKs4ML8g_XGuHdwHYrg");
			restService.addHeader("X-Auth-Originator",
					"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlVGSlBSRVJCVkMweU1ERTUifQ.eyJpYXQiOjE1NzE3ODA2OTYsImV4cCI6MTU3MTgxNjY5NiwiaXNzIjoiaHR0cHM6Ly9icmFzcy5hY2NvdW50LnQtbW9iaWxlLmNvbSIsImF1ZCI6Ik1ZVE1PIiwibm9uY2UiOiIyMDY4MDI1Mjg1IiwiQVQiOiIwMi5VU1IuNTZUZTdWSmlkdU15dmpEbEIiLCJzdWIiOiJVLWRhYjVjZDYxLTA3MDUtNGVlMy05OTc2LWIzNzE3YTU0OTFlZiIsImFjciI6ImxvYTIiLCJhbXIiOlsicGFzc3dvcmQiLCJzZWN1cml0eV9xdWVzdGlvbiJdLCJ1c24iOiI3Nzc3NDMxMzM1MjJmNWIyIiwiZW50X3R5cGUiOiJkaXJlY3QiLCJlbnQiOnsiYWNjdCI6W3siciI6IkFPIiwiaWQiOiI5MTg3MjEyNTYiLCJ0c3QiOiJJUiIsImxpbmVfY291bnQiOjEwLCJsaW5lcyI6W3sicGhudW0iOiIyMDY4MDI1Mjg1IiwiciI6IkQiLCJkaXNhYmxlZCI6dHJ1ZX1dfV19fQ.gpQXbd3IEP6E73qd3WKK7ZyNElrLXrc701bOIkSSJbQEGippRdqTdLtsPe1c3vAtezITSyTvBxwHgVRR-tc2ddl4n9yJbhhQ3Ww1MkXYq0N1lOoG0mSiCw2YLUsOxD-y8giwTBt8ucZ51bIXW--Eh5W0dQamuGuLJP1twsqsbtJ_pAla83_P33dKGAwNIWoyvj7blAUhGGhB557Yy19XGktsXjQNotpvkJC6ERjHrRC2s9blpEhPGe3N1cG4q-hJeGMO_VWE_B5uHnX4tMA1aAGZ36XS-VrNvAkNJOCVLOAsFZp2y_eIc8IyaACRXXeiRmjUKs4ML8g_XGuHdwHYrg");
			restService.addHeader("Accept", "application/json");
			restService.addHeader("activity-id", "321321312");
			restService.addHeader("X-B3-TraceId", "222");
			restService.addHeader("X-B3-SpanId", "222");
			restService.addHeader("application", "222");
			restService.addHeader("channel-id", "222");
			restService.addHeader("consumertype", "222");
			restService.addHeader("Postman-Token", "eef93768-ed14-4076-afe5-9cbbf947a8f2");

			String rateplans = rateplan;
			String url = "products/v3/rateplans?soc=" + rateplans + "&channel=MYT";

			try {
				response = restService.callService(url, RestCallType.GET);

				if (response.statusCode() == 200 && response.jsonPath().getInt("totalResultCount") == 0) {

					Al.add(rateplans);
				} else {

					System.out.println(rateplan + productcategory + productsubcategory);
					if (response.statusCode() == 200 && response.jsonPath().getString("rateplanSpecs.name") != null) {
						if ((response.jsonPath().getString("rateplanSpecs.classification.productCategory")
								.contains(productcategory))
								&& response.jsonPath().getString("rateplanSpecs.classification.productSubCategory")
										.contains(productsubcategory)) {

							a = a + 1;

						}

						else {
							Reporter.log(
									"FAIL =====================================================================================");
							Reporter.log("Expected         " + rateplan + "                " + productcategory
									+ "                    " + productsubcategory);
							Reporter.log("Actual           " + rateplan + "                "
									+ response.jsonPath().getString("rateplanSpecs.classification.productCategory")
											.toString()
									+ "                     " + response.jsonPath()
											.getString("rateplanSpecs.classification.productSubCategory").toString());
						}
					} else {
						Reporter.log(rateplan + "no response");
					}
				}
			}

			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		if (Al != null) {
			Reporter.log(Al.toString());
			System.out.println(a);
		}

	}
}