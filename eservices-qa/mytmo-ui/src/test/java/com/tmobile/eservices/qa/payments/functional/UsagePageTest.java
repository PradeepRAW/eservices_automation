/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsSelectionPage;
import com.tmobile.eservices.qa.pages.payments.UsageDetailsPage;
import com.tmobile.eservices.qa.pages.payments.UsagePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author sam
 *
 */
public class UsagePageTest extends PaymentCommonLib {

	/*	*//**
			 * US306453 Usage Modernization: HTML Integration - Usage Summary
			 * 
			 * @param data
			 * @param myTmoData
			 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyUsagePageElements(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: US306453 Usage Modernization: HTML Integration - Usage Summary");
		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. Verify header is displayed | Header is displayed");
		Reporter.log("6. Verify cycle is displayed | Cycle is displayed");
		Reporter.log("7. Verify View by Category is displayed | View by Category tab is displayed");
		Reporter.log("8. Verify View by Line is displayed | View by Line tab is displayed");
		Reporter.log("8. Verify Table with details is displayed | Table with details is displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		UsagePage usage = navigateToUsagePage(myTmoData);
		usage.verifyAllPageElements();
	}

	/**
	 * US304043 Usage Modernization: Usage Details Download records
	 * 
	 * US334079 Download Usage Records CTA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "covered" })
	public void testUserCanDownloadUsageRecords(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: US304043 Usage Modernization: Usage Details Download records");
		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click view all usage details | Call Record details page should display");
		Reporter.log("6.Click Download Usage Records | Records should be downloaded");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		UsagePage usagePage = navigateToUsagePage(myTmoData);
		usagePage.clickOptionViewByCategory("Messages");
		usagePage.clickLineViewByCategory();

		UsageDetailsPage usageDeatilsPage = new UsageDetailsPage(getDriver());
		usageDeatilsPage.verifyUsageDetailsPage();
		usageDeatilsPage.verifyDownloadUsageRecordsCTA();
	}

	/**
	 * US304040 Usage Modernization: Usage Details Bill Cycle Selector
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUsageBillCycle(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: US304040 Usage Modernization: Usage Details Bill Cycle Selector");
		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click view all usage details | Call Record details page should display");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		UsagePage usagePage = navigateToUsagePage(myTmoData);

		String selectedBillCycleMonth = usagePage.selectBillCycle("Mar");
		usagePage.clickOptionViewByCategory("Messages");
		usagePage.clickLineViewByCategory();

		UsageDetailsPage usageDeatilsPage = new UsageDetailsPage(getDriver());
		usageDeatilsPage.verifyUsageDetailsPage();
		usageDeatilsPage.verifyDefaultBillCycleValue(selectedBillCycleMonth);
		usageDeatilsPage.verifyUsageBillCycles();
		usageDeatilsPage.selectBillCycleSelector();
	}

	/**
	 * US306452 Usage Modernization: Usage Details Pagination
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUsagePagePagination(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: US306452 Usage Modernization: Usage Details Pagination");
		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click view all usage details | Call Record details page should display");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		// UsagePage usagePage = navigateToUsagePage(myTmoData);
		launchAndPerformLogin(myTmoData);
		getDriver().get("https://qata03.eservice.t-mobile.com/usage");
		UsagePage usagePage = new UsagePage(getDriver());
		usagePage.clickOptionViewByCategory("Messages");
		usagePage.clickLineViewByCategory();

		UsageDetailsPage usageDeatilsPage = new UsageDetailsPage(getDriver());
		usageDeatilsPage.verifyUsageDetailsPage();
		usageDeatilsPage.verifyPagination();
	}

	/**
	 * US305562 - Usage Modernization: Details Header "Back" CTA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testUsagePageBackButton(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: US306452 Usage Modernization: Usage Details Pagination");
		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click view all usage details | Call Record details page should display");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		UsagePage usagePage = navigateToUsagePage(myTmoData);

		usagePage.clickOptionViewByCategory("Messages");
		usagePage.clickLineViewByCategory();

		UsageDetailsPage usageDeatilsPage = new UsageDetailsPage(getDriver());
		usageDeatilsPage.verifyUsageDetailsPage();
		usageDeatilsPage.clickBackButton();
		usagePage.verifyPageUrl();
	}

	/**
	 * US390384 (Automation) Part 1. Updating the test cases for the new angular
	 * Usage page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyUsageOverviewAngularPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. Verify header is displayed | Header is displayed");
		Reporter.log("7. Verify View by Category is displayed | View by Category tab is displayed");
		Reporter.log("8. Verify View by Line is displayed | View by Line tab is displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		UsagePage usage = navigateToUsagePage(myTmoData);
		usage.verifyAllPageElements();
	}

	/**
	 * US390384 (Automation) Part 1. Updating the test cases for the new angular
	 * Usage page
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyDataUsageDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. click on View by Line | Associated lines should be displayed");
		Reporter.log("6. click on any line you desire to | Usage categories has to be displayed");
		Reporter.log("7. Click on Data | Data Usage details page has to be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		UsagePage usage = navigateToUsagePage(myTmoData);
		usage.clickViewByLineSelector();
		usage.clickOnUsageOptionsViewByLine("Data");
		UsageDetailsPage usagedetails = new UsageDetailsPage(getDriver());
		usagedetails.verifyPageLoaded();
	}

	/**
	 * US390384 (Automation) Part 1. Updating the test cases for the new angular
	 * Usage page
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyMessagesUsageDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. click on View by Line | Associated lines should be displayed");
		Reporter.log("6. click on any line you desire to | Usage categories has to be displayed");
		Reporter.log("7. Click on Messages | Message details page has to be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		UsagePage usage = navigateToUsagePage(myTmoData);
		usage.clickViewByLineSelector();
		usage.clickOnUsageOptionsViewByLine("Messages");
		UsageDetailsPage usagedetails = new UsageDetailsPage(getDriver());
		usagedetails.verifyPageLoaded();
	}

	/**
	 * US390384 (Automation) Part 1. Updating the test cases for the new angular
	 * Usage page
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyCallsUsageDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. click on View by Line | Associated lines should be displayed");
		Reporter.log("6. click on any line you desire to | Usage categories has to be displayed");
		Reporter.log("7. Click on Calls | Calls Usage details page has to be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		UsagePage usage = navigateToUsagePage(myTmoData);
		usage.clickViewByLineSelector();
		usage.clickOnUsageOptionsViewByLine("Calls");
		UsageDetailsPage usagedetails = new UsageDetailsPage(getDriver());
		usagedetails.verifyPageLoaded();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = {  "nodata" })
	public void verifyThirdparyPurchaseinvieWbyCatagory(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. Check the month where third party purchases are located | Month should be selected");
		Reporter.log(
				"6. Verify all functionalities for third party purchases | All functionalities should work as expected");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		String Catagory = "Third party purchases";
		checkmonthlyspecificViewbycatagory(myTmoData, Catagory);

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyDatainvieWbyCatagory(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log(
				"5. Verify all functionalities for Data for all months | All functionalities should work as expected");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		String Catagory = "Data";
		checkallMonthsViewbycatagory(myTmoData, Catagory, "Data details");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyMessagesinvieWbyCatagory(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log(
				"5. Verify all functionalities for Messages for all months | All functionalities should work as expected");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		String Catagory = "Messages";
		checkallMonthsViewbycatagory(myTmoData, Catagory, "Message details");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyCallsinvieWbyCatagory(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log(
				"5. Verify all functionalities for Calls for all months | All functionalities should work as expected");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		String Catagory = "Calls";
		checkallMonthsViewbycatagory(myTmoData, Catagory, "Call details");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "testdata" })
	public void verifyThirdparyPurchaseinvieWbyLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. Check the month where third party purchases are located | Month should be selected");
		Reporter.log("6.Click view by line | View by line link should be clicked");
		Reporter.log(
				"7. Verify all functionalities for third party purchases | All functionalities should work as expected");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		String Catagory = "Third party purchases";
		checkspecificMonthViewbyLine(myTmoData, Catagory);

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyDatainvieWbyLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click view by line | View by line link should be clicked");
		Reporter.log(
				"6. Verify all functionalities of data for all months| All functionalities should work as expected");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		String Catagory = "Data";
		checkallMonthsViewbyLine(myTmoData, Catagory, "Data details");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyMessagesinvieWbyLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click view by line | View by line link should be clicked");
		Reporter.log(
				"6. Verify all functionalities of Messages for all months| All functionalities should work as expected");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		String Catagory = "Messages";
		checkallMonthsViewbyLine(myTmoData, Catagory, "Message details");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyCallsinvieWbyLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click view by line | View by line link should be clicked");
		Reporter.log(
				"6. Verify all functionalities of Messages for all months| All functionalities should work as expected");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		String Catagory = "Calls";
		checkallMonthsViewbyLine(myTmoData, Catagory, "Call details");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyLinescountinbothviewsfordata(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log(
				"5.VerifyLinescount in both views for Data| Lines count should be equal for both views for all months");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		String Catagory = "Data";
		checkslinescountinbothviews(myTmoData, Catagory);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyLinescountinbothviewsforMessages(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log(
				"5.VerifyLinescount in both views for Messages| Lines count should be equal for both views for all months");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		String Catagory = "Messages";
		checkslinescountinbothviews(myTmoData, Catagory);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyLinescountinbothviewsforCalls(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US390384 New AngularJS Usage Page - Usage Overview");
		Reporter.log("Data Condition : PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log(
				"5.VerifyLinescount in both views for Calls| Lines count should be equal for both views for all months");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		String Catagory = "Calls";
		checkslinescountinbothviews(myTmoData, Catagory);
	}

	/*	*//**
			 * US497945 Supression of HD Video as conventional datapass
			 * 
			 * @param data
			 * @param myTmoData
			 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testSupressionofHDVideo(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: US497945 Supression of HD Video as conventional datapass");
		Reporter.log("Test Data: Data with enabled HD datapass");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. Verify header is displayed | Header is displayed");
		Reporter.log("6. Verify Data pass is supressed in viewbycategory options| Data pass should be supressed");
		Reporter.log("7. click on View by Line | All lines should be displayed");
		Reporter.log("8. Verify Data pass is supressed in viewbyline options| Data pass should be supressed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		UsagePage usage = navigateToUsagePage(myTmoData);
		usage.verifyViewByCategoryOptionSuppressed("Data pass");
		usage.clickViewByLineSelector();
		usage.verifyViewByLineOptionSuppressed("Data pass");

	}

	/**
	 * US487899 Display Total On Network Usage - View By Line
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testDisplayTotalOnNetworkUsageViewByLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: US487899 Display Total On Network Usage - View By Line");
		Reporter.log("Test Data: Data with TMO One Plan");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. click View by Line tab  | All Lines should be displayed");
		Reporter.log(
				"6. expand line and Verify 'Total on-network data' on data blade| 'Total on-network data'  should be displayed");
		UsagePage usage = navigateToUsagePage(myTmoData);
		usage.clickViewByLineSelector();
		usage.clickonFirstRow();
		usage.verifyTextOnDataBlade(PaymentConstants.TOTAL_ON_NETWORK_DATA);
	}

	/**
	 * US487899 Display Total On Network Usage - View By Line
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testDisplayHighSpeedDataViewByLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: US487899 Display Total On Network Usage - View By Line");
		Reporter.log("Test Data: Non-T-mobile One Customers");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. click View by Line tab  | All Lines should be displayed");
		Reporter.log(
				"6. expand line and Verify 'of Unlimited high-speed data' on data blade| 'of Unlimited high-speed data'  should be displayed");
		UsagePage usage = navigateToUsagePage(myTmoData);
		usage.clickViewByLineSelector();
		usage.clickonFirstRow();
		usage.verifyTextOnDataBlade(PaymentConstants.UNLIMITED_HIGH_SPEED_DATA);
	}

	
	
	
	
	/**
	 * US487900 Display Total On Network Usage - View By Category
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testDisplayTotalOnNetworkUsageViewByCategory(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: US487900 Display Total On Network Usage - View By Category");
		Reporter.log("Test Data: Data with TMO One Plan");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. click View by Line tab  | All Lines should be displayed");
		Reporter.log(
				"6. expand line and Verify 'Total on-network data' on data blade| 'Total on-network data'  should be displayed");
		UsagePage usage = navigateToUsagePage(myTmoData);
		usage.clickonDataRow();
		usage.verifyTextOnDataBlade(PaymentConstants.TOTAL_ON_NETWORK_DATA);
	}

	/**
	 * US487900 Display Total On Network Usage - View By Category
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testDisplayHighSpeedDataViewByCategory(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: US487900 Display Total On Network Usage - View By Category");
		Reporter.log("Test Data: Non-T-mobile One Customers");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. click View by Line tab  | All Lines should be displayed");
		Reporter.log("6. expand line and Verify 'of Unlimited high-speed data' on data blade| 'of Unlimited high-speed data'  should be displayed");
		UsagePage usage = navigateToUsagePage(myTmoData);
		usage.clickViewByLineSelector();
		usage.clickonDataRow();
		usage.verifyTextOnDataBlade(PaymentConstants.UNLIMITED_HIGH_SPEED_DATA);
	}
	
	/**
	 * CDCDWG2-255 [Usage] Display total usage bucket for Hotspot (tether)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
	public void testDisplayTotalUsagebucketForHotspot(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: CDCDWG2-255 [Usage] Display total usage bucket for Hotspot (tether)");
		Reporter.log("Test Data: Non-T-mobile One Customers");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. Verify View by Category is displayed | View by Category tab is displayed");
		Reporter.log("6. expand line and Verify 'of Unlimited high-speed data' on data blade| 'of Unlimited high-speed data'  should be displayed");
		
		Reporter.log("================================");
		
		UsagePage usage = navigateToUsagePage(myTmoData);
		usage.clickonHotspotRow();
		usage.verifyTextOnDataBlade(PaymentConstants.UNLIMITED_HIGH_MOBILE_HOTSPOT);
	}

	
	
	/**
	 * CDCDWG2-255 [Usage] Display total usage bucket for Hotspot (tether)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT})
	public void testMangaedatabuttonfunctionality	(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: CDCDWG2-255 [Usage] Display total usage bucket for Hotspot (tether)");
		Reporter.log("Test Data: Non-T-mobile One Customers");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home page should be dispalyed");
		Reporter.log("4. Click on Usage link | Usage Page should be displayed");
		Reporter.log("5. Check Worried about experiencing slower speeds? text| Worried about experiencing slower speeds? text should be displayed");
		Reporter.log("6. Check Manage Data button | 'Manage Data button  should be displayed");
		Reporter.log("6. Click Manage Data button | 'Page should be navigated to Manage Addons");
		
		Reporter.log("================================");
		
		UsagePage usage = navigateToUsagePage(myTmoData);
		usage.checklowersppedstext();
		usage.checkManageDatabuttonfunctionality();
		ManageAddOnsSelectionPage manageaddons=new ManageAddOnsSelectionPage(getDriver());
		manageaddons.verifyUnblockRoamingAndUnblockIt();
	}
	/**
	 * CDCDWG2-56: SLBG Usage: Single Line Experience on Usage Landing page
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "Sprint" })
	public void testSingleLineforViewbyCategory(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: CDCDWG2-56: SLBG Usage: Single Line Experience on Usage Landing page");
		Reporter.log("Test Data: PAH/Restrcited..etc account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click View by Category | View By Category should display");
		Reporter.log("6.Select Data/Messages/Calls | Single Line(Logged Msisdin data) should be downloaded");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickCallConfirmDoneBtn();
		homePage.clickUsageLink();
		UsagePage usagePage = new UsagePage(getDriver());
		usagePage.verifyPageLoaded();
		usagePage.clickOptionViewByCategory("Data");
		usagePage.clickOptionViewByCategory("Messages");
		usagePage.clickOptionViewByCategory("Calls");
		usagePage.verifySingleLineMsisdin(myTmoData.getPhoneNumber());
		usagePage.verifySingleLineMsisdin(myTmoData.getLoginEmailOrPhone());
	}
	
	/**
	 * CDCDWG2-56: SLBG Usage: Single Line Experience on Usage Landing page
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "Sprint" })
	public void testSingleLineforViewbyLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: CDCDWG2-56: SLBG Usage: Single Line Experience on Usage Landing page");
		Reporter.log("Test Data: PAH/Restrcited..etc account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Page should be displayed");
		Reporter.log("5.Click View by Line | View By Line  should display");
		Reporter.log("6.Select Data/Messages/Calls | Single Line(Logged Msisdin data) should be downloaded");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		
		HomePage homePage = navigateToHomePage(myTmoData);
		if(homePage.verifyCallConfirmDoneBtn()){
			homePage.clickCallConfirmDoneBtn();
		}
		homePage.clickUsageLink();
		UsagePage usagePage = new UsagePage(getDriver());
		usagePage.verifyPageLoaded();
		usagePage.clickViewByLineSelector();
		usagePage.verifySingleLineMsisdin(myTmoData.getPhoneNumber());
		usagePage.verifySingleLineMsisdin(myTmoData.getLoginEmailOrPhone());
	}
	
}