package com.tmobile.eservices.qa.tmng.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.Layer3Page;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

/**
 * @author sputti
 *
 */
public class Layer3PageTest extends TmngCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(Layer3PageTest.class);	
	/***
	 * FLEx - Layer3 End to End Functional Testing 
	 * @param tMNGData
	 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
		public void verifyLayer3Page(TMNGData tMNGData) {
		logger.info("End to End Automation Testing of Layer3 Channel Component");
		Reporter.log("======================================================================");
		Reporter.log("Test Steps | Expected Test Results:");
		Reporter.log("Launch TMNG Layer3 Application | Application Should be Launched");
		Reporter.log("1. Verify Zip Code validations | Zip Code should be displayed, allow only 5 digits");
		Reporter.log("2. Verify CTA Button | CTA Button should be displayed, clicked and User should navigate to List Page");
		Reporter.log("3. Verify Channel List Page | Channel List Headers, Paragraph, Category and Scroll List Imaged are displayed in Layer3 Channels Page");
		Reporter.log("4. Selected the List Category from filter DropDown | List Results should displayed based on the selected List Category");
		Reporter.log("5. Verify New Zip CTA | Component anchors to the top of the parent hero-wrapper component & Zip code input field value is cleared");
		Reporter.log("6. Verify Shop New CTA | Shop New CTA Should be displayed");
		Reporter.log("--------------------------------------------------Actual Test Results:--------------------------------------------------------");
		loadTmngURL(tMNGData);
		getDriver().get("http://dmo-tmo-publisher.corporate.t-mobile.com/content/t-mobile/consumer/tv/demo.html");
		Layer3Page layer3Page = new Layer3Page(getDriver());
		layer3Page.verifyLayer3PageLoaded();
		layer3Page.validZipCode();
		layer3Page.clickOnCTABtn();
		layer3Page.verifyChannels();
		layer3Page.selectListCategory();
		layer3Page.verifyShopCTABtn();
		layer3Page.clickOnNewZipCTABtn();
	}
}