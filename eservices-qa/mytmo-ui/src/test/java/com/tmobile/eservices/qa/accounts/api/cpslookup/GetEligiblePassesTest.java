package com.tmobile.eservices.qa.accounts.api.cpslookup;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetEligiblePassesTest extends AccountsApi {

	public Map<String, String> tokenMap;

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligiblePassesTMOONETI(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Eligible Passes for plan:FRLTUL2LP and dataSOC:FRLTDATA ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("productType", "GSM");
		tokenMap.put("planSOC", "FRLTUL2LP");
		tokenMap.put("dataSOC", "FRLTDATA");
		tokenMap.put("imei", "356219050088550");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligiblePasses(apiTestData, updatedRequest);

		String operationName = "Get Eligible Passes TMO ONE TIs";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			List<String> l = new ArrayList<String>();

			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").length() - 1));
			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").length() - 1));

			List<String> expected = new ArrayList<String>();

			expected.add("HDVPAS24");
			expected.add("INTL24NA");
			expected.add("INTL10NA");
			expected.add("INTL30NA");

			List<?> res = response.path("dataPassesGroupList.dataPasses.soc");
			Reporter.log("size" + res.size());
			Assert.assertTrue(res.size() == 2);

			Reporter.log("Actual" + l);
			Reporter.log("expected" + expected);
			// Assert.assertTrue(expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc")));
			Assert.assertTrue(
					expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[1].soc")));
			Reporter.log("Expected Passes are Returned in Response");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligiblePassesTMOONETE(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Eligible Passes for plan:LTULFM2 and dataSOC:LTDATA ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		String environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("productType", "GSM");
		tokenMap.put("planSOC", "LTULFM2");
		tokenMap.put("dataSOC", "LTDATA");
		tokenMap.put("imei", "356219050088550");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligiblePasses(apiTestData, updatedRequest);

		String operationName = "Get Eligible Passes TMO ONE TE";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			List<String> l = new ArrayList<String>();

			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").length() - 1));
			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").length() - 1));

			List<String> expected = new ArrayList<String>();

			expected.add("HDVPAS24");
			expected.add("INTL24NA");
			expected.add("INTL10NA");
			expected.add("INTL30NA");

			List<?> res = response.path("dataPassesGroupList.dataPasses.soc");
			Reporter.log("size" + res.size());
			Assert.assertTrue(res.size() == 2);

			Reporter.log("Actual" + l);
			Reporter.log("expected" + expected);
			// Assert.assertTrue(expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc")));
			Assert.assertTrue(
					expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[1].soc")));
			Reporter.log("Expected Passes are Returned in Response");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligiblePassesSCNA(ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating Get Eligible Passes for plan:NATTU and dataSOC:NODATA ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		String environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("productType", "GSM");
		tokenMap.put("planSOC", "NATTU");
		tokenMap.put("dataSOC", "NODATA");
		tokenMap.put("imei", "356219050088550");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligiblePasses(apiTestData, updatedRequest);

		String operationName = "Get Eligible Passes TMO ONE SCNA";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			String s = response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").length() - 1);

			if (s.equalsIgnoreCase("NAT7DAY")) {

				assertThat(response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc"),
						hasItems("NA1DAYT", "NA7DAYT"));
				assertThat(response.jsonPath().getList("dataPassesGroupList.dataPasses[1].soc"), hasItem("NAT7DAY"));

			} else {

				assertThat(response.jsonPath().getList("dataPassesGroupList.dataPasses[1].soc"),
						hasItems("NA1DAYT", "NA7DAYT"));
				assertThat(response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc"), hasItem("NAT7DAY"));
			}
			List<?> res = response.path("dataPassesGroupList.dataPasses.soc");
			Assert.assertTrue(res.size() == 2);

			Reporter.log("Actual" + response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc")
					+ response.jsonPath().getList("dataPassesGroupList.dataPasses[1].soc"));
			Reporter.log("expected  NA1DAYT, NA7DAYT, NAT7DAY");

			Reporter.log("Expected Passes are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligiblePassesSC(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Eligible Passes for plan:NAUTTF and dataSOC: Empty ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		String environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("productType", "GSM");
		tokenMap.put("planSOC", "NAUTTF");
		tokenMap.put("dataSOC", "");
		tokenMap.put("imei", "356219050088550");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligiblePasses(apiTestData, updatedRequest);

		String operationName = "Get Eligible Passes TMO ONE SCNA";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			assertThat(response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc"),
					hasItems("NA1DAYT", "NA7DAYT"));
			List<?> res = response.path("dataPassesGroupList.dataPasses.soc");
			Assert.assertTrue(res.size() == 1);

			Reporter.log("Actual" + response.jsonPath().getList("dataPassesGroupList.dataPasses.soc"));
			Reporter.log("expected  NA1DAYT, NA7DAYT");

			Reporter.log("Expected Passes are Returned in Response");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligiblePassesMobileInternet(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Eligible Passes for plan: MITI6GB and dataSOC: empty ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("productType", "MBB");
		tokenMap.put("planSOC", "MITI6GB");
		tokenMap.put("dataSOC", "");
		tokenMap.put("imei", "356219050088550");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligiblePasses(apiTestData, updatedRequest);

		String operationName = "Get Eligible Passes TMO ONE SCNA";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			String s = response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").length() - 1);

			if (s.equalsIgnoreCase("SNPASS24M")) {

				assertThat(response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc"),
						hasItems("NA1DAYT", "NA7DAYT"));
				assertThat(response.jsonPath().getList("dataPassesGroupList.dataPasses[1].soc"), hasItem("SNPASS24M"));

			} else {

				assertThat(response.jsonPath().getList("dataPassesGroupList.dataPasses[1].soc"),
						hasItems("SNPASS24M", "SNPASS30M"));
				assertThat(response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc"),
						hasItems("NA1DAYT", "NA7DAYT"));
			}
			List<?> res = response.path("dataPassesGroupList.dataPasses.soc");
			Assert.assertTrue(res.size() == 2);

			Reporter.log("Actual" + response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc")
					+ response.jsonPath().getList("dataPassesGroupList.dataPasses[1].soc"));
			Reporter.log("expected  NA1DAYT, NA7DAYT, SNPASS24M");

			Reporter.log("Expected Passes are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligiblePassesNCFamily(ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating Get Eligible Passes for plan:LTULFM2 and dataSOC:NCFamily ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("productType", "GSM");
		tokenMap.put("planSOC", "NCCFAMILY");
		tokenMap.put("dataSOC", "NC10GB");
		tokenMap.put("imei", "353024058148280");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligiblePasses(apiTestData, updatedRequest);

		String operationName = "Get Eligible Passes NC Family";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			List<?> res = response.path("dataPassesGroupList.dataPasses");
			Assert.assertTrue(res.size() > 0);

			Reporter.log("No passes are available");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligiblePassesPappyEssential(ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating Get Eligible Passes for plan:LTULFM2 and dataSOC:LTDATA ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("productType", "GSM");
		tokenMap.put("planSOC", "TMESNTL2");
		tokenMap.put("dataSOC", "ESNDATA");
		tokenMap.put("imei", "356219050088550");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligiblePasses(apiTestData, updatedRequest);

		String operationName = "Get Eligible Passes Pappy Essential";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			List<String> l = new ArrayList<String>();

			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").length() - 1));
			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").length() - 1));

			List<String> expected = new ArrayList<String>();
			expected.add("HDVPAS24");
			expected.add("INTL24ES");
			expected.add("INTL10ES");
			expected.add("INTL30ES");
			List<?> res = response.path("dataPassesGroupList.dataPasses.soc");
			Reporter.log("size" + res.size());
			// Assert.assertTrue(res.size() == 2);

			Reporter.log("Actual" + l);
			Reporter.log("expected" + expected);
			// Assert.assertTrue(expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc")));
			Assert.assertTrue(
					expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[1].soc")));
			Reporter.log("Expected Passes are Returned in Response");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligiblePassesPappyTI(ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating Get Eligible Passes for plan:1PLSALL and dataSOC:1PLUS0 ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("productType", "GSM");
		tokenMap.put("planSOC", "1PLSALL");
		tokenMap.put("dataSOC", "1PLUS0");
		tokenMap.put("imei", "356219050088550");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligiblePasses(apiTestData, updatedRequest);

		String operationName = "Get Eligible Passes Pappy Essential";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			List<String> l = new ArrayList<String>();

			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").length() - 1));
			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").length() - 1));

			List<String> expected = new ArrayList<String>();

			expected.add("HDVPASS0");
			expected.add("INTL24NA");
			expected.add("INTL10NA");
			expected.add("INTL30NA");
			List<?> res = response.path("dataPassesGroupList.dataPasses.soc");
			Reporter.log("size" + res.size());
			Assert.assertTrue(res.size() == 2);

			Reporter.log("Actual" + l);
			Reporter.log("expected" + expected);
			// Assert.assertTrue(expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc")));
			Assert.assertTrue(
					expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[1].soc")));
			Reporter.log("Expected Passes are Returned in Response");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligiblePassesUnlimited55(ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating Get Eligible Passes for ONE55TIS and dataSOC:FRLTDATA ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("productType", "GSM");
		tokenMap.put("planSOC", "ONE55TIS");
		tokenMap.put("dataSOC", "FRLTDATA");
		tokenMap.put("imei", "356219050088550");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligiblePasses(apiTestData, updatedRequest);

		String operationName = "Get Eligible Passes Pappy Essential";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			List<String> l = new ArrayList<String>();

			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").length() - 1));
			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").length() - 1));

			List<String> expected = new ArrayList<String>();

			expected.add("HDVPAS24");
			expected.add("INTL24NA");
			expected.add("INTL10NA");
			expected.add("INTL30NA");

			List<?> res = response.path("dataPassesGroupList.dataPasses.soc");
			Reporter.log("size" + res.size());
			Assert.assertTrue(res.size() == 2);

			Reporter.log("Actual" + l);
			Reporter.log("expected" + expected);
			// Assert.assertTrue(expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc")));
			Assert.assertTrue(
					expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[1].soc")));
			Reporter.log("Expected Passes are Returned in Response");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligiblePassesMilitary(ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating Get Eligible Passes for plan:TMSERVE9 and dataSOC:1PLUS ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("productType", "GSM");
		tokenMap.put("planSOC", "1PLSALL");
		tokenMap.put("dataSOC", "1PLUS0");
		tokenMap.put("imei", "356219050088550");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligiblePasses(apiTestData, updatedRequest);

		String operationName = "Get Eligible Passes Pappy Essential";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			List<String> l = new ArrayList<String>();

			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").length() - 1));
			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[1].soc").length() - 1));

			List<String> expected = new ArrayList<String>();

			expected.add("HDVPASS0");
			expected.add("INTL24NA");
			expected.add("INTL10NA");
			expected.add("INTL30NA");

			List<?> res = response.path("dataPassesGroupList.dataPasses.soc");
			Reporter.log("size" + res.size());
			Assert.assertTrue(res.size() == 2);

			Reporter.log("Actual" + l);
			Reporter.log("expected" + expected);
			// Assert.assertTrue(expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc")));
			Assert.assertTrue(
					expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[1].soc")));
			Reporter.log("Expected Passes are Returned in Response");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetEligiblePassesPappyEssential_MI(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Eligible Passes for plan:TMSERVE9 and dataSOC:1PLUS ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("planSOC", "ESNTLMI");
		tokenMap.put("productType", "MBB");
		tokenMap.put("dataSOC", "");
		tokenMap.put("imei", "356219050088550");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligiblePasses(apiTestData, updatedRequest);

		String operationName = "Get Eligible Passes Pappy Essential MI";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			List<String> l = new ArrayList<String>();

			l.add(response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").substring(1,
					response.jsonPath().getString("dataPassesGroupList.dataPasses[0].soc").length() - 1));

			List<String> expected = new ArrayList<String>();
			expected.add("ESPASS24M");
			expected.add("ESPASS10M");
			expected.add("ESPASS30M");

			List<?> res = response.path("dataPassesGroupList.dataPasses.soc");
			Reporter.log("size" + res.size());
			Assert.assertTrue(res.size() == 1);

			Reporter.log("Actual" + l);
			Reporter.log("expected" + expected);
			Reporter.log("expected" + expected);
			Assert.assertTrue(
					expected.containsAll(response.jsonPath().getList("dataPassesGroupList.dataPasses[0].soc")));
			Reporter.log("Expected Passes are Returned in Response");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testgetEligiblePassesMilitary_NoPasses(ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating Get Eligible Passes for plan:TMSERVE9 and dataSOC:Empty ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("productType", "GSM");
		tokenMap.put("planSOC", "1PLSALL");
		tokenMap.put("dataSOC", "");
		tokenMap.put("imei", "356219050088550");

		String requestBody = new ServiceTest().getRequestFromFile("getEligiblePasses.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getEligiblePasses(apiTestData, updatedRequest);

		String operationName = "Get Eligible Passes Pappy Essential";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			List<?> res = response.path("dataPassesGroupList.dataPasses.soc");
			Assert.assertTrue(res.size() == 0);

			Reporter.log("No passes are available");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
}