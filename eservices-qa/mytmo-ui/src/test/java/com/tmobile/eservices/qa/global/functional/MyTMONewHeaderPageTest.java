package com.tmobile.eservices.qa.global.functional;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.UNAVCommonPage;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.UsageOverviewPage;
import com.tmobile.eservices.qa.pages.shop.PhonePages;
import com.tmobile.eservices.qa.pages.shop.ShopPage;

public class MyTMONewHeaderPageTest extends GlobalCommonLib {
	// private static final Logger logger =
	// LoggerFactory.getLogger(MyTMONewHeaderPageTest.class);
	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
	 * UNAV MyTmo | Unav Integration | Testing Verify Bill Tab Redirects To Bill
	 * Page
	 * 
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyBillLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify BILL Tab is displayed | BILL Tab is displayed");
		Reporter.log("4.Click on BILL Tab | Clicked on BILL Tab successfully");
		Reporter.log("5.Verify BILL page | BILL page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyBillLabel();
		unavPage.clickBillLink();
		unavPage.verifyBillPage();
	}

	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
	 * UNAV MyTmo | Unav Integration | Testing Verify Usage Tab Redirects To Usage
	 * Page
	 * 
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyUsageLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Usage Tab is displayed | Usage Tab is displayed");
		Reporter.log("4.Click on Usage Tab | Clicked on Usage Tab successfully");
		Reporter.log("5.Verify Usage page | Usage page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyUsageLabel();
		unavPage.clickUsageLink();
		unavPage.verifyUsagePage();
	}

	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
	 * UNAV MyTmo | Unav Integration | Testing Verify Account Tab Redirects To
	 * Account Page
	 * 
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyAccountLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Account Tab is displayed | Account Tab is displayed");
		Reporter.log("4.Click on Account Tab | Clicked on Account Tab successfully");
		Reporter.log("5.Verify Account page | Account page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyAccountLabel();
		unavPage.clickAccountLink();
		unavPage.verifyAccountPage();
	}

	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
	 * UNAV MyTmo | Unav Integration | Testing Verify Phone Tab Redirects To Phone
	 * Page
	 * 
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyPhoneLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Phone Tab is displayed | Phone Tab is displayed");
		Reporter.log("4.Click on Phone Tab | Clicked on Phone Tab successfully");
		Reporter.log("5.Verify Phone page | Phone page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyHeaderPhoneLabel();
		unavPage.clickHeaderPhoneLink();
		unavPage.verifyHeaderPhonePage();
	}

	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
	 * UNAV MyTmo | Unav Integration | Testing Verify Shop Tab Redirects To Shop
	 * Page
	 * 
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyShopLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Shop Tab is displayed | Shop Tab is displayed");
		Reporter.log("4.Click on Shop Tab | Clicked on BenefitsAndMore Tab successfully");
		Reporter.log("5.Verify Shop page | Shop page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyShopLabel();
		unavPage.clickShopLink();
		unavPage.verifyShopPage();
	}

	/*
	 * // Verify Account Menu Options
	 * 
	 *//**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
		 * UNAV MyTmo | Unav Integration | Testing Verify ManageMyLines Redirects To
		 * ManageMyLines Page
		 * 
		 * @param data
		 */
	/*
	 * 
	 * @Test(dataProvider = "byColumnName", enabled = true, groups = {
	 * Group.FLEX_SPRINT }) public void verifyManageMyLinesLink(ControlTestData
	 * data, MyTmoData myTmoData){ Reporter.
	 * log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
	 * Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
	 * Reporter.log("Test Steps | Expected Results:");
	 * Reporter.log("1.Launch the application | Application Should be Launched");
	 * Reporter.log("2.Verify Home page | Home page should be displayed");
	 * Reporter.log("3.Move to Account Tab | Move to the Account Tab"); Reporter.
	 * log("4.Verify ManageMyLines Label is displayed | ManageMyLines Label is displayed"
	 * ); Reporter.
	 * log("5.Click on ManageMyLines | Clicked on ManageMyLines successfully");
	 * Reporter.
	 * log("6.Verify ManageMyLines page | ManageMyLines page should be displayed");
	 * Reporter.log("========================"); Reporter.log("Actual Results");
	 * UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
	 * navigateToHomePage(myTmoData); unavPage.clickMenuExpandButton();
	 * unavPage.clickAccountPlusExpandButton(); unavPage.verifyManageMyLinesLabel();
	 * unavPage.clickManageMyLineslink(); unavPage.verifyManageMyLinesPage(); }
	 *//**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
		 * UNAV MyTmo | Unav Integration | Testing Verify Edit my Profile Redirects To
		 * Edit my Profile Page
		 * 
		 * @param data
		 */
	/*
	 * 
	 * @Test(dataProvider = "byColumnName", enabled = true, groups = {
	 * Group.FLEX_SPRINT }) public void verifyEditMyProfileLink(ControlTestData
	 * data, MyTmoData myTmoData){ Reporter.
	 * log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
	 * Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
	 * Reporter.log("Test Steps | Expected Results:");
	 * Reporter.log("1.Launch the application | Application Should be Launched");
	 * Reporter.log("2.Verify Home page | Home page should be displayed");
	 * Reporter.log("3.Move to Account Tab | Move to the Account Tab"); Reporter.
	 * log("4.Verify Edit my Profile Label is displayed | Edit my Profile Label is displayed"
	 * ); Reporter.
	 * log("5.Click on Edit my Profile | Clicked on Edit my Profile successfully");
	 * Reporter.
	 * log("6.Verify Edit my Profile page | Edit my Profile page should be displayed"
	 * ); Reporter.log("========================"); Reporter.log("Actual Results");
	 * UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
	 * navigateToHomePage(myTmoData); unavPage.clickMenuExpandButton();
	 * unavPage.clickAccountPlusExpandButton(); unavPage.verifyEditProfileLabel();
	 * unavPage.clickEditMyProfilelink(); unavPage.verifyEditmyProfilePage(); }
	 *//**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
		 * UNAV MyTmo | Unav Integration | Testing Verify Report a lost or stolen device
		 * Redirects To Report a lost or stolen device Page
		 * 
		 * @param data
		 */
	/*
	 * 
	 * @Test(dataProvider = "byColumnName", enabled = true, groups = {
	 * Group.FLEX_SPRINT }) public void verifyReportLostDeviceLink(ControlTestData
	 * data, MyTmoData myTmoData){ Reporter.
	 * log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
	 * Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
	 * Reporter.log("Test Steps | Expected Results:");
	 * Reporter.log("1.Launch the application | Application Should be Launched");
	 * Reporter.log("2.Verify Home page | Home page should be displayed");
	 * Reporter.log("3.Move to Phone Tab | Move to the Phone Tab"); Reporter.
	 * log("4.Verify Report a lost or stolen device Label is displayed | Report a lost or stolen device Label is displayed"
	 * ); Reporter.
	 * log("5.Click on Report a lost or stolen device | Clicked on Report a lost or stolen device successfully"
	 * ); Reporter.
	 * log("6.Verify Report a lost or stolen device page | Report a lost or stolen device page should be displayed"
	 * ); Reporter.log("========================"); Reporter.log("Actual Results");
	 * UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
	 * navigateToHomePage(myTmoData); unavPage.clickMenuExpandButton();
	 * unavPage.clickPhonePlusExpandButton();
	 * unavPage.verifyReportLostDeviceLabel(); unavPage.clickReportLostDevicelink();
	 * unavPage.verifyReportLostDevicePage(); }
	 *//**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
		 * UNAV MyTmo | Unav Integration | Testing Verify Add a person or device
		 * Redirects To Add a person or device Page
		 * 
		 * @param data
		 */
	/*
	 * 
	 * @Test(dataProvider = "byColumnName", enabled = true, groups = {
	 * Group.FLEX_SPRINT }) public void verifyAddDeviceLink(ControlTestData data,
	 * MyTmoData myTmoData){ Reporter.
	 * log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
	 * Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
	 * Reporter.log("Test Steps | Expected Results:");
	 * Reporter.log("1.Launch the application | Application Should be Launched");
	 * Reporter.log("2.Verify Home page | Home page should be displayed");
	 * Reporter.log("3.Move to Phone Tab | Move to the Phone Tab"); Reporter.
	 * log("4.Verify Add a person or device Label is displayed | Add a person or device Label is displayed"
	 * ); Reporter.
	 * log("5.Click on Add a person or device | Clicked on Add a person or device successfully"
	 * ); Reporter.
	 * log("6.Verify Add a person or device page | Add a person or device page should be displayed"
	 * ); Reporter.log("========================"); Reporter.log("Actual Results");
	 * UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
	 * navigateToHomePage(myTmoData); unavPage.clickMenuExpandButton();
	 * unavPage.clickPhonePlusExpandButton(); unavPage.verifyAddDeviceLabel();
	 * unavPage.clickAddDevicelink(); unavPage.verifyAddDevicePage(); }
	 *//**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
		 * UNAV MyTmo | Unav Integration | Testing Verify See the latest deal Redirects
		 * To See the latest deal Page
		 * 
		 * @param data
		 *//*
			 * 
			 * @Test(dataProvider = "byColumnName", enabled = true, groups = {
			 * Group.FLEX_SPRINT }) public void verifySeeLatestDealsLink(ControlTestData
			 * data, MyTmoData myTmoData){ Reporter.
			 * log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			 * Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			 * Reporter.log("Test Steps | Expected Results:");
			 * Reporter.log("1.Launch the application | Application Should be Launched");
			 * Reporter.log("2.Verify Home page | Home page should be displayed");
			 * Reporter.log("3.Move to Shop Tab | Move to the Shop Tab"); Reporter.
			 * log("4.Verify See the latest deal Label is displayed | See the latest deal Label is displayed"
			 * ); Reporter.
			 * log("5.Click on See the latest deal | Clicked on See the latest deal successfully"
			 * ); Reporter.
			 * log("6.Verify See the latest deal page | See the latest deal page should be displayed"
			 * ); Reporter.log("========================"); Reporter.log("Actual Results");
			 * UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			 * navigateToHomePage(myTmoData); unavPage.clickMenuExpandButton();
			 * unavPage.clickShopPlusExpandButton();} unavPage.verifySeeLatestDealsLabel();
			 * unavPage.clickSeeLatestDealslink(); unavPage.verifyLatestDealsPage(); }
			 */
	// UNAV Header Utilities

	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
	 * UNAV MyTmo | Unav Integration | Testing Verify Contact Us Redirects To
	 * Contact Us Page
	 * 
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void verifyHeaderContactUsLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Contact Us Label is displayed |  Contact Us Label is displayed");
		// Reporter.log("4.Click on Contact Us | Clicked on Contact Us successfully");
		// Reporter.log("5.Verify Contact Us page | Contact Us page should be
		// displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyHeaderContactUsLabel();
		// unavPage.clickHeaderContactUslink();
		// unavPage.verifyHeaderContactUsPage();
	}

	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
	 * UNAV MyTmo | Unav Integration | Testing Verify My Account and Profile
	 * 
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void verifyProfileLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify My Account Icon is displayed |  My Account Icon is displayed");
		Reporter.log("4.Verify My Account Drop Down Option is displayed |  My Account Drop Down Option is displayed");
		Reporter.log("5.Click on My Account | Clicked on My Account successfully");
		Reporter.log("6.Verify Profile is displayed |  Profile is displayed");
		Reporter.log("7.Click on Profile | Clicked on Profile successfully");
		Reporter.log("8.Verify Profile page |  Profile page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.verifyUtilityMyAccountLabel();
		unavPage.clickUtilityMyAccountlink();
		unavPage.verifyProfileLabel();
		unavPage.clickProfilelink();
		unavPage.verifyProfilePage();
	}

	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
	 * UNAV MyTmo | Unav Integration | Testing Verify Account History
	 * 
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DUPLICATE })
	public void verifyAccountHistoryLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify My Account Icon is displayed |  My Account Icon is displayed");
		Reporter.log("4.Verify My Account Drop Down Option is displayed |  My Account Drop Down Option is displayed");
		Reporter.log("5.Click on My Account | Clicked on My Account successfully");
		Reporter.log("6.Verify Account History is displayed |  Account History is displayed");
		Reporter.log("7.Click on Account History | Clicked on Account successfully");
		Reporter.log("8.Verify Account History page |  Account History page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.verifyUtilityMyAccountLabel();
		unavPage.clickUtilityMyAccountlink();
		unavPage.verifyAccountHistoryLabel();
		unavPage.clickAccountHistorylink();
		unavPage.verifyAccountHistoryPage();
	}

	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T CDCDWR-330 -
	 * UNAV MyTmo | Unav Integration | Testing Verify Logout
	 * 
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void verifyLogoutLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify My Account Drop Down Option is displayed |  My Account Drop Down Option is displayed");
		Reporter.log("4.Click on My Account | Clicked on My Account successfully");
		Reporter.log("5.Verify Logout is displayed |  Logout is displayed");
		Reporter.log("6.Click on Logout | Clicked on Logout successfully");
		Reporter.log("7.Verify signin page |  signin page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.verifyUtilityMyAccountLabel();
		unavPage.clickUtilityMyAccountlink();
		unavPage.verifyLogoutLabel();
		unavPage.clickLogoutlink();
		unavPage.verifySigninPage();
	}

	/**
	 * CDCDWR-342 - .NET Migration | Gov & Bus | Updates for Bill and Usage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void verifyBillLinkForGovtCust(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-342 - .NET Migration | Gov & Bus | Updates for Bill and Usage");
		Reporter.log("Test Data : Gov/Bus Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify BILL Tab is displayed | BILL Tab is displayed");
		Reporter.log("4.Click on BILL Tab | Clicked on BILL Tab successfully");
		Reporter.log("5.Verify BILL page for Gov/Bus Customer | BILL page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyBillLabel();
		unavPage.clickBillLink();
		unavPage.verifyBillPage();
	}

	/**
	 * CDCDWR-342 - .NET Migration | Gov & Bus | Updates for Bill and Usage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void verifyUsageLinkForGovtCust(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-342 - .NET Migration | Gov & Bus | Updates for Bill and Usage");
		Reporter.log("Test Data : Gov/Bus Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Usage Tab is displayed | Usage Tab is displayed");
		Reporter.log("4.Click on Usage Tab | Clicked on Usage Tab successfully");
		Reporter.log("5.Verify Usage page For Gov Cust | Usage page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.clickMenuExpandButton();
		unavPage.verifyUsageLabel();
		unavPage.clickUsageLink();
		unavPage.verifyUsagePage();
	}

	/**
	 * CDCDWR-330:UNAV MyTmo | Unav Integration | Testing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT })
	public void verifyUnavHeaderNavigationLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-330:UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on billing tab | Billing summary page should be displayed");
		Reporter.log("5. Click on usage link | Usage overview page should be displayed");
		Reporter.log("7. Click on phone tab | Phone page should be displayed");
		Reporter.log("8. Verify shop unav menu | Shop unav menu should be displayed");
		Reporter.log("9. Click on shop tab | Shop page should be displayed");
		Reporter.log("========================");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickBillingLink();

		BillAndPaySummaryPage BillAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
		BillAndPaySummaryPage.verifyPageLoaded();

		homePage.clickUsageMenu();

		UsageOverviewPage usageOverviewPage = new UsageOverviewPage(getDriver());
		usageOverviewPage.verifyUsageOverviewPage();

		homePage.clickPhoneMenu();

		PhonePages phonePages = new PhonePages(getDriver());
		phonePages.verifyPhonesPage();

		homePage.verifyShopUnavMenu();
		homePage.clickShoplink();

		ShopPage newShopPage = new ShopPage(getDriver());
		newShopPage.verifyNewShoppage();
	}

	/**
	 * US503486:Home Page | UNAV Updates | Schedule a Call Modal * CDCDWR-742 -
	 * MyTMO Home | UNAV - Call Us | Angular Versions Support for Schedule Call Modal
	 * TC1546:Ensure click on call us from Global header should bring a call now modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyHomePageScheduleCallUpdate(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US503486:Home Page | UNAV Updates | Schedule a Call Modal");
		Reporter.log("CDCDWR-742 - MyTMO Home | UNAV - Call Us | Angular Versions Support for Schedule Call Modal");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on contact us phone icon | Click on phone icon should be success");
		Reporter.log("5. Verify call set up popup window | Call set up popup window should be displayed");
		Reporter.log("6. Click on schedule a call button | Schedule a call button should be clicked");
		Reporter.log("7.Click on time slot | Time slot should be clicked");
		Reporter.log("8.Click confirm button | Confirm button should be clicked");
		Reporter.log("9.Verify confirmation window | Confirmation window should be displayed");
		Reporter.log("10.Click on confirmation window | Confirmation window should be clicked");
		Reporter.log("11.Verify call team button | Call team button should be displayed");
		Reporter.log("12.Click call team button | Call team button should be clicked");
		Reporter.log("13.Verify make a change button | Make a change button should be displayed");
		Reporter.log("14.Click make a change button | Click on make a change button should be success");
		Reporter.log("15.Click on schedule a call button | Schedule a call button should be clicked");
		Reporter.log("16.Click on time slot | Time slot should be clicked");
		Reporter.log("17.Click confirm button | Confirm button should be clicked");
		Reporter.log("18.Click error modal popup ok button | Click on error modal popup should be success");
		Reporter.log("19.Verify call team button | Call team button should be displayed");
		Reporter.log("20.Click call team button | Call team button should be clicked");
		Reporter.log("21.Verify make a change button | Make a change button should be displayed");

		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickCallUsMenu();
		homePage.verifySetUpCallPopUpForNewAngular();
		homePage.selectScheduledTime();
		homePage.clickConfirmBtn();
		homePage.verfiyConfirmationWindow();
		homePage.clickConfirmationWindow();
		homePage.clickCallUsMenu();
		homePage.verifyMakeAChangeBtn();
		homePage.clickMakeAChangeButton();
		homePage.selectScheduledTime();
		homePage.clickConfirmBtn();
		homePage.verfiyConfirmationWindow();
		homePage.clickConfirmationWindow();
		homePage.clickCallUsMenu();
		homePage.verifyMakeAChangeBtn();
		homePage.clickCancelCallBack();
	}

	/**
	 * CDCDWR2-408: UNAV | Tmo & MyTmo | Magenta Page Indicator - pt 2 S22
	 * 
	 * @param data
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void verifyUNAVMagentaPageIndicatorMytmo(MyTmoData myTmoData) {
		Reporter.log("CDCDWR2-408: UNAV | Tmo & MyTmo | Magenta Page Indicator - pt 2 S22");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4.Click any menu item on UNAV |  Selected menu should be hilighted");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToNewHomePage(myTmoData);
		unavPage.clickBillLink();
		unavPage.verifyBillPage();
		unavPage.verifySelectedMenuisHighlighted("Bill");
		unavPage.clickUsageLink();
		unavPage.verifyUsagePage();
		unavPage.verifySelectedMenuisHighlighted("Usage");

	}
}