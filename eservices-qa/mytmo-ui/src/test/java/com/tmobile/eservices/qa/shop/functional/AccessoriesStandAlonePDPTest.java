package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAlonePDPPage;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAlonePLPPage;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAloneReviewCartPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

/**
 * @author rnallamilli
 *
 */
public class AccessoriesStandAlonePDPTest extends ShopCommonLib {

	/**
	 * US306707: MyTMO - Additional Terms - Standalone Accessories Flow - PLP
	 * Pricing Display US306708: MyTMO - Additional Terms - Standalone
	 * Accessories Flow - PDP Pricing Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
	public void testAdditionalTermsPricingPuertoRico(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case: US306707, US351448: MyTMO - Additional Terms - Standalone Accessories Flow - PLP Pricing Display");
		Reporter.log(
				"Test Case: US306708, US351453: MyTMO - Additional Terms - Standalone Accessories Flow - PDP Pricing Display");
		Reporter.log("Data Condition | Puerto Rico PAH Customer. EIP eligible");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on Accessories link | Accessories PLP page should be displayed");
		Reporter.log("6. Verify that each accessory contains:");
		Reporter.log("6.1. Verify that each accessory contains EIP down payment | EIP down payment should be present");
		Reporter.log(
				"6.2. Verify that each accessory contains loan term length | Loan term length should be present. And should be 9, 12, 24 or 36 month");
		Reporter.log("6.3. Verify that each accessory contains monthly payment | Monthly payment should be present");
		Reporter.log("6.4. Verify that each accessory contains FRP | FRP should be present");
		Reporter.log("7. Click on first accessory | Accessories PDP page should be displayed");
		Reporter.log("8. Verify that Accessory PDP page contains:");
		Reporter.log("8.1. Verify EIP down payment | EIP down payment should be present");
		Reporter.log(
				"8.2. Verify loan term length | Loan term length should be present. And should be 9, 12, 24 or 36 month");
		Reporter.log("8.3. Verify monthly payment | Monthly payment should be present");
		Reporter.log("8.4. Verify FRP | FRP should be present");
		Reporter.log(
				"8.5. Verify that all numbers from PLP are same for PDP (EIP down payment, loan term length and monthly payment, FRP) | Numbers should be the same");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = navigateToStandAloneAccessoriesPLP(myTmoData);
		accessoriesStandAlonePLPPage.verifyEIPDownPayment();
		accessoriesStandAlonePLPPage.verifyMonthlyPayment();
		accessoriesStandAlonePLPPage.verifyLoanTerm();
		accessoriesStandAlonePLPPage.verifyFRPinPLP();
		accessoriesStandAlonePLPPage.verifyLoanTermLengthInPLP();

		Double frpPrice = accessoriesStandAlonePLPPage.getAccessoryFRPPriceInPLP();
		String downPayment = accessoriesStandAlonePLPPage.getAccessoryDownPaymentInPLP();
		String loanTerm = accessoriesStandAlonePLPPage.getAccessoryLoanTermInPLP();
		String monthlyPayment = accessoriesStandAlonePLPPage.getAccessoryMonthlyPaymentInPLP();

		accessoriesStandAlonePLPPage.selectFirstStandAloneAccessoryDevice();
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		accessoriesStandAlonePDPPage.verifyFRPinPDP();
		accessoriesStandAlonePDPPage.verifyEIPDownPayment();
		accessoriesStandAlonePDPPage.verifyMonthlyPayment();
		accessoriesStandAlonePDPPage.verifyLoanTerm();
		accessoriesStandAlonePDPPage.verifyLoanTermLengthInPDP();

		accessoriesStandAlonePDPPage
				.compareFRPPriceIsSameForPLPAndPDP(accessoriesStandAlonePDPPage.getAccessoryFRPPRiceInPDP(), frpPrice);
		accessoriesStandAlonePDPPage.compareDownPaymentIsSameForPLPAndPDP(
				accessoriesStandAlonePDPPage.getAccessoryDownPaymentInPDP(), downPayment);
		accessoriesStandAlonePDPPage
				.compareLoanTermIsSameForPLPAndPDP(accessoriesStandAlonePDPPage.getAccessoryLoanTermInPDP(), loanTerm);
		accessoriesStandAlonePDPPage.compareMonthlyPaymentIsSameForPLPAndPDP(
				accessoriesStandAlonePDPPage.getAccessoryMonthlyPaymentInPDP(), monthlyPayment);
		
	}

	/**
	 * US306707: MyTMO - Additional Terms - Standalone Accessories Flow - PLP
	 * Pricing Display
	 * US306708: MyTMO - Additional Terms - Standalone
	 * Accessories Flow - PDP Pricing Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testAdditionalTermsPricingStandard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case: US306707, US351448: MyTMO - Additional Terms - Standalone Accessories Flow - PLP Pricing Display");
		Reporter.log(
				"Test Case: US306708, US351453: MyTMO - Additional Terms - Standalone Accessories Flow - PDP Pricing Display");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on Accessories link | Accessories PLP page should be displayed");
		Reporter.log("6. Verify that each accessory contains FRP | FRP should be present");
		Reporter.log("7. Click on first accessory | Accessories PDP page should be displayed");
		Reporter.log("8. Verify that Accessory PDP page contains FRP | FRP should be present");
		Reporter.log("8.1. Verify FRP from PLP are same for PDP | Numbers should be the same");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = navigateToStandAloneAccessoriesPLP(myTmoData);
		accessoriesStandAlonePLPPage.verifyFRPinPLP();
		Double frpPriceinPLP = accessoriesStandAlonePLPPage.getAccessoryFRPPriceInPLP();
		accessoriesStandAlonePLPPage.selectFirstStandAloneAccessoryDevice();
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		accessoriesStandAlonePDPPage.verifyFRPinPDP();
		accessoriesStandAlonePDPPage.compareFRPPriceIsSameForPLPAndPDP(
				accessoriesStandAlonePDPPage.getAccessoryFRPPRiceInPDP(), frpPriceinPLP);
	}

	/**
	 * US335505: TEST ONLY: MyTMO - Additional Terms - Accessory with Catalog
	 * Promotion (Standalone Accessory flow)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void CatalogPromotionAccessoryPriceDisplayedCorrectlyForStandaloneAccessoryFlow(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test Case: US335505:TEST ONLY: MyTMO - Additional Terms - Accessory with Catalog Promotion (Standalone Accessory flow)");
		Reporter.log("Data Condition | Catalog Promotion Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on Accessories link | Accessories PLP page should be displayed");
		Reporter.log("6. Verify Accessory with Catalog Promotion | Accessory with Catalog Promotion is present");
		Reporter.log("7. Verify FRP after promo | Save FRP for that accessory as PLPFRP");
		Reporter.log("8. Verify down payment | Save down payment for that accessory as PLPDP");
		Reporter.log("9. Verify EIP price | Save EIP for that accessory as PLPEIP");
		Reporter.log("10. Verify EIP term | Save EIP term for that accessory as PLPEIPTERM");
		Reporter.log("12. Verify new FRP | new PLPFRP = PLPDP + PLPEIP X PLPEIPTERM");
		Reporter.log("13. Click  on Promotin Accessory | Accessory pdp page should be displayed");
		Reporter.log("14. Verify FRP after promo | Save FRP for that accessory as PDPFRP");
		Reporter.log("15. Compare PDPFRP with PLPFRP | PDPFRP should be equal to PLPFRP");
		Reporter.log("16. Verify down payment | Save down payment for that accessory as PDPDP");
		Reporter.log("17. Compare PDPDP with PLPDP | PDPDP should be equal to PLPDP");
		Reporter.log("18. Verify EIP price | Save EIP for that accessory as PDPEIP");
		Reporter.log("19. Compare PDPEIP with PLPEIP | PDPEIP should be equal to PLPEIP");
		Reporter.log("20. Verify EIP term | Save EIP term for that accessory as PDPEIPTERM");
		Reporter.log("21. Compare PDPEIPTERM with PLPEIPTERM | PDPEIPTERM should be equal to PLPEIPTERM");
		Reporter.log("22. Verify new FRP | new PDPFRP = PDPDP + PDPEIP X PDPEIPTERM");
		Reporter.log(
				"23. Verify Strike-through price = original FRP minus the catalog promo amount | Strike-through price should be present");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = navigateToStandAloneAccessoriesPLP(myTmoData);
		accessoriesStandAlonePLPPage.verifyAccessoryWithCatalogPromotion();

		accessoriesStandAlonePLPPage.verifyFRPinPLPforPromoItem();
		Double PLPFRP = accessoriesStandAlonePLPPage.getAccessoryPromoPriceForFirstAccessory();
		accessoriesStandAlonePLPPage.verifyEIPDownPaymentPromo();
		Double PLPDP = accessoriesStandAlonePLPPage.getAccessoryDownPaymentPromoPriceForFirstAccessory();
		accessoriesStandAlonePLPPage.verifyMonthlyPaymentPromo();
		Double PLPEIP = accessoriesStandAlonePLPPage.getAccessoryPromoEIPPriceForFirstAccessory();
		accessoriesStandAlonePLPPage.verifyLoanTermPromo();
		accessoriesStandAlonePLPPage.verifyLoanTermLengthInPLPPromo();
		Double PLPEIPTERM = accessoriesStandAlonePLPPage.getAccessoryPromoEIPLoanTermForFirstAccessory();

		Double caclEIP = (PLPFRP - PLPDP) / (PLPEIPTERM);
		accessoriesStandAlonePLPPage.compareEIPPrice(PLPEIP, caclEIP);

		accessoriesStandAlonePLPPage.selectEIPAccessoryWithPromo();
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		accessoriesStandAlonePDPPage.verifyFRPinPDP();
		Double PDPFRP = accessoriesStandAlonePDPPage.getAccessoryFRPPRiceInPDPValue();
		accessoriesStandAlonePDPPage.compareFRPPriceIsSameForPLPAndPDP(PLPFRP, PDPFRP);
		accessoriesStandAlonePDPPage.verifyEIPDownPayment();
		Double PDPDP = accessoriesStandAlonePDPPage.getAccessoryDownPaymentPriceForAccessory();
		accessoriesStandAlonePDPPage.compareDownPaymentIsSameForPLPAndPDP(PLPDP.toString(), PDPDP.toString());
		accessoriesStandAlonePDPPage.verifyMonthlyPayment();
		Double PDPEIP = accessoriesStandAlonePDPPage.getAccessoryMonthlyPaymentInPDPValue();
		accessoriesStandAlonePDPPage.compareMonthlyPaymentIsSameForPLPAndPDP(PLPEIP.toString(), PDPEIP.toString());//
		accessoriesStandAlonePDPPage.verifyLoanTerm();
		accessoriesStandAlonePDPPage.verifyLoanTermLengthInPDP();
		Double PDPEIPTERM = accessoriesStandAlonePDPPage.getAccessoryEIPLoanTermForAccessory();
		accessoriesStandAlonePDPPage.compareLoanTermIsSameForPLPAndPDP(PLPEIPTERM.toString(), PDPEIPTERM.toString());
		Double caclPDPEIP = (PDPFRP - PDPDP) / (PDPEIPTERM);
		accessoriesStandAlonePDPPage.compareEIPPrice(PDPEIP, caclPDPEIP);
		accessoriesStandAlonePDPPage.verifyAccessoryStrikedThroughPrice();
		// approved by sam
	}

	/**
	 * US380439 Standalone Accessories :
	 * California Proposition 65 legal requirements
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testCaliforniaPropositionLegalRequirementsModalStandaloneAccessoriesFlow(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : US380439 Standalone Accessories : California Proposition 65 legal requirements");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Click Accessory device | Accessory pdp page should be displayed");
		Reporter.log(
				"7. verify text 'California Residents: See the California Proposition 65 WARNING' below device specification section | 'California Residents: See the California Proposition 65 WARNING' should be displayed");
		Reporter.log(
				"7. click on 'California Proposition 65 WARNING' link | California Proposition 65 Warning modal should be displayed");
		Reporter.log(
				"8.verify 'California Proposition 65 Warning ' header |  'California Proposition 65 Warning ' header should be displayed");
		Reporter.log(
				"9. verify ' WARNING: Cancer and Reproductive Harm - www.P65warnings.ca.gov' message | ' WARNING: Cancer and Reproductive Harm - www.P65warnings.ca.gov' message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.verifyPageUrl();
		shopPage.clickQuickLinks("Accessories");

		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		accessoriesStandAlonePLPPage.clickAccesoriesDeviceByName(myTmoData.getAccessoryName());
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		accessoriesStandAlonePDPPage.verifyCaliforniaResidentsWarning();
		accessoriesStandAlonePDPPage.clickonCaliforniaPropositionLink();
		accessoriesStandAlonePDPPage.verifyCaliforniaPropositionWarningModal();
	}
	
	/**
	 * US354983: Standalone accessories : Show Reviews on PDP
	 * US474171: Standalone accessories: Enable "Helpful" and "Unhelpful" for Reviews on PDP
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testShowReviewsOnStandAloneAccessoriesPDPPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case :US354983: Standalone accessories : Show Reviews on PDP");
		Reporter.log("Data Condition | STD PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Click Accessory device | Accessory pdp page should be displayed");
		Reporter.log("7. Verify Star images for Accessory device | Star images for Accessory device should be displayed");
		Reporter.log("8. Verify Accessory device Reviews count | Accessory device Reviews count should be displayed");
		Reporter.log("9. Verify Review Tab in below accessory | In below Accessory Review tab should be displayed");
		Reporter.log("10. Click Review Tab in below accessory | In below Accessory Review tab should be clickable");
		Reporter.log("11. Verify Filter options | Filter options should be displayed");
		Reporter.log("12. Verify 'All Reviews' option in Filter options tab | 'All Reviews' option in Filter options tab should be displayed");
		Reporter.log("10. Verify 'HELPFUL' link for each review count | 'HELPFUL' link for each review should be displayed");
		Reporter.log("11. Get any 'HELPFUL' link review count and Store V1  | 'HELPFUL' link review count should be Stored in V1");
		Reporter.log("12. Click 'HELPFUL' review link and Get link count store in V2  | 'HELPFUL' link review count should be Stored in V2 ");
		Reporter.log("13. Compare 'HELPFUL' review link count, saved in V1 and V2 | 'HELPFUL' review link count values(V1 and V2) should not matched");
		Reporter.log("14. Verify 'UNHELPFUL' link for each review count | 'UNHELPFUL' link for each review should be displayed");
		Reporter.log("15. Get any 'UNHELPFUL' link review count and Store V3  | 'UNHELPFUL' link review count should be Stored in V3");
		Reporter.log("16. Click 'UNHELPFUL' review link and Get link count store in V4  | 'UNHELPFUL' link review count should be Stored in V4 ");
		Reporter.log("17. Compare 'UNHELPFUL' review link count, saved in V3 and V4 | 'UNHELPFUL' review link count values(V3 and V4) should not matched");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.verifyPageUrl();
		shopPage.clickQuickLinks("Accessories");

		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		accessoriesStandAlonePLPPage.clickAccesoriesDeviceByName(myTmoData.getAccessoryName());
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		accessoriesStandAlonePDPPage.verifyStarImageForAccessory();
		accessoriesStandAlonePDPPage.verifyReviewCountForAccessory();
		accessoriesStandAlonePDPPage.verifyReviewTabBelowForAccessory();
		accessoriesStandAlonePDPPage.clickReviewTabBelowForAccessory();
		accessoriesStandAlonePDPPage.verifyFilterOptionForAccessoryReview();
		accessoriesStandAlonePDPPage.clickReviewTabBelowForAccessory();
		accessoriesStandAlonePDPPage.verifyPositiveReviewCountForAccessory();
		accessoriesStandAlonePDPPage.verifyNegativeReviewCountForAccessory();
	
	}

	/**
	 * TA1651844: StandaloneAccessoryPDP
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void verifyFeaturesSectionForAnAccessory(ControlTestData data,MyTmoData myTmoData) {
		Reporter.log("Test Case : TA1651844: StandaloneAccessoryPDP");
		Reporter.log("Data Condition | IR PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Click Accessory device | Accessory pdp page should be displayed");
		Reporter.log("7. Verify features section for an Accessory | Features section should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.verifyPageUrl();
		shopPage.clickQuickLinks("Accessories");

		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		accessoriesStandAlonePLPPage.clickAccesoriesDeviceByName(myTmoData.getAccessoryName());
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		accessoriesStandAlonePDPPage.clickFeaturesOnAccessoriesStandlonePDPPage();
		accessoriesStandAlonePDPPage.verifyFeaturesSectionOnAccessoriesStandalonePDPPage();
	}

	/**
	 * TA1651844: StandaloneAccessoryPDP
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyRelatedDevicesAndAddAnAccessoryFromRelatedDevicesOnStandaloneAccessoryPDPPage(ControlTestData data,MyTmoData myTmoData) {
		Reporter.log("Test Case : TA1651844: StandaloneAccessoryPDP");
		Reporter.log("Data Condition | IR PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Click Accessory device | Accessory pdp page should be displayed");
		Reporter.log("7. Verify related devices for an accessory | Related devices for an accessory should be displayed");
		Reporter.log("8. Add any related device to Cart | Related device should be added to cart");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.verifyPageUrl();
		shopPage.clickQuickLinks("Accessories");

		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
		accessoriesStandAlonePLPPage.clickAccesoriesDeviceByName(myTmoData.getAccessoryName());
		AccessoriesStandAlonePDPPage accessoriesStandAlonePDPPage = new AccessoriesStandAlonePDPPage(getDriver());
		accessoriesStandAlonePDPPage.verifyAccessoriesStandAlonePDPPage();
		accessoriesStandAlonePDPPage.clickRelatedTabOnAccessoriesStandlonePDPPage();
		accessoriesStandAlonePDPPage.verifyRelatedDevicesSectionOnAccessoriesStandalonePDPPage();

		String relatedAccessory =accessoriesStandAlonePDPPage.selectRelatedAccessoryOnAccessoriesStandalonePage();

		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage=new AccessoriesStandAloneReviewCartPage(getDriver());
		accessoriesStandAloneReviewCartPage.verifyReviewCartText();
		accessoriesStandAloneReviewCartPage.verifyAddedAccessoryIsAvailableOnReviewCartPage(relatedAccessory);

	}
}

/**
 * Retired US In this Page: US295819
 */



