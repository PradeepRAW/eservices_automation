package com.tmobile.eservices.qa.global.api;
import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.StoreLocatorApi;
import com.tmobile.eservices.qa.api.eos.YextApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class StoreLocatorApiTest extends StoreLocatorApi {
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL","testGetStoreById",Group.GLOBAL,Group.SPRINT  })
	public void testGetStoreById(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getStoreById test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with store details.");
		Reporter.log("Step 2: Verify store details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetStoreById";
		Response response =getStoreById(apiTestData,"7807");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String id= getPathVal(jsonNode, "id");
				String type= getPathVal(jsonNode, "type");
				String name= getPathVal(jsonNode, "name");
				String url= getPathVal(jsonNode, "url");
				String latitude= getPathVal(jsonNode, "location.latitude");
				String longitude= getPathVal(jsonNode, "location.longitude");
				String inStoreWaitTime= getPathVal(jsonNode, "inStoreWaitTime");
				String spanish= getPathVal(jsonNode, "spanish");
				String services= getPathVal(jsonNode, "services");
				String holidays= getPathVal(jsonNode, "holidays");
				String inStoreAppointment= getPathVal(jsonNode, "inStoreAppointment");
				
				System.out.println("id ="+id);
				Assert.assertEquals(id,"7807","Invalid Id");
				Assert.assertNotNull(type,"Invalid type");
				Assert.assertNotNull(name,"Invalid name");
				Assert.assertNotNull(url,"Invalid url");
				Assert.assertNotNull(latitude,"Invalid latitude");
				Assert.assertNotNull(longitude,"Invalid longitude");
				Assert.assertNotNull(inStoreWaitTime,"Invalid inStoreWaitTime");
				Assert.assertNotNull(spanish,"Invalid spanish");
				Assert.assertNotNull(services,"Invalid services");
				Assert.assertNotNull(holidays,"Invalid holidays");
				Assert.assertNotNull(inStoreAppointment,"Invalid inStoreAppointment");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL","testAddCustomer",Group.GLOBAL,Group.SPRINT  })
	public void testAddCustomer(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: addCustomer test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with new customer details.");
		Reporter.log("Step 2: Verify new customer details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile("StoreLocator_addCustomer.txt");
		/*tokenMap = new HashMap<String, String>();
		tokenMap.put("storeID", apiTestData.getStoreID());
		tokenMap.put("firstName", apiTestData.getFirstName());
		tokenMap.put("lastName", apiTestData.getLastName());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("email", apiTestData.getEmail());
		tokenMap.put("reasonIDs", apiTestData.getReasonIDs());
		tokenMap.put("notes", apiTestData.getNotes());
		tokenMap.put("customerType", apiTestData.getCustomerType());
		tokenMap.put("preferredLanguage", apiTestData.getPreferredLanguage());*/
		
		//String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		System.out.println("requestBody = "+requestBody);
		String operationName="testAddCustomer";
		Response response =addCustomer(apiTestData, requestBody);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String waittime= getPathVal(jsonNode, "[0].Waittime");
				String positionInQueue= getPathVal(jsonNode, "[0].PositionInQueue");
				String customerName= getPathVal(jsonNode, "[0].CustomerName");
				Assert.assertNotNull(waittime,"Invalid waittime");
				Assert.assertNotNull(positionInQueue,"Invalid positionInQueue");
				Assert.assertNotNull(customerName,"Invalid customerName");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL","testGetStoresInCity",Group.GLOBAL,Group.SPRINT  })
	public void testGetStoresInCity(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getStoresInCity test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with store details.");
		Reporter.log("Step 2: Verify store details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetStoresInCity";
		Response response =getStoresInCity(apiTestData, "fl","miami","all");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String name= getPathVal(jsonNode, "name");
				String storeCount= getPathVal(jsonNode, "storeCount");
				String contextPath= getPathVal(jsonNode, "contextPath");
				String id= getPathVal(jsonNode, "storeList[0].id");
				String type= getPathVal(jsonNode, "storeList[0].type");
				String stname= getPathVal(jsonNode, "storeList[0].name");
				String url= getPathVal(jsonNode, "storeList[0].url");
				String latitude= getPathVal(jsonNode, "storeList[0].location.latitude");
				String longitude= getPathVal(jsonNode, "storeList[0].location.longitude");
				Assert.assertEquals(name,"miami","Invalid name");
				Assert.assertNotNull(storeCount,"Invalid storeCount");
				Assert.assertNotNull(contextPath,"Invalid contextPath");
				Assert.assertNotNull(id,"Invalid id");
				Assert.assertNotNull(type,"Invalid type");
				Assert.assertNotNull(stname,"Invalid stname");
				Assert.assertNotNull(url,"Invalid url");
				Assert.assertNotNull(latitude,"Invalid latitude");
				Assert.assertNotNull(longitude,"Invalid longitude");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL","testGetStoresInState",Group.GLOBAL,Group.SPRINT  })
	public void testGetStoresInState(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getStoresInState test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with stores in state details.");
		Reporter.log("Step 2: Verify stores in state details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetStoresInState";
		Response response =getStoresInState(apiTestData,"wa");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String name= getPathVal(jsonNode, "name");
				String abbr= getPathVal(jsonNode, "abbr");
				String contextPath= getPathVal(jsonNode, "contextPath");
				String cityName= getPathVal(jsonNode, "cityList[0].name");
				String storeCount= getPathVal(jsonNode, "cityList[0].storeCount");
				String url= getPathVal(jsonNode, "cityList[0].url");
				String displayName= getPathVal(jsonNode, "cityList[0].displayName");
				
				Assert.assertNotNull(name,"Invalid name");
				Assert.assertNotNull(abbr,"Invalid abbr");
				Assert.assertNotNull(contextPath,"Invalid contextPath");
				Assert.assertNotNull(cityName,"Invalid cityName");
				Assert.assertNotNull(storeCount,"Invalid storeCount");
				Assert.assertNotNull(url,"Invalid url");
				Assert.assertNotNull(displayName,"Invalid displayName");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL","testGetStoreByName",Group.GLOBAL,Group.SPRINT  })
	public void testGetStoreByName(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getStoreByName test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with store details.");
		Reporter.log("Step 2: Verify store details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetStoreByName";
		Response response =getStoreByName(apiTestData,"ky","louisville","towne-center-drive-springhurst-blvd");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String id= getPathVal(jsonNode, "id");
				String type= getPathVal(jsonNode, "type");
				String name= getPathVal(jsonNode, "name");
				String url= getPathVal(jsonNode, "url");
				String latitude= getPathVal(jsonNode, "location.latitude");
				String longitude= getPathVal(jsonNode, "location.longitude");
				String inStoreWaitTime= getPathVal(jsonNode, "inStoreWaitTime");
				String spanish= getPathVal(jsonNode, "spanish");
				String services= getPathVal(jsonNode, "services");
				String holidays= getPathVal(jsonNode, "holidays");
				String inStoreAppointment= getPathVal(jsonNode, "inStoreAppointment");
				String streetAddress= getPathVal(jsonNode, "location.address.streetAddress");
				String addressLocality= getPathVal(jsonNode, "location.address.addressLocality");
				String addressRegion= getPathVal(jsonNode, "location.address.addressRegion");
				String postalCode= getPathVal(jsonNode, "location.address.postalCode");
				
				System.out.println("id ="+id);
				Assert.assertNotNull(id,"Invalid Id");
				Assert.assertNotNull(type,"Invalid type");
				//Assert.assertEquals(name,"Invalid name");
				Assert.assertNotNull(url,"Invalid url");
				Assert.assertNotNull(latitude,"Invalid latitude");
				Assert.assertNotNull(longitude,"Invalid longitude");
				Assert.assertNotNull(inStoreWaitTime,"Invalid inStoreWaitTime");
				Assert.assertNotNull(spanish,"Invalid spanish");
				Assert.assertNotNull(services,"Invalid services");
				Assert.assertNotNull(holidays,"Invalid holidays");
				Assert.assertNotNull(inStoreAppointment,"Invalid inStoreAppointment");
				Assert.assertNotNull(streetAddress,"Invalid streetAddress");
				Assert.assertNotNull(addressLocality,"Invalid addressLocality");
				Assert.assertNotNull(addressRegion,"Invalid addressRegion");
				Assert.assertNotNull(postalCode,"Invalid postalCode");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL","testGetStoresByCoordinates",Group.GLOBAL,Group.SPRINT  })
	public void testGetStoresByCoordinates(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getStoresByCoordinates test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with store details.");
		Reporter.log("Step 2: Verify store details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetStoresByCoordinates";
		Response response =getStoresByCoordinates(apiTestData,"47.6587802","-117.42604649999998","100","20");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String id= getPathVal(jsonNode, "[0].id");
				String type= getPathVal(jsonNode, "[0].type");
				String name= getPathVal(jsonNode, "[0].name");
				String url= getPathVal(jsonNode, "[0].url");
				String latitude= getPathVal(jsonNode, "[0].location.latitude");
				String longitude= getPathVal(jsonNode, "[0].location.longitude");
				String inStoreWaitTime= getPathVal(jsonNode, "[0].inStoreWaitTime");
				String spanish= getPathVal(jsonNode, "[0].spanish");
				String services= getPathVal(jsonNode, "[0].services");
				String holidays= getPathVal(jsonNode, "[0].holidays");
				String inStoreAppointment= getPathVal(jsonNode, "[0].inStoreAppointment");
				String streetAddress= getPathVal(jsonNode, "[0].location.address.streetAddress");
				String addressLocality= getPathVal(jsonNode, "[0].location.address.addressLocality");
				String addressRegion= getPathVal(jsonNode, "[0].location.address.addressRegion");
				String postalCode= getPathVal(jsonNode, "[0].location.address.postalCode");
				
				System.out.println("id ="+id);
				Assert.assertNotNull(id,"Invalid Id");
				Assert.assertNotNull(type,"Invalid type");
				//Assert.assertEquals(name,"Main St & Heritage","Invalid name");
				Assert.assertNotNull(url,"Invalid url");
				Assert.assertNotNull(latitude,"Invalid latitude");
				Assert.assertNotNull(longitude,"Invalid longitude");
				Assert.assertNotNull(inStoreWaitTime,"Invalid inStoreWaitTime");
				Assert.assertNotNull(spanish,"Invalid spanish");
				//Assert.assertNotNull(services,"Invalid services");
				//Assert.assertNotNull(holidays,"Invalid holidays");
				Assert.assertNotNull(inStoreAppointment,"Invalid inStoreAppointment");
				Assert.assertNotNull(streetAddress,"Invalid streetAddress");
				Assert.assertNotNull(addressLocality,"Invalid addressLocality");
				Assert.assertNotNull(addressRegion,"Invalid addressRegion");
				Assert.assertNotNull(postalCode,"Invalid postalCode");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL","testGetAllStoreUrls",Group.GLOBAL,Group.SPRINT  })
	public void testGetAllStoreUrls(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGetAllStoreUrls test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with store url details.");
		Reporter.log("Step 2: Verify store url details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetAllStoreUrls";
		Response response =getAllStoreUrls(apiTestData);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String stateName= getPathVal(jsonNode, "states[0].name");
				String stateUrl= getPathVal(jsonNode, "states[0].url");
				String cityName= getPathVal(jsonNode, "states[0].cities[0].name");
				String cityUrl= getPathVal(jsonNode, "states[0].cities[0].url");
				//String storeName= getPathVal(jsonNode, "states[0].cities[0].stores[0].name");
				//String storeUrl= getPathVal(jsonNode, "states[0].cities[0]stores[0].url");
				
				Assert.assertNotNull(stateName,"Invalid stateName");
				Assert.assertNotNull(stateUrl,"Invalid url");
				Assert.assertNotNull(cityName,"Invalid cityName");
				Assert.assertNotNull(cityUrl,"Invalid cityUrl");
				//Assert.assertNotNull(storeName,"Invalid storeName");
				//Assert.assertNotNull(storeUrl,"Invalid storeUrl");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL","testGetReasons",Group.GLOBAL,Group.SPRINT  })
	public void testGetReasons(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getReasons test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with reasons details.");
		Reporter.log("Step 2: Verify reasons details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetReasons";
		Response response =getReasons(apiTestData);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String reasonID= getPathVal(jsonNode, "[0].ReasonID");
				String reason= getPathVal(jsonNode, "[0].Reason");
				String reasonES= getPathVal(jsonNode, "[0].ReasonES");
				
				Assert.assertNotNull(reasonID,"Invalid reasonID");
				Assert.assertNotNull(reason,"Invalid reason");
				Assert.assertNotNull(reasonES,"Invalid reasonES");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL","testGetInStoreWaitTimes",Group.GLOBAL,Group.REGRESSION  })
	public void testGetInStoreWaitTimes(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getInStoreWaitTimes test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with store wait time details.");
		Reporter.log("Step 2: Verify store wait time details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetInStoreWaitTimes";
		Response response =getInStoreWaitTimes(apiTestData);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String storeID= getPathVal(jsonNode, "[0].StoreID");
				String waittime= getPathVal(jsonNode, "[0].Waittime");
				
				Assert.assertNotNull(storeID,"Invalid storeID");
				Assert.assertNotNull(waittime,"Invalid waittime");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL","testGetStoresByAddress",Group.GLOBAL,Group.SPRINT  })
	public void testGetStoresByAddress(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getStoresByCoordinates test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with store details.");
		Reporter.log("Step 2: Verify store details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGetStoresByAddress";
		Response response =getStoreByAddress(apiTestData, "3618 Factoria Blvd SE, Bellevue, WA 98006", "All", "5", "10");
		Assert.assertTrue(response.statusCode()==200);
		Reporter.log("Response Status :"+response.statusCode());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 *//*
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL","verifyStoresInPuertoRico",Group.GLOBAL,Group.SPRINT  })
	public void verifyStoresInPuertoRico(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verifyStoresInPuertoRico test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with store details.");
		Reporter.log("Step 2: Verify store details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="verifyStoresInPuertoRico";
		Response response =getStoreByAddress(apiTestData, "Bayamon, PR", "All", "5", "10");
		Reporter.log("Response Status :"+response.statusCode());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String line1= getPathVal(jsonNode, "response.entities[0].address.line1");
				String city= getPathVal(jsonNode, "response.entities[0].address.city");
				String region= getPathVal(jsonNode, "response.entities[0].address.region");
				String postalCode= getPathVal(jsonNode, "response.entities[0].address.postalCode");
				String countryCode= getPathVal(jsonNode, "response.entities[0].address.countryCode");
				String addressHidden= getPathVal(jsonNode, "response.entities[0].addressHidden");
				String description= getPathVal(jsonNode, "response.entities[0].description");
				String brands= getPathVal(jsonNode, "response.entities[0].brands");
				String logo= getPathVal(jsonNode, "response.entities[0].logo");
				String name= getPathVal(jsonNode, "response.entities[0].name");
				String closed= getPathVal(jsonNode, "response.entities[0].closed");
				String c_matchCode= getPathVal(jsonNode, "response.entities[0].c_matchCode");
				String c_oldStoreID= getPathVal(jsonNode, "response.entities[0].c_oldStoreID");
				String c_phpStoreFlag= getPathVal(jsonNode, "response.entities[0].c_phpStoreFlag");
				String c_storeName= getPathVal(jsonNode, "response.entities[0].c_storeName");
				String c_storeType= getPathVal(jsonNode, "response.entities[0].c_storeType");
				String c_tMobileEntityId= getPathVal(jsonNode, "response.entities[0].c_tMobileEntityId");
				String c_tRACKINGURL= getPathVal(jsonNode, "response.entities[0].c_tRACKINGURL");
				String featuredMessage= getPathVal(jsonNode, "response.entities[0].featuredMessage");
				String isoRegionCode= getPathVal(jsonNode, "response.entities[0].isoRegionCode");
				String keywords= getPathVal(jsonNode, "response.entities[0].keywords");
				String mainPhone= getPathVal(jsonNode, "response.entities[0].mainPhone");
				String paymentOptions= getPathVal(jsonNode, "response.entities[0].paymentOptions");
				String alternateNames= getPathVal(jsonNode, "response.entities[0].alternateNames");
				String customKeywords= getPathVal(jsonNode, "response.entities[0].customKeywords");
				String rankTrackingEnabled= getPathVal(jsonNode, "response.entities[0].rankTrackingEnabled");
				String rankTrackingFrequency= getPathVal(jsonNode, "response.entities[0].rankTrackingFrequency");
				String rankTrackingQueryTemplates= getPathVal(jsonNode, "response.entities[0].rankTrackingQueryTemplates");
				String rankTrackingSites= getPathVal(jsonNode, "response.entities[0].rankTrackingSites");
				String timezone= getPathVal(jsonNode, "response.entities[0].timezone");
				String yextDisplayCoordinate= getPathVal(jsonNode, "response.entities[0].yextDisplayCoordinate");
				String yextRoutableCoordinate= getPathVal(jsonNode, "response.entities[0].yextRoutableCoordinate");
				String meta= getPathVal(jsonNode, "response.entities[0].meta");
				String googleAttributes= getPathVal(jsonNode, "response.entities[0].googleAttributes");
				String categoryIds= getPathVal(jsonNode, "response.entities[0].categoryIds");
				//Assert.assertNotNull(storeID,"Invalid storeID");
				//Assert.assertNotNull(waittime,"Invalid waittime");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
		YextApi api=new YextApi();
		//Response rs=api.entitiesList(apiTestData,"{\"address.region\":{\"$eq\":\"WA\"}}");
		Response rs=api.entitiesList(apiTestData,"{\"address.line1\":{\"$eq\":\"27520 Covington Way SE\"}}");
		if (rs != null && "200".equals(Integer.toString(rs.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		}else{
			failAndLogResponse(response, operationName);
		}
	}*/
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL", "compareStoreCount", Group.GLOBAL,Group.SPRINT })
	public void compareStoreCount(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: compareStoreCount test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: get response of SLv 2.1 getStoreByAdress api call for given address.");
		Reporter.log("Step 2: Add filter for address.line1 with given Address in Yext entitiesList api call and get the response");
		Reporter.log("Step 3: Compare and count number of stores are equal for both response.");
		Reporter.log("================================");

		String operationName = "compareStoreCount";
		String addressVal = "3618 Factoria Blvd SE, Bellevue, WA 98006";
		String addressLine1="3618 Factoria Blvd SE";
		int slCount = 0;
		int yextCount = 0;
		Response response = getStoreByAddress(apiTestData, addressVal, "All", "5", "1");
		Reporter.log("Response Status :" + response.statusCode());

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if (jsonNode.isArray()) {
					slCount = jsonNode.size();
				}
			}
		}
		YextApi yextApi = new YextApi();
		Response yextResponse = yextApi.entitiesList("{\"address.line1\":{\"$eq\":\"" + addressLine1 + "\"}}");
		if (yextResponse != null && "200".equals(Integer.toString(yextResponse.getStatusCode()))) {
			Reporter.log(yextResponse.body().asString());
			logSuccessResponse(yextResponse, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(yextResponse.asString());
			if (!jsonNode.isMissingNode()) {
				yextCount = jsonNode.get("response").get("count").asInt();
			}

			if (slCount == 0 & yextCount == 0) {
				Reporter.log("Response : No Records in StoreLocator and Yext Api for the given Address");
			} else {
				Assert.assertEquals(slCount, yextCount, "Store Count Mismatch between StoreLocator and Yext Api");
			}
		}

	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL", "verifyCostcoRetailTelephoneNumberIsNull", Group.GLOBAL, Group.SPRINT })
	public void verifyCostcoRetailTelephoneNumberIsNull(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verifyCostcoRetailTelephoneNumberIsNull test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: get response of SLv 2.1 getStoreByAdress api call for given address.");
		Reporter.log("Step 2: Add filter for address.line1 with given Address in Yext entitiesList api call and get the response");
		Reporter.log("Step 3: Compare and count number of stores are equal for both response.");
		Reporter.log("================================");

		String costcoBilling = "2290 King Ave W, Billings, MT, 59102";
		String costcoMiami = "14800 Sole Mia Way, North Miami, FL, 33181";
		String costcoGrove = "7400 Elk Grove Blvd, Elk Grove, CA, 95757";

		Response responseForCostcoBilling = getStoreByAddress(apiTestData, costcoBilling, "All", "5", "1");
		Response responseForCostcoMiami = getStoreByAddress(apiTestData, costcoMiami, "All", "5", "1");
		Response responseForCostcoGrove = getStoreByAddress(apiTestData, costcoGrove, "All", "5", "1");

		boolean isTelephoneCostcoBilling = checkTelephoneNumber(responseForCostcoBilling);
		boolean isTelephoneCostcoMiami = checkTelephoneNumber(responseForCostcoMiami);
		boolean isTelephoneCostcoGrove = checkTelephoneNumber(responseForCostcoGrove);

		Assert.assertTrue(isTelephoneCostcoBilling,	"Telephone Number for Costco Wireless Advocates Billings is not Null");
		Assert.assertTrue(isTelephoneCostcoMiami,"Telephone Number for Costco Wireless Advocates North Miami is not Null");
		Assert.assertTrue(isTelephoneCostcoGrove,"Telephone Number for Costco Wireless Advocates Elk Grove is not Null");
	}

	private boolean checkTelephoneNumber(Response responseForCostco) throws Exception {
		String storeId = null;
		String telephoneSLV = null;
		String yextPhone = null;
		boolean status = false;
		if (responseForCostco != null && "200".equals(Integer.toString(responseForCostco.getStatusCode()))) {
			logSuccessResponse(responseForCostco, "verifyCostcoRetailTelephoneNumberIsNull");
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(responseForCostco.asString());
			if (!jsonNode.isMissingNode()) {
				if (jsonNode.isArray()) {
					storeId = jsonNode.get(0).get("id").asText();
					telephoneSLV = jsonNode.get(0).get("telephone").asText();
				}
			}
		}

		if (StringUtils.isNoneEmpty(storeId)) {
			YextApi yextApi = new YextApi();
			Response yextResponse = yextApi.listStores(storeId,"1");
			if (yextResponse != null && "200".equals(Integer.toString(yextResponse.getStatusCode()))) {

				Reporter.log(yextResponse.body().asString());
				logSuccessResponse(yextResponse, "YextListStores");
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(yextResponse.asString());
				if (!jsonNode.isMissingNode()) {
					yextPhone = jsonNode.get("response").get("phone").asText();
				}
				if (StringUtils.isEmpty(yextPhone) && StringUtils.isEmpty(telephoneSLV)) {
					status = true;
				} else {
					status = false;
				}
			}
		}

		return status;
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL", "verifyCountOfStoreInAState", Group.GLOBAL,Group.SPRINT })
	public void verifyCountOfStoreInAState(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verifyCountOfStoreInAState test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: get response of SLv 2.1 getStoreByAdress api call for given address.");
		Reporter.log("Step 2: Add filter for address.region with given State 'SD' in Yext entitiesList api call and get the response");
		Reporter.log("Step 3: Compare and count number of stores are equal for both response.");
		Reporter.log("================================");

		String operationName = "verifyCountOfStoreInAState";
		String addressVal = "Rapid City, South Dakota, USA";
		int slCount = 0;
		int yextCount = 0;
		Response response = getStoreByAddress(apiTestData, addressVal, "All", "5", "3");
		Reporter.log("Response Status :" + response.statusCode());

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if (jsonNode.isArray()) {
					slCount = jsonNode.size();
				}
			}
		}
		YextApi yextApi = new YextApi();
		Response yextResponse = yextApi.entitiesList("{\"address.region\":{\"$eq\":\"SD\"}}");
		if (yextResponse != null && "200".equals(Integer.toString(yextResponse.getStatusCode()))) {
			Reporter.log(yextResponse.body().asString());
			logSuccessResponse(yextResponse, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(yextResponse.asString());
			if (!jsonNode.isMissingNode()) {
				yextCount = jsonNode.get("response").get("count").asInt();
			}

			if (slCount == 0 & yextCount == 0) {
				Reporter.log("Response : No Records in StoreLocator and Yext Api for the given Address");
			} else {
				Assert.assertEquals(slCount, yextCount, "Store Count Mismatch between StoreLocator and Yext Api");
			}
		}
	}	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL", "verifyMilitaryLocation", Group.GLOBAL,Group.SPRINT })
	public void verifyMilitaryLocation(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verifyMilitaryLocation test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: get response of SLv 2.1 getStoreByAdress api call for given address.");
		Reporter.log("Step 2: Add filter for address.region with given State 'SD' in Yext entitiesList api call and get the response");
		Reporter.log("Step 3: Compare and count number of stores are equal for both response.");
		Reporter.log("================================");

		String operationName = "verifyMilitaryLocation";
		String addressVal = "WA military Bremerton, WA, USA";
		Response response = getStoreByAddress(apiTestData, addressVal, "All", "5", "1");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if (jsonNode.isArray()) {
					String streetAddress= getPathVal(jsonNode, "[0].location.address.streetAddress");
					String addressLocality= getPathVal(jsonNode, "[0].location.address.addressLocality");
					String addressRegion= getPathVal(jsonNode, "[0].location.address.addressRegion");
					String postalCode= getPathVal(jsonNode, "[0].location.address.postalCode");
					String storeId= getPathVal(jsonNode, "[0].id");
					YextApi yextApi = new YextApi();
					Response yextResponse = yextApi.listStores(storeId,"1");
					if (yextResponse != null && "200".equals(Integer.toString(yextResponse.getStatusCode()))) {
						Reporter.log(yextResponse.body().asString());
						logSuccessResponse(yextResponse, operationName);
						 mapper = new ObjectMapper();
						 jsonNode = mapper.readTree(yextResponse.asString());
						if (!jsonNode.isMissingNode()) {
							String address= getPathVal(jsonNode, "response.address");
							String city= getPathVal(jsonNode, "response.city");
							String state= getPathVal(jsonNode, "response.state");
							String zip= getPathVal(jsonNode, "response.zip");
							
							//Assert.assertEquals(streetAddress, address);
							Assert.assertEquals(addressLocality, city);
							Assert.assertEquals(addressRegion, state);
							Assert.assertEquals(postalCode, zip);
						}
					}else{
						failAndLogResponse(yextResponse, operationName);
					}
				}
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL", "verifyKioskLocation", Group.GLOBAL,Group.SPRINT })
	public void verifyKioskLocation(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verifyKioskLocation test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: get response of SLv 2.1 getStoreByAdress api call for given address.");
		Reporter.log("Step 2: Add filter for address.region with given State 'SD' in Yext entitiesList api call and get the response");
		Reporter.log("Step 3: Compare and count number of stores are equal for both response.");
		Reporter.log("================================");
		String operationName = "verifyKioskLocation";
		String addressVal = "Dupont, WA, 98327";
		Response response = getStoreByAddress(apiTestData, addressVal, "All", "25", "25");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if (jsonNode.isArray()) {
					String streetAddress= getPathVal(jsonNode, "[0].location.address.streetAddress");
					String addressLocality= getPathVal(jsonNode, "[0].location.address.addressLocality");
					String addressRegion= getPathVal(jsonNode, "[0].location.address.addressRegion");
					String postalCode= getPathVal(jsonNode, "[0].location.address.postalCode");
					String storeId= getPathVal(jsonNode, "[0].id");
					
					YextApi yextApi = new YextApi();
					Response yextResponse = yextApi.listStores(storeId,"1");
					if (yextResponse != null && "200".equals(Integer.toString(yextResponse.getStatusCode()))) {
						Reporter.log(yextResponse.body().asString());
						logSuccessResponse(yextResponse, operationName);
						 mapper = new ObjectMapper();
						 jsonNode = mapper.readTree(yextResponse.asString());
						if (!jsonNode.isMissingNode()) {
							String address= getPathVal(jsonNode, "response.address");
							String city= getPathVal(jsonNode, "response.city");
							String state= getPathVal(jsonNode, "response.state");
							String zip= getPathVal(jsonNode, "response.zip");
							
							//Assert.assertEquals(streetAddress, address);
							Assert.assertEquals(addressLocality, city);
							Assert.assertEquals(addressRegion, state);
							Assert.assertEquals(postalCode, zip);
						}
					}else{
						failAndLogResponse(yextResponse, operationName);
					}
				}
			}
		}else{
			failAndLogResponse(response, operationName);
		}
		

	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL", "verifyStoresInPuertoRico", Group.GLOBAL,Group.SPRINT })
	public void verifyStoresInPuertoRico(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verifyStoresInPuertoRico test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: get response of SLv 2.1 getStoreByAdress api call for given address.");
		Reporter.log("Step 2: Add filter for address.region with given State 'SD' in Yext entitiesList api call and get the response");
		Reporter.log("Step 3: Compare and count number of stores are equal for both response.");
		Reporter.log("================================");

		String operationName = "verifyStoresInPuertoRico";
		String addressVal = "Bayamon, PR";
		Response response = getStoreByAddress(apiTestData, addressVal, "All", "10", "1");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if (jsonNode.isArray()) {
					String streetAddress= getPathVal(jsonNode, "[0].location.address.streetAddress");
					String addressLocality= getPathVal(jsonNode, "[0].location.address.addressLocality");
					String addressRegion= getPathVal(jsonNode, "[0].location.address.addressRegion");
					String postalCode= getPathVal(jsonNode, "[0].location.address.postalCode");
					String storeId= getPathVal(jsonNode, "[0].id");
					
					YextApi yextApi = new YextApi();
					Response yextResponse = yextApi.listStores(storeId,"1");
					if (yextResponse != null && "200".equals(Integer.toString(yextResponse.getStatusCode()))) {
						logSuccessResponse(yextResponse, operationName);
						 mapper = new ObjectMapper();
						 jsonNode = mapper.readTree(yextResponse.asString());
						if (!jsonNode.isMissingNode()) {
							String address= getPathVal(jsonNode, "response.address");
							String city= getPathVal(jsonNode, "response.city");
							String state= getPathVal(jsonNode, "response.state");
							String zip= getPathVal(jsonNode, "response.zip");
							//Assert.assertEquals(streetAddress, address);
							Assert.assertEquals(addressLocality, city);
							Assert.assertEquals(addressRegion, state);
							Assert.assertEquals(postalCode, zip);
						}
					}else{
						failAndLogResponse(yextResponse, operationName);
					}
				}
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL", "verifyStoresinNebraska", Group.GLOBAL,Group.SPRINT })
	public void verifyStoresinNebraska(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verifyStoresinNebraska test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: get response of SLv 2.1 getStoreByAdress api call for given address.");
		Reporter.log("Step 2: Add filter for address.region with given State 'SD' in Yext entitiesList api call and get the response");
		Reporter.log("Step 3: Compare and count number of stores are equal for both response.");
		Reporter.log("================================");

		String operationName = "verifyStoresinNebraska";
		String addressVal = "Nebraska, NE";
		Response response = getStoreByAddress(apiTestData, addressVal, "All", "10", "1");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if (jsonNode.isArray()) {
					String streetAddress= getPathVal(jsonNode, "[0].location.address.streetAddress");
					String addressLocality= getPathVal(jsonNode, "[0].location.address.addressLocality");
					String addressRegion= getPathVal(jsonNode, "[0].location.address.addressRegion");
					String postalCode= getPathVal(jsonNode, "[0].location.address.postalCode");
					String storeId= getPathVal(jsonNode, "[0].id");
					
					YextApi yextApi = new YextApi();
					Response yextResponse = yextApi.listStores(storeId,"1");
					if (yextResponse != null && "200".equals(Integer.toString(yextResponse.getStatusCode()))) {
						logSuccessResponse(yextResponse, operationName);
						 mapper = new ObjectMapper();
						 jsonNode = mapper.readTree(yextResponse.asString());
						if (!jsonNode.isMissingNode()) {
							String address= getPathVal(jsonNode, "response.address");
							String city= getPathVal(jsonNode, "response.city");
							String state= getPathVal(jsonNode, "response.state");
							String zip= getPathVal(jsonNode, "response.zip");
							//Assert.assertEquals(streetAddress, address);
							Assert.assertEquals(addressLocality, city);
							Assert.assertEquals(addressRegion, state);
							Assert.assertEquals(postalCode, zip);
						}
					}else{
						failAndLogResponse(yextResponse, operationName);
					}
				}
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SL", "verifyStoreinTimesSquare", Group.GLOBAL,Group.SPRINT })
	public void verifyStoreinTimesSquare(ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verifyStoresinNebraska test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: get response of SLv 2.1 getStoreByAdress api call for given address.");
		Reporter.log("Step 2: Add filter for address.region with given State 'SD' in Yext entitiesList api call and get the response");
		Reporter.log("Step 3: Compare and count number of stores are equal for both response.");
		Reporter.log("================================");

		String operationName = "verifyStoreinTimesSquare";
		String addressVal = "Nebraska, NE";
		Response response = getStoreByAddress(apiTestData, addressVal, "All", "10", "1");
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				if (jsonNode.isArray()) {
					String streetAddress= getPathVal(jsonNode, "[0].location.address.streetAddress");
					String addressLocality= getPathVal(jsonNode, "[0].location.address.addressLocality");
					String addressRegion= getPathVal(jsonNode, "[0].location.address.addressRegion");
					String postalCode= getPathVal(jsonNode, "[0].location.address.postalCode");
					String storeId= getPathVal(jsonNode, "[0].id");
					
					YextApi yextApi = new YextApi();
					Response yextResponse = yextApi.listStores(storeId,"1");
					if (yextResponse != null && "200".equals(Integer.toString(yextResponse.getStatusCode()))) {
						logSuccessResponse(yextResponse, operationName);
						 mapper = new ObjectMapper();
						 jsonNode = mapper.readTree(yextResponse.asString());
						if (!jsonNode.isMissingNode()) {
							String address= getPathVal(jsonNode, "response.address");
							String city= getPathVal(jsonNode, "response.city");
							String state= getPathVal(jsonNode, "response.state");
							String zip= getPathVal(jsonNode, "response.zip");
							//Assert.assertEquals(streetAddress, address);
							Assert.assertEquals(addressLocality, city);
							Assert.assertEquals(addressRegion, state);
							Assert.assertEquals(postalCode, zip);
						}
					}else{
						failAndLogResponse(yextResponse, operationName);
					}
				}
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
}

