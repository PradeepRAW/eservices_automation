package com.tmobile.eservices.qa.payments.api;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.EOSCommonMethods;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EOSLegacybilling;

import io.restassured.response.Response;

public class EOSLegacybillTest extends EOSCommonMethods {

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEoslegacybillGetbillList(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSLegacybilling lbilling = new EOSLegacybilling();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response lbilllistresponse = lbilling.getResponselegacybillList(getjwt);

		}

	}

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEoslegacybillprintdetailbillformsisdn(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSLegacybilling lbilling = new EOSLegacybilling();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response lbillist = lbilling.getResponselegacybillList(getjwt);
			String statementid = lbilling.getbillingstatementid(lbillist);
			Response printdetailmsisdnresponse = lbilling.getResponseprintdetailbillmsisdn(getjwt, statementid);

		}

	}

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEoslegacybillprintdetailbillforban(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSLegacybilling lbilling = new EOSLegacybilling();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response lbillist = lbilling.getResponselegacybillList(getjwt);
			String statementid = lbilling.getbillingstatementid(lbillist);
			Response printdetailbanresponse = lbilling.getResponseprintdetailbillban(getjwt, statementid);

		}

	}

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEoslegacybillprintsummarybillformsisdn(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSLegacybilling lbilling = new EOSLegacybilling();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response lbillist = lbilling.getResponselegacybillList(getjwt);
			String statementid = lbilling.getbillingstatementid(lbillist);
			Response printsummarymsisdnresponse = lbilling.getResponseprintsummarybillmsisdn(getjwt, statementid);

		}

	}

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testEoslegacybillprintsummarybillforban(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSLegacybilling lbilling = new EOSLegacybilling();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			Response lbillist = lbilling.getResponselegacybillList(getjwt);
			String statementid = lbilling.getbillingstatementid(lbillist);
			Response printsummarybanresponse = lbilling.getResponseprintsummarybillban(getjwt, statementid);

		}

	}

}