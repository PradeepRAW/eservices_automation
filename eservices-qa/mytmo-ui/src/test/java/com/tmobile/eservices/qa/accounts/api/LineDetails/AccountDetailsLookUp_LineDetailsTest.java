package com.tmobile.eservices.qa.accounts.api.LineDetails;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.accounts.api.LineDetails.responses.line_details.Line;
import com.tmobile.eservices.qa.accounts.api.LineDetails.responses.line_details.LineDetails;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;
import rp.com.google.common.collect.Maps;

public class AccountDetailsLookUp_LineDetailsTest extends AccountsApi {
	public Map<String, String> tokenMap;

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "testMultipleLineDetails", Group.ACCOUNTS,
			Group.PENDING })
	public void testMultipleLineDetailsInTheAccount(ApiTestData apiTestData) throws Exception {

		Reporter.log(
				"Note: The API will provide the details imformation about the lines like name,msisdn,model,imei,image,productType,isLineSuspended in Account overview page");
		Reporter.log("Test data : Account with multiple lines");
		Response response = getLineDetails(apiTestData);
		String operationName = "testMultipleLineDetailsInTheAccount";
		Reporter.log("Test Case : Validating Lines information in the account");

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			LineDetails data = response.as(LineDetails.class);
			for (Line line : data.getLines()) {
				assertThat(line.getMsisdn(), notNullValue());
				assertThat(line.getProductType(), notNullValue());
				assertThat(line.getLineSuspended(), notNullValue());
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	public Response getLineDetails(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildAccountDetailsLookUpAPIHeader(apiTestData, Maps.newHashMap());
		String resourceURL = "v1/accountdetailslookup/linedetails";
		Response response = invokeRestServiceCall(headers, "https://eos.corporate.t-mobile.com", resourceURL, "",
				RestServiceCallType.GET);
		return response;

		/*
		 * Response response = RestAssured.given() .headers(headers)
		 * .baseUri("https://eos.corporate.t-mobile.com")
		 * .get("/v1/accountdetailslookup/linedetails"); return response;
		 */
	}

	public Map<String, String> buildAccountDetailsLookUpAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("transactionId", "123");
		headers.put("ban", apiTestData.getBan());
		headers.put("correlationId", "123");
		// Time being it is Hardcode. Since it is mock response
		// headers.put("Authorization","eyJ0eXAiOiJKV0UiLCJhbGciOiJSU0ExXzUiLCJlbmMiOiJBMjU2Q0JDK0hTNTEyIn0.g8QdfCqum8dhG_3LRyNCkhitiWgTzTUkEzIHglUA09xlGPSOJfoVQ2o2snL4sOBcO3NbKbFQZbMdImkHDedD7s_OBAuR7nLUmizNVTXe1G2EQl5oHqr6I6IIzrXkQwwdqffiO-HfQ--tPhyDyvOpYWSEtiWpGuAxn2d5b-TVkF-CdW6vvFcQMOonetlnfFQEv7SnOXAI-les6CUoVIQr9FaeHhuZMdlFkLNMF7AtN4yjqT4LK-3KOUuAXVLj9_KkibcAwlKmX3WLqrM9uEJ2pzWSZyibAjSmT1PWiVx80TTg4crTPwb_5ISIE1fpjMn7Ff6GK1b4FTbBMIxIHpAO5g.X8R243DG2A8b5LuF6uMe0Q.UFmURqVL5f5UpK6OWdSvoRQJq6oq6t3gGE7lDsF2gr8kNHMz3ZlN_uwRj3IMcSNXhHFch_11vrsJpVcWEGUA3w.kMCygS6_haiwQjQz_tL3S9yYb3BeFiJGdomwzutPJKwBGRG4-BK9eQIUxH8QHF2EZdVAzA-ZMfwVURa53cNJoA");
		headers.put("Authorization", getAccessToken());
		headers.put("applicationId", "123");
		headers.put("channelId", "123");
		headers.put("channelVersion", "123");
		headers.put("clientId", "123");
		headers.put("transactionBusinessKey", "123");
		headers.put("transactionBusinessKeyType", "123");
		headers.put("transactionType", "123");
		return headers;
	}

}
