package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.ChargeFeaturesPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class ChargeFeaturesPageTest extends PaymentCommonLib{

    private static final Logger logger = LoggerFactory.getLogger(ChargeFeaturesPageTest.class);
	
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT})
    public void testChargeFeaturesPageLoad(ControlTestData data, MyTmoData myTmoData) {
        logger.info("testChargeFeaturesPageLoad method called in ChargeFeaturesPageTest");
        Reporter.log("Billing - Verify Charge Features Page");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("5. Click on Services | Charge Features page should be loaded");
        Reporter.log("================================");
        
     	navigateToChargeFeaturesPage(myTmoData,"Services");
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT})
    public void testServicesAddedForChargeFeaturesPage(ControlTestData data, MyTmoData myTmoData) {
        logger.info("testServicesAddedForChargeFeaturesPage method called in ChargeFeaturesPageTest");
        Reporter.log("Billing - Verify Service Details In Charge Features Page");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("5. Click on Services | Charge Features page should be loaded");
        Reporter.log("6. Check for the Service details added to the line | Should be able to find the service details");
        Reporter.log("================================");
       
        ChargeFeaturesPage chargeFeaturesPage =navigateToChargeFeaturesPage(myTmoData, "Services");
        chargeFeaturesPage.verifyServiceDetailsofAllLines();  
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT})
    public void testViewTaxesAndFeesLinkOnChargeFeaturesPage(ControlTestData data, MyTmoData myTmoData) {
        logger.info("testViewTaxesAndFeesLinkOnChargeFeaturesPage method called in ChargeFeaturesPageTest");
        Reporter.log("Billing - Verify Taxes And Fees Details In Charge Features Page");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("5. Click on Services | Charge Features page should be loaded");
        Reporter.log("6. Click on view included taxes and fees link | Should be able to view the link");
        Reporter.log("================================");
        
        ChargeFeaturesPage chargeFeaturesPage =navigateToChargeFeaturesPage(myTmoData, "Services");
		chargeFeaturesPage.clickViewInlcudedTaxesFeesLink();
		chargeFeaturesPage.verifyIncludedTaxesAndFeesModal();   
    }
    
   /* @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING})
    public void verifySliderActionsInChargeFeaturesPage(ControlTestData data, MyTmoData myTmoData) {
        logger.info("verifySliderActionsInChargeFeaturesPage method called in ChargeFeaturesPageTest");
        Reporter.log("Billing - Verify Slider actions In Charge Features Page");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("5. Click on Services | Charge Features page should be loaded");
        Reporter.log("6. Verify the slider actions | Should be able to slide to left/right");
        Reporter.log("================================");
        
        ChargeFeaturesPage chargeFeaturesPage =navigateToChargeFeaturesPage(myTmoData, "Services");
        chargeFeaturesPage.verifyPageSliderActions();
    }*/
}
