package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.global.MobileLandingPage;

/**
 * @author rnallamilli
 * 
 */
public class MobileLandingPageTest extends GlobalCommonLib {

	/**
	 * US539555: TMO - Integrate with additional 3rd party API for content | Testing follow up
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RELEASE_READY})
	public void verifyMobileLandingPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBlockPageInternationalRoamingToggle - Proflie-Blocking link-International Roaming-on/off ");
		Reporter.log("Test Case : Verify Blocking International roaming ");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch mobile landing page url | Mobile landing page url should be launched");
		Reporter.log("2. Verify mobile landing page | Mobile landing page should be displayed");
		Reporter.log("3. Click menu icon | Click on menu should be success");
		Reporter.log("4. Verify terms of use link | Terms of use link should be displayed");
		Reporter.log("5. Verify privacy policy link | Privacy policy link should be displayed");
		Reporter.log("6. Verify internet based ads link | Internet based ads link should be displayed");
		
		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		MobileLandingPage mobileLandingPage= new MobileLandingPage(getDriver());
		mobileLandingPage.launchMobileLandingPage(myTmoData);
		mobileLandingPage.clickMenuIcon();
		mobileLandingPage.verifyTermsOfUseLink();
		mobileLandingPage.verifyPrivacyPolicyLink();
		mobileLandingPage.verifyInternetBasedAdsLink();
	}
}
