package com.tmobile.eservices.qa.accessibility;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.CartPage;
import com.tmobile.eservices.qa.pages.tmng.functional.CheckOutPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.SDPPage;
import com.tmobile.eservices.qa.pages.tmng.functional.SLPPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPManualorFraudReviewPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPNoOfferPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPOfferPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPPreScreenForm;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class TmngPages extends TmngCommonLib {
	AccessibilityCommonLib accessibilityCommonLib = new AccessibilityCommonLib();

	/***
	 * Test Accessibility for Home Page
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testHomePageTmng(ControlTestData data, TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "HomePageTmng");
	}

	/***
	 * Test Accessibility for Phones Page
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testMainDevicePLP(ControlTestData data, TMNGData tMNGData) {
		navigateToPhonesPlpPage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "MainDevicePLP");
	}

	/***
	 * Test Accessibility for Main Device PDP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testMainDevicePDP(ControlTestData data, TMNGData tMNGData) {
		navigateToPhonesPDP(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Main Device PDP");
	}

	/***
	 * Test Accessibility for Accessory PLP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAccessoryMainPLP(ControlTestData data, TMNGData tMNGData) {
		navigateToAccessoryPLP(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Accessory PLP Page");
	}

	/***
	 * Test Accessibility for Accessory PDP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAccessoryMainPDP(ControlTestData data, TMNGData tMNGData) {
		navigateToAccessoryMainPDP(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Accessory Main PDP Page");
	}

	/***
	 * Test Tablets and Devices PLP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testTabletsAndDevicesPLP(ControlTestData data, TMNGData tMNGData) {
		navigateToTabletsAndDevicesPLP(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Tablets and Devices PLP");
	}

	/***
	 * Test Accessibility for Cart Page
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testCartPage(ControlTestData data, TMNGData tMNGData) {
		navigateToCartPageBySelectingPhone(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "CartPage");
	}

	/***
	 * Test Accessibility for Check Out Page
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testCheckoutPage(ControlTestData data, TMNGData tMNGData) {
		navigateToCheckoutWithPhone(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Checkoutpage");
	}

	/***
	 * Test Accessibility for BringYourOwnPhonePage
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testBringYourOwnPhonePage(ControlTestData data, TMNGData tMNGData) {
		navigateToBringYourOwnPhonePage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "BringYourOwnPhonePage");
	}

	/***
	 * Test Accessibility for Deals Page
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testDealsPage(ControlTestData data, TMNGData tMNGData) {
		navigateToDealsPage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "DealsPage");
	}

	/***
	 * Test Accessibility for testDigitsPage
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testDigitsPage(ControlTestData data, TMNGData tMNGData) {
		navigateToDigitsPage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "DigitsPage");
	}

	/***
	 * Test Accessibility for International Calling Page
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testInternationalCallingPage(ControlTestData data, TMNGData tMNGData) {
		navigateToInternationalCallingPage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "InternationalCallingPage");
	}

	/***
	 * Test Accessibility for Plans Page
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testPlansPage(ControlTestData data, TMNGData tMNGData) {
		navigateToPlansPage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "PlansPage");
	}

	/***
	 * Test Accessibility for SmartDevicesPage
	 * 
	 * @param data
	 * @param tMNGData
	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
//	public void testSmartDevicesPage(ControlTestData data, TMNGData tMNGData) {
//		navigateToSmartDevicesPage(tMNGData);
//		Reporter.log("==============================");
//		Reporter.log("Navigated to desired page");
//		accessibilityCommonLib.testAccessibility(getDriver(), "SmartDevicesPage");
//	}

	/***
	 * Test Accessibility for SmartDevicesPage
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testSwitchToTmobilePage(ControlTestData data, TMNGData tMNGData) {
		navigateToSwitchToTmobilePage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "SwitchToTmobilePage");
	}

	/***
	 * Test Accessibility for SmartDevicesPage
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testTravelAbroadPage(ControlTestData data, TMNGData tMNGData) {
		navigateToTravelAbroadPage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TravelAbroadPage");
	}

	/***
	 * Test Accessibility for SmartDevicesPage
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testUnlimitedPlansPage(ControlTestData data, TMNGData tMNGData) {
		navigateToUnlimitedPlansPage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "UnlimitedPlansPage");
	}

	/***
	 * Test Accessibility for SmartDevicesPage
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testWatchsandTabletsPage(ControlTestData data, TMNGData tMNGData) {
		navigateToTabletsAndDevicesPLP(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "WatchandTabletsPage");
	}

	/***
	 * Test Accessibility for Accessory PLP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAccessoriesHubPage(ControlTestData data, TMNGData tMNGData) {
		navigateToAccessoriesHubPage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "AccessoryHubPage");
	}

	/***
	 * Test Accessibility for We’ll help you join Page
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testWeWillHelpYouJoinPage(ControlTestData data, TMNGData tMNGData) {
		navigateToWeWillHelpYouToJoinUsPage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "WeWillHelpYouJoinUsPage");
	}

	/***
	 * Test Accessibility for CheckOutTheCoveragePage
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testCheckOutTheCoveragePage(ControlTestData data, TMNGData tMNGData) {
		navigateToCheckOutTheCoveragePage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "CheckOutTheCoveragePage");
	}

	/***
	 * Test Accessibility for store locator page
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testStoreLocatorPage(ControlTestData data, TMNGData tMNGData) {
		navigateToStoreLocatorPage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "StoreLocatorPage");
	}

	/***
	 * Test Accessibility for EmptyCartPage
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testEmptyCartPage(ControlTestData data, TMNGData tMNGData) {
		navigateToEmptyCart(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "EmptyCartPage");
	}

	/***
	 * Test Accessibility for BYOD carttPage
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testBYODInCartPage(ControlTestData data, TMNGData tMNGData) {
		// navigateToBringYourOwnDevicePdpFromEmptyCart(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "CartPage BYOD Modal");
	}

	/***
	 * Test Accessibility for OrderDetailed page
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testOrderdetailedPage(ControlTestData data, TMNGData tMNGData) {
		navigateToOrderDetailedPagefromCart(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Order Detailed Model");
	}

	/***
	 * Test Accessibility for web Save Cart model
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testWebSaveCartModel(ControlTestData data, TMNGData tMNGData) {
		navigateToWebSaveCartModalfromCart(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Web Save Cart Model");
	}

	/***
	 * Test Accessibility for Retail Save Cart model
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testRetailSaveCartModel(ControlTestData data, TMNGData tMNGData) {
		navigateToRetailSaveCartModalfromCart(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Retail Save Cart Model");
	}

	/***
	 * Test Accessibility for StoreDetail model from PDP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testStoreDetailModelFromPDP(ControlTestData data, TMNGData tMNGData) {
		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.verifyStoreLocatorName();
		phonesPdpPage.clickStoreLocatorName();
		SDPPage sdpPage = new SDPPage(getDriver());
		sdpPage.verifyStoredetailsPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Store Detailed page Model from PDP");
	}

	/***
	 * Test Accessibility for StoreDetail model from Accessory PDP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testStoreDetailModelFromAccessoryPDP(ControlTestData data, TMNGData tMNGData) {
		PdpPage accessoryPdpPage = navigateToAccessoryMainPDP(tMNGData);
		accessoryPdpPage.verifyStoreLocatorName();
		accessoryPdpPage.clickStoreLocatorName();
		SDPPage sdpPage = new SDPPage(getDriver());
		sdpPage.verifyStoredetailsPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Store Detailed page Model from PDP");
	}

	/***
	 * Test Accessibility for StoreDetail model from Cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testStoreDetailModelFromCart(ControlTestData data, TMNGData tMNGData) {
		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickStoreLocatorName();
		SDPPage sdpPage = new SDPPage(getDriver());
		sdpPage.verifyStoreMapPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Store Detailed page Model from Cart");
	}

	/***
	 * Test Accessibility for Store List modal in Accessory Main PDP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testStoreListModalAccessoryMainPDP(ControlTestData data, TMNGData tMNGData) {
		PdpPage accessoryDetailsPage = navigateToAccessoryMainPDP(tMNGData);
		accessoryDetailsPage.clickStoreLocatorName();
		SLPPage slpPage = new SLPPage(getDriver());
		slpPage.verifyStoreLocatorListPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Store List page Model in Accessory Main PDP");
	}

	/***
	 * Test Accessibility for Store List modal in Mini PDP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testStoreListModalCartPage(ControlTestData data, TMNGData tMNGData) {
		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickChangeStoreLocation();
		SLPPage slpPage = new SLPPage(getDriver());

		slpPage.verifyStoreLocatorListPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Store List page Modal in Cart page");
	}

	/***
	 * Test Accessibility for Store List modal in multi cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testStoreListModalOnMultiCartPage(ControlTestData data, TMNGData tMNGData) {

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.clickChangeStoreLocation();
		SLPPage slpPage = new SLPPage(getDriver());

		slpPage.verifyStoreLocatorListPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Store List page Modal in Multi Cart page");
	}

	/***
	 * Test Accessibility for Store List modal in multi cart for Accessory
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testStoreListModalOnMultiCartPageForAccessory(ControlTestData data, TMNGData tMNGData) {

		CartPage cartPage = navigateToCartPageBySelectingAccessory(tMNGData);
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.verifyChangeStoreLocatorCTAOnCartPage();
		cartPage.clickChangeStoreLocation();
		SLPPage slpPage = new SLPPage(getDriver());
		slpPage.verifyStoreLocatorListPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Store List page Modal in Multi Cart page");
	}

	/***
	 * Test Accessibility for Store List modal in cart for Accessory
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testStoreListModalOnCartPageForAccessory(ControlTestData data, TMNGData tMNGData) {
		PdpPage accessoryDetailsPage = navigateToAccessoryMainPDP(tMNGData);
		accessoryDetailsPage.clickOnAddToCartBtn();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifyChangeStoreLocatorCTAOnCartPage();
		cartPage.clickChangeStoreLocation();
		SLPPage slpPage = new SLPPage(getDriver());
		slpPage.verifyStoreLocatorListPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Store List page Modal in Cart page");
	}

	/***
	 * Test Accessibility for Store Detail modal in multi cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testStoreDetailModalOnMultiCartPage(ControlTestData data, TMNGData tMNGData) {

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.clickStoreLocatorName();
		SDPPage sdpPage = new SDPPage(getDriver());
		sdpPage.verifyStoredetailsPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Store Detailed page Modal in Multi Cart page");
	}

	/***
	 * Test Accessibility for StoreDetail page from Accessory Cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testStoreDetailPageFromAccessoryCart(ControlTestData data, TMNGData tMNGData) {
		PdpPage accessoryDetailsPage = navigateToAccessoryMainPDP(tMNGData);
		accessoryDetailsPage.clickOnAddToCartBtn();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickStoreLocatorName();
		SDPPage sdpPage = new SDPPage(getDriver());
		sdpPage.verifyStoredetailsPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to SDP page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Store Detailed page Model from Accessory Cart page");
	}

	/***
	 * Test Accessibility for Accessory Cart Page
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAccessoryCartPage(ControlTestData data, TMNGData tMNGData) {
		PdpPage accessoryDetailsPage = navigateToAccessoryMainPDP(tMNGData);
		accessoryDetailsPage.clickOnAddToCartBtn();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Accessory Cart page");
	}

	/***
	 * US540768 :TMO Essentials HTML Integration
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testTMOEssentialCart(ControlTestData data, TMNGData tMNGData) {
		navigateToCartPageBySelectingPhone(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TMO Essentials HTML Integration");
	}

	/***
	 * US540768 :TMO Essentials HTML Integration
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testTMOEssentialCartFromEmptyCart(ControlTestData data, TMNGData tMNGData) {
		CartPage emptyCartPage = navigateToEmptyCart(tMNGData);
		emptyCartPage.addAPhonetoCartFromCartPage(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifyStickyBannerDisplayed();

		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TMO Essentials HTML Integration");
	}

	/***
	 * US565627 : TPP - Accessibility - Ensure Prescreen/Hard Credit Check Pages
	 * meet accessibility requirements
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testPrescreenIntro(ControlTestData data, TMNGData tMNGData) {
		navigateToTPPPreScreenIntro(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TPP Prescreen Intro");
	}

	/***
	 * US565627 : TPP - Accessibility - Ensure Prescreen/Hard Credit Check Pages
	 * meet accessibility requirements
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testPrescreenForm(ControlTestData data, TMNGData tMNGData) {
		navigateToTPPPreScreenFormPage(tMNGData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TPP Prescreen Form");
	}

	/***
	 * US565627 : TPP - Accessibility - Ensure Prescreen/Hard Credit Check Pages
	 * meet accessibility requirements
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testPrescreenOfferPage(ControlTestData data, TMNGData tMNGData) {
		navigateToTPPPreScreenFormPage(tMNGData);
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();

		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();

		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TPP Offer page");
	}

	/***
	 * US565627 : TPP - Accessibility - Ensure Prescreen/Hard Credit Check Pages
	 * meet accessibility requirements
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPrescreenNoOfferPage(ControlTestData data, TMNGData tMNGData) {
		navigateToTPPPreScreenFormPage(tMNGData);
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();

		TPPNoOfferPage tppNoOfferPage = new TPPNoOfferPage(getDriver());
		tppNoOfferPage.verifyPrescreenNoOfferPage();

		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TPP No Offer page");
	}

	/***
	 * US565627 : TPP - Accessibility - Ensure Prescreen/Hard Credit Check Pages
	 * meet accessibility requirements
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPrescreenExistingCustomerModal(ControlTestData data, TMNGData tMNGData) {
		navigateToTPPPreScreenFormPage(tMNGData);
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppPreScreenForm.verifyExistingCustomerNotifyModalHeader();

		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TPP Existing Customer Modal");
	}

	/***
	 * US565627 : TPP - Accessibility - Ensure Prescreen/Hard Credit Check Pages
	 * meet accessibility requirements
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPrescreenManualReviewPage(ControlTestData data, TMNGData tMNGData) {
		navigateToTPPPreScreenFormPage(tMNGData);
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();

		TPPManualorFraudReviewPage tppManualorFraudReviewPage = new TPPManualorFraudReviewPage(getDriver());
		tppManualorFraudReviewPage.verifyManualRevieworFraudDetectionPage();

		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TPP Manual or Fraud Review Page");
	}

	/***
	 * US565627 : TPP - Accessibility - Ensure Prescreen/Hard Credit Check Pages
	 * meet accessibility requirements
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testHCCFlow(ControlTestData data, TMNGData tMNGData) {
		navigateToTPPHCCForm(tMNGData);
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyLetsCheckYourCreditHeader();

		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TPP HCC Page");
	}

	/***
	 * US578282 :TMNG - Show dynamic messaging to switch from Magenta to Magnenta+
	 * rate plan in Cart banner C632156 : Validate the Banner text is Accessibility
	 * compliant
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAuthorableBannerWhenUserSelectPlusUpServicesForTwoVoicesLines(ControlTestData data,
			TMNGData tMNGData) {

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickFirstAddOn();
		cartPage.verifyDynamicMessageForSwitchPlanInCartBannerNotDisplayed();
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
// 		cartPage.verifyMagentaPlanSelected();
// 		cartPage.verifyDefaultAddOnsForMegentaPlan();
		cartPage.clickFirstAddOnForSecondLine();
		cartPage.verifyDynamicMessageForSwitchPlanInCartBanner();
		accessibilityCommonLib.testAccessibility(getDriver(), "CartPage Banner");
	}

}
