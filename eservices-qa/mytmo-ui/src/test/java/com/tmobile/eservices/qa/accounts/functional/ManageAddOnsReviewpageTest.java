package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsReviewPage;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsSelectionPage;
import com.tmobile.eservices.qa.pages.accounts.ODFDataPassReviewPage;

import io.appium.java_client.AppiumDriver;

public class ManageAddOnsReviewpageTest extends AccountsCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(ManageAddOnsReviewpageTest.class);

	@Test(dataProvider = "byColumnName", enabled = true, groups = "TESTSPRINT")
	public void verifyMessagingWhenErrorHappensWithComponentOutsideOfBenefitsRedemptionPage(ControlTestData data,
			MyTmoData myTmoData) {

		Reporter.log("Test Case  Name : Verify ODF Review Page Change Date");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Change Netflix soc");
		Reporter.log("7. Go to Review page of Manage Add-On after selecting to add a Netflix SOC");
		Reporter.log("8. clicked on Agree and Submit button");
		Reporter.log("9. Verify authorable text");
		Reporter.log(
				"10. Verify  magenta button with the authorable text Go to Plan Benefits AND the authorable link for the button goes to the Plan Details page (/plan-details)");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData,
				"odf/Service:FAMNFX9,FAMNFX13,1SNFLXSD,2SNFLXHD,4SNFLXHD,2SNFLXHDP,4SNFLXHDP,1SSDNFLX,HD2SNFLX,HD4SNFLX,2SPHDNFLX,4SPHDNFLX");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.addOrRemoveNetflixSocs("Netflix Standard plus Family Allowances ($18 value)",
				"Netflix Premium plus Family Allowances ($21 value)");

	}

	/**
	 * US272418#Reusable Calendar modal build out US279512#Selection page - Spike to
	 * research and define persistence US279599#Review page - Spike: implementation
	 * of authored content and enable clicking above the CTAs
	 * 
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyODFReviewPageChangeDate(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case  Name : Verify ODF Review Page Change Date");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7.Click on any Service or data and naviate to review Page | Review page should be displayed");
		Reporter.log("8.Click on Change Date Link | Calendar should be displayed");
		Reporter.log("9.Verify option Starts Immendiately | Option should be displayed");
		Reporter.log("10.Click on Pick a date option and check calendar | Calendar should be displayed");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToODFDataPassReviewPage(myTmoData, "");
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickChangeDateLink();
		oDFDataPassReviewPage.verifyCalenderTitle("Effective Date");
		oDFDataPassReviewPage.verifyImmediately("Immediately");
		oDFDataPassReviewPage.verifyPickaDate("Pick a date");
		oDFDataPassReviewPage.verifyTextBelowImmediatelyTitle("These changes will begin immediately");
		oDFDataPassReviewPage.clickPickUpDateRadioButton();
		oDFDataPassReviewPage.pickupdateselectbutton();
		oDFDataPassReviewPage.pickADateTitle("Pick a Date");
		oDFDataPassReviewPage.clickOnSelectInCalenderPage();
		verifyODFDataPassReviewPage();
	}

	/**
	 * US272418#Reusable Calendar modal build out US279512#Selection page - Spike to
	 * research and define persistence US279599#Review page - Spike: implementation
	 * of authored content and enable clicking above the CTAs
	 * 
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyODFReviewPageChangeDateforServices(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case  Name : Verify ODF Review Page Change Date");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7.Click on any Service or data and naviate to review Page | Review page should be displayed");
		Reporter.log("8.Click on Change Date Link | Calendar should be displayed");
		Reporter.log("9.Verify option Starts Immendiately | Option should be displayed");
		Reporter.log("10.Click on Pick a date option and check calendar | Calendar should be displayed");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToODFReviewPageFlowByCheckBoxname(myTmoData, "Family Allowances");
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickChangeDateLink();
		oDFDataPassReviewPage.verifyCalenderTitle("Effective Date");
		oDFDataPassReviewPage.verifyToday("Today");
		oDFDataPassReviewPage.verifyPickaDate("Next bill cycle");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "xxx")
	public void verifyODFReviewPageChangeDateCancelandSelectButtonsInEffectiveDatePopUp(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyODFReviewPageChangeDateCancelandSelectButtonsInEffectiveDatePopUp method called in ManageAddOnsReviewpageTest");
		Reporter.log(
				"Test Case  Name : Verify Cancel and Continue button functionality on Calendar of ODF Review page");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7.Click on any Service or data and naviate to review Page");
		Reporter.log("7.Verify Cancel and Continue Buttons logic in Effective Date Popup");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnCheckBox("McAfee® Security for T-Mobile");
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickChangeDateLink();
		oDFDataPassReviewPage.verifyCalenderTitle("Effective Date");
		oDFDataPassReviewPage.clickOverlayCancelButton();
		oDFDataPassReviewPage.clickChangeDateLink();
		oDFDataPassReviewPage.clickOverlaySelectButton();
		verifyODFDataPassReviewPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "xxx")
	public void verifyODFReviewPageChangeDateSelectCalendarNextDate(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyODFReviewPageChangeDateSelectCalendarNextDate method called in ManageAddOnsReviewpageTest");
		Reporter.log("Test Case  Name : Verify Cancel and Continue button logic on Calendar of ODF Review page");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7.Click on any Service or data and naviate to review Page");
		Reporter.log("7.Verify Cancel and Continue Buttons logic in Effective Date Popup");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnCheckBox("Scam ID");
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		String effectiveDateBeforeChange = oDFDataPassReviewPage.getEffectiveDate();
		oDFDataPassReviewPage.clickChangeDateLink();
		oDFDataPassReviewPage.verifyCalenderTitle("Effective Date");
		oDFDataPassReviewPage.clickPickUpDateRadioButton();
		oDFDataPassReviewPage.selectNextDate();
		oDFDataPassReviewPage.clickOverlaySelectButton();
		Assert.assertFalse(oDFDataPassReviewPage.getEffectiveDate().contains(effectiveDateBeforeChange),
				"Effective Date is not changed");
		verifyODFDataPassReviewPage();
	}

	/**
	 * US217043#Review Add-Ons - Cancel multiple add on changes CTA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyAddOnReviewPageCancelCTAFunctionalityForService(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnReviewPageCancelCTAFunctionalityForService method called in ManageAddOnsReviewpageTest");
		Reporter.log("Test Case Name : Verify ODF review page Cancel button for services");
		Reporter.log("Test Data :  Any PAH/Full/Standard User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"6. Choose Any Service from service section (Select Device Block/KickBack™ service) and Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"7. Click on Cancel CTA on Review Page and check redirection. | User should be redirected to Plans landing page (plans.html)");
		Reporter.log("8. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"9. Verify Upgrade Your Services page | Upgrade Your Services page should be displayed and selections which made in step6 should not persist");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnCheckBox("");
		// manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		oDFDataPassReviewPageCancelVerification(myTmoData);
	}

	/**
	 * US217043#Review Add-Ons - Cancel multiple add on changes CTA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyAddOnReviewPageCancelCTAFunctionalityForDataPlanAndService(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnReviewPageCancelCTAFunctionalityForDataPlanAndService method called in ManageAddOnsReviewpageTest");
		Reporter.log("Test Case  Name : Verify Review Page Cancel CTA Functionality For Data Plan and Services");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Choose any data plan (Select T-Mobile ONE TE) | User should be able to select Data Plan.");
		Reporter.log(
				"7. Choose Any Service from service section (Select KickBack™) and Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"8. Click on Cancel CTA on Review Page and check redirection. | User should be redirected to Plans landing page.");
		Reporter.log("9. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"10. Verify Upgrade Your Services page | Upgrade Your Services page should be displayed and selections which made in step6 & 7 should not persist");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToODFReviewPageWithServiceAndDataPlan(myTmoData);
		oDFDataPassReviewPageCancelVerification(myTmoData);
	}

	/**
	 * US263121#Review Add-Ons - Effective date for add on data plan - Down grade
	 * US260260 --> Review Add-Ons - Monthly price decrease
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyAddOnDataPlanDowngradeChangeDateFunctionality(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAddOnDataPlanDowngradeChangeDateFunctionality method called in ManageAddOnsReviewpageTest");
		Reporter.log("Test Case  Name : Verify downgrade data plan");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"6. Read Currently Active Data Plan name and Price | User should be able to read Data plan name and Price");
		Reporter.log(
				"7. Choose data plan (here we have to choose lower data plan than currently active data plan). SELECT T-Mobile ONE TE | User should be able to select data plan");
		Reporter.log(
				"8. Read Name and price of selected data plan and go to Review Page | User should be able to select data plan and redirect to Review page");
		Reporter.log(
				"9. Calcuate price difference between currently selected and active data plans(If active is Free then treat it as $0. | User should be able to calculate difference");
		Reporter.log(
				"10. On Review Page, Verify Price under Monthly Price section. | It should be displayed as 'Your monthly bill will decrease by $xx.xx'. ($xx.xx should be calculated price from Step9, just remove -ve sign)");
		Reporter.log(
				"11. On Review page, Verify Price under Monthly Price section. | Price should be selected data plan price (data plan selected in step7");
		Reporter.log(
				"12. Check Data plan name and price of the data plan under 'New Add-Ons' section | Selected data plan name and price (from step7) should be displayed and it should be matched.");
		Reporter.log(
				"13. Check for Change Date link and effective date for Add-on item. | Change Date link should be displayed.");
		Reporter.log(
				"14. Check for Effective Date for Data Plan which is being added. | Effective date should be Next Bill Cycle");
		Reporter.log(
				"15. Check Data plan name and price of the data plan under 'Removable Item' section | Selected data plan name and price (from step6) should be displayed and it should be matched.");
		Reporter.log(
				"16. Check for 'Change date' link under 'Removable Item' section and check Effective Date for Data Plan which is being removed. | 'Change date' link should not be displayed and Effective date should be Next Bill Cycle.");
		Reporter.log(
				"17. Check for Effective Date for Data Plan which is under 'Add-On' section and Data Plan under 'Removable Item' section. | Effective date should be Next Bill Cycle date and both dates should match.");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToODFDecreaseReviewPage(myTmoData);
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyMonthlyBillDecrease();
	}

	/**
	 * US270325#Date logic - Effective date options - Data plan upgrade
	 * US217075#Review Add Ons - Tax display for TI vs. TE
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, Group.MULTIREG })
	public void verifyAddOnDataPlanUpgradeChangeDateFunctionality(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyAddOnDataPlanUpgradeChangeDateFunctionality method called in ManageAddOnsReviewpageTest");
		Reporter.log("Test Case  Name : Verify Upgrade data plan");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");

		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"6. Read Currently Active Data Plan name and Price | User should be able to read Data plan name and Price");
		Reporter.log(
				"7. Choose data plan (here we have to choose higher data plan than currently active data plan. Select ONE PLUS) | User should be able to select data plan");
		Reporter.log(
				"8. Read Name and price of selected data plan and go to Review Page | User should be able to select data plan and redirect to Review page");
		Reporter.log(
				"9. Calcuate price difference between currently selected and active data plans(If active is Free then treat it as $0. | User should be able to calculate difference");
		Reporter.log(
				"10. Verify Price under Monthly Price section. | It should be displayed as 'Your monthly bill will increase by $xx.xx'(here $xx.xx value should be value calculated in Step9)");
		Reporter.log(
				"11. Verify Price under Monthly Price section. | Price should be, calculated difference price (Price calculated in step9).Also Price should not be displayed in decimal format '$xx.xx'.");
		Reporter.log(
				"12. Verify Effective date of AddOn Item and Removed Item | Effective should be equal to Current Bill cycle date.");
		Reporter.log(
				"13. Click on Change Date Link and Check Current bill cycle date Option | Current BIll cycle date should be displayed");
		Reporter.log("14. Click on next bill cycle and click on cancel | User should be returned to Review page ");
		Reporter.log(
				"15. Verify Effective date on Review page has Current bill cycle | Effective date should be Current Bill cycle date");
		Reporter.log(
				"16. Click on change date and select pick a date and choose next bill cycle and click on select button | Next Bill cycle date should be displayed.");
		Reporter.log("17. Verify Review page has next bill cycle | Effective date should be Next Bill cycle date");
		Reporter.log(
				"18. Click on Change date link and select Immediate (1st radio button) and click cancel button | User should be returned to Review page");
		Reporter.log(
				"19. Verify Next Bill Cycle on Review Page. | Next Bill cycle should be displayed on Review page.");

		String selectedPlanPrice = navigateToODFReviewPageFlow(myTmoData, "Radio");

		String locatorType = selectedPlanPrice.substring(0, selectedPlanPrice.indexOf('.')).replaceAll("[$,]", "");
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyMonthlyBillIncrease("Your monthly cost will increase by");
	}

	/**
	 * US250850#Review Add-Ons - Monthly price increase This flow for checking
	 * Increase in value on Review page for Data Pass
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, Group.MULTIREG })
	public void verifyAddOnDataPassIncreasePriceFunctionality(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAddOnDataPassIncreasePriceFunctionality method called in ManageAddOnsReviewpageTest");
		Reporter.log("Test Case  Name : Verify Increase Price functionality when user selects Add Pass");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log(
				"7. Choose Any Data Pass (Select mobile hotspot or Day Pass 512 MB Data and Unlimited Voice 24hrs) and note down price and Continue to Review Page | Review page should be displayed with Selected Service ");
		Reporter.log(
				"8. Verify Your monthly bill will increase by | Your monthly bill will increase by text should be displayed");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToODFIncreaseReviewPage(myTmoData);

		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyMonthlyBillIncrease("Your monthly cost will increase by");
	}

	/**
	 * US260262#Review Add-Ons - No change in monthly price This flow for checking
	 * No Monthly changes in value on Review page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void verifyAddOnReviewPageNoPriceChangeFunctionalityForDataPlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnReviewPageNoPriceChangeFunctionalityForDataPlan method called in ManageAddOnsReviewpageTest");
		Reporter.log("Test Case  Name : Verify No Price change functionality for data plan");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7. Select Data plan (Select No Data which is Free) | User should be able to select Data Plan.");
		Reporter.log("8. Click on Continue to Review page | User should be able to go to Review page.");
		Reporter.log(
				"9. Verify Price under Monthly Price section. | It should be displayed as There is no change to your monthly bill");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToODFFreeRadioButtonToReviewPage(myTmoData, "Service");
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyMonthlyBillNoChange("There is no change to your monthly cost");
	}

	/**
	 * US260262#Review Add-Ons - No change in monthly price This flow for checking
	 * No Monthly changes in value on Review page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "xxx")
	public void verifyAddOnReviewPageNoPriceChangeFunctionalityForServices(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnReviewPageNoPriceChangeFunctionalityForServices method called in ManageAddOnsReviewpageTest");
		Reporter.log("Test Case  Name : Verify Review Page No Price Change Functionality For Services");
		Reporter.log("Test Data : Any PAH/Full/Standard User with TMO One Plans");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service | Upgrade your Service Page should be displayed");
		Reporter.log(
				"7. Uncheck Checkbox for Currently Active service (Voicemail to Text - Active). | User should be able to unselect active service");
		Reporter.log(
				"8. Choose any other Service with same cost (Select service Name ID) and continue to Review page | User should be able to select Services and go to Review page.");
		Reporter.log("9. Verify Price under Monthly Price section. | It should be displayed as "
				+ "There is no change to your monthly bill.");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToODFFreeRadioButtonToReviewPage(myTmoData, "Radio");
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyMonthlyBillNoChange("There's no change to your monthly cost.");
	}

	/**
	 * US260262#Review Add-Ons - No change in monthly price This flow for checking
	 * No Monthly changes in value on Review page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "xxx")
	public void verifyAddOnReviewPageNoChangePriceFunctionalityForServices(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnReviewPageNoChangePriceFunctionalityForServices method called in ManageAddOnsReviewpageTest");

		Reporter.log("Test Case  Name : Verify Add On Review Page No Change Price Functionality For Services");
		Reporter.log("Test Data : Any PAH/Full/Standard User with TMO One Plans");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service | Upgrade your Service Page should be displayed");
		Reporter.log(
				"7. Uncheck Checkbox for Currently Active service (McAfee® Security for T-Mobile). Also read the price of service. | User should be able to unselect active service");
		Reporter.log("8. Click on Continue CTA to Review page | User should be able to go to Review page.");
		Reporter.log(
				"9. On Review Page, Verify Price under Monthly Price section. | It should be displayed as There is no change to your monthly bill");
		Reporter.log(
				"10. On Review page, Verify Price under Monthly Price section. | Price should be selected data plan price (data plan selected in step7");
		Reporter.log("Actual Result:");

		navigateToODFFreeRadioButtonToReviewPage(myTmoData, "Radio");
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyMonthlyBillNoChange("There's no change to your monthly cost.");
	}

	/**
	 * US276198 - Date logic - Effective date options for service additions and
	 * removal (non SITT) This test case is for checking dates options on Review
	 * page when user selected any Non SITT soc service when no Conflict occurs
	 * US262073#Review Add-Ons - Effective date for add on data pass
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyAddOnReviewPageEffectiveDateForNonSITTSOC(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAddOnReviewPageEffectiveDateForNonSITTSOC method called in ManageAddOnsReviewpageTest");
		Reporter.log(
				"Test Case name : Check dates on Review page when user changes the Service and no Conflict occurs");
		Reporter.log("Test Data : any PAH/Full User with any plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"6. Choose Non SITT Service like Name ID, Device Block and Voicemail to Text. Also read prices of each. | User shoudl be able to select Non SITT service.");
		Reporter.log("7. Click on Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"8. Check Services under AddOn item section. | Services which added in Step6 should matched and prices also matched.");
		Reporter.log("9. Click on Change Date link | Change Date modal shoudl be loaded.");
		Reporter.log("10. Check by default selection | By default Today should be selected");
		Reporter.log("11. Check Next bill cycle option. | Next bill cycle option should be displayed.");
		Reporter.log("Actual Result:");
		Reporter.log("================================");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnCheckBoxByName("McAfee® Security for T-Mobile");
		// manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();

		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyNextBillCycle();
	}

	/**
	 * US303855 - Review page - Display pro-rate message for data upgrade US570470
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, Group.MULTIREG })
	public void verifyProRateMessageForDataPlanUpgradeOnReviewPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyProRateMessageForDataPlanUpgradeOnReviewPage method called in ODFReviewPageTest");
		Reporter.log("Test Case  Name : Verify ProRate Message for Data Plan on Review page");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Select any Higher Data Plan than currently active | User should be able to select data plan.");
		Reporter.log("7. Click on Continue Button and Go to Review page. | Review page should be displayed.");
		Reporter.log("8. Check ProRate message under selected data plan. | Message "
				+ "Changes made in the middle of a billing cycle will result in full monthly charges of the new feature. The full data bucket will be available to use during this bill cycle."
				+ "should be displayed.");
		Reporter.log(
				"9. Click on Effective date and select Next bill cycle. | User should be able to select Next Bill cycle.");
		Reporter.log("10. Check ProRate message under selected data plan. | Message should not be displayed.");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = navigateToManageAddOnsSelectionPage(myTmoData);
		manageAddOnsSelectionPage.clickOnNumberRadioButton();
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyProRateMessage();
		oDFDataPassReviewPage.clickChangeDateLink();
		oDFDataPassReviewPage.verifyCalenderTitle("Effective Date");
		oDFDataPassReviewPage.clickOnNextBillCycleRadioBtn();
		oDFDataPassReviewPage.clickOverlaySelectButton();
		oDFDataPassReviewPage.verifyProRateMessageIsNotDisplaying();
	}

	/**
	 * US303855 - Review page - Display pro-rate message for data upgrade
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "xxx")

	public void verifyProRateMessageForDataPlanDowngradeOnReviewPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyProRateMessageForDataPlanDowngradeOnReviewPage method called in ODFReviewPageTest");
		Reporter.log("Test Case  Name : Verify ProRate Message for Data Plan on Review page");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Select any lower Data Plan than currently active | User should be able to select data plan.");
		Reporter.log("7. Click on Continue Button and Go to Review page. | Review page should be displayed.");
		Reporter.log("8. Check ProRate message under selected data plan. | Message should not be displayed.");

		navigateToODFDecreaseReviewPage(myTmoData);
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyProRateMessageIsNotDisplaying();
	}

	/**
	 * US277102 - Persist add on selections: Browser back button
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "xxx")
	public void verifyPersistanceOfChangesWhenUserClicksBrowserBackButtonFromReviewPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyPersistanceOfChangesWhenUserClicksBrowserBackButtonFromReviewPage method called in ODFReviewPageTest");
		Reporter.log(
				"Test Case  Name : Verify Persitance of changes when user clicks on Back button of Browser from Review page");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Select Data plan. Read the name and selection. | User should be able to read the name.");
		Reporter.log("7. Select Data Pass. Read the name and selection. | User should be able to read the name.");
		Reporter.log("8. Select Some services. Read the names and selection. | User should be able to read the name.");
		Reporter.log("9. Click on Continue Button and Go to Review page. | Review page should be displayed.");
		Reporter.log("10. Click on Browser Back button. | User should be returned to AddOn selection.");
		Reporter.log(
				"11. Check selected options on Selection page. | User selected options from Step6, Step7 and Step8 should be in selected state.");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnDataPassRadioButton();
		manageAddOnsSelectionPage.clickOnDataPlanRadioButton();
		manageAddOnsSelectionPage.checkFirstCheckBox();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickOnReviewPageBrowserBackButton();
		verifyManageDataAndAddOnsPage();
	}

	/**
	 * US277195- Persist add on selections: Back arrow in nav bar US570514 US572052
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyPersistanceOfChangesWhenUserClicksBackArrowFromReviewPage(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyPersistanceOfChangesWhenUserClicksBackArrowFromReviewPage method called in ODFReviewPageTest");
		Reporter.log("Test Case  Name : Verify Persitance of changes when user clicks on Back Arrow on Review page");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. Select Data plan. Read the name and selection. | User should be able to read the name.");
		Reporter.log("7. Select Data Pass. Read the name and selection. | User should be able to read the name.");
		Reporter.log("8. Select Some services. Read the names and selection. | User should be able to read the name.");
		Reporter.log("9. Click on Continue Button and Go to Review page. | Review page should be displayed.");
		Reporter.log("10. Click on Back Arrow on Review button. | User should be returned to AddOn selection.");
		Reporter.log(
				"11. Check selected options on Selection page. | User selected options from Step6, Step7 and Step8 should be in selected state.");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = navigateToManageAddOnsSelectionPage(myTmoData);

		manageAddOnsSelectionPage.clickOnDataPassRadioButton();
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnDataPlanRadioButton();
		manageAddOnsSelectionPage.clickOnConflictContinue();
		// manageAddOnsSelectionPage.checkFirstCheckBox();
		manageAddOnsSelectionPage.clickOnContinueBtn();

		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		verifyODFDataPassReviewPage();
		oDFDataPassReviewPage.clickOnReviewPageBackButton();
		verifyManageAddOnsPage();
	}

	/**
	 * US280330#Review page - Revisit implementation of authored content for T&Cs
	 * modals
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "sumit")
	public void verifyReviewPageLegaleseTerms(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyReviewPageLegaleseTerms method called in ManageAddOnsReviewpageTest");

		Reporter.log("Test Case  Name : Verify Review Page Legalese Terms");
		Reporter.log("Test Data : Any PAH/Full User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7.Click on any Service or data and naviate to review Page");
		Reporter.log("7.Verify Review page Increase or decrease Message and Legalese Terms");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToODFReviewPageFlowByCheckBoxname(myTmoData, "McAfee® Security for T-Mobile");

		ManageAddOnsReviewPage odfReviewPage = new ManageAddOnsReviewPage(getDriver());
		odfReviewPage.verifyLegaleseTerms(
				"By clicking Agree and Submit, I agree that (a) my Agreement with T-Mobile includes the Service Agreement, any terms specific to my Rate Plan, and T-Mobile's Terms & Conditions; (b) T-Mobile requires Arbitration of Disputes unless I previously opted out per T-Mobile's Terms & Conditions; (c) I accept T-Mobile's Electronic Signature Terms; (d) I agree to receive SMS messages from T-Mobile about my add-on purchase, including re-order information");

		odfReviewPage.verifyLegaleseTerms2("Customers using >50GB/mo. may have reduced speeds. See full terms");
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());

		if (!(getDriver() instanceof AppiumDriver)) {
			oDFDataPassReviewPage.verifyLegaleseLinks();
		}
	}

	/**
	 * 
	 * DE152999 Adding McAfee throws trans failed with anything except current date
	 * US475242 Pier# 21172533 - Adding McAfee throws trans failed with anything
	 * except current date
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "sprint")
	public void verifyAddingMcAfeeServiceOnlyOnCurrentDate(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAddingMcAfeeServiceOnlyOnCurrentDate method called in ManageAddonsSelectionPage Test");
		Reporter.log("TestCase Name: verify Adding McAfee Service Only On Current Date");
		Reporter.log("Test Data : MSDSIN with  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log("6. select McAfee® Security for T-Mobile and continue | ODF review page should displayed");
		Reporter.log("7. Click on change date link | effective date popup should be displayed");
		Reporter.log(
				"8. verify date option is  only today for McAfee Service| date option should be only today for McAfee Service ");
		Reporter.log("==============================");
		Reporter.log("Actual Output:");

		navigateToODFReviewPageFlowByCheckBoxname(myTmoData, "McAfee® Security for T-Mobile");
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickChangeDateLinknotdisplayed();
	}

	/**
	 * US204208 - [Continued] Bearmode - Show App Section on the ODF Confirmation
	 * Page (D, M) US283531 - [Continued] Bearmode - Show Accessory Section on the
	 * ODF Confirmation Page (D, M) US306449 - Bearmode - Plan landing Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyBearModeFamilymodeReviewPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBearModeFamilymodeReviewPage method called in ManageAddOnsSelectionReviewpagetest");
		Reporter.log("Test Case name : verify Bea rMode Family mode Review Page");

		Reporter.log("Test Data : Any PAH Customer with Family Mode Plans ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Clcik on Plan Page | Plan Landing page should be displayed");
		Reporter.log(
				"5. Clcik on Plus Symbol on Family allowance under the Account section | Family allowance information should be displayed with App Store Badges");
		Reporter.log("6. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"7. Click on Family Mode plan under Family and Entertainment | Family mode confirmation window should be displayed");
		Reporter.log("8. Click on Add Service button on the confirmation window | Family Mode paln should be sleceted");
		Reporter.log("9. Click on Continue button | Plan Review page should be displayed.");

		navigateToManageAddOnsSelectionPage(myTmoData);

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		String newPrice = manageAddOnsSelectionPage.getCheckBoxPrice("Family Mode");
		manageAddOnsSelectionPage.clickOnCheckBoxByName("Family Mode");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyAddedItemsText();
		Assert.assertTrue(oDFDataPassReviewPage.verifyServiceTextByPrice("Family Mode").contains(newPrice),
				"Family Mode price is displaying wrong");
	}

	/**
	 * US277102#Persist add on selections: Browser back button
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, Group.MULTIREG })
	public void verifyReviewPageBackButton(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyReviewPageBackButton method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Review Page Back button");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any Plan");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"6. Choose Any Data Pass (International Pass) and Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"7. Read selected Data pass name and price. | User should be able to read Data pass name and Price.");
		Reporter.log("8.Click on Continue CTA | Review page should be displayed");
		Reporter.log("9.Click on Review page back button | Upgrade Your Services page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToODFIncreaseReviewPage(myTmoData);
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickOnReviewPageBackButton();
		verifyManageAddOnsPage();
	}

	/**
	 * US217043#Review Add-Ons - Cancel multiple add on changes CTA
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void verifyAddOnReviewPageCancelCTAFunctionalityForDataPlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnReviewPageCancelCTAFunctionalityForDataPlan method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case  Name : Verify Review Page Cancel CTA Functionality For Data Plan");
		Reporter.log("Test Data : Any PAH/Full/Standard User with TMO One Plans");
		Reporter.log("===================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"6. Choose data plan (select data plan T-Mobile ONE TE) and Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"7. Click on Cancel CTA on Review Page and check redirection. | User should be redirected to Plans landing page (plans.html)");
		Reporter.log("8. Load ODF url | Upgrade Your Services page should be displayed");
		Reporter.log(
				"9. Verify Upgrade Your Services page | Upgrade Your Services page should be displayed and selections which made in step6 should not persist");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToODFReviewPageFlow(myTmoData, "Radio");
		oDFDataPassReviewPageCancelVerification(myTmoData);
	}

	/**
	 * US278112 - Date logic - Effective date options for channel parity SOCs (SITT)
	 * This test case is for checking dates options on Review page when user
	 * selected any SITT soc service when no Conflict occurs US262073#Review Add-Ons
	 * - Effective date for add on data pass
	 * 
	 * US567964#Selection page - No pending plan: Enable check box for future-dated
	 * service add (lowest priority)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyAddOnReviewPageForEffectiveDateForAdditionOfSITTSOC(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddOnReviewPageForEffectiveDateForAdditionOfSITTSOC method called in ManageAddOnsSelectionpagetest");
		Reporter.log(
				"Test Case name : Check dates on Review page when user adds the SITT Service and no Conflict occurs");
		Reporter.log(
				"Test Data : any PAH/Full User with any plan. Data plan should not be ONE PLUS/International ONE PLUS");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | AddOn page should be displayed");
		Reporter.log(
				"6. Choose SITT Service like North America Stateside International Talk. Read the price of service also | User should be able to select SITT service.");
		Reporter.log("7. Click on Continue to Review Page | Review page should be displayed.");
		Reporter.log(
				"8. Check Service name and price on Review Page under AddOn section | Service name should be North America Stateside International Talk and price should match with step6.");
		Reporter.log("9. Click on Change Date link | Change Date modal shoudl be loaded.");
		Reporter.log("10. Check by default selection | By default Today should be selected");
		Reporter.log("11. Check Next bill cycle option. | Next bill cycle option should be displayed.");
		Reporter.log("12. Select Today as effective date | User should be able to select effective date as Today");
		Reporter.log(
				"14. Select Next Bill cycle as effective date | User should be able to select effective date as Next Bill cycle");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = navigateToManageAddOnsSelectionPage(myTmoData);
		String price = manageAddOnsSelectionPage.getCheckBoxPrice("North America Stateside International Talk");
		manageAddOnsSelectionPage.clickOnCheckBoxByName("North America Stateside International Talk");
		manageAddOnsSelectionPage.clickOnConflictContinue();
		manageAddOnsSelectionPage.clickOnContinueBtn();
		verifyODFDataPassReviewPage();

		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyNextBillCycle();
		Assert.assertTrue(oDFDataPassReviewPage.verifyServiceTextByPrice("North America Stateside International Talk")
				.contains(price), "North America Stateside International Talk price is displaying wrong");
	}

	/**
	 * US446646 Deeplink: Data pass upsell; no conflicts: Redirect users to the
	 * Review Page US478054 Deeplink: Add new query parameter 'isUpsell' (Technical
	 * Story) US463010 Deeplink: Review page - Nav back arrow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyReviewPageWhenUserHitsUpsellURL(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyReviewPageWhenUserHitsUpsellURL method called in ManageAddOnsReviewpageTest");
		Reporter.log("Test Case : Verify Active Test is present in ");
		Reporter.log("Test Data : TI/TE PAH MSISDN and Data Pass should be activated from Future date");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Hit direct url of Upsell pass | User Should get login page.");
		Reporter.log(
				"2. Login to the application | User Should be login successfully. Addons Review page should be displayed. ");
		Reporter.log("3. Click on Back navigation arrow. | User Should be redirected to Manage Addons page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		redirectToUpsellURLOnReviewPage(myTmoData, "DataPass:HDPASS3");
		backNavigationOfUpsellURL();
	}

	/**
	 * US545286#URL with upSell parameter for deeplink to review page for services
	 * (similar to data pass) US571428#US572050#US572067#US574041
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyReviewPageWhenUserHitsUpsellServieURL(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyReviewPageWhenUserHitsUpsellServieURL method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Active Test is present in ");
		Reporter.log("Test Data : TI/TE PAH MSISDN and Data Pass should be activated from Future date");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Hit direct url of Upsell pass | User Should get login page.");
		Reporter.log(
				"2. Login to the application | User Should be login successfully. Addons Review page should be displayed. ");
		Reporter.log("3. Click on Back navigation arrow. | User Should be redirected to Manage Addons page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		redirectToUpsellURLOnReviewPage(myTmoData, "Service:FAMALL10");
		backNavigationOfUpsellURL();
	}

	/**
	 * US576288#Deeplink to Review for services - Multiple SOCs in upSell url
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyReviewPageWhenUserHitsMultipleSOCsInUpSellUrl(ControlTestData data, MyTmoData myTmoData) {

		logger.info(
				"verifyReviewPageWhenUserHitsMultipleSOCsInUpSellUrl method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Active Test is present in ");
		Reporter.log("Test Data : TI/TE PAH MSISDN and Data Pass should be activated from Future date");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Hit direct url of Upsell pass | User Should get login page.");
		Reporter.log(
				"2. Login to the application | User Should be login successfully. Addons Review page should be displayed. ");
		Reporter.log("3. Click on Back navigation arrow. | User Should be redirected to Manage Addons page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/isUpsell:true/Service:FAMALL10,2SPHDNFLX");
		// navigateToAddOnsUpsellURL("home","Service:FAMALL10,2SPHDNFLX");

		ODFDataPassReviewPage reviewPageAddon = new ODFDataPassReviewPage(getDriver());
		reviewPageAddon.verifyReviewPageOfManageDataAndAddOnsPage();

		// verify family allowance is in add ons section
	}

	/**
	 * US572067
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyReviewPageWhenUserHitsUpsellSITServieURL(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyReviewPageWhenUserHitsUpsellSITServieURL method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Review Page When User Hits Up sell SIT Servie URL ");
		Reporter.log("Test Data : TI/TE PAH MSISDN and Data Pass should be activated from Future date");// 3132471604
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Hit direct url of Upsell pass | User Should get login page.");
		Reporter.log(
				"2. Login to the application | User Should be login successfully. Addons Review page should be displayed. ");
		Reporter.log("3. Click on Back navigation arrow. | User Should be redirected to Manage Addons page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		redirectToUpsellURLOnReviewPage(myTmoData, "Service:ZNASIT15");
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.verifyServiceProRateMessage();
		backNavigationOfUpsellURL();
	}

	/**
	 * US572067 US463009 - [TEST ONLY] Deeplink: Cancel
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyReviewPageWhenUserHitsUpsellServieConflictsURL(ControlTestData data, MyTmoData myTmoData) {

		logger.info(
				"verifyReviewPageWhenUserHitsUpsellServieConflictsURL method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Active Test is present in ");
		Reporter.log("Test Data : TI/TE PAH MSISDN and Data Pass should be activated from Future date");// 3132471604
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Hit direct url of Upsell pass | User Should get login page.");
		Reporter.log(
				"2. Login to the application | User Should be login successfully. Addons Review page should be displayed. ");
		Reporter.log("3. Click on Back navigation arrow. | User Should be redirected to Manage Addons page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/isUpsell:true/Service:4SPHDNFLX");
		ODFDataPassReviewPage reviewPageAddon = new ODFDataPassReviewPage(getDriver());
		reviewPageAddon.verifyReviewPageOfManageDataAndAddOnsPage();
		reviewPageAddon.verifyAddedItemsText();

		// verify 2 screen netflix is removed in removed add ons sections and 4 screen
		// is added in add on section
		backNavigationOfUpsellURL();
	}

	/**
	 * US572394 - Deeplink to Review for services - Suspended line
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyUserHitsUpsellURLSuspendedLine(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyUserHitsUpsellURLSuspendedLine method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Active Test is present in ");
		Reporter.log("Test Data : TI/TE PAH MSISDN and Data Pass should be activated from Future date");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Hit direct url of Upsell pass | User Should get login page.");
		Reporter.log(
				"2. Login to the application | User Should be login successfully. Addons Review page should be displayed. ");
		Reporter.log("3. Click on Cancel CTA. | User Should be redirected to Manage Addons page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/isUpsell:true/Service:FAMALLOW,FAMMODE,FRLTDATA");

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyContinueBtnIsDisabled();
	}

	/**
	 * US572397 - Deeplink to Review for services - No permissions to make changes
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyUserHitsUpsellURLNoPermissions(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyUserHitsUpsellURLNoPermissions method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Active Test is present in ");
		Reporter.log("Test Data : TI/TE PAH MSISDN and Data Pass should be activated from Future date");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Hit direct url of Upsell pass | User Should get login page.");
		Reporter.log(
				"2. Login to the application | User Should be login successfully. Addons Review page should be displayed. ");
		Reporter.log("3. Click on Cancel CTA. | User Should be redirected to Manage Addons page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/isUpsell:true/DataPass:INTL24NA");
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifySuspendedorContactPAHMessage("You do not have permission to make this change.");
	}

	/**
	 * US446651 - Deeplink: Data pass upsell; International roaming blocked: Display
	 * message
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyBlockInternationalRoaming(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyBlockInternationalRoaming method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Active Test is present in ");
		Reporter.log("Test Data : TI/TE PAH MSISDN and Data Pass should be activated from Future date");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"1. Login to the application | User Should be login successfully. Addons Review page should be displayed. ");
		Reporter.log("2. Go to Profile page and click on Blocking blade. | Blocking page should be displayed.");
		Reporter.log(
				"3. Check whether Block International Roaming is checked or not. | Block International Roaming should be checked.");
		Reporter.log("4. Hit direct url of Upsell pass | Manage Addons page should be displayed with 1 pass only.");
		Reporter.log("5. Check Unblock Roaming message. | Message should be displayed.");
		Reporter.log(
				"6. Click on Continue CTA. | It should be redirected to Manage Addons page and all addons should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		verifyUnblockRoamingMessageForUpsell(myTmoData);
	}

	/**
	 * US446651 - Deeplink: Data pass upsell; International roaming blocked: Display
	 * message
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyUnblockRoamingURLRedirectionFromBlockInternationalRoamingMessage(ControlTestData data,
			MyTmoData myTmoData) {

		logger.info("verifyBlockInternationalRoaming method called in ManageAddOnsSelectionpagetest");
		Reporter.log("Test Case : Verify Active Test is present in ");
		Reporter.log("Test Data : TI/TE PAH MSISDN and Data Pass should be activated from Future date");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"1. Login to the application | User Should be login successfully. Addons Review page should be displayed. ");
		Reporter.log("2. Go to Profile page and click on Blocking blade. | Blocking page should be displayed.");
		Reporter.log(
				"3. Check whether Block International Roaming is checked or not. | Block International Roaming should be checked.");
		Reporter.log("4. Hit direct url of Upsell pass | Manage Addons page should be displayed with 1 pass only.");
		Reporter.log("5. Check Unblock Roaming message. | Message should be displayed.");
		Reporter.log("6. Click on Unblock Roaming link. | It should be redirected to /profile/blocking page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		verifyUnblockRoamingLinkRedirection(myTmoData);
	}

	/**
	 * US522063:ASG - Update Review Page, Enable CTAs and Show Terms and Data Pass
	 * Start Date
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "abc")
	public void verifyReviewPageTermsAndStartDateForStandardAndRestricted(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyReviewPageTermsAndStartDate method called in ManageAddOnsReviewpageTest");

		Reporter.log("Test Case  Name : Verify Review Page Terms and start date ");
		Reporter.log("Test Data : Any STD/RES User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7. Select a data pass and click Continue| Review Page should be displayed");
		Reporter.log("8. Click on data pass and navigate to review Page");
		Reporter.log("9. Click on data pass Start date under data pass name | Start immediatly should be shown");
		Reporter.log("10. Click on Service Agreement| Link should redirect to https://my.t-mobile.com/plan-agreement");
		Reporter.log(
				"11.Click on Terms & Conditions| Limk has to redirect to https://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions&print=true");
		Reporter.log(
				"12.Click on Electronic Signature Terms| Link has to redirect to links to https://my.t-mobile.com/plan-esignature");
		Reporter.log("13.Verify Review page Increase or decrease Message and Legalese Terms");
		Reporter.log(
				"14.Click on See full terms | Link should redirect to links to https://my.t-mobile.com/legal-copy");
		Reporter.log("15.Click on cancel button | Navigate back to Plans landing page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyDataPassLabel();
		manageAddOnsSelectionPage.clickOnDataPassRadioButton("International Pass");
		manageAddOnsSelectionPage.clickOnContinueBtn();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		verifyODFDataPassReviewAndPayPage();
		oDFDataPassReviewPage.expandArrowForDescriptionOfDataPass();
		oDFDataPassReviewPage.checkWhetherDescriptionOfDataPassIsDisplayedOrNot();
		oDFDataPassReviewPage.collapseArrowForDescriptionOfDataPass();
		oDFDataPassReviewPage.verifyStartImmediately("Starts Immediately");
		oDFDataPassReviewPage.verifyDataPassTotalAmount();
		oDFDataPassReviewPage.verifyLegaleseTerms(
				"By clicking Agree and Submit, I agree that (a) my Agreement with T-Mobile includes the Service Agreement, any terms specific to my Rate Plan, and T-Mobile's Terms & Conditions; (b) T-Mobile requires Arbitration of Disputes unless I previously opted out per T-Mobile's Terms & Conditions; (c) I accept T-Mobile's Electronic Signature Terms; (d) I agree to receive SMS messages from T-Mobile about my add-on purchase, including re-order information");

		oDFDataPassReviewPage.verifyLegaleseText("Customers using >50GB/mo. may have reduced speeds. See full terms");
		if (!(getDriver() instanceof AppiumDriver)) {
			oDFDataPassReviewPage.verifyLegaleseLinks();
		}
	}

	/**
	 * US522063:ASG - Verify PCM blade on Review page
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "abc")
	public void verifyPaymentModuleOnAddonReviewPAgeForStdAndResUsersDataPassPurchase(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("Verify PCM blade on Review page method called in ManageAddOnsReviewpageTest");

		Reporter.log("Test Case  Name : Verify PCM blade on Review page");
		Reporter.log("Test Data : Any STD/RES User with any Plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7. Select a data pass and click Continue| Review Page should be displayed");
		Reporter.log("8. Click on data pass and navigate to review Page");
		Reporter.log("9. Verify and click on Payment Method Blade|Edit Payment Method page should be displayed");
		Reporter.log("10.Click on Add card|Card Information should be dispalyed ");
		Reporter.log("11.Enter vaild details and click on continue button |Agree and submit button should enabled");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// navigateToReviewAndPay(myTmoData);
		navigateToManageAddOnsSelectionPage(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifyDataPassLabel();
		manageAddOnsSelectionPage.clickOnDataPassRadioButton("International Pass");
		manageAddOnsSelectionPage.clickOnContinueBtn();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		verifyODFDataPassReviewAndPayPage();
		oDFDataPassReviewPage.verifyUpfrontPaymentMethod();
		oDFDataPassReviewPage.clickPaymentMethodBlade();
		oDFDataPassReviewPage.verifyEditPaymentMethodText();
		oDFDataPassReviewPage.verifyAddCardBlade();
		oDFDataPassReviewPage.clickAddCard();
		oDFDataPassReviewPage.verifyCardInformationPage("Card information");
		oDFDataPassReviewPage.enterCardName();
		oDFDataPassReviewPage.enterCardNumber();
		oDFDataPassReviewPage.enterexpirationDate();
		oDFDataPassReviewPage.enterCvv();
		oDFDataPassReviewPage.enterZip();
		oDFDataPassReviewPage.clickContinueCTA();
		oDFDataPassReviewPage.agreeAndSubmitButton();
	}

	/**
	 * US519560 - ASG - Deep Link to Review Page for Standard/Restricted Users
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS, })
	public void verifyReviewAndPayPageWhenUserHitsUpsellURL(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyReviewAndPayPageWhenUserHitsUpsellURL method called in ManageAddOnsReviewpagetest");
		Reporter.log("Test Case : Verify Active Test is present in ");
		Reporter.log("Test Data : STD/RES MSISDN and Data Pass should be activated from Future date");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Hit direct url of Upsell pass | User Should get login page.");
		Reporter.log(
				"2. Login to the application | User Should be login successfully. Review & Pay page should be displayed. ");
		Reporter.log("3. Verify caret expended | DAtaPass Description should be seen.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToUpsellURLOnReviewPage(myTmoData);
		verifyCaretExpandedOfUpsellURL();
	}

	/**
	 * CDCAM-576# Tethering Soc
	 *
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyODFReviewPageChangeDateforTetheringDataSoc(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case  Name : Verify ODF Review Page Change Date");
		Reporter.log("Test Data : Any PAH/Full/Standard User with any plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("===================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Plan link| Plan page should be displayed");
		Reporter.log("5. Load ODF url | Data Pass List page should be displayed");
		Reporter.log("6. Verify Upgrade your Service| Upgrade your Service Page should be displayed");
		Reporter.log("7.Click on any Service or data and naviate to review Page | Review page should be displayed");
		Reporter.log("8.Click on Change Date Link | Calendar should be displayed");
		Reporter.log("9.Verify option Starts Immendiately | Option should be displayed");
		Reporter.log("10.Click on Pick a date option and check calendar | Calendar should be displayed");
		Reporter.log("===================");
		Reporter.log("Actual Result:");

		navigateToODFReviewPageFlowByCheckBoxname(myTmoData, "10GB of high-speed mobile hotspot data");
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		oDFDataPassReviewPage.clickChangeDateLink();
		oDFDataPassReviewPage.verifyCalenderTitle("Effective Date");
		oDFDataPassReviewPage.verifyToday("Today");
		oDFDataPassReviewPage.verifyPickaDate("Next bill cycle");
	}

}
