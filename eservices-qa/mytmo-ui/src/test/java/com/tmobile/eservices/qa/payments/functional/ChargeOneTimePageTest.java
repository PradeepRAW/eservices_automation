package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.ChargeOneTimePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class ChargeOneTimePageTest extends PaymentCommonLib{

    private static final Logger logger = LoggerFactory.getLogger(ChargeOneTimePageTest.class);

    @Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
    public void testVerfiyChargeontimepayPageLoad(ControlTestData data, MyTmoData myTmoData) {
        logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
        Reporter.log("Billing - Verify EIP Bill Summary Details");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("6. Click on Charge One-time | Charge One-time page should be loaded");
        Reporter.log("================================");
        ChargeOneTimePage chargeonetimepage=navigateToChargeOneTimePage(myTmoData);
    }


    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS,"billing" })
    public void testChageonetimeTotalAmountForPlansInBriteBill(ControlTestData data, MyTmoData myTmoData) {
        logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
        Reporter.log("Billing - Verify EIP Bill Summary Details");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("6. Click on Charge One-time | Charge One-time page should be loaded");
        Reporter.log("7. Check the Total amount for Charge One time payments");
        Reporter.log("================================");
        ChargeOneTimePage chargeonetimepage=navigateToChargeOneTimePage(myTmoData);
        chargeonetimepage.checklabelTotal("Total");
        chargeonetimepage.checktotalAmount();
        
    }


    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
    public void testChageonetimebacktoSummaryLink(ControlTestData data, MyTmoData myTmoData) {
        logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
        Reporter.log("Billing - Verify EIP Bill Summary Details");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("6. Click on Charge One-time | Charge One-time page should be loaded");
        Reporter.log("7. Click on Back to Summary Link | User should be able to see BillingSummary page");
        Reporter.log("================================");
        ChargeOneTimePage chargeonetimepage=navigateToChargeOneTimePage(myTmoData);
        chargeonetimepage.clickBackToSummaryLink();
        BillAndPaySummaryPage billAndPaySummaryPage=new BillAndPaySummaryPage(getDriver());
        billAndPaySummaryPage.verifyPageLoaded();
    }
    
    
    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
    public void testChageonetimecheckSliderfunctionality(ControlTestData data, MyTmoData myTmoData) {
        logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
        Reporter.log("Billing - Verify EIP Bill Summary Details");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("6. Click on Charge One-time | Charge One-time page should be loaded");
        Reporter.log("7. Validate the slider functionality | Slider should work as expected");
        Reporter.log("================================");
        ChargeOneTimePage chargeonetimepage=navigateToChargeOneTimePage(myTmoData);
        chargeonetimepage.checktotalAmountwithslideramount();
        chargeonetimepage.checkleftslider();
        chargeonetimepage.checkrightslider();
        
    }
    
}

