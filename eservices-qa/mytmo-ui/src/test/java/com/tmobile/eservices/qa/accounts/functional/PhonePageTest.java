
package com.tmobile.eservices.qa.accounts.functional;

import java.util.ArrayList;
import java.util.Arrays;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.LineSettingsPage;
import com.tmobile.eservices.qa.pages.global.PhonePage;

public class PhonePageTest extends AccountsCommonLib {

	/**
	 * US356270 : PDP - Update Phone page message for no PDP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void checkPDPProtectionStatusWhenThereIsNoPDPSOCOnLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : Verify that when user doesn't have PDP/PHP SOC on line then check whether user see PDP PRotection status or not");
		Reporter.log("Test Data : PAH/Full user with PHP/PDP soc on line");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Check text No Device Protection | Text No DEvice Protection should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		phonePage.checkPHPStatusWhenThereIsNoPHPSOCOnLine();
	}

	/**
	 * US356305 : PDP - Test Only File-a-claim
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void checkPDPPopUpAndFileADamageClaimRedirectionForAssurantSOC(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log(
				"Test Case : Verify that when user has PDP/PHP SOC on line and user clicks File Damage claim then check whether user gets pop up or not.");
		Reporter.log("Test Data : PAH/Full user with PHP/PDP soc on line");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on File Damage Claim link. | Pop up should be displayed.");
		Reporter.log("5. Click on Cancel CTA. | Pop up should be disappeared and user should return to Phone page.");
		Reporter.log("5. Click on File Damage Claim link. | Pop up should be displayed.");
		Reporter.log("5. Click on Continue CTA. | User should be redirected to https://myphpinfo.com/?lang=en.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		phonePage.clickOnFileDamageLinkAndVerifyFunctionalityForAssurantSOC();
	}

	/**
	 * US356305 : PDP - Test Only File-a-claim
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void checkPDPPopUpAndFileADamageClaimRedirectionForAssurionSOC(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log(
				"Test Case : Verify that when user has PDP/PHP SOC on line and user clicks File Damage claim then check whether user gets pop up or not.");
		Reporter.log("Test Data : PAH/Full user with PHP/PDP soc on line");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on File Damage Claim link. | Pop up should be displayed.");
		Reporter.log("5. Click on Cancel CTA. | Pop up should be disappeared and user should return to Phone page.");
		Reporter.log("5. Click on File Damage Claim link. | Pop up should be displayed.");
		Reporter.log(
				"5. Click on Continue CTA. | User should be redirected to http://phoneclaim.com/t-mobile?lang=en.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickOnFileDamageLinkAndVerifyFunctionalityForAssurionSOC();
	}

	/**
	 * US390180 : PDP - Copy update on Phone page links
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void testLostAndStolenFlow(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		Reporter.log("Test Case : Test Case : test Lost And Stolen Flow");
		Reporter.log("Test Data : PAH/Full user with PHP/PDP soc on line");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Read PHP SOC name | Should be able to read PHP SOC name.");
		Reporter.log("5. Click on Report Lost and Stolen link | Lost and Stolen Page should be displayed.");
		Reporter.log("5. Click on NEXT CTA. | Lost and Stolen Page should be displayed.");
		Reporter.log("5. Verify PHP/PDP soc on Phone page and Lost/Stolen page. | Both should be matched.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());

		phonePage.verifyiFoundMyDeviceAndRestoreIt();
		phonePage.updateDeviceLostOrStolen("lost");
	}

	/**
	 * US506444 :Phones Page CTAs
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })

	public void verifyPhonepageCTAforISPline(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		Reporter.log("Test Case : Test Case : Verify PDP SOC on Phone PAge and Lost & Stolen page.");
		Reporter.log("Test Data : PAH/Full user with PHP/PDP soc on line");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Select ISP Line from the Dropdown | Should be able to select ISP line.");
		Reporter.log("5. Verify that Available CTA | Lost and Stolen and Temporarily Suspend CTA should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.selectISPline();
		phonePage.verifyPhoneCTA();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void testPhonePageDeviceStatusUpsellURL(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Test Phone Page Upsell URL");
		Reporter.log("Test Data : PAH/Full user ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Load Phone page Upsell URL | Phone page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToFutureURLFromHome(myTmoData, "/account/phone/device-status");
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyDeviceUnlockPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void testPhonePageCheckDeviceUpsellURL(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Test Phone Page Check Device Upsell URL");
		Reporter.log("Test Data : PAH/Full user ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Load Phone page Upsell URL | Phone page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToFutureURLFromHome(myTmoData, "/account/phone/check-device");
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyCheckYourDevicePage();
	}

	/**
	 * US475485 : Lock Status Page - Show Link to Unlock Policy
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyUnlockPolicyLink(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		Reporter.log(
				"Test Case : Verify customer interested in unlocking their phone and has to see a link to the T-Mobile policy");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log(
				"5. Click on T-Mobile's unlock policy link. | User should be redirected to https://support.t-mobile.com/docs/DOC-1588.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.unlockPolicyLink();
		phonePage.verifSupportPage();
	}

	/**
	 * US534688,US533888,US475528:Check Another Device Page - Show "Check your
	 * device" heading
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyCheckYourDeviceHeading(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		Reporter.log("Test Case : Verify the Authorable headline-Check your device");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link |device status page should be displayed.");
		Reporter.log(
				"5. Click on Check a device not on your account link. | Verify Authorable headline-Check your device");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
	}

	/**
	 * US534675:Check Another Device Page - Show Error Message for Invalid IMEIs
	 * (Client Side Error)
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyErrorMessageforInvalidIMEI(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log(
				"Test Case : Verify error message The IMEI you entered is invalid. Please try entering the IMEI again.");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link |device status page should be displayed.");
		Reporter.log(
				"5. Click on Check a device not on your account link. | Verify Authorable headline-Check your device");
		Reporter.log(
				"5. Enter invalid imei. | Verify error message The IMEI you entered is invalid. Please try entering the IMEI again.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
	}

	/**
	 * US475568:Check Another Device Page - Show Text for Instructions
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyTextForInstructions(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		Reporter.log(
				"Test Case : Verify error message The IMEI you entered is invalid. Please try entering the IMEI again.");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link |device status page should be displayed.");
		Reporter.log(
				"5. Click on Check a device not on your account link. | Verify Authorable headline-Check your device");
		Reporter.log(
				"5. Verify the Text for Instructions| Authorable text- Have your device's phone number and IMEI number handy.  Don't know your IMEI number?  Dial *#06# or search for IMEI in settings. (see attached)");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.verifyDeviceStatusImg();

	}

	/**
	 * US533988 : Lock Status Page - Show Link to Unlock Policy in a new tab
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyUnlockPolicyLinkInNewTab(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		Reporter.log(
				"Test Case : Verify customer interested in unlocking their phone and has to see a link to the T-Mobile policy");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log(
				"5. Click on T-Mobile's unlock policy link. | User should be redirected to https://support.t-mobile.com/docs/DOC-1588.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.unlockPolicyLink();
		phonePage.verifSupportPage();
	}

	/**
	 * Check a device not on your account
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyCheckADeviceNotOnYourAccount2(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verify Check A Device Not On Your Account");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Click on Verify on device link and enter a IMEI and verify device Unknown Status");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();
		phonePage.enteriMEINumberText();
		phonePage.clickOnCheckDeviceButton();
		phonePage.verifyStatusUnknown();
	}

	/**
	 * Verifying Device Is Temporarily Unlocked And Eligible For Permanent Unlock
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "regression")
	public void verifyDeviceIsTemporarilyUnlockedAndEligibleForPermanentUnlock(ControlTestData data,
			MyTmoData myTmoData) throws InterruptedException {
		Reporter.log("Test Case : Verifying Device Is Temporarily Unlocked And Eligible For Permanent Unlock");
		Reporter.log("Test Data : 4254106023/Audit123");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify Device status as Device Unlocked");
		Reporter.log(
				"7. Verify message on device status page: Temporary unlocked from YEAR-MM-DD to YEAR-MM-DD.Your device is eligible to be permanetly unlocked.");
		Reporter.log("8. Verify Contact Us button");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();
	}

	/**
	 * Verifying Device Is Temporarily Unlocked And Not Eligible For Permanent
	 * Unlock
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyDeviceIsTemporarilyUnlockedAndNotEligibleForPermanentUnlock(ControlTestData data,
			MyTmoData myTmoData) throws InterruptedException {
		Reporter.log("Test Case : Verifying Device Is Temporarily Unlocked And Not Eligible For Permanent Unlock");
		Reporter.log("Test Data : 2064322550/TM0Test1");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify Device status as Device Unlocked");
		Reporter.log(
				"7. Verify message on device status page : Your device is unlocked and can be used on all compartible networks.");
		Reporter.log("8. Verify Back to phone page button on device status page ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();

	}

	/**
	 * Verifying Device is Permanently Unlocked
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyDeviceIsPermanentlyUnlocked(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verify Check A Device Not On Your Account");
		Reporter.log("Test Data : 5055448496/TM0Test1");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify Device status as  ");
		Reporter.log("7. Verify message on device status page : ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();
	}

	/**
	 * Verifying Lock Status Unknown
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyLockStatusUnknown(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		Reporter.log("Test Case : Verify Lock Status Unknown");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify Device status as Status unknown ");
		Reporter.log(
				"7. Verify message on device status page :We are unable to locate information about your device's unlock status ");
		Reporter.log("8. Verify Contact us button on Device status page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();
	}

	/**
	 * Verifying Device Is Locked And Eligible For Temporary Unlock
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyDeviceIsLockedAndEligibleForTemporaryUnlock(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verifying Device Is Locked And Eligible For Temporary Unlock");
		Reporter.log("Test Data : 4252462273/TM0Test1");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify Device status as Device locked ");
		Reporter.log("7. Verify message on device status page :");
		Reporter.log("8. Verify Contact us button on Device status page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();

	}

	/**
	 * Verifying Device Is locked And Eligible For Permanent Unlock and can be
	 * unlocked remotely
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyDeviceIsLockedAndEligibleForPermanentUnlock(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log(
				"Test Case : Verify Device Is locked And Eligible For Permanent Unlock and can be unlocked remotely");
		Reporter.log("Test Data : 4252405305/TM0Test1");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify Device status as Device locked ");
		Reporter.log("7. Verify message on device status page :");
		Reporter.log("8. Verify Contact us button on Device status page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();

	}

	/**
	 * Verifying device is locked and eligible for permanent unlock and can be
	 * unlocked remotely
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyCheckADeviceNotOnYourAccount3(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verify Check A Device Not On Your Account");
		Reporter.log("Test Data : 4252405305/TM0Test1");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify Device status as Device locked ");
		Reporter.log("7. Verify message on device status page :");
		Reporter.log("8. Verify Contact us button on Device status page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();

	}

	/**
	 * Verifying device is locked and eligible for permanent unlock and can NOT be
	 * unlocked remotely
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyCheckADeviceNotOnYourAccount(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verify Check A Device Not On Your Account");
		Reporter.log("Test Data : 2063351664/TM0Test13");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify Device status as Device locked ");
		Reporter.log("7. Verify message on device status page :");
		Reporter.log("8. Verify Contact us button on Device status page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();

	}

	/**
	 * Verifying Device Is Locked And Not Eligible For Unlock
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void verifyDeviceIsLockedAndNotEligibleForUnlock(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verify Check A Device Not On Your Account");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify Device status as Device locked ");
		Reporter.log("7. Verify message on device status page :");
		Reporter.log("8. Verify Contact us button on Device status page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();

	}

	/**
	 * Verifying Lock and Unlock scenario when Device is temporarily unlocked
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void verifyLockUnlockScenarioWhenTemporarilyUnlocked(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verifying Lock and Unlock scenario when Device is temporarily unlocked");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify following below scenario :");
		Reporter.log("Scenario 1 - Device is temporarily unlocked and eligible for permanent unlock");
		Reporter.log("Scenario 2 - Device is temporarily unlocked and NOT eligible for permanent unlock");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();
		phonePage.enterListOfImeiNumberWhenTemporarilyUnlocked(
				new ArrayList<String>(Arrays.asList("", "865208041836142")));
	}

	/**
	 * Verifying Lock and Unlock scenario when Device is Permanently unlocked
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void verifyLockUnlockScenarioWhenPermanentlyUnlocked(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verifying Lock and Unlock scenario when Device is Permanently unlocked");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify following below scenario :");
		Reporter.log("Scenario 3 - Device is Permanently Unlocked");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();
		phonePage.enterImeiNumberForPermanentlyUnlocked("356420054636402");
	}

	/**
	 * Verifying Lock and Unlock scenario when Lock Status Unknown
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void verifyLockUnlockScenarioWhenLockStatusUnknown(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verifying Lock and Unlock scenario for Device when Lock Status Unknown");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify below scenario :");
		Reporter.log("Scenario 4 - Lock Status Unknown");
		Reporter.log("Scenario 9 - Lock Status Unknown for US578313");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();
		phonePage.enterImeiNumberForLockStatusUnknown(new ArrayList<String>(Arrays.asList("", "357813060006793")));
	}

	/**
	 * Verifying Lock and Unlock scenario when device is locked
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void verifyLockUnlockScenarioWhenDeviceIsLocked(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verifying Lock and Unlock scenario for Device when device is locked");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify following below scenario :");
		Reporter.log("Scenario 5 - The device is locked and eligible for temporary unlock");
		Reporter.log(
				"Scenario 6 - The device is locked and eligible for permanent unlock and can be unlocked remotely");
		Reporter.log(
				"Scenario 7 - The device is locked and eligible for permanent unlock and can NOT be unlocked remotely");
		Reporter.log("Scenario 8 - The device is locked and not eligible for unlock");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();
		phonePage.enterListOfImeiNumberForLockedDevice(new ArrayList<String>(Arrays.asList("353807082428856")));
	}

	/**
	 * Verifying Lock and Unlock scenario when image for Unknown device
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void verifyLockUnlockScenarioWhenImageForUnknownDevice(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verifying Lock and Unlock scenario when image for Unknown device");
		Reporter.log("Test Data : PAH/Full user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Click on Check device unlock status link | device status page should be displayed.");
		Reporter.log("6. Verify below scenario :");
		Reporter.log("Scenario 9 - Verify image for Unknown device");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.clickDeviceUnlockStatusLink();
		phonePage.verifyDeviceUnlockPage();
		phonePage.clickOnCheckADeviceNotOnYourAccount();
		phonePage.verifyCheckYourDevicePage();
		phonePage.enterImeiNumberForUnkownDevice("");
	}

	/**
	 * Verifying phone page with PHP soc
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyPhonePageWithPHPSoc(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		Reporter.log("Test Case : Verify Phone page with PHP soc ");
		Reporter.log("Test Data : PAH user with PHP soc");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Verify Authorable text on Phone Page with PHP Soc  ");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.checkPHPSocDetaiils("Protection<360>");
	}

	/**
	 * Verifying phone page with out PHP soc
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "sprintPhone")
	public void verifyPhonePageWithOutPHPSoc(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {
		Reporter.log("Test Case : Verify Phone page with out PHP soc ");
		Reporter.log("Test Data : PAH user with PHP soc");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Verify Authorable text on Phone Page with out PHP Soc  ");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.checkPHPSocDetaiils("No Device Protection");
	}

	/**
	 * Verifying phone page Device details for first line
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void verifyPhonePageDeviceNFeaturesForFirstLine(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verify Phone page Device and Features For First Line  ");
		Reporter.log("Test Data : PAH user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Verify Phone Page Device Name");
		Reporter.log("6. Verify Phone Page Device Protection soc");
		Reporter.log("7. Verify Phone Page Features");
		Reporter.log("8. Verify Phone Page Device Image");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());
		LineSettingsPage lineSettings = new LineSettingsPage(getDriver());
		lineSettings.verifyAuthorabletext("iPhone XR - Blue - 64GB", phonePage.deviceName);
		phonePage.checkPHPSocDetaiils("Device Protection");
		phonePage.verifyFeatureDetaiils("6.1 Liquid Retina Display");
		phonePage.verifyFeatureDetaiils("Splash, Water, and Dust Resistant");
		phonePage.verifyFeatureDetaiils("12MP Camera with OIS & 7MP TrueDepth Front Camera");
		phonePage.verifyFeatureDetaiils("Face ID");
		phonePage.verifyPhoneImage(phonePage.iphoneXRImage);
	}

	/**
	 * Verifying phone page Device details for Second line
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void verifyPhonePageDeviceNFeaturesForSecondLine(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verify Phone page Device and Features For First Line  ");
		Reporter.log("Test Data : PAH user");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Select second line");
		Reporter.log("6. Verify Phone Page Device Name");
		Reporter.log("7. Verify Phone Page Device Protection soc");
		Reporter.log("8. Verify Phone Page Features");
		Reporter.log("9. Verify Phone Page Device Image");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());

		LineSettingsPage lineSettings = new LineSettingsPage(getDriver());

		phonePage.selectLineFromDropDown(2);
		lineSettings.verifyAuthorabletext("Unknown Device", phonePage.deviceName);
		phonePage.checkPHPSocDetaiils("No Device Protection");
		/*
		 * phonePage.
		 * verifyFeatureDetaiils("AOL®, Yahoo!®, MSN®, and ICQ® Instant Messenger®");
		 * phonePage.verifyFeatureDetaiils("HiFi Ringers™, MegaTones®, Wallpapers");
		 * phonePage.
		 * verifyFeatureDetaiils("MegaTones®, Wallpaper, HiFi Ringers®, & Games");
		 * phonePage.verifyFeatureDetaiils("Voice Notes");
		 */
		phonePage.verifyPhoneImage(phonePage.undefinedImage);
	}

	/**
	 * Verifying phone page Device details for Samsung device
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void verifyPhonePageDeviceNFeaturesForSamsungDevice(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Case : Verify Phone page Device and Features For Samsung device ");
		Reporter.log("Test Data : PAH user with samsung device");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on phone tab | Phone page should be displayed");
		Reporter.log("5. Navigated to phone page");
		Reporter.log("6. Verify Phone Page Device Name");
		Reporter.log("7. Verify Phone Page Device Protection soc");
		Reporter.log("8. Verify Phone Page Features");
		Reporter.log("9. Verify Phone Page Device Image");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonePage(myTmoData);
		PhonePage phonePage = new PhonePage(getDriver());

		LineSettingsPage lineSettings = new LineSettingsPage(getDriver());
		lineSettings.verifyAuthorabletext("Galaxy S7 edge - Blue Coral - 32GB", phonePage.deviceName);
		phonePage.checkPHPSocDetaiils("No Device Protection");
		phonePage.verifyFeatureDetaiils("Edge UX");
		phonePage.verifyFeatureDetaiils("Qualcomm® Snapdragon™ 820 processor");
		phonePage.verifyFeatureDetaiils("Android 6.0 Marshmallow");
		phonePage.verifyFeatureDetaiils("Water & Dust Resistance");
		phonePage.verifyPhoneImage(phonePage.samsungImage);
	}
}
