/**
 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.pages.payments.UpdateCardPage;
import com.tmobile.eservices.qa.pages.payments.WalletPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author pshiva
 *
 */
public class EditCardPageTest extends PaymentCommonLib {

	/**
	 * US577189 UI- L3- Edit Card info 6.0
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testEditCardPageAngular(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US577189 UI- L3- Edit Card info 6.0");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved card.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 7: verify Card details | Card Details should be displayed");
		Reporter.log("Step 8: verify Card number, Cvv | Card should be masked and Cvv should not be displayed");
		Reporter.log("Step 9: Update Card details and click save changes button| Spoke page should be displayed");
		Reporter.log("Step 10: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 11: verify updated Card details | Updated Card Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		WalletPage walletPage = navigateToMyWalletPage(myTmoData);
		if (walletPage.verifyStoredCard()) {
			addCardWallet(myTmoData, walletPage);
		}
		navigateToAngular6PaymentCollectionsPage();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyNewPageLoaded();
		String storedCardNo = spokePage.verifyAndClickStoredCardAngular();
		UpdateCardPage editCardPage = new UpdateCardPage(getDriver());
		editCardPage.verifyCardPageLoaded(PaymentConstants.Edit_card_Header);
		editCardPage.verifyEditCardPageElements(storedCardNo, "v6");
		String nick = editCardPage.updateCardDetailsAndSave(myTmoData.getPayment(), "v6");
		spokePage.verifyPageLoaded();
		spokePage.clickUpdatedStoredCard(storedCardNo);
		editCardPage.verifyUpdatedCardDetails(myTmoData.getPayment(), nick);
	}

	/**
	 * US590356 Phase 1 Update payment method pages (Add bank/add card) 6.0
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testDeleteCardPageAngular(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US590356 Phase 1 Update payment method pages (Add bank/add card) 6.0");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved card.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 7: verify Card details | Card Details should be displayed");
		Reporter.log("Step 8: verify Card number, Cvv | Card should be masked and Cvv should not be displayed");
		Reporter.log("Step 9: Update Card details and click save changes button| Spoke page should be displayed");
		Reporter.log("Step 10: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 11: verify updated Card details | Updated Card Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		WalletPage walletPage = navigateToMyWalletPage(myTmoData);
		if (walletPage.verifyStoredCard()) {
			if (walletPage.verifyWalletIsFullMsg()) {
				deleteAndVerifyStoredBankWallet(walletPage);
			}
			addCardWallet(myTmoData, walletPage);
		}
		navigateToAngular6PaymentCollectionsPage();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyNewPageLoaded();
		String storedCardNo = spokePage.verifyAndClickStoredCardAngular();
		UpdateCardPage editCardPage = new UpdateCardPage(getDriver());
		editCardPage.verifyCardPageLoaded(PaymentConstants.Edit_card_Header);
		editCardPage.verifyEditCardPageElements(storedCardNo, "v6");
		editCardPage.clickRemoveFromMyWalletLink();
		editCardPage.verifyRemoveFromMyWalletModal();
		editCardPage.clickYesDeleteOnModal();
		spokePage.verifyNewPageLoaded();
		spokePage.verifyDeletedCardIsNotDisplayed(storedCardNo);
	}

	/**
	 * US310777 UI- L3- Edit Card info 1.5
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testEditCardPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US310777 UI- L3- Edit Card info 1.5");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved card.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 7: verify Card details | Card Details should be displayed");
		Reporter.log("Step 8: verify Card number, Cvv | Card should be masked and Cvv should not be displayed");
		Reporter.log("Step 9: Update Card details and click save changes button| Spoke page should be displayed");
		Reporter.log("Step 10: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 11: verify updated Card details | Updated Card Details should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");
		SpokePage spokePage = navigateToSpokePage(myTmoData);
		spokePage.verifyPageLoaded();
		String storedCardNo = spokePage.verifyAndClickStoredCard();
		UpdateCardPage editCardPage = new UpdateCardPage(getDriver());
		editCardPage.verifyCardPageLoaded(PaymentConstants.Edit_card_Header);
		editCardPage.verifyEditCardPageElements(storedCardNo, "e1.5");
		String nick = editCardPage.updateCardDetailsAndSave(myTmoData.getPayment(), "v6");
		spokePage.verifyPageLoaded();
		spokePage.clickUpdatedStoredCard(storedCardNo);
		editCardPage.verifyUpdatedCardDetails(myTmoData.getPayment(), nick);
	}

	/**
	 * US598462 7.0 Add Card/edit card HTML implementation.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testAddOrEditCardHtmlAngular(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US598462 7.0 Add Card/edit card HTML implementation.");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved card.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 7: verify Card details | Card Details should be displayed");
		Reporter.log("Step 8: verify Card number, Cvv | Card should be masked and Cvv should not be displayed");
		Reporter.log("Step 9: Update Card details and click save changes button| Spoke page should be displayed");
		Reporter.log("Step 10: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 11: verify updated Card details | Updated Card Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		OneTimePaymentPage oneTimePaymentPage = navigateToNewpaymentBladePage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyNewPageLoaded();
		String storedCardNo = spokePage.verifyAndClickStoredCardAngular();
		UpdateCardPage editCardPage = new UpdateCardPage(getDriver());
		editCardPage.verifyCardPageLoaded(PaymentConstants.Edit_card_Header);
		editCardPage.verifyEditCardPageElements(storedCardNo, "v6");
		String nick = editCardPage.updateCardDetailsAndSave(myTmoData.getPayment(), "v6");
		spokePage.verifyPageLoaded();
		spokePage.clickUpdatedStoredCard(storedCardNo);
		editCardPage.verifyUpdatedCardDetails(myTmoData.getPayment(), nick);
	}

	/**
	 * US590355 Phase 1 Update payment method pages (Add bank/add card) 1.5x
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testDeleteCardPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US590355 Phase 1 Update payment method pages (Add bank/add card) 1.5x");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved card.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 7: Click on Remove from my wallet button | Delete modal should be displayed");
		Reporter.log(
				"Step 8: Click on Yes, Delete CTA | Saved payment record should be deleted and redirected to Spoke page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		WalletPage walletPage = navigateToMyWalletPage(myTmoData);
		if (walletPage.verifyStoredCard()) {
			addCardWallet(myTmoData, walletPage);
		}
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		getDriver().navigate().to(System.getProperty("environment")+"/onetimepayment");
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyPageLoaded();
		String storedCardNo = spokePage.verifyAndClickStoredCard();
		UpdateCardPage editCardPage = new UpdateCardPage(getDriver());
		editCardPage.verifyCardPageLoaded(PaymentConstants.Edit_card_Header);
		editCardPage.verifyEditCardPageElements(storedCardNo, "1.5");
		editCardPage.clickRemoveFromMyWalletLink();
		editCardPage.verifyRemoveFromMyWalletModal();
		editCardPage.clickYesDeleteOnModal();
		spokePage.verifyPageLoaded();
		spokePage.verifyDeletedCardIsNotDisplayed(storedCardNo);
	}

	/**
	 * US577189 UI- L3- Edit Card info 6.0
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testEditCardPageWallet(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US577189 UI- L3- Edit Card info 6.0");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved card.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 7: verify Card details | Card Details should be displayed");
		Reporter.log("Step 8: verify Card number, Cvv | Card should be masked and Cvv should not be displayed");
		Reporter.log("Step 9: Update Card details and click save changes button| Spoke page should be displayed");
		Reporter.log("Step 10: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 11: verify updated Card details | Updated Card Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		WalletPage walletPage = navigateToMyWalletPage(myTmoData);
		if (walletPage.verifyStoredCard()) {
			if (walletPage.verifyWalletIsFullMsg()) {
				deleteAndVerifyStoredBankWallet(walletPage);
			}
			addCardWallet(myTmoData, walletPage);
		}
		String storedCardNo = walletPage.verifyAndClickStoredCard();
		UpdateCardPage editCardPage = new UpdateCardPage(getDriver());
		editCardPage.verifyCardPageLoaded(PaymentConstants.Edit_card_Header);
		editCardPage.verifyEditCardPageElements(storedCardNo, "v6");
		String nick = editCardPage.updateCardDetailsAndSave(myTmoData.getPayment(), "v6");
		walletPage.verifyPageLoaded();
		walletPage.clickUpdatedStoredCard(storedCardNo);
		editCardPage.verifyUpdatedCardDetails(myTmoData.getPayment(), nick);
	}

	/**
	 * US590356 Phase 1 Update payment method pages (Add bank/add card) 6.0
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testDeleteCardPageWallet(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US590356 Phase 1 Update payment method pages (Add bank/add card) 6.0");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved card.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 7: verify Card details | Card Details should be displayed");
		Reporter.log("Step 8: verify Card number, Cvv | Card should be masked and Cvv should not be displayed");
		Reporter.log("Step 9: Update Card details and click save changes button| Spoke page should be displayed");
		Reporter.log("Step 10: Click on Saved Card blade | Edit Card page should be displayed");
		Reporter.log("Step 11: verify updated Card details | Updated Card Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		WalletPage walletPage = new WalletPage(getDriver());
		getDriver().navigate().to(System.getProperty("environment") + "/mywallet");
		walletPage.verifyPageLoaded();
		walletPage.clickCloseModal();
		if (!walletPage.verifyWalletIsFullMsg()) {
			addCardWallet(myTmoData, walletPage);
		}
		String storedCardNo = walletPage.verifyAndClickStoredCard();
		deleteStoredCard(storedCardNo, "version");
		walletPage.verifyPageLoaded();
		walletPage.verifyDeletedPaymentMethodIsNotDisplayed(storedCardNo);
	}

}