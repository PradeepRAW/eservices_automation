package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.global.StoreLocatorPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.IdentityFraudRejectPage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewIntroductionPage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewQuestionPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class IdentityFraudRejectPageTest extends ShopCommonLib {

	
	/**
	 * US448180 - Fraud - Review Questions
	 * @param data
	 * @param myTmoData
	 */
    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING})
    public void verifyFraudReviewQuestions(ControlTestData data, MyTmoData myTmoData) {
	Reporter.log("Test Case name : US448180 - Fraud - Review Questions");
	Reporter.log(
			"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("1. Launch the application | Application Should be Launched");
	Reporter.log("2. Login to the application | User Should be login successfully");
	Reporter.log("3. Verify Home page | Home page should be displayed");
	Reporter.log("4. Click on shop link | Shop page should be displayed");
	Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
	Reporter.log("6. Click on 'Use your own phone' option | Rate plan page should be displayed");
	Reporter.log("7. Click 'Continue' button on Rate plan page | Cart Page should be displayed");
	Reporter.log("8. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
	Reporter.log("9. Click on 'Continue' CTA | Payment Information section should be displayed");
    Reporter.log("10. Click on Accept and Continue|Fraud review page should be displayed");
    Reporter.log("11. Click on Continue CTA |Continue CTA should be clicked");
	Reporter.log("12. Verify Cancel CTA| Cancel CTA should be displayed and should be enabled");
	Reporter.log("13. Verify Submit CTA| Submit CTA should be displayed and should be disabled");
	Reporter.log("14. Select option from review questions |Option should be select");
	Reporter.log("15. Click on Submit |Order reject screen be displayed");
	Reporter.log("================================");
	Reporter.log("Actual Output:");
	
	
	navigateToCartPageFromAALBYODFlow(myTmoData);
	CartPage cartPage = new CartPage(getDriver());
	cartPage.verifyCartPage();
    cartPage.clickContinueToShippingButton();	
	cartPage.clickContinueToPaymentButton();
	cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
    cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
    cartPage.enterCardNumber(myTmoData.getCardNumber());
    cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
    cartPage.enterCVV(myTmoData.getPayment().getCvv());
    cartPage.clickServiceCustomerAgreementCheckBox();
    cartPage.placeOrderCTAEnabled();
    cartPage.clickAcceptAndPlaceOrder();

    IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
    identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
    identityReviewIntroductionPage.verifyIdentityReviewIntroductionCancelCTA();
    identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
    identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
    
    IdentityReviewQuestionPage identityReviewQuestionPage =  new IdentityReviewQuestionPage(getDriver());
    identityReviewQuestionPage.verifyReviewQuestionPage();
    identityReviewQuestionPage.verifySubmitDisabled();
    identityReviewQuestionPage.clickFirstOption();
    identityReviewQuestionPage.verifySubmitEnabled();
    identityReviewQuestionPage.clickSubmit();
    
    IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
    identityFraudRejectPage.verifyFraudRejectPage();
    identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
    
    
	}	
	
	/**
	 * US448180 - Fraud - Review Questions
	 * @param data
	 * @param myTmoData
	 */

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING})
    public void verifyFraudReviewQuestionsNonBYOD(ControlTestData data, MyTmoData myTmoData) {
	Reporter.log("Test Case name : US448180 - Fraud - Review Questions");
	Reporter.log(
			"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("1. Launch the application | Application Should be Launched");
	Reporter.log("2. Login to the application | User Should be login successfully");
	Reporter.log("3. Verify Home page | Home page should be displayed");
	Reporter.log("4. Click on shop link | Shop page should be displayed");
	Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
	Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
	Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
	Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
	Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
	Reporter.log("10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
	Reporter.log("11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
	Reporter.log("12. Click on 'Continue' CTA | Payment Information section should be displayed");
    Reporter.log("13. Click on Accept and Continue|Fraud review page should be displayed");
    Reporter.log("14. Click on Continue CTA |Continue CTA should be clicked");
	Reporter.log("15. Verify Cancle CTA| Cancle CTA should be displayed and should be enabled");
	Reporter.log("16. Verify Submit CTA| Submit CTA should be displayed and should be disabled");
	Reporter.log("17. Select option from review questions |Option should be select");
	Reporter.log("18. Click on Submit |Order reject screen be displayed");
	Reporter.log("================================");
	Reporter.log("Actual Output:");
	
	
	navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
	CartPage cartPage = new CartPage(getDriver());
	cartPage.clickContinueToPaymentButton();
	cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
    cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
    cartPage.enterCardNumber(myTmoData.getCardNumber());
    cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
    cartPage.enterCVV(myTmoData.getPayment().getCvv());
    
    cartPage.clickServiceCustomerAgreementCheckBox();
    cartPage.placeOrderCTAEnabled();
    
    cartPage.clickAcceptAndPlaceOrder();
    IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
    identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
    identityReviewIntroductionPage.verifyIdentityReviewIntroductionCancelCTA();
    identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
    identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
    
    IdentityReviewQuestionPage identityReviewQuestionPage =  new IdentityReviewQuestionPage(getDriver());
    identityReviewQuestionPage.verifyReviewQuestionPage();
    identityReviewQuestionPage.verifySubmitDisabled();
    identityReviewQuestionPage.clickFirstOption();
    identityReviewQuestionPage.verifySubmitEnabled();
    identityReviewQuestionPage.clickSubmit();
    
    IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
    identityFraudRejectPage.verifyFraudRejectPage();
    identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
    
    
	}	
	
	/**
	 * US448183 - Fraud - Review Questions CTAs
	 * @param data
	 * @param myTmoData
	 */
    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING})
    public void verifyFraudReviewQuestionsCTA(ControlTestData data, MyTmoData myTmoData) {
	Reporter.log("Test Case name : US448183 - Fraud - Review Questions CTAs");
	Reporter.log(
			"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("1. Launch the application | Application Should be Launched");
	Reporter.log("2. Login to the application | User Should be login successfully");
	Reporter.log("3. Verify Home page | Home page should be displayed");
	Reporter.log("4. Click on shop link | Shop page should be displayed");
	Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
	Reporter.log("6. Click on 'Use your own phone' option | Rate plan page should be displayed");
	Reporter.log("7. Click 'Continue' button on Rate plan page | Cart Page should be displayed");
	Reporter.log("8. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
	Reporter.log("9. Click on 'Continue' CTA | Payment Information section should be displayed");
    Reporter.log("10. Click on Accept and Continue|Fraud review page should be displayed");
	Reporter.log("11. Click on Continue CTA |Continue CTA should be clicked");
	Reporter.log("12. Verify Cancle CTA| Cancle CTA should be displayed and should be enabled");
	Reporter.log("13. Verify Submit CTA| Submit CTA should be displayed and should be disabled");
	Reporter.log("14. Select option from review questions |Option should be select");
	Reporter.log("15. Verify Submit CTA| Submit CTA should be displayed and should be enabled");
	Reporter.log("16. Click on Submit |Order reject screen be displayed");
	Reporter.log("================================");
	Reporter.log("Actual Output:");
	
	
	navigateToCartPageFromAALBYODFlow(myTmoData);
	CartPage cartPage = new CartPage(getDriver());
	cartPage.verifyCartPage();
    cartPage.clickContinueToShippingButton();	
	cartPage.clickContinueToPaymentButton();
	cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
    cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
    cartPage.enterCardNumber(myTmoData.getCardNumber());
    cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
    cartPage.enterCVV(myTmoData.getPayment().getCvv());
    
    cartPage.clickServiceCustomerAgreementCheckBox();
    cartPage.placeOrderCTAEnabled();
    
    cartPage.clickAcceptAndPlaceOrder();
    IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
    identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
    identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
    identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
    
    IdentityReviewQuestionPage identityReviewQuestionPage =  new IdentityReviewQuestionPage(getDriver());
    identityReviewQuestionPage.verifyReviewQuestionPage();
    identityReviewQuestionPage.verifySubmitDisabled();
    identityReviewQuestionPage.clickFirstOption();
    identityReviewQuestionPage.verifySubmitEnabled();
    identityReviewQuestionPage.clickSubmit();
    
    IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
    identityFraudRejectPage.verifyFraudRejectPage();
    identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
	}
	

	/**
	 * US448183 - Fraud - Review Questions CTAs
	 * @param data
	 * @param myTmoData
	 */
    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING})
	public void verifyFraudReviewQuestionsCTANONBYOD(ControlTestData data, MyTmoData myTmoData) {
	Reporter.log("Test Case name : US448183 - Fraud - Review Questions CTAs");
	Reporter.log(
			"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("1. Launch the application | Application Should be Launched");
	Reporter.log("2. Login to the application | User Should be login successfully");
	Reporter.log("3. Verify Home page | Home page should be displayed");
	Reporter.log("4. Click on shop link | Shop page should be displayed");
	Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
	Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
	Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
	Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
	Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
	Reporter.log("10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
	Reporter.log("11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
	Reporter.log("12. Click on 'Continue' CTA | Payment Information section should be displayed");
	Reporter.log("13. Click on Accept and Continue|Fraud review page should be displayed");
	Reporter.log("14. Click on Continue CTA |Continue CTA should be clicked");
	Reporter.log("15. Verify Cancle CTA| Cancle CTA should be displayed and should be enabled");
	Reporter.log("16. Verify Submit CTA| Submit CTA should be displayed and should be disabled");
	Reporter.log("17. Select option from review questions |Option should be select");
	Reporter.log("18. Verify Submit CTA| Submit CTA should be displayed and should be enabled");
	Reporter.log("19. Click on Submit |Order reject screen be displayed");
	Reporter.log("================================");
	Reporter.log("Actual Output:");
	
	navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
	CartPage cartPage = new CartPage(getDriver());
	cartPage.clickContinueToPaymentButton();
	cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
    cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
    cartPage.enterCardNumber(myTmoData.getCardNumber());
    cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
    cartPage.enterCVV(myTmoData.getPayment().getCvv());
    
    cartPage.clickServiceCustomerAgreementCheckBox();
    cartPage.placeOrderCTAEnabled();
    
    cartPage.clickAcceptAndPlaceOrder();
    IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
    identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
    identityReviewIntroductionPage.verifyIdentityReviewIntroductionContinueCTA();
    identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
    
    IdentityReviewQuestionPage identityReviewQuestionPage =  new IdentityReviewQuestionPage(getDriver());
    identityReviewQuestionPage.verifyReviewQuestionPage();
    identityReviewQuestionPage.verifySubmitDisabled();
    identityReviewQuestionPage.clickFirstOption();
    identityReviewQuestionPage.verifySubmitEnabled();
    identityReviewQuestionPage.clickSubmit();
    
    IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
    identityFraudRejectPage.verifyFraudRejectPage();
    identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
	}

	/**
	 * US448172 -Fraud - Reject Screen (immediate and canceling and failing questions)
	 * @param data
	 * @param myTmoData
	 */
    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING})
	public void verifyFraudRejectScreen(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : US448172 - Fraud Screen -Rejected Authentication");
		Reporter.log(
				"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Use your own phone' option | Rate plan page should be displayed");
		Reporter.log("7. Click 'Continue' button on Rate plan page | Cart Page should be displayed");
		Reporter.log("8. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("9. Click on 'Continue' CTA | Payment Information section should be displayed");
		Reporter.log("10. Click on Accept and Continue|Fraud verification page should be displayed");
		Reporter.log("11. Click on Continue|Review question page should be displayed");
		Reporter.log("12. Select answer in mutiple choice and click continue|Reject Screen will be displayed");
		Reporter.log("13. Verify authorable Text and Header |Authorable Text and Header should be displayed");
		Reporter.log("14. Verify primary and secondary  CTA |Primary and Secondary  CTA should be displayed");
		Reporter.log("15. Verify header and footer |Header and Footer  should not be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToCartPageFromAALBYODFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();

		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
		IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
		identityReviewQuestionPage.verifyReviewQuestionPage();
		identityReviewQuestionPage.clickFirstOption();
		identityReviewQuestionPage.clickSubmit();
		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		identityFraudRejectPage.verifyRejectFraudScreenPageHeaderText();
		identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
		identityFraudRejectPage.verifyPrimaryRejectFraudScreen();
		identityFraudRejectPage.verifySecondaryRejectFraudScreenCTA();
		identityFraudRejectPage.verifyIdentityFraudRejectheaderAndFooter();
		
		}
	
	/**
	 * US448172 -Fraud - Reject Screen (immediate and canceling and failing questions)
	 * @param data
	 * @param myTmoData
	 */
    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING})
	public void verifyFraudRejectScreenThroughCancel(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : US448172 - Fraud Screen -Rejected Authentication");
		Reporter.log(
				"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Use your own phone' option | Rate plan page should be displayed");
		Reporter.log("7. Click 'Continue' button on Rate plan page | Cart Page should be displayed");
		Reporter.log("8. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("9. Click on 'Continue' CTA | Payment Information section should be displayed");
		Reporter.log("10. Click on Accept and Continue|Fraud verification page should be displayed");
		Reporter.log("11. Click on Continue|Review question page should be displayed");
		Reporter.log("12. Select cancel |  cancel modal should be displayed");
		Reporter.log("13. Select Yes, cancel this order|Reject Screen will be displayed");
		Reporter.log("14. Verify authorable Text and Header |Authorable Text and Header should be displayed");
		Reporter.log("15. Verify primary and secondary  CTA |Primary and Secondary  CTA should be displayed");
		Reporter.log("16. Verify header and footer |Header and Footer  should not be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToCartPageFromAALBYODFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionCancelCTA();
		identityReviewIntroductionPage.clickSecondaryCancelCTAIconOnIdentityReviewIntroductionWarningModal();
		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		identityFraudRejectPage.verifyRejectFraudScreenPageHeaderText();
		identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
		identityFraudRejectPage.verifyPrimaryRejectFraudScreen();
		identityFraudRejectPage.verifySecondaryRejectFraudScreenCTA();
		identityFraudRejectPage.verifyIdentityFraudRejectheaderAndFooter();
      }

	
	/**
	 * US448172 -Fraud - Reject Screen (immediate and canceling and failing questions)
	 * @param data
	 * @param myTmoData
	 */
    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING})
	public void verifyFraudRejectScreenNonBYOD(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : US448172 - Fraud Screen -Rejected Authentication");
		Reporter.log(
				"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
		Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log("10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
		Reporter.log("11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("12. Click on 'Continue' CTA | Payment Information section should be displayed");
        Reporter.log("13. Click on Accept and Continue|Fraud verification page should be displayed");
		Reporter.log("14. Click on Continue|Review question page should be displayed");
		Reporter.log("15. Select answer in mutiple choice and click continue|Reject Screen will be displayed");
		Reporter.log("16. Verify authorable Text and Header |Authorable Text and Header should be displayed");
		Reporter.log("17. Verify primary and secondary  CTA |Primary and Secondary  CTA should be displayed");
		Reporter.log("18. Verify header and footer |Header and Footer  should not be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();

		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
		IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
		identityReviewQuestionPage.verifyReviewQuestionPage();
		identityReviewQuestionPage.clickFirstOption();
		identityReviewQuestionPage.clickSubmit();
		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		identityFraudRejectPage.verifyRejectFraudScreenPageHeaderText();
		identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
		identityFraudRejectPage.verifyPrimaryRejectFraudScreen();
		identityFraudRejectPage.verifySecondaryRejectFraudScreenCTA();
		identityFraudRejectPage.verifyIdentityFraudRejectheaderAndFooter();
		
		}
	
	/**
	 * US448172 -Fraud - Reject Screen (immediate and canceling and failing questions)
	 * @param data
	 * @param myTmoData
	 */
    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING})
	public void verifyFraudRejectScreenThroughCancelNonBYOD(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : US448172 - Fraud Screen -Rejected Authentication");
		Reporter.log(
				"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
		Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log("10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
		Reporter.log("11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("12. Click on 'Continue' CTA | Payment Information section should be displayed");
        Reporter.log("13. Click on Accept and Continue|Fraud verification page should be displayed");
		Reporter.log("14. Click on Continue|Review question page should be displayed");
		Reporter.log("15. Select cancel |  cancel modal should be displayed");
		Reporter.log("15. Select Yes, cancel this order|Reject Screen will be displayed");
		Reporter.log("16. Verify authorable Text and Header |Authorable Text and Header should be displayed");
		Reporter.log("17. Verify primary and secondary  CTA |Primary and Secondary  CTA should be displayed");
		Reporter.log("18. Verify header and footer |Header and Footer  should not be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionCancelCTA();
		identityReviewIntroductionPage.clickSecondaryCancelCTAIconOnIdentityReviewIntroductionWarningModal();
		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		identityFraudRejectPage.verifyRejectFraudScreenPageHeaderText();
		identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
		identityFraudRejectPage.verifyPrimaryRejectFraudScreen();
		identityFraudRejectPage.verifySecondaryRejectFraudScreenCTA();
		identityFraudRejectPage.verifyIdentityFraudRejectheaderAndFooter();
      }

	/**
	 * US448179 -Fraud - Reject Screen CTAs (for immediate and failing/canceling review questions)
	 * @param data
	 * 
	 * @param myTmoData
	 */
    	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING})
		public void verifyFraudRejectScreenPrimaryAndSecondary(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : US448179 - Fraud Screen - Rejection page ");
		Reporter.log(
				"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("5. Select on 'Buy my own device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("6. Click Continue button on Consolidated Rate Plan Page | Cart Page should be displayed");
	    Reporter.log("7. Click 'Continue to shipping' button | Shipping details should be displayed");	
		Reporter.log("8. Click on 'Continue' CTA | Payment Information section should be displayed");
		Reporter.log("9. Click on Accept and Continue|Fraud verification page should be displayed");
		Reporter.log("10. Click on Continue|Review question page should be displayed");
		Reporter.log("11. Select answer in mutiple choice and click continue|Reject Screen will be displayed");
		Reporter.log("12. Verify authorable Text and Header |Authorable Text and Header should be displayed");
		Reporter.log("13. Verify primary and secondary  CTA |Primary and Secondary  CTA should be displayed");
		Reporter.log("14. Click Primary CTA  'Find a T-Mobile store near you'|Store Locator Page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToCartPageFromAALBYODFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
		IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
		identityReviewQuestionPage.verifyReviewQuestionPage();
		identityReviewQuestionPage.clickFirstOption();
		identityReviewQuestionPage.clickSubmit();
		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		identityFraudRejectPage.verifyRejectFraudScreenPageHeaderText();
		identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
		identityFraudRejectPage.verifyPrimaryRejectFraudScreen();
		identityFraudRejectPage.verifySecondaryRejectFraudScreenCTA();		
		identityFraudRejectPage.clickPrimaryRejectFraudCTA();
		StoreLocatorPage StoreLocatorPage = new StoreLocatorPage(getDriver());
		StoreLocatorPage.verifyStoreLocatorPage();
		
		}
	
	/**
	 * US448179 -Fraud - Reject Screen CTAs (for immediate and failing/canceling review questions)
	 * @param data
	 * 
	 * @param myTmoData
	 */

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING})
	public void verifyFraudRejectScreenPrimaryAndSecondaryNonBYOD(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : US448179 - Fraud Screen - Rejection page ");
		Reporter.log(
				"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
		Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log("10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
		Reporter.log("11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("12. Click on 'Continue' CTA | Payment Information section should be displayed");
        Reporter.log("13. Click on Accept and Continue|Fraud verification page should be displayed");
		Reporter.log("14. Click on Continue|Review question page should be displayed");
		Reporter.log("15. Select answer in mutiple choice and click continue|Reject Screen will be displayed");
		Reporter.log("16. Verify authorable Text and Header |Authorable Text and Header should be displayed");
		Reporter.log("17. Verify primary and secondary  CTA |Primary and Secondary  CTA should be displayed");
		Reporter.log("18. Click Primary CTA  'Find a T-Mobile store near you'|Store Locator Page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
		IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
		identityReviewQuestionPage.verifyReviewQuestionPage();
		identityReviewQuestionPage.clickFirstOption();
		identityReviewQuestionPage.clickSubmit();
		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		identityFraudRejectPage.verifyRejectFraudScreenPageHeaderText();
		identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
		identityFraudRejectPage.verifyPrimaryRejectFraudScreen();
		identityFraudRejectPage.verifySecondaryRejectFraudScreenCTA();		
		identityFraudRejectPage.clickPrimaryRejectFraudCTA();
		StoreLocatorPage StoreLocatorPage = new StoreLocatorPage(getDriver());
		StoreLocatorPage.verifyStoreLocatorPage();
		
		
		}
	
	/**
	 * US448179 -Fraud - Reject Screen CTAs (for immediate and failing/canceling review questions)
	 * @param data
	 * 
	 * @param myTmoData
	 */
    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING})
	public void verifyFraudRejectScreenPrimaryAndSecondaryClickSecondaryCTA(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : US448179 - Fraud Screen - Rejection page ");
		Reporter.log(
				"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("5. Select on 'Buy my own device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("6. Click Continue button on Consolidated Rate Plan Page | Cart Page should be displayed");
	    Reporter.log("7. Click 'Continue to shipping' button | Shipping details should be displayed");	
		Reporter.log("8. Click on 'Continue' CTA | Payment Information section should be displayed");
		Reporter.log("9. Click on Accept and Continue|Fraud verification page should be displayed");
		Reporter.log("10. Click on Continue|Review question page should be displayed");
		Reporter.log("11. Select answer in mutiple choice and click continue|Reject Screen will be displayed");
		Reporter.log("12. Verify authorable Text and Header |Authorable Text and Header should be displayed");
		Reporter.log("13. Verify primary and secondary  CTA |Primary and Secondary  CTA should be displayed");
		Reporter.log("14. Click Secondary CTA  'Back TO Shopping'|Shop Page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToCartPageFromAALBYODFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
		IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
		identityReviewQuestionPage.verifyReviewQuestionPage();
		identityReviewQuestionPage.clickFirstOption();
		identityReviewQuestionPage.clickSubmit();
		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		identityFraudRejectPage.verifyRejectFraudScreenPageHeaderText();
		identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
		identityFraudRejectPage.verifyPrimaryRejectFraudScreen();
		identityFraudRejectPage.verifySecondaryRejectFraudScreenCTA();		
		identityFraudRejectPage.clickSecondaryRejectFraudCTA();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyShoppage();
		
		}
	
	/**
	 * US448179 -Fraud - Reject Screen CTAs (for immediate and failing/canceling review questions)
	 * @param data
	 * 
	 * @param myTmoData
	 */
    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.AAL_PENDING})
	public void verifyFraudRejectScreenPrimaryAndSecondaryNonBYODClickSecondaryCTA(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case name : US448179 - Fraud Screen - Rejection page ");
		Reporter.log(
				"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
		Reporter.log("8. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("9. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log("10. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be displayed");
		Reporter.log("11. Click on 'Continue to shipping' CTA in cart page | Shipping Information section should be displayed");
		Reporter.log("12. Click on 'Continue' CTA | Payment Information section should be displayed");
        Reporter.log("13. Click on Accept and Continue|Fraud verification page should be displayed");
		Reporter.log("14. Click on Continue|Review question page should be displayed");
		Reporter.log("15. Select answer in mutiple choice and click continue|Reject Screen will be displayed");
		Reporter.log("16. Verify authorable Text and Header |Authorable Text and Header should be displayed");
		Reporter.log("17. Verify primary and secondary  CTA |Primary and Secondary  CTA should be displayed");
		Reporter.log("18. Click Secondary CTA  'Back TO Shopping'|Shop Page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
		IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
		identityReviewQuestionPage.verifyReviewQuestionPage();
		identityReviewQuestionPage.clickFirstOption();
		identityReviewQuestionPage.clickSubmit();
		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		identityFraudRejectPage.verifyRejectFraudScreenPageHeaderText();
		identityFraudRejectPage.verifyRejectFraudScreenIntroductionPagebodyText();
		identityFraudRejectPage.verifyPrimaryRejectFraudScreen();
		identityFraudRejectPage.verifySecondaryRejectFraudScreenCTA();		
		identityFraudRejectPage.clickSecondaryRejectFraudCTA();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyShoppage();
		
		
		}
    
	/**
	 * US512630AAL - Fraud pages: add Header back without back arrow
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = Group.AAL_PENDING)
	public void verifyFraudReviewPagesSedonaHeader(ControlTestData data, MyTmoData myTmoData) {
	Reporter.log("US512630AAL - Fraud pages: add Header back without back arrow");
	Reporter.log(
			"Test Data : User should be PAH or FULL customer who has AAL intent for a new voice line and device");
	Reporter.log("================================");
	Reporter.log("Test Steps | Expected Results:");
	Reporter.log("1. Launch the application | Application Should be Launched");
	Reporter.log("2. Login to the application | Home page should be displayed");
	Reporter.log("3. Click on shop | shop page should be displayed");
	Reporter.log("4. Click on 'Add A LINE' button | Customer intent page should be displayed");
	Reporter.log("5. Select on 'Buy my own device' option | Consolidated Rate Plan Page  should be displayed");
	Reporter.log("6. Click Continue button on Consolidated Rate Plan Page | Cart Page should be displayed");
    Reporter.log("7. Click 'Continue to shipping' button | Shipping details should be displayed");	
	Reporter.log("8. Click on 'Continue' CTA | Payment Information section should be displayed");
    Reporter.log("9. Click on Accept and Continue|Fraud review page should be displayed");
    Reporter.log("10. Validate Sedona Header | Sedona Header should be displayed");
    Reporter.log("11. Validate Back button Not displayed |Back Button should not be displayed");
	Reporter.log("12. Click on Continue CTA | Review Questions Page should be displayed");
	Reporter.log("13. Validate Sedona Header | Sedona Header should be displayed");
	Reporter.log("14. Validate Back button Not displayed |Back Button should not be displayed");
	Reporter.log("15. Select option from review questions |Option should be select");
	Reporter.log("16. Click on Submit |Order reject screen be displayed");
	Reporter.log("17. Validate Sedona Header | Sedona Header should be displayed");
    Reporter.log("18. Validate Back button Not displayed |Back Button should not be displayed");
	
	Reporter.log("================================");
	Reporter.log("Actual Output:");
	
	CartPage cartPage = navigateToCartPageFromAALBYODFlow(myTmoData);
	cartPage.verifyCartPage();
	cartPage.clickContinueToShippingButton();
	cartPage.clickContinueToPaymentButton();
	cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
	cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
	cartPage.enterCardNumber(myTmoData.getCardNumber());
	cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
	cartPage.enterCVV(myTmoData.getPayment().getCvv());
	cartPage.clickServiceCustomerAgreementCheckBox();
	cartPage.clickAcceptAndPlaceOrder();
	IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
	identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
	identityReviewIntroductionPage.verifySedonaHeaderDisplayed();
	identityReviewIntroductionPage.verifyBackButtonNotDisplayed();
	identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();
	IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
	identityReviewQuestionPage.verifyReviewQuestionPage();
	identityReviewQuestionPage.verifySedonaHeaderDisplayed();
	identityReviewQuestionPage.verifyBackButtonNotDisplayed();
	identityReviewQuestionPage.clickFirstOption();
	identityReviewQuestionPage.clickSubmit();
	IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
	identityFraudRejectPage.verifyFraudRejectPage();
	identityFraudRejectPage.verifySedonaHeaderDisplayed();
	identityFraudRejectPage.verifyBackButtonNotDisplayed();
	
	}
	
	/**
	 * CDCSM-161:Fraud Reject response (UI): Leverage latest payments API with
	 * system message
	 *
	 * @param data
	 * @param myTmoData
	 *
	 ***/
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testVerifyFraudRejectResponseModel(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :Fraud Reject response (UI): Leverage latest payments API with system message");
		Reporter.log("Data Condition | customer is eligible for AAL");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on 'Add A LINE' button | Customer intent page should be displayed");
		Reporter.log("5. Select on 'Buy my own device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("6. Click Continue button on Consolidated Rate Plan Page | Cart Page should be displayed");
		Reporter.log("7. Click 'Continue to shipping' button | Shipping details should be displayed");
		Reporter.log("8. Click on 'Continue' CTA | Payment Information section should be displayed");
		Reporter.log("9. Fill the details and click on Accept and Continue | fraud reject modal should be display");
		Reporter.log("10. Verify Sorry--we waren't able to verify your information text in fraud reject modal | Sorry--we waren't able to verify your information text should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		CartPage cartPage = navigateToCartPageFromAALBYODFlow(myTmoData);
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		identityFraudRejectPage.verifyRejectFraudScreenPageHeaderText();
		identityFraudRejectPage.verifyPrimaryRejectFraudScreen();
		identityFraudRejectPage.verifySecondaryRejectFraudScreenCTA();
		identityFraudRejectPage.verifyTextForFraudRejectModel();
	}
	}
