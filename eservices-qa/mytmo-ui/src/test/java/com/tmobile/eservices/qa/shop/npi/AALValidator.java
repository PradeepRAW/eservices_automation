package com.tmobile.eservices.qa.shop.npi;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.ConsolidatedRatePlanPage;
import com.tmobile.eservices.qa.pages.shop.DeviceIntentPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.ESignaturePage;
import com.tmobile.eservices.qa.pages.shop.InterstitialTradeInPage;
import com.tmobile.eservices.qa.pages.shop.OrderConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.PLPPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

import io.appium.java_client.ios.IOSDriver;

public class AALValidator extends ShopCommonLib {

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "aal1" })
	public void testAALFlowForNewSku1(MyTmoData myTmoData) {
		aalFlow(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "aal" })
	public void testAALFlowForNewSkus5(MyTmoData myTmoData) {
		aalFlow(myTmoData);
	}
	
    @Test(dataProvider = "byColumnName", enabled = true, groups = { "aal" })
    public void testAALFlowForNewSkus9(MyTmoData myTmoData) {
        aalFlow(myTmoData);
    }
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "aal" })
	public void testAALFlowForNewSkus13(MyTmoData myTmoData) {
		aalFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "aal" })
	public void testAALFlowForNewSkus17(MyTmoData myTmoData) {
		aalFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "aal" })
	public void testAALFlowForNewSkus21(MyTmoData myTmoData) {
		aalFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "aal" })
	public void testAALFlowForNewSkus25(MyTmoData myTmoData) {
		aalFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "aal" })
	public void testAALFlowForNewSkus29(MyTmoData myTmoData) {
		aalFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "aal" })
	public void testAALFlowForNewSkus33(MyTmoData myTmoData) {
		aalFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "aal" })
	public void testAALFlowForNewSkus37(MyTmoData myTmoData) {
		aalFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "aal" })
	public void testAALFlowForNewSkus41(MyTmoData myTmoData) {
		aalFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "mobile" })
	public void testAALFlowForNewSkuInMobile(MyTmoData myTmoData) {
		aalFlow(myTmoData);
	}


	public void aalFlow(MyTmoData myTmoData){
		navigateToDeviceIntentPageFromAALQuickLink(myTmoData);
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.clickBuyANewPhoneOption();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.clickOnContinueCTA();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.clickDeviceByName(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyDeviceNameAndManufacturerName(myTmoData.getDeviceName());
		pdpPage.selectMemory(myTmoData.getDeviceMemory());
		pdpPage.chooseColor(myTmoData.getDeviceColor());
		pdpPage.clickOnContinueAALButton();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		
		Double estimateDueTodayPrice = cartPage.getEstDueTodayCart();
		Reporter.log("Estimated Due today in cart page is Verified");
		Double estimateShippingPrice = cartPage.verifyAndGetEstimateShippingPrice();
		Reporter.log("Estimated Shipping Price in cart page is Verified");
		Double estimateSalesTaxPrice = cartPage.verifyAndGetEstimateSalexTaxPrice();
		Reporter.log("Estimated Due Sales Tax in cart page is Verified");
		Double dueTodayTotal = cartPage.getEstTotalDueTodayPriceOrderDetailsCartPage();
		Reporter.log("Estimated Due today in cart page is Verified");
		Double totalPrice = estimateDueTodayPrice + estimateShippingPrice + estimateSalesTaxPrice;
		cartPage.verifyTotalDueTodayPrices(dueTodayTotal,totalPrice);
		
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingAddress();
		if (getDriver() instanceof IOSDriver) {
			Reporter.log("Navigating to Payments page");
		} else {
			cartPage.clickEditShippingAddress();
			cartPage.fillWAAddressInfo();
		}

		cartPage.verifyE911AddressTitle();
		cartPage.verifyPPUAddressTitle();

		cartPage.verifyUPSGroungShippingMethodOption();
		cartPage.verifyTwoDayShippingMethodOption();
		cartPage.verifyNextDayDeliveryBtn();
		cartPage.clickContinueToPaymentButton();
		cartPage.clickBillingAndShippingCheckBox();
		cartPage.fillBillingAddressInfo();
		
		Double dueTodayOnCartPaymentPage = cartPage.getDueTodayCartPaymentPage();
		Double shippingAmountOnCartPaymentPage =cartPage.verifyAndGetEstimateShippingPricePaymentPage();
		Double salesTaxOnCartPaymentPage =cartPage.verifyAndGetEstimateSalexTaxPricePaymentPage();
		Double totalDuePrice = dueTodayOnCartPaymentPage + shippingAmountOnCartPaymentPage + salesTaxOnCartPaymentPage;
		Double totalDueTodayPaymentPage = cartPage.verifyAndGetDueTodayTotalPaymentPage();
		cartPage.verifyTotalDueTodayPrices(totalDueTodayPaymentPage,totalDuePrice);
	    cartPage.clickServiceCustomerAgreementCheckBox();

	    if(getDriver() instanceof IOSDriver){
	    	Reporter.log("AAL flow completed successfully");
		}else{
			cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
			cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
			cartPage.enterCVV(myTmoData.getPayment().getCvv());
			Reporter.log("Added credit card details");
		}

	    cartPage.clickAcceptAndPlaceOrder();
		ESignaturePage esignaturePage = new ESignaturePage(getDriver());
        esignaturePage.verifyESignaturePage();
        esignaturePage.selectESignatureCheckBox();
        esignaturePage.clickESignatureContinueCTA();
        esignaturePage.clickSignatureImage();
        esignaturePage.clickSelectStyleTab();
        esignaturePage.clickAdoptSignatureCTA();
        esignaturePage.clickSendTheOrderCTA();
        OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
        orderConfirmationPage.verifyOrderConfirmationPage();
        orderConfirmationPage.verifyOrderNumber();
        orderConfirmationPage.getOrderNumber();
        CommonPage commonPage = new CommonPage(getDriver());
		commonPage.takeScreenshot();
	}
}
