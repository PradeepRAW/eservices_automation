///**
// * 
// */
//package com.tmobile.eservices.qa.accounts.acceptance;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.testng.Reporter;
//import org.testng.annotations.Test;
//
//import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
//import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
//import com.tmobile.eservices.qa.accounts.AccountsConstants;
//import com.tmobile.eservices.qa.commonlib.Group;
//import com.tmobile.eservices.qa.data.MyTmoData;
//import com.tmobile.eservices.qa.pages.accounts.ChangePlanPage;
//import com.tmobile.eservices.qa.pages.accounts.PlansListPage;
//import com.tmobile.eservices.qa.pages.accounts.PlansReviewPage;
//
///**
// * @author prokarma
// *
// */
//public class ChangePlanForMBBLineTest extends AccountsCommonLib {
//	private final static Logger logger = LoggerFactory.getLogger(ChangePlanForMBBLineTest.class);
//
//	/**
//	 * 
//	 * 04_US16172_MYTMO Cloud_IR_Customer_Change_Data_Plan from_6 GB to
//	 * 2GB_Volta SOC should be removed
//	 * 
//	 * @param data
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
//			Group.IOS })
//	public void verifyChangeDataPlan6GBTo2GB(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("verifyChangeDataPlan6GBTo2GB method called in ChangePlanForMBBLineTest");
//		Reporter.log("Test Case : Verify Change Data Plan from 6GB  to 2GB.");
//		Reporter.log("Test Data : PAH/Full User on MBB plan with 6GB data plan");
//		Reporter.log("========================");
//		Reporter.log("Test Steps | Expected Results:");
//		Reporter.log("1. Launch the application | Application Should be Launched");
//		Reporter.log("2. Login to the application | User Should be login successfully");
//		Reporter.log("3. Click Plan Tab | Plan page should be displayed");
//		Reporter.log("4. Click Change Plan button | Plan list Page should be displayed");
//		Reporter.log("5. Select 2GB Mobile internet and click Next button | Change notice should be displayed");
//		Reporter.log("6. Click Continue Button | Review Changed should be displayed");
//		Reporter.log("7. Verify 2GB Mobile Internet Text | 2GB Mobile internet should be displayed");
//		Reporter.log("==============================");
//		Reporter.log("Actual Output:");
//
//		navigateToPlanListPage(myTmoData);
//		
//		ChangePlanPage changePlan = new ChangePlanPage(getDriver());
//		changePlan.clickMobileinternetTwoGB();
//		changePlan.clickNextbutton();
//		changePlan.clickContinuebtn();
//		changePlan.verifyReviewChangespage();
//		changePlan.getTextMobileinternetTwoGB("Mobile Internet 2GB");
//	}
//
//	/**
//	 * 035_MYTMO_IR_SL_PAH_Verify New monthly bill amount when TE services are
//	 * being carried over
//	 * 
//	 * @param data
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
//			Group.IOS })
//	public void verifyNewMonthlyBillamtCarriedOverTE(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("verifyNewMonthlyBillamtCarriedOverTE method called in ChangePlanForMBBLineTest");
//		Reporter.log(
//				"Test Case : MYTMO_IR_SL_PAH_Verify New monthly bill amount when TE services are being carried over");
//		Reporter.log("Test Data : PAH/Full User on MBB plan with 6GB data plan");
//		Reporter.log("==========================================");
//		Reporter.log("1. Launch application | Application should be launched");
//		Reporter.log("2. Login to the application | User should be able to login Successfully");
//		Reporter.log("3. Verify Home page | Home Page Should be displayed");
//		Reporter.log("4. Verify Plans Page | Plans Page Should be displayed");
//		Reporter.log("5. Click Change Plan Button | Change Plan page should be displayed");
//		Reporter.log("6. Select TI Plan and Click Next button | Change Notice Dialog box should be displayed");
//		Reporter.log("7. Click Continue Button | Review Change page should be displayed");
//		Reporter.log(
//				"8. Verify New Monthly and Current Monthly Total | New monthly and current monthly bill should be displayed");
//		Reporter.log("===========================================");
//		Reporter.log("Actual Output:");
//
//		navigateToPlanListPage(myTmoData);
//		ChangePlanPage changePlan = new ChangePlanPage(getDriver());
//		changePlan.clickTmobileOneWearable();
//		changePlan.clickNextbutton();
//		changePlan.clickContinuebtn();
//		changePlan.verifyReviewChangespage();
//		changePlan.verifycurrentmonthlyTotaldisplayed();
//		changePlan.verifyNewmonthlyTotaldisplayed();
//	}
//
//	/**
//	 * 027_MYTMO_IR_ML_PAH_MBB_CPS_Verify the Data soc selection for MBB lines
//	 * 
//	 * @param data
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
//			Group.IOS })
//	public void verifyDataSOCforMBB(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("verifyDataSOCforMBB method called in ChangePlanForMBBLineTest");
//		Reporter.log("Test Case : MYTMO_IR_ML_PAH_MBB_CPS_Verify the Data soc selection for MBB lines");
//		Reporter.log("Test Data : PAH/Full User on MBB plan with 6GB data plan");
//		Reporter.log("========================");
//		Reporter.log("Expected Test Steps");
//		Reporter.log("1. Launch application | Application should be launched");
//		Reporter.log("2. Login to the application | User should be able to login Successfully");
//		Reporter.log("3. Verify Home page | Home Page Should be displayed");
//		Reporter.log("4. Verify Plans Page | Plans Page Should be displayed");
//		Reporter.log("5. Click Change Plan Button | Change Plan page should be displayed");
//		Reporter.log("6. Select 2GB Plan and Click Next button | Change Notice Dialog box should be displayed");
//		Reporter.log("7. Click Continue Button | Review Change page should be displayed");
//		Reporter.log(
//				"8. Verify New Monthly and Current Monthly Total | New monthly and current monthly bill should be displayed");
//		Reporter.log("========================");
//		Reporter.log("Actual Test Steps");
//
//		navigateToPlanListPage(myTmoData);
//		ChangePlanPage changePlan = new ChangePlanPage(getDriver());
//		changePlan.clickMobileinternetTwoGB();
//		changePlan.clickNextbutton();
//		changePlan.clickContinuebtn();
//		changePlan.verifyReviewChangespage();
//		changePlan.getTextMobileinternetTwoGB("Mobile Internet 2GB");
//		changePlan.getTextremovedServices("Mobile Internet 6GB");
//	}
//
//	/**
//	 * 004_MYTMO_CPS_IQ_Wearable_Verify Customer is able to Change Plan to $20
//	 * T-Mobile ONE Wearable Plan
//	 * 
//	 * @param 4042792634/Test12345
//	 * 
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
//			Group.IOS })
//	public void verifyTmobileOneWearablePlan(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("verifyTmobileOneWearablePlan method called in ChangePlanForMBBLineTest");
//		Reporter.log(
//				"Test Case Name : MYTMO_CPS_IQ_Wearable_Verify Customer is able to Change Plan to $20 T-Mobile ONE Wearable Plan");
//		Reporter.log("Test Data : PAH/Full User on MBB plan");
//		Reporter.log("========================");
//		Reporter.log(AccountsConstants.EXPECTED_TEST_STEPS);
//		Reporter.log("1. Launch application | Application should be launched");
//		Reporter.log("2. Login to the application | User should be able to login Successfully");
//		Reporter.log("3. Verify Home page | Home Page Should be displayed");
//		Reporter.log("4. Verify Plans page | Plans Page Should be displayed");
//		Reporter.log("5. Verify Featured Plans page | Featured Plans Page Should be displayed");
//		Reporter.log("6. Verify Plans Comparison page | Plans Comparison Page Should be displayed");
//		Reporter.log(
//				"7. Verify One Plus International Title and Price | One Plus International Title and Price Should be displayed");
//		Reporter.log("===========================================");
//		Reporter.log(AccountsConstants.ACTUAL_TEST_STEPS);
//
//		navigateToPlanListPage(myTmoData);
//		PlansListPage plansListPage = new PlansListPage(getDriver());
//		plansListPage.verifyPlansListPage();
//		plansListPage.clickOntMobileONENCCWearableTE();
//		plansListPage.clickOnNextButton();
//		plansListPage.clickOnChangeNoticeContinueBtn();
//		PlansReviewPage plansReviewPage = new PlansReviewPage(getDriver());
//		plansReviewPage.verifyNCCWearableTE("T-Mobile ONE NCC Wearable TE");
//		plansReviewPage.verifyReviewChangespage();
//	}
//
//}
