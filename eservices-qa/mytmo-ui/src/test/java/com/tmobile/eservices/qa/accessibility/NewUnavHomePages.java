package com.tmobile.eservices.qa.accessibility;

import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;

public class NewUnavHomePages extends GlobalCommonLib {
	AccessibilityCommonLib accessibilityCommonLib = new AccessibilityCommonLib();
	/**
	 * Verify ContactUsPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testContactUsPage(ControlTestData data, MyTmoData myTmoData) {
	navigateToContactUsPage(myTmoData);
	accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ContactUsPage");
		accessibilityCommonLib.testAccessibility(getDriver(),"ContactUsPage");
	}
	/**
	 * Verify TermsAndConditionsPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testTermsAndConditionsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTermsAndConditionsPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"TermsAndConditionsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "TermsAndConditionsPage");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testCoveragePage(ControlTestData data, MyTmoData myTmoData) {
		 navigateToCoveragePage(myTmoData);
		 accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PlanPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "PlanPage");
	}

	/**
	 * Verify ReturnPolicyPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testReturnPolicyPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToReturnPolicyPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ReturnPolicyPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "ReturnPolicyPage");
	}

	/**
	 * Verify SupportPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testSupportPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToSupportPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"SupportPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "SupportPage");
	}

	/**
	 * Verify StoreLocatorPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void verifyStoreLocatorPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToStoreLocatorPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"StoreLocatorPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "StoreLocatorPage");
	}

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testTmobilePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTmobilePage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"TmobilePage");
		accessibilityCommonLib.testAccessibility(getDriver(), "TmobilePage");
	}


	/**
	 * Verify FamilyControlsPage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testFamilyControlsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToFamilyControlsPage(myTmoData, "Family Controls");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"FamilyControlsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "FamilyControlsPage");
	}

}
