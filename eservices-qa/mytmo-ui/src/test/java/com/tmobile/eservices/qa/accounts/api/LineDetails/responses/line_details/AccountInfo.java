package com.tmobile.eservices.qa.accounts.api.LineDetails.responses.line_details;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountInfo {

	@JsonProperty
	private Boolean isAccountSuspended;

	private AccountType accountType;

	public Boolean getAccountSuspended() {
		return isAccountSuspended;
	}

	public void setAccountSuspended(Boolean accountSuspended) {
		isAccountSuspended = accountSuspended;
	}

	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}
}
