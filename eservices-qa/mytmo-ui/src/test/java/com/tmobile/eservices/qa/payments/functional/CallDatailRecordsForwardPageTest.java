/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.CallDetailRecordsForwardPage;
import com.tmobile.eservices.qa.pages.payments.UsageOverviewPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

import io.appium.java_client.AppiumDriver;

/**
 * @author Sam Iurkov
 *
 */
public class CallDatailRecordsForwardPageTest extends PaymentCommonLib {
	
	/**
	 * TC002_E-Bill_Usage section_Filter options
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyFilterOptionsInCallDetailRecordsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify Filter Options In Call Detail Records Page");
		Reporter.log("Test Data: Regular MSISDN");
		Reporter.log("================================");
		Reporter.log("Test Steps|Expected results");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Overview Page should be displayed");
		Reporter.log("5.Click view all usage details | Verify call Record details page should be displayed");
		Reporter.log("6.Click Voice tab | Voice information should be displayed");
		Reporter.log("7.Click messaging tab | Messaging information should be displayed");
		Reporter.log("8.Click data tab | Data information should be displayed");
		Reporter.log("7.Click Filter in Voice tab | Filtered voice information should be displayed");
		Reporter.log("8.Click messaging tab | Messaging information should be displayed");
		Reporter.log("9.Click Filter in Message tab | Filtered messaging information should be displayed");
		Reporter.log("10.Click data tab | Data information should be displayed");
		Reporter.log("11.Click Filter in Data tab| Filtered data information should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps");
		
		CallDetailRecordsForwardPage callDetailRecordsForwardPage = navigateToCallDetailRecordsForwardPage(myTmoData);
		if (getDriver() instanceof AppiumDriver) {
			callDetailRecordsForwardPage.clickTabToggleSelector();
			callDetailRecordsForwardPage.clickVoiceTab();
			callDetailRecordsForwardPage.clickTabToggleSelector();
			callDetailRecordsForwardPage.clickMessagingtab();
			callDetailRecordsForwardPage.clickTabToggleSelector();
			callDetailRecordsForwardPage.clickDatatab();
		} else {
			callDetailRecordsForwardPage.clickVoiceTab();
			callDetailRecordsForwardPage.clickFilterInVoiceTab();
			callDetailRecordsForwardPage.verifyVoiceTableAllRowsAreDisplayed();
			callDetailRecordsForwardPage.clickMessagingtab();
			callDetailRecordsForwardPage.clickFilterInMessageTab();
			callDetailRecordsForwardPage.verifyMessagingTableAllRowsAreDisplayed();
			callDetailRecordsForwardPage.clickDatatab();
			callDetailRecordsForwardPage.clickFilterInDataTab();
			callDetailRecordsForwardPage.verifyDataTableAllRowsAreDisplayed();
		}
		//done
	}
	
	/**
	 * TC_Regression_Desktop-Usage Details for Virtual Line
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyUsageInformationforVirtualline(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify Usage details for Virtual Line");
		Reporter.log("Test Data Conditions: Msisdn with virtual line active");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Usage button |  Usage overview page should be displayed");
		Reporter.log(
				"5. Click ViewallUsageDetails link and Select Virtual Line selector |  Voice Date option should display");
		Reporter.log("6. Verify Destination tab option is display | Destination tab should display");
		Reporter.log("7. Verify Minutes option is display | Minutes option should display");
		Reporter.log("8. Verify Type option is display | Type option should display");
		Reporter.log("9. Verify Number option is display | Number option should display");
		Reporter.log("==============================");
		Reporter.log("Actual Steps:");

		CallDetailRecordsForwardPage callDetailRecordsForwardPage = navigateToCallDetailRecordsForwardPage(myTmoData);
		if (getDriver() instanceof AppiumDriver) {
			UsageOverviewPage usageOverviewPage = new UsageOverviewPage(getDriver());
			usageOverviewPage.verifyUsageInfoAndroid();
		} else {
			callDetailRecordsForwardPage.selectVirtuallinLineselector();
			callDetailRecordsForwardPage.verifyVoiceTableAllRowsAreDisplayed();
		}
		//done
	}
	
	/**
	 * TC013_Ebill_Billing_Validating the Voice Usage_Billed and Unbilled Usage
	 * Summary by using CDR
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyVoiceUsageForBilledandUnbilled(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyVoiceUsageForBilledandUnbilled method called in DelugeTest");
		Reporter.log("Test Case Name: Verify Voice Usage For Billed and Unbilled");
		Reporter.log("Test Data: Regular user with Voice usage data");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Overview Page should be displayed");
		Reporter.log("5.Click view all usage details | Call Record details page should display");
		Reporter.log("6.Click Voice tab | Voice Usage information should display");
		Reporter.log("7.Click Voice Filter | Filtered voice information should be  displayed");
		Reporter.log("8.Verify Download Usage Records Link | Download usage records link should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps");

		CallDetailRecordsForwardPage callDetailRecordsForwardPage = navigateToCallDetailRecordsForwardPage(myTmoData);
		if (getDriver() instanceof AppiumDriver) {
			callDetailRecordsForwardPage.clickTabToggleSelector();
			callDetailRecordsForwardPage.clickVoiceTab();
			callDetailRecordsForwardPage.verifyVoiceUsage();
		} else {
			callDetailRecordsForwardPage.clickVoiceTab();
			callDetailRecordsForwardPage.verifyVoiceUsage();
			callDetailRecordsForwardPage.clickFilterInVoiceTab();
			callDetailRecordsForwardPage.verifyFilteringByNumber();
			callDetailRecordsForwardPage.verifyDownloadUsageRecordsLink();
		}
		//done
	}
	
	/**
	 * TC012_EBill_Billing_Validating the Messaging Usage_Billed and Unbilled
	 * Usage Summary by using CDR
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyMessageUsage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify Message Usage Details");
		Reporter.log("Test Data: Regular user with Messaging usage data");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Overview Page should be displayed");
		Reporter.log("5.Click view all usage details | Call Record details page should display");
		Reporter.log("6.Click Messaging tab | Messaging details should display");
		Reporter.log("================================");
		Reporter.log("Actual Steps");

		CallDetailRecordsForwardPage callDetailRecordsForwardPage = navigateToCallDetailRecordsForwardPage(myTmoData);
		if (getDriver() instanceof AppiumDriver) {
			callDetailRecordsForwardPage.clickTabToggleSelector();
		}
		callDetailRecordsForwardPage.clickMessagingtab();
		callDetailRecordsForwardPage.verifyMessageUsage();
		callDetailRecordsForwardPage.clickFilterInMessageTab();
		callDetailRecordsForwardPage.verifyFilteringByNumber();
		callDetailRecordsForwardPage.verifyDownloadUsageRecordsLink();
		//done
	}
	
	/**
	 * TC011_EBill_Billing_Validating the Data Pass Usage_Billed and Unbilled
	 * Usage Summary by using CDR
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyDataPassUsage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: Verify Data Pass Usage Details");
		Reporter.log("Test Data: Regular user with Data usage data");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Overview Page should be displayed");
		Reporter.log("5.Click on all usage details button | Call details records page should be displayed");
		Reporter.log("6.Click data tab | Data pass Usage deatils should display");
		Reporter.log("================================");
		Reporter.log("Actual Steps");

		CallDetailRecordsForwardPage callDetailRecordsForwardPage = navigateToCallDetailRecordsForwardPage(myTmoData);
		callDetailRecordsForwardPage.clickDatatab();
		callDetailRecordsForwardPage.verifyDataPassUsage();
		//done
	}
	
	/**
	 * TC005_eServices_MyTMO_Usage_two new columns
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyTabsOnCallDetailRecordsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyTabsOnCallDetailRecordsPage method called in EBillTest");
		Reporter.log("Test Case : verify Tabs On Call Detail Records Page");
		Reporter.log("Test Data: PAH account");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Check Usage details | Call detail records page should be displayed");
		Reporter.log("5. Verify Voice tab | Voice Tab should be displayed");
		Reporter.log("6. Verify Messaging tab | Messaging Tab should be displayed");
		Reporter.log("7. Verify T-Mobile purchases tab | T-Mobile purchases Tab should be displayed");
		Reporter.log("8. Verify Third-party purchases tab | Third-party purchases Tab should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps");

		CallDetailRecordsForwardPage callDetailRecordsForwardPage = navigateToCallDetailRecordsForwardPage(myTmoData);
		if (getDriver() instanceof AppiumDriver) {
			callDetailRecordsForwardPage.verifyVoiceMobile();
			callDetailRecordsForwardPage.clickTabToggleSelector();
			callDetailRecordsForwardPage.verifyVoiceTab(PaymentConstants.USAGEPAGE_TABS_MOBILE_VOICE);
			callDetailRecordsForwardPage.verifyMessagingTab(PaymentConstants.USAGEPAGE_TABS_MOBILE_MESSAGING);
			callDetailRecordsForwardPage.verifyDataTab(PaymentConstants.USAGEPAGE_TABS_MOBILE_DATA);
			callDetailRecordsForwardPage.verifyTmobilePurchasesTab(PaymentConstants.USAGEPAGE_TABS_MOBILE_TMOBILE);
			callDetailRecordsForwardPage
					.verifyThirdPartyPurchasesTab(PaymentConstants.USAGEPAGE_TABS_MOBILE_THIRD_PARTY);
		} else {
			callDetailRecordsForwardPage.verifyTabs(PaymentConstants.USAGEPAGE_TABS_VOICE);
			callDetailRecordsForwardPage.verifyTabs(PaymentConstants.USAGEPAGE_TABS_MESSAGING);
			callDetailRecordsForwardPage.verifyTabs(PaymentConstants.USAGEPAGE_TABS_DATA);
			callDetailRecordsForwardPage.verifyTabs(PaymentConstants.USAGEPAGE_TABS_TMOBILE);
			callDetailRecordsForwardPage.verifyTabs(PaymentConstants.USAGEPAGE_TABS_THIRD_PARTY);
		}
	}
	
	/**
	 * 8134545839 / CDwebtest1 Ensure user can view past CDR.
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyUserCanViewPastCDR(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserCanViewPastCDR method called in UsageTest");
		Reporter.log("Test Case Name: Ensure user can view past CDR");
		Reporter.log("Test Data: ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on USAGE Button | Usage over view page should be displayed");
		Reporter.log("5. Verify call detail records page | Call detail records page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps");

		CallDetailRecordsForwardPage callDetailRecordsForwardPage = navigateToCallDetailRecordsForwardPage(myTmoData);
		String currentValue = callDetailRecordsForwardPage.getBillCycleSelectedOption();
		callDetailRecordsForwardPage.clickDropDownIcon();
		callDetailRecordsForwardPage.selectPastbillcycle();
		callDetailRecordsForwardPage.verifyBillCycleSelectedOption(currentValue);
	}
	
	/**
	 * 4042502803 / Test12345 Ensure user can toggle between lines on the CDR.
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RETIRED })
	public void verifyEnsureUserCanToggleBetweenLinesOnCDR(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEnsureUserCanToggleBetweenLinesOnCDR method called in UsageTest");
		Reporter.log("Test Case : Ensure user can toggle between lines on the CDR");
		Reporter.log("Test Data: ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on USAGE Button | Usage over view page should be displayed");
		Reporter.log("5. Verify MSISDN Data | MSISDN Data should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Steps");

		CallDetailRecordsForwardPage callDetailRecordsForwardPage = navigateToCallDetailRecordsForwardPage(myTmoData);
		callDetailRecordsForwardPage.clickChangeLineDropDown();
		callDetailRecordsForwardPage.selectLineFromDropDown();
	}

	
	/**
	 * DE167418_Unbilled 3rd-party purchases missing from CDR.
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testUnbilled3rdPartyPurchasesfromCDR(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case Name: DE167418_Unbilled 3rd-party purchases missing from CDR");
		Reporter.log("Test Data: PAH user with ThridParty Purchase data");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be dispalyed");
		Reporter.log("4.Click on Usage link | Usage Overview Page should be displayed");
		Reporter.log("5.Click view all usage details | Call Record details page should display");
		Reporter.log("6.Click Third Party Purchases Tab| Third Party Purchases information should display");
		Reporter.log("================================");
		Reporter.log("Actual Steps");

		CallDetailRecordsForwardPage callDetailRecordsForwardPage = navigateToCallDetailRecordsForwardPage(myTmoData);
		callDetailRecordsForwardPage.clickThirdPartyPurchasestab();

	}
	
}
