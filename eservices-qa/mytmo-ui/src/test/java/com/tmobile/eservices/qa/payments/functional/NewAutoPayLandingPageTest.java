package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.AutoPayPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayPage;
import com.tmobile.eservices.qa.pages.payments.TnCPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class NewAutoPayLandingPageTest extends PaymentCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(NewAutoPayLandingPageTest.class);

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutopayPage(ControlTestData data, MyTmoData myTmoData) {

		NewAutopayPage newAutopayPage = navigateToNewAutoPayPage(myTmoData);
		newAutopayPage.verifyAutoPayLandingPageFields();
	}

	/**
	 * US262982 GLUE Light Reskin - Cancel Autopay Modal
	 * 
	 * @param data
	 * @param myTmoData
	 *            Data Conditions - Regular account. Autopay ON
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "duplicate" })
	public void testVerifyAutopayCancelandTermsNConditionModal(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US262982 GLUE Light Reskin - Cancel Autopay Modal");
		Reporter.log("Data Conditions - Regular account. Autopay ON");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3. Click on autopay ON | Verify autopay page is displayed");
		Reporter.log(
				"Step 4. Click Cancel Autopay | Verify Are you sure you want to cancell autopay? pop-up is present");
		Reporter.log("Step 4.1 Verify header is present: Are you sure you want to cancel AutoPay?");
		Reporter.log("Step 4.2 Verify Yes, Cancel Autopay CTA is present");
		Reporter.log("Step 4.3 Verify No CTA is present");
		Reporter.log("Step 5. Click NO CTA | Verify autopay page is displayed");
		Reporter.log("Step 6: Click link \"terms and conditions\" | Verify termsAndConditions page is loaded");
		Reporter.log("Step 6.1 Verify header Terms and Conditions is present");
		Reporter.log("Step 6.2 Verify Back CTA is present");
		Reporter.log("Step 7. Click Back CTA | Verify Autopay page is displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		HomePage homePage = navigateToHomePage(myTmoData);
		if (!homePage.verifyAutopayisscheduled()) {
			setupAutopay(myTmoData);
		}

		AutoPayPage autoPayPage = navigateToAutoPayPagefromHomePage(myTmoData);
		autoPayPage.clickCancelAutoPayLink();
		autoPayPage.verifyAutoPayCancelModalHeader();
		autoPayPage.verifycancelAutoPayModal();
		autoPayPage.clickAutoPayCancelNoBtn();
		autoPayPage.verifyAutoPayLandingPageFields();
		autoPayPage.clicktermsNconditions();
		TnCPage tnCPage = new TnCPage(getDriver());
		tnCPage.verifyPageLoaded();
		tnCPage.validateTnCPage();
		tnCPage.clickBackButton();
		autoPayPage.verifyPageLoaded();
	}

	/**
	 * DE213866 MyTMO [Autopay] Unwanted text under FAQ link - “history, and
	 * electronic agreements related to your account.” to be removed
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS ,"autopay7"})
	public void testElectronicAgreementsTextRemovedunderFAQ(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Conditions - Autopay eligible User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Set-Auto-Pay | Auto pay page should be displayed");
		Reporter.log(
				"Step 4: Verify electronic agreements related to your account text removed under FAQ | electronic agreements related to your account text should be removed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AutoPayPage autopaylandingpage = navigateToAutoPayPage(myTmoData);
		autopaylandingpage.verifyElectronicAgreementTextRemoved();
	}
}
