/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.BillingPaymentsPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

/**
 * @author Bhavya
 *
 */
public class BillingPaymentPageTest extends PaymentCommonLib {

	/**
	 * US545935-CCS: Profile | Payment Methods | Display Number of Saved Payment
	 * Methods Verify PAH can view the number of stored payments-under
	 * Profile-Billing and Payments page Verify FULL can view the number of stored
	 * payments-under Profile-Billing and Payments page Verify STD can view the
	 * number of stored payments-under Profile-Billing and Payments page Verify
	 * Restricted can view the number of stored payments-under Profile-Billing and
	 * Payments page Verify PAH customer with no saved payment methods- no display
	 * on the blade Verify FULL customer with no saved payment methods- no display
	 * on the blade Verify STD customer with no saved payment methods- no display on
	 * the blade Verify Restricted customer with no saved payment methods- no
	 * display on the blade
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void VerifyNoPaymentMethodSaved(ControlTestData data, MyTmoData myTmoData) {
		logger.info("TestPaymnetMethod method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Verify with Saved payment method count in Profile-->billing & payments tab ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Profile page | Profile should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Verify Billing and paymnets");
		Reporter.log("6. Click Billing and paymnets|Payment method blade should be displayed");
		Reporter.log("7. Verify Payment Method Body |Payment method Body should be displayed");
		Reporter.log("8. Verify Payment Method chevron |Payment method chevron should be displayed");
		Reporter.log("9. Verify No Saved Payment Method Count|No Saved Payment method count should be displayed");
		Reporter.log("10. Clicked on Payment Method|Payment Collection Page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		BillingPaymentsPage billingpaymentspage = navigateToBillingPaymentsPage(myTmoData);
		billingpaymentspage.checkverifypaymentmethoddisplayed();
		billingpaymentspage.checkverifypaymentbodydisplayed();
		billingpaymentspage.checkverifypaymentchevrondisplayed();
		billingpaymentspage.verifyNoSavedPaymentMethodLink();
		billingpaymentspage.clickPaymentmethod();
		SpokePage paymentsCollectionPage = new SpokePage(getDriver());
		paymentsCollectionPage.verifyNewPageLoaded();
		paymentsCollectionPage.verifyNewPageUrl();

	}

	/**
	 * US545935-CCS: Profile | Payment Methods | Display Number of Saved Payment
	 * Methods Verify PAH can view the number of stored payments-under
	 * Profile-Billing and Payments page Verify FULL can view the number of stored
	 * payments-under Profile-Billing and Payments page Verify STD can view the
	 * number of stored payments-under Profile-Billing and Payments page Verify
	 * Restricted can view the number of stored payments-under Profile-Billing and
	 * Payments page Verify PAH customer with no saved payment methods- no display
	 * on the blade Verify FULL customer with no saved payment methods- no display
	 * on the blade Verify STD customer with no saved payment methods- no display on
	 * the blade Verify Restricted customer with no saved payment methods- no
	 * display on the blade
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PUB_RELEASE, "billing" })
	public void testSavedPaymnetMethodCount(ControlTestData data, MyTmoData myTmoData) {
		logger.info("TestPaymnetMethod method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Verify with Saved payment method count in Profile-->billing & payments tab ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Profile page | Profile should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Verify Billing and paymnets");
		Reporter.log("6. Click Billing and paymnets|Saved Payment method blade should be displayed");
		Reporter.log("7. Verify Saved Payment Method Count|Saved Payment method count should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		/*
		 * ProfilePage profilePage = navigateToProfilePage(myTmoData);
		 * profilePage.clickBillingPayments(); BillingAndPaymentsPage
		 * billingAndPaymentsPage=new BillingAndPaymentsPage(getDriver());
		 * billingAndPaymentsPage.verifySavedPaymentMethodLink();
		 * billingAndPaymentsPage.clickSavedPaymentMethodLink(); SpokePage
		 * paymentsCollectionPage = new SpokePage(getDriver());
		 * paymentsCollectionPage.verifyNewPageLoaded();
		 */

		SpokePage paymentsCollectionPage = navigateToPaymentsCollectionPage(myTmoData);
		// Code needs to write for Count of Payment Methods
		int Count = paymentsCollectionPage.gettotalPaymentbuttons();
		paymentsCollectionPage.clickBackButton();
		BillingPaymentsPage billingAndPaymentsPage = new BillingPaymentsPage(getDriver());
		billingAndPaymentsPage.verifySavedPaymentMethodCount(Count);

	}
}
