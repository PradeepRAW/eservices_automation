package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.BlockingPage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;

/**
 * @author rnallamilli
 * 
 */
public class BlockingPageTest extends GlobalCommonLib {

	// Regression Started

	/**
	 * TC_274958 MyTMo_Profile_Blocking_Block international talk, text and data
	 * roaming On-Off verification
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyBlockPageInternationalRoamingToggle(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBlockPageInternationalRoamingToggle - Proflie-Blocking link-International Roaming-on/off ");
		Reporter.log("Test Case : Verify Blocking International roaming ");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Blocking link | Blocking page should be displayed");
		Reporter.log(
				"6. verify Block international roaming header|  Block international roaming header should be displayed");
		Reporter.log("7. Click on International Roaming show more button |  Show more button should be clicked");
		Reporter.log("8. Click on International Roaming On/Off button | International Roaming On/Off should be done");

		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		BlockingPage blockingPage = navigateToBlockingPage(myTmoData, "Blocking");
		blockingPage.verifyBlockInternationalHeader();
		blockingPage.clickBlockInternationalMorebtn();
		blockingPage.clickBlockInternationalToggle();
	}

	/**
	 * TC_274959 MyTMo_Profile_Blocking_Block charged international data roaming
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyBlockChargeInternationalRoaming(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBlockChargeInternationalRoaming - Proflie-Blocking link-International charging-on/off ");
		Reporter.log("Test Case : Verify Block charge international roaming ");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Blocking link | Blocking page should be displayed");
		Reporter.log("6. verify Block charge header| Block charge header should be displayed");
		Reporter.log("7. Click on Block charge show more button | Show more button should be clicked");
		Reporter.log(
				"8. Click on Block charge international roaming On/Off button | Block charge international roaming On/Off should be done");

		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		BlockingPage blockingPage = navigateToBlockingPage(myTmoData, "Blocking");
		blockingPage.verifyBlockChargeHeader();
		blockingPage.clickBlockChargeMorebtn();
		blockingPage.clickBlockChargerToggle();
	}

	/**
	 * US327336/US454528:Profile | Re-Launch Enhancements | Rename Home |
	 * Breadcrumbs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyBlockSettingsBreadCrumbsLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : Msisdn with suspended line");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile link | Profile page should be displayed");
		Reporter.log("5. Click on blocking link | Blocking page should be displayed");
		Reporter.log(
				"6: Click on profile home bread crumb and verify home bread crumb active status | Home bread crumb should be active");
		Reporter.log("7: Verify profile page | Profile page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BlockingPage blockingPage = navigateToBlockingPage(myTmoData, "Blocking");
		blockingPage.clickProfileHomeBreadCrumb();

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}

	// Regression End

	/**
	 * US263280 (Mobile) OPEX - CPS : Fix/Get Top for CPS Error - user friendly UI
	 * message US265730 (Desktop) OPEX - CPS : Fix/Get Top for CPS Error - implement
	 * for desktop (convert + UI) *
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void testConvertCPSErrortoUserFriendlyUIMessage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on profile link| profile page should be displayed");
		Reporter.log("Step 4: click on blocking section |Blocking section  should be displayed");
		Reporter.log(
				"Step 5: select the toggle for Block charget toggle |  Free overseas text and roaming message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		loginToProfilePageAndClickOnLink(myTmoData, "Blocking");
		BlockingPage blockingPage = new BlockingPage(getDriver());
		blockingPage.verifyBlockChargeHeader();
		blockingPage.clickBlockChargerToggle();
		blockingPage.clickBlockInternationalMorebtn();
	}

	/**
	 * US306531:Move Blocking Controls | Angular Changes | Sprint 11
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyBlockingControls(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Move Blocking Controls | Angular Changes | Sprint 11");
		Reporter.log("Test Case : verify Blocking Controls");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Blocking link | Blocking page should be displayed");
		Reporter.log("6. Verify blocking page elements| Blocking page elements should be displayed");

		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		BlockingPage blockingPage = navigateToBlockingPage(myTmoData, "Blocking");
		// blockingPage.verifyBlockInternationalRoamingToggleOperations();
		// blockingPage.verifyBlockChargeToggleOperations();
		blockingPage.verifyScamIDToggleOperations();
		blockingPage.verifyScamBlockToggleOperations();
		blockingPage.verifyBlockContentDownloadsOperations();
		// blockingPage.verifyBlockAllMessagesToggleOperations();
		blockingPage.verifyBlockTextAndPictureMessagesToggleOperations();
		blockingPage.verifyBlockTMOmailToggleOperations();
		blockingPage.verifyBlockInstantMessageToggleOperations();
	}

	/**
	 * US307284:Move Blocking Controls | Test Support
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyBlockingControlsEligilbiltyForCustomers(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Move Blocking Controls | Test Support");
		Reporter.log("Test Case : verify Blocking Controls");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Blocking link | Blocking page should be displayed");
		Reporter.log("6. Verify blocking page elements| Blocking page elements should be displayed");

		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		BlockingPage blockingPage = navigateToBlockingPage(myTmoData, "Blocking");
		blockingPage.verifyBlockChargeHeader();
		blockingPage.verifyBlockInternationalHeader();
		blockingPage.verifyBlockAllMessagesHeader();
		blockingPage.verifyBlockAllMessagesToggle();
		blockingPage.verifyBlockContentDownloadsHeader();
		blockingPage.verifyBlockContentDownloadsToggle();
		blockingPage.verifyBlockInstantMessageHeader();
		blockingPage.verifyBlockInstantMessageToggle();
		blockingPage.verifyTMOmailHeader();
		blockingPage.verifyTMOmailToggle();
		blockingPage.verifyBlockTextMessagesHeader();
		blockingPage.verifyBlockTextMessagesToggle();
		blockingPage.verifyDeviceBlocksHeader();
		blockingPage.verifyDeviceBlockToggleToggle();
		blockingPage.verifyScamBlocksHeader();
		blockingPage.verifyScamBlockToggle();
		blockingPage.verifyScamIDHeader();
		blockingPage.verifyScamIDToggle();
	}

	/**
	 * US348834:Move Blocking Controls | SOC Conflict Handling Enhancement
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyBlockingControlsInternationalRoaming(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Move Blocking Controls | SOC Conflict Handling Enhancement");
		Reporter.log("Test Case : verify Blocking Controls");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile link | Profile page should be displayed");
		Reporter.log("5. Click on blocking link | Blocking page should be displayed");
		Reporter.log(
				"6. Verify block international roaming toggle oprerations | Block international roaming toggle status update should be success");
		Reporter.log(
				"7. Click on block international Roaming toggle | Block international roaming disable message should be displayed");
		Reporter.log(
				"8. Verify block charged international roaming toogle state | Block charged international roaming toogle should be in disabled state");

		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		BlockingPage blockingPage = navigateToBlockingPage(myTmoData, "Blocking");
		String s1 = blockingPage.verifyBlockInternationalRoamingToggleOperations();
		blockingPage.verifyBlockInternationalHeader();
		if (s1 == "ON") {
			blockingPage.clickBlockInternationalToggle();
		}
		blockingPage.verifyBlockChargedInternationRoamingDisableMsg();
		blockingPage.verifyBlockChargedInternationRoamingToggleDisableStatus();
	}

	/**
	 * US348834:Move Blocking Controls | SOC Conflict Handling Enhancement
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyBlockingControlsBlockAllMessages(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Move Blocking Controls | SOC Conflict Handling Enhancement");
		Reporter.log("Test Case : verify Blocking Controls");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile link | Profile page should be displayed");
		Reporter.log("5. Click on blocking link | Blocking page should be displayed");
		Reporter.log("6. Verify block all messages header| Block all messages header should be displayed");
		Reporter.log("7. Click on block all messages toggle | Click on block all messages toggle should be success");
		Reporter.log(
				"8. Verify block text and pictures disable message |Block text and pictures disable message should be displayed");
		Reporter.log(
				"9. Verify block text and pictures toggle state |Block text and picture messages toogle should be in disabled state");
		Reporter.log("10.Verify block instant disable message | Block instant disable message should be displayed");
		Reporter.log("11.Block instant message toogle state |Block instant message toogle should be in disabled state");
		Reporter.log("12.Verify TMO mail disable message | Block TMO mail disable message should be displayed");
		Reporter.log("13.Verify TMO mail toggle state |Block TMO mail toogle should be in disabled state");

		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		BlockingPage blockingPage = navigateToBlockingPage(myTmoData, "Blocking");
		blockingPage.verifyBlockAllMessagesHeader();
		blockingPage.clickBlockAllMessagesToggle();
		blockingPage.verifyblockTextMessagesDisableMsg();
		blockingPage.verifyBlockTextPictureMessageToggleDisableStatus();
		blockingPage.verifyblockTMOmailDisableMsg();
		blockingPage.verifyBlockTMOmailToggleDisableStatus();
		blockingPage.verifyblockInstantMessageDisableMsg();
		blockingPage.verifyBlockTMOmailToggleDisableStatus();
	}

	/**
	 * US359961:Move Blocking Controls | Suspend Line Handling Sdet Testing
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyMessageForSuspendLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Test Data : Msisdn with suspended line(Temporary suspension is required for the selected misisdn via Phone menu link from mytmo)");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile link | Profile page should be displayed");
		Reporter.log("5. Click on blocking link | Blocking page should be displayed");
		Reporter.log("6. Change current line to suspended line | Change to suspended line should be success");
		Reporter.log("7. Verify suspended line  message | Suspended line message should be displayed");

		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		BlockingPage blockingPage = navigateToBlockingPage(myTmoData, "Blocking");
		blockingPage.changeToSuspendedLine();
		blockingPage.verifySuspendLineMessage();
	}

	/**
	 * US359961:Move Blocking Controls | Suspend Line Handling Sdet Testing
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyMessageForActiveRestrictedUserLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : Msisdn with suspended line");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile link | Profile page should be displayed");
		Reporter.log("5. Click on blocking link | Blocking page should be displayed");
		Reporter.log(
				"6. Verify block international toggle state | Block international toggle should be in disabled state");
		Reporter.log(
				"7. Verify block charged international roaming toggle state | Block charged international roaming toggle should be in disabled state");
		Reporter.log("8. Verify device block toggle state | Device block should be in disabled state");
		Reporter.log("9. Verify scam id toggle state | Scam id toggle should be in disabled state");
		Reporter.log("10.Verify scam block toggle state | Scam block toggle should be in disabled state");
		Reporter.log(
				"11.Verify block content downloads toggle state | Block content downloads toggle should be in disabled state");
		Reporter.log(
				"12.Verify block all messages toggle state | Block  all messages toggle should be in disabled state");
		Reporter.log(
				"13.Verify block text and picture messages toggle state | Block text and picture messages toggle should be in disabled state");
		Reporter.log("14.Verify block tmo mail toggle state | Block tmo mail  toggle should be in disabled state");
		Reporter.log(
				"14.Verify block instant messages toggle state | Block instant messages toggle should be in disabled state");

		Reporter.log("================================");
		Reporter.log(" Actual Steps : ");

		BlockingPage blockingPage = navigateToBlockingPage(myTmoData, "Blocking");
		
		blockingPage.verifyBlockInternationRoamingToggleDisableStatus();
		blockingPage.verifyBlockChargedInternationRoamingToggleDisableStatus();
		blockingPage.verifyBlockDeviceToggleDisableStatus();
		blockingPage.verifyscamIDToggleDisableStatus();
		blockingPage.verifyscamBlockToggleDisableStatus();
		blockingPage.verifyblockContentDownloadsToggleDisableStatus();
		blockingPage.verifyblockAllMessagesToggleDisableStatus();
		blockingPage.verifyBlockTextPictureMessageToggleDisableStatus();
		blockingPage.verifyBlockTMOmailToggleDisableStatus();
		blockingPage.verifyBlockInstantMessageToggleDisableStatus();
	}
}
