/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.BillDetailsPage;
import com.tmobile.eservices.qa.pages.payments.BillingSummaryPage;
import com.tmobile.eservices.qa.pages.payments.HistoricBillsPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class BillDetailsPageTest extends PaymentCommonLib {
	
	private static final Logger logger = LoggerFactory.getLogger(BillDetailsPageTest.class);
	/**
	 * Verify Billing Current charges and Download PDF
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS,"billing"})
	public void verifyBillingCurrentChargesAndDownloadPDF(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBillingCurrentChargesAndDownloadPDF method in BillingSummaryTest");
		Reporter.log(
				"Test case: Verify Current Charges, Download PDF option in Billing & Compare to previous bill in Bill details.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("5: Click on Download PDF link | Print bill modal section should be invisible");
		Reporter.log("6: Click on current charges | Bill details page should be displayed");
		Reporter.log(
				"7: If Desktop: Click on Compare to last bill link | Last month bill and current month bull should be loaded in comaprison table");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.clickDownLoadPDF();
		Reporter.log("Print bill modal section is invisible");
		billingSummaryPage.clickViewBillDetails();
		BillDetailsPage billDetailsPage = new BillDetailsPage(getDriver());
		billDetailsPage.verifyPageLoaded();
		
		if (!(getDriver() instanceof AppiumDriver)) {
			billDetailsPage.clickCompareToLastBillLink();
			billDetailsPage.verifyPreviousMonthTitle();
			billDetailsPage.clickDownloadBill();
			billDetailsPage.verifyChooseBillDialogueDisplayed();
		}
	}
	
	/**
	 * TC_Regression_Desktop_ Desktop _Bill Details -Standard/Restricted users
	 * need to be shown generic verbiage about checking with PAH to see
	 * additional bill details
	 * DE238699-E1[ebill]:Content in Bill summary page is not loaded on ‘back to summary’ Link click in bill details page
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS,"billing"})
	public void verifyBilldetailsStandardUser(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("verifyBilldetailsStandardUser method");
		Reporter.log("Test Case : verifyBilldetailsStandardUser method");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4: Click on Billing link | Bill summary page should be displayed");
		Reporter.log("5: Click on current charges row | Bill Details page should be displayed");
		Reporter.log("6: Verify text for Verbiage Bill Details | Verbiage Bill Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		//BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		BillingSummaryPage billingSummaryPage = navigateTopreviousEbillfromBBAccount(myTmoData);
		
		//billingSummaryPage.changeBillCycle();
		billingSummaryPage.clickViewBillDetails();
		BillDetailsPage billDetailsPage = new BillDetailsPage(getDriver());
		billDetailsPage.verifyBillDetailsPage();
		billDetailsPage.selectBillCycle();
		billDetailsPage.verifyVerbiageForNonPAHUser();
	}
	
	
	/**
	 * DE90963-MyTMO [Desktop - Billing] Line selection does not persist to Bill Details page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups={Group.SPRINT})
	public void verifyLineSelectionisPersistedinBillingDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test case: Verify Line Selection is persisted on Billing Details Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Bill and Pay page should be displayed");
		Reporter.log("5: Click on Historical Bills Link | Select Billing Period |Billing Summary Page Should be displayed");
		Reporter.log("6: Click on Line1 in Current charges | Bill details page should be displayed | Line1 Number should be displayed in Account Dropdown");
		Reporter.log("7: Click on Back to Summary Page Link | Billing summary page should be displayed ");
		Reporter.log("8: Click on Line2 in Current charges | Bill details page should be displayed | Line2 Number should be displayed in Account Dropdown");		
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickPreviousBillCycle("Jun", "2017");
		BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
		for(int i=0;i<=billingSummaryPage.getNumberofLinesinCurrentCharges()-1;i++){
			String lineNumber = billingSummaryPage.clickandGetLineNumber(i);
			BillDetailsPage billDetailsPage = new BillDetailsPage(getDriver());
			billDetailsPage.verifyPageLoaded();
			billDetailsPage.verifyLineSelectedDropDown(lineNumber);
		}
	}
	
	
	/**
	 *DE239444
	E1:[Bill details page]-Download bill link click is redirecting to britebillpage when ebill is selected through Britebill
	Details

	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups={Group.SPRINT})
	public void verifyDownloadPDFinBillDetailsPagenavigatedthroughEbillfromBrightBill(ControlTestData data, MyTmoData myTmoData) {
	
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Bill and Pay page should be displayed");
		Reporter.log("5: Click on Historical Bills Link |  Historical Bills  Page Should be displayed");
		Reporter.log("6: Click on view bill | Billing summary page should be displayed");
		Reporter.log("7: Click on view bill details | Bill details should be displayed ");
		Reporter.log("8: Click on download pdf | choose bill dialogue  should be displayed ");	
		Reporter.log("9: verify summary and detailed bill dialogue | summary and detailed bill dialogue  should be displayed ");	
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickViewHistoricalBillsLink();
		HistoricBillsPage historicBillsPage = new HistoricBillsPage(getDriver());
		historicBillsPage.verifyPageLoaded();
		historicBillsPage.clickonviewbill();
		BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
		billingSummaryPage.verifyPageLoaded();
		billingSummaryPage.clickViewBillDetails();
		BillDetailsPage billDetailsPage = new BillDetailsPage(getDriver());
		billDetailsPage.verifyPageLoaded();
		billDetailsPage.clickDownloadBill();
		billDetailsPage.verifyChooseBillDialogueDisplayed();
		billDetailsPage.verifyViewSummaryandDetailedBillDisplayed();	
		
	}
	
	/**
	 * CDCDWG2-55 :SLBG Billing: [DESKTOP & MOBILE] Single Line Experience for Bill Landing page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"Sprint"})
	public void testSingleLineDisplayBriteBillPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("CDCDWG2-55 :SLBG Billing: [DESKTOP & MOBILE] Single Line Experience for Bill Landing page");
		Reporter.log("Test Case : Verify Single Line display in Legacy Billing Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Billing Link | Billing Summary Page should be displayed");
		Reporter.log(
				"4. Verify Signed in Msisdin alone display in Current Charges| Signed in Msisdin alone should display");

		Reporter.log("================================");

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickViewByLineSelector();
		billAndPaySummaryPage.verifySingleLineMsisdin(myTmoData.getLoginEmailOrPhone());

	}
	
	
	/**
	 * CDCDWG2-54 :SLBG Billing: [DESKTOP & MOBILE] Single Line Experience for Bill Details page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"Sprint"})
	public void testSingleLineDisplayBillDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("CDCDWG2-55 :SLBG Billing: [DESKTOP & MOBILE] Single Line Experience for Bill Details page");
		Reporter.log("Test Case : Verify Single Line display in Legacy Billing Details Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Billing Link | Billing Summary Page should be displayed");
		Reporter.log("4. Click View Details Billing Link | Billing Details Page should be displayed");
		Reporter.log(
				"4. Verify Signed in Msisdin alone display in Lines DropDown| Signed in Msisdin alone should display");

		Reporter.log("================================");

		BillDetailsPage billAndDetailsPage = navigateToBillDetailsPage(myTmoData);
		billAndDetailsPage.verifySingleLineMsisdininAccountDropDown(myTmoData.getLoginEmailOrPhone());

	}
}
