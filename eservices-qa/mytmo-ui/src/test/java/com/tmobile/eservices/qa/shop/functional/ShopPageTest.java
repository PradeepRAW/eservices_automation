package com.tmobile.eservices.qa.shop.functional;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.global.ContactUSPage;
import com.tmobile.eservices.qa.pages.shop.*;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class ShopPageTest extends ShopCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(ShopPageTest.class);

	/**
	 * US312042:MyTMO - Additional Terms - Display FRP and EIP Terms & Prices on
	 * Shop Home Landing Page
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testFRPandEIPpricesOnShopPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case:US312042:Additional Terms - Display FRP and EIP Terms & Prices on Shop Home Landing Page");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Verify Popular phones header | Popular phones header should be displayed");
		Reporter.log("6. Verify 'TODAY' text for device | 'TODAY' text should be displayed");
		Reporter.log(
				"7. Verify 'TODAY' price with dollar symbol | 'TODAY' price with dollar symbol should be displayed");
		Reporter.log("8. Verify 'down + tax' text | 'down + tax' text should be displayed");
		Reporter.log("9. Verify 'MONTHLY' text for device | 'MONTHLY' text should be displayed");
		Reporter.log(
				"10. Verify 'MONTHLY' price with dollar symbol | 'MONTHLY' price with dollar symbol should be displayed");
		Reporter.log(
				"11. Verify 'MONTHLY' installment months for device | 'MONTHLY' installment months should be displayed");
		Reporter.log("12. Verify 'Full retail' text for device | 'Full retail' text should be displayed");
		Reporter.log(
				"13. Verify 'Full retail' price with dollar symbol for device | 'Full retail' price with dollar symbol should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);

		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyTodayTextForDevices();
		shopPage.verifyTodayPriceForDevices();
		shopPage.verifyEIPDownPaymentTaxTextForDevices();
		shopPage.verifyMonthlyTextForDevices();
		shopPage.verifyMonthlyPriceForDevices();
		shopPage.verifyLoanTermLength();
		shopPage.verifyFRPPriceForFeaturedDevices();
		shopPage.verifyFRPTextForFeaturedDevices();
		shopPage.verifyFRPPriceForHolidayDevices();
		shopPage.verifyFRPTextForHolidayDevices();
	}

	/**
	 * TA1651836 - Test footer links presence on Shop page US492483 : V3 Gaps -
	 * plp/pdp Device, plp/pdp accessories, shop. C410979
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void testFooterlinksInShopPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US505343 : Mytmo- SHOP- Regression- Gap Analysis");
		Reporter.log("Data Condition | IR PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Verify footerlinks on Shop page | Footer links should be displayed");
		Reporter.log("5. Click on Shop tab on home page | User should be navigated to Shop Page ");
		Reporter.log(
				"6. Validate Device Name and Manufacture name on devices | Device Name and Manufacture name on devices should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.verifyFooterLinks();
		//shopPage.verifyAllDeviceManufacturerName();
		shopPage.verifyAllDeviceName();
	}

	/**
	 * US271504 :Redesigned Shop: Display modal upon clicking on banner legal terms
	 * hyperlink Sprint 04
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testLegalTermsHyperLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :Redesigned Shop: Display modal upon clicking on banner legal terms hyperlink");
		Reporter.log("Data Condition | IR PAH User ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Verify hero banner legal terms link | Legal terms link should be displayed");
		Reporter.log("5. Click hero banner legal terms link | Legal terms modal window should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage newShopPage = navigateToShopPage(myTmoData);
		newShopPage.clickLegalTermsLink();
		newShopPage.verifyLegalTermsModalWindow();
	}

	/**
	 * US284204:Shop Home Page - Display quick links
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testQuickLinksInShopPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:Shop Home Page - Display quick links");
		Reporter.log("Data Condition | PAH User ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("4. Verify Quick links | Quick links should be displayed in shop page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		ShopPage newShopPage = navigateToShopPage(myTmoData);
		newShopPage.clickQuickLinks("Apple");
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.verifyFilterDevicesInPLPPage("Apple");

		plpPage.navigateBack();
		newShopPage.verifyPageLoaded();
		newShopPage.clickQuickLinks("Samsung");
		plpPage.verifyPageLoaded();
		plpPage.verifyFilterDevicesInPLPPage("Samsung");

		plpPage.navigateBack();
		newShopPage.verifyPageLoaded();
		newShopPage.clickQuickLinks("All phones");
		plpPage.verifyPageLoaded();

		plpPage.navigateBack();
		newShopPage.verifyPageLoaded();
		newShopPage.clickQuickLinks("Accessories");
		AccessoriesStandAlonePLPPage accessoriesStandAlonePLPPage = new AccessoriesStandAlonePLPPage(getDriver());
		accessoriesStandAlonePLPPage.verifyAccessoriesStandAlonePLPPage();
	}

	/**
	 * US485067:AAL - Deeplinking from TMO to MYTMO (phone/byod) - load blank
	 * "consolidated charges page"
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION})
	public void testAALDeeplinkFlowWithCookiesAndUserNavigateToShopPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US485067:AAL - Deeplinking from TMO to MYTMO (phone/byod) - load blank consolidated charges page");
		Reporter.log("Data Condition | Non-PAH User and With out AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("5. Select Device | Device PDP page should be displayed");
		Reporter.log("6. Click on 'Add a Line' button | User should be navigated to MYTMO");
		Reporter.log("7. Verify error model | Error model should be displayed in shop page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		launchTmngProdURL(myTmoData);
		PlpPage phonesPage = new PlpPage(getDriver());
		phonesPage.checkPageIsReady();
		phonesPage.clickDeviceWithAvailability(myTmoData.getDeviceName());
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.verifyPhonePdpPageLoaded();
		phoneDetailsPage.clickOnAddALineBtn();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyAALIneligibleModalWindow();
	}

	/**
	 * Verify Add A line button enabled for PAH
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void verifyAddALineButton(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyAddALineButton method called in AALTest");
		Reporter.log("Verify Add A line button is redirecting to Dot net page");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Shop | Add a Line button should be displayed in Shop page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToShopPage(myTmoData);

		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyPageLoaded();
		shopPage.verifyPageUrl();
		shopPage.clickQuickLinks("Add a line");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
	}

	/**
	 * 031_MYTMO_IR_Desktop_ML_PAH_Pending plan change from TE to TI_Verify the
	 * error message in Shop page for Add a line 
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void verifyUserNoteligbleAddline(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : MYTMO_IR_Desktop_ML_PAH_Pending plan change from TE to TI_Verify the error message in Shop page for Add a line.");
		Reporter.log("Data Condition | Standard User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop tab | Shop Page should be displayed");
		Reporter.log("5. Verify Add a line is not eligble | Add a line ineligibility message should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		navigateToShopPage(myTmoData);

		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyPageLoaded();
		shopPage.verifyPageUrl();
		shopPage.clickQuickLinks("Add a line");
		shopPage.verifyAddALineEligibility();
	}

	/**
	 * TA1910085 :Ensure Marquee banner is shown on top of page and takes customer
	 * to correct location based on the banner displayed. C410980 This Test Needs
	 * Maintenance after Every Major release
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.UAT })
	public void testMarqueeBannerTakesCustomerToDesiredLocation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :Ensure Marquee banner is shown on top of page and takes customer to correct location based on the banner displayed.");
		Reporter.log("Data Condition | STD PAH User ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Verify Marquee Banner on Shop Page| Marquee Banner should be displayed");
		Reporter.log("6. Click on Shop Now on Banner | User should navigate to pre filter PLP");
		Reporter.log("7. Click on Any device on PLP | User should navigate to PDP");
		Reporter.log("8. Click on Add To Cart | User should navigate to Lineselector Page");
		Reporter.log("9. Click on any Line | User should navigate to Lineselector Details Page");
		Reporter.log("10. Click on Skip Trade In | User should navigate to device protection Page");
		Reporter.log("11. Click on Continue on Device protection page | User should navigate to Accessory List");
		Reporter.log("12. Click on Skip Accessories | User should navigate to Cart Page ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickOnShopNowButton();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickDeviceByName(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickOnSkipTradeInLink();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage").equals("false")) {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
	}

	/**
	 * US284204:SHOP page: CTA navigation from Shop banner to Call care (on mobile)
	 * or contact us page (on desktop)
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testNavigateToContactUsPageUponClickingBannerInMobile(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:SHOP page: CTA navigation from Shop banner to Call care:Mobile");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		ShopPage newShopPage = navigateToShopPage(myTmoData);
		newShopPage.clickOnHeroBannersContactUsButton();

		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
	}

	/**
	 * US325399: Shop Landing Page - Promo offer text Modal Pop up
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPromoOfferTextModalPopUp(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US325399: Shop Landing Page - Promo offer text Modal Pop up");
		Reporter.log("Data Condition | Standard User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Promo Offer for Any Feature Device | Promo offer modal should be displayed");
		Reporter.log("6. Verify Promo Offer Title | Promo Offer Title should be displayed");
		Reporter.log("7. Verify Promo Offer Text | Promo Offer Text should be displayed");
		Reporter.log("8. Click Outside Promo Modal | Promo Modal should get closed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickFirstOfferPromo();
		shopPage.verifyOfferPromoModal();
		shopPage.verifyOfferPromoModalTitle();
		shopPage.verifyOfferPromoModalText();
		shopPage.clickOutsideOfferPromo();
		shopPage.verifyPageLoaded();
		shopPage.verifyOfferPromoModalNotDisplayed();
	}

	/**
	 * US388893: AAL - Known intent Blocked path - lead max voice/non max MBB to
	 * Modal(quick links/banner CTA)
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testBlockedMessageForMaxVoiceLinesReachedCustomers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case name :US388893- AAL - Known intent Blocked path - lead max voice/non max MBB to Modal(quick links/banner CTA)");
		Reporter.log(
				"Test Data : User should be PAH or  FULL customer who has the max voice lines on their BAN, but not max MBB");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line button in shop page | Modal pop up should be dsiplayed");
		Reporter.log("6. Verify Authorable Title as 'Let's talk' | Authorable Title('Let's talk') should be displayed");
		Reporter.log("7. Verify Authorable text line1 | Authorable text line1 should be displayed");
		Reporter.log(
				"8. Verify Authorable text line2('To a line') message | Authorable text line2('To a line') message should be displayed");
		Reporter.log("9. Verify Contact us CTA | Contact us CTA should be displayed");
		Reporter.log("10. Click on Contact US | Contact Page should be displayed");

		Reporter.log("Actual Result:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		shopPage.verifyAALModalPopup();
		shopPage.verifyAALModalPopupTitleAndText();
		shopPage.verifyContactusOnModalpopup();
		shopPage.clickContactUsBtnInModalpopup();

		ContactUSPage contactUsPage = new ContactUSPage(getDriver());
		contactUsPage.verifyContactUSPage();
	}

	/**
	 * US340859 : AAL: Entry points for SHOP PAGE/Banner CTAs (known intent) - error
	 * messages
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAALEligibilityCheckForPastDueOrDelinquentAcc(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case#US340859 : AAL: Entry points for SHOP PAGE/Banner CTAs (known intent) - error messages");
		Reporter.log("Data Condition | Account should be pastdue or delinquent");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log(
				"5. Click Add a line button in shop page |Authorable ineligibility message for puerto rico customer should be displayed");

		Reporter.log(
				"6. Verify authorable ineligibility message |Authorable ineligibility message for delinquent account should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		shopPage.verifyAALIneligibleModalWindow();
	}

	/**
	 * US340859 : AAL: Entry points for SHOP PAGE/Banner CTAs (known intent) - error
	 * messages
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAALEligibilityCheckForAccountTenuredaysLessThan60(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case#US340859 : AAL: Entry points for SHOP PAGE/Banner CTAs (known intent) - error messages");
		Reporter.log("Data Condition | Account tenure days less than 60");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");

		Reporter.log("5. Click Add a line button in shop page |AAL ineligible modal window should be displayed");
		Reporter.log(
				"6. Verify authorable ineligibility message |Authorable ineligibility message for account tenure days less than 60 should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		shopPage.verifyAALIneligibleModalWindow();
	}

	/**
	 * US463443: MyTMO - Additional Terms - AAL - Featured Device SHOP PAGE Pricing
	 * Display
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testFeatureDevicePricesInShopPage(ControlTestData data, MyTmoData myTmoData, TMNGData tMNGData) {
		Reporter.log("US463443: MyTMO - Additional Terms - AAL - Featured Device SHOP PAGE Pricing Display");
		Reporter.log("Data Condition | Std PAH User With AAL eligibility");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Verify Featured deices | Featured devices should be displayed");
		Reporter.log("5. Verify Device, Today price and Save price in V1  | Today price should be displayed ");
		Reporter.log("6. Verify Device, Monthly price and Save price in V2  | Monthly price should be displayed ");
		Reporter.log(
				"7. Verify Device, Installment months and Save Months in V3  | Installment months should be displayed ");
		Reporter.log(
				"8. Verify Device, Full retail price and Save price in V4  | Full retail price should be displayed ");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.verifyTodayPriceForFeaturedDevices();
		shopPage.verifyMonthlyPriceForDevices();
		shopPage.verifyLoanTermLength();
		shopPage.verifyFRPPriceForFeaturedDevices();
	}

	/**
	 * TA1742376 : QATA0c:Not getting the pending rate plan message on quick links
	 * or/PDP for accounts which has pending rate plan DE222268 : QC 548974 /
	 * QATA0c:Not getting the pending rate plan message on quick links or/PDP for
	 * accounts which has pending rate plan
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testPendingRatePlanModeHeaderlInShopPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case: TA1742376 : QATA0c:Not getting the pending rate plan message on quick links or/PDP for accounts which has pending rate plan");
		Reporter.log("Data Condition | STD PAH User");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on shop link | Shop page should be displayed");
		Reporter.log("3. Click on 'Add A LINE' button | Pending Rate Plan model header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("Add a line");
		shopPage.verifyPendingRatePlanModelHeader();
	}

	/**
	 * US555229 : Update API - Block IQ customers and show existing modal (for
	 * calling care)
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testToDisplayModelForIQcustomersInShopPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case: US555229 : Update API - Block IQ customers and show existing modal (for calling care)");
		Reporter.log("Data Condition | IQ customer");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on shop link | Shop page should be displayed");
		Reporter.log("3. Click on 'Add A LINE' link through quick links | Ineligible modal should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		shopPage.verifyAALIneligibleModalWindow();

	}
	
	/**
	 * US612992: UI - Allow customers with to progress pass Shop Home Page (max voice for BAN, but not max MBB)
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testErrorMessageForMaxVoiceLinesAndMaxMBBLinesCustomers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US612992: UI - Allow customers with to progress pass Shop Home Page (max voice for BAN and max MBB)");
		Reporter.log("Data Condition | AAL eligible, user should max Voice lines and max MBB lines");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | Home Page should be displayed");
		Reporter.log("2. Click On Shop | Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Authorable error message model should be displayed");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		shopPage.verifyAALIneligibleModalWindow();
		shopPage.verifyAALInEligibilityMessageForMaxLinesAndMaxMBB();
	}
/**
 * Retired: US246913, US216312
 */


/**
 * US638699: UI - MyTMO R3 - Technical-  Update UNO cookie information from intent to transactionType
 * @param data
 * @param myTmoData
 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUNOCookiedInformationFromIntentToTransactionType(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US638699: UI - MyTMO R3 - Technical-  Update UNO cookie information from intent to transactionType");
		Reporter.log("Data Condition | Std PAH User ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | UNO PLP page should be displayed");
		Reporter.log("6. Select device in UNO PLP page | UNO PDP page should be displayed");
		Reporter.log("7. Pass the intent value to the transactionType(Upgrade) | intent value to transactionType(Upgrade) should be passed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	
		
		
		
	}

	/**
	 * US638700: MyTMO R3 - Technical - Update UNO PDP redirection URLs
	 * @param data
	 * @param myTmoData
	 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
		public void testVerifyUNOPDPRedirectionURL(ControlTestData data, MyTmoData myTmoData) {
			Reporter.log("Test Case: US638700: MyTMO R3 - Technical - Update UNO PDP redirection URLs");
			Reporter.log("Data Condition | Std PAH User");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | User Should be login successfully");
			Reporter.log("3. Verify Home page | Home page should be displayed");
			Reporter.log("4. Click on shop link | Shop page should be displayed");
			Reporter.log("5. Click on see all phones | UNO PLP page should be displayed");
			Reporter.log("6. Select any device in UNO PLP page | UNO PDP page should be displayed");
			Reporter.log("7. Send the SKU in PDP page Redirection URLs | UNO PDP redirection URLs should be displayed");
			
			Reporter.log("================================");
			Reporter.log("Actual Output:");
			
			
			
		}

}