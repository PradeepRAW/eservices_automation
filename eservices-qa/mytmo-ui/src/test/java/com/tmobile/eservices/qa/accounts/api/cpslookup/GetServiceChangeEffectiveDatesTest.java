package com.tmobile.eservices.qa.accounts.api.cpslookup;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class GetServiceChangeEffectiveDatesTest extends AccountsApi {

	public Map<String, String> tokenMap;

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetServiceChangeEffectiveDates(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating getServiceChangeEffectiveDates  ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", "964804820");
		tokenMap.put("msisdn1", "4256233675");
		tokenMap.put("msisdn2", "4256233546");
		tokenMap.put("msisdn3", "4253623148");
		tokenMap.put("productType", "GSM");

		String requestBody = new ServiceTest().getRequestFromFile("getServiceChangeEffectiveDates.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getServiceChangeEffectiveDates(apiTestData, updatedRequest);

		String operationName = "Get Service Change Effective Dates";

		String todayDate;
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		System.out.println(dateFormatter.format(new Date()));
		todayDate = dateFormatter.format(new Date());

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success :"+response.body().asString());

			Assert.assertTrue(response.jsonPath().getString("productLevelEffectiveDates.serviceChangeEffectiveDate.currentDate").contains(todayDate));
			Reporter.log("Expected getServiceChangeEffectiveDates are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	//This test case is no more required as per Siva's email
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetServiceChangeEffectiveDates_MBB(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating getServiceChangeEffectiveDates  ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("productType", "MBB");

		String requestBody = new ServiceTest().getRequestFromFile("getServiceChangeEffectiveDates.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getServiceChangeEffectiveDates(apiTestData, updatedRequest);

		String operationName = "Get Service Change Effective Dates_MBB";

		String todayDate;
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		System.out.println(dateFormatter.format(new Date()));
		todayDate = dateFormatter.format(new Date());

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("currentDate").contains(todayDate));
			Reporter.log("Expected getServiceChangeEffectiveDates are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

}
