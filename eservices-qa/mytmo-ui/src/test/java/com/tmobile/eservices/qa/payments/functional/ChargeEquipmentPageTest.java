package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.ChargeEquipmentPage;
import com.tmobile.eservices.qa.pages.payments.EIPDetailsPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class ChargeEquipmentPageTest extends PaymentCommonLib{

    private static final Logger logger = LoggerFactory.getLogger(ChargeEquipmentPageTest.class);

    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT})
    public void testChargeEquipmentPageLoad(ControlTestData data, MyTmoData myTmoData) {
        logger.info("testChargeEquipmentPageLoad method called in ChargeEquipmentPageTest");
        Reporter.log("Billing - Verify Charge Equipment Page");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("6. Click on Equipment | Charge Equipment page should be loaded");
        Reporter.log("================================");
       
        navigateToChargeEquipmentPage(myTmoData,"Equipment");
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT})
    public void testEquipmentDetailsOnChargeEquipmentPageLoad(ControlTestData data, MyTmoData myTmoData) {
        logger.info("testEquipmentDetailsOnChargeEquipmentPageLoad method called in ChargeEquipmentPageTest");
        Reporter.log("Billing - Verify EIP Bill Summary Details");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("6. Click on Equipment | Charge Equipment page should be loaded");
        Reporter.log("7. Check for the equipment details that assigned to the line| Should be able to see the equipment details ");
        Reporter.log("8. Click on View equipment details page | Should be able to navigate to EIP page");
        Reporter.log("================================");

        ChargeEquipmentPage chargeEquipmentPage =  navigateToChargeEquipmentPage(myTmoData,"Equipment");
        chargeEquipmentPage.verifyEquipmentDetails();
        chargeEquipmentPage.clickViewEquipmentDetailsLink();
        EIPDetailsPage eipDetails = new EIPDetailsPage(getDriver());
        eipDetails.verifyPageLoaded();
    }

    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT})
    public void testEquipmentTotalAmountOnChargeEquipmentPageLoad(ControlTestData data, MyTmoData myTmoData) {
        logger.info("testEquipmentTotalAmountOnChargeEquipmentPageLoad method called in ChargeEquipmentPageTest");
        Reporter.log("Billing - Verify EIP Bill Summary Details");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
        Reporter.log("6. Click on Equipment | Charge Equipment page should be loaded");
        Reporter.log("7. Click on total amount for the equipment | The total amount should be seen");
        Reporter.log("================================");
        ChargeEquipmentPage chargeEquipmentPage =  navigateToChargeEquipmentPage(myTmoData,"Equipment");
        chargeEquipmentPage.verifyTotalAmount();
    }
}
