package com.tmobile.eservices.qa.monitors;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.data.ApiTestData;

public class PaymentsAPIMonitors extends PaymentsAPIMonitorsHelper {
	
	@BeforeMethod(alwaysRun = true)
	public void clearTokenMapCommon(){
		tokenMapCommon = null;
	}
	@Test(dataProvider = "byColumnName", enabled = true,groups = { "PaymentsMonitors","OTPFlow"})
	public void PaymentsOTPFlow(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("correlationid", getValForCorrelation());
			//tokenMap.put("version", "v1");
			Reporter.log("************************************************************************************************************************");
			Reporter.log("<b>Payments OTP Sedona Flow</b> ");

			// 1. Invoke getSubScriberLines 
			
			invokeGetSubScriberLines(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);
			
			//2. Invoke getSubScriberAccount
			
			invokeGetSubScriberAccount(apiTestData,tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke getPADetails
			
			invokeGetPADetails(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke geSchedulePayment 
			
			invokeGetSchedulePayment(apiTestData, tokenMap);			

			checkEmptyValuesInTokenMap(tokenMap);
			
			// 4. Invoke searchPayment 
			
			invokeSearchPayment(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);
			
			// 5. Invoke OTPPayment 
			tokenMap.put("quoteId", "");
			
			tokenMap.put("chargeAmount", String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));
			
			invokeOTPPayment(apiTestData,tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);


		}catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} 
		catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true,groups = { "PaymentsMonitors","OTPSedonaFlow"})
	public void PaymentsOTPSedonaFlow(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("correlationid", getValForCorrelation());
			//tokenMap.put("version", "v1");
			Reporter.log("************************************************************************************************************************");
			Reporter.log("<b>Payments OTP Sedona Flow</b> ");

			// 1. Invoke getSubScriberLines 
			
			invokeGetSubScriberLines(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);
			
			//2. Invoke getSubScriberAccount
			
			invokeGetSubScriberAccount(apiTestData,tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke getPADetails
			
			invokeGetPADetails(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke geSchedulePayment 
			
			invokeGetSchedulePayment(apiTestData, tokenMap);			

			checkEmptyValuesInTokenMap(tokenMap);
			
			// 4. Invoke searchPayment 
			
			invokeSearchPayment(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);
			
			// 5. Invoke GenerateQuote 
			
			invokeGenerateQuote(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);
			
			// 6. Invoke OTPPayment 
			
			tokenMap.put("chargeAmount", String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));
			
			invokeOTPPayment(apiTestData,tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);


		}catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} 
		catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true,groups = { "PaymentsMonitors","FDPFlow"})
	public void PaymentsFDPFlow(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("correlationid", getValForCorrelation());
			//tokenMap.put("version", "v1");
			Reporter.log("************************************************************************************************************************");
			Reporter.log("<b>Payments FDP Sedona Flow</b> ");

			// 1. Invoke getSubScriberLines 
			
			invokeGetSubScriberLines(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);
			
			//2. Invoke getSubScriberAccount
			
			invokeGetSubScriberAccount(apiTestData,tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke getPADetails
			
			invokeGetPADetails(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke geSchedulePayment 
			
			invokeGetSchedulePayment(apiTestData, tokenMap);			

			checkEmptyValuesInTokenMap(tokenMap);
			
			// 4. Invoke searchPayment 
			
			invokeSearchPayment(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);

			// 5. Invoke FDPPayment 
			
			invokeFDPPayment(apiTestData,tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);


		}catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} 
		catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}
	@Test(dataProvider = "byColumnName", enabled = true,groups = { "PaymentsMonitors","AccountHistoryFlow"})
	public void PaymentsAccountHistoryFlow(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("correlationid", getValForCorrelation());
			//tokenMap.put("version", "v1");
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Payments AccountHistory Flow</b> ");

			// 1. Invoke getSubScriberLines 
			
			invokeGetSubScriberLines(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);
			
			//2. Invoke getSubScriberAccount
			
			invokeGetSubScriberAccount(apiTestData,tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke AccountActivity 
			
			invokeAccountActivity(apiTestData,tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);


		}catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} 
		catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true,groups = { "PaymentsMonitors","BillFlow"})
	public void PaymentsBillFlow(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("correlationid", getValForCorrelation());
			//tokenMap.put("version", "v1");
			Reporter.log("************************************************************************************************************************");
			Reporter.log("<b>Payments Bill Flow</b> ");

			// 1. Invoke getSubScriberLines 
			
			invokeGetSubScriberLines(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);
			
			//2. Invoke getSubScriberAccount
			
			invokeGetSubScriberAccount(apiTestData,tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);
			
			invokeGetPADetails(apiTestData, tokenMap);
			
			// 3. Invoke getDataSet 
			invokeGetDataSet(apiTestData,tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);


		}catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} 
		catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}
	
	@Test(dataProvider = "byColumnName", enabled = true,groups = { "PaymentsMonitors","UsageFlow"})
	public void PaymentsUsageSummaryFlow(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("correlationid", getValForCorrelation());
			//tokenMap.put("version", "v1");
			Reporter.log("************************************************************************************************************************");
			Reporter.log("<b>Payments UsageSummary Flow</b> ");

			// 1. Invoke getSubScriberLines 
			
			invokeGetSubScriberLines(apiTestData, tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);
			
			//2. Invoke getSubScriberAccount
			
			invokeGetSubScriberAccount(apiTestData,tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);

			// 3. Invoke getUsageSummary
			
			invokeUsageSummary(apiTestData,tokenMap);
			
			checkEmptyValuesInTokenMap(tokenMap);


		}catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} 
		catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}
}
