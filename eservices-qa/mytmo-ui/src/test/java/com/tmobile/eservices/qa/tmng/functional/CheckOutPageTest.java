package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.tmng.functional.CartPage;
import com.tmobile.eservices.qa.pages.tmng.functional.CheckOutPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPNoOfferPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPOfferPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPPreScreenForm;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPPreScreenIntro;
import com.tmobile.eservices.qa.pages.tmng.functional.TradeInValuePopUpModal;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class CheckOutPageTest extends TmngCommonLib {

	/**
	 * US332652 [Continued] TEST ONLY: TMO Additional Terms - Checkout: Trade-in
	 * Device Value Display for Promo Trade-in US332459 [Continued] TEST ONLY: TMO
	 * Additional Terms - Checkout: Pricing Display with Promo Applied for Promo
	 * Trade-in
	 *
	 * C363969: TRADE IN:verify trade in TQT tool and add standard trade in to cart
	 * verify trade in info on PB&RO and place order with tablet and voice lines
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyMonthlyTradeinValueinCheckoutReviewandSubmitPage(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US332652 -TEST ONLY: TMO Additional Terms - Checkout: Trade-in Device Value Display for Promo Trade-in");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device |PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Click on 'Trade Device' button | Trade-in modal  should be displayed");
		Reporter.log(
				"6. Fill all Trade-in info , select device condition and click on Get estimate | Trade-in options modal should be displayed");
		Reporter.log("7. select promotional trade-in option and click on Agree button | Cart page should be displayed");
		Reporter.log("8. click on continue button | check out page should be displayed");
		Reporter.log(
				"9. fill all personal and shipping information and click on next| payment and credit page should be displayed");
		Reporter.log(
				"10. fill all payment and credit information , set security pin and click on 'Agree & Next' | Review & submit page  should be displayed");
		Reporter.log(
				"11. verify trade-in device's monthly trade-in value in the tile|trade-in device's monthly trade-in value in the tile  should be displayed");
		Reporter.log(
				"12. verify trade-in device's promo length is same length as the promo device's EIP loan term length|trade-in device's promo length should be same length as the promo device's EIP loan term length");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		String loanTermLengthAtDeviceLevel = phonesPdpPage.getEipLoanTermLength();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnTradeDeviceCTA();
		cartPage.verifyTradeInModalDisplayed();

		TradeInValuePopUpModal tradeInValuePopUpModal = new TradeInValuePopUpModal(getDriver());
		tradeInValuePopUpModal.selectCarrier(tMNGData.getCarrierName().substring(0, 2));
		tradeInValuePopUpModal.selectMakeOptions(tMNGData.getMakeName().substring(0, 2));
		tradeInValuePopUpModal.selectModalOptions(tMNGData.getModalName().substring(0, 2));
		tradeInValuePopUpModal.setIMEINumber(tMNGData.getImeiName());
		tradeInValuePopUpModal.clickOnGoodRadioBtn();
		tradeInValuePopUpModal.clickOnGetEstimatedBtn();

		cartPage.verifyselectYourOptionsModalDisplayed();
		cartPage.verifyEstimatedTradeinValueOnSelectYourOptionsModal();
		String estAmount = cartPage.verifyEstimatedTradeinValueAmountOnSelectYourOptionsModal();
		cartPage.clickOnAgreeCTABtn();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyTradeinValueOnCartPage(estAmount);
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyStandardTradeInDetailDisplayed();
		cartPage.clickOnCloseIconAtOrderDetailPage();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();

		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);

		checkOutPage.verifyTradeInDeviceValueTile();
		checkOutPage.verifyEipLoanTermLength(loanTermLengthAtDeviceLevel);
		checkOutPage.verifyEipMonthlyPayment();
	}

	/**
	 * US331739:[Continued] TMO Additional Terms - Checkout - Display EIP Legal Text
	 * at Cart Level
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testEIPLegalTextNotDisplayedForFRPDevicesInReviewAndSubmitPage(TMNGData tMNGData) {
		Reporter.log("Test Case :US331739:TMO Additional Terms - Checkout - Display EIP Legal Text at Cart Level");
		Reporter.log("Condition | Select FRP device only");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select FRP device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log(
				"6. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log(
				"7. fill all payment and credit information , set security pin and click on 'Agree & Next' | Review & submit page  should be displayed");
		Reporter.log(
				"8. Verify Legal text below Total Monthly price in Review & Submit page | Legal text below Total Monthly price should not be displayed in Review & Submit page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickPayInFullPaymentOption();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();

		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);
		checkOutPage.verifyLegalTextNotDisplayedReviewandSubmitPage();

	}

	/**
	 * TA1742379 - TA1839684 : Prod: Cart Page- Accessory edit does not work when
	 * user navigates back to the cart from Review Order Page Edit Cart C494625: UNO
	 * Prospect End to End Accessory Purchase with products added from Mini PLP/PDP
	 * C518274: Accessory Only ( $69+) : Validate user is able to Edit the color
	 * swatch for accessory and complete transaction
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyEditAcessoryWithDeviceInCheckOutPage(TMNGData tMNGData) {
		Reporter.log(
				"Test Case: TA1742379: Prod: Cart Page- Accessory edit does not work when user navigates back to the cart from Review Order Page Edit Cart");
		Reporter.log(
				"Test Case: C494625:	UNO Prospect End to End Accessory Purchase with products added from Mini PLP/PDP");
		Reporter.log(
				"Test Case: C518274:	Accessory Only ( $69+) : Validate user is able to Edit the color swatch for accessory and complete transaction");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link |Cell phones page should be displayed");
		Reporter.log("3. Select any device |PDP page should be displayed");
		Reporter.log("4. Select finance from payment options| Finance payment option should be selected");
		Reporter.log("5. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("6. Click on Add An accessory| Accessory Mini PLP should be displayed");
		Reporter.log("7. Select any accessory | Accessory Mini PDP should be displayed");
		Reporter.log("8. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("9. Click on Continue CTA | Checkout page should be displayed");
		Reporter.log(
				"10. fill all personal and shipping information and click on next| payment and credit page should be displayed");
		Reporter.log(
				"11. fill all payment and credit information , set security pin and click on 'Agree & Next' | Review & submit page  should be displayed");
		Reporter.log("12. Click edit in cart CTA | Cart page  should be displayed");
		Reporter.log("13. Edit the accessory | Accessory mini PDP Page should be displayed");
		Reporter.log(
				"14. Select different color varian and click Add to cart | Cart page should be displayed and color should be updated");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);

		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);
		checkOutPage.verifyEditInCartCTA();
		checkOutPage.clickEditInCartCTA();

		cartPage.verifyCartPageLoaded();

		cartPage.clickOnCartContinueBtn();
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyPersonalInfo();
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.fillPaymentInfo(tMNGData);
		checkOutPage.setSecurityPin(tMNGData);
		checkOutPage.clickAgreeandNextBtn();
		checkOutPage.verifyReviewandSubmitPage();
//		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
//		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());

	}

	/**
	 * C347326: TMO: Verify Error Messages For Invalid Input in CheckOutPage
	 *
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyErrorMessageForInvalidInputInCheckOutPage(TMNGData tMNGData) {
		Reporter.log("Test Case :  TMO: Verify Error Messages For Invalid Input in CheckOutPage");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link |Cell phones page should be displayed");
		Reporter.log("3. Select any device |PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Click on Continue CTA | Checkout page should be displayed");
		Reporter.log(
				"6. Verify manditory fileds error message in Personal information section | Error message should be displayed for personal information manditory fields");
		Reporter.log(
				"7. Verify manditory fileds error message in Shipping information section | Error message should be displayed for Shipping information manditory fields");
		Reporter.log(
				"8. Verify tool tip for fileds in Personal information section | Tool tip should be displayed for Personal information fields");

		Reporter.log(
				"9. Verify tool tip for fileds in Shipping information section | Tool tip should be displayed for Shipping information fields");

		Reporter.log(
				"10. Verify Lable text for fileds in Personal information section | Lable text should be displayed for Personal information fields");

		Reporter.log(
				"11. Verify Lable text for fileds in Shipping information section | Lable text should be displayed for Shipping information fields");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CheckOutPage checkOutPage = navigateToCheckoutWithPhone(tMNGData);
		checkOutPage.verifyLabelTextForFirstName();
		checkOutPage.verifyTooltipForFirstName();
		checkOutPage.verifyErrorMessageForFirstName();
		checkOutPage.verifyLabelTextForLastName();
		checkOutPage.verifyTooltipForLastName();
		checkOutPage.verifyErrorMessageForLastName();
		checkOutPage.verifyLabelTextForEmail();
		checkOutPage.verifyTooltipForEmail();
		checkOutPage.verifyErrorMessageForEmail();
		checkOutPage.verifyLabelTextForPhoneNumber();
		checkOutPage.verifyTooltipForPhoneNumber();
		checkOutPage.verifyErrorMessageForPhoneNumber();

		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();

		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		checkOutPage.clickCheckOutNowCTAOrderModal();
		checkOutPage.verifyCheckOutPageLoaded();

		checkOutPage.verifyLabelTextForShippingAddressLine1();
		checkOutPage.verifyTooltipForShippingAddressLine1();
		checkOutPage.verifyErrorMessageForShippingAddressLine1();
		checkOutPage.verifyLabelTextForCity();
		checkOutPage.verifyTooltipForCity();
		checkOutPage.verifyErrorMessageForCity();
		checkOutPage.verifyLabelTextForState();
		checkOutPage.verifyErrorMessageForState();
		checkOutPage.verifyLabelTextForZipcode();
		checkOutPage.verifyTooltipForZipcode();
		checkOutPage.verifyErrorMessageForZipcode();

	}

	/**
	 * TA1655245: Cart: Monthly payable amount mismatch for 2nd line
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testPlanAndAutoPayAmountOnCheckOutPage(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case :TA1655245: Cart: Monthly payable amount mismatch for 2nd line");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | Application Should be Launched");
		Reporter.log("2. Navigate to empty cart | Empty Cart page should be displayed");
		Reporter.log("3. Click on 'Add a Phone' icon | Cart page should be displayed");
		Reporter.log(
				"4. Click on '+' button in device image and Bring your own phone button | Sim Starter Kit should be displayed");
		Reporter.log("5. Click on Add to cart button | CartPage should be displayed");
		Reporter.log("6. Click on Add a phone button | Device image should be displayed");
		Reporter.log(
				"7. Click on '+' button in device image and click Bring your own phone button | Sim Starter Kit should be displayed");
		Reporter.log("9. Click on Add to cart button | CartPage should be displayed");
		Reporter.log("10. Click on Add a phone button | Device image should be displayed");
		Reporter.log(
				"11. Click on '+' button in device image and click Bring your own phone button | Sim Starter Kit should be displayed");
		Reporter.log("12. Click on Add to cart button | CartPage should be displayed");
		Reporter.log("13. Click on Add a phone button | Device image should be displayed");
		Reporter.log(
				"14. Click on '+' button in device image and click Bring your own phone button | Sim Starter Kit should be displayed");
		Reporter.log("15. Click on Add to cart button | CartPage should be displayed");
		Reporter.log("16. Verify cost of plan should be 70/50/20/20 | Plan price should match");
		Reporter.log("17. Verify autopay discount $5 added to each line | AutoPay discount should be added by default");
		Reporter.log("18. Click on View Order Details link | Order Details modal should be displayed");
		Reporter.log("19. Verify cost of plan should be 70/50/20/20 | Plan price should match");
		Reporter.log("20. Verify autopay discount $5 added to each line | AutoPay discount should be added by default");
		Reporter.log("21. Click on close icon | CartPage should be displayed");
		Reporter.log("22. Click Comtinue CTA | Checkout page should be displayed");
		Reporter.log("23. Click on View Order Details link | Order Details modal should be displayed");
		Reporter.log("24. Verify cost of plan should be 70/50/20/20 | Plan price should match");
		Reporter.log("25. Verify autopay discount $5 added to each line | AutoPay discount should be added by default");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.clickOnBYODLinkOnCartEssentials();

		cartPage.verifyCartPageLoaded();
		/*
		 * for (int i = 1; i <= 3; i++) { cartPage.clickOnBYODLinkOnCartEssentials();
		 * miniPDPPage.verifySimStaterKitTitle();
		 * miniPDPPage.clickAddToCartCTAAtMiniPDPPage();
		 * cartPage.verifyCartPageLoaded(); }
		 */
		cartPage.verifyMonthlyPriceIsCorrectForBYOD();
		cartPage.verifyAutoPayDiscountIsAppliedForEachLine();
		cartPage.verifyTotalAutoPayDiscountIsCorrect();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyMonthlyPriceIsCorrectinOrderDetailsCartPage();
		cartPage.verifyTotalAutoPayDiscountIsCorrectInOrderDetailCartPage();
		cartPage.clickOnCloseIconAtOrderDetailPage();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyMonthlyPriceIsCorrectinOrderDetailsCheckOutPage();
		checkOutPage.verifyTotalAutoPayDiscountIsCorrectInOrderDetailCheckOutPage();
	}

	/**
	 * C347331: Validate Hardstop error: Multiple incorrect attempts at credit card
	 * information (number) C347327: Shipping options: Options, Address (911, Ship &
	 * Billing)
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.TPP })
	public void testVerifyHardStopmsgInCheckoutPage(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case : C347331: Validate Hardstop error: Multiple incorrect attempts at credit card information (number)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO cart| TMO empty cart Should be Launched");
		Reporter.log("2. click on Add a Phone |Cart page should be displayed");
		Reporter.log("3. Click on Plus button and then Add a new Phone CTA |Mini PLP page should be displayed");
		Reporter.log("4. Select any device |Mini PDP page should be displayed");
		Reporter.log("5. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("6. click on continue button | check out page should be displayed");
		Reporter.log(
				"7. fill all personal and shipping information and click on next| payment and credit page should be displayed");
		Reporter.log("8. fill wrong credit information multiple times | HardStop modal should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();

		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.verifyShippingAddressOptions();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.verifySameForBillingAddress();
		checkOutPage.verifyE911Address();
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();

		checkOutPage.verifyCheckOutPageLoaded();

		checkOutPage.fillPaymentInfo(tMNGData);
		checkOutPage.fillIdentityInfo(tMNGData);
		checkOutPage.clickAgreeandNextBtn();

		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		/*
		 * checkOutPage.verifySoftStopModalAndClickgotit();
		 * 
		 * checkOutPage.fillPaymentInfo(tMNGData);
		 * checkOutPage.fillIdentityInfo(tMNGData); checkOutPage.clickAgreeandNextBtn();
		 * 
		 * checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		 * checkOutPage.verifySoftStopModalAndClickgotit();
		 * 
		 * checkOutPage.fillPaymentInfo(tMNGData);
		 * checkOutPage.fillIdentityInfo(tMNGData); checkOutPage.clickAgreeandNextBtn();
		 * 
		 * checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		 * checkOutPage.verifyHardStopPage();
		 */

	}

	/**
	 * US508940 DEFECT: Checkout - Credit check - State field value is not resetting
	 * when ID type is changed from state required value to non state required value
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyStateFieldValueWhenIDTypeChanged(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US508940	DEFECT: Checkout - Credit check - State field value is not resetting when ID type is changed from state required value to non state required value");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Phone link on Cart | Device Plus img should  be displayed");
		Reporter.log("3. Click on Plus button and click add a new phone | Credit options modal should  be displayed");
		Reporter.log("4. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("5. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("6. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("7. click on continue button | check out page should be displayed");
		Reporter.log(
				"8. fill all personal and shipping information and click on next| payment and credit page should be displayed");
		Reporter.log(
				"9. Select ID type  as Driving license |  Driving license should be displayed on ID type and State field should be enabled");
		Reporter.log("10. Select any State value  |  State value should be selected");

		Reporter.log(
				"11. Change the ID type to Passport and verify state field| ID type should be changed to Passport and State filed should be disabled.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();

		checkOutPage.verifyCheckOutPageLoaded();

		checkOutPage.selectIdType(tMNGData.getIdType());
		checkOutPage.selectStateForId(tMNGData.getState());
		checkOutPage.selectIdType(tMNGData.getCategoryName());
		checkOutPage.verifyStateDropdownDisabled();

	}

	/**
	 * C478902 Mobile -MixedCart Order: Magenta Plan - Awe/Avg - Pricing (Breakout &
	 * Totals) Review, Submit and Confirmation C494627 UNO Prospect End to End Smart
	 * Watch Purchase with products added from Mini PLP/PDP (Magenta Plan)
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyEndtoEndPurchageWithMixedProductsAndMagentaPlanFromEmptyCart(TMNGData tMNGData) {
		Reporter.log(
				"C478902	Mobile -MixedCart Order: Magenta Plan - Awe/Avg - Pricing (Breakout & Totals) Review, Submit and Confirmation");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click Add a Phone CTA on cart | Credit modal should be displayed.");
		Reporter.log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("4. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("6. Select Magenta Plan | Magenta Plan should be selected");
		Reporter.log("7. Click Add a Tablet CTA on cart | Tablet mini PLP should be displayed.");
		Reporter.log("8. Select any tablet in mini PLP | Tablet Mini PDP Page should  be displayed");
		Reporter.log("9. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("10. Click Add a Wearable CTA on cart | Wearable mini PLP should be displayed.");
		Reporter.log("11. Select any Wearable in mini PLP | Wearable Mini PDP Page should  be displayed");
		Reporter.log("12. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("13. Click Add a Accessory CTA on cart | Accessory mini PLP should be displayed.");
		Reporter.log("14. Select any Accessory in mini PLP | Accessory Mini PDP Page should  be displayed");
		Reporter.log("15. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("16. Click BYOD CTA on cart | BYOD mini PDP should be displayed.");
		Reporter.log("17. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("18. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log(
				"19. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log(
				"20. fill all payment and credit information , set security pin and click on 'Agree & Next' | Review & submit page  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnMagentaPlan();
		cartPage.verifyMagentaPlanSelected();
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.addAWearabletoCartFromCartPage(tMNGData);
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());
	}

	/**
	 * C518279 UNO Prospect End to End Device Purchase with products added from Mini
	 * PLP/PDP (Magenta Plus Plan) C494624 UNO Prospect End to End device purchase
	 * starting on Empty CART page
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyEndtoEndPurchageWithProductsAndMagentaPlusPlanFromEmptyCart(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :C518279	UNO Prospect End to End Device Purchase with products added from Mini PLP/PDP (Magenta Plus Plan)");
		Reporter.log("Test Case :C494624 UNO Prospect End to End device purchase starting on Empty CART page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click Add a Phone CTA on cart | Credit modal should be displayed.");
		Reporter.log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("4. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("6. Select Magenta Plus Plan | Magenta Plan Plus should be selected");
		Reporter.log("7. Click Add a Tablet CTA on cart | Tablet mini PLP should be displayed.");
		Reporter.log("8. Select any tablet in mini PLP | Tablet Mini PDP Page should  be displayed");
		Reporter.log("9. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("10. Select on MegentaPlus plan | MegentaPlus plan should  be active");
		Reporter.log("11. Click on Add A Tablet Link On Cart Essentials | Mini PLP page should be displayed");
		Reporter.log("12. Select any tablet device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("13. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("14. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log(
				"15. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log(
				"16. fill all payment and credit information , set security pin and click on 'Agree & Next' | Review & submit page  should be displayed");
		Reporter.log("17. Select on Agree with Terms and Conditions checkbox | Agree and Submit button should enabled");
		Reporter.log("18. Click on Agree and Submit button | Conformation header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnMagentaPlusPlan();
		cartPage.verifyMagentaPlusPlanSelected();
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());
	}

	/**
	 * C518278 UNO Prospect End to End Device Purchase with products added from Mini
	 * PLP/PDP (Essential Plan)
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyEndtoEndPurchageWithProductsAndEssentialPlanFromEmptyCart(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : C518278	UNO Prospect End to End Device Purchase with products added from Mini PLP/PDP (Essential Plan)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click Add a Phone CTA on cart | Credit modal should be displayed.");
		Reporter.log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("4. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");

		Reporter.log("6. Select T Mobile Essential Plan | Essential Plan Plus should be selected");
		Reporter.log("7. Click Add a Tablet CTA on cart | Tablet mini PLP should be displayed.");
		Reporter.log("8. Select any tablet in mini PLP | Tablet Mini PDP Page should  be displayed");
		Reporter.log("9. Click Add to Cart in mini PDP | Cart Page should  be displayed");

		Reporter.log("10. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log(
				"11. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log(
				"12. fill all payment and credit information , set security pin and click on 'Agree & Next' | Review & submit page header should be displayed");

		Reporter.log("13. Select on Agree with Terms and Conditions checkbox | Agree and Submit button should enabled");
		Reporter.log("14. Click on Agree and Submit button | Conformation header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.clickOnTMobileEssentialsPlan();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());

	}

	/**
	 * US583204 DEFERRED TMNG/QATPRD: $40 discount displayed in View Order Details
	 * Modal for Tablet with Plan Essentials
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TPP })
	public void testVerifyDiscountedPriceOfTabletInViewOrderdetail(TMNGData tMNGData) {
		Reporter.log(
				"US583204	DEFERRED TMNG/QATPRD: $40 discount displayed in View Order Details Modal for Tablet with Plan Essentials");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click Add a Tablet CTA on cart | Credit modal should be displayed.");
		Reporter.log("3. Select Awesome credit options and click Done | Tablet Mini PLP should  be displayed");
		Reporter.log("4. Select any tablet in mini PLP | Tablet Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("6. Click on Add a Phone Link |Mini PLP page should be displayed");
		Reporter.log("7. Select any device | Mini PDP page should be displayed");
		Reporter.log("8. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("9. Select Essential Plan  | Essential plan should be selected");
		Reporter.log(
				"10. Verify discounted price in Monthly tile | Discounted price ($45 for Tablet) should be displayed on monthly tile");
		Reporter.log("11. Click View Order link| View order detail pop up should  be displayed");
		Reporter.log("12. Verify discounted price in modal | Discounted price ($45 for Tablet) should be displayed");
		Reporter.log("13. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log(
				"14. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log(
				"15. fill all payment and credit information , set security pin and click on 'Agree & Next' | Review & submit page  should be displayed");
		Reporter.log("16. Verify discounted price  | Discounted price ($45 for Tablet) should be displayed");
		Reporter.log("17. Click View Order link| View order detail pop up should  be displayed");
		Reporter.log("18. Verify discounted price in modal | Discounted price ($45 for Tablet) should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingTabletFromMainPLP(tMNGData);

		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.verifyDiscountInMonthlyPaymentDisplayed();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyTabletLineDiscountOnOrderDetailsModal();
		cartPage.clickOnCloseIconAtOrderDetailPage();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();

		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);
		checkOutPage.verifyTabletLineDiscountOnReviewPage();
	}

	/**
	 * US589247 TPP - Skip tab 2 for Accessories only scenario
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TPP })
	public void testVerifySkipTabWithAccessoryOnlyinCart(TMNGData tMNGData) {
		Reporter.log("Test Case : US589247	TPP - Skip tab 2 for Accessories only scenario");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click Add a Accessory CTA on cart | Mini PLP should  be displayed");
		Reporter.log("4. Select any Accessory in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		// Reporter.log("6. Click Continue button in Cart page | Checkout page should be
		// displayed");
		// Reporter.log(
		// "7. Verify checkout page tabs| skip from personal info tab to shipping and
		// payments tab in checkout");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage emptyCartPage = navigateToEmptyCart(tMNGData);
		emptyCartPage.clickOnAddAnAccessoryLinkOnCart();
		PlpPage accessoryPlpPage = new PlpPage(getDriver());
		accessoryPlpPage.verifyAccessoriesPlpPageLoaded();
		accessoryPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage accessoryPdpPage = new PdpPage(getDriver());
		accessoryPdpPage.verifyAccessoriesPdpPageLoaded();
		accessoryPdpPage.clickOnAddToCartBtn();
		emptyCartPage.verifyCartPageLoaded();
		// emptyCartPage.clickOnCartContinueBtn();
		// CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		// checkOutPage.verifyCheckOutPageLoaded();

	}

	/**
	 * US587086: HCC: show updated Existing Customer Modal - CHECKOUT page (when
	 * existing customer is found)
	 *
	 * @param data
	 * @param tMNGData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TPP })
	public void testVerifyExistingCustomerHccLogInRedirectsToMyTmo(TMNGData tMNGData, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :US587086: HCC: show updated Existing Customer Modal - CHECKOUT page (when existing customer is found");
		Reporter.log("Precondition: User should be existing customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
		Reporter.log("3. Select Device | PDP page should be displayed");
		Reporter.log("4. Click on Add to cart button | New to T-Mobile and Existing customer CTA should pop up");
		Reporter.log("5. Verify CTA for new to T-Mobile and existing customer | Text and header should be authorable");
		Reporter.log("6. Click on continue as guest|  Cart Page should  be displayed");
		Reporter.log("7. Click on Continue button on sticky banner | Checkout page should be displayed ");
		Reporter.log("8. Fill all personal information and click on next | Credit check form should be displayed");
		Reporter.log(
				"9. Provide all information and click on Run Credit check button | Hard Credit Check processing bar should open");
		Reporter.log(
				"10. Verify existing customer notification bar| Existing customer notification should be displayed");
		Reporter.log("11. Verify Log In CTA | Log In should be displayed");
		Reporter.log("12. Click on Log in CTA | Customer should be directed to my.t-mobile.com/shop ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonesPDP(tMNGData);
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.clickOnAddToCartBtn();
		phoneDetailsPage.verifyContinueAsGuestCTA();
		phoneDetailsPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyPersonalInfo();
		checkOutPage.fillPersonalInfoWithoutMiddleName(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.selectStateForIdIfEnabled(tMNGData.getState());
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyExistingCustomerHeader();
		checkOutPage.verifyLoginCTAOnExistingCustomer();
		checkOutPage.clickLoginCTAOnExistingCustomer();
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.performLoginAction(tMNGData.getLoginEmailOrPhone(), tMNGData.getPassword());
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyShoppage();
	}

	/**
	 * US587086: HCC: show updated Existing Customer Modal - CHECKOUT page (when
	 * existing customer is found)
	 *
	 * @param data
	 * @param tMNGData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TPP })
	public void testVerifyExistingCustomerHccCloseTabRedirectsToCart(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US587086: HCC: show updated Existing Customer Modal - CHECKOUT page (when existing customer is found");
		Reporter.log("Precondition: User should be existing customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
		Reporter.log("3. Select Device | PDP page should be displayed");
		Reporter.log("4. Click on Add to cart button | New to T-Mobile and Existing customer CTA should pop up");
		Reporter.log("5. Verify CTA for new to T-Mobile and existing customer | Text and header should be authorable");
		Reporter.log("6. Click on continue as guest|  Cart Page should  be displayed");
		Reporter.log("7. Click on Continue button on sticky banner | Checkout page should be displayed ");
		Reporter.log("8. Fill all personal information and click on next | Credit check form should be displayed");
		Reporter.log(
				"9. Provide all information and click on Run Credit check button | Hard Credit Check processing bar should open");
		Reporter.log(
				"10. Verify existing customer notification bar| Existing customer notification should be displayed");
		Reporter.log("11. Verify Log In CTA | Log In should be displayed");
		Reporter.log(
				"12. Click on close existing customer notification tab| Tab should be closed and customer should be directed to my.t-mobile.com/cart");
		Reporter.log("13. Verify customer is directed to back to Cart page| Find your price tab should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonesPDP(tMNGData);
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.clickOnAddToCartBtn();
		phoneDetailsPage.verifyContinueAsGuestCTA();
		phoneDetailsPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyPersonalInfo();
		checkOutPage.fillPersonalInfoWithoutMiddleName(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.selectStateForIdIfEnabled(tMNGData.getState());
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyExistingCustomerHeader();
		checkOutPage.verifyLoginCTAOnExistingCustomer();
		checkOutPage.verifyCloseCTAOnExistingCustomer();
		checkOutPage.clickCloseCTAOnExistingCustomer();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyFindYourPricingCTA();

	}

	/**
	 * US588722 - TEST ONLY HCC: show updated pricing Modal - CHECKOUT page (when
	 * new credit class returned) C545700 - HCC at the check-out page - h/i/y
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyUpdatedPriceAfterHCC(TMNGData tMNGData) {
		Reporter.log(
				"US588722 - TEST ONLY HCC: show updated pricing Modal - CHECKOUT page (when new credit class returned)");
		Reporter.log("C545700 - HCC at the check-out page - h/i/y");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Add Wearable,Tablet and Accessory to cart |All type of products should be added into cart");
		Reporter.log(
				"6. Verify Price type of each line on Cart | Each device tile should have CRP8/awesome credit pricing");
		Reporter.log(
				"7. Verify credit type on sticky banner | Bottom pricing sticky should have crp8/awesome credit pricing");
		Reporter.log(
				"8. Verify , Find your pricing CTA is displayed below the estimated total and estimated monthly in cart page | Find your pricing CTA should be displayed on cart below to estimated today and monthly section");
		Reporter.log("9. Click on Continue button on sticky banner | Checkout page should be displayed");
		Reporter.log(
				"10. Verify that CRP8 /awesome price is displayed in the top sticky | CRP8 /awesome price should be displayed in the top sticky");
		Reporter.log(
				"11. Fill all personal information on 'Personal Info' tab and click on next | customer should navigated to the Credit Check Page");
		Reporter.log(
				"12. Fill all required fileds on credit check and click Run Credit Check CTA | Loading screen should be displayed till HCC run.");
		Reporter.log(
				"13. Verify updated price after HCC check | Accurate pricing should be displayed for all the products");
		Reporter.log("14. Verify the update price modal is displayed | update price modal should be displayed");
		Reporter.log(
				"15. Verify authorable header on price modal | 'Hi , <user name >based on your credit here's what you'll pay' should be displayed");
		Reporter.log("16. Verify Order total / Monthly pricing | Order total / Monthly pricing should be displayed");
		Reporter.log(
				"17. Verify eip term under monthly | under monthly, 'For 24 months' should be dynamic based on lowest financing month term");
		Reporter.log("18. Verify Credit class | Credit class should be dynamic A/B, C/D or H/I/Y (based on HCC)");
		Reporter.log(
				"19. Verify this pricing will be final sale price/ no calculations needed | Sku - down payment and monthly, Plan Name - down payment and monthly (will be null down payment) Service - and Data Add Ons and Line Deposit - down payment and monthly (null monthly) should be displayed on price section");
		Reporter.log(
				"20. Verify line item has no due today and Monthly | If any line item has no due today and Monthly , then it should be suppressed");
		Reporter.log(
				"21. Verify dynamic custom text based on credit class 'You'll now see your verified pricing...' | 'You'll now see your verified pricing...'should be displayed");
		Reporter.log(
				"22. Verify CTA's on price modal | Chat With US, Edit your cart and Finish checkout CTA's should be displayed");
		Reporter.log(
				"23. Click Finish Checkout CTA on price modal | Modal should be closed and next tab of checkout should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);

		cartPage.addAWearabletoCartFromCartPage(tMNGData);

		cartPage.addATablettoCartFromCartPage(tMNGData);

		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);

		cartPage.verifyDeviceUnderMonthlyPaymentDisplayedMultiline();
		cartPage.verifyDeviceUnderOneTimePaymentDisplayedMultiline();
		cartPage.verifyStickyBannerMonthlyTotalPriceDisplayed();
		cartPage.verifyFindYourPricingCTA();
		int count = cartPage.getLineCountOnCartPage();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyPricingAtCheckOutStickyBanner();
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheckHeader(tMNGData);
		checkOutPage.verifyOrderTotalPricingForOrderModal();
		checkOutPage.verifyOrderMonthlyPricingForOrderModal();
		checkOutPage.verifyEIPTermUnderMonthlyOrderModal();
		checkOutPage.verifyDynamicTextYouWillNowSeeVerifiedPricingOrderModal();
		checkOutPage.verifyAccessoriesLineSuppressedOnOrderModal(count);
		checkOutPage.clickCaretIconToExpandLine();
		checkOutPage.verifySkuPricingOnOrderModal();
		checkOutPage.verifySkuPlanPricingOnOrderModal();
		checkOutPage.verifyCheckOutNowCTAOrderModal();
		checkOutPage.verifyEditYourCartCTAOrderModal();
		checkOutPage.verifyChatWithUsCTAOrderModal();
		checkOutPage.clickCheckOutNowCTAOrderModal();
		checkOutPage.verifyShippingHeader();

	}

	/**
	 * US589250: TPP - Do Not Reset HCC/SCC flags after Order Submit (May need to
	 * test after production)
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TPP })
	public void testSubmitOrderThroughSCCflow(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US589250: TPP - Do Not Reset HCC/SCC flags after Order Submit (May need to test after production)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click on Find my price CTA | PreScreen Intro page should be displayed");
		Reporter.log("6. Click on Lets go CTA | PreScreen form should be displayed");
		Reporter.log("7. Fill the mandetery field in Prescreen form | Mandetery fields should be filled");
		Reporter.log("8. Click on Find my price CTA | Pre-screen loading model should be displayed");
		Reporter.log("9. Verify updated price | Updated price should be displayed should be displayed");
		Reporter.log("10. Click on Continue button | Check out page should be displayed");
		Reporter.log("11. fill all personal information and click on next | Verify IS tab should be displayed");
		Reporter.log("12. Fill reqired info and click on next | Shipping and Payments should be displayed");
		Reporter.log("13. Fill reqired info and click on Agree and Submit CTA | Review order page should be displayed");
		Reporter.log("14. Click on Submit button | Conformation header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		Double monthlyPrice1 = cartPage.getMonthlyPaymentDevicePrice();
		Double downPayment1 = cartPage.getOneTimePaymentDevicePrice();
		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();
		tppOfferPage.clickGotItCTA();
		cartPage.verifyCartPageLoaded();
		Double monthlyPrice2 = cartPage.getMonthlyPaymentDevicePrice();
		Double downPayment2 = cartPage.getOneTimePaymentDevicePrice();
		cartPage.compareTwoDoubleValuesNotEqual(monthlyPrice1, monthlyPrice2);
		cartPage.compareTwoDoubleValuesNotEqual(downPayment1, downPayment2);
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfoAfterpreScreen(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyVerifyIdentityPage();
		checkOutPage.fillSSN(tMNGData);
		checkOutPage.fillIdentityVerificationForVerifyID(tMNGData);

		checkOutPage.clickNextBtnInVerifyIDPage();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.fillPaymentInfo(tMNGData);
		checkOutPage.setSecurityPin(tMNGData);
		checkOutPage.clickAgreeandNextBtn();
		checkOutPage.verifyReviewandSubmitPage();
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeader();
	}

	/**
	 * US589250: TPP - Do Not Reset HCC/SCC flags after Order Submit (May need to
	 * test after production)
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TPP })
	public void testSubmitOrderThroughHCCflow(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US589250: TPP - Do Not Reset HCC/SCC flags after Order Submit (May need to test after production)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click on Continue button | Check out page should be displayed");
		Reporter.log("6. fill all personal information and click on next | Credit check tab should be displayed");
		Reporter.log("7. Fill reqired info and click on next | Hard credit check processing model should be displayed");
		Reporter.log("8. Verify Updated prices model | Updated price model should be displayed");
		Reporter.log("9. Verify Updated prices on model | Updated prices should be displayed on Updated price model");
		Reporter.log("10. Click on Finish checkout CTA | Shipping and payments tab should be displayed");
		Reporter.log("11. Fill reqired info and click on Agree and Submit CTA | Review order page should be displayed");
		Reporter.log("12. Click on Submit button | Conformation header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		Double monthlyPrice1 = Double.parseDouble(cartPage.getTotalMonthlyPriceStickyBanner());
		Double downPayment1 = Double.parseDouble(cartPage.getTotalTodayPriceInStickyBanner());
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		Double monthly = checkOutPage.getTotalMonthlyPricePricingModal();
		Double today = checkOutPage.getTotalDownPaymentPricePricingModal();
		checkOutPage.compareTwoDoubleValuesNotEqual(monthlyPrice1, monthly);
		checkOutPage.compareTwoDoubleValuesNotEqual(downPayment1, today);
		checkOutPage.clickCheckOutNowCTAOrderModal();
		checkOutPage.verifyAgreeandNextBtnDisabled();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.fillPaymentInfo(tMNGData);
		checkOutPage.setSecurityPin(tMNGData);
		checkOutPage.clickAgreeandNextBtn();
		checkOutPage.verifyReviewandSubmitPage();
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeader();

	}

	/**
	 * US587613: TPP - UI error handling in data validation (Verify ID Tab) US552567
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testValidateIdTabSectionFieldsAndDateOfBirthErrorMessagesForSCCflow(TMNGData tMNGData) {
		Reporter.log("Test Case :US587613: TPP - UI error handling in data validation (Verify ID Tab)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Click on Find my price CTA | PreScreen Intro page should be displayed");
		Reporter.log("6. Click on Lets go CTA | PreScreen form should be displayed");
		Reporter.log("7. Fill the mandetery field in Prescreen form | Mandetery fields should be filled");
		Reporter.log("8. Click on Find my price CTA | Pre-screen loading model should be displayed");
		Reporter.log("9. Verify updated price | Updated price should be displayed should be displayed");
		Reporter.log("10. Click on Continue button | Check out page should be displayed");
		Reporter.log("11. Fill personal info in personal info section | Personal info section should be updated");
		Reporter.log("12. Click on Next button | ID tab should be disabled");
		Reporter.log(
				"13. Leave Id tab info for required fields in ID tab section | ID tab info section error messages should be displayed");
		Reporter.log("14. Now Fill Id tab fields in IDtab info section | ID tab info section should be updated");
		Reporter.log(
				"15. Enter DOB field < 18 years | Agree&Next button should be disabled and DOB field error message should be displayed");
		Reporter.log("16. Now enter DOB field > 18 years | Agree&Next button should be enabled");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TPPPreScreenForm tppPreScreenForm = navigateToTPPPreScreenFormPageFromDeviceCart(tMNGData);
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
	//	tppPreScreenForm.verifyPreScreenloadingPage();
		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();
		tppOfferPage.clickGotItCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfoAfterpreScreen(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyVerifyIdentityPage();
		checkOutPage.verifyErrorMessageForSSN();
		checkOutPage.verifyErrorMessageForIDType();
		checkOutPage.verifyErrorMessageForIDNumber();
		checkOutPage.verifyErrorMessageForCreditCheckExpiryDate();

	}

	/**
	 * US590295:TEST ONLY TPP - Display Accessories as bottom "line item" for
	 * Updated Pricing Modal
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testAccessoriesToBeDisplayedAsBottomLineItemInUpdatedPricingModel(TMNGData tMNGData) {
		Reporter.log("US590295:TEST ONLY TPP - Display Accessories as bottom line item for Updated Pricing Modal");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Add a Tablet and 2 Accessories to cart |All type of products should be added into cart");
		Reporter.log("6. Click on Continue button on sticky banner | Checkout page should be displayed");
		Reporter.log(
				"7. Verify that CRP8 /awesome price is displayed in the top sticky | CRP8 /awesome price should be displayed in the top sticky");
		Reporter.log(
				"8. Fill all personal information on 'Personal Info' tab and click on next | customer should navigated to the Credit Check Page");
		Reporter.log(
				"9. Fill all required fileds on credit check and click Run Credit Check CTA | Loading screen should be displayed till HCC run.");
		Reporter.log("10. Verify the update price modal is displayed | update price modal should be displayed");
		Reporter.log(
				"11. Verify Accessories in update price modal | Accessories should be displayed as line items at last");
		Reporter.log(
				"12. Verify Accessories BreakDown | Accessories breakdown should be displayed as Monthly/Today price");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);

		cartPage.clickOnAddAnAccessoryLinkOnCart();
		PlpPage accessoriesPlpPage = new PlpPage(getDriver());
		accessoriesPlpPage.verifyAccessoriesPlpPageLoaded();
		accessoriesPlpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoriesPdpPage = new PdpPage(getDriver());
		accessoriesPdpPage.verifyAccessoriesPdpPageLoaded();
		accessoriesPdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);

		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyTotalTodayAmount();
		checkOutPage.verifyMonthlyAmount();

		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		checkOutPage.verifyAccessoriesOnUpdatePricingModal();
		checkOutPage.clickdowncaratButtonForAccessories();
		checkOutPage.verifyAccessoriesAreDisplayed();
		checkOutPage.verifyAccessoriesTodayPricingBreakdownDisplayed();

	}

	/**
	 * US590295:TEST ONLY TPP - Display Accessories as bottom "line item" for
	 * Updated Pricing Modal
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TPP })
	public void testAccessoriesToBeDisplayedAsLineItemInUpdatedPricingModel(TMNGData tMNGData) {
		Reporter.log("US590295:TEST ONLY TPP - Display Accessories as bottom line item for Updated Pricing Modal");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Accessories Tab | PLP page should be displayed");
		Reporter.log("3. Select Accessory | PDP page should be displayed");
		Reporter.log("5. Click on 'Verify your pricing' on PDP| 'Pre-screen intro' tab will be displayed");
		Reporter.log("6. Click on 'Get Started' button on 'Pre-screen Intro tab'|Form for pre-screen will displayed");
		Reporter.log(
				"7. Fill the form for Pre-screen and click 'Find my price' button | 'Pre-screen' tab will be display");
		Reporter.log(
				"8. Verify personalized pricing after a successful pre-screen on PDP page| Personalized pricing will be shown on pdp page");
		Reporter.log("9. Click on Add to cart button | New to T-Mobile and Existing customer CTA should pop up");
		Reporter.log("10. Click on continue as guest  | Cart Page should  be displayed");
		Reporter.log("11. Click on Continue button on sticky banner | Checkout page should be displayed");
		Reporter.log(
				"12. Fill all personal information on 'Personal Info' tab and click on next | update price modal should be displayed");
		Reporter.log("13. Verify the update price modal | update price modal should be displayed");
		Reporter.log("14. Verify Accessories in update price modal | Accessories should be displayed as line items");
		Reporter.log(
				"15. Verify Accessories BreakDown | Accessories breakdown should be displayed as Monthly/Today price");
		Reporter.log("================================");

		TPPPreScreenForm tppPreScreenForm = navigateToTPPPreScreenFormPageFromAccessoryPDP(tMNGData);
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppPreScreenForm.verifyPreScreenloadingPage();
		TPPNoOfferPage tppNoOfferPage = new TPPNoOfferPage(getDriver());
		tppNoOfferPage.verifyPrescreenNoOfferPage();
		tppNoOfferPage.clickContinueShoppingCta();
		PdpPage accessoryPdpPage = new PdpPage(getDriver());
		accessoryPdpPage.verifyAccessoriesPdpPageLoaded();
		accessoryPdpPage.clickOnAddToCartBtn();
		accessoryPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyTotalTodayAmount();
		checkOutPage.verifyMonthlyAmount();

		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		checkOutPage.verifyAccessoriesOnUpdatePricingModal();
		checkOutPage.clickdowncaratButtonForAccessories();
		checkOutPage.verifyAccessoriesAreDisplayed();

	}

	/**
	 * US589245:TEST ONLY - update pricing sticky on checkout based on HCC/SCC flag
	 * (estimated / non estimated pricing)
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TPP })
	public void testEstimatedOnCheckOutPageWhenSccIsPassed(TMNGData tMNGData) {
		Reporter.log(
				"US589245:TEST ONLY - update pricing sticky on checkout based on HCC/SCC flag (estimated / non estimated pricing)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
		Reporter.log("3. Select Device | PDP page should be displayed");
		Reporter.log("5. Click on 'Verify your pricing' on PDP| 'Pre-screen intro' tab will be displayed");
		Reporter.log("6. Click on 'Get Started' button on 'Pre-screen Intro tab'|Form for pre-screen will displayed");
		Reporter.log(
				"7. Fill the form for Pre-screen and click 'Find my price' button | 'Pre-screen' tab will be display");
		Reporter.log(
				"8. Verify personalized pricing after a successful pre-screen on PDP page| Personalized pricing will be shown on pdp page");
		Reporter.log("9. Click on Add to cart button | New to T-Mobile and Existing customer CTA should pop up");
		Reporter.log("10. Click on continue as guest  | Cart Page should  be displayed");
		Reporter.log("11. Click on Continue button on sticky banner | Checkout page should be displayed");
		Reporter.log("12. Verify Price details | 'Today/Monthly' label should be displayed below the prices");
		Reporter.log(
				"13. Fill all personal information on 'Personal Info' tab and click on next | 'Verify ID' tab should be displayed");
		Reporter.log("14. Verify Price details | 'Today/Monthly' label should be displayed below the prices");
		Reporter.log(
				"15. Fill all details to Verify Id and Click on Next CTA | Update Pricing model should be displayed");
		Reporter.log("16. Verify Price details | 'Today/Monthly' label should be displayed below the prices");
		Reporter.log("17. Click on Finish CheckOut CTA | Should be navigated to 'Shipping & Payments' tab");
		Reporter.log("18. Verify Price details | 'Today/Monthly' label should be displayed below the prices");
		Reporter.log(
				"19. Fill the Shipping and Payment details and click on Agree & Next CTA | Should be navigated to 'Review and Submit order' tab");
		Reporter.log("20. Verify Price details | 'Today/Monthly' label should be displayed below the prices");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TPPPreScreenForm tppPreScreenForm = navigateToTPPPreScreenFormPageFromDevicePDP(tMNGData);
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppPreScreenForm.verifyPreScreenloadingPage();
		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();
		tppOfferPage.clickGotItCTA();
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyTodayAndMonthlyLabelInAllCheckOutPages();
		checkOutPage.fillPersonalInfoAfterpreScreen(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyVerifyIdentityPage();
		checkOutPage.verifyTodayAndMonthlyLabelInAllCheckOutPages();
		checkOutPage.fillSSN(tMNGData);
		checkOutPage.fillIdentityVerificationForVerifyID(tMNGData);
		checkOutPage.clickNextBtnInVerifyIDPage();
		checkOutPage.verifyShippingHeader();
		checkOutPage.verifyTodayAndMonthlyLabelInAllCheckOutPages();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.fillPaymentInfo(tMNGData);
		checkOutPage.setSecurityPin(tMNGData);
		checkOutPage.clickAgreeandNextBtn();
		checkOutPage.verifyReviewandSubmitPage();
		checkOutPage.verifyTodayOrMonthlyLabelBelowPricesInReviewSubmitPage();

	}

	/**
	 * US590226: TPP - UI error handling in data validation (Shipping and Payaments
	 * Tab)
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testValidateShippingAndPaymentsTabsErrorMessages(TMNGData tMNGData) {
		Reporter.log("Test Case :US590226: TPP - UI error handling in data validation (Shipping and Payaments Tab)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click Add a Phone CTA on cart | Phones PLP page should be displayed");
		Reporter.log("3. Select any device in PLP page | PDP Page should  be displayed");
		Reporter.log("4. Click Add to Cart in PDP page | Cart Page should  be displayed");
		Reporter.log("5. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("6. Fill personal info in personal info section | Personal info section should be updated");
		Reporter.log("7. Click Next button | Credit check tab should be disabled");
		Reporter.log("8. Enter the require info and click on next | Shipping and Payments header should be displayed");
		Reporter.log(
				"9. Leave either or all the required fields in shipping section | Shipping section error messages should be displayed");
		Reporter.log("10. Verify Agree&Next button | Agree&Next button should be disabled");
		Reporter.log("11. Now Fill shipping info in shipping address section | Shipping details should be filled");
		Reporter.log("12. Verify Agree&Next button | Agree&Next button should be enabled");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage emptyCartPage = navigateToCartPagefromEmptyCartFromUNOPLP(tMNGData);
		emptyCartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		checkOutPage.clickCheckOutNowCTAOrderModal();
		checkOutPage.verifyAgreeandNextBtnDisabled();
		checkOutPage.verifyErrorMessageForShippingAddressLine1();
		checkOutPage.verifyErrorMessageForCity();
		checkOutPage.verifyErrorMessageForState();
		checkOutPage.verifyErrorMessageForZipcode();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.verifyAgreeandNextBtnDisabled();
		checkOutPage.verifyErrorMessageForCreditCardNumber();
		checkOutPage.verifyErrorMessageForExpiryDateCC();
		checkOutPage.verifyErrorMessageForCVV();
		checkOutPage.verifyAgreeandNextBtnDisabled();
	}

	/**
	 * US587191: TPP -Check out tab #2 - Skip Verify ID/Credit Check for customers
	 * who have submitted this information
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testCreditCheckInformationIsNotPrePopulatedWhenUserEditCartInCheckOutPage(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US587191: TPP -Check out tab #2 - Skip Verify ID/Credit Check for customers who have submitted this information");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click Add a Phone CTA on cart | Phones Mini PLP should  be displayed");
		Reporter.log("3. Select any device in PLP | PDP Page should  be displayed");
		Reporter.log("4. Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log(
				"6. Fill all personal information on 'Personal Info' tab and click on next | customer should navigated to the Credit Check Page");
		Reporter.log(
				"7. Fill all required fileds on credit check and click 0n Agree&Next CTA | Review order should be displayed");
		Reporter.log("8. Click on Edit in cart | CartPage should be displayed");
		Reporter.log("9. Click Add a Phone CTA on cart | Phones PLP should  be displayed");
		Reporter.log("10. Select any device in PLP | PDP Page should  be displayed");
		Reporter.log("11. Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("12. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log(
				"13. Verify Personal info section previously entered info is re-populated | Personal info section previously entered info should be re-populated");
		Reporter.log("14. Click on Next button | Payments tab should be displayed");
		Reporter.log(
				"15. Verify Payments tab previously entered info | Payments tab previously entered info previously entered info should not be re-populated");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CheckOutPage checkOutPage = navigateToCheckoutWithPhone(tMNGData);
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);
		checkOutPage.verifyEditInCartCTA();
		checkOutPage.clickEditInCartCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.clickOnCartContinueBtn();
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyFirstnamePrePopulated();
		checkOutPage.verifyLastnamePrePopulated();
		checkOutPage.verifyEmailPrePopulated();
		checkOutPage.verifyPhoneNumberPrePopulated();
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();
		checkOutPage.VerifyPaymentInfoNotPrePopulated();

	}

	/**
	 * US345385:[Continued] TEST ONLY: TMO Additional Terms - Checkout Sticky Banner
	 * - Display Total Today and Total Monthly Payments
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testStickyBannerDisplayedTotalTodayAndMonthlyPayments(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US345385:TEST ONLY: TMO Additional Terms - Checkout Sticky Banner - Display Total Today and Total Monthly Payments");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("6. Verify 'Total Today' Price | 'Total Today' price should be displayed");
		Reporter.log("7. Verify 'Total Monthly' price | 'Total Monthly' price should be displayed");
		Reporter.log("8. Capture 'Total Today' price | 'Total Today' price value should be captured");
		Reporter.log("9. Capture 'Total Monthly' price | 'Total Monthly' price value should be captured");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CheckOutPage checkOutPage = navigateToCheckoutWithPhone(tMNGData);
		Double eipPrice = checkOutPage.getPersonalEIPPriceCheckout();
		Double monthlyPrice = checkOutPage.getPersonalMonthlyPriceCheckout();
		Reporter.log("Today price: " + eipPrice + "Monthly price: " + monthlyPrice + " in CheckOutPage");
	}

	/**
	 * US310997:[Continued] TEST ONLY: TMO Additional Terms - Checkout - Display
	 * Today and Monthly Payments at Line Level
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testTodayAndMonthlyPaymentsAtLineLevel(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US310997:TEST ONLY: TMO Additional Terms - Checkout - Display Today and Monthly Payments at Line Level");
		Reporter.log("Condition | Select device with Sim Starter Kit");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click View order details link | Order Details model should be displayed");
		Reporter.log("6. Capture 'Today' price | 'Today' price value should be captured");
		Reporter.log("7. Capture 'Monthly' price | 'Monthly' price value should be captured");
		Reporter.log("8. Close order details model window | Order details model window should be closed");
		Reporter.log("9. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log(
				"10. fill all personal and shipping information and click on next| payment and credit page should be displayed");
		Reporter.log(
				"11. fill all payment and credit information , set security pin and click on 'Agree & Next' | Review & submit page  should be displayed");
		Reporter.log("12. Verify 'Today' price | 'Today' price should be displayed");
		Reporter.log("13. Verify 'Today' price match with Captured 'Today' price | 'Today' price should be equal");
		Reporter.log("14. Verify 'Monthly' price | 'Monthly' price should be displayed");
		Reporter.log(
				"15. Verify 'Monthly price' match with Captured Total Monthly price | Monthly price should be equal");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();

		Double totalToday = cartPage.getTodayTotalCapturedPriceOnOrderDetailedPage();
		Double totalDownpayment = cartPage.getTotalTodaypriceAtOderDetailPage();
		cartPage.compareEIPDownpaymentAndSSKprice(totalToday, totalDownpayment);

		Double totalMonthly = cartPage.getMonthlyCapturedPriceInOrderDetailPage();
		cartPage.clickOnCloseIconAtOrderDetailPage();

		cartPage.verifyCartPageLoaded();
		cartPage.verifyStickyBannerMonthlyTotalPriceDisplayed();
		cartPage.clickOnCartContinueBtn();

		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);

		Double todayDownpaymentPrice = checkOutPage.getTotalTodayAmount();
		checkOutPage.compareTwoPrices(todayDownpaymentPrice, totalToday);

		Double totalMonthlyPrice = checkOutPage.getMonthlyPriceAtCheckOutReviewPage();
		checkOutPage.compareTwoPrices(totalMonthlyPrice, totalMonthly);
	}

	/**
	 * US586093 Updated Sim Starter Kit name and pricing as a line item for MI
	 * Device on Review Order Page BYOD Scenario - Updated Sim name should name for
	 * MI Device should reflect on Review Order Page
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void verifyUpdatedSimCardOnReviewOrderForBYODMIDevices(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US586093    BYOD Scenario - Sim Name card name and Pricing should display as line item for MI Device on Review Order Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO cart  | empty Cart page should be displayed");
		Reporter.log("2. Click on Tablets and Devices | PLP for Tablets and Devices will be displayed");
		Reporter.log("2. Click BYOD CTA on plp page | Mobile Internet SIM Card PDP should be displayed.");
		Reporter.log("4. Click Add to cart in BYOD PDP |Mobile Internet SIM Card should be added to Cart Page.");
		Reporter.log("5. Click on Continue button on sticky banner | Checkout page should be displayed");
		Reporter.log(
				"6. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log(
				"7. fill all payment and credit information , set security pin and click on 'Agree & Next' | Review & submit page  should be displayed");
		Reporter.log(
				"8. Verify Mobile Internet SIM Card name Pricing details of Sim |Sim card name and Pricing should be displayed as Sim Card and it will display as line item");
		Reporter.log("9. Verify the tooltip | Update copy of tool tip should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.verifyAddATabletLinkOnCartEssentials();
		cartPage.clickOnAddATabletLinkOnCart();
		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyTabletsAndDevicesPlpPageLoaded();
		plpPage.selectSimCardFromPlp();
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);
		checkOutPage.verifyMiSimKitNameDisplayed();
		checkOutPage.verifyMiSimKitPriceDisplayed();
	}

	/**
	 * US586092:[Continued] BYOD: Updated SIM name should reflect on Order Details
	 * (MI)
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void verifyUpdatedSimCardDetailsOnOrderDetailsForBYODMIDevices(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US586092    BYOD Scenario - Updated Sim Starter Kit name and pricing as line item for MI Device on Order details Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO cart  | empty Cart page should be displayed");
		Reporter.log("2. Navigate to Tablets and devices | PLP for tablets will pop up.");
		Reporter.log("2. Click BYOD CTA on plp | Mobile Internet SIM Card PDP should be displayed.");
		Reporter.log("4. Click Add to cart in BYOD PDP | T-Mobile® Mobile Internet SIM Card added to Cart Page.");
		Reporter.log(
				"5. Click on View order details button on stickey banner | Order details model should be displayed");
		Reporter.log(
				"6. Verify the Sim Card details | Sim card details and pricing details should be displayed as a line item");
		Reporter.log("7. Click on Continue button on sticky banner | Checkout page should be displayed");
		Reporter.log(
				"10. Fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.verifyAddATabletLinkOnCartEssentials();
		cartPage.clickOnAddATabletLinkOnCart();
		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyTabletsAndDevicesPlpPageLoaded();
		plpPage.selectSimCardFromPlp();
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

		cartPage.verifyMiSimKitNameDisplayed();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifySimKitNameOnOrderDetailsModalDisplayed();
		cartPage.verifySimKitPriceOnOrderDetailsModalDisplayed();
		cartPage.clickOnCloseIconAtOrderDetailPage();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);
	}

	/**
	 * US586083:[Continued] BYOD: Updated SIM name should reflect on Review Order
	 * (Voice)
	 * <p>
	 * T1442628:Validate the Device SIM name change is reflected on Review Order -
	 * HCC (All Plans)
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testSimStarterKitDetailsInSubmitOrderTab(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US586083    BYOD Scenario - Sim Name card name and Pricing should display as line item for Handset(Voice) on Review Order Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones page should be displayed");
		Reporter.log("3. Select SimStarterKit | PDP page should be displayed");
		Reporter.log("4. Verify SIM Card Name | SIM Card Name should be displayed");
		Reporter.log("5. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("6. Verify Selected plan is Magenta | Magenta plan should be selected");
		Reporter.log("7. Click on Continue button on Sticky banner | Check out page should be displayed");
		Reporter.log("8. Verify Personal Info tab | Personal Info tab should be displayed");
		Reporter.log(
				"9. Fill Personal Info tab requred fields and Click on Next button | Credit check tab should be displayed");
		Reporter.log(
				"10. Fill all required fileds on credit check and click Run Credit Check CTA | Loading screen should be displayed till HCC run.");
		Reporter.log("11. Verify the update price modal is displayed | update price modal should be displayed");
		Reporter.log(
				"12. Verify SimStarterKit Name | SimStarterKit Name is SIM CARD should be displayed View Order Details Model");
		Reporter.log(
				"13. Verify  SimStarterKit price is added to the Today Line total price for each line item | SimStarterKit price should be added to the Today Line total price for each line item");
		Reporter.log(
				"14. Verify SimStarterKit price is added to the Cart Total Today price | SimStarterKit price should be added to the Cart Total Today price");
		Reporter.log("15. Click On Next button | Shipping & Payments tab should be displayed");
		Reporter.log(
				"16. Fill Shipping & Payments tab requred fields and Click on Agree & Next button | Review and submit your order header should be displayed");
		Reporter.log(
				"17. Verify SimStarterKit Name | SimStarterKit Name is SIM CARD should be displayed View Order Details Model");
		Reporter.log(
				"18. Verify  SimStarterKit price is added to the Today Line total price for each line item | SimStarterKit price should be added to the Today Line total price for each line item");
		Reporter.log(
				"19. Verify SimStarterKit price is added to the Cart Total Today price | SimStarterKit price should be added to the Cart Total Today price");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.selectSimCardFromPlp();
		PdpPage devicesPdpPage = new PdpPage(getDriver());
		devicesPdpPage.verifyPhonePdpPageLoaded();
		devicesPdpPage.verifySimKitName();
		devicesPdpPage.verifySimKitPrice();
		devicesPdpPage.clickOnAddToCartBtn();
		devicesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());

		cartPage.verifyCartPageLoaded();
		cartPage.verifySimStaterKitPriceDisplayed();
		cartPage.verifyMagentaPlanSelected();

		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		checkOutPage.clickDetailsLinkOnPricingModal();
		checkOutPage.verifySimKitNameDisplayedOnPricingModal();
		checkOutPage.verifySimKitPriceDisplayedOnPricingModal();
		checkOutPage.clickCheckOutNowCTAOrderModal();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.fillPaymentInfo(tMNGData);
		checkOutPage.setSecurityPin(tMNGData);
		checkOutPage.clickAgreeandNextBtn();
		checkOutPage.verifyReviewandSubmitPage();
		checkOutPage.verifyMiSimKitNameDisplayed();
		checkOutPage.verifyMiSimKitPriceDisplayed();

		Double V1 = checkOutPage.getTotalSimPriceOnReviewOrder();
		Double V2 = checkOutPage.getReviewSubmitTotalTodayPrice();
		cartPage.verifyMonthlyPaymentsLinePrices(V1, V2);

	}

	/**
	 * US586012:SSK fee will no longer display on Review Order for a new device
	 * purchase T1442617:HCC - SSK fee is not displayed on Line item, Today total in
	 * Review Order Mixed cart - Magenta
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testSSKfeeDetailsOnReviewOrderPageForDevices(TMNGData tMNGData) {
		Reporter.log("US586012:SSK fee will no longer display on Review Order for a new device purchase");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Click on Add a Tablet CTA | Tablet PLP should be displayed");
		Reporter.log("6. Select any Tablet | PDP page should be displayed");
		Reporter.log("7. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("8. Click on Add a Wearable CTA | Wearable PLP should be displayed");
		Reporter.log("9. Select any Wearable | PDP page should be displayed");
		Reporter.log("10. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("11. Click on Add a Tablet CTA | Tablet PLP should be displayed");
		Reporter.log("12. Select HotSpot device | PDP page should be displayed");
		Reporter.log("13. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("14. Click on Add a Tablet CTA | Tablet Mini PLP should be displayed");
		Reporter.log("15. Select IOT device | PDP page should be displayed");
		Reporter.log("16. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("17. Click on Continue button on sticky banner | Checkout page should be displayed");
		Reporter.log(
				"18. Fill all personal info tab fileds and click on Next button | Credit check tab should be displayed");
		Reporter.log(
				"19. Fill all Credit check tab fields and click on Next button | Shipping tab should be displayed");
		Reporter.log(
				"20. Fill all Shipping tab fields and click on Next button | Review order tab should be displayed");
		Reporter.log(
				"21. Verify SSK Fee presence in One Time Payment Section | SSK fee should not be displayed as line breakdown");
		Reporter.log("22. Verify Line Total | Total should be matched");
		Reporter.log("23. Verify SalesTax calculation | SSK Fee should not added to SalesTax price");
		Reporter.log("24. Verify Total Today price | SSK Fee should not added to Total Today price");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickOnAddATabletLinkOnCart();
		PlpPage tabletsAndDevicesPlpPage = new PlpPage(getDriver());
		tabletsAndDevicesPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		tabletsAndDevicesPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		PdpPage tabletsAndDevicesPdpPage = new PdpPage(getDriver());
		tabletsAndDevicesPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletsAndDevicesPdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnAddAWearableLinkOnCart();

		PlpPage wearablePlpPage = new PlpPage(getDriver());
		wearablePlpPage.verifyWatchesPlpPageLoaded();
		wearablePlpPage.clickDeviceWithAvailability(tMNGData.getWatchName());
		PdpPage wearablePdpPage = new PdpPage(getDriver());
		wearablePdpPage.verifyWatchesPdpPageLoaded();
		wearablePdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnAddATabletLinkOnCart();
		tabletsAndDevicesPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		tabletsAndDevicesPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		tabletsAndDevicesPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletsAndDevicesPdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

		cartPage.clickOnAddATabletLinkOnCart();
		tabletsAndDevicesPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		tabletsAndDevicesPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		tabletsAndDevicesPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletsAndDevicesPdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);

		checkOutPage.verifySimStarterKitTextNotDisplayed();
		checkOutPage.verifyLineTotalCheckout();
		checkOutPage.verifySalesTaxCalculation();
		checkOutPage.verifyTotalTodayCalculation();

	}

	/**
	 * US589257: TPP - Display Ad Ons for Updated Pricing Modal
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testPriceUpdatedModelInCreditCheckTab(TMNGData tMNGData) {
		Reporter.log("Test Case :US589257: TPP - Display Ad Ons for Updated Pricing Modal");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click Add a Phone CTA on cart | Phones PLP should  be displayed");
		Reporter.log("3. Select any device in PLP | PDP Page should  be displayed");
		Reporter.log("4. Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Click Add a Tablet CTA on cart | Tablets PLP should  be displayed");
		Reporter.log("6. Select any device in PLP | PDP Page should  be displayed");
		Reporter.log("7. Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("8. Select any Add-ons to Tablet | Add-ons should be added");
		Reporter.log("9. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("10. Verify personal info tab | Personal info tab should be displayed");
		Reporter.log("11. Fill the required fields and click next button | Credit check tab should be displayed");
		Reporter.log("12. Fill the required fields and click next button | Updated pricing model should be displayed");
		Reporter.log("13. Verify price and Saved in V1 | Device price should be saved in V1");
		Reporter.log("14. Verify Add is available for each line | Add should be displayed for each line");
		Reporter.log("15. Verify Add-on services | Add-on services should be displayed");
		Reporter.log("16. Select any Add-on service | Add-on service should be selected");
		Reporter.log("17. Verify price | Add-on service Price should be added to V1");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPagefromEmptyCartFromUNOPLP(tMNGData);
		cartPage.clickOnAddATabletLinkOnCart();
		PlpPage tabletsAndDevicesPlpPage = new PlpPage(getDriver());
		tabletsAndDevicesPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		tabletsAndDevicesPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		PdpPage tabletsAndDevicesPdpPage = new PdpPage(getDriver());
		tabletsAndDevicesPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletsAndDevicesPdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		// cartPage.selectProtection360PlanOnModal();
		// cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		checkOutPage.clickCaretIconToExpandAllLine();
		checkOutPage.verifyPlanNameOnOrderModalForAllLine();
		Float devicePrice = checkOutPage.getMonthlyPriceForDeviceAndTablet();
		Float actualtotal = checkOutPage.gettotalMonthlyPricingPlanAndDeviceOnOrderModal();
		checkOutPage.compareTwoFloatValues(devicePrice, actualtotal);
	}

	/**
	 * US587610: [Continued] TPP - UI error handling in data validation (Personal
	 * Info Tab)
	 *
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testValidatePersonalInfoSectionFieldsForHCCflow(TMNGData tMNGData) {
		Reporter.log("Test Case :US587610: [Continued] TPP - UI error handling in data validation (Personal Info Tab)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Click on Continue button | Check out page should be displayed");
		Reporter.log("6. Verify Personal Info Page is displayed| Personal Info Page should be displayed");
		Reporter.log(
				"7. Leave personal Info for required fields in Personal info section | Personal info section error messages should be displayed");
		Reporter.log("8. Verify Next button | Next button should be disabled");
		Reporter.log(
				"9. Now Fill personal info in personal info section | Personal info section should be updated, Next button should be enabled.");
		Reporter.log("10. Click on Next Button | Credit Check Tab should be displayed");
		Reporter.log(
				"11. Enter required fileds and enter DOB greater than 18 years old | Run Credit Check CTA should be enabled");
		Reporter.log(
				"12. Change DOB to less than 18 years old | Error message- 'You must be at least 18 years of age.' should be displayed and Run Credit Check CTA should be disabled.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CheckOutPage checkOutPage = navigateToCheckoutWithPhone(tMNGData);
		checkOutPage.verifyErrorMessageForFirstName();
		checkOutPage.verifyErrorMessageForLastName();
		checkOutPage.verifyErrorMessageForEmail();
		checkOutPage.verifyErrorMessageForPhoneNumber();
		checkOutPage.verifyNextBtnDisabled();
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.verifyRunCreditCheckBtnEnabled();
		checkOutPage.fillDOBforCreditCheckLessThan18Years();
		checkOutPage.verifyErrorMessageForDOBLessThan18Years();
		checkOutPage.verifyRunCreditCheckBtnDisabled();

	}

	/**
	 * US587614: TPP - UI error handling in data validation (Credit Check Tab)
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testValidateCreditCheckSectionAndDateOfBirthErrorMessagesForHCCflow(TMNGData tMNGData) {
		Reporter.log("Test Case :US587614: TPP - UI error handling in data validation (Credit Check Tab) ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click on Continue button | Check out page should be displayed");
		Reporter.log(
				"6. Fill personal info in personal info section and click Next button | Credit check tab should be displayed");
		Reporter.log(
				"7. Leave required fields in Credit check tab | Credit check tab error messages should be displayed");
		Reporter.log("8. Now Fill reqired info and Enter DOB field < 18 years | Agree&Next button should be disabled");
		Reporter.log(
				"9. Verify the Validation error message | 'You must be at least 18 years of age' error message should be displayed");
		Reporter.log("10. Now enter DOB field > 18 years | Agree&Next button should be enabled");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CheckOutPage checkOutPage = navigateToCheckoutWithPhone(tMNGData);
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();

		checkOutPage.verifyErrorMessageForCreditCheckAddressLine1();
		checkOutPage.verifyErrorMessageForCreditCheckCity();
		checkOutPage.verifyErrorMessageForCreditCheckState();
		checkOutPage.verifyErrorMessageForCreditCheckZipcode();
		checkOutPage.verifyErrorMessageForDOB();
		checkOutPage.verifyErrorMessageForSSN();
		checkOutPage.verifyErrorMessageForIDNumber();
		checkOutPage.verifyErrorMessageForCreditCheckExpiryDate();
		checkOutPage.verifyRunCreditCheckBtnDisabled();

		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.verifyRunCreditCheckBtnEnabled();
	}

	/**
	 * US554696: TPP - Retain Checkout Page values/ Pre populate personal info tab
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testAccuratePriceIsDisplayedAndPersonalInfoTabFieldsEditableForSCCflow(TMNGData tMNGData) {
		Reporter.log("Test Case :US554696: TPP - Retain Checkout Page values/ Pre populate personal info tab");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click on Find my price CTA | PreScreen Intro page should be displayed");
		Reporter.log("6. Click on Lets go CTA | PreScreen form should be displayed");
		Reporter.log("7. Fill the mandatory field in Prescreen form | Mandatory fields should be filled");
		Reporter.log("8. Click on Find my price CTA | Pre-screen loading model should be displayed");
		Reporter.log("9. Verify updated price | Updated price should be displayed should be displayed");
		Reporter.log("10. Click on Continue button | Check out page should be displayed");
		Reporter.log("11. Verify First Name is not editable or should be locked | First Name should be locked ");
		Reporter.log("12. Verify Last Name is not editable or should be locked | Last Name should be locked");
		Reporter.log("13. Verify Middle Initial will be editable | Middle Initial should be editable");
		Reporter.log("14. Verify Email will be editable | Email field should be editable");
		Reporter.log("15. Verify Phone number will be null | Phone number should be null");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		TPPOfferPage tPPOfferPage = new TPPOfferPage(getDriver());
		tPPOfferPage.verifyTPPOfferPage();
		tPPOfferPage.clickGotItCTA();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyPricingBasedOnCreditTextInCart();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyFirstnameNotEditableAndLocked();
		checkOutPage.verifyMiddlenameEditable();
		checkOutPage.verifyLastnameNotEditableAndLocked();
		checkOutPage.verifyEmailEditable();
		checkOutPage.verifyPhoneNumberIsNull();

	}

	/**
	 * US590222:TEST ONLY TPP - Update Order Details button in cart and remove CTA
	 * in Checkout
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TPP })
	public void testUpdateOrderDetailsCTAOnCartAndRemoveCTAOnCheckOutPageForHccIsPassed(TMNGData tMNGData) {
		Reporter.log("US590222:TEST ONLY TPP - Update Order Details button in cart and remove CTA in Checkout");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Add an  Accessory to cart | Accessory products should be added into cart");
		Reporter.log("6. Click on Continue button on sticky banner | Checkout page should be displayed");
		Reporter.log(
				"7. Verify that CRP8 /awesome price is displayed in the top sticky | CRP8 /awesome price should be displayed in the top sticky");
		Reporter.log("8. Verify View Order Details link | View Order details link should not be displayed");
		Reporter.log(
				"9. Fill all personal information on 'Personal Info' tab and click on next | customer should navigated to the Verify Id Page");
		Reporter.log("10. Verify View Order Details link | View Order details link should not be displayed");
		Reporter.log(
				"11. Fill all required fileds on credit check and click Run Credit Check CTA | Loading screen should be displayed till HCC run.");
		Reporter.log("12. Verify the update price modal is displayed | update price modal should be displayed");
		Reporter.log("13. Verify View Order Details link | View Order details link should not be displayed");
		Reporter.log("14. Verify Pricing for each Item | Should be displayed as Monthly, Today for pricing");
		Reporter.log("15. Verify Total amount of all lines | Should be displayed as Monthly, Today for pricing");
		Reporter.log("15. Click on 'Finish CheckOut' CTA | Should be navigated to Shipping and Payments Tab");
		Reporter.log("16. Verify Pricing for each Item | Should be displayed as Monthly, Today for pricing");
		Reporter.log(
				"17. Fill Payment section fields and Credit check fields and click on 'Agree&Next' CTA |  Should be navigated to Review and Submit order page");
		Reporter.log("18. Verify View Order Details link | View Order details link should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();

		cartPage.clickOnAddAnAccessoryLinkOnCart();
		PlpPage accessoriesPlpPage = new PlpPage(getDriver());
		accessoriesPlpPage.verifyAccessoriesPlpPageLoaded();
		accessoriesPlpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoriesPdpPage = new PdpPage(getDriver());
		accessoriesPdpPage.verifyAccessoriesPdpPageLoaded();
		accessoriesPdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyPricingAtCheckOutStickyBanner();
		checkOutPage.verifyViewOrderDetailsLinkNotDisplayed();

		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		checkOutPage.verifyAccessoriesOnUpdatePricingModal();
		checkOutPage.clickdowncaratButtonForAccessories();
		checkOutPage.verifyAccessoriesPricingAreDisplayed();
	}

	/**
	 * US590222:TEST ONLY TPP - Update Order Details button in cart and remove CTA
	 * in Checkout
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TPP })
	public void testUpdateOrderDetailsCTAOnCartAndRemoveCTAOnCheckOutPage(TMNGData tMNGData) {
		Reporter.log("US590222:TEST ONLY TPP - Update Order Details button in cart and remove CTA in Checkout");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Add an  Accessory to cart | Accessory products should be added into cart");
		Reporter.log("6. Click on Continue button on sticky banner | Checkout page should be displayed");
		Reporter.log(
				"7 Verify that CRP8 /awesome price is displayed in the top sticky | CRP8 /awesome price should be displayed in the top sticky");
		Reporter.log("8. Verify Remove Order Details CTA | Remove Order details CTA should be displayed");
		Reporter.log(
				"9. Fill all personal information on 'Personal Info' tab and click on next | customer should navigated to the Credit Check Page");
		Reporter.log("10. Verify Remove Order Details CTA | Remove Order details CTA should be displayed");
		Reporter.log(
				"11. Fill all required fileds on credit check and click Run Credit Check CTA | Loading screen should be displayed till HCC run.");
		Reporter.log("12. Verify the update price modal is displayed | update price modal should be displayed");
		Reporter.log("13. Verify Remove Order Details CTA | Remove Order details CTA should be displayed");
		Reporter.log("14. Verify Pricing for each Item | Should be displayed as Monthly, Today for pricing");
		Reporter.log("15. Verify Total amount of all lines | Should be displayed as Monthly, Today for pricing");
		Reporter.log(
				"16. Fill Payment section fields and Credit check fields and click on 'Agree&Next' CTA |  Should be navigated to Review and Submit order page");
		Reporter.log("17. Verify Remove Order Details CTA | Remove Order details CTA should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();

		cartPage.clickOnAddAnAccessoryLinkOnCart();
		PlpPage accessoriesPlpPage = new PlpPage(getDriver());
		accessoriesPlpPage.verifyAccessoriesPlpPageLoaded();
		accessoriesPlpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoriesPdpPage = new PdpPage(getDriver());
		accessoriesPdpPage.verifyAccessoriesPdpPageLoaded();
		accessoriesPdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyPricingAtCheckOutStickyBanner();

		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		checkOutPage.verifyAccessoriesOnUpdatePricingModal();
		checkOutPage.clickdowncaratButtonForAccessories();
		checkOutPage.verifyAccessoriesPricingAreDisplayed();
	}

	/**
	 * US590238:TEST ONLY Check Out - Create form fields and functionality for
	 * Verify ID tab
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TPP })
	public void testFormFieldsAndFunctionalityForVerifyIdTab(TMNGData tMNGData) {
		Reporter.log("US590238:TEST ONLY Check Out - Create form fields and functionality for Verify ID tab");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
		Reporter.log("3. Select Device | PDP page should be displayed");
		Reporter.log(
				"8. Click on Add to cart button, then click on continue as guest |  Device should be added to Cart, and Cart Page should be displayed.");
		Reporter.log("5. Click on 'See your payments' on Cart Page| 'Pre-screen intro' tab will be displayed");
		Reporter.log("6. Click on 'Let's do it' button on 'Pre-screen Intro tab'|Form for pre-screen will displayed");
		Reporter.log(
				"6. Fill the form for Pre-screen and click 'See your payments' button | 'Pre-screen' tab will be display");
		Reporter.log(
				"7. Verify personalized pricing after a successful pre-screen on Cart page| Personalized pricing will be shown on Cart page");
		Reporter.log("10. Click on Continue button on sticky banner | Checkout page should be displayed");
		Reporter.log(
				"11. Fill all personal information on 'Personal Info' tab and click on next | 'Verify ID' tab should be displayed");
		Reporter.log("12. Verify Authorable Section Title | 'We need to verify your identity' should be displayed");
		Reporter.log(
				"13. Verify Authorable Section Text  | 'Make sure your Social Security Number matches...' should be displayed");
		Reporter.log("14. Verify Authorable Text | 'Personal Information' should be displayed");
		Reporter.log(
				"15. Verify Social Security Number editable Text Field  | 'SSN' editable Text Field should be displayed");
		Reporter.log("15. Verify 'ID type*' field  | 'Id type*' drop down should be displayed");
		Reporter.log("15. Verify 'ID Number*' field  | 'ID Number*' editable Text field should be displayed");
		Reporter.log(
				"15. Verify 'Expiration Date*' field'  | 'Expiration Date*' editable Text field should be displayed");
		Reporter.log("15. Verify 'State*' field  | State* drop down Section should be displayed'");
		Reporter.log("15. Verify Next CTA'  | 'Next CTA' should be displayed'");
		Reporter.log(
				"16. Verify 'Terms&Condition Policy' | Ensure that all T-Mobile products and services comply with...' should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TPPPreScreenForm tppPreScreenForm = navigateToTPPPreScreenFormPageFromDeviceCart(tMNGData);
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppPreScreenForm.verifyPreScreenloadingPage();
		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();
		tppOfferPage.clickGotItCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfoAfterpreScreen(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyVerifyIdentityPage();
		checkOutPage.verifyVerifyIdentityPageSubHeader();
		checkOutPage.verifyVerifyIdentityPersonalInfoHeader();
		checkOutPage.verifySSNTextfield();
		checkOutPage.verifyidTypefield();
		checkOutPage.verifyIDNumberTextfield();
		checkOutPage.verifyExpirationDateTextfield();
		checkOutPage.verifyVerifyIDStatefield();
		checkOutPage.verifyNextCTAfield();
		checkOutPage.verifyPrivacyImageAndPolicy();
	}

	/**
	 * US552574:TEST ONLY Check Out - #4 Review and Submit should work as production
	 * (no tpp changes)
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TPP })
	public void testAllDetailsInReviewAndSubmitTabOnCheckOutPage(TMNGData tMNGData) {
		Reporter.log("US552574:TEST ONLY Check Out -  #4 Review and Submit should work as production (no tpp changes)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
		Reporter.log("3. Select Device | PDP page should be displayed");
		Reporter.log("5. Click on 'Verify your pricing' on PDP| 'Pre-screen intro' tab will be displayed");
		Reporter.log("6. Click on 'Get Started' button on 'Pre-screen Intro tab'|Form for pre-screen will displayed");
		Reporter.log(
				"6. Fill the form for Pre-screen and click 'Find my price' button | 'Pre-screen' tab will be display");
		Reporter.log(
				"7. Verify personalized pricing after a successful pre-screen on PDP page| Personalized pricing will be shown on pdp page");
		Reporter.log("8. Click on Add to cart button | New to T-Mobile and Existing customer CTA should pop up");
		Reporter.log("9. Click on continue as guest  | Cart Page should  be displayed");
		Reporter.log("10. Click on Continue button on sticky banner | Checkout page should be displayed");
		Reporter.log(
				"11. Fill all personal information on 'Personal Info' tab and click on next | 'Verify ID' tab should be displayed");
		Reporter.log(
				"12. Fill all details to Verify Id and Click on Next CTA | Should be navigated to Shipment & Payments tab");
		Reporter.log(
				"13. Fill all Shipping and Payments' information and click on next | Should be navigated to 'Review and Submit Order'");
		Reporter.log(
				"14. Verify Authourable section order | Header section text should be 'Review and submit your order'");
		Reporter.log("15. Verify 'Edit Cart' | Edit Cart CTA should be displayed");
		Reporter.log("16. Verify 'Order Estimated Ship Date' | Order Estimated ship date should be 'x/x-x/x' format");
		Reporter.log(
				"17. Verify each line added in cart | 'Line number',SKU Name and Plan, due Today and due Monthly,Expandable Carrot should be displayed");
		Reporter.log("18. Verify Expandable Carrot | Should be collapsed by default");
		Reporter.log("19. Click on Expandable Carrot | Carrot should be expanded");
		Reporter.log(
				"20. Verify Line Breakdown | Plan Name/discounts, add ons/sim kit info, Line Deposit should be displayed");
		Reporter.log("21. Verify Sku details | Sku Image,Sku Name,details/Size variants,etc should be displayed");
		Reporter.log(
				"22. Verify Legal text under sku | Legal text under sku,Promotional text should be displayed if applicable");
		Reporter.log(
				"23. Verify  Subtotal Section| Title - Subtotal,One Time Fees,Sales Tax,Shipping details should be displayed");
		Reporter.log(
				"24. Verify Total from the above lines | Today and Monthly Total, 'Monthly Payment details(text)' should be displayed");
		Reporter.log(
				"25. Verify Shipping Review | Shipping,Edit - CTA, Shipping address as entered in previous page should be displayed");
		Reporter.log(
				"26. Verify Payment Review | Payment,Edit - CTA, Payment Type and Payment address as entered in previous page,Display Auto Payment and Terms and Conditions should be displayed");
		Reporter.log("27. Select Terms and Conditions check box at bottom | 'Agree & Submit' CTA should be enabled");
		Reporter.log("28. Verify Privacy Image | Privacy Image,Text, link should be displayed at bottom");
		Reporter.log(
				"29. Verify 'Terms&Condition Policy' | Ensure that all T-Mobile products and services comply with...' should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.verifyPhonesPlpPageLoaded();
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		String downPaymentBefore = phonesPdpPage.getDownPaymentPrice();
		String EIPPriceBefore = phonesPdpPage.getEIPPaymentPrice();
		phonesPdpPage.clickFindYourPricingLinkInPDP();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();
		tppOfferPage.clickGotItCTA();
		phonesPdpPage.verifyPhonePdpPageLoaded();
		String downPaymentAfter = phonesPdpPage.getDownPaymentPrice();
		String EIPPriceAfter = phonesPdpPage.getEIPPaymentPrice();
		phonesPdpPage.compareTwoStringValues(EIPPriceBefore, EIPPriceAfter);
		phonesPdpPage.compareTwoStringValues(downPaymentBefore, downPaymentAfter);
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfoAfterpreScreen(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyVerifyIdentityPage();
		checkOutPage.fillSSN(tMNGData);
		checkOutPage.fillIdentityVerificationForVerifyID(tMNGData);

		checkOutPage.clickRunCreditCheck();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.fillPaymentInfo(tMNGData);
		checkOutPage.setSecurityPin(tMNGData);
		checkOutPage.clickAgreeandNextBtn();
		checkOutPage.verifyReviewandSubmitPage();
		checkOutPage.verifyEditInCartCTA();
		checkOutPage.verifyShippingDateDisplayedAndFormat();
		checkOutPage.verifyLineOnReviewAndSubmit();
		checkOutPage.clickCarrot();
		checkOutPage.verifyCarrotCollapsed();
		checkOutPage.clickCarrot();
		checkOutPage.verifyPlanNameAndDiscount();
		checkOutPage.verifySKUDetails();
		checkOutPage.verifyPromoTextForDevice();
		checkOutPage.verifyLegalTextForDevice();
		checkOutPage.verifySubTotalSection();
		checkOutPage.verifyTotal();
		checkOutPage.verifyTodayAndMonthly();
		checkOutPage.verifyShippingReviewHeader();
		checkOutPage.verifyShippingReviewEditCTA();
		checkOutPage.verifyIfShippingAddressIsCorrect(tMNGData);
		checkOutPage.verifyPaymentReviewHeader();
		checkOutPage.verifyPaymentReviewEditCTA();
		checkOutPage.verifyIfPaymentAddressIsCorrect(tMNGData);
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyPrivacyImageAndPolicy();
		checkOutPage.verifyTermsAndConditions();

	}

	/**
	 * US589243: TEST ONLY Check Out - Verify ID #2 tab - Validate SSN to match last
	 * 4 of pre screen T1302436 : Verify error should be displayed when the customer
	 * provides incorrect info or does not enter the required info
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testSSNMismatchBetweenPreScreenAndIdPageForSCCflow(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US589243: TEST ONLY Check Out - Verify ID #2 tab - Validate SSN to match last 4 of pre screen");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Click on Find my price CTA | PreScreen Intro page should be displayed");
		Reporter.log("6. Click on Lets go CTA | PreScreen form should be displayed");
		Reporter.log("7. Fill the mandetery field in Prescreen form | Mandetery fields should be filled");
		Reporter.log("8. Click on Find my price CTA | Pre-screen loading model should be displayed");
		Reporter.log("9. Verify updated price | Updated price should be displayed should be displayed");
		Reporter.log("10. Click on GotIt CTA button in Offer page | Cart page should be displayed");
		Reporter.log("11. Click on Continue button | CheckOut page should be displayed");
		Reporter.log(
				"12. Verify personal info for required fields pre populated | Personal info section required fields should be pre populated");
		Reporter.log("13. Click Next button | ID tab should be displayed");
		Reporter.log(
				"14. Enter SSN value that is different from Pre-Screen form | SSN mismatch error message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TPPPreScreenForm tppPreScreenForm = navigateToTPPPreScreenFormPageFromDeviceCart(tMNGData);
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();
		tppOfferPage.clickGotItCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyFirstnamePrePopulated();
		checkOutPage.verifyLastnamePrePopulated();
		checkOutPage.verifyEmailPrePopulated();
		checkOutPage.fillPersonalInfoAfterpreScreen(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyVerifyIdentityPage();
		checkOutPage.fillSSN(tMNGData);
		checkOutPage.fillIdentityVerificationForVerifyID(tMNGData);
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyErrorMessageForSSNMismatch();
	}

	/**
	 * US588826:[Continued] TEST ONLY HCC: show updated FRAUD Modal - CHECKOUT page
	 * (when manual review /fraud)
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testFraudModelForHCCFlow(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US588826:[Continued] TEST ONLY HCC: show updated FRAUD Modal - CHECKOUT page (when manual review /fraud)");
		Reporter.log("Test Steps | Expected Results:");

		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click on Continue button | Check out page should be displayed");
		Reporter.log(
				"6. Fill personal info in personal info section and click Next button | Credit check tab should be displayed");
		Reporter.log("7. Fill required info and enter wrong SSN | Fraud review model should be displayed");
		Reporter.log("8. Verify Fraud review model header | Fraud review model header should be displayed");
		Reporter.log("9. Verify Fraud review model text | Fraud review model text should be displayed");
		Reporter.log("10. Verify Call 1-800-t-mobile CTA | Call 1-800-t-mobile CTA should be displayed");
		Reporter.log("11. Verify 'Schedule a store visit' CTA | 'Schedule a store visit' CTA should be displayed");
		Reporter.log("12. Close Fraud review model  | Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CheckOutPage checkOutPage = navigateToCheckoutWithPhone(tMNGData);
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyTppFraudModal();
		checkOutPage.verifyTppFraudReviewModalHeader();
		checkOutPage.verifyTppFraudReviewModalText();
		checkOutPage.verifyCallCta();
		checkOutPage.verifyStoreVisitCta();
		checkOutPage.clickOnFraudModalCloseCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
	}

	/**
	 * US619677: TPP - Checkout: payment name to be UNeditable (replacing US614055 )
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TPP })
	public void testNameOnCardFieldUnEditableOnPaymentsTab(TMNGData tMNGData) {
		Reporter.log("Test Case :US619677: TPP - Checkout:  payment name to be UNeditable");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click on Continue button | Check out page should be displayed");
		Reporter.log("6. fill all personal information and click on next | Credit check tab should be displayed");
		Reporter.log(
				"7. Fill all Residential address,Credit approval information and Identity verification fields and Click Run credit check button | Credit check Order Modal should be displayed ");
		Reporter.log("8. Click on Check out now CTA | Shipping and Payments tab should be displayed");
		Reporter.log("9. Fill Shipping Info fields | Shipping info fields should be entered");
		Reporter.log("10. Verify Name field is blocked for editing | Name field should be blocked for editing");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CheckOutPage checkOutPage = navigateToCheckoutWithPhone(tMNGData);
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtnTPP();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.verifyOrderModalForCreditCheck();
		checkOutPage.clickCheckOutNowCTAOrderModal();
		checkOutPage.verifyAgreeandNextBtnDisabled();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.verifyNameOnCardIsDisplayedIsNotEditable();
	}

	/**
	 * US610118: [TEST ONLY]BYOD - Promotion changes for SIM fee with PROMO CODE
	 * Discount
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testSimCartPromoOfferPriceInCheckOutPage(TMNGData tMNGData) {
		Reporter.log("Test Case : US610118: [TEST ONLY]BYOD - Promotion changes for SIM fee with PROMO CODE Discount");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select Sim Card from phones page | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log(
				"5. Enter the Promo code for Sim Card in CartPage | Discount for the sim card should be reflected on the one time charges section");
		Reporter.log("6. Click on View Order Details link | Order details model should be displayed");
		Reporter.log(
				"7. Verify Discount price for the SimCard | Discount for the Sim Card should be reflected in Order details model");
		Reporter.log("8. fill all personal information and click on next | Credit check tab should be displayed");
		Reporter.log(
				"9. fill all Residential address information and Identiy verification info and Click continue button | Shipping and payments tab should be displayed");
		Reporter.log(
				"10. fill shipping address information and Set your security PIN info and Click Agress $ Next button | Review & Submit tab should be displayed");
		Reporter.log(
				"11. Verify Discount price for the SimCard | Discount for the Sim Card should be reflected in Review and Submit tab");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.selectSimCardFromPlp();
		PdpPage devicesPdpPage = new PdpPage(getDriver());
		devicesPdpPage.verifyPhonePdpPageLoaded();
		devicesPdpPage.clickOnAddToCartBtn();
		devicesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifySimStaterKitPriceDisplayed();

		Double V1 = cartPage.getBYODPriceForSIM();
		cartPage.enterPromoCodeValue(TmngCommonLib.PROMO_CODE);
		cartPage.clickApplyButton();
		Double V2 = cartPage.getBYODPriceForSIM();
		cartPage.compareMonthlyPaymentsLinePrices(V1, V2);
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();

		cartPage.verifystrikeThroughPriceInOrderDetailsModel();
		Double V3 = cartPage.getSimStarterKitPriceIntegerAtOderDetailPage();
		cartPage.compareMonthlyPaymentsLinePrices(V1, V3);
		cartPage.clickOnCloseIconAtOrderDetailPage();
		cartPage.clickOnCartContinueBtn();

		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		checkOutPage.clickCheckOutNowCTAOrderModal();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.fillPaymentInfo(tMNGData);
		checkOutPage.setSecurityPin(tMNGData);
		checkOutPage.clickAgreeandNextBtn();
		checkOutPage.verifyReviewandSubmitPage();
		Double V4 = checkOutPage.getSimStarterKitPrice();
		cartPage.compareMonthlyPaymentsLinePrices(V1, V4);

	}

	/**
	 * TPP - Checkout: with HCC
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TPP_CHECK })
	public void testVerifyCompleteCheckOutWithHCC(TMNGData tMNGData) {
		Reporter.log("Test Case :  Add multiple type of devices to cart");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones link | Cell Phones page should be displayed");
		Reporter.log("3. Select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart | Cart page should be displayed");

		Reporter.log("5. Click on 'Add a phone' from cart | Cell Phones page should be displayed");
		Reporter.log("6. Select any device | PDP page should be displayed");
		Reporter.log("7. Click on Add to Cart | Cart page should be displayed");

		Reporter.log("8. Click on 'Add a phone' from cart | Cell Phones page should be displayed");
		Reporter.log("9. Select any device | PDP page should be displayed");
		Reporter.log("10. Select 'Want to pay in full?' link | FRP price should be displayed");
		Reporter.log("11. Click on Add to Cart | Cart page should be displayed");

		Reporter.log("12. Click on 'Add a tablet' from cart | Tablets PLP page should be displayed");
		Reporter.log("13. Select any tablet device | PDP page should be displayed");
		Reporter.log("14. Click on Add to Cart | Cart page should be displayed");

		Reporter.log("15. Click on 'Add a wearable' from cart | Wearable PLP page should be displayed");
		Reporter.log("16. Select any wearable device | PDP page should be displayed");
		Reporter.log("17. Click on Add to Cart | Cart page should be displayed");

		Reporter.log("18. Click on 'Add an accessory' from cart | Accessory PLP page should be displayed");
		Reporter.log("19. Select any accessory device | PDP page should be displayed");
		Reporter.log("20. Click on Add to Cart | Cart page should be displayed");

		Reporter.log(
				"21. Click 'See More' link on Add ons Extras section for First line  | Enter Your ZIP Code model should be displayed");
		Reporter.log("22. Enter valid ZIP Code and click on Next button | Select services should be displayed");
		Reporter.log(
				"23. Select any Recommended services and Click on Update button | Select services should be displayed in cartPage");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

//		Device #1 EIP
		PlpPage plpPage = navigateToPhonesPlpPage(tMNGData);
//		plpPage.clickDevice(tMNGData.getDeviceName());
		plpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.selectMemoryOption(tMNGData.getDeviceMemoryOption());
		phonesPdpPage.selectColorOption(tMNGData.getDeviceColorOption());
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
//		Adding Protection SOC
		cartPage.clickProtectionInAddOnsAndExtras();
		cartPage.enterYourZIPCode(tMNGData.getZipcode());
		cartPage.clickOnNextBtn();

//		Device #2 EIP
		cartPage.clickOnAddAPhoneLinkOnCart();
		plpPage.verifyPhonesPlpPageLoaded();
//		plpPage.clickDevice(tMNGData.getSecondDeviceName());
		plpPage.clickDeviceWithAvailability(tMNGData.getSecondDeviceName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.selectMemoryOption(tMNGData.getSecondDeviceMemoryOption());
		phonesPdpPage.selectColorOption(tMNGData.getSecondDeviceColorOption());
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

//		Device #3 FRP
		cartPage.clickOnAddAPhoneLinkOnCart();
		plpPage.verifyPhonesPlpPageLoaded();
//		plpPage.clickDevice(tMNGData.getThirdDeviceName());		
		plpPage.clickDeviceWithAvailability(tMNGData.getThirdDeviceName());
		pdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.selectMemoryOption(tMNGData.getThirdDeviceMemoryOption());
		phonesPdpPage.selectColorOption(tMNGData.getThirdDeviceColorOption());
		pdpPage.clickPayInFullPaymentOption();
		pdpPage.verifyFRPPriceAfterClickingOnPayInFull();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

//		Tablet EIP
		cartPage.clickOnAddATabletLinkOnCart();
		plpPage.verifyTabletsAndDevicesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		pdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		phonesPdpPage.selectMemoryOption(tMNGData.getTabletMemoryOption());
		phonesPdpPage.selectColorOption(tMNGData.getTabletColorOption());
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

//		Watch EIP
		cartPage.clickOnAddAWearableLinkOnCart();
		plpPage.verifyWatchesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getWatchName());
		pdpPage.verifyWatchesPdpPageLoaded();
		pdpPage.selectMemoryOption(tMNGData.getWatchMemoryOption());
		pdpPage.selectColorOption(tMNGData.getWatchColorOption());
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

//		Accessory
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		plpPage.verifyAccessoriesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		pdpPage.verifyAccessoriesPdpPageLoaded();
		pdpPage.selectColorOption(tMNGData.getAccessoryColorOption());
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

		cartPage.clickOnCartContinueBtn();

		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());

	}

	/**
	 * TPP - Checkout: with SCC
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TPP_CHECK })
	public void testVerifyCompleteCheckOutWithSCC(TMNGData tMNGData) {
		Reporter.log("Test Case :  Add multiple type of devices to cart");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones link | Cell Phones page should be displayed");
		Reporter.log("3. Select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart | Cart page should be displayed");

		Reporter.log("5. Click on 'Add a phone' from cart | Cell Phones page should be displayed");
		Reporter.log("6. Select any device | PDP page should be displayed");
		Reporter.log("7. Click on Add to Cart | Cart page should be displayed");

		Reporter.log("8. Click on 'Add a phone' from cart | Cell Phones page should be displayed");
		Reporter.log("9. Select any device | PDP page should be displayed");
		Reporter.log("10. Select 'Want to pay in full?' link | FRP price should be displayed");
		Reporter.log("11. Click on Add to Cart | Cart page should be displayed");

		Reporter.log("12. Click on 'Add a tablet' from cart | Tablets PLP page should be displayed");
		Reporter.log("13. Select any tablet device | PDP page should be displayed");
		Reporter.log("14. Click on Add to Cart | Cart page should be displayed");

		Reporter.log("15. Click on 'Add a wearable' from cart | Wearable PLP page should be displayed");
		Reporter.log("16. Select any wearable device | PDP page should be displayed");
		Reporter.log("17. Click on Add to Cart | Cart page should be displayed");

		Reporter.log("18. Click on 'Add an accessory' from cart | Accessory PLP page should be displayed");
		Reporter.log("19. Select any accessory device | PDP page should be displayed");
		Reporter.log("20. Click on Add to Cart | Cart page should be displayed");

		Reporter.log(
				"21. Click 'See More' link on Add ons Extras section for First line  | Enter Your ZIP Code model should be displayed");
		Reporter.log("22. Enter valid ZIP Code and click on Next button | Select services should be displayed");
		Reporter.log(
				"23. Select any Recommended services and Click on Update button | Select services should be displayed in cartPage");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

//		Device #1 EIP
		PlpPage plpPage = navigateToPhonesPlpPage(tMNGData);
//		plpPage.clickDevice(tMNGData.getDeviceName());
		plpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.selectMemoryOption(tMNGData.getDeviceMemoryOption());
		phonesPdpPage.selectColorOption(tMNGData.getDeviceColorOption());
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
//		Adding Protection SOC
		cartPage.clickProtectionInAddOnsAndExtras();
		cartPage.enterYourZIPCode(tMNGData.getZipcode());
		cartPage.clickOnNextBtn();

//		Device #2 EIP
		cartPage.clickOnAddAPhoneLinkOnCart();
		plpPage.verifyPhonesPlpPageLoaded();
//		plpPage.clickDevice(tMNGData.getSecondDeviceName());
		plpPage.clickDeviceWithAvailability(tMNGData.getSecondDeviceName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.selectMemoryOption(tMNGData.getSecondDeviceMemoryOption());
		phonesPdpPage.selectColorOption(tMNGData.getSecondDeviceColorOption());
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

//		Device #3 FRP
		cartPage.clickOnAddAPhoneLinkOnCart();
		plpPage.verifyPhonesPlpPageLoaded();
//		plpPage.clickDevice(tMNGData.getThirdDeviceName());		
		plpPage.clickDeviceWithAvailability(tMNGData.getThirdDeviceName());
		pdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.selectMemoryOption(tMNGData.getThirdDeviceMemoryOption());
		phonesPdpPage.selectColorOption(tMNGData.getThirdDeviceColorOption());
		pdpPage.clickPayInFullPaymentOption();
		pdpPage.verifyFRPPriceAfterClickingOnPayInFull();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

//		Tablet EIP
		cartPage.clickOnAddATabletLinkOnCart();
		plpPage.verifyTabletsAndDevicesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		pdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		phonesPdpPage.selectMemoryOption(tMNGData.getTabletMemoryOption());
		phonesPdpPage.selectColorOption(tMNGData.getTabletColorOption());
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

//		Watch EIP
		cartPage.clickOnAddAWearableLinkOnCart();
		plpPage.verifyWatchesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getWatchName());
		pdpPage.verifyWatchesPdpPageLoaded();
		pdpPage.selectMemoryOption(tMNGData.getWatchMemoryOption());
		pdpPage.selectColorOption(tMNGData.getWatchColorOption());
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

//		Accessory
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		plpPage.verifyAccessoriesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		pdpPage.verifyAccessoriesPdpPageLoaded();
		pdpPage.selectColorOption(tMNGData.getAccessoryColorOption());
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

		cartPage.completeTPPInCartPage(tMNGData);

		cartPage.clickOnCartContinueBtn();

		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesSCC(tMNGData);
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());

	}

	/**
	 * US587091- TPP R2 - Checkout Suppress autopay and PIN on FRP accessory only
	 * cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testSuppressAutopayAndPINForAccessoryFRPOnly(TMNGData tMNGData) {
		Reporter.log(" US587091- TPP R2 - Checkout Suppress autopay and PIN on FRP accessory only cart");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO cart | TMO cart page should be launched");
		Reporter.log("2. Click on Phones  Link | Cell Phones PLP should be displayed");
		Reporter.log("3. Click on accessories | Accessories PLP page should be displayed");
		Reporter.log("4. Select any accessory| Accessories PDP page should be displayed");
		Reporter.log("5. Select Payment option as FRP | Payment option FRP is should be selected");
		Reporter.log("6. click on Add to cart CTA | Cart page should be displayed");
		Reporter.log("7. Click on continue CTA in cartpage | Checkout page should be displayed");
		Reporter.log("8. Verify personal info tab | Personal info tab should be displayed");
		Reporter.log("9. Fill the required fields and click next button | Verify tab should be displayed");
		Reporter.log(
				"10. Fill the required fields and click next button in verify tab | shipping and payment tab should be displayed ");
		Reporter.log(
				"11. Verify Autopay and PIN section are supressed | Autopay and PIN for FRP accessory should be Suppressed ");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToEmptyCart(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickOnAddAPhoneLinkOnCart();
		PlpPage plppage = new PlpPage(getDriver());
		plppage.verifyPhonesPlpPageLoaded();
		plppage.clickOnAccessoriesMenuLink();
		plppage.verifyAccessoriesPlpPageLoaded();
		plppage.clickOnSortDropdown();
		plppage.clickSortByPriceHighToLow();
		plppage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.clickPayInFullPaymentOption();
		phonesPdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillShippingAddress(tMNGData);
		checkOutPage.verifyAutoPaySectionNotDisplayed();
		checkOutPage.verifySecurityPinSectionNotDisplayed();

	}

	/**
	 * CDCDWM-301 : TC-682 : Enter in-valid address : Complete order TC-686 : Enter
	 * in-valid address : Existing user TC-684 : Enter in-valid address : Manual
	 * review TC-681 : Enter Valid address- complete order TC-685 : Enter Valid
	 * address- Existing user TC-683 : Enter Valid address- Manual Review
	 * CDCDWM-413 :CLONE - TPP R2 - Validate Customer Address on Checkout Credit Check Tab 2
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testInValidErrorMessageInCreditCheckTabAndPlaceTheOrder(TMNGData tMNGData) {
		Reporter.log(" CDCDWM-301, CDCDWM-413 :CLONE - TPP R2 - Validate Customer Address on Checkout Credit Check Tab 2");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones link | Cell Phones page should be displayed");
		Reporter.log("3. Select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart | Cart page should be displayed");
		Reporter.log("5. Click on Continue CTA | Check out page should be displayed");
		Reporter.log("6. Verify Personal Info section | Personal info section should be displayed");
		Reporter.log("7. Fill all personal information and click on next | Credit check tab should be displayed");
		Reporter.log(
				"8. Enter In-valid address, enter data for required fields and Click on 'Credit check' button | Credit check tab should be displayed");
		Reporter.log(
				"9. Verify Error message as 'Please check that the provided address is correct' | Error message should be displayed");
		Reporter.log(
				"10. Verfiy the fields 'Address Line 1',City, Address Line 2, State & 'zipcode' are highlighted in red with the previously entered address pre-populated | Address should be pre-populated");
		Reporter.log(
				"11. Enter valid address, enter data for required fields and Click on 'Credit check' button | Updated pricing model should be displayed");
		Reporter.log("12. Click on Check out now CTA | Shipping and Payments tab should be displayed");
		Reporter.log(
				"13. Fill Shipping and Payment Info fields and Click on Next button | Review Order tab should be displayed");
		Reporter.log("14. Click on Agree and submit button | Order confirmation page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		if (getTogglevalue("isValidateAddressAPI").equals("true")) {
			checkOutPage.verifyCheckOutPageLoaded();
			checkOutPage.fillPersonalInfo(tMNGData);
			checkOutPage.clickTermsNConditions();
			checkOutPage.clickNextBtn();
			checkOutPage.verifyCheckOutPageLoaded();

			checkOutPage.fillInvalidAddressforCreditCheck(tMNGData);
			checkOutPage.clickRequiredCheckBoxForCreditCheck();
			checkOutPage.clickRunCreditCheck();

			checkOutPage.verifyInvalidAddressMessage();
			checkOutPage.verifyHighlightedFieldsForInvalidAddress();

			checkOutPage.verifyAddressLine1PrePopulated();
			checkOutPage.verifyCityPrePopulated();
			checkOutPage.verifyZipcodePrePopulated();
			checkOutPage.verifyStatePrePopulated();

			checkOutPage.fillValidAddressForInvalidFields(tMNGData);
			checkOutPage.clickRunCreditCheck();
			checkOutPage.verifyOrderModalForCreditCheck();
			checkOutPage.clickCheckOutNowCTAOrderModal();
			checkOutPage.fillShippingAddress(tMNGData);
			checkOutPage.fillPaymentInfo(tMNGData);
			checkOutPage.setSecurityPin(tMNGData);
			checkOutPage.clickAgreeandNextBtn();
			checkOutPage.verifyReviewandSubmitPage();

			checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
			checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());
		} else {
			Reporter.log("'isValidateAddressAPI' toggle is false and address validation will not occure");
		}

	}

	/**
	 * CDCSM-153 - US634483 - TPP R2 - Display Error message on Prescreen Form if
	 * Address is invalid
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TPP })
	public void testVerifyPreScreenFormAddressValidation(TMNGData tMNGData) {
		Reporter.log("CDCSM-153 - US634483 - TPP R2 - Display Error message on Prescreen Form if Address is invalid");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("4. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"5. Click on 'Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("6. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log(
				"7. Pass invalid address on Address field | Error message should be displayed for Incorrect Address");
		Reporter.log(
				"9. Pass valid address on Address field | Error message should not be displayed for Address field");
		Reporter.log("10. Fill all manditory fields and click See your payments CTA | Offer page should be displayed");
		Reporter.log("11. Click Got It CTA on offer page | PLP Page should be displayed");
		Reporter.log("12. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("13. Click Add to Cart in PDP | Cart Page should  be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlp = navigateToPhonesPlpPage(tMNGData);
		phonesPlp.clickFindYourPricingLink();

		TPPPreScreenIntro tppIntro = new TPPPreScreenIntro(getDriver());
		tppIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppScreenForm = new TPPPreScreenForm(getDriver());
		tppScreenForm.verifyPreScreenForm();
		tppScreenForm.verifyPreScreenFormPageTitle();
//		invalid address 1
		tppScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppScreenForm.fillInvalidAddress("wrong zip code");
		tppScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppScreenForm.verifyErrorMessageForInvalidAddress();
//		invalid address 2
		tppScreenForm.fillInvalidAddress("wrong street");
		tppScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppScreenForm.verifyErrorMessageForInvalidAddress();
//		invalid address 3
		tppScreenForm.fillInvalidAddress("only house number");
		tppScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppScreenForm.verifyErrorMessageForInvalidAddress();
//		invalid address 4
		tppScreenForm.fillInvalidAddress("incomplete street address");
		tppScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppScreenForm.verifyErrorMessageForInvalidAddress();
//		invalid address 5
		tppScreenForm.fillInvalidAddress("wrong house number");
		tppScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppScreenForm.verifyErrorMessageForInvalidAddress();

		tppScreenForm.provideCorrectAddress(tMNGData);
		tppScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppScreenForm.verifyPreScreenloadingPage();
		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();
		tppOfferPage.clickGotItCTA();
		phonesPlp.verifyPhonesPlpPageLoaded();
		phonesPlp.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdp = new PdpPage(getDriver());
		phonesPdp.verifyPhonePdpPageLoaded();
		phonesPdp.clickOnAddToCartBtn();
		CartPage cartpage = new CartPage(getDriver());
		cartpage.verifyCartPageLoaded();

	}

}
