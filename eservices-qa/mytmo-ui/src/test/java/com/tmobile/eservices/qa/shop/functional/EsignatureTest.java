package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.ConsolidatedRatePlanPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.ESignaturePage;
import com.tmobile.eservices.qa.pages.shop.InterstitialTradeInPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.PLPPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class EsignatureTest extends ShopCommonLib {

	/**
	 * US321691 :TEST ONLY: MyTMO - Additional Terms - Standard Upgrade Flow -
	 * eSig: Offline Experience
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEsignOfflineExperienceForStandardUpgradeFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US321691 :TEST ONLY: MyTMO - Additional Terms - Standard Upgrade Flow - eSig: Offline Experience");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select EIP Payment | EIP payments is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log(
				"9. Select a line and Expand the Selected line | I should see remaining lines should not be Displayed");
		Reporter.log("10. Click No,skip trade-in' button | Insurance migration page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log(
				"12. Select  accessory devices and click Continue button | Accessory PDP page should be displayed");
		Reporter.log("13. Click add to cart button | Cart page should be displayed");
		Reporter.log(
				"14. Verify that Order details and click on Continue to shipping button | Shipping information should be displayed");
		Reporter.log("15. Click Continue to Payment button | Payment information form should be displayed");
		Reporter.log(
				"16.Fill the credit card details and Click Accept and Continue button | Equipment Installation Plan Agreement page for Offline Experience should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US321040
	 * 
	 * [Continued] TEST ONLY: MyTMO - Additional Terms - JUMP Upgrade Flow -
	 * eSig: REIP Experience
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEsignREIPExperienceForJumpUpgradeFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US321040 :TEST ONLY: MyTMO - Additional Terms - JUMP Upgrade Flow - eSig: REIP Experience");
		Reporter.log("Data Condition | Jump Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select EIP Payment | EIP payments is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log(
				"9. Select a line and Expand the Selected line | I should see remaining lines should not be Displayed");
		Reporter.log("10. Click No,skip trade-in' button | Insurance migration page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log(
				"12. Select  accessory devices and click Continue button | Accessory PDP page should be displayed");
		Reporter.log("13. Click add to cart button | Cart page should be displayed");
		Reporter.log(
				"14. Verify that Order details and click on Continue to shipping button | Shipping information should be displayed");
		Reporter.log("15. Click Continue to Payment button | Payment information form should be displayed");
		Reporter.log(
				"16. Fill the credit card details and Click Accept and Continue button | Equipment Installation Plan Agreement review page should be displayed");
		Reporter.log(
				"17. Verify user should have only 1 eSig interaction for transaction| user should have only 1 eSig interaction for transaction");
		Reporter.log(
				"18. Verify separate EIP document for each loan term length| separate EIP document for each loan term length should be displayed");
		Reporter.log(
				"19. Verify separate signature for each EIP document | separate signature for each EIP document  should be displayed");
		Reporter.log(
				"20. Sign in Buyer signature  and click on finish | Equipment Installation Plan Agreement page should be displayed");
		Reporter.log("21. Click on submit order button| Order is Completed page is displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US320719: [Continued] TEST ONLY: MyTMO - Additional Terms - Standard
	 * Upgrade Flow - eSig: REIP Experience
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEsignREIPExperienceForStandardUpgradeFlowForEnglishCustomers(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test US320719 :TEST ONLY: MyTMO - Additional Terms - Standard Upgrade Flow - eSig: REIP Experience");
		Reporter.log("Data Condition | STD PAH English Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select EIP Payment | EIP payments is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log(
				"9. Select a line and Expand the Selected line | I should see remaining lines should not be Displayed");
		Reporter.log("10. Click No,skip trade-in' button | Insurance migration page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log(
				"12. Select  accessory devices and click Continue button | Accessory PDP page should be displayed");
		Reporter.log("13. Click add to cart button | Cart page should be displayed");
		Reporter.log(
				"14. Verify that Order details and click on Continue to shipping button | Shipping information should be displayed");
		Reporter.log("15. Click Continue to Payment button | Payment information form should be displayed");
		Reporter.log(
				"16. Fill the credit card details and Click Accept and Continue button | Equipment Installation Plan Agreement review page should be displayed");
		Reporter.log(
				"17. Verify user should have only 1 eSig interaction for transaction| user should have only 1 eSig interaction for transaction");
		Reporter.log(
				"18. Verify separate EIP document for each loan term length| separate EIP document for each loan term length should be displayed");
		Reporter.log(
				"19. Verify separate signature for each EIP document | separate signature for each EIP document  should be displayed");
		Reporter.log(
				"20. Sign in Buyer signature  and click on finish| Equipment Installation Plan Agreement page should be displayed");
		Reporter.log("21. Click on submit order button | Complete order page is displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US321702 [Continued] TEST ONLY: MyTMO - Additional Terms - JUMP Upgrade
	 * Flow - eSig: Offline Experience
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.PENDING })
	public void testEsignOfflineExperienceForJumpUpgradeFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US321702 :TEST ONLY: MyTMO - Additional Terms - JUMP Upgrade Flow - eSig: Offline Experience");
		Reporter.log("Data Condition | Jump Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select EIP Payment | EIP payments is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log(
				"9. Select a line and Expand the Selected line | I should see remaining lines should not be Displayed");
		Reporter.log("10. Click No,skip trade-in' button | Insurance migration page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log(
				"12. Select  accessory devices and click Continue button | Accessory PDP page should be displayed");
		Reporter.log("13. Click add to cart button | Cart page should be displayed");
		Reporter.log(
				"14. Verify that Order details and click on Continue to shipping button | Shipping information should be displayed");
		Reporter.log("15. Click Continue to Payment button | Payment information form should be displayed");
		Reporter.log(
				"16.Fill the credit card details and Click Accept and Continue button | Equipment Installation Plan Agreement page for Offline Experience should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US321057# [Continued] TEST ONLY: MyTMO - Additional Terms - Standalone
	 * Accessories Flow - eSig: REIP Experience
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testREIPExperienceForStandardAccessoriesFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US321057: [Continued] TEST ONLY: MyTMO - Additional Terms - Standalone Accessories Flow - eSig: REIP Experience");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Accessory button | Accessory plp page should be displayed");
		Reporter.log("6. Select any Accessory device | Accessory pdp page should be displayed");
		Reporter.log("7. Click add to cart button | Review Cart page should be displayed");
		Reporter.log(
				"8. Verify that Order details and click on Continue button | Customer information should be displayed");
		Reporter.log("8. Click Continue to Pay and Reviwe button | Pay and Review section should be displayed");
		Reporter.log(
				"9. Fill the credit card details and Click Accept and Continue button | EIP signature page should be displayed");
		Reporter.log(
				"10. Click on SignIn and Submit button | Equipment Installation Plan Agreement page for REIP Experience should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

	}

	/**
	 * US429263 : myTMO AUC: POIP - PAH or FULL signs EIP Agreement on behalf of BRP
	 * US429264 : myTMO AUC: POIP - Ensure Billing Responsible Party name is passed
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEsignEIPAgreementOnBehalfOfBRP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US429263 : myTMO AUC: POIP - PAH or FULL signs EIP Agreement on behalf of BRP" +
						"US429264 : myTMO AUC: POIP - Ensure Billing Responsible Party name is passed");
		Reporter.log("Data Condition | PAH user performing a POIP from JOD");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select PaymentOption | PaymentOption should be selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select a line |  Line Selector Details page should be displayed");
		Reporter.log("9. Click skip trade in button | Device Protection page should be displayed");
		Reporter.log("10. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("11. Click OK button on Dont brake the bank modal | Accessory PLP page should be displayed");
		Reporter.log("12. Select Skip Accessories | Cart page should be displayed");
		Reporter.log("13. Verify that Order details and click on Continue to shipping button | Shipping information should be displayed");
		Reporter.log("14. Click Continue to Payment button | Payment information form should be displayed");
		Reporter.log(
				"15. Fill the credit card details and Click Accept and Continue button | Equipment Installation Plan Agreement page should be displayed");
		Reporter.log(
				"16.Verify Buyer's section | Buyer's name and stating that user is 'signing on behalf of the' Billing Responsible Party should be displayed");
		Reporter.log(
				"17. Verify first and last name of person requesting EIP | first and last name of person requesting EIP should be displayed");
		Reporter.log(
				"18. Verify Name field on the EIP Disclosure Agreement | Ensure that Name field on EIP disclosure Agreement should not be blank");
		Reporter.log(
				"19. Verify BRP Signature section on EIP Disclosure Agreement | Buyer signature 'Sign' and Behalf of BRP details should be displayed");
		Reporter.log(
				"20. Click on 'Sign' and select the signature on EIP Disclosure Agreement | Buyer signature should be reflected above BRP and ensure that BRP contains First name & Last Name");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US455070 :[Continued] [TEST ONLY] AAL - Integrate EIP experience into AAL flow
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testUserNavigatedToEipAgreementPageThroughAALflow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US455070 :[TEST ONLY] AAL - Integrate EIP experience into AAL flow");
		Reporter.log("Data Condition | PAH user With AAL eligibility");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click Contune button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Select PaymentOption as EIP | EIP PaymentOption should be selected");
		Reporter.log("10. Click Contune button | Interstitial page should be displayed ");
		Reporter.log("11. Verify Skip trade-in tile | Skip trade-in tile should be displayed ");
		Reporter.log("12. Select on 'No thanks, skip trade-in' | PHP page should be displayed");
		Reporter.log("13. Click Continue button | Accessories list should be displayed");
		Reporter.log("14. Click Skip accessories button | Cart Page should be displayed");
		Reporter.log("15. Verify that Order details and click on Continue to shipping button | Shipping information should be displayed");
		Reporter.log("16. Click Continue to Payment button | Payment information form should be displayed");
		Reporter.log("17. Fill the credit card details and Click Accept and Continue button | Equipment Installation Plan Agreement page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		ConsolidatedRatePlanPage ratePlanpage = navigateToRatePlanPageFromAALBuyNewPhoneFlow(myTmoData);
		ratePlanpage.verifyConsolidatedRatePlanPage();
		ratePlanpage.clickOnContinueCTA();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.verifyPageUrl();
		plpPage.selectManufactures();
		plpPage.verifyPageLoaded();
		plpPage.verifyPageUrl();
		plpPage.clickDeviceByName(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickOnContinueAALButton();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
			
		ESignaturePage eSignaturePage =  new ESignaturePage(getDriver());
		eSignaturePage.verifyESignaturePage();
	}

}
