package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.PoipDocuSign;

import io.restassured.response.Response;

public class PoipDocuSignTest extends PoipDocuSign {
	public Map<String, String> tokenMap;
	/**
	/**
	 * UserStory# Description:US488096:Implementation of Docusign Flow
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PoipDocuSignTest","testPoipDocuSignResponse",Group.PAYMENTS,Group.SPRINT  })
	public void testPoipDocuSignResponse(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testPoipDocuSignResponse");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with docusign endpoint.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testPoipDocuSignResponse";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("PoipDocuSign.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = PoipDocuSignBuilder(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				
				Assert.assertNotNull(jsonNode.get("documentsResponse").get("documentDetails").get(2).get("documentType"),
						"Document Type is Not presented");
				Assert.assertNotNull(jsonNode.get("documentsResponse").get("documentDetails").get(2).get("documentId"),
						"Document Id  is Not presented");
				Assert.assertNotNull(jsonNode.get("documentsResponse").get("documentDetails").get(2).get("agreementId"),
						"Agreement Id is Not presented");
				Assert.assertNotNull(jsonNode.get("documentsResponse").get("envelopeId"),
						"Enevelop Id is Not presented");
				Assert.assertNotNull(jsonNode.get("documentsResponse").get("documentUrl"),
						"Document Url Not presented");
				
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	
}