package com.tmobile.eservices.qa.global.api;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.LazerSharkApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class LazerSharkApiTest1 extends LazerSharkApi {
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "LS","testIsAddressExisting",Group.GLOBAL,Group.SPRINT  })
	public void testIsAddressExisting(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: IsAddressExisting test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response IsAddressExisting details.");
		Reporter.log("Step 2: Verify IsAddressExisting details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile("LazerSharkAddress.txt");
		System.out.println("requestBody = "+requestBody);
		String operationName="testIsAddressExisting";
		Response response =isAddressExist(apiTestData, requestBody);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String exist= getPathVal(jsonNode, "isExistingAddress");
				Reporter.log("response"+exist);
				Assert.assertEquals(exist,"true");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
}

