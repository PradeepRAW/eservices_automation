package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.PaymentManagerApiV1 ;

import io.restassured.response.Response;


public class PaymentManagerApiV1Test extends PaymentManagerApiV1 {
	public Map<String, String> tokenMap;
	/**
	 *PaymentManagerApiV1
	 * 
	 * @param 
	 * @param
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentManagerApiV1","testSchedulePaymentDetails",Group.PAYMENTS,Group.APIREG })
	public void testSchedulePaymentDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: test testSchedulePaymentDetails API");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: SchedulePaymentDetails should return PaymentDetails in service response");
		Reporter.log("Step 2: Verify expected SchedulePaymentDetails  are returned in response");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="SchedulePaymentDetails";
		Response response =schedulePaymentDetails(apiTestData);
		System.out.println("response=  "+response);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode= getPathVal(jsonNode, "statusCode");
				String paymentIndicator=getPathVal(jsonNode, "paymentIndicator");
				Assert.assertEquals(statusCode,"100","statusCode Invalid");
				Assert.assertNotNull(paymentIndicator,"paymentIndicator not found");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	
	/**
	 * testSubcriberPaymentDetails in PaymentManagerApiV1
	 * 
	 * @param data:qlab03
	 * @param myTmoData:COD-16
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentManagerApiV1","testSubcriberPaymentDetails",Group.PAYMENTS,Group.APIREG })
	public void testSubcriberPaymentDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: test testSubcriberPaymentDetails API");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: SubcriberPaymentDetails should return in service response");
		Reporter.log("Step 2: Verify expected SubcriberPaymentDetails   are returned in response");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="SubcriberPaymentDetails";
		Response response =SubcriberPaymentDetails(apiTestData);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode= getPathVal(jsonNode, "statusCode");
				String subscriberDetails=getPathVal(jsonNode, "subscriberDetails");
				Assert.assertEquals(statusCode,"100","statusCode Invalid");
				Assert.assertNotNull(subscriberDetails,"subscriberDetails not found");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	
	/**
	 * 
	 * PaymentarrangementDetails
	 * @param data:qlab03
	 * @param myTmoData:COD-16
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentManagerApiV1","testPaymentarrangementDetails",Group.PAYMENTS,Group.APIREG })
	public void testPaymentarrangementDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: test testPaymentarrangementDetails API");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: PaymentarrangementDetails API should return in paymentArrangementEligibility  in service response");
		Reporter.log("Step 2: Verify expected PaymentarrangementDetails   are returned in response");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="PaymentarrangementDetails";
		Response response =PaymentarrangementDetails(apiTestData);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertNotNull(getPathVal(jsonNode, "isPAEligible"),"isPAEligible not found");
				Assert.assertNotNull(getPathVal(jsonNode, "isPAalreadySetUp"),"isPAalreadySetUp not found");
				Assert.assertNotNull(getPathVal(jsonNode, "paymentArrangementEligibilityType"),"paymentArrangementEligibilityType not found");			
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	
	/**
	 * 
	 * autopayLandingDetails
	 * @param data:stage2
	 * @param myTmoData:COD-20
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentManagerApiV1","autopayLandingDetails",Group.PAYMENTS,Group.SPRINT })
	public void testAutopayLandingDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: test autopayLandingDetails API");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: autopayLandingDetails API should return in autopayLandingDetails  in service response");
		Reporter.log("Step 2: Verify expected autopayLandingDetails   are returned in response");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		String operationName="PaymentManager-autopayLandingDetails";
		Response response =autopayLandingDetails(apiTestData,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertNotNull(getPathVal(jsonNode, "easyPayInfo.isEligibleForAutoPay"),"isEligibleForAutoPay not found");
				Assert.assertNotNull(getPathVal(jsonNode, "easyPayInfo.isAutoPayEnrrolled"),"isAutoPayEnrrolled not found");
				Assert.assertNotNull(getPathVal(jsonNode, "subscriberInfo.accountNo"),"accountNo not found");	
				Assert.assertNotNull(getPathVal(jsonNode, "subscriberInfo.creditClass"),"creditClass not found");	
				Assert.assertNotNull(getPathVal(jsonNode, "profileInfo.msisdn"),"msisdn not found");	
				Assert.assertNotNull(getPathVal(jsonNode, "profileInfo.ban"),"ban not found");	
				Assert.assertNotNull(getPathVal(jsonNode, "discountInfo.isAutoPayDiscountAvailable"),"isAutoPayDiscountAvailable not found");	
				Assert.assertNotNull(getPathVal(jsonNode, "discountInfo.autoPayCreditAmount"),"autoPayCreditAmount not found");	
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
}
