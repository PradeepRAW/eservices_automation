/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.BillingSummaryPage;
import com.tmobile.eservices.qa.pages.payments.EIPDetailsPage;
import com.tmobile.eservices.qa.pages.payments.EipJodPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class EIPDetailsPageTest extends PaymentCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(EIPDetailsPageTest.class);

	/**
	 * P1_TC_Regression_Desktop: Copy the EIP component on the desktop bill
	 * summary page to the desktop bill details page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"merged"})
	public void verifyEIPBillSummaryDetails(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
		Reporter.log("Billing - Verify EIP Bill Summary Details");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("5. Verify EIP section | EIP section deatils should display");
		Reporter.log("6. Verify View details section | View details section should be displayed");
		Reporter.log("7. Click on View details | EIP details page should be displayed");
		Reporter.log("8. Verify documents and receipts | documents and receipts should be displayed");
		Reporter.log("9. Click on Account activities | Alerts and activities should be displayed");
		Reporter.log("================================");

		EIPDetailsPage eIPDetailsPage = navigateToEIPDetailsPage(myTmoData);
		eIPDetailsPage.verifyDocumentsAndReceipts();
		eIPDetailsPage.verifyAccountActivities();
		eIPDetailsPage.clickAccountActivities();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
	}

	/**
	 * Verify EQUIPMENT INSTALLMENT PLAN Estimator is functional
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"merged"})
	public void verifyEIPEstimatorPlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPBillSummaryDetails method called in BillingSummaryTest");
		Reporter.log("Test Case : Verify EQUIPMENT INSTALLMENT PLAN Estimator is functional");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("5. Verify EIP section | EIP section deatils should display");
		Reporter.log("6. Verify View details section | View details section should be displayed");
		Reporter.log("7. Click on View details | EIP details page should be displayed");
		Reporter.log("8. Click EIP Estimator | EIP Estimator should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		EIPDetailsPage eIPDetailsPage = navigateToEIPDetailsPage(myTmoData);
		eIPDetailsPage.clickEIPestimator();
		eIPDetailsPage.verifyEIPEstimatordialogBox();
	}

	

	/**
	 * Verify a stored payment method can be removed via the EIP payment flow
	 * 
	 * @param data
	 * @param myTmoData
	 * Test data not available: require an EIP MISSDN with stored payment type
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING})
	public void verifyEIPstoredPaymentremove(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPstoredPaymentremove method called in BillingSummaryTest");
		Reporter.log("Test Case : Verify a stored payment method can be removed via the EIP payment flow");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("5. Verify EIP section | EIP section deatils should display");
		Reporter.log("6. Verify View details section | View details section should be displayed");
		Reporter.log("7. Click on View details | EIP details page should be displayed");
		Reporter.log("8. Click Make Payment | Extra device payment should be displayed");
		Reporter.log("9. Click Remove link on Stored Credit card | Confirmation dialog box should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");


		EIPDetailsPage eIPDetailsPage = navigateToEIPDetailsPage(myTmoData);
		eIPDetailsPage.clickMakepayment();
		eIPDetailsPage.verifyEIPDevicePaymentPage();
		eIPDetailsPage.verifyRemoveStoredPaymentLink();
		eIPDetailsPage.verifyRemoveStoredPaymentModal();
	}

	// US186136 Payment Hub (Mobile) - Equipment Installment Plan
	// US85418 Payment Hub (Desktop) - Installment Plan blade
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.RETIRED})
	public void verifyPaymentHubActiveEIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log("Step 4: verify Active EIP details | Active EIP details   should be dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		navigateToBillingPage(myTmoData);
		if (getDriver().getCurrentUrl().contains("/billandpay")) {
			BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
			billAndPaySummaryPage.verifyPageLoaded();
			billAndPaySummaryPage.verifyEquipmentInstallmentPlansShortcut();
			billAndPaySummaryPage.clickEquipmentInstallmentPlansShortcut();
		
			
		}else {//if (getDriver().getCurrentUrl().contains("//bill")){
	
			BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
			billingSummaryPage.verifyPageLoaded();
			billingSummaryPage.verifyEipText();
			billingSummaryPage.verifyEipSubAlertText();
			billingSummaryPage.clickInstallemntArrow();
			
			
		}
		EipJodPage eipJodPage = new EipJodPage(getDriver());
		eipJodPage.verifyPageLoaded();
		
	}

	/**
	 * 4042478367 / Test12345 16_MY TMO_IR ML_Customer with Account level EIP
	 * plan_Bill Details - New Bill - EIP Block (Desktop)
	 */
	@Test(dataProvider = "byColumnName",enabled=true,groups = {Group.RETIRED})
	public void testVerifyAllElementsinEIPDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testVerifyAllElementsinEIPDetailsPage method called in EIPDetailsPageTest");
		Reporter.log(
				"Test Case : MY TMO_IR ML_Customer with Account level EIP plan_Bill Details - New Bill - EIP Block (Desktop)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on View bill link | Billing summary page should be displayed");
		Reporter.log("5. Click on Installment ViewDetails link | eipdetails page should be displayed");
		Reporter.log("6. Verify InstallmentPlan Type | InstallmentPlan Type should be EIP");
		Reporter.log("7. Verify EIP PlanId | EIP PlanId should be displayed");
		Reporter.log("8. Verify Plan Status | Plan Status should be Active");
		Reporter.log("9. Verify Plan Balance Amount | Plan Balance Amount should be displayed");
		Reporter.log("10. Verify Completed Plans | Completed plan should be displayed");
		Reporter.log("11. Verify documents and receipts | documents and receipts should be displayed");
		Reporter.log("12. Click on Account activities | Alerts and activities should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		EIPDetailsPage eipDetailsPage = navigateToEIPDetailsPage(myTmoData);
		eipDetailsPage.verifyEIPplanId();
		eipDetailsPage.verifyPlanStatusText("Active");
		eipDetailsPage.verifyPlanBalanceAmount();
	/*	if (getDriver() instanceof AppiumDriver) {
			eipDetailsPage.clickViewCompletedPlansMobile();
		}*/
		eipDetailsPage.verifyCompletedplandDisplayed();
		eipDetailsPage.verifyDocumentsAndReceipts();
		eipDetailsPage.verifyAccountActivities();
		eipDetailsPage.clickAccountActivities();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
	}
	/**
	 * Verify Completed EQUIPMENT INSTALLMENT PLANS are displayed
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"merged"})
	public void verifyEIPCompletedplans(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEIPCompletedplans method called in BillingSummaryTest");
		Reporter.log("Test Case : Verify Completed EQUIPMENT INSTALLMENT PLANS are displayed");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log("5. Verify EIP section | EIP section deatils should display");
		Reporter.log("6. Verify View details section | View details section should be displayed");
		Reporter.log("7. Click on View details | EIP details page should be displayed");
		Reporter.log("8. Verify Completed Plans | Completed plan should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		EIPDetailsPage eIPDetailsPage = navigateToEIPDetailsPage(myTmoData);
		if (getDriver() instanceof AppiumDriver) {
			eIPDetailsPage.clickViewCompletedPlansMobile();
		}
		eIPDetailsPage.verifyCompletedplandDisplayed();
	}
	
	/**
	 * US230488	DE84683 - MyTMO [Mobile] – Payments – EIP estimated amounts are not carried over to EIP payment page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.RETIRED})
	public void verifyEstimatedAmountInEipPaymentsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEstimatedAmountInEipPaymentsPage method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : US230488	DE84683 verifyEstimatedAmountInEipPaymentsPage");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing Link | Bill Summary page should be displayed");
		Reporter.log(
				"5.verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("6.Verify EIP balance | Eip balance should be displayed Corectly");
		Reporter.log("7.Click on view details | EIP details page should be displayed");
		Reporter.log("8.Click on Account Activities | Account history section should be displayed");

		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		EIPDetailsPage eIPDetailsPage = navigateToEIPDetailsPage(myTmoData);
		eIPDetailsPage.clickEIPestimator();
		eIPDetailsPage.verifyEIPEstimatordialogBox();
		Double amount = eIPDetailsPage.enterEIPPaymentamount();
		eIPDetailsPage.clickEstimateBtn();
		eIPDetailsPage.clickPayThisAmountLink();
		eIPDetailsPage.verifyEIPDevicePaymentPage();
		eIPDetailsPage.verifyEIPPaymentAmount(amount);
		
		
	}
	
	/**
	 * US300284 - Additional Terms : Desktop/Mobile - Update the EIP Details page to
	 * support EIP Plan numbers of up to 18 characters
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testEIPPlanNumber(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US300284 - Additional Terms : Desktop/Mobile - Update the EIP Details page support EIP Plan numbers of up to 18 characters");
		Reporter.log("Data Condition - EIP account");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing Link | Bill Summary page should be displayed");
		Reporter.log(
				"5.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("6. Click on Installment Plan blade | EIP details page should be displayed");
		Reporter.log("7. Verufy EIP Plan ID number | Number should contain 14 or 18 chars");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		EIPDetailsPage eIPDetailsPage = navigateToEIPDetailsPage(myTmoData);
		eIPDetailsPage.verifyEipPlanIDNumber();
	}
	
	
	/***************************
	 * TestName:testMalbecpromodetails
	 * US376221:(Copy of) PT# 19999649 - EIP details page is not displaying active Malbec promo info
	 * Precondition:Customer should have Equipment with Promotion and test is implemented for single device
	 ********************************/
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testMalbecpromodetails(ControlTestData data, MyTmoData myTmoData) {
	Reporter.log("Test Case : US376221:(Copy of) PT# 19999649 - EIP details page is not displaying active Malbec promo info");
	Reporter.log("Data Condition - EIP account");
	Reporter.log("================================");
	Reporter.log("1. Launch the application | Application Should be Launched");
	Reporter.log("2. Login to the application | User Should be login successfully");
	Reporter.log("3. Verify Home page | Home page should be displayed");
	Reporter.log("4. Click on Billing Link | Bill Summary page should be displayed");
	Reporter.log(
			"5.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
	Reporter.log("6. Click on Installment Plan blade | EIP details page should be displayed");
	Reporter.log("7. Verify Promotion,MONTHLY CREDITS REMAINING,AMOUNT OF MONTHLY CREDIT lables | Lables should be displayed");
	Reporter.log("================================");
	Reporter.log("Actual Result:");
	
	EIPDetailsPage eIPDetailsPage = navigateToEIPDetailsPage(myTmoData);
	String devid="0";
	eIPDetailsPage.checkpromotionlabel(devid);
	eIPDetailsPage.checkmonthlyCreditsRemaininglabel(devid);
	eIPDetailsPage.checkamountOfMonthlyCreditsRemainingLabel(devid);
	}
}
