/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.BillDetailsPage;
import com.tmobile.eservices.qa.pages.payments.BillingSummaryPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayLandingPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.UsagePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class BillingSummaryPageTest extends PaymentCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(BillingSummaryPageTest.class);
	private static final String PLATFORM_TYPE = "platform-type";

	/**
	 * P1_TC_Regression_Desktop_Bill Summary Bill Highlights validation Merged cases
	 * are:verifyEnsureUserCanDownloadPDFBill,
	 * verifyPaperLessLinkOnBillingSummaryPage,
	 * testVerifyPaymentHistoryActiveStat,verifyBillingSummaryColumnRenamed,
	 * billingSummaryTestbillHighlights
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing"})
	public void billingSummaryTestbillHighlights(ControlTestData data, MyTmoData myTmoData) throws Exception {
		logger.info("billingSummaryTestbillHighlights method called in BillingSummaryTest");
		Reporter.log("Test Case : Desktop_Bill Summary Bill Highlights validation");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on billing link | Billing summary page should be displayed");
		Reporter.log(
				"5. Verify billing highlight section verifyBillingCurrentChargesAndDownloadPDF| Billing highlight section should be displayed");
		Reporter.log("6. Verify table header updated as 'Change from Last Bill'| Table header should be updated");
		Reporter.log("7: verify payment History Active alert| payment History Active alert  should be displayed");
		Reporter.log("8.Verify Paper less billing section| Paper less billing section should be displayed");
		Reporter.log("9. Verify PDF Downloadable | PDF Downloadable with Right Information Should be Displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
	
		BillingSummaryPage billingSummaryPage = navigateToEbillpagethroughBBpage(myTmoData);
		//BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyBillingHighLightsSection();
		String platformType = getTestParameters().get(PLATFORM_TYPE).toString();
		if ("android".equals(platformType) || "iPhone".equals(platformType)) {
			// change from last month not appearing in mobile
		} else {
			billingSummaryPage.getTableHeaderChange("Change from last bill");
		}

		billingSummaryPage.verifyPaymentHistoryText();
		billingSummaryPage.checkPaperLessLink();
		billingSummaryPage.clickDownLoadPDF();
	}

	/**
	 * Ensure bill history graph and popouts reflect the correct information.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void verifyBillHistoryGraphReflectsCorrectInformation(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBillHistoryGraphReflectsCorrentInformation method called in BillingSummaryTest");
		Reporter.log("Test Case : Ensure bill history graph and popouts reflect the correct information.");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Billing Link | Billing Summary page should be displayed");
		Reporter.log("5. Click on view bill graph | Bill graph information should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		if (getDriver() instanceof AppiumDriver) {
			billingSummaryPage.verifyGraph();
		} else {
			billingSummaryPage.clickBillHistoryChart();
			billingSummaryPage.verifyGraph();
			billingSummaryPage.clickCloseGraph();
		}
	}

	/**
	 * Ensure user can change bill cycle in bill summary
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void verifyUserCanChangeBillCycle(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserCanChangeBillCycle method called in BillingSummaryTest");
		Reporter.log("Test Case : Ensure user can change bill cycle in bill summary");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on billing link | Billing summary page should be displayed");
		Reporter.log("5. Verify bill cycle dropdown | Bill cycle dropdown should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.changeBillCycle();
	}

	/**
	 * 23_MYTMO_PAH_SL on TE plan_Verify Column name renamed from Change from last
	 * month to Change from last bill
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void verifyBillingSummaryColumnRenamed(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBillingSummaryColumnRenamed");
		Reporter.log(
				"TestCase : MYTMO_PAH_SL on TE plan_Verify Column name renamed from Change from last month to Change from last bill");
		Reporter.log("Data Conditon | IR STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on billing link | Billing summary page should be displayed");
		Reporter.log("5. Verify table header updated as 'Change from Last Bill'| Table header should be updated");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		String platformType = getTestParameters().get(PLATFORM_TYPE).toString();
		if ("android".equals(platformType) || "iPhone".equals(platformType)) {
			// change from last month not appearing in mobile
		} else {
			billingSummaryPage.getTableHeaderChange("Change from last bill");
		}
	}

	/**
	 * Verify Bill summary - Credits & adjustments
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void verifyBillingsummaryCreditsAdjustments(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBillingsummaryCreditsAdjustments method called in BtoBCustomerTest");
		Reporter.log("Test Case : Verify Credits Adjustments in Billing Summary ");
		Reporter.log("Data Condition - Master Customer. EBill should be present");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on billing link | Billing summary page should be displayed");
		Reporter.log("5. Click on Credits and adjustments | Credits and Adjustments should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.checkPageIsReady();
		billingSummaryPage.verifyPageLoaded();
		billingSummaryPage.clickOnCreditsAndAdjustments();
		billingSummaryPage.verifyCreditsAndAdjustmentsHeading();
		billingSummaryPage.clickOnCreditsAdjustmentsClose();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testVerifyPAIneligibilityOnPaymentHub(ControlTestData data, MyTmoData myTmoData) throws Exception {
		Reporter.log("================================");
		Reporter.log("Data Condition | Standard User");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log("Step 4: verify pa ineligibility alert| pa ineligibility alert   should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		// BillingSummaryPage billingSummaryPage =
		// navigateToBillingSummaryPage(myTmoData);
		BillingSummaryPage billingSummaryPage = navigateTopreviousEbillfromBBAccount(myTmoData);
		billingSummaryPage.verifyPaymentArrangementText();
		billingSummaryPage.verifyPASubTitileForInEligibleUser();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testVerifyPaperlessBillingONState(ControlTestData data, MyTmoData myTmoData) throws Exception {
		Reporter.log("================================");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log(
				"Step 4: verify paper less billing ON messages | paper less billing ON  message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		// BillingSummaryPage billingSummaryPage =
		// navigateToBillingSummaryPage(myTmoData);
		BillingSummaryPage billingSummaryPage = navigateTopreviousEbillfromBBAccount(myTmoData);
		billingSummaryPage.verifypaperlessBillingTextDisplayed("Paperless Billing");
		billingSummaryPage.verifyPaperLessBillingStatusON();
	}

	/**
	 * Verify paper less billing for restricted user
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testVerifyPaperlessBillingForRestrictedUser(ControlTestData data, MyTmoData myTmoData)
			throws Exception {
		Reporter.log("Test Case : Verify paper less billing for restricted user");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on billing link | Billing summary page should be displayed");
		Reporter.log(
				"Step 4: Verify paper less billing Restricted user messages | paper less billing Restricted user message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		// BillingSummaryPage billingSummaryPage =
		// navigateToBillingSummaryPage(myTmoData);
		BillingSummaryPage billingSummaryPage = navigateTopreviousEbillfromBBAccount(myTmoData);
		billingSummaryPage.verifypaperlessBillingTextDisplayed("Paperless Billing");
		billingSummaryPage.verifyRestrictedUserAlertForPaperLessBilling();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testVerifyAutopayForRestrictedUser(ControlTestData data, MyTmoData myTmoData) throws Exception {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log(
				"Step 4:verify  autopay restricted user message |resticted user autopay message  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		// BillingSummaryPage billingSummaryPage =
		// navigateToBillingSummaryPage(myTmoData);
		BillingSummaryPage billingSummaryPage = navigateTopreviousEbillfromBBAccount(myTmoData);
		billingSummaryPage.verifyRestrictedUserAutopayMessage();
	}

	// US186138 Payment Hub (Mobile) - Payment History
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testVerifyPaymentHistoryActiveState(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log("Step 4: verify payment History Active alert| payment History Active alert  should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyPaymentHistoryText();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testVerifyAutopayOnPAHubWhenAutopayON(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log("Step 4: verify autopay on alert | autopay on alert   should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyAutopayAlertwhenON();
		// Subtitle: Payment scheduled for [DATE]
	}

	// US202105 Payment Hub (Desktop) - Download PDF component

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testPaymentHubDownLoadPDFComponent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log(
				"Step 4: verify  Download PDF component on billing summary page|  Download PDF component should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);

		if (getDriver() instanceof AppiumDriver) {
			billingSummaryPage.verifyDownLoadPDFMobile();
			billingSummaryPage.clickDownLoadPDF();
			billingSummaryPage.verifyChooseBillVersionHeaderMobile();
			billingSummaryPage.verifyDownLoadSummaryBill();
			billingSummaryPage.verifyDownLoadDetailedBill();
		} else {
			billingSummaryPage.verifyGetYourBillHeader("Get your Bill");
			billingSummaryPage.clickDownLoadSummaryBill();
			billingSummaryPage.clickDownLoadDetailedBill();
		}
		Reporter.log(" Download PDF component is verified");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testPaymentHubDownLoadPDFComponentForRestrictedUser(ControlTestData data, MyTmoData myTmoData)
			throws Exception {
		Reporter.log("================================");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log(
				"Step 4: verify  Download PDF component restricted user message|  restricted user message for Download PDF component should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		/*
		 * navigateToBillingPage(myTmoData); CommonPage commonPage = new
		 * CommonPage(getDriver()); commonPage.waitforSpinner();
		 * waitForPageLoad(getDriver()); commonPage.verifyAjaxSpinnerInvisible();
		 * BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
		 * billingSummaryPage.verifyPageLoaded();
		 */
		BillingSummaryPage billingSummaryPage = navigateTopreviousEbillfromBBAccount(myTmoData);
		billingSummaryPage.verifyBillingSummaryPage();
		if (getDriver() instanceof AppiumDriver) {
			billingSummaryPage.verifyDownLoadPDFMobile();
		} else {
			// billingSummaryPage.verifyGetYourBillHeader("Get your Bill");
			billingSummaryPage.verifyNoDownLoadPermissionFOrRestrictedUser(
					"You don't have sufficient permissions to download a copy of the bill. Please check with your primary account holder.");
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testPaymentHubJumpOnDemandComponent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log(
				"Step 4: verify jump on demand content on billing summary page| jump on demand content should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyjumponDemandComponentOnBillingPage();
		billingSummaryPage.verifyjumponDemandLeaseMessage();
		billingSummaryPage.clickviewleasedetails();
		billingSummaryPage.verifyjumponDemandLeasepage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testVerifyPaymentHubAutopayDueEstimation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log("Step 4: verify autopay due date alert | autopay due date alert   should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyDueDatePastHeader();
	}

	/**
	 * 4255891524 / 4049601986 / workL!f3balanceACT 38_MYTMO_PAH_SL on TI plan_Bill
	 * Summary_Verify Tax and fee cloumn when TE services are available on TI plan
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void verifySingleLineUserBillingTaxesFeesColHiddenFromCurrentChagesTable(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"Test::: Verify Single Line User Billing - Taxes And Fees Column Hidden From Current Chages Table.");
		Reporter.log(
				"Test Case : MYTMO_PAH_SL on TI plan_Bill Summary_Verify Tax and fee cloumn when TE services are available on TI plan");

		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application as a Single Line user | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on View bill link | Billing summary page should be displayed");
		Reporter.log(
				"5. Verify the Billing Page for Taxes and Fees text | Billing summary page should not display Taxes and Fees info for a Single Line user");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyTaxesandFeesSection(PaymentConstants.TAXES_FEES_TEXT);
	}

	/**
	 * TC016_Ebill_IR ML_To Verify_Current charges
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyCurrentChargesInPDF(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyCurrentChargesInPDF method called in DelugeTest");

		Reporter.log("Test Case : Verify Current Charges In PDF");

		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be displayed");
		Reporter.log("4.Click on Billing link |  Billing Summary page should be displayed");
		Reporter.log("5.Click on Download PDF | PDF should be downloaded");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		clickDownloadPdfOnBillingPage(myTmoData);
		/*
		 * boolean fileName = new PDFServiceHelper().validateContent(getFilePath(),
		 * billingSummaryPage.getCurrentCharges()); Reporter.log("Verify '"+
		 * billingSummaryPage.getCurrentCharges()+"'"); Assert.assertEquals(fileName,
		 * true); File file = new File(getFilePath());
		 * Assert.assertEquals(file.delete(), true);
		 */
	}

	/**
	 * TC019_Ebill_IR_ML_To Verify Bill Summary-Layout and Bill Selector
	 * (Desktop)_English
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void verifyPaperLessLinkOnBillingSummaryPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPaperLessLinkOnBillingSummaryPage method called in DelugeTest");

		Reporter.log("Test Case :Verify Paper Less Link On Billing Summary Page");

		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be displayed");
		Reporter.log("4.Click on Billing link | Billing summary page should be displayed");
		Reporter.log("5.Verify Paper less billing section| Paper less billing section should be displayed");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.checkPaperLessLink();
	}

	/**
	 * TC020_Ebill_IR_SL_New Bill - Itemized Charges_Verify _Sub-sections_Taxes and
	 * other charges
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifySubSectionsTaxesAndOtherChargesOnPdf(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifySubSectionsTaxesAndOtherChargesOnPdf method called in DelugeTest");

		Reporter.log("Test Case Name:Verify Sub Sections Taxes And Other Charges On Pdf");

		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be displayed");
		Reporter.log("4.Click on Billing link |  Billing Summary page should be displayed");
		Reporter.log("5.Click on Download PDF |  pdf should be downloaded");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		clickDownloadPdfOnBillingPage(myTmoData);
		// String home = System.getProperty("user.home");
		/*
		 * boolean fileName = new PDFServiceHelper().validateContent(home +
		 * Constants.PDFFILE_DOWNLOAD_PATH, billingSummaryPage.getCurrentCharges());
		 * Reporter.log("Verify '"+ billingSummaryPage.getCurrentCharges()+"'");
		 * Assert.assertEquals(fileName, true);
		 */
		/*
		 * fileName = new PDFServiceHelper().validateContent(home +
		 * Constants.PDFFILE_DOWNLOAD_PATH, billingSummaryPage.getSummaryBalance());
		 * Reporter.log("Verify " +billingSummaryPage.getSummaryBalance()+"");
		 */
		/*
		 * billingSummaryPage.clickCurrentCharges(myTmoData.getLoginEmailOrPhone ());
		 * BillDetailsPage billDetailsPage = new BillDetailsPage(getDriver());
		 * Assert.assertEquals(billDetailsPage.verifyPageLoaded(), true); boolean
		 * fileName = new PDFServiceHelper().validateContent(home +
		 * Constants.PDFFILE_DOWNLOAD_PATH, billDetailsPage.getTotalAmount());
		 * Assert.assertEquals(fileName, true); Reporter.log(Constants.VERIFY
		 * +billDetailsPage.getTotalAmount()+""); Assert.assertEquals(fileName, true);
		 * Reporter.log(Constants.VERIFY_BILLING_PAGE); Assert.assertEquals(new
		 * PDFServiceHelper().validateContent(home + Constants.PDFFILE_DOWNLOAD_PATH,
		 * billDetailsPage.getServiceFund()), true);
		 * Reporter.log("Verify '"+billDetailsPage.getServiceFund()+"'");
		 * Assert.assertEquals(new PDFServiceHelper().validateContent(home +
		 * Constants.PDFFILE_DOWNLOAD_PATH,
		 * billDetailsPage.getRegulatoryProgramsTelcoRecoveryFee()), true);
		 * Reporter.log("Verify '"
		 * +billDetailsPage.getRegulatoryProgramsTelcoRecoveryFee()+"'");
		 * Assert.assertEquals(new PDFServiceHelper().validateContent(home +
		 * Constants.PDFFILE_DOWNLOAD_PATH, billDetailsPage.getStateAndLocalSalesTax()),
		 * true); Reporter.log(
		 * "Verify '"+billDetailsPage.getStateAndLocalSalesTax()+"'");
		 * Assert.assertEquals(new PDFServiceHelper().validateContent(home +
		 * Constants.PDFFILE_DOWNLOAD_PATH, billDetailsPage.getState911()), true);
		 * Reporter.log("Verify '"+billDetailsPage.getState911()+"'");
		 */
		/*
		 * Assert.assertEquals(new PDFServiceHelper().validateContent(home +
		 * Constants.PDFFILE_DOWNLOAD_PATH, billDetailsPage.getCounty911()), true);
		 * Reporter.log("Verify '"+billDetailsPage.getCounty911()+"'");
		 */
		/*
		 * File file = new File(home + Constants.PDFFILE_DOWNLOAD_PATH);
		 * Assert.assertEquals(file.delete(), true);
		 */
	}

	/**
	 * TC021_Ebill_IR_To Verify the Account level Charges_ PDF_English
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyTheAccountLevelChargesPDFEnglish(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyTheAccountLevelChargesPDFEnglish method called in DelugeTest");

		Reporter.log("Test Case Name:Verify Account Level Charges PDF English");

		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be displayed");
		Reporter.log("4.Click on Billing link |  Billing Summary page should be displayed");
		Reporter.log("5.Click on Download PDF |  PDF should be downloaded");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		clickDownloadPdfOnBillingPage(myTmoData);
		/*
		 * billingSummaryPage.clickCurrentCharges(myTmoData.getLoginEmailOrPhone ());
		 * Reporter.log(Constants.CLICK_ON +myTmoData.getLoginEmailOrPhone()+"");
		 * BillDetailsPage billDetailsPage = new BillDetailsPage(getDriver()); String
		 * home = System.getProperty("user.home"); boolean fileName = new
		 * PDFServiceHelper().validateContent(home + Constants.PDFFILE_DOWNLOAD_PATH,
		 * billDetailsPage.getTotalAmount()); Assert.assertEquals(fileName, true);
		 * Reporter.log(Constants.VERIFY+billDetailsPage.getTotalAmount()+""); File file
		 * = new File(home + Constants.PDFFILE_DOWNLOAD_PATH);
		 * Assert.assertEquals(file.delete(), true);
		 */
	}

	/**
	 * TC018_Ebill_IR_ML_Bill Summary - Current Charges_Print Bill_PAH customer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyebillIRMLBillSummaryCurrentChargesInPrintBillPAHcustomer(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("verifyebillIRMLBillSummaryCurrentChargesInPrintBillPAHcustomer method called in DelugeTest");

		Reporter.log("Test Case Name: Verify ebill IR/PAH ML Bill Summary Current Charges Print Bill");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1.Launch application | Application should be launched");
		Reporter.log("2.Login to the application  | User should be able to login Successfully");
		Reporter.log("3.Verify Home page | Home page should be displayed");
		Reporter.log("4.Click on Billing link |  Billing Summary page should be displayed");
		Reporter.log("5.Click on Download PDF |  Verify Summary Balance From Application With Pdf file");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		clickDownloadPdfOnBillingPage(myTmoData);
		/*
		 * String home = System.getProperty("user.home"); boolean fileName = new
		 * PDFServiceHelper().validateContent(home + Constants.PDFFILE_DOWNLOAD_PATH,
		 * billingSummaryPage.getSummaryBalance()); Reporter.log("Verify "
		 * +billingSummaryPage.getSummaryBalance()+""); Assert.assertEquals(fileName,
		 * true); File file = new File(home + Constants.PDFFILE_DOWNLOAD_PATH);
		 * Assert.assertEquals(file.delete(), true);
		 */
	}

	/**
	 * 32_MYTMO_PAH_ML on TI plan_Verify display of recurring charges and other
	 * charges under Current charges in Bill Summary Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void verifyRecurringChargesAndOtherChargesInBillSummaryPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyRecurringChargesAndOtherChargesInBillSummaryPage method called in NewEBillTest");
		Reporter.log(
				"Test Case : MYTMO_PAH_ML on TI plan_Verify display of recurring charges and other charges under Current charges in Bill Summary Page");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Click Billing link | Billing Page Should be displayed");
		Reporter.log(
				"5. Verify CurrentCharges Section in the BalanceForward does not have a price | CurrentCharges Section in the BalanceForward does not have any price displayed in front of it.");
		Reporter.log(
				"6. Verify Under CurrentCharges Section title show's as Recurring Charges and it Show's Recurring charges Amount | Under CurrentCharges Section title show's as Recurring Charges and it Show's Recurring Charges Amount is Displayed.");
		Reporter.log(
				"7. Verify Under CurrentCharges Section title show's as Misc Charges and it Show's Misc charges Amount | Under CurrentCharges Section title show's as Misc Charges and it Show's Misc charges Amount is Displayed.");
		Reporter.log("===========================================");

		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.checkPageIsReady();
		billingSummaryPage.verifyCurrentCharges();
		billingSummaryPage.verifyRecurringCharges();
		billingSummaryPage.verifyMiscCharges();
	}

	/**
	 * 30_MYTMO_PAH_SL on TI plan_Verify the PDF is downloadable and presents right
	 * information when migrated from TE to TI plan
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void verifyPDFDownloadedAndPresentRightInformation(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPDFDownloadedAndPresentRightInformation method called in NewEBillTest");
		Reporter.log(
				"Test Case : MYTMO_PAH_SL on TI plan_Verify the PDF is downloadable and presents right information when migrated from TE  to TI plan");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Click Billing link | Billing Page Should be displayed");
		Reporter.log("5. Verify PDF Downloadable | PDF Downloadable with Right Information Should be Displayed");
		Reporter.log("===========================================");

		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.clickDownLoadPDF();
	}

	/**
	 * 22_MYTMO_PAH_ML on TI plan_Verify Current charges section in the balance
	 * forward does not have a price in front of it 4253244019 / Test12345
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DUPLICATE })
	public void verifyCurrentChargesSectionBalanceForwardDoesNotHaveAPriceInFrontOfIt(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyCurrentChargesSectionBalanceForwardDoesNotHaveAPriceInFrontOfIt method called in NewEBillTest");
		Reporter.log(
				"Test Case : MYTMO_PAH_ML on TI plan_Verify Current charges section in the balance forward does not have a price in front of it");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Click Billing link | Billing Page Should be displayed");
		Reporter.log(
				"5. Verify CurrentCharges Section in the BalanceForward does not have a price | CurrentCharges Section in the BalanceForward does not have any price displayed in front of it.");
		Reporter.log(
				"6. Verify Under CurrentCharges Section title show's as Recurring Charges and it Show's Recurring charges Amount | Under CurrentCharges Section title show's as Recurring Charges and it Show's Recurring Charges Amount is Displayed.");
		Reporter.log(
				"7. Verify Under CurrentCharges Section title show's as Misc Charges and it Show's Misc charges Amount | Under CurrentCharges Section title show's as Misc Charges and it Show's Misc charges Amount is Displayed.");

		Reporter.log("===========================================");

		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.checkPageIsReady();
		verifyPage("Billing page", "Bill Summary");
		billingSummaryPage.verifyCurrentCharges();
		billingSummaryPage.verifyRecurringCharges();
		billingSummaryPage.verifyMiscCharges();

	}

	/**
	 * Ensure user can view past bills.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void verifyUserCanViewPastBills(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserCanViewPastBills method called in BillingTest");
		Reporter.log("Test Case : Ensure user can view past bills");

		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on View bill link | Billing summary page should be displayed");
		Reporter.log("5. Verify User can View past bills| Billing summary page shows past bills");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.clickDatesDropDown();
		billingSummaryPage.verifyBillingCyclePastBills();
	}

	/**
	 * Ensure user can download a bill PDF.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void verifyEnsureUserCanDownloadPDFBill(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyEnsureUserCanDownloadPDFBill method called in BillingTest");
		Reporter.log("Test Case : Ensure user can download a bill PDF");

		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Click Billing link | Billing Page Should be displayed");
		Reporter.log("5. Verify PDF Downloadable | PDF Downloadable with Right Information Should be Displayed");
		Reporter.log("===========================================");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.checkPageIsReady();
		Reporter.log("4. Billing Page Should be displayed");
		billingSummaryPage.clickDownLoadPDF();

	}

	/**
	 * Verify with AutoPay enabled the correct payment-type icon AND text (VISA,
	 * AMEX…) on the Billing Home page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void verifyAutopayCardIconDisplayedBillingpage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAutopayCardIconDisplayedBillingpage method called in EservicesPaymnetsTest");
		Reporter.log(
				"Test Case : Verify with AutoPay enabled the correct payment-type icon AND text (VISA, AMEX…) on the Billing Home page");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Billing Link | Billing Page should be displayed");
		Reporter.log("5. Verify AutoPay Payment Schdeuled date | AutoPay Payment Schdeuled date is loaded");
		Reporter.log("================================");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyAutopayCardiconBillingsummary();
	}

	/**
	 * Verify the scheduled AutoPay date shows correctly on the Billing page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void verifyScheduledautoPaydateBillingpage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyScheduledautoPaydateBillingpage method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Verify the scheduled AutoPay date shows correctly on the Billing page");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Billing Link | Billing Page should be displayed");
		Reporter.log("4. Verify AutoPay Payment Schdeuled date | AutoPay Payment Schdeuled date is loaded");
		Reporter.log("================================");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyAutoPayscheduledDateBillingpage();
	}

	/**
	 * DE91587 : PT# 16776629 - MyTMO [Desktop - Billing] Incorrect math on Bill
	 * Summary page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyTotalIncorrectMathOnBillSummaryPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : DE91587 : PT# 16776629 - MyTMO [Desktop - Billing] Incorrect math on Bill Summary page");
		Reporter.log("Data Condition : Regular user");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Billing Link | Billing Page should be displayed");
		Reporter.log(
				"5. In Current charges section, Verify Amount math is calculated accurately | Amount math should be correctly displayed to Total amount");
		Reporter.log("================================");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyAmountTotalInCurrentCharges();
	}

	/**
	 * P1_TC_Regression_Desktop_Bill Summary Bill Highlights validation
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void TestUsageChargesUnderBillHighlights(ControlTestData data, MyTmoData myTmoData) {
		logger.info("billingSummaryTestbillHighlights method called in BillingSummaryTest");
		Reporter.log("Test Case : Desktop_Bill Summary Bill Highlights validation");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on billing link | Billing summary page should be displayed");
		Reporter.log(
				"5. Verify billing highlight section verifyBillingCurrentChargesAndDownloadPDF| Billing highlight section should be displayed");
		Reporter.log("6. Click on 'you had a usage charges'| Should be able to navigate to bill details page");
		Reporter.log(
				"7. Verify the service details prior to the given date | Should be able to see the details prior to the date");
		Reporter.log("Actual Results");
	}

	/**
	 * P1_TC_Regression_Desktop_Bill Summary Bill Highlights validation
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void TestAutoPaySettingUnderBillHighlights(ControlTestData data, MyTmoData myTmoData) {
		logger.info("billingSummaryTestbillHighlights method called in BillingSummaryTest");
		Reporter.log("Test Case : Desktop_Bill Summary Bill Highlights validation");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on billing link | Billing summary page should be displayed");
		Reporter.log(
				"5. Verify billing highlight section verifyBillingCurrentChargesAndDownloadPDF| Billing highlight section should be displayed");
		Reporter.log("6. Click on 'Autopay settings link'| Should be able to navigate to Autopay landing page");
		Reporter.log(
				"7. Verify the service details prior to the given date | Should be able to see the details prior to the date");
		Reporter.log("Actual Results");
	}

	/**
	 * P1_TC_Regression_Desktop_Bill Summary Bill Highlights validation
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, "billing" })
	public void TestUsageSummaryUnderChargesSummary(ControlTestData data, MyTmoData myTmoData) {
		logger.info("TestUsageSummaryUnderChargesSummary method called in BillingSummaryTest");
		Reporter.log("Test Case : Desktop_Bill Summary Bill Highlights validation");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on billing link | Billing summary page should be displayed");
		Reporter.log("5. Verify Current charges| Current charges section should be displayed");
		Reporter.log("6. Click on 'View Usage summary'| Should be able to navigate to usage details page");
		Reporter.log("Actual Results");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		if (!(getDriver() instanceof AppiumDriver)) {
			billingSummaryPage.clickUsageDetails();
			// UsageOverviewPage usageOverviewPage = new
			// UsageOverviewPage(getDriver());
			// usageOverviewPage.verifyPageLoaded();
			UsagePage usage = new UsagePage(getDriver());
			usage.verifyPageLoaded();
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PERFORMANCE, "billing" })
	public void testNavigationToAutopayAndOtpFromBilling(ControlTestData data, MyTmoData myTmoData) throws IOException {
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on billing link | Billing summary page should be displayed");
		Reporter.log("Step 4: click autopay button | Autopay page should be displayed");
		Reporter.log("Step 5: click cancel autopay button | Cancel Autopay modal should be displayed");
		Reporter.log("Step 6: click YES on cancel autopay modal | HomePage should be displayed");
		Reporter.log("Step 7: click on billing link | Billing summary page should be displayed");
		Reporter.log("Step 8: click 'view bill details' link | BillDetails page should be displayed");
		Reporter.log("Step 9: click PayNow button | OneTimePayment page should be displayed");
		Reporter.log("Step 10: click cancel OTP button | Cancel OTP modal should be displayed");
		Reporter.log("Step 11: click continue on cancel OTP modal | HomePage should be displayed");
		Reporter.log("Step 12: click Logout | Login page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
	//	BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		
		Autopayswitch("OFF");
		homePage.clickBillingLink();
		BillingSummaryPage billingSummaryPage =new BillingSummaryPage(getDriver());
		billingSummaryPage.verifyAutopayAlertwhenOFf();
		billingSummaryPage.verifyAutopaySubAlertwhenOFf();
		billingSummaryPage.clickEasyPayArrow();

		NewAutopayLandingPage autoPayPage = new NewAutopayLandingPage(getDriver());
		// AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyPageLoaded();
		autoPayPage.clickBackbutton();
		autoPayPage.clickYesPleaseExitbutton();

		//HomePage homePage = new HomePage(getDriver());
		homePage.clickBillingLink();
		billingSummaryPage.verifyPageLoaded();
		billingSummaryPage.changeBillCycle();
		billingSummaryPage.clickViewBillDetails();
		BillDetailsPage billDetailsPage = new BillDetailsPage(getDriver());
		billDetailsPage.verifyPageLoaded();
		billDetailsPage.clickPayNowBtn();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.clickContinueBtnCancelModal();
		homePage.verifyPageLoaded();

	}

	/**
	 * DE144301 MyTMO[Desktop - Billing] M3 EIP blade disabled when customers have
	 * paid off all plans Validation Flow: Regular EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPBladeForFullPaidOffCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("DE144301 MyTMO[Desktop - Billing] M3 EIP blade disabled when customers have paid off all plans");
		Reporter.log("Data Condition | EIP eligible with Full Paid");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log(
				"5.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("6. Click on Installment Plan blade | EIP details page should be displayed");

		navigateToEIPDetailsPage(myTmoData);
	}

	/**
	 * US356342 PII Masking > Variables on Ebill Page
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEBillPIIMasking(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US356342	PII Masking > Variables on Ebill Page");
		Reporter.log("Data Condition | Any User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch the application | Application Should be Launched");
		Reporter.log("Step 2: Login to the application | User Should be login successfully");
		Reporter.log("Step 3: Verify Home page | Home page should be displayed");
		Reporter.log("Step 4: Click on Billing link | Billing summary page should be displayed");
		Reporter.log("Step 5: Verify PII masking for Ban| PII masking attribute should be present for Ban");
		Reporter.log(
				"Step 6: Verify PII masking for FirstName | PII masking attribute should be present for FirstName");
		Reporter.log("Step 7: Verify PII masking for Msisdn | PII masking attribute should be present for Msisdn");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyPIIMaskingForBan(PaymentConstants.PII_CUSTOMER_BANNUMBER_PID);
		billingSummaryPage.verifyPIIMaskingForFirsName(PaymentConstants.PII_CUSTOMER_CUSTFIRSTNAME_PID);
		billingSummaryPage.verifyPIIMaskingForMsisdn(PaymentConstants.PII_CUSTOMER_MSISDN_PID);

	}

	/**
	 * US434257 Desktop: Create New Payment Settings Blade US434760 Mobile: Create
	 * New Payment Settings Blade
	 *
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void testMyPromotionsBladeonBillingPage(ControlTestData data, MyTmoData myTmoData) throws Exception {
		Reporter.log("Test Case : US434257- Desktop: Create New Payment Settings Blade");
		Reporter.log("Data Condition | Any User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch the application | Application Should be Launched");
		Reporter.log("Step 2: Login to the application | User Should be login successfully");
		Reporter.log("Step 3: Verify Home page | Home page should be displayed");
		Reporter.log("Step 4: Click on Billing link | Billing summary page should be displayed");
		Reporter.log(
				"Step 5: verify 'My Promotions' blade|'My Promotions' blade should be displayed in Billing Summary page");
		Reporter.log(
				"Step 6: Verify Icon Image for 'My Promotions' blade| Gray promotion icon for inactive state ,magenta image icon for hover state should be displayed");
		Reporter.log("Step 7: Verify right pointing chevron on right end of the blade| chevron should be displayed");
		Reporter.log(
				"Step 8: verify Promotions blade is  last in blade stack | Promotions blade should be  last in blade stack");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		// BillingSummaryPage billingSummaryPage =
		// navigateToBillingSummaryPage(myTmoData);
		BillingSummaryPage billingSummaryPage = navigateTopreviousEbillfromBBAccount(myTmoData);

		billingSummaryPage.verifyMyPromotionBlade();
		billingSummaryPage.verifyMyPromotionBladeIcon();
		billingSummaryPage.verifyMyPromotionBladeChevron();
	}

	/**
	 * Verify the scheduled AutoPay date shows correctly on the Billing page merge
	 * cases:verifyScheduledautoPaydateBillingpage,
	 * testVerifyPaymentHubAutopayDueEstimation,
	 * testVerifyAutopayOnPAHubWhenAutopayON
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void verifyAutopayenablespecificcases(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyScheduledautoPaydateBillingpage method called in BillingSummaryPageTest");
		Reporter.log("Test Case : Verify the scheduled AutoPay date shows correctly on the Billing page");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Billing Link | Billing Page should be displayed");
		Reporter.log("4. Verify AutoPay Payment Schdeuled date | AutoPay Payment Schdeuled date is loaded");
		Reporter.log("5: verify autopay due date alert | autopay due date alert   should be displayed");
		Reporter.log("6: verify autopay on alert | autopay on alert   should be displayed");
		Reporter.log("================================");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyAutoPayscheduledDateBillingpage();
		billingSummaryPage.verifyDueDatePastHeader();
		billingSummaryPage.verifyAutopayAlertwhenON();

	}

	/**
	 * Verify PayNow Button Disabled For Previous Cycles
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void verifyPayNowButtonDisabledForPreviousCycles(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPayNowButtonDisabledForPreviousCycles method called in BillingSummaryPageTest");
		Reporter.log("Test Case : Verify PayNow Button Disabled For Previous Cycles on the Billing page");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Billing Link | Billing Page should be displayed");
		Reporter.log(
				"4. Verify paynow button disabled for previous cycles| paynow button should be disabled for previous cycles");

		Reporter.log("================================");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifypayNowButtonDisabledForPreviousBillCycles();

	}

	/**
	 * Verify the scheduled AutoPay date shows correctly on the Billing page merge
	 * cases:verifyScheduledautoPaydateBillingpage,
	 * testVerifyPaymentHubAutopayDueEstimation,
	 * testVerifyAutopayOnPAHubWhenAutopayON
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Throwable
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void Checkebillpagesinbritebill(ControlTestData data, MyTmoData myTmoData) throws Throwable {
		logger.info("verifyScheduledautoPaydateBillingpage method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : DE140270:MyTMO [Desktop/Mobile - Billing] Legacy billing page link mapped to UMB");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Billing Link | Billing Page should be displayed");
		Reporter.log("4. Click on History page | History page loaded");
		Reporter.log("5: Clcik on ebill bill summary page | Bill summary page loaded");
		Reporter.log("6: check bill detials page and usage navigation | both should work as expected");
		Reporter.log("================================");
		BillingSummaryPage billingSummaryPage = navigateToEbillpagethroughBBpage(myTmoData);
		billingSummaryPage.changeBillCycle();
		billingSummaryPage.clickViewBillDetails();
		BillDetailsPage billDetailsPage = new BillDetailsPage(getDriver());
		billDetailsPage.verifyPageLoaded();
		billDetailsPage.clickOnBackToSummaryLink();
		billingSummaryPage.verifyPageLoaded();
		billingSummaryPage.clickUsageDetails();
		UsagePage usage = new UsagePage(getDriver());
		usage.verifyPageLoaded();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, "billing" })
	public void CheckautopaypaymentmethodinLegacybillpage(ControlTestData data, MyTmoData myTmoData) throws Throwable {
		logger.info("Check Autopay paymentmethod displayed on Legacy billing page");
		Reporter.log(
				"Test Case : CDCDWG2-375:(Core) EP-18218 MyTMO [Billing] Payment method erroneously displayed on legacy bill details page");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Enable autopay | Autopayshold be enabled");
		Reporter.log("5. Click Billing Link | Billing Page should be displayed");
		Reporter.log("6. check autopay payment method | Autopayment method should be displayed");

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		Autopayswitch("ON");
		homePage.clickBillingLink();
		BillingSummaryPage billingSummaryPage = TopreviousEbillfromBBAccount(myTmoData);
		billingSummaryPage.verifyPageLoaded();
		billingSummaryPage.checkpaymentdateEasyPay();
		billingSummaryPage.checkcardImageEasyPay();
		billingSummaryPage.checkcardDetailEasyPay();
		billingSummaryPage.clickViewBillDetails();
		BillDetailsPage billDetailsPage = new BillDetailsPage(getDriver());
		billDetailsPage.verifyPageLoaded();
		billDetailsPage.checkAutopayText();
		billDetailsPage.checkAutopayduedateText();
		billDetailsPage.Checksprite();
	}

}