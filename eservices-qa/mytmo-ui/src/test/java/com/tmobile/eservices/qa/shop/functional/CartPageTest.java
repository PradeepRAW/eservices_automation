package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.ConsolidatedRatePlanPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.InterstitialTradeInPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.PLPPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.shop.TradeInAnotherDevicePage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionValuePage;
import com.tmobile.eservices.qa.pages.shop.UNOPDPPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import com.tmobile.eservices.qa.shop.ShopConstants;

public class CartPageTest extends ShopCommonLib {

	/**
	 * US183029 Standard Trade-in : Navigation from Cart - Clear/Remove cart
	 * US271797: Device : Order Details - Device remove CT C410981
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testTradeInClearCartNavigationFromCartToPLP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Case : Verify Trade-In value page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Log in to the application | Logged in Successfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
		Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
		Reporter.log("4. Click on the device on Device PLP page |Device PDP page should be displayed ");
		Reporter.log(
				"5. Select FRP from payment option and click on Add to cart after verify the FRP amount  | Line Selector page should be displayed");
		Reporter.log("6. Select Standard line on LS page | Trade in Phone Selection page should be displayed");
		Reporter.log(
				"7. Select device phone selection page and click on continue | Trade in confirmation page should be displayed");
		Reporter.log("8. Click on Continue button on confirmation page |  PHP page should be displayed");
		Reporter.log("9. Click on Continue button on PHP page | Cart page should be displayed");
		Reporter.log("10. Click on remove link |Remove model header should be displayed");
		Reporter.log("11. Click on remove yes button |Empty cart should be displayed");
		Reporter.log("12. Click on shop phone button |Line selector page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByFeaturedDevicesWithTradeInFlow(myTmoData);
		cartPage.clickRemoveLink();
		cartPage.verifyRemoveModelHeader();
		cartPage.clickYesRemoveButton();
		cartPage.verifyEmptyCart();
		cartPage.clickShopPhones();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyShoppage();
	}

	/**
	 * US234912 :New Cart : UX UI feedback
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testDeviceDetailsInCartAndEditDeviceFunctionality(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US234912 :New Cart : UX UI feedback");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log(
				"1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See all phones on shop page |PLP Page should be diplayed");
		Reporter.log("4. Click on the device on Device PLP page | Device PDP Page should be displayed ");
		Reporter.log("5. Select payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select Standared line on LS page |Phone Selection Page should be displayed");
		Reporter.log("7. Click on Skip Trade in Hyper link on Phone selection page | PHP Page should be displayed ");
		Reporter.log("8. Click on Continue on PHP page| Cart Page should be displayed");
		Reporter.log("8. Click on Continue on PHP page| Cart Page should be displayed");
		Reporter.log("10. Verify edit button | Edit button should be displayed");
		Reporter.log("11. Click on Edit button | PLP page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		String deviceName = pdpPage.getDeviceName();
		String deviceColor = pdpPage.getDeviceColor();
		String deviceMemory = pdpPage.getDeviceMemory();
		pdpPage.clickAddToCart();

		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();

		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyDeviceImage();
		cartPage.verifyDeviceName(deviceName);
		cartPage.verifyDeviceColor(deviceColor);
		cartPage.verifyDeviceMemory(deviceMemory);
		cartPage.verifyEditButtonIsDisplayed();
		cartPage.clickEditButtonOnDevice();
		pdpPage.verifyPDPPage();
		pdpPage.clickContinueBtn();
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		lineSelectorDetailsPage.clickOnSkipTradeInLink();
		deviceProtectionPage.verifyDeviceProtectionURL();
	}

	/**
	 * US279527: New Cart : How Sales tax is calculated link and modal US268727:
	 * New Cart : New Device cost - Monthly break down modal * US306687: MyTMO -
	 * Additional Terms - Standard Upgrade Flow & Upgrade with Accessories
	 * Attached Flow - Cart Order Detail: Product Tile Pricing Display - No
	 * Trade-in US314024, US351482, US351484 * US347749: MyTMO - Additional
	 * Terms - Standard Upgrade with Accessories Attached Flow: Display Total
	 * Financed Amount in Cart * US329559: TEST ONLY: MyTMO - Additional Terms -
	 * Upgrade with Accessories
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testNewCartHowSalesTaxIsCalculatedLinkAndModalAndEIPLegalText(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case:New Cart : How Sales tax is calculated link and modal");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log(
				"1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See all phones on shop page |PLP Page should be diplayed");
		Reporter.log("4. Click on the device on Device PLP page | Device PDP Page should be displayed ");
		Reporter.log("5. Select payment option and click on Add to cart | Line Selector page should be displayed");
		Reporter.log("6. Select Standared line on LS page |Phone Selection Page should be displayed");
		Reporter.log("7. Click on Skip Trade in Hyper link on Phone selection page | PHP Page should be displayed ");
		Reporter.log("8. Click on Continue on PHP page| Cart Page should be displayed");
		Reporter.log("9. Verify Due today | Due today should be present for each device and accessory");
		Reporter.log("10. Verify EIP terms | EIP terms should be present for each device and accessory");
		Reporter.log("11. Verify Monthly payment | Monthly payment should be present for each device and accessory");
		Reporter.log("12. Verify Due Monthly text | Due Monthly text should be present");
		Reporter.log("13. Verify Legal Text is present under Due Monthly | Legal text should be displayed");
		Reporter.log(
				"13. Verify shortest EIP term is present in Legal Text | Shortest EIP term is present in Legal text");
		Reporter.log(
				"14. Verify  how it calculated sales tax link | How it calculated sales tax link should be displayed");
		Reporter.log("15. Click on how it calculated sales tax link | Tax breadown window headere should be displayed");
		Reporter.log("16. Verify Sales tax sub title | Sales tax sub title should be displayed");
		Reporter.log("17. Verify device name | Device name should be displayed");
		Reporter.log("18. Verify device price | Device price should be displayed");
		Reporter.log("19. Verify sales tax legal disclaimer | Sales tax legal disclaimer should be displayed");
		Reporter.log("20. Verify Got it button | Got it button should be displayed");
		Reporter.log("21. Verify Due monthly total | Due monthly total should be present and contain only D1");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		int accessoryCount = 1;

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickContinueButton();

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();

		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyEIPTermForDeviceAndAccessories();
		cartPage.verifyDueTodayText(accessoryCount);
		cartPage.verifyMonthlyPayment(accessoryCount);
		cartPage.verifyDueMonthlyText();
		cartPage.verifyDueMonthlyTotalIsDisplayed();
		cartPage.verifyFullFinancedAmount();
		cartPage.verifyEIPLegalText();
		cartPage.verifyEIPTermIsPresentInLegalText();
		cartPage.verifyHowItCalculatedSalesTaxLink();
		cartPage.clickHowItCalculatedSalesTaxLink();
		cartPage.verifyHowItCalculatedTaxBreakDownWindow();
	}

	/**
	 * 
	 * Select 64 GB memory and FRP in PDP page and Navigated to Payment section
	 * in cart page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.PERFORMANCE })
	public void testSelectMemoryAndNavigatedToCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Select 64 gb memory and navigated to payment section in cart page");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"1. Log in to the application with valid FA Standared customer  | Logged in Succesfully and landed on home page ");
		Reporter.log("2. Click on Shop on home page | Shop Page should be displayed");
		Reporter.log("3. Click on See all phones on shop page |Product list page should be diplayed");
		Reporter.log("4. Filter Apple devices and Select IPhoneX device | Product details page should be displayed ");
		Reporter.log(
				"5. Select 64 gb memory, FRP and Click Add to cart button | LineSelector page should be displayed ");
		Reporter.log("6. Select line | PhoneSelection Page should be displayed ");
		Reporter.log("7. Click Skip trade-in link | Insurance migration Page should be displayed ");
		Reporter.log("8. Click Decline insurance button | Decline insurance model window should be displayed ");
		Reporter.log("9. Click decline insurance yes button | Cart page should be displayed ");
		Reporter.log("10. Click Continue to shipping button | Ship to different address button should be displayed");
		Reporter.log("11. Click Continue to Payment button | Payment information form should be displayed");
		Reporter.log(
				"12. Verify Accept and Continue button is disabled | Accept and Continue button should be displayed ");
		Reporter.log("13. Click T-Mobile Icon | Home page should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickSeeAllPhones();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickFilterDropdown();
		plpPage.selectApple();
		plpPage.clickOnCloseFilter();
		plpPage.clickDeviceByName(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.selectMemory(ShopConstants.DEVICE_MEMORY_SIZE);
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption(ShopConstants.PAYMENT_OPTION_FRP);
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		lineSelectorDetailsPage.clickOnSkipTradeInLink();
		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageBySeeAllPhonesWithTradeInFlow(
				myTmoData);
		deviceProtectionPage.verifyDeviceProtectionURL();
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}
		deviceProtectionPage.verifyInsuranceNotificationModalPopWindow();
		deviceProtectionPage.clickInsuranceModalPopupWindowOkButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.verifyAcceptAndContinue();
		cartPage.clickOnTMobileIcon();
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		homePage.clickLogoutButton();
	}

	/**
	 * US319849: TEST ONLY: MyTMO - Additional Terms - Standard Upgrade Flow &
	 * Upgrade with Accessories Attached Flow - FRP: Suppress Legal Text
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testLegalTextDeviceFRPAccessoryFRP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case:US319849:TEST ONLY: MyTMO - Additional Terms - Standard Upgrade Flow & Upgrade with Accessories Attached Flow - FRP: Suppress Legal Text");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select FRP Payment | FRP payments is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button |  Insurance migration page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log(
				"12. Select 2 accessory devices, select Pay In Full and click Continue button | Cart page should be displayed");
		Reporter.log("13. Verify Legal Text | Legal text should not be displayed");
		Reporter.log(
				"14. verify Full Retail Price label for the product  separated from the legal text| Full Retail Price label for the product should be separated from the legal text");
		Reporter.log(
				"15. verify Full Retail Price placed under  EIP pricing / loan term length data|Full Retail Price should be placed under  EIP pricing / loan term length data");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickContinueButton();

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories("1");
		accessoryPLPPage.clickContinueButton();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyNoEIPLegalText();
		cartPage.verifyFRPseperatedfromLegalText();
		cartPage.verifyFullRetailPrice();
	}

	/**
	 * US319849: TEST ONLY: MyTMO - Additional Terms - Standard Upgrade Flow &
	 * Upgrade with Accessories Attached Flow - FRP: Suppress Legal Text
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testLegalTextDeviceEIPAccessoryFRP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case:US319849:TEST ONLY: MyTMO - Additional Terms - Standard Upgrade Flow & Upgrade with Accessories Attached Flow - FRP: Suppress Legal Text");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select EIP Payment | EIP payments is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button |  Insurance migration page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log(
				"12. Select 2 accessory devices, select Pay In Full and click Continue button | Cart page should be displayed");
		Reporter.log("13. Verify Legal Text | Legal text should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionURL();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickPayMonthlyPopupCloseButton();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		// accessoryPLPPage.clickAccessoriesContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyEIPLegalText();
	}

	/**
	 * US307155: [Continued] Accessories: Price Mismatch failure from EOS Create
	 * Quote API response and create EIP Error Modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testPriceMismatchFailure(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select EIP Payment | EIP payments is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button |  Insurance migration page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("12. Click Skip Accessory Button | Cart page should be displayed");
		Reporter.log("13. Verify EIP Error Modal | EIP Error Modal should be displayed");
		Reporter.log("13. Verify Got IT Button | Got IT Button should be displayed");
		Reporter.log("14. Click Close Modal Button | Shop page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();

		accessoryPLPPage.clickSkipAccessoriesLink();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyEIPErrorModal();//
		cartPage.verifyGotItButton();//
		// Step 14 is missing
	}

	/**
	 * US346498 SSU MyTMO > Trade in Promo > Deep link - SMS/e-mail > Trade in
	 * value page US348496 SSU MyTMO > Trade in Promo > Deep link - SMS/e-mail >
	 * Trade in > Cart promo pricing
	 * 
	 * Due to #DE169604 insurance migration page is not displayed
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testDeeplinkFromSmsAndEmailTradeInValueAndCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US346498 SSU MyTMO > Trade in Promo > Deep link - SMS/e-mail > Trade in value page");
		Reporter.log(
				"Test case: US348496 SSU MyTMO > Trade in Promo > Deep link - SMS/e-mail > Trade in > Cart promo pricing");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Follow the link | Login page for MyTMO should be displayed");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify PDP page | Product details page should be displayed");
		Reporter.log("4. Select EIP Payment | EIP payments is selected");
		Reporter.log("5. Click add to cart button | Line selector page should be displayed");
		Reporter.log("6. In line Selection page click on first line | Phone selection page should be displayed");
		Reporter.log(
				"7. In Phone selection page click on Got a diffrent phone | Trade In Another Device page should be displayed");
		Reporter.log(
				"8. In Trade In Another Device page Select Carrier,Make,Model And enter IMEI | Details should be entered");
		Reporter.log(
				"9. Click on contiune button in Trade In Another Device page | Trade In Device Condition should be displayed");
		Reporter.log(
				"10. In Trade In Device Condition page select Its good radio button and click on contiune button | Trade in value page should be displayed");
		Reporter.log(
				"11. In Trade in value page click on Continue button | Insurance migration page should be displayed");
		Reporter.log("12. In Insurance migration page click on Continue button | Cart should be displayed");
		Reporter.log(
				"13. Verify EIP pricing And applied Promo | Promo value and EIP pricing should be displayed. Final EIP price should be difference between regular EIP and promo prices");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToTradeInValuePageThroughDeeplinkUsingLineseltor(myTmoData);
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.clickContinueTradeInButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());

		accessoryPLPPage.clickSkipAccessoriesLink();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyeipPricingBeforepromo();
		cartPage.verifyTradeInPromoTextIsDisplayed();
		cartPage.verifyTradeInPromoValueIsDisplayed();
		cartPage.verifyeipPricingAfterpromo();
		cartPage.VerifyFinalEip();

	}

	/**
	 * US342377 myTMO > PDP > Simplification > Standard Upgrade > Enrollment >
	 * NY customers > Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS})
	public void testDeviceProtectionSocsDisplayedForNYcustomersAndCartFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US342377: myTMO > PDP > Simplification > Standard Upgrade > Enrollment > NY customers > Display");
		Reporter.log("Data Condition | IR STD PAH Customer ,NY customers, no device protection");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select FRP | FRP is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button | Insurance migration page should be displayed");
		Reporter.log("11. Verify Insurance header as'How do you want to protect your phone?' | "
				+ "Insurance header as'How do you want to protect your phone?' should be displayed");
		Reporter.log("12. Verify 'Best Value!' text below header| 'Best Value!' text should be displayed");
		Reporter.log(
				"13. Verify 'Protection 360' Soc insurance description | 'Protection 360' soc insurance description should be displayed");
		Reporter.log(
				"14. Verify 'Protection 360' SOC selected by default |  'Protection 360' SOC should be selected by default");
		Reporter.log("15. Verify All available New York SOCs |  All available New York SOCs should be displayed");
		Reporter.log(
				"16. Verify NY SOCs displayed in order 'Extended warranty SOC' , 'Insurance only SOC',' Extended warranty and insurance SOC'| NY SOCs should be  displayed in order 'Extended warranty SOC' , 'Insurance only SOC',' Extended warranty and insurance SOC'");
		Reporter.log(
				"17. Verify Amount  for Extended warranty SOC below the SOC name '$6.75/mo'| Amount  for Extended warranty SOC below the SOC name '$6.75/mo' should be displayed");
		Reporter.log("18. Verify two decimal points for pricing| pricing should be two decimal points");
		Reporter.log(
				"19. Verify Extended warranty benefit description| Extended warranty benefit description should be displayed");
		Reporter.log(
				"20. Verify Amount  for Insurance only SOC below the SOC name '$4.00/mo'| Amount  for Insurance only SOC below the SOC name '$4.00/mo' should be displayed");
		Reporter.log("21. Verify two decimal points for pricing| pricing should be two decimal points");
		Reporter.log(
				"22. Verify Insurance only benefit description|  Insurance only benefit description should be displayed");
		Reporter.log(
				"23. Verify Amount for Extended warranty and insurance SOC below the SOC name '$8.50/mo'| Amount for Extended warranty and insurance SOC below the SOC name '$8.50/mo' should be displayed");
		Reporter.log("24. Verify two decimal points for pricing| pricing should be two decimal points");
		Reporter.log(
				"25. Verify Extended warranty and insurance benefit description.|  Extended warranty and insurance benefit description should be displayed");
		Reporter.log(
				"26. Verify 'I don't need protection for my phone' radio button |  'I don't need protection for my phone' radio button should be displayed");
		Reporter.log("27. Verify legal text for NY customers | Legal text for NY customers should be displayed");
		Reporter.log("28. Select any SOC  and click on continue button| Accessories PLP  should be displayed");
		Reporter.log("29. Click on skip accessories| Cart page  should be displayed");
		Reporter.log("30. Verify selected SOC added to  cart | Selected SOC should  be added  to cart");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.verifyInsuranceHeaderForNY();
		deviceProtectionPage.verifyLegalTextForBestValue();
		deviceProtectionPage.verify360SOCDescription();
		deviceProtectionPage.verify360SOCIsSelected();
		deviceProtectionPage.verifyAllNYSOCPresent();
		deviceProtectionPage.verifyExtendedWarrantySOCDescriptionPresent();
		deviceProtectionPage.verifyExtendedWarrantySOCPricePresent();
		deviceProtectionPage.verifyInsuranceOnlySOCDescriptionPresent();
		deviceProtectionPage.verifyInsuranceOnlySOCPricePresent();
		deviceProtectionPage.verifyExtendedWarrantyAndInsuranceSOCDescriptionPresent();
		deviceProtectionPage.verifyExtendedWarrantyAndInsuranceSOCPricePresent();
		deviceProtectionPage.verifyDontNeedProtectionRadioButtonForNY();
		deviceProtectionPage.verifyLegalTextForNYCustomers();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();

		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyInsuranceName();
	}

	/**
	 * US342386 myTMO > PDP Simplification > Standard Upgrade > Migration > Non
	 * - NY customers > Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testDeviceMigrationNonNYCustomersDisplayCartFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test  US342386:  myTMO > PDP Simplification > Standard Upgrade > Migration > Non - NY customers > Display");
		Reporter.log("Data Condition | IR STD PAH Customer, Non-NY customers, with device protection");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select FRP | FRP is selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select line | Phone selection page should be displayed");
		Reporter.log("10. Click skip trade in button | Insurance migration page should be displayed");
		Reporter.log("11. Choose a more expensive device (different tier)");
		Reporter.log(
				"12. Verify header 'Your new phone requires higher level of protection. Let's keep it protected.' | 'Your new phone requires higher level of protection. Let's keep it protected.'  should be displayed");
		Reporter.log(
				"13. Verify current SOC status changed as 'Previous'| 'current SOC status should be changed as 'Previous' ");
		Reporter.log(
				"14. Verify the previous SOC name (grandfathered SOC name if the current SOC hasn't been migrated to the new SOC)| the previous SOC name should be displayed");
		Reporter.log(
				"15. Verify the amount of the previous SOC '$12.00/mo.'| the amount of the previous SOC '$12.00/mo.' should be displayed");
		Reporter.log(
				"16. Verify 'Protection 360' as a recommended SOC with pricing and description| 'Protection 360' as a recommended SOC with pricing and description should be displayed");
		Reporter.log(
				"17. Verify radiobutton for both current and recommended SOCs.| radiobutton for both current and recommended SOCs. should be displayed");
		Reporter.log(
				"18. Verify 'I don't need protection for my phone' radio button |  'I don't need protection for my phone' radio button should be displayed");
		Reporter.log(
				"19. Verify legal text for non NY customers | Legal text for non NY customers should be displayed");
		Reporter.log("20. Select any SOC  and click on continue button | Accessories PLP should be displayed");
		Reporter.log("21. Click on skip accessories | Cart page  should be displayed");
		Reporter.log("22. Verify selected SOC added to  cart | Selected SOC should  be added  to cart");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSelectorDetailsPage lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();

		deviceProtectionPage.verifyHighLevelProtectionTitle();
		deviceProtectionPage.verifyPrevoiusSOCStatus();
		deviceProtectionPage.verifyPrevoiusSOCName();
		deviceProtectionPage.verifyPrevoiusSOCPrice();
		deviceProtectionPage.verify360SOCName();
		deviceProtectionPage.verify360SOCPrice();
		deviceProtectionPage.verify360SOCDescription();
		deviceProtectionPage.verifyIDOntNeedProtectionSOC();
		deviceProtectionPage.verifyRadioButtonsPresent();
		deviceProtectionPage.verifyLegalTextFornonNYCustomers();
		deviceProtectionPage.clickContinueButton();

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();

		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyInsuranceName();
	}


	/**
	 * US383729 Accessories - Enable the Accessories Card and Add Accessory CTA
	 * on the cart
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testAccessoriesCTAandUpgradeSavingsMsgInCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case#US383729- Accessories - Enable the Accessories Card and Add Accessory CTA on the cart");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select Feature phone from shop page | Product details page should be displayed");
		Reporter.log("6. Click add to cart button | Line selection page should be displayed");
		Reporter.log("7. Select line |  Phone selection page should be displayed");
		Reporter.log("8. Click skip trade in button |  Insurance migration page should be displayed");
		Reporter.log("9. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("10. Click Skip Accessory Button | Cart page should be displayed");
		Reporter.log("11. Verify Add accessories button |  'Add accessories' button should be displayed");
		Reporter.log("12. click on 'Add accessories' CTA |  Accessory PLP page should be displayed");
		Reporter.log("13. select accessories and click on continue button| Selected accessory should be added to cart");
		Reporter.log(
				"14. Verify Upgrade saving message in cart page | Authorable text 'You're saving $20 by upgrading...' should be present on above Continue shopping CTA.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);

		cartPage.verifyCartPage();
		cartPage.verifyAddAccessoryCartButton();
		cartPage.clickOnAddAccessoryCartButton();

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupCloseButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.verifyLegalDisclousureatEachRow();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		cartPage.verifyCartPage();
		cartPage.verifyUpgradeSavingMessage();
		cartPage.clickSeeDetailsLink();
		cartPage.verifySeeDetailsPopUp();
		cartPage.verifyModalCloseCTA();

	}

	/**
	 * US355337: AAL - Order Details: Due Today Section US355314: AAL - Order
	 * Details: Aditional Voice Line Section US355324: AAL - Order Details:
	 * Item/Hard Goods section (BYOD scenario) US488854: AAL - suppress trade in
	 * messaging for BYOD (cart)
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION})
	public void testBYODSimAndDueTodaySectionInOrderDetailsCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US355337: AAL - Order Details: Due Today Section");
		Reporter.log("Test case: US355314: AAL - Order Details: Aditional Voice Line Section");
		Reporter.log("Test case: US355324: AAL - Order Details: Item/Hard Goods section (BYOD scenario)");
		Reporter.log("Test case: US488854: AAL - suppress trade in messaging for BYOD (cart)");
		Reporter.log("Data Condition |  PAH Customer, eligible for AAL");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on 'Add A LINE' button | Customer intent page should be displayed");
		Reporter.log("5. Select on 'Bring my own device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("6. Click Continue button on Consolidated Rate Plan Page | PHP page should be displayed");
		Reporter.log("7. Click Continue button on PHP page | Accessory PLP Page should be displayed");
		Reporter.log("8. Click Skip accessories button on Accessory PLP page | Cart Page should be displayed");
		Reporter.log("9. Verify Phone device Section details | User name should not be diplayed beside Line Name and"
				+ " should be displayed with 'New Line' and Ship date should be displayed");
		Reporter.log("10. Verify SIM Section details | SIM Kit name, Pay in full & SIM Price should be displayed");
		Reporter.log(
				"11. Verify Additional Voice line section details  | Additional Voice line name, Plan Name, Monthly total "
						+ "for new line(+Taxes if needed) should be displayed");
		Reporter.log("12. Verify Due details section | Due details section should be displayed");
		Reporter.log(
				"13. Verify AAL rate and Sim card price under Due details section | AAL Monthly Price & Sim Card price should be displayed");
		Reporter.log(
				"14. Verify Estimated details in Due details section | Estimated due today, Estimated Shipping, Estimated sales tax, Total Due, Additional monthly prices should be displayed");
		Reporter.log(
				"15. Verify Trade in message presence | Ensure that TradeIn message details shouldn't be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageFromAALBYODFlow(myTmoData);
		cartPage.verifyPhoneSectionDetails();
		cartPage.verifySIMSection();
		cartPage.verifyRemoveCtaIsPresent();
		cartPage.verifyAdditionalVoiceLineSection();
		cartPage.verifyEstDueTodayIsDisplayed();
		cartPage.verifyEstShippingIsDisplayedInOrderDetailPage();
		cartPage.verifyEstSalesTaxesIsDisplayedInOrderDetail();
		cartPage.verifyOrderEstTotalDueTodayIsDisplayedInOrderDetail();
		Double simKitPrice = cartPage.getSIMKitPrice();
		Double estimateShippingPrice = cartPage.verifyAndGetEstimateShippingPrice();
		Double estimateSalesTaxPrice = cartPage.verifyAndGetEstimateSalexTaxPrice();
		Double monthlyTotalCharges = simKitPrice + estimateShippingPrice + estimateSalesTaxPrice;
		Double dueTotal = cartPage.getEstTotalDueTodayPriceOrderDetailsCartPage();
		cartPage.verifyTotalDueTodayPrices(dueTotal,monthlyTotalCharges);
		cartPage.verifyTradeInMsgTextNotDisplayed();

	}

	/**
	 * US406762: CART - Deposit for order details
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testDepositeAmountInCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US406762- CART - Deposit for order details");
		Reporter.log("Data Condition | STD PAH Customer with Deposit amount and AAL elligble");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Deive | PDP page should be displayed");
		Reporter.log("6. click on 'AAL' CTA | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log("7. Veify Due Today amount | Due Today amount should be displayed ");
		Reporter.log(
				"8. Veify Deposite amount in RatePlan page | Deposite amount should be displayed in RatePlan page ");
		Reporter.log("9. Click Continue button | PHP page should be displayed");
		Reporter.log("10. Click Continue button | Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.verifyDueTodayText();
		consolidatedRatePlanPage.verifyDueTodayAmount();
		consolidatedRatePlanPage.verifyAuthorableDepositAmount();
		consolidatedRatePlanPage.verifyContinueCTA();
		consolidatedRatePlanPage.clickOnContinueCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionURL();
		deviceProtectionPage.clickOnYesProtectMyPhoneButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();

		Double downPayment = cartPage.getEstDueTodayIntegerUpgradeCart();
		Double estimateShippingPrice = cartPage.verifyAndGetEstimateShippingPrice();
		Double estimateSalesTaxPrice = cartPage.verifyAndGetEstimateSalexTaxPrice();
	}

	/**
	 * US412634: Promo Codes - Adding promo code CTA US413882: Promo Codes -
	 * Applying promo code (valid) US413889: Promo Codes - Applying promo code -
	 * Remove promo code entered
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.AAL })
	public void testAddingPromoCodeCTAInCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test : US412634: Promo Codes - Adding promo  code CTA" + "US413889: Promo Codes - Applying promo code"
						+ "US413889: Promo Codes - Applying promo code - Remove promo code entered");
		Reporter.log("Data Condition | STD PAH Customer AAL eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Device | PDP page should be displayed");
		Reporter.log("6. click on 'AAL' CTA | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log("7. Verify Due Today amount | Due Today amount should be displayed ");
		Reporter.log(
				"8. Verify Deposit amount in RatePlan page | Deposit amount should be displayed in RatePlan page ");
		Reporter.log("9. Click Continue button | PHP page should be displayed");
		Reporter.log("10. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("11. Click OK button on Don't brake the bank modal | Accessory PLP page should be displayed");
		Reporter.log("12. Select Skip Accessories | Cart page should be displayed");
		Reporter.log("10. Verify Add a promo code button presence | Add a promo code button should be displayed");
		Reporter.log("11. Click on Add a promo code button | Promo code editing field should be displayed");
		Reporter.log("12. Enter valid Promo code | Promo code should be entered");
		Reporter.log("13. Click on APPLY button | 'Promo code' has been applied message should be displayed");
		Reporter.log(
				"14. Again enter valid Promo code and click on APPLY button | 'Promo code' has been applied message should be displayed");
		Reporter.log("15. Verify  Remove CTA| Remove CTA will be to the right of promo text applied");
		Reporter.log("16. Click on  Remove CTA| In order details pricing should be updated");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US412736: Promo Codes - Applying promo code (invalid)
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.AAL })
	public void testInvalidPromoCodeInCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US412736: Promo Codes - Applying promo code (invalid)");
		Reporter.log("Data Condition | STD PAH Customer AAL elligble");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Deive | PDP page should be displayed");
		Reporter.log("6. click on 'AAL' CTA | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log("7. Verify Due Today amount | Due Today amount should be displayed ");
		Reporter.log(
				"8. Verify Deposit amount in RatePlan page | Deposit amount should be displayed in RatePlan page ");
		Reporter.log("9. Click Continue button | PHP page should be displayed");
		Reporter.log("10. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("11. Click OK button on Dont brake the bank modal | Accessory PLP page should be displayed");
		Reporter.log("12. Select Skip Accessories | Cart page should be displayed");

		Reporter.log("13. Verify Add a promo code button presence | Add a promo code button should be displayed");
		Reporter.log("14. Click on Add a promo code button | Promo code editing field should be displayed");
		Reporter.log("15. Enter invalid Promo code and click on APPLY button | Sorry, 'promo code' is not a valid code"
				+ " message should be displayed in RED");
		Reporter.log("16. Try entering valid Promo code | Warning message should go off");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US413894# Promo Codes - Applying promo code - overriding
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.AAL })
	public void testAppliedPromoCodeCTAInCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US413894#Promo Codes - Applying promo code - overriding");
		Reporter.log("Data Condition | STD PAH Customer AAL eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Device | PDP page should be displayed");
		Reporter.log("6. click on 'AAL' CTA | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log("7. Verify Due Today amount | Due Today amount should be displayed ");
		Reporter.log(
				"8. Verify Deposit amount in RatePlan page | Deposit amount should be displayed in RatePlan page ");
		Reporter.log("9. Click Continue button | PHP page should be displayed");
		Reporter.log("10. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("11. Click OK button on Don't brake the bank modal | Accessory PLP page should be displayed");
		Reporter.log("12. Select Skip Accessories | Cart page should be displayed");
		Reporter.log("13. Verify 'Add a promo code' button presence | 'Add a promo code button should be displayed");
		Reporter.log("14. Enter PromoCode in 'Add a promo code' field and click on Apply button | User friendly message"
				+ " stating Promo code has been applied message should be displayed");
		Reporter.log(
				"16. Enter PromoCode which has larger discount in 'Add a promo code' field and click on Apply button | Warning icon and message "
						+ "'The previous code has been removed because only one promo code is allowed for one item' message should be displayed");
		Reporter.log(
				"17. Enter Promo code which provides less discount than above promo code| Warning message stating 'Promo code already applies to a SKU in the cart' should be displayed"
						+ " and there should be no change in the pricing");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

	}

	/**
	 * US432527: Promo Codes - Applying promo code - Duplicate Code
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEnterDuplicatePromoValueAndCheckWarningMwassage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US432527: Promo Codes - Applying promo code- Duplicate Code");
		Reporter.log("Data Condition | STD PAH Customer with Promo eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Device | PDP page should be displayed");
		Reporter.log("6. Select Payment option | Payment option is selected");
		Reporter.log("7. Click add to cart button | Line selection page should be displayed");
		Reporter.log("8. Select line |  Line Selector Details page is displayed");
		Reporter.log("9. Click skip trade in button | Device Protection page should be displayed");
		Reporter.log("10. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("11. Click OK button on Dont brake the bank modal | Accessory PLP page should be displayed");
		Reporter.log("12. Select Skip Accessories | Cart page should be displayed");
		Reporter.log("13. Verify 'add promo' button | 'add promo' button should be displayed");
		Reporter.log(
				"14. Enter valid promo value and Click 'Apply button'| Applied promo discount should be reflected on CartPage");
		Reporter.log(
				"15. Re-use same promo code and Click 'Apply button'| Warning Icon and Warning message should be displayed and Applied promo code pricing to above order details should not be affected.");
		Reporter.log("16. Enter New promo code | Warning Icon and Warning message should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

	}

	/**
	 * US408910: CART Pricing - Instant Discount FRP/EIP (phone and sim)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.AAL })
	public void testFRPpriceThroughAALFlowAndInstantDiscount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US408910: CART Pricing - Instant Discount FRP/EIP (phone and sim)");
		Reporter.log("Data Condition | STD PAH Customer with AAL eligiblity and Instant discount");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Device | PDP page should be displayed");
		Reporter.log("6. Select Payment option as FRP | Payment FRP option is selected");
		Reporter.log("6. Click on 'ADD A LINE' CTA | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log("7. Verify Instant discount price | Instant discount price should be displayed");
		Reporter.log("8. Click Continue button | Device Protection page should be displayed");
		Reporter.log("9. Click Continue button | Cart page should be displayed");
		Reporter.log("10. Verify 'Full retail' text | 'Full retail' text should be displayed");
		Reporter.log(
				"11. Verify Strike through of original price | Strike through of original price should be displayed");
		Reporter.log("12. Verify New FRP price | New FRP price should be displayed");
		Reporter.log("13. Verify Instant discount price | Instant discount price should be displayed");
		Reporter.log(
				"14. Verify New FRP amount= (Strike through price - Instant discount amount) | FRP Prices should be matched");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US408910: CART Pricing - Instant Discount FRP/EIP (phone and sim)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.AAL })
	public void testEIPpriceThroughAALFlowAndInstantDiscount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US408910: CART Pricing - Instant Discount FRP/EIP (phone and sim)");
		Reporter.log("Data Condition | STD PAH Customer with AAL eligiblity and Instant discount");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Device | PDP page should be displayed");
		Reporter.log("6. Select Payment option as EIP | Payment EIP option is selected");
		Reporter.log("6. Click on 'ADD A LINE' CTA | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log("7. Verify Instant discount price | Instant discount price should be displayed");
		Reporter.log("8. Click Continue button | Device Protection page should be displayed");
		Reporter.log("9. Click Continue button | Cart page should be displayed");
		Reporter.log("10. Verify 'Full retail' text | 'Full retail' text should be displayed");
		Reporter.log(
				"11. Verify Strike through of original price | Strike through of original price should be displayed");
		Reporter.log("12. Verify New FRP price | New FRP price should be displayed");
		Reporter.log("13. Verify Instant discount price | Instant discount price should be displayed");
		Reporter.log("14. Verify 'Due Today' text | 'Due Today' text should be displayed");
		Reporter.log("15. Verify 'Due Today' Price | 'Due Today' price should be displayed");
		Reporter.log("16. Verify 'Due Monthly' text | 'Due Monthly' text should be displayed");
		Reporter.log("17. Verify 'Due Monthly' price | 'Due Monthly' price should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US411412: CART Pricing - Promo Code Pricing FRP/EIP (phone and sim)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.AAL })
	public void testFRPpriceThroughAALFlowAndPromoDiscount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US411412: CART Pricing - Promo Code Pricing FRP/EIP (phone and sim)");
		Reporter.log("Data Condition | STD PAH Customer with AAL eligibility and Promo discount");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Device | PDP page should be displayed");
		Reporter.log("6. Select Payment option as FRP | Payment FRP option is selected");
		Reporter.log("7. Click on ADD A LINE' CTA | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log("8. Click Continue button | Device Protection page should be displayed");
		Reporter.log("9. Click Continue button | Cart page should be displayed");
		Reporter.log("10. Enter valid Promo value | Promo discount price should be displayed");
		Reporter.log("11. Verify 'Full retail' text | 'Full retail' text should be displayed");
		Reporter.log("12. Verify 'Full retail' price | 'Full retail' price should be displayed");
		Reporter.log("13. Verify FRP price | FRP price should be displayed");
		Reporter.log(
				"14. Verify FRP price= (Full retail price - promo discount amount) | FRP Prices should be matched");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US411412: CART Pricing - Promo Code Pricing FRP/EIP (phone and sim)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.AAL })
	public void testEIPpriceThroughAALFlowAndPromoDiscount(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US411412: CART Pricing - Promo Code Pricing FRP/EIP (phone and sim)");
		Reporter.log("Data Condition | STD PAH Customer with AAL eligibility and Promo discount");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Device | PDP page should be displayed");
		Reporter.log("6. Select Payment option as EIP | Payment EIP option is selected");
		Reporter.log("7. Click on 'ADD A LINE' CTA | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log("8. Click Continue button | Device Protection page should be displayed");
		Reporter.log("9. Click Continue button | Cart page should be displayed");
		Reporter.log("10. Enter valid Promo value | Promo discount price should be displayed");
		Reporter.log("11. Verify FRP price | FRP price should be displayed");
		Reporter.log(
				"12. Verify FRP price= (Full retail price - promo discount amount) | FRP Prices should be matched");
		Reporter.log("13. Verify 'Pay Today' text | 'Pay Today' text should be displayed");
		Reporter.log("14. Verify 'Pay Today' Price | 'Pay Today' price should be displayed");
		Reporter.log("15. Verify 'Pay Monthly' text | 'Pay Monthly' text should be displayed");
		Reporter.log("16. Verify 'Pay Monthly' price | 'Pay Monthly' price should be displayed");
		Reporter.log("17. Verify 'FRP price' = (('Pay Monthly') * Instalment Months) |  FRP price should matched ");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US411412: CART Pricing - Promo Code Pricing FRP/EIP (phone and sim)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.AAL })
	public void testFRPpriceThroughAALFlowAndPromoDiscountForSimStarterKit(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US411412: CART Pricing - Promo Code Pricing FRP/EIP (phone and sim)");
		Reporter.log("Data Condition | STD PAH Customer with AAL eligibility and Promo discount for Sim Starter Kit");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Device | PDP page should be displayed");
		Reporter.log("6. Select Payment option as FRP | Payment FRP option is selected");
		Reporter.log("7. Click on ADD A LINE' CTA | Consolidated Rate Plan Page  should be displayed ");
		Reporter.log("8. Verify SimStarter Kit | SimStarter Kit should be displaye");
		Reporter.log("9. Verify SimStarter Kit price | SimStarter Kit price should be displaye");
		Reporter.log("10. Store SimStarter Kit price in V1 | SimStarter Kit price should be Stored in V1");
		Reporter.log("11. Click Continue button | Device Protection page should be displayed");
		Reporter.log("12. Click Continue button | Cart page should be displayed");
		Reporter.log("13. Enter valid Promo value | Promo discount price should be displayed");
		Reporter.log("14. Verify 'Full retail' text | 'Full retail' text should be displayed");
		Reporter.log("15. Verify 'Full retail' price | 'Full retail' price should be displayed");
		Reporter.log("16. Verify FRP price | FRP price should be displayed");
		Reporter.log("17. Verify SimStarterKit text | SimStarterKit text should be displayed");
		Reporter.log(
				"18. Verify SimStarterKit strike through price | SimStarterKit strike through price should be displayed");
		Reporter.log(
				"19. Store SimStarterKit strike through price in V2 | SimStarterKit strike through price should be stored in V2");
		Reporter.log("20. Store SimStarterKit New price in V3 | SimStarterKit New price should be stored in V3");
		Reporter.log("21. Verify V3 = (V2 - Promo Discount)| SimStarterKit New price should matched");
		Reporter.log("22. Verify Promo success message | Promo success message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US449525: Mytmo Accessibility : Review Cart(Shipping info) Error states
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testShippingAddressErrorMessages(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US449525: Mytmo Accessibility : Review Cart(Shipping info) Error states");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select PaymentOption | PaymentOption should be selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select a line |  Line Selector Details page should be displayed");
		Reporter.log("10. Click skip trade in button | Device Protection page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		// Reporter.log("12. Click OK button on Dont brake the bank modal |
		// Accessory
		// PLP page should be displayed");
		Reporter.log("13. Select Skip Accessories | Cart page should be displayed");
		Reporter.log("14. Click Continue to Shipping CTA | Shipping page should be displayed");
		Reporter.log("15. Verify Edit icon presence beside shipping address | Edit icon should be displayed");
		Reporter.log(
				"16. Click edit icon beside shipping address | Shipping address should expand and Address line1, Address line2, City, State, Zipcode fields should be displayed");
		Reporter.log("17. Enter Address fields by leaving any of the field blank | Error warning should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

	}

	/**
	 * US449547: Mytmo Accessibility : Review Cart(Billing) - Error states
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testBillingAddressErrorMessages(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US449547: Mytmo Accessibility : Review Cart(Billing) - Error states");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select PaymentOption | PaymentOption should be selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select a line |  Line Selector Details page should be displayed");
		Reporter.log("10. Click skip trade in button | Device Protection page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		// Reporter.log("12. Click OK button on Dont brake the bank modal |
		// Accessory
		// PLP page should be displayed");
		Reporter.log("13. Select Skip Accessories | Cart page should be displayed");
		Reporter.log("14. Click Continue to Shipping CTA | Shipping page should be displayed");
		Reporter.log("15. Click on 'Continue to Payment' cta on Shipping page | Payment page should be displayed");
		Reporter.log("16. Uncheck 'Same as shipping address' checkbox | Billing address section should be displayed");
		Reporter.log(
				"17. Enter billing address by leaving any of the field blank/Enter incorrect address | Error warning should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

	}

	/**
	 * US449785: Mytmo Accessibility : Review Cart(Payment) - Error states
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPaymentDetailsErrorMessages(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US449785: Mytmo Accessibility : Review Cart(Payment) - Error states");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select PaymentOption | PaymentOption should be selected");
		Reporter.log("8. Click add to cart button | Line selection page should be displayed");
		Reporter.log("9. Select a line |  Line Selector Details page should be displayed");
		Reporter.log("10. Click skip trade in button | Device Protection page should be displayed");
		Reporter.log("11. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("12. Click OK button on Dont brake the bank modal | Accessory PLP page should be displayed");
		Reporter.log("13. Select Skip Accessories | Cart page should be displayed");
		Reporter.log("14. Click Continue to Shipping CTA | Shipping page should be displayed");
		Reporter.log("15. Click on 'Continue to Payment' cta on Shipping page | Payment page should be displayed");
		Reporter.log(
				"16. Enter payment details by leaving any of the field blank/Enter incorrect details | Error warning should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}


	/**
	 * US461806 -[SDET] AAL: Cart Page Phone BYOD flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.AAL })
	public void testCartPageAALBYODFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test : US461806 -[SDET] AAL: Cart Page Phone BYOD flow");
		Reporter.log("Data Condition |  PAH Customer with AAL eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on 'Add A LINE' button | Customer intent page should be displayed");
		Reporter.log("5. Select on 'Bring my own device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log(
				"8. Verify 'DueToday' price with doller symbol | 'DueToday' price with doller symbol should be displayed ");
		Reporter.log(
				"9. Verify 'Sim cost' price with doller symbol | 'Sim cost' price with doller symbol should be displayed ");
		Reporter.log("10. Click on See Details CTA | Sim details should be displayed ");
		Reporter.log("11. Verify Plan Name | Plan Name should be displayed ");
		Reporter.log("12. Verify TE Plan | TI Plan with taxes read not Included should be displayed");
		Reporter.log("13. Click Continue button | Cart page should be displayed");
		Reporter.log("14. Verify Customer Name| Customer Name should be displayed");
		Reporter.log("15. Verify Ship Date | Ship Date  should be displayed");
		Reporter.log("17. Verify Due Today for phone | Due Today for phone  should be displayed");
		Reporter.log(
				"18. Verify Sim sku, Strike through and Image below with sim price| Sim sku, Strike through and Image below with sim price should be displayed");
		Reporter.log("19. Verify Edit Remove CTA below skus | Edit Remove CTA below skus page should be displayed");
		Reporter.log(
				"20. Verify Edit Remove on Device Protection is Not Present | Device Protection should not be displayed");
		Reporter.log("21. Verify Additional Voice Lines Section | Additional Voice Lines Section  should be displayed");
		Reporter.log(
				"22. verify Due Today section, Shippng, Total and sales tax | Due Today section, Shippng, Total and sales taxshould be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US455108:[TEST ONLY] AAL - Deeplink without cookies (through to cart)
	 * TA1910095 : C410990: (NA for App) Ensure that customer is able to reach
	 * the end of VIP AAL flow on MyTMO when deeplinking through TMOBILE.COM AAL
	 * CTA on PDP page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.UAT })
	public void testAALDeeplinkFlowWithOutCookiesAndWithDevice(ControlTestData data, MyTmoData myTmoData,
			TMNGData tMNGData) {
		Reporter.log("Test US455108:[TEST ONLY] AAL - Deeplink without cookies (through to cart)");
		Reporter.log("Data Condition | PAH User For Device");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Launch TMO  | Application Should be Launched");
		Reporter.log("5. click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("6. Select Device | Device PDP page should be displayed");
		Reporter.log("7. Click on 'Add A LINE' button | Rate Plan Page should be displayed");
		Reporter.log("8. Click on Contune button  | Interstitial page should be displayed");
		Reporter.log("9. Verify Skip trade-in tile | Skip trade-in tile should be displayed ");
		Reporter.log("10. Select on 'No thanks, skip trade-in' | PHP page should be displayed");
		Reporter.log("11. Click Continue button | Cart Page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		launchTmngProdURL(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickPhonesLink();
		PlpPage phonesPlpPage = new PlpPage(getDriver());
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.clickOnAddALineBtn();
		ConsolidatedRatePlanPage ratePlanpage = new ConsolidatedRatePlanPage(getDriver());
		ratePlanpage.clickOnContinueCTA();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage").equals("false")) {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
	}

	/**
	 * US505947 :DEFECT: Prod [MyTMO|Shop]: FRP pricing in Legal disclaimer on
	 * CART page is displaying incorrectly.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testFrpPriceInCartPageEipLegalDisclaimer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :US505947 :DEFECT: Prod [MyTMO|Shop]:  FRP pricing in Legal disclaimer on CART page is displaying incorrectly.");
		Reporter.log("Data Condition | PAH User ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones link | PLP page should be displayed");
		Reporter.log("6. Select device in PLP page | PDP page should be displayed");
		Reporter.log("7. Select payment option as Monthly | Monthly option should be selected");
		Reporter.log("8. Get FRP price and saved in V1 | FRP price should be saved in V1");
		Reporter.log("9. Click Add to cart button | Lineselector page should be displayed");
		Reporter.log("10. Select a line | Lineselector details page should be displayed");
		Reporter.log("11. Click on Skip trade-in button | Device protection page should be displayed");
		Reporter.log("12. Click Continue button | Accessories list should be displayed");
		Reporter.log("13. Click Skip accessories button | Cart Page should be displayed");
		Reporter.log("14. Verify EIP legal disclaimer | EIP legal disclaimer should be displayed");
		Reporter.log("15. Verify FRP price in EIP legal disclaimer and Saved in V2 | FRP price should be saved in V2");
		Reporter.log("16. Compare FRP prices(V1, V2) | FRP price should be equal");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
	//	Double v1 = pdpPage.getFRPPDPPage();
		pdpPage.clickAddToCart();

		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();

		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		lineSelectorDetailsPage.clickOnSkipTradeInLink();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyEIPLegalText();
		/*Double v2 = cartPage.verifyFullRetailPriceInPresentInLegalText();
		cartPage.compareDeviceFullRetailAmountInLegalTextAndProductDetail(v1, v2);*/
	}

	/**
	 * TA1655255: E5- Accessory: Desktop: Accessory (Popsokets Mount) are given
	 * FRP pricing in the Cart( Price lockup bar in PLP displays EIP) DE169237 :
	 * E5: Accessory: Desktop: Accessory (Popsokets Mount) are given FRP pricing
	 * in the Cart( Price lockup bar in PLP displays EIP)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testEipOptionForAccessoryDevicesInCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"TA1655255: E5- Accessory: Desktop: Accessory (Popsokets Mount) are given FRP pricing in the Cart( Price lockup bar in PLP displays EIP)");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selection page should be displayed");
		Reporter.log("8. Select a line |  Line Selector Details page should be displayed");
		Reporter.log("9. Click skip trade in button | Device Protection page should be displayed");
		Reporter.log("10. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("11. Select 3 or 4 Accessories | EIP option should be elabled");
		Reporter.log("12. Click Continue button | CartPage should be displayed");
		Reporter.log("13. Verify Accessory device | Accessory device should be displayed in cart page");
		Reporter.log(
				"14. Verify EIP option for Accessory devices | EIP option for Accessory devices should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToAccessoryPLPPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.selectNumberOfAccessoriesToBeEIPeligible();
		accessoryPLPPage.verifyPayInMonthly();
		accessoryPLPPage.clickContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyAccessoryImage();
		cartPage.verifyAccesoryDeviceEipOption();
		cartPage.clickOnRemoveButtonForAccessory();

	}

	/**
	 * US338650 :MyTMO - Additional Terms - AAL - Cart Order Detail: Due Today
	 * Subtotal, Shipping, Sales Tax and Due Today Total US338645 :MyTMO -
	 * Additional Terms - AAL - Cart Order Detail: Product Tile Pricing Display
	 * - No Trade-in
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testDeviceDueTodayAndSubTotalPriceInCartPageForAALflow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US338650 :MyTMO - Additional Terms - AAL - Cart Order Detail: Due Today Subtotal, Shipping, Sales Tax and Due Today Total");
		Reporter.log("Data Condition | PAH user With AAL eligibility");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click Contune button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Select PaymentOption as EIP | EIP PaymentOption should be selected");
		Reporter.log("10. Click Contune button | Interstitial page should be displayed ");
		Reporter.log("11. Verify Skip trade-in tile | Skip trade-in tile should be displayed ");
		Reporter.log("12. Select on 'No thanks, skip trade-in' | PHP page should be displayed");
		Reporter.log("13. Click Continue button | Accessories list should be displayed");
		Reporter.log("14. Click Skip accessories button | Cart Page should be displayed");
		Reporter.log("15. Verify Due Today Subtotal price | Due Today Subtotal price should be displayed");
		Reporter.log("16. Save Due Today Subtotal price in V1 | Due Today Subtotal price should be saved in V1");
		Reporter.log("17. Verify DueToday price | Due Today price should be displayed");
		Reporter.log("18. Save DueToday price in V2 | DueToday price should be saved in V2");
		Reporter.log("19. Verify Shipping charges price | Shipping charges price should be displayed");
		Reporter.log("20. Save Shipping charges price in V3 | Shipping charges price should be saved in V3");
		Reporter.log("21. Verify Sales tax price | Sales tax price should be displayed");
		Reporter.log("22. Save Sales tax price in V4 | Sales tax price should be saved in V4");
		Reporter.log("23. Verify Device DueToday price(V1 = (V2 + V3 + V4)) | Device price should be equal");
		Reporter.log("24. Verify Due Monthly Text | Due Monthly Text should be displayed");
		Reporter.log("25. Verufy No.of Installment Months | No.of Installment Months should be displayed");
		Reporter.log("26. Save Due Monthly Amount in V1 | Due Monthly Amount should be saved in V1");
		Reporter.log("27. Save No.of Installment Months in V2 | No.of Installment Months should be saved in V2");
		Reporter.log("28. Save Down payment price in V4 | Down payment price should be saved in V4");
		Reporter.log(
				"29. Save Device FRP price(After deduction any promotion) in V3 | Device FRP price((After deduction any promotion)) should be saved in V3");
		Reporter.log("30. Compare Device price(V3= V4+(V1* V2)) | Device price should be equal");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageFromAALBuyNewPhoneFlowUsingInterstitialTradeIn(myTmoData);
		cartPage.verifyEstDueTodayIsDisplayed();
		cartPage.verifyEstShippingIsDisplayedInOrderDetailPage();
		cartPage.verifyEstSalesTaxesIsDisplayedInOrderDetail();
		cartPage.verifyOrderEstTotalDueTodayIsDisplayedInOrderDetail();
		Double downPayment = cartPage.getEstDueTodayCart();
		Double shipping = cartPage.getShippingpriceIntegerAALCartPage();
		Double taxesAndFees = cartPage.getTaxesPriceIntegerAALCartPage();
		Double dueTodayTotal = cartPage.getEstTotalDueTodayPriceOrderDetailsCartPage();
		Double totalPrice = downPayment + shipping + taxesAndFees;
		totalPrice = (double) Math.round(totalPrice * 100) / 100;
		cartPage.verifyOrderTotalSum(totalPrice, dueTodayTotal);
		cartPage.verifyEIPAmountAndDuration();
		cartPage.verifyEIPTermIsPresentInLegalText();
		Double V1 = cartPage.getDeviceMonthlyIntegerCartPage();
		Double V2 = cartPage.getEipTermCDevice();
		Double V4 = Double.parseDouble(cartPage.getDeviceDownPayment());
		Double V3 = cartPage.getNewFRPAfterPromoCart();
		cartPage.compareDevicePrice(V3, (V4 + (V1 * V2)));
	}

	/**
	 * US488997 - Order Details- Show EIP Payoff amount
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION})
	public void testOrderDetailsShowEipPayOffAmountandBillCreditInCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US488997 - Order Details- Show EIP Payoff amount");
		Reporter.log("Data Condition | PAH User and User must have EIP payoff offer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button from quicklinks | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page should be displayed");
		Reporter.log("7. Click Continue button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Select Payment Option as EIP | EIP Payment Option should be selected");
		Reporter.log("10. Click Continue button | Interstitial page should be displayed ");
		Reporter.log(
				"11. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
		Reporter.log("12. Verify Authorable title | Authorable title should be displayed");
		Reporter.log("13. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
		Reporter.log("14. Click on Continue button | Device condition page should be displayed");
		Reporter.log(
				"15. Verify good condition authorable title | Good condition authorable title should be displayed");
		Reporter.log("16. Select good condition option | Device value page should be displayed");
		Reporter.log("17. Verify Trade-In this Phone CTA | Trade-In this Phone CTA should be displayed");
		Reporter.log("18. Click on Trade-In this Phone CTA | Device protection page should be displayed");
		Reporter.log("19. Click on Continue button | Accessories Plp page should be displayed");
		Reporter.log("20. Click on Skip Accessories button | Cart page should be displayed");
		Reporter.log(
				"21. Verify 'EIP payoff' text in Price break down | 'EIP payoff' text should be displayed in Price break down");
		Reporter.log(
				"22. Verify 'EIP payoff' price in Price break down | 'EIP payoff' price should be displayed in Price break down");
		Reporter.log("23. Verify 'Bill credit value' details | Trade-in bill credit value details should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();

		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInDeviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();

		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyBillCreditValueText();
	}

	/**
	 * US487387 - Trade in - Order Details - display Recurring Device credit
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testTradeInPromotionsValueAndItsImpactOnMonthlyEIPValueOnCartPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US487387 - Trade in - Order Details - display Recurring Device credit");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button from quicklinks | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page should be displayed");
		Reporter.log("7. Click Continue button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Select Payment Option as EIP | EIP Payment Option should be selected");
		Reporter.log("10. Click Continue button | Interstitial page should be displayed ");
		Reporter.log(
				"11. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
		Reporter.log("12. Verify Authorable title | Authorable title should be displayed");
		Reporter.log("13. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
		Reporter.log("14. Click on Continue button | Device condition page should be displayed");
		Reporter.log(
				"15. Verify good condition authorable title | Good condition authorable title should be displayed");
		Reporter.log("16. Select good condition option | Device value page should be displayed");
		Reporter.log("17. Verify Trade-In this Phone CTA | Trade-In this Phone CTA should be displayed");
		Reporter.log("18. Click on Trade-In this Phone CTA | Device protection page should be displayed");
		Reporter.log("19. Click on Continue button | Accessories Plp page should be displayed");
		Reporter.log("20. Click on Skip Accessories button | Cart page should be displayed");
		Reporter.log(
				"21. Verify TradeIn promotion text presence | Trade In Promotions text should be displayed under Pay Monthly section");
		Reporter.log(
				"22. Verify Promotions monthly discount value | Trade In Promotion price monthly discount value(-$10.00/as per AC) should be displayed under Pay Monthly section");
		Reporter.log(
				"23. Verify Promotions monthly complete breakdown details | The details should match up with the device price after applying TradeIn discount");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.verifyTradeInAnotherDeviceTitle();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInDeviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyPromotionDetailsNonTradeIn();
		cartPage.verifyPromotionDiscountPriceNonTradeIn();
		cartPage.verifyTotalPromotionDiscountPriceNonTradeIn();
		cartPage.verifyMonthlyBreakdownDetailsAreCorrect();

	}

	/**
	 * US487387 - Trade in - Order Details - display Recurring Device credit
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testTradeInPromotionsValueAfterApplyingInstanstDiscountAndItsImpactOnMonthlyEIPOnCartPage(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US487387 - Trade in - Order Details - display Recurring Device credit");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button from quicklinks | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page should be displayed");
		Reporter.log("7. Click Continue button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Select Payment Option as EIP | EIP Payment Option should be selected");
		Reporter.log("10. Click Continue button | Interstitial page should be displayed ");
		Reporter.log(
				"11. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
		Reporter.log("12. Verify Authorable title | Authorable title should be displayed");
		Reporter.log("13. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
		Reporter.log("14. Click on Continue button | Device condition page should be displayed");
		Reporter.log(
				"15. Verify good condition authorable title | Good condition authorable title should be displayed");
		Reporter.log("16. Select good condition option | Device value page should be displayed");
		Reporter.log("17. Verify Trade-In this Phone CTA | Trade-In this Phone CTA should be displayed");
		Reporter.log("18. Click on Trade-In this Phone CTA | Device protection page should be displayed");
		Reporter.log("19. Click on Continue button | Accessories Plp page should be displayed");
		Reporter.log("20. Click on Skip Accessories button | Cart page should be displayed");
		Reporter.log(
				"21. Verify instant discount text available for the device | Instanct discount should be applied and it should be reflected for the device");
		Reporter.log(
				"22. Verify TradeIn promotion text presence | Trade In Promotions text should be displayed under Pay Monthly section after applying Instant discount");
		Reporter.log(
				"23. Verify Promotions monthly discount value | Trade In Promotion price monthly discount value(-$10.00/as per AC) should be displayed under Pay Monthly section");
		Reporter.log(
				"24. Verify Promotions monthly complete breakdown details | The details should match up with the device price after applying TradeIn discount");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.verifyTradeInAnotherDeviceTitle();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInDeviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyPromotionDetailsNonTradeIn();
		cartPage.verifyPromotionDiscountPriceNonTradeIn();
		cartPage.verifyTotalPromotionDiscountPriceNonTradeIn();
		cartPage.verifyMonthlyBreakdownDetailsAreCorrect();

	}

	/**
	 * US541990:myTMO > Handoff > AAL > Consolidated Charges page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCustomerNavigatedToCartPageWithUnKnownIntent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US541990:myTMO > Handoff > AAL > Consolidated Charges page");
		Reporter.log("Data Condition | PAH User with AAl eligible");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones button | UNO PLP page should be displayed");
		Reporter.log("6. Select device | UNO PDP page should be displayed");
		Reporter.log("7. Verify Product family name and Saved in V1 | Product family name should be saved in V1");
		Reporter.log("8. Verify Price options for EIP and FRP | Device price FRP and EIP options should be displayed");
		Reporter.log("9. Click on AAL button | Consolidated Rate Plan Page should be displayed");
		Reporter.log("10. Click Continue button | Interstitial page should be displayed ");
		Reporter.log("11. Select on 'No thanks, skip trade-in' | Device Protection page should be displayed");
		Reporter.log("12. Click Continue button | Cart page should be displayed");
		Reporter.log("13. Verify Product family name | Product family name should be same as V1");
		Reporter.log("14. Verify Price options for EIP and FRP | Device price FRP and EIP options should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US541990:myTMO > Handoff > AAL > Consolidated Charges page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCustomerNavigatedToCartPageWithKnownIntent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US541990:myTMO > Handoff > AAL > Consolidated Charges page");
		Reporter.log("Data Condition | PAH User with AAl eligible");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on AAL link | Device Intent page should be displayed");
		Reporter.log("6. Select 'Buy a new phone' option | UNO PLP page should be displayed");
		Reporter.log("7. Select device | UNO PDP page should be displayed");
		Reporter.log("8. Verify Product family name and Saved in V1 | Product family name should be saved in V1");
		Reporter.log("9. Verify Price options for EIP and FRP | Device price FRP and EIP options should be displayed");
		Reporter.log("10. Click on Continue button | Consolidated Rate Plan Page should be displayed");
		Reporter.log("11. Click on Continue button | Interstitial page should be displayed");
		Reporter.log("12. Select on 'No thanks, skip trade-in' | Device Protection page should be displayed");
		Reporter.log("13. Click Continue button | Cart page should be displayed");
		Reporter.log("14. Verify Product family name | Product family name should be same as V1");
		Reporter.log("15. Verify Price options for EIP and FRP | Device price FRP and EIP options should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US545760 - Display monthly credit in device tile if promo requirements
	 * are met - AAL/Upgrade
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPromoDetailsInCartPageForAALFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case US545760 - Display monthly credit in device tile if promo requirements are met - AAL/Upgrade");
		Reporter.log("Data Condition | PAH Customer and AAL eligiblity and User must have Promo Offer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on 'Add A LINE' button | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click 'Continue' button on Rate Plan page | PLP page should be displayed");
		Reporter.log("8. Select a device on PLP | PDP should be displayed");
		Reporter.log("9. Select 'Add to Cart' cta | Interstitial page should be displayed");
		Reporter.log("10. Click 'Skip Trade in' option | Device Protection page should be displayed");
		Reporter.log("11. Click 'Continue' button on device protection page | Cart page should be displayed");
		Reporter.log("12. Verify Promotion details | Promotion details should be displayed in CartPage");
		Reporter.log(
				"13. Verify Monthly Price after Promo subtraction amount | Monthly price should be displayed correctly");
		Reporter.log("14. Verify Total Price after Promo discount amount | Total price should be displayed correctly");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageFromAALNonBYODFlow(myTmoData);
		cartPage.verifyCartPage();
		cartPage.verifyPromotionDetailsNonTradeIn();
		cartPage.verifyPromotionDiscountPriceNonTradeIn();
		cartPage.verifyTotalPromotionDiscountPriceNonTradeIn();

	}

	/**
	 * US559489 - Tier6 : Standard Upgrade Enrollment C498548 - Standard upgrade
	 * Enrollment with tier 6 eligible device
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TIER_SIX })
	public void testDeviceProtectionSocWithDefaultOptionInPHPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("TestCase:US559489 - Tier6 : Standard Upgrade Enrollment");
		Reporter.log("Data Condition | PAH user and User should not have any Protection Soc");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Device | PDP page should be displayed");
		Reporter.log("6. Click on 'Add to cart' CTA | LineSelection Page  should be displayed ");
		Reporter.log("7. Select a line | LineSelector details page should be displayed");
		Reporter.log("8. Click on Skip trade-in option | PHP page should be displayed");
		Reporter.log("9. Save Default selected SOC name in V1 | Default selected soc should be saved in V1");
		Reporter.log("10. Save Default selected SOC price in V2 | Default selected soc should be saved in V2");
		Reporter.log("11. Click Continue button with default option | Accessory plp page should be displayed");
		Reporter.log("12. Click on Skip accessory  | CartPage should be displayed");
		Reporter.log(
				"13. Verify Tire 6 Soc in cartPage | Tire 6 Soc should be displayed in cartPage and it should be same as V1");
		Reporter.log("14. Verify Soc price | Soc price should be displayed and it should be same as V2");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(
				myTmoData);
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickOtherProtectionOption();
		deviceProtectionPage.verifyDeviceProtectionPage();
		String NameV1 = deviceProtectionPage.getNameforDefaultSelectedSOC();
		String PriceV1 = deviceProtectionPage.getPriceforDefaultSelectedSOC();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyDeviceProtectionSelectedSoc();
		String NameV2 = cartPage.getPHPSelectedSocName();
		String PriceV2 = cartPage.getPHPSelectedSocPrice();
		cartPage.compareTwoStrings(NameV1, NameV2);
		cartPage.compareTwoStrings(PriceV1, PriceV2);
	}

	/**
	 * US559489 - Tier6 : Standard Upgrade Enrollment C498548 - Standard upgrade
	 * Enrollment with tier 6 eligible device
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {	Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS})
	public void testDeviceProtectionSocWithOtherOptionInPHPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("TestCase:US559489 - Tier6 : Standard Upgrade Enrollment");
		Reporter.log("Data Condition | PAH user and User should not have any Protection Soc and Non NY customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Device | PDP page should be displayed");
		Reporter.log("6. Click on 'Add to cart' CTA | LineSelection Page  should be displayed ");
		Reporter.log("7. Select a line | LineSelector details page should be displayed");
		Reporter.log("8. Click on Skip trade-in option | PHP page should be displayed");
		Reporter.log("9. Click on Other protection options | All Device protection options should be displayed");
		Reporter.log(
				"10. Verify 'I don't need protection for my phone' radio button | 'I don't need protection for my phone' radio button should be available");
		Reporter.log(
				"11. Select one device protection option and Saved in V1 | Selected Device protection Soc should be saved in V1");
		Reporter.log("12. Click Continue button  | Accessory plp page should be displayed");
		Reporter.log("13. Click on Skip accessory  | CartPage should be displayed");
		Reporter.log(
				"14. Verify Device protection Soc | Device protection Soc should be displayed in cartPage and it should be same as V1");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(
				myTmoData);
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.verifyOtherProtectionOption();
		deviceProtectionPage.clickOtherProtectionOption();
		deviceProtectionPage.verifyIDOntNeedProtectionSOC();
		deviceProtectionPage.verifyIDontNeedProtectionRadioButton();
		deviceProtectionPage.selectTier6SocNonNY();
		String NameV1 = deviceProtectionPage.getNameforDefaultSelectedSOC();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyDeviceProtectionSelectedSoc();
		String NameV2 = cartPage.getPHPSelectedSocName();
		cartPage.compareTwoStrings(NameV1, NameV2);
	}

	/**
	 * US559489 - Tier6 : Standard Upgrade Enrollment C498548 - Standard upgrade
	 * Enrollment with tier 6 eligible device
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, Group.TIER_SIX })
	public void testUserNavigateToCartPageWithDeviceProtectionAndEditDeviceProtectionSocInCartPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("TestCase:US559489 - Tier6 : Standard Upgrade Enrollment");
		Reporter.log("Data Condition | PAH user and User should not have any Protection Soc");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Device | PDP page should be displayed");
		Reporter.log("6. Click on 'Add to cart' CTA | LineSelection Page  should be displayed ");
		Reporter.log("7. Select a line | LineSelector details page should be displayed");
		Reporter.log("8. Click on Skip trade-in option | PHP page should be displayed");
		Reporter.log("9. Verify Default selected SOC | Default selected SOC should be selected");
		Reporter.log("10. Click Continue button with default option | Accessory plp page should be displayed");
		Reporter.log("11. Click on Skip accessory  | CartPage should be displayed");
		Reporter.log("12. Click on Edit button for Device protection Soc | PHP page should be displayed");
		Reporter.log("13. Select different Soc and Saved in V1 | Selected Soc should be saved in V1");
		Reporter.log("14. Click on Continue button | Accessory plp page should be displayed");
		Reporter.log("15. Click on Skip accessory  | CartPage should be displayed");
		Reporter.log(
				"16. Verify Selected Device protection Soc in cart page | Device protection Soc should displayed  and same as V1");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(
				myTmoData);
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyDeviceProtectionSelectedSoc();
		cartPage.clickEditInsuranceSoc();
		deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.selectTier6SocNonNY();
		String NameV1 = deviceProtectionPage.getNameforDefaultSelectedSOC();
		deviceProtectionPage.clickContinueButton();
		accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();
		cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyDeviceProtectionSelectedSoc();
		String NameV2 = cartPage.getPHPSelectedSocName();
		cartPage.compareTwoStrings(NameV1, NameV2);
	}

	/**
	 * US559491 - Tier6 : Standard Upgrade Migration C500543: Desktop_04_Std
	 * Upgrade_NonNY_GSM_Migration_With FRP Tier 6 to Tier 2 P3602
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TIER_SIX })
	public void testTierSixSocsInCartPageForFRPPaymentOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("TestCase:US559491 - Tier6 : Standard Upgrade Migration");
		Reporter.log("Data Condition | PAH user and User should have Tire-6 soc for DeskTop");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Deive which is compatible for Tier 2 soc | PDP page should be displayed");
		Reporter.log("6. Select FRP from payment option | FRP Payment option should be selected");
		Reporter.log("7. Click on 'Add to cart' CTA | LineSelection Page  should be displayed ");
		Reporter.log("8. Select a line | LineSelector details page should be displayed");
		Reporter.log("9. Click on Skip trade-in option | PHP page should be displayed");
		Reporter.log("10. Verify Tier 6 socs displayed as previous | Tier 6 socs should be displayed as previous");
		Reporter.log("11. Save new insurance soc price in V1 | New Soc price should be saved in V1");
		Reporter.log("12. Click Continue button | Accessory plp page should be displayed");
		Reporter.log("13. Click on Skip accessory | CartPage should be displayed");
		Reporter.log("14. Verify added Tire 2 soc in cartPage | Tire 2 soc should be displayed in cart page");
		Reporter.log(
				"15. Verify added Tire 2 soc price in cartPage  | Tire 2 soc price should be displayed and It's equal to V1");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(
				myTmoData);
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.verifyPrevoiusSOCName();
		deviceProtectionPage.verifyPrevoiusSOCPrice();
		deviceProtectionPage.verifyPrevoiusSOCNameTier6();
		String PriceV1 = deviceProtectionPage.getPriceforDefaultSelectedSOC();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesLink();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		String PriceV2 = cartPage.getPHPSelectedSocPrice();
		cartPage.compareTwoStrings(PriceV1, PriceV2);
	}

	/**
	 * TA1904786: Preprod:E5:Total monthly calculation is coming incorrect when
	 * user is adding or deleting the in line accessories on upgrade flow cart
	 * page DE240116 : Preprod:E5:Total monthly calculation is coming incorrect
	 * when user is adding or deleting the in line accessories on upgrade flow
	 * cart page
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testTotalMonthlyPriceWhenUserAddingOrDeletingAccessories(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test case: TA1904786: Preprod:E5:Total monthly calculation is coming incorrect when user is adding or deleting the in line accessories on upgrade flow cart page");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Select device from feature device | PDP page should be displayed");
		Reporter.log("5. Click Add to cart button | LineSelector page should be displayed");
		Reporter.log("6. Select a line | LineSelector details page should be displayed");
		Reporter.log("7. Click on Skip trade-in button | Device Protection page should be displayed");
		Reporter.log("8. Click on Continue button | Accessory list page should be displayed");
		Reporter.log(
				"9. Select accessorys and FRP price is more than $69 | Pay in full price should be displayed more than $69");
		Reporter.log("10. Click Continue button | Cart page should be displayed");
		Reporter.log(
				"11. Save before deleting accessory due today price in V1 | Due today price should be saved in V1");
		Reporter.log("12. Remove 1 accessory | Remove accessory model should be displayed");
		Reporter.log("13. Click on Accessories model Continue shopping CTA | Accessories plp page should be displayed");
		Reporter.log(
				"14. Save Accessories pay in full price in V2 | Accessories pay in full price should be saved in V2");
		Reporter.log("15. Click on Continue button | Cart page should be displayed");
		Reporter.log("16. Save after deleting accessory due today price in V3 | Due today price should be saved in V3");
		Reporter.log("17. Comapre Due today price | Due today price should be updated");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToAccessoryPLPPageByFeaturedDevicesWithSkipTradeIn(myTmoData);
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.selectNumberOfAccessoriesToBeEIPeligible();
		accessoryPLPPage.clickContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		Double dueTodayPriceBeforeDeletingAccessory = cartPage.getEstDueTodayIntegerUpgradeCart();
		cartPage.clickOnRemoveButtonForAccessory();
		cartPage.verifyRemoveAccessoryModelWindow();
		cartPage.clickAccessoriesModelContinueShoppingCTA();
		accessoryPLPPage.verifyAccessoryPLPPage();
		Double payInFullPrice = accessoryPLPPage.getEIPTotalPriceInteger();
		accessoryPLPPage.clickContinueButton();
		cartPage.verifyCartPage();
		Double dueTodayPriceAfterDeletingAccessory = cartPage.getEstDueTodayIntegerUpgradeCart();
		cartPage.compareDueTodayPrice(dueTodayPriceAfterDeletingAccessory, dueTodayPriceBeforeDeletingAccessory,
				payInFullPrice);

	}
	/**
	 * US602156:Accessories > Standard Upgrade > Configure EIP amount
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testEIPAmountForAccessoriesWithStandardUpgrade(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US602156:Accessories > Standard Upgrade > Configure EIP amount");
		Reporter.log("Data Condition | PAH user, And User should have $49 accessory device");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selection page should be displayed");
		Reporter.log("8. Select line | Line selector details page should be displayed");
		Reporter.log("9. Click skip trade in button | Device protection page should be displayed");
		Reporter.log("10. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log(
				"11. Select accessories by checking checkbox (minimum purchase over $49) |  Verify sticky banner is displayed");
		Reporter.log("12. Verify EIP Price for Accessories on sticky banner | EIP Pricing should be displayed ");
		Reporter.log("13. Verify cancellation legal text | Cancellation legal text should be loaded");
		Reporter.log("14. Click on continue CTA | Cart page should be loaded");
		Reporter.log(
				"15. Verify Pricing for Accessories | EIP Pricing with cancellation legal text should be displayed for each Accessory");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		DeviceProtectionPage deviceProtectionPage = navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(
				myTmoData);
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessoriesMoreThan49();
		accessoryPLPPage.verifyStickyBanner();
		accessoryPLPPage.verifyCancellationLegalText();
		accessoryPLPPage.clickContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyAccesoryDeviceEipOption();
		cartPage.verifyAccessoryEIPPriceIsDisplayed();
		cartPage.verifyEIPLegalText();

	}

	
	/**
	 * US577985:Day 1 Fee: No SSK fee displayed in Cart for a user adding a line when buying a device
	 * US616175:{SDET ONLY] US577985: Day 1 Fee: No SSK fee displayed in Cart for a user adding a line when buying a device
	 * 
	 * T1414083:Validate if the user is not charged for Sim Starter Kit in the cart 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS })
	public void testSimCardPriceInCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US577985:Day 1 Fee: No SSK fee displayed in Cart for a user adding a line when buying a device");
		Reporter.log("Data Condition | PAH user, And User should have AAL eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");		
		Reporter.log("5. Click on Add a line button | Device-Intent page should be displayed");
		Reporter.log("6. Select 'Buy a new phone' option | RatePlan page should be displayed");
		Reporter.log("7. Click on Continue button | PLP page should be displayed");
		Reporter.log("8. Select phone | PDP page should be displayed");
		Reporter.log("9. Click 'Add to Cart' CTA | Trade In page should be displayed");
		Reporter.log("10. Click Skip Trade In CTA | PHP page should be displayed");
		Reporter.log("11. Click Continue CTA | Cart page should be displayed");
		Reporter.log("12. Verify Sim Starter Kit | Sim Starter Kit should not be displayed");
		Reporter.log("13. Verify Sim Starter Kit Fee | Sim Starter Kit Fee should not be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		//CartPage cartPage = navigateToCartPageFromAALBYODFlow(myTmoData);
		CartPage cartPage = navigateToCartPageFromAALNonBYODFlow(myTmoData);
		cartPage.verifyCartPage();
		cartPage.verifySimStarterKitPriceNotDisplayed();
	}

	/**
	 * Test cart pae webelements
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true )
	public void testCartPageElements(ControlTestData data, MyTmoData myTmoData) {


		navigateToShopPage(myTmoData);
	//	cartPage.verifyPageElements();

	}



/**
 * Retired US In this Page: US163984, US220526(Duplicate), US231307, US259561,
 * US342373
 */

/**
 * US638699: UI - MyTMO R3 - Technical-  Update UNO cookie information from intent to transactionType
 * @param data
 * @param myTmoData
 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUpdateUNOCookieInformationFromIntentToTransactionType(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US638699: UI - MyTMO R3 - Technical-  Update UNO cookie information from intent to transactionType");
		Reporter.log("Data Condition | Std PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");	
		Reporter.log("5. Click on see all phones | PLP page should be displayed");
		Reporter.log("6. Select Device | PDP page should be displayed");
		Reporter.log("7. Select Payment option as FRP | Payment FRP option is selected");
		Reporter.log("8. Click on 'Update' CTA | Line selector Page  should be displayed ");
		Reporter.log("9. Click on first line | Line selector details page should be displayed");
		Reporter.log("10. Click on skip tradein | Device protection page should be displayed");
		Reporter.log("11. Click Continue button | Accessories list page should be displayed");
		Reporter.log("12. Click on skip accessories link | Cart page should be displayed");
		Reporter.log("13. Remove select device in cart page | Remove item model should be displayed");
		Reporter.log("14. Click on 'yes,remove item' CTA | Empty cart page should be displayed");
		Reporter.log("15. Click on shop phones CTA | shop page should be displayed");
		Reporter.log("16. Click on see all phones link | UNO PLP page should be displayed");
		Reporter.log("17. Select device in UNO PLP page | UNO PDP page should be displayed");
		Reporter.log("18. pass the intent value to the transactionType(Unknown) | intent value to transactionType(Unknown) should be passed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		
		

	}

	/**
	 * US638700: MyTMO R3 - Technical - Update UNO PDP redirection URLs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUpadateUNOPDPRedirectionToURLThroughCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US638700: MyTMO R3 - Technical - Update UNO PDP redirection URLs");
		Reporter.log("Data Condition | Std PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | PLP page should be displayed");
		Reporter.log("6. Select on Device | PDP page should be displayed");
		Reporter.log("7. Select Payment option as FRP | Payment FRP option is selected");
		Reporter.log("8. Click on 'Update' CTA | Line selector Page  should be displayed ");
		Reporter.log("9. Click on first line | Line selector details page should be displayed");
		Reporter.log("10. Click on skip tradein | Device protection page should be displayed");
		Reporter.log("11. Click Continue button | Accessories list page should be displayed");
		Reporter.log("12. Click on skip accessories link | Cart page should be displayed");
		Reporter.log("13. Click on edit cart CTA | UNO PDP page should be displayed");
		Reporter.log("14. Send the SKU in PDP page Redirection URLs | UNO PDP redirection URLs should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

	}

	/**
	 * CDCSM-151, TR-2641, TC-1705, TC-1702, TC-1708, TC-1706 
	 * US611515 - As an authenticated user, I want to be able to navigate back to the PDP when I edit
	 * my device in cart for an AAL transaction, so that my intent is maintained for
	 * a specific transaction.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEditCartTradeInUnoPDPSKUInfo(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US611515 - As an authenticated user, I want to be able to navigate back to the PDP when I edit my device in cart for an AAL transaction, so that my intent is maintained for a specific transaction.");
		Reporter.log("Data Condition | Std PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select on Device | UNO PDP page should be displayed");
		Reporter.log("8. Click on 'ADD A LINE' CTA | Consolidated charges page should be displayed");
		Reporter.log("9. Click on Continue on Consolidated charges page| interstitial-tradein page should be displayed");
		Reporter.log("10. Click Yes Trade In My device | tradeInDeviceConditionValue page should be displayed");
		Reporter.log("11. Choose Good Options and click Continue |tradeIn Device Condition Value Confirmation page should be displayed");
		Reporter.log("12. Click Continue on Confirmation Page | Device Protection page should be displayed");
		Reporter.log("13. Click on Continue on Device protection Page | Cart Page should be displayed");
		Reporter.log("14. Click on EDIT in Cart Page | UNO PDP Page should be displayed With Upgrade CTA Displayed");
		Reporter.log("15. Click on Different Color and Save In v1| Color should be Choosen on UNO PDP");
		Reporter.log("16. Click on Different Memory and Save In v2| Memory should be Choosen on UNO PDP");
		Reporter.log("17. Save Updated Device Price In v3| Device Price should be saved on UNO PDP");
		Reporter.log("18. Click on ADD To Cart | Consolidated rate plan page should be displayed");
		Reporter.log("19. Click on Continue on Consolidated charges page| interstitial-tradein page should be displayed");
		Reporter.log("20. Click Yes Trade In My device | tradeInDeviceConditionValue page should be displayed");
		Reporter.log("21. Choose Good Options and click Continue |tradeIn Device Condition Value Confirmation page should be displayed");
		Reporter.log("22. Click Continue on Confirmation Page | Device Protection page should be displayed");
		Reporter.log("23. Click on Continue on Device protection Page | Cart Page should be displayed");
		
		Reporter.log("24. Save Color in v4 and Compare with v1 |Device Color should match from pdp");
		Reporter.log("25. Save Memory in v5 and Compare with v2 |Device Memory should match from pdp");
		Reporter.log("26. Save Device Price In Cart v6 and Compare with v3| Device Price should match from pdp");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		 
		UNOPDPPage unoPDPPage = navigateToUNOPDPPageByFeatureDevices(myTmoData, myTmoData.getDeviceName());
		unoPDPPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();		
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.verifyTradeInAnotherDeviceTitle();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInDeviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickEditButtonOnDevice();
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyUpGradeCTAEnabled();
		//unoPDPPage.clickColorOnUNOPDP();			
		String deviceColorInUNOPDPPage = unoPDPPage.getDeviceColor();		
		unoPDPPage.selectMemoryOnUNOPDP(myTmoData.getDeviceMemory());
		String deviceMemoryInUNOPDPPage = unoPDPPage.getDeviceMemory();
		String deviceFRPPriceInUNOPDPPage = unoPDPPage.getFRPPrice();
		unoPDPPage.clickOnAddToCartCTA();
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();		
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.verifyTradeInAnotherDeviceTitle();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();		
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		
		tradeInDeviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();
		
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();				
		cartPage.verifyCartPage();
		String deviceColorInCartPage = cartPage.getDeviceColor();
		String deviceMemoryInCartPage = cartPage.getDeviceMemory();
		String deviceFRPPriceInCartPage = cartPage.getDeviceFRP();		
		cartPage.compareDeviceColor(deviceColorInCartPage,deviceColorInUNOPDPPage);
		cartPage.compareDeviceMemory(deviceMemoryInCartPage,deviceMemoryInUNOPDPPage);	
		cartPage.compareDeviceFRPPrice(deviceFRPPriceInCartPage,deviceFRPPriceInUNOPDPPage);	
	}

	/**
	 * CDCSM-151, TC-1703 US611515 - As an authenticated user, I want to be able to
	 * navigate back to the PDP when I edit my device in cart for an AAL
	 * transaction, so that my intent is maintained for a specific transaction.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEditCartTradeInUnoPDPCompareSkuInCart(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US611515 - As an authenticated user, I want to be able to navigate back to the PDP when I edit my device in cart");
		Reporter.log("Data Condition | Std PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select on Device | UNO PDP page should be displayed");
		Reporter.log("8. Click on 'ADD A LINE' CTA |consolidated charges page should be displayed");
		Reporter.log("9. Click on Continue on Consolidated charges page| interstitial-tradein page should be displayed");
		Reporter.log("10. Click Yes Trade In My device | tradeInDeviceConditionValue page should be displayed");
		Reporter.log("11. Choose Good Options and click Continue |tradeIn Device Condition Value Confirmation page should be displayed");
		Reporter.log("12. Click Continue on Confirmation Page | Device Protection page should be displayed");
		Reporter.log("13. Click on Continue on Device protection Page | Cart Page should be displayed");
		Reporter.log("14. Click on EDIT in Cart Page | UNO PDP Page should be displayed With Upgrade CTA Displayed");
		Reporter.log("15. Save Color and Save In v1| Color should be saved on UNO PDP");
		Reporter.log("16. Save Device Memory and Save In v2| Memory should be saved on UNO PDP");
		Reporter.log("16. Save Device Price In v3| Device Price should be saved on UNO PDP");
		Reporter.log("17. Click on ADD To Cart | Cart Page Should be displayed");
		Reporter.log("17. Save Color in v4 and Compare with v1 |Device Color should match from pdp");
		Reporter.log("17. Save Memory in v5 and Compare with v2 |Device Memory should match from pdp");
		Reporter.log("16. Save Device Price In Cart v6 and Compare with v3| Device Price should match from pdp");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		UNOPDPPage unoPDPPage = navigateToUNOPDPPageByFeatureDevices(myTmoData, myTmoData.getDeviceName());
		unoPDPPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.verifyTradeInAnotherDeviceTitle();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInDeviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickEditButtonOnDevice();
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyUpGradeCTAEnabled();					
		String deviceColorInUNOPDPPage = unoPDPPage.getDeviceColor();			
		String deviceMemoryInUNOPDPPage = unoPDPPage.getDeviceMemory();
		String deviceFRPPriceInUNOPDPPage = unoPDPPage.getDeviceFRP();
		//String deviceFRPPriceInUNOPDPPage = unoPDPPage.getFRPPrice();
		unoPDPPage.clickOnAddToCartCTA();
		cartPage.verifyCartPage();
		String deviceColorInCartPage = cartPage.getDeviceColor();
		String deviceMemoryInCartPage = cartPage.getDeviceMemory();
		String deviceFRPPriceInCartPage = cartPage.getDeviceFRP();		
		cartPage.compareDeviceColor(deviceColorInCartPage,deviceColorInUNOPDPPage);
		cartPage.compareDeviceFRPPrice(deviceFRPPriceInCartPage,deviceFRPPriceInUNOPDPPage);
		cartPage.compareDeviceMemory(deviceMemoryInCartPage,deviceMemoryInUNOPDPPage);				
	}

	

	/**
	 * CDCSM-151, TC-1703 US611515 - As an authenticated user, I want to be able to
	 * navigate back to the PDP when I edit my device in cart for an AAL
	 * transaction, so that my intent is maintained for a specific transaction.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEditCartSkipTradeInUnoPDPCompareSkuInCart(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US611515 - As an authenticated user, I want to be able to navigate back to the PDP when I edit my device in cart");
		Reporter.log("Data Condition | Std PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select on Device | UNO PDP page should be displayed");
		Reporter.log("8. Click on 'ADD A LINE' CTA |consolidated charges page should be displayed");
		Reporter.log("9. Click on Continue on Consolidated charges page| interstitial-tradein page should be displayed");
		Reporter.log("10. Click Skip TradeIn | Device Protection page should be displayed\"");
		Reporter.log("11. Click on Continue on Device protection Page | Cart Page should be displayed");
		Reporter.log("12. Click on EDIT in Cart Page | UNO PDP Page should be displayed With Upgrade CTA Displayed");
		Reporter.log("13. Save Color and Save In v1| Color should be saved on UNO PDP");
		Reporter.log("14. Save Device Memory and Save In v2| Memory should be saved on UNO PDP");
		Reporter.log("15. Save Device Price In v3| Device Price should be saved on UNO PDP");
		Reporter.log("16. Click on ADD To Cart | Cart Page Should be displayed");
		Reporter.log("17. Save Color in v4 and Compare with v1 |Device Color should match from pdp");
		Reporter.log("18. Save Memory in v5 and Compare with v2 |Device Memory should match from pdp");
		Reporter.log("19. Save Device Price In Cart v6 and Compare with v3| Device Price should match from pdp");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		UNOPDPPage unoPDPPage = navigateToUNOPDPPageByFeatureDevices(myTmoData, myTmoData.getDeviceName());
		unoPDPPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickEditButtonOnDevice();
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyUpGradeCTAEnabled();
		unoPDPPage.chooseColor(myTmoData.getDeviceColor());
		String deviceColorInUNOPDPPage = unoPDPPage.getDeviceColor();		
		unoPDPPage.selectMemoryOnUNOPDP(myTmoData.getDeviceMemory());
		String deviceMemoryInUNOPDPPage = unoPDPPage.getDeviceMemory();
		String deviceFRPPriceInUNOPDPPage = unoPDPPage.getDeviceFRP();
		unoPDPPage.clickOnAddToCartCTA();
		consolidatedRatePlanPage.clickOnContinueCTA();		
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();		
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();		
		cartPage.verifyCartPage();
		String deviceColorInCartPage = cartPage.getDeviceColor();
		String deviceMemoryInCartPage = cartPage.getDeviceMemory();
		String deviceFRPPriceInCartPage = cartPage.getDeviceFRP();		
		cartPage.compareDeviceColor(deviceColorInCartPage,deviceColorInUNOPDPPage);
		cartPage.compareDeviceMemory(deviceMemoryInCartPage,deviceMemoryInUNOPDPPage);	
		cartPage.compareDeviceFRPPrice(deviceFRPPriceInCartPage,deviceFRPPriceInUNOPDPPage);	
	}
	
	/**
	 * CDCSM-149 :US616355 - As an authenticated user I want to be able to navigate back to the PDP 
	 * when I edit my device in cart for an upgrade transaction, 
	 * so that my intent is maintained for a specific transaction. 
	 * TC- 1586 : Desktop: Validate that edit flow in upgrade with no changes navigates back to cart directly	 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testUserNavigatedTOCartPageWithOutSKUInformationEdit(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: TC- 1586 CDCSM-149 :US616355 - As an authenticated user I want to be able to navigate back to the PDP \n" + 
				"	 * when I edit my device in cart for an upgrade transaction, \n" + 
				"	 * so that my intent is maintained for a specific transaction. ");
		Reporter.log("Data Condition | Std PAH User");
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Select device from featured device | UNO PDP page should be displayed");
		Reporter.log("4. Click on Upgrade CTA | Line selector page should be displayed");
		Reporter.log("5. Select Line | Line selector details page should be displayed");
		Reporter.log("6. Click on trade in this phone CTA | TradeIn device condition value page should be displayed");
		Reporter.log("7. Select good device options and Click on Continue CTA | TradeIn device condition value confirmation page should be displayed");
		Reporter.log("8. Click on trade in this phone CTA | PHP page should be displayed");
		Reporter.log("9. Click on Continue CTA | Accessory list page should be displayed");
		Reporter.log("10. Select assessory device and Click on Continue CTA | Cart page should be displayed");
		Reporter.log("11. Click on EDIT CTA | UNOPDP page should be displayed");
		Reporter.log("12. Click on Add to cart CTA | Cart page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		UNOPDPPage unoPDPPage = navigateToUNOPDPPageByFeatureDevices(myTmoData, myTmoData.getDeviceName());
		unoPDPPage.clickOnAddToCartCTA();		
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();		
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickOnTradeInThisPhone();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(getDriver());
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInDeviceConditionConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();
		tradeInDeviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());		
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories("1");
		accessoryPLPPage.clickContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickEditButtonOnDevice();
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.clickOnAddToCartCTA();
		cartPage.verifyCartPage();
	}
	
	/**
	 * CDCSM-149 :US616355 - As an authenticated user I want to be able to navigate back to the PDP 
	 * when I edit my device in cart for an upgrade transaction, 
	 * so that my intent is maintained for a specific transaction. 
	 * TC- 1587 : Desktop: Validate that edit flow in upgrade with  changes to Sku color navigates back to cart with all the flows
	 * TC- 1591 : Desktop: Validate that edit CTA on cart in upgrade flow takes user back to PDP page  with no bread crumbs	 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION})
	public void testUserNavigatedTOCartPageWithSKUInformationEdit(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: TC- 1587 CDCSM-149 :US616355 - As an authenticated user I want to be able to navigate back to the PDP \n" + 
				"	 * when I edit my device in cart for an upgrade transaction, \n" + 
				"	 * so that my intent is maintained for a specific transaction. ");
		Reporter.log("Data Condition | Std PAH User");
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Select device from featured device | UNO PDP page should be displayed");
		Reporter.log("4. Click on Upgrade CTA | Line selector page should be displayed");
		Reporter.log("5. Select Line | Line selector details page should be displayed");
		Reporter.log("6. Click on trade in this phone CTA | TradeIn device condition value page should be displayed");
		Reporter.log("7. Select good device options and Click on Continue CTA | TradeIn device condition value confirmation page should be displayed");
		Reporter.log("8. Click on trade in this phone CTA | PHP page should be displayed");
		Reporter.log("9. Click on Continue CTA | Accessory list page should be displayed");
		Reporter.log("10. Select assessory device and Click on Continue CTA | Cart page should be displayed");
		Reporter.log("11. Save Device color in V1 | Device color should be saved in V1");
		Reporter.log("12. Save Device memory in V2 | Device memory should be saved in V2");		
		Reporter.log("13. Click on EDIT CTA | UNOPDP page should be displayed");
		Reporter.log("14. Verify Add to Cart CTA is enabled | Add to Cart CTA should be enabled");		
		Reporter.log("15. Select device color | Device color should be selected");
		Reporter.log("16. Select device memory | Device memory should be selected");		
		Reporter.log("17. Cilck on Add to Cart CTA | Lineselector page should be displayed");
		Reporter.log("18. Select a line | Lineselector details page should be displayed");
		Reporter.log("19. Click on Skip trade-in CTA | PHP page should be displayed");
		Reporter.log("20. Click on Skip Accessories Link | CartPage should be displayed");
		Reporter.log("21. Save Device color in V3 | Device color should be saved in V3");
		Reporter.log("22. Save Device memory in V4 | Device memory should be saved in V4");	
		Reporter.log("23. Compare Device color(V1 and V3) | Device color should be updated");
		Reporter.log("24. Compare Device memory(V2 and V4) | Device memory should be updated");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		UNOPDPPage unoPDPPage = navigateToUNOPDPPageByFeatureDevices(myTmoData, myTmoData.getDeviceName());
		unoPDPPage.clickOnAddToCartCTA();		
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();		
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickOnTradeInThisPhone();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(getDriver());
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInDeviceConditionConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();
		tradeInDeviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();
		//DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		//deviceProtectionPage.verifyDeviceProtectionURL();		
		//deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());		
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories("1");
		accessoryPLPPage.clickContinueButton();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		String deviceColorInCartPageBeforeEditing = cartPage.getDeviceColor();
		String deviceMemoryInCartPageBeforeEditing = cartPage.getDeviceMemory();
		cartPage.clickEditButtonOnDevice();
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.verifyUpGradeCTAEnabled();
		unoPDPPage.chooseColor(myTmoData.getDeviceColor());
		unoPDPPage.selectMemoryOnUNOPDP(myTmoData.getDeviceMemory());
		unoPDPPage.clickOnAddToCartCTA();	
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();		
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickOnSkipTradeInLink();		
		//deviceProtectionPage.verifyDeviceProtectionURL();
		//deviceProtectionPage.clickContinueButton();			
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesLink();		
		cartPage.verifyCartPage();
		String deviceColorInCartPageAfterEditing = cartPage.getDeviceColor();
		String deviceMemoryInCartPageAfterEditing = cartPage.getDeviceMemory();
		cartPage.deviceColorUpdatedAfterEditingSKUInformation(deviceColorInCartPageAfterEditing,deviceColorInCartPageBeforeEditing);
		cartPage.deviceMemoryUpdatedAfterEditingSKUInformation(deviceMemoryInCartPageAfterEditing,deviceMemoryInCartPageBeforeEditing);
	}


}