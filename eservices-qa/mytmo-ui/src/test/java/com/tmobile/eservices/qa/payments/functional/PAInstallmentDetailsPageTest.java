/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AddCardPage;
import com.tmobile.eservices.qa.pages.payments.PAInstallmentDetailsPage;
import com.tmobile.eservices.qa.pages.payments.PaymentArrangementPage;
import com.tmobile.eservices.qa.pages.payments.PaymentCollectionPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class PAInstallmentDetailsPageTest extends PaymentCommonLib {

	/**
	 * US296086 Payment Arrangement - Amount titles for Sedona customers Modal
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - Sedona user. With current due and past due
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY, Group.SEDONA })
	public void testPAEditDetailsAmountTitlesSedona(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US296086 Payment Arrangement - Amount titles for Sedona customers");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2. Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3. Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4. click on payment arrangement | payment arrangemnet page should be loaded");
		Reporter.log("Step 5. Click on Payment Installements | Verify installmentDetails page is loaded");
		Reporter.log("Step 5.1 Verify Total Balance is dispalyed");
		Reporter.log("Step 5.2 Verify Minimum to Restore is displayed");
		Reporter.log("================================");
		Reporter.log(Constants.ACTUAL_TEST_STEPS);

		PAInstallmentDetailsPage paInstallmentDetailsPage = navigateToPAInstallmentDetailsPage(myTmoData);
		paInstallmentDetailsPage.verifyTotalBalanceDisplayd();
		paInstallmentDetailsPage.verifyMinToRestoreDisplayd();

	}

	/**
	 * DE225555 REG9_Pastdue amount is not getting displayed in Edit deatils (PA)
	 * 
	 * @param data
	 * @param myTmoData Data Conditions - user With total due and past due
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {})
	public void testPastDueBalanceonInstallmentDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: DE225555 REG9_Pastdue amount is not getting displayed in Edit deatils (PA)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1. Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2. Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3. Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4. click on payment arrangement | payment arrangemnet page should be loaded");
		Reporter.log("Step 5. Click on Payment Installements | Verify installmentDetails page is loaded");
		Reporter.log("Step 6 Verify pastdue Balance is dispalyed | pastdue Balance should be displayed");
		Reporter.log("================================");
		Reporter.log(Constants.ACTUAL_TEST_STEPS);

		PAInstallmentDetailsPage paInstallmentDetailsPage = navigateToPAInstallmentDetailsPage(myTmoData);
		paInstallmentDetailsPage.verifyPastDueBalDisplayd();

	}

	/**
	 * CDCDWG2-81 --> PA : Setup - Amount Field Display Logic
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupAmountFieldDisplayLogicNonSuspended(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-81 --> PA : Setup - Amount Field Display Logic");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: Click on Installment blade. | Installment spoke page should be displayed.");
		Reporter.log("Step 6: Check header | Header 'Edit details' should be displayed.");
		Reporter.log("Step 7: Check text below it. | Text 'Choose an amount' should be displayed.");
		Reporter.log(
				"Step 8: Check options on Spoke page. | Options should be displayed as 'Total Balance' and 'Past Due'.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paymentArrangementPage.clickinstallemtBlade();
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyHeaderandSubHeader(PaymentConstants.PA_INSTALLMENT_PAGE_HEADER,
				PaymentConstants.PA_INSTALLMENT_PAGE_CHOOSE_AN_AMOUNT);
		paInstallmentDetailsPage.verifyAmountOptions(PaymentConstants.TOTAL_AMOUNT);
		paInstallmentDetailsPage.verifyAmountOptions(PaymentConstants.PAST_DUE);
	}

	/**
	 * CDCDWG2-81 --> PA : Setup - Amount Field Display Logic
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupAmountFieldDisplayLogicNonSuspendedPastDueEqualsTotalBal(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-81 --> PA : Setup - Amount Field Display Logic");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: Click on Installment blade. | Installment spoke page should be displayed.");
		Reporter.log("Step 6: Check header | Header 'Edit details' should be displayed.");
		Reporter.log("Step 7: Check text below it. | Text 'Choose an amount' should be displayed.");
		Reporter.log("Step 8: Check options on Spoke page. | Options should be displayed as 'Total Balance'.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paymentArrangementPage.clickinstallemtBlade();
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyHeaderandSubHeader(PaymentConstants.PA_INSTALLMENT_PAGE_HEADER,
				PaymentConstants.PA_INSTALLMENT_PAGE_CHOOSE_AN_AMOUNT);
		paInstallmentDetailsPage.verifyAmountOptions(PaymentConstants.TOTAL_AMOUNT);
	}

	/**
	 * CDCDWG2-81 --> PA : Setup - Amount Field Display Logic
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupAmountFieldDisplayLogicSuspended(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-81 --> PA : Setup - Amount Field Display Logic");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: Click on Installment blade. | Installment spoke page should be displayed.");
		Reporter.log("Step 6: Check header | Header 'Edit details' should be displayed.");
		Reporter.log("Step 7: Check text below it. | Text 'Choose an amount' should be displayed.");
		Reporter.log(
				"Step 8: Check options on Spoke page. | Options should be displayed as 'Total Balance' and 'Minimum to Restore'.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paymentArrangementPage.clickinstallemtBlade();
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyHeaderandSubHeader(PaymentConstants.PA_INSTALLMENT_PAGE_HEADER,
				PaymentConstants.PA_INSTALLMENT_PAGE_CHOOSE_AN_AMOUNT);
		paInstallmentDetailsPage.verifyAmountOptions(PaymentConstants.TOTAL_AMOUNT);
		paInstallmentDetailsPage.verifyAmountOptions(PaymentConstants.MINIMUM_TO_RESTORE);

	}

	/**
	 * CDCDWG2-81 --> PA : Setup - Amount Field Display Logic
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupAmountFieldDisplayLogicSuspendedPastDueEqualsTotalBal(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-81 --> PA : Setup - Amount Field Display Logic");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: Click on Installment blade. | Installment spoke page should be displayed.");
		Reporter.log("Step 6: Check header | Header 'Edit details' should be displayed.");
		Reporter.log("Step 7: Check text below it. | Text 'Choose an amount' should be displayed.");
		Reporter.log("Step 8: Check options on Spoke page. | Options should be displayed as 'Past Due'.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paymentArrangementPage.clickinstallemtBlade();
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyHeaderandSubHeader(PaymentConstants.PA_INSTALLMENT_PAGE_HEADER,
				PaymentConstants.PA_INSTALLMENT_PAGE_CHOOSE_AN_AMOUNT);
		paInstallmentDetailsPage.verifyAmountOptions(PaymentConstants.PAST_DUE);
	}

	/**
	 * CDCDWG2-252 --> PA: Setup - Installment spoke page.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupInstallmentSpokePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-252 --> PA: Setup - Installment spoke page.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to One time payment | One time payment Landing Page should be displayed");
		Reporter.log("Step 4: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 5: Click on Installment blade. | Installment spoke page should be displayed.");
		Reporter.log("Step 6: Check dropdown for Number of payments. | Dropdown should be displayed.");
		Reporter.log(
				"Step 7: Select 1 installment from number of installment dropdown. | Only 1 installment should be displayed.");
		Reporter.log(
				"Step 8: Select 2 installment from number of installment dropdown. | 2 installments should be displayed.");
		Reporter.log("Step 9: Click on Cancel CTA | User should be redirected to PA landing page.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paymentArrangementPage.clickinstallemtBlade();
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyHeaderandSubHeader(PaymentConstants.PA_INSTALLMENT_PAGE_HEADER,
				PaymentConstants.PA_INSTALLMENT_PAGE_CHOOSE_AN_AMOUNT);
		paInstallmentDetailsPage.verifyAmountOptions(PaymentConstants.NUMBER_OF_SCHEDULED_PAYMENTS);
		paInstallmentDetailsPage.selectAndVerifyInstallmentsDropdown();
		paInstallmentDetailsPage.clickCancelCTA();
		paymentArrangementPage.verifyPageLoaded();
	}

	/**
	 * CDCDWG2-39 --> PA : Setup - Installment Blade
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupSingleInstallmentBlade(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-39 --> PA : Setup - Installment Blade");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log(
				"Step 4: Check number of installment | Installment number should be 1 and only 1 installment should be displayed.");
		Reporter.log("Step 5: Check amount | Amount should be total amount.");
		Reporter.log("Step 6: Check text 'Total' | Text 'Total' should be displayed.");
		Reporter.log("Step 7: Check total amount at total and at header. | both amounts should be same.");
		Reporter.log("Step 8: Click on Installment blade. | It should be redirected to Installment spoke page.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyPaymentScheduleHeader(PaymentConstants.PAYMENT_SCHEDULE);
		paymentArrangementPage.verifyInstallmentNumber("1");
		Double totalAmount = paymentArrangementPage.getTotalBal();
		paymentArrangementPage.verifyInstallementAmount(totalAmount.toString());
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paymentArrangementPage.clickinstallemtBlade();
		paInstallmentDetailsPage.verifyPageLoaded();
	}

	/**
	 * CDCDWG2-39 --> PA : Setup - Installment Blade
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupDoubleInstallmentBlade(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-39 --> PA : Setup - Installment Blade");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Check number of installment | Total 2 installments should be dispalyed.");
		Reporter.log(
				"Step 5: Check 1st installment. | Installment number should be 1 and amount should be displayed as 20%.");
		Reporter.log(
				"Step 6: Check 2nd installment. | Installment number should be 2 and amount should be displayed as 80%.");
		Reporter.log(
				"Step 7: Check text 'Total' | Text 'Total' should be displayed with addition of 1st and 2nd installment.");
		Reporter.log("Step 8: Check total amount at total and at header. | Both amounts should be same.");
		Reporter.log("Step 9: Click on Installment blade. | It should be redirected to Installment spoke page.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyPaymentScheduleHeader(PaymentConstants.PAYMENT_SCHEDULE);
		paymentArrangementPage.verifyInstallmentNumber("1");
		paymentArrangementPage.verifyInstallmentNumber("2");
		Double totalAmount = paymentArrangementPage.getTotalBal();
		paymentArrangementPage.verifyInstallementAmount(calculateInstallmentAmount(totalAmount, 20).toString());
		paymentArrangementPage.verifyInstallementAmount(calculateInstallmentAmount(totalAmount, 80).toString());
		paymentArrangementPage.verifyTotalBalanceDisplayd();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paymentArrangementPage.clickinstallemtBlade();
		paInstallmentDetailsPage.verifyPageLoaded();
	}

	/**
	 * CDCDWG2-204 (ATDD) PA : Setup - View Details Link Logic
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupNoViewDetailsLinkNonSedona(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-204 (ATDD) PA : Setup - View Details Link Logic");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Click on Installment blade. | It should be redirected to Installment spoke page.");
		Reporter.log("Step 5: Verify 'view details' link for Non Sedona | It should not be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyViewDetailsLink(false);
	}

	/**
	 * CDCDWG2-204 (ATDD) PA : Setup - View Details Link Logic
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupNoViewDetailsLinkNonSedonaNoPAFee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-204 (ATDD) PA : Setup - View Details Link Logic");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Click on Installment blade. | It should be redirected to Installment spoke page.");
		Reporter.log("Step 5: Verify 'view details' link for Non Sedona & No PA Fee | It should not be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyViewDetailsLink(false);
	}

	/**
	 * CDCDWG2-204 (ATDD) PA : Setup - View Details Link Logic
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupNoViewDetailsLinkSedonaNoPAFee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-204 (ATDD) PA : Setup - View Details Link Logic");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Click on Installment blade. | It should be redirected to Installment spoke page.");
		Reporter.log("Step 5: Verify 'view details' link for Sedona & No PA Fee | It should not be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyViewDetailsLink(false);
	}

	/**
	 * CDCDWG2-204 (ATDD) PA : Setup - View Details Link Logic
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupViewDetailsLinkSedona(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-204 (ATDD) PA : Setup - View Details Link Logic");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Click on Installment blade. | It should be redirected to Installment spoke page.");
		Reporter.log("Step 5: Verify 'view details' link for Sedona | It should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyViewDetailsLink(true);
	}

	/**
	 * CDCDWG2-204 (ATDD) PA : Setup - View Details Link Logic
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupViewDetailsLinkSedonaSuspendedNoPAFee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-204 (ATDD) PA : Setup - View Details Link Logic");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Click on Installment blade. | It should be redirected to Installment spoke page.");
		Reporter.log(
				"Step 5: Verify 'view details' link for Sedona and Suspended and No PA Fee | It should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyViewDetailsLink(true);
	}

	/**
	 * CDCDWG2-196 (ATDD) PA : Setup - Breakdown modal (Total Balance)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupBreakDownModalTotalbalWithCurrBalPastDueRestoreFeePAFee(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-196	(ATDD) PA : Setup - Breakdown modal (Total Balance)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Click on Installment blade. | It should be redirected to Installment spoke page.");
		Reporter.log("Step 5: Verify 'view details' link for Non Sedona | It should not be displayed.");
		Reporter.log("Step 6: Click 'view details' link | Breakdown modal should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyViewDetailsLink(true);
		paInstallmentDetailsPage.clickViewDetailsLinkTotalBal();
		paInstallmentDetailsPage.verifyBreakDownModal();
	}

	/**
	 * CDCDWG2-196 (ATDD) PA : Setup - Breakdown modal (Total Balance)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupBreakDownModalTotalbalWithCurrBalPastDueRestoreFee(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-196	(ATDD) PA : Setup - Breakdown modal (Total Balance)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Click on Installment blade. | It should be redirected to Installment spoke page.");
		Reporter.log("Step 5: Verify 'view details' link for Non Sedona | It should not be displayed.");
		Reporter.log("Step 6: Click 'view details' link | Breakdown modal should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyViewDetailsLink(true);
		paInstallmentDetailsPage.clickViewDetailsLinkTotalBal();
		paInstallmentDetailsPage.verifyBreakDownModal();
	}

	/**
	 * CDCDWG2-196 (ATDD) PA : Setup - Breakdown modal (Total Balance)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupBreakDownModalTotalbalWithCurrBalRestoreFee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-196	(ATDD) PA : Setup - Breakdown modal (Total Balance)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Click on Installment blade. | It should be redirected to Installment spoke page.");
		Reporter.log("Step 5: Verify 'view details' link for Non Sedona | It should not be displayed.");
		Reporter.log("Step 6: Click 'view details' link | Breakdown modal should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyViewDetailsLink(true);
		paInstallmentDetailsPage.clickViewDetailsLinkTotalBal();
		paInstallmentDetailsPage.verifyBreakDownModal();
	}

	/**
	 * CDCDWG2-196 (ATDD) PA : Setup - Breakdown modal (Total Balance)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupBreakDownModalClose(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-196	(ATDD) PA : Setup - Breakdown modal (Total Balance)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Click on Installment blade. | It should be redirected to Installment spoke page.");
		Reporter.log("Step 5: Verify 'view details' link for Non Sedona | It should not be displayed.");
		Reporter.log("Step 6: Click 'view details' link | Breakdown modal should be displayed.");
		Reporter.log("Step 7: Click 'Close' icon on modal | Breakdown modal should be closed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyViewDetailsLink(true);
		paInstallmentDetailsPage.clickViewDetailsLinkTotalBal();
		paInstallmentDetailsPage.verifyBreakDownModal();
		paInstallmentDetailsPage.closeBreakDownModal();
	}

	/**
	 * CDCDWG2-80 (ATDD) PA : Setup - Breakdown modal (Past Due Balance)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupBreakDownModalPastDuebalCurrBalPastDueRestoreFeePAFee(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-80 (ATDD) PA : Setup - Breakdown modal (Past Due Balance)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Click on Installment blade. | It should be redirected to Installment spoke page.");
		Reporter.log("Step 5: Verify 'view details' link for Non Sedona | It should not be displayed.");
		Reporter.log("Step 6: Click 'view details' link | Breakdown modal should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyViewDetailsLink(true);
		paInstallmentDetailsPage.clickViewDetailsLinkPastDueBal();
		paInstallmentDetailsPage.verifyBreakDownModal();
	}

	/**
	 * CDCDWG2-80 (ATDD) PA : Setup - Breakdown modal (Past Due Balance)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupBreakDownModalPastDuebalCurrBalPastDueRestoreFee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-80 (ATDD) PA : Setup - Breakdown modal (Past Due Balance)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Click on Installment blade. | It should be redirected to Installment spoke page.");
		Reporter.log("Step 5: Verify 'view details' link for Non Sedona | It should not be displayed.");
		Reporter.log("Step 6: Click 'view details' link | Breakdown modal should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyViewDetailsLink(true);
		paInstallmentDetailsPage.clickViewDetailsLinkPastDueBal();
		paInstallmentDetailsPage.verifyBreakDownModal();
	}

	/**
	 * CDCDWG2-80 (ATDD) PA : Setup - Breakdown modal (Past Due Balance)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPASetupBreakDownModalPastDuebalCurrBalRestoreFeePAFee(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWG2-80 (ATDD) PA : Setup - Breakdown modal (Past Due Balance)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on payment arrangement | payment arrangement page should be displayed");
		Reporter.log("Step 4: Click on Installment blade. | It should be redirected to Installment spoke page.");
		Reporter.log("Step 5: Verify 'view details' link for Non Sedona | It should not be displayed.");
		Reporter.log("Step 6: Click 'view details' link | Breakdown modal should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifyViewDetailsLink(true);
		paInstallmentDetailsPage.clickViewDetailsLinkPastDueBal();
		paInstallmentDetailsPage.verifyBreakDownModal();
	}

	private Double calculateInstallmentAmount(Double value, int percent) {
		Double calculatedValue = null;
		try {
			calculatedValue = (Double) (value * (percent / 100.0f));
		} catch (Exception e) {
			Reporter.log("Error while calculating installment amount percentage");
		}
		return calculatedValue;
	}

	/**
	 * CDCDWG2-84 Sedona PA : Setup - Installment Spoke Field Validations Sprint -
	 * 20
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAmountFieldValidationForTotalBalance(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-84 Sedona PA : Setup - Installment Spoke Field Validations");
		Reporter.log("Step 1 :Login to site | User should be logged in to site.");
		Reporter.log("Step 2 :Go to PA page. | PA page should be displayed.");
		Reporter.log("Step 3 :Click on Installment blade | Installment blade clicked successfully");
		Reporter.log("Step 4: Verify Installment page is loaded | Installment page is loaded successfully");
		Reporter.log(
				"Step 5: Enter Amount in First installment inputbox less than minimum amount | First installment amount should be changed to Minimum amount");
		Reporter.log(
				"Step 6: Enter Amount in First installment inputbox more than maximum amount | First installment amount should be changed to Maximum amount");
		Reporter.log(
				"Step 7: Enter Amount in First installment inputbox which is more than minimum and less  than maximum amount | First Installment and second installment amounts are changed as per total amount");
		Reporter.log(
				"Step 8: Enter Amount in First installment inputbox without decimals |Amount in First installment will be updated with decimals");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		// Need to fetch min and total amounts properly and modify script
		paInstallmentDetailsPage.enterAmountInFirstInstallmentfield(myTmoData.getCardNumber());
		paInstallmentDetailsPage.verifyEnteringAmountLessThanMinimumAmount();
		paInstallmentDetailsPage.enterAmountInFirstInstallmentfield(myTmoData.getAccNumber());
		paInstallmentDetailsPage.verifyEnteringAmountMoreThanMaximumAmount();
		paInstallmentDetailsPage.enterAmountInFirstInstallmentfield(myTmoData.getAllowance());
		paInstallmentDetailsPage.verifyEnteringAmountBetweenMinAndMaxAmount(myTmoData.getCardNumber());
	}

	/**
	 * CDCDWG2 - 429 - PA: Modify PCM changes Sprint - 20
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPaymentMethodInModifyFlowWhenUserHasSetupPAWithCard(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2 - 429 - PA: Modify PCM changes");
		Reporter.log("Step 1 :Login to site | User should be logged in to site.");
		Reporter.log("Step 2 :Go to PA page. | PA page should be displayed.");
		Reporter.log("Step 3 :Check Payment blade. | Payment method blade should be displayed.");
		Reporter.log("Step 4 :Check Card Sprite, last 4 digits. | It should be displayed.");
		Reporter.log("Step 5 :Check 'Edit' link below last 4 digits. | Link should be displayed.");
		Reporter.log("Step 6 :Click on 'Edit' link | It should redirect to Edit PAyment method spoke page.");
		Reporter.log("================================");

		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyPaymentMethodBlade();
		paymentArrangementPage.clickpaymentMethod();
		PaymentCollectionPage paymentsCollectionPage = new PaymentCollectionPage(getDriver());
		paymentsCollectionPage.verifyPageLoaded();
		paymentsCollectionPage.verifySelectpaymentMethodCTAEnabledOrDisabled(false);
		paymentsCollectionPage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.fillCardInfo(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		paymentsCollectionPage.verifySelectpaymentMethodCTAEnabledOrDisabled(true);
	}

	/**
	 * CDCDWG2 - 429 - PA: Modify PCM changes Sprint - 20
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPaymentMethodInModifyFlowWhenUserHasSetupPAWithBank(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2 - 429 - PA: Modify PCM changes");
		Reporter.log("Step 1 : Login to site | User should be logged in to site.");
		Reporter.log("Step 2 : Go to PA page. | PA page should be displayed.");
		Reporter.log("Step 3 : Check Payment blade. | Payment method blade should be displayed.");
		Reporter.log("Step 4 : Check Bank Sprite, last 4 digits of account number. | It should be displayed.");
		Reporter.log("Step 5 : Check 'Edit' link below last 4 digits. | Link should be displayed.");
		Reporter.log("Step 6 : Click on 'Edit' link | It should redirect to Edit PAyment method spoke page.");
		Reporter.log("================================");

		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifypaymentMethodBlade();
		paInstallmentDetailsPage.verifyCardSprite();

	}

	/**
	 * CDCDWG2 - 429 - PA: Modify PCM changes Sprint - 20
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPaymentMethodInModifyFlowWhenUserHasSetupUnsecuredPA(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2 - 429 - PA: Modify PCM changes");
		Reporter.log("Step 1 : Login to site | User should be logged in to site.");
		Reporter.log("Step 2 : Go to PA page. | PA page should be displayed.");
		Reporter.log("Step 3 : Check Payment blade. | Payment method blade should be displayed.");
		Reporter.log("Step 4 : Check text 'Non Provided'. | It should be displayed.");
		Reporter.log("Step 5 : Check 'Edit' link below text 'Non Provided'. | Link should be displayed.");
		Reporter.log("Step 6 : Click on 'Edit' link | It should redirect to Edit Payment method spoke page.");
		Reporter.log("================================");

		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage;

		paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyPaymentMethodNotProvidedOnPaymentBlade();
		paymentArrangementPage.clickpaymentMethod();
		PaymentCollectionPage paymentsCollectionPage = new PaymentCollectionPage(getDriver());
		paymentsCollectionPage.verifyPageLoaded();
	}

	/**
	 * CDCDWG2-268 - PA: Setup agree and submit CTA Sprint - 20
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testTheErrorPageIsDisplayedWhenTheTrasactionIsNotSuccesful(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-268 - PA: Setup agree and submit CTA");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log(
				"Step 4 : Verify the Agree and Submit CTA| Agree and Submit should be displayed as Active/Enabled");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage;

		paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.verifypaymentMethodBlade();
		paInstallmentDetailsPage.verifyCardSprite();
	}

	/**
	 * CDCDWG2 - 460 - PA: Calendar modifications. Sprint - 20
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPACalendarModifications(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2 - 460 - PA: Calendar modifications.");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Go to payment arrangement page | PA landing page should be displayed");
		Reporter.log("Step 4 : Click on the Installment blade | PA Installment page should be displayed");
		Reporter.log(
				"Step 4 : Click on the calendar and verify the CTA | Calendar should be displayed in full page modal matching the HTML & Back CTA should be displayed .");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		PaymentArrangementPage paymentArrangementPage;

		paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.clickDateIcon();
		paInstallmentDetailsPage.verifyCalendarModal();
		paInstallmentDetailsPage.verifyCancelCTAOnCalendarModal();
		paInstallmentDetailsPage.clickCancelCTAOnModal();
	}

	/**
	 * CDCDWG2-313 -(ATDD) PA : Setup - Installment defaults
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testInstallmentsAmounts(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-313 -(ATDD) PA : Setup - Installment defaults");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Click on Make a payment arrangement | 'Payment arrangement' page should be displayed");
		Reporter.log("Step 4 : Click on installments blade | Edit details page should be displayed");
		Reporter.log(
				"Step 5 : In first installment amount enter less than 1.01 or greater than 25000 in first installment | Amount field should not allow less than 1.01 and greater than 25000");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateDirectlyToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyTotalBalisDefaulted();
		// paymentArrangementPage.verifyInstallmentDatesAreDefaultedToMaxTotalBal();
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.enterAmountInFirstInstallmentfield("0.25");
		paInstallmentDetailsPage.verifyAmountinFirstInstallmentField("0.25", false);
		paInstallmentDetailsPage.enterAmountInFirstInstallmentfield("250000");
		paInstallmentDetailsPage.verifyAmountinFirstInstallmentField("250000", false);
		String minAmt = paInstallmentDetailsPage.retrieveMinimumAmount();
		String totAmt = paInstallmentDetailsPage.retrieveTotalAmount();

		Double amtValue = Double.parseDouble(minAmt) + 1;
		paInstallmentDetailsPage.enterAmountInFirstInstallmentfield(amtValue.toString());
		paInstallmentDetailsPage.verifyAmountinFirstInstallmentField(amtValue.toString(), true);
		amtValue = Double.parseDouble(totAmt) - amtValue;
		paInstallmentDetailsPage.verifyAmountinSecondInstallmentField(amtValue.toString(), true);
	}

	/**
	 * CDCDWG2-83 - Sedona PA : Setup - Installment Spoke Messaging+
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testSedonaPASetupInstallmentSpokeMessaging(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("CDCDWG2-83 - Sedona PA : Setup - Installment Spoke Messaging+");
		Reporter.log("Step 1 : Launch the test Environment | Login page should be displayed");
		Reporter.log("Step 2 : Login with MSISDN | Should be able to login and home page displayed");
		Reporter.log("Step 3 : Click on Make a payment arrangement | 'Payment arrangement' page should be displayed");
		Reporter.log("Step 4 : Click on installments blade | Edit details page should be displayed");
		Reporter.log(
				"Step 5 : Verify the link 'View Details' under Total bal and Past due | There should be NO View Details link for Non sedona");
		Reporter.log(
				"Step 6 : Toggle between Total bal and Past due | Amounts should vary to past due and Messaging should be displayed");
		Reporter.log(
				"Step 7 : Toggle between Past due and Total bal | Amounts should be restored and Messaging should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PaymentArrangementPage paymentArrangementPage = navigateDirectlyToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyTotalBalisDefaulted();
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage paInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		paInstallmentDetailsPage.verifyPageLoaded();
		paInstallmentDetailsPage.clickRadioButton("Past");
		paInstallmentDetailsPage.verifySpokeMessaging(PaymentConstants.PA_SPOKE_MESSAGING);
		paInstallmentDetailsPage.verifySpokeMessaging(PaymentConstants.PA_SPOKE_MESSAGING_ONE);
		paInstallmentDetailsPage.clickRadioButton("Total");
		paInstallmentDetailsPage.verifySpokeMessagingIsNotDisplayed();
		String minAmt = paInstallmentDetailsPage.retrieveMinimumAmount();
		Double amtValue = Double.parseDouble(minAmt) + 1;
		paInstallmentDetailsPage.enterAmountInFirstInstallmentfield(amtValue.toString());
		paInstallmentDetailsPage.verifySpokeMessaging(PaymentConstants.PA_SPOKE_MESSAGING);
	}

}
