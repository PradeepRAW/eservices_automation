package com.tmobile.eservices.qa.global.acceptance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.NewHomePage;

public class LoginTest extends GlobalCommonLib {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginTest.class);


	/**
	 * Ensure "Australis" lines can be authenticated and logged in
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true,groups = "Login")
	public void verifyAustralisUserLogin(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Ensure Australis lines can be authenticated and logged in" );
		Reporter.log("Ensure Australis lines can be authenticated and logged in");
		Reporter.log("");
		Reporter.log("Test Data Conditions: Msisdn with Australis Login info");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("==============================");
		Reporter.log("Actual Result:");
		
		launchAndPerformLogin(myTmoData);
		Assert.assertTrue(getDriver().getTitle().contains("Home"), "User not logged In");		
		Reporter.log("User logged in successfully");
	}
	
	
	/**
	 *Ensure legacy customers (prepaid) are successfully redirected to the legacy site after authentication.
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PENDING})
	public void verifyLegacyCustomerLogin(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Ensure legacy customers (prepaid) are successfully redirected to the legacy site after authentication." );
		Reporter.log("Ensure legacy customers (prepaid) are successfully redirected to the legacy site after authentication.");
		Reporter.log("");
		Reporter.log("Test Data Conditions: Msisdn with Legacy Customer Login info");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("==============================");
		Reporter.log("Actual Result:");
		
		launchAndPerformLogin(myTmoData);
		Assert.assertTrue(getDriver().getTitle().contains("Access Messages, Minutes & Bills"), "User not logged In");		
		Reporter.log("User logged in successfully");
	}
	
	/**
	 *Ensure government customers are successfully redirected to the site after authentication.
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PENDING})
	public void verifyGovernmentCustomerLogin(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Ensure government customers are successfully redirected to the site after authentication." );
		Reporter.log("Ensure government customers are successfully redirected to the site after authentication.");
		Reporter.log("");
		Reporter.log("Test Data Conditions: Msisdn with Government Customer Login info");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("==============================");
		Reporter.log("Actual Result:");
		
		launchAndPerformLogin(myTmoData);
		Assert.assertTrue(getDriver().getTitle().contains("Home"), "User not logged In");		
		Reporter.log("User logged in successfully");
	}
	
	/**
	 *Ensure B2b customer with 16 lines are successfully redirected to the site after authentication.
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PENDING})
	public void verifyB2b16LinesCustomerLogin(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Ensure B2b customer with 16 lines are successfully redirected to the site after authentication." );
		Reporter.log("Ensure B2b customer with 16 lines are successfully redirected to the site after authentication.");
		Reporter.log("");
		Reporter.log("Test Data Conditions: Msisdn with B2B16 Customer Login info");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("==============================");
		Reporter.log("Actual Result:");
		
		launchAndPerformLogin(myTmoData);
		Assert.assertTrue(getDriver().getTitle().contains("Home"), "User not logged In");		
		Reporter.log("User logged in successfully");
	}
	
	
	/**
	 * US486489/US511399:.NET | Splash Pages | Test Support
	 * US558159:.NET Migration | Prepaid | MBB Splash Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", groups = {Group.RELEASE_READY})
	public void verifyPuertoRicoCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify PuertoRico Customer");
		Reporter.log("Test Data Conditions: Only PuertoRico Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify PuertoRico customer header | PuertoRico customer header should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		
		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyPuertoRicoCustomerHeader();
	}
	
	
	/**
	 * US486489/US511399:.NET | Splash Pages | Test Support
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", groups = {Group.FLEX})
	public void verifyVIPCustomer(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBladeuserInfo P1_TC_Regression_Desktop_HomePage_MSISDN Info");
		Reporter.log("Test Case : Verify blade user information");
		Reporter.log("Test Data Conditions: Only Blade user Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify VIP customer header | VIP customer header should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifyVIPCustomerHeader();
	}
	
	
	/**
	 * US486489:.NET | Splash Pages | Test Support
	 * US558159:.NET Migration | Prepaid | MBB Splash Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", groups = {Group.RELEASE_READY})
	public void verifyGovernmentCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify Government customer");
		Reporter.log("Test Data Conditions: Goverment customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify Government customer header | Government customer header should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyGovernmentCustomerHeader();
	}
	
}
