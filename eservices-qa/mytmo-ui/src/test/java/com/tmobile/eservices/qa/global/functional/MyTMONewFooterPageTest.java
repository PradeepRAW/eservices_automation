package com.tmobile.eservices.qa.global.functional;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.UNAVCommonPage;
import com.tmobile.eservices.qa.pages.global.CoveragePage;
import com.tmobile.eservices.qa.pages.global.NewContactUSPage;
import com.tmobile.eservices.qa.pages.global.StoreLocatorPage;
import com.tmobile.eservices.qa.pages.global.SupportPage;
import com.tmobile.eservices.qa.global.GlobalCommonLib;

public class MyTMONewFooterPageTest extends GlobalCommonLib {
	//private static final Logger logger = LoggerFactory.getLogger(MyTMONewFooterPageTest.class);

	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
	 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
	 * Verify My TMO Footer Title and English Link
	 * @param data
	 * @param myTmoData
	 */	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void verifyEnglishLink(ControlTestData data, MyTmoData myTmoData){

		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Connect With T-Mobile text is displayed | Connect With T-Mobile Title text displayed successfully");
		Reporter.log("4.Verify English text is displayed | English text is displayed");
		Reporter.log("5.Click on English link | Clicked on English link successfully");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.verifyEnglishLanguage();
		unavPage.clickEnglishlink();
	}
	
	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
	 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
	 * Verify Spanish Link Redirects To Spanish Page
	 * @param data
	 * @param myTmoData
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void verifySpanishLink(ControlTestData data, MyTmoData myTmoData){
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Spanish text is displayed | Spanish text is displayed");
		Reporter.log("4.Click on Spanish link | Clicked on Spanish link successfully");
		Reporter.log("5.Verify Spanish page | Spanish Home page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.verifySpanishLanguage();
		unavPage.clickSpanishlink();
		unavPage.verifySpanishPage();
	}	
	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
	 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
	 * Verify Support Redirects To Support Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PROGRESSION })
	public void verifyMyTMOSupportLink(ControlTestData data, MyTmoData myTmoData){
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Support label is displayed | Support is displayed");
		Reporter.log("4.Click on Support link | Clicked on Support link successfully");
		Reporter.log("5.Verify Support page | Support page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.verifySupportLabel();
		unavPage.clickSupportlink();
		unavPage.verifySupportPage();
	}
	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
	 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
	 * Verify Contact Us Redirects To Contact Us Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PROGRESSION })
	public void verifyMyTMOContactUsLink(ControlTestData data, MyTmoData myTmoData){
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Contact Us Link is displayed | Contact Us link is displayed");
		Reporter.log("4.Click on Contact Us link | Clicked on Contact Us link successfully");
		Reporter.log("5.Verify Contact Us page | Contact Us page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.verifyContactUsLabel();
		unavPage.clickContactUslink();
		unavPage.verifyContactUsPage();
	}
	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
	 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
	 * Verify StoreLocator Redirects To StoreLocator Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PROGRESSION })
	public void verifyMyTMOStoreLocatorLink(ControlTestData data, MyTmoData myTmoData){
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify StoreLocator Link is displayed | StoreLocator link is displayed");
		Reporter.log("4.Click on StoreLocator link | Clicked on StoreLocator link successfully");
		Reporter.log("5.Verify StoreLocator page | StoreLocator page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.verifyStoreLocatorLabel();
		unavPage.clickStoreLocatorlink();
		unavPage.verifyStoreLocatorPage();
	}
	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
	 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
	 * Verify Coverage Redirects To Coverage Page
	 * TC-678:Ensure footer links are working as expected
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PROGRESSION })
	public void verifyMyTMOCoverageLink(ControlTestData data, MyTmoData myTmoData){
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Coverage Link is displayed | Coverage link is displayed");
		Reporter.log("4.Click on Coverage link | Clicked on Coverage link successfully");
		Reporter.log("5.Verify Coverage page | Coverage page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
		unavPage.verifyCoverageLabel();
		unavPage.clickCoveragelink();
		unavPage.verifyCoveragePage();
	}

	/**
	 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
	 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
	 * Verify TMobile.com Redirects To TMobile Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PROGRESSION })
	public void verifyMyTMOTMobileLink(ControlTestData data, MyTmoData myTmoData){
		Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
		Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify TMobile Link is displayed | TMobile link is displayed");
		Reporter.log("4.Click on TMobile link | Clicked on TMobile link successfully");
		Reporter.log("5.Verify TMobile page | TMobile page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
		navigateToHomePage(myTmoData);
        unavPage.clickAppsExpandButton();
		unavPage.verifyTMobileLabel();
		unavPage.clickTMobilelink();
		unavPage.verifyTMobilePage();
	}	
		// Verify Footer 2 - ABOUT Section
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify ABOUT Redirects To ABOUT Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
		public void clickAboutlink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify ABOUT link is displayed | ABOUT link is displayed");
			Reporter.log("4.Click on ABOUT link | Clicked on ABOUT link successfully");
			Reporter.log("5.Verify ABOUT page | ABOUT page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyAboutLabel();
			unavPage.clickAboutlink();
			unavPage.verifyAboutPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify INVESTOR RELATIONS Redirects To INVESTOR RELATIONS Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
		public void clickFooterInvestorRelationslink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify INVESTOR RELATIONS link is displayed | INVESTOR RELATIONS link is displayed");
			Reporter.log("4.Click on INVESTOR RELATIONS link | Clicked on INVESTOR RELATIONS link successfully");
			Reporter.log("5.Verify INVESTOR RELATIONS page | INVESTOR RELATIONS page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyFooterInvestorRelationsLabel();
			unavPage.clickFooterInvestorRelationslink();
			unavPage.verifyFooterInvestorRelationsPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify PRESS Redirects To PRESS Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
		public void clickPresslink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify PRESS link is displayed | PRESS link is displayed");
			Reporter.log("4.Click on PRESS link | Clicked on PRESS link successfully");
			Reporter.log("5.Verify PRESS page | PRESS page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyPressLabel();
			unavPage.clickPresslink();
			unavPage.verifyPressPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify CAREERS Redirects To CAREERS Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
		public void clickFooterCareerslink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify CAREERS link is displayed | CAREERS link is displayed");
			Reporter.log("4.Click on CAREERS link | Clicked on CAREERS link successfully");
			Reporter.log("5.Verify CAREERS page | CAREERS page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyFooterCareersLabel();
			unavPage.clickFooterCareerslink();
			unavPage.verifyFooterCareersPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify DEUTSCHE TELEKOM Redirects To DEUTSCHE TELEKOM Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PROGRESSION})
		public void clickDeutscheTelekomlink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify DEUTSCHE TELEKOM link is displayed | DEUTSCHE TELEKOM link is displayed");
			Reporter.log("4.Click on DEUTSCHE TELEKOM link | Clicked on DEUTSCHE TELEKOM link successfully");
			Reporter.log("5.Verify DEUTSCHE TELEKOM page | DEUTSCHE TELEKOM page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyDeutscheTelekomLabel();
			unavPage.clickDeutscheTelekomlink();
			unavPage.verifyDeutscheTelekomPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify PUERTO RICO Redirects To PUERTO RICO Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
		public void clickPuertoRicolink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify PUERTO RICO link is displayed | PUERTO RICO link is displayed");
			Reporter.log("4.Click on PUERTO RICO link | Clicked on PUERTO RICO link successfully");
			Reporter.log("5.Verify PUERTO RICO page | PUERTO RICO page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyPuertoRicoLabel();
			unavPage.clickPuertoRicolink();
			unavPage.verifyPuertoRicoPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify PRIVACY POLICY Redirects To PRIVACY POLICY Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
		public void clickPrivacyPolicylink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify PRIVACY POLICY link is displayed | PRIVACY POLICY link is displayed");
			Reporter.log("4.Click on PRIVACY POLICY link | Clicked on PRIVACY POLICY link successfully");
			Reporter.log("5.Verify PRIVACY POLICY page | PRIVACY POLICY page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyPrivacyPolicyLabel();
			unavPage.clickPrivacyPolicylink();
			unavPage.verifyPrivacyPolicyPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify INTEREST-BASED ADS Redirects To INTEREST-BASED ADS Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
		public void clickInterestBasedAdslink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify INTEREST-BASED ADS link is displayed | INTEREST-BASED ADS link is displayed");
			Reporter.log("4.Click on INTEREST-BASED ADS link | Clicked on INTEREST-BASED ADS link successfully");
			Reporter.log("5.Verify INTEREST-BASED ADS page | INTEREST-BASED ADS page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyInterestBasedAdsLabel();
			unavPage.clickInterestBasedAdslink();
			unavPage.verifyInterestBasedAdsPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify PRIVACY CENTER Redirects To PRIVACY CENTER Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
		public void clickPrivacylink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify PRIVACY CENTER link is displayed | PRIVACY CENTER link is displayed");
			Reporter.log("4.Click on PRIVACY CENTER link | Clicked on PRIVACY CENTER link successfully");
			Reporter.log("5.Verify PRIVACY CENTER page | PRIVACY CENTER page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyPrivacyLabel();
			unavPage.clickPrivacylink();
			unavPage.verifyPrivacyPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify CONSUMER INFORMATION Redirects To CONSUMER INFORMATION Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
		public void clickConsumerlink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify CONSUMER INFORMATION link is displayed | CONSUMER INFORMATION link is displayed");
			Reporter.log("4.Click on CONSUMER INFORMATION link | Clicked on CONSUMER INFORMATION link successfully");
			Reporter.log("5.Verify CONSUMER INFORMATION page | CONSUMER INFORMATION page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyConsumerLabel();
			unavPage.clickConsumerlink();
			unavPage.verifyConsumerPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify PUBLIC SAFETY/911 Redirects To PUBLIC SAFETY/911 Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
		public void clickPublicSafetylink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify PUBLIC SAFETY/911 link is displayed | PUBLIC SAFETY/911 link is displayed");
			Reporter.log("4.Click on PUBLIC SAFETY/911 link | Clicked on PUBLIC SAFETY/911 link successfully");
			Reporter.log("5.Verify PUBLIC SAFETY/911 page | PUBLIC SAFETY/911 page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyPublicSafetyLabel();
			unavPage.clickPublicSafetylink();
			unavPage.verifyPublicSafetyPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify TERMS & CONDITIONS Redirects To TERMS & CONDITIONS Page
		 * TC-678:Ensure footer links are working as expected
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
		public void clickTermslink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify TERMS & CONDITIONS link is displayed | TERMS & CONDITIONS link is displayed");
			Reporter.log("4.Click on TERMS & CONDITIONS link | Clicked on TERMS & CONDITIONS link successfully");
			Reporter.log("5.Verify TERMS & CONDITIONS page | TERMS & CONDITIONS page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyTermsLabel();
			unavPage.clickTermslink();
			unavPage.verifyTermsPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify TERMS OF USE Redirects To TERMS OF USE Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
		public void clickTermsOfUselink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify TERMS OF USE link is displayed | TERMS OF USE link is displayed");
			Reporter.log("4.Click on TERMS OF USE link | Clicked on TERMS OF USE link successfully");
			Reporter.log("5.Verify TERMS OF USE page | TERMS OF USE page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyTermsOfUseLabel();
			unavPage.clickTermsOfUselink();
			unavPage.verifyTermsOfUsePage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify Accessibility Redirects To Accessibility Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
		public void clickAccessibilitylink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Accessibility link is displayed | Accessibility link is displayed");
			Reporter.log("4.Click on Accessibility link | Clicked on Accessibility link successfully");
			Reporter.log("5.Verify Accessibility page | Accessibility page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyAccessibilityLabel();
			unavPage.clickAccessibilitylink();
			unavPage.verifyAccessibilityPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify Open Internet Redirects To OpenInternet Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
		public void clickOpenInternetlink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Open Internet link is displayed | Open Internet link is displayed");
			Reporter.log("4.Click on Open Internet link | Clicked on Open Internet link successfully");
			Reporter.log("5.Verify Open Internet page | Open Internet page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyOpenInternetLabel();
			unavPage.clickOpenInternetlink();
			unavPage.verifyOpenInternetPage();
		}		
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify Footer2 Instagram Redirects To Instagram Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS})
		public void verifyFooter2InstagramLink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Footer2 Instagram Link is displayed | Instagram link is displayed");
			Reporter.log("4.Click on Instagram link | Clicked on Instagram link successfully");
			Reporter.log("5.Verify Instagram page | Instagram page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.clickFooter2Instagramlink();
			unavPage.verifyInstagramPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify Footer2 Facebook Redirects To Facebook Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
		public void verifyFooter2FacebookLink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Facebook Link is displayed | Facebook link is displayed");
			Reporter.log("4.Click on Facebook link | Clicked on Facebook link successfully");
			Reporter.log("5.Verify Facebook page | Facebook page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.clickFooter2Facebooklink();
			//unavPage.verifyFacebookPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify Footer2 Twitter Redirects To Twitter Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
		public void verifyFooter2TwitterLink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Footer2 Twitter Link is displayed | Twitter link is displayed");
			Reporter.log("4.Click on Twitter link | Clicked on Twitter link successfully");
			Reporter.log("5.Verify Twitter page | Twitter page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.clickFooter2Twitterlink();
			unavPage.verifyTwitterPage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify Footer2 YouTube Redirects To YouTube Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
		public void verifyFooter2YouTubeLink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Footer2 YouTube Link is displayed | YouTube link is displayed");
			Reporter.log("4.Click on YouTube link | Clicked on YouTube link successfully");
			Reporter.log("5.Verify YouTube page | YouTube page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.clickFooter2YouTubelink();
			unavPage.verifyYouTubePage();
		}
		/**
		 * CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T
		 * CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing
		 * Verify Footer2 Copy Right Text
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.GLOBAL,Group.DESKTOP,Group.ANDROID,Group.IOS })
		public void verifyFooter2CopyRight(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR-485 - UNAV |MYTMO | SDET | Integration testing for D/M/T");
			Reporter.log("CDCDWR-330 - UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Footer2 Copy Right Text is displayed | Copy Right Text is displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyCopyRightLabel();
		}
		
		/**
		 * CDCDWR-330:UNAV MyTmo | Unav Integration | Testing
		 * 
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT})
		public void verifyUnavFooterNavigationLinks(ControlTestData data, MyTmoData myTmoData) {
			Reporter.log("CDCDWR-330:UNAV MyTmo | Unav Integration | Testing");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Login to the application | User Should be login successfully");
			Reporter.log("3. Verify Home page | Home page should be displayed");
			Reporter.log("4. Click support link | Support page should be displayed");
			Reporter.log("5. Navigate back in browser | Home page should be displayed");
			Reporter.log("7. Click store locator link | Store locator page should be displayed");
			Reporter.log("8. Navigate back in browser | Home page should be displayed");
			Reporter.log("9. Click coverage link | Coverage page should be displayed");
			Reporter.log("10.Navigate back in browser | Home page should be displayed");
			Reporter.log("11.Click contact us link | Contact us page should be displayed");
			Reporter.log("12.Navigate back in browser | Home page should be displayed");

			Reporter.log("========================");

			NewHomePage homePage=navigateToNewHomePage(myTmoData);
			homePage.clickFooterSupportlink();
			
			SupportPage supportPage = new SupportPage(getDriver());
			supportPage.verifySupportPage();
			
			homePage.navigateBack();
			homePage.verifyHomePage();
			homePage.clickFooterStoreLocatorlink();		
			
			StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
			storeLocatorPage.verifyStoreLocatorPage();
			homePage.navigateBack();
			homePage.verifyHomePage();
			homePage.clickCoverageLink();		
			
			CoveragePage coveragePage = new CoveragePage(getDriver());
			coveragePage.verifyCoveragePage();
			
			homePage.navigateBack();
			homePage.verifyHomePage();
			homePage.clickContactUSlink();

			NewContactUSPage newContactUSPage = new NewContactUSPage(getDriver());
			newContactUSPage.verifyContactUSPage();
		}
		
		/**
		 * CDCDWR2-42 - CCPA WS4 | No Sell Web, Sell/No Sell App | DNS Page | UI
		 * @param data
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
		public void verifyDoNotSellLink(ControlTestData data, MyTmoData myTmoData){
			Reporter.log("CDCDWR2-42 - CCPA WS4 | No Sell Web, Sell/No Sell App | DNS Page | UI");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Do Not Sell Link is displayed | Do Not Sell is displayed");
			Reporter.log("4.Click on Do Not Sell Link | Clicked on Do Not Sell successfully");
			Reporter.log("5.Verify DNS page | DNS page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			UNAVCommonPage unavPage = new UNAVCommonPage(getDriver());
			navigateToHomePage(myTmoData);
			unavPage.verifyDoNotSellLabel();
			unavPage.clickDoNotSellLink();
			unavPage.verifyDoNotSellPage();
		}

}