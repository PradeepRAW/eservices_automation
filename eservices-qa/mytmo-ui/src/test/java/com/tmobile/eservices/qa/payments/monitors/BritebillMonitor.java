package com.tmobile.eservices.qa.payments.monitors;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.EOSCommonMethods;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EOSBillingFilters;

import io.restassured.response.Response;

public class BritebillMonitor extends EOSCommonMethods {

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PUBMonitors" })
	public void monitorBritebillpage(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSBillingFilters billing = new EOSBillingFilters();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			// data set response and check tags and alues
			Response datasetresponse = billing.getResponsedataset(getjwt);
			checkexpectedvalues(datasetresponse, "statusCode", "100");
			checkexpectedvalues(datasetresponse, "statusMessage", "Success");

			checkexpectedvalues(datasetresponse, "accountNumber", getjwt[3]);
			checkexpectedvalues(datasetresponse, "msisdn", getjwt[0]);

			String datasettags[] = { "arBalance.balanceDue", "loggedInUserInfo.userRole",
					"loggedInUserInfo.emailAddress", "currentBillCharges.currentBillDueAmount", "autoPay.easyPayStatus",
					"eipDetailsSection.isEIPDetailsExists", "jodDetailsSection.isJumpLeaseDetailsExists",
					"paymentArrangementDetails.paymentArrangementIndicator", "loggedInUserInfo", "accountLinesInfo",
					"pastdueAmountDetails.pastdueAmount", "paperlessBilling" };
			for (String tag : datasettags) {
				checkjsontagitems(datasetresponse, tag);
			}

			// bill list response and check tags and values

			Response billlistresponse = billing.getResponsebillList(getjwt);
			checkexpectedvalues(billlistresponse, "statusCode", "100");
			checkexpectedvalues(billlistresponse, "statusMessage", "Bill list found");

			String billListtags[] = { "BillList.documentId", "BillList.currentCharges", "BillList.startDate",
					"BillList.endDate", "BillList.amountDue" };
			for (String tag : billListtags) {
				checkjsontagitemsList(billlistresponse, tag);
			}

			String documentid = billing.getdocumentid(billlistresponse);

			// check summarybill

			Response responseprintsummarybill = billing.getResponseprintsummaryBill(getjwt, documentid);

			String billsummarytags[] = { "bill_period_from", "bill_period_to" };
			for (String tag : billsummarytags) {
				checkjsontagitems(responseprintsummarybill, tag);
			}

			// check detailedbill

			Response responseprintdetailedbill = billing.getResponseprintdetailedBill(getjwt, documentid);

			String billdetailtags[] = { "bill_period_from", "bill_period_to" };
			for (String tag : billdetailtags) {
				checkjsontagitems(responseprintdetailedbill, tag);
			}

		}else Assert.fail("JWT token retrieval failed check msisdn and password");

	}

}