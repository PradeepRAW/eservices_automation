package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.ReturnPolicyPage;

public class ReturnPolicyPageTest extends GlobalCommonLib{


	/**
	 * TC005_MYTMO_Cloud_Footer_Verify the Return Policy link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PENDING})
	public void verifyReturnPolicyLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyReturnPolicyLink method");
		Reporter.log("Test Case : MYTMO_Cloud_Footer_Verify the Return Policy link");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Return Policy link | Return policy Page should be displayed");		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		NewHomePage homePage=navigateToNewHomePage(myTmoData);
		homePage.clickReturnPolicyLink();
		homePage.switchToWindow();
		
		ReturnPolicyPage returnPolicyPage = new ReturnPolicyPage(getDriver());
		returnPolicyPage.verifyReturnPolicyPage();
		closeNewWindow();
	}
}
