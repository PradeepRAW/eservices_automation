package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.TradeinApiV3;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class TradeinApiV3Test extends TradeinApiV3{
	JsonPath jsonPath;
	public Map<String, String> tokenMap;
	
	/**
	/**
	 * UserStory# Description:US549732 :Create EOS API for trade-in conditions page
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 	
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP,Group.PROGRESSION, "0801" })
	public void testTradeInQuestionDetails(ControlTestData data, ApiTestData apiTestData,String jwtToken) throws Exception {
		Reporter.log("TestName: Get Cart");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get trade in/questions should return the details  regarding the condition of the device based on carrier/make/model");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap = new HashMap<String, String>();
		
        Response response = getTradeInApi(apiTestData,tokenMap,jwtToken);
		String operationName = "Get Tradein";

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath= new JsonPath(response.asString());
				logSuccessResponse(response, operationName);
				Assert.assertNotNull(jsonPath.get("questions"), "Questionnaire is null");
				Reporter.log("Questionnaire present while tradein");
			}
		}else {
			failAndLogResponse(response, operationName);
		}
		} 
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP,Group.PROGRESSION, "0801" })
	public void testTradeInQuestionDetailsforTradeInFlow(ControlTestData data, ApiTestData apiTestData,String jwtToken) throws Exception {
		Reporter.log("TestName: Get Cart");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get trade in/questions should return the details  regarding the condition of the device based on carrier/make/model");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap = new HashMap<String, String>();
		
        Response response = getTradeInApi(apiTestData,tokenMap,jwtToken);
		String operationName = "Get Tradein";

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath= new JsonPath(response.asString());
				logSuccessResponse(response, operationName);
				Assert.assertNotNull(jsonPath.get("questions"), "Questionnaire is null");
				Reporter.log("Questionnaire present while tradein");
			}
		}else {
			failAndLogResponse(response, operationName);
		}
		} 
}
