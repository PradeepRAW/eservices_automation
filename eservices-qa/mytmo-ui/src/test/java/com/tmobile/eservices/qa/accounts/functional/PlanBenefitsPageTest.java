package com.tmobile.eservices.qa.accounts.functional;

import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.accounts.BenefitsRedemption;
import com.tmobile.eservices.qa.pages.accounts.CrazyLegsBenefitsPage;
import com.tmobile.eservices.qa.pages.accounts.FeaturedPlanPage;
import com.tmobile.eservices.qa.pages.accounts.LineDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsSelectionPage;
import com.tmobile.eservices.qa.pages.accounts.ODFDataPassReviewPage;
import com.tmobile.eservices.qa.pages.accounts.PlanDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.PlansComparisonPage;
import com.tmobile.eservices.qa.pages.global.ContactUSPage;
import com.tmobile.eservices.qa.pages.global.LoginPage;

/**
 * @author prokarma
 *
 */
public class PlanBenefitsPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(PlanBenefitsPageTest.class);

	/**
	 * US191691#Crazy Legs: Legalese Story US196354#Crazy legs restricted offer
	 * (M&D) - Benefits page - Suppress footer and no global UNAV
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredby08092018release")
	public void verifyCrazyLegBenefitPageLegaleseAndSuppressFooter(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyCrazyLegBenifitPageLegaleseAndSuppressFooter method called in PlanBenefitsPageTest");

		Reporter.log("Test Case Name: Test Benefits page Legalese Terms and Conditions");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn");
		Reporter.log("================	========");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be logged in successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Signup for Netflix link | Nextflix plan-benefits Page should be displayed");
		Reporter.log("4. Verify Legalese terms | Legalese terms should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.checkLegalease(
				"Redeeming this offer will replace your current Netflix payment method with T-Mobile. On all T-Mobile plans, if congested, customers using >50GB/mo. may notice reduced speeds due to prioritization. Video typically streams at 480p. Tethering at max 3G speeds. Int'l speeds approx. 128 kbps. Limited time offer; subject to change. Receive Netflix Standard 2-screen (up to a $10.99/mo. value) while you maintain 2+ qual'g T-Mobile ONE lines in good standing. Value may be applied to Netflix Premium plan. Not redeemable or refundable for cash. Cancel Netflix anytime. Netflix Terms of Use apply:www.netflix.com/termsofuse .1 offer per T-Mobile account; may take 1-2 bill cycles. Like all plans, features may change or be discontinued at any time; see T-Mobile Terms and Conditions for details.");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredby08092018release")
	public void testAuthorizedUserwithRatePlanAndCLSOCAddedButNotRegisteredforNetflixEntersBenefitsPage(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testAuthorizedUserwithRatePlanAndCLSOCAddedEntersBenefitsPage method called in PlanBenefitsPageTest");
		Reporter.log(
				"Verify Authorized user with RatePlan and Added CL SOC reaches Benefits Page and see set-up My Netflix Account button");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with Standard Netflix added");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to Crazy legs Benefit Page | Cazy legs Benefit Page should be displayed");
		Reporter.log("4. Verify Crazy legs Benefit Page | Cazy legs Benefit Page should be verified");
		Reporter.log("5. Click on SignUpMyNetflix Button | Page Should Navigate to Netflix page");
		Reporter.log("6.Verify Netflix page is displayed |  Netflix Page should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.verifySignUpMyNetflixAccount();
		crazyLegsBenefitsPage.clickSignUpMyNetflixAccount();
		CommonPage CommonPage = new CommonPage(getDriver());
		CommonPage.acceptIOSAlert();
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.switchToSecondWindow();
		verifyCurrentPageURL("Netflix Page", "netflix");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredby08092018release")
	public void testAuthorizedUserwithRatePlanAndCLSOCAddedButNotRegiteredforNetFlixEntersHomePage(ControlTestData data,
			MyTmoData myTmoData) {

		logger.info("testAuthorizedUserwithRatePlanAndCLSOCAddedEntersHomePage method called in PlanBenefitsPageTest");

		Reporter.log(
				"Test Case Name : Verify Authorized user with RatePlan and Added CL SOC reaches Home Page and see set-up My Netflix Account link at the right bottom of page");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with Standard Netflix added");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Register for Netflix link  |  Page Should navigate to Benefits page");
		Reporter.log("4. Navigate to Crazy legs Benefit Page | Crazy legs Benefit Page should be displayed");
		Reporter.log("5. Verify Crazy legs Benefit Page | Crazy legs Benefit Page should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.verifyImportantMessage("As a T-Mobile ONE customer, you’re now eligible for Netflix!");
		homePage.clickOnRegisterForNetflixLink();
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.verifyBenifitsPage();

	}

	/**
	 * US189765#Crazy Legs Restricted Offer (M&D) - Allow CL Email Retrieval
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredby08092018release")
	public void verifyCLRegisteredUserAllowCLEmailRetrievalinBenefitsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyCrazyLegRatePlanRestrictedOfferAllowCLEmailRetrieval method called in PlanBenefitsPageTest");

		Reporter.log(
				"Test Case Name :Verify Netflix Registered Customer reaches benefit page has to see Recover Email link");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with Registered on netflix");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate Crazy legs Benefit Page | Crazy legs Benefit Page should be displayed");
		Reporter.log("4. Verify Crazy legs Benefit Page | Crazy legs Benefit Page should be verified");
		Reporter.log("5. Verify Email Recovering link is present | Recovr it here link should be displayed");
		Reporter.log("6. Click on Email recover link  | Email used to register in netflix Should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.clickRecoveritHerelink();
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.acceptIOSAlert();
		crazyLegsBenefitsPage.verifyNetflixPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredby08092018release")
	public void testCLRegisteredUserEntersHomePage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testAuthorizedUserwithRatePlanAndCLSOCAddedEntersHomePage method called in PlanBenefitsPageTest");
		Reporter.log(
				"Test Case Name :Verify Authorized user with RatePlan and Added CL SOC reaches Home Page and see set-up My Netflix Account link at the right bottom of page");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with Registered on netflix");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. verify register for Netflix is present | register for Netflix should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		launchAndPerformLogin(myTmoData);

		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.verifySignUpForNetflix();
	}

	/**
	 * US345085 Netflix Registered User content change
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "prodissue")
	public void verifyCrazyLegsBenefitsPageOptionsforNetflixegisteredUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyCrazyLegsBenefitsPageOptions method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Page  options");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with Registered on netflix");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log(
				"4.Verify Benefits Page content fo Netflix Registed cusotmer | Benefits Page Options should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.verifyNetflixSubscriptionText("You officially have Netflix, on us!");
		crazylegspage.verifyAddToYourAccountText("Standard Netflix plan $0/mo. for T-Mobile customers");
		crazylegspage.verifySignUpWithNetflixText("Premium Netflix plan only $3/mo. for T-Mobile customers");
	}

	/**
	 * US356335 Netflix Plan Benefits page - Add Netflix message copy based on
	 * Standard/Premium user
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void verifyCrazyLegsBenefitsPageOptionsforNetflixegisteredUser_Standard(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("verifyCrazyLegsBenefitsPageOptions method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Page  options");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with Registered on netflix");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log(
				"4.Verify Benefits Page content fo Netflix Registed cusotmer | Benefits Page Options should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.verifyNetflixSubscriptionText("You officially have Netflix, on us!");
		crazylegspage.verifyAddToYourAccountText("Standard Netflix plan $0/mo. for T-Mobile customers");
		crazylegspage.verifytextnotpresent("Premium Netflix plan only $3/mo. for T-Mobile customers");

	}

	/**
	 * US356335 Netflix Plan Benefits page - Add Netflix message copy based on
	 * Standard/Premium user
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE })
	public void verifyCrazyLegsBenefitsPageOptionsforNetflixegisteredUser_Premium(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("verifyCrazyLegsBenefitsPageOptions method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Page  options");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with Registered on netflix");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log(
				"4.Verify Benefits Page content fo Netflix Registed cusotmer | Benefits Page Options should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.verifyNetflixSubscriptionText("You officially have Netflix, on us!");

		crazylegspage.verifySignUpWithNetflixText("Premium Netflix plan only $3/mo. for T-Mobile customers");
		crazylegspage.verifytextnotpresent("Standard Netflix plan $0/mo. for T-Mobile customers");

	}

	/**
	 * US229374#CrazyLegs - Enable Scaling for Benefits Page - Add Netflix Image on
	 * the benefit page
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredby08092018release")
	public void testGoHomeCTAOnTheBenefitPageForRestrictedUsers(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testGoHomeCTAOnTheBenefitPageForRestrictedUsers method called in PlanBenefitsPageTest");
		Reporter.log("Test Case Name  : Verify Restricted USer Behavior in Benefits page");
		Reporter.log("Test Data : Standard msisdn from TMO TI POOLED Account");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("4.Verify go home button for standard and restricted Cusotmer | GO home should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.refreshPage();
		crazylegspage.verifyBenifitsPage();

	}

	/**
	 * US217133#CrazyLegs - Enable Business Accounts for Netflix Redemption -
	 * upgrade my plan - call care
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredby08092018release")
	public void verifyCrazyLegsNotEligible(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyCrazyLegsNotEligible method called in PlanBenefitsPageTest");
		Reporter.log("Test Case : Test Business Customer Experience in Benefits page.");
		Reporter.log("Test Data : Business Master Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5. Verify Header message is Dispalyed | Header message is Dispalyed");
		Reporter.log("6.VerifyContactCare Message for BUsiness User");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.verifyContactUsBtn();
		crazylegspage.clickOnContactUsBtn();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
	}

	/**
	 *
	 * US273070#Modify Netflix Microservice to Allow Hybrid BANs to Sign-Up for
	 * Netflix
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredby08092018release")
	public void verifyHybridBANsToSignUpForNetflix(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyHybridBANsToSignUpForNetflix method called in PlanBenefitsPageTest");

		Reporter.log("Test Case : Test Hybrid Customer Experience in Benefits page .");
		Reporter.log("Test Data : Hybrid Ban with Standard or Premium Netflix SOC not added");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5.Click on Set up Netflix button | Service list Page should be displayed");
		Reporter.log("6.Verify Service list ODF url | Service list ODF Page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.verifyTMobileOneNetflixImg();
		crazylegspage.clickOnSetUpNetFlixBtn();
		// verifyPlanServiceListPageTitle();
	}

	/**
	 *
	 * Un Authorized Customer Angular 4 upgrade
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredon0809RelWaseUpgradedToAngular4")
	public void testUnAuthorizedUserEntersBenefitsPage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testUnAuthorizedUserEntersBenefitsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test Case : Test Standard or Restricted  User Experience in Benefits page.");
		Reporter.log("Test Data : Standard/Restricted Msisdn");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5. Verify Header message is Dispalyed | Header message is Dispalyed");
		Reporter.log("6.Verify Message to Contact primary Account holder | Message should be dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.unAuthorizedheaderMessage("You made a solid choice going with T-Mobile ONE");
		crazyLegsBenefitsPage.unAuthorizedbodyDescription(
				"We're also throwing in Netflix, on us. Just one small detail, you're not authorized to make changes to this account. Please ask an authorized user to help you access Netflix.");

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void testSuspendedUserEntersBenefitsPage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testSuspendedUserEntersBenefitsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test Case : Test Involuntory Suspended Customer Experience in Benefits page.");
		Reporter.log("Test Data : Suspended PAH Msisdn due to non payment Msisdn");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5. Verify Header message is Dispalyed | Header message is Dispalyed");
		Reporter.log("6.Verify Suspended message and Payment Button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.verifyBenifitsPage();
		crazyLegsBenefitsPage.suspendedUserbodyDescription(
				"It looks like your account is currently suspended. Once your account is current, return to this page and set up Netflix. It's on us");
		crazyLegsBenefitsPage.verifyMakeAPaymentButton();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredon0809RelWaseUpgradedToAngular4")
	public void testBusinessUserExperienceInBenefitsPage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testBusinessUserExperienceInBenefitsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test Case : Test Business Customer Experience in Benefits page.");
		Reporter.log("Test Data : Business Master Msisdn");
		Reporter.log("==============================");
		Reporter.log(
				"Verify UnAuthorized User with RightPlan Reaches benefit Page and see to Contact Primary Account Holder");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5. Verify Header message is Dispalyed | Header message is Dispalyed");
		Reporter.log("6.VerifyContactCare Message for BUsiness User");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");

		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.verifyBenifitsPage();
		crazyLegsBenefitsPage
				.businessCustomerheaderMessage("Hmm, it doesn't look like you're eligible for these benefits");
		crazyLegsBenefitsPage.businessUserbodyDescription(
				"You'll need to upgrade your plan to T-Mobile ONE to get all of the perks, such as Netflix. Upgrade today; you'll be glad you did");
		crazyLegsBenefitsPage.verifyContactCarebutton();
	}

	/**
	 *
	 * ineligible rate plan Customer Angular 4 upgrade
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void testInEligibleRatePlanScenarioInBenefitsPage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testInEligibleRatePlanScenarioInBenefitsPag method called in PlanBenefitsPageTest");
		Reporter.log("Test Case : In eligible rate plan  User Experience in Benefits page.");
		Reporter.log("Test Data : SC or TE PAH Msisdn");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5. Verify Header message is Dispalyed | Header message is Dispalyed");
		Reporter.log("6.Verify Message to Upgrade plan is displayed | Message should be dispalyed");
		Reporter.log("6.Click on Upgrade button | Plan Feature page Should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.inEligibleRatePlanHeaders("Hmm, it doesn't look like you're eligible for these benefits");
		crazyLegsBenefitsPage.inEligibleRatePlanHeaders(
				"You'll need to upgrade your plan to T-Mobile ONE to get all of the perks, such as Netflix. Upgrade today; you'll be glad you did");
		crazyLegsBenefitsPage.verifyButton("Upgrade my plan");
		crazyLegsBenefitsPage.clickOnContactCareButton();
		FeaturedPlanPage featuredPlanPage = new FeaturedPlanPage(getDriver());
		featuredPlanPage.verifyFeaturedPlanPage();
	}

	/**
	 *
	 * Un Authorized permission Customer Angular 4 upgrade
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void testUnAuthorizedScenarioInBenefitsPage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testUnAuthorizedScenarioInBenefitsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test Case : Un Unthorized permission user Experience in Benefits page.");
		Reporter.log("Test Data :  pooled TMO TI non PAH Msisdn");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5. Verify Header message is Dispalyed | Header message is Dispalyed");
		Reporter.log("6.Verify Message to Contact primary Account holder | Message should be dispalyed");
		Reporter.log("7.lick on Go Home Button| Home Page should be dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.inEligibleRatePlanHeaders("You made a great choice in plans!");
		crazyLegsBenefitsPage.inEligibleRatePlanHeaders(
				"Netflix is also part of your plan--just one small detail: you're not authorized to make changes to this account. Please ask an authorized user on this account to help you access your Netflix options.");
		crazyLegsBenefitsPage.verifyButton("Go to My Account");
		crazyLegsBenefitsPage.clickOnContactCareButton();
		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
	}

	/**
	 * US217133#CrazyLegs - Enable Business Accounts for Netflix Redemption -
	 * upgrade my plan - call care
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void verifyBusinessUserScenarioInNetflixPlanBenefitsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBusinessUserScenarioInPlanBenefitsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test Case : Test Business Customer Experience in Benefits page.");
		Reporter.log("Test Data : Business Master Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5. Verify Header message is Dispalyed | Header message is Dispalyed");
		Reporter.log("6.Verify ContactCare Message for BUsiness User | Contact Care Button Should be Displayed");
		Reporter.log("7.Click on Contact Care Button | Contact Care page Should be Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/Netflix");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		// crazylegspage.InEligibleRatePlanHeader("We’re Throwing in Netflix on
		// us");
		crazylegspage.verifyButton("Contact care");
		crazylegspage.clickOnContactCareButton();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
	}

	/**
	 * US217133#CrazyLegs - Enable Business Accounts for Hulu Redemption - upgrade
	 * my plan - call care
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void verifyBusinessUserScenarioInHuluPlanBenefitsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBusinessUserScenarioInPlanBenefitsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test Case : Test Business Customer Experience in Benefits page.");
		Reporter.log("Test Data : Business Master Msisdn");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5. Verify Header message is Dispalyed | Header message is Dispalyed");
		Reporter.log("6.Verify ContactCare Message for BUsiness User | Contact Care Button Should be Displayed");
		Reporter.log("7.Click on Contact Care Button | Contact Care page Should be Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/HULU/");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		// crazylegspage.InEligibleRatePlanHeader("We’re Throwing in Netflix on
		// us");
		crazylegspage.verifyButton("Contact care");
		crazylegspage.clickOnContactCareButton();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
	}

	/**
	 * US217133#CrazyLegs -
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void verifyNetflixSOCNotAddedUserScenarioInPlanBenefitsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyNetflixSOCNotAddedUserScenarioInPlanBenefitsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test Case : Test Ntflix SOC not added Experience in Benefits page.");
		Reporter.log("Test Data : Pooled TMO ONE TI PAH ");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5. Verify Header message is Dispalyed | Header message is Dispalyed");
		Reporter.log("6.Set-Up netflix on my plan button is Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.inEligibleRatePlanHeaders("Good news! You get Netflix benefits too.");
		crazylegspage.verifyButton("Set up Netflix");
		crazylegspage.clickOnContactCareButton();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
	}

	/*
	 * DE174548#CrazyLegs -
	 *
	 * @param data
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void verifyTMOONEPlusMilitaryWithFamilyONEPlusFamilyNetflixSOCNotAddedUserScenario(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("verifyNetflixSOCNotAddedUserScenarioInPlanBenefitsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test Case : Test Ntflix SOC not added Experience in Benefits page.");
		Reporter.log("Test Data : Pooled TMO ONE TI PAH ");
		Reporter.log("==============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5. Verify Header message is Dispalyed | Header message is Dispalyed");
		Reporter.log("6.Set-Up netflix on my plan button is Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.inEligibleRatePlanHeaders("Good news! You get Netflix benefits too.");
		crazylegspage.verifyButton("Set up Netflix");
		crazylegspage.clickOnContactCareButton();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
	}

	/**
	 * US189765#Crazy Legs Restricted Offer (M&D) - Allow CL Email Retrieval
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void verifyNetflixRegisteredUserRecoverMail(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyNetflixRegisteredUserRecoverMail method called in PlanBenefitsPageTest");

		Reporter.log(
				"Test Case Name :Verify Netflix Registered Customer reaches benefit page has to see Recover Email link");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with Registered on netflix");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate Crazy legs Benefit Page | Crazy legs Benefit Page should be displayed");
		Reporter.log("4. Verify Crazy legs Benefit Page | Crazy legs Benefit Page should be verified");
		Reporter.log("5. Verify Email Recovering link is present | Recovr it here link should be displayed");
		Reporter.log("6. Click on Email recover link  | Email used to register in netflix Should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.clickRecoveritHerelink();
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.acceptIOSAlert();
		crazyLegsBenefitsPage.verifyNetflixPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredduetonewhomepage")
	public void testNetSOCAddedButNotRegisteredScenarioInPlanBenefitsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testNetSOCAddedButNotRegisteredScenarioInPlanBenefitsPage method called in PlanBenefitsPageTest");
		Reporter.log(
				"Verify Authorized user with RatePlan and Added CL SOC reaches Benefits Page and see set-up My Netflix Account button");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with Standard Netflix added");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to Crazy legs Benefit Page | Cazy legs Benefit Page should be displayed");
		Reporter.log("4. Verify Crazy legs Benefit Page | Cazy legs Benefit Page should be verified");
		Reporter.log("5. Click on SignUpMyNetflix Button | Page Should Navigate to Netflix page");
		Reporter.log("6.Verify Netflix page is displayed |  Netflix Page should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.inEligibleRatePlanHeaders("We're throwing in Netflix on us");
		crazyLegsBenefitsPage.verifyButton("Sign up for Netflix");
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.acceptIOSAlert();
		commonPage.switchToSecondWindow();
	}

	/**
	 * US191691#Crazy Legs: Legalese Story US196354#Crazy legs restricted offer
	 * (M&D) - Benefits page - Suppress footer and no global UNAV
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyPlanBenefitsLegaleseTerms(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyCrazyLegBenifitPageLegaleseAndSuppressFooter method called in PlanBenefitsPageTest");

		Reporter.log("Test Case Name: Test Benefits page Legalese Terms and Conditions");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be logged in successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Signup for Netflix link | Nextflix plan-benefits Page should be displayed");
		Reporter.log("4. Verify Legalese terms | Legalese terms should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.checkLegaleasee(
				"Redeeming this offer will replace your current Netflix payment method with T-Mobile. Offer subject to change. Receive Netflix Basic on us (1-screen, up to $8.99/mo. value) while you maintain 2+ qual'g Magenta lines, receive Netflix Standard on us (2-screen, up to $12.99/mo. value) while you maintain 2+ qual’g Magenta Plus lines, or receive Netflix Standard for $2/mo. (2-screen, up to $12.99 value) while you maintain 2+ qual’g T-Mobile ONE lines. Lines must remain in good standing");
	}

	/**
	 *
	 * US273070#Modify Netflix Microservice to Allow Hybrid BANs to Sign-Up for
	 * Netflix
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void verifyHybridBanToSignUpForNetflix(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyHybridBANsToSignUpForNetflix method called in PlanBenefitsPageTest");

		Reporter.log("Test Case : Test Hybrid Customer Experience in Benefits page .");
		Reporter.log("Test Data : Hybrid Ban with Standard or Premium Netflix SOC not added");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5.Click on Set up Netflix button | Service list Page should be displayed");
		Reporter.log("6.Verify Service list ODF url | Service list ODF Page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.verifyTMobileOneNetflixImg();
		crazylegspage.verifyButton("Sign up for Netflix");
		crazylegspage.clickOnContactCareButton();
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.acceptIOSAlert();
		crazylegspage.verifyNetflixPage();
	}

	/**
	 * US310747 -( M & D )Show 'Eligible for NFX' in Important Message only for
	 * those w/o NFX SOC - MYTMO Homepage
	 * 
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void TestNeflixRegisteredUserDontSeeSignUpMessageInHomePage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testAuthorizedUserwithRatePlanAndCLSOCAddedEntersHomePage method called in PlanBenefitsPageTest");
		Reporter.log(
				"Test Case Name :Verify Authorized user with RatePlan and Added CL SOC reaches Home Page and see set-up My Netflix Account link at the right bottom of page");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with Registered on netflix");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. verify register for Netflix is present | register for Netflix should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.verifySignUpForNetflixNotPresent();

	}

	/**
	 * US310747 -( M & D )Show 'Eligible for NFX' in Important Message only for
	 * those w/o NFX SOC - MYTMO Homepage
	 * 
	 *
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredduetonewhomepage")
	public void verifyNetflixLinkOnHomePageForNetflixSOCAddedButNotRegistered(ControlTestData data,
			MyTmoData myTmoData) {

		logger.info(
				"VerifyNetflixLinkOnHomePageForNetflixSOCAddedButNotRegistered method called in PlanBenefitsPageTest");

		Reporter.log(
				"Test Case Name : Verify Authorized user with RatePlan and Added CL SOC reaches Home Page and see set-up My Netflix Account link at the right bottom of page");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with Standard Netflix added");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Register for Netflix link  |  Page Should navigate to Benefits page");
		Reporter.log("4. Navigate to Crazy legs Benefit Page | Crazy legs Benefit Page should be displayed");
		Reporter.log("5. Verify Crazy legs Benefit Page | Crazy legs Benefit Page should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.verifyImportantMessage("As a T-Mobile ONE customer, you’re now eligible for Netflix!");
		homePage.clickOnRegisterForNetflixLink();
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.verifyBenifitsPage();

	}

	/**
	 * US310747 -( M & D )Show 'Eligible for NFX' in Important Message only for
	 * those w/o NFX SOC - MYTMO Homepage
	 * 
	 *
	 * @param data
	 * @param myTmoData US408345
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredduetonewhomepage")
	public void verifyNetflixLinkOnHomePageForNetflixSOCNotAdded(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyNetflixLinkOnHomePageForNetflixSOCNotAdded method called in PlanBenefitsPageTest");

		Reporter.log(
				"Test Case Name : Verify Authorized user with RatePlan and Added CL SOC reaches Home Page and see set-up My Netflix Account link at the right bottom of page");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with Standard Netflix added");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Register for Netflix link  |  Page Should navigate to Benefits page");
		Reporter.log("4. Navigate to Crazy legs Benefit Page | Crazy legs Benefit Page should be displayed");
		Reporter.log("5. Verify Crazy legs Benefit Page | Crazy legs Benefit Page should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.verifyImportantMessage("As a T-Mobile ONE customer, you’re now eligible for Netflix!");
		homePage.clickOnRegisterForNetflixLink();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifyfamilyandEntertainmentHeader();
	}

	/**
	 * US408345 -URGENT - The Tonight Show - Netflix On Us Eligibility Update
	 * 
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retiredduetonewhomepage")
	public void verifyNetflixRedemptionforPromotionalPlan_PFRULFM2(ControlTestData data, MyTmoData myTmoData) {

		logger.info(
				"VerifyNetflixLinkOnHomePageForNetflixSOCAddedButNotRegistered method called in PlanBenefitsPageTest");

		Reporter.log(
				"Test Case Name : Verify Authorized user with RatePlan and Added CL SOC reaches Home Page and see set-up My Netflix Account link at the right bottom of page");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED MPFRULFM2 premium soc added");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Register for Netflix link  |  Page Should navigate to Benefits page");
		Reporter.log("4. Navigate to Crazy legs Benefit Page | Crazy legs Benefit Page should be displayed");
		Reporter.log("5. Verify Crazy legs Benefit Page | Crazy legs Benefit Page should be verified");
		Reporter.log("6. Click on SignUpMyNetflix Button | Page Should Navigate to Netflix page");
		Reporter.log("7.Verify Netflix page is displayed |  Netflix Page should be verified");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.verifyImportantMessage("As a T-Mobile ONE customer, you’re now eligible for Netflix!");
		homePage.clickOnRegisterForNetflixLink();
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.verifyBenifitsPage();
		crazyLegsBenefitsPage.verifyButton("Sign up for Netflix");
		CommonPage CommonPage = new CommonPage(getDriver());
		CommonPage.acceptIOSAlert();
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.switchToSecondWindow();
	}

	/**
	 * US408345 -URGENT - The Tonight Show - Netflix On Us Eligibility Update
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void verifyNetflixRegisteredforPromotionalPlan_PFRULFM3(ControlTestData data, MyTmoData myTmoData) {

		logger.info(
				"VerifyNetflixLinkOnHomePageForNetflixSOCAddedButNotRegistered method called in PlanBenefitsPageTest");

		Reporter.log("Test Case Name : Netflix Registered");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn PFRULFM3 registered");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Cazy legs Benifit Page | Cazy legs Benifit Page should be verified");
		Reporter.log("5. Verify Header message is Dispalyed | Header message is Dispalyed");
		Reporter.log("6.Verify Message to Contact primary Account holder | Message should be dispalyed");
		Reporter.log("7.lick on Go Home Button| Home Page should be dispalyed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazyLegsBenefitsPage = new CrazyLegsBenefitsPage(getDriver());
		crazyLegsBenefitsPage.verifyButton("Go Home");
		crazyLegsBenefitsPage.clickOnContactCareButton();
		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "q1qq")
	public void registerMsisdnqlab03(ControlTestData data, MyTmoData myTmoData) throws InterruptedException {

		LoginPage loginpage = new LoginPage(getDriver());
		loginpage.unlinkMsisdn(myTmoData);
		getTMOURL(myTmoData);
		loginpage.registerMsisdnqlab03(myTmoData);
	}

	/**
	 * DE120177 - I/S account type not being treated as Business account on Plan
	 * Benefits page
	 * 
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyContactCareOnclickingSignUpNetflixforBusinessCustomer(ControlTestData data, MyTmoData myTmoData) {

		logger.info(
				"VerifyContactCareOnclickingSignUpNetflixforBusinessCustomer method called in PlanBenefitsPageTest");

		Reporter.log("Test Case Name : verify contact care business user on clicking sign up for netflixon home page");
		Reporter.log("Test Data : PAH pooled Business Customer");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Register for Netflix link  |  Page Should navigate to Benefits page");
		Reporter.log("4. verify Contact Care Popup");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		PlanDetailsPage planDetailsPage = navigateToPlanDetailsPage(myTmoData);
		planDetailsPage.verifyContactUsPage();
	}

	/**
	 * DE120177 - I/S account type not being treated as Business account on Plan
	 * Benefits page
	 * 
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "accounts")
	public void verify_ISAccoutnType_ExperienceInPlanBenefitsPage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("Verify_ISAccoutnType_ExperienceInPlanBenefitsPage method called in PlanBenefitsPageTest");

		Reporter.log("Test Case Name : verify contact care business user on plan benefits page");
		Reporter.log("Test Data : PAH I/S Type pooled Business Customer");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Naviagte to Plan-Benefits page | Plan-bnefits page should be displayed");
		Reporter.log("4. verify Contact Care button");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");

		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.verifyButton("Contact care");
		crazylegspage.clickOnContactCareButton();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
	}

	/**
	 * US559395 - May 2019 - MyTMO [D&M] - Netflix updates needed for existing
	 * Change Plan confirmation page
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void verifySCTOMegantaPendingRatePlanExperienceInPlanBenefitsPage(ControlTestData data,
			MyTmoData myTmoData) {

		logger.info(
				"verifySCTOMegantaPendingRatePlanExperienceInPlanBenefitsPage method called in PlanBenefitsPageTest");

		Reporter.log(
				"Test Case Name : verify benefits page for pending rate plan customer from a plan with no netflix");
		Reporter.log("Test Data : simple choice to Meganta plus rateplan with future dated");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Naviagte to Plan-Benefits page | Plan-bnefits page should be displayed");
		Reporter.log("4. verify benefits page");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.clickOnSetUpNetFlixBtn();
		verifyManageDataAndAddOnsPage();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("1 Screen SD Netflix $8.99", "On Us");
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("2 Screens HD Netflix $12.99", "$4.00/mo");
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("4 Screens HD Netflix $15.99", "$7.00/mo");
		manageAddOnsSelectionPage.verifyNetflixServicesSize(3);

	}

	/**
	 * US559395 - May 2019 - MyTMO [D&M] - Netflix updates needed for existing
	 * Change Plan confirmation page
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "netflix")
	public void verifyTOMegantaPendingRatePlanExperienceInPlanBenefitsPage(ControlTestData data, MyTmoData myTmoData) {

		logger.info(
				"verifySCTOMegantaPendingRatePlanExperienceInPlanBenefitsPage method called in PlanBenefitsPageTest");

		Reporter.log(
				"Test Case Name : verify benefits page for pending rate plan customer from a plan with no netflix");
		Reporter.log("Test Data : simple choice to Meganta plus rateplan with future dated");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Naviagte to Plan-Benefits page | Plan-bnefits page should be displayed");
		Reporter.log("4. verify benefits page");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.clickOnSetUpNetFlixBtn();
		verifyManageDataAndAddOnsPage();
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("1 Screen SD Netflix $8.99", "On Us");
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("2 Screens HD Netflix $12.99", "$4.00/mo");
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("4 Screens HD Netflix $15.99", "$7.00/mo");
		manageAddOnsSelectionPage.verifyNetflixServicesSize(3);
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifySocNotAddedScenario(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifySocNotAddedScenario method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Page  options");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with  netflix SOC not Added");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Benefits Page content for  SOC not Added");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());

		crazylegspage.verifyThrowingInNetflixText("Good news! You get Netflix benefits too.");

		crazylegspage.verifyNetflixSubscriptionText(
				"We're including Netflix benefits in your plan. Just two easy steps and you'll be streaming TV shows and movies in no time");
		crazylegspage.verifyNetflixSubscriptionText("Add it to your T-Mobile account");
		crazylegspage.verifyNetflixSubscriptionText("Set up your Netflix account");

		crazylegspage.verifyButton("Set up Netflix");
		crazylegspage.clickOnContactCareButton();

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("Netflix Standard plus Family Allowances ($18 value)",
				"On us");
		manageAddOnsSelectionPage.verifypriceforNetflixSoc("Netflix Premium plus Family Allowances ($21 value)",
				"$3.00/mo");
		// manageAddOnsSelectionPage.verifyNetflixServicesSize(2);
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifySocAddedScenario(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifySocAddedScenario method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Page  options");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with  netflix SOC  Added");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Benefits Page content for  SOC  Added");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());

		crazylegspage.verifyThrowingInNetflixText("Good news! Netflix is part of your plan.");

		crazylegspage.verifyNetflixSubscriptionText("Activate your Netflix offer below.");
		crazylegspage
				.verifyNetflixSubscriptionText("If you would like to manage your T-Mobile services, you can see them");

		crazylegspage.verifyButton("Activate your Netflix offer");
		crazylegspage.clickOnContactCareButton();

		crazylegspage.verifyNetflixPage();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verify_1Screen_SocAddedScenario(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Verify_1Screen_SocAddedScenario method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Page  options");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with 1 screen  netflix SOC  Added");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Benefits Page content for 1 screen  SOC  Added");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());

		crazylegspage.verifyThrowingInNetflixText("Good news! Netflix is part of your plan.");

		crazylegspage.verifyNetflixSubscriptionText("Activate your Netflix offer below.");
		crazylegspage
				.verifyNetflixSubscriptionText("If you would like to manage your T-Mobile services, you can see them");
		crazylegspage.verifyNetflixSubscriptionText(
				"Customers who select Netflix Basic (1-screen) may receive Netflix Standard (2-screen) for a short time at no additional cost. After that point, these accounts will automatically move to Netflix Basic.");

		crazylegspage.verifyButton("Activate your Netflix offer");
		crazylegspage.clickOnContactCareButton();

		crazylegspage.verifyNetflixPage();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testNetflixRegisteredScenario(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testNetflixRegisteredScenario method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Page  options");
		Reporter.log("Test Data : PAH TMO ONE TI POOLED Msisdn with  netflix SOC  Added and Registered");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Verify Benefits Page content for  SOC Netflix registered Added");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());

		crazylegspage.verifyThrowingInNetflixText("Thanks for signing up for Netflix!");

		crazylegspage.verifyNetflixSubscriptionText(
				"Solid choice. If you can't remember the email address used to sign up for your Netflix account,");

		crazylegspage.verifyButton("Go to My Account");
		crazylegspage.clickOnContactCareButton();

		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();

		navigateToAnyURL("com", "com/plan-benefits");
		crazylegspage.verifyBenifitsPage();
		crazylegspage.clickRecoveritHerelink();

		crazylegspage.verifyNetflixPage();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyUnAuthorizedPermissionScenario(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyUnAuthorizedPermissionScenario method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Page  options");
		Reporter.log("Test Data : Standard Customer with netflix on  ban");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.Standard Customer with netflix on  ban");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());

		crazylegspage.verifyThrowingInNetflixText("You made a great choice in plans!");

		crazylegspage.verifyNetflixSubscriptionText(
				"Netflix is also part of your plan--just one small detail: you're not authorized to make changes to this account. Please ask an authorized user on this account to help you access your Netflix options.");
		crazylegspage.verifyButton("Go to My Account");
		crazylegspage.clickOnContactCareButton();

		LineDetailsPage lineDetailsPage = new LineDetailsPage(getDriver());
		lineDetailsPage.verifyLineDetailsPage();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void VerifyInEligibleRatePlanPermissionScenario(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyInEligibleRatePlanPermissionScenario method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Page  options");
		Reporter.log("Test Data : non pooled or TE plans");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Get Cazy legs Benifit Page | Cazy legs Benifit Page should be displayed");
		Reporter.log("4.  verify Upgrade your plan");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/plan-benefits");
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());

		crazylegspage.verifyThrowingInNetflixText("Hmm, it doesn't look like you're eligible for these benefits");

		crazylegspage.verifyNetflixSubscriptionText(
				"You'll need to upgrade your plan to get all the cool benefits. Upgrade today, you'll be glad you did");
		crazylegspage.verifyButton("Upgrade my plan");
		crazylegspage.clickOnContactCareButton();

		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
		plansComparisonPage.verifyPlanComparisionPage();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void verifyBenefitRedemptionForSuspendedAccountForLatePaymentNetflix(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyBenefitRedemptionForSuspendedAccountForLatePaymentNetflix method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Redemption page for Netflix ");
		Reporter.log("Test Data : suspended account for late payment ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Netflix should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/Netflix");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyAuthorabletext("Oops!", benefitsRedemptionPage.oopsText);
		benefitsRedemptionPage.verifyAuthorabletext("Your account is temporarily suspended",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext(
				"You will be unable to register for this benefit until your account has been restored. For more information, please contact Customer Care",
				benefitsRedemptionPage.messages);
		benefitsRedemptionPage.verifyAuthorabletext3InGreyColor(
				"You will be unable to register for this benefit until your account has been restored.  For more information, please contact");
		benefitsRedemptionPage.verifyCustomerCareBtnInAuthorabletext("Customer Care");
		benefitsRedemptionPage.verifyAndClickOnRestoreBtn("Restore account");
		benefitsRedemptionPage.verifyAndClickOnCancelBtn("Cancel");

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void verifyBenefitRedemptionForSuspendedAccountForLatePaymentHulu(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyBenefitRedemptionForSuspendedAccountForLatePaymentHulu method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Redemption page for Hulu ");
		Reporter.log("Test Data : suspended account for late payment ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Netflix should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/HULU/");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyAuthorabletext("Oops!", benefitsRedemptionPage.oopsText);
		benefitsRedemptionPage.verifyAuthorabletext("Your account is temporarily suspended",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext(
				"You will be unable to register for this benefit until your account has been restored. For more information, please contact Customer Care",
				benefitsRedemptionPage.messages);
		benefitsRedemptionPage.verifyAuthorabletext3InGreyColor(
				"You will be unable to register for this benefit until your account has been restored.  For more information, please contact");
		benefitsRedemptionPage.verifyCustomerCareBtnInAuthorabletext("Customer Care");
		benefitsRedemptionPage.verifyAndClickOnRestoreBtn("Restore account");
		benefitsRedemptionPage.verifyAndClickOnCancelBtn("Cancel");

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void verifyBenefitRedemptionForSuspendedAccountForLatePaymentvideochoice(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifyBenefitRedemptionForSuspendedAccountForLatePaymentvideochoice method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Redemption page for Video Choice ");
		Reporter.log("Test Data : suspended account for late payment ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Netflix should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyAuthorabletext("Oops!", benefitsRedemptionPage.oopsText);
		benefitsRedemptionPage.verifyAuthorabletext("Your account is temporarily suspended",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext(
				"You will be unable to register for this benefit until your account has been restored. For more information, please contact Customer Care",
				benefitsRedemptionPage.messages);
		benefitsRedemptionPage.verifyAuthorabletext3InGreyColor(
				"You will be unable to register for this benefit until your account has been restored.  For more information, please contact");
		benefitsRedemptionPage.verifyCustomerCareBtnInAuthorabletext("Customer Care");
		benefitsRedemptionPage.verifyAndClickOnRestoreBtn("Restore account");
		benefitsRedemptionPage.verifyAndClickOnCancelBtn("Cancel");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void verifyBenefitRedemptionForRestrictedUserHulu(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBenefitRedemptionForRestrictedUserHulu method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Redemption page for Video Choice ");
		Reporter.log("Test Data : suspended account for late payment ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Netflix should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyAuthorabletext("Oops!", benefitsRedemptionPage.oopsText);
		benefitsRedemptionPage.verifyAuthorabletext("Your account is temporarily suspended",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext(
				"You will be unable to register for this benefit until your account has been restored. For more information, please contact Customer Care",
				benefitsRedemptionPage.messages);
		benefitsRedemptionPage.verifyAuthorabletext3InGreyColor(
				"You will be unable to register for this benefit until your account has been restored.  For more information, please contact");
		benefitsRedemptionPage.verifyCustomerCareBtnInAuthorabletext("Customer Care");
		benefitsRedemptionPage.verifyAndClickOnRestoreBtn("Restore account");
		benefitsRedemptionPage.verifyAndClickOnCancelBtn("Cancel");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void verifyBenefitRedemptionForRestricteduserNetflix(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBenefitRedemptionForRestricteduserNetflix method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Redemption page for Video Choice ");
		Reporter.log("Test Data : suspended account for late payment ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Netflix should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyAuthorabletext("Oops!", benefitsRedemptionPage.oopsText);
		benefitsRedemptionPage.verifyAuthorabletext("Your account is temporarily suspended",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext(
				"You will be unable to register for this benefit until your account has been restored. For more information, please contact Customer Care",
				benefitsRedemptionPage.messages);
		benefitsRedemptionPage.verifyAuthorabletext3InGreyColor(
				"You will be unable to register for this benefit until your account has been restored.  For more information, please contact");
		benefitsRedemptionPage.verifyCustomerCareBtnInAuthorabletext("Customer Care");
		benefitsRedemptionPage.verifyAndClickOnRestoreBtn("Restore account");
		benefitsRedemptionPage.verifyAndClickOnCancelBtn("Cancel");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void verifyBenefitRedemptionForRestrictedUservideochoice(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBenefitRedemptionForRestrictedUservideochoice method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Redemption page for Video Choice ");
		Reporter.log("Test Data : suspended account for late payment ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Netflix should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyAuthorabletext("Oops!", benefitsRedemptionPage.oopsText);
		benefitsRedemptionPage.verifyAuthorabletext("Your account is temporarily suspended",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext(
				"You will be unable to register for this benefit until your account has been restored. For more information, please contact Customer Care",
				benefitsRedemptionPage.messages);
		benefitsRedemptionPage.verifyAuthorabletext3InGreyColor(
				"You will be unable to register for this benefit until your account has been restored.  For more information, please contact");
		benefitsRedemptionPage.verifyCustomerCareBtnInAuthorabletext("Customer Care");
		benefitsRedemptionPage.verifyAndClickOnRestoreBtn("Restore account");
		benefitsRedemptionPage.verifyAndClickOnCancelBtn("Cancel");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "828Release")
	public void verifyBenefitRedemptionNameIdSocNotAdded(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBenefitRedemptionNameIdSocNotAdded method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify Benefit Redemption Name IdSoc NotAdded ");
		Reporter.log("Test Data : Megenta user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for NAme id|  Benefits Redemption page for name id should be displayed");
		Reporter.log("4.  Manage addons page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/nameid");
		ManageAddOnsSelectionPage ManageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();
		ManageAddOnsPage.verifyCurrentPageURL("/odf/Service:ALL", "/odf/Service:ALL");
		ManageAddOnsPage.VerifyManageAddOnSOCs("Name ID");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "828Release1")
	public void verifySuzySheepPitchPageAuthorabletext(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepPitchPage method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify Suzy Sheep Pitch Page ");
		Reporter.log("Test Data :  Name id Eligible user");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify authorable text Suzy sheep");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/account/pitch-page/suzysheep");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("SuzySheep", benefitsRedemptionPage.pitchPageM1);
		benefitsRedemptionPage.verifyAuthorabletext(
				"The un-carrier leads the pack when it comes to caller protection services.",
				benefitsRedemptionPage.pitchPageM2);
		benefitsRedemptionPage.verifyAllAuthorabletext("Block and categorize callers",
				benefitsRedemptionPage.pitchPageAuthHeaders);
		benefitsRedemptionPage.verifyAllAuthorabletext(
				"With SuzySheep Premium, you can block numbers, send numbers straight to voice mail, or add them your favorites. You can also use our number lookup tool to find out who is calling and why",
				benefitsRedemptionPage.pitchPageAuthMsgs);
		benefitsRedemptionPage.verifyAllAuthorabletext("Caller ID and scam blocking",
				benefitsRedemptionPage.pitchPageAuthHeaders);
		benefitsRedemptionPage.verifyAllAuthorabletext(
				"With SuzySheep, you’ll be able to identify any caller before even picking up the phone. You’ll automatically be able to block scam calls. ",
				benefitsRedemptionPage.pitchPageAuthMsgs);
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "828Release1")
	public void verifySuzySheepPitchPageManageCallerIDLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepPitchPage method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify Suzy Sheep Pitch Page ");
		Reporter.log("Test Data :  Name id Eligible user");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify Suzy sheep Manage CallerID Link");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/account/pitch-page/suzysheep");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyNclickPitchPageAuthorableLinks("Manage Caller ID >");
		benefitsRedemptionPage.verifyBlockingPageURL();
		benefitsRedemptionPage.verifyAllAuthorabletext("Caller ID and scam blocking",
				benefitsRedemptionPage.pitchPageAuthHeaders);
		benefitsRedemptionPage.verifyAllAuthorabletext(
				"With SuzySheep, you’ll be able to identify any caller before even picking up the phone. You’ll automatically be able to block scam calls. ",
				benefitsRedemptionPage.pitchPageAuthMsgs);
		benefitsRedemptionPage.verifyNclickPitchPageAuthorableLinks("Buy SuzySheep Premium >");
		benefitsRedemptionPage.verifyNameIdPageURL();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "828Release1")
	public void verifySuzySheepPitchPageBuySuzySheepPremiumLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepPitchPage method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify Suzy Sheep Pitch Page ");
		Reporter.log("Test Data :  Name id Eligible user");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify Suzy sheep Manage CallerID Link");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/account/pitch-page/suzysheep");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyNclickPitchPageAuthorableLinks("Buy SuzySheep Premium >");
		benefitsRedemptionPage.verifyNameIdPageURL();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "828Release")
	public void verifySuzySheepInstructionsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepInstructionsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify SuzySheep Instructions Page ");
		Reporter.log("Test Data : Megenta plus user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify authorable text Suzy sheep");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/nameid");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("It's time to take control of your calls.",
				benefitsRedemptionPage.message4);

		benefitsRedemptionPage.verifyAuthorabletext(
				"Make sure you have NameId App to enjoy premium call management features.",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext("Turn on Caller ID, Scam Block, and more.",
				benefitsRedemptionPage.message5);

		benefitsRedemptionPage.verifybtnAppStore();
		benefitsRedemptionPage.verifybtnPlayStore();
		benefitsRedemptionPage.verifyAndClickOnBtn("Back to plan details");
		benefitsRedemptionPage.verifyAuthorabletext(
				"Visit the manage data and add-ons page to manage your premium T-Mobile services.",
				benefitsRedemptionPage.message8);

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "828Release")
	public void verifySuzySheepNavigatesToManageAddonsWhenNAmeIdisNotAdded(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepInstructionsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify SuzySheep Instructions Page ");
		Reporter.log("Test Data : Megenta plus user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify authorable text Suzy sheep");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/nameid");

		ManageAddOnsSelectionPage ManageAddonsSelection = new ManageAddOnsSelectionPage(getDriver());
		ManageAddonsSelection.verifyAddOnPageWhenOnlyOneSOCEligible();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-775s1NotDone")
	public void verifySuzySheepInstructionsPageWhenCustomerAddsNameIDSOC(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepInstructionsPageWhenCustomerAddsNameIDSOC method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verifySuzySheepInstructionsPageWhenCustomerAddsNameIDSOC ");
		Reporter.log("Test Data : Meganta or TMO one user Name id added ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify authorable text Suzy sheep");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/DataService:ALL/Service:ALL/DataPass:ALL");
		/*
		 * ManageAddOnsSelectionPage ManageAddonsSelection = new
		 * ManageAddOnsSelectionPage(getDriver());
		 * ManageAddonsSelection.checkNameIDCheckBoxs();
		 * 
		 * ODFDataPassReviewPage ODFDataPass = new ODFDataPassReviewPage(getDriver());
		 * ODFDataPass.agreeAndSubmitButton();
		 */

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("It's time to take control of your calls.",
				benefitsRedemptionPage.message4);

		benefitsRedemptionPage.verifyAuthorabletext(
				"Make sure you have NameId App to enjoy premium call management features.",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext("Turn on Caller ID, Scam Block, and more.",
				benefitsRedemptionPage.message5);
		benefitsRedemptionPage.verifyAndClickOnBtn("Turn on Caller ID, Scam Block, and more.");
		benefitsRedemptionPage.verifyBlockingPageURL();
		benefitsRedemptionPage.verifybtnAppStore();
		benefitsRedemptionPage.verifybtnPlayStore();
		benefitsRedemptionPage.verifyAndClickOnBtn("Back to plan details");
		benefitsRedemptionPage.verifyAuthorabletext(
				"Visit the manage data and add-ons page to manage your premium T-Mobile services.",
				benefitsRedemptionPage.message8);
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-775s2NotDone")
	public void verifyNameidSocAvilableInManageAddonsPageWithEligibleRatePlan(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("verifySuzySheepInstructionsPageWhenCustomerAddsNameIDSOC method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verifySuzySheepInstructionsPageWhenCustomerAddsNameIDSOC ");
		Reporter.log("Test Data : Megenta or TMO one user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify authorable text Suzy sheep");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/DataService:ALL/Service:ALL/DataPass:ALL");
		ManageAddOnsSelectionPage ManageAddonsSelection = new ManageAddOnsSelectionPage(getDriver());
		try {
			for (WebElement webElement : ManageAddonsSelection.nameIDCheckBox) {
				webElement.isEnabled();
				Reporter.log("Name id check box enabled");
			}
		} catch (Exception e) {
			Assert.fail("Name Id CheckBox is not enabled");
		}
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-775s2NotDone")
	public void verifySuzySheepInstructionsPageWhenCustomerAddsNameIDandNetflixSOCsattheSameTime(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"verifySuzySheepInstructionsPageWhenCustomerAddsNameIDandNetflixSOCsattheSameTime method called in PlanBenefitsPageTest");
		Reporter.log(
				"Test case Name : verifySuzySheepInstructionsPageWhenCustomerAddsNameIDandNetflixSOCsattheSameTime ");
		Reporter.log("Test Data : Any Plan PAH user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify authorable text Suzy sheep");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/DataService:ALL/Service:ALL/DataPass:ALL");
		ManageAddOnsSelectionPage ManageAddonsSelection = new ManageAddOnsSelectionPage(getDriver());
		ManageAddonsSelection.checkNameIDCheckBoxs();
		ManageAddonsSelection.checkNetFlixPremiumSocCheckBox();

		ODFDataPassReviewPage ODFDataPass = new ODFDataPassReviewPage(getDriver());
		ODFDataPass.agreeAndSubmitButton();

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("One last step...", benefitsRedemptionPage.message4);

		benefitsRedemptionPage.verifyAuthorabletext(
				"To finish activating, just log in to your Hulu account or create a new one.",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext("By clicking Activate Netflix, I agree to the ",
				benefitsRedemptionPage.message5);
		benefitsRedemptionPage.verifyAuthorabletext("Terms and condition ", benefitsRedemptionPage.message6);
		benefitsRedemptionPage.clickBtnNVerifyNewTab("Activate Netflix", "Netflix");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-784s1NotDone")
	public void verifySuzySheepInstructionsforEligibleRatePlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepInstructionsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify SuzySheep Instructions Page ");
		Reporter.log("Test Data : Megenta plus user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify authorable text Suzy sheep");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/nameid");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("It's time to take control of your calls.",
				benefitsRedemptionPage.message4);

		benefitsRedemptionPage.verifyAuthorabletext(
				"Make sure you have NameId App to enjoy premium call management features.",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext("Turn on Caller ID, Scam Block, and more.",
				benefitsRedemptionPage.message5);
		benefitsRedemptionPage.verifyAndClickOnBtn("Turn on Caller ID, Scam Block, and more.");
		benefitsRedemptionPage.verifyBlockingPageURL();
		benefitsRedemptionPage.verifybtnAppStore();
		benefitsRedemptionPage.verifybtnPlayStore();
		benefitsRedemptionPage.verifyAndClickOnBtn("Back to plan details");
		benefitsRedemptionPage.verifyAuthorabletext(
				"Visit the manage data and add-ons page to manage your premium T-Mobile services.",
				benefitsRedemptionPage.message8);
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-784s1NotDone")
	public void verifyNameIdNotEnabledInManageAddonsPageforEligibleRatePlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepInstructionsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify SuzySheep Instructions Page ");
		Reporter.log("Test Data : Megenta plus user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify authorable text Suzy sheep");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/DataService:ALL/Service:ALL/DataPass:ALL");
		ManageAddOnsSelectionPage ManageAddonsSelection = new ManageAddOnsSelectionPage(getDriver());
		try {
			for (WebElement webElement : ManageAddonsSelection.nameIDCheckBox) {
				webElement.isEnabled();
				Assert.fail("Name id check box should not be enabled");
			}
		} catch (Exception e) {
			Reporter.log("Name Id CheckBox is not enabled");
		}
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-996")
	public void verifyNameIdSocNotEnabledInManageAddonsPageforBusinessUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepInstructionsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify SuzySheep Instructions Page ");
		Reporter.log("Test Data : Business user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to Manage Addons Page");
		Reporter.log("4.  verify Name id soc enabled");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "odf/DataService:ALL/Service:ALL/DataPass:ALL");
		ManageAddOnsSelectionPage ManageAddonsSelection = new ManageAddOnsSelectionPage(getDriver());
		try {
			for (WebElement webElement : ManageAddonsSelection.nameIDCheckBox) {
				webElement.isEnabled();
				Assert.fail("Name id check box should not be enabled");
			}
		} catch (Exception e) {
			Reporter.log("Name Id CheckBox is not enabled");
		}
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-996")
	public void verifyBusinessUserTemplateforNameIDNotadded(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepInstructionsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify SuzySheep Instructions Page ");
		Reporter.log("Test Data : Business user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify Contact care Authorable text");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/nameid");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("Please contact us to add Call Manager Premium",
				benefitsRedemptionPage.message4);

		benefitsRedemptionPage.verifyAuthorabletext(
				"Contact Customer Care to add premium call management features to your T-Mobile for Business account.",
				benefitsRedemptionPage.message2);
		CrazyLegsBenefitsPage crazylegspage = new CrazyLegsBenefitsPage(getDriver());
		crazylegspage.verifyButton("Contact care");
		crazylegspage.clickOnContactCareButton();
		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-996")
	public void verifyBusinessUserTemplateforNameIDAdded(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepInstructionsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify SuzySheep Instructions Page ");
		Reporter.log("Test Data : Business user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify Contact care Authorable text");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/nameid");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("It's time to take control of your calls.",
				benefitsRedemptionPage.message4);

		benefitsRedemptionPage.verifyAuthorabletext(
				"Make sure you have NameId App to enjoy premium call management features.",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext("Turn on Caller ID, Scam Block, and more.",
				benefitsRedemptionPage.message5);
		benefitsRedemptionPage.verifyAndClickOnBtn("Turn on Caller ID, Scam Block, and more.");
		benefitsRedemptionPage.verifyBlockingPageURL();
		benefitsRedemptionPage.verifybtnAppStore();
		benefitsRedemptionPage.verifybtnPlayStore();
		benefitsRedemptionPage.verifyAndClickOnBtn("Back to plan details");
		benefitsRedemptionPage.verifyAuthorabletext(
				"Visit the manage data and add-ons page to manage your premium T-Mobile services.",
				benefitsRedemptionPage.message8);

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-784s2NotDone")
	public void verifySuzySheepInstructionsforEligibleDataPlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepInstructionsPage method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify SuzySheep Instructions Page ");
		Reporter.log("Test Data : Megenta plus user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify authorable text Suzy sheep");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/nameid");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("It's time to take control of your calls.",
				benefitsRedemptionPage.message4);

		benefitsRedemptionPage.verifyAuthorabletext(
				"Make sure you have NameId App to enjoy premium call management features.",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext("Turn on Caller ID, Scam Block, and more.",
				benefitsRedemptionPage.message5);
		benefitsRedemptionPage.verifyAndClickOnBtn("Turn on Caller ID, Scam Block, and more.");
		benefitsRedemptionPage.verifyBlockingPageURL();
		benefitsRedemptionPage.verifybtnAppStore();
		benefitsRedemptionPage.verifybtnPlayStore();
		benefitsRedemptionPage.verifyAndClickOnBtn("Back to plan details");
		benefitsRedemptionPage.verifyAuthorabletext(
				"Visit the manage data and add-ons page to manage your premium T-Mobile services.",
				benefitsRedemptionPage.message8);
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "828Release")
	public void verifySuzySheepViewsForStandardUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepViewsForStandardUser method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify SuzySheep  Page ");
		Reporter.log("Test Data : Standard user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify authorable text Suzy sheep");
		Reporter.log("5. Verify Plan details page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/nameid");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("Oops!", benefitsRedemptionPage.oopsText);
		benefitsRedemptionPage.verifyAuthorabletext("You don't have permissions to redeem this benefit",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext("To get this, contact your primary account holder",
				benefitsRedemptionPage.message3);
		benefitsRedemptionPage.verifyAndClickOnCancelBtn("Cancel");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "828Release")
	public void verifySuzySheepViewsForRestrictedUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepViewsForRestrictedUser method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify SuzySheep  Page ");
		Reporter.log("Test Data : Restricted user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify authorable text Suzy sheep");
		Reporter.log("5. Verify Plan details page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/nameid");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("Oops!", benefitsRedemptionPage.oopsText);
		benefitsRedemptionPage.verifyAuthorabletext("You don't have permissions to redeem this benefit",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext("To get this, contact your primary account holder",
				benefitsRedemptionPage.message3);
		benefitsRedemptionPage.verifyAndClickOnCancelBtn("Cancel");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "828Release")
	public void verifySuzySheepViewsForSuspendedUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifySuzySheepViewsForSuspendedUser method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : verify SuzySheep  Page ");
		Reporter.log("Test Data : suspended user ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Navigate to static page url");
		Reporter.log("4.  verify authorable text Suzy sheep");
		Reporter.log("5. Verify Plan details page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/nameid");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("Oops!", benefitsRedemptionPage.oopsText);
		benefitsRedemptionPage.verifyAuthorabletext("Your account is temporarily suspended",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext(
				"You will be unable to register for this benefit until your account has been restored.  For more information, please contact",
				benefitsRedemptionPage.message3);
		benefitsRedemptionPage.verifyCustomerCareBtnInAuthorabletext("Customer Care");
		benefitsRedemptionPage.verifyAndClickOnBtn("Restore account");
		benefitsRedemptionPage.verifyOneTimePayment();

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PLANBENEFITS })
	public void verifyBenefitRedemptionForStandardOrRestrictedUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBenefitRedemptionForStandardOrRestrictedUser method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Redemption page for Netflix ");
		Reporter.log("Test Data : Standard Or Restricted User ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Netflix should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/Netflix");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.checkPageIsReady();
		// benefitsRedemptionPage.verifyRedIcon();
		benefitsRedemptionPage.verifyAuthorabletext1("Oops!");
		benefitsRedemptionPage.verifyAuthorabletext2("You don't have permissions to redeem this benefit");
		benefitsRedemptionPage.verifyAuthorabletext3("To get this, contact your primary account holder");
		// benefitsRedemptionPage.verifyAndClickOnRestoreBtn("Restore account");
		benefitsRedemptionPage.verifyAndClickOnCancelBtn("Cancel");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PLANBENEFITS })
	public void verifyBenefitRedemptionForSimpleChoiceRatePlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBenefitRedemptionForSimpleChoiceRatePlan method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Redemption page for Netflix ");
		Reporter.log("Test Data : Simple Choice rate plan (rate plan ineligible for Netflix) ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Netflix should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/Netflix");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext1("Oops!");
		benefitsRedemptionPage.verifyAuthorabletext2("Your rate plan is not eligible for this benefit");
		benefitsRedemptionPage.verifyAuthorabletext3InGreyColor("To get this benefit, change your rate plan");
		benefitsRedemptionPage.verifyAuthorabletext3("To get this benefit, change your rate plan");
		benefitsRedemptionPage.verifyAndClickOnBtn3("Explore plans");
		benefitsRedemptionPage.verifyAndClickOnRestoreBtn("Restore account");
		benefitsRedemptionPage.verifyAndClickOnCancelBtn("Cancel");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PLANBENEFITS })
	public void verifyBenefitRedemptionForNetflixSOC(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBenefitRedemptionForNetflixSOC method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Netflix SOC ");
		Reporter.log("Test Data :  Netflix SOC  AND has already registered for Netflix");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Netflix should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/Netflix");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext4("Thanks for signing up for Netflix");
		benefitsRedemptionPage.verifyAuthorabletext7("You're all set to watch TV shows and movies anytime, anywhere.");
		benefitsRedemptionPage.verifyAuthorabletext8("Forgot your Netflix login info?");
		benefitsRedemptionPage.verifyRecoverYourAccount("Recover your account");
		benefitsRedemptionPage.verifyAndClickOnBtn5("Manage plan benefits");
		// benefitsRedemptionPage.verifyAuthorabletext9("Lorem ipsum dolor,");
		// benefitsRedemptionPage.verifyAuthorabletext10("Terms and condition");
		benefitsRedemptionPage.verifybtnAppStore();
		benefitsRedemptionPage.verifybtnPlayStore();

		benefitsRedemptionPage.verifyAndClickOnBtn("Back to plan details");

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyBenefitRedemptionForNetflixEligiblePlanButDoesnotHaveAnyNetflixSOCs(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForNetflixEligiblePlanButDoesnotHaveAnyNetflixSOCs method called in PlanBenefitsPageTest");
		Reporter.log(
				"Test case Name : Verify Benefit Redemption For  Netflix eligible plan, but doesn't have any Netflix SOCs  ");
		Reporter.log(
				"Test Data :  Netflix eligible rate plan  AND I don't have the Netflix SOC AND WHEN I click on the link to redeem Netflix ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Netflix should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/Netflix");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyManageAddonsURL1();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void VerifyBenefitRedemptionForNetflixEligiblePlanAndNetflixSOCNowAndTheyHaveAPendingRatePlanChangeToAnotherNetflixEligibleRatePlanButNoFutureDatedNetflixSOC(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForNetflixEligiblePlanAndNetflixSOCNowAndTheyHaveAPendingRatePlanChangeToAnotherNetflixEligibleRatePlanButNoFutureDatedNetflixSOC method called in PlanBenefitsPageTest");
		Reporter.log(
				"Test case Name : Verify Benefit Redemption For   Netflix eligible plan and Netflix SOC now, and they have a pending rate plan change to another Netflix eligible rate plan, but no future dated Netflix SOC  ");
		Reporter.log(
				"Test Data :  Netflix eligible rate plan AND I have a Netflix SOC AND I have a pending rate plan change to a Netflix eligible rate plan, but no pending (future dated) Netflix SOC addition ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Netflix should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/Netflix");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyManageAddonsURL1();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForNetFlixEligibleRatePlanUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBenefitRedemptionForNetFlixEligibleRatePlanUser method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Hulu Eligible Rate Plan User ");
		Reporter.log(
				"Test Data :  Netflix eligible rate plan  AND I have a Hulu SOC AND I haven't signed up for Hulu yet");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix Page |  Benefits Redemption page for Netflix should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  Click on Go To Netflix");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/Netflix");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("One last step...", benefitsRedemptionPage.message4);

		benefitsRedemptionPage.verifyAuthorabletext(
				"To finish activating, just log in to your Hulu account or create a new one.",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext("By clicking Activate Netflix, I agree to the ",
				benefitsRedemptionPage.message5);
		benefitsRedemptionPage.verifyAuthorabletext("Terms and condition ", benefitsRedemptionPage.message6);
		benefitsRedemptionPage.clickBtnNVerifyNewTab("Activate Netflix", "Netflix");

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForHuluEligibleRatePlanUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBenefitRedemptionForHuluEligibleRatePlanUser method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Hulu Eligible Rate Plan User ");
		Reporter.log(
				"Test Data :  Hulu eligible rate plan  AND I have a Hulu SOC AND I haven't signed up for Hulu yet");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Hulu  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  Click on Go to Hulu");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/HULU/");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext("One last step...", benefitsRedemptionPage.message4);
		benefitsRedemptionPage.verifyAuthorabletext(
				"To finish activating, just log in to your Hulu account or create a new one.",
				benefitsRedemptionPage.message2);
		benefitsRedemptionPage.verifyAuthorabletext("By clicking Activate Hulu, I agree to the ",
				benefitsRedemptionPage.message5);
		benefitsRedemptionPage.verifyAuthorabletext("Terms and condition ", benefitsRedemptionPage.message6);
		benefitsRedemptionPage.clickBtnNVerifyNewTab("Activate Hulu", "Hulu");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyBenefitRedemptionWithoutMisdinInURLForNetflixEligibleRatePlan(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionWithoutMisdinInURLForNetflixEligibleRatePlan method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Redemption page for Netflix ");
		Reporter.log("Test Data : Netflix eligible rate plan");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Netflix should be displayed without entering the misdin");
		Reporter.log("4.  verify authorable ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/Netflix");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext1("Oops!");

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void VerifyBenefitRedemptionForHuluEligibleRatePlanUserWithoutHuluSoc(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForHuluEligibleRatePlanUserWithoutHuluSoc method called in PlanBenefitsPageTest");
		Reporter.log(
				"Test case Name : Verify Benefit Redemption For Hulu Eligible Rate Plan User and Without Hulu SOC ");
		Reporter.log(
				"Test Data :  Hulu eligible rate plan  AND I donot have a Hulu SOC AND I haven't signed up for Hulu yet");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Hulu  Page |  Benefits Redemption page will redirect to Manage Add-ons");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/HULU/");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyManageAddonsURL2();
		// adding a Hulu SOC
		// click the submit button
		// verify order details
		benefitsRedemptionPage.verifyAuthorabletext4("One last step...");
		benefitsRedemptionPage
				.verifyAuthorabletext2("To finish activating, just log in to your Hulu account or create a new one.");
		benefitsRedemptionPage.verifyAuthorabletext5("Lorem ipsum dolor");
		benefitsRedemptionPage.verifyAuthorabletext6("terms and condition");
		benefitsRedemptionPage.verifyAndClickOnBtn6("Go to Hulu");
		benefitsRedemptionPage.verifyAndClickOnBtn("Back to account overview");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void VerifyErrorPageForBenefitRedemptionPageForNetflixAccount(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyErrorPageForBenefitRedemptionPageForNetflix method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Netlix Eligible Rate Plan User  ");
		Reporter.log("Test Data :  Netflix eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page will redirect to Manage Add-ons");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/Netflix");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyErrorUrl();
		benefitsRedemptionPage.verifyAuthorabletextForErrorPage("Looks like we got our wires crossed.",
				"We weren't able to process your request at this time. Please try again later.");
		benefitsRedemptionPage.verifyAndClickOnBtn7("Back");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void VerifyErrorPageForBenefitRedemptionPageForHulluAccount(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyErrorPageForBenefitRedemptionPageForHulluAccount method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Hulu Eligible Rate Plan User  ");
		Reporter.log("Test Data :  Hulu eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Hulu  Page |  Benefits Redemption page will redirect to Manage Add-ons");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/HULU/");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyErrorUrl();
		benefitsRedemptionPage.verifyAuthorabletextForErrorPage("Looks like we got our wires crossed.",
				"We weren't able to process your request at this time. Please try again later.");
		benefitsRedemptionPage.verifyAndClickOnBtn7("Back");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "release")
	public void VerifyErrorPageForBenefitRedemptionPageForNetflixAccountWithoutMisdin(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"VerifyErrorPageForBenefitRedemptionPageForNetflixAccountWithoutMisdin method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Netlix Eligible Rate Plan User  ");
		Reporter.log("Test Data :  Netflix eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page will redirect to Manage Add-ons");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/Netflix");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyErrorUrl();
		benefitsRedemptionPage.verifyAuthorabletextForErrorPage("Looks like we got our wires crossed.",
				"We weren't able to process your request at this time. Please try again later.");
		benefitsRedemptionPage.verifyAndClickOnBtn7("Back");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-97")
	public void VerifyBenefitRedemptionForStandardOrRestrictedUserForHulu(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBenefitRedemptionForStandardOrRestrictedUserForHulu method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Redemption page for Hulu ");
		Reporter.log("Test Data : Standard Or Restricted User with Hulu eligible");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Hulu  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/HULU/");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyAuthorabletext1("Oops!");
		benefitsRedemptionPage.verifyAuthorabletext2("You don't have permissions to redeem this benefit");
		benefitsRedemptionPage.verifyAuthorabletext3("To get this, contact your primary account holder");
		benefitsRedemptionPage.verifyAuthorabletext3InGreyColor("To get this, contact your primary account holder");
		benefitsRedemptionPage.verifyAndClickOnBtn("Cancel");

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-97")
	public void VerifyBenefitRedemptionForSuspendedUserForHulu(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBenefitRedemptionForSuspendedUserForHulu method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Redemption page for Hulu ");
		Reporter.log("Test Data : Suspended User with Hulu eligible");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Hulu  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/HULU/");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyAuthorabletext1("Oops!");
		benefitsRedemptionPage.verifyAuthorabletext2("Your account is temporarily suspended");
		benefitsRedemptionPage.verifyAuthorabletext3(
				"You will be unable to register for this benefit until your account has been restored.  For more information, please contact");
		benefitsRedemptionPage.verifyAuthorabletext3InGreyColor(
				"You will be unable to register for this benefit until your account has been restored.  For more information, please contact");
		benefitsRedemptionPage.verifyCustomerCareBtnInAuthorabletext("Customer Care");
		benefitsRedemptionPage.verifyAndClickOnBtn("Restore account");
		benefitsRedemptionPage.verifyAndClickOnBtn("Cancel");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "CDCWW-97")
	public void VerifyBenefitRedemptionForIneligibleUserForHulu(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBenefitRedemptionForIneligibleUserForHulu method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefits Redemption page for Hulu ");
		Reporter.log("Test Data : Ineligible User with Hulu eligible");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Hulu  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/HULU/");
		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyAuthorabletext1("Oops!");
		benefitsRedemptionPage.verifyAuthorabletext2("Your rate plan is not eligible for this benefit");
		benefitsRedemptionPage.verifyAuthorabletext3("To get this benefit, change your rate plan");
		benefitsRedemptionPage.verifyAuthorabletext3InGreyColor("To get this benefit, change your rate plan");
		benefitsRedemptionPage.verifyAndClickOnBtn3("Restore account");
		benefitsRedemptionPage.verifyAndClickOnBtnCancel("Cancel");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForHuluEligibleRatePlanUserAndSignedUp(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForHuluEligibleRatePlanUserAndSignedUp method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Hulu Eligible Rate Plan User ");
		Reporter.log("Test Data :  Hulu eligible rate plan  AND I have a Hulu SOC AND I  signed up for Hulu yet");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Hulu  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.clicke on Back to plan Details  ");
		Reporter.log("6. Navigate to Account Over view page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/HULU/");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext4("Thanks for signing up for Hulu");
		// benefitsRedemptionPage.verifyAuthorabletext7("Hulu is now added to
		// your account. Access your subscription at Hulu.com or via the Hulu
		// mobile app.");
		// benefitsRedemptionPage.verifyAuthorabletext8("Forgot your Hulu login
		// info?");
		// benefitsRedemptionPage.verifyRecoverYourAccount("Recover your
		// account");
		// benefitsRedemptionPage.verifyAndClickOnBtn5("Manage plan benefits");
		// benefitsRedemptionPage.verifyAuthorabletext9("Lorem ipsum dolor,");
		// benefitsRedemptionPage.verifyAuthorabletext10("Terms and condition");
		benefitsRedemptionPage.verifybtnAppStore();
		benefitsRedemptionPage.verifybtnPlayStore();
		benefitsRedemptionPage.verifyAndClickOnBtn("Back to plan details");
		benefitsRedemptionPage.verifyPlanDetailsPayment();

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForVedioChoiceEligibleRatePlanUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyBenefitRedemptionForVedioChoiceEligibleRatePlanUser method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Vedio choice Eligible Rate Plan User ");
		Reporter.log("Test Data :  Vedio choice eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Vedio choice  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("Choose your video perk");
		Reporter.log("Choose Hulu or Netflix.  Changed your mind?  You can switch once per bill cycle (28 days)");
		Reporter.log("Verify Authorable text Netflix");
		Reporter.log(
				"A streaming service that allows you to watch a wide variety of award-winning TV shows, movies, documentaries, and more.");
		Reporter.log("Verify Authorable text Hulu");
		Reporter.log(
				"Unlimited access to the Hulu streaming library with limited or no ads.  Enjoy full seasons of exclusive series, hit movies, and Hulu Originals.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext4("Choose your video perk");
		benefitsRedemptionPage.verifyAuthorabletext12(
				"Choose Hulu or Netflix. Changed your mind? You can switch once per bill cycle (28 days).");
		benefitsRedemptionPage.verifyCardAuthorabletext("Select Netflix");
		benefitsRedemptionPage.verifyCardStreamingtext(
				"A streaming service that allows you to watch a wide variety of award-winning TV shows, movies, documentaries, and more.");
		benefitsRedemptionPage.verifyCardAuthorabletext("Select Hulu");
		benefitsRedemptionPage.verifyCardStreamingtext(
				"A streaming service that allows you to watch a wide variety of award-winning TV shows, movies, documentaries, and more.");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForNetflixEligibleRatePlanUserAndSignedUp(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForNetflixEligibleRatePlanUserAndSignedUp method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Netflix Eligible Rate Plan User ");
		Reporter.log("Test Data :  Netflix eligible rate plan  AND I have a Hulu SOC AND I  signed up for Hulu yet");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Netflix  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.click on Back to plan Details  ");
		Reporter.log("6.  Verify Account Over view page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/Netflix");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyAuthorabletext4("Thanks for signing up for Netflix");
		// benefitsRedemptionPage.verifyAuthorabletext7("Netflix is now added to
		// your account. Access your subscription at Hulu.com or via the Hulu
		// mobile app.");
		// benefitsRedemptionPage.verifyAuthorabletext8("Forgot your Netflix
		// login info?");
		// benefitsRedemptionPage.verifyRecoverYourAccount("Recover your
		// account");
		// benefitsRedemptionPage.verifyAndClickOnBtn5("Manage plan benefits");
		// benefitsRedemptionPage.verifyAuthorabletext9("Lorem ipsum dolor,");
		// benefitsRedemptionPage.verifyAuthorabletext10("Terms and condition");
		benefitsRedemptionPage.verifybtnAppStore();
		benefitsRedemptionPage.verifybtnPlayStore();
		benefitsRedemptionPage.verifyAndClickOnBtn("Back to plan details");
		benefitsRedemptionPage.verifyPlanDetailsPayment();

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForVedioChoiceDoesNotHaveNetflixOrHuluSoc(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForVedioChoiceDoesNotHaveNetflixOrHuluSoc method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Vedio choice Eligible Rate Plan User ");
		Reporter.log("Test Data :  Vedio choice eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Vedio choice  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  verify authorable text  and click on  Select Netflix");
		Reporter.log("6.  verify authorable text  and Manage Add ons");
		Reporter.log("7.  verify authorable text  and click on  Select Hulu");
		Reporter.log("8.  verify authorable text  and Manage Add ons");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyCardAuthorabletext("Select Netflix");
		benefitsRedemptionPage.clickOnCardtextlink("Select Netflix");
		ManageAddOnsSelectionPage ManageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();
		getDriver().navigate().back();
		benefitsRedemptionPage.verifyCardAuthorabletext("Select Hulu");
		benefitsRedemptionPage.clickOnCardtextlink("Select Hulu");
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForVedioChoiceHaveNetflixSocNotSignedUp(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForVedioChoiceDoesNotHaveNetflixOrHuluSoc method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Vedio choice Eligible Rate Plan User ");
		Reporter.log("Test Data :  Vedio choice eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Vedio choice  Page |  Benefits Redemption page for Vedio choice should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  verify authorable text  and click on  Redeem Netflix");
		Reporter.log("6.  verify authorable text  and One last step...");
		Reporter.log("7.  verify authorable text  and click on  Switch to Hulu");
		Reporter.log("8.  verify authorable text  and Manage Add ons");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());
		benefitsRedemptionPage.verifyCardAuthorabletext("Redeem Netflix");
		benefitsRedemptionPage.clickOnCardtextlink("Redeem Netflix");
		benefitsRedemptionPage.verifyAuthorabletext4("One last step...");
		getDriver().navigate().back();
		benefitsRedemptionPage.verifyCardAuthorabletext("Switch to Hulu");
		benefitsRedemptionPage.clickOnCardtextlink("Switch to Hulu");
		ManageAddOnsSelectionPage ManageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForVedioChoiceHaveHuluSocNotSignedUp(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForVedioChoiceDoesNotHaveNetflixOrHuluSoc method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Vedio choice Eligible Rate Plan User ");
		Reporter.log("Test Data :  Vedio choice eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Vedio choice  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  verify authorable text  and click on  Redeem to Hulu");
		Reporter.log("6.  verify authorable text  and One last step...");
		Reporter.log("7.  verify authorable text  and click on  Switch to Netflix");
		Reporter.log("8.  verify authorable text  and Manage Add ons");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyCardAuthorabletext("Redeem to Hulu");
		benefitsRedemptionPage.clickOnCardtextlink("Redeem to Hulu");
		benefitsRedemptionPage.verifyAuthorabletext4("One last step...");
		getDriver().navigate().back();
		benefitsRedemptionPage.verifyCardAuthorabletext("Switch to Netflix");
		benefitsRedemptionPage.clickOnCardtextlink("Switch to Netflix");
		ManageAddOnsSelectionPage ManageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForVedioChoiceHaveNetflixSocAndSignedUp(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForVedioChoiceDoesNotHaveNetflixOrHuluSoc method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Vedio choice Eligible Rate Plan User ");
		Reporter.log("Test Data :  Vedio choice eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Vedio choice  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  verify authorable text  and click on  Manage Netflix");
		Reporter.log("6.  verify authorable text  and Thanks for signing up for Netflix");
		Reporter.log("7.  verify authorable text  and click on  Switch to Hulu");
		Reporter.log("8.  verify authorable text  and Manage Add ons");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyCardAuthorabletext("Manage Netflix");
		benefitsRedemptionPage.clickOnCardtextlink("Manage Netflix");
		benefitsRedemptionPage.verifyAuthorabletext4("Thanks for signing up for Netflix");
		getDriver().navigate().back();
		benefitsRedemptionPage.verifyCardAuthorabletext("Switch to Hulu");
		benefitsRedemptionPage.clickOnCardtextlink("Switch to Hulu");
		ManageAddOnsSelectionPage ManageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForVedioChoiceHaveHuluSocAndSignedUp(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForVedioChoiceDoesNotHaveNetflixOrHuluSoc method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Vedio choice Eligible Rate Plan User ");
		Reporter.log("Test Data :  Vedio choice eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Vedio choice  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  verify authorable text  and click on  Manage Hulu");
		Reporter.log("6.  verify authorable text  and Thanks for signing up for Hulu");
		Reporter.log("7.  verify authorable text  and click on  Switch to Netflix");
		Reporter.log("8.  verify authorable text  and Manage Add ons");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyCardAuthorabletext("Manage Hulu");
		benefitsRedemptionPage.clickOnCardtextlink("Manage Hulu");
		benefitsRedemptionPage.verifyAuthorabletext4("Thanks for signing up for Hulu");
		getDriver().navigate().back();
		benefitsRedemptionPage.verifyCardAuthorabletext("Switch to Netflix");
		benefitsRedemptionPage.clickOnCardtextlink("Switch to Netflix");
		ManageAddOnsSelectionPage ManageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForVedioChoiceHaveNetflixSocTodayAndPendingHuluNotSignedUpForHulu(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForVedioChoiceDoesNotHaveNetflixOrHuluSoc method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Vedio choice Eligible Rate Plan User ");
		Reporter.log("Test Data :  Vedio choice eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Vedio choice  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  verify authorable text  and click on  Redeem Hulu");
		Reporter.log("6.  verify authorable text  and One last step...");
		Reporter.log("7.  verify authorable text  and click on  Switch to Netflix");
		Reporter.log("8.  verify authorable text  and Manage Add ons");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyCardAuthorabletext("Redeem Hulu");
		benefitsRedemptionPage.clickOnCardtextlink("Redeem Hulu");
		benefitsRedemptionPage.verifyAuthorabletext4("One last step...");
		getDriver().navigate().back();
		benefitsRedemptionPage.verifyCardAuthorabletext("Switch to Netflix");
		benefitsRedemptionPage.clickOnCardtextlink("Switch to Netflix");
		ManageAddOnsSelectionPage ManageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForVedioChoiceHaveHuluSocTodayAndPendingNetflixNotSignedUpForNetflix(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForVedioChoiceDoesNotHaveNetflixOrHuluSoc method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Vedio choice Eligible Rate Plan User ");
		Reporter.log("Test Data :  Vedio choice eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Vedio choice  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  verify authorable text  and click on  Redeem Netflix");
		Reporter.log("6.  verify authorable text  and One last step...");
		Reporter.log("7.  verify authorable text  and click on  Switch to Hulu");
		Reporter.log("8.  verify authorable text  and Manage Add ons");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyCardAuthorabletext("Redeem Netflix");
		benefitsRedemptionPage.clickOnCardtextlink("Redeem Netflix");
		benefitsRedemptionPage.verifyAuthorabletext4("One last step...");
		getDriver().navigate().back();
		benefitsRedemptionPage.verifyCardAuthorabletext("Switch to Hulu");
		benefitsRedemptionPage.clickOnCardtextlink("Switch to Hulu");
		ManageAddOnsSelectionPage ManageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForVedioChoiceHaveNetflixSocTodayNotRedemedAndPendingHuluNotSignedUpForHulu(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForVedioChoiceDoesNotHaveNetflixOrHuluSoc method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Vedio choice Eligible Rate Plan User ");
		Reporter.log("Test Data :  Vedio choice eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Vedio choice  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  verify authorable text  and click on  Redeem Hulu");
		Reporter.log("6.  verify authorable text  and One last step...");
		Reporter.log("7.  verify authorable text  and click on  Switch to Netflix");
		Reporter.log("8.  verify authorable text  and Manage Add ons");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyCardAuthorabletext("Redeem Hulu");
		benefitsRedemptionPage.clickOnCardtextlink("Redeem Hulu");
		benefitsRedemptionPage.verifyAuthorabletext4("One last step...");
		getDriver().navigate().back();
		benefitsRedemptionPage.verifyCardAuthorabletext("Switch to Netflix");
		benefitsRedemptionPage.clickOnCardtextlink("Switch to Netflix");
		ManageAddOnsSelectionPage ManageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForVedioChoiceHaveHuluSocTodayNotReedemedPendingNetflixAndNotSignedUpForNetflix(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForVedioChoiceDoesNotHaveNetflixOrHuluSoc method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Vedio choice Eligible Rate Plan User ");
		Reporter.log("Test Data :  Vedio choice eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Vedio choice  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  verify authorable text  and click on  Redeem Netflix");
		Reporter.log("6.  verify authorable text  and One last step...");
		Reporter.log("7.  verify authorable text  and click on  Switch to Hulu");
		Reporter.log("8.  verify authorable text  and Manage Add ons");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyCardAuthorabletext("Redeem Netflix");
		benefitsRedemptionPage.clickOnCardtextlink("Redeem Netflix");
		benefitsRedemptionPage.verifyAuthorabletext4("One last step...");
		getDriver().navigate().back();
		benefitsRedemptionPage.verifyCardAuthorabletext("Switch to Hulu");
		benefitsRedemptionPage.clickOnCardtextlink("Switch to Hulu");
		ManageAddOnsSelectionPage ManageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();

	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForVedioChoiceHaveNetflixSocTodayNotRedemedAndPendingHuluAndReddemedHulu(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForVedioChoiceDoesNotHaveNetflixOrHuluSoc method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Vedio choice Eligible Rate Plan User ");
		Reporter.log("Test Data :  Vedio choice eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Vedio choice  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  verify authorable text  and click on  Manage Hulu");
		Reporter.log("6.  verify authorable text  and Thanks for signing up for Hulu");
		Reporter.log("7.  verify authorable text  and click on  Switch to NetFlix");
		Reporter.log("8.  verify authorable text  and Manage Add ons");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyCardAuthorabletext("Manage Hulu");
		benefitsRedemptionPage.clickOnCardtextlink("Manage Hulu");
		benefitsRedemptionPage.verifyAuthorabletext4("Thanks for signing up for Hulu");
		getDriver().navigate().back();
		benefitsRedemptionPage.verifyCardAuthorabletext("Switch to NetFlix");
		benefitsRedemptionPage.clickOnCardtextlink("Switch to NetFlix");
		ManageAddOnsSelectionPage ManageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "88N814R")
	public void VerifyBenefitRedemptionForVedioChoiceHaveHuluSocTodayNotRedemedAndPendingNetflixAndRedemedNetflix(
			ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"VerifyBenefitRedemptionForVedioChoiceDoesNotHaveNetflixOrHuluSoc method called in PlanBenefitsPageTest");
		Reporter.log("Test case Name : Verify Benefit Redemption For Vedio choice Eligible Rate Plan User ");
		Reporter.log("Test Data :  Vedio choice eligible rate plan  ");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log(
				"3. Get Benefits Redemption page for Vedio choice  Page |  Benefits Redemption page for Hulu should be displayed");
		Reporter.log("4.  verify authorable text  and button");
		Reporter.log("5.  verify authorable text  and click on  Manage Netflix");
		Reporter.log("6.  verify authorable text  and Thanks for signing up for Netflix!");
		Reporter.log("7.  verify authorable text  and click on  Switch to Hulu");
		Reporter.log("8.  verify authorable text  and Manage Add ons");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/benefit-redemption/videochoice");

		BenefitsRedemption benefitsRedemptionPage = new BenefitsRedemption(getDriver());

		benefitsRedemptionPage.verifyCardAuthorabletext("Manage Netflix");
		benefitsRedemptionPage.clickOnCardtextlink("Manage Netflix");
		benefitsRedemptionPage.verifyAuthorabletext4("Thanks for signing up for Netflix");
		getDriver().navigate().back();
		benefitsRedemptionPage.verifyCardAuthorabletext("Switch to Hulu");
		benefitsRedemptionPage.clickOnCardtextlink("Switch to Hulu");
		ManageAddOnsSelectionPage ManageAddOnsPage = new ManageAddOnsSelectionPage(getDriver());
		ManageAddOnsPage.verifymanageDataAndAddOnsPage();

	}

}
