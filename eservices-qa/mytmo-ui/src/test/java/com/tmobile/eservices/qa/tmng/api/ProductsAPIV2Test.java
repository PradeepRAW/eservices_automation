package com.tmobile.eservices.qa.tmng.api;

import java.util.HashMap;
import java.util.List;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eservices.qa.pages.tmng.api.ProductsAPIV2;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ProductsAPIV2Test extends ProductsAPIV2 {

	@Test(dataProvider = "byColumnName")
	public List<String> getProducts(String url) {
		try {
			String skuVal = "";
			Response productsresponse = retrieveProductsUsingPOST(url);
			JsonNode jsonNode = getParentNodeFromResponse(productsresponse);
			JsonNode productsNode = jsonNode.path("products");
			ObjectMapper mapper = new ObjectMapper();
			JsonPath jsonPath = new JsonPath(mapper.writeValueAsString(productsNode));
			List<String> productName = null, availability = null;
			String deviceDetails = "", deviceAvailability = "";
			HashMap<String, String> productDetails = new HashMap<>();

			if (!productsNode.isMissingNode() && productsNode.isArray()) {
				for (int i = 0; i < productsNode.size(); i++) {
					// JsonNode dataNode = productsNode.get(i);
					productName = jsonPath.getList("familyName");
					availability = jsonPath.getList("skus.availability.availabilityStatus");
					productDetails.put(productName.get(i), availability.get(i));
					/*
					 * if (dataNode.isObject()) { // List<String> familyName =
					 * dataNode.path("familyName").get(i); String availability =
					 * dataNode.path("skus.availability.availabilityStatus").asText(); String
					 * colorOptions = dataNode.path("skus.color").asText(); String memory =
					 * dataNode.path("skus.memory").asText(); String skuCode =
					 * dataNode.path("skus.Code").asText();
					 */
					/* + "|" + availability + "|" + colorOptions + "|" + memory + "|" + skuCode; */
					/*
					 * if("AVAILABLE".equalsIgnoreCase(dataNode.path(
					 * "skus.availability.availabilityStatus").asText())) { skuVal =
					 * dataNode.path("skus.sku").asText(); break; }
					 */
				}
				Reporter.log("Products:" + productDetails);
			}
			// }

		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}

}
