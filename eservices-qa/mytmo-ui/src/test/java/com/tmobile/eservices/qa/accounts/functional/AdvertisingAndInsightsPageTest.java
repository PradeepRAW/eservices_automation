package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.AdvertisingAndInsightsPage;
import com.tmobile.eservices.qa.pages.accounts.PrivacyAndNotificationsPage;

public class AdvertisingAndInsightsPageTest extends AccountsCommonLib {

	// Regression Start

	/**
	 *
	 * DE205629#PT#22390761 - Prod issue: Updates are not reflecting on Privacy -
	 * Advertising - Interest based Ads
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyAdvertisingInsights(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on advertising link| Click on advertising link should be success");
		Reporter.log("6. Veirfy insights header | Insights header should be displayed");
		Reporter.log("7. Click insights toggle | Insights toggle should be clicked");
		Reporter.log("8. Verify internet based ads header | Internet based ads header should be displayed");
		Reporter.log("9. Click internet based ads toggle | Internet based ads toggle should be clicked");
		Reporter.log("10.Verify web browsing header| Web browsing header should be displayed");
		Reporter.log("11.Click web browsing toggle | Web browsing toggle should be clicked");
		Reporter.log("12.Verify device location header | Device location header should be displayed");
		Reporter.log("13.Click web browsing toggle | Web browsing toggle should be clicked");
		Reporter.log("14.Verify device location header | Device location header should be displayed");
		Reporter.log("15.Click web browsing toggle | Web browsing toggle should be clicked");
		Reporter.log("16.Verify device location header | Device location header should be displayed");
		Reporter.log("17.Click device location toggle | Device location toggle should be clicked");
		Reporter.log("18.Click insights more button | Insights more button should be clicked");
		Reporter.log("19.Click internet based ads more button | Internet based ads more button should be clicked");
		Reporter.log("20.Click web browsing more button | Web browsing more button should be clicked");
		Reporter.log("21.Click device location more button | Device location more button should be clicked");
		Reporter.log("22.Click on Privacy And Notifications | Privacy And Notifications should be clicked");
		Reporter.log("23.Verify Privacy And Notifications | Privacy And Notifications should be displayed");
		Reporter.log("24.Click on Advertising Link | Insights Header should be displayed");
		Reporter.log("25.Verify Insights Toggle value | Insights Toggle is saved");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PrivacyAndNotificationsPage privacyAndNotificationsPage = navigateToPrivacyAndNotificationsPage(myTmoData,
				"Privacy and Notifications");
		privacyAndNotificationsPage.clickAdvertisingLink();
		privacyAndNotificationsPage.verifyHeaderByName("Insights");

		boolean insightsTogglevalue = privacyAndNotificationsPage.holdInsightsTogglevalue();
		privacyAndNotificationsPage.clickInsightsToggle();
//		privacyAndNotificationsPage.verifyInterestBasedAdsHeader();
//		privacyAndNotificationsPage.clickInterestBasedAdsToggle();
//		privacyAndNotificationsPage.verifyWebBrowsingHeader();
//		privacyAndNotificationsPage.clickWebBrowsingToggle();
//		privacyAndNotificationsPage.verifyDeviceLocationHeader();
//		privacyAndNotificationsPage.clickDeviceLocationToggle();
//		privacyAndNotificationsPage.clickInsightsMorebtn();
//		privacyAndNotificationsPage.clickInterestBasedAdsMorebtn();
//		privacyAndNotificationsPage.clickWebBrowsingMorebtn();
//		privacyAndNotificationsPage.clickDeviceLocationMorebtn();
//
//		privacyAndNotificationsPage.clickOnPrivacyAndNotificationsLink();
//		privacyAndNotificationsPage.verifyPrivacyAndNotificationsPage();
//		privacyAndNotificationsPage.clickAdvertisingLink();
//		privacyAndNotificationsPage.verifyInsightsTogglevalue(insightsTogglevalue);
	}

	/**
	 * US321857:Profile | Customers with no TMOID should see appropriate warning
	 * message while accessing profile page
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyUnregisteredLineWarningMessage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : Msisdn with unregistered line");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log(
				"5. Click on privacy and notifications link |Privacy and notifications page should be be displayed");
		Reporter.log("6. Change current line to unregistered line | Change to unregisterd line should be success");
		Reporter.log("7. Click on marketing communication link | Marketing communication page  should be displayed");
		Reporter.log("8. Verify warning message | Warning message should be displayed");
		Reporter.log("9. Navigate to previous page | Navigation to previous page should be success");
		Reporter.log("10.Click on advertising link | Advertising link should be clicked");
		Reporter.log("11.Verify warning message | Warning message should be displayed");

		PrivacyAndNotificationsPage privacyAndNotificationsPage = navigateToPrivacyAndNotificationsPage(myTmoData,
				"Privacy and Notifications");
		privacyAndNotificationsPage.changeToUnregisteredLine();
		privacyAndNotificationsPage.clickMarketingCommunicationsLink();
		privacyAndNotificationsPage.verifyWarningMessage();
		privacyAndNotificationsPage.navigateToPreviousPage();
		privacyAndNotificationsPage.clickAdvertisingLink();
		privacyAndNotificationsPage.verifyWarningMessage();
	}

	/**
	 * CDCWW-1455: Verify Network Data Consent Toggle Status update - Advertising -
	 * Network Data Consent ads
	 *
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyNetworkDataConsentgToggleStatusUpdates(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on advertising link| Click on advertising link should be success");
		Reporter.log("10.Verify Network Data consent blade| Network Data Consent should be displayed");
		Reporter.log(
				"11.Veirfy Network Data Consent toggle default state | Network Data Consent based default state should be OFF");
		Reporter.log(
				"12.Verify Network Data Consent toggle toggle operations | Network Data Consent toggle status update should be success");
		Reporter.log(
				"13. Click on show more link for Network Data Consent | Should be able to click on Show moer  link");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AdvertisingAndInsightsPage advertisingAndInsightsPage = navigateToAdvertisingPage(myTmoData,
				"Advertising & Insights");
		advertisingAndInsightsPage.verifyNetworkDataConsentHeader();
		advertisingAndInsightsPage.clickAdvertisingLink();
		advertisingAndInsightsPage.checkToggleStatusForNetworkDataConsent();
		advertisingAndInsightsPage.toggleAdvertisingNetworkUsageData(true);

	}

	/**
	 * CDCWW-1455: Verify Network Data Consent Toggle Status update - Advertising -
	 * Network Data Consent ads
	 *
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyNetworkLocationConsentToggleStatusUpdates(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on advertising link| Click on advertising link should be success");
		Reporter.log("10.Verify Network Data consent blade| Network Data Consent should be displayed");
		Reporter.log(
				"11.Veirfy Network Data Consent toggle default state | Network Data Consent based default state should be OFF");
		Reporter.log(
				"12.Verify Network Data Consent toggle toggle operations | Network Data Consent toggle status update should be success");
		Reporter.log(
				"13. Click on show more link for Network Data Consent | Should be able to click on Show moer  link");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AdvertisingAndInsightsPage advertisingAndInsightsPage = navigateToAdvertisingPage(myTmoData,
				"Advertising & Insights");
		advertisingAndInsightsPage.verifyNetworkDataConsentHeader();
		advertisingAndInsightsPage.clickAdvertisingLink();
		advertisingAndInsightsPage.checkToggleStatusForLocationDataConsent();
		advertisingAndInsightsPage.toggleAdvertisingLocationConsent(true);
	}

	// Regression End

	/**
	 * US355500:MyTMO - Profile - Handle IBA Error scenarios gracefully
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyAdvertisingInsightsToggle(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log(
				"5. Click on privacy and notifications link |Privacy and notifications page should be be displayed");
		Reporter.log("6. Click on advertising link | Advertising link should be clicked");
		Reporter.log("7. Verify insights header | Insights header should be displayed");
		Reporter.log("8. Verify insights toggle | Insights toggle should be in enable/disable state");
		Reporter.log("9. Verify internet based ads header | Internet based ads header should be displayed");
		Reporter.log(
				"10.Verify internet based ads toggle | Internet based ads toggle should be in enable/disable state");
		Reporter.log("11.Verify web browsing header| Web browsing header should be displayed");
		Reporter.log("12.Verify web browsing toggle | Web browsing toggle should be in enable/disable state");
		Reporter.log("13.Verify device location header | Device location header should be displayed");
		Reporter.log("14.Verify device location toggle | Device location toggle should be in enable/disable state");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PrivacyAndNotificationsPage privacyAndNotificationsPage = navigateToPrivacyAndNotificationsPage(myTmoData,
				"Privacy and Notifications");
		privacyAndNotificationsPage.clickAdvertisingLink();
		privacyAndNotificationsPage.verifyHeaderByName("Insights");
		privacyAndNotificationsPage.checkToggleStatusForInsights();
		privacyAndNotificationsPage.verifyInterestBasedAdsHeader();
		privacyAndNotificationsPage.checkToggleStatusForInternetBasedAds();
		privacyAndNotificationsPage.verifyWebBrowsingHeader();
		privacyAndNotificationsPage.checkToggleStatusForWebBrowsing();
		privacyAndNotificationsPage.verifyDeviceLocationHeader();
		privacyAndNotificationsPage.checkToggleStatusForDeviceLocation();
	}

	/**
	 * DE159763:IBA and Insights are set to off as default when they should default
	 * to On
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyIBAAndInsightsDefaultToggleState(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on advertising link| Click on advertising link should be success");
		Reporter.log("6. Veirfy insights header | Insights header should be displayed");
		Reporter.log("7. Veirfy insights toggle default state | Insights toggle default state should be on");
		Reporter.log("8. Verify insights toggle operations | Insights toggle status update should be success");
		Reporter.log("9. Click insights toggle | Insights toggle should be clicked");
		Reporter.log("10.Verify internet based ads header | Internet based ads header should be displayed");
		Reporter.log(
				"11.Veirfy internet based ads toggle default state | Internet based ads toggle default state should be on");
		Reporter.log(
				"12.Verify internet based ads toggle operations | Internet based ads toggle status update should be success");
		Reporter.log("13.Click internet based ads toggle | Internet based ads toggle should be clicked");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PrivacyAndNotificationsPage privacyAndNotificationsPage = navigateToPrivacyAndNotificationsPage(myTmoData,
				"Privacy and Notifications");
		privacyAndNotificationsPage.clickAdvertisingLink();
		privacyAndNotificationsPage.verifyHeaderByName("Insights");
		privacyAndNotificationsPage.verifyinsightsToggleDefaultState();
		privacyAndNotificationsPage.verifyinsightsToggleOperations();
		privacyAndNotificationsPage.clickInsightsToggle();
		privacyAndNotificationsPage.verifyInterestBasedAdsHeader();
		privacyAndNotificationsPage.verifyInternetBasedAdsToggleDefaultState();
		privacyAndNotificationsPage.verifyInternetBasedAdsToggleOperations();
		privacyAndNotificationsPage.clickInterestBasedAdsToggle();
	}

	/**
	 * DE205629:Prod issue: Updates are not reflecting on Privacy - Advertising -
	 * Interest based Ads
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyAdvertisingToggleStatusUpdates(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on advertising link| Click on advertising link should be success");
		Reporter.log("6. Veirfy insights header | Insights header should be displayed");
		Reporter.log("7. Veirfy insights toggle default state | Insights toggle default state should be on");
		Reporter.log("8. Verify insights toggle operations | Insights toggle status update should be success");
		Reporter.log("9. Click insights toggle | Insights toggle should be clicked");
		Reporter.log("10.Verify internet based ads header | Internet based ads header should be displayed");
		Reporter.log(
				"11.Veirfy internet based ads toggle default state | Internet based ads toggle default state should be on");
		Reporter.log(
				"12.Verify internet based ads toggle operations | Internet based ads toggle status update should be success");
		Reporter.log("13.Click internet based ads toggle | Internet based ads toggle should be clicked");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PrivacyAndNotificationsPage privacyAndNotificationsPage = navigateToPrivacyAndNotificationsPage(myTmoData,
				"Privacy and Notifications");
		privacyAndNotificationsPage.clickAdvertisingLink();
		privacyAndNotificationsPage.verifyHeaderByName("Insights");
		privacyAndNotificationsPage.verifyinsightsToggleDefaultState();
		privacyAndNotificationsPage.verifyinsightsToggleOperations();
		privacyAndNotificationsPage.clickInsightsToggle();
		privacyAndNotificationsPage.verifyInterestBasedAdsHeader();
		privacyAndNotificationsPage.verifyInternetBasedAdsToggleDefaultState();
		privacyAndNotificationsPage.verifyInternetBasedAdsToggleOperations();
		privacyAndNotificationsPage.clickInterestBasedAdsToggle();
	}

	/**
	 * Deeplink: Advertising and Insights Breadcrumb Interest based Ads
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyBreadCrumbDeeplinkAdvertisingAndInsights(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Check with the deeplink | deeplink to the advertising and Insights page should be on");
		Reporter.log(
				"9. Verify the bread crumb advertising and insights | breadcrumb should be in magenta and should not be clickable");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToFutureURLFromHome(myTmoData, "account/profile/privacy_notifications/advertising/4146290000");
		AdvertisingAndInsightsPage advertisingAndInsightsPage = navigateToAdvertisingPage(myTmoData,
				"Advertising & Insights");
		advertisingAndInsightsPage.checkcolorForBreadCrumbAdvertisingandInsights();
		advertisingAndInsightsPage.checkAdvertisingInsightsNotClickableForDeeplink();

	}


	/**
	 * Deeplink: Advertising and Insights Breadcrumb Interest based Ads
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyDeeplinkAdvertisingAndInsights(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Check with the deeplink | deeplink to the advertising and Insights page should be on");
		Reporter.log(
				"9. Verify the bread crumb advertising and insights | breadcrumb should be in magenta and should not be clickable");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToFutureURLFromHome(myTmoData, "account/profile/privacy_notifications/advertising/4146290000");
		AdvertisingAndInsightsPage advertisingAndInsightsPage = navigateToAdvertisingPage(myTmoData,
				"Advertising & Insights");
		advertisingAndInsightsPage.clickInsightsToggle();
		advertisingAndInsightsPage.checkAdvertisingInsightsNotClickableForDeeplink();
		advertisingAndInsightsPage.verifyNetworkDataConsentHeader();
		advertisingAndInsightsPage.verifyLocationDataConsentHeader();
		advertisingAndInsightsPage.selectMsisdindropdown();
		advertisingAndInsightsPage.VerifycheckForOnlyInsightsHeader();

	}

	/**
	 * Deeplink: Advertising and Insights Breadcrumb Interest based Ads
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyErrormodalAdvertisingAndInsights(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Check with the deeplink | deeplink to the advertising and Insights page should be on");
		Reporter.log(
				"9. Verify the bread crumb advertising and insights | breadcrumb should be in magenta and should not be clickable");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToFutureURLFromHome(myTmoData, "account/profile/privacy_notifications/advertising/4146290000");
		AdvertisingAndInsightsPage advertisingAndInsightsPage = navigateToAdvertisingPage(myTmoData,
				"Advertising & Insights");
		advertisingAndInsightsPage.checkcolorForBreadCrumbAdvertisingandInsights();
		advertisingAndInsightsPage.verifyHeaderByName("Insights");
		advertisingAndInsightsPage.checkToggleStatusForInsights();
		advertisingAndInsightsPage.clickInsightsToggle();
		advertisingAndInsightsPage.checkForErrorModel();

	}
}
