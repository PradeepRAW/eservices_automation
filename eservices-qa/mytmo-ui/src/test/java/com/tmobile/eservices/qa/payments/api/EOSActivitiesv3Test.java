package com.tmobile.eservices.qa.payments.api;

import java.time.LocalDate;
import java.util.List;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.EOSCommonLib;
import com.tmobile.eservices.qa.api.EOSCommonMethods;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.EOSActivitiesV3;

import io.restassured.response.Response;

public class EOSActivitiesv3Test extends EOSCommonMethods {

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testactivitiesaccountsummary(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		EOSCommonLib ecl = new EOSCommonLib();
		EOSActivitiesV3 activity = new EOSActivitiesV3();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			LocalDate a = LocalDate.now();
			String fromdate = "2016-03-10";
			String todate = a.toString();

			Response Accountsummaryresponse = activity.getResponseAccountsummary(getjwt, fromdate, todate);
			checkexpectedvalues(Accountsummaryresponse, "statusCode", "100");
			checkexpectedvalues(Accountsummaryresponse, "statusMessage", "Account Summary successfully retrieved");

			String alltags[] = { "accountActivity", "totalRows" };
			for (String tag : alltags) {
				checkjsontagitems(Accountsummaryresponse, tag);
			}

		}

	}

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testactivitiesgetdoucumentid(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSActivitiesV3 activity = new EOSActivitiesV3();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			LocalDate a = LocalDate.now();
			String fromdate = "2016-03-10";
			String todate = a.toString();
			Response Accountsummaryresponse = activity.getResponseAccountsummary(getjwt, fromdate, todate);
			List<String> docids = activity.getalldocids(Accountsummaryresponse);
			if (!docids.isEmpty()) {
				for (String docid : docids) {
					Response pdfresponse = activity.getResponsegetpdfsummary(getjwt, docid);
					String statuscode = activity.checkstatuscodepdfsummary(pdfresponse);
					if (statuscode != null) {
						if (statuscode.equalsIgnoreCase("100"))
							Reporter.log("pdf documet copy passed");
						else
							Assert.fail("pdf documet copy faliled for" + docid);
					} else {
						Assert.fail("call failed with status" + pdfresponse.statusCode());
					}
				}

			}

		}

	}

	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "NewAPI" })
	public void testactivitiesgetEipHistory(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		EOSActivitiesV3 activity = new EOSActivitiesV3();

		String[] getjwt = getjwtfromfiltersforAPI(apiTestData);

		if (getjwt != null) {
			LocalDate a = LocalDate.now();
			String fromdate = "2016-03-10";
			String todate = a.toString();
			Response Accountsummaryresponse = activity.getResponseAccountsummary(getjwt, fromdate, todate);
			List<String> docids = activity.getalleippaymentids(Accountsummaryresponse);
			List<String> refunids = activity.getalleippaymentrefundids(Accountsummaryresponse);

			if (!docids.isEmpty()) {
				for (String docid : docids) {
					if (!refunids.contains(docid)) {
						Response eipresponse = activity.getResponsegeteipHistory(getjwt, docid);
						String statuscode = activity.checkstatuscodeEiphistory(eipresponse);
						if (statuscode != null) {
							if (statuscode.equalsIgnoreCase("100"))
								Reporter.log("eippayment pdf passed for: " + docid);
							else
								Assert.fail("eippayment pdf for: " + docid);
						} else {
							Assert.fail("call failed with status: " + eipresponse.statusCode());
						}

					}
				}

			}

		}

	}

}