package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.AccessoryPDPPage;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class AccessoriesPDPTest extends ShopCommonLib {
	
	/**
	 * US320262: MyTMO - Additional Terms - Upgrade with Accessories - Accessory PDP Pricing Display
	 * US351455: [Unfinished] MyTMO - Additional Terms - Upgrade with Accessories - Accessory PDP Pricing Display
	 * US492483 : V3 Gaps - plp/pdp Device, plp/pdp accessories, shop.
	 * US216516 :Accessories Product Details Page - CTA navigation Add to Order
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS})
	public void testAdditionalTermsUpgradeWithAccessoriesAccessoryPDPPricingDisplay(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US320262, US351455: MyTMO - Additional Terms - Upgrade with Accessories - Accessory PDP Pricing Display");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selection page should be displayed");
		Reporter.log("8. Select line |  Phone selection page should be displayed");
		Reporter.log("9. Click skip trade in button |  Insurance migration page should be displayed");
		Reporter.log("10. Click continue button |  Accessory PLP page should be displayed");
		Reporter.log("11. Validate Device Name and Manufacture name on device| Device Name and Manufacture name should be displayed");
		Reporter.log("13. Click  on Accessory device | Accessory pdp page should be displayed");
		Reporter.log("14. Verify EIP Down Payment| EIP Down Payment should be displayed");
		Reporter.log("15. Verify Terms and Monthly Payment |Terms and Monthly Payment should be displayed");
		Reporter.log("16. Verify check box status for the selected accessory in PLP page| Check box should be selected ");
		Reporter.log("17. Click add to cart button | Cart page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		LineSelectorDetailsPage  lineSelectorDetailsPage = navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		lineSelectorDetailsPage.clickOnSkipTradeInLink();		
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickContinueButton();		
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.verifyDeviceNameAndManufacturerName();
		accessoryPLPPage.clickAccesoriesDeviceByName(myTmoData.getAccessoryName());				
		AccessoryPDPPage accessoryPDPPage = new AccessoryPDPPage(getDriver());
		//accessoryPDPPage.verifyDeviceNameAndManufacturerName(myTmoData.getAccessoryName());
		accessoryPDPPage.verifyAccessoryProductDetailsPage();
		accessoryPDPPage.verifyEIPInstallmentsMonths();
		accessoryPDPPage.verifyEIPMonthlyPrice();
		accessoryPDPPage.verifyEIPDownPaymentPrice();
		accessoryPDPPage.verifyAddToOrderButton();
		accessoryPDPPage.clickAddToOrderButton();
		accessoryPLPPage.verifySelectedAccessoryCheckbox();
		accessoryPLPPage.clickPriceLockupBarContinueButton();
		CartPage cartPage = new CartPage(getDriver());		
		cartPage.verifyCartPage();
	}
	
}
