/**
 * 
 */
package com.tmobile.eservices.qa.payments;

/**
 * @author charshavardhana
 *
 */
public class PaymentConstants {
	public static final String Add_card_Header="Card information";
	public static final String Edit_card_Header="Edit Card Information";

	public static final String Add_bank_Header="Bank information";
	public static final String Edit_bank_Header="Edit Bank Information";
	
	public static final String Add_card_Save_Payment_Error_Msg="You can only have 1 saved Credit/Debit card on file. Proceed if you want to replace your existing stored card with a new card.";
	public static final String Add_card_Tool_Tip_Text="Save account (optional). Allow T-Mobile to store your payment method for future use by you and verified users to make a payment.";
	public static final String Bank_Delete_Model_Header="Delete this bank account?";
	public static final String Card_Delete_Model_Header="Delete this card?";
	public static final String Delete_Model_Sub_Header="Are you sure you want to delete:";
	public static final String Tool_Tip_Msg="Save account (optional). Allow T-Mobile to store your payment method for future use by you and verified users to make a payment.";
	public static final String EXPECTED_TEST_STEPS = "Expected Test Steps:";
	public static final String ACTUAL_TEST_STEPS = "Actual Test Steps:";
	public static final String SAVE_PAYMENT_ERROR_MSG = "You can only have 1 saved checking account on file. Proceed if you want to replace your existing stored checking account with a new checking account";
	public static final String HOME_PAGE_ERROR = "Home page is not displayed";
	public static final String VERIFY_BILLINGGROUP_BOX = "Billing highlighs group box displayed";
	public static final String ERROR_VIEWBILL = "Missing View Bill";
	public static final String CURRENT_CHARGES_HEADER_VERIFY = "Currrent charges header on bill summary page is not displayed.";
	public static final String BILL_DETAILS_PAGE_ERROR = "Bill Details page is not displayed.";
	public static final String PREVIOUS_MONTH_COMPARISION_ERROR= "Comparision of current bill with previous bill is not loaded.";
	public static final String EIP_ESTIMATOR_ERROR="EIP Estimator dialog box is not displayed";
	public static final String EXTRA_DEVCIE_PAYMENT_ERROR="Extra Device Payment page is not displayed";
	public static final String REVIEW_DEVICE_PAYMENT_ERROR="Review Device payment page is not displayed";
	public static final String COMPLETED_PLAN_NOTDISPLAYED="Completed Plan is not displayed";
	public static final String EXTRA_DEVCIE_PAYMENT_NOTDISPLAYED="Extra Device Payment page is not displayed";
	public static final String VERIFY_REMOVEPAYMENT_DIALOGBOX_NOTDISPLAYED="Remove payment dialog box is not displayed";
	public static final String CREDITS_AND_ADJUSTMENTS_OVERLAY = "Credits and adjustments overlay is not displaying";
	public static final String BILLING_PAGE_ERROR= "Billing Summary page is not displayed.";
	public static final String BILLING_LINK = "Billing";
	public static final String BILL_DETAILS_VERBIAGE_TEXT = "To see details for additional lines on the account, please check with the Primary Account Holder.";
	public static final String BILL_DETAILS_VERBIAGE_VERIFY = "Verbiage Text is not displayed for Standard User";
	public static final String TAXES_FEES_TEXT = "Taxes and fees";
	public static final String VERIFY_TAXES_FEES_HIDDEN = "taxes and fees are not hidden in Current chages table";
	public static final String VERIFY_EIP_PLANID = "EIP PlanID Shows empty";
	public static final String VERIFY_PLAN_BALANCE_AMOUNT = "PlanBalance Amount Shows empty";
	public static final String VERIFY_INSTALLMENTPLAN_TYPE_EIP = "Installment Plan Type is not EIP";
	public static final String VERIFY_PLANSTATUS_ACTIVE = "Plan Status Shows not Active";
	public static final String CHECK_PAPERLESS_LINK = "paper less billing section not displayed";
	public static final String CURRENT_CHARGES_DISPLAYED ="Current Charges are Displayed which is not as expected.";
	public static final String RECURRING_CHARGES_DISPLAYED ="Recurring Charges are not displayed.";
	public static final String MISC_CHARGES_DISPLAYED ="Misc Charges are not displayed.";
	public static final String PLAN_PAGE_ERROR = "Plan page is not displayed";
	public static final String VERIFY_CHANGEPLANPAGE_NOTDISPLAYED="Change plan page is not displayed";
	public static final String VERIFY_REVIEWCHANGEPAGE_NOTDISPLAYED="Review Change page is not displayed";
	public static final String VERIFY_CURRENTMONTHLY_NOTDISPLAYED="Current Monthly tab is not displayed";
	public static final String VERIFY_NEWMONTHLY_NOTDISPLAYED="New Monthly tab is not displayed";
	public static final String MOBILE_INTERNET_2GB="Mobile Internet 2GB";
	public static final String REMOVED_MOBILE_INTERNET_6GB="Mobile Internet 6GB";
	public static final String VERIFY_BILL_SUMMARYPAGE_PAST_BILLS="Bill Summary Page Past Bills Should be displayed Grater than 12 months";
	public static final String NO_EIP_DETAILS_PAGE = "User Is Not Navigated EIP Details Page";
	public static final String NO_EIP_DEVICE_PAGE = "User Is Not Navigated To EIP Device Payment Page.";
	public static final String CARD = "Card";
	public static final String NO_REVIEW_PAYMENT_PAGE = "User Is Not Navigated To Review Payment Page";
	public static final String EQUIPMENT_INSTALLMENT_PLAN = "Installment Plan";
	public static final String NO_DOCUMENTS_TAB_DISPLAYED = "Documents  tab is not displayed";
	public static final String ACC_HISTORY_HEADER = "ACCOUNT HISTORY";
	public static final String BILLING_PAGE_HEADER_PAGE_ERROR_MSG = "Bill summary Page is not displayed";
	public static final String VERIFY_CURRENTMONTHLY_DISPLAYED="Current Monthly tab is displayed";
	public static final String VERIFY_NEWMONTHLY_DISPLAYED="New Monthly tab is displayed";
	public static final String VERIFY_MOBILE_INTERNET_2GB_ISMATCHED="Mobile Internet 2GB is Matched";
	public static final String VERIFY_REMOVED_MOBILE_INTERNET_6GB_ISMATCHED="Mobile Internet 6GB";
	public static final String NO_ONE_TIME_PAYMENT_PAGE = "User Is Not Navigated To One Time Payment Page";
	public static final String PAYMENT_ARRANGEMENT_NOTDISPLAYED="Payment Arrangement is not displayed";
	public static final String PAYMENT_ARRANGEMENT_DISPLAYED="Payment Arrangement is displayed";
	public static final String VERIFY_CURRENT_ACTIVITY_NOTDISPLAYED="Current Activity page is not displayed";
	public static final String VERIFY_REMAINING_PAYMENTS_DISPLAYED="Remaining Payments page is displayed";
	public static final String VERIFY_SCHEDULING_PAYMENTS_NOTDISPLAYED="Scheduling Payment Details is not displayed";
	public static final String VERIFY_SCHEDULING_PAYMENTS_DISPLAYED="Scheduling Payment Details is displayed";
	public static final String VERIFY_VISIBLE_DATES_DISPLAYED="Visible dates is displayed";
	public static final String VERIFY_REMOVEPAYMENT_DIALOGBOX_DISPLAYED="Remove payment dialog box is displayed";
	public static final String INSTALLEMENT_TWO_AMOUNT_NOT_EQUAL_BALANCE = "Installment  amount not equals to balance";
	public static final String VERIFY_CURRENT_ACTIVITY_DISPLAYED="Current Activity page is displayed";
	public static final String VERIFY_INVALID_CARD_ERROR_MSG = "Verify invalid credit card error message";
	public static final String AUTOPAY_LINK_NOT_DISPLAYED="Auto Pay link is not displayed";
	public static final String AUTOPAY_ALERTS_DISPLAYED="Autopay Payemnt information alerts Icon is displayed";
	public static final String AUTOPAY_ALERTS_NOTDISPLAYED="Autopay Payemnt information alerts is not displayed";
	public static final String AUTOPAY_CARD_ICON_NOTDISPLAYED="Autopay Card Icon is not displayed";
	public static final String AUTOPAY_CARD_ICON_DISPLAYED="Autopay Card Icon is displayed";
	public static final String PROFILE_PAGE_ERROR_MSG = "Profile page is not displaying";
	public static final String HOME_PAGE_RIGHT_HEADERS_PROFILE = "PROFILE";
	public static final String AUTOPAY_PAYEMENT_SCHEDULEDDATE_NOTDISPLAYED="Autopay Payment Scheduled Date message is not displayed";
	public static final String AUTOPAY_PAYEMENT_SCHEDULEDDATE_DISPLAYED="Autopay Payment Scheduled Date message is displayed";
	public static final String VERIFY_SUCCESS_MESSAGE_NOTDISPLAYED="Success message is not displayed";
	public static final String VERIFY_SUCCESS_MESSAGE_DISPLAYED="Success message is displayed";
	public static final String VERIFY_AUTOSTATE_ON =  "Auto Pay not turned into 'ON'";
	public static final String NO_MANAGE_AUTOPAY_PAGE = "manage autopay page not loaded";
	public static final String VERIFY_AUTOSTATE_OFF = "Auto Pay not turned into 'OFF'";
	public static final String BILL_DUEDATE_NOTDISPLAYED="Bill due date is not displayed";
	public static final String BILL_DUEDATE_DISPLAYED="Bill due Date is displayed";
	public static final String REMAININGDAYS_DUEDATE_NOTDISPLAYED="Remaining days of due date is not displayed";
	public static final String REMAININGDAYS_DUEDATE_DISPLAYED="Remaining days of due date is displayed";
	public static final String VOICE_INFORMATION_DISPLAYED = "Voice  details information is displayed";
	public static final String MESSAGING_INFORMATION_DISPLAYED = "Messaging details information is displayed";
	public static final String DATA_INFORMATION_DISPLAYED = "Data details information is displayed";
	public static final String FILTERED_VOICE_INFORMATION_DISPLAYED = "Filtered voice information is displayed";
	public static final String FILTERED_MESSAGING_INFORMATION_DISPLAYED = "Filtered messaging information should be displayed";
	public static final String FILTERED_DATA_INFORMATION_DISPLAYED = "Filtered data information is displayed";
	public static final String FILTERED_VOICE_INFORMATION_NOT_DISPLAYED = "Filtered voice information not displayed";
	public static final String FILTERED_MESSAGING_INFORMATION_NOT_DISPLAYED = "Filtered messaging information should not displayed";
	public static final String FILTERED_DATA_INFORMATION_NOT_DISPLAYED = "Filtered data information not displayed";
	public static final String NO_USAGE_OVERVIEW_PAGE = "Usage Overview Page is not displayed";
	public static final String CALL_DETAIL_RECORDS_PAGE_DISPLAYED = "Call details records page displayed";
	public static final String NO_VOICE_USAGE = "voice usage details not displayed";
	public static final String NO_DOWNLOAD_USAGE_RECORDS_LINK = "download usage records link not displayed";
	public static final String DOWNLOAD_USAGE_RECORDS_LINK_DISPLAYED = "download usage records link displayed";
	public static final String USAGE_RECORDS_DOWNLOADED = "usage records downloaded";
	public static final String VERIFY_MESSAGING_TAB = "messaging tab details displayed";
	public static final String NO_VERIFY_MSG_USAGE = "message usage details not displayed";
	public static final String VERIFY_DATA_TAB = "data tab displayed";
	public static final String DATA_USAGE_DISPLAYED = "data pass usage deatils displayed";
	public static final String NO_DATA_USAGE = "data pass usage deatils not displayed";
	public static final String NO_VERIFY_DATA_STASH = "Verify Data Stash link is not  displayed";
	public static final String VERIFY_DATA_STASH = "Verify Data Stash link is displayed";
	public static final String NO_CALL_DEATIL_RECORDS_PAGEHK = "User Is Not Navigated To Call Detail Records Pagehk.";
	public static final String USAGEPAGE_TABS_MOBILE_VOICE = "Voice: All";
	public static final String USAGEPAGE_TABS_MOBILE_MESSAGING = "Messaging: All";
	public static final String USAGEPAGE_TABS_MOBILE_DATA = "Data: All";
	public static final String USAGEPAGE_TABS_MOBILE_TMOBILE = "T-Mobile purchases: All";
	public static final String USAGEPAGE_TABS_MOBILE_THIRD_PARTY = "3rd party purchases All";
	public static final String USAGEPAGE_TABS_VOICE = "Voice";
	public static final String USAGEPAGE_TABS_MESSAGING = "Messaging";
	public static final String USAGEPAGE_TABS_DATA = "Data";
	public static final String USAGEPAGE_TABS_TMOBILE = "T-Mobile purchases";
	public static final String USAGEPAGE_TABS_THIRD_PARTY = "Third-party purchases";
	public static final String USAGE_LINK = "Usage";
	public static final String USAGE_PAGE = "Usage page should be displayed";
	public static final String USAGE_PAGE_ERROR = "Usage page is not displayed";
	public static final String VERIFY_CALL_DEATIL_RECORDS_PAGEHK = "Verify call detail records pagehk.";
	public static final String TABS_MOBILE_THIRD_PARTY = "Third-party purchases: All";
	public static final String USAGEPAGE_OVERVIEW_TABS_LINES = "Lines";
	public static final String USAGEPAGE_OVERVIEW_TABS_MINUTES = "Minutes";
	public static final String USAGEPAGE_TABS_MESSAGES = "Messages";
	public static final String NAVIGATED_TO_TAB = "Navigated To the tab.";
	public static final String USAGE_OVER_TIME = "Usage Over Time Should be displayed";
	public static final String VERIFY_MSISDN_DATA="MSISDN Data not displayed";
	public static final String PAYMENT_ARRANGMENTS_AUTOPAY_ALERT_MSG = "You will be automatically un-enrolled from AutoPay & will no longer receive your $5/mo. AutoPay discount. Re-enroll once your payment arrangement is complete";
	public static final String VERIFY_VISIBLE_DATES_NOTDISPLAYED="Visible dates is not displayed";
	public static final String INLINE_PAYMENT_ERRORS = "We are unable to accept payments from this account.";
	public static final String INLINE_BANK_PAYMENT_ERRORS = "We are unable to accept bank account payments on your account.";
	public static final String INLINE_CARD_PAYMENT_ERRORS = "We are unable to accept card payments on your account.";
	public static final String CANCEL_TEXT_MODAL_PAST_DUE = "Are you sure you want to cancel this transaction? Make a payment now to avoid account suspension.";
	public static final String CANCEL_TEXT_MODAL_NO_PAST_DUE = "Are you sure you want to cancel this transaction? Make a payment now to avoid account suspension.";
	public static final String CANCEL_TEXT_MODAL_SEDONA = "Are you sure you want to cancel this transaction? A payment is required to restore your account.";
	public static final String PAYMENT_ARRANGEMENT_TXT1 = "Make a Payment Arrangement (pay in installments)";
	public static final String PAYMENT_ARRANGEMENT_TXT2 = "Make a payment arrangement online and it's free. Payments made over the phone may incur a $8 Payment Support Fee.";
	public static final String AUTO_PAY_OUTSIDE_BLACKOUT_AMOUNT_ABOVE_ZERO = "If you make a payment now, AutoPay will process any remaining balance on";
	public static final String AUTO_PAY_OUTSIDE_BLACKOUT_AMOUNT_EQUALORLESS_ZERO = "Your next AutoPay payment is scheduled for .";
	public static final String AUTOPAY_ALERT_SEDONA_FULL =  "Once you make this payment your service will be restored within two hours";
	public static final String AUTOPAY_ALERT_SEDONA_LESS = "This payment amount will not restore your service";
	public static final String AUTOPAY_ALERT_NON_SEDONA_FULL = "Upon submission, your account will be immediately restored.";
	public static final String AUTOPAY_ALERT_NON_SEDONA_LESS = "This payment amount will not restore your account.";
	public static final String PII_CUSTOMER_IMEI_PID = "cust_imei";
	public static final String PII_CUSTOMER_PHONENUMBER_PID = "cust_account";
	public static final String PII_CUSTOMER_CUSTOMERNAME_PID = "cust_name";
	public static final String PII_CUSTOMER_PAYMENTINFO_PID = "cust_crd";
	public static final String PII_CUSTOMER_ZIPCODE_PID = "cust_zip";
	public static final String PII_CUSTOMER_BANNUMBER_PID = "cust_account";
	public static final String PII_CUSTOMER_MSISDN_PID = "cust_msisdn";
	public static final String PII_CUSTOMER_CUSTFIRSTNAME_PID = "cust_first";
	public static final String PII_CUSTOMER_BILLINGADDRESS_PID = "cust_bill";
	public static final String PII_CUSTOMER_CUSTLASTNAME_PID = "cust_last";
	public static final String PAYMENT_ARRANGEMENT_OTP_MODAL = "DON'T FORGET! This payment may NOT cancel your scheduled payment of [$XX.XX] from ****<XXXX> on [MM/DD/YYYY] for your Payment Arrangement.";	
	public static final String PAYMENT_ARRANGEMENT_OTP_CONFIRMATION_PAGE = "Your scheduled payment of [$XX.XX] from  ****<XXXX> is still being processed for your Payment Arrangement. View payment schedule";	
	public static final String PA_OTP_SECURED_PA_INSIDE_BLACKOUT_MSG = "PLEASE NOTE! Your scheduled payment of [$XX.XX] from ****<XXXX> is currently being processed for your Payment Arrangement and cannot be cancelled.";
	public static final String PA_OTP_UNSECURE_INSIDE_MORE_AMOUNT = "This payment covers what's due for your Payment Arrangement today.";
	public static final String PA_OTP_UNSECURE_INSIDE_LESS_AMOUNT = "You need to have paid $[XX.XX] toward your Payment Arrangement by today to avoid suspension & fees. Increase amount or review previous payments.";
	public static final String PA_ON_OTP = "This payment may NOT cancel your scheduled  Payment Arrangement.";
	public static final String PA_OTP_UNSECURE_OUTSIDE_LESS_AMOUNT = "You need to have paid $[XX.XX] toward your Payment Arrangement by [MM/DD/YYYY] to avoid suspension & fees. Review payments made.";
	public static final String PA_OTP_SECURED_PA_INSIDE_BLACKOUT_MESSAGE_NOTIFICATION = "This payment will NOT cancel your scheduled Payment Arrangement.";
	public static final String PA_OTP_UNSECURE_MORE_AMOUNT_AFTER12AM_DUEDATE = "This payment covers your payment arrangement amount due today";
	public static final String PA_OTP_UNSECURE_MORE_AMOUNT_BEFORE12AM_DUEDATE = "This payment covers your Payment Arrangement amount due [INSTALLMENT DATE]";
	public static final String PA_OTP_UNSECURE_MORE_AMOUNT_AFTER12AM_DUEDATE2 = "This payment alone will not satisfy the total [$XX.XX] due today for your Payment Arrangement to avoid suspension. Review previous payments";
	public static final String PA_OTP_UNSECURE_LESS_AMOUNT_BEFORE12AM_DUEDATE2 = "This payment alone will not satisfy the total [$XX.XX] due on [INSTALLMENT DATE] for your Payment Arrangement to avoid suspension. Review previous payments";
	public static final String PA_OTP_MISSED_LESS_AMOUNT = "This payment alone will not satisfy the total [$XX.XX] due today for your Payment Arrangement to avoid suspension. Review previous payments";
	public static final String PA_OTP_PAGE_MISSED_LESS_AMOUNT = "This payment covers your Payment Arrangement amount due today.";
	public static final String PA_OTP_PAGE_MISSED_MORE_AMOUNT = "You need to have paid $[XX.XX] toward your Payment Arrangement by today to avoid suspension & fees. Increase amount or review payments made.";
	public static final String PA_OTP_CONF_TOTAL_AMOUNT_OUTSIDE_BLACKOUT = "Your Payment Arrangement is now complete! Save time & avoid missing payments in the future by enrolling in AutoPay.";
	
	public static final String PA_OTP_EDIT_PAGE_MISSED_LESS_AMOUNT = "You need to have paid $[XX.XX] toward your Payment Arrangement by today to avoid suspension & fees. Increase amount or review payments made.";
	public static final String PA_OTP_EDIT_AMOUNT_PAGE_UPDATE_LESSAMOUNT = "This payment alone will not satisfy the total [$XX.XX] due today for your Payment Arrangement to avoid suspension. Increase amount or review previous payments";
	public static final String PA_OTP_EDIT_AMOUNT_PAGE_UPDATE_MOREAMOUNT = "This payment alone will not satisfy the total [$XX.XX] due on [INSTALLMENT DATE] for your Payment Arrangement to avoid suspension. Increase amount or review previous payments";

	public static final String DIGITAL_PA_AMOUNT_BETWEEN_PAST_DUE_AND_TOTAL_DUE = "Once you make this payment, your account will become eligible for a Payment Arrangement";
	public static final String DIGITAL_PA_AMOUNT_LESS_THAN_PAST_DUE = "This payment will not make you eligible for a Payment Arrangement";
	public static final String DIGITAL_PA_AMOUNT_GREATER_THAN_TOTAL_DUE = "This payment will cover your past due balance";
	public static final String DIGITAL_PA_AMOUNT_GREATER_THAN_PAST_DUE_EDIT_AMOUNT_PAGE = "Upon submission, your account will be eligible for a Payment Arrangement";
	public static final String DIGITAL_PA_AMOUNT_LESS_THAN_PAST_DUE_EDIT_AMOUNT_PAGE = "This payment will not make you eligible for Payment Arrangement";
	public static final String INTERRUPT_MODAL_30_DAYS_PAST_DUE = "30+ Day Past Due Amount";
	public static final String TEXT_UNDER_TOTAL_DUE = "Pay my total balance";
	public static final String TEXT_UNDER_PAST_DUE = "Become eligible for Payment Arrangement and pay 30+ days past-due balance";
	public static final String TEXT_UNDER_OTHER_AMOUNT = " required for Payment Arrangement";
	
	public static final String NEW_OTP_PAGE_HEADER = "Please pay the past-due amount";
	public static final String INTERRUPT_MODAL_TEXT = "Before we can set up a payment arrangement, we’ll need a payment for the amount that’s more than 30 days past-due.";
	
	public static final String INTERRUPT_MODAL_TEXT_CONTINUE = "Select Continue to make a payment today. Then we can set up a payment arrangement for the remaining amount.";
	
	public static final String EIPJOD_DOC_RECEIPT_HEADER = "Documents & receipts";
	public static final String EIPJOD_DOC_RECEIPT_BODY = "To view billing, payment history, and electronic agreements related to your account, go to Account History";
	public static final String NO_ACTIVE_EIP = "You don't currently have an active installment plan or lease. Check out all the latest phones and accessories";

	public static final String CARD_NUMBER = "5105105105105100";
	public static final String POIP_OPTION_ONE = "Option 1: Pay for your device in installments";
	public static final String POIP_OPTION_TWO = "Option 2 : Turn in or upgrade";
	public static final String POIP_OPTION_THREE = "Option 3 : Pay for your device later";
	
	public static final String POIP_OPTION_ONE_SUB = "Set up an installment plan to make monthly payments and buy your device.";
	public static final String POIP_OPTION_TWO_SUB = "You can turn in your device instead of paying the purchase option price. Visit a retail store to turn in your device or to learn about your upgrade options.";
	public static final String POIP_OPTION_THREE_SUB = "If you prefer, you can pay for your device all at once. Your purchase option price will be on your monthly bill after your final lease payment.";
	public static final String UNLIMITED_HIGH_SPEED_DATA = "of Unlimited high speed data";
	public static final String UNLIMITED_HIGH_MOBILE_HOTSPOT = "of high speed mobile hotspot";
	public static final String TOTAL_ON_NETWORK_DATA = "Total on-network data";
	public static final String WALLET_FULL_MESSAGE = "Your Wallet is full, you must delete a payment method before you can save another.";
	public static final String POIP_CONFIRMATION_ALERT_MESSAGE = "This transaction is complete. Return to the lease details page to check for additional devices that may require action.";
	public static final String POIP_CONFIRMATION_PAYMENT_MESSAGE = "Your [card type] ending in [last 4] was charged $[XX.XX] on [PAYMENT DATE]";
	public static final String POIP_CONFIRMATION_TWOHOURS_MESSAGE = "Allow up to two hours for this payment to post on your account";
	public static final String EIP_CANCEL_MDOELDIALOG_TEXT= "You must complete this payment to be eligible for an upgrade.  Are you sure you'd like to cancel your payment?";
	public static final String SHOP_OTP_QUALIFYING_AMOUNT_MSG = "Once the past-due amount is paid, you’ll be eligible to continue shopping.";
	public static final String SHOP_OTP_LESS_THAN_QUALIFYING_AMOUNT_MSG = "This payment will not make you eligible to continue shopping.";
	public static final String SHOP_OTP_AMOUNT_PAGE_TOTAL_BAL = "Pay total balance, become eligible to continue shopping.";
	public static final String SHOP_OTP_AMOUNT_PAGE_PAST_DUE = "Pay past-due amount, become eligible to continue shopping.";
	public static final String SHOP_OTP_AMOUNT_PAGE_OTHER = "required to continue shopping.";
	public static final String POIP_TAKEACTION_TEXT = "No action needed.  You have enrolled in a Purchase Option installment plan";
	public static final String CLAIMS_PAGE_HEADER = "Please confirm payment methods.For your security, please re-enter your card or bank account number.We'll only ask you to complete this extra step once.";
	public static final String CLAIMS_REMOVE_ALERT = "Deleting this payment method won’t affect AutoPay or other people on the account.";
	public static final String CLAIM_INLINE_ERROR_MESSAGE = "This number doesn't match our records.";
	public static final String PA_MIN_INSTALLMENT_CASE_ERROR_MSG = "Your balance is too low to set up a payment arrangement. Please contact customer care if you have any questions.";
	public static final String PA_INSTALLMENT_PAGE_HEADER = "Edit details";
	public static final String PA_INSTALLMENT_PAGE_CHOOSE_AN_AMOUNT = "Choose an amount";
	public static final String TOTAL_AMOUNT = "Total Balance";
	public static final String PAST_DUE = "Past Due";
	public static final String NUMBER_OF_SCHEDULED_PAYMENTS = "Number of scheduled payments";
	public static final String MINIMUM_TO_RESTORE = "Minimum to Restore";
	public static final String PAYMENT_SCHEDULE = "Payment schedule";
	public static final String PA_SPOKE_MESSAGING = "Your installment dates and amounts have been adjusted.";
	public static final String PA_SPOKE_MESSAGING_ONE = "Choose Total balance for maximum installment options";
	public static final String MODIFY_PA_CONFIRM_PAGE_ALERT_PAUPDATED = "Your payment arrangement has been updated";
	public static final String MODIFY_PA_CONFIRM_PAGE_ALERT_PAYMENTS_PROCESSING = "Payments will be processed according to the schedule below. Any new bill must be paid by its due date.";
	public static final String MODIFY_PA_CONFIRM_PAGE_ALERT_INSTALLMENT_DATES_AMOUNTS_UPDAETD = "Installment dates and amounts cannot be deleted or changed.";
	public static final String PA_CONFIRM_PAGE_SETUP = "Your payment arrangement has been scheduled";
	public static final String PA_MODIFY_MISSED_ALERT_1 = "We have not yet received your installment payment due";
	public static final String PA_MODIFY_MISSED_ALERT_2 = "Make a One-Time Payment to avoid suspension and collections.";
	
	
	private PaymentConstants() {

	}
}
