package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.NewsPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

/**
 * @author ramesh
 *
 */
public class NewsPageTest extends TmngCommonLib {

	/***
	 * US467705 UNAV - On Site Search
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
	public void VerifyTmngNewsPage(TMNGData tMNGData) {
		Reporter.log("US467705 UNAV - On Site Search");
		Reporter.log("Data Condition | Any Query Parameter for Search Bar ");
		Reporter.log("========================================================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Navigate to T-Mobile news Page | T-Mobile news page is displayed");
		Reporter.log("2. Verify rss fedd link | RSS feed link should be displayed");
		Reporter.log("3. Verify contact us link | Contact us link should be displayed");
		Reporter.log("4. Click and verify contact us link | Contact us page should be displayed");
		Reporter.log("========================================================================");
		Reporter.log("Actual Output:");

		loadTmngURL(tMNGData);

		NewsPage newsPage = new NewsPage(getDriver());
		newsPage.verifyNewsPage();
		newsPage.verifyRSSFeedLink();
		newsPage.verifyContactUsLink();
		newsPage.clickAndVerifyContactUsLink();
	}
}