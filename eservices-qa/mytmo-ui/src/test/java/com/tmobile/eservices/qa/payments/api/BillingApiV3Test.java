package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.BillingApiV3;

import io.restassured.response.Response;

public class BillingApiV3Test extends BillingApiV3 {
	public Map<String, String> tokenMap;

	/**
	 * /**@Updated by Gopimanohar UserStory# Description:US545316:Pass Sprint
	 * Indicator and Sprint BAN to getbilllist
	 * 
	 * @Note : To Validate the Billing Response with Sprint Account and Indicator
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "BillingApiV3Test",
			"testBillsWithLegacyAcctandLegacyIndicator", Group.PAYMENTS, Group.SPRINT })
	public void testBillsWithLegacyAcctandLegacyIndicator(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testBillsWithLegacyAcctandLegacyIndicator");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN also Sprint Ban and Indicator.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with Billing Api.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testBillsWithLegacyAcctandLegacyIndicator";

		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("legacyAccountNumber", apiTestData.getlegacyaccountnumber());
		tokenMap.put("legacyIndicator", apiTestData.getlegacysystemindicator());

		Response response = getBillListwithLegacyAccountandIndicator(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {

				Assert.assertEquals(getPathVal(jsonNode, "statusCode"), "100", "Status Code Mis Matched");
				Assert.assertEquals(getPathVal(jsonNode, "statusMessage"), "Bill list found",
						"Status Message is not Matached");
				Assert.assertEquals(getPathVal(jsonNode, "BillList[0].invoiceType"), "D",
						"Invoice Type Code is not Matached");
				Assert.assertEquals(getPathVal(jsonNode, "BillList[0].billingSystemCode"), "UBP",
						"Billing System Code is not Matached");
				Assert.assertEquals(getPathVal(jsonNode, "BillList[0].legacySystemIndicator"), "SPR",
						"Legacy System Indicator is not Matached");
				/*
				 * Assert.assertEquals(getPathVal(jsonNode,"status"), "SUCCESS",
				 * " Status is Mismatched ");
				 * Assert.assertNotNull(jsonNode.get("documentsResponse").get("documentUrl"),
				 * "Document Url Not presented");
				 */

			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /**@Updated by Gopimanohar UserStory# Description:US545316:Pass Sprint
	 * Indicator and Sprint BAN to getbilllist
	 * 
	 * @Note : To Validate the Billing Response with Sprint Account alone
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "BillingApiV3Test", "testBillsWithLegacyAccount",
			Group.PAYMENTS, Group.SPRINT })
	public void testBillsWithLegacyAccount(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testBillsWithLegacyAccount");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN also Sprint Ban .");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with Billing Api.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testBillsWithLegacyAccount";

		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("legacyAccountNumber", apiTestData.getlegacyaccountnumber());

		Response response = getBillListwithLegacyAccount(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {

				Assert.assertEquals(getPathVal(jsonNode, "statusCode"), "100", "Status Code Mis Matched");
				Assert.assertEquals(getPathVal(jsonNode, "statusMessage"), "Bill list found",
						"Status Message is not Matached");
				Assert.assertEquals(getPathVal(jsonNode, "BillList[2].invoiceType"), "D",
						"Invoice Type Code is not Matached");
				Assert.assertEquals(getPathVal(jsonNode, "BillList[2].billingSystemCode"), "UBP",
						"Billing System Code is not Matached");
				Assert.assertEquals(getPathVal(jsonNode, "BillList[2].legacySystemIndicator"), "SPR",
						"Legacy System Indicator is not Matached");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /**@Updated by Gopimanohar UserStory# Description:US545316:Pass Sprint
	 * Indicator and Sprint BAN to getbilllist
	 * 
	 * @Note : To Validate the Billing Response with Sprint Account alone
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "BillingApiV3Test", "testBillsWithTmobileAccount",
			Group.PAYMENTS, Group.SPRINT })
	public void testBillsWithTmobileAccount(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testBillsWithTmobileAccount");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN .");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with Billing Api.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testBillsWithTmobileAccount";

		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		Response response = getBillListwithTmobileAccount(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {

				Assert.assertEquals(getPathVal(jsonNode, "statusCode"), "100", "Status Code Mis Matched");
				Assert.assertEquals(getPathVal(jsonNode, "statusMessage"), "Bill list found",
						"Status Message is not Matached");
				Assert.assertNotNull(jsonNode.get("BillList").get(0).get("documentId"), "Document Id is Not presented");
				Assert.assertNotNull(jsonNode.get("BillList").get(0).get("currentCharges"),
						"Current Charges is Not presented");
				Assert.assertNotNull(jsonNode.get("BillList").get(0).get("startDate"), "Start Date is Not presented");
				Assert.assertNotNull(jsonNode.get("BillList").get(0).get("endDate"), "End Date is Not presented");
				Assert.assertNotNull(jsonNode.get("BillList").get(0).get("amountDue"), "AmountDue is Not presented");

			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

}