package com.tmobile.eservices.qa.accounts.api;

import org.json.JSONObject;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class NPIRegression {

	static Response response = null;

	@Test
	public void post_OnDeviceFulFillment_ONEPLUSINTERNATIONALDATAPLAN1() {
		methodOne();
	}

	private void methodOne() {

		String url = "https://eos.corporate.t-mobile.com/";

		Reporter.log("Environment is:" + url);
		String acctypes[] = { "IA", "IF", "IG", "IH", "IJ", "IK", "IM", "IR", "IS", "IT", "IU", "IV", "IW", "IX", "IY",
				"BC", "BH", "BL", "BM", "BN", "BO", "BP", "GF", "GG", "GR", "SC", "SD", "SH", "SK", "ST", "SV", "SY",
				"EE", "W1", "W2", "RR" };

		for (String accs : acctypes) {
			String accountType = accs.trim().substring(0, 1);
			String accountsubType = accs.trim().substring(1, 2);

			RestAssured.baseURI = url;
			RequestSpecification request = RestAssured.given();

			request.header("accept", "application/json");
			request.header("applicationid", "MYTMO");

			request.header("channelid", "WEB");

			request.header("postman-token", "3da7abf7-efde-fa6e-2873-1c1bf220b9f8");

			request.header("Authorization", "");

			request.header("transactionid", "unique Guid");
			request.header("transactiontype", "Tradein");
			request.header("usn", "1234567890");
			request.header("transactionBusinessKey", "msisdnno");

			request.header("clientId", "e-servicesUI");
			request.header("transactionBusinessKeyType", "Msisdn Text");
			request.header("phoneNumber", "");

			request.header("correlationId", "test-id-101");
			request.contentType("application/json");

			JSONObject requestParams = new JSONObject();

			JSONObject shopperinfo = new JSONObject();
			shopperinfo.put("salesChannel", "WEB");
			shopperinfo.put("accountType", accountType.toString());
			shopperinfo.put("accountSubType", accountsubType.toString());
			shopperinfo.put("crpid", "CRP1");
			requestParams.put("shopper", shopperinfo);

			JSONObject promotionsinfo = new JSONObject();
			promotionsinfo.put("transactionType", "UPGRADE");
			requestParams.put("promotions", promotionsinfo);

			request.body(requestParams.toString());

			request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");

			response = request.post("v1/promotions/browse");

			JsonPath jsonPathEvaluator = response.jsonPath();

			Reporter.log(
					"***************************************************************************************************************************");

			if (response.body().asString() != null && response.getStatusCode() == 200) {

				if (jsonPathEvaluator.get("status.code").toString().equalsIgnoreCase("200")) {

					if (jsonPathEvaluator.get("status.message").toString().equalsIgnoreCase("OK")) {

						Reporter.log("AccountType: <font face=\"verdana\" color=\"green\">" + accountType
								+ "</font> Account Sub Type: <font face=\"verdana\" color=\"green\">" + accountsubType
								+ "</font>");
						Reporter.log("Promo Codes:<font face=\"verdana\" color=\"green\">"
								+ jsonPathEvaluator.get("promotions.promoName").toString() + "</font>");
						Reporter.log("Promo Names:<font face=\"verdana\" color=\"green\">"
								+ jsonPathEvaluator.get("promotions.promoCustomerFacingName").toString() + "</font>");
					} else {
						Reporter.log("AccountType: <font face=\"verdana\" color=\"blue\">" + accountType
								+ "</font> Account Sub Type: <font face=\"verdana\" color=\"blue\">" + accountsubType
								+ "</font>");
						Reporter.log("Promo Names:<font face=\"verdana\" color=\"blue\">"
								+ jsonPathEvaluator.get("status.message").toString() + "</font>");
					}
				} else {
					Reporter.log("AccountType: <font face=\"verdana\" color=\"red\">" + accountType
							+ "</font> Account Sub Type: <font face=\"verdana\" color=\"red\">" + accountsubType
							+ "</font>");
					Reporter.log("Statuscode is:<font face=\"verdana\" color=\"red\">"
							+ jsonPathEvaluator.get("status.code").toString() + "</font>");
				}

			} else {
				Reporter.log("AccountType: <font face=\"verdana\" color=\"red\">" + accountType
						+ "</font> Account Sub Type: <font face=\"verdana\" color=\"red\">" + accountsubType
						+ "</font>");
				Reporter.log("Statuscode is:<font face=\"verdana\" color=\"red\">"
						+ response.body().asString().toString() + "</font>");
			}
		}

		Reporter.log(
				"***********************************************************************************************************************");

	}

}
