package com.tmobile.eservices.qa.global.functional;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.global.GlobalConstants;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.ChooseAccountPage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.global.NewChooseAccountPage;

import io.appium.java_client.AppiumDriver;

public class ChooseAccountPageTest extends GlobalCommonLib{
	
	
	/**
	 * TC006_MyTMO_IR_Account Chooser and Switch account UNAV Test Case
	 * ID:3273871
	 * 
	 * @param controlTestData
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
	public void verifyIRAccountSwitchUNAV(ControlTestData controlTestData, MyTmoData myTmoData) {

		logger.info("VerifyIRAccountSwitchUNAV method called in TMobileIDTest");
		Reporter.log("Test Data Conditions: Any Msisdn associated with IR account");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Choose Account Page | Choose Account page should be displayed");
		Reporter.log("4. Click on logged in account missdn | Account Home page should be displayed");
		Reporter.log("5. Click on Switch Account | Choose Account page should be displayed");
		Reporter.log("6. Click on another account| Home page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickSwitchAccount();
		ChooseAccountPage accountPage = new ChooseAccountPage(getDriver());
		accountPage.verifySelectAccountHeader();
		Reporter.log("Choose account page is displayed");
		accountPage.chooseAccount(myTmoData.getSwitchAcc());
		homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
	}
	
	
	/**
	 *Ensure hybrid TMOID can log in and switch between lines.
	 *	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.PENDING})
	public void verifyHybridUserLogin(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyHybridUserLogin" );
		Reporter.log("Ensure hybrid TMOID can log in and switch between lines");
		Reporter.log("");
		Reporter.log("Test Data Conditions: Msisdn with Hybrid Customer Login info");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("==============================");
		Reporter.log("Actual Result:");
		
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		/*if(getDriver() instanceof AppiumDriver){
			homePage.clickMobileNavMenu();
		}
		homePage.clickSwitchAccount();
		ChooseAccountPage accountPage = new ChooseAccountPage(getDriver());
		accountPage.verifySelectAccountHeader();
		Reporter.log("Choose account page is displayed");
		myTmoData.setLoginEmailOrPhone(myTmoData.getSwitchAcc());*/
	}
	
	/**
	 *Ensure 'Rebellion' users are routed to the correct experience.	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true,groups = "Login")
	public void verifyRebellionUserLogin(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyHybridUserLogin" );
		Reporter.log("Ensure 'Rebellion' users are routed to the correct experience.");
		Reporter.log("");
		Reporter.log("Test Data Conditions: Msisdn with Rebellion Customer Login info");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("==============================");
		Reporter.log("Actual Result:");
		
		launchAndPerformLogin(myTmoData);
		ChooseAccountPage accountPage = new ChooseAccountPage(getDriver());
		accountPage.verifySelectAccountHeader();
		Reporter.log("Choose Account page is displayed");
		accountPage.chooseAccount(myTmoData.getLoginEmailOrPhone());
		HomePage homePage = new HomePage(getDriver());
		Assert.assertNotNull(homePage.verifyHomePage(), GlobalConstants.HOMEPAGE_ERROR_MSG);
		Reporter.log("Home page is displayed");
		if(getDriver() instanceof AppiumDriver){
			homePage.clickMobileNavMenu();
		}		
		homePage.clickSwitchAccount();
		accountPage.verifySelectAccountHeader();
		Reporter.log("Choose Account page is displayed");
		myTmoData.setLoginEmailOrPhone(myTmoData.getSwitchAcc());
		
		Assert.assertTrue(getDriver().getCurrentUrl().contains("prepaid"), "Rebellion Account Not logged In");
		Reporter.log("Rebellion Account Logged in");
	}
	
	/**
	 * CDCDWR-313 - UNAV MyTmo | Logout & Switch account (New design)
	 * TC-711-Sim Swap_Ensure multiaccount user not able to enter into Sim swap flow
	 * TC-687-Ensure home page loads when multi account user accessing it
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void verifySwitchAccountAndLogOutOperation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("CDCDWR-313 - UNAV MyTmo | Logout & Switch account (New design)");
		Reporter.log("TC-711:Sim Swap_Ensure multiaccount user not able to enter into Sim swap flow");
		Reporter.log("TC-687-Ensure home page loads when multi account user accessing it");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Choose account page should be displayed");
		Reporter.log("3. Choose first account | Home page should be displayed");
		Reporter.log("4. Verify linked phone number in home page | Linked phone number should be matched with selected account missdn");
		Reporter.log("5. Click on switch account | Choose account page should be displayed");
		Reporter.log("6. Choose logged in  account | Home page should be displayed");
		Reporter.log("7. Verify linked misssdn in my line section with switched missdn  | Linked missdn should match with switched missdn");
		Reporter.log("8. Click logout menu | Login page should be displayed");

		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
        launchAndPerformLoginForChooseAccountPage(myTmoData);
		
		NewChooseAccountPage accountPage = new NewChooseAccountPage(getDriver());
		accountPage.verifyChooseAccountPage();
		accountPage.chooseOtherAccount(myTmoData.getSwitchAcc());
		
		NewHomePage homePage=new NewHomePage(getDriver());
		homePage.verifyHomePage();
		homePage.verifyMissdnNumber(myTmoData.getSwitchAcc());
		homePage.clickSwitchAccountMenu();
		
		accountPage.verifyChooseAccountPage();
		accountPage.chooseOtherAccount(myTmoData.getLoginEmailOrPhone());
		homePage.verifyHomePage();
		homePage.verifyMissdnNumber(myTmoData.getLoginEmailOrPhone());
		homePage.clickNewUnavLogOutMenu();

		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPageLoaded();
	}
	
	/**
	 * US307208/US433711/US433718:Account Chooser | Test Support
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void testChooseAccountPageForZeroMultipleAccounts(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Customer with zero multiple accounts");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Verify switch account header menu | Switch account header menu should not be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.verifySwitchAccountHeaderMenu();
	}

}

