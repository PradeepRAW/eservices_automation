package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.InternationalCallingPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class InternationalCallingPageTest extends TmngCommonLib {

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TMNG })
	public void testInternationalCallingPage(TMNGData tMNGData) {
		navigateToInternationalCallingPage(tMNGData);
	}
	
	/**
	 * US578466: Int Calling | Country| Check Rates
	 * US578467: Int Calling | Country| Display Rates (result)
	 * TC303308
	 * TC303310
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testInternationalCallingForStatesideInternationalTalk(TMNGData tMNGData) {

		Reporter.log("US578466: Int Calling | Country| Check Rates");
		Reporter.log("US578467: Int Calling | Country| Display Rates (result)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify International calling link| International calling link should be displayed");
		Reporter.log("4. Click on International calling link | International calling page should be displayed");
		Reporter.log("5. Verify International calling page is displayed | International Calling page loaded");
		Reporter.log("6. Verify country name text box and enter details | Entered country name in the text box");
		Reporter.log("7. Verify country name type ahead list | Selected the value from the type ahead list");
		Reporter.log("8. Click on Check rates cta | Clicked on 'Check rates' CTA successfully");
		Reporter.log("9. Verify entered country name in header. | Country name should be displayed in the header");
		Reporter.log("10. Verify Mobile to Landline, Mobile to Mobile and Text(SMS) columns | Mobile to Landline, Mobile to Mobile and Text(SMS) columns should be displayed");
		Reporter.log("11. Verify 'Stateside International Talk' header | 'Stateside International Talk' header should be displayed");
		Reporter.log("12. Verify information for Stateside International Talk plans | Data is displayed successfully for Stateside International Talk");
		Reporter.log("13. Verify data in Mobile to Landline column | Data is displayed successfully in Mobile To Landline column for Stateside International Talk");
		Reporter.log("14. Verify data in Mobile to Mobile column | Data is displayed successfully in Mobile To Mobile column for Stateside International Talk");
		Reporter.log("15. Verify data in Text(SMS) column | 'Unlimited' is shown in Text(SMS) column for Stateside International Talk, as there is no data");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToInternationalCallingPage(tMNGData);
		InternationalCallingPage internationalCallingPage = new InternationalCallingPage(getDriver());
		internationalCallingPage.setEnterCountryNameTextField(tMNGData.getDestinationName());
		internationalCallingPage.verifyTypeAheadListOfCountryDetails();
		internationalCallingPage.clickCheckRatesButton();
		internationalCallingPage.verifyCountryNameInHeader(tMNGData.getDestinationName());
		internationalCallingPage.verifyResultantTableRowsandColumns();
		internationalCallingPage.verifyStatesideInternationalTalkRates();
		}
	
	/**
	 * US578466: Int Calling | Country| Check Rates
	 * US578467: Int Calling | Country| Display Rates (result)
	 * TC303309
	 * TC303313
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testInternationalCallingForPayPerUse(TMNGData tMNGData) {

		Reporter.log("US578466: Int Calling | Country| Check Rates");
		Reporter.log("US578467: Int Calling | Country| Display Rates (result)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify International calling link| International calling link should be displayed");
		Reporter.log("4. Click on International calling link | International calling page should be displayed");
		Reporter.log("5. Verify International calling page is displayed | International Calling page loaded");
		Reporter.log("6. Verify country name text box and enter details | Entered country name in the text box");
		Reporter.log("7. Verify country name type ahead list | Selected the value from the type ahead list");
		Reporter.log("8. Click on Check rates cta | Clicked on 'Check rates' CTA successfully");
		Reporter.log("9. Verify entered country name in header. | Country name should be displayed in the header");
		Reporter.log("10. Verify Mobile to Landline, Mobile to Mobile and Text(SMS) columns | Mobile to Landline, Mobile to Mobile and Text(SMS) columns should be displayed");
		Reporter.log("11. Verify 'Pay Per Use' header | 'Pay Per Use' header should be displayed");
		Reporter.log("12. Verify information for Pay Per Use plans | Data is displayed successfully for Pay Per Use");
		Reporter.log("13. Verify data in Mobile to Landline column | Data is displayed successfully in Mobile To Landline column for Pay Per Use");
		Reporter.log("14. Verify data in Mobile to Mobile column | Data is displayed successfully in Mobile To Mobile column for Pay Per Use");
		Reporter.log("15. Verify data in Text(SMS) column | 'Unlimited' is shown in Text(SMS) column for Pay Per Use, as there is no data");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToInternationalCallingPage(tMNGData);
		InternationalCallingPage internationalCallingPage = new InternationalCallingPage(getDriver());
		internationalCallingPage.setEnterCountryNameTextField(tMNGData.getDestinationName());
		internationalCallingPage.verifyTypeAheadListOfCountryDetails();
		internationalCallingPage.clickCheckRatesButton();
		internationalCallingPage.verifyCountryNameInHeader(tMNGData.getDestinationName());
		internationalCallingPage.verifyResultantTableRowsandColumns();
		internationalCallingPage.verifyPayPerUseRates();
		}
	
	/**
	 * US578467: Int Calling | Country| Display Rates (result)
	 * TC306354
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testInternationalCallingDestinationWithInvalidData(TMNGData tMNGData) {

		Reporter.log("US578467: Int Calling | Country| Display Rates (result)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify International calling link| International calling link should be displayed");
		Reporter.log("4. Click on International calling link | International calling page should be displayed");
		Reporter.log("5. Verify International calling page is displayed | International Calling page loaded");
		Reporter.log("6. Verify country name text box and enter invalid destination details | Entered country name in the text box.");
		Reporter.log("7. Click on Check rates cta | Clicked on 'Check rates' CTA successfully");
		Reporter.log("8. Verify the Result Message | Message 'Sorry, there are no results for this entry.' is displayed, as the entered destination is invalid");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToInternationalCallingPage(tMNGData);
		InternationalCallingPage internationalCallingPage = new InternationalCallingPage(getDriver());
		internationalCallingPage.setEnterCountryNameTextField(tMNGData.getDestinationName());
		internationalCallingPage.clickCheckRatesButton();
		internationalCallingPage.verifyMsgForInvalidDestination();
		}
	
	/**
	 * US578635: Int Calling | Country| Alias
	 * TC303314
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void testInternationalCallingWithCountryAliasName(TMNGData tMNGData) {

		Reporter.log("US578635: Int Calling | Country| Alias");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the T-Mobile website on browser | T-Mobile homepage should be launched successfully");
		Reporter.log("2. Verify menu hamburger icon | Menu hamburger icon should be displayed");
		Reporter.log("3. Click menu hamburger icon and verify International calling link| International calling link should be displayed");
		Reporter.log("4. Click on International calling link | International calling page should be displayed");
		Reporter.log("5. Verify International calling page is displayed | International Calling page loaded");
		Reporter.log("6. Verify country name text box and enter Country which has Alias | Entered Alias name in the text box.");
		Reporter.log("7. Verify country name type ahead list | User should see name of the country to which Alias belongs ");
		Reporter.log("8. Select the country | Selected the value from the type ahead list");
		Reporter.log("9. Click on Check rates cta | Clicked on 'Check rates' CTA successfully");
		Reporter.log("10. Verify entered country name in header. | Destination name should be displayed in the header");
		Reporter.log("11. Verify Mobile to Landline, Mobile to Mobile and Text(SMS) columns | Mobile to Landline, Mobile to Mobile and Text(SMS) columns should be displayed");
		Reporter.log("12. Verify 'Stateside International Talk' header | 'Stateside International Talk' header should be displayed");
		Reporter.log("13. Verify information for Stateside International Talk plans | Data is displayed successfully for Stateside International Talk");
		Reporter.log("14. Verify data in Mobile to Landline column | Data is displayed successfully in Mobile To Landline column for Stateside International Talk");
		Reporter.log("15. Verify data in Mobile to Mobile column | Data is displayed successfully in Mobile To Mobile column for Stateside International Talk");
		Reporter.log("16. Verify data in Text(SMS) column | 'Unlimited' is shown in Text(SMS) column for Stateside International Talk, as there is no data");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToInternationalCallingPage(tMNGData);
		InternationalCallingPage internationalCallingPage = new InternationalCallingPage(getDriver());
		internationalCallingPage.setEnterCountryNameTextField(tMNGData.getDestinationName());
		internationalCallingPage.verifyDestinationNameForTheAliasNameEntered();
		internationalCallingPage.selectDestinationForAliasFromTypeAheadList();
		internationalCallingPage.clickCheckRatesButton();
		internationalCallingPage.verifyResults();
		
		}
}
