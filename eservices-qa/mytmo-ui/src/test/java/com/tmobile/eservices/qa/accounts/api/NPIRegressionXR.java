package com.tmobile.eservices.qa.accounts.api;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class NPIRegressionXR {

	static Response response = null;

	@Test
	public void post_OnDeviceFulFillment_ONEPLUSINTERNATIONALDATAPLAN1() {
		methodOne();
	}

	private void methodOne() {

		String url = "https://prod-prw.dcp.digital.t-mobile.com/";

		Reporter.log("Environment is:" + url);
		String iresult = "Pass";
		Reporter.log("<table><tbody>");
		Reporter.log("<td>SKU</td><td>Promo ID</td><td>Promo Text</td>");
		String skus[] = { "190198778086", "190198778093", "190198778109", "190198778116", "190198778123",
				"190198778130", "190198778147", "190198778154", "190198778161", "190198778178", "190198778185",
				"190198778192", "190198778024", "190198778031", "190198778048", "190198778055", "190198778062",
				"190198778079" };

		// String acctypes[]=
		// {"IA","IF","IG","IH","IJ","IK","IM","IR","IS","IT","IU","IV","IW","IX","IY","BC","BH","BL","BM","BN","BO","BP","GF","GG","GR","SC","SD","SH","SK","ST","SV","SY","EE","W1","W2","RR"};

		// {"shopper" : {"salesChannel" : "WEB","accountType":
		// "I","accountSubType": "R","crpid": "CRP6"},"products" : [
		// {"tradeInDeviceModel": "iPhone 8 64GB Silver -
		// Bluegrass","newItemSku" : [ "190198793911"]} ],"promotions" :
		// {"transactionTypes" : [ "UPGRADE" ]}}
		for (String sku : skus) {

			RestAssured.baseURI = url;
			RequestSpecification request = RestAssured.given();

			request.header("accept", "application/json");
			request.contentType("application/json");
			request.header("Authorization", "");
			request.header("applicationId", "MYTMO");
			request.header("interactionid", "xasdf1234as");
			request.header("postman-token", "3e58479c-2fa8-91fd-ec9e-068e9f36c150");
			request.header("api-version", "v2");
			request.header("cache-control", "no-cache");
			request.header("channelid", "application/json");
			request.header("Authorization", "WEB");
			request.header("storeid", "MYTMO");
			request.header("transactiontype", "UPGRADE");

			String part1 = "{\"shopper\" : {\"salesChannel\" : \"WEB\",\"accountType\": \"I\",\"accountSubType\": \"R\",\"crpid\": \"CRP6\"},\"products\" : [ {\"newItemSku\" : [ \"";
			String part2 = "\"]} ],\"promotions\" : {\"transactionTypes\" : [ \"UPGRADE\" ]}}";

			String requestbody = part1 + sku + part2;

			request.body(requestbody);

			request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");

			response = request.post("v2/promotions/browse");

			JsonPath jsonPathEvaluator = response.jsonPath();

			Reporter.log("<tr>");

			if (response.body().asString() != null && response.getStatusCode() == 200) {

				if (jsonPathEvaluator.get("notifications").toString() == "[]") {
					Reporter.log("<td>");
					Reporter.log("<font face=\"verdana\" color=\"green\">" + sku + "</font>");
					Reporter.log("</td><td>");
					Reporter.log("<font face=\"verdana\" color=\"green\">"
							+ jsonPathEvaluator.get("promotions.promoName").toString() + "</font>");
					Reporter.log("</td><td>");
					Reporter.log("<font face=\"verdana\" color=\"green\">"
							+ jsonPathEvaluator.get("promotions.promoCustomerFacingName").toString() + "</font>");
					Reporter.log("</td>");
				} else {
					Reporter.log("<td>");
					Reporter.log("<font face=\"verdana\" color=\"red\">" + sku + "</font>");
					Reporter.log("</td><td>");
					Reporter.log("<font face=\"verdana\" color=\"red\">No promos</font>");
					Reporter.log("</td><td>");
					Reporter.log("</td>");
					iresult = "Fail";
				}

			} else {
				Reporter.log("<td>");
				Reporter.log("<font face=\"verdana\" color=\"red\">" + sku + "</font>");
				Reporter.log("</td><td>");
				Reporter.log("<font face=\"verdana\" color=\"red\">" + response.getStatusCode() + "</font>");
				Reporter.log("</td><td>");
				Reporter.log("</td>");
				iresult = "Fail";
			}

			Reporter.log("</tr>");

		}

		Reporter.log("</tbody></table>");
		if (iresult.equalsIgnoreCase("Fail")) {
			Assert.fail();
		}
	}

}
