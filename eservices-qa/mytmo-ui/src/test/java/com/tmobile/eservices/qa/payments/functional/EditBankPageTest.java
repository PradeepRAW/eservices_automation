/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.pages.payments.UpdateBankPage;
import com.tmobile.eservices.qa.pages.payments.WalletPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author Gopimanohar
 *
 */
public class EditBankPageTest extends PaymentCommonLib {

	/**
	 * US577188 UI- L3- Edit Bank info 6.0
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testEditBankPageAngular(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US577188 UI- L3- Edit Bank info 6.0");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved Bank.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Bank blade | Edit Bank page should be displayed");
		Reporter.log("Step 7: verify Bank details | Bank Details should be displayed");
		Reporter.log(
				"Step 8: verify Acocunt number and Routing number | Acocunt number and Routing number should be masked");
		Reporter.log("Step 9: Update Bank details and click save changes button| Spoke page should be displayed");
		Reporter.log("Step 10: Click on Saved Bank blade | Edit Bank page should be displayed");
		Reporter.log("Step 11: verify updated Bank details | Updated Bank Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		WalletPage walletPage = navigateToMyWalletPage(myTmoData);
		if (walletPage.verifyStoredBank()) {
			addBankWallet(myTmoData, walletPage);
		}
		navigateToAngular6PaymentCollectionsPage();
		SpokePage spokePage = navigateToNewSpokePage(myTmoData);
		String storedBankNo = spokePage.verifyAndClickStoredBankAngular();
		UpdateBankPage editBankPage = new UpdateBankPage(getDriver());
		editBankPage.verifyBankPageLoaded(PaymentConstants.Edit_bank_Header);
		editBankPage.verifyEditBankPageElements();
		String nick = editBankPage.updateBankDetailsAndSave(myTmoData.getPayment());
		spokePage.verifyNewPageLoaded();
		spokePage.clickUpdatedStoredBankAngular(storedBankNo);
		editBankPage.verifyUpdatedBankDetails(myTmoData.getPayment(), nick);

	}

	/**
	 * US590356 Phase 1 Update payment method pages (Add bank/add card) 6.0
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testDeleteBankPageAngular(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US590356 Phase 1 Update payment method pages (Add bank/add card) 6.0");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved Bank.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Bank blade | Edit Bank page should be displayed");
		Reporter.log("Step 7: verify Bank details | Bank Details should be displayed");
		Reporter.log(
				"Step 8: verify Acocunt number and Routing number | Acocunt number and Routing number should be masked");
		Reporter.log("Step 9: Update Bank details and click save changes button| Spoke page should be displayed");
		Reporter.log("Step 10: Click on Saved Bank blade | Edit Bank page should be displayed");
		Reporter.log("Step 11: verify updated Bank details | Updated Bank Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		WalletPage walletPage = navigateToMyWalletPage(myTmoData);
		if (walletPage.verifyStoredBank()) {
			addBankWallet(myTmoData, walletPage);
		}
		navigateToAngular6PaymentCollectionsPage();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyNewPageLoaded();
		String storedBankNo = spokePage.verifyAndClickStoredBankAngular();
		UpdateBankPage editBankPage = new UpdateBankPage(getDriver());
		editBankPage.verifyBankPageLoaded(PaymentConstants.Edit_bank_Header);
		editBankPage.verifyEditBankPageElements();
		editBankPage.clickRemoveFromMyWalletLink();
		editBankPage.verifyRemoveFromMyWalletModal();
		editBankPage.clickYesDeleteOnModal();
		spokePage.verifyNewPageLoaded();
		spokePage.verifyDeletedBankIsNotDisplayed(storedBankNo);
	}

	/**
	 * US310778 UI- L3- Edit Bank info 1.5
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testEditBankPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US310778 UI- L3- Edit Bank info 1.5");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved Bank.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Bank blade | Edit Bank page should be displayed");
		Reporter.log("Step 7: verify Bank details | Bank Details should be displayed");
		Reporter.log(
				"Step 8: verify Acocunt number and Routing number | Acocunt number and Routing number should be masked");
		Reporter.log("Step 9: Update Bank details and click save changes button| Spoke page should be displayed");
		Reporter.log("Step 10: Click on Saved Bank blade | Edit Bank page should be displayed");
		Reporter.log("Step 11: verify updated Bank details | Updated Bank Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		WalletPage walletPage = navigateToMyWalletPage(myTmoData);
		if (!walletPage.verifyWalletIsFullMsg()) {
			addBankWallet(myTmoData, walletPage);
		}
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		getDriver().navigate().to(System.getProperty("environment")+"/onetimepayment");
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyPageLoaded();
		String storedBankNo = spokePage.verifyAndClickStoredBank();
		UpdateBankPage editBankPage = new UpdateBankPage(getDriver());
		editBankPage.verifyBankPageLoaded(PaymentConstants.Edit_bank_Header);
		editBankPage.verifyEditBankPageElements();
		String nick = editBankPage.updateBankDetailsAndSave(myTmoData.getPayment());
		spokePage.verifyPageLoaded();
		spokePage.clickUpdatedStoredBank(storedBankNo);
		editBankPage.verifyUpdatedBankDetails(myTmoData.getPayment(), nick);
	}

	/**
	 * US590355 Phase 1 Update payment method pages (Add bank/add card) 1.5x
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testDeleteBankPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US590355 Phase 1 Update payment method pages (Add bank/add card) 1.5x");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved Bank.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Bank blade | Edit Bank page should be displayed");
		Reporter.log("Step 7: Click on Remove from my wallet button | Delete modal should be displayed");
		Reporter.log(
				"Step 8: Click on Yes, Delete CTA | Saved payment record should be deleted and redirected to Spoke page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		WalletPage walletPage = navigateToMyWalletPage(myTmoData);
		if (walletPage.verifyStoredBank()) {
			if (walletPage.verifyWalletIsFullMsg()) {
				deleteAndVerifyStoredCardWallet(walletPage);
			}
			addBankWallet(myTmoData, walletPage);
		}
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		getDriver().navigate().to(System.getProperty("environment")+"/onetimepayment");
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyPageLoaded();
		String storedBankNo = spokePage.verifyAndClickStoredBank();
		UpdateBankPage editBankPage = new UpdateBankPage(getDriver());
		editBankPage.verifyBankPageLoaded(PaymentConstants.Edit_bank_Header);
		editBankPage.verifyEditBankPageElements();
		editBankPage.clickRemoveFromMyWalletLink();
		editBankPage.verifyRemoveFromMyWalletModal();
		editBankPage.clickYesDeleteOnModal();
		spokePage.verifyPageLoaded();
		spokePage.verifyDeletedBankIsNotDisplayed(storedBankNo);
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testEditBankPageWallet(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US577188 UI- L3- Edit Bank info 6.0");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved Bank.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Bank blade | Edit Bank page should be displayed");
		Reporter.log("Step 7: verify Bank details | Bank Details should be displayed");
		Reporter.log(
				"Step 8: verify Acocunt number and Routing number | Acocunt number and Routing number should be masked");
		Reporter.log("Step 9: Update Bank details and click save changes button| Spoke page should be displayed");
		Reporter.log("Step 10: Click on Saved Bank blade | Edit Bank page should be displayed");
		Reporter.log("Step 11: verify updated Bank details | Updated Bank Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		navigateToHomePage(myTmoData);
		WalletPage walletPage = new WalletPage(getDriver());
		getDriver().navigate().to(System.getProperty("environment") + "/mywallet");
		walletPage.verifyPageLoaded();
		walletPage.clickCloseModal();
		if (walletPage.verifyStoredBank()) {
			addBankWallet(myTmoData, walletPage);
		}
		String storedBankNo = walletPage.verifyAndClickStoredBank();
		UpdateBankPage editBankPage = new UpdateBankPage(getDriver());
		editBankPage.verifyBankPageLoaded(PaymentConstants.Edit_bank_Header);
		editBankPage.verifyEditBankPageElements();
		String nick = editBankPage.updateBankDetailsAndSave(myTmoData.getPayment());
		walletPage.verifyPageLoaded();
		walletPage.clickUpdatedStoredBank(storedBankNo);
		editBankPage.verifyUpdatedBankDetails(myTmoData.getPayment(), nick);
	}

	/**
	 * US590356 Phase 1 Update payment method pages (Add bank/add card) 6.0
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PUB_RELEASE })
	public void testDeleteBankPageWallet(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : US590356 Phase 1 Update payment method pages (Add bank/add card) 6.0");
		Reporter.log("Data Condition | Any PAH /Standard User with existing saved Bank.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: Verify OTP Header | OTP Header should be displayed");
		Reporter.log("Step 5: click on payment method blade | Payment Method Spoke Page should be displayed");
		Reporter.log("Step 6: Click on Saved Bank blade | Edit Bank page should be displayed");
		Reporter.log("Step 7: verify Bank details | Bank Details should be displayed");
		Reporter.log(
				"Step 8: verify Acocunt number and Routing number | Acocunt number and Routing number should be masked");
		Reporter.log("Step 9: Update Bank details and click save changes button| Spoke page should be displayed");
		Reporter.log("Step 10: Click on Saved Bank blade | Edit Bank page should be displayed");
		Reporter.log("Step 11: verify updated Bank details | Updated Bank Details should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToHomePage(myTmoData);
		WalletPage walletPage = new WalletPage(getDriver());
		getDriver().navigate().to(System.getProperty("environment") + "/mywallet");
		walletPage.verifyPageLoaded();
		walletPage.clickCloseModal();
		if (walletPage.verifyStoredBank()) {
			if (walletPage.verifyWalletIsFullMsg()) {
				deleteAndVerifyStoredCardWallet(walletPage);
			}
			addBankWallet(myTmoData, walletPage);
		}
		String storedBankNo = walletPage.verifyAndClickStoredBank();
		deleteStoredBank();
		walletPage.verifyPageLoaded();
		walletPage.verifyDeletedPaymentMethodIsNotDisplayed(storedBankNo);
	}

}