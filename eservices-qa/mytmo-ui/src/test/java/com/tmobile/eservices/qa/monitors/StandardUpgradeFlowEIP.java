package com.tmobile.eservices.qa.monitors;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eqm.testfrwk.ui.core.service.SoapService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.eos.CartApi;
import com.tmobile.eservices.qa.api.eos.CatalogApiV3;
import com.tmobile.eservices.qa.api.eos.OrderApi;
import com.tmobile.eservices.qa.api.eos.QuoteApi;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class StandardUpgradeFlowEIP extends ApiCommonLib {


	//public Map<String, String> tokenMap;
	public static Response response = null;
	public Map<String, String> tokenMap;

	@BeforeClass
	public void intitializeService() {
		tokenMap = new HashMap<String, String>();
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void getCRP_EIP(ApiTestData apiTestData) {

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.sku);
		tokenMap.put("correlationid", getValForCorrelation());
		tokenMap.put("email", apiTestData.getEmailId());
		Reporter.log(
				"***************************************************************************************************************************");
		try {
			String requestBody = new ServiceTest().getRequestFromFile("getCRP_XMLRequest.txt");
			String soapAction = "gs.CustomerQualification.runCreditCheck";

			Reporter.log(soapUrl.get("creditCheckRequest"));
			Reporter.log("Request Type : GET");

			SoapService soapService = new SoapService(requestBody, soapAction, soapUrl.get("creditCheckRequest"));
			soapService.addHeader("Content-Type", "text/xml");

			Reporter.log("getCRP Request payload : " + requestBody);

			Response response = soapService.callService();

			tokenMap.put("CRPID",
					response.xmlPath().get("Envelope.Body.creditCheckResponse.creditRiskProfile.identifier"));

			Reporter.log(" <b>Response :</b> ");
			Reporter.log(response.body().asString());

		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}

	}

	@Test(dataProvider = "byColumnName", enabled = true, dependsOnMethods = { "getCRP_EIP" })
	public void familyIdProductPostAPI_EIP(ApiTestData apiTestData) {

		Boolean skuFound = false;
		int jVal=0;
		try {

			String requestBody = new ServiceTest().getRequestFromFile("familyIdProductPostAPI_EIP.txt");
			String firstSku="";
			Reporter.log("TestName: fet Products Catalog List by family");
			Reporter.log("Data Conditions:MyTmo registered misdn.");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("Step 1: Get all the Products Catalog list should get from service response");
			Reporter.log("Step 2: Verify expected Products Catalog list should be in the response list");
			Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
			Reporter.log("================================");			

			System.out.println("requestBody = " + requestBody);
			Reporter.log("familyIdProductPostAPI Request : " + requestBody);
			CatalogApiV3 catalogApiV3 = new CatalogApiV3();
			Response response = catalogApiV3.retrieveGroupedProductsUsingPOST(apiTestData, requestBody, tokenMap);
			System.out.println("response = " + response.asString());
			Reporter.log(" <b>Response :</b> ");

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				Reporter.log("<textarea style=\"border:none;height:300px;\" cols=\"120\" rows=\"5\">"
						+ formatXML(response.body().asString()) + "</textarea>");

				String sku = tokenMap.get("sku");
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode() && jsonNode.has("deviceFamilyInfo")) {
					JsonNode deviceFamilyInfoPathNode = jsonNode.path("deviceFamilyInfo");
					JsonNode parentNode = deviceFamilyInfoPathNode.path("data");
					if (!parentNode.isMissingNode() && parentNode.isArray()) {
						for (int i = 0; i < parentNode.size(); i++) {
							JsonNode dataNode = parentNode.get(i);
							if (dataNode.isObject()) {
								JsonNode devicesInfoPath = dataNode.path("devicesInfo");
								if (!devicesInfoPath.isMissingNode() && devicesInfoPath.isArray()) {
									for (int j = 0; j < devicesInfoPath.size(); j++) {
										if (StringUtils.isNoneEmpty(sku) && sku.equals(devicesInfoPath.get(j).get("skuNumber").asText())) {
											skuFound = true;
											jVal = j;
										}else{
											skuFound = false;
											if((i==parentNode.size()-1) && (j==devicesInfoPath.size()-1)){
												firstSku = getPathVal(devicesInfoPath.get(0), "skuNumber");
												jVal=0;
											}
										}
										if(skuFound || (!skuFound && StringUtils.isNoneEmpty(firstSku))){
											tokenMap.put("sku", getPathVal(devicesInfoPath.get(jVal), "skuNumber"));
											tokenMap.put("color", getPathVal(devicesInfoPath.get(jVal), "color"));
											tokenMap.put("deviceId", getPathVal(devicesInfoPath.get(jVal), "deviceId"));
											tokenMap.put("colorSwatch", getPathVal(devicesInfoPath.get(jVal), "swatch"));
											tokenMap.put("description",
													getPathVal(devicesInfoPath.get(jVal), "description"));
											tokenMap.put("memory", getPathVal(devicesInfoPath.get(jVal), "memory"));
											tokenMap.put("memoryUom", getPathVal(devicesInfoPath.get(jVal), "memoryUOM"));
											tokenMap.put("availabilityStatus", getPathVal(devicesInfoPath.get(jVal),
													"productAvailability.availabilityStatus"));
											tokenMap.put("estimatedShipDateTo", getPathVal(devicesInfoPath.get(jVal),
													"productAvailability.estimatedShipDateTo"));
											tokenMap.put("estimatedShipDateFrom", getPathVal(devicesInfoPath.get(jVal),
													"productAvailability.estimatedShipDateFrom"));
											tokenMap.put("fullRetailPrice",
													getPathVal(devicesInfoPath.get(jVal), "pricing.offerPrice"));
											tokenMap.put("imageUrl",
													getPathVal(devicesInfoPath.get(jVal), "images[0].url"));
											tokenMap.put("familyId", getPathVal(devicesInfoPath.get(jVal), "family"));
											tokenMap.put("modelName", getPathVal(devicesInfoPath.get(j), "modelName"));
											tokenMap.put("payNowAmount", getPathVal(devicesInfoPath.get(jVal),
													"pricing.loans.financeDetails[0].eipInfo.dueTodayAmount.value"));
											tokenMap.put("monthlyAmount", getPathVal(devicesInfoPath.get(jVal),
													"pricing.loans.financeDetails[0].eipInfo.financeAmount.value"));
											tokenMap.put("monthlynoOfMonths",
													getPathVal(devicesInfoPath.get(jVal), "pricing.loans.configuredTerm"));
										}
									}
								}
							}

						}
					}
				}
				
			/*	if(!skuFound){
					Reporter.log(" <b>Invalid Data Response : </b> " + sku +" <b> SKU Not Found in Response");
					Reporter.log(" <b>CreateCart, Quote, Payment, Order Api will Fail : </b> ");
					Assert.fail("<b>Invalid Data Response :</b> " + sku +" <b> SKU Not Found in Response");
			}*/

			} else {
				logStep(StepStatus.FAIL, "response.getStatusCode()", response.body().asString());
				Assert.fail("<b>Response :</b> " + " response.getStatusCode() : "+ response.body().asString());
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Response status code : " + response.getStatusCode());
				Reporter.log("Response : " + response.body().asString());
			}

		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}

	}

	@Test(dataProvider = "byColumnName", enabled = true, dependsOnMethods = { "familyIdProductPostAPI_EIP" })
	public void createCartAPI_EIP(ApiTestData apiTestData) {

		try {
			String requestBody = new ServiceTest().getRequestFromFile("createCartAPI_EIP_forOrder.txt");
			Reporter.log("TestName: Create Cart");
			Reporter.log("Data Conditions:MyTmo registered misdn.");
			Reporter.log("================================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("Step 1: Create Cart should return for the specific user based on the Alert Type");
			Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
			Reporter.log("================================");
			Reporter.log("Actual Result:");

			String updatedRequest = prepareRequestParam(requestBody, tokenMap);
			System.out.println("updated request :" + updatedRequest);
			Reporter.log("createCartAPI Request : " + requestBody);
			CartApi cartApi = new CartApi();
			Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);
			System.out.println(" Create Cart Response : " + response.body().asString());

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

				Reporter.log(" <b>Response :</b> ");
				Reporter.log(response.body().asString());

				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {
					tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
				}
			} else {
				logStep(StepStatus.FAIL, "response.getStatusCode()", response.body().asString());
				Assert.fail("<b>Response :</b> " + " response.getStatusCode() : "+ response.body().asString());
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Response status code : " + response.getStatusCode());
				Reporter.log("Response : " + response.body().asString());
			}
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, dependsOnMethods = { "createCartAPI_EIP" })
	public void CreateQuoteAPI_EIP(ApiTestData apiTestData) {

		try {
			String requestBody = new ServiceTest().getRequestFromFile("CreateQuote_req.txt");
			String latestRequest = prepareRequestParam(requestBody, tokenMap);
			Reporter.log("CreateQuoteAPI Request : " + latestRequest);
			QuoteApi quoteApi = new QuoteApi();
			response = quoteApi.createQuote(apiTestData, latestRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

				Reporter.log(" <b>Response :</b> ");
				Reporter.log(response.body().asString());

				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {

					tokenMap.put("orderId", getPathVal(jsonNode, "orderId"));
					tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));

					Double totaltax = Double.parseDouble(getPathVal(jsonNode, "taxCollection.totalTax"));
					Double payNowAmount = Double.parseDouble(tokenMap.get("payNowAmount"));
					Double chargeAmount = totaltax + 6.99 + payNowAmount;
					DecimalFormat df = new DecimalFormat("###.##");

					tokenMap.put("chargeAmount", df.format(chargeAmount));

					tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[2].shipmentDetailId"));
					tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[2].shippingOptionId"));
					tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
					tokenMap.put("addressLine1", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressLine1"));
					tokenMap.put("cityName", getPathVal(jsonNode, "shippingDetails[0].shipTo.cityName"));
					tokenMap.put("stateCode", getPathVal(jsonNode, "shippingDetails[0].shipTo.stateCode"));
					tokenMap.put("zip", getPathVal(jsonNode, "shippingDetails[0].shipTo.zip"));
					tokenMap.put("countryCode", getPathVal(jsonNode, "shippingDetails[0].shipTo.countryCode"));
					tokenMap.put("tax", getPathVal(jsonNode, "taxCollection.totalTax"));

				}
			} else {
				logStep(StepStatus.FAIL, "response.getStatusCode()", response.body().asString());
				Assert.fail("<b>Response :</b> " + " response.getStatusCode() : "+ response.body().asString());
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Response status code : " + response.getStatusCode());
				Reporter.log("Response : " + response.body().asString());
			}

		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, dependsOnMethods = { "CreateQuoteAPI_EIP" })
	public void UpdateQuoteAPI_EIP(ApiTestData apiTestData) {

		try {
			String requestBody = new ServiceTest().getRequestFromFile("UpdateQuote_EIP_req.txt");
			Reporter.log(
					"***************************************************************************************************************************");

			Reporter.log("Request Type : PUT");

			String updatedRequest = prepareRequestParam(requestBody, tokenMap);

			Reporter.log("UpdateQuoteAPI Request : " + updatedRequest);

			QuoteApi quoteApi = new QuoteApi();
			response = quoteApi.updateQuote(apiTestData, updatedRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				Reporter.log(" <b>Response :</b> ");
				Reporter.log(response.body().asString());

			} else {
				logStep(StepStatus.FAIL, "response.getStatusCode()", response.body().asString());
				Assert.fail("<b>Response :</b> " + " response.getStatusCode() : "+ response.body().asString());
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Response status code : " + response.getStatusCode());
				Reporter.log("Response : " + response.body().asString());
			}

		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, dependsOnMethods = { "UpdateQuoteAPI_EIP" })
	public void UpdatePayment_EIP(ApiTestData apiTestData) {

		try {
			String requestBody = new ServiceTest().getRequestFromFile("updatePayment_req.txt");
			Reporter.log(
					"***************************************************************************************************************************");
			Reporter.log("Request Type : POST");

			String updatedRequest = prepareRequestParam(requestBody, tokenMap);
			Reporter.log("UpdatePayment Request : " + updatedRequest);
			QuoteApi quoteApi = new QuoteApi();
			response = quoteApi.updatePayment(apiTestData, updatedRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

				Reporter.log(" <b>Response :</b> ");
				Reporter.log(response.body().asString());

			} else {
				logStep(StepStatus.FAIL, "response.getStatusCode()", response.body().asString());
				Assert.fail("<b>Response :</b> " + " response.getStatusCode() : "+ response.body().asString());
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Response status code : " + response.getStatusCode());
				Reporter.log("Response : " + response.body().asString());
			}

		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, dependsOnMethods = { "UpdatePayment_EIP" })
	public void OrderAPI_EIP(ApiTestData apiTestData) {

		try {
			String requestBody = new ServiceTest().getRequestFromFile("order_EIP_req.txt");
			Reporter.log(
					"***************************************************************************************************************************");
			Reporter.log("Request Type : POST");

			String updatedRequest = prepareRequestParam(requestBody, tokenMap);

			Reporter.log("CreateOrder Request : " + updatedRequest);

			OrderApi createOrder = new OrderApi();
			response = createOrder.createOrder(apiTestData, updatedRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

				Reporter.log(" <b>Response :</b> ");
				Reporter.log(response.body().asString());

			} else {
				logStep(StepStatus.FAIL, "response.getStatusCode()", response.body().asString());
				Assert.fail("<b>Response :</b> " + " response.getStatusCode() : "+ response.body().asString());
				Reporter.log("Response status code : " + response.getStatusCode());
				Reporter.log("Response : " + response.body().asString());
			}

		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

}
