package com.tmobile.eservices.qa.payments.api;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.SedonaApiCollectionV1;

import io.restassured.response.Response;

public class SedonaApiCollectionV1Test extends SedonaApiCollectionV1 {
	
	public Map<String, String> tokenMap;
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SedonaApiCollectionV1","testGeneratequoteDataPass",Group.PAYMENTS,Group.APIREG  })
	public void testGeneratequoteDataPass(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGeneratequoteDataPass");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with GeneratequoteDataPass.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGeneratequoteDataPass";
		tokenMap=new HashMap<String, String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
        Calendar calCurr = Calendar.getInstance();
        calCurr.setTime(date);
        calCurr.add(Calendar.DATE, 1);
        Date oneDayAheadDate = calCurr.getTime();
        tokenMap.put("startdate", sdf.format(oneDayAheadDate));
        
        calCurr.add(Calendar.DATE, 2);
        Date twoDaysAheadDate = calCurr.getTime();
        tokenMap.put("enddate", sdf.format(twoDaysAheadDate));
		String requestBody = new ServiceTest().getRequestFromFile("generatequote_datapass.txt");	
		
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("startdate", sdf.format(oneDayAheadDate));
		tokenMap.put("enddate", sdf.format(twoDaysAheadDate));
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
	
		Response response = generatequote_datapass(apiTestData, updatedRequest, tokenMap);
	
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				
			}else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SedonaApiCollectionV1","testGeneratequotePA",Group.PAYMENTS,Group.APIREG  })
	public void testGeneratequotePA(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGeneratequotePA");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with testGeneratequotePA.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGeneratequotePA";
		
		tokenMap=new HashMap<String, String>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
        Calendar calCurr = Calendar.getInstance();
        calCurr.setTime(date);
        calCurr.add(Calendar.DATE, 1);
        Date oneDayAheadDate = calCurr.getTime();
        tokenMap.put("startdate", sdf.format(oneDayAheadDate));
        
		String requestBody = new ServiceTest().getRequestFromFile("generatequote_PA.txt");	
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = generatequote_PA(apiTestData, updatedRequest, tokenMap);
	
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				
			}else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	
	/**
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "SedonaApiCollectionV1","testGeneratequoteOTP",Group.PAYMENTS,Group.APIREG  })
	public void testGeneratequoteOTP(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGeneratequoteOTP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with testGeneratequoteOTP.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testGeneratequoteOTP";
		
		tokenMap = new HashMap<String, String>();
		String requestBody = new ServiceTest().getRequestFromFile("generatequote_OTP.txt");	
		
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = generatequote_OTP(apiTestData, updatedRequest, tokenMap);
	
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				
			}else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

}
