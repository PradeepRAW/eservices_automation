package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.ServicesApiV2;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.shop.ShopConstants;

import io.restassured.response.Response;

public class ServiceApiV2Test extends ServicesApiV2 {

	public Map<String, String> tokenMap;

	/**
	 * /** UserStory# Description:
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	// @Test(dataProvider = "byColumnName", enabled = true, groups = {
	// "socapiv2","getServicesForSaleV2",Group.SHOP })
	public void getServicesForSale(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getServicesForSale test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with socs.");
		Reporter.log("Step 2: Verify cart socs returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "getSocsV2";
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_SERVICE + "getSocsV2.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getServicesForSale(apiTestData, updatedRequest);
		Reporter.log("Response Status:" + response.statusCode());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				logSuccessResponse(response, "getServicesForSale");
				String socCode = getPathVal(jsonNode, "socForSale[0].socCode");
				Assert.assertNotNull(socCode, "socCode not found.");

				if (null != response) {
					JSONObject jsonObject = new JSONObject(getJSONfromResponse(response));
					Assert.assertNotNull(jsonObject.get("socForSale"));
					// Assert.assertNotNull(jsonObject.getJSONArray("socForSale"));
				}

			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

}
