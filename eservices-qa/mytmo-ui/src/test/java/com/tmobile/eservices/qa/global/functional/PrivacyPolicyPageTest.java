package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.global.PrivacyPolicyPage;

public class PrivacyPolicyPageTest extends GlobalCommonLib {
	
	/**
	 * Verify Interest Based Ads Link
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true,groups= {Group.PROGRESSION})
	public void verifyInterestBasedAdsLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyInterestBasedAdsLink method");
		Reporter.log("Test Case : Verify Interest-Based Ads Link redirects to Interest-Based Ads info on Privacy Policy Page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Interest-Based Ads link | Interest-Based Ads Info should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickInterestBasedAdsLink();
		homePage.switchToWindow();
		PrivacyPolicyPage privacyPolicyPage = new PrivacyPolicyPage(getDriver());
		privacyPolicyPage.verifyInterestBasedAds();
		Reporter.log("Interest based ads is displayed");

	}
	
	/**
	 * Verify Privacy And Policy Link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups={Group.PENDING})
	public void verifyPrivacyAndPolicy(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyPrivacyAndPolicy method");
		Reporter.log("Test Case : Verify Privacy And Policy Link redirects to privacy policy page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on privacy policy link | Privacy policy Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();	
		homePage.clickPrivacyPolicyLink();
		homePage.switchToWindow();
		homePage.acceptIOSAlert();
		
		PrivacyPolicyPage privacyPolicyPage = new PrivacyPolicyPage(getDriver());
		privacyPolicyPage.verifyPrivacyPolicyPage();
		Reporter.log("Privacy policy page is displayed");

	}

}
