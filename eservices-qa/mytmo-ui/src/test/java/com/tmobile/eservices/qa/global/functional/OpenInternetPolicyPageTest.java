package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.OpenInternetPolicyPage;

public class OpenInternetPolicyPageTest extends GlobalCommonLib{
	
	/**
	 * Verify Privacy Resources Link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups={Group.GLOBAL,Group.DESKTOP,Group.ANDROID})
	public void verifyOpenInternetPolicyLink(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Inside verifyOpenInternetPolicyLink method");
		Reporter.log("Test Case : Verify Open Internet Policy Link redirects to Open Internet Policy page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Open Internet Policy link | Open Internet Policy Page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		NewHomePage homePage=navigateToNewHomePage(myTmoData);
		homePage.clickOpenInternetPolicyLink();
		homePage.switchToWindow();
		
		OpenInternetPolicyPage openInternetPolicyPage = new OpenInternetPolicyPage(getDriver());
		openInternetPolicyPage.verifyOpenInternetPrivacyPage();
	}
	

}
