package com.tmobile.eservices.qa.shop.functional;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.shop.*;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class UNOPLPPageTest extends ShopCommonLib {

	/**
	 * CDCSM-131 :US634740 - As an authenticated customer, I want to view the authenticated version of the 
	 * 			  PLP/PDP pages, so that I can go through the authenticated experience.
	 * TC- 932 : Desktop: Validate that authenticated user is navigated to UNO PLP page from Shop home page
	 * TC- 937 : Desktop: Validate that the  authenticated user is able to view the right PLP URL
	 * TC- 935 : Desktop: Validate that the authenticated user is able to view the legal text onPLP 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "uno"})
	public void testUserNavigatedToUNOPLPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on See All Phones on shop page | Application Navigated to UNO PLP page");
		Reporter.log("4. Verify page URL | URL should contains 'www.t-mobile.com/cell-phones'");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToUNOPLPPageBySeeAllPhones(myTmoData);			
	}
	
	/**
	 * CDCSM-131 :US634740 - As an authenticated customer, I want to view the authenticated version of the 
	 * 			  PLP/PDP pages, so that I can go through the authenticated experience.
	 * TC- 945 : Desktop: Validate that authenticated user is navigated to UNO PLP  page from Upgrade CTA on Shop home page	
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "uno"})
	public void testWhenUserSelectUpgradeCTAInHomeUserNavigateToUNOPLPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Up grade CTA in home page | UNO PLP page should be displayed ");	
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToUNOPLPPageByClickingUpgradeButtonInHomePage(myTmoData);			
	}
	
	/**
	 * CDCSM-131 :US634740 - As an authenticated customer, I want to view the authenticated version of the 
	 * 			  PLP/PDP pages, so that I can go through the authenticated experience.
	 * TC- 946 : Desktop: Validate that authenticated user is navigated to UNO PLP (Shop) page from Upgrade CTA on  Phones page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testUserNavigatedToUNOPLPPageFromPhonesPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Phones tab on home page | Application Navigated to Phones Page ");
		Reporter.log("3. Click on Upgrade CTA | User should navigated to Shop page");
		Reporter.log("4. Click on All phones CTA | UNOPLPPage should be displayed");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToUNOPLPPageThroughPhonesPage(myTmoData);			
	}
	
	/**
	 * CDCSM-131 :US634740 - As an authenticated customer, I want to view the authenticated version of the 
	 * 			  PLP/PDP pages, so that I can go through the authenticated experience.
	 * TC- 947 : Desktop: Validate that authenticated user is navigated to UNO PLP  page from Quick links on Shop home page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "uno"})
	public void testUserNavigatedToUNOPLPPageThroughQuickLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page");
		Reporter.log("3. Click on All phones quick link | UNOPLP Page shuld be displayed");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToUNOPLPPageThroughQuickLink(myTmoData);			
	}
	
	/**
	 * CDCSM-147 :US618543 -As an authenticated user, I want to navigate to UNO PLP from the device intent page when
	 *  			 I've a known intent to add a line, so that I can select the device I desire.
	 * TC-1463 : Desktop:Validate that "Buy a new Phone" CTA on device intent page navigates to PLP page when the entry point was AAL CTA on the Shop landing page
	 *@param data
	 * @param myTmoData 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testVerifyBuyANewPhoneCTAOnDeviceIntentPageNavigateToUNOPLPPageFromShopPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on 'Add a Line' link in shop page | DeviceIntent Page should be displayed");
		Reporter.log("4. Select 'Buy a new phone' option | UNO PLP Page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToUNOPLPPageFromAALBuyNewPhoneFlow(myTmoData);	
			
	}
	
	/**
	 * CDCSM-147 :US618543 -As an authenticated user, I want to navigate to UNO PLP from the device intent page when
	 *  			 I've a known intent to add a line, so that I can select the device I desire.
	 * TC-1464 : Desktop:Validate that "Buy a new Phone" CTA on device intent page navigates to PLP page when the entry point was AAL CTA on the Plan page
	 *@param data
	 * @param myTmoData 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testVerifyBuyANewPhoneCTAOnDeviceIntentPageNavigateToUNOPLPPageFromPlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click Plan link | Plans page should be display");
		Reporter.log("3. Click on 'Add a Line' CTA in plan page | DeviceIntent Page should be displayed");
		Reporter.log("4. Select 'Buy a new phone' option | UNO PLP Page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickPlanLink();
		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
		accountoverviewpage.clickOnAddaLine();
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyANewPhoneOption();
		UNOPLPPage unoPlpPage=new UNOPLPPage(getDriver());
		unoPlpPage.verifyPageLoaded();
	}
	
	/**
	 * CDCSM-147 :US618543 -As an authenticated user, I want to navigate to UNO PLP from the device intent page when
	 *  			 I've a known intent to add a line, so that I can select the device I desire.
	 * TC-1465: Desktop:Validate that "Buy a new Phone" CTA on device intent page navigates to PLP page when the entry point was AAL CTA on the mytmo home page
	 *@param data
	 * @param myTmoData 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testVerifyBuyANewPhoneCTAOnDeviceIntentPageNavigateToUNOPLPPageFromHomePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on 'Add a Line' CTA on home page | DeviceIntent Page should be displayed");
		Reporter.log("3. Select 'Buy a new phone' option | UNO PLP Page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickAddaLineLink();
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyANewPhoneOption();
		UNOPLPPage unoPlpPage=new UNOPLPPage(getDriver());
		unoPlpPage.verifyPageLoaded();
		}
	
	
	/**
	 * CDCSM-147 :US618543 -As an authenticated user, I want to navigate to UNO PLP from the device intent page when
	 *  			 I've a known intent to add a line, so that I can select the device I desire.
	 * TC-1468 : Desktop:Validate that the PLP page displays the pricing of the device based on the AAL known intent
	 *@param data
	 * @param myTmoData 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testVerifyPriceBasedOnAddANewLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on 'Add a Line' link in shop page | DeviceIntent Page should be displayed");
		Reporter.log("4. Select 'Buy a new phone' option | UNO PLP Page should be displayed");
		Reporter.log("5. Validate the pricing of the devices based on  adding a new line | The pricing should be based on Adding a new line ");
	
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToUNOPLPPageFromAALBuyNewPhoneFlow(myTmoData);	
		UNOPLPPage unoPlpPage= new UNOPLPPage(getDriver());
		unoPlpPage.selectSortBy("Price High to Low");
	
	}
	
	/**
	 * CDCSM-147 :US618543 -As an authenticated user, I want to navigate to UNO PLP from the device intent page when
	 *  			 I've a known intent to add a line, so that I can select the device I desire.
	 * TC-1469 : Desktop:Validate that the PLP page displays the promotion  of the device based on the AAL known intent
	 *@param data
	 * @param myTmoData 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testVerifyPromotionBasedOnAddANewLine (ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on 'Add a Line' link in shop page | DeviceIntent Page should be displayed");
		Reporter.log("4. Select 'Buy a new phone' option | UNO PLP Page should be displayed");
		Reporter.log("5. Validate the promotion of the devices based on adding a new line | The promotion  should be based on Adding a new line ");
	
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToUNOPLPPageFromAALBuyNewPhoneFlow(myTmoData);	
		UNOPLPPage unoPlpPage= new UNOPLPPage(getDriver());
		unoPlpPage.clickOnDealsTab();
		unoPlpPage.selectSpecialOfferOnDeals();
		unoPlpPage.verifyPageLoaded();
		unoPlpPage.verifyPromoDescription();
	}
	
	/**
	 * CDCSM-147 :US618543 -As an authenticated user, I want to navigate to UNO PLP from the device intent page when
	 *  			 I've a known intent to add a line, so that I can select the device I desire.
	 * TC-1470 : Desktop:Validate click of back arrow or button on the UNO PLP page 
	 *@param data
	 * @param myTmoData 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testVerifyNavigateBackToDeviceIntentFromUNOPLPpage (ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on 'Add a Line' link in shop page | DeviceIntent Page should be displayed");
		Reporter.log("4. Select 'Buy a new phone' option | UNO PLP Page should be displayed");
		Reporter.log("5. Click on back button on the UNO PLP page | User should be navigated back to the Device intent page ");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToUNOPLPPageFromAALBuyNewPhoneFlow(myTmoData);	
		UNOPLPPage unoPlpPage= new UNOPLPPage(getDriver());
		unoPlpPage.navigateBack();	
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
	}
	
	/**
	 * User Navigated To UNO-PLPPage Through All Quick Links
	 
	 *@param data
	 * @param myTmoData 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno" })
	public void testUserNavigatedToUNOPLPPageThroughAllQuickLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User and eligible for AAL");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop link on home page | Shop Page should be displayed ");			
		Reporter.log("3. Click on 'Apple' quick link | UNOPLPPage should be displayed");
		Reporter.log("4. Navigate back | Shop page should be displayed");
		Reporter.log("5. Click on 'Samsung' quick link | UNOPLPPage should be displayed");
		Reporter.log("6. Navigate back | Shop page should be displayed");
		Reporter.log("7. Click on 'Latest deals' quick link | UNOPLPPage should be displayed");
		Reporter.log("8. Navigate back | Shop page should be displayed");
		Reporter.log("9. Click on 'Accessories' quick link | UNOPLPPage should be displayed");
		Reporter.log("10. Navigate back | Shop page should be displayed");
		Reporter.log("11. Click on 'Add a Line' quick link | DeviceIntent Page should be displayed");
		Reporter.log("12. Select 'Buy a new phone' option | UNO PLP Page should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToShopPage(myTmoData);		
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyPageLoaded();		
		shopPage.clickQuickLinks("Apple");		
		UNOPLPPage unoPLPPage = new UNOPLPPage(getDriver());
		unoPLPPage.verifyPageLoaded();
		
		unoPLPPage.navigateBack();
		shopPage.verifyPageLoaded();
		shopPage.clickQuickLinks("Samsung");		
		unoPLPPage.verifyPageLoaded();
		
		unoPLPPage.navigateBack();
		shopPage.verifyPageLoaded();
		shopPage.clickQuickLinks("Latest deals");		
		unoPLPPage.verifyPageLoaded();
		
		unoPLPPage.navigateBack();
		shopPage.verifyPageLoaded();
		shopPage.clickQuickLinks("All phones");		
		unoPLPPage.verifyPageLoaded();
		
		unoPLPPage.navigateBack();
		shopPage.verifyPageLoaded();
		shopPage.clickQuickLinks("Accessories");		
		unoPLPPage.verifyPageLoaded();
		
		unoPLPPage.navigateBack();
		shopPage.clickQuickLinks("Add a line");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyANewPhoneOption();		
		unoPLPPage.verifyPageLoaded();
		
	}
	
	/**
	 * CDCSM-282 :UNO P2 - Test Only: UNO - MyTMO Redirects
	 * TC-2098 : Validate if the MyTMO PLP URL is redirected to UNOPLP
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno"})
	public void testUserNavigatedToUNOPLPPageFromAllPhonesCTA(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on See All Phones on shop page | Application Navigated to UNO PLP page");
		Reporter.log("4. Verify page URL | URL should contains 'www.t-mobile.com/cell-phones'");
		Reporter.log("5. Verify Phones Header in UNO PLP Page | UNO PLP cell-phones page is loaded");

		
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToUNOPLPPageThroughQuickLink(myTmoData);
		UNOPLPPage unoPLPPage = new UNOPLPPage(getDriver());
		unoPLPPage.verifyPageLoaded();
		unoPLPPage.verifyPhonesHeader();
	}
	
	/**
	 * CDCSM-282 :UNO P2 - Test Only: UNO - MyTMO Redirects
	 * TC-2099 :Validate if the MyTMO PLP URL is redirected to UNOPLP
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno"})
	public void testUserNavigatedToUNOPLPPageWithMyTmoPLPURL(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Enter URL  https://my.t-mobile.com/purchase/productlist  | Application Should Navigate to Uno Plp Page ");
		Reporter.log("4. Verify page URL | URL should contains 'www.t-mobile.com/cell-phones'");
		Reporter.log("5. Verify Phones Header in UNO PLP Page | UNO PLP cell-phones page is loaded");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		getDriver().get(getDriver().getCurrentUrl().replace("/home", "").concat("/purchase/productlist"));
		UNOPLPPage unoPLPPage = new UNOPLPPage(getDriver());
		unoPLPPage.verifyPageLoaded();
		unoPLPPage.verifyPhonesHeader();

	}
	/**
	 * CDCSM-282 :UNO P2 - Test Only: UNO - MyTMO Redirects
	 * TC-2100 :Validate if the MyTMO PDP URL is redirected to UNO PDP 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno"})
	public void testUserNavigatedToUNOPLPPageWithMyTmoPDPURL(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Enter URL  https://my.t-mobile.com/purchase/productdetails   | Application Should Navigate to Uno Plp Page ");
		Reporter.log("4. Verify page URL | URL should contains 'www.t-mobile.com/cell-phones'");
		Reporter.log("5. Verify Phones Header in UNO PLP Page | UNO PLP cell-phones page is loaded");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		getDriver().get(getDriver().getCurrentUrl().replace("/home", "").concat("/purchase/productdetails"));
		UNOPLPPage unoPLPPage = new UNOPLPPage(getDriver());
		unoPLPPage.verifyPageLoaded();
		unoPLPPage.verifyPhonesHeader();
	}
	
	   /**
     * CDCSM-294: UNO Ph2 - Standalone Accessories - Display only Accessories on UNO PLP 
     * if MyTMO CART has Accessory only
     * TC- 1984 : Validate if user is able navigate to other product types
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
    public void testUserNavigatedTOUNOPLPPageWhenUserClickOnContinueShoppingInReviewCartPage(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("================================");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
        Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
        Reporter.log("3. Click on Accessories Quick Link on shop page | Application Navigated to UNO Stand Alone Accessory PLP page");
        Reporter.log("4. Select accessory device | UNOPDP page should be displayed");
        Reporter.log("5. Click on Add to Cart CTA | Cart page should be displayed");
        Reporter.log("6. Click on Continue Shopping CTA | UNOAccessory PLP page should be displayed");    
        Reporter.log("7. Click on Phones & devices tab from UNAV |UNOPLP page should be displayed");
        Reporter.log("8. Verify Phones header |Phones header should be displayed");
        
        Reporter.log("================================");
        Reporter.log("Actual Output:");
        
        UNOPLPPage unoPLPPage = navigateToUNOStandAloneAccessoryPLPPageThroughQuickLink(myTmoData);    
        unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
        UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
        unoPDPPage.verifyPageLoaded();
        unoPDPPage.ClickOnUpGradeCTA();
        AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
                getDriver());
        accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
        accessoriesStandAloneReviewCartPage.clickContinueShoppingButton();
        unoPLPPage.verifyPageLoaded();    
        HomePage homePage = new HomePage(getDriver());    
        homePage.clickOnPhonesLink();       
        unoPLPPage.verifyPageLoaded();
        unoPLPPage.verifyPhonesHeader();
    }
    //********************************
    //********************************
    //********************************
    
    /**
	 * CDCSM-131 :US634740 - As an authenticated customer, I want to view the authenticated version of the 
	 * 			  PLP/PDP pages, so that I can go through the authenticated experience.
	 * TC-948 : Desktop: Validate that authenticated user is navigated to UNO PLP  page from Accessories Quick links on Shop home page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testUserNavigatedToUNOStandAloneAccessoryPLPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on Accessories Quick Link on shop page | Application Navigated to UNO Stand Alone Accessory PLP page");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToUNOStandAloneAccessoryPLPPageThroughQuickLink(myTmoData);			
	}
	
	/**
	 * CDCSM-150: US633348 - As a user, I want to be able to only see all product categories on accessories 
	 * on PLP when i've removed my last item from accessory cart, so that I'm not taken out from 
	 * the accessory flow.
	 * TC- 1837 : Desktop:Validate that when all accessories are removed from cart, user is taken back to accessories flow
	 * TC- 1838 : Desktop:Validate that when user is navigated to PLP page from accessory cart, he is able to navigate to all product catagories
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "uno"})
	public void testUserNavigatedTOAccessoryPLPPageWhenUserDeleteAccessoryInCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on Accessories Quick Link on shop page | Application Navigated to UNO Stand Alone Accessory PLP page");
		Reporter.log("4. Select accessory device | UNOPDP page should be displayed");
		Reporter.log("5. Click on Add to Cart CTA | Cart page should be displayed");
		Reporter.log("6. Click on Remove CTA for Accessory device | UNOAccessory PLP page should be displayed");
		Reporter.log("7. Verify Other product categories i.ePhones, Watches and Accessories | Other product categories links should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		UNOPLPPage unoPLPPage = navigateToUNOStandAloneAccessoryPLPPageThroughQuickLink(myTmoData);	
		unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.ClickOnUpGradeCTA();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.clickRemoveAccessoryDeviceFromReviewCart();
		unoPLPPage.verifyPageLoaded();	
		unoPLPPage.verifyProductCategoriesLinks();
	}
	
	/**
	 * CDCSM-294: UNO Ph2 - Standalone Accessories - Display only Accessories on UNO PLP 
	 * if MyTMO CART has Accessory only
	 * TC- 1980 : Validate if user is navigated to Accessories PLP page when user continues shopping  from acessory cart
	 * TC- 1981 : Validate if user is unable to see the other product categories when navigated from accessories cart
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUserNavigatedTOAccessoryPLPPageWhenUserClickOnContinueShoppingInCartPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on Accessories Quick Link on shop page | Application Navigated to UNO Stand Alone Accessory PLP page");
		Reporter.log("4. Select accessory device | UNOPDP page should be displayed");
		Reporter.log("5. Click on Add to Cart CTA | Cart page should be displayed");
		Reporter.log("6. Click on Continue Shopping CTA | UNOAccessory PLP page should be displayed");	
		Reporter.log("7. Verify Page URL | Page URL contains Accessories only");
		Reporter.log("8. Verify Accessories header | Accessories header should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		UNOPLPPage unoPLPPage = navigateToUNOStandAloneAccessoryPLPPageThroughQuickLink(myTmoData);	
		unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOPDPPage unoPDPPage = new UNOPDPPage(getDriver());
		unoPDPPage.verifyPageLoaded();
		unoPDPPage.ClickOnUpGradeCTA();
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = new AccessoriesStandAloneReviewCartPage(
				getDriver());
		accessoriesStandAloneReviewCartPage.verifyAccessoriesStandAloneReviewCartPage();
		accessoriesStandAloneReviewCartPage.clickContinueShoppingButton();
		unoPLPPage.verifyPageLoaded();	
		unoPLPPage.verifyAccessoriesHeader();		
	}	
		
	/**
	 * CDCSM-282 :UNO P2 - Test Only: UNO - MyTMO Redirects
	 * TC-2102 : Validate if the user is navigated to UNO Accessories PLP page from MyTMO PLP page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno"})
	public void testUserNavigatedToUNOPLPPageFromAccessoriesCTA(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Click on Shop on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on Accessories Quick Link on shop page | Application Navigated to UNO Accessory PLP page");
		Reporter.log("4. Verify page URL | URL should contains 'https://www.t-mobile.com/accessories'");
		Reporter.log("5. Verify Accessories Header in UNO Accessory PLP Page | UNO PLP accessories page is loaded");

		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		UNOPLPPage unoPLPPage = navigateToUNOStandAloneAccessoryPLPPageThroughQuickLink(myTmoData);	
		unoPLPPage.verifyAccessoriesPageLoaded();	
		unoPLPPage.verifyAccessoriesHeader();

	}
	
	/**
	 * CDCSM-282 :UNO P2 - Test Only: UNO - MyTMO Redirects
	 * TC-2101 :Validate if the MyTMO PLP Accessories URL is redirected to UNO Accessories PLP
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "uno"})
	public void testUserNavigatedToUNOAccessoryPLPPageWithMyTmoAccessoryPLPURL(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("================================");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application And Login | Application Should be Launched and Home Page Should be Displayed");
		Reporter.log("2. Enter URL  https://my.t-mobile.com/shop/accessories  | Application Should Navigate to Uno Accessory Plp Page ");
		Reporter.log("4. Verify page URL | URL should contains ' https://www.t-mobile.com/accessories'");
		Reporter.log("5. Verify Accessories Header in UNO PLP Page | UNO PLP accessories page is loaded");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		getDriver().get(getDriver().getCurrentUrl().replace("/home", "").concat("/shop/accessories"));
		UNOPLPPage unoPLPPage = new UNOPLPPage(getDriver());
		unoPLPPage.verifyAccessoriesPageLoaded();
		unoPLPPage.verifyAccessoriesHeader();

	}
	
}

