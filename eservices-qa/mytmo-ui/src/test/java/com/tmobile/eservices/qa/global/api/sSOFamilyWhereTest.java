package com.tmobile.eservices.qa.global.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.sSOFamilyWhere;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class sSOFamilyWhereTest extends sSOFamilyWhere {

	public Map<String, String> tokenMap;

	/**
	 * US553776 - Partner Integration with SSO Family Where | Microservice endpoint
	 * exposure
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "sSOFamilyWhereTest", "testSSOFamilyWhere",
			Group.GLOBAL, Group.SPRINT })
	public void testSSOFamilyWhere(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testSSOFamilyWhere test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response from SSO Family where API.");
		Reporter.log("Step 2: Verify response returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		String operationName = "getSSOFamilyWhere";
		Response response = getSSOFamilyWhere(apiTestData, tokenMap);
		System.out.println(response.getStatusCode());
		System.out.println(response.getBody());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(jsonNode.path("tokenType").textValue(), "cookie");
				Assert.assertNotNull(jsonNode.path("partnerUrl").textValue());
				Assert.assertNotNull(jsonNode.path("partnerToken").textValue());

			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * US515590- Partner Integration with SSO | Migrate 'Ericsson' link from .NET to
	 * eService| eos | Subscriber | Partner
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "sSOFamilyWhereTest",
			"testSSOFamilyWhereMigrateEricssonLink", Group.GLOBAL, Group.SPRINT })
	public void testSSOFamilyWhereMigrateEricssonLink(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testSSOFamilyWhere test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response from SSO Family where API.");
		Reporter.log("Step 2: Verify response returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		String operationName = "getSSOFamilyWhere";
		Response response = getSSOFamilyWhereMigrateEricssonLink(apiTestData, tokenMap);
		System.out.println(response.getStatusCode());
		System.out.println(response.getBody());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(jsonNode.path("tokenType").textValue(), "query");
				Assert.assertNotNull(jsonNode.path("partnerUrl").textValue());
				Assert.assertNotNull(jsonNode.path("partnerToken").textValue());

			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * US523912- Partner Integration with SSO | Migrate 'eBill' link from .NET to
	 * eService| eos | Subscriber | Partner
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "sSOFamilyWhereTest",
			"testSSOFamilyWhereMigrateCallerTunesLink", Group.GLOBAL, Group.SPRINT })
	public void testSSOFamilyWhereMigrateCallerTunesLink(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testSSOFamilyWhere test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response from SSO Family where API.");
		Reporter.log("Step 2: Verify response returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		String operationName = "getSSOFamilyWhere";
		Response response = getSSOFamilyWhereMigrateCallerTunesLink(apiTestData, tokenMap);
		System.out.println(response.getStatusCode());
		System.out.println(response.getBody());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(jsonNode.path("tokenType").textValue(), "query");
				Assert.assertNotNull(jsonNode.path("partnerUrl").textValue());
				Assert.assertNotNull(jsonNode.path("partnerToken").textValue());

			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * US523913 - Partner Integration with SSO | Migrate 'CallerTunes' link from
	 * .NET to eService | eos | Subscriber | Partner
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "sSOFamilyWhereTest",
			"testSSOFamilyWhereMigrateEbillLink", Group.GLOBAL, Group.SPRINT })
	public void testSSOFamilyWhereMigrateEbillLink(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testSSOFamilyWhere test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response from SSO Family where API.");
		Reporter.log("Step 2: Verify response returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		String operationName = "getSSOFamilyWhere";
		Response response = getSSOFamilyWhereMigrateEBillLink(apiTestData, tokenMap);
		System.out.println(response.getStatusCode());
		System.out.println(response.getBody());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(jsonNode.path("tokenType").textValue(), "query");
				Assert.assertNotNull(jsonNode.path("partnerUrl").textValue());
				Assert.assertNotNull(jsonNode.path("partnerToken").textValue());

			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * US523911 - Partner Integration with SSO | Migrate 'VestaCredit' link from
	 * .NET to eService| eos | Subscriber | Partner
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "sSOFamilyWhereTest", "testSSOVistaCredit",
			Group.GLOBAL, Group.SPRINT })
	public void testSSOVistaCredit(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testSSOFamilyWhere test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response from SSO Family where API.");
		Reporter.log("Step 2: Verify response returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		String operationName = "getSSOVistaCredit";
		Response response = getSSOVistaCredit(apiTestData, tokenMap);
		System.out.println(response.getStatusCode());
		System.out.println(response.getBody());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(jsonNode.path("tokenType").textValue(), "form");
				Assert.assertNotNull(jsonNode.path("partnerUrl").textValue());
				Assert.assertNotNull(jsonNode.path("partnerToken").textValue());

			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * US523901- PI SSO | Migrate 'VestaAutoRefill" link from .NET to eService| eos
	 * | Subscriber | Partner
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "sSOFamilyWhereTest", "testSSOVistaAutoRefill",
			Group.GLOBAL, Group.SPRINT })
	public void testSSOVistaAutoRefill(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testSSOFamilyWhere test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response from SSO Family where API.");
		Reporter.log("Step 2: Verify response returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		String operationName = "getSSOVistaAutoRefill";
		Response response = getSSOVistaAutoRefill(apiTestData, tokenMap);
		System.out.println(response.getStatusCode());
		System.out.println(response.getBody());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(jsonNode.path("tokenType").textValue(), "form");
				Assert.assertNotNull(jsonNode.path("partnerUrl").textValue());
				Assert.assertNotNull(jsonNode.path("partnerToken").textValue());

			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * US568685 - Partner Integration with SSO | Migrate 'VestaManagePayment' link
	 * from .NET to microService| eos | Subscriber | Partner
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "sSOFamilyWhereTest", "testSSOVistaManagePayment",
			Group.GLOBAL, Group.SPRINT })
	public void testSSOVistaManagePayment(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testSSOFamilyWhere test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response from SSO Family where API.");
		Reporter.log("Step 2: Verify response returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		String operationName = "getSSOVistaManagePayment";
		Response response = getSSOVistaManagePayment(apiTestData, tokenMap);
		System.out.println(response.getStatusCode());
		System.out.println(response.getBody());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(jsonNode.path("tokenType").textValue(), "form");
				Assert.assertNotNull(jsonNode.path("partnerUrl").textValue());
				Assert.assertNotNull(jsonNode.path("partnerToken").textValue());

			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

}
