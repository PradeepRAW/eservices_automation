package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.AccountVerificationPage;
import com.tmobile.eservices.qa.pages.accounts.LineSettingsPage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.global.AUCMessagePage;
import com.tmobile.eservices.qa.pages.global.AUCTermsAndConditionsPage;
import com.tmobile.eservices.qa.pages.global.LoginPage;

/**
 * @author rnallamilli
 * 
 */
public class LineSettingsPageTest extends GlobalCommonLib {

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROFILE, Group.GLOBAL, Group.DESKTOP,
			Group.ANDROID, Group.IOS })
	public void testNickNameChange(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on line settings menu | Line settings page should be displayed");
		Reporter.log("6. Click nick name link and update | Line settings page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.clickNickNameLink();
		lineSettingsPage.UpdateNickNameAndClickSave();
		lineSettingsPage.verifyLineSettingsPage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROFILE, Group.GLOBAL, Group.DESKTOP,
			Group.ANDROID, Group.IOS })
	public void testUpdateE911address(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Click E911 adress link and click cancel button | Line settings page should be displayed");
		Reporter.log("7. Click E911 adress link and update | Line settings page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result: ");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.clickE911Address();
		lineSettingsPage.verifyCancelButtonCTA();
		lineSettingsPage.verifyLineSettingsPage();
		lineSettingsPage.clickE911Address();
		String addressLine = lineSettingsPage.updateBillingAddress(myTmoData);
		lineSettingsPage.clickSaveChangesBtn();
		lineSettingsPage.verifyUpdatedBillingAddress(addressLine, "E911");
		lineSettingsPage.verifyLineSettingsPage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROFILE, Group.GLOBAL, Group.DESKTOP,
			Group.ANDROID, Group.IOS })
	public void testUsageAddressChange(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Click Usage Address link | Usage Address link should be clicked");
		Reporter.log("7. Verify Usage Address bread crumb active status | Usage Address bread crumb should be active");
		Reporter.log(
				"8: Click Line Settings link and verify Line Settings bread crumb active status| Line Settings bread crumb should be active");
		Reporter.log(
				"9: Click on profile bread crumb and verify Profile bread crumb active status | Profile bread crumb should be active");
		Reporter.log("10:Verify profile page | Profile page should be displayed");
		Reporter.log("11.Click on line settings link | Line settings page should be displayed");
		Reporter.log("12.Click Usage adress link and click cancel button | Line settings page should be displayed");
		Reporter.log("13.Click Usage adress link and update | Line settings page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.clickUsageAddress();
		lineSettingsPage.verifyBreadCrumbActiveStatus("Usage Address");
		lineSettingsPage.clickBreadCrumb("Line Settings");
		lineSettingsPage.verifyLineSettingsPage();
		lineSettingsPage.verifyBreadCrumbActiveStatus("Line Settings");
		lineSettingsPage.clickProfileHomeBreadCrumb();
		lineSettingsPage.verifyBreadCrumbActiveStatus("Profile");

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
		profilePage.clickProfilePageLink("Line Settings");

		lineSettingsPage.clickUsageAddress();
		lineSettingsPage.verifyCancelButtonCTA();
		lineSettingsPage.verifyLineSettingsPage();
		lineSettingsPage.clickUsageAddress();
		String addressLine = lineSettingsPage.updateBillingAddress(myTmoData);
		lineSettingsPage.clickSaveChangesBtn();
		lineSettingsPage.verifyUpdatedBillingAddress(addressLine, "Usage");
		lineSettingsPage.verifyLineSettingsPage();
	}

	/**
	 * 4252402673 / workL!f3balanceACT Ensure Sawtooth KickBack offer does not
	 * appear in profile - Line settings section for Simple Choice Plan
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROFILE, Group.GLOBAL, Group.DESKTOP,
			Group.ANDROID, Group.IOS })
	public void verifySawtoothKickBackOfferNotAppearLineSettingSection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify profile page | Profile page should be displayed");
		Reporter.log("5. Verify KickBack Offers | KickBack Offers should not be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyKickBackNotAppear();
	}

	/**
	 * 4254995365 / workL!f3balanceACT Ensure Sawtooth KickBack offer appears within
	 * profile for PAH, Standard, Full and no toggle for Restricted in Line settings
	 * section for T1 account
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROFILE, Group.GLOBAL, Group.DESKTOP,
			Group.ANDROID, Group.IOS })
	public void verifySawtoothKickBackOfferAppearForPAHandStandard(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify profile page | Profile page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify permissions header | Permissions header should be displayed");
		Reporter.log("7. Verify Primary Account Holder | Primary Account Holder should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyPermissionsHeader();
		lineSettingsPage.verifyPermissionsHolderText("Primary Account Holder");
	}

	/**
	 * 4254425709 / Test12345 Ensure Sawtooth KickBack offer appears within profile
	 * for PAH, Standard, Full and no toggle for Restricted in Line settings section
	 * for T1 account
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifySawtoothKickBackNotAppearForRestricted(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify Profile page | Profile page should be displayed");
		Reporter.log("5. Verify Restricted Access | Restricted access should be displayed");
		Reporter.log("6. Verify KickBack | KickBack should not be displayed for restricted");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyPermissionsHolderText("Restricted Access");
		lineSettingsPage.verifyKickBackNotAppear();
	}

	/**
	 * US390755:Profile | AUC | Confirmation
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPermissionAccessLevel(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify permissions header | Permissions header should be displayed");
		Reporter.log("7. Click on standard access line | Standard access line should be clicked");
		Reporter.log("8. Verify permisson updated to standard access | Standard access permission should be updated");
		Reporter.log("9. Click permissions header | Permissions header should be clicked");
		Reporter.log("10.Select full access permission radio button | Full access radio button should be clicked");
		Reporter.log("11.Click on save changes button | AUC terms and conditions page should be displayed");
		Reporter.log(
				"12.Select legal check box and click on save button | Account verification page should be displayed");
		Reporter.log(
				"13.Verify enter verification code header text | Enter verification code header text should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyPermissionsHeader();
		lineSettingsPage.clickOnStandardAccessLine(myTmoData.getPhoneNumber());
		lineSettingsPage.verifyStandardAccess();
		lineSettingsPage.clickPermissionHeader();
		lineSettingsPage.clickFullAccessRadioButton();

		AUCTermsAndConditionsPage aucTermsAndConditionsPage = new AUCTermsAndConditionsPage(getDriver());
		aucTermsAndConditionsPage.verifyAUCTermsAndConditionsPage();
		aucTermsAndConditionsPage.clickCheckBox();
		aucTermsAndConditionsPage.clickSaveBtn();

		AccountVerificationPage accountVerificationPage = new AccountVerificationPage(getDriver());
		accountVerificationPage.verifyAccountVerificationPage();
		accountVerificationPage.verifyEnterVerificationCodeHeaderText();
	}

	/**
	 * US390755:Profile | AUC | Confirmation
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPermissionAccessLevelForFailurePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify permissions header | Permissions header should be displayed");
		Reporter.log("7. Click on standard access line | Standard access line should be clicked");
		Reporter.log("8. Verify permisson updated to standard access | Standard access permission should be updated");
		Reporter.log("9. Click permissions header | Permissions header should be clicked");
		Reporter.log("10.Select full access permission radio button | Full access radio button should be clicked");
		Reporter.log("11.Click on save changes button | AUC message page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyPermissionsHeader();
		lineSettingsPage.clickOnStandardAccessLine(myTmoData.getPhoneNumber());
		lineSettingsPage.verifyStandardAccess();
		lineSettingsPage.clickPermissionHeader();
		lineSettingsPage.clickFullAccessRadioButton();

		AUCMessagePage aucMessagePage = new AUCMessagePage(getDriver());
		aucMessagePage.verifyAUCMessagePage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyPermissionAccessUpdates(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify permissions header | Permissions header should be displayed");
		Reporter.log(
				"7. Click on standard access line From line selector dropdown | Standard access line should be clicked");
		Reporter.log("8. Verify permisson updated to standard access | Standard access permission should be updated");
		Reporter.log("9. Click permissions header | Permissions header should be clicked");
		Reporter.log("10.Select no access permission radio button | No access radio button should be clicked");
		Reporter.log("11.Verify permisson updated to no access | No access permission should be updated");
		Reporter.log("12.Click permissions header | Permissions header should be clicked");
		Reporter.log(
				"13.Select restricted access permission radio button | Restricted access radio button should be clicked");
		Reporter.log(
				"14.Verify permisson updated to restricted access | Restricted access permission should be updated");
		Reporter.log("15.Click permissions header | Permissions header should be clicked");
		Reporter.log(
				"16.Select standard access permission radio button | Standard access radio button should be clicked");
		Reporter.log("17.Verify permisson updated to standard access | Standard access permission should be updated");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyPermissionsHeader();
		lineSettingsPage.clickOnStandardAccessLine(myTmoData.getPhoneNumber());
		lineSettingsPage.verifyStandardAccess();
		lineSettingsPage.clickPermissionHeader();
		lineSettingsPage.clickNoAccessRadioButton();
		lineSettingsPage.verifyNoAccess();
		lineSettingsPage.clickPermissionHeader();
		lineSettingsPage.clickRestrictedAccessRadioButton();
		lineSettingsPage.verifyRestrictedAccess();
		lineSettingsPage.clickPermissionHeader();
		lineSettingsPage.clickStandardAccessRadioButton();
		lineSettingsPage.verifyStandardAccess();
	}

	/**
	 * US327336/US454528:Profile | Re-Launch Enhancements | Rename Home |
	 * Breadcrumbs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROFILE, Group.GLOBAL, Group.DESKTOP,
			Group.ANDROID, Group.IOS })
	public void verifyLineSettingsBreadCrumbsLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Click Usage Address link | Usage Address link should be clicked");
		Reporter.log("7. Verify Usage Address bread crumb active status | Usage Address bread crumb should be active");
		Reporter.log(
				"8: Click Line Settings link and verify Line Settings bread crumb active status| Line Settings bread crumb should be active");
		Reporter.log(
				"9: Click on profile bread crumb and verify Profile bread crumb active status | Profile bread crumb should be active");
		Reporter.log("10:Verify profile page | Profile page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.clickUsageAddress();
		lineSettingsPage.verifyBreadCrumbActiveStatus("Usage Address");
		lineSettingsPage.clickBreadCrumb("Line Settings");
		lineSettingsPage.verifyLineSettingsPage();
		lineSettingsPage.verifyBreadCrumbActiveStatus("Line Settings");
		lineSettingsPage.clickProfileHomeBreadCrumb();
		lineSettingsPage.verifyBreadCrumbActiveStatus("Profile");

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}

	/**
	 * DE204479:Update Failed while updating the permission level to no acess in
	 * linesettings
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyUpdateOperationFailedDailogBoxInLineSettingsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Click permissions header | Permissions header should be clicked");
		Reporter.log("7. Select no access permission radio button | No access radio button should be clicked");
		Reporter.log(
				"8. Click on save button and verify update operation failed dialog box| Update operation failed dialog box should not be displayedd");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.clickOnStandardAccessLine(myTmoData.getPhoneNumber());
		lineSettingsPage.clickPermissionHeader();
		lineSettingsPage.clickNoAccessRadioButton();
		lineSettingsPage.verifyUpdateOperationFailedDialogBox();
	}

	/**
	 * US531129:Home Page | UNAV | Hamburger Menu
	 * TC-689:Ensure updating name in profile nick name should update in home page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROFILE, Group.GLOBAL, Group.DESKTOP,
			Group.ANDROID, Group.IOS })
	public void verifyFirstNameInHamburgerMenu(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on line settings menu | Line settings page should be displayed");
		Reporter.log("6. Click nick name link | Click on nick name link should be success");
		Reporter.log("7. update first name to 20 characters | Update first name to 20 characters should be success");
		Reporter.log(
				"8. Verify first name in hamburger menu | First name should be updated successfully in hamburger menu");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.clickNickNameLink();
		lineSettingsPage.updateFirstNameTo20CharactersAndClickSave();
		lineSettingsPage.verifyFirstNameInHamburgerMenu();
	}

	/**
	 * US584751:.NET Migration | Gov & Bus | Permissions
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyGovAccountPermissionForMaster(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Govt customer with master permission");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on line settings menu | Line settings page should be displayed");
		Reporter.log("6. Verify permissions header | Permissions header should be displayed");
		Reporter.log("7. Verify master text in permissions holder | Master text should be displayed");
		Reporter.log("8. Click on permission header | Click on permission header should be success");
		Reporter.log("9. Verify gov account permission content | Gov account permission content should be displayed");
		Reporter.log("10.Click on permit all radio button | Click on permit all radio button should be success");
		Reporter.log("11.Verify permissions header | Permissions header should be displayed");
		Reporter.log("12.Verify master text in permissions holder | Master text should be displayed");
		Reporter.log("13.Click on permission header | Click on permission header should be success");
		Reporter.log("14.Verify permit all radio button state| Permit all radio button should be in selected state");
		Reporter.log("15.Verify permissions header | Permissions header should be displayed");
		Reporter.log("16.Verify master text in permissions holder | Master text should be displayed");
		Reporter.log("17.Click on permission header | Click on permission header should be success");
		Reporter.log("18.Click on restrict all radio button | Click on restrict all radio button should be success");
		Reporter.log("19.Verify permissions header | Permissions header should be displayed");
		Reporter.log("20.Verify master text in permissions holder | Master text should be displayed");
		Reporter.log("21.Click on permission header | Click on permission header should be success");
		Reporter.log("22.Verify permit all radio button state| Permit all radio button should be in selected state");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.verifyPermissionsHeader();
		lineSettingsPage.verifyPermissionsHolderText("Master");
		lineSettingsPage.clickPermissionHeader();
		lineSettingsPage.verifyGovAccountPermissionsContent();
		lineSettingsPage.clickPermitAllRadioButton();
		lineSettingsPage.verifyPermissionsHeader();
		lineSettingsPage.verifyPermissionsHolderText("Master");
		lineSettingsPage.clickPermissionHeader();
		lineSettingsPage.verifyPermitAllRadioButtonSelected();
		lineSettingsPage.verifyPermissionsHeader();
		lineSettingsPage.verifyPermissionsHolderText("Master");
		lineSettingsPage.clickPermissionHeader();
		lineSettingsPage.clickRestrictAllRadioButton();
		lineSettingsPage.verifyPermissionsHeader();
		lineSettingsPage.verifyPermissionsHolderText("Master");
		lineSettingsPage.clickPermissionHeader();
		lineSettingsPage.verifyRestrictAllRadioButtonSelected();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_MANAGEADDONS })
	public void verifyPermissionFromPAHToFull(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: PAH with 2 lines");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on line settings link | Line settings page should be displayed");
		Reporter.log("6. Verify permissions header | Permissions header should be displayed");
		Reporter.log("7. Select second line | Second line should be selected");
		Reporter.log("8. Select PAH/Standard Access | PAH/Standard Access should be selected");
		Reporter.log("9. Verify Second Factor Text Message page  | Text Message page should be displayed");
		Reporter.log("10. Verify home page | Home page should be displayed");
		Reporter.log("11. Click on profile menu | Profile page should be displayed");
		Reporter.log("12. Click on line settings link | Line settings page should be displayed");
		Reporter.log("13. Verify permissions header | Permissions header should be displayed");
		Reporter.log("14. Select second line | Second line should be selected");
		Reporter.log("15. Verify Selected Access | PAH/Standard Access should be selected");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LineSettingsPage lineSettingsPage = navigateToLineSettingsPage(myTmoData, "Line Settings");
		lineSettingsPage.selectLineFromDropDownList();
		lineSettingsPage.clickPermissions();

		String selectedAccess = lineSettingsPage.checkAccountAccessAndSelect();
		lineSettingsPage.clickSaveChangesBtn();
		lineSettingsPage.checkPageIsReady();

		if (selectedAccess.contains("fullAccess")) {
			LoginPage loginPage = new LoginPage(getDriver());
			loginPage.enterChooseSecondFactorTextMessage(myTmoData.getLoginEmailOrPhone().trim());

			lineSettingsPage.verifyPermissionChangesTextMessage();
			lineSettingsPage.clickSaveChangesBtn();
		}
		lineSettingsPage.verifyLineSettingsPage();
		lineSettingsPage.clickPermissions();

		lineSettingsPage.verifyFullAccessOrStandardAccess(selectedAccess);
	}
}
