
/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.accounts.ChangePlanAddonsBreakdownPage;
import com.tmobile.eservices.qa.pages.accounts.ChangePlanPlansBreakdownPage;
import com.tmobile.eservices.qa.pages.accounts.ChangePlansReviewPage;
import com.tmobile.eservices.qa.pages.accounts.ExplorePlanPage;
import com.tmobile.eservices.qa.pages.accounts.LineDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.MonthlyTotalBreakdownPage;
import com.tmobile.eservices.qa.pages.accounts.PlanComparisonPage;
import com.tmobile.eservices.qa.pages.accounts.PlanDetailsPage;

public class ChangePlansReviewPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(ChangePlansReviewPageTest.class);

	// Regression Starts

	/**
	 * 
	 * US489050 - Review page - CTAs and Cancel action
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyRedirectionOfCancelCTAOnPlansReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyRedirectionOfCancelCTAOnPlansReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify redirection of Cancel CTA whether it goes to Account Overview page or not");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Click on Cancel CTA. | User should be navigated to Account Overview page");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.clickOnCancelCTA();
		changePlansReviewPage.clickOnCancelYesButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyPooledHybridbanPlanReviewpage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyPooledHybridbanPlanReviewpage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Pooled Hybrid ban Plan Review page");
		Reporter.log("Test Data : Any Pooled Hybrid ban");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage should be loaded");
		Reporter.log("6. Click on TMobile ONE |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log("8. Click on Select Plan Cta |Plan Review Page Should be displayd");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyPooledHybridBanMILinePlanReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyPooledHybridbanPlanReviewpage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Pooled Hybrid ban Plan Review page");
		Reporter.log("Test Data : Any Pooled Hybrid ban");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage should be loaded");
		Reporter.log("6. Click on TMobile ONE |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log("8. Click on Select Plan Cta |Plan Review Page Should be displayd");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();

		AccountOverviewPage accountoverviewpage =  navigateToAccountoverViewPageFlow(myTmoData);
		accountoverviewpage.verifyAccountOverviewPage();
		accountoverviewpage.clickOnLineNamebyText("(425) 362-3148");

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
		linedetailspage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();

		exploreplanpage.navigateToPlanComparisionPage(planName);
		if (plandetailspage.verifyPlanDetailsTitle()) {
			plandetailspage.clickOnSelectPlanButton();
		} else {
			PlanComparisonPage planComparisonPage = new PlanComparisonPage(getDriver());
			planComparisonPage.choosePlanFromChoosePlanSection(planName);
			planComparisonPage.clickOnSelectPlanCta();
		}

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();
		changePlansReviewPage.clickOnCancelCTA();
	}

	/**
	 * US489166 - Review page - Below the CTAs - congestion related T&Cs
	 * 
	 * @throws InterruptedException
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyCongetionRelatedTermsAndConditionsOnPlansReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyCongetionRelatedTermsAndConditionsOnPlansReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Congestion related T&C on Plans Review page");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Check Congestion related T&C. | Those should be displayed.");
		Reporter.log("7. Verify Congestion related T&C. | Those should be correct.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();

		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.checkCongestionRelatedTAndC();
		changePlansReviewPage.clickOnCancelCTA();
		changePlansReviewPage.clickOnCancelYesButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
	}

	/**
	 * @author prokarma
	 *
	 *         US489011 - Review page - Plan header and name display
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyPlanHeaderAndPlanNameOnPlansReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyPlanHeaderAndPlanNameOnPlansReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Plan header,Plan name and subtext and new Plan image on Plans Review page");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Check Header,Subheader and Promotional text. | It should be displayed.");
		Reporter.log("7. Check Image of New Plan. | It should be displayed.");
		Reporter.log("8. Check New Plan name. | It should be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();

		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.checkHeaderOnPlansReviewPage();
		changePlansReviewPage.checkSubHeaderOnPlansReviewPage();
		changePlansReviewPage.checkPromotionalTextOnPlansReviewPage();
		changePlansReviewPage.checkTextNewPlanOnPlansReviewPage();
		changePlansReviewPage.checkImageOfNewPlanOnPlansReviewPage();
	}

	/**
	 * @author prokarma
	 *
	 *         US489035 - Review page - Plan cost blade
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyPlanCostOnPlanBladeOnPlansReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyPlanCostOnPlanBladeOnPlansReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify cost on Plans blade. It should match with the cost on plans breakdown page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Check cost on Plans blade. | It should be matched with cost on Plans breakdown page.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);

		changePlansReviewPage.checkTextPlansOnPlansBlade();
		changePlansReviewPage.checkTaxesAndFeesTextOnPlansBlade();
		String costOnPlansBlade = changePlansReviewPage.getCostOnPlansBlade();
		changePlansReviewPage.clickOnPlansBlade();

		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();
		String totalMonthlyCostOfPlansBreakdown = changePlanPlansBreakdownPage
				.getTotalMonthlyCostWithFormatOfDollarSign();

		changePlanPlansBreakdownPage.verifyAddOnCostDetails(costOnPlansBlade, totalMonthlyCostOfPlansBreakdown);
	}

	/**
	 * @author prokarma
	 *
	 *         US489037 - Review page - Add-on cost blade
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyAddOnCostOnAddonBladeOnPlansReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyPlanCostOnPlanBladeOnPlansReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify cost on Plans blade. It should match with the cost on plans breakdown page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Check cost on Addons blade. | It should be matched with cost on Addons breakdown page.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.checkTextAddonsOnAddonsBlade();
		String costOnPlansBlade = changePlansReviewPage.verifyCostOnAddonsBlade();

		changePlansReviewPage.clickOnAddonsBlade();

		ChangePlanAddonsBreakdownPage changePlanAddonsBreakdownPage = new ChangePlanAddonsBreakdownPage(getDriver());
		changePlanAddonsBreakdownPage.verifyAddonsBreakdownPage();
		String totalAddOnsBreakdownCost = changePlanAddonsBreakdownPage
				.checkAndVerifyCostAtAccountLevelAndLineLevelAgainstTotalMonthlyCost();
		changePlanAddonsBreakdownPage.verifyAddOnCostDetails(costOnPlansBlade, totalAddOnsBreakdownCost);
	}

	/**
	 * @author prokarma
	 *
	 *         US489040 - Review page - New Monthly Total blade
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyCostOnNewMonthlyTotalBladeOnPlansReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyCostOnNewMonthlyTotalBladeOnPlansReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify cost on New Monthly Total Blade");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log(
				"6. Check cost on New Monthly Total blade. | It should be matched with cost on New Monthly total breakdown page.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.checkTextNewMonthlyTotalOnMonthlyTotalBlade();
		changePlansReviewPage.checkTaxesAndFeesTextOnNewMonthlyTotalBlade(planName);
		String costOnNewMonthlyTotalBlade = changePlansReviewPage.verifyCostOnNewMonthlyTotalBlade();

		changePlansReviewPage.clickOnNewMonthlyTotalBlade();

		MonthlyTotalBreakdownPage monthlyTotalBreakdownPage = new MonthlyTotalBreakdownPage(getDriver());

		monthlyTotalBreakdownPage.verifyNewMonthlyTotalBreakdownPage();
		String monthlyTotalCostOnBreakDownPage = monthlyTotalBreakdownPage.getTotalMonthlyCostWithFormatOfDollarSign();

		monthlyTotalBreakdownPage.verifyCostOnNewMonthlyTotalBlade(costOnNewMonthlyTotalBlade,
				monthlyTotalCostOnBreakDownPage);
	}

	/**
	 * @author prokarma
	 *
	 *         US489045 - Review page - Effective date for changes
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyEffectiveDateOnPlansReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyEffectiveDataPlansReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Effective data below delta cost on PlansReview page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log(
				"6. Check Effective date on Plans Review page. | Effective date should be displayed on Plans Review page.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.checkEffectiveDateText();
		changePlansReviewPage.checkEffectiveDate();
	}

	/**
	 * @author prokarma
	 *
	 *         US489049 - Review page - Above the CTAs legalese related T&Cs
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyLegalTermsAndConditionsOnPlansReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyLegalTermsAndConditionsOnPlansReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Legal Terms and Conditions on PlansReview page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log(
				"6. Check Legal text on Plans Review page. | Legal text should be displayed on Plans Review page.");
		Reporter.log(
				"7. Click on Service Agreement link in Legal text on Plans Review page. | It should redirect to Plan-agreement page.");
		Reporter.log(
				"8. Click on Electronic Signature link in Legal text on Plans Review page. | It should redirect to Plan-esignature page.");
		Reporter.log(
				"9. Click on Terms & Conditions link in Legal text on Plans Review page. | It should redirect to Terms and conditions page and that should be opened in new tab.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.checkLegalTextOnPlansAndReviewPage();
	}

	/**
	 * @author prokarma
	 *
	 *         US489313 - Review page - AutoPay - T&Cs copy and link
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyTermsAndConditionsForAutopayOnPlansReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyTermsAndConditionsForAutopayOnPlansReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Terms and Conditions for Autopay on PlansReview page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full and Autopay should be OFF");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log(
				"6. Check Terms and conditions link of Autopay on Plans Review page. | Terms and conditions page shoul dbe loaded in same tab.");
		Reporter.log("7. Check Terms and conditions text. | Correct contents should be displayed.");
		Reporter.log("8. Click on Back CTA on Terms and Conditions page. | It should redirect to Plans Review page.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.checkAutopayLegalTextOnPlansAndReviewPage();
		changePlansReviewPage.clickOnTermsAndConditionsLinkOnAutopayLegalText();
		changePlansReviewPage.checkHeaderOnAutopayTermsAndConditionsPage();
		changePlansReviewPage.checkPrintIconAndPrintThisPageLink();
		changePlansReviewPage.checkAutopayTextFromAutopayTermsAndConditionsPage();
		changePlansReviewPage.checkBackCTAOnAutopayTermsAndConditionsPage();
	}

	/**
	 * @author prokarma
	 *
	 *         US527960 - Review page - AutoPay: First time sign up - toggle on
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyAutopayCostAndMessageOnAutopayBlade(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyAutopayCostAndMessageOnAutopayBlade method called in ChangePlansReviewPage");
		Reporter.log(
				"Test Case : Verify Autopay saving message on Autopay blade. Also check amoutn saved which will be $5/line.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full and Autopay should be OFF");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log(
				"6. Check Message on Autopay blade. | Message 'You’re saving $<XX.XX>/mo. with AutoPay.' should be displayed.");
		Reporter.log("7. Check discount amount. | discount amount should be displayed correctly.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);

		changePlansReviewPage.checkFirstAutopayDateAndMessage();
		changePlansReviewPage.checkAutoPaySavingMessageOnAutopayBlade();
	}

	/**
	 * @author prokarma
	 *
	 *         US527961 - Review page - AutoPay: First time sign up - toggle off
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyPaymentBladeAndAutopayMessageWhenAutopayIsOFF(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyPaymentBladeAndAutopayMessageWhenAutopayIsOFF method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Payment blade and autopay message when user turnoff Autopay.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full and Autopay should be OFF");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Turn off Autopay on Autopay blade. | User should be able to Turn OFF Autopay");
		Reporter.log("7. Check Payment blade. | Payment blade should not be displayed.");
		Reporter.log("8. Check First Autopay Payment date and discount message. | It should not be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.turnAutoPayOFFToggle();
		changePlansReviewPage.whenPaymentMethodBladeNotDisplayed();
		changePlansReviewPage.whenFirstAutopayDateAndMessageNotDisplayed();
	}

	/**
	 * @author prokarma US551662 - Review page - AutoPay: Suppress Autopay and
	 *         payment method blade for users already signed up for Autopay
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyWhetherAutopayBladeIsSuppressedOrNotWhenAutopayIsAlreadyON(ControlTestData data,
			MyTmoData myTmoData) throws InterruptedException {
		logger.info(
				"verifyWhetherAutopayBladeIsSuppressedOrNotWhenAutopayIsAlreadyON method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify whether Autopay blade is Suppressed or not when Autopay is already ON.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full and Autopay should be ON");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Check Autopay blade. | Autopay blade should not be displayed.");
		Reporter.log("7. Check Payment blade. | It should not be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.whenAutopayBladeNotDispalyed();
		changePlansReviewPage.whenPaymentMethodBladeNotDisplayed();

		changePlansReviewPage.clickOnPlansBlade();

		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();
		changePlanPlansBreakdownPage.checkWhetherAutoPayDiscountComingOrNot();
	}

	/**
	 * @author prokarma
	 *
	 *         US553870 - Review page - Display warning icon for Plans blade in
	 *         migration scenario only where we default MBB plan
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyWarningIconOnPlansBladelWhenGSMStartsChangePlanAndMBBIsCompatible(ControlTestData data,
			MyTmoData myTmoData) throws InterruptedException {
		logger.info(
				"verifyWarningIconOnPlansBladelWhenGSMStartsChangePlanAndMBBIsCompatible method called in ChangePlansReviewPage");
		Reporter.log(
				"Test Case : Verify whether user gets warning icon on plans blade or not when GSM starts change plan and MBB is compatible");
		Reporter.log("Test Data : Any MSDSIN PAH/Full and on TI plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Check Warning Modal for MBB. | Warning modal should be displayed.");
		Reporter.log("7. Click Continue CTA. | Plans Review page should be displayed.");
		Reporter.log("8. Check warning icon on Plans blade. | Warning icon should not be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalWhenMBBIsCompatibleAndGSMStartsChangePlan();
		changePlansReviewPage.clickOnPlansBlade();

		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();
		changePlanPlansBreakdownPage.checkWhetherMBBIncompatibleMessageIsNotDisplayed();
	}

	/**
	 * @author prokarma
	 *
	 *         US489011 - Review page - Plan header and name display
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyPlanNameWhenUserChangePlanFromSCtoTI(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyPlanNameWhenUserChangePlanFromSCtoTI method called in ChangePlansReviewPage");
		Reporter.log(
				"Test Case : Verify Plan name whether plan name on Comprision page and Plans Review page is matched or not");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Check Header,Subheader and Promotional text. | It should be displayed.");
		Reporter.log("7. Check Image of New Plan. | It should be displayed.");
		Reporter.log("8. Check New Plan name. | It should be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.verifyChangePlansReviewPage();
		changePlansReviewPage.clickOnPlansBlade();

		ChangePlanPlansBreakdownPage changePlanPlansBreakdownPage = new ChangePlanPlansBreakdownPage(getDriver());
		changePlanPlansBreakdownPage.verifyPlansBreakdownPage();
		changePlanPlansBreakdownPage.getTotalMonthlyCostWithFormatOfDollarSign();
		changePlanPlansBreakdownPage.clickOnBackToSummaryCTA();
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.clickOnAddonsBlade();

		ChangePlanAddonsBreakdownPage changePlanAddonsBreakdownPage = new ChangePlanAddonsBreakdownPage(getDriver());
		changePlanAddonsBreakdownPage.verifyAddonsBreakdownPage();
		changePlanAddonsBreakdownPage.checkAndVerifyCostAtAccountLevelAndLineLevelAgainstTotalMonthlyCost();
		changePlanAddonsBreakdownPage.clickOnBackToSummaryCTA();
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.clickOnNewMonthlyTotalBlade();

		MonthlyTotalBreakdownPage monthlyTotalBreakdownPage = new MonthlyTotalBreakdownPage(getDriver());
		monthlyTotalBreakdownPage.verifyNewMonthlyTotalBreakdownPage();
		monthlyTotalBreakdownPage.checkAndVerifyTotalMonthlyLevelCostAgainstAccountLevelAndLineLevel();
		monthlyTotalBreakdownPage.getTotalMonthlyCostWithFormatOfDollarSign();
		monthlyTotalBreakdownPage.clickOnBackToSummaryCTA();
		changePlansReviewPage.verifyChangePlansReviewPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyNonPooledHybridIOTLineReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyNonPooledHybridIOTLineReviewPage  method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Non Pooled Hybrid IOT Line Review Page");
		Reporter.log("Test Data : Any Pooled Hybrid ban with IOT Line");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage should be loaded");
		Reporter.log("6. Click on Plan name |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log("8. Click on Select Plan Cta |Plan Review Page Should be displayd");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		// String planName = myTmoData.getplanname().trim();

		navigateToAccountoverViewPageFlow(myTmoData);
		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
		accountoverviewpage.clickOnLineNamebyText("(425) 393-8547");

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
		linedetailspage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();

		exploreplanpage.navigateToPlanComparisionPage("");
		if (plandetailspage.verifyPlanDetailsTitle()) {
			plandetailspage.clickOnSelectPlanButton();
		}
		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.verifyNewPlanName("SyncUP");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyNonPooledHybridISPLineBestPlanMessageOnExplorerPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info(
				"VerifyNonPooledHybridISPLineBestPlanMessageOnExplorerPage   method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Non Pooled Hybrid ISP Line Best Plan Message On Explorer Page ");
		Reporter.log("Test Data : Any Pooled Hybrid ban with ISP line");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage should be loaded");
		Reporter.log("6. Click on Plan name |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log("8. Click on Select Plan Cta |Plan Review Page Should be displayd");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToAccountoverViewPageFlow(myTmoData);
		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
		accountoverviewpage.clickOnLineNameByMisdn("(425) 410-6986");

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
		linedetailspage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
		exploreplanpage.verifyNotificationMessage(
				"You are not eligible for any other rate plans at this time. Amp up your plan with add-ons");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyNonPooledHybridMBBLineReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyNonPooledHybridMBBLineReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Non Pooled Hybrid MBB Line Review Page ");
		Reporter.log("Test Data : Any Pooled Hybrid ban with MBB line");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage should be loaded");
		Reporter.log("6. Click on Plan name |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log("8. Click on Select Plan Cta |Plan Review Page Should be displayd");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		// String planName = myTmoData.getplanname().trim();

		navigateToAccountoverViewPageFlow(myTmoData);
		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
		accountoverviewpage.clickOnLineNamebyText("(425) 393-8332");

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
		linedetailspage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();

		exploreplanpage.navigateToPlanComparisionPage("");
		if (plandetailspage.verifyPlanDetailsTitle()) {
			plandetailspage.clickOnSelectPlanButton();
		} else {
			PlanComparisonPage planComparisonPage = new PlanComparisonPage(getDriver());
			planComparisonPage.choosePlanFromChoosePlanSection("");
			planComparisonPage.clickOnSelectPlanCta();
		}

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.verifyNewPlanName("Mobile Internet");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyNonPooledHybridDataWithDigitsLineBestPlanMessageOnExplorerPage(ControlTestData data,
			MyTmoData myTmoData) throws InterruptedException {
		logger.info(
				"VerifyNonPooledHybridDataWithDigitsLineBestPlanMessageOnExplorerPage  method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Non Pooled Hybrid Digits Line Best Plan Message On Explorer Page ");
		Reporter.log("Test Data : Any Pooled Hybrid ban with Digits line");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage should be loaded");
		Reporter.log("6. Click on Plan name |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log("8. Click on Select Plan Cta |Plan Review Page Should be displayd");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToAccountoverViewPageFlow(myTmoData);
		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
		accountoverviewpage.clickOnLineNamebyText("(425) 393-8553");

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
		linedetailspage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
		exploreplanpage.verifyNotificationMessage(
				"You are not eligible for any other rate plans at this time. Amp up your plan with add-ons");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyNonPooledHybridDigitsTalkandTextVirtualLineBestPlanMessageOnExplorerPage(ControlTestData data,
			MyTmoData myTmoData) throws InterruptedException {
		logger.info(
				"VerifyNonPooledHybridDigitsTalkandTextVirtualLineBestPlanMessageOnExplorerPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Non Pooled Hybrid Virtual Line Best Plan Message On Explorer Page ");
		Reporter.log("Test Data : Any Pooled Hybrid ban with Virtual line");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage should be loaded");
		Reporter.log("6. Click on Plan name |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log("8. Click on Select Plan Cta |Plan Review Page Should be displayd");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToAccountoverViewPageFlow(myTmoData);
		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
		accountoverviewpage.clickOnLineNameByMisdn("(425) 443-1421");

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
		linedetailspage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
		exploreplanpage.verifyNotificationMessage(
				"You are not eligible for any other rate plans at this time. Amp up your plan with add-ons");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyNonPooledGSMLinePlanReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("VerifyNonPooledGSMLinePlanReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Non Pooled Hybrid GSM Line Review Page ");
		Reporter.log("Test Data : Any Pooled Hybrid ban with GSM line");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage should be loaded");
		Reporter.log("6. Click on Plan name |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log("8. Click on Select Plan Cta |Plan Review Page Should be displayd");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();

		navigateToAccountoverViewPageFlow(myTmoData);
		AccountOverviewPage accountoverviewpage = new AccountOverviewPage(getDriver());
		accountoverviewpage.verifyAccountOverviewPage();
		accountoverviewpage.clickOnLineNamebyText("(425) 435-4426");

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
		linedetailspage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();

		exploreplanpage.navigateToPlanComparisionPage("");
		if (plandetailspage.verifyPlanDetailsTitle()) {
			plandetailspage.clickOnSelectPlanButton();
		} else {
			PlanComparisonPage planComparisonPage = new PlanComparisonPage(getDriver());
			planComparisonPage.choosePlanFromChoosePlanSection(planName);
			planComparisonPage.clickOnSelectPlanCta();
		}

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();

		changePlansReviewPage.verifyNewPlanName("Magenta Plus");
	}

	// Regression End

	/*
	 * Test Plan Price - TestPriceInPlanReviewPageforTMobileONEwithONEPlus
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testPriceAndConflictDataPlanInPlanReviewPageforTMobileONEwithONEPlus(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("TestPriceAndConflictDataPlanInPlanReviewPageforTMobileONEwithONEPlus");
		Reporter.log("Test Case : TestPriceAndConflictDataPlanInPlanReviewPageforTMobileONEwithONEPlus");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage should be loaded");
		Reporter.log("6. Click on TMobile ONEwith ONEPlus plan |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log("8. Click on Select Plan Cta |Plan Review Page Should be displayd");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnDigitsline();
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
		exploreplanpage.clickonTMobileONEwithONEPlus();

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.clickOnSelectPlanCta();

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());

		changePlansReviewPage.clickOnAddonsBlade();
		ChangePlanAddonsBreakdownPage changePlanAddonsBreakdownPage = new ChangePlanAddonsBreakdownPage(getDriver());
		changePlanAddonsBreakdownPage.verifyAddonsBreakdownPage();

		changePlanAddonsBreakdownPage.verifyAddOnsPlanName("T-Mobile ONE Plus Family");
		changePlanAddonsBreakdownPage.verifyProtection360AddOnWithPrice();
		changePlanAddonsBreakdownPage.verifyPlanChangeName("warning icon Plan change—items will be removed");
	}

	/*
	 * Test Plan Price - TestPriceInPlanReviewPageforTMobileONE
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testPriceAndNonConflictInPlanReviewPageforTMobileONE(ControlTestData data, MyTmoData myTmoData) {
		logger.info("TestPriceAndNonConflictInPlanReviewPageforTMobileONE");
		Reporter.log("Test Case : TestGetEligiblePlansforTMobileONEMilitary");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Plan link | New Plan page should be displayed");
		Reporter.log("5. Click on ChangePlan CTA |ExplorePage should be loaded");
		Reporter.log("6. Click on TMobile ONE |Navigated to plan Comparison Page");
		Reporter.log("7. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log("8. Click on Select Plan Cta |Plan Review Page Should be displayd");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnDigitsline();
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
		exploreplanpage.clickonTMobileONE();

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.clickOnSelectPlanCta();
		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());

		changePlansReviewPage.clickOnAddonsBlade();
		ChangePlanAddonsBreakdownPage changePlanAddonsBreakdownPage = new ChangePlanAddonsBreakdownPage(getDriver());
		changePlanAddonsBreakdownPage.verifyAddonsBreakdownPage();

		changePlanAddonsBreakdownPage.verifyAddOnsPlanName("T-Mobile ONE Plus Family");
		changePlanAddonsBreakdownPage.verifyProtection360AddOnWithPrice();
		changePlanAddonsBreakdownPage.verifyPlanChangeName("There will be no changes to your bill");
	}

	/*
	 * Test Plan Price - testPooledHybridPlanInReviewPage
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testPooledHybridPlanInReviewPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testPooledHybridPlanInReviewPage");
		Reporter.log("Test Case : TestGetEligiblePlansforTMobileONEMilitary");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnDigitsline();
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
		exploreplanpage.clickonTMobileONE();

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.clickOnSelectPlanCta();
		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());

		changePlansReviewPage.clickOnAddonsBlade();
		ChangePlanAddonsBreakdownPage changePlanAddonsBreakdownPage = new ChangePlanAddonsBreakdownPage(getDriver());
		changePlanAddonsBreakdownPage.verifyAddonsBreakdownPage();

		changePlanAddonsBreakdownPage.verifyAddOnsPlanName("T-Mobile ONE Plus Family");
		changePlanAddonsBreakdownPage.verifyProtection360AddOnWithPrice();
		changePlanAddonsBreakdownPage.verifyPlanChangeName("There will be no changes to your bill");
	}

	/*
	 * <<<<<<< HEAD US566560 : [Change Plan] Navigating back from Review Page to
	 * Plan Comparison ======= US566560 : [Change Plan] Navigating back from Review
	 * Page to Plan Comparison >>>>>>>
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void testNavigationBackfromReviewPagetoPlanComparionPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testNavigationBackfromReviewPagetoPlanComparionPage");
		Reporter.log("Test Case : testNavigationBackfromReviewPagetoPlanComparionPage");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Account | AccountOverview page should be displayed");
		Reporter.log("5. Click on Plan Name |PLan Page should be loaded");
		Reporter.log("6. Click on ChangePlan CTA |ExplorePage should be loaded");
		Reporter.log("7. Click on TMobile ONE |Navigated to plan Comparison Page");
		Reporter.log("8. Verify that selectPlan CTA on Choose Plan Section |selectPlan cta should be available");
		Reporter.log("9. Click on Select Plan Cta |Plan Review Page Should be displayd");
		Reporter.log("10.Click on Browser Back button |Plan Comparison Page Should be displayd");
		Reporter.log("11.Click on Select Plan Cta |Plan Review Page Should be displayd");
		Reporter.log("12.Click on Unav back Arrow |Plan Comparison Page Should be displayd");
		Reporter.log("13.Referesh the Page from Plan Comparison page |Explore Plan Page Should be displayd");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
		exploreplanpage.clickOnfeaturePlan();

		PlanComparisonPage newplancomparisionpage = new PlanComparisonPage(getDriver());
		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.clickOnSelectPlanCta();

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();
		changePlansReviewPage.checkHeaderOnPlansReviewPage();

		getDriver().navigate().back();

		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifythatSamePlanisPresented();
		newplancomparisionpage.verifySelectPlanCtabeowChoosePlan();
		newplancomparisionpage.clickOnSelectPlanCta();
		changePlansReviewPage.clickOnUNAVArrow();

		newplancomparisionpage.verifyComparePlan();
		newplancomparisionpage.verifyExplorePageafterRefersh();
	}

	/**
	 * @author prokarma
	 *
	 *         US489038 - Review page - Device cost blade
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "qqqq")
	public void verifyDevicesCostOnDevicesBladeOnPlansReviewPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPlanCostOnPlanBladeOnPlansReviewPage method called in ChangePlansReviewPage");
		Reporter.log(
				"Test Case : Verify cost on Devices blade. It should match with the cost on Devices breakdown page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full with having active EIP Devices");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Check cost on Device blade. | It should be matched with cost on Devices breakdown page.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();

		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlanReviewPage = new ChangePlansReviewPage(getDriver());
		changePlanReviewPage.checkTextDevicesOnAddonsBlade();
		String costOnPlansBlade = changePlanReviewPage.verifyCostOnDevicesBlade();
		System.out.println("Cost on Devices blade is " + costOnPlansBlade);

		/*
		 * int totalMonthlyCostOnPlansBreakdown =
		 * changePlanPlansBreakdownPage.verifyTotalMonthlyCost();
		 * if(costOnPlansBlade==totalMonthlyCostOnPlansBreakdown) Reporter.log(
		 * "Price on Plans Blade and Plans breakdown page is matched."); else
		 * Reporter.log(
		 * "Price on Plans Blade and Plans breakdown page is mismatched.");
		 */
	}

	/**
	 * @author prokarma
	 *
	 *         US489035 - Review page - Plan cost blade
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyDeltaCostOnPlansReviewPage(ControlTestData data, MyTmoData myTmoData)
			throws InterruptedException {
		logger.info("verifyDeltaCostOnPlansReviewPage method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify cost on Plans blade. It should match with the cost on plans breakdown page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Check Total cost on Account overview page. |Cost should be displayed");
		Reporter.log("6. Go to Plans Review page. | Plans review page should be displayed.");
		Reporter.log(
				"7. Check Delta cost. | Delta cost show be displayed with correct difference of Current plan and changing plan.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		double monthlyTotalCostOnAccountOverviewPage = navigateToPlanDetailsPageAndGetMonthlyTotalCostFromAccountOverviewPage(
				myTmoData);

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();
		exploreplanpage.navigateToPlanComparisionPage(planName);

		PlanComparisonPage planComparisonPage = new PlanComparisonPage(getDriver());
		planComparisonPage.verifyComparePlan();
		planComparisonPage.choosePlanFromChoosePlanSection(planName);
		planComparisonPage.clickOnSelectPlanCta();

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.verifyDeltaCostValueDifference(monthlyTotalCostOnAccountOverviewPage);
	}

	/**
	 * @author prokarma
	 *
	 *         US489011 - Review page - Plan header and name display
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyPlanHeaderAndPlanNameOnPlansReviewPageWhenUserChangePlanFromSCToTI(ControlTestData data,
			MyTmoData myTmoData) throws InterruptedException {
		logger.info(
				"verifyPlanHeaderAndPlanNameOnPlansReviewPageWhenUserChangePlanFromSCToTI method called in ChangePlansReviewPage");
		Reporter.log("Test Case : Verify Plan header,Plan name and subtext and new Plan image on Plans Review page");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Check Header,Subheader and Promotional text. | It should be displayed.");
		Reporter.log("7. Check Image of New Plan. | It should be displayed.");
		Reporter.log("8. Check New Plan name. | It should be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlanReviewPage = new ChangePlansReviewPage(getDriver());
		changePlanReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlanReviewPage.verifyChangePlansReviewPage();

		changePlanReviewPage.checkHeaderOnPlansReviewPage();
		changePlanReviewPage.checkSubHeaderOnPlansReviewPage();
		changePlanReviewPage.checkPromotionalTextOnPlansReviewPage();
		changePlanReviewPage.checkTextNewPlanOnPlansReviewPage();
		changePlanReviewPage.checkImageOfNewPlanOnPlansReviewPage();
	}

	/**
	 * @author prokarma
	 *
	 *         US517907 - Plan change warning modal - Trigger modal load and content
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyWarningModalDuringMigrationScenarioWhenMBBPlanIsNotCompatible(ControlTestData data,
			MyTmoData myTmoData) throws InterruptedException {
		logger.info(
				"verifyWarningModalDuringMigrationScenarioWhenMBBPlanIsNotCompatible method called in ChangePlansReviewPage");
		Reporter.log(
				"Test Case : Verify whether user gets warning modal or not during migration scenario when MBB is not compatible");
		Reporter.log("Test Data : Any MSDSIN PAH/Full and on TE plan with MBB line on TE plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Check Warning Modal for MBB. | Warning modal should be displayed.");
		Reporter.log("7. Check icon of Warning Modal. | Icon should be displayed.");
		Reporter.log("8. Check header of Modal. | Header 'Plan update' should be displayed.");
		Reporter.log(
				"9. Check authorable copy. | Copy 'Your current Mobile Internet plan isn't compatible with this new plan, but we'll select the best available plan for you. Please review this change by tapping 'Plans' in the next step.' should be displayed.");
		Reporter.log("10. Click Continue CTA. | Plans Review page should be displayed.");
		Reporter.log("11. Check warning icon on Plans blade. | Warning icon should be displayed.");
		Reporter.log("12. Check 'Tap to review changes' text. | Text should be displayed.");
		Reporter.log("13. Click on 'Plans blade'. | Plans Breakdown page should be displayed.");
		Reporter.log("14. Check MBB incompatible message. | Message should be displayed.");
		Reporter.log("15. Check Back CTA. | Back CTA should be displayed.");
		Reporter.log("16. Check Continue CTA. | Continue CTA should be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		navigateToPlanReviewPageTest(myTmoData, planName);

		ChangePlansReviewPage changePlanReviewPage = new ChangePlansReviewPage(getDriver());
		changePlanReviewPage.checkWarningModalWhenMBBIsIncompatibleAndGSMStartsChangePlan();
		changePlanReviewPage.verifyChangePlansReviewPage();
	}

	/**
	 * @author prokarma
	 *
	 *         US517907 - Plan change warning modal - Trigger modal load and content
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = Group.RETIRED)
	public void verifyWarningModalWhenMBBStartsChangePlan(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyWarningModalWhenMBBStartsChangePlan method called in ChangePlansReviewPage");
		Reporter.log(
				"Test Case : Verify whether user gets warning modal or not when MBB line start change planand and not compatible");
		Reporter.log("Test Data : Any MSDSIN PAH/Full and on TE plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Check Warning Modal for MBB. | Warning modal should not be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		navigateToPlansReviewPage(myTmoData);
		ChangePlansReviewPage changePlanReviewPage = new ChangePlansReviewPage(getDriver());
		changePlanReviewPage.iconNotDisplayedOnWarningModal();
	}

	/**
	 * @author prokarma
	 *
	 *         US553870 - Review page - Display warning icon for Plans blade in
	 *         migration scenario only where we default MBB plan
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = Group.RETIRED)
	public void verifyWarningIconOnPlansBladelWhenMBBStartsChangePlanAndMBBIsNotCompatible(ControlTestData data,
			MyTmoData myTmoData) throws InterruptedException {
		logger.info(
				"verifyWarningIconOnPlansBladelWhenMBBStartsChangePlanAndMBBIsNotCompatible method called in ChangePlansReviewPage");
		Reporter.log(
				"Test Case : Verify whether user gets warning icon on plans blade or not when MBB starts change plan and MBB is compatible");
		Reporter.log("Test Data : Any MSDSIN PAH/Full and on TE plan and MBB also on TE plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Review Page Should be displayed");
		Reporter.log("6. Check Warning Modal for MBB. | Warning modal should be displayed.");
		Reporter.log("7. Click Continue CTA. | Plans Review page should be displayed.");
		Reporter.log("8. Check warning icon on Plans blade. | Warning icon should not be displayed.");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		String planName = myTmoData.getplanname().trim();
		System.out.println("Passed plan name is " + planName);

		navigateToPlanReviewPageTest(myTmoData, "");

		ChangePlansReviewPage changePlanReviewPage = new ChangePlansReviewPage(getDriver());
		changePlanReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlanReviewPage.verifyChangePlansReviewPage();

		changePlanReviewPage.checkWarningImageOnPlansBladeOnPlansReviewPage();
	}
}