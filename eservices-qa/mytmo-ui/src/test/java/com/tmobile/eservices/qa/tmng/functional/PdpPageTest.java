package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.CommonLibrary;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPManualorFraudReviewPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPNoOfferPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPPreScreenForm;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPPreScreenIntro;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class PdpPageTest extends TmngCommonLib {

	/**
	 * C347314: TMO- verify Sub navigations in Main PDP US351402:- [Continued] TMO -
	 * IV - PDP/mini Container Box C443270 C347315
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifySubNavigationsInPDP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : TMO- verify SIM Starter price and text in PDP Phone");
		Reporter.log("US351402:- [Continued] TMO - IV - PDP/mini Container Box");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. Click on any device| Device PDP page should be displayed");
		Reporter.log("4. Verify Add to Cart button | Add to Cart button should be displayed in PDP page");
		Reporter.log("5. Verify Schedule Appointment | Schedule Appointment should be displayed in PDP page");
		Reporter.log("6. Verify Features Tab in PDP | Features Tab should be displayed on PDP");
		Reporter.log("7. Verify Reviewes Tab in PDP | Reviewes Tab should be displayed on PDP");
		Reporter.log("8. Click Features Tab in PDP | Should Navigate to features tab");
		Reporter.log("9. Click Reviewes Tab in PDP | Should Navigate to reviewes tab");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.verifyAddToCartCTA();
		phonesPdpPage.verifyLegalTextAtFooter();
		phonesPdpPage.verifySpecificationsTab();
		phonesPdpPage.verifyReviewsTab();
		phonesPdpPage.clickSpecificationsTabAndVerifyNavigation();
		phonesPdpPage.clickReviewsTabAndVerifyNavigation();

	}

	/**
	 * US330326:- TEST ONLY: TMO Additional Terms - Main PDP - @Work English Site -
	 * Display EIP Pricing (Primary Area) C478542 : Validate the Product Grid is
	 * displayed correctly with Product Image/Name/Colors and Reviews
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testPhoneEIPLoanTermLegthInPDPPage(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US330326:- TEST ONLY: TMO Additional Terms - Main PDP - "
				+ "@Work English Site - Display EIP Pricing (Primary Area)");
		Reporter.log(
				"Test Case: C478542 : Validate the Product Grid is displayed correctly with Product Image/Name/Colors and Reviews");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select Device | PDP page should be displayed");
		Reporter.log("4. Verify Monthly payment | Monthly payment should be displayed");
		Reporter.log("5. Verify EIP loan term length | EIP loan term length should be displayed");
		Reporter.log("6. Verify Color varient for device | Color varient for device should be displayed");
		Reporter.log("7. Verify Device description | Device description should be displayed");
		Reporter.log("8. Verify Memory varient | Memory varient should be displayed");
		Reporter.log("9. Save FullRetail amount in V1 | FullRetail amount should be saved in V1");
		Reporter.log("10. Save DownPayment amount in V2 | DownPayment amount should be saved in V2");
		Reporter.log(
				"11. Calculate (Installment amount * Loan term length) and Save in V3 | Calculated (Installment amount * Loan term length) should be saved in V3 ");
		Reporter.log("12. Calculate (V3 + V2) and Save V4 | Calculated (V3 + V2) should be saved in V4");
		Reporter.log("13. Verify (V1 = V4) amounts are equal | (V1 = V4) amounts should be equal");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.verifyEIPPayment();
		phonesPdpPage.verifyLoanTermLength();
		phonesPdpPage.verifyColorSwatches();
		phonesPdpPage.verifyMemoryVariant();

		Double v1 = Double.parseDouble(phonesPdpPage.getDeviceFRP());
		Double v2 = Double.parseDouble(phonesPdpPage.getDownPaymentPrice());
		Double V3 = phonesPdpPage.calculateTotalMonthlyEIPValue(v1, v2);
		Double V4 = Double.parseDouble(phonesPdpPage.getEIPPaymentPrice());
		phonesPdpPage.compareEIPValues(V4, V3);
	}

	/**
	 * US481689 TMO - IV Accessories Accessibility - Main PDP - In Stock US481690
	 * TMO - IV Accessories Accessibility - Main PDP - Hurry, Only a few left
	 * US329614:- [Continued] [Continued] TMO - IV - PDP/mini Inventory Availability
	 * C443269 US330326:- TEST ONLY: TMO Additional Terms - Main PDP - @Work English
	 * Site - Display EIP Pricing (Primary Area)
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testIVAccessoriesAccessibilityOnPDP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US481689	TMO - IV Accessories Accessibility - Main PDP - In Stock");
		Reporter.log("Test Case : US481690	TMO - IV Accessories Accessibility - Main PDP - Hurry, Only a few left");
		Reporter.log("Test Case : US329614:- [Continued] [Continued] TMO - IV - PDP/mini Inventory Availability");
		Reporter.log("Test Case : US329614:- [Continued] [Continued] TMO - IV - PDP/mini Inventory Availability");
		Reporter.log("Test Case : US330326:- TEST ONLY: TMO Additional Terms - Main PDP - "
				+ "@Work English Site - Display EIP Pricing (Primary Area)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessories should be displayed");
		Reporter.log("4. Select any Accessory | Accessory PDP Page should be selected");
		Reporter.log("5. Verify Monthly payment | Monthly payment should be displayed");
		Reporter.log("6. Verify EIP loan term length | EIP loan term length should be displayed");
		Reporter.log(
				"7. Verify Color varient for accessory device | Color varient for accessory device should be displayed");
		Reporter.log("8. Verify Memory varient | Memory varient should be displayed");
		Reporter.log("9. Save FullRetail amount in V1 | FullRetail amount should be saved in V1");
		Reporter.log("10. Save DownPayment amount in V2 | DownPayment amount should be saved in V2");
		Reporter.log(
				"11. Calculate (Installment amount * Loan term length) and Save in V3 | Calculated (Installment amount * Loan term length) should be saved in V3 ");
		Reporter.log("12. Calculate (V3 + V2) and Save V4 | Calculated (V3 + V2) should be saved in V4");
		Reporter.log("13. Verify (V1 = V4) amounts are equal | (V1 = V4) amounts should be equal");
		Reporter.log(
				"14. Verify Inventory status on PDP |Product Inventory Hurry, Only a few left or  In Srock should be displayed");
		Reporter.log(
				"15. Verify Skip the wait. Make an appointment header | Skip the wait. Make an appointment header should be displayed");
		Reporter.log("16. Verify Schedule appointment CTA| Schedule appointment CTA should be displayed");
		Reporter.log("17. Verify Store name on Inventory container | Store Name  should be displayed");
		Reporter.log("18. Verify Find nearby stores CTA | Find Nearby stores CTA should be displayed");
		Reporter.log("19. Verify dinstance on inventory container | Distance in miles should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToAccessoryMainPDP(tMNGData);
		PdpPage accessoryDetailsPage = new PdpPage(getDriver());
		accessoryDetailsPage.verifyEIPPayment();
		Double V1 = Double.parseDouble(accessoryDetailsPage.getAccessoryDeviceFRP());
		Double V2 = Double.parseDouble(accessoryDetailsPage.getDownPaymentPrice());
		Double V3 = accessoryDetailsPage.calculateTotalMonthlyEIPValue(V1, V2);
		Double V4 = Double.parseDouble(accessoryDetailsPage.getEIPPaymentPrice());
		accessoryDetailsPage.compareEIPValues(V3, V4);
		accessoryDetailsPage.verifyStoreLocatorName();
		accessoryDetailsPage.verifyInventoryStatus();
		accessoryDetailsPage.verifyOnlineStock();
		accessoryDetailsPage.verifyScheduleInStoreAppointment();
		// accessoryDetailsPage.verifyCheckOtherLocationLink();
		accessoryDetailsPage.verifyDistanceInInventoryContainer();

	}

	/**
	 * C347311: TMO- verify Bring Your Own Phone/ MI device - PDP Tablet
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testVerifyBYODForTablet(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : TMO- Verify BYOD For Tablet");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. click on Tablets and devices tab |Tablets and devices page should be displayed");
		Reporter.log(
				"4. Click on T-Mobile® 3-in-1 Mobile Internet SIM Kit| T-Mobile® 3-in-1 Mobile Internet SIM Kit PDP page should be displayed");
		Reporter.log("Actual Output:");

		PlpPage phonesPage = navigateToPhonesPlpPage(tMNGData);
		phonesPage.clickOnTabletsAndDevicesLink();
		phonesPage.clickDeviceWithAvailability(tMNGData.getDeviceName());

		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyInternetSimKitTitle();
	}

	/**
	 * TMNG: Review link navigation in Phones PDP C347311: TMO- verify SIM Starter
	 * price and text in PDP Phone C494412
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testReviewLinkNavigationInPhonePDPPage(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Review link navigation in Phones PDP");
		Reporter.log("C347311: TMO- verify SIM Starter price and text in PDP Phone");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select Device | PDP page should be displayed");
		Reporter.log(
				"4. Verify SIM Starter kit info in PDP | Ensure the '+ tax + SIM starter kit' is displayed on PHONES/WATCHES/TABLETS for EIP/FRP pricing");
		Reporter.log("5. Click Review link On PDP | Page should navigate to review section of PDP");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonePdp = navigateToPhonesPDP(tMNGData);
		phonePdp.verifyPlusTaxPlusSimStarterKitIsDisplayed();
		phonePdp.clickReviewLinkOnPDP();
		phonePdp.verifyReviewsSectionOnPDP();
	}

	/**
	 * TMNG: Review link navigation in Tablet PDP C347311: TMO- verify SIM Starter
	 * price and text in PDP Phone C494412
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testReviewLinkNavigationInTabletPDPPage(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Review link navigation in Phones PDP");
		Reporter.log("C347311: TMO- verify SIM Starter price and text in PDP Phone");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Tablets Link | Tablets plp page should be displayed");
		Reporter.log("3. Select Device | PDP page should be displayed");
		Reporter.log("4. Verify Memory Varients Availability | Memory Varients should be displayed");
		Reporter.log(
				"5. Verify Memory Varients Order | Order should be in left to right order ex: 64GB | 256GB | 512GB | 1TB");
		Reporter.log(
				"6. Verify SIM Starter kit info in PDP | Ensure the '+ tax + SIM starter kit' is displayed on PHONES/WATCHES/TABLETS for EIP/FRP pricing");
		Reporter.log("7. Click Review link On PDP | Page should navigate to review section of PDP");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonePdp = navigateToTabletsAndDevicesPDP(tMNGData);
		phonePdp.verifyMemoryVariant();
		phonePdp.verifyMemoryVariantsOrder();
		phonePdp.verifyPlusTaxPlusSimStarterKitIsDisplayed();
		phonePdp.clickReviewLinkOnPDP();
		phonePdp.verifyReviewsSectionOnPDP();
	}

	/**
	 * ` TMNG: Review link navigation in Watch PDP C347311: TMO- verify SIM Starter
	 * price and text in PDP Phone C494412
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testReviewLinkNavigationInWatchPDPPage(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Review link navigation in Phones PDP");
		Reporter.log("C347311: TMO- verify SIM Starter price and text in PDP Phone");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select Device | PDP page should be displayed");
		Reporter.log(
				"4. Verify SIM Starter kit info in PDP | Ensure the '+ tax + SIM starter kit' is displayed on PHONES/WATCHES/TABLETS for EIP/FRP pricing");
		Reporter.log("5. Click Review link On PDP | Page should navigate to review section of PDP");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonePdp = navigateToWatchesPDP(tMNGData);
		phonePdp.verifyPlusTaxPlusSimStarterKitIsDisplayed();
		phonePdp.clickReviewLinkOnPDP();
		phonePdp.verifyReviewsSectionOnPDP();
	}

	/**
	 * US385695: TMNG Main PLP Migration to V2 - Product Grid: Product Review
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyProductReviewStarImgAndCTAOnInternetDevicesMainPLP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US385691: TMNG Main PLP Migration to V2 - Product Grid: Product Name");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Tablet&Devices | Internet Devices Main PLP should be displayed");
		Reporter.log("4. Verify Review Star image | Review Star image should be displayed for all the devices.");
		Reporter.log("5.Click on Review Star image | the product's Main PDP Reviews section should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToTabletsAndDevicesPLP(tMNGData);
		PlpPage tabletsPage = new PlpPage(getDriver());
		tabletsPage.verifyAllRatingStarsImage();
		tabletsPage.clickOnRatingStarsImage();
		PdpPage tabletsDetailsPage = new PdpPage(getDriver());
		tabletsDetailsPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletsDetailsPage.clickReviewsTabAndVerifyNavigation();
		tabletsDetailsPage.verifyReviewSectionIsDisplayed();
	}

	/**
	 * US385695: TMNG Main PLP Migration to V2 - Product Grid: Product Review
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyProductReviewCountAndCTAOnTabletsAndDevicesMainPLP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US385691: TMNG Main PLP Migration to V2 - Product Grid: Product Name");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Tablet&Devices | Internet Devices Main PLP should be displayed");
		Reporter.log("4. Verify Reviews count | Review count should be displayed for all the devices");
		Reporter.log("5. Click on Reviews Count | the product's Main PDP Reviews section should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToTabletsAndDevicesPLP(tMNGData);
		PlpPage tabletsPage = new PlpPage(getDriver());
		tabletsPage.verifyTabletsAndDevicesPlpPageLoaded();
		tabletsPage.verifyAllRatingCount();
		tabletsPage.clickOnRatingCount();
		PdpPage tabletsDetailsPage = new PdpPage(getDriver());
		tabletsDetailsPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletsDetailsPage.verifyReviewSectionIsDisplayed();
	}

	/**
	 * US330326:- TEST ONLY: TMO Additional Terms - Main PDP - @Work English Site -
	 * Display EIP Pricing (Primary Area)
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testInternetDeviceEIPLoanTermLegthInPDPPage(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US330326:- TEST ONLY: TMO Additional Terms - Main PDP - "
				+ "@Work English Site - Display EIP Pricing (Primary Area)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | Application Should be Launched");
		Reporter.log("2. Click on Phone Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Tablets &Devices Link | Internet-devices should be displayed");
		Reporter.log("4. Select Internet Device | PDP page should be displayed");
		Reporter.log("5. Verify Monthly payment | Monthly payment should be displayed");
		Reporter.log("6. Verify EIP loan term length | EIP loan term length should be displayed");
		Reporter.log(
				"7. Verify Color varient for accessory device | Color varient for accessory device should be displayed");
		Reporter.log("8. Verify Memory varient | Memory varient should be displayed");
		Reporter.log("9. Save FullRetail amount in V1 | FullRetail amount should be saved in V1");
		Reporter.log("10. Save DownPayment amount in V2 | DownPayment amount should be saved in V2");
		Reporter.log(
				"11. Calculate (Installment amount * Loan term length) and Save in V3 | Calculated (Installment amount * Loan term length) should be saved in V3 ");
		Reporter.log("12. Calculate (V3 + V2) and Save V4 | Calculated (V3 + V2) should be saved in V4");
		Reporter.log("13. Verify (V1 = V4) amounts are equal | (V1 = V4) amounts should be equal");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage tabletsAndDevicesPdpPage = navigateToTabletsAndDevicesPDP(tMNGData);

		tabletsAndDevicesPdpPage.verifyEIPPayment();
		tabletsAndDevicesPdpPage.verifyLoanTermLength();
		tabletsAndDevicesPdpPage.verifyColorSwatches();
		tabletsAndDevicesPdpPage.verifyMemoryVariant();

		Double v1 = Double.parseDouble(tabletsAndDevicesPdpPage.getDeviceFRP());
		Double v2 = Double.parseDouble(tabletsAndDevicesPdpPage.getDownPaymentPrice());
		Double V3 = tabletsAndDevicesPdpPage.calculateTotalMonthlyEIPValue(v1, v2);
		Double V4 = Double.parseDouble(tabletsAndDevicesPdpPage.getEIPPaymentPrice());
		tabletsAndDevicesPdpPage.compareEIPValues(V4, V3);
	}

	/**
	 * US576909: As a Marketing manager, I want to be able to present romance copy
	 * on PDP, so customers have additional information about the product benefits.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testRomanceCopyOnTabletsPDPPage(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Romance Copy should be displayed in PDP Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select Tablets and Devices |Tablets and Devices should be displayed");
		Reporter.log("4. Verify Romance Copy on PDP | Romance Copy should be displayed of PDP");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage tabletsPdp = navigateToTabletsAndDevicesPDP(tMNGData);
		tabletsPdp.verifyRomanceCopyOnPDP();
	}

	/**
	 * US576909: As a Marketing manager, I want to be able to present romance copy
	 * on PDP, so customers have additional information about the product benefits.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testRomanceCopyOnWatchesPDPPage(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Romance Copy should be displayed in PDP Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select Watches | Watches page should be displayed");
		Reporter.log("4. Verify Romance Copy on PDP | Romance Copy should be displayed of PDP");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage watchPdp = navigateToWatchesPDP(tMNGData);
		watchPdp.verifyRomanceCopyOnPDP();

	}

	/**
	 * US576909: As a Marketing manager, I want to be able to present romance copy
	 * on PDP, so customers have additional information about the product benefits.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testRomanceCopyOnPDPPageForCookiedUser(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Romance Copy should be displayed in PDP Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Phones Link | UNO Cell Phones page should be displayed");
		Reporter.log("5. Select Device | Device PDP page should be displayed");
		Reporter.log("6. Verify Romance Copy on PDP | Romance Copy should be displayed of PDP");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToMyTMOHome(tMNGData);
		PdpPage phonesPdp = navigateToPhonesPDP(tMNGData);
		phonesPdp.verifyRomanceCopyOnPDP();

	}

	/**
	 * US578342 As a Product manager, I want to be able to supress the unavailable
	 * fields for Coming soon products on PDP, so that customers can see relevant
	 * information. * @param data
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TPP })
	public void testComingSoonStatusBehaviourInPhonesPdp(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Test Test Coming Soon Status Behaviour In Phones Pdp ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO URL  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | UNO Cell Phones page should be displayed");
		Reporter.log(
				"3. Select Device With Coming Soon Inventory Status | Coming Soon On PDP page should be displayed");
		Reporter.log(
				"4. Verify EIP and FRP Prices should not be Displayed | EIP and FRP Prices should not be Displayed");
		Reporter.log("5. Verify Offers should not be Displayed | Offers should not be Displayed ");
		Reporter.log("6. Verify Color Swatches should not be Displayed | Color Swatches should not be Displayed ");
		Reporter.log("7. Verify Memory variant should not be Displayed | Memory variant should not be Displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonesPDP(tMNGData);
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyDeviceInventoryStatus("Coming Soon");
		phonesPdpPage.verifyEIPPrice();
		phonesPdpPage.verifyFRPPrice();
		phonesPdpPage.verifyOffers();
		phonesPdpPage.verifyColorSwatches();
		phonesPdpPage.verifyMemoryVariant();
	}

	/**
	 * US578342 As a Product manager, I want to be able to supress the unavailable
	 * fields for Coming soon products on PDP, so that customers can see relevant
	 * information. * @param data
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TPP })
	public void testComingSoonStatusBehaviourInTabletsPdp(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Test Coming Soon Status Behaviour In Tablets Pdp ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO URL  | Application Should be Launched");
		Reporter.log("2. Click on Tablets Link | UNO Tablets page should be displayed");
		Reporter.log(
				"3. Select Device With Coming Soon Inventory Status | Coming Soon On PDP page should be displayed");
		Reporter.log(
				"4. Verify EIP and FRP Prices should not be Displayed | EIP and FRP Prices should not be Displayed");
		Reporter.log("5. Verify Offers should not be Displayed | Offers should not be Displayed ");
		Reporter.log("6. Verify Color Swatches should not be Displayed | Color Swatches should not be Displayed ");
		Reporter.log("7. Verify Memory variant should not be Displayed | Memory variant should not be Displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToTabletsAndDevicesPDP(tMNGData);
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyDeviceInventoryStatus("Coming Soon");
		phonesPdpPage.verifyEIPPrice();
		phonesPdpPage.verifyFRPPrice();
		phonesPdpPage.verifyOffers();
		phonesPdpPage.verifyColorSwatches();
		phonesPdpPage.verifyMemoryVariant();

	}

	/**
	 * US578342 As a Product manager, I want to be able to supress the unavailable
	 * fields for Coming soon products on PDP, so that customers can see relevant
	 * information. * @param data
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TPP })
	public void testComingSoonStatusBehaviourInWatchesPdp(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Test Coming Soon Status Behaviour In Watches Pdp ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO URL  | Application Should be Launched");
		Reporter.log("2. Click on Watches Link | Watches page should be displayed");
		Reporter.log(
				"3. Select Device With Coming Soon Inventory Status | Coming Soon On PDP page should be displayed");
		Reporter.log(
				"4. Verify EIP and FRP Prices should not be Displayed | EIP and FRP Prices should not be Displayed");
		Reporter.log("5. Verify Offers should not be Displayed | Offers should not be Displayed ");
		Reporter.log("6. Verify Color Swatches should not be Displayed | Color Swatches should not be Displayed ");
		Reporter.log("7. Verify Memory variant should not be Displayed | Memory variant should not be Displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToWatchesPDP(tMNGData);
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyDeviceInventoryStatus("Coming Soon");
		phonesPdpPage.verifyEIPPrice();
		phonesPdpPage.verifyFRPPrice();
		phonesPdpPage.verifyOffers();
		phonesPdpPage.verifyColorSwatches();
		phonesPdpPage.verifyMemoryVariant();

	}

	/**
	 * US578342 As a Product manager, I want to be able to supress the unavailable
	 * fields for Coming soon products on PDP, so that customers can see relevant
	 * information. * @param data
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TPP })
	public void testComingSoonStatusBehaviourInPhonesPdpForCookiedCustomer(ControlTestData data, MyTmoData myTmoData,
			TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Test Coming Soon Status Behaviour In Phones Pdp ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Phones Link | Phones page should be displayed");
		Reporter.log(
				"5. Select Device With Coming Soon Inventory Status | Coming Soon On PDP page should be displayed");
		Reporter.log(
				"6. Verify EIP and FRP Prices should not be Displayed | EIP and FRP Prices should not be Displayed");
		Reporter.log("7. Verify Offers should not be Displayed | Offers should not be Displayed ");
		Reporter.log("7. Verify Color Swatches should not be Displayed | Color Swatches should not be Displayed ");
		Reporter.log("7. Verify Memory variant should not be Displayed | Memory variant should not be Displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		CommonLibrary commonLibrary = new CommonLibrary();
		commonLibrary.navigateToHomePage(myTmoData);
		navigateToTabletsAndDevicesPDP(tMNGData);
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyDeviceInventoryStatus("Coming Soon");
		phonesPdpPage.verifyEIPPrice();
		phonesPdpPage.verifyFRPPrice();
		phonesPdpPage.verifyOffers();
		phonesPdpPage.verifyColorSwatches();
		phonesPdpPage.verifyMemoryVariant();
	}

	/**
	 * US575533:TPP - Prescreen - Display Prescreen form page C543888 - PDP:
	 * Validate "skip for now "CTA on the Pre Screen Form intro Page
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testSkipForNowCTAOnPreScreenFormIntroPageFromPDP(TMNGData tMNGData) {
		Reporter.log("US575533:TPP - Prescreen - Display Prescreen form page");
		Reporter.log("C543888 - PDP: Validate 'skip for now' CTA on the Pre Screen Form intro Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log(
				"3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing"
						+ " should be displayed on PLP ");
		Reporter.log("4. Select any device from PLP page | PDP page should be displayed");
		Reporter.log("5. Verify 'Find your pricing' link on PDP page | 'Find your pricing' link should be displayed");
		Reporter.log("6. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("7. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"8. Click on 'Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log(
				"9. Verify 'Skip for now' cta presence on Pre-screen form | 'Skip for now' cta should be displayed");
		Reporter.log("10. Click on 'Skip for now' cta | User should be redirected to previous entry point page (PDP)");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdp = navigateToPhonesPDP(tMNGData);
		phonesPdp.verifyCRPorAwesomePricesInPDP();
		phonesPdp.verifyYourPricingBoxInPDP();
		phonesPdp.verifyYourPricingBoxTextInPDP();
		phonesPdp.verifyFindYourPricingLinkInPDP();
		phonesPdp.clickFindYourPricingLinkInPDP();

		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();

		TPPPreScreenForm tppPrescreenform = new TPPPreScreenForm(getDriver());
		tppPrescreenform.verifyPreScreenForm();
		tppPrescreenform.verifySkipForNowCta();
		tppPrescreenform.clickSkipForNowCta();

		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();

	}

	/**
	 * US565403:TPP - Prescreen- Load manual review/fraud page (CART/PLP/PDP)
	 * C543909 - PDP:Verify Find my price CTA - Manual review Page
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TPP })
	public void verifyManualReviewPageForFindMyPriceCTAFromPDP(TMNGData tMNGData) {
		Reporter.log("US565403:TPP - Prescreen- Load manual review/fraud page (CART/PLP/PDP)");
		Reporter.log("C543909 - PDP:Verify Find my price CTA - Manual review Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log(
				"3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing"
						+ " should be displayed on PLP ");
		Reporter.log(
				"4. Select any device and Verify 'Find your pricing' link on PDP | 'Find your pricing' link should be displayed in PDP");
		Reporter.log("5. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("6. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"7. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("8. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("9. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("10. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log("11. Verify pre screen page after loading page | Manual review should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		for (int i = 0; i < 7; i++) {
			PdpPage phonesPdp = navigateToPhonesPDP(tMNGData);
			phonesPdp.verifyCRPorAwesomePricesInPDP();
			phonesPdp.verifyYourPricingBoxInPDP();
			phonesPdp.verifyYourPricingBoxTextInPDP();
			phonesPdp.clickFindYourPricingLinkInPDP();

			TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
			tppPreScreenIntro.verifyPreScreenIntroPage();
			tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();

			TPPPreScreenForm tppPrescreenform = new TPPPreScreenForm(getDriver());
			tppPrescreenform.verifyPreScreenForm();
			tppPrescreenform.verifyPreScreenFormPageURL();
			tppPrescreenform.verifyPreScreenFormPageTitle();
			tppPrescreenform.fillAllFieldsOnPreScreenform(tMNGData);
			tppPrescreenform.clickFindMyPriceCTAOnPreScreenForm();
			tppPrescreenform.verifyPreScreenloadingPage();
			getDriver().close();
		}

		TPPManualorFraudReviewPage tppManualorFraudReviewPage = new TPPManualorFraudReviewPage(getDriver());
		tppManualorFraudReviewPage.verifyManualRevieworFraudDetectionPage();
	}

	/**
	 * US585763:TPP - Suppress TPP experience for customers viewing FRP ONLY items
	 * on PDP C543842 - Verify for FRP only products TPP experience should be
	 * suppressed. Verify on Mobile
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void verifyTPPExpereinceForFRPProductsFromPDP(TMNGData tMNGData) {
		Reporter.log("US585763:TPP - Suppress TPP experience for customers viewing FRP ONLY items on PDP");
		Reporter.log("C543842 - Verify for FRP only products TPP experience should be suppressed.");
		Reporter.log("Verify on Mobile");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Select any device/Simstarter kit with FRP | SimStarter kit/deivce PDP should be displayed");
		Reporter.log(
				"4. Verify presence of 'verify your pricing' link | 'verify your pricing' link should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdp = navigateToPhonesPDP(tMNGData);
		phonesPdp.verifyFindYourPricingLinkInPDP();
		phonesPdp.clickPayInFullPaymentOption();
		phonesPdp.verifyFRPPriceAfterClickingOnPayInFull();
		phonesPdp.verifyFindYourPricingDisable();
	}

	/**
	 * US585761 : TPP - Display Base Customer experience on PDP for customers who
	 * are cookied as a MyTMO customer (logged in or not logged in) C547718 : ON PDP
	 * , verify TPP is suppressed for cookied and My Tmo customers. US496996 : As a
	 * cookied/authenticated user, I'd like to see pricing based on my CRP on PDP,
	 * so that I get the most accurate pricing. C501297 Cookied User: Inventory
	 * Status for Phones, Tablets, and Watches, Display 'UPGRADED and ADD TO A NEW
	 * LINE CTAs for Available Status US576909: As a Marketing manager, I want to be
	 * able to present romance copy on PDP, so customers have additional information
	 * about the product benefits.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testVerifyCookedUserTPPinPDP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"US585761 : TPP - Display Base Customer experience on PDP for customers who are cookied as a MyTMO customer (logged in or not logged in)");
		Reporter.log(
				"US496996 : As a cookied/authenticated user, I'd like to see pricing based on my CRP on PDP, so that I get the most accurate pricing.");
		Reporter.log(
				"Test  C501297	Cookied User: Inventory Status for Phones, Tablets, and Watches, Display 'UPGRADED and ADD TO A NEW LINE CTAs for Available Status");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Phones Link and select any phone| Phones PDP page should be displayed");
		Reporter.log("5. Verify Inventory container | Inventory should be dispayed");
		Reporter.log("6. Verify UPGRADE and Add aline CTA | UPGRADE and Add aline CTA should be displayed");
		Reporter.log("7. Navigate to Accessories Page | Accessories page should be displayed");
		Reporter.log("8. Select an accessory | Accessory PDP page should be displayed");
		Reporter.log("9. Verify AddToCart CTA | Add To Cart CTA should be displayed");
		Reporter.log("10. Verify Romance Copy on PDP | Romance Copy should be displayed of PDP");
		Reporter.log(
				"11. Verify 'Check your price' in PDP | 'Check your price' should not be displayed for cooked user.");
		Reporter.log(
				"12. Verify EIP downpayment, monthly payment and loan term length that corresponds to customers CRP in PDP | EIP downpayment, monthly payment and loan term length that corresponds to customers CRP should be displayed for cooked user.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToMyTMOHome(tMNGData);
		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.verifyDistanceInInventoryContainer();
		phonesPdpPage.verifyStoreLocatorName();
		phonesPdpPage.verifyInventoryStatus();
		phonesPdpPage.verifyUpgradeCTA();
		phonesPdpPage.verifyAddaLineCTA();
		phonesPdpPage.verifyRomanceCopyOnPDP();

		phonesPdpPage.verifyFindYourPricingDisable();
		phonesPdpPage.verifyEIPPayment();
		phonesPdpPage.verifyLoanTermLength();
		phonesPdpPage.verifyFRPPrice();

		phonesPdpPage.verifyDownPaymentPrice();

	}

	/**
	 * US585761 : TPP - Display Base Customer experience on PDP for customers who
	 * are cookied as a MyTMO customer (logged in or not logged in) C547718 : ON PDP
	 * , verify TPP is suppressed for cookied and My Tmo customers. US496996 : As a
	 * cookied/authenticated user, I'd like to see pricing based on my CRP on PDP,
	 * so that I get the most accurate pricing.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testVerifyCookedUserTPPinWatchPDP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"US585761 : TPP - Display Base Customer experience on PDP for customers who are cookied as a MyTMO customer (logged in or not logged in)");
		Reporter.log(
				"US496996 : As a cookied/authenticated user, I'd like to see pricing based on my CRP on PDP, so that I get the most accurate pricing.");

		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Watches Link and select any watch| Watch PDP page should be displayed");
		Reporter.log(
				"5. Verify 'Check your price' in PDP | 'Check your price' should not be displayed for cooked user.");
		Reporter.log(
				"6. Verify EIP downpayment, monthly payment and loan term length that corresponds to customers CRP in PDP | EIP downpayment, monthly payment and loan term length that corresponds to customers CRP should be displayed for cooked user.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToMyTMOHome(tMNGData);
		PdpPage phonesPdpPage = navigateToWatchesPDP(tMNGData);
		phonesPdpPage.verifyFindYourPricingDisable();

		phonesPdpPage.verifyEIPPayment();
		phonesPdpPage.verifyLoanTermLength();
		phonesPdpPage.verifyFRPPrice();

		phonesPdpPage.verifyDownPaymentPrice();

	}

	/**
	 * US585761 : TPP - Display Base Customer experience on PDP for customers who
	 * are cookied as a MyTMO customer (logged in or not logged in) C547718 : ON PDP
	 * , verify TPP is suppressed for cookied and My Tmo customers. US496996 : As a
	 * cookied/authenticated user, I'd like to see pricing based on my CRP on PDP,
	 * so that I get the most accurate pricing. TA1682649: Re-order Placement of
	 * Memory Variants: Apple iPad Pro 11-in & 12.9-in 3rd Gen
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testVerifyCookedUserTPPinTabletPDP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"US585761 : TPP - Display Base Customer experience on PDP for customers who are cookied as a MyTMO customer (logged in or not logged in)");
		Reporter.log(
				"US496996 : As a cookied/authenticated user, I'd like to see pricing based on my CRP on PDP, so that I get the most accurate pricing.");

		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Tablet Link and select any Tablet| Tablet PDP page should be displayed");
		Reporter.log(
				"5. Verify 'Check your price' in PDP | 'Check your price' should not be displayed for cooked user.");
		Reporter.log(
				"6. Verify EIP downpayment, monthly payment and loan term length that corresponds to customers CRP in PDP | EIP downpayment, monthly payment and loan term length that corresponds to customers CRP should be displayed for cooked user.");
		Reporter.log("7. Verify Memory Varients Availability | Memory Varients should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToMyTMOHome(tMNGData);
		PdpPage phonesPdpPage = navigateToTabletsAndDevicesPDP(tMNGData);
		phonesPdpPage.verifyFindYourPricingDisable();
		phonesPdpPage.verifyEIPPayment();
		phonesPdpPage.verifyLoanTermLength();
		phonesPdpPage.verifyFRPPrice();

		phonesPdpPage.verifyDownPaymentPrice();
		phonesPdpPage.verifyMemoryVariant();
		phonesPdpPage.verifyMemoryVariantsOrder();

	}

	/**
	 * US585761 : TPP - Display Base Customer experience on PDP for customers who
	 * are cookied as a MyTMO customer (logged in or not logged in) C547718 : ON PDP
	 * , verify TPP is suppressed for cookied and My Tmo customers. US496996 : As a
	 * cookied/authenticated user, I'd like to see pricing based on my CRP on PDP,
	 * so that I get the most accurate pricing.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testVerifyCookedUserTPPinAccessoryPDP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"US585761 : TPP - Display Base Customer experience on PDP for customers who are cookied as a MyTMO customer (logged in or not logged in)");
		Reporter.log(
				"US496996 : As a cookied/authenticated user, I'd like to see pricing based on my CRP on PDP, so that I get the most accurate pricing.");

		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Accessory Link and select any accessory| Accessory PDP page should be displayed");
		Reporter.log(
				"5. Verify 'Check your price' in PDP | 'Check your price' should not be displayed for cooked user.");
		Reporter.log(
				"6. Verify EIP downpayment, monthly payment and loan term length that corresponds to customers CRP in PDP | EIP downpayment, monthly payment and loan term length that corresponds to customers CRP should be displayed for cooked user.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToMyTMOHome(tMNGData);
		PdpPage phonesPdpPage = navigateToAccessoryMainPDP(tMNGData);
		phonesPdpPage.verifyFindYourPricingDisable();

		phonesPdpPage.verifyEIPPayment();
		phonesPdpPage.verifyLoanTermLength();
		phonesPdpPage.verifyFRPPrice();

		phonesPdpPage.verifyDownPaymentPrice();

	}

	/**
	 * US588459 :TPP - Prescreen Fail - Display Pre Screen No Offer Page C543895 -
	 * PDP:Pre-screen Fail - Continue Shopping
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testVerifyPrescreenFailContinueShopping(TMNGData tMNGData) {
		Reporter.log(" US588459 :TPP - Prescreen Fail - Display Pre Screen No Offer Page");
		Reporter.log("C543895 - PDP:Pre-screen Fail - Continue Shopping");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log(
				"3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing should be displayed on PLP ");
		Reporter.log("4. Click 'Find your pricing' link | Pre-screen intro page should be displayed.");
		Reporter.log("5. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"6. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log(
				"7. Enter required information and  click on the 'Find my price' CTA | pre-screen processing page should be displayed.");
		Reporter.log(
				"8. Verify processing page is displayed when the pre-screen is being done  | Processing page should be displayed after pre-screen");
		Reporter.log(
				"7. Verify header on proceessing page  | 'Pre screen No Offer Page' header on Processing page should be displayed.");
		Reporter.log("8. Click on 'Continue Shopping' link on the page | Phones PLP page should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdp = navigateToPhonesPDP(tMNGData);
		phonesPdp.verifyCRPorAwesomePricesInPDP();
		phonesPdp.verifyYourPricingBoxInPDP();
		phonesPdp.verifyYourPricingBoxTextInPDP();
		phonesPdp.verifyFindYourPricingLinkInPDP();
		phonesPdp.clickFindYourPricingLinkInPDP();

		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPrescreenform = new TPPPreScreenForm(getDriver());
		tppPrescreenform.verifyPreScreenForm();
		tppPrescreenform.verifyTextUnderPrescreenFormPageHeader();
		tppPrescreenform.fillAllFieldsOnPreScreenform(tMNGData);
		tppPrescreenform.clickFindMyPriceCTAOnPreScreenForm();
		tppPrescreenform.verifyPreScreenloadingPage();
		TPPNoOfferPage tppNoOfferPage = new TPPNoOfferPage(getDriver());
		tppNoOfferPage.verifyPrescreenNoOfferPage();
		tppNoOfferPage.clickContinueShoppingCta();

		PlpPage phonesPlpPage = new PlpPage(getDriver());
		phonesPlpPage.verifyPhonesPlpPageLoaded();
	}

	/**
	 * US578414 :[Release on 8/24] Remove SSK Fee messaging on UNO Device PDP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifySSKFeeMessageDisableonPDP(TMNGData tMNGData) {
		Reporter.log(" US578414 :[Release on 8/24] Remove SSK Fee messaging on UNO Device PDP");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Select any device from PLP |Device PDP should displayed ");
		Reporter.log(
				"4. Verify '+$25.00 SIM starter kit' text on PDP for EIP| '+$25.00 SIM starter kit' should not be displayed for EIP on PDP");
		Reporter.log("5. Click Pay in full on PDP |FRP price should be displayed on PDP.");
		Reporter.log(
				"6. Verify '+$25.00 SIM starter kit' text on PDP for FRP | '+$25.00 SIM starter kit' should not be displayed for FRP on PDP");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdp = navigateToPhonesPDP(tMNGData);
		phonesPdp.verifySSKFeeMessageDisableOnPDPForEIP();
		phonesPdp.clickPayInFullPaymentOption();
		phonesPdp.verifyFRPPriceAfterClickingOnPayInFull();
		phonesPdp.verifySSKFeeMessageDisableOnPDPForFRP();

	}

	/**
	 * US513946: As a cookied/authenticated user, I want to be able to see promotion
	 * offered for each device on the PDP page based on the transaction type
	 * upgrade, so that I can shop for devices based on promotions.
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyPromoOfferForCookedUserInPDP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test US513946:  As a cookied/authenticated user, I want to be able to see promotion offered for each device on the PDP page based on the transaction type upgrade, so that I can shop for devices based on promotions.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Phones and select any device which has offer | Phones PDP page should be displayed");
		Reporter.log(
				"5. Verify Promo Offer for devices in PDP |  Promo Offer should be displayed for Phones in PDP for cooked user.");
		Reporter.log(
				"6. Click on Promo Offer link for on of the device in PDP |  Promo Offer modal should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToMyTMOHome(tMNGData);

		PdpPage phonesPDPPage = navigateToPhonesPDP(tMNGData);
		phonesPDPPage.verifyPromotionTextonPLP();
		phonesPDPPage.clickPromotionTextonPLP();
		phonesPDPPage.verifyPromotionModal();
		phonesPDPPage.verifyDeviceNameOnPromotionModal();
		phonesPDPPage.verifyPromotionTextOnPromotionModal();

	}

	/**
	 * US513946: As a cookied/authenticated user, I want to be able to see promotion
	 * offered for each device on the PDP page based on the transaction type
	 * upgrade, so that I can shop for devices based on promotions.
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testVerifyPromoOfferForCookedUserInTabletPDP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test US513946:  As a cookied/authenticated user, I want to be able to see promotion offered for each device on the PDP page based on the transaction type upgrade, so that I can shop for devices based on promotions.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Phones and select any device which has offer | Phones PDP page should be displayed");
		Reporter.log(
				"5. Verify Promo Offer for devices in PDP |  Promo Offer should be displayed for Phones in PDP for cooked user.");
		Reporter.log(
				"6. Click on Promo Offer link for on of the device in PDP |  Promo Offer modal should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToMyTMOHome(tMNGData);

		PdpPage tabletsPDPPage = navigateToTabletsAndDevicesPDP(tMNGData);
		tabletsPDPPage.verifyPromotionTextonPLP();
		tabletsPDPPage.clickPromotionTextonPLP();
		tabletsPDPPage.verifyPromotionModal();
		tabletsPDPPage.verifyDeviceNameOnPromotionModal();
		tabletsPDPPage.verifyPromotionTextOnPromotionModal();

	}

	/**
	 * US513946: As a cookied/authenticated user, I want to be able to see promotion
	 * offered for each device on the PDP page based on the transaction type
	 * upgrade, so that I can shop for devices based on promotions.
	 *
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testVerifyPromoOfferForCookedUserInWatchesPDP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test US513946:  As a cookied/authenticated user, I want to be able to see promotion offered for each device on the PDP page based on the transaction type upgrade, so that I can shop for devices based on promotions.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Phones and select any device which has offer | Phones PDP page should be displayed");
		Reporter.log(
				"5. Verify Promo Offer for devices in PDP |  Promo Offer should be displayed for Phones in PDP for cooked user.");
		Reporter.log(
				"6. Click on Promo Offer link for on of the device in PDP |  Promo Offer modal should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToMyTMOHome(tMNGData);

		PdpPage watchesPDPPage = navigateToWatchesPDP(tMNGData);
		watchesPDPPage.verifyPromotionTextonPLP();
		watchesPDPPage.clickPromotionTextonPLP();
		watchesPDPPage.verifyPromotionModal();
		watchesPDPPage.verifyDeviceNameOnPromotionModal();
		watchesPDPPage.verifyPromotionTextOnPromotionModal();

	}

	/**
	 * US578417 Updated Sim Starter Kit name and pricing as a line item for
	 * Handset(Voice)on Cart page BYOD Scenario - Updated Sim name should name
	 * should reflect on cart page T1442619 :Validate the name and price is changed
	 * to "SIM CARD" from "SIM Kit" on PLP/PDP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testSimStarterKitPriceInPDPPage(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US578417	BYOD Scenario - Sim Name card name and Pricing should display as a line item for Handset(Voice) on Cart Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones PLP page should be displayed");
		Reporter.log("3. Select SIM Card | PDP page should be displayed");
		Reporter.log("4. Verify SIM Card Name | SIM Card Name should be displayed");
		Reporter.log("5. Verify SIM Card price | SIM Card price should be $10");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.selectSimCardFromPlp();
		PdpPage devicesPdpPage = new PdpPage(getDriver());
		devicesPdpPage.verifyPhonePdpPageLoaded();
		devicesPdpPage.verifySimKitName();
		devicesPdpPage.verifySimKitPrice();
	}

	/**
	 * US586088 Updated Sim Starter Kit name and pricing as a line item for MI
	 * Device on Cart page BYOD Scenario - Updated Sim name should name should
	 * reflect on cart page T1442621 :Validate the name is changed to "SIM CARD"
	 * from "Mobile Internet SIM Kit" on PLP/PDP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testSimStarterKitPriceInPDPPageThroughInterNetDevices(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US578417	BYOD Scenario - Sim Name card name and Pricing should display as a line item for Handset(Voice) on Cart Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Tablets Link | Tablets page should be displayed");
		Reporter.log("4. Select SimStarterKit | PDP page should be displayed");
		Reporter.log("5. Verify SIM Card Name | SIM Card Name should be displayed");
		Reporter.log("6. Verify SimStarterKit price | SimStarterKit price should be $10");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage tabletsPlpPage = navigateToTabletsAndDevicesPLP(tMNGData);
		tabletsPlpPage.selectSimCardFromPlp();
		PdpPage tabletsAndDevicesPdpPage = new PdpPage(getDriver());
		tabletsAndDevicesPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletsAndDevicesPdpPage.verifySimKitName();
		tabletsAndDevicesPdpPage.verifySimKitPrice();

	}

	/**
	 * US617619:As a Product Manager, I want to be able to display a "How does
	 * pre-order work? " link for pre-order devices on PDP, so that customers are
	 * aware of the pre-order process.
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testUnCookiedCustomersPreOrderStatusForDevice(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US617619- As a Product Manager, I want to be able to display a 'How does pre-order work?' link for pre-order devices on PDP, so that customers are aware of the pre-order process.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Select newly launched device | Phones Pdp page should be displayed");
		Reporter.log("4. Verify 'Pre-order' button | 'Pre-order' button should be displayed");
		Reporter.log("5. Verify 'How does pre-order work?' link | 'How does pre-order work?' link should be displayed");
		Reporter.log("6. Click on 'Pre-order' button | 'Pre-order' model window should be displayed");
		Reporter.log("7. Verify PreOrder header | PreOrder header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdp = navigateToPhonesPDP(tMNGData);
		phonesPdp.verifyPreOrderCTA();
		phonesPdp.verifyHowPreOrderWorkTextLink();
		phonesPdp.clickOnHowPreOrderWorkTextLink();
		phonesPdp.verifyHowPreOrderWorksModel();
		phonesPdp.verifyHowPreOrderWorksModelHeader();
	}

	/**
	 * US385695: TMNG Main PLP Migration to V2 - Product Grid: Product Review
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyProductReviewStarImgAndCTAOnPhonesMainPLP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US385691: TMNG Main PLP Migration to V2 - Product Grid: Product Name");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log(
				"3. Verify Review Star image in PLP | Review Star image should be displayed for all the phones in PLP");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage plpPage = navigateToPhonesPlpPage(tMNGData);
		plpPage.verifyAllRatingStarsImage();
		plpPage.clickOnRatingStarsImage();
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.verifyPhonePdpPageLoaded();
		phoneDetailsPage.verifyReviewSectionIsDisplayed();
	}

	/**
	 * US508916 DEFECT: Accessories - PureGear Tempered Glass Screen Protector for
	 * LG V40 ThinQ image is missing C478813 ACCESSORIES : Images should be
	 * displayed for every accessory on the main PLP and PDP pages
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void verifyAccessoryImgAtMainPLPandMainPDP(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US508916	DEFECT: Accessories - PureGear Tempered Glass Screen Protector for LG V40 ThinQ image is missing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on accessories Link |Accessories page should be displayed");
		Reporter.log("3. Click on Any accessory |PDP page should be displayed");
		Reporter.log("4. Verify Accessory Image | Accessory Image should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage accessoriesMainPLPPage = navigateToAccessoryPLP(tMNGData);
		accessoriesMainPLPPage.verifyDeviceImagesInMainPLP();
		accessoriesMainPLPPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryDetailsPage = new PdpPage(getDriver());
		accessoryDetailsPage.verifyAccessoriesPdpPageLoaded();
		accessoryDetailsPage.verifDeviceImageAtPDP();

	}

}
