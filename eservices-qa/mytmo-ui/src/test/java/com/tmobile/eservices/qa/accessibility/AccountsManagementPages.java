package com.tmobile.eservices.qa.accessibility;

import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsConfirmationPage;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsSelectionPage;
import com.tmobile.eservices.qa.pages.accounts.ODFDataPassReviewPage;
import com.tmobile.eservices.qa.pages.accounts.PlansComparisonPage;
import com.tmobile.eservices.qa.pages.accounts.PlansConfigurePage;

public class AccountsManagementPages extends AccountsCommonLib {
	AccessibilityCommonLib accessibilityCommonLib = new AccessibilityCommonLib();
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = "AMAccessibility")
	public void testPlanPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAccountOverviewPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PlanPage");
		accessibilityCommonLib.testAccessibility(getDriver(),"PlanPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = "AMAccessibility")
	public void testPlanFeaturePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPlanFeaturesPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PlanFeaturePage");
		accessibilityCommonLib.testAccessibility(getDriver(),"PlanFeaturePage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = "AMAccessibility")
	public void testPlanComparisionPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPlanComparisionPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PlanComparisionPage");
		accessibilityCommonLib.testAccessibility(getDriver(),"PlanComparisionPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = "AMAccessibility")
	public void testPlanConfigurePage(ControlTestData data, MyTmoData myTmoData) {
		if (navigateToPlanComaprisionPage(myTmoData)) {
			PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
			plansComparisonPage.clickMorePlanOptions();
			plansComparisonPage.clickSelectandContinueButtonUsingSOC();

			PlansConfigurePage plansConfigurePage = new PlansConfigurePage(getDriver());
			plansConfigurePage.verifyPlanConfigurePage();
			verifyPlanConfigurePageTitle();}
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PlanConfigurePage");
		accessibilityCommonLib.testAccessibility(getDriver(),"PlanConfigurePage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true,groups = "AMAccessibility")
	public void testPlanConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		//navigateToPlanConfirmationPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PlanConfirmationPage");
		accessibilityCommonLib.testAccessibility(getDriver(),"PlanConfirmationPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = "AMAccessibility")
	public void testPlanListPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPlanListPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PlanListPage");
		accessibilityCommonLib.testAccessibility(getDriver(),"PlanListPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = "AMAccessibility")
	public void testPlanReviewPage(ControlTestData data, MyTmoData myTmoData) {
		//navigateToPlanReviewPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PlanReviewPage");
		accessibilityCommonLib.testAccessibility(getDriver(),"PlanReviewPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = "AMAccessibility")
	public void testManageAddonsSelectionPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToManageDataAndAddOnsFlow(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ManageAddonsSelectionPage");
		accessibilityCommonLib.testAccessibility(getDriver(),"ManageAddonsSelectionPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = "AMAccessibility")
	public void testManageAddonsReviewPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToODFFreeRadioButtonToReviewPage(myTmoData,"Service");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ManageAddonsReviewPage");
		accessibilityCommonLib.testAccessibility(getDriver(),"ManageAddonsReviewPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true,groups = "AMAccessibility")
	public void testManageAddonsConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToManageDataAndAddOnsFlow(myTmoData);
		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.clickOnCheckBox("Voicemail to Text");
		manageAddOnsSelectionPage.clickOnCheckBox("Name ID");
		manageAddOnsSelectionPage.clickOnContinueBtn();
		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		verifyODFDataPassReviewPage();
		oDFDataPassReviewPage.clickOnAgreeandSubmitButton();
		ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
		newODFConfirmationPage.verifyODFConfirmationPage();
		newODFConfirmationPage.verifyThanksHeaderOnConfirmationPage();
		newODFConfirmationPage.verifyOrderDetailsBladeDisplayedwithDownwarCarat();
		newODFConfirmationPage.clickOnOrderDetailsBlade();
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"ManageAddonsConfirmationPage");
		accessibilityCommonLib.testAccessibility(getDriver(),"ManageAddonsConfirmationPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = "AMAccessibility")
	public void testPlanBenefitsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToFutureURLFromHome(myTmoData,"/plan-benefits");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PlanBenefitsPage");
		accessibilityCommonLib.testAccessibility(getDriver(),"PlanBenefitsPage");
	}
	
	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = "AMAccessibility")
	public void testPromotionsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToMyPromotionsPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PromotionsPage");
		accessibilityCommonLib.testAccessibility(getDriver(),"PromotionsPage");
	}

	/**
	 * Verify PrivacyAndNotificationsPage Information
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "global" })
	public void testPrivacyAndNotificationsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPrivacyAndNotificationsPage(myTmoData, "Privacy and Notifications");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(),"PrivacyAndNotificationsPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "PrivacyAndNotificationsPage");
	}
	
}
