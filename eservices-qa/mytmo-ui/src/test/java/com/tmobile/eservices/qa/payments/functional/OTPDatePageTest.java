/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.OTPDatePage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

/**
 * @author charshavardhana
 *
 */
public class OTPDatePageTest extends PaymentCommonLib{

	/**
	 * US283065	GLUE Light Reskin - OTP Edit Payment Date Page
	 * @param data
	 * @param myTmoData
	 * Data Conditions - Regular user. Eligible for future payment (due date in future)
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void testGLRReskinOTPPaymentDatePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US283065	GLUE Light Reskin - OTP Edit Payment Date Page");
		Reporter.log("Data Condition | EIP Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on date blade| Date selction page should be displayed");
		Reporter.log("Step 5: verify calendar modal| Calendar should be displayed for active date selection.");
		Reporter.log("================================");
		Reporter.log(Constants.ACTUAL_TEST_STEPS);

		OTPDatePage otpDatePage = navigateToOTPDatePage(myTmoData);
		otpDatePage.verifyDatePageHeader();
		otpDatePage.verifycalendar();
		otpDatePage.clickDateBackBtn();
	}


	/**
	 * US283065	GLUE Light Reskin - OTP Edit Payment Date Page 
	 * @param data
	 * @param myTmoData
	 * Data Condition - Regular user. Eligible for immediate payments only
	 * Pending - since hard to get test data on production
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING })
	public void testGLROTPPaymentDatepageIneligible(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US283065	GLUE Light Reskin - OTP Edit Payment Date Page");
		Reporter.log("Data Conditions - Regular user. Eligible for immediate payments only");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on date blade| Date selction page should be displayed");
		Reporter.log("Step 5: verify Immediate payments message| Message should be displayed without date selection.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

        OTPDatePage otpDatePage = navigateToOTPDatePage(myTmoData);
        otpDatePage.verifyInelgibleDateSelectionMsg();
        otpDatePage.clickDateBackBtn();
        otpDatePage.verifyPageLoaded();
	}


	/**
	 * US262823	Angular + GLUE - OTP - Landing Page - Payment Date Blade (BAU)
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testAngularGLUEOTPLandingPagePaymentDateBlade(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262823	Angular + GLUE - OTP - Landing Page - Payment Date Blade (BAU)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on choose date blade | Date Selection page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyDateBlade();
	}

	/**
	 * US262823	Angular + GLUE - OTP - Landing Page - Payment Date Blade (BAU)
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups={Group.SEDONA})
	public void testAngularGLUEOTPLandingPagePaymentDateBladeSedona(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US262823	Angular + GLUE - OTP - Landing Page - Payment Date Blade (BAU)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on choose date blade | Date Selection page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyDateBlade();
	}

}
