package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.AutopayDiscounts;

import io.restassured.response.Response;

public class AutopayDiscountsTest extends AutopayDiscounts {

	public Map<String, String> tokenMap;

	/**
	/**
	 * UserStory# US496577: My-TMO-Payments-AutoPay Service- autopay and employee discount on future rateplan. MBB;
	 * UserStory# US496584: My-TMO-Payments-AutoPay Service- sum of autopay discount and employee discount information. MBB Plans only
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AutopayDiscountsTest","testAutopayDiscountFutureSumDetailsMBBandGSM",Group.PAYMENTS,Group.SPRINT  })
	public void testAutopayDiscountFutureSumDetailsMBBandGSM(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testAutopayDiscountFutureSumDetailsMBBandGSM");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with AutopayDiscountDetails.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testAutopayDiscountFutureSumDetailsMBBandGSM";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("AutopayDiscount_FutureSumDetails_MBBandGSM.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("currentPlanCodes", apiTestData.getCurrentPlanCode());
		tokenMap.put("futurePlanCodes", apiTestData.getFuturePlanCode());
		tokenMap.put("productTypes", apiTestData.getProductType());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = AutopayDisocuntDetailsBuilder(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"autopayStatus.isEligibleForAutoPay"), 
						"true", "AutoPay Eligible displayed  ");
				Assert.assertEquals(getPathVal(jsonNode,"accountNumber"),
						apiTestData.getBan() , "Account Number is MisMatched");
					Assert.assertEquals(getPathVal(jsonNode,"plans.soc"), 
							"["+apiTestData.getFuturePlanCode()+"]", "Current Plan code is Mismatched");
					Assert.assertEquals(getPathVal(jsonNode,"plans.lines.msisdn"), 
							"[["+apiTestData.getMsisdn()+"]]", "Msisdin is  Mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"plans.lines.productType"), 
						"[["+apiTestData.getProductType()+"]]", "Product Type is Mismatched");
				
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("lines").get(0).get("discounts").get(0).get("type"),
						"Discount Type is Not presented");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("lines").get(0).get("discounts").get(0).get("name"),
						"Discount Description is Not presented");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("lines").get(0).get("discounts").get(0).get("amount"),
						"Discount Amount is Not presented");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("account").get("discountSums").get(0).get("banLevel"),
						"Ban Level is Not presented");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("account").get("discountSums").get(0).get("total"),
						"Total is Not presented");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("account").get("discountSums").get(0).get("type"),
						"Type is Not presented");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("totalDiscountSum"),
						"Total Discount Sum is Not presented");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}


	/**
	/**
	 * UserStory# US496577: My-TMO-Payments-AutoPay Service- autopay and employee discount on future rateplan. MBB;
	 * UserStory# US496584: My-TMO-Payments-AutoPay Service- sum of autopay discount and employee discount information. MBB Plans only
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AutopayDiscountsTest","testAutopayDiscountCurrentSumDetailsMBBandGSM",Group.PAYMENTS,Group.SPRINT  })
	public void testAutopayDiscountCurrentSumDetailsMBBandGSM(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testAutopayDiscountCurrentSumDetailsMBBandGSM");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with AutopayDiscountDetails.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testAutopayDiscountCurrentSumDetailsMBBandGSM";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("AutopayDiscount_CurrentSumDetails_MBBandGSM.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("currentPlanCodes", apiTestData.getCurrentPlanCode());
		tokenMap.put("productTypes", apiTestData.getProductType());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = AutopayDisocuntDetailsBuilder(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"autopayStatus.isEligibleForAutoPay"), 
						"true", "AutoPay Eligible displayed  ");
				Assert.assertEquals(getPathVal(jsonNode,"accountNumber"),
						apiTestData.getBan() , "Account Number is MisMatched");
				Assert.assertEquals(getPathVal(jsonNode,"plans.soc"), 
						"["+apiTestData.getCurrentPlanCode()+"]", "Current Plan code is Mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"plans.lines.msisdn"), 
						"[["+apiTestData.getMsisdn()+"]]", "Msisdin is Mismatched");
			Assert.assertEquals(getPathVal(jsonNode,"plans.lines.productType"), 
					"[["+apiTestData.getProductType()+"]]", "Product Type is Mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"plans.isCurrent"), 
						"["+"true"+"]", "is Currentplan is not true");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("lines").get(0).get("discounts").get(0).get("type"),
						"Discount Type is Not presented");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("lines").get(0).get("discounts").get(0).get("name"),
						"Discount Description is Not presented");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("lines").get(0).get("discounts").get(0).get("amount"),
						"Discount Amount is Not presented");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("account").get("discountSums").get(0).get("banLevel"),
						"Ban Level is Not presented");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("account").get("discountSums").get(0).get("total"),
						"Total is Not presented");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("account").get("discountSums").get(0).get("type"),
						"Type is Not presented");
				Assert.assertNotNull(jsonNode.get("plans").get(0).get("totalDiscountSum"),
						"Total Discount Sum is Not presented");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	/**
	 * UserStory# US496574: My-TMO-Payments-AutoPay eligibility service;
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AutopayDiscountsTest","testAutopayEligibilityCheck",Group.PAYMENTS,Group.SPRINT  })
	public void testAutopayEligibilityCheck(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testAutopayEligibilityCheck");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with AutoPayeEligibility.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testAutopayEligibilityCheck";

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		Response response = AutoPayDiscountEligibility(apiTestData,tokenMap,apiTestData.getBan());

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"accountNumber"),
						apiTestData.getBan() , "Account Number Matched");

				Assert.assertEquals(getPathVal(jsonNode,"eligible"),
						"true" , "eligibile condition is False");
			}
		} else {

			failAndLogResponse(response, operationName);
		}


	}


	/**
	/**
	 * UserStory# US496574: My-TMO-Payments-AutoPay eligibility service;
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AutopayDiscountsTest","testAutopayInEligibilityCheck",Group.PAYMENTS,Group.SPRINT  })
	public void testAutopayInEligibilityCheck(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testAutopayInEligibilityCheck");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with AutoPayeInEligibility.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testAutopayInEligibilityCheck";

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		Response response = AutoPayDiscountEligibility(apiTestData,tokenMap,apiTestData.getBan());

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"accountNumber"),
						apiTestData.getBan() , "Account Number Matched");

				Assert.assertEquals(getPathVal(jsonNode,"eligible"),
						"false" , "eligibile condition is true");
				Assert.assertEquals(getPathVal(jsonNode,"ineligibilityCode"),
						"01" , "Reason Code is mismatched");

				Assert.assertEquals(getPathVal(jsonNode,"ineligibilityDescription"),
						"The account type/subtype is not eligible for auto-payment" , "Reason Code is mismatched");
			}
		} else {

			failAndLogResponse(response, operationName);
		}


	}

	/**
	/**
	 * UserStory# US496587: My-TMO-Payments-AutoPay Discount Service- Autopay Details
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AutopayDiscountsTest","testAutoPayStatusForEligibilityBan",Group.PAYMENTS,Group.SPRINT  })
	public void testAutoPayStatusForEligibilityBan(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testAutoPayStatusForEligibilityBan");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with autopaydiscount status for autopay eligible ban.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify all fields present in the response.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testAutoPayStatusForEgliblityBan";

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		Response response = AutoPayDiscountStatus(apiTestData,tokenMap,apiTestData.getBan());

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"isAutoPayEnrrolled"),
						"true" , "isAutoPayEnrrolled is mismatched");
				Assert.assertNotNull(jsonNode.get("autopayRecurringDay"),
						"autopayRecurringDay  is not present");
				Assert.assertNotNull(jsonNode.get("isInsideBlackoutPeriod"),
						"isInsideBlackoutPeriod  is not present");
				Assert.assertNotNull(jsonNode.get("isDueDateToday"),
						"isDueDateToday  is not present");
				Assert.assertNotNull(jsonNode.get("isDueDateAvailable"),
						"isDueDateAvailable  is not present");
				Assert.assertNotNull(jsonNode.get("dateStatus"),
						"dateStatus  is not present");
				Assert.assertNotNull(jsonNode.get("autoPayDueDate"),
						"autoPayDueDate  is not present");
				Assert.assertNotNull(jsonNode.get("billDueDate"),
						"billDueDate  is not present");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	/**
	 * UserStory# US496587: My-TMO-Payments-AutoPay Discount Service- Autopay Details
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AutopayDiscountsTest","testAutoPayStatusForInEligibilityBan",Group.PAYMENTS,Group.SPRINT  })
	public void testAutoPayStatusForInEligibilityBan(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testAutoPayStatusForInEligibilityBan");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with autopaydiscount status for autopay Ineligible ban.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify all fields present in the response.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testAutoPayStatusForEgliblityBan";

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		Response response = AutoPayDiscountStatus(apiTestData,tokenMap,apiTestData.getBan());

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"isAutoPayEnrrolled"),
						"false" , "isAutoPayEnrrolled is mismatched");
				Assert.assertNotNull(jsonNode.get("isInsideBlackoutPeriod"),
						"isInsideBlackoutPeriod  is not present");
				Assert.assertNotNull(jsonNode.get("isDueDateToday"),
						"isDueDateToday  is not present");
				Assert.assertNotNull(jsonNode.get("isDueDateAvailable"),
						"isDueDateAvailable  is not present");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	/**
	 * UserStory# US496576 My-TMO-Payments-AutoPay Service- list of autopay and list of employee discount on Current rateplan.
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AutopayDiscountsTest",
			"testListOfAutopayAndListOfEmployeeDiscountOnCurrentRateplan",Group.PAYMENTS,Group.SPRINT  })
	public void testListOfAutopayAndListOfEmployeeDiscountOnCurrentRateplan(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("US496576 My-TMO-Payments-AutoPay Service- list of autopay and list of employee discount on Current rateplan. GSM");
		Reporter.log("Data Conditions:current plan type is GSM Pooled");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1:test  EOS-AutopayDiscount with autopay eligible ban |   list of autopay discount and list of Employee discount of GSM Pooled lines and add on lines for the current rate plan returned in response");
		Reporter.log("Step 2:verify List of autopay discount information for Plan level ,Line level returned in response");
		Reporter.log("Step 3:verify List of employee eligibility discount information for Plan level Line level returned in response");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="test_ListOfAutopayAndListOfEmployeeDiscountOnCurrentRateplan";

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		Response response = listOfAutopayAndListOfEmployeeDiscountOnCurrentRateplan(apiTestData,tokenMap,apiTestData.getBan());

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"status.statusCode"),
						"S" , "Status Code is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"status.statusMessage"),
						"Success" , "Status Message is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"ban"),
						apiTestData.getBan() , "Account Number Matched");
				Assert.assertEquals(getPathVal(jsonNode,"discount.msisdn"),
						"["+apiTestData.getMsisdn()+"]" , "MSISDN Number Matched");
				Assert.assertNotNull(jsonNode.get("discount").get(0).get("code"),
						"code  is not present");
				Assert.assertNotNull(jsonNode.get("discount").get(0).get("description"),
						"description  is not present");
				Assert.assertNotNull(jsonNode.get("discount").get(0).get("type"),
						"type  is not present");
				Assert.assertNotNull(jsonNode.get("discount").get(0).get("percentage"),
						"percentage  is not present");
				Assert.assertNotNull(jsonNode.get("discount").get(0).get("amount"),
						"amount  is not present");
				Assert.assertNotNull(jsonNode.get("discount").get(0).get("startDate"),
						"startDate  is not present");
				Assert.assertNotNull(jsonNode.get("discount").get(0).get("expirationDate"),
						"expirationDate  is not present");
				Assert.assertNotNull(jsonNode.get("discount").get(0).get("kickbackDiscountEligibility").get("value"),
						"value  is not present");

			}
		} else {

			failAndLogResponse(response, operationName);
		}


	}

}
