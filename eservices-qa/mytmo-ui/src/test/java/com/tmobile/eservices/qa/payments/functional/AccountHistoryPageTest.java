/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.BillingSummaryPage;
import com.tmobile.eservices.qa.pages.payments.EIPDetailsPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author charshavardhana
 *
 */
public class AccountHistoryPageTest extends PaymentCommonLib {

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" , Group.SMOKE })
	public void verifyAccountActivitiesInEipDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : verify Account Activities In Eip Details Page");
		Reporter.log("Data Conditions - Regular user. eBill data");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("01. Launch the application | Application Should be Launched");
		Reporter.log("02. Login to the application | User Should be login successfully");
		Reporter.log("03. Verify Home page | Home page should be displayed");
		Reporter.log("04. Click on Billing Link | eBill Summary page should be displayed");
		Reporter.log(
				"05.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("06.Click on view details | EIP details page should be displayed");
		Reporter.log("07.Click on Account Activities | Account history section should be displayed");
		Reporter.log("09.Click on AccountHistory Toggle |Document tab should be displayed");
		Reporter.log("10.Verify documents tab |Document tab should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyEipHeaderText();
		billingSummaryPage.clickViewDetails();
		EIPDetailsPage eIPDetailsPage = new EIPDetailsPage(getDriver());
		eIPDetailsPage.verifyPageLoaded();
		eIPDetailsPage.clickAccountActivities();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
	/*	if (getDriver() instanceof AppiumDriver) {
			accountHistoryPage.clickAccountHistoryToggleMobile();
		}*/
		accountHistoryPage.selectCategoryDropdown("Documents");
		Reporter.log("Document tab is loaded");
	}

	/**
	 * US216967 - [Continued] [Continued] UMB - Desktop/Mobile/App: Enable
	 * Download PDF for eBill/BriteBill
	 * 
	 * US183518 [Continued] UMB - AH Redesign - Create/Download PDF Receipt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" , Group.SMOKE })
	public void testDownloadPDFLinkForEbill(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Veify Download PDF link for brite bill in alert and activites page");
		Reporter.log("Data Condition - Brite Bill Data");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on account history link | Alert and activires page should be displayed");
		Reporter.log("5. click on billing tab & verify download PDF link | Download PDF link should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		accountHistoryPage.selectCategoryDropdown("Billing");
		accountHistoryPage.verifyDownloadPDFLink();

		accountHistoryPage.verifyPageLoaded();
		accountHistoryPage.selectCategoryDropdown("All Categories");
		accountHistoryPage.verifyDownloadPDFLink();

		// To Validate PDF content
		/*
		 * boolean fileName = new
		 * PDFServiceHelper().validateContent(getFilePath(), "Monthly Statement"
		 * ); Assert.assertEquals(fileName, true); File file = new
		 * File(getFilePath()); Assert.assertEquals(file.delete(), true);
		 */
	}

	/**
	 * US217080, US217079, US232954 UMB - AH Redesign - Filter Records based on
	 * Date, Line Selector, and Category
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" , Group.SMOKE})
	public void testAccountActivityGridAndFiltering(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test case: US232954	UMB - AH Redesign - Filter Records based on Date, Line Selector, and Category");
		Reporter.log("Data Condition - PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Account Activity | Account activity page should be displayed");
		Reporter.log(
				"5. Click on Line selector and select a line | Selected Line records should be displayed and Virtual line text should not be displayed.");
		Reporter.log("6. Select a category | Selected category records should be displayed");
		Reporter.log("===============================");
		Reporter.log("Actual Test Steps");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.selectALineFromDropDown(myTmoData);
		accountHistoryPage.selectCategoryDropdown("Payments");
		accountHistoryPage.verifySelectedCategoryInAccHistory("Payment");
		accountHistoryPage.verifySelectedLineInAccHistory();
	}

	/**
	 * US185738 - UMB - AH Redesign - Line Selector UI Component
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS , Group.SMOKE,"thulasi"})
	public void testLineSelectorDropdownInAccountHistoryPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US185738 - UMB - AH Redesign - Line Selector UI Component");
		Reporter.log("Data Condition - PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Account Activity | Account activity page should be displayed");
		Reporter.log(
				"5. Click on Line selector & verify account title | Accoutn title 'Account (all lines)' should be displayed ");
		Reporter.log("===============================");
		Reporter.log("Actual Test Steps");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.verifyLineSelectorAccountTitle();
		//accountHistoryPage.verifyMsisdnInAccountDropDown(myTmoData);
		accountHistoryPage.selectALineFromDropDown(myTmoData);
		accountHistoryPage.verifySelectedLineInAccHistory();
		
		
	}

	/**
	 * US259632 - UMB - AH Redesign - Add Motts records to AH - Implementation
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testJumpOrIPhoneUpgradeProgramInAccountHistoryPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test case: US259632 - UMB - AH Redesign - Add Motts records to AH - Implementation");
		Reporter.log("Data Condition - PAH account eligible for JUMP");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Account Activity | Account activity page should be displayed");
		Reporter.log("5. Click on JUMP record | Accoutn title 'Account (all lines)' should be displayed ");
		Reporter.log("===============================");
		Reporter.log("Actual Test Steps");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.clickOnJumpRecord();
		accountHistoryPage.verifyJumpRecordInfo();
		Reporter.log("Jump record is displayed");
		accountHistoryPage.clickOnJumpRecord();
		accountHistoryPage.verifyJumpRecordTermsAndConditionsLink();
		Reporter.log("Jump record terms & conditions link is displayed");
	}

	/**
	 * Verify that all Date Options are present in Date Filter Dropdown US312483
	 * - Additional Terms : Account History Filter Max
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS , Group.SMOKE })
	public void testAcctHistoryDateFilterOptions(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US312483, Verify that all Date Options are present in Date Filter Dropdown");
		Reporter.log("Data Condition - PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3: Click on account history | Account History page should be displayed");
		Reporter.log("4: Click on Date Filter dropdown | All options should be present in Date Filter dropdown");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.verifyDateFilterDefaultOption("Last 90 days");
		accountHistoryPage.clickOnDateFilterDropdown();
		accountHistoryPage.verifyDateOptionsFromDropDown("Last 30 days");
		accountHistoryPage.verifyDateOptionsFromDropDown("Last 90 days");
		accountHistoryPage.verifyDateOptionsFromDropDown("Current year");
		accountHistoryPage.verifyDateOptionsFromDropDown("Previous year");
		// accountHistoryPage.verifyDateOptionsFromDropDown("Last 24 months");
		accountHistoryPage.verifyDateOptionsFromDropDown("All (24 months)");
		accountHistoryPage.verifyDateOptionsFromDropDown("Custom dates");
	}

	/**
	 * Avengers Sprint 23 US185740 UMB - AH Redesign - Date Filter UI Component
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS , Group.SMOKE })
	public void testAcctHistoryDateFilterUI(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Date Filter functionality on Account History Angular UI pages");
		Reporter.log("Data Condition - PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3: Click on account history | Account History page should be displayed");
		Reporter.log("4: Select Custom Dates from date drop-down | Date picker should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.selectDateFromDropDown("Custom Dates");
		accountHistoryPage.verifyDatePicker();
		accountHistoryPage.verifyDatePickerCalendar();
		accountHistoryPage.verifyPageLoaded();
	}

	/**
	 * US185741 UMB - AH Redesign - Page number UI Component
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" , Group.SMOKE})
	public void testAHRedesignPageNumberUIComponent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US185741	UMB - AH Redesign - Page number UI Component");
		Reporter.log("Data Condition - PAH account");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log(
				"Step 3: Navigate to Account History Angular page | Account History Angular page should be displayed");
		Reporter.log("Step 4: Verify Pagenumber component | Puge number component should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.verifyPagination();
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS , Group.SMOKE })
	public void testFilterCategoryOptionsDropdown(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify that all options are present at Category Filter dropdown");
		Reporter.log("Data Condition - PAH account.Brite Bill Data");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on account history link | Account History page should be displayed");
		Reporter.log(
				"5. Click on Category Filter dropdown | All options should be present in Category Filter dropdown");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.clickOnCategoryFilterDropdown();
		accountHistoryPage.verifyCategoryPresentInDropdown("All Categories");
		
		accountHistoryPage.verifyCategoryPresentInDropdown("Billing");
		
		accountHistoryPage.verifyCategoryPresentInDropdown("Payments");
		accountHistoryPage.verifyCategoryPresentInDropdown("Services");
		accountHistoryPage.verifyCategoryPresentInDropdown("Account");
		accountHistoryPage.verifyCategoryPresentInDropdown("Device");
		accountHistoryPage.verifyCategoryPresentInDropdown("Documents");
		accountHistoryPage.verifyCategoryPresentInDropdown("Conversations");
	}

	/**
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.PERFORMANCE , Group.SMOKE })
	public void testFilterCategoryDropdownSelection(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify that all options are present at Category Filter dropdown");
		Reporter.log("Data Condition - PAH account.Brite Bill Data");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on account history link | Account History page should be displayed");
		Reporter.log(
				"5. Select Category from Category Filter dropdown | Approppriate selected category records should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.selectCategoryDropdown("All Categories");
		accountHistoryPage.verifyDownloadPDFLink();
		accountHistoryPage.selectCategoryDropdown("Billing");
		accountHistoryPage.verifySelectedCategoryInAccHistory("Billing");
		accountHistoryPage.verifyDownloadPDFLinkForBriteBill();
		accountHistoryPage.selectCategoryDropdown("Payments");
		accountHistoryPage.verifySelectedCategoryInAccHistory("Payment");
		accountHistoryPage.selectCategoryDropdown("Services");
		accountHistoryPage.verifySelectedCategoryInAccHistory("Services");
		accountHistoryPage.selectCategoryDropdown("Account");
		accountHistoryPage.verifySelectedCategoryInAccHistory("Account");//Security
		accountHistoryPage.selectCategoryDropdown("Device");
		accountHistoryPage.verifySelectedCategoryInAccHistory("Device");
		
		
		accountHistoryPage.selectCategoryDropdown("Documents");
		accountHistoryPage.verifySelectedCategoryInAccHistory("Documents");
		accountHistoryPage.selectCategoryDropdown("Conversations");
		accountHistoryPage.verifySelectedCategoryInAccHistory("Conversations");
	
		

	}

	/**
	 * US308109 - "Download PDF" CTAs For Non-PAH Users
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS , Group.SMOKE})
	public void testDownloadPDFLinkForNonPAH(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US308109 - Veify \"Download PDF\" CTAs For Non-PAH Users");
		Reporter.log("Data Condition - Non-PAH (standard, restricted) account.Brite Bill Data");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on account history link | Account History page should be displayed");
		Reporter.log("5. Click on billing tab & verify download PDF link | Download PDF link should NOT be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.selectCategoryDropdown("Billing");
		accountHistoryPage.verifyDownloadPDFLinkIsNotPresent();
	}

	/**
	 * US284353 - PDE - Account History - Add PDE Enrollment Record to AH -
	 * Implementation US303782 - PDE - Account History - Add PDE
	 * Closure/Cancellation Record to AH - Implementation
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "nodata" })
	public void testPromotionsFilter(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : US303782, US284353 - PDE - Account History - Add PDE Enrollment Record to AH - Implementation");
		Reporter.log("Data Condition - Regular, PAH/FA account. Account with promotions. PDE account");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on account history link | Account History page should be displayed");
		Reporter.log("5. Click on filter | Promotion option should be present");
		Reporter.log(
				"6. Select Promotion filter | Records related to promotions Redeemed, Completed, Ended should be present");
		Reporter.log("7. No other records should be present other than promotions");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.selectCategoryDropdown("Device");
		accountHistoryPage.verifyPromotionRecordIsPresent();
	}

	/**
	 * US123887 - UMB - AH Redesign - New Bill Available - Add Bill Summary Link
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testViewBillLinkRedirectsToBillSummaryPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US123887 - UMB - AH Redesign - New Bill Available - Add Bill Summary Link");
		Reporter.log("Data Condition - Regular, non-PAH account. Brite Bill Data");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on account history link | Account History page should be displayed");
		Reporter.log("5. Click on billing tab & verify View Bill link | View Bill link should be displayed");
		Reporter.log("6. Click on View Bill link | View Bill link shall redirected me to the Bill Summary page");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		
		accountHistoryPage.selectCategoryDropdown("Billing");

		accountHistoryPage.verifyViewBillLink();
		accountHistoryPage.clickViewBillLink();
		BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
		billAndPaySummaryPage.verifyPageLoaded();
	}

	/**
	 * US183518 - [Continued] UMB - AH Redesign - Create/Download PDF Receipt
	 * US259367 - UMB - AH Redesign - Create/Download PDF Receipt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" })
	public void testDownloadPDFLinkIsPresentPAH(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US183518 - [Continued] UMB - AH Redesign - Create/Download PDF Receipt");
		Reporter.log("Data Condition - PAH/FA account. Brite Bill Data");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on account history link | Alert and activires page should be displayed");
		Reporter.log("5. Click on billing tab & verify download PDF link | Download PDF link should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.selectCategoryDropdown("Billing");
		accountHistoryPage.verifyDownloadPDFLinkForBriteBill();
	}

	/**
	 * US216533 - UMB - Desktop/Mobile/App: Update the Print All Payments link
	 * to include Sedona payments
	 * 
	 * US216538 - UMB - Desktop/Mobile/App: Update the Print All Payments
	 * template
	 * US377931 Account History Print Receipt
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "retired" })
	public void testPrintAllPaymentsLinkInAccountHistoryPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US216538 Veify print all payments link is present in s page");
		Reporter.log("Data Condition - PAH account with Payments history");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on account history link | Alert and activires page should be displayed");
		Reporter.log(
				"5. Click on payemnts tab & verify print all payments link | Print all paymentslink should be displayed");
		Reporter.log("6. Click on paymentsLink | Should be able to print all the payments");
		Reporter.log("7. Select 'all categories'| Should be able to see payment blades with print payments receipt links");
		Reporter.log("8.  Click on paymentsreceiptLink for categories  |Should be able to print all the payments");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.verifyPrintPaymentReceiptLink();
		accountHistoryPage.selectCategoryDropdown("Payments");
		accountHistoryPage.verifyPrintPaymentReceiptLink();
		accountHistoryPage.verifyPrintAllPaymentsLink();
	}

	/**
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "merged" , Group.SMOKE })
	public void testAccountHistorySelectBillingPaymentSrvices(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : For performance");
		Reporter.log("Data Condition - PAH account with Payments history");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on account history link | Alert and activires page should be displayed");
		Reporter.log("5. Click on Billing,payemnts and service tabs | All tabs should be clickable");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.selectCategoryDropdown("Billing");
		accountHistoryPage.verifySelectedCategoryInAccHistory("Billing");
		accountHistoryPage.selectCategoryDropdown("Payments");
		accountHistoryPage.verifySelectedCategoryInAccHistory("Payment");
		accountHistoryPage.selectCategoryDropdown("Services");
		accountHistoryPage.verifySelectedCategoryInAccHistory("Services");
		accountHistoryPage.selectCategoryDropdown("Account");
		accountHistoryPage.verifySelectedCategoryInAccHistory("Security");
	

	}

	/**
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PERFORMANCE, Group.SMOKE })
	public void testAccountHistoryEquipmentageementdownloadpdf(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : For Defect DE167446");
		Reporter.log("Data Condition - PAH account with Payments history");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on account history link | Alert and activires page should be displayed");
		Reporter.log("5. Click on Billing,payemnts and service tabs | All tabs should be clickable");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.selectCategoryDropdown("Documents");
		accountHistoryPage.Clickall24monthslink();
		accountHistoryPage.verifyDownloadPDFLinksinDocumentspage();

	}

	/**
	 * US356343	PII Masking > Variables on Account History Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "sprint" })
	public void testPIIMaskingAccountHistoryPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US356343	PII Masking > Variables on Account History Page");
		Reporter.log("Data Condition - PAH account with Payments history");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch the application | Application Should be Launched");
		Reporter.log("Step 2: Login to the application | User Should be login successfully");
		Reporter.log("Step 3: Verify Home page | Home page should be displayed");
		Reporter.log("Step 4: Click on account history link | Alert and activires page should be displayed");
		Reporter.log("Step 5: verify PII masking | PII masking attributes should be present for all personal information");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.verifyPiiMaskingForSelectedLine(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
		accountHistoryPage.verifyPiiMaskingForPaymentInfoOnGrid(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		accountHistoryPage.verifyPiiMaskingForMsisdnOnGrid(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
		accountHistoryPage.verifyPiiMaskingForPaymentInfoOnScheduledActivity(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
	}

	/**
	 * US387466	SPIKE: Additional Terms 36 Month Filter
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING  , Group.SMOKE})
	public void testAccountHistoryFilterFor36Months(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US387466	Additional Terms 36 Month Filter");
		Reporter.log("Data Condition - PAH account with Payments history");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Launch the application | Application Should be Launched");
		Reporter.log("Step 2: Login to the application | User Should be login successfully");
		Reporter.log("Step 3: Verify Home page | Home page should be displayed");
		Reporter.log("Step 4: Click on account history link | Alert and activires page should be displayed");
		Reporter.log("Step 5: Select for the filter for 36 months | 36months filter should be able to select");
		Reporter.log("Step 6: Check the filter for 36months | Should be able to get all 3 years of data");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AccountHistoryPage accountHistoryPage = navigateToAccountHistoryPage(myTmoData);
		accountHistoryPage.selectDateFromDropDown("All (36 months)");
		accountHistoryPage.verifyAccHistory();
	}

	/**
	 * US242113:PA Messaging Enhancements- Secured PA - OTP Landing Page
	 * (Outside blackout)
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testSecuredPADetailsInScheduleActivity(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Data Conditions| IR PAH With scheduled secure PA upcoming outside blackout period");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Account History link | Account History page should be displayed");
		Reporter.log("Step 4: Verify and get Scheduled PA details(Date, Amount and Payment Method) | PA details should be displayed");
	}

	
	/**
	 * DE150916 MyTMO [Desktop/Mobile - Account. Hist.] Missed PA not indicated in scheduled activity
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testMissedPAAlertInScheduleActivity(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("DE150916 MyTMO [Desktop/Mobile - Account. Hist.] Missed PA not indicated in scheduled activity");
		Reporter.log("Data Condition | IR PAH With scheduled Missed PA Installement");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Click on Account History link | Account History page should be displayed");
		Reporter.log("Step 4: Verify missed PA alert in scheduled activity section | missed PA alert should be displayed");
	}
}

