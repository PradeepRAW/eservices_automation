package com.tmobile.eservices.qa.global.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.global.api.RefillApiV1;

import io.restassured.response.Response;

public class RefillApiV1Test extends RefillApiV1 {
	
	public Map<String, String> tokenMap;
	/**
	/**
	 * UserStory# Description:CDCDWR-247:MyTMO - develop Eos-refill-api Unit Test & Support
	 * 
	 * @param data 
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "RefillApiV1Test", "redeemPrepaidCoupon_used", Group.GLOBAL,Group.SPRINT })
	public void testRedeemPrepaidCoupon_valid_used(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Refill_redeemPrepaidCoupon used test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with Refill_redeemPrepaidCoupon.");
		Reporter.log("Step 2: Verify Refill_redeemPrepaidCoupon details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("pin", "43782673300739");
		String operationName="Refill_redeemPrepaidCoupon";
		String requestBody = new ServiceTest().getRequestFromFile("Refill_redeemPrepaidCoupon.txt");
		Response response =redeemPrepaidCoupon(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			String msisdn = jsonNode.findPath("msisdn").asText();
			Assert.assertEquals(msisdn,apiTestData.getMsisdn(),"Invalid msisdn.");
			JsonNode couponInfo = jsonNode.findPath("couponInfoList").get("couponInfo");
			if (!couponInfo.isMissingNode() && couponInfo.isArray()) {
				JsonNode jsonNode1 = couponInfo.get(0);
				String status = jsonNode1.findPath("status").textValue();
				String couponPin = jsonNode1.findPath("pin").textValue();
				Assert.assertEquals(status, "used", "Coupon status not matched.");
				Assert.assertEquals(couponPin, tokenMap.get("pin"), "Coupon pin not matched.");
			}else {
				failAndLogResponse(response, operationName);
			}
				
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**
	 * UserStory# Description:CDCDWR-247:MyTMO - develop Eos-refill-api Unit Test & Support
	 * 
	 * @param data 
	 * @param myTmoData
	 * @throws Exception 
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "RefillApiV1Test", "redeemPrepaidCouponNotExist", Group.GLOBAL,Group.SPRINT })
	public void testRedeemPrepaidCoupon_notExist(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Refill_redeemPrepaidCoupon pin not exist test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with Refill_redeemPrepaidCoupon.");
		Reporter.log("Step 2: Verify Refill_redeemPrepaidCoupon details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("pin", "11111111111111");
		String operationName="Refill_redeemPrepaidCoupon";
		String requestBody = new ServiceTest().getRequestFromFile("Refill_redeemPrepaidCoupon.txt");
		Response response =redeemPrepaidCoupon(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			String msisdn = jsonNode.findPath("msisdn").asText();
			Assert.assertEquals(msisdn,apiTestData.getMsisdn(),"Invalid msisdn.");
			JsonNode couponInfo = jsonNode.findPath("couponInfoList").get("couponInfo");
			if (!couponInfo.isMissingNode() && couponInfo.isArray()) {
				JsonNode jsonNode1 = couponInfo.get(0);
				String status = jsonNode1.findPath("status").textValue();
				String couponPin = jsonNode1.findPath("pin").textValue();
				Assert.assertEquals(status, "doesNotExist", "Coupon status not matched.");
				Assert.assertEquals(couponPin, tokenMap.get("pin"), "Coupon pin not matched.");
			}else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	/**Note: This tests only works one time for one pin
	 * UserStory# Description:CDCDWR-247:MyTMO - develop Eos-refill-api Unit Test & Support
	 * 
	 * @param data 
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "RefillApiV1Test", "redeemPrepaidCoupon_new", Group.GLOBAL,Group.SPRINT })
	public void testRedeemPrepaidCoupon_valid_new(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Refill_redeemPrepaidCoupon new test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with Refill_redeemPrepaidCoupon new.");
		Reporter.log("Step 2: Verify Refill_redeemPrepaidCoupon new details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("pin", "43782673300739");
		String operationName="Refill_redeemPrepaidCoupon";
		String requestBody = new ServiceTest().getRequestFromFile("Refill_redeemPrepaidCoupon.txt");
		Response response =redeemPrepaidCoupon(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			String msisdn = jsonNode.findPath("msisdn").asText();
			Assert.assertEquals(msisdn,apiTestData.getMsisdn(),"Invalid msisdn.");
			JsonNode couponInfo = jsonNode.findPath("couponInfoList").get("couponInfo");
			if (!couponInfo.isMissingNode() && couponInfo.isArray()) {
				JsonNode jsonNode1 = couponInfo.get(0);
				String couponPin = jsonNode1.findPath("pin").textValue();
				Assert.assertEquals(couponPin, tokenMap.get("pin"), "Coupon pin not matched.");
			}else {
				failAndLogResponse(response, operationName);
			}
				
		} else {
			failAndLogResponse(response, operationName);
		}
	}
}
