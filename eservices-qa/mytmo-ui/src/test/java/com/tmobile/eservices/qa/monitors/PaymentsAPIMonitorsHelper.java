package com.tmobile.eservices.qa.monitors;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.eos.AccountsApiV3;
import com.tmobile.eservices.qa.api.eos.BillingApiV1;
import com.tmobile.eservices.qa.api.eos.CustomerAccountApiV1;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.ActivitiesApiV1;
import com.tmobile.eservices.qa.pages.payments.api.OrchestrateOrdersCollectionV2;
import com.tmobile.eservices.qa.pages.payments.api.PaymentManagerApiV1;
import com.tmobile.eservices.qa.pages.payments.api.SedonaApiCollectionV1;
import com.tmobile.eservices.qa.pages.payments.api.UsageApiV1;
import com.tmobile.eservices.qa.payments.api.PaymentsFdpApiV3Test;

import io.restassured.response.Response;

public class PaymentsAPIMonitorsHelper extends ApiCommonLib {

	public Response response = null;
	public String requestBody = null;
	public String updatedRequest = "";

	public Map<String, String> prepareTokenMap(String requestName, Response response, Map<String, String> tokenMap)	throws Exception {

		switch (requestName) {

		case "getSubScriberLines":
			tokenMap = getSubScriberLines(response, tokenMap);
			break;
		case "getSubScriberAccount":
			tokenMap = getSubScriberAccount(response, tokenMap);
			break;
		case "getPADetails":
			tokenMap = getPADetails(response, tokenMap);
			break;
		case "geSchedulePayment":
			tokenMap = geSchedulePayment(response, tokenMap);
			break;
		case "searchPayment":
			tokenMap = searchPayment(response, tokenMap);
			break;
		case "OTPPayment":
			tokenMap = OTPPayment(response, tokenMap);
			break;
		}
		return tokenMap;
	}

	private Map<String, String> getSubScriberLines(Response response, Map<String, String> tokenMap) {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {
			tokenMap.put("accountType",getPathVal(jsonNode, "accountType"));
			tokenMap.put("accountSubType",getPathVal(jsonNode, "accountSubType"));
		}
		return tokenMap;
	}

	private Map<String, String> getPADetails(Response response, Map<String, String> tokenMap) {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {
			//tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
			//tokenMap.put("quotePrice", getPathVal(jsonNode, "quotePrice"));
		}
		return tokenMap;
	}

	private Map<String, String> geSchedulePayment(Response response, Map<String, String> tokenMap) {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {
			//tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
			//tokenMap.put("quotePrice", getPathVal(jsonNode, "quotePrice"));
		}
		return tokenMap;
	}

	private Map<String, String> searchPayment(Response response, Map<String, String> tokenMap) {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {
			//tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
			//tokenMap.put("quotePrice", getPathVal(jsonNode, "quotePrice"));
		}
		return tokenMap;
	}

	private Map<String, String> OTPPayment(Response response, Map<String, String> tokenMap) {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {
			/*tokenMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
			tokenMap.put("quotePrice", getPathVal(jsonNode, "quotePrice"));*/
		}
		return tokenMap;
	}

	private Map<String, String> getSubScriberAccount(Response response, Map<String, String> tokenMap) {

		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {
			tokenMap.put("lines.msisdn", getPathVal(jsonNode, "lines.msisdn"));
		}
		return tokenMap;
	}

	/*private JsonNode getParentNodeFromResponse(Response response) {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;
		try {
			jsonNode = mapper.readTree(response.asString());
		} catch (JsonProcessingException e) {
			Assert.fail("<b>JsonProcessingException Response :</b> " + e);
			Reporter.log(" <b>JsonProcessingException Response :</b> ");
			Reporter.log(" " + e);
			e.printStackTrace();
		} catch (IOException e) {
			Assert.fail("<b>IOException Response :</b> " + e);
			Reporter.log(" <b>IOException Response :</b> ");
			Reporter.log(" " + e);
			e.printStackTrace();
		}
		return jsonNode;
	}*/

	public Map<String, String> invokeOTPPayment(ApiTestData apiTestData, Map<String, String> tokenMap)throws Exception {
		requestBody = new ServiceTest().getRequestFromFile("OrchestrateOrders_SedonaV2.txt");	
		 String operationName="testOrchestrateOrdersSedonaPayment";
		 String orderId=null;
		 updatedRequest = prepareRequestParam(requestBody, tokenMap);
		 logRequest(updatedRequest, operationName);
		 OrchestrateOrdersCollectionV2 orchestrateOrdersCollectionV2=new OrchestrateOrdersCollectionV2();
		 response = orchestrateOrdersCollectionV2.orchestrateorders_sedona(apiTestData, updatedRequest, tokenMap);
	
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				 String quoteId= getPathVal(jsonNode, "quoteId");
				 orderId= getPathVal(jsonNode, "orderId");
				 Assert.assertEquals(quoteId, "","Invalid quoteId.");
				 Assert.assertEquals(orderId, "","Invalid orderId.");
				 tokenMap.remove("quoteId");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return tokenMap;
	}
	
	public Map<String, String> invokeOTPSedonaPayment(ApiTestData apiTestData, Map<String, String> tokenMap)throws Exception {
		requestBody = new ServiceTest().getRequestFromFile("OrchestrateOrders_SedonaV2.txt");	
		 String operationName="testOrchestrateOrdersSedonaPayment";
		 String orderId=null;
		 updatedRequest = prepareRequestParam(requestBody, tokenMap);
		 logRequest(updatedRequest, operationName);
		 OrchestrateOrdersCollectionV2 orchestrateOrdersCollectionV2=new OrchestrateOrdersCollectionV2();
		 response = orchestrateOrdersCollectionV2.orchestrateorders_sedona(apiTestData, updatedRequest, tokenMap);
	
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				 String quoteId= getPathVal(jsonNode, "quoteId");
				 orderId= getPathVal(jsonNode, "orderId");
				 Assert.assertNotEquals(quoteId, "","Invalid quoteId.");
				 Assert.assertNotEquals(orderId, "","Invalid orderId.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return tokenMap;
	}
	public Map<String, String> invokeFDPPayment(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		tokenMap = new HashMap<String, String>();
		
		String requestBody =  new ServiceTest().getRequestFromFile("PaymentsFdpwithCardV3.txt");	
        
        tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("primaryMsisdn", apiTestData.getPrimaryMsisdn());
		tokenMap.put("email", apiTestData.getPrimaryMsisdn()+"@yopmail.com");
		tokenMap.put("chargeAmount", String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));
		String encryptedCard=getEncryptedCardNumber(apiTestData,tokenMap);
		tokenMap.put("cardNumber", encryptedCard);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
        Calendar calCurr = Calendar.getInstance();
        calCurr.setTime(date);
        calCurr.add(Calendar.DATE, 2);
        Date twoDaysAheadDate = calCurr.getTime();
        tokenMap.put("fdpdate", sdf.format(twoDaysAheadDate));

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, "PaymentsFdpCardApi");
		PaymentsFdpApiV3Test paymentsFdpApiV3Test = new PaymentsFdpApiV3Test();
		response = paymentsFdpApiV3Test.PaymentsFdpApi(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "PaymentsFdpCardApi");
			tokenMap = prepareTokenMap("PaymentsFdpCardApi", response, tokenMap);
		} else {
			failAndLogResponse(response, "PaymentsFdpCardApi");
		}
		return tokenMap;
	}

	public Map<String, String> invokeSearchPayment(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest("searchPayment-GET", "searchPayment");
		AccountsApiV3 accountsApi = new AccountsApiV3();
		response = accountsApi.getStoredPayment(apiTestData, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "searchPayment");
			tokenMap = prepareTokenMap("searchPayment", response, tokenMap);
		} else {
			failAndLogResponse(response, "searchPayment");
		}
		return tokenMap;
	}

	public Map<String, String> invokeGetSchedulePayment(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		PaymentManagerApiV1 paymentManagerApiV1=new PaymentManagerApiV1();
		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest("getSchedulePayment-GET", "getSchedulePayment");
		response = paymentManagerApiV1.schedulePaymentDetails(apiTestData);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "getSchedulePayment");
			tokenMap = prepareTokenMap("getSchedulePayment", response, tokenMap);
		} else {
			failAndLogResponse(response, "getSchedulePayment");
		}
		return tokenMap;
	}

	public Map<String, String> invokeGetPADetails(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		PaymentManagerApiV1 paymentManagerApiV1=new PaymentManagerApiV1();
		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest("getPADetails-GET", "getPADetails");
		response = paymentManagerApiV1.PaymentarrangementDetails(apiTestData);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "getPADetails");
			tokenMap = prepareTokenMap("getPADetails", response, tokenMap);
		} else {
			failAndLogResponse(response, "getPADetails");
		}
		return tokenMap;
	}

	public Map<String, String> invokeGetSubScriberAccount(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		CustomerAccountApiV1 customerAccountApiV1=new CustomerAccountApiV1();
		response = customerAccountApiV1.getCustomerAccountDetails(apiTestData, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "getCustomerAccountDetails");
			tokenMap = prepareTokenMap("getCustomerAccountDetails", response, tokenMap);
		} else {
			failAndLogResponse(response, "getCustomerAccountDetails");
		}
		return tokenMap;
	}

	public Map<String, String> invokeGetSubScriberLines(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		CustomerAccountApiV1 customerAccountApiV1=new CustomerAccountApiV1();
		response = customerAccountApiV1.getSubscriberLines(apiTestData, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "getSubscriberLines");
			tokenMap = prepareTokenMap("getSubscriberLines", response, tokenMap);
		} else {
			failAndLogResponse(response, "getSubscriberLines");
		}
		return tokenMap;
	}

	public void checkEmptyValuesInTokenMap(Map<String, String> tokenMap) throws Exception {
		Iterator<Map.Entry<String, String>> itr = tokenMap.entrySet().iterator();
		StringBuilder exceptionMessage = new StringBuilder();
		exceptionMessage = new StringBuilder();
		if (StringUtils.isNoneEmpty(tokenMap.get("exceptionMessage"))) {
			exceptionMessage.append(tokenMap.get("exceptionMessage"));
		}
		while (itr.hasNext()) {
			Map.Entry<String, String> curr = itr.next();
			if (curr.getValue() == "") {
				exceptionMessage = exceptionMessage.append(" " + curr.getKey() + " is Empty " + "\n");
			}
		}
		if (StringUtils.isNoneEmpty(exceptionMessage)) {
			throw new Exception(exceptionMessage.toString());
		}

	}
	public Map<String, String> invokeAccountActivity(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		ActivitiesApiV1 activitiesApiV1=new ActivitiesApiV1();
		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response = activitiesApiV1.getAccountSummary(apiTestData,tokenMap);
		System.out.println("account activity response"+response);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "getAccountSummary");
			tokenMap = prepareTokenMap("getAccountSummary", response, tokenMap);
		} else {
			failAndLogResponse(response, "getAccountSummary");
		}
		return tokenMap;
	}
	
	

	
	public Map<String, String> invokeGetDataSet(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		BillingApiV1 billingApiV1=new BillingApiV1();
        response = billingApiV1.getDataSet(apiTestData, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "getDataSet");
			tokenMap = prepareTokenMap("getDataSet", response, tokenMap);
		} else {
			failAndLogResponse(response, "getDataSet");
		}
		return tokenMap;
	}
	
	public Map<String, String>  invokeGenerateQuote(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		String requestBody = new ServiceTest().getRequestFromFile("generatequote_OTP_Sedona.txt");	
        String quoteId = null;
        tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("chargeAmount", String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));
		String operationName="generatequote_OTP_Sedona";
		String minAmountToRestoreAccount=null;
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		SedonaApiCollectionV1 SedonaApiCollectionV1_Service = new SedonaApiCollectionV1();
		
		Response response = SedonaApiCollectionV1_Service.generatequote_OTP(apiTestData, updatedRequest, tokenMap);
	
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				 quoteId= getPathVal(jsonNode, "quoteId");
				 tokenMap.put("quoteId", quoteId);
				 minAmountToRestoreAccount= getPathVal(jsonNode, "minAmountToRestoreAccount");
				 Assert.assertNotEquals(quoteId, "","Invalid quoteId.");
				 Assert.assertNotEquals(minAmountToRestoreAccount, "","Invalid minAmountToRestoreAccount.");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
		return tokenMap;
	}
	
	public Map<String, String> invokeUsageSummary(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		requestBody = new ServiceTest().getRequestFromFile("Usage_getUsageSummary.txt");
		String operationName = "Usage_getUsageSummary";
		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		UsageApiV1 usageApiV1 = new UsageApiV1();
		response = usageApiV1.getUsageSummay(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode= getPathVal(jsonNode, "statusCode");
				String statusMessage= getPathVal(jsonNode, "statusMessage");
				Assert.assertNotEquals(statusCode,"","statusCode is Null or Empty.");
				Assert.assertNotEquals(statusMessage,"","statusMessage is Null or Empty.");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return tokenMap;
	}
}
