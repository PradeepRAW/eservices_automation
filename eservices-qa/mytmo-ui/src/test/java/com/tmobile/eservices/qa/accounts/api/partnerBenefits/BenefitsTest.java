package com.tmobile.eservices.qa.accounts.api.partnerBenefits;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.shop.ShopConstants;

import io.restassured.response.Response;

public class BenefitsTest extends AccountsApi {

	public Map<String, String> tokenMap;
	
	@Test( dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testBenefitsServiceForNetflixWhenIsSuspendedTrue(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Making generic component to get the benefit response with  suspended account ");
		Reporter.log("Test data : Enter valid ban and phone number with  suspended account and Netflix soc ");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testBenefitsServiceForNetflixWhenIsSuspendedTrue";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_BENEFITSCL+"BenefitsV1.txt");
		System.out.println(tokenMap);
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner","benefits.registrationLink",
					"benefits.loginRecoveryLink","benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "isSuspended", "true");
			benefits.checkexpectedvalues(response, "benefits[0].partner", "netflix");
			benefits.checkexpectedvalues(response, "benefits[0].registrationLink", "https://www.netflix.com/partner/home?ptoken=A5hMA3No9gH4PytWxv7PxIBNML8");
			benefits.checkexpectedvalues(response, "benefits[0].loginRecoveryLink", "https://www.netflix.com/partner/home?ptoken=A5hMA3No9gH4PytWxv7PxIBNML8");
			benefits.checkexpectedvalues(response, "benefits[0].isEligible", "true");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "false");
		} else {
			failAndLogResponse(response, operationName);
		}	
	}
    
	
	@Test( dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testBenefitsServiceForNetflixWhenIsSuspendedFalse(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Making generic component to get the benefit response with non suspended account ");
		Reporter.log("Test data : Enter valid ban and phone number with non suspended account and Netflix soc ");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testBenefitsServiceForNetflixWhenIsSuspendedFalse";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_BENEFITSCL+"BenefitsV1.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner","benefits.registrationLink",
					"benefits.loginRecoveryLink","benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "isSuspended", "false");
			benefits.checkexpectedvalues(response, "benefits[0].partner", "netflix");
			benefits.checkexpectedvalues(response, "benefits[0].registrationLink", "https://www.netflix.com/partner/home?ptoken=A5hMA3No9gH4PytWxv7PxIBNML8");
			benefits.checkexpectedvalues(response, "benefits[0].loginRecoveryLink", "https://www.netflix.com/partner/home?ptoken=A5hMA3No9gH4PytWxv7PxIBNML8");
			benefits.checkexpectedvalues(response, "benefits[0].isEligible", "true");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "false");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test( dataProvider = "byColumnName", enabled = true, groups = "mock")
	public void testBenefitsServiceForHuluWhenISRegistredFalse(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Making generic component to get the benefit response for Hulu and isRegistered is false for hullu"
				+ "  ");
		Reporter.log("Test data : Enter valid ban and phone number with non suspended account and Hulu ");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testBenefitsServiceForHuluWhenISRegistredFalse";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_BENEFITSCL+"Benefits_hulu.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner","benefits.registrationLink",
					"benefits.loginRecoveryLink","benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "benefits[0].partner", "hulu");
			benefits.checkexpectedvalues(response, "benefits[0].registrationLink", "https://www.google.com/");
			benefits.checkexpectedvalues(response, "benefits[0].loginRecoveryLink", "https://www.google.com/");
			benefits.checkexpectedvalues(response, "benefits[0].isEligible", "true");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "false");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "true");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test( dataProvider = "byColumnName", enabled = true, groups = "Sprint15")
	public void testBenefitsServiceForNetflixWhenISRegistredFalse(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Making generic component to get the benefit response for Netflix and isRegistered is false for hullu"
				+ "  ");
		Reporter.log("Test data : Enter valid ban and phone number with non suspended account and Netflix ");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testBenefitsServiceForNetflixWhenISRegistredFalse";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		
		String requestBody = new ServiceTest().getRequestFromFile("Benefits_Netflix.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner","benefits.registrationLink",
					"benefits.loginRecoveryLink","benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "benefits[0].partner", "Netflix");
			benefits.checkexpectedvalues(response, "benefits[0].registrationLink", "https://www.google.com/");
			benefits.checkexpectedvalues(response, "benefits[0].loginRecoveryLink", "https://www.google.com/");
			benefits.checkexpectedvalues(response, "benefits[0].isEligible", "true");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "false");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "true");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test( dataProvider = "byColumnName", enabled = true, groups = "Sprint15")
	public void testBenefitsServiceForNetflixWhenISRegistredTrue(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Making generic component to get the benefit response for Netflix and isRegistered is true for hullu"
				+ "  ");
		Reporter.log("Test data : Enter valid ban and phone number with non suspended account and Netflix ");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testBenefitsServiceForNetflixWhenISRegistredTrue";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("Benefits_Netflix.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner","benefits.registrationLink",
					"benefits.loginRecoveryLink","benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "benefits[0].partner", "Netflix");
			benefits.checkexpectedvalues(response, "benefits[0].registrationLink", "https://www.amazon.com/");
			benefits.checkexpectedvalues(response, "benefits[0].loginRecoveryLink", "https://www.amazon.com/");
			benefits.checkexpectedvalues(response, "benefits[0].isEligible", "true");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "false");
			benefits.checkexpectedvalues(response, "isSuspended", "false");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	
	@Test( dataProvider = "byColumnName", enabled = true, groups = "sprint15")
	public void testBenefitsServiceForHuluWhenISRegistredTrue(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Making generic component to get the benefit response for Hulu and isRegistered is true for hullu"
				+ "  ");
		Reporter.log("Test data : Enter valid ban and phone number with non suspended account and Hulu ");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testBenefitsServiceForHuluWhenISRegistredTrue";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("Benefits_hulu.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner","benefits.registrationLink",
					"benefits.loginRecoveryLink","benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "benefits[0].partner", "hulu");
			benefits.checkexpectedvalues(response, "benefits[0].registrationLink", "https://www.amazon.com/");
			benefits.checkexpectedvalues(response, "benefits[0].loginRecoveryLink", "https://www.amazon.com/");
			benefits.checkexpectedvalues(response, "benefits[0].isEligible", "true");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "false");
			benefits.checkexpectedvalues(response, "isSuspended", "false");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	
	@Test( dataProvider = "byColumnName", enabled = true,  groups = "sprint15")
	public void testBenefitsServiceForHuluWhenIsRegisteredFalse(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Making generic component to get the benefit response for Hulu when Registered False  ");
		Reporter.log("Test data : Enter valid ban and phone number to get the benefit response for Tidal ");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testBenefitsServiceForHuluWhenIsRegisteredFalse";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("Benefits_Hulu.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner","benefits.registrationLink",
					"benefits.loginRecoveryLink","benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "benefits[0].partner", "hulu");
			benefits.checkexpectedvalues(response, "benefits[0].registrationLink", "https://www.amazon.com/");
			benefits.checkexpectedvalues(response, "benefits[0].loginRecoveryLink","https://www.amazon.com/");
			benefits.checkexpectedvalues(response, "benefits[0].isEligible", "true");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "false");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "false");
			benefits.checkexpectedvalues(response, "isSuspended", "false");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test( dataProvider = "byColumnName", enabled = true,  groups = "CDCWW-694")
	public void testBenefitsServiceForNameidSocNotPresent(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Making generic component to get the benefit response for Name when Soc is not present  ");
		Reporter.log("Test data : Enter valid ban and phone number to get the benefit response for Name id ");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testBenefitsServiceForHuluWhenIsRegisteredFalse";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("Benefits_Nameid.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner",
					"benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "benefits[0].partner", "nameid");
				benefits.checkexpectedvalues(response, "benefits[0].isEligible", "false");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "false");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "false");
			benefits.checkexpectedvalues(response, "isSuspended", "false");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test( dataProvider = "byColumnName", enabled = true,  groups = "CDCWW-694")
	public void testBenefitsServiceForNameidSocPresent(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Making generic component to get the benefit response for Name when Soc is not present  ");
		Reporter.log("Test data : Enter valid ban and phone number to get the benefit response for Name id ");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testBenefitsServiceForHuluWhenIsRegisteredFalse";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("Benefits_Nameid.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner",
					"benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "benefits[0].partner", "nameid");
				benefits.checkexpectedvalues(response, "benefits[0].isEligible", "true");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "false");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "false");
			benefits.checkexpectedvalues(response, "isSuspended", "false");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test( dataProvider = "byColumnName", enabled = true, groups = "CDCWW2-207")
	public void testTokenAPIWhenNoPromoId(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Tocken API call when Promoid is removed ");
		Reporter.log("Test data : Enter valid ban and phone number to get the response ");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testBenefitsServiceForTidalWhenIsRegisteredTrue";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("Benefits_Netflix.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner","benefits.registrationLink",
					"benefits.loginRecoveryLink","benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "benefits[0].partner", "Netflix");
			benefits.checkexpectedvalues(response, "benefits[0].registrationLink", "https://www.netflix.com/partner/home?ptoken=Y1sU4MT8pq3emYB7aKjb4jV1PdI");
			benefits.checkexpectedvalues(response, "benefits[0].loginRecoveryLink","https://www.netflix.com/partner/home?ptoken=Y1sU4MT8pq3emYB7aKjb4jV1PdI");
			benefits.checkexpectedvalues(response, "benefits[0].isEligible", "true");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "false");
			benefits.checkexpectedvalues(response, "isSuspended", "false");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test( dataProvider = "byColumnName", enabled = true, groups = "mock")
	public void testBenefitsServiceForTidalWhenIsRegisteredTrue(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Making generic component to get the benefit response for Tidal when Registered True  ");
		Reporter.log("Test data : Enter valid ban and phone number to get the benefit response for Tidal ");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testBenefitsServiceForTidalWhenIsRegisteredTrue";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_BENEFITSCL+"Benefits_Tidal.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner","benefits.registrationLink",
					"benefits.loginRecoveryLink","benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "benefits[0].partner", "Tidal");
			benefits.checkexpectedvalues(response, "benefits[0].registrationLink", "https://www.amazon.com/");
			benefits.checkexpectedvalues(response, "benefits[0].loginRecoveryLink","https://www.amazon.com/");
			benefits.checkexpectedvalues(response, "benefits[0].isEligible", "true");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "false");
			benefits.checkexpectedvalues(response, "isSuspended", "false");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void testBenefitsService(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : verify benefits response  ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_BENEFITSCL+"Benefits.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getBenefits(apiTestData, updatedRequest);

		String operationName = "Benefits Service";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("benefits.partner").contains("Netflix"));
			Assert.assertTrue(response.jsonPath().getString("benefits.isEligible").contains("true")
					|| response.jsonPath().getString("benefits.isEligible").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("benefits.isRegistered").contains("true")
					|| response.jsonPath().getString("benefits.isRegistered").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("benefits.hasEligiblePlan").contains("true")
					|| response.jsonPath().getString("benefits.hasEligiblePlan").contains("false"));

			Reporter.log("Expected Details are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void testBenefitsService1(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Verify benefits response  ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_BENEFITSCL+"Benefits.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getBenefits(apiTestData, updatedRequest);

		String operationName = "Benefits Service";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("benefits.partner").contains("Netflix"));
			Assert.assertTrue(response.jsonPath().getString("benefits.isEligible").contains("true")
					|| response.jsonPath().getString("benefits.isEligible").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("benefits.isRegistered").contains("true")
					|| response.jsonPath().getString("benefits.isRegistered").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("benefits.hasEligiblePlan").contains("true")
					|| response.jsonPath().getString("benefits.hasEligiblePlan").contains("false"));

			Reporter.log("Expected Details are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "retired")
	public void testBenefitsService_Registered(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : verify netflix registered Customer Scenario ");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_BENEFITSCL+"Benefits.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getBenefits(apiTestData, updatedRequest);

		String operationName = "Benefits Service";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("benefits.partner").contains("Netflix"));
			Assert.assertTrue(response.jsonPath().getString("benefits.isEligible").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("benefits.isRegistered").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("benefits.hasEligiblePlan").contains("true"));
			Assert.assertTrue(response.jsonPath().getString("benefits.loginRecoveryLink")
					.contains("https://www.netflix.com/partner/home?ptoken="));
			Assert.assertTrue(response.jsonPath().getString("benefits.registrationLink")
					.contains("https://www.netflix.com/partner/home?ptoken="));

			Reporter.log("Expected Details are Returned in Response");

		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	@Test( dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testUpdateCallerIdProfinityCheck(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Update caller Id Profinity check ");
		Reporter.log("Test data : Enter valid ban phone number and give first name or last name as bad word");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testUpdateCallerIdProfinityCheck";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("CallerId_ProfinityCheck.txt");
		System.out.println(tokenMap);
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 500) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner","benefits.registrationLink",
					"benefits.loginRecoveryLink","benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "isSuspended", "true");
			benefits.checkexpectedvalues(response, "benefits[0].partner", "netflix");
			benefits.checkexpectedvalues(response, "benefits[0].registrationLink", "https://www.netflix.com/partner/home?ptoken=A5hMA3No9gH4PytWxv7PxIBNML8");
			benefits.checkexpectedvalues(response, "benefits[0].loginRecoveryLink", "https://www.netflix.com/partner/home?ptoken=A5hMA3No9gH4PytWxv7PxIBNML8");
			benefits.checkexpectedvalues(response, "benefits[0].isEligible", "true");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "false");
		} else {
			failAndLogResponse(response, operationName);
		}	
	}
	
	
	@Test( dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testUpdateCallerId(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Update caller Id  ");
		Reporter.log("Test data : Enter valid ban phone number first name and last name");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		String operationName = "testUpdateCaller";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("accountNumber", apiTestData.getBan());
		tokenMap.put("phoneNumber", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("CallerId_ProfinityCheck.txt");
		System.out.println(tokenMap);
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = getBenefitsV1Api(updatedRequest);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			AccountsApi benefits=new AccountsApi();
			String alltags[]={"isSuspended","benefits","benefits.partner","benefits.registrationLink",
					"benefits.loginRecoveryLink","benefits.isEligible","benefits.isRegistered","benefits.hasEligiblePlan","benefits.hasPendingServiceRemoval"};
			for(String tag:alltags)	{
				benefits.checkjsontagitems(response, tag);
			}
			
			String Removedtags[]={"benefits.hasPremiumSOC","benefits.appStoreLinks"};
			for(String tag:Removedtags)	{
				benefits.checkjsontagitemsNotExist(response, tag);
			}
			benefits.checkexpectedvalues(response, "isSuspended", "true");
			benefits.checkexpectedvalues(response, "benefits[0].partner", "netflix");
			benefits.checkexpectedvalues(response, "benefits[0].registrationLink", "https://www.netflix.com/partner/home?ptoken=A5hMA3No9gH4PytWxv7PxIBNML8");
			benefits.checkexpectedvalues(response, "benefits[0].loginRecoveryLink", "https://www.netflix.com/partner/home?ptoken=A5hMA3No9gH4PytWxv7PxIBNML8");
			benefits.checkexpectedvalues(response, "benefits[0].isEligible", "true");
			benefits.checkexpectedvalues(response, "benefits[0].isRegistered", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasEligiblePlan", "true");
			benefits.checkexpectedvalues(response, "benefits[0].hasPendingServiceRemoval", "false");
		} else {
			failAndLogResponse(response, operationName);
		}	
	}
}