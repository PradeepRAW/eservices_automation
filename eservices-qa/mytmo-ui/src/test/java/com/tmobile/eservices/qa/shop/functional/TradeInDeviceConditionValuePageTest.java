package com.tmobile.eservices.qa.shop.functional;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.*;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TradeInDeviceConditionValuePageTest extends ShopCommonLib {
 
    /**
     * US354199 MyTMO > Trade - in Redesign > Device Condition > Display
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RELEASE_READY})
    public void testReDesignDeviceCoditionDisplayONDeviceConditionsPage(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case: US354199: MyTMO > Trade - in Redesign > Device Condition > Display");
        Reporter.log("Data Condition |STD PAH Customer");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on shop link | Shop page should be displayed");
        Reporter.log("5. Click on see all phones | Product list  page should be displayed");
        Reporter.log("6. Select device | Product details page should be displayed");
        Reporter.log("7. Select Monthly payments | Monthly payments is selected");
        Reporter.log("8. Click add to cart button | Line selection page should be displayed");
        Reporter.log("9. Click on First Device | Phone selection page should be displayed");
        Reporter.log("10. Click on Got A Different Phone | Trade-In another device page should be displayed");
        Reporter.log("11. Select trade-in info & click continue |  Device condition page should be displayed");
        Reporter.log(
                "12. verify 'Tell us about the condition of your old phone' header on the Device condition page. |  'Tell us about the condition of your old phone' header should be displayed");
        Reporter.log(
                "13. verify 'Good condition' sub header on the Device condition page. |  'Good condition' sub header should be displayed");
        Reporter.log(
                "14. verify 'All below are true' text under 'Good condition'. |  'All below are true' text under 'Good condition' should be displayed");
        Reporter.log(
                "15. verify following conditions 'Device powers on' ,'No screen damage' &'No liquid damage'  under Good condition. | 'Device powers on' ,'No screen damage' &'No liquid damage'  under Good condition should be displayed");
        Reporter.log("16. verify Good condition device image. | Good condition device image should be displayed");
        Reporter.log(
                "17. verify 'It has issues' sub header on the Device condition page. |  'It has issues' sub header should be displayed");
        Reporter.log(
                "18. verify 'Any of the below applies' text under 'It has issues'. |  'Any of the below applies' text under 'It has issues' should be displayed");
        Reporter.log(
                "19. verify follwing conditions 'Doesn't power on' ,'Has screen damage' & 'Has liquid damage'  under 'It has issues'. | 'Doesn't power on' ,'Has screen damage' & 'Has liquid damage'  under 'It has issues' should be displayed");
        Reporter.log("20. verify Issue with device image. |Issue with device image should be displayed");
        Reporter.log(
                "21. verify 'Not sure what to select ' link  |'Not sure what to select ' link should be displayed");

        Reporter.log("================================");
        Reporter.log("Actual Output:");

        TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = navigateToTradeInDeviceConditionValuePageBySeeAllPhones(
                myTmoData);

        tradeInDeviceConditionValuePage.verifyTellUsConditionHeaderIsDisplayed();
        tradeInDeviceConditionValuePage.verifyDeviceConditionAsItsGood();
        tradeInDeviceConditionValuePage.verifyAllTrueIsDisplayed();
        tradeInDeviceConditionValuePage.verifyDevicePowerOnIsDisplayed();
        tradeInDeviceConditionValuePage.verifyNoScreenDamageIsDisplayed();
        tradeInDeviceConditionValuePage.verifyNoLiquidDamageIsDisplayed();
        tradeInDeviceConditionValuePage.verifyDeviceConditionAsitsGoodImage();
        tradeInDeviceConditionValuePage.verifyDeviceConditionAsItsBad();
        tradeInDeviceConditionValuePage.verifyAnyOfTheBelowIsDisplayed();
        tradeInDeviceConditionValuePage.verifyDoesntPowerOnIsDisplayed();
        tradeInDeviceConditionValuePage.verifyHasScreenDamageIsDisplayed();
        tradeInDeviceConditionValuePage.verifyHasLiquidDamageIsDisplayed();
        tradeInDeviceConditionValuePage.verifyDeviceConditionAsItsBadImage();
        tradeInDeviceConditionValuePage.verifyNotSureWhatToSelectHyperLink();
    }

    /**
     * US354211 MyTMO > Trade in Redesign > Device Condition > Learn more link
     * US347129 MyTMO > Trade - in Redesign > Line selector > CTA's Account
     * US384323 myTMO > Trade in Redesign > Device Condition > URL Account level
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RELEASE_READY})
    public void testTradeinReDesignDeviceCoditionLearnMoreLink(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case: US354211 MyTMO > Trade in Redesign > Device Condition > Learn more link");
        Reporter.log("Data Condition |STD PAH Customer");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on shop link | Shop page should be displayed");
        Reporter.log("5. Click on see all phones | Product list  page should be displayed");
        Reporter.log("6. Select device | Product details page should be displayed");
        Reporter.log("7. Select Monthly payments | Monthly payments is selected");
        Reporter.log("8. Click add to cart button | Line selection page should be displayed");
        Reporter.log("9. Click on First Device |Phone selection page should be displayed");
        Reporter.log("10. Click on Got A Different Phone | Trade-In another device page should be displayed");
        Reporter.log("11. Select trade-in info & click continue |  Device condition page should be displayed");
        Reporter.log("12. click on  'Not sure what to select' link | Popup modal should be displayed");
        Reporter.log(
                "13. verify modal title and modal description | modal title and modal description should be displayed");
        Reporter.log(
                "14. click on  Close button on the modal  | modal should be closed  and user should navigate to the previous state (device condition page");

        Reporter.log("================================");
        Reporter.log("Actual Output:");

        TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = navigateToTradeInDeviceConditionValuePageBySeeAllPhones(
                myTmoData);
        tradeInDeviceConditionValuePage.clickNotSureWhatToSelectHyperLink();
        tradeInDeviceConditionValuePage.verifyPopupModalIsDisplayed();
        tradeInDeviceConditionValuePage.verifyPopupModalHeaderIsDisplayed();
        tradeInDeviceConditionValuePage.verifyPopupModalDescriptionIsDisplayed();
        tradeInDeviceConditionValuePage.clickCloseButtonOfModal();
        tradeInDeviceConditionValuePage.verifyTradeInDeviceCondition();

    }

    /**
     * US487385 :AAL: Trade In Value - (trade in this phone) cta behavior
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PROMO_REGRESSION})
    public void testTradeInPriceInCartPageWithPromotion(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case :US487385 :AAL: Trade In Value - (trade in this phone) cta behavior");
        Reporter.log("Data Condition | PAH User with Promo and AAL Eligible");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Shop link | Shop page should be displayed");
        Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
        Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
        Reporter.log("7. Click Contune button | PLP page should be displayed ");
        Reporter.log("8. Select device | PDP page should be displayed ");
        Reporter.log("9. Click Contune button | Interstitial page should be displayed ");
        Reporter.log("10. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
        Reporter.log("11. Verify Authorable title | Authorable title  should be displayed");
        Reporter.log("12. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
        Reporter.log("13. Click on Continue button | Device condition page should be displayed");
        Reporter.log("14. Verify good condition authorable title | Good condition authorable title should be displayed");
        Reporter.log("15. Select good condition option | Device value page should be displayed");
        Reporter.log("16. Verify Trade-In this Phone CTA | Trade-In this Phone CTA should be displayed");
        Reporter.log("17. Click on Trade-In this Phone CTA | Device protection page should be displayed");
        Reporter.log("18. Click on Continue button | Accessories Plp page should be displayed");
        Reporter.log("19. Click on Skip Accessories button | Cart page should be displayed");
        Reporter.log("20. Verify Standard trade-in price | Standard trade-in price should be displayed");
        Reporter.log("21. Verify Promo price in cart page | Promo price should be displayed in cartPage");

        Reporter.log("================================");
        Reporter.log("Actual Output:");

        navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
        InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
        interstitialTradeInPage.verifyAuthorableTradeInImage();
        interstitialTradeInPage.verifyYesTradeinTile();
        interstitialTradeInPage.clickOnYesWantToTradeInCTA();
        TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
        tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
        tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
        tradeInAnotherDevicePage.verifyContinueButtonEnabled();
        tradeInAnotherDevicePage.clickContinueButton();
        TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(getDriver());
        tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
        tradeInDeviceConditionValuePage.verifyGoodTitleTextOnDeviceConditionAAL();
        tradeInDeviceConditionValuePage.verifyGoodImageOnDeviceConditionAAL();
        tradeInDeviceConditionValuePage.clickOnItsGoodCondition();
        TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = new TradeInDeviceConditionConfirmationPage(getDriver());
        tradeInDeviceConditionConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();
        tradeInDeviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();
        DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
        deviceProtectionPage.verifyDeviceProtectionPage();
        deviceProtectionPage.clickContinueButton();
        CartPage cartPage = new CartPage(getDriver());
        cartPage.verifyCartPage();
        cartPage.verifyTradeInBodyText();
        cartPage.verifyTradeInPromoValueIsDisplayed();
    }

}
