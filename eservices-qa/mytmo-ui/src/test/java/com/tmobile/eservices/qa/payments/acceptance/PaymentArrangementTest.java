package com.tmobile.eservices.qa.payments.acceptance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.PaymentArrangementPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author Srajana
 *
 */
public class PaymentArrangementTest extends PaymentCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(PaymentArrangementTest.class);

	/**
	 * Make Future Dated Payment using new Debit Card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyUserAbleTOMakeFuturedatePayemntDebitcard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakeFuturedatePayemntDebitcard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Make Future Dated Payment using new Debit Card");
		Reporter.log("================================");
		Reporter.log("Exepected Test Stpes");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Click on Payment Arrangement | Payment Arrangements page should be displayed.");
		Reporter.log("6. Click on one installement | One installement amount should be displayed.");
		Reporter.log("7. Click Date Picker | Date Picker dialog box should be displayed");
		Reporter.log("8. Select Future date in Date picker | Selected Date should be displayed correctly");
		Reporter.log(
				"9. Click Balance radio button and Select New Debit Card Radio button| Debit Card section should be displayed.");
		Reporter.log("10. Fill Dedit Card details and click next | Review payment page should be loaded");
		Reporter.log("11. Verify Dedit Card Information | Entered Dedit card details should be displayed correctly.");
		Reporter.log("12. Click on Dedit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("13. Click Submit Button | Payment Confirmation message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		paymentArrangementPage.verifyInstallmentPageDisplayed();
		paymentArrangementPage.clickDatePicker();
		paymentArrangementPage.pickDate();
		paymentArrangementPage.clickinstallemtOne();
		paymentArrangementPage.validateOneInstallment();
		paymentArrangementPage.clickCancelButton();
		paymentArrangementPage.verifyPApageDisplayed();
		paymentArrangementPage.clickpaymentMethod();
		SpokePage spokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), spokePage);
		paymentArrangementPage.verifyAgreeAndSubmitButton();
		paymentArrangementPage.verifyStoredCardNumber(myTmoData.getPayment().getCardNumber());
	}
	/**
	 * Make Future Dated Payment using new Hybrid Card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = {Group.PENDING})
	public void verifyUserAbleTOMakeFuturedatePayemntHybridcard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakeFuturedatePayemntHybridcard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Make Future Dated Payment using new Hybrid Card");
		Reporter.log("================================");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Click on Payment Arrangement | Payment Arrangements page should be displayed.");
		Reporter.log("6. Click on one installement | One installement amount should be displayed.");
		Reporter.log("7. Click Date Picker | Date Picker dialog box should be displayed");
		Reporter.log("8. Select Future date in Date picker | Selected Date should be displayed correctly");
		Reporter.log(
				"9. Click Balance radio button and Select New Debit Card Radio button| Debit Card section should be displayed.");
		Reporter.log("10. Fill Hybrid Card details and click next | Review payment page should be loaded");
		Reporter.log("11. Verify Hybrid Card Information | Entered Hybrid card details should be displayed correctly.");
		Reporter.log("12. Click on Hybrid Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("13. Click Submit Button | Payment Confirmation message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Test Steps");
		
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		paymentArrangementPage.verifyInstallmentPageDisplayed();
		paymentArrangementPage.clickDatePicker();
		paymentArrangementPage.pickDate();
		paymentArrangementPage.clickinstallemtOne();
		paymentArrangementPage.validateOneInstallment();
		paymentArrangementPage.clickCancelButton();
		paymentArrangementPage.verifyPApageDisplayed();
		paymentArrangementPage.clickpaymentMethod();
		SpokePage spokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), spokePage);
		paymentArrangementPage.verifyAgreeAndSubmitButton();
		paymentArrangementPage.verifyStoredCardNumber(myTmoData.getPayment().getCardNumber());
	}

	/**
	 * Verify a stored payment method can be removed via the PA flow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void verifyUserAbleTOMakeremoveStoredcardPA(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakeremoveStoredcardPA method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Verify a Stored Card can be removed in the PA flow");
		Reporter.log("Expected Test Steps");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Click on Payment Arrangement | Payment Arrangements page should be displayed.");
		Reporter.log("6. Click Remove link on Stored bank | Confirmation Dialog box should be displayed");
		Reporter.log("7. Verify Remove confirmation Dialog box | Confirmation Dialog should be displayed");
		Reporter.log("------------------------------");
		Reporter.log("Actual Test Steps");
		
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickpaymentMethod();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickRemoveStoredBankLink();
		otpSpokePage.verifyRemoveStoredCardLink();
	}

	/**
	 * Make Future Dated Payment using new Checking account
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "PaymentArrangementPage")
	public void verifyUserAbleTOMakeFuturedatePayemntcheckingAccount(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakeFuturedatePayemntcheckingAccount method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Make Future Dated Payment using new Checking account");

		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Click on Payment Arrangement | Payment Arrangements page should be displayed.");
		Reporter.log("6. Click on one installement | One installement amount should be displayed.");
		Reporter.log("7. Click Date Picker | Date Picker dialog box should be displayed");
		Reporter.log("8. Select Future date in Date picker | Selected Date should be displayed correctly");
		Reporter.log("9. Click New Checking radio Button | Enter Payment Details page should be displayed");
		Reporter.log("10. Verify Remove confirmation Dialog box | Confirmation Dialog should be displayed");
		Reporter.log(
				"11. Fill all the Payment Account details and click Next button | Review Payment Information should be displayed");
		Reporter.log("12. Click on terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallemtBlade();
		paymentArrangementPage.verifyInstallmentPageDisplayed();
		paymentArrangementPage.clickDatePicker();
		paymentArrangementPage.pickDate();
		paymentArrangementPage.clickinstallemtOne();
		paymentArrangementPage.validateOneInstallment();
		paymentArrangementPage.clickCancelButton();
		paymentArrangementPage.verifyPApageDisplayed();
		paymentArrangementPage.clickpaymentMethod();
		SpokePage spokePage = new SpokePage(getDriver());
		Long accNo= addNewBank(myTmoData.getPayment(), spokePage);
		paymentArrangementPage.verifyAgreeAndSubmitButton();
		paymentArrangementPage.verifySavedBankAccount(accNo.toString());

	}

	/**
	 * 04_Desktop_MC 2 Series BIN_PaymentArrangement_Set amount and Payment
	 * information_Validate system accepts new MC BIN series and identifies card
	 * brand_Validate Charge
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = {Group.PENDING})
	public void verifyUserAbleTOMakePaymentArrangemntwithMCBINCard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakePaymentArrangemntwithMCBINCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make Payment Arrangemnt with MC BINCard");

		Reporter.log("Expected Test Stpes");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Click on Payment Arrangement | Payment Arrangements page should be displayed.");
		Reporter.log("6. Click on one installement | One installement amount should be displayed.");
		Reporter.log("7. Click Date Picker | Date Picker dialog box should be displayed");
		Reporter.log("8. Select Future date in Date picker | Selected Date should be displayed correctly");
		Reporter.log(
				"9. Click Balance radio button and Select New Debit Card Radio button| Debit Card section should be displayed.");
		Reporter.log("10. Fill Dedit Card details and click next | Review payment page should be loaded");
		Reporter.log("11. Verify Dedit Card Information | Entered Dedit card details should be displayed correctly.");
		Reporter.log("12. Click on Dedit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("13. Click Submit Button | Payment Confirmation message should be displayed");
		Reporter.log("================================");

		Reporter.log("Actual Test Steps");

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickpaymentMethod();
		SpokePage spokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), spokePage);
		paymentArrangementPage.verifyAgreeAndSubmitButton();
		paymentArrangementPage.verifyStoredCardNumber(myTmoData.getPayment().getCardNumber());
	}

}