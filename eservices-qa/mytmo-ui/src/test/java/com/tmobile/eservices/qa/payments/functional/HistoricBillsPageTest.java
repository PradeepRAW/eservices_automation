/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.HistoricBillsPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

/**
 * @author charshavardhana
 *
 */
public class HistoricBillsPageTest extends PaymentCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(HistoricBillsPageTest.class);

	/**
	 * P1_TC_Regression_Desktop: Copy the EIP component on the desktop bill
	 * summary page to the desktop bill details page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"merged"})
	public void verifyHistorcibillspageheader(ControlTestData data, MyTmoData myTmoData) {
		
		Reporter.log("Verify Historic bills page header");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billandpay page should be displayed");
		Reporter.log("5. Click on Historic bills link | Historic bills page displayed");
		Reporter.log("6. Check Historic bills page header | Historic bills page header should be Pastbills");
		Reporter.log("================================");
		
		HistoricBillsPage historicbillsPage=navigateToHistoricBillsPage(myTmoData);
		historicbillsPage.checkheadertext("Past Bills");

	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS,"billing"})
	public void verifyallcontentinhistoricalbills(ControlTestData data, MyTmoData myTmoData) {
		
		Reporter.log("Verify Historic bills page header");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billandpay page should be displayed");
		Reporter.log("5. Click on Historic bills link | Historic bills page displayed");
		Reporter.log("6. Check summarypdf,detailed pdf and view bill in each row | summarypdf,detailed pdf and view bill should be in each row");
		Reporter.log("================================");
		
		HistoricBillsPage historicbillsPage=navigateToHistoricBillsPage(myTmoData);
		historicbillsPage.checkheadertext("Past Bills");
		historicbillsPage.checkallpreviousbillscontent();

	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS,"billing"})
	public void verifybacktobillfunctionality(ControlTestData data, MyTmoData myTmoData) {
		
		Reporter.log("Verify Historic bills page header");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billandpay page should be displayed");
		Reporter.log("5. Click on Historic bills link | Historic bills page displayed");
		Reporter.log("6. Click on back to bill link | Britebillpage should be diaplayed");
		Reporter.log("================================");
		
		HistoricBillsPage historicbillsPage=navigateToHistoricBillsPage(myTmoData);
		historicbillsPage.clickBacktobill();
		BillAndPaySummaryPage bbpage=new BillAndPaySummaryPage(getDriver());
		bbpage.verifyPageLoaded();
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS,"billing"})
	public void verifyviewfunctionality(ControlTestData data, MyTmoData myTmoData) {
		
		Reporter.log("Verify Historic bills page header");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billandpay page should be displayed");
		Reporter.log("5. Click on Historic bills link | Historic bills page displayed");
		Reporter.log("6. click on viewbill link | Britebillpage should be diaplayed");
		Reporter.log("================================");
		
		HistoricBillsPage historicbillsPage=navigateToHistoricBillsPage(myTmoData);
		historicbillsPage.clickviewbill();
		BillAndPaySummaryPage bbpage=new BillAndPaySummaryPage(getDriver());
		bbpage.verifyPageLoaded();
	}
}
