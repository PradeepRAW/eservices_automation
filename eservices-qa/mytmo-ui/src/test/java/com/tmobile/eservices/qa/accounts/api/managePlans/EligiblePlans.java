/**
 * 
 */
package com.tmobile.eservices.qa.accounts.api.managePlans;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.api.eos.JWTTokenApi;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EligiblePlans extends AccountsApi {

	public Map<String, String> tokenMap;

	@BeforeMethod(alwaysRun = true)
	public void refreshTokenMap() {
		tokenMapCommon = null;
	}

	/**
	 * UserStory# Account plans api saas:
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "")

	public void VerifyCurrentPlanAutopayDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verify Autopay Details of current plan");
		Reporter.log("Data Conditions:3 line pooled rate plan with Autopay enrolled.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: verify Autopay details for current plan");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "VerifyCurrentPlanAutopayDetails");
			JsonPath jsonPathEvaluator = response.jsonPath();
			Assert.assertTrue(jsonPathEvaluator.getString("accountDetails.autopayDetails.isEligible").contains("true"));
			Assert.assertTrue(jsonPathEvaluator.getString("accountDetails.autopayDetails.isEnrolled").contains("true"));

		} else {
			failAndLogResponse(response, "VerifyCurrentPlanAutopayDetails");
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "")

	public void VerifyCurrentPlanAddons(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verify Current plan add on size");
		Reporter.log("Data Conditions:rate plan wirh account level addons' or line level add ons is present.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: verify account level, line level add ons's are returning in the response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "VerifyCurrentPlanAddons");
			JsonPath jsonPathEvaluator = response.jsonPath();
			Assert.assertTrue(jsonPathEvaluator.getList("accountDetails.currentPlans.addOns.code").size() > 0);

		} else {
			failAndLogResponse(response, "VerifyCurrentPlanAddons");
		}
	}

	public void VerifyAccountlevelAutopayDiscount(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verify autopay discount on account level");
		Reporter.log(
				"Data Conditions:rate plan autopay discount on account level for pooled 2 line msisdn rateplan ex: MAGENTA2");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: verify account level, line level add ons's are returning in the response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "VerifyAccountlevelAutopayDiscount");
			JsonPath jsonPathEvaluator = response.jsonPath();
			Assert.assertTrue(
					jsonPathEvaluator.getString("accountDetails.currentPlans.discount.accountTotal").contains("10"));

		} else {
			failAndLogResponse(response, "VerifyAccountlevelAutopayDiscount");
		}
	}

	public void VerifyCurrentRatePlanListDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verify current plan SOC, Name Tax Treatment, Price and features");
		Reporter.log(
				"Data Conditions:TM1TI2 rate plan is sutable for this Test case as price in below assertion is verifying to 130");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: verify account level, line level add ons's are returning in the response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "VerifyCurrentRatePlanListDetails");
			JsonPath jsonPathEvaluator = response.jsonPath();
			Assert.assertTrue(
					jsonPathEvaluator.getString("accountDetails.currentPlans.ratePlanList.planSoc").contains("TM1TI2"));
			Assert.assertTrue(jsonPathEvaluator.getString("accountDetails.currentPlans.ratePlanList.taxTreatment")
					.contains("TI"));
			Assert.assertTrue(jsonPathEvaluator.getString("accountDetails.currentPlans.ratePlanList.name")
					.contains("T-Mobile ONE"));
			Assert.assertTrue(
					jsonPathEvaluator.getString("accountDetails.currentPlans.ratePlanList.price").contains("130"));
			Assert.assertTrue(
					jsonPathEvaluator.getList("accountDetails.currentPlans.ratePlanList.features.code").size() > 15);

		} else {
			failAndLogResponse(response, "VerifyCurrentRatePlanListDetails");
		}
	}

	public void VerifyCurrentRatePlanAccountStatusDetails(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: verify current plan Account Status Details");
		Reporter.log("Data Conditions:Any Account with status as Open");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: verify account level, line level add ons's are returning in the response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "VerifyCurrentRatePlanAccountStatusDetails");
			JsonPath jsonPathEvaluator = response.jsonPath();
			Assert.assertTrue(jsonPathEvaluator.getString("accountDetails.accountStatus.statusCode").contains("O"));
			Assert.assertTrue(jsonPathEvaluator.getString("accountDetails.isPooled").contains("true"));
			Assert.assertTrue(jsonPathEvaluator.getString("accountDetails.isHybrid").contains("false"));

		} else {
			failAndLogResponse(response, "VerifyCurrentRatePlanAccountStatusDetails");
		}
	}

	public void VerifyEligibleRatePlanSOCandNames(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verify Eligible Rate plan details");
		Reporter.log("Data Conditions:TM1TI2 rate plan is sutable for this Test case");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: verify account level, line level add ons's are returning in the response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "VerifyEligibleRatePlanSOCandNames");
			JsonPath jsonPathEvaluator = response.jsonPath();
			Assert.assertTrue(jsonPathEvaluator.getList("ratePlans.ratePlanList.planSoc").size() == 7);
			assertThat(response.jsonPath().getList("ratePlans.ratePlanList.planSoc"),
					hasItems("MPLSMLFM", "MGMIL2", "MAG55TI2", "TMESNTL2", "MGPLS552", "MGPLSFM", "MAGENTA2"));

			assertThat(response.jsonPath().getList("ratePlans.ratePlanList.planSoc"),
					hasItems("Magenta® Plus Military", "Magenta® Military", "Magenta® Unlimited 55",
							"T-Mobile Essentials ", "Magenta® Plus Unlimited 55", "Magenta® Plus", "Magenta®"));

		} else {
			failAndLogResponse(response, "VerifyEligibleRatePlanSOCandNames");
		}
	}

	public void VerifyEligibleRatePlanPrices(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verify eligible rate plan prices");
		Reporter.log("Data Conditions:TM1TI2 rate plan is sutable for this Test case");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: verify account level, line level add ons's are returning in the response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "VerifyEligibleRatePlanPrices");
			JsonPath jsonPathEvaluator = response.jsonPath();

			assertThat(response.jsonPath().getList("ratePlans.ratePlanList.price"),
					hasItems("110", "90", "80", "100", "100", "150", "130"));

		} else {
			failAndLogResponse(response, "VerifyEligibleRatePlanPrices");
		}
	}

	public void VerifyDefaultDataSOCOfEligibleRatePlans(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: verify eligible rate plan prices");
		Reporter.log("Data Conditions:TM1TI2 rate plan is sutable for this Test case");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: verify account level, line level add ons's are returning in the response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "VerifyDefaultDataSOCOfEligibleRatePlans");
			JsonPath jsonPathEvaluator = response.jsonPath();

			assertThat(response.jsonPath().getList("ratePlans.ratePlanList.defaultDataServiceSoc"),
					hasItems("MGPLS0", "MAGDATA", "MAGDATA", "ESNDATA", "MGPLS0", "MGPLS0", "MAGDATA"));

		} else {
			failAndLogResponse(response, "VerifyDefaultDataSOCOfEligibleRatePlans");
		}
	}

	public void VerifyimageUrlandDescriptionOfEligibleRatePlans(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: verify eligible rate plan prices");
		Reporter.log("Data Conditions:TM1TI2 rate plan is sutable for this Test case");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: verify account level, line level add ons's are returning in the response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "VerifyimageUrlandDescriptionOfEligibleRatePlans");
			JsonPath jsonPathEvaluator = response.jsonPath();

			assertThat(response.jsonPath().getList("ratePlans.ratePlanList.imageUrl"),
					hasItems("/content/dam/t-mobile/brandassets/magenta-plus-military.svg",
							"/content/dam/t-mobile/brandassets/magenta-military.svg",
							"/content/dam/t-mobile/brandassets/magenta-unlimited-55.svg",
							"/content/dam/t-mobile/brandassets/T-Mobile-Essentials.svg",
							"/content/dam/t-mobile/brandassets/magenta-plus-unlimited-55.svg",
							"/content/dam/t-mobile/brandassets/magenta-plus.svg",
							"/content/dam/t-mobile/brandassets/magenta.svg"));

			assertThat(response.jsonPath().getList("ratePlans.ratePlanList.description"), hasItems(
					"Unlimited talk, text, high-speed data, plus Netflix, HD video streaming, and much more for our customers with a qualifying U.S. Military affiliation. All taxes and fees are included.",
					"Unlimited talk, text, high-speed data, plus Netflix, and high-speed mobile hotspot for our customers with a qualifying U.S. Military affiliation. All taxes and fees are included.",
					"Unlimited talk, text, high-speed data, and high-speed mobile hotspot for customers 55 and older. All taxes and fees are included.",
					"Get just the essentials: unlimited talk, text, and high-speed data.",
					"Unlimited talk, text, high-speed data, plus Netflix, HD video streaming, and high-speed mobile hotspot for customers 55 and older. All taxes and fees are included.",
					"Unlimited talk, text, high-speed data, plus Netflix, HD video streaming, high-speed mobile hotspot, and much more.  All taxes and fees are included.",
					"Unlimited talk, text, high-speed data, plus Netflix, and high-speed mobile hotspot. All taxes and fees are included."));

		} else {
			failAndLogResponse(response, "VerifyimageUrlandDescriptionOfEligibleRatePlans");
		}
	}

	public void VerifyAutoPayDiscountforEligibleRatePlans(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: verify Autopay Discounts for Eligible Rate plan");
		Reporter.log("Data Conditions: TMO ONE pooled TI account ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: verify account level, line level add ons's are returning in the response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "VerifyAutoPayDiscountforEligibleRatePlans");
			JsonPath jsonPathEvaluator = response.jsonPath();
			Assert.assertTrue(jsonPathEvaluator.getList("ratePlans.autoPayDiscount").size() == 7);
			Assert.assertTrue(jsonPathEvaluator.getList("ratePlans[0].autoPayDiscount").contains("10"));
			Assert.assertTrue(jsonPathEvaluator.getList("ratePlans[1].autoPayDiscount").contains("10"));
			Assert.assertTrue(jsonPathEvaluator.getList("ratePlans[2].autoPayDiscount").contains("10"));
			Assert.assertTrue(jsonPathEvaluator.getList("ratePlans[3].autoPayDiscount").contains("10"));
			Assert.assertTrue(jsonPathEvaluator.getList("ratePlans[4].autoPayDiscount").contains("10"));
			Assert.assertTrue(jsonPathEvaluator.getList("ratePlans[5].autoPayDiscount").contains("10"));
			Assert.assertTrue(jsonPathEvaluator.getList("ratePlans[6].autoPayDiscount").contains("10"));

		} else {
			failAndLogResponse(response, "VerifyAutoPayDiscountforEligibleRatePlans");
		}
	}

	public void VerifyEligiblePlansTaxTreatmentforcurrentTMOONEPlanPooledPlan(ControlTestData data,
			ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verify Eligible Rate plan's tax treatment");
		Reporter.log("Data Conditions: TMO ONE pooled TI account ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: verify account level, line level add ons's are returning in the response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "VerifyAutoPayDiscountforEligibleRatePlans");
			JsonPath jsonPathEvaluator = response.jsonPath();
			Assert.assertTrue(jsonPathEvaluator.getList("ratePlans.ratePlanList.taxTreatment").size() == 7);
			assertThat(response.jsonPath().getList("ratePlans.ratePlanList.taxTreatment"),
					hasItems("TI", "TI", "TI", "TE", "TI", "TI", "TI"));

		} else {
			failAndLogResponse(response, "VerifyAutoPayDiscountforEligibleRatePlans");
		}
	}

	public void VerifyHYBRID_MBBEligiblePlans(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: verify eligible plans details of current TE MBB line");
		Reporter.log("Data Conditions: TMO ONE pooled TI account ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: ");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getAccountPlansMethod(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "VerifyAutoPayDiscountforEligibleRatePlans");
			JsonPath jsonPathEvaluator = response.jsonPath();

			assertThat(response.jsonPath().getList("ratePlans.ratePlanList.taxTreatment"),
					hasItems("TE", "TE", "TE", "TE", "TE", "TE"));

			assertThat(response.jsonPath().getList("ratePlans.ratePlanList.planSoc"),
					hasItems("NAMI2GB", "NAMI14GB", "NAMI18GB", "NAMI22GB", "NAMI6GB", "ONDEMMI"));

			assertThat(response.jsonPath().getList("ratePlans.ratePlanList.autoPayDiscount"),
					hasItems("5", "5", "5", "5", "5", "0"));
			assertThat(response.jsonPath().getList("ratePlans.ratePlanList.type"),
					hasItems("MBB", "MBB", "MBB", "MBB", "MBB", "MBB"));

//				monthly total and eligible plan price difference =210
//				[
//				  220,
//				  265,
//				  280,
//				  295,
//				  235,
//				  215
//				]
//
//				[
//				  10,
//				  55,
//				  70,
//				  85,
//				  25,
//				  0
//				]

		} else {
			failAndLogResponse(response, "VerifyAutoPayDiscountforEligibleRatePlans");
		}
	}
}
