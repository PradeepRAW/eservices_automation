/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.accounts.AddOnDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.LineDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.PlanDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.global.PhonePage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class LineDetailsPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(LineDetailsPageTest.class);

	// Regression Start

	/*
	 * US386426 - Acct. Overview: Lines Detail: Display Line's Plan US380045 - Acct.
	 * Overview: Lines List: Lines and Devices Component US380318 - Acct Overview:
	 * Lines List_Display Cost of Associated Lines
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyDeviceDetailsInLineDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyDeviceDetailsInLineDetailsPage");
		Reporter.log("Test Case : test Device and line details page");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Home page should be launched ");
		Reporter.log("2.Click on Account link  |Account overview page should be displayed");
		Reporter.log("3.Click on Line | Line details page should be displayed");
		Reporter.log("4.Verify Plan name |Plan name should be displayed");
		Reporter.log("5.Verify line device icon | Line device icon should be displayed");
		Reporter.log("6.Click on Device Support | Phone page should be displayed on top ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.verifyPlanName();
		linedetailspage.verifyDeviceIcon();
		linedetailspage.clickOnDeviceSupport();

		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyProfileandLine(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyProfileandLine");
		Reporter.log("Test Case : Verify Profile and Line");
		Reporter.log("Test Data :  PAH  customer");
		Reporter.log("==================================");
		Reporter.log("1.Launch the application | Home page should be launched ");
		Reporter.log("2.Click on Account link  |Account overview page should be displayed");
		Reporter.log("3.Click on Line | Line details page should be displayed");
		Reporter.log("4.Click on profile and line | Profile page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.clickOnProfileAndLine();

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void verifyMyPromotions(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyMyPromotions");
		Reporter.log("Test Case : Verify My Promotions page ");
		Reporter.log("Test Data :  Promotions data  customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Home page should be launched ");
		Reporter.log("2.Click on Account link  |Account overview page should be displayed");
		Reporter.log("3.Click on Line | Line details page should be displayed");
		Reporter.log("4.Click on My Promotions | Promotions page should be Displayed");
		Reporter.log("4.Click on Back button | Line details page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.clickOnMypromotions();

		verifyMyPromotionsPage();
		linedetailspage.clickOnBackBtn();
		linedetailspage.verifyLineDetailsPage();
	}

	/*
	 * VerifyFeaturelandingPagefromplanDetailsPage US414637 : Feature Details:
	 * Landing on Features Detail Page VerifyFeaturenamefromlineDetailsPage US414647
	 * : Feature Details Page_Display Feature Name
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testLineDetailsPageActiveAddOns(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testLineDetailsPageActiveAddOns");
		Reporter.log("Test Case : Navigate to Feature Details page from Plan details page in Account Level");
		Reporter.log("Test Data : Any PAH/standard customer");

		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log("5.Verify that List of Plan Feature | All the feature should be displayed on plan details Page ");
		Reporter.log("6.Clik on the feature from the listed |Application should navigate to Feature Details Page ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.clickOnActiveAddOnsByName("");

		AddOnDetailsPage addOnDetailsPage = new AddOnDetailsPage(getDriver());
		addOnDetailsPage.verifyAddOnDetailsPage();
		addOnDetailsPage.clickOnBackCTA();
		linedetailspage.verifyLineDetailsPage();
	}

	/*
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_CPS })
	public void testLineDetailspage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testLineDetailspage");
		Reporter.log("Test Data : Any PAH/standard customer");

		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the plan name | User should be navigated to PlanDetails Page ");
		Reporter.log("5.Verify that List of Plan Feature | All the feature should be displayed on plan details Page ");
		Reporter.log("6.Clik on the feature from the listed |Application should navigate to Feature Details Page ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage lineDetailsPage = navigateToLineDetailsPage(myTmoData);

		lineDetailsPage.verifyLineDetailsPage();
		lineDetailsPage.verifyPlanDetailsPageHeaders();
		lineDetailsPage.verifymsdin();
		lineDetailsPage.verifyPlanName();
		lineDetailsPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnBackBtn();

		lineDetailsPage.verifyLineDetailsPage();
		lineDetailsPage.clickOnFirstactiveaddOnsList();

		AddOnDetailsPage addOnDetailsPage = new AddOnDetailsPage(getDriver());
		addOnDetailsPage.verifyAddOnDetailsPage();
		addOnDetailsPage.clickOnBackCTA();

		lineDetailsPage.verifyLineDetailsPage();
		lineDetailsPage.clickManageaddOnButton();
	}

	// Regression Scripts END

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyAddTVService(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAddTVService");
		Reporter.log("Test Case : verifyAddTVService");
		Reporter.log("Test Data :  PAh data  customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on Add Tv Service| benefits page shoud be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.clickOnAddTVService();
	}

	/*
	 * US484579 : [Nickname] Pop Up Modal _Save and Cancel CTA Functionality
	 * US484576 : [Nickname] Edit Modal Design and Functionality US484568 : Nickname
	 * editing/tapping capability in Line Details Page
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void modalDesignforNicknameEditing(ControlTestData data, MyTmoData myTmoData) {
		logger.info("testLineDetailspage");
		Reporter.log("Test Data : Any PAH/standard customer");

		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the line name | User should be navigated to lineDetails Page ");
		Reporter.log("5.Click on PencilIcon | Modal PopUd Window should be displayed ");
		Reporter.log(
				"6.Enter first and last name and click on Save button |Both First and last name updaed succesfully ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnLineName();

		// Verify the Edit name window

		LineDetailsPage lineDetailsPage = new LineDetailsPage(getDriver());
		lineDetailsPage.verifyLineDetailsPage();
		lineDetailsPage.clcikOnpencilIcon();
		lineDetailsPage.verifyEditNameWindow();

		// Enter both First and Last name and click on Save button

		lineDetailsPage.enterFirstName();
		lineDetailsPage.lastFirstName();
		lineDetailsPage.clickSaveButton();

		// either Successful or UnSuccesful

		lineDetailsPage.updatedSuccesful();

		// click on Cancel button

		lineDetailsPage.clickCancelButton();

		// verify the name after updated
	}

	/*
	 * US516977: Display overridePriceDescription
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyOverrideTextOnLineDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyChangePlanButton is called in PlanDetailsPageTest");
		Reporter.log("Test Case : VerifyChangePlanButton");
		Reporter.log("Test Data :  StandatedNetflix PAH customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Line | User should be navigated to LineDetails Page ");
		Reporter.log("5.Verify that Price for Netflix addon | User should be displayed as On Us ");
		Reporter.log("6.Click on the Neflix Addon | Application should navigatet to Addon Details Page ");
		Reporter.log("7.Verify that Price for Netflix addon | User should be displayed as On Us ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnLineName();

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();

		AddOnDetailsPage addonDetailsPage = new AddOnDetailsPage(getDriver());
		addonDetailsPage.verifyActiveAddonComponent();
		addonDetailsPage.clickOnActiveAddonAssociatedline();
		addonDetailsPage.verifyNetflixStandaredAddonOverridePrice();
		addonDetailsPage.verifyAddonPage();
		addonDetailsPage.verifyOnUs();
	}

	/*
	 * US516849 : [ACM] Navigate voluntarily suspended lines to restore flow
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyVoluntarilySuspendedErrorMessageforRestrictedCustomer(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyChangePlanButton is called in PlanDetailsPageTest");
		Reporter.log("Test Case : verifyVoluntarilySuspendedErrorMessageforRestrictedCustomer");
		Reporter.log("Test Data : Restricted customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Line Details page should be displayed  ");
		Reporter.log("4.Veify that Suspended Error Message| Suspended Error message should be displayed ");
		Reporter.log("5.Click on Error Message | User should be navigated to PhoneDetails Page ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToFutureURLFromHome(myTmoData, "account-overview");

		// AccountOverviewPage accountoverviewpage = new
		// AccountOverviewPage(getDriver());

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		// linedetailspage.verifyLineSuspendedErrorMessage();
		linedetailspage.verifySuspnedErrorMessageforRestoreLine();

		/*
		 * PhonePage phonePage = new PhonePage(getDriver());
		 * phonePage.verifyPhonePage();
		 */
	}

	/*
	 * US516849 : [ACM] Navigate voluntarily suspended lines to restore flow
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyVoluntarilySuspendedErrorMessageforPAHCustomer(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyChangePlanButton is called in PlanDetailsPageTest");
		Reporter.log("Test Case : verifyVoluntarilySuspendedErrorMessageforPAHCustomer");
		Reporter.log("Test Data : PAH Customer customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Line | User should be navigated to LineDetails Page ");
		Reporter.log("5.Veify that Suspended Error Message| Suspended Error message should be displayed ");
		Reporter.log("6.Click on Error Message | User should be navigated to PhoneDetails Page ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.verifyRetoreErrorMessage();
	}

	/*
	 * US493431 : [ACM] Displaying DIGITS Link only for DIGITS lines
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyManageDigitsSettingsonlineDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyChangePlanButton is called in PlanDetailsPageTest");
		Reporter.log("Test Case : verifyVoluntarilySuspendedErrorMessageforPAHCustomer");
		Reporter.log("Test Data : PAH Customer customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the Virtual Digits Line | User should be navigated to LineDetails Page ");
		Reporter.log("5.Veify that manage DIGITS Settings| manage DIGITS Settings should be available ");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.clickOnDigitsline();

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
		linedetailspage.verifyManageDigitsSetting();
	}

	/*
	 * US520956 : [ACM] Including Shared Add-ons in add-ons count
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifySharedandActiveAddonCountinAcctOverviewPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAllBackButtonScenariosInAccountOveview");
		Reporter.log("Test Case : verifyAllBackButtonScenariosInAccountOveview");
		Reporter.log("Test Data : Any PAH/Full customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log("4.Click on the line | User should be navigated to line details Page ");
		Reporter.log("5.Click on HeaderBackArrow | User should be navigated to AccountOverview Page");
		Reporter.log("6.Click on monthly total| User should be navigated to CostDetails Page ");
		Reporter.log("7.Click on BrowserBackArrow | User should be navigated to AccountOverview Page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage lineDetailsPage = navigateToLineDetailsPage(myTmoData);

		lineDetailsPage.verifyactiveaddOnsComponent();
		lineDetailsPage.verifyActiveAddonsCount();
	}

	/*
	 * Verify alert text for PAH User
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "Sprint")
	public void verifyAlertsOnLineDetailsPageForPAHUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAlertsOnLineDetailsPageForPAHUser");
		Reporter.log("Test Case : verify Alerts On Line Details Page For PAHUser");
		Reporter.log("Test Data : Any PAHor Full Permission User");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on Account Overview page| Account Overview page should be displayed");
		Reporter.log("Click on ");
		Reporter.log("Click on Alert ");
		Reporter.log("Verify Alert on line details page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		AccountOverviewPage accountOverviewPage = navigateToAccountoverViewPageFlow(myTmoData);
		accountOverviewPage.verifyAlertsOnAccountOverviewPage();
		accountOverviewPage.clickOnAlertsOnAccountOverviewPage();

		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
		linedetailspage.verifyAlert();
	}

	/*
	 * Verify alert text for Restricted User
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "Sprint")
	public void verifyAlertsOnLineDetailsPageForRestrictedUser(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyAlertsOnLineDetailsPageForRestrictedUser");
		Reporter.log("Test Case : verify Alerts On Line Details Page For Restricted User");
		Reporter.log("Test Data : Any Restricted customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on Account Overview page| Line-details Page displayed");
		Reporter.log("3.Verify Alert on the Line details Page");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		homePage.clickPlanLink();
		LineDetailsPage linedetailspage = new LineDetailsPage(getDriver());
		linedetailspage.verifyLineDetailsPage();
		linedetailspage.verifyAlert();
	}

	/*
	 * US401815 - Acct Overview: Line Details: Manage Add-Ons CTA Logic US387403 -
	 * Acct. Overview: Lines Detail: Device Details Component US406775 - Acct.
	 * Overview: Lines Detail: Device Details Component_Device Support US387735 -
	 * Acct. Overview: Line Details:Equipment Financing Component US388273 - Acct.
	 * Overview: Line Details: Settings Component US388603 - Acct. Overview: Lines
	 * Detail: Contact us Link
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyLineDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifythatDetailsinLineDetailsPage");
		Reporter.log("Test Case : VerifythatDetailsinLineDetailsPage");
		Reporter.log("Test Data : Any PAH/standard customer");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1.Launch the application | Application should be launched ");
		Reporter.log("2.Click on plan page | Plan Landing page should be displayed");
		Reporter.log("3.DeepLinking the Account Overview page | Account Overview home page should be displayed  ");
		Reporter.log(
				"4.Click on the line from AccOVerview page | Application should be navigated to LineDetails Page ");
		Reporter.log(
				"5.Verify that AddOn CTA on LineDetails page | ManageAddonCTA should be available on LineDetails Page");
		Reporter.log(
				"6.Verify that Device component details on LineDetails Page | Device component details should be displayed ");
		Reporter.log("7.Verify that Device support on LineDetails Page | DeviceSupport should be displayed ");
		Reporter.log("8.Verify that Equipment Financing on LineDetails Page | Equipment Financing should be displayed");
		Reporter.log("9.Verify that Settings on LineDetails Page | Settings should be displayed");
		Reporter.log(
				"10.Verify that Equipment Contact us Link on LineDetails Page | Contact us Link should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.verifyPlanDetailsPageHeaders();
		linedetailspage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();

		plandetailspage.clickOnbreadcrumbLInks(1);

		linedetailspage.clickOnActiveAddOnsByName("T-Mobile ONE Plus");

		AddOnDetailsPage addOnDetailsPage = new AddOnDetailsPage(getDriver());
		addOnDetailsPage.verifyAddOnDetailsPage();
		addOnDetailsPage.clickOnBackCTA();
	}

}
