package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.PrivacyAndNotificationsPage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;

public class PrivacyAndNotificationsPageTest extends AccountsCommonLib {

	/**
	 * In-Progress 4253625344 / workL!f3balanceACT Marketing Communications: Ensure
	 * user can toggle on/off for myphone, my email, my billing address, monthly
	 * t-mobile e-newsletter
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyMarketingCommunicationToggle(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log(
				"5. Click on privacy and notifications link |Privacy and notifications page should be be displayed");
		Reporter.log("6. Click on marketing communication link | Marketing communication page  should be displayed");
		Reporter.log("7. Verify text my phone header | Text my phone header should be displayed");
		Reporter.log("8. Verify and click on text my phone toggle | Text my phone toggle should be clicked");
		Reporter.log("9. Verify send me mail header | Send me mail header should be displayed");
		Reporter.log("10.Verify and click on send me mail toggle | Send me mail toggle should be clicked");
		Reporter.log("11.Verify receive news letter header | Receive news letter header should be displayed");
		Reporter.log(
				"12.Verify and click on receive news letter toggle | Receive news letter toggle should be clicked");
		Reporter.log("13.Verify mail to billing address header | Mail to billing address should be displayed");
		Reporter.log(
				"14.Verify and click on mail to billing address toggle | Mail to billing address toggle should be clicked");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PrivacyAndNotificationsPage privacyAndNotificationsPage = navigateToPrivacyAndNotificationsPage(myTmoData,
				"Privacy and Notifications");
		privacyAndNotificationsPage.verifyPrivacyAndNotificationsPage();
		privacyAndNotificationsPage.verifyPageHeader();
		privacyAndNotificationsPage.clickMarketingCommunicationsLink();
		privacyAndNotificationsPage.verifyHeaderByName("Text or Call My Phone");
		privacyAndNotificationsPage.clickTextMyPhoneToggle();
		privacyAndNotificationsPage.verifyHeaderByName("Send Me Email");
		privacyAndNotificationsPage.clickSendMeEmailToggle();
		privacyAndNotificationsPage.verifyHeaderByName("Receive Monthly T-Mobile Newsletter");
		privacyAndNotificationsPage.clickRecieveMonthlyNewsletterToggle();
		privacyAndNotificationsPage.verifyHeaderByName("Mail to My Billing Address");
		privacyAndNotificationsPage.clickMailToMyAddresstoggle();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyNotificationsOptions(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log(
				"5. Click on privacy and notifications link |Privacy and notifications page should be be displayed");
		Reporter.log("6. Click on notifications link | Notifications  page should be displayed");
		Reporter.log("7. Verify tmobile notifications header | TMobile notifications header should be displayed");
		Reporter.log(
				"8. Verify and click on tmobile notifications toggle | TMobile notifications toggle should be clicked");
		Reporter.log("9. Verify recieve alerts header | Recieve alerts header should be displayed");
		Reporter.log("10.Verify and click on recieve alerts toggle | Recieve alerts toggle should be clicked");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PrivacyAndNotificationsPage privacyAndNotificationsPage = navigateToPrivacyAndNotificationsPage(myTmoData,
				"Privacy and Notifications");
		privacyAndNotificationsPage.verifyPageHeader();
		privacyAndNotificationsPage.clickNotificationsLink();
		privacyAndNotificationsPage.verifyHeaderByName("T-Mobile will always send notifications");
		privacyAndNotificationsPage.clickTMobileNotificationsToggle();
		privacyAndNotificationsPage.verifyHeaderByName("Receive alerts when other");
		privacyAndNotificationsPage.verifyHeaderByName("T-Mobile will always send notifications");

		privacyAndNotificationsPage.clickReceiveAlertsToggle();
	}

	/**
	 * US321854:Profile Page | Mail to my billing address option should only be
	 * available to PAH users.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyMailToMyBillingAddressOptionForPAHUsers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log(
				"5. Click on privacy and notifications link |Privacy and notifications page should be be displayed");
		Reporter.log("6. Click on marketing communication link | Marketing communication page  should be displayed");
		Reporter.log("7. Verify mail to my billing address option | Mail to my billing address should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PrivacyAndNotificationsPage privacyAndNotificationsPage = navigateToPrivacyAndNotificationsPage(myTmoData,
				"Privacy and Notifications");
		privacyAndNotificationsPage.verifyPageHeader();
		privacyAndNotificationsPage.clickMarketingCommunicationsLink();
		privacyAndNotificationsPage.verifyHeaderByName("Mail to My Billing Address");
	}

	/**
	 * US327336/US454528:Profile | Re-Launch Enhancements | Rename Home |
	 * Breadcrumbs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyPrivacyAndNotificationsBreadCrumbsLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log(
				"5. Click on privacy and notifications link |Privacy and notifications page should be be displayed");
		Reporter.log("6. Click on marketing communication link | Marketing communication page  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PrivacyAndNotificationsPage privacyAndNotificationsPage = navigateToPrivacyAndNotificationsPage(myTmoData,
				"Privacy and Notifications");
		privacyAndNotificationsPage.verifyPrivacyAndNotificationsPage();
		privacyAndNotificationsPage.verifyPageHeader();
		privacyAndNotificationsPage.clickMarketingCommunicationsLink();

		privacyAndNotificationsPage.verifyBreadCrumbActiveStatus("Marketing Communications");
		privacyAndNotificationsPage.clickBreadCrumb("Privacy & Notifications");
		privacyAndNotificationsPage.verifyPrivacyAndNotificationsPage();
		privacyAndNotificationsPage.verifyBreadCrumbActiveStatus("Privacy & Notifications");
		privacyAndNotificationsPage.clickProfileHomeBreadCrumb();
		privacyAndNotificationsPage.verifyBreadCrumbActiveStatus("Profile");

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}

}
