package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.CartPage;
import com.tmobile.eservices.qa.pages.tmng.functional.SDPPage;
import com.tmobile.eservices.qa.pages.tmng.functional.SLPPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class SDPPageTest extends TmngCommonLib {
	/**
	 * TA1655289: Prod : Inventory status not displayed when there is a SIM in cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.NON_MOBILE })
	public void testVerifyInventoryStatusOnSDPwithSIMandPhoneLineOnCart(TMNGData tMNGData) {
		Reporter.log("Test Case : TA1655289: Prod : Inventory status not displayed when there is a SIM in cart");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | Cart page Should be Launched");
		Reporter.log("2. click on Add a phone tile |Cart  page should be displayed");
		Reporter.log("3. Click Device Plus button | Add a new Phone and BYOD should be displayed");
		Reporter.log(
				"4. Click on Add a new phone and click done on credit class pop up| Mini PLP page should be displayed");
		Reporter.log("5. Select any phone from Mini PLP| Mini PDP page should be displayed");
		Reporter.log("6. Click Add to cart on Mini PDP| Cart page should be displayed");
		Reporter.log(
				"7. Click on Add a Tablet/Wearable CTA and click Bring your own tablet | Internet Sim PDP should be displayed");
		Reporter.log("8. Click Add to cart on InternetSim mini PDP| Cart page should be displayed");
		Reporter.log("9. Verify storeName CTA | storeName CTA should be displayed");
		Reporter.log("10. Click storeName CTA | Store detailed page should be displayed");
		Reporter.log("11. Verify Inventory status in SDP | Inventory status should be displayed on SDP");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		/*
		 * cartPage.clickOnBYODLinkOnCartEssentials();
		 * 
		 * MiniPDPPage miniPdpdPage = new MiniPDPPage(getDriver());
		 * miniPdpdPage.verifyMiniPDPPageLoaded(); miniPdpdPage.clickSimCardDropdown();
		 * miniPdpdPage.clickSimCardTabletDropdownOption();
		 * miniPdpdPage.verifyInternetSIMKitMiniPDP();
		 * miniPdpdPage.clickAddToCartCTAAtMiniPDPPage();
		 * 
		 * cartPage.verifyCartPageLoaded(); cartPage.clickDuplicateCTA();
		 * cartPage.verifyCartPageLoaded(); cartPage.clickEditCTA();
		 * miniPdpdPage.verifyMiniPDPPageLoaded(); miniPdpdPage.changeColorVarient();
		 * miniPdpdPage.clickAddToCartCTAAtMiniPDPPage();
		 */

		cartPage.verifyCartPageLoaded();
		cartPage.verifyStoreName();
		cartPage.clickStoreLocatorName();

		SDPPage sdpPage = new SDPPage(getDriver());
		sdpPage.verifyStoredetailsPage();
		sdpPage.verifyInventoryTextOnSDPPage();
		sdpPage.clickSDPModalClose();

		cartPage.verifyCartPageLoaded();
		cartPage.clickChangeStoreLocation();
		SLPPage slpPage = new SLPPage(getDriver());

		slpPage.verifyStoreLocatorListPage();
		if (slpPage.verifyshowItemsInStockSlp()) {
			slpPage.clickShowHideItemsOnSLP();
			slpPage.verifyItemListonClickingShowItemsCTAOnStoreListPage();
		} else {
			Reporter.log("All items are in stock");
		}
	}

	/**
	 * US481922 TMO - IV Accessories Accessibility - SDP from Cart US481923 TMO - IV
	 * Accessories Accessibility - SDP from Multi Cart US433886: TMO - IV Fast
	 * Follower: Accessories on SDP US306684: [Continued] TMO - IV - SDP from Cart
	 * US352086 TMO - IV - SDP Map
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.NON_MOBILE })
	public void testIVAccessoriesSDPFromCart(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US481922	TMO - IV Accessories Accessibility - SDP from Cart");
		Reporter.log("Test Case : US481923	TMO - IV Accessories Accessibility - SDP from Multi Cart");
		Reporter.log("Test Case :  US433886: TMO - IV Fast Follower: Accessories on SDP");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessories should be displayed");
		Reporter.log("4. Select any Accessory | Accessory PDP Page should be selected");
		Reporter.log("5. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("6. Click on 'ADD AN ACCESSORY' | Accessory Mini PLP Page should be displayed");
		Reporter.log("7. Select any Accessory from Mini PLP | Accessory MiniPDP Page should be displayed");
		Reporter.log("8. Click on Add to Cart button on Mini PDP | Cart page should be displayed");
		Reporter.log("9. Verify 'Store Locator' CTA | 'Store Locator' CTA should be selected");
		Reporter.log("10. Click on  'Store Locator' CTA |Store detailed page  should be displayed");
		Reporter.log("11. Verify Stores detailed modal |Stores Detailed modal  should be displayed");
		Reporter.log(
				"12. Verify Inventory status on SDP |Product Inventory Hurry, Only a few left or  In Srock or  Contact store for availability should be displayed");
		Reporter.log(
				"13. verify inventory status   text(i.e In stock,1 item in stock) | inventory status text should be displayed");
		Reporter.log(
				"14. Click Show items CTA  | List of items avalable on selected store and inventory staus should be displayed");
		Reporter.log("15. Verify Hide items CTA  | Hide item CTA should be displayed");
		Reporter.log("16. Click Hide items CTA  | List of items should be collapsed");
		Reporter.log("17. Click on (X) cross symbol  | Cart page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingAccessory(tMNGData);
		cartPage.clickStoreLocatorName();

		SDPPage sdpPage = new SDPPage(getDriver());
		sdpPage.verifyStoredetailsPage();
		sdpPage.verifyInventoryTextOnSDPPage();
		sdpPage.storeInfoTextOnStoreDetailsPage();
		sdpPage.clickSDPModalClose();
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.verifyAllProductsStoreName();
		cartPage.clickStoreLocatorName();
		sdpPage.verifyStoredetailsPage();
		sdpPage.verifyInventoryTextOnSDPPage();
		if (sdpPage.verifyOutofStockMsgOnSDP()) {
			Reporter.log("All items are out of stock on SDP");
		} else {
			sdpPage.clickShowHideItemsOnSDP();
			sdpPage.verifyItemsInStoreDisplayedOnSDPCartPage();
			sdpPage.verifyShowHideCTADisplayedOnSDPCartPage();
			sdpPage.clickShowHideItemsOnSDP();
			sdpPage.verifyItemsInStoreNotDisplayedOnSDPCartPage();
		}
		sdpPage.clickSDPModalClose();
		cartPage.verifyCartPageLoaded();
	}

}
