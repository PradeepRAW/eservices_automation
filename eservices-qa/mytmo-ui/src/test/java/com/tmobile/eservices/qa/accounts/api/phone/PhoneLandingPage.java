
package com.tmobile.eservices.qa.accounts.api.phone;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.api.eos.DeviceUnlockStatus;
import com.tmobile.eservices.qa.api.eos.JWTTokenApi;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PhoneLandingPage extends DeviceUnlockStatus {

	String environment;

	public Map<String, String> tokenMap;

	@Test(groups = "CDCWW-879")
	public void validatePhoneLandingPageDeviceDetailsRequestWithOneMisdin(ApiTestData apiTestData) throws Exception {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();
		request.header("Accept", "application/json");
		request.header("Accept-Encoding", "gzip, deflate");
		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("Cache-Control", "no-cache");
		request.header("Connection", "keep-alive");
		request.header("Content-Type", "application/json");
		request.header("Host", "eos.corporate.t-mobile.com");
		request.header("application_id", "ESERVICE");
		request.header("cache-control", "no-cache");
		request.header("interactionId", "123456787");

		String operationName = "";

		JSONObject requestParams = new JSONObject();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceDetails.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		response = getDeviceDetailsResponse(updatedRequest);
		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/getDevices");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating Phone landing page Device details ");
		Reporter.log("Note : Phone landing page device details ");
		Reporter.log("Test data : ");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("statusCode").contains("200"));
			Assert.assertTrue(response.jsonPath().getString("message").contains("Success"));
			Assert.assertTrue(response.jsonPath().getString("processingTime").contains("0ms"));
			Assert.assertTrue(response.jsonPath().getString("totalResultCount").contains("1"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(groups = "CDCWW-879")
	public void validatePhoneLandingPageDeviceDetailsRequestWithOnetac(ApiTestData apiTestData) throws Exception {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();
		request.header("Accept", "application/json");
		request.header("Accept-Encoding", "gzip, deflate");
		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("Cache-Control", "no-cache");
		request.header("Connection", "keep-alive");
		request.header("Content-Type", "application/json");
		request.header("Host", "eos.corporate.t-mobile.com");
		request.header("application_id", "ESERVICE");
		request.header("cache-control", "no-cache");
		request.header("interactionId", "123456787");

		String operationName = "";

		JSONObject requestParams = new JSONObject();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceDetails.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		response = getDeviceDetailsResponse(updatedRequest);
		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/getDevices");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating Phone landing page Device details ");
		Reporter.log("Note : Phone landing page device details ");
		Reporter.log("Test data : ");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("statusCode").contains("200"));
			Assert.assertTrue(response.jsonPath().getString("message").contains("Success"));
			Assert.assertTrue(response.jsonPath().getString("processingTime").contains("0ms"));
			Assert.assertTrue(response.jsonPath().getString("totalResultCount").contains("1"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(groups = "CDCWW-879")
	public void validatePhoneLandingPageDeviceDetailsRequestWithmultipleDeviceSpecNodes(ApiTestData apiTestData)
			throws Exception {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();
		request.header("Accept", "application/json");
		request.header("Accept-Encoding", "gzip, deflate");
		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("Cache-Control", "no-cache");
		request.header("Connection", "keep-alive");
		request.header("Content-Type", "application/json");
		request.header("Host", "eos.corporate.t-mobile.com");
		request.header("application_id", "ESERVICE");
		request.header("cache-control", "no-cache");
		request.header("interactionId", "123456787");

		String operationName = "";

		JSONObject requestParams = new JSONObject();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceDetails.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		response = getDeviceDetailsResponse(updatedRequest);
		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/getDevices");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating Phone landing page Device details ");
		Reporter.log("Note : Phone landing page device details ");
		Reporter.log("Test data : ");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("Code").contains("TOOP"));
			Assert.assertTrue(response.jsonPath().getString("name").contains("Out of Pocket"));
			Assert.assertTrue(response.jsonPath().getString("Level").contains("line"));
			Assert.assertTrue(response.jsonPath().getString("incuredAt").contains("immediate"));
			Assert.assertTrue(response.jsonPath().getString("amount").contains("0"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(groups = "CDCWW-879")
	public void validatePhoneLandingPageDeviceDetailsRequestWithmultipleMsisdns(ApiTestData apiTestData)
			throws Exception {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();
		request.header("Accept", "application/json");
		request.header("Accept-Encoding", "gzip, deflate");
		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("Cache-Control", "no-cache");
		request.header("Connection", "keep-alive");
		request.header("Content-Type", "application/json");
		request.header("Host", "eos.corporate.t-mobile.com");
		request.header("application_id", "ESERVICE");
		request.header("cache-control", "no-cache");
		request.header("interactionId", "123456787");

		String operationName = "";

		JSONObject requestParams = new JSONObject();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceDetails.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		response = getDeviceDetailsResponse(updatedRequest);
		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/getDevices");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating Phone landing page Device details ");
		Reporter.log("Note : Phone landing page device details ");
		Reporter.log("Test data : ");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("deviceOferId").contains("559"));
			Assert.assertTrue(response.jsonPath().getString("name").contains("Jet"));
			Assert.assertTrue(response.jsonPath().getString("splitShipmentEligible").contains("true"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(groups = "CDCWW-879")
	public void validatePhoneLandingPageDeviceDetailsRequestWithmultipletacs(ApiTestData apiTestData) throws Exception {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();
		request.header("Accept", "application/json");
		request.header("Accept-Encoding", "gzip, deflate");
		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("Cache-Control", "no-cache");
		request.header("Connection", "keep-alive");
		request.header("Content-Type", "application/json");
		request.header("Host", "eos.corporate.t-mobile.com");
		request.header("application_id", "ESERVICE");
		request.header("cache-control", "no-cache");
		request.header("interactionId", "123456787");

		String operationName = "";

		JSONObject requestParams = new JSONObject();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceDetails.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		response = getDeviceDetailsResponse(updatedRequest);
		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/getDevices");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating Phone landing page Device details ");
		Reporter.log("Note : Phone landing page device details ");
		Reporter.log("Test data : ");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("deviceOfferId").contains("TOOP"));
			Assert.assertTrue(response.jsonPath().getString("deviceCompositeId").contains("Out of Pocket"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(groups = "CDCWW-879")
	public void validatePhoneLandingPageDeviceDetailsRequestWithInvalidMsisdn(ApiTestData apiTestData)
			throws Exception {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();
		request.header("Accept", "application/json");
		request.header("Accept-Encoding", "gzip, deflate");
		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("Cache-Control", "no-cache");
		request.header("Connection", "keep-alive");
		request.header("Content-Type", "application/json");
		request.header("Host", "eos.corporate.t-mobile.com");
		request.header("application_id", "ESERVICE");
		request.header("cache-control", "no-cache");
		request.header("interactionId", "123456787");

		String operationName = "";

		JSONObject requestParams = new JSONObject();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceDetails.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		response = getDeviceDetailsResponse(updatedRequest);
		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/getDevices");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating Phone landing page Device details ");
		Reporter.log("Note : Phone landing page device details ");
		Reporter.log("Test data : ");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 500) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("error").contains("Internal Server Error"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(groups = "CDCWW-879")
	public void validatePhoneLandingPageDeviceDetailsRequestWithInvalidtac(ApiTestData apiTestData) throws Exception {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();
		request.header("Accept", "application/json");
		request.header("Accept-Encoding", "gzip, deflate");
		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("Cache-Control", "no-cache");
		request.header("Connection", "keep-alive");
		request.header("Content-Type", "application/json");
		request.header("Host", "eos.corporate.t-mobile.com");
		request.header("application_id", "ESERVICE");
		request.header("cache-control", "no-cache");
		request.header("interactionId", "123456787");

		String operationName = "";

		JSONObject requestParams = new JSONObject();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceDetails.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		response = getDeviceDetailsResponse(updatedRequest);
		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/getDevices");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating Phone landing page Device details ");
		Reporter.log("Note : Phone landing page device details ");
		Reporter.log("Test data : ");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 500) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("error").contains("Internal Server Error"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(groups = "CDCWW-879")
	public void validatePhoneLandingPageDeviceDetailsRequestWithMsisdinNIMEI(ApiTestData apiTestData) throws Exception {

		Response response = null;

		// public static void main(String args[]) throws Exception {

		environment = System.getProperty("environment");
		RestAssured.baseURI = environment;

		RequestSpecification request = RestAssured.given();
		request.header("Accept", "application/json");
		request.header("Accept-Encoding", "gzip, deflate");
		request.header("Authorization", "WdYiXZ0M867K2jeHUI4Blxdpe77v");
		request.header("Cache-Control", "no-cache");
		request.header("Connection", "keep-alive");
		request.header("Content-Type", "application/json");
		request.header("Host", "eos.corporate.t-mobile.com");
		request.header("application_id", "ESERVICE");
		request.header("interactionId", "123456787");

		String operationName = "";

		JSONObject requestParams = new JSONObject();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String requestBody = new ServiceTest().getRequestFromFile("getDeviceDetails.txt");
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		response = getDeviceDetailsResponse(updatedRequest);
		request.body(requestParams.toString());

		request.trustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		response = request.post("/myPhone/getDevices");

		int statusCode = response.getStatusCode();
		System.out.println("The status code recieved: " + statusCode);
		System.out.println(response.prettyPrint());

		Reporter.log("Test Case : Validating Phone landing page Device details ");
		Reporter.log("Note : Phone landing page device details ");
		Reporter.log("Test data : ");

		// System.out.println("Response body: " + response.asString());
		if (response.body().asString() != null && response.getStatusCode() == 200) {

			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("name").contains("GT-S5200C"));
			Assert.assertTrue(response.jsonPath().getString("displayName").contains("none"));
		} else {
			System.out.println("output Failure");
			Assert.fail("invalid response");
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = "testphonee0s")
	public void testGetPhonePageDeviceDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Test Phone Page Device Details");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Verify get devices Success responce ");
		Reporter.log("Step 2: get IMEI");
		Reporter.log("Step3: Pass IMEI and verify Device Features and Images ");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v3");
		
		AccountsApi phonePage=new AccountsApi();

		Map<String, String> jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);

		Response response = getDeviceDtlsIMEI(apiTestData, jwtTokens.get("jwtToken"));

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, "testGetPhonePageDeviceDetails");
			JsonPath jsonPathEvaluator = response.jsonPath();
			if (jsonPathEvaluator.get("addOns") != null) {
				JsonNode addOnsNode = jsonPathEvaluator.get("addOns");
				JsonNode codeNode = addOnsNode.get(0);
				Assert.assertNotNull(codeNode.asText());
			}

		} else {
			failAndLogResponse(response, "testGetPhonePageDeviceDetails");
		}
	}

}