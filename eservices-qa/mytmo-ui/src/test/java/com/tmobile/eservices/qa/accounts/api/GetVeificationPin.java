package com.tmobile.eservices.qa.accounts.api;

import org.testng.annotations.Test;

import com.tmobile.eservices.qa.api.unlockdata.RequestData;
import com.tmobile.eservices.qa.api.RestServiceHelper;

public class GetVeificationPin {

	@Test(groups = "pin")
	public void getTextMessagePin() {

		String msisdnn = System.getProperty("msisdn");

		String smsPin = null;
		RequestData requestData = new RequestData();
		String environment = "ppd";

		requestData.setMsisdn(msisdnn);
		requestData.setEnvironment(environment);
		requestData.setProfileType("TMO");

		try {
			smsPin = new RestServiceHelper().getTextMessagePin(requestData);
			System.out.println("msisdn" + "  " + msisdnn);
			System.out.println("TwoFactorPin" + "  " + smsPin);
		} catch (Exception e) {
			System.out.println("Unable to get Text message pin from service");
		}

	}
}
