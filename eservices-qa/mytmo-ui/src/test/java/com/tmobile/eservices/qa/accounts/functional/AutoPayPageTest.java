package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.AutoPayPage;
import com.tmobile.eservices.qa.pages.accounts.BillingAndPaymentsPage;

public class AutoPayPageTest extends GlobalCommonLib{
	
	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.ACCOUNTS,Group.DESKTOP,Group.ANDROID,Group.IOS})
	public void testAutopayPageLanding(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on Billing and Payments link | Billing and Payments page should be displayed");
		Reporter.log("5. Click on Autopay link| Autopay page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		BillingAndPaymentsPage billingAndPaymentsPage = navigateToBillingAndPaymentsPage(myTmoData,"Billing & Payments");
		billingAndPaymentsPage.clickAutoPayBtn();
		
		AutoPayPage autoPayPage=new AutoPayPage(getDriver());
		autoPayPage.verifyAutoPayPage();
	}
}
