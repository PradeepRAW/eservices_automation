/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.accounts.PrepaidBillingAndPaymentPage;
import com.tmobile.eservices.qa.pages.accounts.PrepaidProfileLandingPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class PrepaidBillingAndPaymentsPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(PrepaidBillingAndPaymentsPageTest.class);

	/*
	 * CDCAM-860 [Prepaid Profile Page .NET Migration] Profile Page_Billing
	 * &Payments TAB
	 * 
	 */
/*	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkBillingAndPaymentsTabAndURL(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkBillingAndPaymentsTabAndURL");
		Reporter.log("Test Case : Check Billing & Payments page url and header,subheader and sub tabs");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Billing and Payments tab | Billing and payments page should be displayed");
		Reporter.log("6. Verify URL, Header,Subheader. | It should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickProfileMenu();
		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.verifyPrePaidProfilePage();

		
		 * navigateToProfilePage(myTmoData); PrepaidProfileLandingPage
		 * prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		 * prepaidProfilePage.clickBillingAndPaymentsTab();
		 
		PrepaidBillingAndPaymentPage prepaidBillingAndPaymentPage = new PrepaidBillingAndPaymentPage(getDriver());
		prepaidBillingAndPaymentPage.verifyBillingandPaymentsPage();

		prepaidBillingAndPaymentPage.clickOnBilingAddressTab();

		prepaidBillingAndPaymentPage.checkHeaderOnBilingAddressPage();
		prepaidBillingAndPaymentPage.getAddressLine1();
		// prepaidBillingAndPaymentPage.postNewAddressLine1();
		prepaidBillingAndPaymentPage.getAddressLine2();
		prepaidBillingAndPaymentPage.getCityName();

		prepaidBillingAndPaymentPage.getSelectedState();
		prepaidBillingAndPaymentPage.getZipCode();

	}
*/
	/*
	 * 
	 * 
	 */
/*	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkWhetherUserAbleToUpdateAddress1Field(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkWhetherUserAbleToUpdateAddress1Field");
		Reporter.log(
				"Test Case : Check when user update only Address1 field. then verifying on address page whether user able to see updated address or not.");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login a the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Billing and Payments tab | Billing and payments page should be displayed");
		Reporter.log("6. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log("7. Update Address1 only and click Save Changes CTA. | User should be able to Update Address1");
		Reporter.log("8. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log("9. Verify Address1,Address2,City,State and Zipcode. | It should be matched.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		
		 * navigateToHomePage(myTmoData);
		 * 
		 * PrepaidProfileLandingPage prepaidProfilePage = new
		 * PrepaidProfileLandingPage(getDriver());
		 * prepaidProfilePage.directURLForPrepaidProfilePage();
		 * prepaidProfilePage.verifyPrePaidProfilePage();
		 
		navigateToProfilePage(myTmoData);
		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.clickBillingAndPaymentsTab();

		PrepaidBillingAndPaymentPage prepaidBillingAndPaymentPage = new PrepaidBillingAndPaymentPage(getDriver());
		prepaidBillingAndPaymentPage.verifyBillingandPaymentsPage();

		prepaidBillingAndPaymentPage.clickOnBilingAddressTab();

		prepaidBillingAndPaymentPage.checkHeaderOnBilingAddressPage();
		prepaidBillingAndPaymentPage.updateOnlyValidAddressLine1();
	}
*/
	/*
	 * 
	 * 
	 */
/*	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkWhetherUserAbleToUpdateAddress1AndAddress2Fields(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkWhetherUserAbleToUpdateAddress1Field");
		Reporter.log(
				"Test Case : Check when user update only Address1 field. then verifying on address page whether user able to see updated address or not.");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login a the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Billing and Payments tab | Billing and payments page should be displayed");
		Reporter.log("6. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log(
				"7. Update Address1 and Address2 and click Save Changes CTA. | User should be able to Update Address1 & Address2");
		Reporter.log("8. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log("9. Verify Address1,Address2,City,State and Zipcode. | It should be matched.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		
		 * navigateToHomePage(myTmoData);
		 * 
		 * PrepaidProfileLandingPage prepaidProfilePage = new
		 * PrepaidProfileLandingPage(getDriver());
		 * prepaidProfilePage.directURLForPrepaidProfilePage();
		 * prepaidProfilePage.verifyPrePaidProfilePage();
		 

		navigateToProfilePage(myTmoData);
		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.clickBillingAndPaymentsTab();

		PrepaidBillingAndPaymentPage prepaidBillingAndPaymentPage = new PrepaidBillingAndPaymentPage(getDriver());
		prepaidBillingAndPaymentPage.verifyBillingandPaymentsPage();

		prepaidBillingAndPaymentPage.clickOnBilingAddressTab();

		prepaidBillingAndPaymentPage.checkHeaderOnBilingAddressPage();
		prepaidBillingAndPaymentPage.updateValidAddressLine1AndAddressLine2();
	}
*/
	/*
	 * 
	 * 
	 */
	/*@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkWhetherUserAbleToUpdateCityWithoutChangingState(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkWhetherUserAbleToUpdateCityWithoutChangingState");
		Reporter.log(
				"Test Case : Check when user update Address line1,line2 and City. Then will check whether Zip code automatically updates or not.");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login a the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Billing and Payments tab | Billing and payments page should be displayed");
		Reporter.log("6. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log(
				"7. Update Address1,Address2 and City name and click Save Changes CTA. | User should be able to Update Address1, Address2 & City");
		Reporter.log("8. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log("9. Verify Address1,Address2,City,State and Zipcode. | It should be matched.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		
		 * navigateToHomePage(myTmoData);
		 * 
		 * PrepaidProfileLandingPage prepaidProfilePage = new
		 * PrepaidProfileLandingPage(getDriver());
		 * prepaidProfilePage.directURLForPrepaidProfilePage();
		 * prepaidProfilePage.verifyPrePaidProfilePage();
		 

		navigateToProfilePage(myTmoData);
		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.clickBillingAndPaymentsTab();

		PrepaidBillingAndPaymentPage prepaidBillingAndPaymentPage = new PrepaidBillingAndPaymentPage(getDriver());
		prepaidBillingAndPaymentPage.verifyBillingandPaymentsPage();

		prepaidBillingAndPaymentPage.clickOnBilingAddressTab();

		prepaidBillingAndPaymentPage.checkHeaderOnBilingAddressPage();
		prepaidBillingAndPaymentPage.updateValidAddressLine1AndAddressLine2AndCity()
		prepaidBillingAndPaymentPage.updateValidAddress1Address2CityButNotUpdatingStateWithAllValidFields();
	}
*/
	/*
	 * 
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkWhetherUserAbleToUpdateAllFieldsOfAddressOrNot(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkWhetherUserAbleToUpdateAllFieldsOfAddressOrNot");
		Reporter.log(
				"Test Case : Check when user update Address line1,line2,City,State and Zip. Then will check whether it's updated or not.");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login a the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Billing and Payments tab | Billing and payments page should be displayed");
		Reporter.log("6. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log(
				"7. Update Address1,Address2,City,State and Zipcode.And click Save Changes CTA. | User should be able to Update Address");
		Reporter.log("8. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log("9. Verify Address1,Address2,City,State and Zipcode. | It should be matched.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);

		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.directURLForPrepaidProfilePage();
		prepaidProfilePage.verifyPrePaidProfilePage();
		prepaidProfilePage.clickBillingAndPaymentsTab();
		
		PrepaidBillingAndPaymentPage prepaidBillingAndPaymentPage = new PrepaidBillingAndPaymentPage(getDriver());
		prepaidBillingAndPaymentPage.verifyBillingandPaymentsPage();
		
		prepaidBillingAndPaymentPage.clickOnBilingAddressTab();
		prepaidBillingAndPaymentPage.checkHeaderOnBilingAddressPage();
		//prepaidBillingAndPaymentPage.updateCompleteValidAddress()
		prepaidBillingAndPaymentPage.updateValidAddressWithAllValidFields();
	}

	/*
	 * 
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkWhenUserEnteredValidAddressButEnteredInvalidCityCheckWhetherAbleToSaveOrNot(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("checkWhetherUserAbleToUpdateAllFieldsOfAddressOrNot");
		Reporter.log(
				"Test Case : Check when user update Address line1,line2,City,State and Zip. Then will check whether it's updated or not.");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login a the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Billing and Payments tab | Billing and payments page should be displayed");
		Reporter.log("6. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log(
				"7. Update Address1,Address2,City,State and Zipcode.And click Save Changes CTA. | User should be able to Update Address");
		Reporter.log("8. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log("9. Verify Address1,Address2,City,State and Zipcode. | It should be matched.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);

		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.directURLForPrepaidProfilePage();
		prepaidProfilePage.verifyPrePaidProfilePage();
		prepaidProfilePage.clickBillingAndPaymentsTab();
		
		PrepaidBillingAndPaymentPage prepaidBillingAndPaymentPage = new PrepaidBillingAndPaymentPage(getDriver());
		prepaidBillingAndPaymentPage.verifyBillingandPaymentsPage();
		
		prepaidBillingAndPaymentPage.clickOnBilingAddressTab();
		prepaidBillingAndPaymentPage.checkHeaderOnBilingAddressPage();
		prepaidBillingAndPaymentPage.updateValidAddressWithWrongCity();
	}

	/*
	 * 
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkWhenUserEnteredValidAddressButEnteredInvalidStateCheckWhetherAbleToSaveOrNot(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("checkWhetherUserAbleToUpdateAllFieldsOfAddressOrNot");
		Reporter.log(
				"Test Case : Check when user update Address line1,line2,City,State and Zip. Then will check whether it's updated or not.");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login a the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Billing and Payments tab | Billing and payments page should be displayed");
		Reporter.log("6. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log(
				"7. Update Address1,Address2,City,State and Zipcode.And click Save Changes CTA. | User should be able to Update Address");
		Reporter.log("8. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log("9. Verify Address1,Address2,City,State and Zipcode. | It should be matched.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToHomePage(myTmoData);

		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.directURLForPrepaidProfilePage();
		prepaidProfilePage.verifyPrePaidProfilePage();
		prepaidProfilePage.clickBillingAndPaymentsTab();
		
		PrepaidBillingAndPaymentPage prepaidBillingAndPaymentPage = new PrepaidBillingAndPaymentPage(getDriver());
		prepaidBillingAndPaymentPage.verifyBillingandPaymentsPage();
		
		prepaidBillingAndPaymentPage.clickOnBilingAddressTab();
		prepaidBillingAndPaymentPage.checkHeaderOnBilingAddressPage();
		prepaidBillingAndPaymentPage.updateValidAddressWithWrongState();
	}
	/*
	 * 
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void checkWhenUserEnteredValidAddressButEnteredInvalidZIPCheckWhetherAbleToSaveOrNot(ControlTestData data,
			MyTmoData myTmoData) {
		logger.info("checkWhetherUserAbleToUpdateAllFieldsOfAddressOrNot");
		Reporter.log(
				"Test Case : Check when user update Address line1,line2,City,State and Zip. Then will check whether it's updated or not.");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login a the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on Billing and Payments tab | Billing and payments page should be displayed");
		Reporter.log("6. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log(
				"7. Update Address1,Address2,City,State and Zipcode.And click Save Changes CTA. | User should be able to Update Address");
		Reporter.log("8. Click on Billing Address tab. | Billing Address page should be displayed");
		Reporter.log("9. Verify Address1,Address2,City,State and Zipcode. | It should be matched.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");


		navigateToHomePage(myTmoData);

		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.directURLForPrepaidProfilePage();
		prepaidProfilePage.verifyPrePaidProfilePage();
		prepaidProfilePage.clickBillingAndPaymentsTab();
		
		PrepaidBillingAndPaymentPage prepaidBillingAndPaymentPage = new PrepaidBillingAndPaymentPage(getDriver());
		prepaidBillingAndPaymentPage.verifyBillingandPaymentsPage();
		
		prepaidBillingAndPaymentPage.clickOnBilingAddressTab();
		prepaidBillingAndPaymentPage.checkHeaderOnBilingAddressPage();
		//prepaidBillingAndPaymentPage.updateValidAddress1CityStateButWrongZipCode();
		prepaidBillingAndPaymentPage.updateValidAddressWithWrongZip();

	}


}