package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.PromotionApiV2;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class PromotionsApiV2Test extends PromotionApiV2 {
	
	/**
	 * UserStory#US548315 Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	JsonPath jsonPath;
	public Map<String, String> tokenMap;
	//browse
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PromotionsApiV1Test", "testSearch_promotions", Group.SHOP, Group.APIREG, "12321" })
	public void testSearch_promotions(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Search  Promotions");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Search Promotions return for the specific Line (Msisdn)");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 2: Verify Trade in model in response |Trade in items should be present");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = "{\r\n  \"shopper\": {\r\n    \"salesChannel\": \"WEB\",\r\n    \"accountType\": \"I\",\r\n    \"accountSubType\": \"R\",\r\n    \"crpid\": \"CRP1\"\r\n  },\r\n  \"promotions\" : {\r\n    \"transactionType\" : \"UPGRADE\"\r\n}\r\n}";		
		
		
		String operationName="search_promotions";
		tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = search_promotions(apiTestData, updatedRequest,tokenMap);
		logRequest(updatedRequest, operationName);
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue("200".equals(Integer.toString(response.getStatusCode())), "Search Promotions Successfull");
				if(null!=response){			
					JSONObject jsonObject = new JSONObject(getJSONfromResponse(response));
					jsonPath= new JsonPath(response.asString());
					Assert.assertNotNull(jsonObject.get("promotions"));
					Assert.assertEquals(jsonPath.get("promotions[0].itemsInCondition[1].itemType"),"TRADEINMODEL", "Trade in model not present in promotions");
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

	}

	//price
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "PromotionsApiV1Test", "testPromotions_prices", Group.SHOP,Group.APIREG })
	public void testPromotions_prices(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Promotions_prices");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Promotions Prices return for the specific sku");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="promotions_prices";
		String requestBody = "{ \r\n  \"crpId\": \"1\", \r\n  \"priceType\": [ \r\n    \"EIP\" \r\n  ], \r\n  \"skuCode\": [ \r\n   \""+apiTestData.getSku()+"\" \r\n  ], \r\n  \"promotionDetails\": { \r\n    \"transactionType\": \"UPGRADE\", \r\n    \"salesChannel\": \"WEB\", \r\n    \"promoName\": \"ss\" \r\n  } \r\n}";		
		logRequest(requestBody, operationName);
		Response response = promotions_prices(apiTestData, requestBody,tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue("200".equals(Integer.toString(response.getStatusCode())), "Search Promotions Successfull");
				if(null!=response){			
					JSONObject jsonObject = new JSONObject(getJSONfromResponse(response));
					Assert.assertNotNull(jsonObject.get("skuItems"));					
				}
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

	}		
}
