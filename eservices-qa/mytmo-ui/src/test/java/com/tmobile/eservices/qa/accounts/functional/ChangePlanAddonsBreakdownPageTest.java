/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.ChangePlanAddonsBreakdownPage;
import com.tmobile.eservices.qa.pages.accounts.ChangePlanPlansReviewPage;

/**
 *
 * US515750 - Plans detail breakdown page - Multi-line pooled: Plan name
 */

public class ChangePlanAddonsBreakdownPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(ChangePlanAddonsBreakdownPageTest.class);

	@Test(dataProvider = "byColumnName", enabled = true, groups = "coveredinEndtoEnd")
	public void verifyAddonsBreakdownPageForSingleLineUserWhenDataFlagIsOFF(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddonsBreakdownPageForSingleLineUserWhenDataFlagIsOFF method called in ChangePlanAddonsBreakdownPageTest");
		Reporter.log("Test Case : Verify Header, Subheader,name,MSISDN and Addons on Addons breakdown page.");
		Reporter.log("Test Data : Any MSDSIN PAH/Full");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Go to Account overview page | Account overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA |Plans Configure Page Should be displayed");
		Reporter.log("6. Click on Addons blade. | Addons details Breadkdown Should be displayed");
		Reporter.log("7. Check Header. | Header Your devices should be displayed.");
		Reporter.log("8. Check below subtitle. | Subtitle should be displayed.");
		Reporter.log("9. Check Account blade. | Acount blade should not be displayed.");
		Reporter.log("10. Check monthly cost. | Total monthly device cost should be displayed.");
		Reporter.log("11. Check line name and msisdn. | Line name and msisdn should be displayed");
		Reporter.log(
				"12. Check Addons section with Addons and cost. | Addons section with Addons and cost should be displayed");
		Reporter.log(
				"13. Check Back to summary CTA and click on it. | Back to Summary CTa should be displayed and it redirects to Review page");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		// navigateToChangePlanPlansBreakDownPage(myTmoData);
		// changePlanPlansBreakdownPage.clickOnChangePlanCTAOnAccountOverviewPage();

		ChangePlanPlansReviewPage changePlanReviewPage = new ChangePlanPlansReviewPage(getDriver());
		changePlanReviewPage.verifyChangePlansReviewPage();
		changePlanReviewPage.clickOnAddonsBlade();

		ChangePlanAddonsBreakdownPage changePlanAddonBreakdownPage = new ChangePlanAddonsBreakdownPage(getDriver());
		changePlanAddonBreakdownPage.verifyAddonsBreakdownPage();
		changePlanAddonBreakdownPage.checkHeaderOfPlansBreakDownPage();
		changePlanAddonBreakdownPage.checkSubHeaderOfPlansBreakDownPage();
		changePlanAddonBreakdownPage.checkTotalMonthlyDeviceCostText();
		changePlanAddonBreakdownPage.calculateTotalMonthlyCostForAddonsForSingleLine();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = Group.RELEASE_READY)
	public void Sample(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyAddonsBreakdownPageForSingleLineUserWhenDataFlagIsOFF method called in ChangePlanAddonsBreakdownPageTest");

		navigateToPlansReviewPage(myTmoData);

		ChangePlanPlansReviewPage changePlanReviewPage = new ChangePlanPlansReviewPage(getDriver());
		changePlanReviewPage.clickOnAddonsBlade();

		ChangePlanAddonsBreakdownPage changePlanAddonsBreakdownPage = new ChangePlanAddonsBreakdownPage(getDriver());
		changePlanAddonsBreakdownPage.verifyAddonsBreakdownPage();
		// changePlanAddonsBreakdownPage.verifyMSISDNOfEachLine();
		changePlanAddonsBreakdownPage.verifyNumberOFAddonsForEachLine();
	}
}
