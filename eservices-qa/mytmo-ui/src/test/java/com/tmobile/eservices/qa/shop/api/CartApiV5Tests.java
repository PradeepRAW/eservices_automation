package com.tmobile.eservices.qa.shop.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.CartApiV4;
import com.tmobile.eservices.qa.api.eos.CartApiV5;
import com.tmobile.eservices.qa.api.eos.ServicesApiV3;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.shop.ShopConstants;

import io.restassured.response.Response;

public class CartApiV5Tests extends CartApiV5 {

	JsonPath jsonPath;
	public Map<String, String> tokenMap;

	/**
	 * US588194# Description:In Update Cart(SIM) SIM Price & modelName should be
	 * coming from DCP
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CartApiV4Test", "testCreateCart", Group.SHOP,
			Group.RELEASE })
	public void testUpdateSimPriceAndModelName(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update cart with Category as SimCard");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");
		Reporter.log("Step 3: Update Cart should Update sim to cart request for specific cart id");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		createCartAALSimCategory(apiTestData, tokenMap);

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "UpdateCartWithSimCategory.txt");
		String operationName = "Update Sim";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				Assert.assertNotNull(jsonPath.get("lines[0].items.device.category"), "Category is not SimCard");
				Reporter.log("Category is SimCard");

				Assert.assertNotNull(jsonPath.get("lines[0].items.device.modelName"), "Model Name is not Present");
				Reporter.log("Model Name is Present");

				Assert.assertNotNull(jsonPath.get("lines[0].items.device.fullRetailPrice"),
						"fullRetailPrice is not Present");
				Reporter.log("fullRetailPrice is present");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US586778# Description:Update cart with Category as Sim
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CartApiV4Test", "testCreateCart", Group.SHOP,
			Group.RELEASE })
	public void testUpdateSimCategory(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update cart with Category");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");
		Reporter.log("Step 3: Update Cart should Update sim to cart request for specific cart id");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		createCartAALSimCategory(apiTestData, tokenMap);

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "UpdateCartWithSimCategory.txt");
		String operationName = "Update Sim";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				Assert.assertNotNull(jsonPath.get("lines[0].items.device.category"), "Category is not SimCard");
				Reporter.log("Category is SimCard");

			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US586778# Description:Update cart with Category as HandSet for Device
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CartApiV4Test", "testCreateCart", Group.SHOP,
			Group.RELEASE })
	public void testUpdateDeviceCategory(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update cart with Category as HandSet for device ");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "CreateCartWithDeviceCategory.txt");
		String operationName = "Create Cart";
		// tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = createCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("cart.lines[0].items.device.category"),
						" device category is not Handset");
				Reporter.log("Category is Handset");

				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	Map<String, String> createCartAALSimCategory(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception, IOException, JsonProcessingException {
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "CreateCartWithSimCategory.txt");
		String operationName = "Create Cart";
		// tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		// tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = createCart(apiTestData, updatedRequest, tokenMap);
		System.out.println("Updated request body");
		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return tokenMap;

	}



	// create cart for US516325 ADDALINE header check

	Map<String, String> createCartAAL_NBYOD_FRPUS516325(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception, IOException, JsonProcessingException {
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "createCartAPI_NBYOD_FRP_US516325.txt");
		String operationName = "Create Cart";
		// tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = createCart(apiTestData, updatedRequest, tokenMap);
		System.out.println("Updated request body");
		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return tokenMap;
	}	

	Map<String, String> createCartAALDeviceCategory(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception, IOException, JsonProcessingException {
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "CreateCartWithDeviceCategory.txt");
		String operationName = "Create Cart";
		// tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = createCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));

				Assert.assertTrue(jsonPath.get("lines[0].items.device.category"), "Handset");
				Reporter.log("Category is Handset");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return tokenMap;

	}

	private Map<String, String> createCartAAL_WithOutAutoPay_US551893(ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception, IOException, JsonProcessingException {
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "createCartAPI_NBYOD_FRP_US516325.txt");
		String operationName = "Create Cart";
		// tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = createCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return tokenMap;
	}

	Map<String, String> createCartAAL_NBYOD_EIP_US516325(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception, IOException, JsonProcessingException {
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "createCartAPI_NBYOD_EIP_US516325.txt");
		String operationName = "Create Cart";
		// tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = createCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return tokenMap;
	}

	Map<String, String> createCartAAL_BYOD_FRP_US516325(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception, IOException, JsonProcessingException {
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "createCartAPI_BYOD_FRP_US516325.txt");
		String operationName = "Create Cart";
		// tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = createCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return tokenMap;
	}

	private Map<String, String> createCartFRP(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception, IOException, JsonProcessingException {
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "createCartAPI_FRP_US446868.txt");
		String operationName = "Create Cart";
		// tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = createCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return tokenMap;
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CartApiV4Test", "testCreateCart1", Group.SHOP,
			Group.SPRINT })
	public void testCreateCart(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Create Cart");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should create cart with a cart id");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		createCartCommon(apiTestData, tokenMap);
	}

	private Map<String, String> createCartCommon(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception, IOException, JsonProcessingException {
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "createCartAPI.txt");
		String operationName = "Create Cart";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = createCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		return tokenMap;
	}

	/**
	 * #Remove from Cart
	 * 
	 * @param data
	 * @param apiTestData
	 */
	// @Test(dataProvider = "byColumnName", enabled = true, groups = {
	// "CartApiV4Test", "testRemoveFromCart", Group.SHOP, Group.SPRINT })
	public void testRemoveFromCart(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Remove from Cart");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Remove from Cart should remove specific items from cart request for specific cart id.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		createCartCommon(apiTestData, tokenMap);

		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "removeFromCart.txt");
		String operationName = "Remove From Cart";

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = removeFromCart(apiTestData, updatedRequest, tokenMap);

		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				// tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * US446868# Description:EOS updateCart API integration with DCP v3 endpoints-
	 * Upgrade Flow-EIP
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CartApiV4Test", "testCreateCart", Group.APIREG })
	public void testUpdateCartWithEIP(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Update Cart for EIP");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Step 1: Test create cart with EIP terms|cartId should be generated and Response should be 200 OK");
		Reporter.log("Step 1: Update Cart should update specific items to cart request for specific cart id");
		Reporter.log(
				"Step 2: Verify Success services response code|Response code should be 200 OK and cart should be updated successfully.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "createCartAPI_EIP_US446868.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");
		String operationName = "createCartForPayment";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		String requestBodyUpdateCart = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "UpdateCartAPI_EIP_US446868.txt");
		operationName = "Update Cart For Payment";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * US446868# Description:EOS updateCart API integration with DCP v3 endpoints-
	 * Upgrade Flow-FRP
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CartApiV4Test", "testCreateCart", Group.APIREG })
	public void testUpdateCartWithFRP(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Update Cart for FRP");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Step 1: Test create cart with FRP terms|cartId should be generated and Response should be 200 OK");
		Reporter.log("Step 1: Update Cart should update specific items to cart request for specific cart id");
		Reporter.log(
				"Step 2: Verify Success services response code|Response code should be 200 OK and cart should be updated successfully.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		createCartFRP(apiTestData, tokenMap);

		String requestBodyUpdateCart = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "UpdateCartAPI_FRP_US446868.txt");
		String operationName = "Update Cart";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		Response response = updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * US516325# Description:Update Cart with NBYOD FRP flow
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	///// Vikas Ranpise
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CartApiV4Test", "testCreateCart", Group.SHOP,
			Group.APIREG })
	public void testUpdateCartAAL_NBYOD_FRP_US516325(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update cart of AAL flow with NBYOD FRP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");
		Reporter.log("Step 2: Verify create cart services response code|Response code should be 200 OK.");
		Reporter.log("Step 3: Update Cart should Update specific items to cart request for specific cart id");
		Reporter.log(
				"Step 4: Verify Update cart of plan,accessories and services are updated|Response code should be 200 OK.");
		Reporter.log("step 5: Verify remove accessories and line|accessories and line should be removed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		createCartAAL_NBYOD_FRPUS516325(apiTestData, tokenMap);

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "UpdatePlanAccessoriesAPI_NBYOD_FRP.txt");
		String operationName = "Update FRP Plan and Accessories";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				Assert.assertNotNull(jsonPath.get("lines[0].items.accessories[0].sku"), "SKu is Null");
				Reporter.log("sku is not null");
				// Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"),"Plan id
				// is Null");
				// Reporter.log("Plan id is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_SERVICE + "UpdateServiceAPI_NBYOD_FRP.txt");
		operationName = "Update FRP Services";
		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response = updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				Assert.assertNotNull(jsonPath.get("lines[0].items.services[0].socCode"), "Soc code is null");
				Reporter.log("soc code is present");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "RemoveAccessoriesAPI_NBYOD_FRP.txt");
		operationName = "Remove FRP Accessories";
		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response = updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				Assert.assertFalse(jsonNode.has("accessories"), "Accessories node is present");
				Reporter.log("Accessories node is not present");

			}
		} else {
			failAndLogResponse(response, operationName);
		}

		requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "RemoveLineAPI_NBYOD_FRP.txt");
		operationName = "Remove FRP Line";
		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response = updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");

				Assert.assertFalse(jsonNode.has("lines"), "Lines node is present");
				Reporter.log("Lines node is not present");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * US516325# Description:Update Cart with NBYOD EIP flow
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CartApiV4Test", "testCreateCart", Group.SHOP,
			Group.APIREG })
	public void testUpdateCartAAL_NBYOD_EIP_US516325(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update cart of AAL flow with NBYOD EIP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");
		Reporter.log("Step 2: Verify create cart services response code|Response code should be 200 OK.");
		Reporter.log("Step 3: Update Cart should Update specific items to cart request for specific cart id");
		Reporter.log(
				"Step 4: Verify Update cart of plan,accessories and services are updated|Response code should be 200 OK.");
		Reporter.log("step 5: Verify remove accessories and line|accessories and line should be removed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap = new HashMap<String, String>();
		createCartAAL_NBYOD_EIP_US516325(apiTestData, tokenMap);

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "UpdatePlanAccessoriesAPI_NBYOD_EIP.txt");
		String operationName = "Update EIP Plan and Accessories";

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				Assert.assertNotNull(jsonPath.get("lines[0].items.accessories[0].sku"), "SKu is Null");
				Reporter.log("sku is not null");
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan id is Null");
				Reporter.log("plan id is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_SERVICE + "UpdateServiceAPI_NBYOD_EIP.txt");
		operationName = "Update EIP Services";

		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response = updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				Assert.assertNotNull(jsonPath.get("lines[0].items.services[0].socCode"), "Soc code is null");
				Reporter.log("soc code is present");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "RemoveAccessoriesAPI_NBYOD_EIP.txt");
		operationName = "Remove EIP Accessories";

		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response = updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
				Assert.assertFalse(jsonNode.has("accessories"), "Accessories node is present");
				Reporter.log("Accessories node is not present");

			}
		} else {
			failAndLogResponse(response, operationName);
		}

		requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "RemoveLineAPI_NBYOD_EIP.txt");
		operationName = "Remove EIP Line";

		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response = updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");

				Assert.assertFalse(jsonNode.has("lines"), "Lines node is present");
				Reporter.log("Lines node is not present");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US516325# Description:Update Cart with BYOD FRP flow
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "CartApiV4Test", "testCreateCart", Group.SHOP,
			Group.APIREG })
	public void testUpdateCartAAL_BYOD_FRP_US516325(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update cart of AAL flow with BYOD FRP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");
		Reporter.log("Step 2: Verify create cart services response code|Response code should be 200 OK.");
		Reporter.log("Step 3: Update Cart should Update specific items to cart request for specific cart id");
		Reporter.log("Step 4: Verify Update cart of plan and sim are updated|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		createCartAAL_BYOD_FRP_US516325(apiTestData, tokenMap);

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "UpdatePlanSimAPI_BYOD_FRP.txt");
		String operationName = "Update FRP Plan and Sim";

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = updateCart(apiTestData, updatedRequest, tokenMap);
		logRequest(updatedRequest, operationName);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);

				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");

				//Assert.assertNotNull(jsonPath.get("lines[0].items.simStarterKit.sku"), "simStarterKit is null");
				//Reporter.log("SIM starter kit sku is present");

				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");

			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "cartTest", "createCart", Group.SHOP,
			Group.SPRINT })
	public void testCreateCartAddALine(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: create quote test");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response successfully created quote.");
		Reporter.log("Step 2: Verify quote created successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName = "createCartAALheaderTest";
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "createCartAALheaderTest.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");

		Stream.of(tokenMap.keySet().toString()).forEach(System.out::println);
		Stream.of(tokenMap.values().toString()).forEach(System.out::println);
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.SPRINT })
	public void testUpdateCartAAL_WithOutAutoPay_US551893(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: update cart of AAL flow with NBYOD FRP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");
		Reporter.log("Step 2: Verify get services response code|Response code should be 200 OK.");
		Reporter.log("Step 3: Check services response code|Response code should have autopay as false");
		Reporter.log("Step 4: Update plan for rate plan|Response code should be 200 OK.and plan should be updated");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		tokenMap = createCartAAL_WithOutAutoPay_US551893(apiTestData, tokenMap);
		ServicesApiV3 servicesApiV3 = new ServicesApiV3();
		Response response = servicesApiV3.getServicesForWithOutAutopay(apiTestData, tokenMap);
		String operationName = "Get Service";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertTrue(response.statusCode() == 200);
				Assert.assertFalse(jsonPath.get("planInfo[0].autoPayDiscountApplied"), "Autopay is not false");
				Reporter.log("AutoPay is false");

			} else {
				failAndLogResponse(response, operationName);
			}

			String requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_CART + "UpdatePlan_US515893.txt");
			operationName = "Update Plan";
			String updatedRequest = prepareRequestParam(requestBody, tokenMap);
			response = updateCart(apiTestData, updatedRequest, tokenMap);
			logRequest(updatedRequest, operationName);
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				mapper = new ObjectMapper();
				jsonNode = mapper.readTree(response.asString());

				if (!jsonNode.isMissingNode()) {
					Assert.assertTrue(response.statusCode() == 200);
					jsonPath = new JsonPath(response.asString());

					Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
					Reporter.log("Cart Id is not null");
					Assert.assertEquals(jsonPath.get("promotions[0].promoType"), "auto_pay");
					Reporter.log("auto pay is present");

				}
			} else {
				failAndLogResponse(response, operationName);
			}

		}

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.SPRINT })
	public void testUpdateCartAAL_WithAutoPay_US551893(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: update cart of AAL flow with NBYOD FRP");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Create Cart should create specific items to cart request for specific cart id");
		Reporter.log("Step 2: Verify get services response code|Response code should be 200 OK.");
		Reporter.log("Step 3: Check services response code|Response code should have autopay as true");
		Reporter.log("Step 4: Update plan for rate plan|Response code should be 200 OK.and plan should be updated");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		tokenMap = createCartAAL_WithOutAutoPay_US551893(apiTestData, tokenMap);
		ServicesApiV3 servicesApiV3 = new ServicesApiV3();
		Response response = servicesApiV3.getServicesForWithOutAutopay(apiTestData, tokenMap);
		String operationName = "Get Service";
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertTrue(response.statusCode() == 200);
				Assert.assertTrue(jsonPath.get("planInfo[0].autoPayDiscountApplied"), "true");
				Reporter.log("AutoPay is True");

			} else {
				failAndLogResponse(response, operationName);
			}

			String requestBody = new ServiceTest()
					.getRequestFromFile(ShopConstants.SHOP_CART + "UpdatePlan_US519010.txt");
			operationName = "Update Plan";
			String updatedRequest = prepareRequestParam(requestBody, tokenMap);
			response = updateCart(apiTestData, updatedRequest, tokenMap);
			logRequest(updatedRequest, operationName);
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				mapper = new ObjectMapper();
				jsonNode = mapper.readTree(response.asString());

				if (!jsonNode.isMissingNode()) {
					Assert.assertTrue(response.statusCode() == 200);
					jsonPath = new JsonPath(response.asString());

					Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
					Reporter.log("Cart Id is not null");
					Assert.assertEquals(jsonPath.get("promotions[0].promoType"), "auto_pay");
					Reporter.log("auto pay is present");

				}
			} else {
				failAndLogResponse(response, operationName);
			}
		}
	}

	/**
	 * US559681# Description:EOS Get Cart -verify deposit amount in cart -->
	 * cart.lines.linePriceSummary.deposit.amount
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.SPRINT })
	public void testGetCartForDepositAmountDisplay(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get Cart for EIP");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"Step 1: Test create cart with EIP terms|cartId should be generated and Response should be 200 OK");
		Reporter.log("Step 2: Test get cart |Response code should be 200 OK ");
		Reporter.log("Step 3: Verify get Cart details|cart id should have deposit amount and desposit display");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName = "createCart";
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "createCartForPaymentEIP.txt");
		CartApiV4 cartApiV4 = new CartApiV4();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApiV4.createCart(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		operationName = "Get Cart";
		response = cartApiV4.getCart(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				logSuccessResponse(response, operationName);
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("lines[0].linePriceSummary.deposit.amount"),
						"Deposit amount is not displayed ");
				Reporter.log("Deposit amount is not null");

				Assert.assertNotNull(jsonPath.get("lines[0].linePriceSummary.deposit.display"),
						"Deposit is not displayed");
				Reporter.log("Deposit is displayed");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US556002# Description:BYOD SIM card -EOS to store shipping dates
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.SPRINT })
	public void testEstimatedShippingDateInGetCartForBYOD(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: Get Cart for EIP");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Test create cart |cartId should be generated and Response should be 200 OK");
		Reporter.log(
				"Step 2: Update cart with shipping and SIM deatils for BYOD scenario |Response code should be 200 OK ");
		Reporter.log(
				"Step 3: Verify Update cart response  |estimatedShipDateFrom: estimatedShipDateTo should be present");
		Reporter.log(
				"Step 3: Verify get Cart details|Response should be 200 Ok and estimatedShipDateFrom: estimatedShipDateTo should not be null");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String operationName = "createCart";
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "createCartAAL_BYOD_FRP.txt");
		CartApiV4 cartApiV4 = new CartApiV4();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApiV4.createCart(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "UpdateCartWithShippingDates.txt");
		operationName = "Update Cart";
		updatedRequest = prepareRequestParam(requestBody, tokenMap);
		response = updateCart(apiTestData, updatedRequest, tokenMap);

		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(
						jsonPath.get("lines[0].items.simStarterKit.deviceAvailability.estimatedShipDateFrom"),
						"estimated Ship date  is null");
				Reporter.log("estimated Ship date is not null");
				Assert.assertNotNull(
						jsonPath.get("lines[0].items.simStarterKit.deviceAvailability.estimatedShipDateTo"),
						"estimated Ship date to is null");
				Reporter.log("estimated Ship date to is not null");

			}
		} else {
			failAndLogResponse(response, operationName);
		}

		operationName = "Get Cart";
		response = cartApiV4.getCart(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				logSuccessResponse(response, operationName);
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(
						jsonPath.get("lines[0].items.simStarterKit.deviceAvailability.estimatedShipDateFrom"),
						"estimated Ship date  is null");
				Reporter.log("estimated Ship date is not null in get cart");
				Assert.assertNotNull(
						jsonPath.get("lines[0].items.simStarterKit.deviceAvailability.estimatedShipDateTo"),
						"estimated Ship date to is null");
				Reporter.log("estimated Ship date to is not null in get cart");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}


	/**
	 * US597275# Description:BYOD SIM card -EOS to store shipping dates
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.SPRINT })
	public void testCreateCartUS597275(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("Create Cart with lines.lineType=WEARABLE");
		Reporter.log("Create Cart with lines.association node with lines.association.type=DUPLICATE");
		Reporter.log("Test create cart |cartId should be generated and Response should be 200 OK");
		Reporter.log("Test if the above fields are in response");
	}

	/**
	 * US596896# Technical - EOS Cart API change to include new promotion types
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.SPRINT })
	public void testGetCartAPIWithPromotionalCredits(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: Get Cart for EIP");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Test create cart |cartId should be generated and Response should be 200 OK");
		Reporter.log("Step 2: Perform Get Cart |Response should be 200 Ok  ");
		Reporter.log("Step 3: Verify Get Cart response  | lines[0].items.device.promotions should be present");
		Reporter.log("Step 4: Verify Get Cart response  | lines[0].items.device.promotions[0].discountValue should be present");
		Reporter.log("Step 5: Verify Get Cart response  | lines[0].items.device.promotions[0].promotionalCredits[x].recurringAmount should be present");
		Reporter.log("Step 6: Verify Get Cart response  | lines[0].items.device.promotions[0].promotionalCredits[x].amount should be present");
		Reporter.log("Step 7: Verify Get Cart response  | lines[0].items.device.promotions[0].promotionalCredits[x].contractTerm should be present");
		Reporter.log("Step 8: Verify Get Cart response  | lines[0].items.device.promotions[0].promotionalCredits[x].creditType should be present");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

	}

	/**
	 * https://jirasw.t-mobile.com/browse/CDCSM-84
	 * US597275 Call association API to associate wearable device to a GSM line.
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.SPRINT })
	public void testCreateCartWithLineTypeWearable(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: CreateCartAPI With LineTypeWearable");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: As part of create cart/update cart operation, pass value 'WEARABLE' for 'lineType'");
		Reporter.log("Step 5: Test create cart |cartId should be generated and Response should be 200 OK");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();

		String operationName = "createCart";
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "CreateCart_CDCSM_84_LineTypeWearable.txt");
		CartApiV5 cartApiV5 = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		logRequest(updatedRequest, operationName);
		Response response = cartApiV5.createCart(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * https://jirasw.t-mobile.com/browse/CDCSM-84
	 * US597275 Call association API to associate wearable device to a GSM line.
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.SPRINT })
	public void testCreateCartWithLineTypeHandSet(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: CreateCartAPI With LineTypeWearable");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: As part of create cart/update cart operation, pass value 'HANDSET' for 'lineType'");
		Reporter.log("Step 2: Test create cart |cartId should be generated and Response should be 200 OK");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap = new HashMap<String, String>();

		String operationName = "createCart";
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "CreateCart_CDCSM_84_LineType_HANDSET.txt");
		CartApiV5 cartApiV5 = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		logRequest(updatedRequest, operationName);
		Response response = cartApiV5.createCart(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * https://jirasw.t-mobile.com/browse/CDCSM-84
	 * US597275 Call association API to associate wearable device to a GSM line.
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.SPRINT })
	public void testCreateCartWithoutLineType(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: CreateCartAPI With LineTypeWearable");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: As part of create cart operation, do not pass lineType'");
		Reporter.log("Step 2: Test create cart |cartId should be generated and Response should be 200 OK");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap = new HashMap<String, String>();

		String operationName = "createCart";
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "CreateCart_CDCSM_84_No_LineType.txt");
		CartApiV5 cartApiV5 = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		logRequest(updatedRequest, operationName);
		Response response = cartApiV5.createCart(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}


	/**
	 * https://jirasw.t-mobile.com/browse/CDCSM-84
	 * US597275 Call association API to associate wearable device to a GSM line.
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.SPRINT })
	public void testUpdateCartWithAssociationTypeDuplicate(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: CreateCartAPI With LineTypeWearable");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: As part of update cart pass 'association' node with attributes 'type' and 'lineAssociated'");
		Reporter.log("Step 2: Value of 'type' should be defaulted to 'DUPLICATE'");
		Reporter.log("Step 3: Value of 'lineAssociated' will be the parent line msisdn");		
		Reporter.log("Step 4: Test update cart |cartId should be generated and Response should be 200 OK");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		

		
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "CreateCart_CDCSM_84_LineType_HANDSET.txt");
		CartApiV5 cartApiV5 = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");
		String operationName = "createCartForPayment";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApiV5.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		String requestBodyUpdateCart = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "UpdateCart_CDCSM_84_AssociationType_DUPLICATE.txt");
		operationName = "Update Cart For Payment";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}


	/**
	 * https://jirasw.t-mobile.com/browse/CDCSM-84
	 * US597275 Call association API to associate wearable device to a GSM line.
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.SPRINT })
	public void testUpdateCartWithAssociationTypeVirtual(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: CreateCartAPI With LineTypeWearable");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: As part of update cart pass 'association' node with attributes 'type' and 'lineAssociated'");
		Reporter.log("Step 2: Value of 'type' should be defaulted to 'VIRTUAL'");
		Reporter.log("Step 3: Value of 'lineAssociated' will be the parent line msisdn");		
		Reporter.log("Step 4: Test update cart |Response should be 400 Bad Request");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "CreateCart_CDCSM_84_LineType_HANDSET.txt");
		CartApiV5 cartApiV5 = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");
		String operationName = "createCartForPayment";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApiV5.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		String requestBodyUpdateCart = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "UpdateCart_CDCSM_84_AssociationType_VIRTUAL.txt");
		operationName = "Update Cart For Payment";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);

		if (response != null && "400".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
			}
		} else {
			failAndLogResponse(response, operationName);
		}


	}

	/**
	 * https://jirasw.t-mobile.com/browse/CDCSM-84
	 * US597275 Call association API to associate wearable device to a GSM line.
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.SPRINT })
	public void testUpdateCartWithoutAssociation(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: CreateCartAPI With LineTypeWearable");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: As part of update cart do not pass 'association' node");
		Reporter.log("Step 2: Test update cart |cartId should be generated and Response should be 200 OK");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "CreateCart_CDCSM_84_LineType_HANDSET.txt");
		CartApiV5 cartApiV5 = new CartApiV5();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("emailAddress", apiTestData.getMsisdn() + "@yoipmail.com");
		String operationName = "createCartForPayment";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApiV5.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Cart Response status code : " + response.getStatusCode());
			Reporter.log("Cart Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}

		String requestBodyUpdateCart = new ServiceTest()
				.getRequestFromFile(ShopConstants.SHOP_CART + "UpdateCart_CDCSM_84_NO_AssociationType.txt");
		operationName = "Update Cart For Payment";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());

			if (!jsonNode.isMissingNode()) {
				jsonPath = new JsonPath(response.asString());

				Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
				Reporter.log("Cart Id is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

		

	}


}