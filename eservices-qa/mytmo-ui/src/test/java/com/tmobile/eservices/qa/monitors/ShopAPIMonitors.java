package com.tmobile.eservices.qa.monitors;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.tmobile.eservices.qa.api.ShopAPIMonitorsHelper;
import com.tmobile.eservices.qa.data.ApiTestData;

public class ShopAPIMonitors extends ShopAPIMonitorsHelper {

	@BeforeMethod(alwaysRun = true)
	public void clearTokenMapCommon() {
		tokenMapCommon = null;
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeFRPFlow(ApiTestData apiTestData) {
		standardUpgradeFRPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeEIPFlow(ApiTestData apiTestData) {
		standardUpgradeEIPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeAddSocsEIPFlow(ApiTestData apiTestData) {
		standardUpgradeAddSocsEIPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeAddAccessoriesEIPFlow(ApiTestData apiTestData) {
		standardUpgradeAddAccessoriesEIPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardUpgradeTradeInEIPFlow(ApiTestData apiTestData) {
		standardUpgradeTradeInEIPFlow(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void AALPhoneOnEIPNoPDPSOC(ApiTestData apiTestData) {
		aalPhoneOnEIPNoPDPSOC(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void AAL_FRP_PDP_NY_WithDeposit(ApiTestData apiTestData) {
		aal_FRP_PDP_NY_WithDeposit(apiTestData);
	}

	@Test(dataProvider = "byColumnName", enabled = true)
	public void AAL_EIP_PDP_WithSecurityDeposit(ApiTestData apiTestData) {
		aal_EIP_PDP_WithSecurityDeposit(apiTestData);
	}
	
	//@Test(dataProvider = "byColumnName", enabled = true)
	public void StandardBYODFlow(ApiTestData apiTestData) {
		BYODFlow(apiTestData);
	}
}
