/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AutoPayCancelConfirmationPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

/**
 * @author charshavardhana
 *
 */
public class AutopayCancelConfirmationPageTest extends PaymentCommonLib{

	/**
	 * US283057	GLUE Light Reskin - AutoPay Cancel confirmation page
	 * 
	 * @param data
	 * @param myTmoData
	 * Data Conditions - Regular account. Autopay ON/OFF
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.RETIRED})
	public void testGLRAutoPayCancelConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US283057	GLUE Light Reskin - AutoPay Cancel confirmation page");
		Reporter.log("Data Conditions - Regular account. Autopay ON/OFF");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: Navigate to Auto Pay landing page  | Auto Pay landing Page should be displayed");
		Reporter.log("Step 4. Click Cancel Autopay | Verify Are you sure you want to cancell autopay? pop-up is present");
		Reporter.log("Step 5. Click Yes, Cancel Autopay | Verify cancelConfirmation page is displayed");
		Reporter.log("Step 5.1 Verify Autopay cancelled! is dispplayed");
		Reporter.log("Step 5.2 Verify information line is present: You will no longer recieve AutoPay discount"); 
		Reporter.log("Step 5.3 Verify Return to home CTA is present");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		AutoPayCancelConfirmationPage autoPayCancelConfirmationPage = navigateToAutoPayCancelConfirmationPage(myTmoData);
		autoPayCancelConfirmationPage.verifyCancelAutoPayConfirmationPageElements();
		autoPayCancelConfirmationPage.clickreturnToHome();
		setupAutopay(myTmoData);
		
	/*	HomePage homePage = navigateToHomePage(myTmoData);
		if (!homePage.verifyAutopayisscheduled()) {
			setupAutopay(myTmoData);
		}	
		AutoPayPage autoPayPage = navigateToAutoPayPagefromHomePage(myTmoData);
		autoPayPage.clickCancelAutoPayLink();
		autoPayPage.verifyAutoPayCancelModalHeader();
		autoPayPage.verifycancelAutoPayModal();
		autoPayPage.clickAutoPayCancelNoBtn();
		autoPayPage.verifyAutoPayLandingPageFields();
		autoPayPage.clicktermsNconditions();
		TnCPage tnCPage = new TnCPage(getDriver());
		tnCPage.verifyPageLoaded();
		tnCPage.validateTnCPage();
		tnCPage.clickBackButton();
		autoPayPage.verifyPageLoaded();
		
		AutoPayPage autoPayPage = navigateToAutoPayPagefromHomePage(myTmoData);
		autoPayPage.clickCancelAutoPayLink();*/
	}
	
}
