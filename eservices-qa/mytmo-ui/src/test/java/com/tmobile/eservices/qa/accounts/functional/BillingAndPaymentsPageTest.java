package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.BillingAndPaymentsPage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;

public class BillingAndPaymentsPageTest extends GlobalCommonLib {

	/**
	 * DE175441:Prod - Profile - Billing address updates are not reflecting after
	 * save
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyBillingAddressIsUpadted(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("Test Case : verifyBillingAddressIsUpadted - Verify the billing address and edit the info");
		Reporter.log("========================");
		Reporter.log("Expected Result:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log(
				"5: Click on Billing Address link and verify Billing Address bread crumb active status | Billing Address bread crumb should be active");
		Reporter.log(
				"5: Edit billing address and click on save changes button| Save changes button should be clicked successfully");
		Reporter.log("6: Verify updated billing address| Billing address should be updated successfully");
		Reporter.log("7: Verify billing and payments page| Billing and payments page should be displayed");
		Reporter.log(
				"8: Verify Billing & Payments bread crumb active status| Billing & Payments bread crumb should be active");
		Reporter.log(
				"9: Click on profile bread crumb and verify profile bread crumb active status | Profile bread crumb should be active");
		Reporter.log("10:Verify profile page | Profile page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillingAndPaymentsPage billingAndPaymentsPage = navigateToBillingAndPaymentsPage(myTmoData,
				"Billing & Payments");
		billingAndPaymentsPage.clikHeaderByName("Billing Address");
		billingAndPaymentsPage.verifyBreadCrumbActiveStatus("Billing Address");
		String addressline = billingAndPaymentsPage.updateBillingAddress(myTmoData);
		billingAndPaymentsPage.verifye911Address();
		billingAndPaymentsPage.verifyUsageAddress();
		billingAndPaymentsPage.clickSaveChangesBtn();
		billingAndPaymentsPage.verifyUpdatedBillingAddress(addressline, "Billing");
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.verifyBreadCrumbActiveStatus("Billing & Payments");

		billingAndPaymentsPage.clickProfileHomeBreadCrumb();
		billingAndPaymentsPage.verifyBreadCrumbActiveStatus("Profile");

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testToggleOnOffEmailPaperlessBillingOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on Billing and Payments link | Billing and payments page should be displayed");
		Reporter.log("6. Click on Paperless Billing link | Paperless Billing page should be displayed");
		Reporter.log("7. Click on Paperless Billing radio button | Paperless Billing radio button should be selected");
		Reporter.log("8. Select email to check box | Check box should be selected");
		Reporter.log("9. Click on Agree and Submit| Billing and Payments page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		BillingAndPaymentsPage billingAndPaymentsPage = navigateToBillingAndPaymentsPage(myTmoData,
				"Billing & Payments");
		billingAndPaymentsPage.clikHeaderByName("Paperless Billing");
		billingAndPaymentsPage.verifyCancelButtonCTA();
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.clikHeaderByName("Paperless Billing");
		billingAndPaymentsPage.clickPaperLessBillRadioBtn();
		billingAndPaymentsPage.clickEmailRadioBtn();
		billingAndPaymentsPage.clickAgreeAndSubmitBtn();
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testToggleSummaryOrDetailedPaperBillingOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on Billing and Payments link | Billing and payments page should be displayed");
		Reporter.log("6. Click on Paperless billing link|Paperless billing page should be displayed");
		Reporter.log("7. Click on paper bill radio button | paper bill radio button should be clicked");
		Reporter.log("8. Click on Summary bill radio button | Summary bill radio button should be clicked");
		Reporter.log("9. Click on Agree and submit  button | Billing and Payments page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		BillingAndPaymentsPage billingAndPaymentsPage = navigateToBillingAndPaymentsPage(myTmoData,
				"Billing & Payments");
		billingAndPaymentsPage.clikHeaderByName("Paperless Billing");

		billingAndPaymentsPage.selectPaperBillingAddress();
		billingAndPaymentsPage.clickSummaryBillRadioBtn();
		billingAndPaymentsPage.clickAgreeAndSubmitBtn();
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
	}

	/**
	 * DE176518:Prod_ Sev2_Paper Billing – Detailed Bill signUp is failing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyUpdateOperationFailedDailogBoxForDetailedBillSignUp(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on Billing and Payments link | Billing and payments page should be displayed");
		Reporter.log("6. Click on Paperless billing link|Paperless billing page should be displayed");
		Reporter.log("7. Click on detailed bill radio button | Detailed bill radio button should be clicked");
		Reporter.log(
				"8. Click on detailed bill radio agree and submit button | Paperless billing page should be displayed");
		Reporter.log(
				"9. Click on Agree and submit  button and Verify update operation failed dialog box| Update operation failed dialog box should not be displayedd");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		BillingAndPaymentsPage billingAndPaymentsPage = navigateToBillingAndPaymentsPage(myTmoData,
				"Billing & Payments");
		billingAndPaymentsPage.clikHeaderByName("Paperless Billing");
		billingAndPaymentsPage.selectPaperBillingAddress();
		billingAndPaymentsPage.clickDetailedBillRadioBtn();
		billingAndPaymentsPage.clickDetailedBillAgreeAndSubmitBtn();
		billingAndPaymentsPage.clickAgreeAndSubmitBtn();
		billingAndPaymentsPage.verifyUpdateOperationFailedDialogBox();
	}

	/**
	 * US327336/US454528:Profile | Re-Launch Enhancements | Rename Home |
	 * Breadcrumbs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyBillingAddressBreadCrumbsLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log("Test Case : verifyBillingAddressIsUpadted - Verify the billing address and edit the info");
		Reporter.log("========================");
		Reporter.log("Expected Result:");
		Reporter.log("1: Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4: Click on Profile link | Profile Page should be displayed");
		Reporter.log(
				"5: Click on Billing Address link and verify Billing Address bread crumb active status | Billing Address bread crumb should be active");
		Reporter.log("6: Click on Billing & Payments bread crumb link | Billing & Payments Page should be displayed");
		Reporter.log(
				"7: Verify Billing & Payments bread crumb active status| Billing & Payments bread crumb should be active");
		Reporter.log(
				"8: Click on profile bread crumb and verify profile bread crumb active status | Profile bread crumb should be active");
		Reporter.log("9: Verify profile page | Profile page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillingAndPaymentsPage billingAndPaymentsPage = navigateToBillingAndPaymentsPage(myTmoData,
				"Billing & Payments");
		billingAndPaymentsPage.clikHeaderByName("Billing Address");
		billingAndPaymentsPage.verifyBreadCrumbActiveStatus("Billing Address");
		billingAndPaymentsPage.clickBreadCrumb("Billing & Payments");
		billingAndPaymentsPage.verifyBillingAndPaymentsPage();
		billingAndPaymentsPage.verifyBreadCrumbActiveStatus("Billing & Payments");
		billingAndPaymentsPage.clickProfileHomeBreadCrumb();
		billingAndPaymentsPage.verifyBreadCrumbActiveStatus("Profile");

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}

	/**
	 * DE214006:State Field drop down does not expand when first letter of the state
	 * abbreviation is entered
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX })
	public void verifyStateFieldDropDownFuntionality(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data : PAH,Standard ");
		Reporter.log(
				"Test Case : State Field drop down does not expand when first letter of the state abbreviation is entered");
		Reporter.log("========================");
		Reporter.log("Expected Result:");
		Reporter.log("1. Launch Application | Application should be launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile link | Profile page should be displayed");
		Reporter.log("5. Click on billing address link  | Billing address link should be clicked");
		Reporter.log("6. Verify state dropdown toggle | State dropdown toggle should be displayed");
		Reporter.log(
				"7. Click on state dropdown toggle and verify state dropdown menu | State dropdown menu div should be expanded");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		BillingAndPaymentsPage billingAndPaymentsPage = navigateToBillingAndPaymentsPage(myTmoData,
				"Billing & Payments");
		billingAndPaymentsPage.clikHeaderByName("Billing Address");
		billingAndPaymentsPage.verifyStateDropDownToggle();
		billingAndPaymentsPage.verifyStateDropDownMenu();
	}
}
