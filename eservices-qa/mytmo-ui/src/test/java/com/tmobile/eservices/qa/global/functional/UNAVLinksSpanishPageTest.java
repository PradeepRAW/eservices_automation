package com.tmobile.eservices.qa.global.functional;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.global.GlobalConstants;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;

public class UNAVLinksSpanishPageTest extends GlobalCommonLib {

	/**
	 * Verify HomePage Information
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = "UNAV Spanish Links")
	public void verifyHomePageInformationForSpanish(ControlTestData data, MyTmoData myTmoData) {
		
		Reporter.log("Test Case : Verify HomePage Information");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logined successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on spanish language|Home page should be displayed for spanish");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		Reporter.log("Home page is displayed");
	
		homePage.changeLanguage();
		verifyPage("Home Page", "Inicio");
		Reporter.log("Home page is displayed for spanish");
		homePage.changeLanguage();
	}

	/**
	 * Verify Billing Tab Redirects To Billing Summary Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyBillingLinkForSpanish(ControlTestData data, MyTmoData myTmoData) {
		
		Reporter.log("Test Case : Verify Billing Tab Redirects To Billing Summary Page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logined successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on billing language|Billing page should be displayed for spanish");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		Reporter.log("Verify home page");
		
		homePage.changeLanguage();
		verifyPage("Home Page", "Inicio");
		Reporter.log("Home page is displayed for spanish");
		
		homePage.clickBillingLink();
		verifyPage("Billing Summary Page", "Resumen de factura");
		Reporter.log("Bill summary page is displayed");
		homePage.changeLanguage();
	}

	/**
	 * Verify Phone Tab Redirects To My Phone Details Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyPhoneLinkForSpanish(ControlTestData data, MyTmoData myTmoData) {
		
		Reporter.log("Test Case : Verify Phone Tab Redirects To My Phone Details Page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logined successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on phone language|Phone page should be displayed for spanish");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		Reporter.log("Verify home page");
		
		homePage.changeLanguage();
		verifyPage("Home Page", "Inicio");
		Reporter.log("Home page is displayed for spanish");
		
		homePage.clickPhoneLink();
		verifyPage("Phone Page", "Teléfono");
		Reporter.log("Phone page is displayed for spanish");
		homePage.changeLanguage();
	}

	/**
	 * Verify Shop Tab Redirects To Shop Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyShopLinkForSpanish(ControlTestData data, MyTmoData myTmoData) {
		
		Reporter.log("Test Case : Verify Shop Tab Redirects To Shop Page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logined successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on shop|Shop page should be displayed for spanish");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		Reporter.log("Home page verified");
		
		homePage.changeLanguage();
		verifyPage("Home Page", "Inicio");
		Reporter.log("Home page is displayed for spanish"); 
		
		homePage.clickShoplink();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.checkPageIsReady();
		Assert.assertTrue(getDriver().getCurrentUrl().contains("shop"), GlobalConstants.SHOPPAGE_ERROR_MSG);
		Reporter.log("Shop page verified");
		homePage.changeLanguage();
	}

	/**
	 * Verify Plan Tab Redirects To Plan Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyPlanLinkForSpanish(ControlTestData data, MyTmoData myTmoData) {
		
		Reporter.log("Test Case : Verify Plan Tab Redirects To Plan Page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logined successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on plan|Plan page should be displayed for spanish");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		
		homePage.changeLanguage();
		verifyPage("Home Page", "Inicio");
		Reporter.log("Home page is displayed for spanish");  
		
		homePage.clickAccountLink();
		Assert.assertTrue(getDriver().getTitle().contains("Plan"), GlobalConstants.PLAN_PAGE_ERROR);
		Reporter.log(GlobalConstants.VERIFY_PLAN_PAGE);
		homePage.changeLanguage();
	}
	
	/**
	 * Verify Usage Tab Redirects To Plan Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyUsageLinkForSpanish(ControlTestData data, MyTmoData myTmoData) {
		
		Reporter.log("Test Case : Verify Usage Tab Redirects To Plan Page");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logined successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on usage|Usage page should be displayed for spanish");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		Reporter.log("Verify home page");
		
		homePage.changeLanguage();
		verifyPage("Home Page", "Inicio");
		Reporter.log("Home page is displayed for spanish");  
		
		homePage.clickUsageLink();
		verifyPage("Usage Page", "Uso");
		Reporter.log("Usage page is displayed for spanish");  
		homePage.changeLanguage();
	}

}
