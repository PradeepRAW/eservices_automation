package com.tmobile.eservices.qa.payments.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.api.EOSCommonLib;
import com.tmobile.eservices.qa.pages.accounts.api.EOSODF;
import com.tmobile.eservices.qa.pages.accounts.api.EOSPartnerBenefits;
import com.tmobile.eservices.qa.pages.accounts.api.EOSSAMAPI;
import com.tmobile.eservices.qa.pages.global.api.EOSCustomerAccount;
import com.tmobile.eservices.qa.pages.global.api.EOSProfile;
import com.tmobile.eservices.qa.pages.payments.api.EOSAALEligibilityCheckApiV1;
import com.tmobile.eservices.qa.pages.payments.api.EOSAccountsV3Filters;
import com.tmobile.eservices.qa.pages.payments.api.EOSAutopayDiscount;
import com.tmobile.eservices.qa.pages.payments.api.EOSBillingFilters;
import com.tmobile.eservices.qa.pages.payments.api.EOSPAFilter;
import com.tmobile.eservices.qa.pages.payments.api.EOSPaymentmanager;
import com.tmobile.eservices.qa.pages.payments.api.EOSservicequotefilter;
import com.tmobile.eservices.qa.pages.payments.api.EOSusageFilter;

import io.restassured.response.Response;

public class restmisdingrade {

	EOSCommonLib ecl = new EOSCommonLib();
	EOSBillingFilters billing = new EOSBillingFilters();
	EOSAccountsV3Filters accounts = new EOSAccountsV3Filters();
	EOSProfile profile = new EOSProfile();
	EOSODF odf = new EOSODF();
	EOSSAMAPI samapi = new EOSSAMAPI();
	EOSPartnerBenefits partnerbenefits = new EOSPartnerBenefits();
	EOSPaymentmanager paymentmanger = new EOSPaymentmanager();
	EOSPAFilter eospa = new EOSPAFilter();
	EOSusageFilter eosusage = new EOSusageFilter();
	EOSservicequotefilter eossquote = new EOSservicequotefilter();
	EOSAALEligibilityCheckApiV1 eosAALCheck = new EOSAALEligibilityCheckApiV1();
	EOSCustomerAccount eosCustAccount = new EOSCustomerAccount();
	EOSAutopayDiscount apdiscount = new EOSAutopayDiscount();

	@DataProvider(name = "testdata", parallel = true)
	public Iterator<Object[]> prepareBooleans() {
		String FILE_NAME;
		FILE_NAME = System.getProperty("user.dir") + "/src/test/resources/testdata/testdatavalidator.xlsx";
		Set<Object[]> misdinpawords = getallmisdins(FILE_NAME);
		return misdinpawords.iterator();
	}

	invalidtestdataidentifier gaps = new invalidtestdataidentifier();

	@Test(dependsOnMethods = { "Testdataevaluation" })

	public void identifygaps() throws IOException {
		gaps.tesdatagapidentifier();
	}

	@Test(dataProvider = "testdata")
	public void Testdataevaluation(String misdpwdcap) {

		try {
			boolean itest = true;

			String misdin = misdpwdcap.split("/")[0];
			String pwd = misdpwdcap.split("/")[1];
			String capability = misdpwdcap.split("/")[2];
			String misdpwd = misdin.trim() + "/" + pwd.trim();
			// misdin="8589146039";
			// pwd="Auto12346";
			// misdpwd=misdin+"/"+pwd;
			if (misdin.trim().length() > 0 && pwd.trim().length() > 0) {

				// Reporter.log("<tr>");
				JSONObject adddata = new JSONObject();
				adddata.put("id", misdin);
				adddata.put("capability", capability);
				adddata.put("misdin", misdpwd);
				String[] getjwt = ecl.getauthorizationandregisterinapigee(misdin, pwd);
				if (getjwt != null) {

					String accesscode = getjwt[1];
					String jwttoken = getjwt[2];
					String ban = getjwt[3];
					String usertype = getjwt[4].trim();
					String mailid = getjwt[5];
					String firstname = getjwt[6];
					String lastname = getjwt[7];
					String acctype = getjwt[8];
					String status = getjwt[9];
					String lines = getjwt[10];

					adddata.put("misdin", misdpwd);
					adddata.put("ban", ban);
					adddata.put("usrtype", usertype);
					adddata.put("acctype", acctype);
					adddata.put("status", status);
					adddata.put("lines", Integer.parseInt(lines));

					Response eosCustAccountResponse = eosCustAccount.getCustomerAccountDetails(getjwt);
					/*
					 * String isDeviceUpgradeEligible =
					 * eosCustAccount.getDeviceUpgradeEligible(eosCustAccountResponse); if
					 * (isDeviceUpgradeEligible != null) { adddata.put("isDeviceUpgradeEligible",
					 * isDeviceUpgradeEligible); }
					 */

					Map<String, String> upgradeMap = new HashMap<String, String>();
					upgradeMap = eosCustAccount.getDeviceUpgradeEligible(eosCustAccountResponse);
					if (upgradeMap.get("eligible") != null) {
						adddata.put("isDeviceUpgradeEligible", upgradeMap.get("eligible"));
					}
					if (upgradeMap.get("programType") != null) {
						adddata.put("programType", upgradeMap.get("programType"));
					}

					Response sedonaquote = eossquote.getResponsesedonaquote(getjwt);
					String issedona = eossquote.checkissedona(sedonaquote);
					if (issedona != null) {
						adddata.put("issedona", issedona);
					}

					Response apdiscountresponse = apdiscount.getResponseAutopayDiscountEligible(getjwt);
					String autopaydiscounteligible = apdiscount.discounteligible(apdiscountresponse);
					if (autopaydiscounteligible != null) {
						adddata.put("autopaydiscounteligible", autopaydiscounteligible);
					}

					// Billing-getDataset
					Response datasetrespons = billing.getResponsedataset(getjwt);
					/*
					 * String ispa=billing.getpaeligibility(datasetrespons); if(ispa!=null)
					 * {adddata.put("pa", ispa);}
					 */

					String iseip = billing.geteipdetails(datasetrespons);
					if (iseip != null) {
						adddata.put("eip", iseip);
					}
					String isjod = billing.getjoddetails(datasetrespons);
					if (isjod != null) {
						adddata.put("jod", isjod);
					}
					String isauto = billing.getautopaystatus(datasetrespons);
					if (isauto != null) {
						adddata.put("auto", isauto);
					}
					String duedate = billing.getdueDate(datasetrespons);
					if (duedate != null) {
						adddata.put("duedate", duedate);
						adddata.put("duedatetype", billing.getduedatetype(duedate));
					}

					String haspastdue = billing.getIspastdueAmount(datasetrespons);
					if (haspastdue != null) {
						adddata.put("haspastdue", haspastdue);
					}
					String hasarbalance = billing.getARbalance(datasetrespons);
					if (hasarbalance != null) {
						adddata.put("arbalance", hasarbalance);
					}

					// Billing-getBillList
					Response billlistrespons = billing.getResponsebillList(getjwt);
					String billtype = billing.getbilltype(billlistrespons);
					if (billtype != null) {
						adddata.put("bill", billtype);
					}
					String previousbills = billing.checkpreviousbills(billlistrespons);
					if (previousbills != null) {
						adddata.put("priviousbill", previousbills);
					}

					String previousebill = billing.getebillstatementmonth(billlistrespons);
					if (previousebill != null) {
						adddata.put("previousebill", "true");
					} else {
						adddata.put("previousebill", "false");
					}

					// v3 Accounts
					Response getpaaymentmethods = accounts.getResponseSearchpayments(getjwt);
					String bpeligible = accounts.geteligibilityforbankpayment(getpaaymentmethods);
					if (bpeligible != null) {
						adddata.put("bankpaymenteligible", bpeligible);
					}
					List<String> storedbank = accounts.getstoredbankalias(getpaaymentmethods);
					if (storedbank != null) {
						if (storedbank.isEmpty())
							adddata.put("storedbank", "false");
						else
							adddata.put("storedbank", "true");
					}
					List<String> storedcard = accounts.getstoredcardalias(getpaaymentmethods);
					if (storedcard != null) {
						if (storedcard.isEmpty())
							adddata.put("storecreditcard", "false");
						else
							adddata.put("storecreditcard", "true");
					}
					List<String> storeddebitcard = accounts.getstoreddebitcardalias(getpaaymentmethods);
					if (storeddebitcard != null) {
						if (storeddebitcard.isEmpty())
							adddata.put("storedebitcard", "false");
						else
							adddata.put("storedebitcard", "true");
					}

					/*
					 * String[] getstoredinfo = accounts.getstoreddata(getpaaymentmethods); if
					 * (getstoredinfo[0] != null) adddata.put("storedbank", getstoredinfo[0]); if
					 * (getstoredinfo[1] != null) adddata.put("storecreditcard", getstoredinfo[1]);
					 * if (getstoredinfo[2] != null) adddata.put("storedebitcard",
					 * getstoredinfo[1]);
					 */

					// profile
					Response getprofile = profile.getProfileresponse(getjwt);
					String billstate = profile.getbillingState(getprofile);
					if (billstate != null) {
						adddata.put("billstate", billstate);
					}

					// payment manager
					Response getpainfo = paymentmanger.getPAinformationresponse(getjwt);
					String pasetup = paymentmanger.isPAAlreadysetup(getpainfo);
					if (pasetup != null) {
						adddata.put("paalreadysetup", pasetup);
					}
					String paval = paymentmanger.ispaeligible(getpainfo);
					if (paval != null) {
						if (paval.equalsIgnoreCase("true") && hasarbalance.equalsIgnoreCase("true"))
							adddata.put("pa", "true");
						else
							adddata.put("pa", "false");
					}

					Response subscriberinfo = paymentmanger.getsubscriberinforesponse(getjwt);
					String autopayeligible = paymentmanger.isautopayeligible(subscriberinfo);
					if (autopayeligible != null) {
						adddata.put("autopayeligible", autopayeligible);
					}

					String creditclass = paymentmanger.getcreditclass(subscriberinfo);
					if (creditclass != null) {
						adddata.put("creditclass", creditclass);
					}

					/*
					 * String isInsideBlackoutPeriod =
					 * paymentmanger.autopayblackoutperiod(subscriberinfo); if
					 * (isInsideBlackoutPeriod != null) { adddata.put("isInsideBlackoutPeriod",
					 * isInsideBlackoutPeriod); }
					 */

					String fdpeligible = paymentmanger.isfdpeligible(subscriberinfo);
					if (fdpeligible != null) {
						adddata.put("fdpeligible", fdpeligible);
					}

					// ODF
					Response odfinfo = odf.getResponseODF(getjwt);
					List<String> allcurrenservicedetails = odf.getallCurrentServiceDetails(odfinfo);
					if (allcurrenservicedetails != null) {
						adddata.put("allcurrenservicedetails", allcurrenservicedetails);
					}
					String currentdataservicedetailssoc = odf.getcurrentdataservicedetailssoc(odfinfo);
					if (currentdataservicedetailssoc != null) {
						adddata.put("currentdataservicedetailssoc", currentdataservicedetailssoc);
					}
					String plansoc = odf.getplansoc(odfinfo);
					if (plansoc != null) {
						adddata.put("plansoc", plansoc);
					}

					// partner benefits
//					Response responsepartnerbenefits = partnerbenefits.getResponsepartnerbenefits(getjwt);
//					String isregistred = partnerbenefits.getisregistered(responsepartnerbenefits);
//					String hasPremiumSOC = partnerbenefits.hasPremiumSOC(responsepartnerbenefits);
//					if (isregistred != null) {
//						adddata.put("isregistred", isregistred);
//					}
//					if (hasPremiumSOC != null) {
//						adddata.put("hasPremiumSOC", hasPremiumSOC);
//					}

					// sam API
					Response responseSAMAPI = samapi.getResponseSAMAPI(getjwt);
					String pendingChanges = samapi.getPendingChanges(responseSAMAPI);

					if (pendingChanges != null) {

						adddata.put("pendingChanges", "rateplan");
					} else {
						adddata.put("pendingChanges", "no pending changes");
					}

					List<String> planName = samapi.getPlanName(responseSAMAPI);

					if (planName != null) {
						adddata.put("planName", planName);
					}

					String Accountstatus = samapi.getstatus(responseSAMAPI);

					if (Accountstatus != null) {
						adddata.put("Accountstatus", Accountstatus);
					}

					List<Boolean> taxtype = samapi.getTaxType(responseSAMAPI);

					if (taxtype != null) {

						if (taxtype.get(0).booleanValue()) {
							adddata.put("taxtype", "TE");
						} else {
							adddata.put("taxtype", "TI");
						}
					}

					int nooflines = samapi.noOflines(responseSAMAPI);

					if (nooflines != 0) {
						adddata.put("nooflines", nooflines);
					}

					int rateplan = samapi.getRatePlanName(responseSAMAPI);

					List<String> ratePlantype = samapi.ratePlantype(responseSAMAPI);

					if (ratePlantype != null) {
						if (nooflines == rateplan) {
							adddata.put("plantype", "nonpooled");
						} else if (rateplan == 1 && nooflines > rateplan) {
							adddata.put("plantype", "pooled");
						} else if (rateplan != nooflines && rateplan > 1 && ratePlantype.contains("Data")) {
							adddata.put("plantype", "Hybrid");
						} else if (rateplan != nooflines && rateplan > 1 && !ratePlantype.contains("Data")) {
							adddata.put("plantype", "pooled");
						}

					}

					String protectionValue = samapi.getProtectionFromAddOnCategory(responseSAMAPI, misdin);

					if (StringUtils.isNoneEmpty(protectionValue)) {
						adddata.put("Protection", protectionValue);
					}

					// eospa

					Response respa = eospa.getResponsePA(getjwt);
					String aginghistory = eospa.getaginghistory(respa);
					if (aginghistory != null) {
						adddata.put("aginghistory", aginghistory);
					}

					// eos-usage
					Response usagesummary = eosusage.getResponseusagesummary(getjwt);
					String useddata = eosusage.getuseddata(usagesummary);
					if (useddata != null) {
						adddata.put("useddata", useddata);
					}

					String usedmessages = eosusage.getusedmessages(usagesummary);
					if (usedmessages != null) {
						adddata.put("usedmessages", usedmessages);
					}

					String usedcalls = eosusage.getusedcalls(usagesummary);
					if (usedcalls != null) {
						adddata.put("usedcalls", usedcalls);
					}

					String thirdparypurchases = eosusage.getthirdpartypurchases(usagesummary);
					if (thirdparypurchases != null) {
						adddata.put("thirdparypurchases", thirdparypurchases);
					}

					String tmobpurchases = eosusage.gettmobilepurchases(usagesummary);
					if (tmobpurchases != null) {
						adddata.put("tmobpurchases", tmobpurchases);
					}

					Response eosAALCheckResponse = eosAALCheck.getResponseeligibilityCheckAAL(getjwt);
					String isAALEligible = eosAALCheck.isAALEligible(eosAALCheckResponse);
					if (isAALEligible != null) {
						adddata.put("isAALEligible", isAALEligible);
					}
					
					Map<String, String> aalMsisdnStatusTypesVal = new HashMap<String, String>();
					aalMsisdnStatusTypesVal = eosAALCheck.aalMsisdnStatusType(eosAALCheckResponse);
					if(!aalMsisdnStatusTypesVal.isEmpty()){
						
						adddata.put("pendingRatePlanSoc", aalMsisdnStatusTypesVal.get("pendingRatePlanSoc"));
						adddata.put("eligibleLinesMbbLinesZero", aalMsisdnStatusTypesVal.get("eligibleLinesMbbLinesZero"));
						adddata.put("nonMatchingRatePlanSoc", aalMsisdnStatusTypesVal.get("nonMatchingRatePlanSoc"));
						adddata.put("maxLimitExceeded", aalMsisdnStatusTypesVal.get("maxLimitExceeded"));
						adddata.put("voiceLinesMaxedout", aalMsisdnStatusTypesVal.get("voiceLinesMaxedout"));
					}
				}

				else {

					System.out.println(misdin);
					adddata.put("login", "invalid");
					String ban = ecl.getbanfromstoreddata(misdin);
					if (!ban.equalsIgnoreCase("false")) {
						adddata.put("ban", ban);
					}

				}

				// jtestdata.put(adddata);

				System.out.println(adddata.toString(0));
				if (ecl.getjsondataformisdin(misdin).equalsIgnoreCase("true")) {
					ecl.putjsondataformisdin(misdin, adddata.toString(0));
				} else {
					ecl.postjsondataformisdin(misdin, adddata.toString(0));
				}

			}

			// rootval.put("testdata",jtestdata);
			// System.out.println(adddata.toString(0));

		} catch (Exception eq11) {
			eq11.printStackTrace();

		}
	}

	public Set<Object[]> getallmisdins(String excel) {
		Set<Object[]> misdinpassword = new HashSet<Object[]>();
		// Set<String> misdinpassword = new HashSet<>();
		try {

			Workbook workbook;
			FileInputStream excelFile = new FileInputStream(new File(excel));
			if (excel.contains("xlsx")) {
				workbook = new XSSFWorkbook(excelFile);
			} else {
				workbook = new HSSFWorkbook(excelFile);
			}

			Sheet datatypeSheet = workbook.getSheetAt(0);

			Iterator<Row> iterator = datatypeSheet.iterator();

			while (iterator.hasNext()) {

				Row currentRow = iterator.next();

				if (currentRow.getCell(0) != null && currentRow.getCell(1) != null) {

					String misdin = currentRow.getCell(0).getStringCellValue();
					String pwd = currentRow.getCell(1).getStringCellValue();
					String capability = currentRow.getCell(2).getStringCellValue();
					if (!misdin.trim().equals("")) {
						if (!misdin.equalsIgnoreCase("loginEmailOrPhone")) {

							misdinpassword
									.add(new Object[] { misdin.trim() + "/" + pwd.trim() + "/" + capability.trim() });

						}
					}
				}
			}
			workbook.close();
			excelFile.close();

		} catch (Exception e) {

		}
		return misdinpassword;
	}

}
