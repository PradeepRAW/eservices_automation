package com.tmobile.eservices.qa.payments.api;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.OrchestrateOrdersCollectionV2;
import com.tmobile.eservices.qa.pages.payments.api.SedonaApiCollectionV1;

import io.restassured.response.Response;

public class OrchestrateOrdersCollectionV2Test extends OrchestrateOrdersCollectionV2 {
	
	public Map<String, String> tokenMap;
	
	/**
	 * # US445494:MyTMO - PAYMENTS - Tokenization - Sedona OTP Flow 2: Partial Payments Successful and Create Order Successful:
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "OrchestrateOrdersCollectionV2","testOrchestrateOrdersSedonaPayment",Group.PAYMENTS,Group.SPRINT  })
	public void testOrchestrateOrdersSedonaPayment(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testOrchestrateOrdersSedonaPayment");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with OrchestrateOrders.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		
		String requestBody = new ServiceTest().getRequestFromFile("generatequote_OTP_Sedona.txt");	
        String quoteId = null;
        tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("chargeAmount", String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));
		//System.out.println(String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));
		String operationName="generatequote_OTP_Sedona";
		String minAmountToRestoreAccount=null;
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		SedonaApiCollectionV1 SedonaApiCollectionV1_Service = new SedonaApiCollectionV1();
		
		Response response = SedonaApiCollectionV1_Service.generatequote_OTP(apiTestData, updatedRequest, tokenMap);
	
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				 quoteId= getPathVal(jsonNode, "quoteId");
				 minAmountToRestoreAccount= getPathVal(jsonNode, "minAmountToRestoreAccount");
				 Assert.assertNotEquals(quoteId, "","Invalid quoteId.");
				 Assert.assertNotEquals(minAmountToRestoreAccount, "","Invalid minAmountToRestoreAccount.");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
		
		if(quoteId!=null){
			 requestBody = new ServiceTest().getRequestFromFile("OrchestrateOrders_SedonaV2.txt");	
			 operationName="testOrchestrateOrdersSedonaPayment";
			 tokenMap.put("quoteId", quoteId);
			 String orderId=null;
			 updatedRequest = prepareRequestParam(requestBody, tokenMap);
			 logRequest(updatedRequest, operationName);
			 response = orchestrateorders_sedona(apiTestData, updatedRequest, tokenMap);
		
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				Assert.assertTrue(response.statusCode()==200);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {
					 quoteId= getPathVal(jsonNode, "quoteId");
					 orderId= getPathVal(jsonNode, "orderId");
					 Assert.assertNotEquals(quoteId, "","Invalid quoteId.");
					 Assert.assertNotEquals(orderId, "","Invalid orderId.");
				}
			} else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory# US445492:MyTMO - PAYMENTS - Tokenization - Sedona OTP Flow 1: Full payment Successful, Create Order Successful
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { "OrchestrateOrdersCollectionV2","testOrchestrateOrdersSedonaFullPayment",Group.PAYMENTS,Group.SPRINT  })
	public void testOrchestrateOrdersSedonaFullPayment(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testOrchestrateOrdersSedonaFullPayment");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with OrchestrateOrders.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();
		
		String requestBody = new ServiceTest().getRequestFromFile("generatequote_OTP_Sedona.txt");	
        String quoteId = null;
        String minAmountToRestoreAccount=null;
        tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("chargeAmount", String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));
		//System.out.println(String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));
		String operationName="generatequote_OTP_Sedona";

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		SedonaApiCollectionV1 SedonaApiCollectionV1_Service = new SedonaApiCollectionV1();
		
		Response response = SedonaApiCollectionV1_Service.generatequote_OTP(apiTestData, updatedRequest, tokenMap);
	
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				 quoteId= getPathVal(jsonNode, "quoteId");
				 minAmountToRestoreAccount= getPathVal(jsonNode, "minAmountToRestoreAccount");
				 Assert.assertNotEquals(quoteId, "","Invalid quoteId.");
				 Assert.assertNotEquals(minAmountToRestoreAccount, "","Invalid minAmountToRestoreAccount.");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
		
		if(quoteId!=null){
			 requestBody = new ServiceTest().getRequestFromFile("OrchestrateOrders_SedonaV2.txt");	
			 operationName="testOrchestrateOrdersSedonaPayment";
			 tokenMap.put("quoteId", quoteId);
			 tokenMap.put("chargeAmount", minAmountToRestoreAccount);
			 String orderId=null;
			 updatedRequest = prepareRequestParam(requestBody, tokenMap);
			 logRequest(updatedRequest, operationName);
			 response = orchestrateorders_sedona(apiTestData, updatedRequest, tokenMap);
		
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {
					 quoteId= getPathVal(jsonNode, "quoteId");
					 orderId= getPathVal(jsonNode, "orderId");
					 Assert.assertNotEquals(quoteId, "","Invalid quoteId.");
					 Assert.assertNotEquals(orderId, "","Invalid orderId.");
				}
			} else {
				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}
		
	
	
	}
	
}
