package com.tmobile.eservices.qa.tmng.api;

import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.tmng.api.CreditCheckV1;
import com.tmobile.eservices.qa.shop.ShopConstants;

import io.restassured.response.Response;

public class CreditCheckV1Test extends CreditCheckV1 {

	JsonPath jsonPath;
	public Map<String, String> tokenMap;

	/**
	 * /** UserStory# Description:US546085 - qlab02 - TMNG credit check
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "eosTmngCreditCheck", "CreditCheckV1Test",
			Group.PENDING })
	public void eosTmngCreditCheck_US546085(ControlTestData data, ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		Reporter.log("TestName: tmng credit check");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log("Step 2: Verify expected credit check response |Response should be 200 OK");
		Reporter.log("Step 3: Verify credit Reference number|Refrence number should be there in response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmngCreditCheck.txt");
		String operationName = "Get credit check";
		tokenMap.put("ssn", apiTestData.getSsn());
		tokenMap.put("phoneNumber", apiTestData.getPhoneNumber());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(updatedRequest);
		if (!jsonNode.isMissingNode()) {
			jsonPath = new JsonPath(updatedRequest);

			Assert.assertEquals(jsonPath.get("customerInfo.ssn"), apiTestData.getSsn(), "SSN value does not match");
			Reporter.log("Ssn value match in request and in given payload");
		}

		Response response = createCreditCheck(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			mapper = new ObjectMapper();
			jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonPath jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("creditReferenceNumber"), "credit reference number is null");
				Reporter.log("credit reference number is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * UserStory# Description:US546085 - qlab02 - TMNG credit check
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "eosTmngCreditCheck", "CreditCheckV1Test",
			Group.PENDING })
	public void eosTmngCreditCheck_US546085_WithoutSSN(ControlTestData data, ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {
		Reporter.log("TestName: tmng credit check");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log("Step 2: Verify expected credit check response |Response should be 200 OK");
		Reporter.log("Step 3: Verify credit Reference number|Refrence number should be there in response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmngCreditCheckWithoutSSN.txt");
		String operationName = "Get credit check without ssn";
		tokenMap.put("phoneNumber", apiTestData.getPhoneNumber());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = createCreditCheck(apiTestData, updatedRequest, tokenMap);
		if (response != null && "400".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonPath jsonPath = new JsonPath(response.asString());
				Assert.assertEquals(jsonPath.get("details"), "Mandatory property 'ssn' is missing in the body.");
				Reporter.log("SSN missing in payload");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * /** UserStory# Description:US551290 - qlab02 - TMNG credit refresh through
	 * reference number
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "eosTmngCreditRefresh", "CreditCheckV1Test",
			Group.PENDING })
	public void eosTmngCreditRefresh_US551290(ControlTestData data, ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {
		Reporter.log("TestName: tmng credit refresh");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log("Step 2: Verify expected credit check response |Response should be 200 OK");
		Reporter.log("Step 3: Verify credit Reference number|Refrence number should be there in response");
		Reporter.log("Step 2: Verify EOS Credit refresh Request |Response should be 200 OK");
		Reporter.log(
				"Step 3: Verify credit Reference number ,application status and credit class |Refrence number ,application status and credit class should be there in response");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmngCreditCheck.txt");
		String operationName = "Get credit check";
		tokenMap.put("ssn", apiTestData.getSsn());
		tokenMap.put("phoneNumber", apiTestData.getPhoneNumber());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = createCreditCheck(apiTestData, updatedRequest, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				tokenMap.put("referenceNumber", getPathVal(jsonNode, "creditReferenceNumber"));

			}
		} else {
			failAndLogResponse(response, operationName);
		}
		response = createCreditRefresh(apiTestData, tokenMap);
		;
		operationName = "Get credit refresh";

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {

				Assert.assertNotNull(jsonPath.get("creditClass"), "credit class is null");
				Reporter.log("credit class is not null");

				Assert.assertNotNull(jsonPath.get("creditReferenceNumber"), "credit reference number is null");
				Reporter.log("credit reference number is not null");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * /** UserStory# Description:US546087 - qlab02 - check preScreen API
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "eosTmngCreditCheck", "CreditCheckV1Test",
			Group.PENDING })
	public void eosTmngCheckPreScreen_US546087(ControlTestData data, ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {
		Reporter.log("TestName: tmng credit check");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log("Step 2: Verify expected credit check response |Response should be 200 OK");
		Reporter.log(
				"Step 3: Verify credit Reference number ,application Status and credit class |Refrence number and credit class  should be there and application status should be incomplete");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmngCreditPreScreen.txt");
		String operationName = "Get tmng credit prescreen";
		tokenMap.put("ssn", apiTestData.getSsn());
		tokenMap.put("phoneNumber", apiTestData.getPhoneNumber());
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = createCreditCheck(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				JsonPath jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("creditReferenceNumber"), "credit reference number is null");
				Reporter.log("credit reference number is not null");

				Assert.assertNotNull(jsonPath.get("creditClass"), "credit class is null");
				Reporter.log("credit class is not null");

				Assert.assertEquals(jsonPath.get("applicationStatus"), "INCOMPLETE");
				Reporter.log("Application is in complete");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * /** UserStory# Description:US581052 - qlab03 - EOS to decrypt the PII data
	 * using EOS Private key - Hard Credit Check
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "eosTmngHardCreditCheckDecryptPIIData",
			"CreditCheckV1Test", Group.SHOP, Group.PROGRESSION })
	public void eosTmngHardCreditCheckDecryptPIIData_US581052(ControlTestData data, ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {
		Reporter.log("TestName: tmng credit check");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log("Step 2: Get encrypted ssn ,document id and DOB");
		Reporter.log(
				"Step 3: Verify expected Hard credit checkand PreScreen credit check response |Response should be 200 OK");
		Reporter.log(
				"Step 4: Verify credit Reference number ,application Status and credit class |Refrence number and credit class  should be there and application status should be incomplete");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmngHardCreditCheck.txt");
		String operationName = "Get identity hard credit check";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = createHardCreditCheck(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				JsonPath jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("creditReferenceNumber"), "credit reference number is null");
				Reporter.log("credit reference number is not null");

				Assert.assertNotNull(jsonPath.get("creditClass"), "credit class is null");
				Reporter.log("credit class is not null");

				Assert.assertEquals(jsonPath.get("applicationStatus"), "COMPLETE");
				Reporter.log("Application is in complete");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * /** UserStory# Description:US581052 - qlab03 - EOS to decrypt the PII data
	 * using EOS Private key - PreScreen
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "eosTmngPreScreenCreditCheck", "CreditCheckV1Test",
			Group.SHOP, Group.PROGRESSION })
	public void eosTmngPreScreenCreditCheckDecryptPIIData_US581052(ControlTestData data, ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {
		Reporter.log("TestName: tmng credit check");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log("Step 2: Get encrypted ssn ,document id and DOB");
		Reporter.log(
				"Step 3: Verify expected Hard credit checkand PreScreen credit check response |Response should be 200 OK");
		Reporter.log(
				"Step 4: Verify credit Reference number ,application Status and credit class |Refrence number and credit class  should be there and application status should be incomplete");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest()
				.getRequestFromFile(ShopConstants.TMNG_CREDITCHECK + "tmngCreditPreScreen.txt");
		String operationName = "Get identity PreScreen credit check";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = createHardCreditCheck(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				JsonPath jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("creditReferenceNumber"), "credit reference number is null");
				Reporter.log("credit reference number is not null");

				Assert.assertNotNull(jsonPath.get("creditClass"), "credit class is null");
				Reporter.log("credit class is not null");

				Assert.assertEquals(jsonPath.get("applicationStatus"), "INCOMPLETE");
				Reporter.log("Application is in complete");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * /** UserStory# Description:US581052 - qlab03 - EOS to decrypt the PII data
	 * using EOS Private key - PreScreen
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "eosTmngPreScreenCreditCheck", "CreditCheckV1Test",
			Group.SHOP, Group.PROGRESSION })
	public void eosTmngIntegrateValidateAddressAPI_US633800(ControlTestData data, ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {
		Reporter.log("TestName: tmng credit check");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log("Step 2: Get encrypted ssn ,document id and DOB");
		Reporter.log(
				"Step 3: Verify expected Hard credit checkand PreScreen credit check response |Response should be 200 OK");
		Reporter.log(
				"Step 4: Verify credit Reference number ,application Status and credit class |Refrence number and credit class  should be there and application status should be incomplete");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		String requestBody = new ServiceTest().getRequestFromFile("IntegradeAddressValidateAPI.txt");
		String operationName = "Get identity PreScreen credit check";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = createHardCreditCheck(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode() == 200);
				JsonPath jsonPath = new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("creditReferenceNumber"), "credit reference number is null");
				Reporter.log("credit reference number is not null");

				Assert.assertNotNull(jsonPath.get("creditClass"), "credit class is null");
				Reporter.log("credit class is not null");

				Assert.assertEquals(jsonPath.get("applicationStatus"), "INCOMPLETE");
				Reporter.log("Application is in complete");
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}
	
	/**
	 * UserStory# Description: CDCDWM-419 - TPP R2 - EOS Technical - Compare 200 OK response from backend for changes
	 * 
	 * @param data
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void eosValidAddressValidationForCreditCheckAPI(ControlTestData data,
			ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {

		Reporter.log("TestName: tmng address service validation");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log(
				"Step 2: Verify createCreditCheck service response with Valid Address in pay load| Response should be 200 for Valid address");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
	}

	/**
	 * UserStory# Description: CDCDWM-419 - TPP R2 - EOS Technical - Compare 200 OK response from backend for changes
	 * 
	 * @param data
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void eosInValidAddressValidationForCreditCheckAPI(ControlTestData data,
			ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {

		Reporter.log("TestName: tmng address service validation");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get oauth token ");
		Reporter.log(
				"Step 2: Verify createCreditCheck service response with InValid Address in pay load| Response should be 400 for InValid address");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
	}
}
