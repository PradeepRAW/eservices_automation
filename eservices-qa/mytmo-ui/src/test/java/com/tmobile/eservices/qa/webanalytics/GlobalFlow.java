package com.tmobile.eservices.qa.webanalytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;

public class GlobalFlow extends GlobalCommonLib {

	AnalyticsLib al = new AnalyticsLib();
	SoftAssert softAssert = new SoftAssert();
	/***
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "analytics1" })
	public void testHomePageAnalytics(ControlTestData data, MyTmoData myTmoData){
		navigateToHomePage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5","my/home.html");
		pageTags.put("pageName","my/home.html");
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
}
