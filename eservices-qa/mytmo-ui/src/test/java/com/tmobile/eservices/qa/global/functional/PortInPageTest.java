package com.tmobile.eservices.qa.global.functional;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.global.PortInPage;

public class PortInPageTest extends GlobalCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(PortInPageTest.class);	

	/**
	 * US441625 - Port-In: Confirmation page
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING})
	public void verifyPortInConfPage(ControlTestData data, MyTmoData myTmoData) {
		
		logger.info("verifyPortIn Confirmation");
		Reporter.log("Test Case 1: Ensure the User should be Login to MyTMO Application:");
		Reporter.log("Test Steps 					| Expected Test Results:");
		Reporter.log("1. Launch the application		| Application Should be Launched");
		Reporter.log("2. Login to the application	| User Should be login successfully");
		Reporter.log("======================================================================");
		Reporter.log("Actual Test Results:");
		launchAndPerformLogin(myTmoData);
		getDriver().manage().timeouts().setScriptTimeout(80, TimeUnit.SECONDS);
		Reporter.log("======================================================================");
		Reporter.log("Test Case 2: Ensure Port-In Confirmation Page should display with expected values:");
		Reporter.log("Test Steps 							| Expected Test Results:");
		Reporter.log("1. Verify Confirmation Page			| Confirmation Page should be displayed");
		Reporter.log("2. Verify Confirmation Page Status	| Authorable Status should be displayed");
		Reporter.log("3. Verify Confirmation Page Header	| Authorable Header should be displayed");
		Reporter.log("4. Verify Confirmation Page SubHeader	| Authorable SubHeader should be displayed");
		Reporter.log("5. Verify 'All Done' Button 			| 'All Done' Authorable Button should be displayed on page load");
		Reporter.log("6. Verify 'All Done' Button 			| 'All Done' Authorable Button should be enabled on page load");
		Reporter.log("7. Verify Header Font Style			| Confirmation Header Font Style should be displayed");
		Reporter.log("8. Verify SubHeader Font Style		| Confirmation SubHeader Font Style should be displayed");
		Reporter.log("======================================================================");
		Reporter.log("Actual Test Results:");
		getDriver().get("https://e9.my.t-mobile.com/port-in/confirmation");
		getDriver().manage().timeouts().setScriptTimeout(80, TimeUnit.SECONDS);
		PortInPage portinPage = new PortInPage(getDriver());
		portinPage.verifyConfirmPage();
		portinPage.verifyConfStatus();
		portinPage.verifyConfHeader();
		portinPage.verifyConfSubHeader();
		portinPage.verifyAllDoneButton();
		portinPage.verifyAllDoneButtonEnable();
		portinPage.readHeaderFontProperty();
		getDriver().manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
		getDriver().close();
	}	
	
	/**
	 *  US441827 - Port-In: Input Page
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.PENDING})
	public void verifyPortInInputPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPortIn Input Page");
		Reporter.log("Test Case 1: Ensure the User should be Login to MyTMO Application:");
		Reporter.log("Test Steps 					| Expected Test Results:");
		Reporter.log("1. Launch the application		| Application Should be Launched");
		Reporter.log("2. Login to the application	| User Should be login successfully");
		Reporter.log("======================================================================");
		Reporter.log("Actual Test Results:");
		launchAndPerformLogin(myTmoData);
		getDriver().manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
		Reporter.log("======================================================================");
		Reporter.log("Test Case 2: Ensure Port-In Phone Number Input Page should display with expected values:");
		Reporter.log("Test Steps 							| Expected Test Results:");
		Reporter.log("1. Verify Phone Number Input Page		| Phone Number Input Page should be displayed");
		Reporter.log("2. Verify Input Page Status 			| Authorable Status should be displayed");
		Reporter.log("3. Verify Input Page Header			| Authorable Header should be displayed");
		Reporter.log("4. Verify Input Page SubHeader 		| Authorable SubHeader should be displayed");
		Reporter.log("5. Verify Input Page Phone Number		| Input Page Phone Number field should be displayed");
		Reporter.log("6. Verify Input Page Helper Text		| Input Page Helper Text should be displayed");
		Reporter.log("7. Verify 'Submit' Button Displayed	| Authorable 'Submit' Button should be displayed");
		Reporter.log("8. Verify 'Submit' Button Disabled	| Authorable 'Submit' Button should be disabled on page load");
		Reporter.log("9. Verify Phone Number Error Text		| Invalid Phone Number error message should displayed");
		Reporter.log("10. Verify Enter Phone Number			| Phone Number field should be allow to enter the value");
		Reporter.log("11. Verify Phone Number validations	| Allow to be enter valid Phone Number");		
		Reporter.log("12. Verify 'Submit' Button Enabled	| Authorable 'Submit' Button should be enabled by entering the phone number");	
		Reporter.log("======================================================================");
		Reporter.log("Actual Test Results:");
		getDriver().get("https://e9.my.t-mobile.com/port-in/input");
		getDriver().manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
		CommonPage commonpage = new CommonPage(getDriver());
		commonpage.verifyCurrentPageTitle("My T-Mobile");
		PortInPage portinPage = new PortInPage(getDriver());
		portinPage.verifyInputPage();
		portinPage.verifyInputPageStatus();
		portinPage.verifyInputPageHeader();
		portinPage.verifyInputPageSubHeader();
		portinPage.verifyPhoneNumberTextField();
		portinPage.verifyInputHelper();
		portinPage.verifySubmitButtonIsDisplayedonpageload();
		portinPage.verifySubmitButtonIsDisabledonpageload();
		portinPage.verifyInvalidPhoneError();
		portinPage.enterPhoneNumber();
		portinPage.validPhoneNumber();
		portinPage.verifySubmitButtonIsEnabledByEnterPhoneNumber();
		getDriver().manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
		getDriver().close();
	}
}