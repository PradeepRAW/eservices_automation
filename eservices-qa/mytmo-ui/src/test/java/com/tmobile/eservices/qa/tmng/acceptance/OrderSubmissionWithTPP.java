package com.tmobile.eservices.qa.tmng.acceptance;


import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.CartPage;
import com.tmobile.eservices.qa.pages.tmng.functional.CheckOutPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPOfferPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPPreScreenForm;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPPreScreenIntro;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPManualorFraudReviewPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class OrderSubmissionWithTPP extends TmngCommonLib {
	
	/**
	 *  US585262:TPP - Display POST verified Price version of PLP
	 * 	C543836 - PLP(cell phones) :Verify that accurate pricing should be displayed after successful pre-screen
	 * C543851 - Verify on Mobile
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.DESKTOP,  Group.TPP})
	public void testVerifyAccuratePricingAfterPreScreeningInPhonesPLP(TMNGData tMNGData) {
		Reporter.log(" US585262:TPP - Display POST verified Price version of PLP");
		Reporter.log("C543836 - PLP(cell phones) :Verify that accurate pricing should be displayed after successful prescreen");
		Reporter.log("C543851 - Verify on Mobile");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log(
				"3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing should be displayed on PLP ");
		Reporter.log("4. Click 'Find your pricing' | pre-screen intro page should be displayed.");
		Reporter.log("5. Enter required information and  click on the 'Find my price' CTA | pre-screen processing page should be displayed.");
		Reporter.log("6. Verify processing page is displayed when the pre-screen is being done  | Processing page should be displayed after pre-screen");
		Reporter.log("7. Verify header on proceessing page  | 'Hi <Customer name> , you'll now see your verified pricing reflected throughout your shopping exoerience' header on Processing page should be displayed.");
		Reporter.log("8. Click Got it CTA | the new price based on the credit class is reflected on the PLP Page.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PlpPage phonesPlp = navigateToPhonesPlpPage(tMNGData);
		phonesPlp.verifyCRPorAwesomePricesInPLP();
		phonesPlp.verifyYourPricingBoxInPLP();
		phonesPlp.verifyYourPricingBoxTextInPLP();
		phonesPlp.verifyFindYourPricingLink();
		phonesPlp.clickFindYourPricingLink();
		
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		
		TPPPreScreenForm tppPrescreenform = new TPPPreScreenForm(getDriver());
		tppPrescreenform.verifyPreScreenForm();
		tppPrescreenform.verifyPreScreenFormPageURL();
		tppPrescreenform.verifyPreScreenFormPageTitle();
		tppPrescreenform.fillAllFieldsOnPreScreenform(tMNGData);
		tppPrescreenform.clickFindMyPriceCTAOnPreScreenForm();	
		tppPrescreenform.verifyPreScreenloadingPage();
		
		TPPOfferPage offerPage = new TPPOfferPage(getDriver());
		offerPage.verifyTPPOfferPage();
		offerPage.clickGotItCTA();
		phonesPlp.verifyFindYourPricingDisable();
		
		phonesPlp.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage pdpPage =  new PdpPage(getDriver());
		
		pdpPage.verifyPhonePdpPageLoaded();
		pdpPage.verifyFindYourPricingDisable();
		
		pdpPage.clickOnAddToCartBtn();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifyFindYourPricingLinkNotDisplayedInCart();
		
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesSCC(tMNGData);
		//checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		//checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());
		
	}
	
	/**
	 * US575533:TPP - Prescreen - Display Prescreen form page
	 * C543879 - PDP:Verify Find my price CTA - Prescreen Offer Page
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP , Group.TPP  })
	public void verifyPrescreenOfferPageFromPDP(TMNGData tMNGData) {
		Reporter.log("US575533:TPP - Prescreen - Display Prescreen form page");
		Reporter.log("C543879 - PDP:Verify Find my price CTA - Prescreen Offer Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing" +
				" should be displayed on PLP ");
		Reporter.log("4. Select any device and Verify 'Find your pricing' link on PDP | 'Find your pricing' link should be displayed in PDP");
		Reporter.log("5. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("6. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log("7. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("8. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("9. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("10. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log("11. Verify pre screen page after loading page | Prescreen Offer Page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdp = navigateToPhonesPDP(tMNGData);
		phonesPdp.verifyCRPorAwesomePricesInPDP();
		phonesPdp.verifyYourPricingBoxInPDP();
		phonesPdp.verifyYourPricingBoxTextInPDP();
		phonesPdp.verifyFindYourPricingLinkInPDP();
		phonesPdp.clickFindYourPricingLinkInPDP();

		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();

		TPPPreScreenForm tppPrescreenform = new TPPPreScreenForm(getDriver());
		tppPrescreenform.verifyPreScreenForm();
		tppPrescreenform.verifyPreScreenFormPageURL();
		tppPrescreenform.verifyPreScreenFormPageTitle();
		tppPrescreenform.fillAllFieldsOnPreScreenform(tMNGData);
		tppPrescreenform.clickFindMyPriceCTAOnPreScreenForm();
		tppPrescreenform.verifyPreScreenloadingPage();

		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();
		tppOfferPage.clickGotItCTA();
		
		phonesPdp.verifyFindYourPricingDisable();
		
		phonesPdp.clickOnAddToCartBtn();
		phonesPdp.selectContinueAsGuest();
		
		CartPage cartPage = new CartPage(getDriver());
		
		cartPage.verifyCartPageLoaded();
		cartPage.verifyFindYourPricingLinkNotDisplayedInCart();
		
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesSCC(tMNGData);
		//checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		//checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());

	}
	
	/**
	 * US588475:TPP - Prescreen: show Pre Screen Offer page at the end of Prescreen
	 * flow (PLP/PDP/CART) C543929 - Cart:Verify Find my price CTA -Prescreen Offer
	 * Page
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void verifyPrescreenOfferPageFromCart(TMNGData tMNGData) {
		Reporter.log(
				"US588475:TPP - Prescreen: show Pre Screen Offer page at the end of Prescreen flow (PLP/PDP/CART)");
		Reporter.log("C543929 - Cart:Verify Find my price CTA - Prescreen Offer Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log(
				"3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing"
						+ " should be displayed on PLP ");
		Reporter.log("4. Select any device and click on Add to cart from PDP page | Cart page should be displayed");
		Reporter.log("5. Verify 'Find your pricing' link on cart page | 'Find your pricing' link should be displayed");
		Reporter.log("6. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("7. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"8. Click on 'Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("9. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("10. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("11. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log("12. Verify pre screen page after loading page | Prescreen Offer Page should be displayed");
		Reporter.log(
				"13. Click on 'GOT IT' cta from Prescreen Offer Page | User should be redirected to entry point for customer to"
						+ " pre screen (plp/lpdp/cart) with updated pricing should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyFindYourPricingCTA();
		cartPage.clickFindYourPricingCTA();

		TPPPreScreenIntro tppIntro = new TPPPreScreenIntro(getDriver());
		tppIntro.clickLetsGoCTAOnPreScreenIntroPage();

		TPPPreScreenForm tppScreenForm = new TPPPreScreenForm(getDriver());
		tppScreenForm.verifyPreScreenForm();
		tppScreenForm.verifyPreScreenFormPageURL();
		tppScreenForm.verifyPreScreenFormPageTitle();
		tppScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppScreenForm.verifyPreScreenloadingPage();
		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();
		tppOfferPage.clickGotItCTA();

		cartPage.verifyCartPageLoaded();
		cartPage.verifyFindYourPricingLinkNotDisplayedInCart();
		
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesSCC(tMNGData);
		//checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		//checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());
		

	}
	
	/**
     * :TEST ONLY - update pricing sticky on checkout based on HCC/SCC flag (estimated / non estimated pricing)
     *
     * @param data
     * @param tMNGData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.DESKTOP, Group.TPP})
    public void testEstimatedOnCheckOutPageWhenSccIsNotPassed(TMNGData tMNGData) {
        Reporter.log("US589245:TEST ONLY - update pricing sticky on checkout based on HCC/SCC flag (estimated / non estimated pricing)");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch TMO | TMO should be launched");
        Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
        Reporter.log("3. Select Device | PDP page should be displayed");
        Reporter.log("4. Click on Add to cart button | New to T-Mobile and Existing customer CTA should pop up");
        Reporter.log("5. Click on continue as guest  | Cart Page should  be displayed");
        Reporter.log("6. Click on Continue button on sticky banner | Checkout page should be displayed");
        Reporter.log("7. Fill all personal information on 'Personal Info' tab and click on next | 'Credit Check' tab should be displayed");
        Reporter.log("8. Fill Credit check fields and click Next button | Shipping info tab should be displayed");
        Reporter.log("9. Fill the Shipping and Payment details and click on Agree & Next CTA | Should be navigated to 'Review and Submit order' tab");
        Reporter.log("10. Verify Price details | 'Today/Monthly' label should be displayed above the prices");

        Reporter.log("================================");
        Reporter.log("Actual Output:");


        CheckOutPage checkOutPage = navigateToCheckoutWithPhone(tMNGData);
        checkOutPage.verifyCheckOutPageLoaded();
        checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);
        
        //checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
      	//checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());

    }
    
    /**
     * Prescreen – “Manual Review”
     *
     * @param data
     * @param tMNGData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT, Group.TPP})
    public void testCheckOutwithPreScreenManualReview(TMNGData tMNGData) {
        Reporter.log("Prescreen – Incomplete");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log(
				"3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing"
						+ " should be displayed on PLP ");
		Reporter.log("4. Select any device and click on Add to cart from PDP page | Cart page should be displayed");
		Reporter.log("5. Verify 'Find your pricing' link on cart page | 'Find your pricing' link should be displayed");
		Reporter.log("6. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("7. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"8. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("9. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("10. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("11. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log("12. Verify pre screen page after loading page | Manual Review page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyFindYourPricingCTA();
		cartPage.clickFindYourPricingCTA();

		TPPPreScreenIntro tppIntro = new TPPPreScreenIntro(getDriver());
		tppIntro.clickLetsGoCTAOnPreScreenIntroPage();

		TPPPreScreenForm tppScreenForm = new TPPPreScreenForm(getDriver());
		tppScreenForm.verifyPreScreenForm();
		TPPManualorFraudReviewPage tppManualReviewPage = new TPPManualorFraudReviewPage(getDriver());
		tppManualReviewPage.verifyManualRevieworFraudDetectionPage();
		
		/*cartPage.clickOnCartContinueBtn();
        CheckOutPage checkOutPage = new CheckOutPage(getDriver());
        checkOutPage.verifyCheckOutPageLoaded();
        checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);*/
        
        //checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
      	//checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());

    }
    
    /**
     * HCC – “Manual Review”
     *
     * @param data
     * @param tMNGData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT, Group.TPP})
    public void testCheckOutwithHCCManualReview(TMNGData tMNGData) {
        Reporter.log("HCC – Manual Review");
        Reporter.log("================================");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("1. Launch TMO | TMO should be launched");
        Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
        Reporter.log("3. Select Device | PDP page should be displayed");
        Reporter.log("4. Click on Add to cart button | New to T-Mobile and Existing customer CTA should pop up");
        Reporter.log("5. Click on continue as guest  | Cart Page should  be displayed");
        Reporter.log("6. Click on Continue button on sticky banner | Checkout page should be displayed");
        Reporter.log("7. Fill all personal information on 'Personal Info' tab and click on next | 'Credit Check' tab should be displayed");
        Reporter.log("8. Fill Credit check fields and click Next button | Shipping info tab should be displayed");
        Reporter.log("9. Fill the Shipping and Payment details and click on Agree & Next CTA | Should be navigated to 'Review and Submit order' tab");
        Reporter.log("10. Verify Price details | 'Today/Monthly' label should be displayed above the prices");

        Reporter.log("================================");
        Reporter.log("Actual Output:");


        CheckOutPage checkOutPage = navigateToCheckoutWithPhone(tMNGData);
        checkOutPage.verifyCheckOutPageLoaded();
        
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.fillResidentialAddressforCreditCheck(tMNGData);
		checkOutPage.clickRequiredCheckBoxForCreditCheck();
		checkOutPage.clickRunCreditCheck();
		
		TPPManualorFraudReviewPage tppManualReviewPage = new TPPManualorFraudReviewPage(getDriver());
		tppManualReviewPage.verifyManualRevieworFraudDetectionPage();

    }



}