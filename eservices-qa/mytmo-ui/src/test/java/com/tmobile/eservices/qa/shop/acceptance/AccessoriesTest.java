package com.tmobile.eservices.qa.shop.acceptance;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionConfirmationPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class AccessoriesTest extends ShopCommonLib {

	/**
	 * Verify PAH User Accessories Integration Choosing EIP Device and Nine
	 * Accessories With TradeIn
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
	public void testPAHUserAccessoriesIntegrationChoosingEIPDeviceandNineAccessoriesWithTradeIn(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : Verify PAH User Accessories Integration Choosing EIP Device and Nine Accessories(EIP and FRP) With Trade In Flow");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on select device | PDP page should be displayed");
		Reporter.log("6. Choose EIP and Click on Add to cart | Line slector page should be displayed");
		Reporter.log("7. Click on First line|Phone selection page should be displayed");
		Reporter.log("8. Click on First Device|Trade In Device Condition page should be displayed");
		Reporter.log("9. Click its good radio button & click continue|Trade in value page should be displayed");
		Reporter.log("10. Click continue|Insurance Migration page should be displayed");
		Reporter.log("11. Click Continue In Insurance Migration Page |Accessories List Page should be displayed");
		Reporter.log("12. Select Nine Accessories And Click Continue |  Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = navigateToTradeInDeviceConditionConfirmationPageByFeatureDevicesWithTradeInFlow(myTmoData,
				myTmoData.getDeviceName());
		tradeInDeviceConditionConfirmationPage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.checkPageIsReady();
		if (deviceProtectionPage.verifyDeviceProtectionURL() == true) {
			deviceProtectionPage.clickContinueButton();
			deviceProtectionPage.checkPageIsReady();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		/*
		 * cartPage.clickAcceptAndPlaceOrder(); OrderConfirmationPage
		 * orderConfirmationPage = new OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}

	/**
	 * Verify
	 * PAHUserAccessoriesIntegrationChoosingEIPDeviceandOneAccessoriesWithSkipTradeInFlow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups ={  Group.PENDING })
	public void testPAHUserAccessoriesIntegrationChoosingEIPDeviceandOneAccessoriesWithSkipTradeInFlow(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : Verify PAH User Accessories Integration Choosing EIP Device and One Accessories With Skip Trade In Flow");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on select device | PDP page should be displayed");
		Reporter.log("7. Choose EIP and Click  Add to cart | Line slector page should be displayed");
		Reporter.log("8. Click on First line|Phone selection page should be displayed");
		Reporter.log("9. Click on Skip Trade In|Insurance Migration page should be displayed");
		Reporter.log("10. Click Continue In Insurance Migration Page |Accessories List Page should be displayed");
		Reporter.log("11. Select One Accessory And Click Continue |  Cart page should be displayed");
		Reporter.log(
				"12. Verify Accessory device edit button in cartpage |Accessories device edit button should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.checkPageIsReady();
		if (deviceProtectionPage.verifyDeviceProtectionURL() == true) {
			deviceProtectionPage.clickContinueButton();
			deviceProtectionPage.checkPageIsReady();
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyAccessoryDeviceEditButton();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();

		/*
		 * cartPage.verifyAcceptAndContinue();
		 * cartPage.clickAcceptAndPlaceOrder(); OrderConfirmationPage
		 * orderConfirmationPage = new OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}

	/**
	 * Verify
	 * PAHUserAccessoriesIntegrationChoosingEIPDeviceandSecondLineWithOneAccessories
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
	public void testPAHUserAccessoriesIntegrationChoosingEIPDeviceandSecondLineWithOneAccessories(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : Verify PAH User Accessories Integration Choosing One EIP Device and Selecting Second Device and One Accessory");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on select device | PDP page should be displayed");
		Reporter.log("6. Choose EIP and Click on Add to cart | Line slector page should be displayed");
		Reporter.log("7. Click on Second line|Phone selection page should be displayed");
		Reporter.log("8. Click on Skip Trade In|Insurance Migration page should be displayed");
		Reporter.log("9. Click Continue In Insurance Migration Page |Accessories List Page should be displayed");
		Reporter.log("10. Select One Accessory And Click Continue |  Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageByFeatureDevices(myTmoData);
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		lineSelectorDetailsPage.clickOnSkipTradeInLink();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();

		/*
		 * cartPage.verifyAcceptAndContinue();
		 * cartPage.clickAcceptAndPlaceOrder(); OrderConfirmationPage
		 * orderConfirmationPage = new OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}

	/**
	 * Verify EIP Trade-In With PHP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testPAHUserAccessoriesIntegrationChoosingEIPDevicewithTradeInAnotherDeviceEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify EIP Trade-In With PHP");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on select device | PLP page should be displayed");
		Reporter.log("6. Click on the device | PDP page should be displayed");
		Reporter.log("7. Clcik on Add to cart | Line slector page should be displayed");
		Reporter.log("8. Click on line |Trade in another page should be displayed");
		Reporter.log("9. Select trade in info & click continue button | Device condition page should be displayed");
		Reporter.log("10. Click on continue button | Trade-In value page should be displayed");
		Reporter.log("11. Click on continue button | Product list page should be displayed");
		Reporter.log("12. Click on device | Product details page should be displayed");
		Reporter.log("13. Click on add to cart | Cart page should be displayed");
		Reporter.log("14. Click Continue In Insurance Migration Page |Accessories List Page should be displayed");
		Reporter.log("15. Select One Accessory And Click Continue |  Cart page should be displayed");
		Reporter.log(
				"16. Fill payment information & click accept & continue | Order confirmation page should be displayed");
		Reporter.log("17. Verify order success message | Order success message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToTradeInDeviceConditionConfirmationPageBySeeAllPhones(myTmoData);
		TradeInDeviceConditionConfirmationPage tradeInValuePage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInValuePage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();

		/*
		 * cartPage.verifyAcceptAndContinue();
		 * cartPage.clickAcceptAndPlaceOrder(); OrderConfirmationPage
		 * orderConfirmationPage = new OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}


	/**
	 * Verify EIP Trade-In With PHP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.PENDING })
	public void testPAHUserAccessoriesIntegrationChoosingEIPDevicewithTradeInwithOneAccessory(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify EIP Trade-In With PHP");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on select device | PLP page should be displayed");
		Reporter.log("6. Click on the device | PDP page should be displayed");
		Reporter.log("7. Clcik on Add to cart | Line slector page should be displayed");
		Reporter.log("8. Click on line |Trade in another page should be displayed");
		Reporter.log("9. Select trade in info & click continue button | Device condition page should be displayed");
		Reporter.log("10. Click on continue button | Trade-In value page should be displayed");
		Reporter.log("11. Click on continue button | Product list page should be displayed");
		Reporter.log("12. Click on device | Product details page should be displayed");
		Reporter.log("13. Click on add to cart | Cart page should be displayed");
		Reporter.log("14. Click Continue In Insurance Migration Page |Accessories List Page should be displayed");
		Reporter.log("15. Select 5 Accessory And Click Continue |  Cart page should be displayed");
		Reporter.log(
				"16. Fill payment information & click accept & continue | Order confirmation page should be displayed");
		Reporter.log("17. Verify order success message | Order success message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();

		/*
		 * cartPage.verifyAcceptAndContinue();
		 * cartPage.clickAcceptAndPlaceOrder(); OrderConfirmationPage
		 * orderConfirmationPage = new OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}

	/**
	 * Verify
	 * PAHUserAccessoriesIntegrationChoosingFRPDeviceNineAccessoriesWithSkipTradeInFlow
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testPAHUserAccessoriesIntegrationChoosingFRPDeviceNineAccessoriesWithSkipTradeInFlow(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : Verify PAH User Accessories Integration Choosing One FRP Device and One Accessory With Skip Trade In Flow");
		Reporter.log("Data Condition | IR STD PAH Customer With Trade-In Eligibility");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Phone page should be displayed");
		Reporter.log("5. Click on select device | PLP page should be displayed");
		Reporter.log("6. Click on the device | PDP page should be displayed");
		Reporter.log("7. Choose FRP and Click on Add to cart | Line slector page should be displayed");
		Reporter.log("8. Click on First line|Phone selection page should be displayed");
		Reporter.log("9. Click on Skip Trade In|Insurance Migration page should be displayed");
		Reporter.log("10. Click Continue In Insurance Migration Page |Accessories List Page should be displayed");
		Reporter.log("11. Select Nine Accessories And Click Continue |  Cart page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageByFeatureDevices(myTmoData);
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		lineSelectorDetailsPage.clickOnSkipTradeInLink();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		//accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();

		/*
		 * cartPage.verifyAcceptAndContinue();
		 * cartPage.clickAcceptAndPlaceOrder(); OrderConfirmationPage
		 * orderConfirmationPage = new OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}

	/**
	 * Verify
	 * PAHUserAccessoriesIntegrationChoosingFRPDeviceandOneAccessorieWithTradeIn
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.PENDING })
	public void testPAHUserAccessoriesIntegrationChoosingFRPDeviceandOneAccessorieWithTradeIn(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : Verify PAH User Accessories Integration Choosing FRP Device and Nine Accessories(EIP and FRP) With Trade In Flow");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on select device | PDP page should be displayed");
		Reporter.log("6. Choose FRP and Click on Add to cart | Line slector page should be displayed");
		Reporter.log("7. Click on First line|Phone selection page should be displayed");
		Reporter.log("8. Click on First Device|Trade In Device Condition page should be displayed");
		Reporter.log("9. Click its good radio button & click continue|Trade in value page should be displayed");
		Reporter.log("10. Click continue|Insurance Migration page should be displayed");
		Reporter.log("11. Click Continue In Insurance Migration Page |Accessories List Page should be displayed");
		Reporter.log("12. Select One Accessory And Click Continue |  Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		TradeInDeviceConditionConfirmationPage tradeInValuePage = navigateToTradeInDeviceConditionConfirmationPageByFeatureDevicesWithTradeInFlow(myTmoData,
				myTmoData.getDeviceName());
		tradeInValuePage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();

		/*
		 * cartPage.verifyAcceptAndContinue();
		 * cartPage.clickAcceptAndPlaceOrder(); OrderConfirmationPage
		 * orderConfirmationPage = new OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}

	/**
	 * Verify FRP Trade-In With PHP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.PENDING})
	public void testPAHUserAccessoriesIntegrationChoosingFRPDevicewithTradeInAnotherDevicewithMultipleAccessories(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify EIP Trade-In With PHP");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on select device | PLP page should be displayed");
		Reporter.log("6. Click on the device | PDP page should be displayed");
		Reporter.log("7. Clcik on Add to cart | Line slector page should be displayed");
		Reporter.log("8. Click on line |Trade in another page should be displayed");
		Reporter.log("9. Select trade in info & click continue button | Device condition page should be displayed");
		Reporter.log("10. Click on continue button | Trade-In value page should be displayed");
		Reporter.log("11. Click on continue button | Product list page should be displayed");
		Reporter.log("12. Click on device | Product details page should be displayed");
		Reporter.log("13. Click on add to cart | Cart page should be displayed");
		Reporter.log("14. Click Continue In Insurance Migration Page |Accessories List Page should be displayed");
		Reporter.log("15. Select 5 Accessory And Click Continue |  Cart page should be displayed");
		Reporter.log(
				"16. Fill payment information & click accept & continue | Order confirmation page should be displayed");
		Reporter.log("17. Verify order success message | Order success message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToTradeInDeviceConditionConfirmationPageBySeeAllPhones(myTmoData);
		TradeInDeviceConditionConfirmationPage tradeInValuePage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInValuePage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();

		/*
		 * cartPage.verifyAcceptAndContinue();
		 * cartPage.clickAcceptAndPlaceOrder(); OrderConfirmationPage
		 * orderConfirmationPage = new OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}

	/**
	 * Verify EIP Trade-In With PHP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.PENDING })
	public void testPAHUserAccessoriesIntegrationChoosingFRPDevicewithTradeInAnotherDevicewithoneAccessories(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify EIP Trade-In With PHP");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on select device | PLP page should be displayed");
		Reporter.log("6. Click on the device | PDP page should be displayed");
		Reporter.log("7. Clcik on Add to cart | Line slector page should be displayed");
		Reporter.log("8. Click on line |Trade in another page should be displayed");
		Reporter.log("9. Select trade in info & click continue button | Device condition page should be displayed");
		Reporter.log("10. Click on continue button | Trade-In value page should be displayed");
		Reporter.log("11. Click on continue button | Product list page should be displayed");
		Reporter.log("12. Click on device | Product details page should be displayed");
		Reporter.log("13. Click on add to cart | Cart page should be displayed");
		Reporter.log("14. Click Continue In Insurance Migration Page |Accessories List Page should be displayed");
		Reporter.log("15. Select 5 Accessory And Click Continue |  Cart page should be displayed");
		Reporter.log(
				"16. Fill payment information & click accept & continue | Order confirmation page should be displayed");
		Reporter.log("17. Verify order success message | Order success message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToTradeInDeviceConditionConfirmationPageBySeeAllPhones(myTmoData);
		TradeInDeviceConditionConfirmationPage tradeInValuePage = new TradeInDeviceConditionConfirmationPage(getDriver());
		tradeInValuePage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL()) {
			deviceProtectionPage.clickContinueButton();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();

		/*
		 * cartPage.verifyAcceptAndContinue();
		 * cartPage.clickAcceptAndPlaceOrder(); OrderConfirmationPage
		 * orderConfirmationPage = new OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}

	/**
	 * Verify FRP Trade-In With PHP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.PENDING })
	public void testPAHUserAccessoriesIntegrationChoosingFRPDevicewithTradeInwithMultipleAccessories(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify EIP Trade-In With PHP");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on select device | PLP page should be displayed");
		Reporter.log("6. Click on the device | PDP page should be displayed");
		Reporter.log("7. Clcik on Add to cart | Line slector page should be displayed");
		Reporter.log("8. Click on line |Trade in another page should be displayed");
		Reporter.log("9. Select trade in info & click continue button | Device condition page should be displayed");
		Reporter.log("10. Click on continue button | Trade-In value page should be displayed");
		Reporter.log("11. Click on continue button | Product list page should be displayed");
		Reporter.log("12. Click on device | Product details page should be displayed");
		Reporter.log("13. Click on add to cart | Cart page should be displayed");
		Reporter.log("14. Click Continue In Insurance Migration Page |Accessories List Page should be displayed");
		Reporter.log("15. Select 5 Accessory And Click Continue |  Cart page should be displayed");
		Reporter.log(
				"16. Fill payment information & click accept & continue | Order confirmation page should be displayed");
		Reporter.log("17. Verify order success message | Order success message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		TradeInDeviceConditionConfirmationPage tradeInValuePage = navigateToTradeInDeviceConditionConfirmationPageByFeatureDevicesWithTradeInFlow(myTmoData, myTmoData.getDeviceName());
		tradeInValuePage.clickOnTradeInThisPhoneButton();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.checkPageIsReady();
		if (deviceProtectionPage.verifyDeviceProtectionURL() == true) {
			deviceProtectionPage.clickContinueButton();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();

		/*
		 * cartPage.verifyAcceptAndContinue();
		 * cartPage.clickAcceptAndPlaceOrder(); OrderConfirmationPage
		 * orderConfirmationPage = new OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}

	/**
	 * Verify EIP Trade-In With PHP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {  Group.PENDING})
	public void testPAHUserAccessoriesIntegrationChoosingFRPDevicewithTradeInwithOneAccessory(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify EIP Trade-In With PHP");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on select device | PLP page should be displayed");
		Reporter.log("6. Click on the device | PDP page should be displayed");
		Reporter.log("7. Clcik on Add to cart | Line slector page should be displayed");
		Reporter.log("8. Click on line |Trade in another page should be displayed");
		Reporter.log("9. Select trade in info & click continue button | Device condition page should be displayed");
		Reporter.log("10. Click on continue button | Trade-In value page should be displayed");
		Reporter.log("11. Click on continue button | Product list page should be displayed");
		Reporter.log("12. Click on device | Product details page should be displayed");
		Reporter.log("13. Click on add to cart | Cart page should be displayed");
		Reporter.log("14. Click Continue In Insurance Migration Page |Accessories List Page should be displayed");
		Reporter.log("15. Select 5 Accessory And Click Continue |  Cart page should be displayed");
		Reporter.log(
				"16. Fill payment information & click accept & continue | Order confirmation page should be displayed");
		Reporter.log("17. Verify order success message | Order success message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		TradeInDeviceConditionConfirmationPage tradeInValuePage = navigateToTradeInDeviceConditionConfirmationPageByFeatureDevicesWithTradeInFlow(myTmoData, myTmoData.getDeviceName());
		tradeInValuePage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.checkPageIsReady();
		if (deviceProtectionPage.verifyDeviceProtectionURL() == true) {
			deviceProtectionPage.clickContinueButton();
		}
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupOkButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories(myTmoData.getNumberOfAccessories());
		accessoryPLPPage.clickContinueButton();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		/*
		 * cartPage.verifyAcceptAndContinue();
		 * cartPage.clickAcceptAndPlaceOrder(); OrderConfirmationPage
		 * orderConfirmationPage = new OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}

}
