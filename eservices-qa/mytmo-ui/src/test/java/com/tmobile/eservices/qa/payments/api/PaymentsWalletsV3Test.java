package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.PaymentsWalletsV3;

import io.restassured.response.Response;

public class PaymentsWalletsV3Test extends PaymentsWalletsV3 {

	public Map<String, String> tokenMap;

	/**
	/**
	 * UserStory# US545158: Create New EOS for Wallet;
	 * UserStory# 
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV3Test","testCreatePaymentWallet",Group.PAYMENTS,Group.SPRINT  })
	public void testCreatePaymentWallet(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testCreatePaymentWallet");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with CreatePaymentWallet API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testCreatePaymentWallet";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("CreateandUpdatePaymentWallet.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = WalletCreation(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"status"), 
						"SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode,"reasonDescription"),
						"Created Succesfully" , "Reason Description is Mismatched");
				
							}
		} else {

			failAndLogResponse(response, operationName);
		}
	}


	/**
	/**
	 * UserStory# US545158: Create New EOS for Wallet;
	 * UserStory# 
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV3Test","testUpdatePaymentWallet",Group.PAYMENTS,Group.SPRINT  })
	public void testUpdatePaymentWallet(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testUpdatePaymentWallet");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with UpdatePaymentWallet API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testUpdatePaymentWallet";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("CreateandUpdatePaymentWallet.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = WalletUpdate(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"status"), 
						"SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode,"reasonDescription"),
						"Updated SuccessFully" , "Reason Description is Mismatched");
				
							}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	/**
	 * UserStory# US545158: Create New EOS for Wallet;
	 * UserStory# 
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV3Test","testSearchPaymentWallet",Group.PAYMENTS,Group.SPRINT  })
	public void testSearchPaymentWallet(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testSearchPaymentWallet");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with SearchPaymentWallet API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testSearchPaymentWallet";

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		Response response = WalletSearch(apiTestData,tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"eligibleForCardPayment"),
						"true" , "eligibleForCardPayment misMatched");

				Assert.assertEquals(getPathVal(jsonNode,"eligibleForBankPayment"),
						"true" , "eligibleForBankPayment is misMatched");
				Assert.assertEquals(getPathVal(jsonNode,"isWalletCapacityReached"),
						"false" , "iswalletcapacity is misMatched");
			}
		} else {

			failAndLogResponse(response, operationName);
		}


	}


	/**
	/**
	 * UserStory# US545158: Create New EOS for Wallet;
	 * UserStory# 
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentsWalletsV3Test","testDeletePaymentWallet",Group.PAYMENTS,Group.SPRINT  })
	public void testDeletePaymentWallet(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testDeletePaymentWallet");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with DeletePaymentWallet API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="testDeletePaymentWallet";

		tokenMap = new HashMap<String, String>();

		String requestBody =  new ServiceTest().getRequestFromFile("DeletePaymentWallet.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = WalletDelete(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"status"), 
						"SUCCESS", " Status is Mismatched ");
				Assert.assertEquals(getPathVal(jsonNode,"reasonDescription"),
						"Deleted Succesfully" , "Reason Description is Mismatched");
				
							}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	

}
