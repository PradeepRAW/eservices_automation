package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.EipJodPage;
import com.tmobile.eservices.qa.pages.payments.PoipConfirmationPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

public class PoipPaymentConformationPageTest extends PaymentCommonLib {

	/*
	 * US479056 :Implement POIP Success Page US597262 : Notify EIP Team of
	 * eSignature Completion (Front End)
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPOIPConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US479056 | Setup Installment Plan - Page Load DocuSign Call");
		Reporter.log("Data Condition | EIP-JOD Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Navigate to EIP/JOD page' | EIPJOD page should be displayed");
		Reporter.log("5. Click Take Action on Active EIP' | EIPJOD lease ending page should be displayed");
		Reporter.log("6. Click Setup Installments Button' | POIP payment page should be displayed");
		Reporter.log(
				"7. Add Payment Method and Click on Agree Submit Button' | DocuSign page with EIP lease details  should be displayed");
		Reporter.log("8. Verify EIP lease deatils' |  EIP lease details  are verified");
		Reporter.log(
				"9. Click on Submit Button' |  Poip Confirmation Page or Docusign Thank you Page should be displayed");
		Reporter.log("10. Verify 'Back To Lease Option Link''Order Deatils' ...ect  |  Details should be displayed");
		Reporter.log("11. Click Lease Back Button|  EIPJOD Page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		PoipConfirmationPage popSuccessPage = navigateToPoipPaymentConfirmationPage(myTmoData);
		popSuccessPage.verifyPageUrl();
		popSuccessPage.verifyPageLoaded();
		String poipText = PaymentConstants.POIP_CONFIRMATION_PAYMENT_MESSAGE.replace("[card type]", "")
				.replace("[PAYMENT DATE]", "").replace("[last 4]", "").replace("$[XX.XX]", "");
		popSuccessPage.verifyPopipConfirmationPaymentAlert(poipText);
		popSuccessPage.verifyPopipConfirmationAlert(PaymentConstants.POIP_CONFIRMATION_ALERT_MESSAGE);
		popSuccessPage.verifyPopipConfirmationTwoHoursAlert(PaymentConstants.POIP_CONFIRMATION_TWOHOURS_MESSAGE);
		popSuccessPage.verifyOrderDetails();
		popSuccessPage.verifyAccountHistoryLinkDisplayed();
		popSuccessPage.verifyBackToLeaseButton();
		popSuccessPage.clickBackToLeaseButton();
		EipJodPage eipJodPage = new EipJodPage(getDriver());
		eipJodPage.verifyPageLoaded();

	}

	/*
	 * US479056 :Implement POIP Success Page US597262 : Notify EIP Team of
	 * eSignature Completion (Front End)
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPOIPconfirmationnavigatestoAccountHistoryPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US479056 | Setup Installment Plan - Page Load DocuSign Call");
		Reporter.log("Data Condition | EIP-JOD Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Navigate to EIP/JOD page' | EIPJOD page should be displayed");
		Reporter.log("5. Click Take Action on Active EIP' | EIPJOD lease ending page should be displayed");
		Reporter.log("6. Click Setup Installments Button' | POIP payment page should be displayed");
		Reporter.log(
				"7. Add Payment Method and Click on Agree Submit Button' | DocuSign page with EIP lease details  should be displayed");
		Reporter.log("8. Verify EIP lease deatils' |  EIP lease details  are verified");
		Reporter.log(
				"9. Click on Submit Button' |  Poip Confirmation Page or Docusign Thank you Page should be displayed");
		Reporter.log("10. Verify 'AccountHitosry Link'  |  'Account History' Link should be displayed");
		Reporter.log("11. Click 'Account Histoty' Link|  'Account History' Page displayed");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		PoipConfirmationPage popSuccessPage = navigateToPoipPaymentConfirmationPage(myTmoData);
		popSuccessPage.verifyPageUrl();
		popSuccessPage.verifyPageLoaded();
		popSuccessPage.verifyAccountHistoryLinkDisplayed();
		popSuccessPage.clickAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();

	}

	/*
	 * US479056 :Implement POIP Success Page US597262 : Notify EIP Team of
	 * eSignature Completion (Front End)
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPIImaskinginPoipConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US479056 | Setup Installment Plan - Page Load DocuSign Call");
		Reporter.log("Data Condition | EIP-JOD Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Navigate to EIP/JOD page' | EIPJOD page should be displayed");
		Reporter.log("5. Click Take Action on Active EIP' | EIPJOD lease ending page should be displayed");
		Reporter.log("6. Click Setup Installments Button' | POIP payment page should be displayed");
		Reporter.log(
				"7. Add Payment Method and Click on Agree Submit Button' | DocuSign page with EIP lease details  should be displayed");
		Reporter.log("8. Verify EIP lease deatils' |  EIP lease details  are verified");
		Reporter.log(
				"9. Click on Submit Button' |  Poip Confirmation Page or Docusign Thank you Page should be displayed");
		Reporter.log(
				"10. Verify PII Masking for Account History Link'  | PII for 'Account History' Link should be Masked");
		Reporter.log(
				"11. Verify PII Masking for Payment Confirmation Message|  Payment Confirmation Message should be Masked");

		Reporter.log("================================");
		Reporter.log("Actual Test Steps");

		PoipConfirmationPage popSuccessPage = navigateToPoipPaymentConfirmationPage(myTmoData);
		popSuccessPage.verifyPageUrl();
		popSuccessPage.verifyPageLoaded();
		popSuccessPage.verifyAccountHistoryLinkDisplayed();
		popSuccessPage.verifyPiiMasking(PaymentConstants.PII_CUSTOMER_PHONENUMBER_PID,
				PaymentConstants.PII_CUSTOMER_CUSTOMERNAME_PID);

	}

}