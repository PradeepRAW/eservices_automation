package com.tmobile.eservices.qa.accessibility;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.shop.AccessoriesStandAloneReviewCartPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.DeviceIntentPage;
import com.tmobile.eservices.qa.pages.shop.IdentityFraudRejectPage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewIntroductionPage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewQuestionPage;
import com.tmobile.eservices.qa.pages.shop.PLPPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.shop.UNOWatchPDPPage;
import com.tmobile.eservices.qa.pages.shop.UNOWatchPLPPage;
import com.tmobile.eservices.qa.pages.shop.WatchPlanPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class ShopPages extends ShopCommonLib {
	AccessibilityCommonLib accessibilityCommonLib = new AccessibilityCommonLib();

	/***
	 * Test Accessibility for AccessoriesPDP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAccessoriesPDP(ControlTestData data, MyTmoData myTmoData) {
		navigateToAccessoryPDPPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "AccessoriesPDP");
	}

	/***
	 * Test Accessibility for AccessoriesPLP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAccessoriesPLP(ControlTestData data, MyTmoData myTmoData) {
		navigateToAccessoryPLPPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "AccessoriesPLP");
	}

	/***
	 * Test Accessibility for AccessoriesStandAlonePDP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAccessoriesStandAlonePDP(ControlTestData data, MyTmoData myTmoData) {
		navigateToStandAloneAccessoriesPDP(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "AccessoriesStandAlonePDP");
	}

	/***
	 * Test Accessibility for AccessoriesStandAlonePLP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAccessoriesStandAlonePLP(ControlTestData data, MyTmoData myTmoData) {
		navigateToStandAloneAccessoriesPLP(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "AccessoriesStandAlonePLP");
	}

	/***
	 * Test Accessibility for AccessoriesStandAloneReviewCartPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAccessoriesStandAloneReviewCartPage(ControlTestData data, MyTmoData myTmoData) {
		AccessoriesStandAloneReviewCartPage accessoriesStandAloneReviewCartPage = navigateToStandAloneReviewCartPage(
				myTmoData);
		accessoriesStandAloneReviewCartPage.verifyAccessoryIsPresentOnCartPage(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "AccessoriesStandAloneReviewCartPage");
	}

	/***
	 * Test Accessibility for CartPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testCartPageOrderDetail(ControlTestData data, MyTmoData myTmoData) {
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "CartPage Order Detail");
	}

	/***
	 * Test Accessibility for CartPage Shipping tab
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testCartPageShipping(ControlTestData data, MyTmoData myTmoData) {
		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingDetails();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "CartPage Shipping Default");
	}

	/***
	 * Test Accessibility for CartPage Shipping tab
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testCartPageShippingChangeShippingAddress(ControlTestData data, MyTmoData myTmoData) {
		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingDetails();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickShipToDifferentAddress();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "CartPage Shipping Changed Address");
	}

	/***
	 * Test Accessibility for CartPage Payment tab
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testCartPagePayment(ControlTestData data, MyTmoData myTmoData) {
		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingDetails();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "CartPage Payment Default");
	}

	/***
	 * Test Accessibility for Esignature
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testEsignature(ControlTestData data, MyTmoData myTmoData) {
		// Esignature
		accessibilityCommonLib.testAccessibility(getDriver(), "Esignature");
	}

	/***
	 * Test Accessibility for InsuranceMigrationPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testDeviceProtectionPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToDeviceProtectionPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "DeviceProtectionPage");
	}

	/***
	 * Test Accessibility for JumpCartPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testJumpCartPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToJumpCartPage(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "JumpCartPage");
	}

	/***
	 * Test Accessibility for Jump Trade In Device Condition
	 * 
	 * @param data
	 * @param myTmoData
	 */
	/*
	 * @Test(dataProvider = "byColumnName", enabled = true, groups = {
	 * Group.REGRESSION, Group.PENDING }) public void
	 * testJumpTradeInDeviceConditionPage(ControlTestData data, MyTmoData myTmoData)
	 * { navigateToDeviceConditionPageJumpFlowThroughLineSelectorPage(myTmoData);
	 * Reporter.log("==============================");
	 * Reporter.log("Navigated to desired page");
	 * accessibilityCommonLib.testAccessibility(getDriver(),
	 * "Jump Trade In Device Condition"); }
	 */

	/***
	 * Test Accessibility for Jump Trade In Confirmation
	 * 
	 * @param data
	 * @param myTmoData
	 */
	/*
	 * @Test(dataProvider = "byColumnName", enabled = true, groups = {
	 * Group.REGRESSION, Group.PENDING }) public void
	 * testJumpTradeInConfirmationGoodPage(ControlTestData data, MyTmoData
	 * myTmoData) { JumpTradeInPage jumpTradeInPage =
	 * navigateToDeviceConditionPageJumpFlowThroughLineSelectorPage( myTmoData);
	 * jumpTradeInPage.clickDisableFindMyPhoneCheckBox();
	 * jumpTradeInPage.clickLCDDisplayCheckBox();
	 * jumpTradeInPage.clickPowerOnCheckBox();
	 * jumpTradeInPage.clickContinueButton(); JumpTradeInConfirmationPage
	 * jumpTradeInConfirmationPage = new JumpTradeInConfirmationPage(getDriver());
	 * jumpTradeInConfirmationPage.verifyPageUrl();
	 * Reporter.log("==============================");
	 * Reporter.log("Navigated to desired page");
	 * accessibilityCommonLib.testAccessibility(getDriver(),
	 * "Jump Trade In Confirmation Good"); }
	 */

	/***
	 * Test Accessibility for Jump Trade In Confirmation
	 * 
	 * @param data
	 * @param myTmoData
	 */
	/*
	 * @Test(dataProvider = "byColumnName", enabled = true, groups = {
	 * Group.REGRESSION, Group.PENDING }) public void
	 * testJumpTradeInConfirmationBadPage(ControlTestData data, MyTmoData myTmoData)
	 * { JumpTradeInPage jumpTradeInPage =
	 * navigateToDeviceConditionPageJumpFlowThroughLineSelectorPage( myTmoData);
	 * jumpTradeInPage.clickDisableFindMyPhoneCheckBox();
	 * jumpTradeInPage.clickLCDDisplayCheckBox();
	 * jumpTradeInPage.clickContinueButton(); JumpTradeInConfirmationPage
	 * jumpTradeInConfirmationPage = new JumpTradeInConfirmationPage(getDriver());
	 * jumpTradeInConfirmationPage.verifyPageUrl();
	 * Reporter.log("==============================");
	 * Reporter.log("Navigated to desired page");
	 * accessibilityCommonLib.testAccessibility(getDriver(),
	 * "Jump Trade In Confirmation Bed"); }
	 */

	/***
	 * Test Accessibility for LineSelectorPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testLineSelectorPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "LineSelectorPage");
	}

	/***
	 * Test Accessibility for NewLineSelectorDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testNewLineSelectorDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "NewLineSelectorDetailsPage");
	}

	/***
	 * Test Accessibility for OfferDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testOfferDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToOfferDetailsPage(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "OfferDetailsPage");
	}

	/***
	 * Test Accessibility for OrderConfirmation
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testOrderConfirmation(ControlTestData data, MyTmoData myTmoData) {
		navigateToOrderConformationPageBySeeAllPhonesWithFRPPaymentOption(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(), "OrderConfirmation");
		accessibilityCommonLib.testAccessibility(getDriver(), "OrderConfirmation");
	}

	/***
	 * Test Accessibility for OrderStatusPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testOrderStatusPage(ControlTestData data, MyTmoData myTmoData) {
		launchAndNavigateToOrderStatusPage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(), "OrderStatusPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "OrderStatusPage");
	}

	/***
	 * Test Accessibility for PDPPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testPDPPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPDPPageBySeeAllPhones(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "PDPPage");
	}

	/***
	 * Test Accessibility for PhonePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testPhonePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPhonePage(myTmoData);
		accessibilityCommonLib.collectPageServiceCalls(getDriver(), "PhonePage");
		accessibilityCommonLib.testAccessibility(getDriver(), "PhonePage");
	}

	/***
	 * Test Accessibility for PLPPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testPLPPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPLPPageBySeeAllPhones(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "PLPPage");
	}

	/***
	 * Test Accessibility for PLPPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testPLPPageDownPaymentModal(ControlTestData data, MyTmoData myTmoData) {
		PLPPage plpPage = navigateToPLPPageBySeeAllPhones(myTmoData);
		plpPage.clickTodayButton();
		plpPage.verifyDownPaymentModelHeader();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "PLPPage Price Modal");
	}

	/***
	 * Test Accessibility for PLPPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testPLPPagePromoModal(ControlTestData data, MyTmoData myTmoData) {
		PLPPage plpPage = navigateToPLPPageBySeeAllPhones(myTmoData);
		plpPage.clickPromoOfferText();
		plpPage.verifyPromoDescriptionInModal();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "PLPPage Promo Modal");
	}

	/***
	 * Test Accessibility for Returns
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testReturns(ControlTestData data, MyTmoData myTmoData) {
		// Returns
		accessibilityCommonLib.collectPageServiceCalls(getDriver(), "Returns");
		accessibilityCommonLib.testAccessibility(getDriver(), "Returns");
	}

	/***
	 * Test Accessibility for SecureCheckOutPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testSecureCheckOutPage(ControlTestData data, MyTmoData myTmoData) {
		// SecureCheckOutPage
		accessibilityCommonLib.collectPageServiceCalls(getDriver(), "SecureCheckOutPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "SecureCheckOutPage");
	}

	/***
	 * Test Accessibility for ShopPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testShopPageShopFlow(ControlTestData data, MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "ShopPage");
	}

	/***
	 * Test Accessibility for TradeInAnotherDevicePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testTradeInAnotherDevicePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTradeInAnotherDevicePageBySeeAllPhones(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TradeInAnotherDevicePage");
	}

	/***
	 * Test Accessibility for TradeInConfirmationPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testTradeInConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTradeInDeviceConditionConfirmationPageBySeeAllPhones(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TradeInConfirmationPage");
	}

	/***
	 * Test Accessibility for TradeInDeviceConditionPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testTradeInDeviceConditionPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTradeInDeviceConditionValuePageBySeeAllPhones(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TradeInDeviceConditionPage");
	}

	/***
	 * Test Accessibility for TradeInPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "pending" })
	public void testTradeInPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTradeInAnotherDevicePageBySeeAllPhones(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.collectPageServiceCalls(getDriver(), "TradeInPage");
		accessibilityCommonLib.testAccessibility(getDriver(), "TradeInPage");
	}

	/***
	 * Test Accessibility for TradeInValuePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testTradeInValuePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToTradeInDeviceConditionConfirmationPageByFeatureDevicesWithTradeInFlow(myTmoData,
				"Apple iPhone XS Max");
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "TradeInValuePage");
	}

	/**
	 * Test Accessibility for DeviceIntentPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testDeviceIntentPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToDeviceIntentPageFromAALQuickLink(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "DeviceIntentPage");
	}

	/**
	 * Test Accessibility for Rate plan page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToRatePlanPageFromAALBYODFlow(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "ConsolidatedRatePlanPage");
	}

	/***
	 * Test Accessibility for AAL Non-BYOD Fraud Review Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testFraudReviewPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToCartPageFromAALNonBYODFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Fraud Review Page");
	}

	/***
	 * Test Accessibility for AAL BuyNewPhone Fraud Review Questions Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.PENDING })
	public void testFraudReviewQuestionsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());

		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();

		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionContinueCTA();

		IdentityReviewQuestionPage identityReviewQuestionPage = new IdentityReviewQuestionPage(getDriver());
		identityReviewQuestionPage.verifyReviewQuestionPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Fraud Review Questions Page");
	}

	/***
	 * Test Accessibility for AAL Non-BYOD Fraud Reject Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.PENDING })
	public void testFraudRejectPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToCartPageFromAALNonBYODFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionCancelCTA();
		identityReviewIntroductionPage.clickSecondaryCancelCTAIconOnIdentityReviewIntroductionWarningModal();
		IdentityFraudRejectPage identityFraudRejectPage = new IdentityFraudRejectPage(getDriver());
		identityFraudRejectPage.verifyFraudRejectPage();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Fraud Reject Page");
	}

	/***
	 * Test Accessibility for AAL Non-BYOD Fraud Review Page Modal
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.PENDING })
	public void testFraudReviewModalPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToCartPageFromAALNonBYODFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.clickAcceptAndPlaceOrder();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPage();
		identityReviewIntroductionPage.clickIdentityReviewIntroductionCancelCTA();
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionWarningModal();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Fraud Reject Page");
	}

	/**
	 * Test Accessibility to Cart page using BYOD AAL flow
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAALBYODCartPage(MyTmoData myTmoData) {
		navigateToCartPageFromAALBYODFlow(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Cart Page through AAL BYOD flow");
	}

	/**
	 * Test Accessibility to shipping page using BYOD AAL flow
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAALBYODShippingPage(MyTmoData myTmoData) {
		navigateToCartPageFromAALBYODFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.verifyAALShippingDetails();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Shipping Page through AAL BYOD flow");
	}

	/**
	 * Test Accessibility to shipping page using BYOD AAL flow
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAALNonBYODShippingPage(MyTmoData myTmoData) {
		navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Shipping Page through AAL Non-BYOD flow");
	}

	/**
	 * Test Accessibility to payment page using BYOD AAL flow
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testAALBYODPaymentPage(MyTmoData myTmoData) {
		navigateToCartPageFromAALBYODFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.verifyAALShippingDetails();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Payment Page through AAL BYOD flow");
	}

	/**
	 * Test Accessibility to payment page using Non-YOD AAL flow
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAALNonBYODPaymentPage(MyTmoData myTmoData) {
		navigateToCartPageFromAALNonBYODFlow(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Payment Page through AAL Non-BYOD flow");
	}

	/**
	 * US485299 - Accessibility - Interstitial Page Test Accessibility to
	 * Interstitial page using AAL(Quicklinks) flow
	 *
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.REGRESSION })
	public void testInterstitialPageAALBuyNewPhoneFromQuicklinks(MyTmoData myTmoData) {
		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(),
				"Interstitial Page through Quicklinks - AAL Buy New Phone flow");
	}

	/**
	 * US508722 - Mytmo Accessibility : Error states - Invalid entry call out
	 *
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testErrorStatesOnPaymentPage(MyTmoData myTmoData) {
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "Payment Page- Error states");
	}

	/**
	 * US597805: Accessibility - Modals
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testHardStopModalOnDeviceIntentPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case#US597805 - Accessibility - Modals");
		Reporter.log("Data Condition | puerto rico customers, etc... and Not eligible to AAL");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application|Home Page should be displayed");
		Reporter.log("2. Click On Shop|Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button | Hard Stop model should be displayed");
		Reporter.log("4. Verify Model title | Hard Stop model title(puerto rico Account) should be displayed");
		Reporter.log("3. Verify 'Return to shopping' button | 'Return to shopping' CTA should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		shopPage.verifyAALIneligibleModalWindow();
		shopPage.verifyAALIneligibleMessageForPuertoRicoCustomer();
		shopPage.verifyReturnToShoppingCTAForPuertoRicoCustomer();
		Reporter.log("==============================");
		Reporter.log("Navigated to desired Pop UP");
		accessibilityCommonLib.testAccessibility(getDriver(), "AAL Popup for Puertorico Customer");

	}

	/**
	 * US597805: Accessibility - Modals
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testNonEligibleModalOnDeviceIntentPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US597805 - Accessibility - Modals");
		Reporter.log("Data Condition | STD PAH but not eligible to AAL and max soc, bad rate plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application|Home Page should be displayed");
		Reporter.log("2. Click On Shop|Shop page should be displayed");
		Reporter.log("3. Click On Add a Line button| Non eligible model should be displayed");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		shopPage.verifyAALIneligibleModalWindow();
		accessibilityCommonLib.testAccessibility(getDriver(), "AAL Popup for In eligible Customer");

		Reporter.log("==============================");
		Reporter.log("Navigated to desired Pop UP");
	}

	/**
	 * US631525: Accessibility - Modals (Test only)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testNonEligibleModalDisplayedOnDeviceIntentPageForPRCustomers(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case: US631525 - Accessibility - Modals (Test only)");
		Reporter.log(
				"Data Condition | STD PAH but not eligible to AAL and max soc, bad rate plan and Puertorico customer etc...");
		Reporter.log("================================");
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();
		DeviceIntentPage deviceIntentPage = navigateToDeviceIntentUsingDeeplink();
		deviceIntentPage.verifyAALIneligibleModalWindow();
		accessibilityCommonLib.testAccessibility(getDriver(), "AAL Popup for In eligible Customer");
		Reporter.log("==============================");
		Reporter.log("Navigated to desired Pop UP");
	}

	/**
	 * US544744: Display Line Selector title and lines below cost details CDCSM-2 :
	 * Display Line Selector title and lines below cost details TC-12: CDCSM-2:
	 * Accessibility testing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testLineSelectionSectionForWearablesInRatePlanPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US544744: Display Line Selector title and lines below cost details");
		Reporter.log("Data Condition | User should have AAL eligible");
		Reporter.log("================================");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyShoppage();
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickBuyAWatch();
		UNOWatchPLPPage unoPLPPage = new UNOWatchPLPPage(getDriver());
		unoPLPPage.verifyWatchesPageLoaded();
		unoPLPPage.clickDeviceByName(myTmoData.getDeviceName());
		UNOWatchPDPPage unoPDPPage = new UNOWatchPDPPage(getDriver());
		unoPDPPage.verifyWatchesPDPPageLoaded();
		unoPDPPage.clickOnAddToCartCTA();
		WatchPlanPage watchPlanPage = new WatchPlanPage(getDriver());
		watchPlanPage.verifyWatchPlanPage();
		accessibilityCommonLib.testAccessibility(getDriver(), "Watch Plan Page");
	}

	/***
	 * Test Accessibility for UNO PLP Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testMyTmoUNOPLP(ControlTestData data, MyTmoData myTmoData) {
		navigateToUNOPLPPageFromAALBuyNewPhoneFlow(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "My TMO UNO PLP");
	}

	/***
	 * Test Accessibility for UNO PDP Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testMyTmoUNOPdP(ControlTestData data, MyTmoData myTmoData) {
		navigateToUNOPDPPageBySeeAllPhones(myTmoData);
		Reporter.log("==============================");
		Reporter.log("Navigated to desired page");
		accessibilityCommonLib.testAccessibility(getDriver(), "My TMO UNO PDP");
	}

}
