package com.tmobile.eservices.qa.tmng.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.BringYourOwnPhonePage;
import com.tmobile.eservices.qa.pages.tmng.functional.CartPage;
import com.tmobile.eservices.qa.pages.tmng.functional.CheckOutPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.SLPPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPManualorFraudReviewPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPNoOfferPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPOfferPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPPreScreenForm;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPPreScreenIntro;
import com.tmobile.eservices.qa.pages.tmng.functional.TradeInValuePopUpModal;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class CartPageTest extends TmngCommonLib {

	/*-----------MY CART --------------*/
	/**
	 * US353086: TMO -IV - (Web) Save Cart Confirmation US338178: TMO - IV - (Web)
	 * Save Cart US446656 and US479052
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testSaveCartPageWeb(TMNGData tMNGData) {
		Reporter.log("Test Case : US338178: TMO - IV - (Web) Save Cart");
		Reporter.log("Test Case : US353086: TMO -IV - (Web) Save Cart Confirmation");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones  Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device |PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5.Click on Save Cart  | Save Cart pop up should display");
		Reporter.log("6. Verify Cart icon on top of the page| Cart icon should be displayed");
		Reporter.log("7. Verify Page title under icon | Page title shold be displayed");
		Reporter.log("8. Verify message under the title | message under the title should be displayed");
		Reporter.log(
				"9. Verify input email in a field under the message | input email in a field under the message should be displayed");
		Reporter.log("10. Click on save | Conformation Page should be displayed");
		Reporter.log("11. Click on close icon | Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickOnSaveCart();
		cartPage.verifyCartIconOnSaveCartModel();
		cartPage.verifySaveCartHeader();
		cartPage.verifySaveCartSubHeader();
		String email = cartPage.getEmailWithDate(tMNGData);
		cartPage.enterEmailWithDateInSaveCart(email);
		cartPage.clickSaveButtonOnSaveCartModel();
		cartPage.verifyConfirmationMessageonSaveCartModel();
		cartPage.verifyStoresInSaveCartModalPopup();
		cartPage.verifyChangeStoreLocatorCTAOnCartPage();
		cartPage.clickCloseOnSaveCartModel();
		cartPage.verifyCartPageLoaded();

	}

	/**
	 * TA1655247: TMNG:Staging: Cart - Unable to get into cart while checking
	 * retrieve cart DE167387 : TMNG:Staging: Cart - Unable to get into cart while
	 * checking retrieve cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testAddDeviceFromRetrieveCartThroughEmptyCart(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case : TA1655247: TMNG:Staging: Cart -  Unable to get into cart while checking retrieve cart");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Cart Icon | Empty Cart page should be displayed");
		Reporter.log("3. Enter email id | Email should be entered");
		Reporter.log("4. Click Retrieve cart button | Order details model should be displayed");
		Reporter.log("5. Click on Order details Close icon | Order details model should be closed");
		Reporter.log("6. Click on Add a phone button | Device + image should be displayed");
		Reporter.log("7. Click on Device + image and Click Add a new phone button | Mini PLP page should be displayed");
		Reporter.log("8. Select device | Mini PDP page should be displayed");
		Reporter.log("9. Get device name and saved in V1 | Device name should be saved in V1");
		Reporter.log("10. Click Add to cart button | Cart Page should be displayed");
		Reporter.log("11. Click on View order details link | Order details model should be displayed");
		Reporter.log("12. Verify Added device in Order details  | Added device should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.enterEmail(tMNGData);
		cartPage.clickRetrieveCartButton();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyRetrieveCartItemsOnOrderDetails();
		cartPage.clickOnCloseIconAtOrderDetailPage();
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		String deviceName = tMNGData.getSecondDeviceName();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyAddedDeviceInOrderDetails(deviceName);
	}

	/**
	 * C347320: TMO: Save Cart functionality (pop up): Valid Email, Invalid Email
	 * error message and Save. modal
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyElementsOnSaveCartModelPopup(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case : TMO: Save Cart functionality (pop up): Valid Email, Invalid Email error message and Save.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phone Cart Icon |Empty Cart page should be displayed");
		Reporter.log("3. Click on 'Add a Phone' button | Device image should be displayed");
		Reporter.log(
				"4. click on '+' button in device image and Add a new Phone button | Mini PLP page should be displayed");
		Reporter.log("5. select any device | Mini PDP page should be displayed");
		Reporter.log("6. click on Add to Cart | Cart page  should be displayed");
		Reporter.log("7. Verify 'Save cart' link | 'Save cart' link should be displayed");
		Reporter.log("8. Click 'Save Cart' link | 'Save cart' model window should be displayed ");
		Reporter.log(
				"9. Enter Invalid email and change the focus of cursor | Error message should be displayed for invalid mail");
		Reporter.log(
				"10. Enter valid email and change the focus of cursor | Entered email should be accepted and Save CTA should be enabled");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifySaveCartCTAOnCart();
		cartPage.clickOnSaveCart();
		cartPage.verifySaveCartModelWindow();
		cartPage.enterInvalidEmailinSaveCart(tMNGData);
		cartPage.enterEmail(tMNGData);
		cartPage.verifySaveCartCtaInModalWindow();

	}

	/**
	 * US364850: Save Cart/Save cart to retail > Retrieve cart > Cart > Default to
	 * order details view
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testRetrieveCartOpensOrderDetails(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US364850 :Save Cart/Save cart to retail > Retrieve cart > Cart > Default to order details view");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones  Link | Cell Phones PLP should be displayed");
		Reporter.log("3. Click device | PDP should be displayed");
		Reporter.log("4. Add device to cart| Cart page should be displayed");
		Reporter.log("5. Click on Save Cart  | Save Cart pop up should display");
		Reporter.log("6. Verify Cart icon on top of the page| Cart icon should be displayed");
		Reporter.log(
				"7. Verify input email in a field under the message | input email in a field under the message should be displayed");
		Reporter.log("8. Click on save | Conformation Page should be displayed");
		Reporter.log("9. Click on close icon | Cart page should be displayed");
		Reporter.log("10. click Retrieve cart | RetriveCart pop up should be displayed");
		Reporter.log("11. Enter email id and click Retrieve cart | Order details popup should be displayed");
		Reporter.log(
				"12. Verify Cart Summary for retrieved cart | Cart Summary for retrieved cart should be displayed");
		Reporter.log("13. Click close order details | cart Page shold be displayed");
		Reporter.log(
				"14. Verify Cart Summary from Order Deatails is matching Summary from Cart page | Cart Summary from Order Deatails should matching Summary from Cart page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickDuplicateCTA();
		cartPage.clickOnSaveCart();
		cartPage.verifyCartIconOnSaveCartModel();
		String email = cartPage.getEmailWithDate(tMNGData);
		cartPage.enterEmailWithDateInSaveCart(email);
		cartPage.clickSaveButtonOnSaveCartModel();
		cartPage.verifySaveCartReplaceModalAndClickSaveForWebSaveCart();
		cartPage.verifyConfirmationMessageonSaveCartModel();
		cartPage.clickCloseOnSaveCartModel();
		cartPage.verifyCartPageLoaded();
		cartPage.clickRemoveCTA();
		cartPage.verifyRetrieveCartOnCartPage();
		cartPage.clickRetrieveCartOnCartPage();
		cartPage.verifyRetrieveModalForCartPage();
		cartPage.enterEmailWithDateInSaveCart(email);
		cartPage.clickRetrieveCartButton();
		cartPage.verifyViewOrderDetailsModel();
		String Summary = cartPage.cartSummaryTextOnOrderDetailPage();
		cartPage.clickOnCloseIconAtOrderDetailPage();
		String myCartSummary = cartPage.cartSummaryTextOnCartPage();
		cartPage.compareSummaryForAddedcart(Summary, myCartSummary);

	}

	/**
	 * US373579 :Save Cart/Save cart to retail > Retrieve cart > Save Cart Deeplink
	 * > Default to order details view US480208:TMNG > TMO Essentials > Deeplink >
	 * V1 to V2 Migration > Retrieve cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testRetriveCartWithDeeplink(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US373579 :Save Cart/Save cart to retail > Retrieve cart > Save Cart Deeplink > Default to order details view");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Launch Cart page by providing deeplink Url | Cart page should be displayed");
		Reporter.log("3. Click Yes Retrieve cart | Order details popup should be displayed");
		Reporter.log("4. Verify Cart Summary for retrieved cart | Cart Summary for retrieved cart should be displayed");
		Reporter.log("5. Click close order details | cart Page shold be displayed");
		Reporter.log(
				"6. Verify Cart Summary from Order Deatails is matching Summary from Cart page | Cart Summary from Order Deatails should matching Summary from Cart page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		String cartId = cartPage.getCartIdFromCookiesInCartPage();
		cartPage.clickDuplicateCTA();
		cartPage.clickOnSaveCart();
		cartPage.verifyCartIconOnSaveCartModel();
		String email = cartPage.getEmailWithDate(tMNGData);
		cartPage.enterEmailWithDateInSaveCart(email);
		cartPage.clickSaveButtonOnSaveCartModel();
		cartPage.verifySaveCartReplaceModalAndClickSaveForWebSaveCart();
		cartPage.verifyConfirmationMessageonSaveCartModel();
		cartPage.clickCloseOnSaveCartModel();
		cartPage.verifyCartPageLoaded();

		getDriver().manage().deleteAllCookies();

		navigateToCartPageUsingDeeplink(cartId);
		CartPage emptyCartPage = new CartPage(getDriver());
		emptyCartPage.verifyViewOrderDetailsModel();
		String Summary = emptyCartPage.cartSummaryTextOnOrderDetailPage();
		emptyCartPage.clickOnCloseIconAtOrderDetailPage();
		String myCartSummary = emptyCartPage.cartSummaryTextOnCartPage();
		emptyCartPage.compareSummaryForAddedcart(Summary, myCartSummary);

	}

	/**
	 * TA1682648:PROD and WWW2: Retrieve Cart - Total price in the sticky banner
	 * displays Awesome price for Average credit DE216555 :PROD and WWW2: Retrieve
	 * Cart - Total price in the sticky banner displays Awesome price for Average
	 * credit
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testRetrieveCartTotalPriceInStickyBanner(ControlTestData data, TMNGData tMNGData) {

		Reporter.log(
				"Test Case :TA1682648:PROD and WWW2: Retrieve Cart - Total price in the sticky banner displays Awesome price for Average credit");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. Select any device | PDP page should be displayed");
		Reporter.log("4. Select GoodCredit option and click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Click Add accessories link on Cart | Accessories Mini PLP page should be displayed");
		Reporter.log("6. Select Accessory device | Accessories Mini PDP page should be displayed");
		Reporter.log("7. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("8. Save Today price value in V1 | Today price value should be saved in V1");
		Reporter.log("9. Save Monthly price value in V2 | Monthly price value should be saved in V2");
		Reporter.log("10. Click on Save Your Cart Icon | Save your cart model should be displayed");
		Reporter.log(
				"11. Enter valid email-id and click on Save button | Replace your saved cart? model should be displayed");
		Reporter.log("12. Click on Yes,save new cart button | Saved cart message should be displayed");
		Reporter.log("13. Click on close icon for save cart model | Cart page should be displayed");
		Reporter.log("14. Clear browser cache | CartPage should be displayed");
		Reporter.log("15. Enter Tmo URL | Cell Phones page should be displayed");
		Reporter.log("16. Select any device | PDP page should be displayed");
		Reporter.log("17. Select AwesomeCredit option and click on Add to Cart| Cart page should be displayed");
		Reporter.log("18. Click on Retrieve cart | RetrrieveCartModel should be displayed");
		Reporter.log("19. Enter email id and enter Retrieve cart button | Order details model should be displayed");
		Reporter.log("20. Close order details model  | CartPage should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);

		cartPage.clickOnSaveCart();
		cartPage.verifySaveCartCtaInModalWindow();
		String email = cartPage.getEmailWithDate(tMNGData);
		cartPage.enterEmailWithDateInSaveCart(email);
		cartPage.clickSaveButtonOnSaveCartModel();
		cartPage.verifySaveCartReplaceModalAndClickSaveForWebSaveCart();
		cartPage.verifyConfirmationMessageonSaveCartModel();
		cartPage.clickCloseOnSaveCartModel();
		cartPage.verifyCartPageLoaded();
		getDriver().manage().deleteAllCookies();
		getDriver().get("https://www.t-mobile.com/cell-phones");

		PlpPage phonesPlpPage = new PlpPage(getDriver());
		phonesPlpPage.verifyPhonesPlpPageLoaded();
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		cartPage.verifyCartPageLoaded();

		cartPage.clickRetrieveCartOnCartPage();
		cartPage.verifyRetrieveModalForCartPage();
		cartPage.enterEmail(tMNGData);
		cartPage.clickRetrieveCartButton();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.clickOnCloseIconAtOrderDetailPage();
		cartPage.verifyCartPageLoaded();

	}

	/*----------- Plan Section --------------*/

	/**
	 * US485370 TMNG > TMO Essentials > Cart > Choose a Plan > Change plan
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyChangePlanFromMainPLP(TMNGData tMNGData) {
		Reporter.log("Test Case : US485370	TMNG > TMO Essentials > Cart > Choose a Plan > Change plan");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click  Add a Phone link on Cart | Device Plus img should  be displayed");
		Reporter.log("6. Click on Plus button and click add a new phone | Mini PLP should  be displayed");
		Reporter.log("7. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("8. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("9. Verify Your Plan section on Cart | Your Plan Header should be displayed");
		Reporter.log(
				"10. Verify T-Mobile ONE Plan section on Cart | T-Mobile ONE Plan should be displayed selected as defaule");
		Reporter.log(
				"11. Change plan by selecting other than T-Mobile ONE plan on Cart | Plan should be changed in selected lines");
		Reporter.log(
				"12. Verify updated prices in selected Lines on Cart | Prices should be changed for selected lines");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySelectingPhone(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.verifyYourPlanHeaderIsDisplayed();
		cartPage.verifyMagentaPlanSection();
		String oldPrice = cartPage.getTotalMonthlyPriceStickyBanner();
		cartPage.clickOnTMobileEssentialsPlan();
		cartPage.verifyTMobileEssentialsPlanSelected();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyAddAWearablebtnDisabled();
		cartPage.verifyAuthorableMsgOnAddWearableImg();
		cartPage.compareStickyBannerMonthlyPriceNotEqual(oldPrice);

	}

	/**
	 * US485371: TMNG > TMO Essentials > Cart > Choose a Plan > Ineligible plan >
	 * Multiple lines with GSM or tablet
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyIneligibleplanFromEmptyCartByAddingWearableFirst(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US485371: TMNG > TMO Essentials > Cart > Choose a Plan > Ineligible plan > Multiple lines with GSM or tablet");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Wearable link on Cart |Credit options modal should  be displayed");
		Reporter.log("3. Select Awesome credit options and click Done | Wearable Mini PLP should  be displayed");
		Reporter.log("4. Select any Wearable in mini PLP |Wearable Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in Wearable mini PDP | Cart Page should  be displayed");
		Reporter.log("6. Click  Add a Phone link on Cart | Phones Mini PLP should  be displayed");
		Reporter.log("7. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("8. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("9. Verify Your Plan section on Cart | Your Plan Header should be displayed");
		Reporter.log(
				"10. Verify Magenta Plan section on Cart | T-Mobile ONE Plan should be displayed selected as default");
		Reporter.log(
				"11. Verify T-Mobile Essentials Plan section on Cart | T-Mobile Essentials Plan should be disabled");
		Reporter.log("12. Verify T-Mobile Essentials Plan on Cart | T-Mobile Essentials Plan should be disabled");
		Reporter.log(
				"13. Verify In eligible plan message | 'Products that you have in cart may not be eligible for Essentials. Please change your rate plan' message should be disabled");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.addAWearabletoCartFromCartPage(tMNGData);
		cartPage.addAPhonetoCartFromCartPage(tMNGData);

		cartPage.verifyYourPlanHeaderIsDisplayed();
		cartPage.verifyMagentaPlanHeader();
		cartPage.verifyTMobileEssentialPlanSectionDisabled();
		cartPage.verifyTMobileEssentialPlanIneligiblemsgDisabled();

	}

	/**
	 * US485371: TMNG > TMO Essentials > Cart > Choose a Plan > Ineligible plan >
	 * Multiple lines with GSM or tablet
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyIneligibleplanFromMainPLP(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US485371: TMNG > TMO Essentials > Cart > Choose a Plan > Ineligible plan > Multiple lines with GSM or tablet");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click  Add a Wearable link on Cart | Wearable Mini PLP should  be displayed");
		Reporter.log("6. Select any Wearable in mini PLP |Wearable Mini PDP Page should  be displayed");
		Reporter.log("7. Click Add to Cart in Wearable mini PDP | Cart Page should  be displayed");
		Reporter.log("8. Verify Your Plan section on Cart | Your Plan Header should be displayed");
		Reporter.log(
				"9. Verify T-Mobile ONE Plan section on Cart | T-Mobile ONE Plan should be displayed selected as defaule");
		Reporter.log(
				"10. Verify T-Mobile Essentials Plan section on Cart | T-Mobile Essentials Plan should be disabled");
		Reporter.log("11. Verify T-Mobile Essentials Plan on Cart | T-Mobile Essentials Plan should be disabled");
		Reporter.log(
				"12. Verify In eligible plan message | 'Products that you have in cart may not be eligible for Essentials. Please change your rate plan' message should be disabled");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToCartPageBySelectingPhone(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.addAWearabletoCartFromCartPage(tMNGData);
		cartPage.verifyYourPlanHeaderIsDisplayed();
		cartPage.verifyMagentaPlanHeader();
		cartPage.verifyTMobileEssentialPlanSectionDisabled();
		cartPage.verifyTMobileEssentialPlanIneligiblemsgDisabled();
	}

	/**
	 * US486769 TMNG > TMO Essentials > Cart > Choose a plan > Ineligible Plan >
	 * Wearable only
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyIneligibleplanForWearableOnlyFromMainPLP(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US486769	TMNG > TMO Essentials > Cart > Choose a plan > Ineligible Plan >  Wearable only");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Tablets and Wearables Link | Tablets and Wearables list page should be displayed");
		Reporter.log("4. Select any wearable | PDP page should be displayed");
		Reporter.log("5. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("6. Verify Your Plan section on Cart | Your Plan Header should be displayed");
		Reporter.log("7. Verify T-Mobile Essentials Plan on Cart | T-Mobile Essentials Plan should be disabled");
		Reporter.log(
				"8. Verify In eligible plan message | 'Products that you have in cart may not be eligible for Essentials. Please change your rate plan' message should be disabled");
		Reporter.log(
				"9. Verify See details and compare Plans CTA | See details and compare Plans CTA should be disabled");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage tabletsAndDevicesPdpPage = navigateToTabletsAndDevicesPDP(tMNGData);
		tabletsAndDevicesPdpPage.clickOnAddToCartBtn();
		tabletsAndDevicesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifyYourPlanHeaderIsDisplayed();
		cartPage.verifyMagentaPlanSection();
		cartPage.verifyTMobileEssentialPlanSectionDisabled();
		// cartPage.verifyTMobileEssentialPlanIneligiblemsgDisabled();
		if (getTogglevalue("isComparePlansCTAEnabled").equals("false")) {
			cartPage.verifySeeDetailsAndComparePlansCTADisabled();
		} else {
			Reporter.log(
					"isComparePlansCTAEnabled is true and 'See details and compare Plans' CTA is present on Cart page.");
		}
	}

	/**
	 * US407815 : TMO - IV - Cart line level detail for Accessories
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testInventoryVisibilityContainerForAccessoryInCartPage(TMNGData tMNGData) {
		Reporter.log("Test Case : TMO - IV - Cart line level detail for Accessories");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessory list page should be displayed");
		Reporter.log("4. Click on ac cessory | Accessory details page should be displayed");
		Reporter.log("5. Click on add to cart | Cart page should be displayed");
		Reporter.log("6. Verify inventory container| Inventory container should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage accessoryDetailsPage = navigateToAccessoryMainPDP(tMNGData);
		String priceOnPDP = accessoryDetailsPage.getAccessoryDeviceFRP();
		accessoryDetailsPage.clickOnAddToCartBtn();
		accessoryDetailsPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifyStoreName();
		cartPage.verifyDistanceToStore();
		cartPage.verifyFindNearbyStoresForAccessory();
		cartPage.verifyInventoryStatusForAccessory();
		String priceOnCart = cartPage.getPriceOfAccessoryOnCartPage();
		cartPage.comparePricesOfAccessoryInPDPandCartPage(priceOnPDP, priceOnCart);
	}

	/**
	 * US429497: TMO - IV Fast Follower - Cart Accessories IV Container
	 * 
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void verifyCartWithAccessoryAndOtherDevice(TMNGData tMNGData) {
		Reporter.log("Test Case : US429497: TMO - IV Fast Follower - Cart Accessories IV Container");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link |Cell phones page should be displayed");
		Reporter.log("3. Click on Any device |PDP page should be displayed");
		Reporter.log("4. Verify Inventory container | Inventory container should be displayed");
		Reporter.log("5. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("6. Click on Add An Accessories Link |Accessories PLP page should be displayed");
		Reporter.log("7. Select any Accessory |Mini PDP page should be displayed");
		Reporter.log("8. Verify Inventory container | Inventory container should be displayed");
		Reporter.log("9. Click on Add to Cart| Cart page should be displayed");
		Reporter.log(
				"10. Verify Inventory container for Accessories section | Inventory container should not be displayed for Accessories section");
		Reporter.log(
				"11. Verify Change locator CTA  | Change locator CTA should not be displayed for Accessories section");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingAccessory(tMNGData);
		cartPage.verifyFindNearbyStoresForAccessory();
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.verifyFindNearbyStoresIsDisplayed();
		cartPage.verifyFindNearbyStoresForAccessory();

	}

	/**
	 * US485373 TMNG > TMO Essentials > Cart > Plans not compatible > Not qualified
	 * Modal US561021 [Continued] [Unfinished] [Continued] TMNG > TMO Essentials >
	 * Cart > Plans not compatible > Not qualified Modal
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyPlanIneligibleModalPopupChangeTMobilePlanCTAFromMainPLP(TMNGData tMNGData) {
		Reporter.log("Test Case : US485373	TMNG > TMO Essentials > Cart > Plans not compatible > Not qualified Modal");
		Reporter.log(
				"Test Case : US561021	[Continued] [Unfinished] [Continued] TMNG > TMO Essentials > Cart > Plans not compatible > Not qualified Modal");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click Add to Cart CTA | Cart Page should be Displayed");
		Reporter.log("5. Select Essential Plan | Essential Plan should be selected");
		Reporter.log("6. Click Add a Phone CTA on cart | Phones mini PLP should be displayed.");
		Reporter.log("7. Click Browse all phones CTA on cart | Internet devices Main PLP should be displayed.");
		Reporter.log("8. Select a wearable from main PLP | Selected Wearable PDP should be displayed.");
		Reporter.log("9. Click Add to Cart CTA on PDP| Cart Page should be Displayed");
		Reporter.log("10. Verify InEligible message on modal popup | 'Oops-wearable devices require "
				+ "the Magenta plan. Do you want to change your plan to Magenta?' message should be displayed");
		Reporter.log(
				"11. Verify 'Yes, change to Magenta' CTA on modal | 'Yes, change to Magenta' CTA should be displayed");
		Reporter.log("12. Click 'Yes, change to Magenta' CTA on modal | 'Magenta' plan should be selected on Cart");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		Reporter.log("!!! DEFECT IS CREATED - https://jiraintake.com/browse/EP-18504");
		navigateToCartPageBySelectingPhone(tMNGData);
		CartPage cartPage = new CartPage(getDriver());

		cartPage.clickOnTMobileEssentialsPlan();
		cartPage.verifyTMobileEssentialsPlanSelected();
		cartPage.clickOnAddAPhoneLinkOnCart();

		PlpPage phonesPage = new PlpPage(getDriver());
		phonesPage.verifyPhonesPlpPageLoaded();
		phonesPage.clickOnWatchesLink();

		phonesPage.verifyWatchesPlpPageLoaded();
		phonesPage.clickDeviceWithAvailability(tMNGData.getWatchName());

		PdpPage watchesAndTabletsDetailsPage = new PdpPage(getDriver());
		watchesAndTabletsDetailsPage.verifyWatchesPdpPageLoaded();
		watchesAndTabletsDetailsPage.clickOnAddToCartBtn();

		cartPage.verifyWearablesIneligibleModal();
		cartPage.verifyWearablesIneligibleModalMessage();
		cartPage.verifyChangePlanCtaDisplayed();
		cartPage.clickChangePlanCta();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyMagentaPlanSelected();

	}

	/**
	 * US485373 TMNG > TMO Essentials > Cart > Plans not compatible > Not qualified
	 * Modal US561021 [Continued] [Unfinished] [Continued] TMNG > TMO Essentials >
	 * Cart > Plans not compatible > Not qualified Modal
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyPlanIneligibleModalPopupRemoveWearableCTAFromMainPLP(TMNGData tMNGData) {
		Reporter.log("Test Case : US485373	TMNG > TMO Essentials > Cart > Plans not compatible > Not qualified Modal");
		Reporter.log(
				"Test Case : US561021	[Continued] [Unfinished] [Continued] TMNG > TMO Essentials > Cart > Plans not compatible > Not qualified Modal");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click Add to Cart CTA | Cart Page should be Displayed");
		Reporter.log("5. Select TMO-Essential Plan | TMO-Essential Plan should be selected");
		Reporter.log("6. Click Add a Phone CTA on cart | Phones mini PLP should be displayed.");
		Reporter.log("7. Click Browse all phones CTA on cart | Phones Main PLP should be displayed.");
		Reporter.log("8. Click Tablets and devices | Internet devices Main PLP should be displayed.");
		Reporter.log("9. Select a wearable from main PLP | Selected Wearable PDP should be displayed.");
		Reporter.log("10. Click Add to Cart CTA on PDP| Cart Page should be Displayed");
		Reporter.log(
				"11. Verify InEligible message on modal popup | 'Oops-wearable devices require the T-Mobile ONE plan. Do you want to change your plan to T-Mobile ONE?' message should be displayed");
		Reporter.log("12. Verify 'No, remove wearable' CTA on modal | 'No, remove wearable' CTA should be displayed");
		Reporter.log(
				"13. Click 'No, remove wearable' CTA on modal | wearable should be removed from cart and navigate user to cart with prior selection of plan");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySelectingPhone(tMNGData);
		CartPage cartPage = new CartPage(getDriver());

		cartPage.clickOnTMobileEssentialsPlan();
		cartPage.verifyTMobileEssentialsPlanSelected();
		cartPage.clickOnAddAPhoneLinkOnCart();

		PlpPage phonesPage = new PlpPage(getDriver());
		phonesPage.verifyPhonesPlpPageLoaded();
		phonesPage.clickOnWatchesLink();

		phonesPage.verifyWatchesPlpPageLoaded();
		phonesPage.clickDeviceWithAvailability(tMNGData.getWatchName());

		PdpPage watchesAndTabletsDetailsPage = new PdpPage(getDriver());
		watchesAndTabletsDetailsPage.verifyWatchesPdpPageLoaded();
		watchesAndTabletsDetailsPage.clickOnAddToCartBtn();

		cartPage.verifyWearablesIneligibleModal();
		cartPage.verifyWearablesIneligibleModalMessage();
		cartPage.verifyNoRemoveCTADisplayed();
		cartPage.clickNoRemoveWearableCTA();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyWearableIsNotDisplayed();
		cartPage.verifyTMobileEssentialsPlanSelected();
	}

	/**
	 * US485373 TMNG > TMO Essentials > Cart > Plans not compatible > Not qualified
	 * Modal US561021 [Continued] [Unfinished] [Continued] TMNG > TMO Essentials >
	 * Cart > Plans not compatible > Not qualified Modal
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyPlanIneligibleModalPopupCloseCTAFromMainPLP(TMNGData tMNGData) {
		Reporter.log("Test Case : US485373	TMNG > TMO Essentials > Cart > Plans not compatible > Not qualified Modal");
		Reporter.log(
				"Test Case : US561021	[Continued] [Unfinished] [Continued] TMNG > TMO Essentials > Cart > Plans not compatible > Not qualified Modal");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click Add to Cart CTA | Cart Page should be Displayed");
		Reporter.log("5. Select TMO-Essential Plan | TMO-Essential Plan should be selected");
		Reporter.log("6. Click Add a Phone CTA on cart | Phones mini PLP should be displayed.");
		Reporter.log("7. Click Browse all phones CTA on cart | Phones Main PLP should be displayed.");
		Reporter.log("8. Click Tablets and devices | Internet devices Main PLP should be displayed.");
		Reporter.log("9. Select a wearable from main PLP | Selected Wearable PDP should be displayed.");
		Reporter.log("10. Click Add to Cart CTA on PDP| Cart Page should be Displayed");
		Reporter.log(
				"11. Verify InEligible message on modal popup | 'Oops-wearable devices require the T-Mobile ONE plan. Do you want to change your plan to T-Mobile ONE?' message should be displayed");
		Reporter.log("12. Verify 'X' close CTA on modal | 'X' close CTA should be displayed");
		Reporter.log(
				"13. Click 'X' CTA on modal | wearable should be removed from cart and navigate user to cart with prior selection of plan");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySelectingPhone(tMNGData);
		CartPage cartPage = new CartPage(getDriver());

		cartPage.clickOnTMobileEssentialsPlan();
		cartPage.verifyTMobileEssentialsPlanSelected();
		cartPage.clickOnAddAPhoneLinkOnCart();

		PlpPage phonesPage = new PlpPage(getDriver());
		phonesPage.verifyPhonesPlpPageLoaded();
		phonesPage.clickOnWatchesLink();

		phonesPage.verifyWatchesPlpPageLoaded();
		phonesPage.clickDeviceWithAvailability(tMNGData.getWatchName());

		PdpPage watchesAndTabletsDetailsPage = new PdpPage(getDriver());
		watchesAndTabletsDetailsPage.verifyWatchesPdpPageLoaded();
		watchesAndTabletsDetailsPage.clickOnAddToCartBtn();

		cartPage.verifyWearablesIneligibleModal();
		cartPage.verifyWearablesIneligibleModalMessage();
		cartPage.clickWearableIneligibleModalCTA();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyWearableIsNotDisplayed();
		cartPage.verifyTMobileEssentialsPlanSelected();
	}

	/**
	 * US486404 TMNG > TMO Essentials > Product First > Accessories added
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyPlanSectionWithOnlyAccessoryFromEmptyCart(TMNGData tMNGData) {
		Reporter.log("Test Case : US486404	TMNG > TMO Essentials > Product First > Accessories added");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | Cart page should be displayed");
		Reporter.log("2. Click  Add a Accessory link on Cart | Accessory Mini PLP should  be displayed");
		Reporter.log("3. Select any Accessory in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("4. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("5. Verify 'Add An Accessoty' tile on Cart | 'Add An Accessoty' tile should be supressed");
		Reporter.log("6. Verify  'Rate Plan section' on Cart | Rate Plan Section should be supressed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
		cartPage.verifyOldAddAnAccessoryTileIsNotDisplayed();
		cartPage.verifyRatePlanSectionIsNotDisplayed();
	}

	/*----------- Line Section --------------*/

	/**
	 * C347318: TMO - Cart: Remove line
	 * 
	 * @param data
	 * @param tMNGData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void removeDeviceFromTheCart(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case: TMO - Cart:  Remove line");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Cart icon| Cart page should be displayed");
		Reporter.log("4. Click on 'Add a Phone' button | Device image should be displayed");
		Reporter.log(
				"5. click on '+' button in device image and Add a new Phone button | Mini PLP page should be displayed");
		Reporter.log("6. select any device | Mini PDP page should be displayed");
		Reporter.log("7. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("8. Verify remove CTA| Remove CTA should be displayed");
		Reporter.log("9. Click remove CTA| Device should be removed from cart");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyRemoveCTA();
		cartPage.clickRemoveCTA();
		cartPage.verifyEmptyCart();
	}

	/**
	 * C347318: TMO - Cart: Duplicate line
	 * 
	 * @param data
	 * @param tMNGData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void duplicateDeviceFromTheCart(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case: C347318: TMO - Cart: Duplicate line");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Cart icon| Cart page should be displayed");
		Reporter.log("4. Click on 'Add a Phone' button | Device image should be displayed");
		Reporter.log(
				"5. Click on '+' button in device image and Add a new Phone button | Mini PLP page should be displayed");
		Reporter.log("6. Select any device | Mini PDP page should be displayed");
		Reporter.log("7. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("8. Verify duplicate CTA| Duplicate CTA should be displayed");
		Reporter.log("9. Click duplicate CTA| Duplicate line should be added in  cart");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyDuplicateCTA();
		cartPage.clickDuplicateCTA();
		cartPage.verifyDuplicateLinesOnCart();
	}

	/**
	 * C545948 - Cart Page:Edit button is removed from the product tile US587344 -
	 * TPP - Remove Edit Button on devices
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyEditCTAAvailabilityOnCartForDifferentProducts(TMNGData tMNGData) {
		Reporter.log("Test Case: C545948 - Cart Page:Edit button is removed from the product tile");
		Reporter.log("Test Case: 	US587344 - TPP - Remove Edit Button on devices");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Add Tablet, Wearable and Accessory to cart |All type of products should be added into cart");
		Reporter.log("6. Verify EDIT CTA for all lines | EDIT CTA  should be disabled for all lines in the cart");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.addAWearabletoCartFromCartPage(tMNGData);
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
		cartPage.verifyEditCTAForAllLinesNotDisplayed();
		if (getTogglevalue("disableEditV3Cart").equals("true")) {
			cartPage.verifyEditCTAForAllLinesNotDisplayed();
		} else {
			cartPage.verifyEditCTA();
			cartPage.verifyEditCTAOnCartPageForAccessory();
		}
	}

	/**
	 * US451172 TMNG > Product First > Cart > Awesome Credit > Max Lines
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyMaxLineErrorMsgWithAwesomeCreditFromEmptyCart(TMNGData tMNGData) {
		Reporter.log("Test Case : US451172	TMNG > Product First > Cart > Awesome Credit > Max Lines");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Phone link on Cart | Credit options modal should  be displayed");
		Reporter.log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("4. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("6. Add 2 more devices to cart | 3 devices should be added into cart");
		Reporter.log(
				"7. Verify Error message below Add More Lines on Cart | 'Want more lines? Please give us a call at 1-844-889-4983' should be displayed.");
		Reporter.log(
				"8. Verify Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's on Cart | Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's should be disabled");
		Reporter.log("9. Verify Add a Accessories img's on Cart | Add a Accessories img  should be enabled");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnDuplicateNTimes(2);

		cartPage.addATablettoCartFromCartPage(tMNGData);

		cartPage.addAWearabletoCartFromCartPage(tMNGData);

		cartPage.verifyDuplicateLinesCountOnCart(5);
		cartPage.verifyLineMaxLimitReachedMessageOnCart();
		cartPage.verifyAddAPhonebtnDisabled();
		cartPage.verifyAddAWearablebtnDisabled();
		cartPage.verifyAddATabletbtnDisabled();
		cartPage.verifyBYODbtnDisabled();
		cartPage.verifyAddAAcessorybtnEnabled();
	}

	/**
	 * US332641 [Continued] TEST ONLY: TMO Additional Terms - View Order Details
	 * Modal: Trade-in Device Value Display for Promo Trade-in US332456 [Continued]
	 * TEST ONLY: TMO Additional Terms - Cart: Pricing Display with Promo Trade-in
	 * Value Applied for Promo Trade-in US332457 -TEST ONLY: TEST ONLY: TMO
	 * Additional Terms - View Order Details Modal: Pricing Display with Promo
	 * Trade-in Value Applied for Promo Trade-in US332626:[Continued] TEST ONLY: TMO
	 * Additional Terms - Cart: Trade-in Device Tile Value Display for Promo
	 * Trade-in
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testVerifyTradeindevicesMonthlyTradeinValueByselectingPromo(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US332641 -TEST ONLY: TMO Additional Terms - View Order Details Modal: Trade-in Device Value Display for Promo Trade-in");
		Reporter.log(
				"Test Case : US332456 -TEST ONLY: TMO Additional Terms - Cart: Pricing Display with Promo Trade-in Value Applied for Promo Trade-in");
		Reporter.log(
				"Test Case : US332457 -TEST ONLY: TEST ONLY: TMO Additional Terms - View Order Details Modal: Pricing Display with Promo Trade-in Value Applied for Promo Trade-in");
		Reporter.log(
				"Test Case : US332626:[Continued] TEST ONLY: TMO Additional Terms - Cart: Trade-in Device Tile Value Display for Promo Trade-in");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device |PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Click on 'Trade Device' button | Trade-in modal  should be displayed");
		Reporter.log(
				"6. Fill all Trade-in info , select device condition and click on Get estimate | Trade-in options modal should be displayed");
		Reporter.log("7. select promotional trade-in option and click on Agree button | Cart page should be displayed");
		Reporter.log("8. click on view order details link| view order details modal should be displayed");
		Reporter.log(
				"9. verify trade-in device's monthly trade-in value in the tile|trade-in device's monthly trade-in value in the tile  should be displayed");
		Reporter.log(
				"10. verify trade-in device's promo length is same length as the promo device's EIP loan term length|trade-in device's promo length should be same length as the promo device's EIP loan term length");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickOnTradeDeviceCTA();
		cartPage.verifyTradeInModalDisplayed();

		TradeInValuePopUpModal tradeInValuePopUpModal = new TradeInValuePopUpModal(getDriver());
		tradeInValuePopUpModal.selectCarrier(tMNGData.getCarrierName().substring(0, 2));
		tradeInValuePopUpModal.selectMakeOptions(tMNGData.getMakeName().substring(0, 2));
		tradeInValuePopUpModal.selectModalOptions(tMNGData.getModalName().substring(0, 2));
		tradeInValuePopUpModal.setIMEINumber(tMNGData.getImeiName());

		tradeInValuePopUpModal.clickOnGoodRadioBtn();
		tradeInValuePopUpModal.clickOnTradeInModal();
		tradeInValuePopUpModal.clickOnGetEstimatedBtn();

		cartPage.verifyselectYourOptionsModalDisplayed();
		cartPage.clickOnPromoTradeInBtn();
		cartPage.clickOnAgreeCTABtn();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyPromoTradeinDetailsDisplayedOnCart();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyPromoTradeInDetailDisplayedonOrderDetails();
	}

	/**
	 * US526148 TMO Essentials - MOBILE and TABLET - Line Level Pricing new layout
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.IOS, Group.ANDROID })
	public void testVerifyLineLevelPricingFromEmptyCartTabletView(TMNGData tMNGData) {
		Reporter.log("Test Case : US526148	TMO Essentials - MOBILE and TABLET - Line Level Pricing new layout");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Phone link on Cart | Device Plus img should  be displayed");
		Reporter.log("3. Click on add a tablet  | tablets plp page  should  be displayed");
		Reporter.log("4. Select any device in PLP  PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("6. Verify Line level Pricing block on Cart | Line level Pricing block on Cart should be displayed.");
		Reporter.log("7. Verify Header Monthly Payment | Monthly Payment header should be displayed");
		Reporter.log("8. Verify Monthly Payment Line items | Line, Device, Add-ons(does not display on first page load) and Autopay discount should be displayed along with prices");
		Reporter.log("9. Verify Header One Time Payment | One Time Payment header should be displayed");
		Reporter.log("10. Verify One Time Payment Line items | SIM Starter Kit, Line Deposit (when applicable) and taxes and shipping should be displayed along with prices");
		Reporter.log("11. Verify Price breakdown under One Time Payment | Price Breakdown should be displayed");
		Reporter.log("12. Verify Price breakdown items under One Time Payment | Today/Monthly pricing, taxes and shipping, and Includes $5 monthly Autopay discount should be displayed along with prices");
		Reporter.log("13. Verify PLAN block on Cart | PLAN block on Cart should be displayed for each line");
		Reporter.log("14. Verify Header PLAN |PLAN header should be displayed");
		Reporter.log("15. Verify PLAN items | Selected Plan should be displayed. Like T-Mobile ONE or T-Mobile Essentials");
		Reporter.log("16. Verify Tool tip for selected PLAN |Tooltip should be displayed for selected PLAN");
		Reporter.log("17. Verify ADD-ONS AND EXTRAS hearder under PLAN| ADD-ONS AND EXTRAS header should be displayed");
		Reporter.log("18. Verify List of Add-ons and extras |List of Add-ons and extras should be displayed like T-Mobile ONE PLUS etc");
		Reporter.log("19. Verify check boxes of List of Add-ons and extras |check box of List of Add-ons and extras should be displayed");
		Reporter.log("20. Verify Price under List of Add-ons and extras  | Price should be displayed under List of Add-ons and extras");
		Reporter.log("21. Verify  tool tip for List of Add-ons and extras  | Tool-tip should be displayed for List of Add-ons and extras");
		Reporter.log("22. Verify  Add-ons price on Monthly payment section | Add-ons should not be displayed on Monthly section if user not selected any add-on");
		Reporter.log("23. Select any Add on from List of Add-ons and extras  | Selected Add On should be displayed in Monthly Payment layout with proper title and price.");
		Reporter.log("24. Click update credit  | Credit modal should be displayed");
		Reporter.log("25. Select No credit check and click done  | Credit type should be updated to No credit");
		Reporter.log("26. Verify Line deposite at One Ttime payment  | Line deposit should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		CartPage cartPage = navigateToEmptyCart(tMNGData);

		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.verifylineLevelPriceBlockDisplayed();
		cartPage.verifyMonthlyPaymentHeaderDisplayed();
		cartPage.verifyMonthlyPaymentLinePriceDisplayed();
		cartPage.verifyMonthlyPaymentDeviceDisplayed();
		cartPage.verifyMonthlyPaymentDevicePriceDisplayed();
		cartPage.verifyMonthlyPaymentDiscountDisplayed();
		cartPage.verifyMonthlyPaymentDiscountPriceDisplayed();
		cartPage.verifyOneTimePaymentDisplayed();
		cartPage.verifyPriceBreakDownUnderOneTimePaymentDisplayed();
		cartPage.verifyPriceBreakDownMonthlyPriceDisplayedLineLevel();
		cartPage.verifyPriceBreakDownAutoPayDiscountMessageDisplayed();
		cartPage.verifyPlanBlockForLine();
		cartPage.verifyPlanHeaderForLine();
		cartPage.verifyPlanItemForLine();
		cartPage.verifyToolTipForPlan();
		cartPage.verifyAddOnExtrasHeaderDisplayed();
		cartPage.verifyListOfAddOnsDisplayed();
		cartPage.verifyListOfPriceAddOnExtrasHeaderDisplayed();
		cartPage.verifyListOfCheckBoxDisplayed();
		cartPage.verifyListOfToolTipsAddOnExtrasHeaderDisplayedForMobile();
		cartPage.verifyMonthlyPaymentAddonsLineNotDisplayed();
		cartPage.verifyMonthlyPaymentAddonPriceNotDisplayed();
		/*
		 * cartPage.verifySeeMoreOptionsCTADisplayed();
		 * cartPage.clickSeeMoreOptionsCTAAddOnsAndExtras();
		 * cartPage.verifyEnterZipModal();
		 * cartPage.enterYourZIPCode(tMNGData.getZipcode());
		 * cartPage.clickNextCTAOnZipCodeModal(); cartPage.verifyProtectionModal();
		 * cartPage.selectProtection360PlanOnModal();
		 * cartPage.clickUpdateCTAonProtectionModal();
		 */
		cartPage.clickFirstOptionInAddOnsAndExtras();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyMonthlyPaymentAddonsLineDisplayed();
		cartPage.verifyMonthlyPaymentAddonPriceDisplayed();
		cartPage.verifyLineDepositDisplayed();
		cartPage.verifyLineDepositPriceDisplayed();

	}

	/**
	 * C347334: EIP to FRP ineligibility 2: FRP accessories worth of greater than
	 * $69
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testFrpAccessoriesPriceInCartPage(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case :C347334: EIP to FRP ineligibility 2: FRP accessories worth of greater than $69");
		Reporter.log("Test Steps | Expected Results:");

		Reporter.log("1. Launch TMO | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Cart icon | Cart page should be displayed");
		Reporter.log("4. Click on Accessory + Icon | Accessory Mini plp page should be displayed");
		Reporter.log("5. Select accessory wich is more than $69 | Accessory Mini pdp page should be displayed");
		Reporter.log("6. Save Accessory FRP price in V1 | Accessory FRP price should be saved in V1");
		Reporter.log("7. Click Add to cart button | Cart page should be displayed");
		Reporter.log("8. Click on Accessory + Icon | Accessory Mini plp page should be displayed");
		Reporter.log("9. Save Accessory FRP price in V2 | Accessory FRP price should be saved in V2");
		Reporter.log("10.Compare V1 and V2 | Accessory FRP price should be equal");
		Reporter.log("11.Verify Accessory FRP price | Accessory FRP price should be > $69");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage accessoryPlp = navigateToAccessoryPLP(tMNGData);
		accessoryPlp.clickOnSortDropdown();
		accessoryPlp.clickSortByPriceHighToLow();
		accessoryPlp.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryPDPPage = new PdpPage(getDriver());
		accessoryPDPPage.verifyAccessoriesPdpPageLoaded();
		String totalPriceInMiniPDP = accessoryPDPPage.getDeviceFRP();
		accessoryPDPPage.clickOnAddToCartBtn();
		accessoryPDPPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		String todayPrice = cartPage.getTotalPriceOfAccessoryOnCartPage();
		cartPage.compareAccessoryPriceInCartPage(totalPriceInMiniPDP, todayPrice);
		cartPage.verifyAccessoryFrpPrice(todayPrice);
	}

	/*----------- Add more lines or devices --------------*/

	/*----------- Sticky Banner --------------*/

	/**
	 * US523775: TMO Essentials - Sticky Banner - Desktop US523777: TMO Essentials -
	 * Sticky Banner - Mobile
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.IOS, Group.ANDROID })
	public void testEssentialsStickyBannerInCartPage(TMNGData tMNGData) {
		Reporter.log("Test Case: US523775: TMO Essentials - Sticky Banner - Desktop");
		Reporter.log("Test Case: US523777: TMO Essentials - Sticky Banner - Mobile");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Verify Sticky banner at bottom of cart page | Sticky banner should be displayed in cart page");
		Reporter.log("6. Verify Based on:'With credit option' | Based on:'With credit option' should be displayed");
		Reporter.log(
				"7. Verify Monthly total with 'Include $'price' monthly AuoPay discount' text | Monthly total with 'Include $'price' monthly AuoPay discount' text should be displayed");
		Reporter.log("8. Verify Monthly total price | Monthly total price should be displayed");
		Reporter.log(
				"9. Verify Today with '+taxes and shipping' text | Today with '+taxes and shipping' text should be displayed");
		Reporter.log("10. Verify Today price | Today price should be displayed");
		Reporter.log("11. Verify estimated to ship text | estimated to ship text should be displayed");
		Reporter.log("12. Verify estimated to ship date | estimated to ship date should be displayed");
		Reporter.log("13. Verify Update Credit link | Update Credit link should be displayed");
		Reporter.log("14. Click on Update Credit link | Credit class model should be displayed");
		Reporter.log("15. Click on Done button | CartPage should be displayed");
		Reporter.log("16. Verify View order details button | View order details button should be displayed");
		Reporter.log("17. Click on View order details button | Order Details model should be displayed");
		Reporter.log("18. Verify complete in store link | Complete in store link should be displayed");
		Reporter.log("19. Click on complete in store link | Retail Cart model should be displayed");
		Reporter.log("20. Click on Continue button | Check out page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyStickyBannerDisplayed();
		cartPage.verifyStickyBannerAutoPayTextDisplayed();
		cartPage.verifyStickyBannerMonthlyTotalPriceDisplayed();
		cartPage.verifyStickyBannerTaxAndShippingTextDisplayed();
		cartPage.verifyStickyBannerTodayTotalPriceDisplayed();
		cartPage.verifyStickyBannerEstimatedShippingTextDisplayed();
		cartPage.verifyStickyBannerEstimatedShippingDateDisplayed();
		cartPage.verifyStickyBannerOrderDetailsLinkDisplayed();
		cartPage.verifySaveCartCTAOnCart();
		cartPage.verifyRetrieveCartOnCartPage();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.clickOnCloseIconAtOrderDetailPage();
		cartPage.verifyCompleteInStoreLinkDisplayed();
		cartPage.clickCompleteInStoreLink();
		cartPage.verifyCompleteStoreModalWindow();
		cartPage.clickCloseIconInCompleteInStoreModalWindow();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();

	}

	/**
	 * US433890: TMO - IV Fast Follower: Web Save Cart Confirmation for Accessories
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void verifyWebSaveCartForAccessories(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case :  US433890: TMO - IV Fast Follower: Web Save Cart Confirmation for Accessories");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. click on Accessories Link |Accessories PLP page should be displayed");
		Reporter.log("4. Select any Accessory |PDP page should be displayed");
		Reporter.log("5. Verify Inventory container | Inventory container should be displayed");
		Reporter.log("6. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("7. Click on Save Cart  | Save Cart pop up should display");
		Reporter.log("8. Verify Cart icon on top of the page| Cart icon should be displayed");
		Reporter.log(
				"9. Verify input email in a field under the message | input email in a field under the message should be displayed");
		Reporter.log("10.Enter email id in email text filed | Enterted email should be displayed in email text box");
		Reporter.log("11. Verify checkbox | checkbox should be displayed");
		Reporter.log("12. Verify check box CTA| checkbox should be selected");
		Reporter.log("13. Click on save button | Conformation Page should be displayed");
		Reporter.log(
				"14. Verify web save cart page | Page should contain selected accessory name, inventory status and invenroty conatiner");// only
		// container
		Reporter.log("15. Click on close(X) icon | Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		CartPage cartPage = navigateToCartPageBySelectingAccessory(tMNGData);
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
		cartPage.clickOnSaveCart();
		cartPage.verifySaveCartModelWindow();
		cartPage.verifyCartIconOnSaveCartModel();
		cartPage.verifySaveCartHeader();
		cartPage.verifySaveCartSubHeader();
		String email = cartPage.getEmailWithDate(tMNGData);
		cartPage.enterEmailWithDateInSaveCart(email);
		cartPage.clickSaveButtonOnSaveCartModel();
		cartPage.verifyConfirmationMessageonSaveCartModel();
		cartPage.clickShowItemsLinkOnSaveCartConfirmationModal();
		cartPage.verifyAccessoryNameOnSavecartConfirmationModal();
		cartPage.verifyInventoryStatusOnSavecartConfirmationModal();
		cartPage.clickCloseOnSaveCartModel();
		cartPage.verifyCartPageLoaded();
	}

	/**
	 * US338185 :TMO - IV- (Retail) Save Cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testSaveRetailCart(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US338185 :TMO - IV- (Retail) Save Cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device |PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Verify 'Complete in Store' link | 'Complete in Store' link should be displayed");
		Reporter.log("6. Click 'Complete in Store' link | 'Complete in Store' model window should be displayed ");
		Reporter.log("7. Verify Cart Icon in model window | Cart Icon in model window should be displayed ");
		Reporter.log("8. Verify Title in model window | Title in model window should be displayed");
		Reporter.log("9. Verify Store name in model window | Store name in model window should be displayed");
		Reporter.log("10. Verify message in model window | Message in model window should be displayed");
		Reporter.log("11. Verify Input Email box in model window | Email box in model window should be displayed");
		Reporter.log(
				"12. Verify Check box and a message next to it in model window | Check box and a message next to it in model window should be displayed");
		Reporter.log(
				"13. Verify Save Cart button in model window and It's Enabled | Save Cart button in model window should be displayed and it is enabled");
		Reporter.log("14. Verify model window 'X' icon | Model window 'X' icon should be displayed");
		Reporter.log("15. Click model window 'X' icon | CartPage should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyCompleteInStoreLinkDisplayed();
		cartPage.clickCompleteInStoreLink();
		cartPage.verifyCompleteStoreModalWindow();
		cartPage.verifyCarticonInModalWindow();
		cartPage.verifyTitleInSaveCartOnlineModalWindow();
		cartPage.verifyEmailFieldInmodalWindow();
		String email = cartPage.getEmailWithDate(tMNGData);
		cartPage.enterEmailWithDateInSaveCart(email);
		cartPage.clickSaveMyCartOnlineCtaInModelWindow();
		cartPage.verifyConfirmationMessage();
		cartPage.verifyLetsTalkMessage();
		cartPage.verifyTFNnumber();
		cartPage.VerifyTFNFunctionalityTurnOn();
		cartPage.verifyCloseIconInModalWindow();
		cartPage.clickCloseIconInCompleteInStoreModalWindow();
		cartPage.verifyCartPageUrl();
	}

	/**
	 * US347763: TMO - Display Total Financed Amount in Cart View Order Details
	 * Modal - EIP Amount in Cart View Order Details Modal
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testTotalFinancedAmountInOrderDetailsModalPurchasedOnEIPOnCartPage(TMNGData tMNGData) {
		Reporter.log("Test Case : US347763 - TMO -  Display Total Financed Amount in Cart View Order Details Modal");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link |Cell phones page should be displayed");
		Reporter.log("3. Select any device |PDP page should be displayed");
		Reporter.log("4. Select finance from payment options| Finance payment option should be selected");
		Reporter.log("5. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("6. Click view order details link | Order details modal window should be displayed");
		Reporter.log("7. Verify total financed amount | Total finance amount should be displayed");
		Reporter.log(
				"8. Calculate total finance amount as per frp and downpayment | Finance amount sum should be frp minus downpayment");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		if (getTogglevalue("showFinancedAmount").equals("true")) {
			cartPage.verifyTotalFinanceAmount();
			cartPage.financeAmountValuationForEIP();
		} else {
			Reporter.log("showFinancedAmount is false and TFA is not present on OrderDetailed page.");
		}

	}

	/**
	 * US347763: TMO - Display Total Financed Amount in Cart View Order Details
	 * Modal - FRP Amount in Cart View Order Details Modal
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testTotalFinancedAmountInOrderDetailsModalPurchasedOnFRP(TMNGData tMNGData) {
		Reporter.log("Test Case : US347763: TMO -  Display Total Financed Amount in Cart View Order Details Modal");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link |Cell phones page should be displayed");
		Reporter.log("3. Select any device |PDP page should be displayed");
		Reporter.log(
				"4. Select pay in full today from payment options| Pay in full today payment option should be selected");
		Reporter.log("5. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("6. Click view order details link | Order details modal window should be displayed");
		Reporter.log("7. Verify total financed amount | Total finance amount should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageWithFullPayment(tMNGData);
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyTotalFinanceAmountForFRP();

	}

	/**
	 * TA1655232: TMNG:DMO: Order details page : Order details page still showing 24
	 * month as the loan term for device with 36*month loan term. Ref : DE165195
	 * 
	 * Modified test to verify that loan term is same in miniPdp and Order details
	 * pages
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testLoanTermLengthInOrderDetailsSection(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case :TA1655232: TMNG: DMO: Order details page : Order details page still showing 24 month as the loan term for  device with 36*month loan term.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Cart icon | Cart page should be displayed");
		Reporter.log("4. Click on 'Add a Phone' button | Mini PLP page should be displayed");
		Reporter.log("5. Select device wich has 36 months loan term length | Mini PDP page should be displayed");
		Reporter.log(
				"6. Get Device loan term length and store the value in 'loanTermLengthInPDP' | Should be stored device loan term length value in 'loanTermLengthInPDP'");
		Reporter.log("7. Click Add to cart button | Cart page should be displayed");
		Reporter.log("8. Click View order details link | Order details model should be displayed");
		Reporter.log("9. Verify Device loan term length | Device loan term lenth should be displayed");
		Reporter.log("10. Compare Device loan term length | Device loan term lenth should be equal");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage pdpPage = navigateToPhonesPDP(tMNGData);
		String loanTermLengthInPDP = pdpPage.getEipLoanTermLength();
		pdpPage.clickOnAddToCartBtn();
		pdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyDeviceNameInOrderDetailsModel();
		cartPage.verifyDeviceLoanTermLength();
		cartPage.compareDeviceLoanTermLengthSameForPDPAndCartPages(cartPage.getDeviceLoanTermLengthInCartPage(),
				loanTermLengthInPDP);
	}

	/**
	 * US477655 TMNG > TMO Essentials > Cart > V1 to V2 Migration > Order details -
	 * EIP
	 * 
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testVerifyEIPDiscountStrikeThroughPriceInOrderDetailModal(TMNGData tMNGData) {
		Reporter.log("Test Case :  US477655	TMNG > TMO Essentials > Cart > V1 to V2 Migration > Order details - EIP");
		Reporter.log("Test Conditions: Select device which is having discount");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device whichis having discount | PDP page should be displayed");
		Reporter.log("4. Select EIP from payment options | EIP should be selected");
		Reporter.log("5. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("6. Click View order details link | Order details should be displayed");
		Reporter.log("7. Verify EIP in order detail modal | EIP price should be displayed");
		Reporter.log("8. Verify FRP in order detail modal | FRP price should be displayed");
		Reporter.log("9. Verify Instant discount in order detail modal | Instant discount should be displayed");
		Reporter.log(
				"10. Verify Strike through price in order detail modal | Strike through price should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifystrikeThroughPriceInOrderDetailsModel();
		cartPage.verifyOrderDetailsModalEIP();
		cartPage.verifyOrderDetailsModalFRP();
		cartPage.verifyOrderDetailsModalInstantDiscount();
	}

	/**
	 * C347321: Multi-line order: Pricing (Breakout & Totals) Review, Submit and
	 * Confirmation
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testMultiLineOrderPriceBreakDownTotals(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case :C347321: Multi-line order: Pricing (Breakout & Totals) Review, Submit and Confirmation");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | PLP page should be displayed");
		Reporter.log("3. Selectd device | PDP page should be displayed");
		Reporter.log("4. Click on 'Add to cart' button and select 'As a guyst' | Cart page should be displayed");
		Reporter.log("5. Click on 'BYOD' cta | Mini PDP page should be displayed");
		Reporter.log("6. Click on 'Add to cart' cta | Cart page should be displayed");
		Reporter.log("7. Click on 'Tablets' cta | Mini PLP should be displayed");
		Reporter.log("8. Select device and click on 'Add to cart' cta | Cart page should be displayed");
		Reporter.log("9. Click on 'Wearables' cta | Mini PLP should be displayed");
		Reporter.log("10. Select device and click on 'Add to cart' cta | Cart page should be displayed");
		Reporter.log("11. Verify 'Today' and 'Monthly' prices in sticky banner | Prices should be displayed");
		Reporter.log(
				"12. Click 'View order details' link in sticky banner | 'Order details' modal should be displayed");
		Reporter.log("13. Click close icon | 'Order details' modal should be closed");
		Reporter.log("14. Click close icon | 'Order details' modal should be closed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.addAWearabletoCartFromCartPage(tMNGData);

		cartPage.verifyStickyBannerTodayTotalPriceDisplayed();
		cartPage.verifyStickyBannerMonthlyTotalPriceDisplayed();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.clickOnCloseIconAtOrderDetailPage();
	}

	/**
	 * C347333: EIP/FRP eligibility 1: FRP eligible Accessories are converted to EIP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testFrpEligibleAccessoriesAreConvertedToEip(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case :C347333: EIP/FRP eligibility 1: FRP eligible Accessories are converted to EIP");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | Application Should be Launched");
		Reporter.log("2. Click on Accessories Link | Accessories page should be displayed");
		Reporter.log("3. Select sort option as 'High to Low' | All device FRP price should be sorted");
		Reporter.log("4. Select Accessory device | Accessory PDP page should be sorted");
		Reporter.log("5. Click Add to cart button | Cart page should be sorted");
		Reporter.log("6. Click 'Add A Phone' button | Device image should be displayed");
		Reporter.log(
				"7. click on '+' button in device image and Add a new Phone button | Mini PLP page should be displayed");
		Reporter.log("8. Select device  | Mini PDP page should be displayed");
		Reporter.log("9. Click Add to cart button | Cart page should be displayed");
		Reporter.log("10. Verify EIP message | Eip message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage accessoriesPage = navigateToAccessoryPLP(tMNGData);
		accessoriesPage.clickOnSortDropdown();
		accessoriesPage.clickSortByPriceHighToLow();
		accessoriesPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryDetailsPage = new PdpPage(getDriver());
		accessoryDetailsPage.verifyAccessoriesPdpPageLoaded();
		accessoryDetailsPage.verifDeviceImageAtPDP();
		accessoryDetailsPage.clickOnAddToCartBtn();
		accessoryDetailsPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnAddAPhoneLinkOnCart();
		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyPhonesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getSecondDeviceName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyEipMessage();
	}

	/**
	 * C347334: EIP to FRP ineligibility 2: FRP accessories worth of greater than
	 * $71
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testEipPricesForAccessoryDeviceAndVoiceLineInCartPage(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case :C347334: EIP to FRP ineligibility 2: FRP accessories worth of greater than $71");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch TMO | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Cart icon | Cart page should be displayed");
		Reporter.log("4. Click on Accessory + Icon | Accessory Mini plp page should be displayed");
		Reporter.log("5. Select accessory wich is more than $69 | Accessory Mini pdp page should be displayed");
		Reporter.log("6. Click Add to cart button | Cart page should be displayed");
		Reporter.log("7. Click on Add a phone button | Device image should be displayed");
		Reporter.log(
				"8. Click on '+' button in device image and Add a new Phone button | Credit class options should be displayed");
		Reporter.log(
				"9. Verify defaults selection of credit class component | Awesome credit should be displayed as default");
		Reporter.log("10. Click on Done button | Mini PDP page should be displayed");
		Reporter.log("11. Select any device | Mini PDP page should be displayed");
		Reporter.log("12. Click on Add to cart button | CartPage should be displayed");
		Reporter.log("13. Verify Accessory EIP Price | Accessory EIP Price page should be displayed");
		Reporter.log("14. Verify Voice line EIP price | Voice line EIP price should displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage accessoriesPage = navigateToAccessoryPLP(tMNGData);
		accessoriesPage.clickOnSortDropdown();
		accessoriesPage.clickSortByPriceHighToLow();
		accessoriesPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryDetailsPage = new PdpPage(getDriver());
		accessoryDetailsPage.verifyAccessoriesPdpPageLoaded();
		accessoryDetailsPage.verifDeviceImageAtPDP();
		accessoryDetailsPage.clickOnAddToCartBtn();
		accessoryDetailsPage.selectContinueAsGuest();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifyAccessoryFrpPriceInAccessoryTileIsDisplayed();
		cartPage.verifyAccessoryIepPriceNotDisplayedInAccessoryTile();

		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyAccessoryEipPriceInOrderDetails();
		cartPage.verifyDeviceEipPriceInOrderDetails();
	}

	/**
	 * DE218577: TMO - Full price is not present on Order Details modal for EIP
	 * devices
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyTotalPriceInOrderDetailsModalPurchasedOnEIPOnCartPage(TMNGData tMNGData) {
		Reporter.log("Test Case : DE218577: TMO - Full price is not present on Order Details modal for EIP devices");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link |Cell phones page should be displayed");
		Reporter.log("3. Select any device |PDP page should be displayed");
		Reporter.log("4. Select finance from payment options| Finance payment option should be selected");
		Reporter.log("5. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("6. Click view order details link | Order details modal window should be displayed");
		Reporter.log("7. Verify total Price | Total price should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonePdpPage = navigateToPhonesPDP(tMNGData);
		String frp = phonePdpPage.getDeviceFRP();
		phonePdpPage.clickOnAddToCartBtn();
		phonePdpPage.selectContinueAsGuest();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyTotalPriceOnOrderDetailedModal(frp);

	}

	/**
	 * C347334: EIP to FRP ineligibility 2: FRP accessories worth of less than $71
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testFrpPricesForAccessoryDeviceAndVoiceLineInCartPage(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case :C347334: EIP to FRP ineligibility 2: FRP accessories worth of less than $71");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Cart icon | Cart page should be displayed");
		Reporter.log("4. Click on Accessory Icon | Accessory Mini plp page should be displayed");
		Reporter.log("5. Select accessory wich is less than $69 | Accessory Mini pdp page should be displayed");
		Reporter.log("6. Click Add to cart button | Cart page should be displayed");
		Reporter.log("7. Click on Add a phone button | Device image should be displayed");
		Reporter.log(
				"8. Verify defaults selection of credit class component | Awesome credit should be displayed as default");
		Reporter.log("9. Click on Done button | Mini PDP page should be displayed");
		Reporter.log("10. Select any device | Mini PDP page should be displayed");
		Reporter.log("11. Click on Add to cart button | CartPage should be displayed");
		Reporter.log("12. Verify Accessory EIP Price | Accessory EIP Price page should be displayed");
		Reporter.log("13. Verify Voice line EIP price | Voice line EIP price should displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingAccessory(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.verifyAccessoryFrpPriceInAccessoryTileIsDisplayed();
		cartPage.verifyAccessoryIepPriceNotDisplayedInAccessoryTile();
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.verifyAccessoryFrpPriceInAccessoryTileIsDisplayed();
		// cartPage.verifyAccessoryIepPriceNotDisplayedInAccessoryTile();
	}

	/**
	 * US436761: TMNG > Product First > Cart > Remove CTA's
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyAddAPhoneAddaTabletorWearableRemoveCTAsFromMainPLP(TMNGData tMNGData) {
		Reporter.log("Test Case : US436761: TMNG > Product First > Cart > Remove CTA's");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Verify Add a Phone CTA on Cart | Add a Phone CTA should not be displayed");
		Reporter.log(
				"6. Verify Add a Tablet or Wearable CTA on Cart | Add a Tablet or Wearable  CTA should not be displayed");
		Reporter.log(
				"7. Verify Accessory section | Accessory section should be supressed (Header, title and Add Accessory Img)");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.verifyAddAPhoneCTAOnCartNotDisplayed();
		cartPage.verifyOldAddATabletOrWearableOnCartNotDisplayed();
		cartPage.verifyOldAddAAccessoryCTAOnCartNotDisplayed();
		cartPage.verifyOldAddAAccessoryHeaderOnCartNotDisplayed();
		cartPage.verifyOldAddAAccessoryImageOnCartNotDisplayed();

	}

	/**
	 * US492600 TMNG > TMO Essentials > Plan Selection > Display appropriate
	 * services
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testVerifyDifferentServicesforDifferentPlansForMobileFromEmptyCart(TMNGData tMNGData) {
		Reporter.log("Test Case : US492600 TMNG > TMO Essentials > Plan Selection > Display appropriate services");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Phone link on Cart | Credit otpions should be displayed");
		Reporter.log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("4. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("6. Select T-Mobile ONE Plan |T-Mobile ONE plan should be selected");
		Reporter.log("7. Verify ADD-ONS AND EXTRAS hearder under PLAN| ADD-ONS AND EXTRAS header should be displayed");
		Reporter.log(
				"8. Verify List of Add-ons and extras |List of Add-ons and extras should be displayed for T-Mobile ONE");
		Reporter.log("9. Select T-Mobile Essential Plan |T-Mobile Essential plan should be selected");
		Reporter.log(
				"10. Verify List of Add-ons and extras |List of Add-ons and extras should be displayed for T-Mobile Essentials");
		Reporter.log(
				"11. Select One Add on that is available on both T-Mobile ONE and T-Mobile Essential Plan |Selected Add on should be present in Payment section");
		Reporter.log("12. Now Select T-Mobile ONE Plan |Earler Selected Add on should be present in Payment section");
		Reporter.log(
				"13. Select One Add on that is available on Only T-Mobile ONE Plan |Selecte Add on should be present in Payment section");
		Reporter.log(
				"14. Now Select T-Mobile Essentials Plan | Selected T-Mobile ONE  Add on should be removed in Payment section");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US492600 TMNG > TMO Essentials > Plan Selection > Display appropriate
	 * services
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testVerifyDifferentServicesforDifferentPlansForTabletFromEmptyCart(TMNGData tMNGData) {
		Reporter.log("Test Case : US492600 TMNG > TMO Essentials > Plan Selection > Display appropriate services");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Tablet link on Cart | Credit otpions should be displayed");
		Reporter.log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("4. Select any Tablet in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("6. Select T-Mobile ONE Plan |T-Mobile ONE plan should be selected");
		Reporter.log("7. Verify ADD-ONS AND EXTRAS hearder under PLAN| ADD-ONS AND EXTRAS header should be displayed");
		Reporter.log(
				"8. Verify List of Add-ons and extras |List of Add-ons and extras should be displayed for T-Mobile ONE");
		Reporter.log("9. Select T-Mobile Essential Plan |T-Mobile Essential plan should be selected");
		Reporter.log(
				"10. Verify List of Add-ons and extras |List of Add-ons and extras should be displayed for T-Mobile Essentials");
		Reporter.log(
				"11. Select One Add on that is available on both T-Mobile ONE and T-Mobile Essential Plan |Selected Add on should be present in Payment section");
		Reporter.log("12. Now Select T-Mobile ONE Plan |Earler Selected Add on should be present in Payment section");
		Reporter.log(
				"13. Select One Add on that is available on Only T-Mobile ONE Plan |Selecte Add on should be present in Payment section");
		Reporter.log(
				"14. Now Select T-Mobile Essentials Plan | Selected T-Mobile ONE  Add on should be removed in Payment section");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US541063: Integrate AEM component with UI based on device type and plan type
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testAddOnsDataSocAndServiceForEachLine(TMNGData tMNGData) {
		Reporter.log("Test Case :US541063: Integrate AEM component with UI based on device type and plan type");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. Select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Click on Add a Tablet CTA | Tablet Mini PLP should be displayed");
		Reporter.log("6. Select any Tablet | Mini PDP page should be displayed");
		Reporter.log("7. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("8. Click on Add a Wearable CTA | Wearable Mini PLP should be displayed");
		Reporter.log("9. Select any Wearable | Mini PDP page should be displayed");
		Reporter.log("10. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("11. Click on BYOD CTA | BYOD Mini PDP page should be displayed");
		Reporter.log("12. Click on Add to Cart| Cart page should be displayed");
		Reporter.log(
				"13. Verify 'ADD-ONS AND EXTRAS' tile for each line (Tablet, Wearable, Device and BYOD) | 'ADD-ONS AND EXTRAS' tile should be displayed for each line (Tablet, Wearable, Device and BYOD)");
		Reporter.log(
				"14. Verify Data SOC and Service is available for each line  | Ensure that Service and SOC should be displayed for each line");
		Reporter.log(
				"15. Click on Tooltip next to Add on  | tooltip should open and display text description of associated add-on");
		Reporter.log("16. Click outside the Tooltip box  | tooltip should be closed.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySelectingPhone(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyMonthlyPaymentAddonsLineIsNotDisplayed();
		cartPage.clickFirstOptionInAddOnsAndExtras();
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.addAWearabletoCartFromCartPage(tMNGData);
		cartPage.addASimKittoCartFromCartPage();

		cartPage.verifylineLevelPriceBlockDisplayed();
		cartPage.verifyMonthlyPaymentHeaderDisplayed();
		cartPage.verifyMonthlyPaymentLineDisplayed();
		cartPage.verifyMonthlyPaymentLinePriceDisplayed();
		cartPage.verifyMonthlyPaymentDeviceDisplayed();
		cartPage.verifyMonthlyPaymentDevicePriceDisplayed();
		cartPage.verifyMonthlyPaymentDiscountDisplayed();
		cartPage.verifyMonthlyPaymentDiscountPriceDisplayed();
		cartPage.verifyOneTimePaymentDisplayed();
		cartPage.verifySimStaterKitPriceDisplayed();
		cartPage.verifyLineLevelTaxAndShippingDisplayed();
		cartPage.verifyPriceBreakDownUnderOneTimePaymentDisplayed();
		cartPage.verifyPriceBreakDownTotalPriceDisplayedLineLevel();
		cartPage.verifyPriceBreakDownMonthlyPriceDisplayedLineLevel();
		cartPage.verifyPriceBreakDownAutoPayDiscountMessageDisplayed();
		cartPage.verifyPlanBlockForLine();
		cartPage.verifyPlanHeaderForLine();
		cartPage.verifyPlanItemForLine();
		cartPage.verifyToolTipForPlan();
		cartPage.verifyAddOnExtrasHeaderDisplayed();
		cartPage.verifyListOfAddOnsDisplayed();
		cartPage.verifyDataSocDisplayed();
		cartPage.verifyListOfPriceAddOnExtrasHeaderDisplayed();
		cartPage.verifyListOfToolTipsAddOnExtrasHeaderDisplayed();

		cartPage.verifyMonthlyPaymentAddonsLineDisplayed();
		cartPage.verifyMonthlyPaymentAddonPriceDisplayed();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyLineDepositDisplayed();
		cartPage.verifyLineDepositPriceDisplayed();
	}

	/**
	 * US541071: Add-ons modal html integration US541074: Add-ons modal api
	 * integration and transformation
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testRecommendedServicesAndOtherServicesInCartPage(TMNGData tMNGData) {
		Reporter.log("Test Case: US541071: Add-ons modal html integration");
		Reporter.log("Test Case: US541074: Add-ons modal api integration and transformation");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. Select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Verify 'ADD-ONS AND EXTRAS' tile | 'ADD-ONS AND EXTRAS' tile should be displayed");
		Reporter.log(
				"6. Click on 'ADD-ONS AND EXTRAS' service See more options link | Add ons service ZIP Code model should be displayed");
		Reporter.log("7. Enter valid ZIP code | Select Service model should be displayed");
		Reporter.log("8. Verify Recommended Services | Recommended Service should be displayed");
		Reporter.log("9. Click on Other Services | Other Services should be displayed");
		Reporter.log(
				"10. Verify Communication and Data services | Communication and Data services should be displayed");
		Reporter.log("11. Verify Device Protection services | Device Protection services should be displayed");
		Reporter.log(
				"12. Verify Entertainment and More services | Entertainment and More services should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySelectingPhone(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyAddOnExtrasHeaderDisplayed();
		cartPage.clickAddOnsAndExtrasSeeMoreOptionsCTA();
		cartPage.verifyEnterYourZipCodeModalHeaderDisplayed();
		cartPage.enterYourZIPCode(tMNGData.getZipcode());
		cartPage.clickOnNextBtn();
		cartPage.verifySelectServiceModelHeaderDisplayed();
		cartPage.verifyRecommendedServiceHeaderDisplayed();
		cartPage.verifyRecommendedServiceDisplayed();
		cartPage.verifyOtherServicesDisplayed();
		cartPage.clickOnOtherServices();
		cartPage.verifyCommunicationAndDataDisplayed();
		cartPage.verifyDeviceProtectionDisplayed();
		cartPage.verifyEntertaintmentAndMoreDisplayed();

	}

	/**
	 * US541085: Latest value from DCP after api interaction will be defaulted vs
	 * authored value
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testServicePriceUpdatedInCartPage(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US541085: Latest value from DCP after api interaction will be defaulted vs authored value");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Verify 'ADD-ONS AND EXTRAS' tile | 'ADD-ONS AND EXTRAS' tile should be displayed");
		Reporter.log(
				"6. Verify 'ADD-ONS AND EXTRAS' service name and price | 'ADD-ONS AND EXTRAS' service name and price should be displayed");
		Reporter.log(
				"7. Verify 'ADD-ONS AND EXTRAS' Data Soc name and price | 'ADD-ONS AND EXTRAS' Data Soc name and price should be displayed");
		Reporter.log(
				"8. Click on 'ADD-ONS AND EXTRAS' service See more options link | Add ons service ZIP Code model should be displayed");
		Reporter.log("9. Enter valid ZIP code | Select Service model should be displayed");
		Reporter.log("10. Select one service and click on update button | CartPage should be displayed");
		Reporter.log(
				"11. Verify Selected serice and price in 'ADD-ONS AND EXTRAS' container | Selected service and price should be displayed in 'ADD-ONS AND EXTRAS' container");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyAddOnExtrasHeaderDisplayed();
		cartPage.verifyListOfAddOnsDisplayed();
		cartPage.verifyDataSocDisplayed();
		cartPage.verifyListOfPriceAddOnExtrasHeaderDisplayed();
		cartPage.verifyAddOnsAndExtrasSeeMoreOptionsCTADisplayed();
		cartPage.clickAddOnsAndExtrasSeeMoreOptionsCTA();
		cartPage.verifyEnterZipModal();
		cartPage.enterYourZIPCode(tMNGData.getZipcode());
		cartPage.clickNextCTAOnZipCodeModal();
		cartPage.verifyProtectionModal();
		cartPage.selectProtection360PlanOnModal();
		String service = cartPage.getServiceNameForMoreAddOnsService();
		cartPage.clickUpdateCTAonProtectionModal();
		cartPage.verifyCartPageLoaded();
		cartPage.verifySelectedServiceAndPriceForAddOns(service);
	}

	/**
	 * US541069: Services Integration of api upon checkbox selection
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyServiceIntegrationInCartPageFromEmptyCart(TMNGData tMNGData) {
		Reporter.log("Test Case :US541069: Services Integration of api upon checkbox selection");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Phone link on Cart | PLP should  be displayed");
		Reporter.log("4. Select any device in PLP |PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("6. Verify 'ADD-ONS AND EXTRAS' tile | 'ADD-ONS AND EXTRAS' tile should be displayed");
		Reporter.log(
				"7. Click on 'ADD-ONS AND EXTRAS' service See more options link | Add ons service ZIP Code model should be displayed");
		Reporter.log("8. Enter valid ZIP code | Select Service model should be displayed");
		Reporter.log("9. Select any two services | Services should selected and Update CTA should be enabled");
		Reporter.log("10. Click Update CTA | Cart page should be loaded");
		Reporter.log(
				"11. Verify Selected serice and price in 'ADD-ONS AND EXTRAS' container | Selected service and price should be displayed in 'ADD-ONS AND EXTRAS' container");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage emptyCartPage = navigateToEmptyCart(tMNGData);
		emptyCartPage.addAPhonetoCartFromCartPage(tMNGData);

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifyAddOnExtrasHeaderDisplayed();
		cartPage.clickAddOnsAndExtrasSeeMoreOptionsCTA();
		cartPage.verifyEnterYourZipCodeModalHeaderDisplayed();
		cartPage.enterYourZIPCode(tMNGData.getZipcode());
		cartPage.clickOnNextBtn();
		cartPage.verifySelectServiceModelHeaderDisplayed();
		cartPage.verifyOtherServicesDisplayed();
		cartPage.clickOnOtherServices();
		cartPage.selectOtherServicesNameIDOption();
		cartPage.selectOtherServicesVoiceMailToTextOption();
		cartPage.clickOnSelectServiceUpdateBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.verifySelectedServicesOnAddOnContainer();
		cartPage.verifySelectedServicePricesOnAddOnContainer();
	}

	/**
	 * US556490 TEST ONLY - TMO Essentials BYOD Scenario - Remove BYOD line
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyBYODRemoveCTAOnCartFromMainPLP(TMNGData tMNGData) {
		Reporter.log("Test Case : US556490	TEST ONLY - TMO Essentials BYOD Scenario - Remove BYOD line");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application should be launched");
		Reporter.log("2. Click Phones link  | Phones page should be launched");
		Reporter.log("3. Select SIM Card | SIM Card PDP should be displayed");
		Reporter.log("4. Click Add to card | SIM Card should be added to Cart Page.");
		Reporter.log(
				"5. Select Magenta plan |Magenta plan should be present on Plan section of Sim Starter Kit 3-in-1 SIM Kit");
		Reporter.log("6. Click Remove for SIM Card| SIM Card should be removed from Cart Page.");
		Reporter.log("7. Click add a phone | Phones PLP should be displayed ");
		Reporter.log("8. Select SIM Card | SIM Card PDP should be displayed");
		Reporter.log("9. Click Add to card | SIM Card should be added to Cart Page.");
		Reporter.log(
				"10. Select TMobile Essential  plan |T-Mobile Essential Plan should be present on Plan section of SIM Card");
		Reporter.log("11. Click Remove for SIM Card | SIM Card  should be removed from Cart Page.");
		Reporter.log("12. Click add a Tablet | Tablets PLP should be displayed");
		Reporter.log("13. Select Mobile Internet SIM Card |Mobile Internet SIM Card PDP should be diplayed");
		Reporter.log("14. Click Add to card | Mobile Internet SIM Card should be added to Cart Page.");
		Reporter.log(
				"15. Select Magenta plan |Magents Plan should be present on Plan section of Mobile Internet SIM Card");
		Reporter.log(
				"16. Click Remove for Mobile Internet SIM Card| Mobile Internet SIM Card should be removed from Cart Page.");
		Reporter.log("17. Click add a Tablet | Tablets PLP should be displayed");
		Reporter.log("18. Select Mobile Internet SIM Card |Mobile Internet SIM Card PDP should be diplayed");
		Reporter.log("19. Click Add to card | Mobile Internet SIM Card should be added to Cart Page.");
		Reporter.log(
				"20. Select TMobile Essential plan |T-Mobile Essential Plan should be present on Plan section of Mobile Internet SIM Card");
		Reporter.log(
				"21. Click Remove for Mobile Internet SIM Card| Mobile Internet SIM Card should be removed from Cart Page.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByAddingPhonesSSKFromMainPLP(tMNGData);
		cartPage.clickOnMagentaPlan();
		cartPage.verifyMagentaPlanSelected();
		cartPage.clickRemoveCTA();
		cartPage.verifySelectedDeviceIsNotDisplayedOnCartPage();

	}

	/**
	 * US556465 TMO Essentials BYOD Scenario - 3-in-1 SIM Starter kit PDP (Phones)
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyBYODForPhonesOnCartFromURL(TMNGData tMNGData) {
		Reporter.log("Test Case : US556465	TMO Essentials BYOD Scenario - 3-in-1 SIM Starter kit PDP (Phones)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log(
				"1. Navigate to  https://www.t-mobile.com/resources/bring-your-own-phone?icid=WMM_TM_Q417UNAVME_2JDVKVMA5T12838 (Note: Use IMEI 356697082451728)");
		Reporter.log("2. Click Add to cart in BYOD PDP | Sim Starter Kit 3-in-1 SIM Kit added to Cart Page.");
		Reporter.log(
				"3. Verify selected Plan for BYOD on cart |T-Mobile One Plan should be present on Plan section of 3-in-1 SIM Starter Kit");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		BringYourOwnPhonePage bringYourOwnPhonePage = navigateToBYODPage(tMNGData);
		bringYourOwnPhonePage.setIMEINumber(tMNGData.getImeiName());
		bringYourOwnPhonePage.clickCheckThisPhoneCTA();
		bringYourOwnPhonePage.clickBuySIMCTA();
		bringYourOwnPhonePage.switchToWindow();
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.clickOnAddToCartBtn();
		phoneDetailsPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifyMagentaPlanSelected();
		cartPage.verifyPlanItemForLine();
		cartPage.verifyPlanItemForLineIsMagenta();

	}

	/**
	 * US556492 TEST ONLY - TMO Essentials BYOD Scenario - Duplicate BYOD Line
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyBYODDuplicateCTAOnCartFromMainPLP(TMNGData tMNGData) {
		Reporter.log("Test Case : US556492  TEST ONLY - TMO Essentials BYOD Scenario - Duplicate BYOD Line");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application should be launched");
		Reporter.log("2. Click Phones link  | Phones page should be launched");
		Reporter.log("3. Select 3-in-1-sim-starter-kit | 3-in-1-sim-starter-kit main PDP should be displayed");
		Reporter.log("4. Click Add to cart in BYOD PDP | Sim Starter Kit 3-in-1 SIM Kit added to Cart Page.");
		Reporter.log("5. Select Magenta plan | Magent plan should be selected");
		Reporter.log("6. Verify Plan Item for Line is Magenta | Plan Item Magenta should be selected ");
		Reporter.log("7. Click Duplicate link | Duplicate Lines count should be 2 ");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByAddingPhonesSSKFromMainPLP(tMNGData);
		cartPage.clickOnMagentaPlan();
		cartPage.verifyMagentaPlanSelected();
		cartPage.verifyPlanItemForLineIsMagenta();
		cartPage.clickDuplicateCTA();
		cartPage.verifyDuplicateLinesCountOnCart(2);

	}

	/**
	 * US566758: TMO Multi-Plan - Plan Container displays Magenta+ Plan US566474:
	 * TMNG - TMO Multi-Plan - Display Magenta+ Plan
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyMagentaPlusPlanInCartPageFromMainPLP(TMNGData tMNGData) {
		Reporter.log("Test Case :US566758: TMO Multi-Plan - Plan Container displays Magenta+ Plan");
		Reporter.log("Test Case :US566474: TMNG - TMO Multi-Plan - Display Magenta+ Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application should be Launched");
		Reporter.log("2. Click phone link | Main PLP should be displayed");
		Reporter.log("3. Select any device from main PLP  | Device main PDP should be displayed");
		Reporter.log("4. Click Add to cart in PDP | Cart Page should be displayed.");
		Reporter.log(
				"5. Verify Essentials/ Magenta plan and Magenta Plus Plan  In Plan secion in cart | Essentials/ Magenta and Magenta Plus plan should  be displayed");
		Reporter.log(
				"6. Select Magenta Plus Plan | Magenta Plus plan should  be selected and Plan should be displayed on Plan tile of Line in Cart");
		Reporter.log(
				"7. Verify  tool tip for Magenta Plus Plan in Plan tile  | Tool-tip should be displayed for Magenta Plus Plan in plan tile of line");
		Reporter.log(
				"8. Click  tool tip for Magenta Plus Plan in Plan tile  | Tool-tip box should be displayed with Magenta Plus Plan");
		Reporter.log(
				"9. Verify  tool tip description for Magenta Plus Plan in Plan tile  | Tool-tip Text should be displayed for Magenta Plus Plan");
		Reporter.log(
				"10. Verify ADD-ONS AND EXTRAS section for  Magenta Plus| ADD-ONS AND EXTRAS header should be displayed");
		Reporter.log(
				"11. Verify List of Add-ons and extras for Magenta Plus plan |List of Add-ons and extras should be displayed And add ons should be Protection <360>");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySelectingPhone(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifyEssentialMagentaAndMagentaPlusPlanSectionDisplayed();
		cartPage.clickOnMagentaPlusPlan();
		cartPage.verifyPlanItemForLineIsMagentaPlus();
		cartPage.verifyToolTipForPlan();
		cartPage.clickToolTipForPlan();
		cartPage.verifyPlanDescriptionTextOnModal();
		cartPage.clickOutsideToolTipForPlan();
		cartPage.verifyToolTipBlockClosed();
		cartPage.verifyaddOnsAndExtraSection();
		cartPage.verifyAddOnExtrasHeaderDisplayed();
		cartPage.verifyDefaultAddOnsForMegentaPlan();

	}

	/**
	 * US566758: TMO Multi-Plan - Plan Container displays Magenta+ Plan US566474:
	 * TMNG - TMO Multi-Plan - Display Magenta+ Plan
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyMagentaPlusPlanInCartPageFromEmptyCart(TMNGData tMNGData) {
		Reporter.log("Test Case :US566758: TMO Multi-Plan - Plan Container displays Magenta+ Plan");
		Reporter.log("Test Case :US566474: TMNG - TMO Multi-Plan - Display Magenta+ Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Phone link on Cart | Credit options modal should  be displayed");
		Reporter.log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("4. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log(
				"6. Verify Essentials / Magenta plan and Magenta Plus Plan  In Plan secion in cart | Essentials/ Magenta and Magenta Plus plan should  be displayed");
		Reporter.log(
				"7. Select Magenta Plus Plan | Magenta Plus plan should  be selected and Plan should be displayed on Plan tile of Line in Cart");
		Reporter.log(
				"8. Verify  tool tip for Magenta Plus Plan in Plan tile  | Tool-tip should be displayed for Magenta Plus Plan in plan tile of line");
		Reporter.log(
				"9. Click  tool tip for Magenta Plus Plan in Plan tile  | Tool-tip box should be displayed with Magenta Plus Plan");
		Reporter.log(
				"10. Verify  tool tip description for Magenta Plus Plan in Plan tile  | Tool-tip Text should be displayed for Magenta Plus Plan");
		Reporter.log(
				"11. Verify ADD-ONS AND EXTRAS section for  Magenta Plus | ADD-ONS AND EXTRAS header should be displayed");
		Reporter.log(
				"12. Verify List of Add-ons and extras for Magenta Plus plan |List of Add-ons and extras should be displayed And add ons should be Protection <360>");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage emptyCartPage = navigateToEmptyCart(tMNGData);
		emptyCartPage.addAPhonetoCartFromCartPage(tMNGData);
		emptyCartPage.verifyCartPageLoaded();
		emptyCartPage.verifyEssentialMagentaAndMagentaPlusPlanSectionDisplayed();
		emptyCartPage.clickOnMagentaPlusPlan();
		emptyCartPage.verifyPlanItemForLineIsMagentaPlus();
		emptyCartPage.verifyToolTipForPlan();
		emptyCartPage.clickToolTipForPlan();
		emptyCartPage.verifyPlanDescriptionTextOnModal();
		emptyCartPage.clickOutsideToolTipForPlan();
		emptyCartPage.verifyToolTipBlockClosed();
		emptyCartPage.verifyaddOnsAndExtraSection();
		emptyCartPage.verifyAddOnExtrasHeaderDisplayed();
		emptyCartPage.verifyDefaultAddOnsForMegentaPlan();

	}

	/**
	 * US566474: TMNG - TMO Multi-Plan - Display Magenta+ Plan
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyMagentaPlusPlanInCartPageForWearable(TMNGData tMNGData) {
		Reporter.log("Test Case :US566474: TMNG - TMO Multi-Plan - Display Magenta+ Plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Wearable link on Cart | Credit options modal should  be displayed");
		Reporter.log("3. Select Awesome credit options and click Done | Wearable Mini PLP should  be displayed");
		Reporter.log("4. Select any wearable in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log(
				"6. Verify Magenta Plus Plan  In Plan secion in cart | Magenta Plus plan should  be in disabled mode");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingWearableFromMainPLP(tMNGData);
		cartPage.verifyMagentaPlusPlanSectionDisabled();
	}

	/**
	 * US577492: TMO Multi-Plan - Order of lines displayed on the Order Details Page
	 * to mirror Cart C518573 : Validate Line order is in same numerical order as
	 * they were added to the cart (Order detail ) - Mix cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAddedDevicesLineOrderIsSameAsCartPage(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US577492: TMO Multi-Plan - Order of lines displayed on the Order Details Page to mirror Cart");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application should be Launched");
		Reporter.log("2. Click phone link | Main PLP should be displayed");
		Reporter.log("3. Select any device from PLP  | Device PDP should be displayed");
		Reporter.log("4. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log("5. Select 'Add a tablet' from cart page | Tablets plp Page should be displayed.");
		Reporter.log("6. Select any tablet device from PLP  | Tablet device PDP page should be displayed");
		Reporter.log("7. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log("8. Select 'Add a wearable' from cart page | Watches plp Page should be displayed.");
		Reporter.log("9. Select any device from PLP  | Device PDP page should be displayed");
		Reporter.log("10. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log("11. Select 'Bring your device' from cart page | PDP should be displayed");
		Reporter.log("12. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log("13. Select 'Add an accessory' from cart page | Accessory plp Page should be displayed.");
		Reporter.log("14. Select any accessory device from PLP  | Accessory PDP page should be displayed");
		Reporter.log("15. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log("16. Select 'Add an accessory' from cart page | Accessory plp Page should be displayed.");
		Reporter.log("17. Select any accessory device from PLP  | Accessory PDP page should be displayed");
		Reporter.log("18. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log(
				"19. Verify order of added devices on cartPage | Devices should be displayed according to order of added devices");
		Reporter.log(
				"20. Click on View Order Details | Ensure that devices should be displayed according to order and line number against the cartPage");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.addAWearabletoCartFromCartPage(tMNGData);
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
		cartPage.addSecondAccessorytoCartFromCartPage(tMNGData);
		cartPage.verifyDeviceListedInSameSequenceAsAddedOnCartPage(tMNGData.getDeviceName(), tMNGData.getTabletName(),
				tMNGData.getWatchName(), tMNGData.getSecondDeviceName());
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyDeviceListedInSameSequenceAsAddedOnViewOrderModal(tMNGData.getDeviceName(),
				tMNGData.getTabletName(), tMNGData.getWatchName(), tMNGData.getSecondDeviceName());

	}

	/**
	 * US577492: TMO Multi-Plan - Order of lines displayed on the Order Details Page
	 * to mirror Cart C518575 : Validate that accessory will be displayed after the
	 * line in mix cart ( Order detail ) -Mix cart C518576 : Validate the order when
	 * products are in Essentials and Magenta Plus plan
	 * 
	 * @param data
	 * @param tMNGData Note: Created script for only Magenta Plus Plan
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testAccessoryDevicesDisplayedAfterVoiceLines(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US577492: TMO Multi-Plan - Order of lines displayed on the Order Details Page to mirror Cart");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application should be Launched");
		Reporter.log("2. Click phone link | Main PLP should be displayed");
		Reporter.log("3. Select any device from PLP  | Device PDP should be displayed");
		Reporter.log("4. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log(
				"5. Select Magenta Plus Plan | Magenta Plus plan should  be selected and Plan should be displayed on Plan tile of Line in Cart");
		Reporter.log("6. Select 'Add a phone' from cart page | Phones plp Page should be displayed.");
		Reporter.log("7. Select any Pre-Order device from PLP  | Device PDP should be displayed");
		Reporter.log("8. Click Pre-Order button in PDP | Cart Page should be displayed.");

		Reporter.log("9. Select 'Add an accessory' from cart page | Accessory plp Page should be displayed.");
		Reporter.log("10. Select any accessory device from PLP  | Accessory PDP page should be displayed");
		Reporter.log("11. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log("12. Select 'Add a tablet' from cart page | Tablets plp Page should be displayed.");
		Reporter.log("13. Select any tablet device from PLP  | Tablet device PDP page should be displayed");
		Reporter.log("14. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log("15. Select 'Add a wearable' from cart page | Watches plp Page should be displayed.");
		Reporter.log("16. Select any Pre-Order device from PLP  | Device PDP page should be displayed");
		Reporter.log("17. Click Pre-Order button in PDP | Cart Page should be displayed.");

		Reporter.log("18. Select 'Add an accessory' from cart page | Accessory plp Page should be displayed.");
		Reporter.log("19. Select any accessory device from PLP  | Accessory PDP page should be displayed");
		Reporter.log("20. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log(
				"21. Click on View Order Details link | Accessory devices should be displayed after voice or tablets or wearable devices");
		Reporter.log(
				"22. Verify accessory devices line order | Ensure that accessory devices should be displayed according to the cartPage");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.verifyEssentialMagentaAndMagentaPlusPlanSectionDisplayed();
		cartPage.clickOnMagentaPlusPlan();
		cartPage.verifyPlanItemForLineIsMagentaPlus();

		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.addAWearabletoCartFromCartPage(tMNGData);
		cartPage.addSecondAccessorytoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();

		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyAccessoryIsLastOnOrderDetailsModal(tMNGData.getAccessoryName());
		cartPage.verifyAccessoryDevicesLineOrder(tMNGData.getAccessoryName(), tMNGData.getSecondAccessoryName());
	}

	/**
	 * US577492: TMO Multi-Plan - Order of lines displayed on the Order Details Page
	 * to mirror Cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	/*
	 * @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT
	 * }) public void
	 * testVerifyMagentaPlusPlanInOrderDetailedPageFromMainPLP(TMNGData tMNGData) {
	 * Reporter.
	 * log("Test Case :US577492: TMO Multi-Plan - Order of lines displayed on the Order Details Page to mirror Cart"
	 * ); Reporter.log("================================");
	 * Reporter.log("Test Steps | Expected Results:");
	 * Reporter.log("1. Launch TMO  | Application should be Launched");
	 * Reporter.log("2. Click phone link | Main PLP should be displayed"); Reporter.
	 * log("3. Select any device from main PLP  | Device main PDP should be displayed"
	 * );
	 * Reporter.log("4. Click Add to cart in PDP | Cart Page should be displayed.");
	 * Reporter.
	 * log("5. Add one more device, SIM kit and Accessory to cart | Cart should contain two devices, SIM KIT and accessory."
	 * ); Reporter.
	 * log("6. Select Magenta Plus Plan | Magenta Plus plan should  be selected and Plan should be displayed on Plan tile of Line in Cart"
	 * ); Reporter.
	 * log("7. Click View Order detail CTA  | Review order modal should be displayed"
	 * ); Reporter.
	 * log("8. Verify  plans for each line on order detail modal  | Plan should be magenta plus for all the lines on cart"
	 * ); Reporter.
	 * log("9. Verify lines on Order Details Page | Lines should have same numerical order they were added to the cart"
	 * ); Reporter.
	 * log("10. Verify pre-order and backorder SKUs on Order Details | Pre-order and backorder SKUs should display in the same numerical order they were added to the cart"
	 * ); Reporter.
	 * log("11. Verify if accessories are added to the order| Accessories should dispalay after the lines on the Order Details page"
	 * );
	 * 
	 * Reporter.log("================================");
	 * Reporter.log("Actual Output:");
	 * 
	 * navigateToCartPageBySelectingPhone(tMNGData); CartPage cartPage = new
	 * CartPage(getDriver()); cartPage.verifyCartPageLoaded();
	 * cartPage.addAPhonetoCartFromCartPage(tMNGData);
	 * cartPage.addATablettoCartFromCartPage(tMNGData);
	 * cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
	 * cartPage.verifyCartPageLoaded();
	 * cartPage.verifyEssentialMagentaAndMagentaPlusPlanSectionDisplayed();
	 * cartPage.clickOnMagentaPlusPlan();
	 * cartPage.verifyDeviceListedInSameSequenceAsAddedOnCartPage(tMNGData.
	 * getDeviceName(), tMNGData.getSecondDeviceName(), tMNGData.getTabletName());
	 * cartPage.verifyMagentaPlusPlanSelected();
	 * cartPage.clickOnViewOrderDetailsLink();
	 * cartPage.verifyViewOrderDetailsModel();
	 * cartPage.verifyPlanNameForOrderDetailsModalMagentaPlus();
	 * cartPage.verifyDeviceListedInSameSequenceAsAddedOnViewOrderModal(tMNGData.
	 * getDeviceName(), tMNGData.getSecondDeviceName(), tMNGData.getTabletName());
	 * cartPage.verifyAccessoryIsLastOnOrderDetailsModal(tMNGData.getAccessoryName()
	 * );
	 * 
	 * }
	 * 
	 *//**
		 * US577492: TMO Multi-Plan - Order of lines displayed on the Order Details Page
		 * to mirror Cart
		 * 
		 * @param data
		 * @param tMNGData
		 *//*
			 * @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT
			 * }) public void
			 * testVerifyMagentaPlusPlanInOrderDetailedPageFromEmptyCart(TMNGData tMNGData)
			 * { Reporter.log(
			 * "Test Case :US577492: TMO Multi-Plan - Order of lines displayed on the Order Details Page to mirror Cart"
			 * ); Reporter.log("================================");
			 * Reporter.log("Test Steps | Expected Results:"); Reporter.
			 * log("1. Launch TMO empty cart  | empty Cart page should be displayed");
			 * Reporter.
			 * log("2. Click  Add a Phone link on Cart | Credit options modal should  be displayed"
			 * ); Reporter.
			 * log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed"
			 * ); Reporter.
			 * log("4. Select any device in mini PLP | Mini PDP Page should  be displayed");
			 * Reporter.
			 * log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
			 * Reporter.log(
			 * "6. Add one more device, SIM kit and Accessory to cart | Cart should contain two devices, SIM KIT and accessory."
			 * ); Reporter.log(
			 * "7. Select Magenta Plus Plan | Magenta Plus plan should  be selected and Plan should be displayed on Plan tile of Line in Cart"
			 * ); Reporter.
			 * log("8. Click View Order detail CTA  | Review order modal should be displayed"
			 * ); Reporter.log(
			 * "9. Verify  plans for each line on order detail modal  | Plan should be magenta plus for all the lines on cart"
			 * ); Reporter.log(
			 * "9. Verify lines on Order Details Page | Lines should have same numerical order they were added to the cart"
			 * ); Reporter.log(
			 * "10. Verify pre-order and backorder SKUs on Order Details | Pre-order and backorder SKUs should display in the same numerical order they were added to the cart"
			 * ); Reporter.log(
			 * "11. Verify if accessories are added to the order| Accessories should dispalay after the lines on the Order Details page"
			 * );
			 * 
			 * Reporter.log("================================");
			 * Reporter.log("Actual Output:");
			 * 
			 * CartPage cartPage = navigateToEmptyCart(tMNGData);
			 * cartPage.clickOnAddAPhoneLinkOnCart(); PlpPage plpPage = new
			 * PlpPage(getDriver()); plpPage.verifyPhonesPlpPageLoaded();
			 * plpPage.clickDeviceWithAvailability(tMNGData.getDeviceName()); PdpPage
			 * pdpPage = new PdpPage(getDriver()); pdpPage.verifyPhonePdpPageLoaded();
			 * pdpPage.clickOnAddToCartBtn(); cartPage.verifyCartPageLoaded();
			 * cartPage.addAPhonetoCartFromCartPage(tMNGData);
			 * cartPage.addATablettoCartFromCartPage(tMNGData);
			 * cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
			 * cartPage.verifyCartPageLoaded();
			 * cartPage.verifyEssentialMagentaAndMagentaPlusPlanSectionDisplayed();
			 * cartPage.clickOnMagentaPlusPlan();
			 * cartPage.verifyDeviceListedInSameSequenceAsAddedOnCartPage(tMNGData.
			 * getDeviceName(), tMNGData.getSecondDeviceName(), tMNGData.getTabletName());
			 * cartPage.verifyMagentaPlusPlanSelected();
			 * cartPage.clickOnViewOrderDetailsLink();
			 * cartPage.verifyViewOrderDetailsModel();
			 * cartPage.verifyPlanNameForOrderDetailsModalMagentaPlus();
			 * cartPage.verifyDeviceListedInSameSequenceAsAddedOnViewOrderModal(tMNGData.
			 * getDeviceName(), tMNGData.getSecondDeviceName(), tMNGData.getTabletName());
			 * 
			 * 
			 * }
			 */

	/**
	 * US576035: [TEST ONLY} TMNG Essentials - Calculate Promotion discount
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyPromotionDiscountForLineandTabletInCart(TMNGData tMNGData) {
		Reporter.log("Test Case :US576035: [TEST ONLY} TMNG Essentials - Calculate Promotion discount");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Phone link on Cart | Credit options modal should  be displayed");
		Reporter.log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("4. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("6. Click  Add a Tablet link on Cart | Tablet Mini PLP should  be displayed");
		Reporter.log("7. Select any tablet in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("8. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("9. Verify discount  Hybrid account discount. | Discount price should be applied on price.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.addAPhonetoCartFromCartPage(tMNGData);

		cartPage.verifyHybridDiscountDisplayed();
		cartPage.verifyHybridDiscountPriceDisplayed();

	}

	/**
	 * US545036 : TMNG - Enable parameter trigger for default rate plan selection in
	 * empty cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testVerifyemptyCartNavigatingFromRatePlanPage(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US545036 : TMNG - Enable parameter trigger for default rate plan selection in empty cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click rate plan | Rate plan page should be displayed ");
		Reporter.log("3. Verify list of plan in rate plan page | Different available plans should  be displayed");
		Reporter.log("4. Select any Plan click start shoppin | Empty Cart Page should  be displayed");
		Reporter.log("5. Verify Your Plan section on Cart | Your Plan Header should be displayed");
		Reporter.log("6. Verify Magenta  Plan section on Cart | Magenta Plan Header should be displayed");
		Reporter.log(
				"7. Verify Magenta Plan Description on Cart | Magenta plan Dec - 'Unlimited talk, text, and 4G LTE data, Netflix On us with 2+ lines, and more benefits.' should be displayed");
		Reporter.log(
				"8. Verify Magenta Plan Cost on Cart | Magenta Cost - '1 line $70/mo. | 2 lines $120/mo. | 3 lines $140/mo. | 4 lines $160/mo' should be displayed");
		Reporter.log(
				"9. Verify Magenta section bottom text on Cart | ' taxes and fees included' text should be displayed");
		Reporter.log(
				"10. Verify T-Mobile essentials section on Cart | T-Mobile essentials Plan Header should be displayed");
		Reporter.log(
				"11. Verify T-Mobile essentials Plan Description on Cart | T-Mobile essentials plan Dec - 'The basics: Unlimited talk, text, and 4G LTE data.' should be displayed");
		Reporter.log(
				"12. Verify T-Mobile essentials Plan Cost on Cart | T-Mobile essentials Cost - '1 line $60/mo. | 2 lines $90/mo. | 3 lines $105/mo. | 4 lines $120/mo' should be displayed");
		Reporter.log(
				"13. Verify T-Mobile essentials section bottom text on Cart | 'Plus taxes and fees' text should be displayed");

		Reporter.log("14. Verify Magenta Plus  Plan section on Cart | Magenta Plus Plan Header should be displayed");
		Reporter.log(
				"15. Verify Magenta Plus plan Description on Cart | Magenta plus plan Dec - 'Unlimited talk, text, and 4G LTE data, Netflix On us with 2+ lines, and more benefits.' should be displayed");
		Reporter.log(
				"16. Verify Magenta plus Plan Cost on Cart | Magenta plus planCost - '1 line $85/ mo. | '2 lines $145/mo. | 3 lines $165/mo. | 4 lines $185/mo.' should be displayed");
		Reporter.log(
				"17. Verify Magenta plus section bottom text on Cart | ' taxes and fees included' text should be displayed");
		Reporter.log(
				"18. Verify eligible product categories CTA's| Add a Phone, Add A Tablet, Add A wearable, Add a Accessory, and BYOD CTA should be displayed on bottom.");
		Reporter.log(
				"19. Create toggle for the rate plan containers in the empty cart. | the rate plan containers in the empty cart toggle should be verified.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US583226 : TMNG Multiplan : Placeholder - Disable Magenta plus tile for
	 * Tablet and wearable
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyMagentaPlusPlanForTabletDevices(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US583226 : TMNG Multiplan : Placeholder - Disable Magenta plus tile for Tablet and wearable");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click phone link | Main PLP should be displayed");
		Reporter.log("3. Click on Tablets & Devices Tab  | Tablet devices should be displayed");
		Reporter.log("4. Select Tablet device  | PDP page should be displayed");
		Reporter.log("5. Click on Add to cart button | Cart page should be displayed ");
		Reporter.log("6. Verify Tablet device | Tablet device should be added in cart page ");
		Reporter.log("7. Verify Magenta Plus plan | Magenta Plus plan should  be disabled");
		Reporter.log("8. Verify Pricing details | Pricing details should  be hidden");
		Reporter.log("9. Verify Tax related text | Tax related text should be hidden");
		Reporter.log("10. Verify Authorable error message | Authorable error message should be disabled");
		Reporter.log(
				"11. Click on Add a phone link from Add more lines or devices tab | Mini PLP page should be displayed ");
		Reporter.log("12. Select any device from mini plp page | Mini PDP page should be displayed");
		Reporter.log("13. Click on Add to cart button | Cart page should be displayed ");
		Reporter.log("14. Verify handset device | Handset device should be added in cart page ");
		Reporter.log("15. Verify Magenta Plus plan | Magenta Plus plan should  be enabled");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingTabletFromMainPLP(tMNGData);
		cartPage.verifyMagentaPlusPlanSectionDisabled();
		cartPage.verifymagentaPlusPlanPricingDetailsNotDisplayed();
		cartPage.verifymagentaPlusPlanTaxesTextNotDisplayed();
		cartPage.verifyMagentaPlusPlanErrorMessageDisabled();
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.verifyMagentaPlusPlanEnabled();
	}

	/**
	 * US583226 : TMNG Multiplan : Placeholder - Disable Magenta plus tile for
	 * Tablet and wearable
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyMagentaPlusPlanForWearableDevices(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US583226 : TMNG Multiplan : Placeholder - Disable Magenta plus tile for Tablet and wearable");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click phone link | Main PLP should be displayed");
		Reporter.log("3. Click on Tablets & Devices Tab  | Tablet devices should be displayed");
		Reporter.log("4. Select Wearable device(Ex: Watches) | PDP page should be displayed");
		Reporter.log("5. Click on Add to cart button | Cart page should be displayed ");
		Reporter.log("6. Verify Wearable device | Wearable device should be added in cart page ");
		Reporter.log("7. Verify Magenta Plus plan | Magenta Plus plan should  be disabled");
		Reporter.log("8. Verify Pricing details | Pricing details should  be hidden");
		Reporter.log("9. Verify Tax related text | Tax related text should be hidden");
		Reporter.log("10. Verify Authorable error message | Authorable error message should be disabled");
		Reporter.log(
				"11. Click on Add a phone link from Add more lines or devices tab | Mini PLP page should be displayed ");
		Reporter.log("12. Select any device from mini plp page | Mini PDP page should be displayed");
		Reporter.log("13. Click on Add to cart button | Cart page should be displayed ");
		Reporter.log("14. Verify handset device | Handset device should be added in cart page ");
		Reporter.log("15. Verify Magenta Plus plan | Magenta Plus plan should  be enabled");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingWearableFromMainPLP(tMNGData);
		cartPage.verifyMagentaPlusPlanSectionDisabled();
		cartPage.verifymagentaPlusPlanPricingDetailsNotDisplayed();
		cartPage.verifymagentaPlusPlanTaxesTextNotDisplayed();
		cartPage.verifyMagentaPlusPlanErrorMessageDisabled();
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.verifyMagentaPlusPlanEnabled();
	}

	/**
	 * US545115 : TMNG: Store customer rate plan intent from empty cart to cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUserNavigateToEmptyCartAndAddDeviceWithEssentialsPlan(TMNGData tMNGData) {
		Reporter.log("Test Case : US545115 : TMNG: Store customer rate plan intent from empty cart to cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on plans link | Plans page should be displayed");
		Reporter.log("3. Select plan and click on See Essentials plan button  | Start shopping should be displayed");
		Reporter.log("4. Click on Start shopping button  | Empty cartpage should be displayed");
		Reporter.log(
				"5. Verify Essentials plan in empty cart page | Selected Essentials plan should be selected in empty cart page");
		Reporter.log(
				"6. Click on Add a phone link from Add more lines or devices tab | Mini PLP page should be displayed ");
		Reporter.log("7. Select any device from mini plp page | Mini PDP page should be displayed");
		Reporter.log("8. Click on Add to cart button | Cart page should be displayed ");
		Reporter.log("9. Verify Handset device | Handset device should be added in cart page");
		Reporter.log("10. Verify Essentials plan in cart page| Essentials plan should be selected in cart page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US545115 : TMNG: Store customer rate plan intent from empty cart to cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUserSwitchesEssentialsPlanToMagentaPlanFromEmptyCart(TMNGData tMNGData) {
		Reporter.log("Test Case : US545115 : TMNG: Store customer rate plan intent from empty cart to cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on plans link | Plans page should be displayed");
		Reporter.log("3. Select plan and click on See Essentials plan button  | Start shopping should be displayed");
		Reporter.log("4. Click on Start shopping button  | Empty cartpage should be displayed");
		Reporter.log(
				"5. Verify Essentials plan in empty cart page | Selected Essentials plan should be selected in empty cart page");
		Reporter.log(
				"6. Switch Essentials plan to Magenta or Magenta+ plan | Magenta or Magenta+ plan should be selected in empty cart page");
		Reporter.log(
				"7. Click on Add a phone link from Add more lines or devices tab | Mini PLP page should be displayed ");
		Reporter.log("8. Select any device from mini plp page | Mini PDP page should be displayed");
		Reporter.log("9. Click on Add to cart button | Cart page should be displayed ");
		Reporter.log("10. Verify Handset device | Handset device should be added in cart page");
		Reporter.log(
				"11. Verify Magenta or Magenta+ plan in cart page| Magenta or Magenta+ plan should be selected in cart page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US545115 : TMNG: Store customer rate plan intent from empty cart to cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUserNavigateToEmptyCartAndAddDeviceWithMagentaPlan(TMNGData tMNGData) {
		Reporter.log("Test Case : US545115 : TMNG: Store customer rate plan intent from empty cart to cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on plans link | Plans page should be displayed");
		Reporter.log("3. Select plan and click on See Magenta plan button  | Start shopping should be displayed");
		Reporter.log("4. Click on Start shopping button  | Empty cartpage should be displayed");
		Reporter.log(
				"5. Verify Magenta plan in empty cart page | Selected Magenta plan should be selected in empty cart page");
		Reporter.log(
				"6. Click on Add a phone link from Add more lines or devices tab | Mini PLP page should be displayed ");
		Reporter.log("7. Select any device from mini plp page | Mini PDP page should be displayed");
		Reporter.log("8. Click on Add to cart button | Cart page should be displayed ");
		Reporter.log("9. Verify Handset device | Handset device should be added in cart page");
		Reporter.log("10. Verify Magenta plan in cart page| Magenta plan should be selected in cart page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US545115 : TMNG: Store customer rate plan intent from empty cart to cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUserSwitchesMagentaPlanToMagentaPlusFromEmptyCart(TMNGData tMNGData) {
		Reporter.log("Test Case : US545115 : TMNG: Store customer rate plan intent from empty cart to cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on plans link | Plans page should be displayed");
		Reporter.log("3. Select plan and click on See Magenta plan button  | Start shopping should be displayed");
		Reporter.log("4. Click on Start shopping button  | Empty cartpage should be displayed");
		Reporter.log(
				"5. Verify Magenta plan in empty cart page | Selected Magenta plan should be selected in empty cart page");
		Reporter.log("6. Switch Magenta plan to Magenta+ plan | Magenta+ plan should be selected in empty cart page");
		Reporter.log(
				"7. Click on Add a phone link from Add more lines or devices tab | Mini PLP page should be displayed ");
		Reporter.log("8. Select any device from mini plp page | Mini PDP page should be displayed");
		Reporter.log("9. Click on Add to cart button | Cart page should be displayed ");
		Reporter.log("10. Verify Handset device | Handset device should be added in cart page");
		Reporter.log("11. Verify Magenta+ plan in cart page| Magenta+ plan should be selected in cart page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US545115 : TMNG: Store customer rate plan intent from empty cart to cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUserNavigateToEmptyCartAndAddDeviceWithMagentaPlusPlan(TMNGData tMNGData) {
		Reporter.log("Test Case : US545115 : TMNG: Store customer rate plan intent from empty cart to cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on plans link | Plans page should be displayed");
		Reporter.log("3. Select plan and click on See Magenta Plus plan button  | Start shopping should be displayed");
		Reporter.log("4. Click on Start shopping button  | Empty cartpage should be displayed");
		Reporter.log(
				"5. Verify Magenta Plus plan in empty cart page | Selected Magenta Plus plan should be selected in empty cart page");
		Reporter.log(
				"6. Click on Add a phone link from Add more lines or devices tab | Mini PLP page should be displayed ");
		Reporter.log("7. Select any device from mini plp page | Mini PDP page should be displayed");
		Reporter.log("8. Click on Add to cart button | Cart page should be displayed ");
		Reporter.log("9. Verify Handset device | Handset device should be added in cart page");
		Reporter.log("10. Verify Magenta Plus plan in cart page| Magenta Plus plan should be selected in cart page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US545115 : TMNG: Store customer rate plan intent from empty cart to cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUserSwitchesMagentaPlusPlanToEssentialsPlanFromEmptyCart(TMNGData tMNGData) {
		Reporter.log("Test Case : US545115 : TMNG: Store customer rate plan intent from empty cart to cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on plans link | Plans page should be displayed");
		Reporter.log("3. Select plan and click on See Magenta Plus plan button  | Start shopping should be displayed");
		Reporter.log("4. Click on Start shopping button  | Empty cartpage should be displayed");
		Reporter.log(
				"5. Verify Magenta+ plan in empty cart page | Selected Magenta Plus plan should be selected in empty cart page");
		Reporter.log(
				"6. Switch Magenta+ plan to Essentials plan | Essentials plan should be selected in empty cart page");
		Reporter.log(
				"7. Click on Add a phone link from Add more lines or devices tab | Mini PLP page should be displayed ");
		Reporter.log("8. Select any device from mini plp page | Mini PDP page should be displayed");
		Reporter.log("9. Click on Add to cart button | Cart page should be displayed ");
		Reporter.log("10. Verify Handset device | Handset device should be added in cart page");
		Reporter.log("11. Verify Essentials plan in cart page| Essentials plan should be selected in cart page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * TA1904780:TMNG/QATPRD : With Essential plans selected, if user removes all
	 * products from cart and adds a new product(from empty cart), the default plan
	 * is not set to T-Mobile One DE239312 :TMNG/QATPRD : With Essential plans
	 * selected, if user removes all products from cart and adds a new product(from
	 * empty cart), the default plan is not set to T-Mobile One
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testDefaultPlanSetToMegantaPlanForDevices(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :TA1904780:TMNG/QATPRD : With Essential plans selected, if user removes all products from cart and adds a new product(from empty cart), the default plan is not set to T-Mobile One");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
		Reporter.log("3. Select Device | PDP page should be displayed");
		Reporter.log("4. Click on Add to cart button | Cartpage should be displayed");
		Reporter.log("5. Verify device | Device should be added to cart");
		Reporter.log("6. Verify Magenta plan is selected | Magenta plan is should be selected");
		Reporter.log("7. Change Magenta plan to Essentials plan | Essentials plan is should be selected");
		Reporter.log("8. Click on Device Remove link | Empty Cart should be displayed");
		Reporter.log("9. Click on Add a phone link from empty cart | Mini PLP page should be displayed");
		Reporter.log("10. Select device from mini plp page | Mini PDP page should be displayed");
		Reporter.log("11. Click on Add to cart button | Cartpage should be displayed");
		Reporter.log("12. Verify device | Device should be added to cart");
		Reporter.log("13. Verify Magenta plan is selected | Magenta plan is should be selected");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySelectingPhone(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyDeviceInCart();
		cartPage.verifyMagentaPlanSelected();
		cartPage.clickOnTMobileEssentialsPlan();
		cartPage.verifyTMobileEssentialsPlanSelected();
		cartPage.clickRemoveCTA();
		cartPage.verifyEmptyCart();
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.verifyDeviceInCart();
		cartPage.verifyMagentaPlanSelected();
	}

	/**
	 * TA1904782:TMNG: QATPRD(qlab03): Line Price for TMO One and Essentials plan
	 * should display plan price without $5 auto pay on Line container DE239502
	 * :TMNG: QATPRD(qlab03): Line Price for TMO One and Essentials plan should
	 * display plan price without $5 auto pay on Line container
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testLinePriceWhenUserSwitchToMagentaPlanToEssentialsPlan(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :TA1904782:TMNG: QATPRD(qlab03): Line Price for TMO One and Essentials plan should display plan price without $5 auto pay on Line container");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
		Reporter.log("3. Select Device | PDP page should be displayed");
		Reporter.log("4. Click on Add to cart button | Cartpage should be displayed");
		Reporter.log("5. Verify device | Device should be added to cart");
		Reporter.log("6. Verify Magenta plan is selected | Magenta plan is should be selected");
		Reporter.log("7. Save Line price in V1 | LinePrice should be saved in V1");
		Reporter.log("8. Switch Magenta plan to Essentials plan | Essentials plan is should be selected");
		Reporter.log("9. Now Save Line price in V2 | LinePrice should be saved in V2");
		Reporter.log("10. Compare the Line prices(V1 and V2) | Line prices should not be equal");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySelectingPhone(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyDeviceInCart();
		cartPage.verifyMagentaPlanSelected();
		cartPage.verifyMonthlyPaymentLinePriceDisplayed();
		Double V1 = cartPage.getMonthlyPaymentLinePrice();
		cartPage.clickOnTMobileEssentialsPlan();
		cartPage.verifyTMobileEssentialsPlanSelected();
		Double V2 = cartPage.getMonthlyPaymentLinePrice();
		cartPage.compareMonthlyPaymentsLinePrices(V1, V2);

	}

	/**
	 * TA1904783:TMNG/DMO: With TMO One Plus selected on the Add on Container Line
	 * price is updated to $85 for TMO One DE239503 :TMNG/DMO: With TMO One Plus
	 * selected on the Add on Container Line price is updated to $85 for TMO One
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testLinePriceIsNotUpdatedWhenUserSelectAnyAddons(TMNGData tMNGData) {
		Reporter.log("Test Case :");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
		Reporter.log("3. Select Device | PDP page should be displayed");
		Reporter.log("4. Click on Add to cart button | Cartpage should be displayed");
		Reporter.log("5. Verify device | Device should be added to cart");
		Reporter.log("6. Save Line price in V1 | LinePrice should be saved in V1");
		Reporter.log("7. Select any Addon Service | Addon Service should be selected");
		Reporter.log("8. Now Save Line price in V2 | LinePrice should be saved in V2");
		Reporter.log("9. Compare the Line prices(V1 and V2) | Line prices should be equal");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPageBySelectingPhone(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyDeviceInCart();
		cartPage.verifyMonthlyPaymentLinePriceDisplayed();
		Double V1 = cartPage.getMonthlyPaymentLinePrice();
		cartPage.clickFirstOptionInAddOnsAndExtras();
		Double V2 = cartPage.getMonthlyPaymentLinePrice();
		cartPage.verifyMonthlyPaymentsLinePrices(V1, V2);
	}

	/**
	 * TA1904784:TMNG/DMO: TMO One Plus Data service that is selected for Phones on
	 * TMO One plan does not display in, View Order and Review Order DE239653
	 * :TMNG:qatprd(qlab03): TMO One Plus Data service that is selected for Phones
	 * on TMO One plan does not display in, View Order and Review Order
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testAddOnsServicesDisplayedInOrderDetailsModelAndReviewOrderPage(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :TA1904784:TMNG/DMO:  TMO One Plus Data service that is selected for Phones on TMO One plan does not display in, View Order and Review Order");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Phone link on Cart | Credit options modal should  be displayed");
		Reporter.log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("4. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("6. Verify Add ons and Extras | Add ons and Extras should be displayed");
		Reporter.log(
				"7. Select PlusUp and Protection 360 services from Add ons and Extras | PlusUp and Protection 360 services from Add ons and Extras should be selected");
		Reporter.log("8. Click on View order details link | Order Details model should be displayed");
		Reporter.log(
				"9. Verify Addons service in Order Details model | Addons service should be displayed in Order Details model");
		Reporter.log("10. Close on OrderDetails model | CartPage should be displayed");
		Reporter.log("11. Click on Continue button | Checkout page should be diplayed");
		Reporter.log(
				"12. fill all personal and shipping information and click on next| payment and credit page should be displayed");
		Reporter.log(
				"13. fill all payment and credit information , set security pin and click on 'Agree & Next' | Review & submit page  should be displayed");
		Reporter.log(
				"14. Verify Addons service in Review & submit page | Addons service should be displayed in Review & submit page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyaddOnsAndExtraSection();
		cartPage.clickFirstOptionInAddOnsAndExtras();
		cartPage.clickProtectionInAddOnsAndExtras();
		cartPage.enterYourZIPCode(tMNGData.getZipcode());
		cartPage.clickOnNextBtn();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyProtectionAddonsService();
		cartPage.clickOnCloseIconAtOrderDetailPage();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);
		cartPage.verifyProtectionAddonsService();
	}

	/**
	 * TA1904781:TMNG/QATPRD : (Multi Cart) The BYOD price is changing to EIP, when
	 * the plan is changed to Essentials in cart. DE239432 :TMNG/QATPRD : (Multi
	 * Cart) The BYOD price is changing to EIP, when the plan is changed to
	 * Essentials in cart.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testBYODdevicePriceWhenUserChangeOnePlanToAnotherPlan(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :TA1904781:TMNG/QATPRD : (Multi Cart) The BYOD price is changing to EIP, when the plan is changed to Essentials in cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Phone link on Cart | Credit options modal should  be displayed");
		Reporter.log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("4. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Select payment option as 'Pay in full' | Payment option should be selected");
		Reporter.log("6. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("7. Click  Add a tablet link on Cart | Tablets Mini PLP should  be displayed");
		Reporter.log("8. Select any device in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("9. Select payment option as 'Pay in full' | Payment option should be selected");
		Reporter.log("10. Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("11. Click  'BYOD' link on Cart | BYOD PDP Page should  be displayed");
		Reporter.log("12. Select Device SIM Card Type as 'For Phone' | Device SIM Card Type option should be selected");
		Reporter.log("13. Click Add to Cart in BYOD PDP | Cart Page should  be displayed");
		Reporter.log("14. Click  'BYOD' link on Cart | BYOD PDP Page should  be displayed");
		Reporter.log(
				"15. Select Device SIM Card Type as 'For Tablet' | Device SIM Card Type option should be selected");
		Reporter.log("16. Click Add to Cart in BYOD PDP | Cart Page should  be displayed");
		Reporter.log("17. Verify Magenta plan is selected | Magenta plan is should be selected");
		Reporter.log("18. Save BYOD Device price in V1 | BYOD Device price should be saved in V1");
		Reporter.log("19. Change Magenta plan to Essentials plan | Essential plan should be active");
		Reporter.log("20. Save BYOD Device price in V2 | BYOD Device price should be saved in V2");
		Reporter.log("21. Compare BYOD price | BYOD Device price should be equal");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToEmptyCart(tMNGData);
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.verifyCartPageLoaded();

		cartPage.addATablettoCartFromCartPage(tMNGData);

		cartPage.verifyCartPageLoaded();
		cartPage.verifyMagentaPlanSelected();
		Double magentaPlanBYODPriceForPhones = cartPage.getBYODPriceForPhones();
		Double magentaPlanBYODPriceForTablet = cartPage.getBYODPriceForTablet();
		cartPage.clickOnTMobileEssentialsPlan();
		Double essentialsPlanBYODPriceForPhones = cartPage.getBYODPriceForPhones();
		Double essentialsPlanBYODPriceForTablet = cartPage.getBYODPriceForTablet();
		cartPage.compareFRPPricesForBYODPhones(magentaPlanBYODPriceForPhones, essentialsPlanBYODPriceForPhones);
		cartPage.compareFRPPricesForBYODTablets(magentaPlanBYODPriceForTablet, essentialsPlanBYODPriceForTablet);

	}

	/**
	 * US570044 :Tier6: Present Dynamic P360 pricing in cart C519736
	 * :01_Phones_Tier6_Dynamic price on cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testProtectionPriceDisplayedUnderAddOnsAndExtrasForPhones(TMNGData tMNGData) {
		Reporter.log("Test Case :US570044:Tier6: Present Dynamic P360 pricing in cart");
		Reporter.log("Test Case :C519736 :01_Phones_Tier6_Dynamic price on cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
		Reporter.log("3. Select Tier-6 eligible Device | PDP page should be displayed");
		Reporter.log("4. Click on Add to cart button | Cartpage should be displayed");
		Reporter.log("5. Verify added device | Added Device should be displayed in cart");
		Reporter.log(
				"6. Verify Protection 360 price under Add ons and Extras section | Protection 360 price should be displayed under Add ons and Extras section");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US570044 :Tier6: Present Dynamic P360 pricing in cart C519738
	 * :01_tablets_Tier2_Dynamic price on cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testProtectionPriceDisplayedUnderAddOnsAndExtrasForTablets(TMNGData tMNGData) {
		Reporter.log("Test Case :US570044:Tier6: Present Dynamic P360 pricing in cart");
		Reporter.log("Test Case :C519738  :01_tablets_Tier2_Dynamic price on cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
		Reporter.log("3. Click on Tablets & Devices Tab from Shop | PLP page should be displayed");
		Reporter.log("4. Select Tier-2 eligible Tablet Device | PDP page should be displayed");
		Reporter.log("5. Click on Add to cart button | Cartpage should be displayed");
		Reporter.log("6. Verify added Tablet device | Added Tablet Device should be displayed in cart");
		Reporter.log(
				"7. Verify Protection 360 price under Add ons and Extras section | Protection 360 price should be displayed under Add ons and Extras section");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US570044 :Tier6: Present Dynamic P360 pricing in cart C520680
	 * :01_wearables_Tier5_Dynamic price on cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testProtectionPriceDisplayedUnderAddOnsAndExtrasForWatches(TMNGData tMNGData) {
		Reporter.log("Test Case :US570044:Tier6: Present Dynamic P360 pricing in cart");
		Reporter.log("Test Case :C520680  :01_wearables_Tier5_Dynamic price on cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
		Reporter.log("3. Click on Watches Tab from Shop | PLP page should be displayed");
		Reporter.log("4. Select Tier-5 eligible wearable Device | PDP page should be displayed");
		Reporter.log("5. Click on Add to cart button | Cartpage should be displayed");
		Reporter.log("6. Verify added wearable device | Added wearable Device should be displayed in cart");
		Reporter.log(
				"7. Verify Protection 360 price under Add ons and Extras section | Protection 360 price should be displayed under Add ons and Extras section");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US570044 :Tier6: Present Dynamic P360 pricing in cart C520683 :02_mixed cart
	 * with phone_Tablet and Wearable_Dynamic price for all
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testProtectionPriceDisplayedUnderAddOnsAndExtrasForPhonesAndTablesAndWearables(TMNGData tMNGData) {
		Reporter.log("Test Case :US570044:Tier6: Present Dynamic P360 pricing in cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO | TMO should be launched");
		Reporter.log("2. Click on Phones Tab | PLP page should be displayed");
		Reporter.log("3. Select Tier-6 eligible Device | PDP page should be displayed");
		Reporter.log("4. Click on Add to cart button | Cartpage should be displayed");
		Reporter.log("5. Verify added device | Added Device should be displayed in cart");
		Reporter.log("6. Click  Add a tablet link on Cart | Credit options modal should  be displayed");
		Reporter.log("7. Select Awesome credit options and click Done | Tablet Mini PLP should  be displayed");
		Reporter.log("8. Select any Tier-6 eligible Tablet in mini PLP | Tablet Mini PDP Page should  be displayed");
		Reporter.log("9. Click Add to Cart in Tablet mini PDP | Cart Page should  be displayed");
		Reporter.log("10. Verify added Tablet device | Added Tablet Device should be displayed in cart");
		Reporter.log("11. Click  Add a Wearable link on Cart | Credit options modal should  be displayed");
		Reporter.log("12. Select Awesome credit options and click Done | Wearable Mini PLP should  be displayed");
		Reporter.log(
				"13. Select any Tier-6 eligible Wearable in mini PLP |Wearable Mini PDP Page should  be displayed");
		Reporter.log("14. Click Add to Cart in Wearable mini PDP | Cart Page should  be displayed");
		Reporter.log(
				"15. Verify Protection 360 price under Add ons and Extras section | Protection 360 price should be displayed under Add ons and Extras section");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US310958:[Continued] TEST ONLY: TMO Additional Terms - Cart Line Level
	 * Display Today and Monthly Payment
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testCartPageDisplayedTodayAndMonthlyPaymentsWithOutSimStarterKit(ControlTestData data,
			TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US310958:TEST ONLY: TMO Additional Terms - Cart Line Level Display Today and Monthly Payment");
		Reporter.log("Condition | Select device with out Sim Starter Kit");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Verify Today Price in Cart page | Today Price in Cart page should be displayed");
		Reporter.log("5. Verify Monthly Price in Cart page | Monthly Price in Cart page should be displayed");
		Reporter.log("6. Click View order details link | Order details should be displayed");
		Reporter.log("7. Verify SIM Starter Kit | SIM Starter Kit should not be displayed");
		Reporter.log("8. Verify 'Today price and EIP Downpayment' | 'Today price and EIP Downpayment' should be equal");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage accessoryPdpPage = navigateToAccessoryMainPDP(tMNGData);
		accessoryPdpPage.clickOnAddToCartBtn();
		accessoryPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifyStickyBannerTodayTotalPriceDisplayed();
		cartPage.verifyStickyBannerMonthlyTotalPriceDisplayed();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifySimStarterKitTextNotDisplayed();
		Double totalToday = cartPage.getTotalTodaypriceAtOderDetailPage();
		Double totalDownpayment = cartPage.getTodayDownPaymentPriceAtOderDetailPage();
		cartPage.compareEIPDownpaymentAndSSKprice(totalToday, totalDownpayment);
	}

	/**
	 * US569506 :TPP - Prescreen - Display Loading screen for Pre Screen (after SSN
	 * submission on form page) C547639 Verify Loading screen is displayed during
	 * pre-screening process
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testVerifyLoadingScreenDuringPrescreeningCheckonCart(TMNGData tMNGData) {
		Reporter.log(
				" US569506 :TPP - Prescreen - Display Loading screen for Pre Screen (after SSN submission on form page)");
		Reporter.log("C547639	Verify Loading screen is displayed during pre-screening process");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO cart | TMO cart page should be launched");
		Reporter.log("2. Click Add Phone  | Phones PLP page should be displayed");
		Reporter.log("4. Select a phone from PLP | Phones PDP should  be displayed");
		Reporter.log("5. Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("6. Add Wearable,Tablet and Accessory to cart |All type of products should be added into cart");
		Reporter.log("7. Verify verify your credit link on Cart | Verify your credit link should be displayed on Cart");
		Reporter.log("8. Click verify your credit link | Verify your credit modal should be displayed");
		Reporter.log("9. Fill all manditory fields on modal | All fileds should be filled on modal");
		Reporter.log("10. Click on 'Run credit check' | Loading page should display while Hard Credit check run");
		Reporter.log(
				"11. Verify text's on Loading screen | Text should be 'We're running a quick credit check and 'This won't take long at all and text are authorable.");
		Reporter.log(
				"12. After it is finished, verify it will automatically display the updated price modal | Updated price should be displayed for lines");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.addAWearabletoCartFromCartPage(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);
//		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);

		Double monthlyPrice1 = cartPage.getMonthlyPaymentDevicePrice();
		Double downPayment1 = cartPage.getOneTimePaymentDevicePrice();
		Double monthlyPriceTab1 = cartPage.getMonthlyPaymentDevicePriceForSecondItem();
		Double downPaymentTab1 = cartPage.getOneTimePaymentDevicePriceSecondDevice();

		cartPage.verifyFindYourPricingCTA();
		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tppPrescreenintro = new TPPPreScreenIntro(getDriver());
		tppPrescreenintro.clickLetsGoCTAOnPreScreenIntroPage();

		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppPreScreenForm.verifyPreScreenloadingPage();

		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();
		tppOfferPage.clickGotItCTA();

		cartPage.verifyCartPageLoaded();

		Double monthlyPrice2 = cartPage.getMonthlyPaymentDevicePrice();
		Double downPayment2 = cartPage.getOneTimePaymentDevicePrice();
		Double monthlyPriceTab2 = cartPage.getMonthlyPaymentDevicePriceForSecondItem();
		Double downPaymentTab2 = cartPage.getOneTimePaymentDevicePriceSecondDevice();
		cartPage.compareTwoDoubleValuesNotEqual(monthlyPrice1, monthlyPrice2);
		cartPage.compareTwoDoubleValuesNotEqual(downPayment1, downPayment2);
		cartPage.compareTwoDoubleValuesNotEqual(monthlyPriceTab1, monthlyPriceTab2);
		cartPage.compareTwoDoubleValuesNotEqual(downPaymentTab1, downPaymentTab2);
		cartPage.verifyMonthlyPaymentDevicePriceDisplayed();
		cartPage.verifyStickyBannerTodayTotalPriceDisplayed();
		cartPage.verifyStickyBannerMonthlyTotalPriceDisplayed();
		cartPage.verifyNewPriceBasedOnCreditClass();
	}

	/**
	 * US578417 Updated Sim Starter Kit name and pricing as a line item for
	 * Handset(Voice)on Cart page BYOD Scenario - Updated Sim name should name
	 * should reflect on cart page T1442618:Phone SIM card name and pricing in cart
	 * under One Time Payment section- All Plans - Before and After Prescreen In
	 * Cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void verifyUpdatedSimCardDetailsOnCartForBYODHandSetDeviceFromEmptyCart(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US578417	BYOD Scenario - Sim Name card name and Pricing should display as a line item for Handset(Voice) on Cart Page");
		Reporter.log("Test Steps | Expected Results:");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select SimStarterKit | PDP page should be displayed");
		Reporter.log("4. Verify SIM Card Name | SIM Card Name should be displayed");
		Reporter.log("5. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("6. Verify Selected plan is Magenta | Magenta plan should be selected");
		Reporter.log(
				"7. Verify Line price(SSK price)in one time payment section | Line price should be displayed in Onetime payment section");
		Reporter.log("8. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("9. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"10. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("11. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("12. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("13. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log(
				"14. Verify pre screen page after loading page and click Continue CTA on offer page | CartPage should be displayed with updated price");
		Reporter.log(
				"15. Validate SSK name change in line item in the Product tile | SSK and the price should be displayed on line item");
		Reporter.log(
				"16. Validate SSK price is added to the Today Line price in line item | SSK price should be listed in the Line total");
		Reporter.log(
				"17. Validate SSK price is added to the Cart Total Today price | SSK price should be added to the Cart Total Today price");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		PdpPage devicesPdpPage = new PdpPage(getDriver());
		devicesPdpPage.verifyPhonePdpPageLoaded();
		devicesPdpPage.verifySimKitName();
		devicesPdpPage.verifySimKitPrice();
		devicesPdpPage.clickOnAddToCartBtn();
		devicesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifySimStaterKitPriceDisplayed();
		cartPage.verifyMagentaPlanSelected();
		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		TPPOfferPage tPPOfferPage = new TPPOfferPage(getDriver());
		tPPOfferPage.verifyTPPOfferPage();
		tPPOfferPage.clickGotItCTA();
		cartPage.verifyCartPageLoaded();
		cartPage.verifySimStaterKitPriceDisplayed();
		cartPage.verifyTodayPriceUnderMonthlyPaymentSection();

	}

	/**
	 * US586088 Updated Sim Starter Kit name and pricing as a line item for MI
	 * Device on Cart page BYOD Scenario - Updated Sim name should name should
	 * reflect on cart page T1442640 : MI SIM card name and pricing in cart under
	 * One Time Payment section -All Plans - Before and After Prescreen In Cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void verifyUpdatedSimNameAndPricingOnCartThroughTablets(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US586088	BYOD Scenario - Sim Name card name and Pricing should display as a line item for MI Device on Cart Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Tablets and Devices | Tablets PLP page should be displayed");
		Reporter.log("3. Select Mobile Internet SIm Card | PDP page should be displayed");
		Reporter.log("4. Verify SIM Card Name | SIM Card Name should be displayed");
		Reporter.log("5. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("6. Verify Selected plan is Magenta | Magenta plan should be selected");
		Reporter.log(
				"7. Verify Line price(SSK price)in one time payment section | Line price should be displayed in Onetime payment section");
		Reporter.log("8. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("9. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"10. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("11. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("12. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("13. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log(
				"14. Verify pre screen page after loading page and click Continue CTA on offer page | CartPage should be displayed with updated price");
		Reporter.log(
				"15. Validate SSK name change in line item in the Product tile | SSK and the price should be displayed on line item");
		Reporter.log(
				"16. Validate SSK price is added to the Today Line price in line item | SSK price should be listed in the Line total");
		Reporter.log(
				"17. Validate SSK price is added to the Cart Total Today price | SSK price should be added to the Cart Total Today price");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickOnTabletsAndDevicesLink();
		PlpPage tabletsAndDevicesPlpPage = new PlpPage(getDriver());
		tabletsAndDevicesPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		tabletsAndDevicesPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		PdpPage tabletsAndDevicesPdpPage = new PdpPage(getDriver());
		tabletsAndDevicesPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletsAndDevicesPdpPage.clickOnAddToCartBtn();
		tabletsAndDevicesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifySimStaterKitPriceDisplayed();
		cartPage.verifyMagentaPlanSelected();
		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		TPPOfferPage tPPOfferPage = new TPPOfferPage(getDriver());
		tPPOfferPage.verifyTPPOfferPage();
		tPPOfferPage.clickGotItCTA();
		cartPage.verifyCartPageLoaded();
		cartPage.verifySimStaterKitPriceDisplayed();
		cartPage.verifyTodayPriceUnderMonthlyPaymentSection();

	}

	/**
	 * US575533:TPP - Prescreen - Display Prescreen form page C543884 - Cart:Verify
	 * Find my price CTA - Manual review Page
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void verifyManualReviewPageForFindMyPriceCTAFromCart(TMNGData tMNGData) {
		Reporter.log("US575533:TPP - Prescreen - Display Prescreen form page");
		Reporter.log("C543884 - Cart:Verify Find my price CTA - Manual review Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log(
				"3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing"
						+ " should be displayed on PLP ");
		Reporter.log("4. Select any device and click on Add to cart from PDP page | Cart page should be displayed");
		Reporter.log("5. Verify 'Find your pricing' link on cart page | 'Find your pricing' link should be displayed");
		Reporter.log("6. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("7. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"8. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("9. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("10. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("11. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log("12. Verify pre screen page after loading page | Manual Review page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyFindYourPricingCTA();
		cartPage.clickFindYourPricingCTA();

		TPPPreScreenIntro tppIntro = new TPPPreScreenIntro(getDriver());
		tppIntro.clickLetsGoCTAOnPreScreenIntroPage();

		TPPPreScreenForm tppScreenForm = new TPPPreScreenForm(getDriver());
		tppScreenForm.verifyPreScreenForm();
		TPPManualorFraudReviewPage tppManualReviewPage = new TPPManualorFraudReviewPage(getDriver());
		tppManualReviewPage.verifyManualRevieworFraudDetectionPage();

	}

	/**
	 * US575533:TPP - Prescreen - Display Prescreen form page C543886 - Cart:Verify
	 * Find my price CTA - Existing customers
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void verifyExistingCustomersPageForFindMyPriceCTAFromCart(TMNGData tMNGData) {
		Reporter.log("US575533:TPP - Prescreen - Display Prescreen form page");
		Reporter.log("C543886 - Cart:Verify Find my price CTA - Existing customers");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log(
				"3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing"
						+ " should be displayed on PLP ");
		Reporter.log("4. Select any device and click on Add to cart from PDP page | Cart page should be displayed");
		Reporter.log("5. Verify 'Find your pricing' link on cart page | 'Find your pricing' link should be displayed");
		Reporter.log("6. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("7. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"8. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("9. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("10. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("11. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log("12. Verify pre screen page after loading page | Existing customer page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyFindYourPricingCTA();
		cartPage.clickFindYourPricingCTA();

		TPPPreScreenIntro tppIntro = new TPPPreScreenIntro(getDriver());
		tppIntro.clickLetsGoCTAOnPreScreenIntroPage();

		TPPPreScreenForm tppScreenForm = new TPPPreScreenForm(getDriver());
		tppScreenForm.verifyPreScreenForm();
		tppScreenForm.verifyPreScreenFormPageTitle();
		tppScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppScreenForm.verifyPreScreenloadingPage();
		tppScreenForm.verifyExistingCustomerNotifyModalHeader();

	}

	/**
	 * US575533:TPP - Prescreen - Display Prescreen form page C543882 - Cart:Verify
	 * Find my price CTA - Pre screen No Offer Page
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void verifyPrescreenNoOfferPageFromCart(TMNGData tMNGData) {
		Reporter.log("US575533:TPP - Prescreen - Display Prescreen form page");
		Reporter.log("C543882 - Cart:Verify Find my price CTA - Pre screen No Offer Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log(
				"3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing"
						+ " should be displayed on PLP ");
		Reporter.log("4. Select any device and click on Add to cart from PDP page | Cart page should be displayed");
		Reporter.log("5. Verify 'Find your pricing' link on cart page | 'Find your pricing' link should be displayed");
		Reporter.log("6. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("7. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"8. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("9. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("10. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("11. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log("12. Verify pre screen page after loading page | Prescreen No Offer Page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyFindYourPricingCTA();
		cartPage.clickFindYourPricingCTA();

		TPPPreScreenIntro tppIntro = new TPPPreScreenIntro(getDriver());
		tppIntro.verifyPreScreenIntroPage();
		tppIntro.clickLetsGoCTAOnPreScreenIntroPage();

		TPPPreScreenForm tppScreenForm = new TPPPreScreenForm(getDriver());
		tppScreenForm.verifyPreScreenForm();
		tppScreenForm.verifyPreScreenFormPageURL();
		tppScreenForm.verifyPreScreenFormPageTitle();
		tppScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppScreenForm.verifyPreScreenloadingPage();
		TPPNoOfferPage tppNoOfferpage = new TPPNoOfferPage(getDriver());
		tppNoOfferpage.verifyPrescreenNoOfferPage();
	}

	/**
	 * US578416:SSK fee will no longer display in Cart for a new device purchase
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TPP, Group.DESKTOP })
	public void testSSKfeeDetailsOnCartForDevices(TMNGData tMNGData) {
		Reporter.log("US578416:SSK fee will no longer display in Cart for a new device purchase");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log(
				"5. Verify SSK Fee in One Time Payment Section | SSK fee should not be displayed as line breakdown on One Time Payment section");
		Reporter.log("6. Verify Line Total | Line total should be matched");
		Reporter.log("7. Click on Add a Tablet CTA | Tablet PLP should be displayed");
		Reporter.log("8. Select any Tablet | PDP page should be displayed");
		Reporter.log("9. Click on Add to Cart| Cart page should be displayed");
		Reporter.log(
				"10. Verify SSK Fee presence in One Time Payment Section | SSK fee should not be displayed as line breakdown");
		Reporter.log("11. Verify Line Total | Line total should be matched");
		Reporter.log("12. Click on Add a Wearable CTA | Wearable PLP should be displayed");
		Reporter.log("13. Select any Wearable | PDP page should be displayed");
		Reporter.log("14. Click on Add to Cart| Cart page should be displayed");
		Reporter.log(
				"15. Verify SSK Fee presence in One Time Payment Section | SSK fee should not be displayed as line breakdown");
		Reporter.log("16. Verify Line Total | Line total should be matched");
		Reporter.log("17. Verify Toggle Value | 'isSimKitEnabled' value should be 'FALSE'");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifySimStarterKitTextNotDisplayed();
		cartPage.clickOnAddATabletLinkOnCart();
		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyTabletsAndDevicesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.verifySimStarterKitTextNotDisplayed();
		cartPage.clickOnAddAWearableLinkOnCart();
		plpPage.verifyWatchesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getWatchName());
		pdpPage.verifyWatchesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.verifySimStarterKitTextNotDisplayed();
		Double V1 = cartPage.getTotalLinePriceForFirstItem();
		Double V2 = cartPage.getMonthlyLineTotalPriceForFirstItem();
		cartPage.compareMonthlyPaymentsLinePrices(V1, V2);
		Double V3 = cartPage.getTotalLinePriceForSecondItem();
		Double V4 = cartPage.getMonthlyLineTotalPriceForSecondItem();
		cartPage.compareMonthlyPaymentsLinePrices(V3, V4);
		Double V5 = cartPage.getTotalLinePriceForThirdItem();
		Double V6 = cartPage.getMonthlyLineTotalPriceForThirdItem();
		cartPage.compareMonthlyPaymentsLinePrices(V5, V6);

		if (getTogglevalue("isSimKitEnabled").equals("false")) {
			cartPage.verifySimStarterKitTextNotDisplayed();
		} else {
			Reporter.log("isSimKitEnabled is true and Sim kit text is present on cart page.");
		}
	}

	/**
	 * US588459 :TPP - Prescreen Fail - Display Pre Screen No Offer Page C543899 -
	 * Cart:Pre-screen Fail - "Schedule an in-store visit"
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testVerifyPrescreenFailScheduleInStoreVisit(TMNGData tMNGData) {
		Reporter.log(" US588459 :TPP - Prescreen Fail - Display Pre Screen No Offer Page");
		Reporter.log("C543899 - Cart:Pre-screen Fail - Schedule an in-store visit");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log(
				"3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing should be displayed on PLP ");
		Reporter.log("4. Click 'Find your pricing' link | Pre-screen intro page should be displayed.");
		Reporter.log("5. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"6. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log(
				"7. Enter required information and  click on the 'Find my price' CTA | pre-screen processing page should be displayed.");
		Reporter.log(
				"8. Verify processing page is displayed when the pre-screen is being done  | Processing page should be displayed after pre-screen");
		Reporter.log(
				"7. Verify header on proceessing page  | 'Pre screen No Offer Page' header on Processing page should be displayed.");
		Reporter.log(
				"8. Click on 'Schedule an in-store visit' link on the page | User should navigate to Store locator page.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyFindYourPricingCTA();
		cartPage.clickFindYourPricingCTA();

		TPPPreScreenIntro tppIntro = new TPPPreScreenIntro(getDriver());
		tppIntro.verifyPreScreenIntroPage();
		tppIntro.clickLetsGoCTAOnPreScreenIntroPage();

		TPPPreScreenForm tppScreenForm = new TPPPreScreenForm(getDriver());
		tppScreenForm.verifyPreScreenForm();
		tppScreenForm.verifyPreScreenFormPageTitle();
		tppScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppScreenForm.verifyPreScreenloadingPage();

		TPPNoOfferPage tppNoOfferPage = new TPPNoOfferPage(getDriver());
		tppNoOfferPage.verifyPrescreenNoOfferPage();
		tppNoOfferPage.clickScheduleAnInStoreVisitCTA();
		SLPPage slpPage = new SLPPage(getDriver());
		slpPage.verifyStoreLocatorListPage();

	}

	/**
	 * US589843 - TPP - Display Ad Ons for Updated Order Details
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testVerifyAdOnsInViewOrderDetailedPage(TMNGData tMNGData) {
		Reporter.log("Test Case :US589843 - TPP - Display Ad Ons for Updated Order Details");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select any Phone | PDP page should be displayed");
		Reporter.log("4. Click on Add to cart CTA and Click on Continue as guest CTA | Cart page should be displayed");
		Reporter.log("5. Select any ad on to cart | Ad on should be added to cart");
		Reporter.log("6. Click on View Order detail Link | View order detail modal should be displayed");
		Reporter.log(
				"7. Verify ad on price breakdown on view order | Ad on price break down should be displayed under plan section.");
		Reporter.log("8. Verify prices totals with ad ons | Prices total calculation should be correct with Ad ons");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifySimStarterKitTextNotDisplayed();
		cartPage.clickFirstAddOn();
		cartPage.verifyCartPageLoaded();
		// verifySelectedServiceAndPriceForAddOns();
		// cartPage.verifyViewOrderDetailsModel();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyPriceBreakDownAddOnDisplayedOnOrderdetailedModel();
		Double V1 = cartPage.getTotalCalculationWithAddon();
		Double V2 = cartPage.getOrderDetailsLineMonthlyTotalPriceForFirstItem();
		cartPage.verifyMonthlyPaymentsLinePrices(V1, V2);
	}

	/**
	 * US588603 - TEST ONLY TPP CART - Update One Time Charges section to
	 * "unverified price" version
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testFrpDevicePricesInCartPage(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US588603 - TEST ONLY TPP CART - Update One Time Charges section to unverified price version");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select any Phone | PDP page should be displayed");
		Reporter.log("4. Select PayInfull link | PayInFull link should be selected");
		Reporter.log("5. Click on Add to cart CTA and Click on Continue as guest CTA | Cart page should be displayed");
		Reporter.log("6. Verify FRP price for device | FRP price should be displayed");
		Reporter.log("7. Verify SimStarterKit fee | Ensure that SimStarter kit fee is not displayed");
		Reporter.log(
				"8. Verify '+taxes and shipping' text in below one time chages | '+taxes and shipping' should be displayed in one time chages ");
		Reporter.log(
				"9. Verify 'Includes $5 monthly AutoPay discount' | 'Includes $5 monthly AutoPay discount' should be displayed");
		Reporter.log("10. Verify 'Verify your pricing' CTA | 'Verify your pricing' CTA should be displayed");
		Reporter.log("11. Click on 'Verify your pricing' CTA | Should be directed to NARWAL PREscreen/HCC flow");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickPayInFullPaymentOption();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifyFRPPriceForDeviceIsDisplayed();
		cartPage.verifySimStarterKitNotDisplayedMultiline();
		cartPage.verifyTaxesAndShippingDisplayedMultiline();
		cartPage.verifyTodayPriceUnderMonthlyPaymentSection();
		cartPage.verifyMonthlyPriceUnderMonthlyPaymentSection();
		cartPage.verifyAutoPayMessageDisplayedMultiline();
		cartPage.verifyFindYourPricingCTA();
		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tPPPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tPPPreScreenIntro.verifyPreScreenIntroPage();

	}

	/**
	 * US588603 - TEST ONLY TPP CART - Update One Time Charges section to
	 * "unverified price" version
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testEIPDevicePricesInCartPage(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US588603 - TEST ONLY TPP CART - Update One Time Charges section to unverified price version");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Select any Phone | PDP page should be displayed");
		Reporter.log("4. Click on Add to cart CTA and Click on Continue as guest CTA | Cart page should be displayed");
		Reporter.log("5. Verify EIP price for device | EIP price should be displayed");
		Reporter.log("6. Verify  Due Today and monthly price |  Due Today and monthly price should be displayed");
		Reporter.log("7. Verify SimStarterKit fee | Ensure that SimStarter kit fee is not displayed");
		Reporter.log(
				"8. Verify '+taxes and shipping' text in below  estimated Today | '+taxes and shipping' should be displayed in  estimated Today");
		Reporter.log(
				"9. Verify 'Includes $5 monthly AutoPay discount' | 'Includes $5 monthly AutoPay discount' should be displayed");
		Reporter.log("10. Verify 'Verify your pricing' CTA | 'Verify your pricing' CTA should be displayed");
		Reporter.log("11. Click on 'Verify your pricing' CTA | Should be directed to NARWAL PREscreen/HCC flow");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyMonthlyPaymentDevicePriceDisplayed();
		cartPage.verifyTodayPriceUnderMonthlyPaymentSection();
		cartPage.verifyMonthlyPriceUnderMonthlyPaymentSection();
		cartPage.verifySimStarterKitNotDisplayedMultiline();
		cartPage.verifyTaxesAndShippingDisplayedMultiline();
		cartPage.verifyAutoPayMessageDisplayedMultiline();
		cartPage.verifyFindYourPricingCTA();
		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tPPPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tPPPreScreenIntro.verifyPreScreenIntroPage();
	}

	/**
	 * US589248:TEST ONLY TPP CART - test scenarios for Mixed cart
	 * (device/tablet/accessories)
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testDevicesPricesBeforeAndAfterVerifiedPrice(TMNGData tMNGData) {
		Reporter.log("US589248:TEST ONLY TPP CART - test scenarios for Mixed cart (device/tablet/accessories)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO cart | TMO cart page should be launched");
		Reporter.log("2. Click on Add a phone | Credit modal should be displayed");
		Reporter.log("3. Click done on credit modal | mini PLP page should be displayed");
		Reporter.log("4. Select any device mini PLP | mini PDP page should be displayed");
		Reporter.log("5. Click on Add to cart button | Cart page should be displayed");
		Reporter.log("6. Add tablet and Accessory to Cart | Cart page should be displayed");
		Reporter.log(
				"7. verify CRP8 /Awesome pricing in line level and on sticky banner | CRP8 /Awesome price should be displayed on line level and on sticky banner");
		Reporter.log("8. Click on Find my price CTA | PreScreen Intro page should be displayed");
		Reporter.log("9. Click on Lets go CTA | PreScreen form should be displayed");
		Reporter.log("10. Fill the mandetery field in Prescreen form | Mandetery fields should be filled");
		Reporter.log("11. Click on Find my price CTA | Pre-screen loading model should be displayed");
		Reporter.log("12. Verify updated price | Updated price should be displayed should be displayed");
		Reporter.log(
				"13. Verify updated price on sticky banner | Updated price should be displayed should be displayed on sticky banner");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPagefromEmptyCartFromUNOPLP(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickOnAddATabletLinkOnCart();
		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyTabletsAndDevicesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		plpPage.verifyAccessoriesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		pdpPage.verifyAccessoriesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyMonthlyPaymentDevicePriceDisplayed();
		cartPage.verifyStickyBannerTodayTotalPriceDisplayed();
		cartPage.verifyStickyBannerMonthlyTotalPriceDisplayed();
		Double monthlyPrice1 = cartPage.getMonthlyPaymentDevicePrice();
		Double downPayment1 = cartPage.getOneTimePaymentDevicePrice();
		Double monthlyPriceTab1 = cartPage.getMonthlyPaymentDevicePriceForSecondItem();
		Double downPaymentTab1 = cartPage.getOneTimePaymentDevicePriceSecondDevice();
		Double monthlyPriceSticky1 = Double.parseDouble(cartPage.getTotalMonthlyPriceStickyBanner());
		Double downPaymentSticky1 = Double.parseDouble(cartPage.getTotalTodayPriceInStickyBanner());

		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tPPPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tPPPreScreenIntro.verifyPreScreenIntroPage();
		tPPPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tPPPreScreenForm = new TPPPreScreenForm(getDriver());
		tPPPreScreenForm.verifyPreScreenFormPageTitle();
		tPPPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tPPPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();
		tppOfferPage.clickGotItCTA();

		CartPage cartPage1 = new CartPage(getDriver());
		cartPage1.verifyCartPageLoaded();
		Double monthlyPrice2 = cartPage1.getMonthlyPaymentDevicePrice();
		Double downPayment2 = cartPage1.getOneTimePaymentDevicePrice();
		Double monthlyPriceTab2 = cartPage1.getMonthlyPaymentDevicePriceForSecondItem();
		Double downPaymentTab2 = cartPage1.getOneTimePaymentDevicePriceSecondDevice();
		Double monthlyPriceSticky2 = Double.parseDouble(cartPage.getTotalMonthlyPriceStickyBanner());
		Double downPaymentSticky2 = Double.parseDouble(cartPage.getTotalTodayPriceInStickyBanner());
		cartPage.compareTwoDoubleValuesNotEqual(monthlyPrice1, monthlyPrice2);
		cartPage.compareTwoDoubleValuesNotEqual(downPayment1, downPayment2);
		cartPage.compareTwoDoubleValuesNotEqual(monthlyPriceTab1, monthlyPriceTab2);
		cartPage.compareTwoDoubleValuesNotEqual(downPaymentTab1, downPaymentTab2);
		cartPage.compareTwoDoubleValuesNotEqual(monthlyPriceSticky1, monthlyPriceSticky2);
		cartPage.compareTwoDoubleValuesNotEqual(downPaymentSticky1, downPaymentSticky2);
		cartPage.verifyDeviceUnderMonthlyPaymentDisplayedMultiline();

		cartPage.verifyStickyBannerTodayTotalPriceDisplayed();
		cartPage.verifyStickyBannerMonthlyTotalPriceDisplayed();
	}

	/**
	 * US588610:TEST ONLY TPP CART - Update bottom pricing sticky to "unverified
	 * price" version
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testUpdatePricingStickyOnCartPageForUnverifiedVersion(TMNGData tMNGData) {
		Reporter.log("US588610:TEST ONLY TPP CART - Update bottom pricing sticky to unverified price version");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO cart | TMO cart page should be launched");
		Reporter.log("2. Click on Add a phone | Credit modal should be displayed");
		Reporter.log("3. Click done on credit modal | mini PLP page should be displayed");
		Reporter.log("4. Select any device mini PLP | mini PDP page should be displayed");
		Reporter.log("5. Click on Add to cart button | Cart page should be displayed");
		Reporter.log("6. Add tablet and Accessory to Cart | Cart page should be displayed");
		Reporter.log(
				"6. verify CRP8 /Awesome pricing in line level and on sticky banner | CRP8 /Awesome price should be displayed on line level and on sticky banner");
		Reporter.log(
				"7. Verify Today and Monthly Prices on Sticky Banner | Estimated Today and Estimated Monthly Pricing should be displayed");
		Reporter.log(
				"8. Verify Total due Today Prices on Sticky Banner | sim kits, device down payments should be displayed");
		Reporter.log(
				"9. Verify Total due Monthly Prices on Sticky Banner | ad ons, line charges,device due monthly payments should be displayed");
		Reporter.log(
				"10. Verify +taxes and Shipping on Sticky Banner | +taxes and shipping under estimated Today should be displayed");
		Reporter.log(
				"11. Verify Includes $5 monthly AutoPay discount on Sticky Banner | 'Includes $5 monthly AutoPay discount' under estimated monthly for each line should be displayed");
		Reporter.log("12. Verify order estimated ship | Order estimated ship dates should be displayed");
		Reporter.log("12. Verify 'Complete in store' link and click on that | existing Modal should be displayed ");
		Reporter.log(
				"12. Verify 'Verify your pricing -CTA' | 'Verify your pricing -CTA' should be displayed on sticky banner");
		Reporter.log("13. Click on 'verify your pricing - CTA' | Should be navigated to NARWHAL PREscreen/HCC flow");
		Reporter.log("14. Click on Continue - CTA | should be navigated to check out page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToCartPagefromEmptyCartFromUNOPLP(tMNGData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickOnAddATabletLinkOnCart();
		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyTabletsAndDevicesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		plpPage.verifyAccessoriesPlpPageLoaded();
		plpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		pdpPage.verifyAccessoriesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyDeviceUnderMonthlyPaymentDisplayedMultiline();
		cartPage.verifyTodayPriceUnderMonthlyPaymentSection();
		cartPage.verifyMonthlyPriceUnderMonthlyPaymentSection();
		cartPage.verifyStickyBannerTodayTotalPriceDisplayed();
		cartPage.verifyStickyBannerMonthlyTotalPriceDisplayed();
		cartPage.verifyStickyBannerTaxAndShippingTextDisplayed();
		cartPage.verifyStickyBannerAutoPayTextDisplayed();
		cartPage.clickCompleteInStoreLink();
		cartPage.verifyCompleteStoreModalWindow();
		cartPage.clickCloseIconInCompleteInStoreModalWindow();
		cartPage.verifyStickyBannerEstimatedShippingTextDisplayed();
		cartPage.verifyFindYourPricingCTA();
		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tPPPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tPPPreScreenIntro.verifyPreScreenIntroPage();
		tPPPreScreenIntro.clickNoThanksOnPreScreenIntroPage();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
	}

	/**
	 * US588618:TEST ONLY Cart - Update pricing sticky and one time charges to
	 * "verified price" version (remove ctas/show true pricing)
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testStickyBannerPriceForSCCflow(TMNGData tMNGData) {
		Reporter.log(
				"US588618:TEST ONLY Cart - Update pricing sticky and one time charges to 'verified price' version (remove ctas/show true pricing)");
		Reporter.log("================================");

		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log(
				"3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing"
						+ " should be displayed on PLP ");
		Reporter.log("4. Select any device and click on Add to cart from PDP page | Cart page should be displayed");
		Reporter.log("5. Click on Add an accessory from cart | Accessory plp page should be displayed");
		Reporter.log("6. Select accessory | PDP page should be displayed");
		Reporter.log("7. Click on Add to cart button | Cart page should be displayed");
		Reporter.log("8. Verify 'Find your pricing' link on cart page | 'Find your pricing' link should be displayed");
		Reporter.log("9. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("10. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"11. Click on 'Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("12. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("13. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("14. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log("15. Verify pre screen page after loading page | Prescreen Offer Page should be displayed");
		Reporter.log(
				"16. Click on 'GOT IT' cta from Prescreen Offer Page | User should be redirected to entry point for customer to"
						+ " pre screen (cart) with updated pricing should be displayed in sticky banner");
		Reporter.log(
				"17. Verify 'Estimated' text from Monthly and Today | 'Estimated' text should not displayed from Monthly and Today");
		Reporter.log(
				"18. Verify Monthly and Today prices in sticky banner |  Monthly and Today prices should be displayed in sticky banner");
		Reporter.log(
				"19. Verify +shipping and taxes text and Autopay text in OneTimePayment section and sticky banner| +shipping and taxes text and Autopay text should be displayed in OneTimePayment section and sticky banner");
		Reporter.log(
				"20. Verify Pricing links in sticky banner and Onetime charges section | Pricing links should be removed from Sticky banner and OneTimeCharges section");
		Reporter.log(
				"21. Verify text as 'Showing your verified pricing based on your credit' in sticky banner | 'Showing your verified pricing based on your credit' text should be displayed in sticky banner");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.verifyCRPorAwesomePricesInPLP();
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.clickOnAddToCartBtn();
		pdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		phonesPlpPage.verifyAccessoriesPlpPageLoaded();
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		pdpPage.verifyAccessoriesPdpPageLoaded();
		pdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyFindYourPricingCTA();
		cartPage.clickFindYourPricingCTA();

		TPPPreScreenIntro tppIntro = new TPPPreScreenIntro(getDriver());
		tppIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppScreenForm = new TPPPreScreenForm(getDriver());
		tppScreenForm.verifyPreScreenForm();
		tppScreenForm.verifyPreScreenFormPageTitle();
		tppScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		tppScreenForm.verifyPreScreenloadingPage();
		TPPOfferPage tppOfferPage = new TPPOfferPage(getDriver());
		tppOfferPage.verifyTPPOfferPage();
		tppOfferPage.clickGotItCTA();

		cartPage.verifyMonthlyPaymentDevicePriceDisplayed();
		cartPage.verifyEstimatedMonthlyTextNotDisplayed();
		cartPage.verifyStickyBannerMonthlyTotalPriceDisplayed();
		cartPage.verifyStickyBannerTodayTotalPriceDisplayed();
		cartPage.verifyTaxesAndShippingDisplayedMultilineOTP();
		cartPage.verifyAutoPayMessageDisplayedMultiline();
		cartPage.verifyFindYourPricingLinkNotDisplayedInCart();
		cartPage.verifyPricingBasedOnCreditTextInCart();
	}

	/**
	 * US586082 [Continued] BYOD: Updated SIM name should reflect on Order Details
	 * (Voice)
	 * 
	 * T1442624: Validate the device SIM name change is reflected on Order Details -
	 * All plans
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testSimDetailsInOrderDetailsModel(TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US586082    BYOD Scenario - Updated Sim Starter Kit name and pricing as line item for Handset(Voice)  on Order details Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones page should be displayed");
		Reporter.log("3. Select SimStarterKit | PDP page should be displayed");
		Reporter.log("4. Verify SIM Card Name | SIM Card Name should be displayed");
		Reporter.log("5. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("6. Verify Selected plan is Magenta | Magenta plan should be selected");
		Reporter.log(
				"7. Verify Line price(SSK price)in one time payment section | Line price should be displayed in Onetime payment section");
		Reporter.log("8. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("9. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log(
				"10. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("11. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("12. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("13. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log(
				"14. Verify pre screen page after loading page and click Continue CTA on offer page | CartPage should be displayed with updated price");
		Reporter.log("15. Click on View Order Details link in Sticky banner | Order details model should be displayed");
		Reporter.log(
				"16. Verify SimStarterKit Name | SimStarterKit Name is SIM CARD should be displayed View Order Details Model");
		Reporter.log(
				"17. Verify  SimStarterKit price is reference in the line item and added to the Today Line total | SimStarterKit price should be listed in the Line total");
		Reporter.log(
				"18. Verify SimStarterKit price is added to the Cart Total Today price | SimStarterKit price should be added to the Cart Total Today price");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.verifyCRPorAwesomePricesInPLP();
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifySimKitName();
		pdpPage.clickOnAddToCartBtn();
		pdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifySimStaterKitPriceDisplayed();
		cartPage.verifyMagentaPlanSelected();
		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		TPPOfferPage tPPOfferPage = new TPPOfferPage(getDriver());
		tPPOfferPage.verifyTPPOfferPage();
		tPPOfferPage.clickGotItCTA();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifySimKitPriceOnOrderDetailsModalDisplayed();
		cartPage.verifySimKitNameOnOrderDetailsModalDisplayed();
		cartPage.verifyTotalPriceOnOrderDetailedModal();

	}

	/**
	 * US619004: TPP - Disable Accessories only in cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testContinueButtonIsDisabledWhenUserAddedOnlyAccessoriesToCart(TMNGData tMNGData) {
		Reporter.log("Test Case :US619004: TPP - Disable Accessories only in cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Accessories Link | Accessories page should be displayed");
		Reporter.log("3. Select on Accessories device | Accessories PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log(
				"5. Verify selected Accessory device in cart page | Only Selected Accessory device should be displayed in cart page");
		Reporter.log("6. Verify Continue button | Continue button should be disabled in cart page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToAccessoryPLP(tMNGData);
		PlpPage accessoriesPage = new PlpPage(getDriver());
		accessoriesPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryDetailsPage = new PdpPage(getDriver());
		accessoryDetailsPage.verifyAccessoriesPdpPageLoaded();
		accessoryDetailsPage.clickOnAddToCartBtn();
		accessoryDetailsPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		if (getTogglevalue("isV3AccessoryOnlyDisabled").equals("true")) {
			cartPage.verifyCartContinueBtnisDisabled();
		} else {
			cartPage.verifyCartContinueBtnisEnabled();
		}
	}

	/**
	 * US619004: TPP - Disable Accessories only in cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testContinueButtonIsDisabledWhenUserAddedOnlyWearableToCart(TMNGData tMNGData) {
		Reporter.log("Test Case :US619004: TPP - Disable Accessories only in cart");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Watches Link | Wearable page should be displayed");
		Reporter.log("3. Select on Wearable device | Wearable PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log(
				"5. Verify selected Wearable device in cart page | Only Selected Wearable device should be displayed in cart page");
		Reporter.log("6. Verify Continue button | Continue button should be disabled in cart page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickOnWatchesLink();
		PlpPage wearablePlpPage = new PlpPage(getDriver());
		wearablePlpPage.verifyWatchesPlpPageLoaded();
		wearablePlpPage.clickDeviceWithAvailability(tMNGData.getWatchName());
		PdpPage wearablePdpPage = new PdpPage(getDriver());
		wearablePdpPage.verifyWatchesPdpPageLoaded();
		wearablePdpPage.clickOnAddToCartBtn();
		wearablePdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.verifyCartContinueBtnisDisabled();
	}

	/**
	 * US541077: Show Add ons and Extras Modal C632422: Validate the Eligible Data
	 * Socs/Services for Magenta Plan in Recommended and Others
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAddonsAndExtrasServicesModal(TMNGData tMNGData) {
		Reporter.log("Test Case :US541077: Show Add ons and Extras Modal");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones page should be displayed");
		Reporter.log("3. Select Device | Device PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");

		Reporter.log("5. Select 'Add a tablet' from cart page | Tablets plp Page should be displayed.");
		Reporter.log("6. Select any tablet device from PLP  | Tablet device PDP page should be displayed");
		Reporter.log("7. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log("8. Select 'Add a wearable' from cart page | Watches plp Page should be displayed.");
		Reporter.log("9. Select any device from PLP  | Device PDP page should be displayed");
		Reporter.log("10. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log("11. Select 'Add a phone' from cart page | Phones page should be displayed");
		Reporter.log("12. Select Sim Card | PDP page should be displayed");
		Reporter.log("13. Click on Add to cart button | Cart page should be displayed");

		Reporter.log("14. Select 'Add a tablet' from cart page | PLP should be displayed");
		Reporter.log("15. Select Sim Card | PDP page should be displayed");
		Reporter.log("16. Click on Add to cart button | Cart page should be displayed");

		Reporter.log("17. Verify selected device in cart page | Only Selected device should be displayed in cart page");
		Reporter.log(
				"18. Verify Add ons and Extras section | Add ons and Extras section should be displayed in cart page");
		Reporter.log(
				"19. Verify SeeMore link in Add ons and Extras section | See More Link should be displayed in Add ons and Extras section");
		Reporter.log("20. Click on SeeMore link | Add ons service ZIP Code model should be displayed");
		Reporter.log("21. Enter valid ZIP code and Click on Next button | Select Service model should be displayed");
		Reporter.log("22. Verify Recommended services | Recommended services should be displayed");
		Reporter.log("23. Verify Updated button | Update button should be disabled");
		Reporter.log("24. Select any eligible service | Service should be selected");
		Reporter.log("25. Verify Updated button | Update button should be enabled");
		Reporter.log("26. Verify Other services | Other services should be displayed");
		Reporter.log("27. Select any eligible service | Service should be selected");
		Reporter.log(
				"28. Click on Service Model Close(X) icon | Service Model should be closed and Cart Page should be displayed");

		Reporter.log(
				"29. Verify Add ons and Extras section for Tablet | Add ons and Extras section should be displayed for tablet");
		Reporter.log(
				"30. Verify SeeMore link in Add ons and Extras section for Tablet | See More Link should be displayed for tablet on Add ons and Extras section");
		Reporter.log("31. Click on SeeMore link for Tablet | Select Service model should be displayed");
		Reporter.log("32. Verify Recommended services | Recommended services should be displayed");
		Reporter.log("33. Verify Updated button | Update button should be disabled");
		Reporter.log("34. Select any eligible service | Service should be selected");
		Reporter.log("35. Verify Updated button | Update button should be enabled");
		Reporter.log(
				"36. Click on Service Model Close(X) icon | Service Model should be closed and Cart Page should be displayed");

		Reporter.log(
				"37. Verify Add ons and Extras section for Wearable | Add ons and Extras section should be displayed for wearable");
		Reporter.log(
				"38. Verify SeeMore link in Add ons and Extras section for wearable | See More Link should be displayed for wearable on Add ons and Extras section");
		Reporter.log("39. Click on SeeMore link for wearable | Select Service model should be displayed");
		Reporter.log("40. Verify Recommended services | Recommended services should be displayed");
		Reporter.log("41. Verify Updated button | Update button should be disabled");
		Reporter.log("42. Select any eligible service | Service should be selected");
		Reporter.log("43. Verify Updated button | Update button should be enabled");
		Reporter.log(
				"44. Click on Service Model Close(X) icon | Service Model should be closed and Cart Page should be displayed");

		Reporter.log(
				"45. Verify Add ons and Extras section for Byod | Add ons and Extras section should be displayed for Byod");
		Reporter.log(
				"46. Verify SeeMore link in Add ons and Extras section for Byod | See More Link should be displayed for Byod on Add ons and Extras section");
		Reporter.log("47. Click on SeeMore link for wearable | Select Service model should be displayed");
		Reporter.log("48. Verify Recommended services | Recommended services should be displayed");
		Reporter.log("49. Verify Updated button | Update button should be disabled");
		Reporter.log("50. Select any eligible service | Service should be selected");
		Reporter.log("51. Verify Updated button | Update button should be enabled");
		Reporter.log(
				"52. Click on Service Model Close(X) icon | Service Model should be closed and Cart Page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.addAWearabletoCartFromCartPage(tMNGData);
		cartPage.addAByodPhoneToCartFromCartPageFromPLP(tMNGData);
		cartPage.addAByodTabletToCartFromCartPageFromPLP(tMNGData);

		cartPage.verifyAddOnExtrasHeaderDisplayedForPhone();
		cartPage.verifySeeMoreOptionsCTADisplayedForPhone();
		cartPage.clickSeeMoreOptionsCTAAddOnsAndExtrasForPhone();
		cartPage.verifyEnterZipModal();
		cartPage.enterYourZIPCode(tMNGData.getZipcode());
		cartPage.clickNextCTAOnZipCodeModal();
		cartPage.verifyProtectionModal();
		cartPage.verifyRecommendedServiceHeaderDisplayed();
		cartPage.verifyRecommendedServiceDisplayed();
		cartPage.verifyUpdateCTAOnAddOnsModalIsDisabled();
		cartPage.selectServiceOption();
		cartPage.verifyRecommendedServicesCheckboxSelected();
		cartPage.verifyUpdateCTAOnAddOnsModalIsEnabled();
		cartPage.verifyOtherServicesDisplayed();
		cartPage.clickSelectServicesCloseIcon();
		cartPage.verifyCartPageLoaded();

		cartPage.verifyAddOnExtrasHeaderDisplayedForTablet();
		cartPage.verifySeeMoreOptionsCTADisplayedForTablet();
		cartPage.clickSeeMoreOptionsCTAAddOnsAndExtrasForTablet();
		cartPage.verifyProtectionModal();
		cartPage.verifyRecommendedServiceHeaderDisplayed();
		cartPage.verifyRecommendedServiceDisplayed();
		cartPage.verifyUpdateCTAOnAddOnsModalIsDisabled();
		cartPage.selectServiceOption();
		cartPage.verifyRecommendedServicesCheckboxSelected();
		cartPage.verifyUpdateCTAOnAddOnsModalIsEnabled();
		cartPage.clickSelectServicesCloseIcon();
		cartPage.verifyCartPageLoaded();

		cartPage.verifyAddOnExtrasHeaderDisplayedForWearable();
		cartPage.verifySeeMoreOptionsCTADisplayedForWearable();
		cartPage.clickSeeMoreOptionsCTAAddOnsAndExtrasForWearable();
		cartPage.verifyProtectionModal();
		cartPage.verifyRecommendedServiceHeaderDisplayed();
		cartPage.verifyRecommendedServiceDisplayed();
		cartPage.verifyUpdateCTAOnAddOnsModalIsDisabled();
		cartPage.selectServiceOption();
		cartPage.verifyRecommendedServicesCheckboxSelected();
		cartPage.verifyUpdateCTAOnAddOnsModalIsEnabled();
		cartPage.clickSelectServicesCloseIcon();
		cartPage.verifyCartPageLoaded();

		cartPage.verifyAddOnExtrasHeaderDisplayedForPhoneSIM();
		cartPage.verifySeeMoreOptionsCTADisplayedForPhoneSIM();
		cartPage.clickSeeMoreOptionsCTAAddOnsAndExtrasForPhoneSIM();
		cartPage.verifyProtectionModal();
		cartPage.verifyRecommendedServiceHeaderDisplayed();
		// cartPage.verifyRecommendedServiceDisplayed();
		cartPage.verifyUpdateCTAOnAddOnsModalIsDisabled();
		cartPage.selectServiceOption();
		cartPage.verifyRecommendedServicesCheckboxSelected();
		cartPage.verifyUpdateCTAOnAddOnsModalIsEnabled();
		cartPage.clickSelectServicesCloseIcon();
		cartPage.verifyCartPageLoaded();

		cartPage.verifyAddOnExtrasHeaderDisplayedForTabletSIM();
		cartPage.verifySeeMoreOptionsCTADisplayedForTabletSIM();
		cartPage.clickSeeMoreOptionsCTAAddOnsAndExtrasForTabletSIM();
		cartPage.verifyProtectionModal();
		cartPage.verifyRecommendedServiceHeaderDisplayed();
		// cartPage.verifyRecommendedServiceDisplayed();
		cartPage.verifyUpdateCTAOnAddOnsModalIsDisabled();
		cartPage.selectServiceOption();
		cartPage.verifyRecommendedServicesCheckboxSelected();
		cartPage.verifyUpdateCTAOnAddOnsModalIsEnabled();
		cartPage.clickSelectServicesCloseIcon();
		cartPage.verifyCartPageLoaded();
	}

	/**
	 * US617509: TMNG: PRW: Accessories <$49 that are duplicated do not change to
	 * EIP when total is >= $49 with Phone in cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAccessorysEIPPrice(TMNGData tMNGData) {
		Reporter.log(
				"Test Case :US617509: TMNG: PRW: Accessories <$49 that are duplicated do not change to EIP  when total is >= $49 with Phone in cart");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones page should be displayed");
		Reporter.log("3. Select Device | Device PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Verify device in cart page | Device should be displayed in cart page");
		Reporter.log("6. Click 'Add an accessory' cart | Accessory plp page should  be displayed");
		Reporter.log("7. Select accessory device which is < $49 | Accessory PDP should be displayed");
		Reporter.log("8. Verify accessory device < $49 | Selected device should be < $49");
		Reporter.log("9. Click on Add to cart button | Cart page should be displayed");
		Reporter.log("10. Verify accessory device in cart page | Accessory device should be displayed in cart page");
		Reporter.log("11. Verify accessory device FRP price | Accessory device FRP price should be displayed");
		Reporter.log("12. Verify accessory device EIP price | Accessory device EIP price should not be displayed");
		Reporter.log(
				"13. Click on Accessory duplicate link Till the Accessorys sum is >= $49 | Accessory price should be >= $49");
		Reporter.log("14. Verify accessory device EIP price | Accessory device EIP price should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
	}

	/**
	 * US590222:TEST ONLY TPP - Update Order Details button in cart and remove CTA
	 * in Checkout
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testOrderDetailsTotalPricesForTPPFlow(TMNGData tMNGData) {
		Reporter.log("US590222:TEST ONLY TPP - Update Order Details button in cart and remove CTA in Checkout");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart | Cart page should be displayed");
		Reporter.log("5. Click 'Add an accessory' cart | Accessory plp page should  be displayed");
		Reporter.log("6. select any accessory device | Accessory PDP page should be displayed");
		Reporter.log("7. Click on Add to Cart | Cart page should be displayed");
		Reporter.log("8. Verify View order details link | View order details link should be displayed");
		Reporter.log("9. Click on View order details link | View order details model should be displayed");
		Reporter.log(
				"10. Verify Estimated Due Today price for added device level | Estimated Due Today price should be displayed device level");
		Reporter.log(
				"11. Verify Estimated Monthly price for added device level | Estimated Monthly price should be displayed device level");
		Reporter.log(
				"12. Save Estimated Due Today Total price in V1 | Estimated Due Today price should be saved in V1");
		Reporter.log("13. Save Estimated Monthly Total price in V2 | Estimated Monthly price should be saved in V2");
		Reporter.log("14. Click on close(X) icon for Order Details model | Cartpage should be saved");
		Reporter.log("15. Click on Find my price CTA | PreScreen Intro page should be displayed");
		Reporter.log("16. Click on Lets go CTA | PreScreen form should be displayed");
		Reporter.log("17. Fill the mandetery field in Prescreen form | Mandetery fields should be filled");
		Reporter.log("18. Click on Find my price CTA | Pre-screen loading model should be displayed");
		Reporter.log("19. Click on GotIt CTA button in Offer page | Cart page should be displayed");
		Reporter.log("20. Verify updated price | Updated price should be displayed should be displayed");
		Reporter.log("21. Verify View order details link | View order details link should be displayed");
		Reporter.log(
				"22. Verify Due Today price for added device level | Due Today price should be displayed device level");
		Reporter.log(
				"23. Verify Monthly price for added device level | Monthly price should be displayed device level");
		Reporter.log("24. Save Today Total price in V3 | Today price should be saved in V3");
		Reporter.log("25. Compare Today price | Today price should be updated(V1 not equal to V3)");
		Reporter.log("26. Save Monthly Total price in V4 | Monthly price should be saved in V4");
		Reporter.log("27. Compare Monthly price | Monthly price should be updated(V2 not equal to V4)");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();

		cartPage.clickOnAddAnAccessoryLinkOnCart();
		PlpPage accessoriesPlpPage = new PlpPage(getDriver());
		accessoriesPlpPage.verifyAccessoriesPlpPageLoaded();
		accessoriesPlpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoriesPdpPage = new PdpPage(getDriver());
		accessoriesPdpPage.verifyAccessoriesPdpPageLoaded();
		accessoriesPdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyViewOrderDetailsLinkDisplayed();
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.verifyTodayPriceOrderDetailModel();
		cartPage.verifyMonthlyPriceOrderDetailModel();
		Double V1 = cartPage.getTotalTodaypriceAtOderDetailPage();
		Double V2 = cartPage.getMonthlyCapturedPriceForAccessory();
		cartPage.clickOnCloseIconAtOrderDetailPage();
		cartPage.verifyCartPageLoaded();
		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		tppPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		TPPOfferPage tPPOfferPage = new TPPOfferPage(getDriver());
		tPPOfferPage.verifyTPPOfferPage();
		tPPOfferPage.clickGotItCTA();
		cartPage.verifyCartPageLoaded();
		cartPage.verifyViewOrderDetailsLinkDisplayed();
		cartPage.verifyDeviceUnderMonthlyPaymentDisplayedMultiline();
		cartPage.verifyTodayPriceUnderMonthlyPaymentSection();
		Double V3 = Double.parseDouble(cartPage.getTotalTodayPriceInStickyBanner());
		Double V4 = Double.parseDouble(cartPage.getTotalMonthlyPriceStickyBanner());
		cartPage.compareTwoDoubleValuesNotEqual(V1, V3);
		cartPage.compareTwoDoubleValuesNotEqual(V2, V4);

	}

	/**
	 * US586010:SSK fee will no longer display on Order Details for a new device
	 * purchase
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testSSKfeeDetailsOnOrderDetailsPageForDevices(TMNGData tMNGData) {
		Reporter.log("US586010:SSK fee will no longer display on Order Details for a new device purchase");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("8. Click on Add a Wearable CTA | Wearable PLP should be displayed");
		Reporter.log("9. Select any Wearable | PDP page should be displayed");
		Reporter.log("10. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("5. Click on Add a Tablet CTA | Tablet PLP should be displayed");
		Reporter.log("6. Select any Tablet | PDP page should be displayed");
		Reporter.log("7. Click on Add to Cart| Cart page should be displayed");
		Reporter.log("17. Click on View order details Link on sticky banner | Order details model should be displayed");
		Reporter.log(
				"18. Verify SSK Fee in One Time Payment Section | SSK fee should not be displayed as line breakdown");
		Reporter.log("19. Verify Line Total | Line total should be matched");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.addAWearabletoCartFromCartPage(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		cartPage.clickOrderDetailsCollapseIcon();
		cartPage.verifySimStarterKitTextNotDisplayed();

		Double V1 = cartPage.getOrderDetailsModalTotalLinePriceForFirstItem();
		Double V2 = cartPage.getOrderDetailsLineMonthlyTotalPriceForFirstItem();
		cartPage.verifyMonthlyPaymentsLinePrices(V1, V2);
		Double V3 = cartPage.getOrderDetailsModalTotalLinePriceForSecondItem();
		Double V4 = cartPage.getOrderDetailsLineMonthlyTotalPriceForSecondItem();
		cartPage.verifyMonthlyPaymentsLinePrices(V3, V4);

		Double V5 = cartPage.getOrderDetailsModalTotalLinePriceForThirdItem();
		Double V6 = cartPage.getOrderDetailsLineMonthlyTotalPriceForThirdItem();
		cartPage.verifyMonthlyPaymentsLinePrices(V5, V6);

		cartPage.clickOnCloseIconAtOrderDetailPage();
		cartPage.verifyCartPageLoaded();
	}

	/**
	 * US578282:TMNG - Show dynamic messaging to switch from Magenta to Magnenta+
	 * rate plan in Cart banner
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAuthorableBannerWhenUserSwitchMagentaPlanToMagentaPlusPlan(TMNGData tMNGData) {
		Reporter.log(
				"US578282:TMNG - Show dynamic messaging to switch from Magenta to Magnenta+ rate plan in Cart banner");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. select any device | PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart | Cart page should be displayed");
		Reporter.log(
				"5. Select 'PlusUp' data soc for 1st line | Magenta banner should not be displayed when Plus Up is only selected for 1st line");
		Reporter.log("6. Click 'Add a phone' from cart page | Cell Phones page should be displayed");
		Reporter.log("7. select any device | PDP page should be displayed");
		Reporter.log("8. Click on Add to Cart | Cart page should be displayed");
		Reporter.log("9. Select 'PlusUp' data soc for 2nd line | Magenta banner should be displayed");
		Reporter.log(
				"10. Verify  banner at the top of cart with authorable text,i.e(Consider switching to Magenta+ to save money. Netflix benefits and hotspot service are limited. Learn more) | Authorable banner should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickFirstAddOn();
		cartPage.verifyDynamicMessageForSwitchPlanInCartBannerNotDisplayed();
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
//		cartPage.verifyMagentaPlanSelected();
//		cartPage.verifyDefaultAddOnsForMegentaPlan();
		// cartPage.clickFirstAddOnForSecondLine();
		cartPage.verifyDynamicMessageForSwitchPlanInCartBanner();

	}

	/**
	 * US579533:TMNG - Create Retrieve Cart Container on Empty Cart C632915
	 * :Validate the Invalid email functionality for Retrieve cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testRetrieveCartValidationsWithInValidEmailId(TMNGData tMNGData) {
		Reporter.log("US579533:TMNG - Create Retrieve Cart Container on Empty Cart");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO cart | TMO cart page should be launched");
		Reporter.log("2. Verify Retrieve cart header | Retrieve cart header should be displayed");
		Reporter.log(
				"3. Verify Retrieve cart text like 'If you previously saved a cart, enter your email to retrieve it' | Retrieve cart text should be displayed ");
		Reporter.log("4. Enter invalid email-id  | Red color exclamation icon should be displayed");
		Reporter.log("5. Verify 'Please enter a valid e-mail address' text | Error message should be displayed");
		Reporter.log("6. Click on 'Retrieve cart' CTA | Order details modeal should be disabled");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage emptyCartPage = navigateToEmptyCart(tMNGData);
		emptyCartPage.verifyRetrieveCartHeader();
		emptyCartPage.verifyRetrieveCartText();
		emptyCartPage.enterInvalidEmailinSaveCart(tMNGData);
		emptyCartPage.verifyInvalidEmailErrorRedExclamationIcon();
		emptyCartPage.verifyErrorMessageForEmailField();
	}

	/**
	 * US607923: Call Me Link, Modal and Confirmation - HTML Integration C642887 :
	 * Validate that the User is ll be presented with a confirmation message with an
	 * estimated call back time after clicking on CALL ME NOW cta
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testToValidateConfirmationMessageWhenUserClickOnCallMeLink(TMNGData tMNGData) {
		Reporter.log("US607923: Call Me Link, Modal and Confirmation - HTML Integration");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones page should be displayed");
		Reporter.log("3. Select Device | Device PDP page should be displayed");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");

		Reporter.log("5. Select 'Add a tablet' from cart page | Tablets plp Page should be displayed.");
		Reporter.log("6. Select any tablet device from PLP  | Tablet device PDP page should be displayed");
		Reporter.log("7. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log("8. Select 'Add a wearable' from cart page | Watches plp Page should be displayed.");
		Reporter.log("9. Select any device from PLP  | Device PDP page should be displayed");
		Reporter.log("10. Click Add to cart in PDP | Cart Page should be displayed.");

		Reporter.log("11. Select 'Add a phone' from cart page | Phones page should be displayed");
		Reporter.log("12. Select Sim Card | PDP page should be displayed");
		Reporter.log("13. Click on Add to cart button | Cart page should be displayed");

		Reporter.log("14. Select 'Add a tablet' from cart page | PLP should be displayed");
		Reporter.log("15. Select Sim Card | PDP page should be displayed");
		Reporter.log("16. Click on Add to cart button | Cart page should be displayed");

		Reporter.log(
				"17. Verify Data Soc and Protection on Add Ons and Extras | Add Ons and Extras should be displayed");
		Reporter.log("18. Verify Call Me icon | Call Me Icon should be displayed");
		Reporter.log("19. Click on Call Me icon | Personal Information section should be displayed");
		Reporter.log(
				"20. Enter First Name, Last Name,Phone no and valid Zip code | First Name, Last Name,Phone no and valid Zip code should be updated");
		Reporter.log("21. Click on 'Call Me Now' CTA | 'Call Me' modal should be displayed");
		Reporter.log("22. Verify confirmation message | Confiramtion message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.addAWearabletoCartFromCartPage(tMNGData);
		cartPage.addAByodPhoneToCartFromCartPageFromPLP(tMNGData);
		cartPage.addAByodTabletToCartFromCartPageFromPLP(tMNGData);
		cartPage.verifyAddOnExtrasHeaderDisplayedForPhone();
		cartPage.verifyAddOnExtrasHeaderDisplayedForTablet();
		cartPage.verifyAddOnExtrasHeaderDisplayedForWearable();
		cartPage.verifyAddOnExtrasHeaderDisplayedForPhoneSIM();
		cartPage.verifyAddOnExtrasHeaderDisplayedForTabletSIM();
		cartPage.verifyListOfAddOnsDisplayed();
		cartPage.verifyDataSocDisplayed();
		cartPage.verifyCallMeNowIcon();
		cartPage.clickCallMeNowIcon();
		cartPage.verifyRequestCallModel();
		cartPage.fillPersonalInfoInRequestCallModel(tMNGData);
		cartPage.clickCheckBoxInRequestCallModal();
		cartPage.clickCallMeNowButtonInRequestCallModal();
		cartPage.verifyConfirmationMessageInRequestCallModel();

	}

	/**
	 * CDCDWR-331: UNAV UNO & TMNG | V3 API | CART
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testCartCount(TMNGData tMNGData) {
		Reporter.log("Test Case : CDCDWR-331: UNAV UNO & TMNG | V3 API | CART");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones  Link | Cell Phones PLP should be displayed");
		Reporter.log("3. Click device | PDP should be displayed");
		Reporter.log("4. Add device to cart| Cart page should be displayed");
		Reporter.log("5. Click on Save Cart  | Save Cart pop up should display");
		Reporter.log("6. Verify Cart icon on top of the page| Cart icon should be displayed");
		Reporter.log(
				"7. Verify input email in a field under the message | input email in a field under the message should be displayed");
		Reporter.log("8. Click on save | Conformation Page should be displayed");
		Reporter.log("9. Click on close icon | Cart page should be displayed");
		Reporter.log("10. Click on t-mobile icon | home page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickDuplicateCTA();
		cartPage.clickOnSaveCart();
		cartPage.verifyCartIconOnSaveCartModel();
		String email = cartPage.getEmailWithDate(tMNGData);
		cartPage.enterEmailWithDateInSaveCart(email);
		cartPage.clickSaveButtonOnSaveCartModel();
		cartPage.verifySaveCartReplaceModalAndClickSaveForWebSaveCart();
		cartPage.verifyConfirmationMessageonSaveCartModel();
		cartPage.clickCloseOnSaveCartModel();
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnTMobileIcon();
	}

	/**
	 * US579536 - TMNG - Create Customer Care container on Empty Cart
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyCustomerCarecontainerOnEmptyCart(TMNGData tMNGData) {
		Reporter.log("US579536 - TMNG - Create Customer Care container on Empty Cart");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO cart | TMO cart page should be launched");
		Reporter.log("2. Verify Customer Care container header | Customer Care container header should be displayed");
		Reporter.log(
				"3. Verify text 'Your T-Mobile rep will help you find the right fit.' below header | 'Your T-Mobile rep will help you find the right fit.' text should be displayed.");
		Reporter.log("4. Verify Headset icon | Headset icon should be displayed");
		Reporter.log(
				"5. Verify Call Us text next to headset icon | Call Us text next to headset icon should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage emptyCartPage = navigateToEmptyCart(tMNGData);
		emptyCartPage.verifyCustomerCareContainerHeaderOnEmptycart();
		emptyCartPage.verifyCustomerCareContainerDescTextOnEmptycart();
		emptyCartPage.verifyHeadsetIconOnCustomerCareContainerOnEmptycart();
		emptyCartPage.verifyCallUsTextandNumberOnCustomerCareContainerOnEmptycart();
	}

	/**
	 * US640769- TPP R2 - UI - Change Line Deposit Text to Refundable Deposit
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testVerifyLineDepositTextChangedToRefundableDeposit(TMNGData tMNGData) {
		Reporter.log(" US640769- TPP R2 - UI - Change Line Deposit Text to Refundable Deposit");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO cart | TMO cart page should be launched");
		Reporter.log("2. Click on Phones  Link | Cell Phones PLP should be displayed");
		Reporter.log("3. Click device | PDP page should be displayed");
		Reporter.log("4. Add device to cart| Cart page should be displayed");
		Reporter.log("5. verify duplicate CTA | Duplicate CTA should be displayed in cart page");
		Reporter.log("6. Click on duplicate CTA | same devices  should be displayed in line 1 and 2 ");
		Reporter.log("7. Check Line deposit text in each line | Line deposit text should be displayed in each line");
		Reporter.log(
				"8. Verify line deposit amount in each line | Line deposit amount should be displayed in each line");
		Reporter.log(
				"9. Check Refundable Deposit text in each line | Refundable Deposit text should be displayed in each line");
		Reporter.log(
				"10. Verify Refundable Deposit amount in each line | Refundable Deposit amount should be displayed in each line");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyDuplicateCTA();
		cartPage.clickDuplicateCTA();
		cartPage.verifyDuplicateLinesOnCart();
		cartPage.verifyRefundableDepositText();
		cartPage.verifyRefundableDepositAmount();

	}

}