package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.global.PhonePage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.CheckOrderPage;
import com.tmobile.eservices.qa.pages.shop.IdentityReviewIntroductionPage;
import com.tmobile.eservices.qa.pages.shop.OrderConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.PhonePages;
import com.tmobile.eservices.qa.pages.shop.ShipmentTrackerPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class OrderStatusPageTest extends ShopCommonLib{
	
	/**
	 * US268076: Order Status - Update "New Device" to New Item/New Items
	 * US302347: Order Status - Update "New Device" to New Item
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.RELEASE_READY})
	public void verifyNewDeviceTextUpdate(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US268076, US302347:Order Status - Update New Device to New Item/New Items");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4: Click on phone link | Phone page should be displayed");
		Reporter.log("5: Click on Order status link | Order status page should be displayed");
		Reporter.log("6: Verify new device text | New device text should be updated");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToPhonePage(myTmoData);
		
		PhonePage phonePage = new PhonePage(getDriver());
		phonePage.verifyPhonePage();
		phonePage.clickCheckOrderStatusLink();
		
		CheckOrderPage checkorderPage = new CheckOrderPage(getDriver());
		checkorderPage.verifyCheckOrderPage();
	    checkorderPage.verifyNewDeviceTextUpdate();
	}


	/**
	 * US415459: AAL Order Status - order details bar	 	 
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void verifyAALOrderStatusShippingAndSKUDetailsBYODFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US415459: AAL Order Status - order details bar");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Use my own phone' option | Rate page should be displayed");
		Reporter.log("7. Click 'Continue' button on Rate plan page | Cart Page should be displayed");
		Reporter.log("8. Click on 'Continue to shipping' CTA on cart page | Shipping Information section should be displayed");
		Reporter.log("9. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
		Reporter.log("10. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
		Reporter.log("11. Click on Accept & Continue CTA | Order should be processed and Thank You Confirmation page should be displayed");
		Reporter.log("12. Click on See Order Status | Order Status Page should be displayed");
		Reporter.log("13. Verify AAL Order presence on Order Status page | AAL Orders should be displayed");
		Reporter.log("14. Verify AAL Order details bar on Order Status page | AAL Order autorable text with order number should be displayed");
		Reporter.log("15. Verify AAL Order Shipping status | Shipping status(Shipped/Delivered/In Progress...) should be displayed");
		Reporter.log("16. Verify AAL Order Shipping Date | Shipping date should be displayed");
		Reporter.log("17. Verify & Click Track order CTA | User should be redirected to Shipment Tracker Page");
		Reporter.log("18. Verify AAL Order SKU Details | SKU Name, Image, Description etc. details should be displayed(If applicable)");
		Reporter.log("19. Verify SIM Kit Info | Ensure that no device is displayed, only SIM details(Image & Name) are available");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

	}

	/**
	 * US415459: AAL Order Status - order details bar	 
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.SPRINT})
	public void verifyAALOrderStatusShippingAndSKUDetailsBuyNewPhoneFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US415459: AAL Order Status - order details bar");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy New Phone' option | Product list page should be displayed");
		Reporter.log("7. Select a Product from PLP page | Product details page should be displayed");
		Reporter.log("8. Click 'Continue' button on PDP page | Rate Plan page should be displayed");
		Reporter.log("9. Click 'Continue' button on Rate plan page | Cart Page should be displayed");
		Reporter.log("10. Click on 'Continue to shipping' CTA on cart page | Shipping Information section should be displayed");
		Reporter.log("11. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
		Reporter.log("12. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
		Reporter.log("13. Click on Accept & Continue CTA | Order should be processed and Thank You Confirmation page should be displayed");
		Reporter.log("14. Click on See Order Status | Order Status Page should be displayed");
		Reporter.log("15. Verify AAL Order presence on Order Status page | AAL Orders should be displayed");
		Reporter.log("16. Verify AAL Order details bar on Order Status page | AAL Order autorable text with order number should be displayed");
		Reporter.log("17. Verify AAL Order Shipping status | Shipping status(Shipped/Delivered/In Progress...) should be displayed");
		Reporter.log("18. Verify AAL Order Shipping Date | Shipping date should be displayed");
		Reporter.log("19. Verify & Click Track order CTA | User should be redirected to Shipment Tracker Page");
		Reporter.log("20. Verify AAL Order SKU Details | SKU Name, Image, Description etc. details should be displayed(If applicable)");
		Reporter.log("21. Verify Device & SIM Kit Info | Ensure that device is displayed along with SIM details(Image & Name)");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

	}
		
	/**
	 * US480323: AAL Order Status - Order Details (status and total) 
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL})
	public void testOrderStatusAndTotalInOrderStatusPageUsingBuyNewPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US480323: AAL Order Status - Order Details (status and total)");
		Reporter.log("Data Condition | PAH User with AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy New Phone' option | PLP page should be displayed");
		Reporter.log("7. Select a Product from PLP page | PDP page should be displayed");
		Reporter.log("8. Click 'Continue' button on PDP page | Rate Plan page should be displayed");
		Reporter.log("9. Click 'Continue' button on Rate plan page | Cart Page should be displayed");		
		Reporter.log("10. Store Due today total price in V1 | Due today total should be displayed in V1");
		Reporter.log("11. Click on 'Continue to shipping' CTA on cart page | Shipping Information section should be displayed");
		Reporter.log("12. Click on 'Continue to Payment' CTA on Shipping information section | Payment Information section should be displayed");
		Reporter.log("13. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
		Reporter.log("14. Click on Accept & Continue CTA | Order should be processed and Thank You Confirmation page should be displayed");
		Reporter.log("15. Click on See Order Status link | Order Status Page should be displayed");		
		Reporter.log("16. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
		Reporter.log("17. Verify Order Ships by date and Date formate should be in MM/DD/YYYY | Order Ships by date should be displayed in correct formate");
		Reporter.log("18. Verify Order 'Track Items' link text | Order 'Track Items' link text should be displayed");
		Reporter.log("19. Verify 'More details' link text | 'More details' link text should be displayed");
		Reporter.log("20. Verify 'ORDER TOTAL' text | 'ORDER TOTAL' text should be displayed");
		Reporter.log("21. Verify 'ORDER TOTAL' price with $ sign | 'ORDER TOTAL' price should be displayed with $ sign");
		Reporter.log("22. Store 'ORDER TOTAL' price in V2 | 'ORDER TOTAL' price should be stored in V2");
		Reporter.log("23. Compare 'ORDER TOTAL' price(V1 and V2) | 'ORDER TOTAL' price (V1 = V2) should be equal");		
		Reporter.log("24. Click Order 'Track Items' link | Shipment Tracker page should be displayed");		
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		

		navigateToCheckOrderPageNonBYOD(myTmoData);
		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyCheckOrderPage();
		checkOrderPage.verifyOrderStatusOverallNonBYOD();
		checkOrderPage.orderShippingDateNonBYOD();
		checkOrderPage.verifyTrackItemsLinkNonBYOD();
		checkOrderPage.verifyMoreDetailsLinkNonBYOD();
		checkOrderPage.verifyOrderTotalTextNonBYOD();
		checkOrderPage.verifyOrderTotalAmountNonBYOD();
		checkOrderPage.clickTrackItemsLinkNonBYOD();
		
		ShipmentTrackerPage shipmentTrackerPage = new ShipmentTrackerPage(getDriver());
		shipmentTrackerPage.verifyShipmentTrackerPage();
	}
	
	/**
	 * US480323: AAL Order Status - Order Details (status and total) 
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL})
	public void testOrderStatusAndTotalInOrderStatusPageUsingUseMyOwnPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US480323: AAL Order Status - Order Details (status and total)");
		Reporter.log("Data Condition | PAH User with AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Use my own phone' option | Rate page should be displayed");
		Reporter.log("7. Click 'Continue' button on Rate plan page | Cart Page should be displayed");
		Reporter.log("8. Store Due today total price in V1 | Due today total should be displayed in V1");		
		Reporter.log("9. Click on 'Continue to shipping' button | Shipping Information section should be displayed");
		Reporter.log("10. Click on 'Continue to Payment' button | Payment Information section should be displayed");
		Reporter.log("11. Provide Payment information details and Accept the Customer Agreement by selecting the Checkbox | Accept & Continue CTA should be enabled");
		Reporter.log("12. Click on Accept & Continue CTA | Order should be processed and Thank You Confirmation page should be displayed");
		Reporter.log("13. Click on See Order Status | Order Status Page should be displayed");		
		Reporter.log("14. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
		Reporter.log("15. Verify Order Ships by date and Date formate should be in MM/DD/YYYY | Order Ships by date should be displayed in correct formate");
		Reporter.log("16. Verify Order 'Track Items' link text | Order 'Track Items' link text should be displayed");
		Reporter.log("17. Verify 'More details' link text | 'More details' link text should be displayed");
		Reporter.log("18. Verify 'ORDER TOTAL' text | 'ORDER TOTAL' text should be displayed");
		Reporter.log("19. Verify 'ORDER TOTAL' price with $ sign | 'ORDER TOTAL' price should be displayed with $ sign");
		Reporter.log("20. Store 'ORDER TOTAL' price in V2 | 'ORDER TOTAL' price should be stored in V2");
		Reporter.log("21. Compare 'ORDER TOTAL' price(V1 and V2) | 'ORDER TOTAL' price (V1 = V2) should be equal");		
		Reporter.log("22. Click Order 'Track Items' link | Shipment Tracker page should be displayed");		
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		
		navigateToCheckOrderPageBYOD(myTmoData);
		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyCheckOrderPage();
		checkOrderPage.verifyOrderStatusOverallBYOD();
		checkOrderPage.orderShippingDateBYOD();
		checkOrderPage.verifyTrackItemsLinkBYOD();
		checkOrderPage.verifyMoreDetailsLinkBYOD();
		checkOrderPage.verifyOrderTotalTextBYOD();
		checkOrderPage.verifyOrderTotalAmountBYOD();
		checkOrderPage.clickTrackItemsLinkBYOD();
		
		ShipmentTrackerPage shipmentTrackerPage = new ShipmentTrackerPage(getDriver());
		shipmentTrackerPage.verifyShipmentTrackerPage();
		
	}
	
	/**
	 * US480324: AAL Order Status - Order Details expanded (payment/shipped/billed)
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL})
	public void testOrderStatusMoreDetailsInOrderStatusPageUsingBuyNewPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US480324: AAL Order Status - Order Details expanded (payment/shipped/billed)");
		Reporter.log("Data Condition | PAH User with AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy New Phone' option | PLP page should be displayed");
		Reporter.log("7. Select a Product from PLP page | PDP page should be displayed");
		Reporter.log("8. Click 'Continue' button on PDP page | Rate Plan page should be displayed");
		Reporter.log("9. Click 'Continue' button on Rate plan page | Cart Page should be displayed");		
		Reporter.log("10. Click on 'Continue to shipping' button | Shipping Information section should be displayed");
		Reporter.log("11. Click on 'Continue to Payment' button | Payment Information section should be displayed");
		Reporter.log("12. Enter Credit card details | Accept and Continue button should be enabled");
        Reporter.log("13. Select Service agereement check box and Click Accept and Continue button | Order Conformation page should be displayed");
        Reporter.log("14. Click on Order staus link | Order staus page should be displayed");		
		Reporter.log("15. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
	    Reporter.log("16. Verify 'More details' link text | 'More details' link text should be displayed");
		Reporter.log("17. Click 'More details' link | 'Less details' link text should be displayed");		
		Reporter.log("18. Verify 'PAYMENT' header title | 'PAYMENT' header title should be displayed");
        Reporter.log("19. Verify 'Customer' authorable text | 'Customer' authorable text should be displayed");
        Reporter.log("20. Verify 'Customer' name | 'Customer' name should be displayed");// Only Customer header text should be displayed not name in this section
        Reporter.log("21. Verify 'T-Mobile account number' authorable text | 'T-Mobile account number' authorable text should be displayed");
        Reporter.log("22. Verify 'T-Mobile account number' number | 'T-Mobile account number' number should be displayed");
        Reporter.log("23. Verify 'Number' authorable text | 'Number' authorable text should be displayed");
        Reporter.log("24. Verify 'Number' MSISDN number | 'Number' MSISDN number should be displayed");
        Reporter.log("25. Verify 'Email' authorable text | 'Email' authorable text should be displayed");
        Reporter.log("26. Verify 'Email' with @ symbol | 'Email' should be displayed with @ symbol");
        Reporter.log("27. Verify 'Paid on' authorable text | 'Paid on' authorable text should be displayed");
        Reporter.log("28. Verify 'Paid on' date format | 'Paid on' should be displayed with date format");
        Reporter.log("29. Verify 'Paid using' authorable text | 'Paid using' authorable text should be displayed");
        Reporter.log("30. Verify 'Paid using' is displayed with card type and card last 4 digits | 'Paid using' should be displayed with card type and card last 4 digits");
        Reporter.log("31. Verify 'SHIPPED TO' header title | 'SHIPPED TO' header title should be displayed");
        Reporter.log("32. Verify First name and Last name under 'Shipped to' title | First name and Last name under 'Shipped to' title should be displayed");
        Reporter.log("33. Verify Address Line under 'Shipped to' title | Address Line under 'Shipped to' title should be displayed");
        Reporter.log("34. Verify City name, State name and Zip under 'Shipped to' title | City name, State name and Zip under 'Shipped to' title  should be displayed");
		Reporter.log("35. Verify 'BILLED TO' header title | 'BILLED TO' header title should be displayed");
        Reporter.log("36. Verify First name and Last name under 'Billed to' title | First name and Last name under 'Billed to' title should be displayed");
        Reporter.log("37. Verify Address Line under 'Billed to' title | Address Line under 'Billed to' title should be displayed");
        Reporter.log("38. Verify City name, State name and Zip under 'Billed to' title | City name, State name and Zip under 'Billed to' title  should be displayed");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToCheckOrderPageNonBYOD(myTmoData);
		CheckOrderPage checkOrder= new CheckOrderPage(getDriver());
		checkOrder.verifyMoreDetailsLinkBYOD();
		checkOrder.clickMoreDetailsLinkBYOD();
		checkOrder.verifyPaymentAuthorableTextInMoreDetails();
		checkOrder.verifyCustomerTextUnderPaymentTextInMoreDetails();
		checkOrder.verifyTAccountNumberTextUnderPaymentTextInMoreDetails();
		checkOrder.verifyTAccountNumberUnderPaymentTextInMoreDetails();
		checkOrder.verifyPhoneNumberTextUnderPaymentTextInMoreDetails();
		checkOrder.verifyPhoneNumberUnderPaymentTextInMoreDetails();
		checkOrder.verifyEmailTextUnderPaymentTextInMoreDetails();
		checkOrder.verifyEmailUnderPaymentTextInMoreDetails();
		checkOrder.verifyPaidDateTextUnderPaymentTextInMoreDetails();
		checkOrder.verifyPaidDateUnderPaymentTextInMoreDetails();
		checkOrder.verifyCreditCardTextUnderPaymentTextInMoreDetails();
		checkOrder.verifyCreditCardUnderPaymentTextInMoreDetails();
		checkOrder.verifyShippedToAuthorableTextInMoreDetails();
		checkOrder.verifyCustomerNameUnderShippedToTextInMoreDetails();
		checkOrder.verifyCustomerAdddressUnderShippedToTextInMoreDetails();
		checkOrder.verifyBilledToAuthorableTextInMoreDetails();
		checkOrder.verifyCustomerNameUnderBilledToTextInMoreDetails();
		checkOrder.verifyCustomerAddressUnderBilledToTextInMoreDetails();
	}
	
	/**
	 * US480324: AAL Order Status - Order Details expanded (payment/shipped/billed)
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL})
	public void testOrderStatusMoreDetailsInOrderStatusPageUsingUseMyOwnPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US480324: AAL Order Status - Order Details expanded (payment/shipped/billed)");
		Reporter.log("Data Condition | PAH User with AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Use my own phone' option | Rate page should be displayed");
		Reporter.log("7. Click 'Continue' button on Rate plan page | Cart Page should be displayed");		
		Reporter.log("8. Click on 'Continue to shipping' button | Shipping Information section should be displayed");
		Reporter.log("9. Click on 'Continue to Payment' button | Payment Information section should be displayed");
		Reporter.log("10. Enter Credit card details | Accept and Continue button should be enabled");
        Reporter.log("11. Select Service agereement check box and Click Accept and Continue button | Order Conformation page should be displayed");
        Reporter.log("12. Click on Order staus link | Order staus page should be displayed");		
		Reporter.log("13. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
		Reporter.log("14. Verify 'More details' link text | 'More details' link text should be displayed");
		Reporter.log("15. Click 'More details' link | 'Less details' link text should be displayed");		
		Reporter.log("16. Verify 'PAYMENT' header title | 'PAYMENT' header title should be displayed");
        Reporter.log("17. Verify 'Customer' authorable text | 'Customer' authorable text should be displayed");
        Reporter.log("18. Verify 'Customer' name | 'Customer' name should be displayed");
        Reporter.log("19. Verify 'T-Mobile account number' authorable text | 'T-Mobile account number' authorable text should be displayed");
        Reporter.log("20. Verify 'T-Mobile account number' number | 'T-Mobile account number' number should be displayed");
        Reporter.log("21. Verify 'Number' authorable text | 'Number' authorable text should be displayed");
        Reporter.log("22. Verify 'Number' MSISDN number | 'Number' MSISDN number should be displayed");
        Reporter.log("23. Verify 'Email' authorable text | 'Email' authorable text should be displayed");
        Reporter.log("24. Verify 'Email' with @ symbol | 'Email' should be displayed with @ symbol");
        Reporter.log("25. Verify 'Paid on' authorable text | 'Paid on' authorable text should be displayed");
        Reporter.log("26. Verify 'Paid on' date format | 'Paid on' should be displayed with date format");
        Reporter.log("27. Verify 'Paid using' authorable text | 'Paid using' authorable text should be displayed");
        Reporter.log("28. Verify 'Paid using' is displayed with card type and card last 4 digits | 'Paid using' should be displayed with card type and card last 4 digits");
        Reporter.log("29. Verify 'SHIPPED TO' header title | 'SHIPPED TO' header title should be displayed");
        Reporter.log("30. Verify First name and Last name under 'Shipped to' title | First name and Last name under 'Shipped to' title should be displayed");
        Reporter.log("31. Verify Address Line under 'Shipped to' title | Address Line under 'Shipped to' title should be displayed");
        Reporter.log("32. Verify City name, State name and Zip under 'Shipped to' title | City name, State name and Zip under 'Shipped to' title  should be displayed");
		Reporter.log("33. Verify 'BILLED TO' header title | 'BILLED TO' header title should be displayed");
        Reporter.log("34. Verify First name and Last name under 'Billed to' title | First name and Last name under 'Billed to' title should be displayed");
        Reporter.log("35. Verify Address Line under 'Billed to' title | Address Line under 'Billed to' title should be displayed");
        Reporter.log("36. Verify City name, State name and Zip under 'Billed to' title | City name, State name and Zip under 'Billed to' title  should be displayed");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToCheckOrderPageBYOD(myTmoData);
		CheckOrderPage checkOrder= new CheckOrderPage(getDriver());
		checkOrder.verifyMoreDetailsLinkBYOD();
		checkOrder.clickMoreDetailsLinkBYOD();
		checkOrder.verifyPaymentAuthorableTextInMoreDetails();
		checkOrder.verifyCustomerTextUnderPaymentTextInMoreDetails();
		checkOrder.verifyTAccountNumberTextUnderPaymentTextInMoreDetails();
		checkOrder.verifyTAccountNumberUnderPaymentTextInMoreDetails();
		checkOrder.verifyPhoneNumberTextUnderPaymentTextInMoreDetails();
		checkOrder.verifyPhoneNumberUnderPaymentTextInMoreDetails();
		checkOrder.verifyEmailTextUnderPaymentTextInMoreDetails();
		checkOrder.verifyEmailUnderPaymentTextInMoreDetails();
		checkOrder.verifyPaidDateTextUnderPaymentTextInMoreDetails();
		checkOrder.verifyPaidDateUnderPaymentTextInMoreDetails();
		checkOrder.verifyCreditCardTextUnderPaymentTextInMoreDetails();
		checkOrder.verifyCreditCardUnderPaymentTextInMoreDetails();
		checkOrder.verifyShippedToAuthorableTextInMoreDetails();
		checkOrder.verifyCustomerNameUnderShippedToTextInMoreDetails();
		checkOrder.verifyCustomerAdddressUnderShippedToTextInMoreDetails();
		checkOrder.verifyBilledToAuthorableTextInMoreDetails();
		checkOrder.verifyCustomerNameUnderBilledToTextInMoreDetails();
		checkOrder.verifyCustomerAddressUnderBilledToTextInMoreDetails();
	}
	
	/**
	 * US480325: AAL Order Status - Order Details expanded (Order total details)
	 * US480326: AAL Order Status - Order Details expanded (remove monthly section for legal text)
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL})
	public void testTransactionDetailsInOrderStatusPageUsingBuyNewPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US480325: AAL Order Status - Order Details expanded (Order total details)");
		Reporter.log("Test US480326: AAL Order Status - Order Details expanded (remove monthly section for legal text)");
		Reporter.log("Data Condition | PAH User with AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy New Phone' option | PLP page should be displayed");
		Reporter.log("7. Select a Product from PLP page | PDP page should be displayed");
		Reporter.log("8. Click 'Continue' button on PDP page | Rate Plan page should be displayed");
		Reporter.log("9. Click 'Continue' button on Rate plan page | Cart Page should be displayed");		
		Reporter.log("10. Click on 'Continue to shipping' button | Shipping Information section should be displayed");
		Reporter.log("11. Click on 'Continue to Payment' button | Payment Information section should be displayed");
		Reporter.log("12. Enter Credit card details | Accept and Continue button should be enabled");
        Reporter.log("13. Select Service agereement check box and Click Accept and Continue button | Order Conformation page should be displayed");
        Reporter.log("14. Click on Order staus link | Order staus page should be displayed");		
		Reporter.log("15. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
		Reporter.log("16. Verify 'More details' link text | 'More details' link text should be displayed");
		Reporter.log("17. Click 'More details' link | 'Less details' link text should be displayed");	
		Reporter.log("18. Verify 'TRANSACTION' header title text | 'TRANSACTION' header title text should be displayed");		
		Reporter.log("19. Verify 'Insurance Deductible' title text | 'Insurance Deductible' title text should be displayed");		
		Reporter.log("20. Verify 'Insurance Deductible' price and store in V1 | 'Insurance Deductible' price should be displayed and stored in V1");
		Reporter.log("21. Verify 'Deposit' title text | 'Deposit' title text should be displayed");
		Reporter.log("22. Verify 'Deposit' price and store in V2 | 'Deposit' price should be displayed and stored in V2");
		Reporter.log("23. Verify 'Taxes and Fees' title text | 'Taxes and Fees' title text should be displayed");
		Reporter.log("24. Verify 'Taxes and Fees' price and store in V3 | 'Taxes and Fees' price should be displayed and stored in V3");
		Reporter.log("25. Verify 'Shipping' title text | 'Shipping' title text should be displayed");
		Reporter.log("26. Verify 'Shipping' price and store in V4 | 'Shipping' price should be displayed and stored in V4");
		Reporter.log("27. Verify 'Total Due Today' title text | 'Total Due Today' title text should be displayed");
		Reporter.log("28. Verify 'Total Due Today' price and store in V5 | 'Total Due Today' price should be displayed and stored in V5");
		Reporter.log("29. Compare 'Total Due Today Price', i.e V5=(V1+V2+V3+V4) | 'Total Due Today' price should be equal");
		Reporter.log("30. Verify  Monthly section for legal text on Legal Place holder | Monthly section for legal text should not be displayed");
		Reporter.log("31. Click 'Less details'  | 'More details' link text should be displayed");	
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToCheckOrderPageNonBYOD(myTmoData);
		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyCheckOrderPage();
		checkOrderPage.clickMoreDetailsLinkNonBYOD();
		checkOrderPage.verifyTransactionHeaderNonBYOD();
		checkOrderPage.verifyInsuranceDeductibleTitleNonBYOD();
		checkOrderPage.verifyInsuranceDeductibleAmountNonBYOD();
		Double V1 = checkOrderPage.getInsuranceDeductibleAmountNonBYOD();
		checkOrderPage.verifyDepositeTextNonBYOD();
		checkOrderPage.verifyDepositAmountNonBYOD();
		Double V2 = checkOrderPage.getDepositAmountNonBYOD();
		checkOrderPage.verifyTaxesAndFeesTextNonBYOD();
		checkOrderPage.verifyTaxesAndFeesAmountNonBYOD();
		Double V3 = checkOrderPage.getTaxesAndFeesAmountNonBYOD();
		checkOrderPage.verifyShippingTextNonBYOD();
		checkOrderPage.verifyShippingAmountNonBYOD();
		Double V4 = checkOrderPage.getShippingAmountNonBYOD();
		checkOrderPage.verifyTotalDueTextNonBYOD();
		checkOrderPage.verifyTotalDueAmountNonBYOD();
		Double V5 = checkOrderPage.getTotalDueAmountNonBYOD();
		//checkOrderPage.compareTotalDueAmountWithAllAmounts(V1, V2, V3, V4, V5);
		checkOrderPage.verifyMonthlyLegalTextNonBYOD();
		checkOrderPage.verifyLessDetailsLinkNonBYOD();
		checkOrderPage.clickLessDetailsLinkNonBYOD();
		checkOrderPage.verifyMoreDetailsLinkNonBYOD();
		
	}
	
	/**
	 * US480325: AAL Order Status - Order Details expanded (Order total details)
	 * US480326: AAL Order Status - Order Details expanded (remove monthly section for legal text)
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL})
	public void testTransactionDetailsInOrderStatusPageUsingUseMyOwnPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US480325: AAL Order Status - Order Details expanded (Order total details)");
		Reporter.log("Test US480326: AAL Order Status - Order Details expanded (remove monthly section for legal text)");
		Reporter.log("Data Condition | PAH User with AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Use my own phone' option | Rate page should be displayed");
		Reporter.log("7. Click 'Continue' button on Rate plan page | Cart Page should be displayed");					
		Reporter.log("8. Click on 'Continue to shipping' button | Shipping Information section should be displayed");
		Reporter.log("9. Click on 'Continue to Payment' button | Payment Information section should be displayed");
		Reporter.log("10. Enter Credit card details | Accept and Continue button should be enabled");
        Reporter.log("11. Select Service agereement check box and Click Accept and Continue button | Order Conformation page should be displayed");
        Reporter.log("12. Click on Order staus link | Order staus page should be displayed");		
		Reporter.log("13. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
		Reporter.log("14. Verify 'More details' link text | 'More details' link text should be displayed");
		Reporter.log("15. Click 'More details' link | 'Less details' link text should be displayed");	
		Reporter.log("16. Verify 'TRANSACTION' header title text | 'TRANSACTION' header title text should be displayed");		
		Reporter.log("17. Verify 'Insurance Deductible' title text | 'Insurance Deductible' title text should be displayed");		
		Reporter.log("18. Verify 'Insurance Deductible' price and store in V1 | 'Insurance Deductible' price should be displayed and stored in V1");
		Reporter.log("19. Verify 'Deposit' title text | 'Deposit' title text should be displayed");
		Reporter.log("20. Verify 'Deposit' price and store in V2 | 'Deposit' price should be displayed and stored in V2");
		Reporter.log("21. Verify 'Taxes and Fees' title text | 'Taxes and Fees' title text should be displayed");
		Reporter.log("22. Verify 'Taxes and Fees' price and store in V3 | 'Taxes and Fees' price should be displayed and stored in V3");
		Reporter.log("23. Verify 'Shipping' title text | 'Shipping' title text should be displayed");
		Reporter.log("24. Verify 'Shipping' price and store in V4 | 'Shipping' price should be displayed and stored in V4");
		Reporter.log("25. Verify 'Total Due Today' title text | 'Total Due Today' title text should be displayed");
		Reporter.log("26. Verify 'Total Due Today' price and store in V5 | 'Total Due Today' price should be displayed and stored in V5");
		Reporter.log("27. Get Sim price and store in V6 | Sim price should be stored in V6");
		Reporter.log("28. Compare 'Total Due Today Price', i.e V5=(V1+V2+V3+V4+V6) | 'Total Due Today' price should be equal");
		Reporter.log("29. Verify  Monthly section for legal text on Legal Place holder | Monthly section for legal text should not be displayed");
		Reporter.log("30. Click 'Less details' link | 'More details' link text should be displayed");	
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToCheckOrderPageBYOD(myTmoData);
		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyCheckOrderPage();
		checkOrderPage.clickMoreDetailsLinkBYOD();
		checkOrderPage.verifyTransactionHeader();
		checkOrderPage.verifyInsuranceDeductibleTitle();
		checkOrderPage.verifyInsuranceDeductibleAmount();
		Double V1 = checkOrderPage.getInsuranceDeductibleAmount();
		checkOrderPage.verifyDepositeText();
		checkOrderPage.verifyDepositAmount();
		Double V2 = checkOrderPage.getDepositAmount();
		checkOrderPage.verifyTaxesAndFeesText();
		checkOrderPage.verifyTaxesAndFeesAmount();
		Double V3 = checkOrderPage.getTaxesAndFeesAmount();
		checkOrderPage.verifyShippingText();
		checkOrderPage.verifyShippingAmount();
		Double V4 = checkOrderPage.getShippingAmount();
		checkOrderPage.verifyTotalDueText();
		checkOrderPage.verifyTotalDueAmount();
		Double V5 = checkOrderPage.getTotalDueAmount();
		checkOrderPage.clickMoreDetailsLinkForItems();		
		Double V6 = checkOrderPage.getDevicePrice();		
		checkOrderPage.compareTotalDueAmountWithAllAmounts(V1, V2, V3, V4, V6, V5);
		checkOrderPage.verifyMonthlyLegalText();
		checkOrderPage.clickLessDetailsLink();
		checkOrderPage.verifyMoreDetailsLinkBYOD();
	}
	
	/**
	 * US480322: AAL Order Status - Sku Details (SIM CARD)	 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL})
	public void testSKUdetailsInOrderStatusPageUsingBuyNewPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US480322: AAL Order Status - Sku Details (SIM CARD)	");		
		Reporter.log("Data Condition | PAH User with AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy New Phone' option | PLP page should be displayed");
		Reporter.log("7. Select a Product from PLP page | PDP page should be displayed");
		Reporter.log("8. Click 'Continue' button on PDP page | Rate Plan page should be displayed");
		Reporter.log("9. Verify SIM Stater KIT | SIM Stater KIT should be displayed");		
		Reporter.log("10. Click 'Continue' button on Rate plan page | Cart Page should be displayed");		
		Reporter.log("11. Click on 'Continue to shipping' button | Shipping Information section should be displayed");
		Reporter.log("12. Click on 'Continue to Payment' button | Payment Information section should be displayed");
		Reporter.log("13. Enter Credit card details | Accept and Continue button should be enabled");
        Reporter.log("14. Select Service agereement check box and Click Accept and Continue button | Order Conformation page should be displayed");
        Reporter.log("15. Click on Order staus link | Order staus page should be displayed");        
		Reporter.log("16. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
		Reporter.log("17. Verify Device type text, i.e New device | Device type text should be displayed");
		Reporter.log("18. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
		Reporter.log("19. Verify Sim Starter Kit name | Sim Starter Kit name should be displayed");		
		Reporter.log("20. Verify 'More details' link text | 'More details' link text should be displayed");
		Reporter.log("21. Click 'More details' link text | 'More details' link text should be displayed");
		Reporter.log("22. Verify Device price | Device price should be displayed");	
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");		
		

		navigateToCheckOrderPageNonBYOD(myTmoData);
		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyCheckOrderPage();
		checkOrderPage.verifyOrderStatusOverallNonBYOD();
		checkOrderPage.verifyDeviceTypeNonBYOD();
		checkOrderPage.verifyOrderStatusItemNonBYOD();
		checkOrderPage.verifyMoreDetailsItemsNonBYOD();
		checkOrderPage.clickMoreDetailsLinkForItemsNonBYOD();
		checkOrderPage.verifyItemPriceNonBYOD();

	}
	
	/**
	 * US480322: AAL Order Status - Sku Details (SIM CARD)	
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL})
	public void testSKUdetailsInOrderStatusPageUsingUseMyOwnPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US480322: AAL Order Status - Sku Details (SIM CARD)");		
		Reporter.log("Data Condition | PAH User with AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Use my own phone' option | Rate page should be displayed");
		Reporter.log("7. Verify SIM KIT | SIM KIT should be displayed");		
		Reporter.log("8. Click 'Continue' button on Rate plan page | Cart Page should be displayed");					
		Reporter.log("9. Click on 'Continue to shipping' button | Shipping Information section should be displayed");
		Reporter.log("10. Click on 'Continue to Payment' button | Payment Information section should be displayed");
		Reporter.log("11. Enter Credit card details | Accept and Continue button should be enabled");
        Reporter.log("12. Select Service agereement check box and Click Accept and Continue button | Order Conformation page should be displayed");
        Reporter.log("13. Click on Order staus link | Order staus page should be displayed");
        Reporter.log("14. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
		Reporter.log("15. Verify Device type text, i.e New device | Device type text should be displayed");
		Reporter.log("16. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
		Reporter.log("17. Verify Sim Starter Kit name | Sim Starter Kit name should be displayed");		
		Reporter.log("18. Verify 'More details' link text | 'More details' link text should be displayed");
		Reporter.log("19. Click 'More details' link text | 'More details' link text should be displayed");
		Reporter.log("20. Verify Device price | Device price should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToCheckOrderPageBYOD(myTmoData);
		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyCheckOrderPage();
		checkOrderPage.verifyOrderStatusOverallBYOD();
		checkOrderPage.verifyDeviceType();
		checkOrderPage.verifyOrderStatusItem();
		checkOrderPage.verifyMoreDetailsItems();
		checkOrderPage.clickMoreDetailsLinkForItems();
		checkOrderPage.verifyItemPrice();
	}
	
	/**
	 * US480321: AAL Order Status - order details bar (AAL order type) 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL})
	public void testAALOrderTypeInOrderStatusPageUsingBuyNewPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("TestUS480321: AAL Order Status - order details bar (AAL order type)");		
		Reporter.log("Data Condition | PAH User with AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy New Phone' option | PLP page should be displayed");
		Reporter.log("7. Select a Product from PLP page | PDP page should be displayed");
		Reporter.log("8. Click 'Continue' button on PDP page | Rate Plan page should be displayed");			
		Reporter.log("9. Click 'Continue' button on Rate plan page | Cart Page should be displayed");		
		Reporter.log("10. Click on 'Continue to shipping' button | Shipping Information section should be displayed");
		Reporter.log("11. Click on 'Continue to Payment' button | Payment Information section should be displayed");
		Reporter.log("12. Enter Credit card details | Accept and Continue button should be enabled");
        Reporter.log("13. Select Service agereement check box and Click Accept and Continue button | Order Conformation page should be displayed");
        Reporter.log("14. Click on Order staus link | Order staus page should be displayed");        
		Reporter.log("15. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
		Reporter.log("16. Verify Order type as 'Add A Line' in Order status bar | Order type as 'Add A Line' should be displayed in Order status bar");
				
		Reporter.log("================================");
		Reporter.log("Actual Output:");		
		
		navigateToCheckOrderPageNonBYOD(myTmoData);
		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyAALorderTypeNonBYOD();
		checkOrderPage.verifyOrderStatusDisplayedNonBYOD();
	}
	
	/**
	 * US480321: AAL Order Status - order details bar (AAL order type)	
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL})
	public void testAALOrderTypeInOrderStatusPageUsingUseMyOwnPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US480321: AAL Order Status - order details bar (AAL order type)");		
		Reporter.log("Data Condition | PAH User with AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Use my own phone' option | Rate page should be displayed");				
		Reporter.log("7. Click 'Continue' button on Rate plan page | Cart Page should be displayed");					
		Reporter.log("8. Click on 'Continue to shipping' button | Shipping Information section should be displayed");
		Reporter.log("9. Click on 'Continue to Payment' button | Payment Information section should be displayed");
		Reporter.log("10. Enter Credit card details | Accept and Continue button should be enabled");
        Reporter.log("11. Select Service agereement check box and Click Accept and Continue button | Order Conformation page should be displayed");
        Reporter.log("12. Click on Order staus link | Order staus page should be displayed");
        Reporter.log("13. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
        Reporter.log("16. Verify Order type as 'Add A Line' in Order status bar | Order type as 'Add A Line' should be displayed in Order status bar");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToCheckOrderPageBYOD(myTmoData);
		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyAALorderTypeBYOD();
		checkOrderPage.verifyOrderStatusOverallBYOD();
	}
	
	/**
	 * US481995:AAL Order Status-Sku Details(SIM CARD Price on right in "more details")	 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL})
	public void testAALOrderStatusSKUDetailsUsingBuyNewPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US481995:AAL Order Status-Sku Details(SIM CARD Price on right in more details)");
		Reporter.log("Data Condition | PAH User with AAL eligiblity");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy New Phone' option | PLP page should be displayed");
		Reporter.log("7. Select a Product from PLP page | PDP page should be displayed");
		Reporter.log("8. Click 'Continue' button on PDP page | Rate Plan page should be displayed");	
		Reporter.log("9. Verify SIM Stater KIT with price | SIM Stater KIT with price should be displayed");
		Reporter.log("10. Click 'Continue' button on Rate plan page | Cart Page should be displayed");		
		Reporter.log("11. Click on 'Continue to shipping' button | Shipping Information section should be displayed");
		Reporter.log("12. Click on 'Continue to Payment' button | Payment Information section should be displayed");
		Reporter.log("13. Enter Credit card details | Accept and Continue button should be enabled");
        Reporter.log("14. Select Service agereement check box and Click Accept and Continue button | Order Conformation page should be displayed");
        Reporter.log("15. Click on Order staus link | Order staus page should be displayed");        
		Reporter.log("16. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
		Reporter.log("17. Click 'More details' link | 'Less details' link text should be displayed");
		Reporter.log("18. Verify Device 'Down Payment' price | Device 'Down Payment' price should be displayed");
		Reporter.log("19. Verify SIM Stater KIT with price | SIM Stater KIT with price should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");		
		navigateToCheckOrderPageNonBYOD(myTmoData);
		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyOrderStatusOverallNonBYOD();
		checkOrderPage.verifyMoreDetailsItemsNonBYOD();
		checkOrderPage.clickMoreDetailsLinkForItemsNonBYOD();
		checkOrderPage.verifyDevicePriceNonBYOD();
		checkOrderPage.verifySIMPriceNonBYOD();
		checkOrderPage.verifySIMStarterKItNONBYOD();
	}

	/**
	 * US481995:AAL Order Status-Sku Details(SIM CARD Price on right in "more details")	 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL})
	public void testAALOrderStatusSKUDetailsUsingUseMyOwnPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US481995:AAL Order Status-Sku Details(SIM CARD Price on right in more details)");
		Reporter.log("Data Condition | PAH User with AAL eligiblity");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");		
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line link on shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Use my own phone' option | Rate page should be displayed");
		Reporter.log("7. Verify SIM Stater KIT with price | SIM Stater KIT with price should be displayed");
		Reporter.log("8. Click 'Continue' button on Rate plan page | Cart Page should be displayed");					
		Reporter.log("9. Click on 'Continue to shipping' button | Shipping Information section should be displayed");
		Reporter.log("10. Click on 'Continue to Payment' button | Payment Information section should be displayed");
		Reporter.log("11. Enter Credit card details | Accept and Continue button should be enabled");
        Reporter.log("12. Select Service agereement check box and Click Accept and Continue button | Order Conformation page should be displayed");
        Reporter.log("13. Click on Order staus link | Order staus page should be displayed");
        Reporter.log("14. Verify Order status text(i.e Delivered / in progress/etc) | Order Status text should be displayed");
        Reporter.log("15. Click 'More details' link | 'Less details' link text should be displayed");
		Reporter.log("16. Verify Device 'Down Payment' price | Device 'Down Payment' price should be displayed");// For BYOD device is not applicable
		Reporter.log("17. Verify SIM Stater KIT with price | SIM Stater KIT with price should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToCheckOrderPageBYOD(myTmoData);
		CheckOrderPage checkOrderPage = new CheckOrderPage(getDriver());
		checkOrderPage.verifyOrderStatusOverallBYOD();
		checkOrderPage.clickMoreDetailsLinkForItems();
		checkOrderPage.verifyItemPrice();
	    checkOrderPage.verifySIMStarterKItBYOD();		

	}
	
	/**
	 * US488871:Fraud - suppress fraud check for upgrade (review part) 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL })
	public void testReviewFraudCheckPageNotDisplayedWhenUserPlaceOrder(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US488871:Fraud - suppress fraud check for upgrade (review part)");
		Reporter.log("Data Condition | Std PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");		
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");		
		Reporter.log("5. Click on See all phones link | PLP page should be displayed");
		Reporter.log("6. Select device | PDP page should be displayed");
		Reporter.log("7. Select Full price option | Full price option should be selected");		
		Reporter.log("8. Click on Add to cart button | Line selection page should be displayed");					
		Reporter.log("9. Select a line | LineSection details should be displayed");		
		Reporter.log("10. Click on Skip trade-in link | Device protection page should be displayed");
		Reporter.log("11. Click on Continue button | Accessories plp page should be displayed");
		Reporter.log("12. Click on Skip Accessories link | Cart page should be displayed");	
		Reporter.log("13. Click on Continue to shipping button | Shipping information should be displayed");
		Reporter.log("14. Click on Continue to Payment button | Payment information should be displayed");		
		Reporter.log("15. Enter Credit card details | Accept and Continue button should be enabled");
		Reporter.log("16. Click Accept and Continue button | Order Conformation page should be displayed and Fraud review check page should not be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToPaymentTabBySeeAllPhonesWithSkipTradeIn(myTmoData);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		cartPage.clickAcceptAndPlaceOrder();
		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
		orderConfirmationPage.verifyOrderConfirmationPage();
		IdentityReviewIntroductionPage identityReviewIntroductionPage = new IdentityReviewIntroductionPage(getDriver());
		identityReviewIntroductionPage.verifyIdentityReviewIntroductionPageNotDisplayed();
	}

	/**
	 * US487391: Trade In: display Order Status page statuses for AAL
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true,groups = {Group.AAL })
	public void testAALTradeInDetailsInOrderStatusPageUsingBuyNewPhoneOption(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US487391: Trade In: display Order Status page statuses for AAL");
		Reporter.log("Data Condition | PAH User with AAL eligibility");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("7. Click Contiune button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("10. Click Continue button | Interstitial page should be displayed ");
		Reporter.log("11. Verify Trade-In tile 'Yes, I want to trade in a device' | Trade-In tile should be displayed");
		Reporter.log("12. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
		Reporter.log("13. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
		Reporter.log("14. Click on Continue button | Device condition page should be displayed");
		Reporter.log("15. Verify good condition authorable title | Good condition authorable title should be displayed");
		Reporter.log("16. Select good condition option | Device value page should be displayed");
		Reporter.log("17. Verify Trade-In this Phone CTA | Trade-In this Phone CTA should be displayed");
		Reporter.log("18. Click on Trade-In this Phone CTA | Device protection page should be displayed");
		Reporter.log("19. Click on Continue button | Accessories Plp page should be displayed");
		Reporter.log("20. Click on Skip Accessories button | Cart page should be displayed");
		Reporter.log("21. Click on 'Continue to Shipping' button | Shipping details should be displayed");
		Reporter.log("22. Click on 'Continue to Payment' button | Payment details should be displayed");
		Reporter.log("23. Complete payment details and click Accept And PlaceOrder cta | Order Conformation page should be displayed");
		Reporter.log("24. Click on 'T' icon in header | Home page should be displayed");
		Reporter.log("25. Click on 'Phone' link in navigation bar | Phone page should be displayed");
		Reporter.log("26. Click on 'Check Order Status' link | Order Status page should be displayed");
		Reporter.log("27. Verify TradeIn details available on Order Status page | AAL Trade-In details should be displayed");
		Reporter.log("28. Verify DRP Links on Order Status page | DRP links should be displayed");
		Reporter.log("29. Verify Print shipping label | Print shipping label should be displayed");
		Reporter.log("30. Verify Offer label text | Offer label text should be displayed for trade in device");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		CartPage cartPage = navigateToCartPageFromAALBuyNewPhoneFlowUsingInterstitialTradeIn(myTmoData);
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShippingDetails();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.verifyAcceptAndContinue();
		cartPage.clickAcceptAndPlaceOrder();
		
		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
		orderConfirmationPage.verifyOrderConfirmationPage();
		orderConfirmationPage.clickTmobileIcon();
		
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		homePage.clickPhoneLink();
		
		PhonePages phonePage = new PhonePages(getDriver());
		phonePage.verifyPhonesPage();
		phonePage.clickCheckOrderStatusLink();
		
		CheckOrderPage checkorderPage = new CheckOrderPage(getDriver());
		checkorderPage.verifyCheckOrderPage();
		checkorderPage.clickMoreDetailsLink();
		checkorderPage.verifyTradeInDetailsSection();
		checkorderPage.verifyTradeInDetails();
		checkorderPage.verifyDrpLink();
		checkorderPage.verifyPrintShippingLabel();
		checkorderPage.verifyOfferLabel();
		checkorderPage.verifyReceivedAccessingText();
		checkorderPage.verifyProcessingCreditText();
		checkorderPage.verifyCompleteCreditApplied();
		checkorderPage.verifyProcesingReturn();
		checkorderPage.verifyTradeInDetailsSection();

	}

}
