package com.tmobile.eservices.qa.shop.api.common;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.eos.CartApiV4;
import com.tmobile.eservices.qa.api.eos.QuoteApiV5;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;



public class ShopApiCommonLibrary extends ApiCommonLib  {

	JsonPath jsonPath;
    public Map<String, String> tokenMap;
    
    public Response createQuoteThroughFRPWithDeviceAndSimForUpgradeTransaction(ControlTestData data, ApiTestData apiTestData) throws Exception  {
        
        String requestBody = new ServiceTest().getRequestFromFile("createCartForPayment.txt");
        CartApiV4 cartApi = new CartApiV4();
        tokenMap = new HashMap<String, String>();
        tokenMap.put("ban", apiTestData.getBan());
        tokenMap.put("msisdn", apiTestData.getMsisdn());
        tokenMap.put("sku", apiTestData.getSku());
        tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
        String operationName="createCartForPayment";
        String updatedRequest = prepareRequestParam(requestBody, tokenMap);
        logRequest(updatedRequest, operationName);
        Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

        if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
               logSuccessResponse(response, operationName);
               ObjectMapper mapper = new ObjectMapper();
               JsonNode jsonNode = mapper.readTree(response.asString());
               if (!jsonNode.isMissingNode()) 
               {      
                     jsonPath= new JsonPath(response.asString());
                     Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
                     
                     Reporter.log("Cart Id is not null");
                     tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
                     tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
               }
        } else {
               Reporter.log(" <b>Exception Response :</b> ");
               Reporter.log("Cart Response status code : " + response.getStatusCode());
               Reporter.log("Cart Response : " + response.body().asString());
               failAndLogResponse(response, operationName);
        }
        
        String requestBodyUpdateCart = new ServiceTest().getRequestFromFile("UpdateCartForPayment.txt");
        operationName = "Update Cart For Payment";
        String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
        logRequest(updatedRequestUpdateCart, operationName);
        response = cartApi.updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);
        

        if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
               logSuccessResponse(response, operationName);
               ObjectMapper mapper = new ObjectMapper();
               JsonNode jsonNode = mapper.readTree(response.asString());
               
               if (!jsonNode.isMissingNode()) 
               {
                     jsonPath= new JsonPath(response.asString());
                     
                     Assert.assertNotNull(jsonPath.get("cartMetadata.cartId"), "Cart Id is null");
                     Reporter.log("Cart Id is not null");
        
               }
        } else {
               failAndLogResponse(response, operationName);
 }
        operationName="createQuoteforPaymemnt";
        QuoteApiV5 quoteApiV5=new QuoteApiV5();
        String quoteRequestBody = new ServiceTest().getRequestFromFile("createQuoteforPayment.txt");
        String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
        logRequest(latestRequest, operationName);
        response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

        /*if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
               logSuccessResponse(response, operationName);
               ObjectMapper mapper = new ObjectMapper();
               JsonNode jsonNode = mapper.readTree(response.asString());
               if(!jsonNode.isMissingNode())
               {
                     jsonPath= new JsonPath(response.asString());
                     Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
                     Reporter.log("Quote Id is not null");
                     Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
                     Reporter.log("Order Id is not null");
                     tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
                     tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
                     tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
                     tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
               }
        } else {
               Reporter.log(" <b>Exception Response :</b> ");
               Reporter.log("Create Quote Response status code : " + response.getStatusCode());
               Reporter.log("Create Quote Response : " + response.body().asString());
               failAndLogResponse(response, operationName);
        }*/
        
        return response;
        }

    public Response createQuoteThroughFRPWithDeviceForAddALineTransaction(ControlTestData data, ApiTestData apiTestData) throws Exception  {
        
        String requestBody = new ServiceTest().getRequestFromFile("createCartForPayment.txt");
        CartApiV4 cartApi = new CartApiV4();
        tokenMap = new HashMap<String, String>();
        tokenMap.put("ban", apiTestData.getBan());
        tokenMap.put("msisdn", apiTestData.getMsisdn());
        tokenMap.put("sku", apiTestData.getSku());
        tokenMap.put("addaline", "ADDALINE");
        tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
        String operationName="createCartForPayment";
        String updatedRequest = prepareRequestParam(requestBody, tokenMap);
        logRequest(updatedRequest, operationName);
        Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

        if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
               logSuccessResponse(response, operationName);
               ObjectMapper mapper = new ObjectMapper();
               JsonNode jsonNode = mapper.readTree(response.asString());
               if (!jsonNode.isMissingNode()) 
               {      
                     jsonPath= new JsonPath(response.asString());
                     Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
                     
                     Reporter.log("Cart Id is not null");
                     tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
                     tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
               }
        } else {
               Reporter.log(" <b>Exception Response :</b> ");
               Reporter.log("Cart Response status code : " + response.getStatusCode());
               Reporter.log("Cart Response : " + response.body().asString());
               failAndLogResponse(response, operationName);
        }
        
        String requestBodyUpdateCartForPlan = new ServiceTest().getRequestFromFile("updatePlan.txt");
        operationName = "Update plan For EIP";
        String updatedRequestUpdateCartForPlan = prepareRequestParam(requestBodyUpdateCartForPlan, tokenMap);
        logRequest(updatedRequestUpdateCartForPlan, operationName);
        response = cartApi.updateCart(apiTestData, updatedRequestUpdateCartForPlan, tokenMap);
        
        
        if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
               logSuccessResponse(response, operationName);
               ObjectMapper mapper = new ObjectMapper();
               JsonNode jsonNode = mapper.readTree(response.asString());
               
               if (!jsonNode.isMissingNode()) 
               {
                     jsonPath= new JsonPath(response.asString());
                     
                     Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
                     Reporter.log("Plan Id is not null");
               }
           } else {
               failAndLogResponse(response, operationName);
      }
        operationName="createQuoteforPaymemnt";
        QuoteApiV5 quoteApiV5=new QuoteApiV5();
        String quoteRequestBody = new ServiceTest().getRequestFromFile("createQuoteforPayment.txt");
        String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
        logRequest(latestRequest, operationName);
        response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);
        System.out.println("Quote Id created successfully");

      /*  if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
               logSuccessResponse(response, operationName);
               ObjectMapper mapper = new ObjectMapper();
               JsonNode jsonNode = mapper.readTree(response.asString());
               if(!jsonNode.isMissingNode())
               {
                     jsonPath= new JsonPath(response.asString());
                     Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
                     Reporter.log("Quote Id is not null");
                     Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
                     Reporter.log("Order Id is not null");
                     Assert.assertNotNull(jsonPath.get("    lines[0].lineId"), "Line Id is null");
                     Reporter.log("Line Id is not null");
                     tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
                     tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
                     tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
                     tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
               }
        } else {
               Reporter.log(" <b>Exception Response :</b> ");
               Reporter.log("Create Quote Response status code : " + response.getStatusCode());
               Reporter.log("Create Quote Response : " + response.body().asString());
               failAndLogResponse(response, operationName);
        }*/
        
        return response;
        }
 
			public Response createQuoteWithSimForAddALineTransaction(ControlTestData data, ApiTestData apiTestData) throws Exception  {
			 
			 String requestBody = new ServiceTest().getRequestFromFile("createCartWithSIM.txt");
			 CartApiV4 cartApi = new CartApiV4();
			 tokenMap = new HashMap<String, String>();
			 tokenMap.put("ban", apiTestData.getBan());
			 tokenMap.put("msisdn", apiTestData.getMsisdn());
			 tokenMap.put("sku", apiTestData.getSku());
			 tokenMap.put("addaline", "ADDALINE");
			 tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
			String operationName="createCartForPayment";
			 String updatedRequest = prepareRequestParam(requestBody, tokenMap);
			 logRequest(updatedRequest, operationName);
			 Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);
			
			 if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			        logSuccessResponse(response, operationName);
			        ObjectMapper mapper = new ObjectMapper();
			        JsonNode jsonNode = mapper.readTree(response.asString());
			        if (!jsonNode.isMissingNode()) 
			        {      
			               jsonPath= new JsonPath(response.asString());
			               Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
			               
			               Reporter.log("Cart Id is not null");
			               tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
			               tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			        }
			 } else {
			        Reporter.log(" <b>Exception Response :</b> ");
			        Reporter.log("Cart Response status code : " + response.getStatusCode());
			        Reporter.log("Cart Response : " + response.body().asString());
			        failAndLogResponse(response, operationName);
			 }
			 
			 String requestBodyUpdateCartForPlan = new ServiceTest().getRequestFromFile("UpdatePlan.txt");
			 operationName = "Update plan For EIP";
			 String updatedRequestUpdateCartForPlan = prepareRequestParam(requestBodyUpdateCartForPlan, tokenMap);
			 logRequest(updatedRequestUpdateCartForPlan, operationName);
			 response = cartApi.updateCart(apiTestData, updatedRequestUpdateCartForPlan, tokenMap);
			 
			 
			 if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			        logSuccessResponse(response, operationName);
			        ObjectMapper mapper = new ObjectMapper();
			        JsonNode jsonNode = mapper.readTree(response.asString());
			        
			        if (!jsonNode.isMissingNode()) 
			        {
			               jsonPath= new JsonPath(response.asString());
			               
			               Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
			               Reporter.log("Plan Id is not null");
			        }
			    } else {
			        failAndLogResponse(response, operationName);
			}
			 
			 String requestBodyUpdateCartForSIM = new ServiceTest().getRequestFromFile("UpdateSim.txt");
			 operationName = "Update sim For EIP";
			 String updatedRequestUpdateCartForSim = prepareRequestParam(requestBodyUpdateCartForSIM, tokenMap);
			 logRequest(updatedRequestUpdateCartForSim, operationName);
			 response = cartApi.updateCart(apiTestData, updatedRequestUpdateCartForSim, tokenMap);
			 
			 
			 if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			        logSuccessResponse(response, operationName);
			        ObjectMapper mapper = new ObjectMapper();
			        JsonNode jsonNode = mapper.readTree(response.asString());
			        
			        if (!jsonNode.isMissingNode()) 
			        {
			               jsonPath= new JsonPath(response.asString());
			               
			               Assert.assertNotNull(jsonPath.get("lines[0].items.simStarterKit"), "sim kit is null");
			               Reporter.log("sim kit is not null");
			        }
			    } else {
			        failAndLogResponse(response, operationName);
			}
			 
			 operationName="createQuoteforPaymemnt";
			 QuoteApiV5 quoteApiV5=new QuoteApiV5();
			 String quoteRequestBody = new ServiceTest().getRequestFromFile("createQuoteforPayment.txt");
			 String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
			 logRequest(latestRequest, operationName);
			 response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);
			
			 /*if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			        logSuccessResponse(response, operationName);
			        ObjectMapper mapper = new ObjectMapper();
			        JsonNode jsonNode = mapper.readTree(response.asString());
			        if(!jsonNode.isMissingNode())
			        {
			               jsonPath= new JsonPath(response.asString());
			               Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
			               Reporter.log("Quote Id is not null");
			               Assert.assertNotNull(jsonPath.get("orderId"), "Order Id is null");
			               Reporter.log("Order Id is not null");
			               Assert.assertNotNull(jsonPath.get("    lines[0].lineId"), "Line Id is null");
			               Reporter.log("Line Id is not null");
			               tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
			               tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
			               tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
			               tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
			        }
			 } else {
			        Reporter.log(" <b>Exception Response :</b> ");
			        Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			        Reporter.log("Create Quote Response : " + response.body().asString());
			        failAndLogResponse(response, operationName);
			 }*/
			 
			 return response;
			 }
			 
			
			
				
				
				
			}
