package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.shop.ShopConstants;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.CartApiV4;
import com.tmobile.eservices.qa.api.eos.LoanAgreementApiV2;
import com.tmobile.eservices.qa.api.eos.QuoteApiV5;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;


public class LoanAgreementApiV2Test extends LoanAgreementApiV2 {
    
	JsonPath jsonPath;
	public Map<String, String> tokenMap;
	/**
	 * UserStory# Description:US438293:myTMO AUC: Technical Story - EOS integration for Standard/Jump upgrade
	 * 
	 * @param data:qlab02
	 * @param myTmoDataCOD-17
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "loanAgreement","testLoanAgreement",Group.PENDING_REGRESSION})
	public void testLoanAgreement(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: test loanAgreement");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: loanAgreement details should return in service response");
		Reporter.log("Step 2: Verify expected loanAgreement details are returned in response");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 2 00 OK.");
		Reporter.log("Step 6: Verify autopay setup created.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_LOANAGREEMENT + "loanAgreement.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("languagePreference", "ENGLISH");
		String operationName="loanAgreementOps";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response =loanAgreementOps(apiTestData, updatedRequest,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode= getPathVal(jsonNode, "serviceStatus.statusCode");
				String ban= getPathVal(jsonNode, "ban");
				String documentId= getPathVal(jsonNode, "documentId");
				String documentUrl= getPathVal(jsonNode, "documentUrl");
				String esignEligibilityType= getPathVal(jsonNode, "esignEligibilityType");
				String planID= getPathVal(jsonNode, "planID");
				Assert.assertEquals(statusCode,"100","Invalid statusCode");
			//	Assert.assertNotEquals(ban, "","Invalid or empty ban");
				Assert.assertNotEquals(documentId,"","Invalid or empty documentId");
				Assert.assertNotEquals(documentUrl,"","Invalid or empty documentUrl");
			//	Assert.assertNotEquals(esignEligibilityType, "","Invalid or empty esignEligibilityType");
				Assert.assertNotEquals(planID,"","Invalid or empty planID");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	/**
	 * UserStory# Description:US438293:myTMO AUC: Technical Story - EOS integration for Standard/Jump upgrade
	 * 
	 * @param data:qlab02
	 * @param myTmoDataCOD-17
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "loanAgreement","loanAgreement_ENGLISH",Group.PENDING_REGRESSION })
	public void testLoanAgreement_English(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: test loanAgreement");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: loanAgreement details should return in service response");
		Reporter.log("Step 2: Verify expected loanAgreement details are returned in response");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 6: Verify autopay setup created.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_LOANAGREEMENT + "loanAgreement.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("languagePreference", "ENGLISH");
		String operationName="loanAgreementOps";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response =loanAgreementOps(apiTestData, updatedRequest,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode= getPathVal(jsonNode, "serviceStatus.statusCode");
				String ban= getPathVal(jsonNode, "ban");
				String documentId= getPathVal(jsonNode, "documentId");
				String documentUrl= getPathVal(jsonNode, "documentUrl");
				String esignEligibilityType= getPathVal(jsonNode, "esignEligibilityType");
				String planID= getPathVal(jsonNode, "planID");
				Assert.assertEquals(statusCode,"100","Invalid statusCode");
			//	Assert.assertNotEquals(ban, "","Invalid or empty ban");
				Assert.assertNotEquals(documentId,"","Invalid or empty documentId");
				Assert.assertNotEquals(documentUrl,"","Invalid or empty documentUrl");
			//	Assert.assertNotEquals(esignEligibilityType, "","Invalid or empty esignEligibilityType");
				Assert.assertNotEquals(planID,"","Invalid or empty planID");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	/**
	 * UserStory# Description:US438293:myTMO AUC: Technical Story - EOS integration for Standard/Jump upgrade
	 * 
	 * @param data:qlab02
	 * @param myTmoDataCOD-17
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "loanAgreement","loanAgreement_SPANISH",Group.SHOP,Group.PENDING_REGRESSION })
	public void testLoanAgreement_Spanish(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: test loanAgreement");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: loanAgreement details should return in service response");
		Reporter.log("Step 2: Verify expected loanAgreement details are returned in response");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 6: Verify autopay setup created.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_LOANAGREEMENT + "loanAgreement.txt");
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("languagePreference", "SPANISH");
		
		String operationName="loanAgreementOps";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response =loanAgreementOps(apiTestData, updatedRequest,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode= getPathVal(jsonNode, "serviceStatus.statusCode");
				String ban= getPathVal(jsonNode, "ban");
				String documentId= getPathVal(jsonNode, "documentId");
				String documentUrl= getPathVal(jsonNode, "documentUrl");
				String esignEligibilityType= getPathVal(jsonNode, "esignEligibilityType");
				String planID= getPathVal(jsonNode, "planID");
				Assert.assertEquals(statusCode,"100","Invalid statusCode");
		//		Assert.assertNotEquals(ban, "","Invalid or empty ban");
				Assert.assertNotEquals(documentId,"","Invalid or empty documentId");
				Assert.assertNotEquals(documentUrl,"","Invalid or empty documentUrl");
		//		Assert.assertNotEquals(esignEligibilityType, "","Invalid or empty esignEligibilityType");
				Assert.assertNotEquals(planID,"","Invalid or empty planID");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	
	/**
	 * UserStory# Description:US539669:myTMO : Technical Story - EOS passing partner Id In document source of XECM for add a line txn
	 * 
	 * @param data:qlab02
	 * @param myTmoDataCOD-17
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "loanAgreement","loanAgreement_SPANISH",Group.PENDING_REGRESSION })
	public void testLoanAgreementWithAddALineForPartnerIdCheck(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: test loanAgreement with add a line ");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Test create cart with AddAline| CartId should be created and Response should be 200 OK");
		Reporter.log("Step 2: Test update plan |Plan id should not be null and response should be 200");
		Reporter.log("Step 3: Test Create Quote|Quote Id should not be null and response should be 200");
		Reporter.log("Step 4: Test Update Quote|Quote should be updated successfully and response should be 200");
		Reporter.log("Step 5: Test Loan Agreement |Response should be 200");
		Reporter.log("Step 6: Verify Loan Agreement Response |verify  'ban','documnet id','planId','Service status' loan details");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "createCartForPaymentEIP.txt");
		CartApiV4 cartApi = new CartApiV4();
		tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("sku", apiTestData.getSku());
		tokenMap.put("addaline", "ADDALINE");
		tokenMap.put("emailAddress", apiTestData.getMsisdn()+"@yoipmail.com");
		String operationName="createCartForPayment";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response = cartApi.createCart(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) 
			{	
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("cartId"), "Cart Id is null");
				tokenMap.put("quoteId", getPathVal(jsonNode, "cartId"));
				tokenMap.put("cartId", getPathVal(jsonNode, "cartId"));
			}	} else {
				Reporter.log(" <b>Exception Response :</b> ");
				Reporter.log("Cart Response status code : " + response.getStatusCode());
				Reporter.log("Cart Response : " + response.body().asString());
				failAndLogResponse(response, operationName);
			}
		
		
		String requestBodyUpdateCart = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_CART + "UpdatePlanForFraudEIP.txt");
		operationName = "Update plan For EIP";
		String updatedRequestUpdateCart = prepareRequestParam(requestBodyUpdateCart, tokenMap);
		logRequest(updatedRequestUpdateCart, operationName);
		response = cartApi.updateCart(apiTestData, updatedRequestUpdateCart, tokenMap);
		
		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			
			if (!jsonNode.isMissingNode()) 
			{
				jsonPath= new JsonPath(response.asString());
				
				Assert.assertNotNull(jsonPath.get("lines[0].items.plans[0].planId"), "Plan Id is null");
				Reporter.log("Plan Id is not null");
			}
		   } else {
			failAndLogResponse(response, operationName);
	     }
		operationName="createQuoteforPaymemnt";
		QuoteApiV5 quoteApiV5=new QuoteApiV5();
		String quoteRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "createQuoteforPayment.txt");
		String latestRequest = prepareRequestParam(quoteRequestBody, tokenMap);
		logRequest(latestRequest, operationName);
		response = quoteApiV5.createQuote(apiTestData, latestRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if(!jsonNode.isMissingNode())
			{
				jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("quoteId"), "Quote Id is null");
				Reporter.log("Quote Id is not null");
				tokenMap.put("quoteId",getPathVal(jsonNode, "quoteId"));
				tokenMap.put("orderId",getPathVal(jsonNode, "orderId"));
				tokenMap.put("shipmentDetailId", getPathVal(jsonNode, "shippingOptions[0].shipmentDetailId"));
				tokenMap.put("shippingOptionId", getPathVal(jsonNode, "shippingOptions[0].shippingOptionId"));
				tokenMap.put("addressId", getPathVal(jsonNode, "shippingDetails[0].shipTo.addressId"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Create Quote Response status code : " + response.getStatusCode());
			Reporter.log("Create Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
		operationName="UpdateQuoteForPayment";
		String UpdateQuoteForPaymentRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_QUOTE + "UpdateQuoteForPayment.txt");
		String UpdateQuoteForPaymentRequest =prepareRequestParam(UpdateQuoteForPaymentRequestBody,tokenMap);
		logRequest(UpdateQuoteForPaymentRequest,operationName);
		response=quoteApiV5.updateQuote(apiTestData,UpdateQuoteForPaymentRequest,tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) 
		{
			jsonPath= new JsonPath(response.asString());
			Assert.assertNotNull(jsonPath.get("status.statusMessage"), "quote updated successfully");
			Reporter.log("quote updated successfully");
			logSuccessResponse(response, operationName);
			
	    }
		else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Update Quote Response status code : " + response.getStatusCode());
			Reporter.log("Update Quote Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		operationName="loanAgreementOps";
		String loanRequestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_LOANAGREEMENT + "loanAgreement.txt");
		tokenMap.put("languagePreference", "ENGLISH");
	    
		String updatedloanRequestBody = prepareRequestParam(loanRequestBody, tokenMap);
		logRequest(updatedloanRequestBody, operationName);
	    response =loanAgreementOps(apiTestData, updatedloanRequestBody,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				jsonPath= new JsonPath(response.asString());
				
				Assert.assertEquals(jsonPath.get("ban"), apiTestData.getBan());
				Reporter.log("MSISDN present in response is same as one fetched from excel sheet");
				
				String statusCode= getPathVal(jsonNode, "serviceStatus.statusCode");
				Assert.assertEquals(statusCode,"100","Invalid statusCode");
				
				Assert.assertNotNull(jsonPath.get("documentId"), "document Id is null");
				Reporter.log("document Id is not null");	
				
				Assert.assertNotNull(jsonPath.get("documentUrl"), "document URL is null");
				Reporter.log("document URL is not null");
				
				Assert.assertNotNull(jsonPath.get("planID"), "planID is null");
				Reporter.log("planID is not null");
				

			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}


}
