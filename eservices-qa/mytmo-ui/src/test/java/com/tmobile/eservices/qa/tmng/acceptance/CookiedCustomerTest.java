package com.tmobile.eservices.qa.tmng.acceptance;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.shop.AccessoryPLPPage;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.ConsolidatedRatePlanPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.InterstitialTradeInPage;
import com.tmobile.eservices.qa.pages.shop.JumpCartPage;
import com.tmobile.eservices.qa.pages.shop.JumpConditionPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.PhoneSelectionPage;
import com.tmobile.eservices.qa.pages.shop.TradeInAnotherDevicePage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionValuePage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class CookiedCustomerTest extends ShopCommonLib {
	/**
	 * Customer E2E Order Subission Scenarios (MyTMO - Cookied/Unauthenticated or
	 * logging in through UNO PDP)): C520008 : Standard Order CIHU customer Trades
	 * in device on account
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testE2EUSerNavigatedToPaymentsTabWithTradeInFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : C520008 : Standard Order CIHU customer Trades in device on account");
		Reporter.log("Data Condition | CIHU User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch MY-TMO application | MY TMO Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log(
				"3. Log out from MyTMO Navigate to TMO application | User should navigate TMO home page successfully");
		Reporter.log("4. Click Phones tab| UNO Plp page should be displayed");
		Reporter.log("5. Select any Phone| UNO Pdp page should be displayed");
		Reporter.log("6. Click on UPGRADE CTA| Line Selector page should be displayed");
		Reporter.log("7. Select any phone on Line selector page | Line selector detail page should be displayed ");
		Reporter.log("8. Click Trade This Phone CTA | Trade in device condition page should be displayed ");
		Reporter.log(
				"9. Select It's Good Option and click Continue CTA | Trade in detail value confirmation page should be displayed ");
		Reporter.log("10. Click Trade In this Phone CTA| Device Protection page should be displayed");
		Reporter.log("11. Click on Continue button | Accessories Plp page should be displayed");
		Reporter.log("12. Click on Skip Accessories button | Cart page should be displayed");
		Reporter.log("13. Validate order informatuon on cart| Cart should contain correct order details");
		Reporter.log("14. Click on 'Continue to Shipping' button | Shipping details should be displayed");
		Reporter.log("15. Click on 'Continue to Payment' button | Payment details should be displayed");
		Reporter.log("16. Verify Accept and continue CTA | Accept and continue CTA should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		NewHomePage homePage = new NewHomePage(getDriver());
		homePage.clickNewUnavLogOutMenu();
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPageLoaded();
		getURL(myTmoData);

		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyPhonesPlpPageLoaded();
		plpPage.verifyFindYourPricingDisable();
		plpPage.clickDevice(myTmoData.getDeviceName());
//		plpPage.clickDeviceWithAvailability(myTmoData.getDevice());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();

		pdpPage.verifyFindYourPricingDisable();
		pdpPage.verifyUpgradeCTA();
		pdpPage.clickUpgradeCTA();

		performLoginForCookiedCustomers(myTmoData, loginPage);

		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickOnTradeInThisPhone();

		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInConfirmationPage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();

		TradeInDeviceConditionConfirmationPage tradeInValuePage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInValuePage.clickOnTradeInThisPhoneButton();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL() == true) {
			deviceProtectionPage.clickOnYesProtectMyPhoneButton();
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesLink();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyDeviceImage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}

	/**
	 * Customer E2E Order Subission Scenarios (MyTMO - Cookied/Unauthenticated or
	 * logging in through UNO PDP)): C520011 : Standard Order / Trade in another
	 * device / PHP and accessories
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testE2ECookiedCustomerNavigatedToPaymentsTabWithTradeInAnotherPHPandAccessory(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : C520011 : Standard Order / Trade in another device / PHP and accessories");
		Reporter.log("Data Condition | CIHU User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch MY-TMO application | MY TMO Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log(
				"3. Log out from MyTMO and Navigate to TMO application | User should navigate TMO home page successfully");
		Reporter.log("4. Click Phones tab| UNO Plp page should be displayed");
		Reporter.log("5. Select any Phone| UNO Pdp page should be displayed");
		Reporter.log("6. Click on UPGRADE CTA| Line Selector page should be displayed");
		Reporter.log("7. Select any phone on Line selector page | Line selector detail page should be displayed ");
		Reporter.log(
				"8. In line selection page click on Got a diffrent phone | Trade In Another Device page should be displayed");
		Reporter.log(
				"9. In Trade In Another Device page Select Carrier,Make,Model And enter IMEI | Details should be entered");
		Reporter.log(
				"10. Click on contiune button in Trade In Another Device page | Trade In Device Condition should be displayed");
		Reporter.log(
				"11. In Trade In Device Condition page select Its good radio button and click on contiune button | Trade in value page should be displayed");
		Reporter.log(
				"12. In Trade in value page click on Continue button | Insurance migration page should be displayed");
		Reporter.log("13. Click on Continue button | Accessories Plp page should be displayed");
		Reporter.log("14. Select any Accessory and click continue| Cart page should be displayed");
		Reporter.log("13. Validate order informatuon on cart| Cart should contain correct order details");
		Reporter.log("14. Click on 'Continue to Shipping' button | Shipping details should be displayed");
		Reporter.log("15. Click on 'Continue to Payment' button | Payment details should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		NewHomePage homePage = new NewHomePage(getDriver());
		homePage.clickNewUnavLogOutMenu();
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPageLoaded();
		getURL(myTmoData);

		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyPhonesPlpPageLoaded();
		plpPage.verifyFindYourPricingDisable();
		plpPage.clickDevice(myTmoData.getDeviceName());
//		plpPage.clickDeviceWithAvailability(myTmoData.getDevice());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();

		pdpPage.verifyFindYourPricingDisable();
		pdpPage.verifyUpgradeCTA();
		pdpPage.clickUpgradeCTA();

		performLoginForCookiedCustomers(myTmoData, loginPage);

		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());

		lineSelectorDetailsPage.clickWantToTradeDifferentPhoneLink();
		PhoneSelectionPage phoneSelectionpage = new PhoneSelectionPage(getDriver());
		phoneSelectionpage.verifyPhoneSelectionPage();
		phoneSelectionpage.clickGotADifferentPhone();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage TradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		TradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		TradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		TradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		TradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		TradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		TradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInConfirmationPage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();
		tradeInConfirmationPage.clickOnTradeInThisPhoneButton();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL() == true) {
			deviceProtectionPage.clickOnYesProtectMyPhoneButton();
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessories("1");
		accessoryPLPPage.continueButton();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();

		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}

	/**
	 * Customer E2E Order Subission Scenarios (MyTMO - Cookied/Unauthenticated or
	 * logging in through UNO PDP)): C520400 : Ensure that Accessory Order from Uno
	 * takes cookied user to Standalone Accessories Cart page
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testCookiedCustomerNavigatedToAccessoryPlpPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : C520400 : Ensure that Accessory Order from Uno takes cookied user to Standalone Accessories Cart page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch MY-TMO application | MY TMO Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log(
				"3. Log out from MyTMO and Navigate to TMO application | User should navigate TMO home page successfully");
		Reporter.log("4. Click Phones tab| UNO Plp page should be displayed");
		Reporter.log(
				"5. Select Accessory Tab and select any accessory from Accessory PLP| UNO Accessory Pdp page should be displayed");
		Reporter.log("6. Click on Add to Cart CTA| Login options modal should be displayed");
		Reporter.log("7. Click login as existing user | MyTMO login page should be displayed ");
		Reporter.log("8. Login to MyTmo  | Standalone Accessories Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		NewHomePage homePage = new NewHomePage(getDriver());
		homePage.clickNewUnavLogOutMenu();
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPageLoaded();
		getURL(myTmoData);

		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyPhonesPlpPageLoaded();
		plpPage.verifyFindYourPricingDisable();
		plpPage.clickOnAccessoriesMenuLinkTPP();
		plpPage.verifyAccessoriesPlpPageLoaded();
		plpPage.clickDevice(myTmoData.getDeviceName());
//		plpPage.clickDeviceWithAvailability(myTmoData.getDevice());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyAccessoriesPdpPageLoaded();
		pdpPage.verifyFindYourPricingDisable();
		pdpPage.clickOnAddToCartBtn();

		performLoginForCookiedCustomers(myTmoData, loginPage);

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();

		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}

	/**
	 * Customer E2E Order Subission Scenarios (MyTMO - Cookied/Unauthenticated or
	 * logging in through UNO PDP)): C586686 : Standard Order CIHU customer Trades
	 * in device on account
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testE2ECookiedAndAuthenticatedUserWithTradeInDeviceCondition(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test Case : C586686 : Standard Order CIHU customer Trades in device on account");
		Reporter.log("Data Condition | CIHU User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch MY-TMO application | MY TMO Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log("3. Navigate to TMO application | User should navigate TMO home page successfully");
		Reporter.log("4. Click Phones tab| UNO Plp page should be displayed");
		Reporter.log("5. Select any Phone| UNO Pdp page should be displayed");
		Reporter.log("6. Click on UPGRADE CTA| Line Selector page should be displayed");
		Reporter.log("7. Select any phone on Line selector page | Line selector detail page should be displayed ");
		Reporter.log("8. Click Trade This Phone CTA | Trade in device condition value page should be displayed ");
		Reporter.log(
				"9. Select all options(Good condition) and click Continue CTA | Trade in device condition value confirmation page should be displayed ");
		Reporter.log("10. Click 'Agree and continue' CTA | Device Protection page should be displayed");
		Reporter.log("11. Click on Continue button | Accessories Plp page should be displayed");
		Reporter.log("12. Click on Skip Accessories button | Cart page should be displayed");
		Reporter.log("13. Validate order details section on cart| Cart should contain order details");
		Reporter.log("14. Click on 'Continue to Shipping' button | Shipping details should be displayed");
		Reporter.log("15. Click on 'Continue to Payment' button | Payment details should be displayed");
		Reporter.log("16. Verify payments information form | Payments information form should be displayed");
		Reporter.log(
				"17. Fill Card details and verify 'Accept & continue' button | Accept and Continue CTA should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		getURL(myTmoData);

		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyPhonesPlpPageLoaded();
		plpPage.verifyFindYourPricingDisable();
		plpPage.clickDevice(myTmoData.getDeviceName());
//		plpPage.clickDeviceWithAvailability(myTmoData.getDevice());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();

		pdpPage.verifyFindYourPricingDisable();
		pdpPage.verifyUpgradeCTA();
		pdpPage.clickUpgradeCTA();

		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickOnTradeInThisPhone();

		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInConfirmationPage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();

		TradeInDeviceConditionConfirmationPage tradeInValuePage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInValuePage.clickOnTradeInThisPhoneButton();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL() == true) {
			deviceProtectionPage.clickOnYesProtectMyPhoneButton();
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesLink();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyBillCreditValueText();

		cartPage.clickContinueToShippingButton();
		cartPage.verifyOrderDetailsTickMarkedDisplayed();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// DocuSignPage docuSignPage = new DocuSignPage(getDriver());
		// docuSignPage.editSignature();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
		// orderConfirmationPage.verifyOrderDetailsSection();
	}

	/**
	 * Customer E2E Order Subission Scenarios (MyTMO - Cookied/Unauthenticated or
	 * logging in through UNO PDP)): C586687 : Standard Order / Trade in another
	 * device / PHP and accessories
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testE2ECookiedAndAuthenticatedUserNavigatedToPaymentsTabWithTradeInAnotherPHPandAccessoryFlow(
			ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : C586687 : Standard Order / Trade in another device / PHP and accessories");
		Reporter.log("Data Condition | CIHU User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch MY-TMO application | MY TMO Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log("3. Click Phones tab| UNO Plp page should be displayed");
		Reporter.log("4. Select any Phone| UNO Pdp page should be displayed");
		Reporter.log("5. Click on UPGRADE CTA| Line Selector page should be displayed");
		Reporter.log("6. Select any phone on Line selector page | Line selector detail page should be displayed ");
		Reporter.log(
				"7. In line selection page click on Got a diffrent phone | Trade In Another Device page should be displayed");
		Reporter.log(
				"8. In Trade In Another Device page Select Carrier,Make,Model And enter IMEI | Details should be entered");
		Reporter.log(
				"9. Click on contiune button in Trade In Another Device page | Trade In Device Condition should be displayed");
		Reporter.log(
				"10. In Trade In Device Condition page select Its good radio button and click on contiune button | Trade in value page should be displayed");
		Reporter.log(
				"11. In Trade in value page click on Continue button | Insurance migration page should be displayed");
		Reporter.log("12. Click on Continue button | Accessories Plp page should be displayed");
		Reporter.log("13. Select any Accessory and click continue| Cart page should be displayed");
		Reporter.log("14. Validate order informatuon on cart| Cart should contain correct order details");
		Reporter.log("15. Click on 'Continue to Shipping' button | Shipping details should be displayed");
		Reporter.log("16. Click on 'Continue to Payment' button | Payment details should be displayed");
		Reporter.log("17. Verify Agree and continue CTA | Agree and continue CTA should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		getURL(myTmoData);

		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyPhonesPlpPageLoaded();
		plpPage.verifyFindYourPricingDisable();
		plpPage.clickDevice(myTmoData.getDeviceName());
//		plpPage.clickDeviceWithAvailability(myTmoData.getDevice());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();

		pdpPage.verifyFindYourPricingDisable();
		pdpPage.verifyUpgradeCTA();
		pdpPage.clickUpgradeCTA();

		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());

		lineSelectorDetailsPage.clickWantToTradeDifferentPhoneLink();
		PhoneSelectionPage phoneSelectionpage = new PhoneSelectionPage(getDriver());
		phoneSelectionpage.verifyPhoneSelectionPage();
		phoneSelectionpage.clickGotADifferentPhone();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage TradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		TradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		TradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		TradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		TradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		TradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		TradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInConfirmationPage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();
		tradeInConfirmationPage.clickOnTradeInThisPhoneButton();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyDeviceProtectionURL() == true) {
			deviceProtectionPage.clickOnYesProtectMyPhoneButton();
		}

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();

		accessoryPLPPage.selectNumberOfAccessories("1");
		accessoryPLPPage.continueButton();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyDeviceImage();

		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}

	/**
	 * Customer E2E Order Subission Scenarios (MyTMO - Cookied/Unauthenticated or
	 * logging in through UNO PDP)): C586691 : Ensure that Accessory Order from Uno
	 * takes cookied user to Standalone Accessories Cart page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testE2ECookiedAndAuthenticatedUserNavigatedToAccessoryPlpPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : C586691 : Ensure that Accessory Order from Uno takes cookied user to Standalone Accessories Cart page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch MY-TMO application | MY TMO Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log(
				"3. Log out from MyTMO and Navigate to TMO application | User should navigate TMO home page successfully");
		Reporter.log("4. Click Phones tab| UNO Plp page should be displayed");
		Reporter.log(
				"5. Select Accessory Tab and select any accessory from Accessory PLP| UNO Accessory Pdp page should be displayed");
		Reporter.log("6. Click on Add to Cart CTA| Login options modal should be displayed");
		Reporter.log("7. Click login as existing user | MyTMO login page should be displayed ");
		Reporter.log("8. Login to MyTmo  | Standalone Accessories Cart page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		getURL(myTmoData);

		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyPhonesPlpPageLoaded();
		plpPage.verifyFindYourPricingDisable();
		plpPage.clickOnAccessoriesMenuLinkTPP();
		plpPage.verifyAccessoriesPlpPageLoaded();
		plpPage.verifyFindYourPricingDisable();
		plpPage.clickDevice(myTmoData.getDeviceName());
//		plpPage.clickDeviceWithAvailability(myTmoData.getDevice());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyAccessoriesPdpPageLoaded();
		pdpPage.verifyFindYourPricingDisable();
		pdpPage.clickOnAddToCartBtn();

		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();

		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}

	/**
	 * Cookied authenticated customer -> on UNO PDP select AAL CTA -> verify that it
	 * redirects to MyTMO Consolidated Charges page, and proceed to Cart/Payment tab
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testCookiedAuthenticatedUserAddALineFlowFromUNOPDP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : Cookied authenticated customer -> on UNO PDP select AAL CTA -> verify that it redirects to MyTMO Consolidated Charges page, and proceed to Cart/Payment tab");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch MY-TMO application | MY TMO Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log("3. Navigate to TMO application | User should navigate TMO home page successfully");
		Reporter.log("4. Click Phones tab| UNO Plp page should be displayed");
		Reporter.log("5. Select any Phone| UNO Pdp page should be displayed");
		Reporter.log("6. Click on Add A line CTA| Consolidated charges page should be displayed");
		Reporter.log("7. Click continue CTA on Rate plan page | Interstitial page should be displayed ");
		Reporter.log("8. Click skip Trade This Phone CTA | Device Protection page should be displayed");
		Reporter.log("9. Click on Continue button |  Cart page should be displayed");
		Reporter.log("10. Validate order informatuon on cart| Cart should contain correct order details");
		Reporter.log("11. Click on 'Continue to Shipping' button | Shipping details should be displayed");
		Reporter.log("12. Click on 'Continue to Payment' button | Payment details should be displayed");
		Reporter.log("13. Verify Accept and continue CTA | Accept and continue CTA should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		getURL(myTmoData);

		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyPhonesPlpPageLoaded();
		plpPage.verifyFindYourPricingDisable();
		plpPage.clickDevice(myTmoData.getDeviceName());
//		plpPage.clickDeviceWithAvailability(myTmoData.getDevice());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();

		pdpPage.verifyFindYourPricingDisable();
		pdpPage.verifyAddaLineCTA();
		pdpPage.clickAddaLineCTA();
		ConsolidatedRatePlanPage ratePlanpage = new ConsolidatedRatePlanPage(getDriver());
		ratePlanpage.verifyConsolidatedRatePlanPage();
		ratePlanpage.clickOnContinueCTA();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyDeviceImage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}

	/**
	 * Cookied unauthenticated customer -> on UNO PDP select AAL CTA -> verify that
	 * it redirects to MyTMO Consolidated Charges page, and proceed to Cart/Payment
	 * tab
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testCookiedUnAuthenticatedUserAddALineFlowFromUNOPDP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : Cookied unauthenticated customer -> on UNO PDP select AAL CTA -> verify that it redirects to MyTMO Consolidated Charges page, and proceed to Cart/Payment tab");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch MY-TMO application | MY TMO Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log(
				"3. Log out from MyTMO Navigate to TMO application | User should navigate TMO home page successfully");
		Reporter.log("4. Click Phones tab| UNO Plp page should be displayed");
		Reporter.log("5. Select any Phone| UNO Pdp page should be displayed");
		Reporter.log("6. Click on Add A line CTA| LogIn page should be displayed");
		Reporter.log("7. Enter login details and click login |Consolidated charges page should be displayed");
		Reporter.log("8. Click continue CTA on Rate plan page | Interstitial page should be displayed ");
		Reporter.log("9. Click skip Trade This Phone CTA | Device Protection page should be displayed");
		Reporter.log("10. Click on Continue button |  Cart page should be displayed");
		Reporter.log("11. Validate order informatuon on cart| Cart should contain correct order details");
		Reporter.log("12. Click on 'Continue to Shipping' button | Shipping details should be displayed");
		Reporter.log("13. Click on 'Continue to Payment' button | Payment details should be displayed");
		Reporter.log("14. Verify Accept and continue CTA | Accept and continue CTA should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		NewHomePage homePage = new NewHomePage(getDriver());
		homePage.clickNewUnavLogOutMenu();
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.verifyLoginPageLoaded();
		getURL(myTmoData);

		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyPhonesPlpPageLoaded();
		plpPage.verifyFindYourPricingDisable();
		plpPage.clickDevice(myTmoData.getDeviceName());
//		plpPage.clickDeviceWithAvailability(myTmoData.getDevice());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();

		pdpPage.verifyFindYourPricingDisable();
		pdpPage.verifyAddaLineCTA();
		pdpPage.clickAddaLineCTA();

		performLoginForCookiedCustomers(myTmoData, loginPage);

		ConsolidatedRatePlanPage ratePlanpage = new ConsolidatedRatePlanPage(getDriver());
		ratePlanpage.verifyConsolidatedRatePlanPage();
		ratePlanpage.clickOnContinueCTA();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.verifyDeviceImage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.clickServiceCustomerAgreementCheckBox();
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}

	/**
	 * Cookied authenticated customer -> on UNO PDP select Upgrade CTA -> verify
	 * that it redirects to MyTMO Consolidated Charges page, and proceed to
	 * Cart/Payment tab
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testCookiedAuthenticatedJumpFlowFromUNOPDP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case : Cookied authenticated customer -> on UNO PDP select AAL CTA -> verify that it redirects to MyTMO Consolidated Charges page, and proceed to Cart/Payment tab");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch MY-TMO application | MY TMO Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log("3. Navigate to TMO application | User should navigate TMO home page successfully");
		Reporter.log("4. Click Phones tab| UNO Plp page should be displayed");
		Reporter.log("5. Select any Phone| UNO Pdp page should be displayed");
		Reporter.log("6. Click on Add To Cart CTA| Line Selector page should be displayed");
		Reporter.log("7. Click On The Jump Line |Device Protection Page should be displayed ");
		Reporter.log("8. Click on Continue| Jump Condition Page should be displayed");
		Reporter.log("9. Choose Good Options | Jump Value Page should be displayed");
		Reporter.log("10. Click Continue | Jump Cart Page is Displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToHomePage(myTmoData);
		getURL(myTmoData);

		PlpPage plpPage = new PlpPage(getDriver());

		plpPage.verifyPhonesPlpPageLoaded();
		plpPage.clickDevice(myTmoData.getDeviceName());
//		plpPage.clickDeviceWithAvailability(myTmoData.getDevice());
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.verifyPhonePdpPageLoaded();

		pdpPage.verifyFindYourPricingDisable();
		pdpPage.verifyUpgradeCTA();
		pdpPage.clickUpgradeCTA();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyJumpProtection()) {
			deviceProtectionPage.clickContinueButton();
		}
		JumpConditionPage newJumpConditionPage = new JumpConditionPage(getDriver());
		newJumpConditionPage.verifyNewJumpConditionPage();
		newJumpConditionPage.selectGoodComponents();
		newJumpConditionPage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		// deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		deviceConditionsPage.clickAcceptAndContinueButton();
		JumpCartPage jumpCartPage = new JumpCartPage(getDriver());
		jumpCartPage.verifyJumpCartPage();

	}

}
