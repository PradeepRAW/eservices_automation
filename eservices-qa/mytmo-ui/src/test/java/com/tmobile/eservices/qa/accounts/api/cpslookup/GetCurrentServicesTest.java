package com.tmobile.eservices.qa.accounts.api.cpslookup;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccountsApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class GetCurrentServicesTest extends AccountsApi {

	public Map<String, String> tokenMap;

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetCurrentServices(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Current Services");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("GetCurrentServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getCurrentServices(apiTestData, updatedRequest);

		String operationName = "Get Current Services";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			Assert.assertTrue(response.jsonPath().getString("accountStatusCode").contains("ACTIVE"));
			Assert.assertTrue(response.jsonPath().getString("accountStatusReasonCode").contains("ACTIVE"));
			Assert.assertTrue(response.jsonPath().getString("accountType").contains("INDIVIDUAL"));
			Assert.assertTrue(response.jsonPath().getString("isSedonaON").contains("true")
					|| response.jsonPath().getString("isSedonaON").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("isAutoPayOn").contains("true")
					|| response.jsonPath().getString("isAutoPayOn").contains("false"));

			if (((response.jsonPath().getList("listLineDetails.planServices.soc").size() > 0))) {
				assertThat(response.jsonPath().getList("sharedServices.soc"), hasItems("TM1TI2"));
			}
			Reporter.log(" 200 ok Success");

			Reporter.log(" Active Services from MW is Returned");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetCurrentServices2(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Current Services");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("GetCurrentServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getCurrentServices(apiTestData, updatedRequest);

		String operationName = "Get Current Services";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("accountStatusCode").contains("ACTIVE"));
			Assert.assertTrue(response.jsonPath().getString("accountStatusReasonCode").contains("ACTIVE"));
			Assert.assertTrue(response.jsonPath().getString("accountType").contains("INDIVIDUAL"));
			Assert.assertTrue(response.jsonPath().getString("isSedonaON").contains("true")
					|| response.jsonPath().getString("isSedonaON").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("isAutoPayOn").contains("true")
					|| response.jsonPath().getString("isAutoPayOn").contains("false"));

			Reporter.log(" 200 ok Success");
			Reporter.log(" Active Services from MW is Returned");
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetCurrentServices3(ApiTestData apiTestData) throws Exception {

		Reporter.log("Test Case : Validating Get Current Services");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("GetCurrentServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getCurrentServices(apiTestData, updatedRequest);

		String operationName = "Get Current Services";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");
			Assert.assertTrue(response.jsonPath().getString("accountStatusCode").contains("ACTIVE"));
			Assert.assertTrue(response.jsonPath().getString("accountStatusReasonCode").contains("ACTIVE"));
			Assert.assertTrue(response.jsonPath().getString("accountType").contains("INDIVIDUAL"));
			Assert.assertTrue(response.jsonPath().getString("isSedonaON").contains("true")
					|| response.jsonPath().getString("isSedonaON").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("isAutoPayOn").contains("true")
					|| response.jsonPath().getString("isAutoPayOn").contains("false"));

			Reporter.log(" 200 ok Success");

			Reporter.log(" Active Services from MW is Returned");

		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetCurrentServices4(ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating Get Current Services");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("GetCurrentServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getCurrentServices(apiTestData, updatedRequest);

		String operationName = "Get Current Services";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("accountStatusCode").contains("ACTIVE"));
			Assert.assertTrue(response.jsonPath().getString("accountStatusReasonCode").contains("ACTIVE"));
			Assert.assertTrue(response.jsonPath().getString("accountType").contains("INDIVIDUAL"));
			Assert.assertTrue(response.jsonPath().getString("isSedonaON").contains("true")
					|| response.jsonPath().getString("isSedonaON").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("isAutoPayOn").contains("true")
					|| response.jsonPath().getString("isAutoPayOn").contains("false"));

			Reporter.log(" 200 ok Success");
			Reporter.log(" Active Services from MW is Returned");
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.APIREG })
	public void testGetCurrentServices5(ApiTestData apiTestData) throws Exception {
		Reporter.log("Test Case : Validating Get Current Services");
		Reporter.log("Test data : any Valid Msisdn present in chub with above details in request");

		tokenMap = new HashMap<String, String>();

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		String requestBody = new ServiceTest().getRequestFromFile("GetCurrentServices.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		Response response = getCurrentServices(apiTestData, updatedRequest);

		String operationName = "Get Current Services";

		logRequest(updatedRequest, operationName);
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			logSuccessResponse(response, operationName);
			System.out.println("output Success");

			Assert.assertTrue(response.jsonPath().getString("accountStatusCode").contains("ACTIVE"));
			Assert.assertTrue(response.jsonPath().getString("accountStatusReasonCode").contains("ACTIVE"));
			Assert.assertTrue(response.jsonPath().getString("accountType").contains("INDIVIDUAL"));
			Assert.assertTrue(response.jsonPath().getString("isSedonaON").contains("true")
					|| response.jsonPath().getString("isSedonaON").contains("false"));
			Assert.assertTrue(response.jsonPath().getString("isAutoPayOn").contains("true")
					|| response.jsonPath().getString("isAutoPayOn").contains("false"));

			Reporter.log(" 200 ok Success");

			Reporter.log(" Active Services from MW is Returned");

		} else {

			failAndLogResponse(response, operationName);

		}
	}
}