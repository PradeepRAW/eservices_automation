package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.MultipleDevicesPage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.global.DigitsLinkPage;

/**
 * @author rnallamilli
 * 
 */
public class MultipleDevicePageTest extends GlobalCommonLib {

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyDigitsLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with multiple devices blade");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on multiple device link | Multiple device page should be displayed");
		Reporter.log("6. Verify digits header | Digits header should be displayed");
		Reporter.log("7. Click on manage settings links| Digits link page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		loginToProfilePageAndClickOnLink(myTmoData, "Multiple Devices");
		MultipleDevicesPage multipleDevicesPage = new MultipleDevicesPage(getDriver());
		multipleDevicesPage.verifyMultipleDevicesPage();
		multipleDevicesPage.verifyDigitsHeader();
		multipleDevicesPage.clickManageSettingsLink();
		multipleDevicesPage.switchToWindow();
		multipleDevicesPage.acceptIOSAlert();

		DigitsLinkPage digitsLinkPage = new DigitsLinkPage(getDriver());
		digitsLinkPage.verifyDigitsLinkPage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyDeviceStatusLinkAndToggle(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with multiple devices blade");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on multiple device link | Multiple device page should be displayed");
		Reporter.log("6: Verify Multiple Devices crumb active status| Multiple Devices bread crumb should be active");
		Reporter.log(
				"7: Click on profile bread crumb and verify profile bread crumb active status | Profile bread crumb should be active");
		Reporter.log("8: Verify profile page | Profile page should be displayed");
		Reporter.log("9. Click on multiple device link | Multiple device page should be displayed");
		Reporter.log(
				"10.Click on device staus toggle and verify show more button | Show more button should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		MultipleDevicesPage multipleDevicesPage = navigateToMultipleDevicesPage(myTmoData, "Multiple Devices");
		multipleDevicesPage.verifyBreadCrumbActiveStatus("Multiple Devices");
		multipleDevicesPage.clickProfileHomeBreadCrumb();
		multipleDevicesPage.verifyBreadCrumbActiveStatus("Profile");

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
		profilePage.clickProfilePageLink("Multiple Devices");

		multipleDevicesPage.clickDeviceStatusToggle();
		multipleDevicesPage.clickDeviceStatusMorebtn();
	}

	/**
	 * US326603:Coverage Device saveChanges button state message
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyMultipleDevicesPageBreadCrumbsLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with multiple devices blade");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on multiple device link | Multiple device page should be displayed");
		Reporter.log("6: Verify Multiple Devices crumb active status| Multiple Devices bread crumb should be active");
		Reporter.log(
				"7: Click on profile bread crumb and verify profile bread crumb active status | Profile bread crumb should be active");
		Reporter.log("8: Verify profile page | Profile page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		MultipleDevicesPage multipleDevicesPage = navigateToMultipleDevicesPage(myTmoData, "Multiple Devices");
		multipleDevicesPage.verifyBreadCrumbActiveStatus("Multiple Devices");
		multipleDevicesPage.clickProfileHomeBreadCrumb();
		multipleDevicesPage.verifyBreadCrumbActiveStatus("Profile");

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}
}
