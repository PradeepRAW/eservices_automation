package com.tmobile.eservices.qa.shop.api;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AddressApiV1;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class AddressApiV1Test extends AddressApiV1 {

	/**
	 * UserStory# Description: Update Address
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, priority = 1, groups = {"AddressApiV1Test", "testUpdateAddress", Group.SHOP, Group.SPRINT })
	public void testUpdateAddress(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Updated Cart");
		Reporter.log("Data Conditions:MyTmo registered msisdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Update address");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName="updateAddress";
		String requestBody = new ServiceTest().getRequestFromFile("updateAddress.txt");
		logRequest(requestBody, operationName);
		Response response = updateAddress(apiTestData, requestBody);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
	}
}
