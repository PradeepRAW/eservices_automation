package com.tmobile.eservices.qa.accounts.api.LineDetails.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AllCurrentServiceDetails {
    private String soc;
    private BigDecimal price;
    private String indicator;

    public String[] getConflictingServiceList() {
        return conflictingServiceList;
    }

    public void setConflictingServiceList(String[] conflictingServiceList) {
        this.conflictingServiceList = conflictingServiceList;
    }

    public String []  conflictingServiceList;

    public String getSoc() {
        return soc;
    }

    public void setSoc(String soc) {
        this.soc = soc;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }

    @Override
    public String toString() {
        return "AllCurrentServiceDetails{" +
                "soc='" + soc + '\'' +
                ", price=" + price +
                '}';
    }
}
