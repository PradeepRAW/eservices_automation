package com.tmobile.eservices.qa.tmng;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeSuite;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceHelper;
import com.tmobile.eservices.qa.api.unlockdata.RequestData;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.commonlib.CommonLibrary;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.global.ChooseAccountPage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.tmng.api.ProductsAPIV3;
import com.tmobile.eservices.qa.pages.tmng.functional.AccessoriesHubPage;
import com.tmobile.eservices.qa.pages.tmng.functional.BringYourOwnPhonePage;
import com.tmobile.eservices.qa.pages.tmng.functional.CartPage;
import com.tmobile.eservices.qa.pages.tmng.functional.CheckOutPage;
import com.tmobile.eservices.qa.pages.tmng.functional.CheckOutTheCoveragePage;
import com.tmobile.eservices.qa.pages.tmng.functional.DealsPage;
import com.tmobile.eservices.qa.pages.tmng.functional.DigitsPage;
import com.tmobile.eservices.qa.pages.tmng.functional.HomePage;
import com.tmobile.eservices.qa.pages.tmng.functional.InternationalCallingPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlansPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.RoamingPage;
import com.tmobile.eservices.qa.pages.tmng.functional.StoreLocatorPage;
import com.tmobile.eservices.qa.pages.tmng.functional.SwitchToTmobilePage;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPPreScreenForm;
import com.tmobile.eservices.qa.pages.tmng.functional.TPPPreScreenIntro;
import com.tmobile.eservices.qa.pages.tmng.functional.TradeInPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TravelAbroadPage;
import com.tmobile.eservices.qa.pages.tmng.functional.UnlimitedPlansPage;
import com.tmobile.eservices.qa.pages.tmng.functional.WeWillHelpYouJoinUsPage;

import io.appium.java_client.AppiumDriver;
import io.restassured.path.json.JsonPath;

public class TmngCommonLib extends CommonLibrary {

	public static final String TMO_URL = "app.tmo.url";
	private final String phonesPageUrl = "/cell-phones";
	private final String plansPageUrl = "/cell-phone-plans";
	public static final String EMPTYCARTURL = "/cart";
	public static final String STORELOCATORURL = "/store-locator";
	public static final String DEEPLINK_PHONESPAGE = "https://dmo.digital.t-mobile.com/cell-phones";
	public static final String BYOD_PAGE = System.getProperty("environment")
			+ "/resources/bring-your-own-phone?icid=WMM_TM_Q417UNAVME_2JDVKVMA5T12838";
	public static final String My_TMOLogin = "https://my.t-mobile.com/";
	public static final String PROMO_CODE = "SIMCARD5";
	public List<String> products;
	static JavascriptExecutor javaScript;

	@BeforeSuite(alwaysRun = true)
	public void getDevicesList() {

		ProductsAPIV3 productsAPIV3 = new ProductsAPIV3();
		try {
			products = productsAPIV3.getTmngProducts();
			File file = new File("src/test/resources/testdata/tmngAvailiableDevices.txt");
			FileWriter fstream = new FileWriter(file);
			BufferedWriter writer = new BufferedWriter(fstream);
			for (String product : products) {
				writer.write(product + "\n");
			}
			writer.close();

		} catch (Exception e) {
			Reporter.log("Error: " + e.getMessage());
		}
	}

	@SuppressWarnings("deprecation")
	public void loadTmngURL(TMNGData tMNGData) {
		try {
			SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
			Reporter.log("Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/"
					+ sessionId
					+ "\">SauceLabs</a>|<a target=\"_blank\" href=\"http://prdplctep000d.unix.gsm1900.org/videos/"
					+ sessionId + ".mp4" + "\"> Sbox</a>");
			logger.info("getTMOURL method called ");
			String tmngUrl = null;
			String environmentVariable = null;
			String environment = tMNGData.getEnv();
			if (StringUtils.isEmpty(environment)) {
				environment = System.getProperty("environment");
				tmngUrl = environment;
				environmentVariable = readEnvFromUrl(environment);
				tMNGData.setEnv(environmentVariable);
			}
			if (StringUtils.isNotEmpty(environmentVariable) && StringUtils.isEmpty(environment)) {
				StringBuilder mytmoBuilder = new StringBuilder();
				mytmoBuilder.append(TMO_URL);
				tmngUrl = getTestConfig().getConfig(mytmoBuilder.toString());
			}
			launchTmng(tmngUrl);
		} catch (Exception ex) {
			Reporter.log("caught exception while loading ");
		}
	}

	private void launchTmng(String tmngUrl) {
		getDriver().get(tmngUrl);
		Reporter.log("Url is provided: " + tmngUrl);
	}

	private static String readEnvFromUrl(String environment) {
		String substring = environment.substring(environment.indexOf("//"), environment.indexOf("."));
		return substring.replaceAll("[^A-Za-z0-9 ]", "");
	}

	/**
	 * We are using this method to verifying the page load status Check Page Is
	 * Ready
	 */
	public void checkPageIsReady() {
		javaScript = (JavascriptExecutor) getDriver();
		try {
			for (int i = 0; i < 30; i++) {

				Thread.sleep(2000);
				if ("complete".equalsIgnoreCase(javaScript.executeScript("return document.readyState").toString())) {
					Thread.sleep(5000);
					break;
				}
			}
		} catch (Exception ex) {
			Reporter.log("caught exception while loading");
		}

	}

	/**
	 * Navigate to My tmo Home page
	 *
	 */
	public void navigateToMyTMOHome(TMNGData tMNGData) {
		getDriver().get(My_TMOLogin);
		Reporter.log("Navigated to: " + My_TMOLogin);
		HomePage homePage = new HomePage(getDriver());
		homePage.performMyTMOLoginAction(tMNGData);

	}

	// common methods

	/**
	 * Navigate to Phones page
	 *
	 */
	public PlpPage navigateToPhonesPlpPage(TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		PlpPage phonesPlpPage = new PlpPage(getDriver());
		String env = System.getProperty("environment").trim();
		if (env.contains("dmo") || env.contains("uatent") || env.contains("www2") || env.contains("stag")
				|| env.contains("qat") || env.contains("qatb01") || env.contains("qa1") || env.contains("tos")
				|| env.contains("uat2")) {
			if (env.endsWith("/")) {
				env = env.substring(0, env.length() - 1);
			}
			getDriver().get(env + phonesPageUrl);
		} else {
			HomePage homePage = new HomePage(getDriver());
			homePage.clickPhonesLink();
		}
		setLocation();
		phonesPlpPage.verifyPhonesPlpPageLoaded();
		return phonesPlpPage;
	}

	/**
	 * Navigate to Tablets and Devices page
	 *
	 */
	public PlpPage navigateToTabletsAndDevicesPLP(TMNGData tMNGData) {
		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickOnTabletsAndDevicesLink();
		PlpPage tabletsAndDevicesPlpPage = new PlpPage(getDriver());
		tabletsAndDevicesPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		return tabletsAndDevicesPlpPage;
	}

	/**
	 * Navigate to Watches page
	 *
	 */
	public PlpPage navigateToWatchesPLP(TMNGData tMNGData) {
		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickOnWatchesLink();
		PlpPage watchesPlpPage = new PlpPage(getDriver());
		watchesPlpPage.verifyWatchesPlpPageLoaded();
		return watchesPlpPage;
	}

	/**
	 * Navigate to Accessories PLP page
	 *
	 */
	public PlpPage navigateToAccessoryPLP(TMNGData tMNGData) {
		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickOnAccessoriesMenuLink();
		PlpPage accessoriesPlpPage = new PlpPage(getDriver());
		accessoriesPlpPage.verifyAccessoriesPlpPageLoaded();
		return accessoriesPlpPage;
	}

	/**
	 * Navigate to Phones Product details page(Main)
	 *
	 */
	public PdpPage navigateToPhonesPDP(TMNGData tMNGData) {
		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getDeviceName());
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		return phonesPdpPage;
	}

	/**
	 * Navigate to Tablets and Devices page
	 *
	 */
	public PdpPage navigateToTabletsAndDevicesPDP(TMNGData tMNGData) {
		PlpPage tabletsAndDevicesPlpPage = navigateToTabletsAndDevicesPLP(tMNGData);
		tabletsAndDevicesPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		PdpPage tabletsAndDevicesPdpPage = new PdpPage(getDriver());
		tabletsAndDevicesPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		return tabletsAndDevicesPdpPage;
	}

	/**
	 * Navigate to Watches page
	 *
	 */
	public PdpPage navigateToWatchesPDP(TMNGData tMNGData) {
		PlpPage watchesPlpPage = navigateToWatchesPLP(tMNGData);
		watchesPlpPage.clickDeviceWithAvailability(tMNGData.getWatchName());
		PdpPage watchesPdpPage = new PdpPage(getDriver());
		watchesPdpPage.verifyWatchesPdpPageLoaded();
		return watchesPdpPage;
	}

	/**
	 * Navigate to Accessory Main Product details page
	 *
	 */
	public PdpPage navigateToAccessoryMainPDP(TMNGData tMNGData) {
		PlpPage accessoriesPlpPage = navigateToAccessoryPLP(tMNGData);
		accessoriesPlpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryPdpPage = new PdpPage(getDriver());
		accessoryPdpPage.verifyAccessoriesPdpPageLoaded();
		return accessoryPdpPage;
	}

	/**
	 * Navigate to cart page
	 *
	 */
	public CartPage navigateToCartPageBySelectingPhone(TMNGData tMNGData) {
		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		return cartPage;
	}

	/**
	 * Navigate to cart page
	 *
	 */
	public CartPage navigateToCartPageBySelectingAccessory(TMNGData tMNGData) {
		PdpPage phonesPdpPage = navigateToAccessoryMainPDP(tMNGData);
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		return cartPage;
	}

	/**
	 * Navigate to cart page by adding Tablet from main PLP
	 *
	 */
	public CartPage navigateToCartPageBySelectingTabletFromMainPLP(TMNGData tMNGData) {
		PdpPage tabletsAndDevicesPdpPage = navigateToTabletsAndDevicesPDP(tMNGData);
		tabletsAndDevicesPdpPage.clickOnAddToCartBtn();
		tabletsAndDevicesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		return cartPage;
	}

	/**
	 * Navigate to cart page by adding Tablet from main PLP
	 *
	 */
	public CartPage navigateToCartPageBySelectingWearableFromMainPLP(TMNGData tMNGData) {

		PdpPage tabletsAndDevicesPdpPage = navigateToWatchesPDP(tMNGData);
		tabletsAndDevicesPdpPage.clickOnAddToCartBtn();
		tabletsAndDevicesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		return cartPage;
	}

	/**
	 * Navigate to checkout
	 *
	 */
	public CheckOutPage navigateToCheckoutWithPhone(TMNGData tMNGData) {
		navigateToPhonesPDP(tMNGData);
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.clickOnAddToCartBtn();
		PdpPage pdpPage = new PdpPage(getDriver());
		pdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnCartContinueBtn();
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.verifyCheckOutPageLoaded();
		checkOutPage.verifyPersonalInfo();
		return checkOutPage;
	}

	/**
	 * Navigate to Empty Cart
	 *
	 */
	public CartPage navigateToEmptyCart(TMNGData tMNGData) {
		SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
		Reporter.log(
				"Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/" + sessionId
						+ "\">SauceLabs</a>|<a target=\"_blank\" href=\"http://prdplctep000d.unix.gsm1900.org/videos/"
						+ sessionId + ".mp4" + "\"> Sbox</a>");
		getDriver().get(System.getProperty("environment") + EMPTYCARTURL);
		Reporter.log("Environment: " + System.getProperty("environment"));
		setLocation();
		CartPage emptyCartPage = new CartPage(getDriver());
		emptyCartPage.verifyEmptyCart();
		return emptyCartPage;
	}

	public BringYourOwnPhonePage navigateToBringYourOwnPhonePage(TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickMenuMenuButton();
		homePage.clickBringYourOwnPhoneLink();
		BringYourOwnPhonePage bringYourOwnPage = new BringYourOwnPhonePage(getDriver());
		bringYourOwnPage.verifyPageLoaded();
		return bringYourOwnPage;

	}

	public DealsPage navigateToDealsPage(TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickDeals1Link();
		DealsPage dealspage = new DealsPage(getDriver());
		dealspage.verifyPageLoaded();
		return dealspage;
	}

	public DigitsPage navigateToDigitsPage(TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickMenuMenuButton();
		homePage.clickUseMultipleNumbersOnMyPhoneLink();
		DigitsPage digitsPage = new DigitsPage(getDriver());
		digitsPage.verifyPageLoaded();
		return digitsPage;

	}

	/**
	 * Navigate to Store Locator Page from Home Page
	 *
	 */
	public StoreLocatorPage navigateToStoreLocatorPage(TMNGData tMNGData) {
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		try {
			getDriver().get(System.getProperty("environment") + STORELOCATORURL);
			SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
			Reporter.log("Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/"
					+ sessionId
					+ "\">SauceLabs</a>|<a target=\"_blank\" href=\"http://prdplctep000d.unix.gsm1900.org/videos/"
					+ sessionId + ".mp4" + "\"> Sbox</a>");
			Reporter.log("Store locator page is displayed");
			setLocation();
			checkPageIsReady();
		} catch (Exception e) {
			Assert.fail("Store locator page is not displayed");
		}
		return storeLocatorPage;
	}

	public WeWillHelpYouJoinUsPage navigateToWeWillHelpYouToJoinUsPage(TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickMenuMenuButton();
		homePage.clickWellHelpYouJoinLink();
		WeWillHelpYouJoinUsPage weWillHelpYouJoinUsPage = new WeWillHelpYouJoinUsPage(getDriver());
		weWillHelpYouJoinUsPage.verifyPageLoaded();
		return weWillHelpYouJoinUsPage;

	}

	public CheckOutTheCoveragePage navigateToCheckOutTheCoveragePage(TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickMenuMenuButton();
		homePage.clickCheckOutTheCoverageLink();
		CheckOutTheCoveragePage checkOutTheCoveragePage = new CheckOutTheCoveragePage(getDriver());
		checkOutTheCoveragePage.verifyCheckOutTheCoveragePage();
		return checkOutTheCoveragePage;

	}

	/**
	 * Navigate to InternationalCalling Page from Overlay Menu
	 *
	 */
	public InternationalCallingPage navigateToInternationalCallingPage(TMNGData tMNGData) {
		InternationalCallingPage internationalCallingPage = new InternationalCallingPage(getDriver());
		try {
			loadTmngURL(tMNGData);
			/*
			 * HomePage homePage = new HomePage(getDriver());
			 * homePage.verifyHamburgerMenuButton();
			 * homePage.clickHamburgerMenuAndverifyInternationalCallingLink();
			 * homePage.clickInternationalCallingLink();
			 */
			internationalCallingPage.verifyPageLoaded();
		} catch (Exception e) {
			Assert.fail("Caught exception while loading Roaming page");
		}
		return internationalCallingPage;
	}

	public void navigateToCheckOutCoveragePage(TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickMenuMenuButton();
		homePage.clickCheckOutTheCoverageLink();

	}

	public PlansPage navigateToPlansPage(TMNGData tMNGData) {

		loadTmngURL(tMNGData);
		String env = System.getProperty("environment").trim();
		if (env.contains("dmo") || env.contains("uatent") || env.contains("www2") || env.contains("stag")
				|| env.contains("qat") || env.contains("qatb01") || env.contains("qa1") || env.contains("tos")
				|| env.contains("uat2")) {
			if (env.endsWith("/")) {
				env = env.substring(0, env.length() - 1);
			}
			getDriver().get(env + plansPageUrl);
		} else {
			HomePage homePage = new HomePage(getDriver());
			homePage.clickPlansLink();
		}
		setLocation();
		PlansPage plansPage = new PlansPage(getDriver());
		plansPage.verifyPageLoaded();
		return plansPage;
	}

	/**
	 * Navigate to Store Locator Page from Overlay Menu
	 *
	 */
	public StoreLocatorPage navigateToStoreLocatorPageFromOverlayMenu(TMNGData tMNGData) {
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		try {
			loadTmngURL(tMNGData);
			HomePage homePage = new HomePage(getDriver());
			homePage.clickMenuMenuButton();
			storeLocatorPage.clickMobileStoreLocaterLink();
			String tmoUrl = "https://www.t-mobile.com/store-locator";
			if (getDriver().getCurrentUrl().toString().contains(tmoUrl)) {
				getDriver().navigate().to(System.getProperty("environment") + "/store-locator");
			}
		} catch (Exception e) {
			Assert.fail("Caught exception while loading StoreLocator page");
		}
		return storeLocatorPage;
	}

	public SwitchToTmobilePage navigateToSwitchToTmobilePage(TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickMenuMenuButton();
		homePage.clickAreYouWithUs5Link();
		SwitchToTmobilePage switchToTmobilePage = new SwitchToTmobilePage(getDriver());
		switchToTmobilePage.verifyPageLoaded();
		return switchToTmobilePage;

	}

	public TravelAbroadPage navigateToTravelAbroadPage(TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickMenuMenuButton();
		homePage.clickTravelingAbroadLink();
		TravelAbroadPage travelAbroadPage = new TravelAbroadPage(getDriver());
		travelAbroadPage.verifyPageLoaded();
		return travelAbroadPage;

	}

	public UnlimitedPlansPage navigateToUnlimitedPlansPage(TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickMenuMenuButton();
		homePage.clickUnlimitedPlanLink();
		UnlimitedPlansPage unlimitedPlanPage = new UnlimitedPlansPage(getDriver());
		unlimitedPlanPage.verifyPageLoaded();
		return unlimitedPlanPage;

	}

	/**
	 * navigate to cart page using deeplink
	 *
	 * 
	 */
	public void navigateToCartPageUsingDeeplink(String cartId) {
		String environment = System.getProperty("environment");
		System.out.println(environment + "cart/SC?id=" + cartId);
		getDriver().get(environment + "cart/SC?id=" + cartId);
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageUrl();
	}

	/**
	 * Set Geo Location
	 *
	 */

	public void setLocation() {

		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript(
				"window.navigator.geolocation.getCurrentPosition = function(success){ var position = {coords : {  latitude : 34.036885,  longitude : -118.144194 }  };  success(position);}");

//		js.executeScript("window.alert = function() { console.log.apply(console, arguments); };");
//		js.executeScript("navigator.geolocation.getCurrentPosition(function(position) { alert('allow'); }, function() { alert('deny'); });");
//			DesiredCapabilities caps = new DesiredCapabilities();	
//			ChromeOptions options = new ChromeOptions();
//			HashMap<String, Object> prefs = new HashMap<String, Object>();
//			prefs.put("profile.default_content_setting_values.geolocation", 1); // 1:allow;
//																				// 2:block
//			options.setExperimentalOption("prefs", prefs);
//			caps.setCapability(ChromeOptions.CAPABILITY, options);
//			((LocationContext) new Augmenter().augment(getDriver()))
//					.setLocation(new Location(47.672049, -122.1706612, 0));
		Reporter.log("Location is set");
	}

	/**
	 * Navigate to Order Detailed page from cart page
	 *
	 */

	public CartPage navigateToOrderDetailedPagefromCart(TMNGData tMNGData) {
		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickOnViewOrderDetailsLink();
		cartPage.verifyViewOrderDetailsModel();
		return cartPage;
	}

	/**
	 * Navigate to Web Save cart Model from cart page
	 *
	 */

	public CartPage navigateToWebSaveCartModalfromCart(TMNGData tMNGData) {
		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickOnSaveCart();
		cartPage.verifySaveCartCtaInModalWindow();

		return cartPage;
	}

	/**
	 * Navigate to Web Save cart Model from cart page
	 *
	 */

	public CartPage navigateToRetailSaveCartModalfromCart(TMNGData tMNGData) {

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickCompleteInStoreLink();
		cartPage.verifyCompleteStoreModalWindow();

		return cartPage;
	}

	public String getTogglevalue(String expval) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request;
		String env = System.getProperty("environment").trim();
		if (env.contains("dmo") || env.contains("uatent") || env.contains("www2") || env.contains("stag")
				|| env.contains("qat")) {
			if (env.endsWith("/")) {
				env = env.substring(0, env.length() - 1);
			}
			request = new HttpGet(env + "/config/aemToggle.json");
		} else {
			request = new HttpGet("https://www.t-mobile.com/config/aemToggle.json");
		}
		try {
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();
			String content = EntityUtils.toString(entity);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(content);
			ApiCommonLib obj = new ApiCommonLib();
			String toggleValue = obj.getPathVal(jsonNode, "toggles." + expval);
			return toggleValue;
		} catch (Exception e) {
			Assert.fail("Failed to get Toggle value : " + expval);
		}
		return "false";
	}

	public AccessoriesHubPage navigateToAccessoriesHubPage(TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickMenuMenuButton();
		homePage.clickAccessoriesHubLink();
		AccessoriesHubPage accessoriesHubPage = new AccessoriesHubPage(getDriver());
		accessoriesHubPage.verifyAccessoriesHubPageUrl();
		return accessoriesHubPage;

	}

	/**
	 * Navigate to Trade In Page from Overlay Menu
	 *
	 */
	public TradeInPage navigateToTradeInPage(TMNGData tMNGData) {
		TradeInPage tradeInPage = new TradeInPage(getDriver());
		try {
			loadTmngURL(tMNGData);
			HomePage homePage = new HomePage(getDriver());
			homePage.clickTradeInProgramLink();
			tradeInPage.verifyTradeInDevicePage();
		} catch (Exception e) {
			Assert.fail("Caught exception while loading Trade In page");
		}
		return tradeInPage;
	}

	/**
	 * Navigate to Roaming Page from Overlay Menu
	 *
	 */
	public RoamingPage navigateToRoamingPage(TMNGData tMNGData) {
		RoamingPage roamingPage = new RoamingPage(getDriver());
		try {
//			loadTmngURL(tMNGData);
//			HomePage homePage = new HomePage(getDriver());
//			homePage.verifyHamburgerMenuButton();
//			homePage.clickHamburgerMenuAndverifyTravelingAbroadLink();
//			homePage.clickTravelingAbroadLink();
//			TravelAbroadPage travelAbroadPage = new TravelAbroadPage(getDriver());
//			travelAbroadPage.verifyAndClickCheckADestination();
			getDriver().navigate().to(System.getProperty("environment"));
			roamingPage.verifyRoamingPage();
		} catch (Exception e) {
			Assert.fail("Caught exception while loading Roaming page");
		}
		return roamingPage;
	}

	/**
	 * navigate to BYOD Page
	 * 
	 */
	public BringYourOwnPhonePage navigateToBYODPage(TMNGData tMNGData) {
		loadTmngURL(tMNGData);
		setLocation();
		getDriver().get(BYOD_PAGE);
		BringYourOwnPhonePage bringYourOwnPhonePage = new BringYourOwnPhonePage(getDriver());
		bringYourOwnPhonePage.verifyPageLoaded();
		return bringYourOwnPhonePage;
	}

	/**
	 * Navigate to cart page by adding T-Mobile® 3-in-1 SIM Starter Kit from Main
	 * PLP
	 *
	 */
	public CartPage navigateToCartPageByAddingPhonesSSKFromMainPLP(TMNGData tMNGData) {
		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);
		phonesPlpPage.selectSimCardFromPlp();
		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		return cartPage;
	}

	/**
	 * Navigate to cart page with full Payment option
	 *
	 */
	public CartPage navigateToCartPageWithFullPayment(TMNGData tMNGData) {
		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickPayInFullPaymentOption();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		return cartPage;
	}

	/**
	 * Navigate to Store Locator Page directly from url
	 *
	 */
	public void navigateToStoreLocatorPageDirectlyThroughUrl() {
		try {
			checkPageIsReady();
			getDriver().navigate().to(System.getProperty("environment"));
			Reporter.log("Application launched successfully");
		} catch (Exception e) {
			Assert.fail("Caught exception while loading StoreLocator page");
		}

	}

	/**
	 * Navigate to Store Locator Page in QAT
	 *
	 */
	public StoreLocatorPage navigateToStoreLocatorPageInQAT(TMNGData tMNGData) {
		StoreLocatorPage storeLocatorPage = new StoreLocatorPage(getDriver());
		try {
			loadTmngURL(tMNGData);
		} catch (Exception e) {
			Assert.fail("Caught exception while loading StoreLocator page");
		}
		return storeLocatorPage;
	}

	/**
	 * Navigate to Tpp PreScreen Intro page from PLP
	 *
	 */
	public TPPPreScreenIntro navigateToTPPPreScreenIntro(TMNGData tMNGData) {
		PlpPage phonesPlp = navigateToPhonesPlpPage(tMNGData);
		phonesPlp.verifyFindYourPricingLink();
		phonesPlp.clickFindYourPricingLink();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyLetsGoCTAOnPreScreenIntroPage();
		return tppPreScreenIntro;
	}

	/**
	 * Navigate to Tpp PreScreen Form page from PLP
	 *
	 */
	public TPPPreScreenForm navigateToTPPPreScreenFormPage(TMNGData tMNGData) {
		PlpPage phonesPlp = navigateToPhonesPlpPage(tMNGData);
		phonesPlp.clickFindYourPricingLink();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyLetsGoCTAOnPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();

		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		return tppPreScreenForm;
	}

	/**
	 * Navigate to Tpp PreScreen Form page from PLP
	 *
	 */
	public CheckOutPage navigateToTPPHCCForm(TMNGData tMNGData) {
		navigateToCheckoutWithPhone(tMNGData);
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.fillPersonalInfo(tMNGData);
		checkOutPage.clickTermsNConditions();
		checkOutPage.clickNextBtn();
		checkOutPage.verifyLetsCheckYourCreditHeader();
		return checkOutPage;
	}

	/**
	 * Navigate to Cart Page by Adding device from Empty Cart for TPP UNO
	 *
	 */
	public CartPage navigateToCartPagefromEmptyCartFromUNOPLP(TMNGData tMNGData) {
		CartPage emptyCartPage = navigateToEmptyCart(tMNGData);
		emptyCartPage.addAPhonetoCartFromCartPage(tMNGData);
		return emptyCartPage;
	}

	/**
	 * Navigate to Tpp PreScreen Form page from Accessory PDP
	 *
	 */
	public TPPPreScreenForm navigateToTPPPreScreenFormPageFromAccessoryPDP(TMNGData tMNGData) {
		PdpPage accessoryPdpPage = navigateToAccessoryMainPDP(tMNGData);
		accessoryPdpPage.clickFindYourPricingLinkInPDP();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();

		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		return tppPreScreenForm;
	}

	/**
	 * Navigate to Tpp PreScreen Form page from Device PDP
	 *
	 */
	public TPPPreScreenForm navigateToTPPPreScreenFormPageFromDevicePDP(TMNGData tMNGData) {
		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickFindYourPricingLinkInPDP();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		return tppPreScreenForm;
	}

	/**
	 * Navigate to Tpp PreScreen Form page from Device Cart Page
	 *
	 */
	public TPPPreScreenForm navigateToTPPPreScreenFormPageFromDeviceCart(TMNGData tMNGData) {

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyCartPageLoaded();
		cartPage.verifyFindYourPricingCTA();
		cartPage.clickFindYourPricingCTA();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPreScreenForm = new TPPPreScreenForm(getDriver());
		tppPreScreenForm.verifyPreScreenForm();
		return tppPreScreenForm;
	}

	public String getDeviceAvailability(String device) {
		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request;
		String env = System.getProperty("environment").trim();
		if (env.contains("dmo") || env.contains("uatent") || env.contains("www2") || env.contains("stag")
				|| env.contains("qat")) {
			request = new HttpGet(env + "/srvspub/browse.main.cell-phones.json");
		} else {
			request = new HttpGet("https://www.t-mobile.com/srvspub/browse.main.cell-phones.json");
		}
		try {
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();
			String content = EntityUtils.toString(entity);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(content);
			JsonPath jsonPath = new JsonPath(mapper.writeValueAsString(jsonNode));

			List<Object> products = jsonPath.getList("values.products");
			List<String> productName, availability, displayName;
			HashMap<String, String> productDetails = new HashMap<>();

			for (int i = 0; i < products.size(); i++) {
				productName = jsonPath.getList("values.products.productName");
				availability = jsonPath.getList("values.products.availability");
				displayName = jsonPath.getList("values.products.productDisplayName");
				productDetails.put(productName.get(i), availability.get(i));
				// productDetails.put(productName.get(i),displayName.get(i));
			}
			Reporter.log("Products:" + productDetails);

			Iterator<?> iterator = productDetails.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry mapElement = (Map.Entry) iterator.next();
				if (mapElement.getKey().equals(device)) {
					String dev = mapElement.getValue().toString();
					Reporter.log("Device: " + device + " and Availability Status:" + dev);
				} /*
					 * else{ String dev = mapElement.getKey(). }
					 */
			}
			return device;
		} catch (Exception e) {
			Assert.fail("Failed to get device availability");
		}
		return "false";
	}

	public void getOrderNumber() {

		try {
			List<LogEntry> entries = getDriver().manage().logs().get(LogType.PERFORMANCE).getAll();
			Reporter.log(entries.size() + " " + LogType.PERFORMANCE + " log entries found");
			for (LogEntry entry : entries) {
				Reporter.log(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
			}

			/*
			 * HttpClient client = HttpClientBuilder.create().build(); HttpGet request;
			 * String env = System.getProperty("environment").trim(); if
			 * (env.contains("dmo") || env.contains("uatent") || env.contains("www2") ||
			 * env.contains("stag") || env.contains("qat")) { request = new HttpGet(env +
			 * "/commerce/v3/orders"); } else { request = new
			 * HttpGet("https://api.t-mobile.com/commerce/v3/orders"); } try { HttpResponse
			 * response = client.execute(request); HttpEntity entity = response.getEntity();
			 * String content = EntityUtils.toString(entity); ObjectMapper mapper = new
			 * ObjectMapper(); JsonNode jsonNode = mapper.readTree(content); // JsonPath
			 * jsonPath = new JsonPath(mapper.writeValueAsString(jsonNode));
			 * 
			 * JsonNode orderNode = jsonNode.path("orders");
			 * 
			 * String orderNumber=orderNode.get(0).path("orderNumber").asText();
			 * System.out.println("Order number: " +orderNumber);
			 * 
			 * String orderId=orderNode.get(0).path("orderId").asText();
			 * System.out.println("Order number: " +orderId);
			 * 
			 * String
			 * externalOrderNumber=orderNode.get(0).path("externalOrderNumber").asText();
			 * System.out.println("External Order Number: " +externalOrderNumber);
			 * 
			 * 
			 * return orderNumber;
			 */

		} catch (Exception e) {
			Assert.fail("Failed to get Order number");
		}
		// return null;
	}

	/**
	 * Navigate To T-mobile Home Page for UNAV stories
	 * 
	 * @param myTmoData
	 * @return
	 */
	public HomePage navigateToTMobilePage(TMNGData tMNGData) {
		HomePage homePage = new HomePage(getDriver());
		try {
			getDriver().navigate().to(System.getProperty("environment"));
			Reporter.log("T-mobile Home page is displayed");
		} catch (Exception e) {
			Reporter.log("Unable to navigate to T-Mobile home page");
		}
		return homePage;
	}

	/**
	 * Login To MyTmo
	 * 
	 * @param myTmoData
	 */
	public void launchAndPerformLogin(MyTmoData myTmoData) {
		String misdin = myTmoData.getLoginEmailOrPhone();
		String pwd = myTmoData.getLoginPassword();
		myTmoData.setLoginEmailOrPhone(misdin);
		myTmoData.setLoginPassword(pwd);
		System.out.println(myTmoData.getTestName());
		System.out.println(myTmoData.getLoginEmailOrPhone());
		SessionId sessionId = ((RemoteWebDriver) getDriver()).getSessionId();
		Reporter.log(
				"Click to view session in <a target=\"_blank\" href=\"https://saucelabs.com/beta/tests/" + sessionId
						+ "\">SauceLabs</a>|<a target=\"_blank\" href=\"http://prdplctep000d.unix.gsm1900.org/videos/"
						+ sessionId + ".mp4" + "\"> Sbox</a>");
		LoginPage loginPage = new LoginPage(getDriver());
		//launchSite(myTmoData, loginPage);
		performLogin(myTmoData, loginPage);
		chooseAccount(myTmoData);
		loginPermission();
	}

	/**
	 * Launch MyTmo
	 * 
	 * @param myTmoData
	 * @param loginPage
	 */
	public void launchSite(MyTmoData myTmoData, LoginPage loginPage) {
		try {
			getTMOURL(myTmoData);
		} catch (Exception e) {
			logger.error(Constants.UNABLE_TO_UNLOCK + e);
		}
	}

	/**
	 * 
	 * @param myTmoData
	 */
	@SuppressWarnings("deprecation")
	public void getTMOURL(MyTmoData myTmoData) {
		logger.info("getTMOURL method called ");
		String mytmoURL = null;
		String environmentVariable = null;
		String environment = myTmoData.getEnv();
		if (StringUtils.isEmpty(environment)) {
			environment = System.getProperty("environment");
			mytmoURL = environment;
			environmentVariable = readEnvFromUrl(environment);
			myTmoData.setEnv(environmentVariable);
		}
		if (StringUtils.isNotEmpty(environmentVariable) && StringUtils.isEmpty(environment)) {
			StringBuilder mytmoBuilder = new StringBuilder();
			mytmoBuilder.append(Constants.MYTMO_URL);
			mytmoURL = getTestConfig().getConfig(mytmoBuilder.toString());
		}
		launchURL(mytmoURL);
	}

	/**
	 * Launch MyTmo URL
	 * 
	 * @param url
	 */
	private void launchURL(String url) {
		getDriver().manage().deleteAllCookies();
		getDriver().getWindowHandles().clear();
		if (getDriver() instanceof AppiumDriver) {
			try {
				for (int i = 0; i < 2; i++) {
					getDriver().get(url);
					waitForPageLoad(getDriver());
					if (getDriver().getCurrentUrl().contains("account.t-mobile.com"))
						break;
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} else {
			getDriver().get(url);
			waitForPageLoad(getDriver());
			CommonPage commonPage = new CommonPage(getDriver());
			try {
				commonPage.verifyLoginPageTitle();
				Reporter.log("Application(T-Mobile URL '" + url + "') Loaded");
			} catch (Exception e) {
				Assert.fail("Login Page not loaded");
			}
		}

	}

	public static void waitForPageLoad(WebDriver driver) {
		JavascriptExecutor javaScript = (JavascriptExecutor) driver;
		for (int i = 0; i < 30; i++) {
			try {
				if ("complete".equalsIgnoreCase(javaScript.executeScript("return document.readyState").toString())) {
					Thread.sleep(2000);
					break;
				}
			} catch (Exception ex) {
				Reporter.log("caught exception while loading" + ex.getMessage());
			}
		}
	}

	/**
	 * Navigate To Home Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public NewHomePage navigateToNewHomePage(MyTmoData myTmoData) {
		launchAndPerformLogin(myTmoData);
		NewHomePage homePage = new NewHomePage(getDriver());
		try {
			homePage.verifyHomePage();
		} catch (Exception e) {
			Reporter.log("Home page not displayed");
		}
		return homePage;
	}

	/**
	 * Perform Login
	 * 
	 * @param myTmoData
	 * @param loginPage
	 */
	protected void performLogin(MyTmoData myTmoData, LoginPage loginPage) {
		try {
			String username = myTmoData.getLoginEmailOrPhone().trim();
			String password = myTmoData.getLoginPassword().trim();
			if (getDriver().getTitle().contains("Login") || getDriver().getTitle().contains("Access Messages")) {
				loginPage.performLoginAction(username, password);
			}
			/*
			 * if (loginPage.passwordOldorInCorrectPassword(myTmoData, loginPage)) {
			 * Assert.fail("Login Unsuccessful=>Test Data Corrupted"); }
			 */
		} catch (Exception e) {
			Reporter.log("Login Unsuccessful");
		}
		try {
			if (loginPage.verifyAccountVerification().equals(true)) {
				loginPage.clickOnTextMsgNextBtn();
				loginPage.selectMsisdnToVerify(myTmoData.getLoginEmailOrPhone());
				String acctVrfnPin = accountVerification(myTmoData);
				loginPage.enterConfirmationCode(acctVrfnPin);
				loginPage.clickOnConfirmationCodeNextBtn();
			}
		} catch (Exception acctVrfnTitleException) {
		}
		Reporter.log(Constants.LOGIN_AS + myTmoData.getLoginEmailOrPhone() + "/" + myTmoData.getLoginPassword());
	}

	public String accountVerification(MyTmoData myTmoData) {
		logger.info("accountVerification method called");
		// clear Counters
		String smsPin = null;
		RequestData requestData = new RequestData();
		String environment = System.getProperty(Constants.IAM_SERVER_ENVIRONEMT);
		// +System.getProperties().toString());
		requestData.setMsisdn(myTmoData.getLoginEmailOrPhone());
		// requestData.setEnvironment("PPD");
		requestData.setEnvironment(environment);
		requestData.setProfileType("TMO");
		String authCode = new RestServiceHelper().getAuthCode(environment);
		requestData.setToken(authCode);
		// myTmoData.getLoginEmailOrPhone());
		long startTime = System.currentTimeMillis();
		logger.info("startTime::::::{}", startTime);
		try {
			smsPin = new RestServiceHelper().getProfilePin(requestData);
		} catch (Exception e) {
			e.printStackTrace();
			// e.printStackTrace();
			// logger.error(Constants.UNABLE_TO_UNLOCK + e);
			// throw new FrameworkException(Constants.UNABLE_TO_UNLOCK, e);
		}
		long stopTime = System.currentTimeMillis();
		logger.info("stopTime::::::{}", stopTime);
		long elapsedTime = stopTime - startTime;
		logger.info("elapsedTime::::::{}", elapsedTime);
		return smsPin;
	}

	/**
	 * @param myTmoData
	 */
	public void chooseAccount(MyTmoData myTmoData) {
		waitForPageLoad(getDriver());
		ChooseAccountPage chooseAccountPage = new ChooseAccountPage(getDriver());
		if (getDriver().getTitle().contains("AccountChooser")) {
			chooseAccountPage.chooseAccount(myTmoData.getLoginEmailOrPhone());
		}
	}

	public void loginPermission() {
		if (getDriver().getCurrentUrl().contains("login/permissions.html")) {
			getDriver().get(System.getProperty("environment") + "/home.html");
		}
	}

	/**
	 * Navigate to Empty Cart With Delete Cookies
	 *
	 */
	public CartPage navigateToEmptyCartWithDeleteCookies(TMNGData tMNGData) {

		CartPage emptyCartPage = new CartPage(getDriver());
		String env = System.getProperty("environment").trim();

		((JavascriptExecutor) getDriver()).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(getDriver().getWindowHandles());

		System.out.println("Before Delete Cookies " + getDriver().manage().getCookies().toString() + " ------------");
		getDriver().manage().deleteAllCookies();
		System.out.println("After Delete Cookies" + getDriver().manage().getCookies().toString() + " ------------");
		getDriver().switchTo().window(tabs.get(1));
		getDriver().manage().deleteAllCookies();
		getDriver().get(env + EMPTYCARTURL);
		emptyCartPage.verifyEmptyCart();
		return emptyCartPage;
	}
}
