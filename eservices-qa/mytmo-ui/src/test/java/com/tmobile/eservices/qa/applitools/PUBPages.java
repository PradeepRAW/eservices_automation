package com.tmobile.eservices.qa.applitools;

import java.time.LocalDateTime;

import org.testng.annotations.Test;

import com.applitools.eyes.BatchInfo;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class PUBPages extends PaymentCommonLib{
	ApplitoolsLib applitoolsLib = new ApplitoolsLib();
	BatchInfo batchInfo = new BatchInfo("accounts " +  LocalDateTime.now());
	/**
	 * Do visual testing for AccountHistoryPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testAccountHistoryPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAccountHistoryPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "AccountHistoryPage", batchInfo);
	}
	
	/**
	 * Do visual testing for AddBankPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testAddBankPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAddBankPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "AddBankPage", batchInfo);
	}
	
	/**
	 * Do visual testing for AddCardPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testAddCardPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAddCardPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "AddCardPage", batchInfo);
	}
	
	/**
	 * Do visual testing for AutoPayCancelConfirmationPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testAutopayCancelConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayCancelConfirmationPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "AutoPayCancelConfirmationPage", batchInfo);
	}
	
	/**
	 * Do visual testing for AutoPayConfirmationPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testAutopayConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayConfirmationPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "AutoPayConfirmationPage", batchInfo);
	}
	
	/**
	 * Do visual testing for AutoPayPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testAutoPayPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "AutoPayPage", batchInfo);
	}
	
	/**
	 * Do visual testing for BillAndPaySummaryPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testBillAndPaySummaryPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToBillAndPaySummaryPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "BillAndPaySummaryPage", batchInfo);
	}
	
	/**
	 * Do visual testing for BillDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testBillDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToBillDetailsPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "BillDetailsPage", batchInfo);
	}
	
	/**
	 * Do visual testing for BillingSummaryPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testBillingSummaryPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToBillingSummaryPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "BillingSummaryPage", batchInfo);
	}
	
	/**
	 * Do visual testing for CallDetailRecordsForwardPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testCallDatailRecordsForwardPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToCallDetailRecordsForwardPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "CallDetailRecordsForwardPage", batchInfo);
	}
	
	/**
	 * Do visual testing for ChargeEquipementPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testChargeEquipementPage(ControlTestData data, MyTmoData myTmoData) {

		applitoolsLib.doVisualTest(getDriver(),"payments", "ChargeEquipementPage", batchInfo);
	}
	
	/**
	 * Do visual testing for ChargeFeaturesPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testChargeFeaturesPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToChargeFeaturesPage(myTmoData, "");
		applitoolsLib.doVisualTest(getDriver(),"payments", "ChargeFeaturesPage", batchInfo);
	}
	
	/**
	 * Do visual testing for ChargeOneTimePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testChargeOnetimePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToChargeOneTimePage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "ChargeOneTimePage", batchInfo);
	}
	
	/**
	 * Do visual testing for ChargePlanPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testChargePlanPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToChargePlanPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "ChargePlanPage", batchInfo);
	}
	
	/**
	 * Do visual testing for EIPDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testEIPDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEIPDetailsPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "EIPDetailsPage", batchInfo);
	}
	
	/**
	 * Do visual testing for EIPPaymentConfirmationCCPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testEIPPaymentConfirmationCCPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEIPPaymentConfirmationCCPage(myTmoData, "Card");
		applitoolsLib.doVisualTest(getDriver(),"payments", "EIPPaymentConfirmationCCPage", batchInfo);
	}
	
	/**
	 * Do visual testing for EIPPaymentForwardPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testEIPPaymentForwardPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEIPPaymentForwardPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "EIPPaymentForwardPage",batchInfo);
	}
	
	/**
	 * Do visual testing for EIPPaymentReviewCCPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testEIPPaymentReviewCCPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEIPPaymentReviewCCPage(myTmoData, "Card");
		applitoolsLib.doVisualTest(getDriver(),"payments", "EIPPaymentReviewCCPage",batchInfo);
	}
	
	/**
	 * Do visual testing for AutoPayFAQPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testFAQPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayFAQPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "AutoPayFAQPage",batchInfo);
	}
	
	/**
	 * Do visual testing for LeaseDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testLeaseDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToLeaseDetailsPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "LeaseDetailsPage",batchInfo);
	}
	
	/**
	 * Do visual testing for ChargeOneTimePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testOneTimePaymentPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToOTPpage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "ChargeOneTimePage",batchInfo);
	}
	
	/**
	 * Do visual testing for EditAmountPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testOTPAmountPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToEditAmountPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "EditAmountPage",batchInfo);
	}
	
	/**
	 * Do visual testing for OTPConfirmationPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testOTPConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToOTPConfirmationPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "OTPConfirmationPage",batchInfo);
	}
	
	/**
	 * Do visual testing for OTPDatePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testOTPDatePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToOTPDatePage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "OTPDatePage",batchInfo);
	}
	
	/**
	 * Do visual testing for PAConfirmationPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testPAConfirmationPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPAConfirmationPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "PAConfirmationPage",batchInfo);
	}
	
	/**
	 * Do visual testing for PAInstallmentDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testPAInstallmentDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPAInstallmentDetailsPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "PAInstallmentDetailsPage",batchInfo);
	}
	
	/**
	 * Do visual testing for PaperlessBillingPage
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testPaperlessBillingPage(ControlTestData data, MyTmoData myTmoData) throws Exception {
		navigateToPaperlessBillingPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "PaperlessBillingPage",batchInfo);
	}
	
	/**
	 * Do visual testing for PaymentArrangementPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testPaymentArrangementPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPaymentArrangementPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "PaymentArrangementPage",batchInfo);
	}
	
	/**
	 * Do visual testing for PaymentErrorPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testPaymentErrorPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToPaymentErrorPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "PaymentErrorPage",batchInfo);
	}
	
	/**
	 * Do visual testing for ProfilePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testProfilePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToProfilePage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "ProfilePage",batchInfo);
	}
	
	/**
	 * Do visual testing for SpokePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testSpokePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToSpokePage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "SpokePage",batchInfo);
	}
	
	/**
	 * Do visual testing for AutoPayTnCPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testTnCPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToAutoPayTnCPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "AutoPayTnCPage",batchInfo);
	}
	
	/**
	 * Do visual testing for UsageDetailsPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testUsageDetailsPage(ControlTestData data, MyTmoData myTmoData) {

		applitoolsLib.doVisualTest(getDriver(),"payments", "UsageDetailsPage",batchInfo);
	}
	
	/**
	 * Do visual testing for UsageOverviewPage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testUsageOverviewPage(ControlTestData data, MyTmoData myTmoData) {
		navigateToUsageOverviewPage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "UsageOverviewPage",batchInfo);
	}
	
	/**
	 * Do visual testing for UsagePage
	 * 
	 * @param data
	 * @param myTmoData
	 */
	//@Test(dataProvider = "byColumnName", enabled = true, groups = { "payments" })
	public void testUsagePage(ControlTestData data, MyTmoData myTmoData) {
		navigateToUsagePage(myTmoData);
		applitoolsLib.doVisualTest(getDriver(),"payments", "UsagePage",batchInfo);
	}


}
