package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.global.FamilyModePage;

public class FamilyModePageTest extends GlobalCommonLib {
	
	/**
	 * US433704:Family Mode | Test Support
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyFamilyModePage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : US433704:Family Mode | Test Support");
		Reporter.log("================================");
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be launched");
		Reporter.log("2. Login to the application | User should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on family mode link | Family mode page should be displayed");

		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		FamilyModePage familyModePage=navigateToMFamilyModePage(myTmoData,"Family Mode");
	}

}
