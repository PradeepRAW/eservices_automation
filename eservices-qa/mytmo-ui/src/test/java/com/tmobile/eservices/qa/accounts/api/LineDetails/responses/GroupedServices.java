package com.tmobile.eservices.qa.accounts.api.LineDetails.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GroupedServices {

	private String groupCategory;

	private AllCurrentServiceDetails[] eligibleServicesByGroup;

	public String getGroupCategory() {
		return groupCategory;
	}

	public void setGroupCategory(String groupCategory) {
		this.groupCategory = groupCategory;
	}

	public AllCurrentServiceDetails[] getEligibleServicesByGroup() {
		return eligibleServicesByGroup;
	}

	public void setEligibleServicesByGroup(AllCurrentServiceDetails[] eligibleServicesByGroup) {
		this.eligibleServicesByGroup = eligibleServicesByGroup;
	}
}
