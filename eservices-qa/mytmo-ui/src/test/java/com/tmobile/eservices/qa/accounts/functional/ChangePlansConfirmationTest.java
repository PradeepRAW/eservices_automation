/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.accounts.ChangePlansConfirmation;
import com.tmobile.eservices.qa.pages.accounts.ChangePlansReviewPage;
import com.tmobile.eservices.qa.pages.accounts.ExplorePlanPage;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsConfirmationPage;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsSelectionPage;
import com.tmobile.eservices.qa.pages.accounts.NetFlixPage;
import com.tmobile.eservices.qa.pages.accounts.ODFDataPassReviewPage;
import com.tmobile.eservices.qa.pages.accounts.PlanComparisonPage;
import com.tmobile.eservices.qa.pages.accounts.PlanDetailsPage;

/**
 * @author Sudheer Reddy Chilukuri.
 *
 */

public class ChangePlansConfirmationTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(ChangePlansConfirmationTest.class);

	@Test(dataProvider = "byColumnName", enabled = true, groups = "qqqq")
	public void testChangePlanConfirmationPageAddingNetflixCompleteFlow(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"testVerifyChangePlanConfirmationPageAddingNetflixCompleteFlow method called in ChangePlansConfirmationTest");
		Reporter.log("Test Case : test Change Plan Confirmation Page Adding Netflix Complete Flow");
		Reporter.log("Test Data : Any MSDSIN PAH/STANDARD");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Plans page | Plans Page Should be displayed");
		Reporter.log("5. Verify Featured Plans page | Featured Plans Page Should be displayed");
		Reporter.log("6. Verify Plans Comparison page | Plans Comparison Page Should be displayed");
		Reporter.log("7. Verify Plans Configure Page |Plans Configure Page Should be displayed");
		Reporter.log("8. Click on Agree and sumit  | Success header  Should be displayed");
		Reporter.log("9. Verify New plan name | New plan name  Should be displayed");
		Reporter.log("10. Verify Autopay discount Message |Autopay discount Message  Should be displayed");
		Reporter.log("11. Verify Total Amount | Total Amount  Should be displayed");
		Reporter.log("12. Verify Back to Home Button|Back to Home Button  Should be displayed");
		Reporter.log("13. click on view more details link | Plans Page Should be displayed");
		Reporter.log("=====================================");
		Reporter.log("Actual Results:");

		launchAndPerformLoginWithJenkinsData(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.verifyHomePage();

		homePage.clickAccountLink();
		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		accountOverviewPage.verifyAccountOverviewPage();

		accountOverviewPage.verifyPlanName();
		accountOverviewPage.clickOnPlanName();

		PlanDetailsPage plandetailspage = new PlanDetailsPage(getDriver());
		plandetailspage.verifyPlanDetailsPageLoad();
		plandetailspage.clickOnChangePlanButton();

		ExplorePlanPage exploreplanpage = new ExplorePlanPage(getDriver());
		exploreplanpage.verifyExplorePage();

		exploreplanpage.navigateToPlanComparisionPage("Magenta Plus");
		if (plandetailspage.verifyPlanDetailsTitle()) {
			plandetailspage.clickOnSelectPlanButton();
		} else {
			PlanComparisonPage planComparisonPage = new PlanComparisonPage(getDriver());
			planComparisonPage.choosePlanFromChoosePlanSection("Magenta Plus");
			planComparisonPage.clickOnSelectPlanCta();
		}

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.verifyChangePlansReviewPage();
		changePlansReviewPage.clickOnAgreeAndSubmitCTA();

		ChangePlansConfirmation changePlansConfirmation = new ChangePlansConfirmation(getDriver());
		changePlansConfirmation.verifyPlansConfirmationPage();
		changePlansConfirmation.verifYourPlanChangeHeader();
		changePlansConfirmation.clickOnNetflixCTA();

		ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
		manageAddOnsSelectionPage.verifymanageDataAndAddOnsPage();
		manageAddOnsSelectionPage.clickOnCheckBox("Netflix Premium for T-Mobile ($15.99 value)");
		manageAddOnsSelectionPage.acceptAddServicePopup();
		manageAddOnsSelectionPage.clickOnContinueBtn();

		ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
		verifyODFDataPassReviewAndPayPage();
		oDFDataPassReviewPage.agreeAndSubmitButton();

		ManageAddOnsConfirmationPage manageAddOnsConfirmationPage = new ManageAddOnsConfirmationPage(getDriver()); // manageAddOnsConfirmationPage.
		manageAddOnsConfirmationPage.verifyOrderDetailsBladeDisplayedwithDownwarCarat();
		manageAddOnsConfirmationPage.clickOnActivateNetflixBtn();
		manageAddOnsConfirmationPage.switchToSecondWindow();

		NetFlixPage netFlixPage = new NetFlixPage(getDriver());

		netFlixPage.enterEmailAddress(myTmoData.getLoginEmailOrPhone());
		netFlixPage.clickOnSubmitbtn();
		netFlixPage.enterPassword();
		netFlixPage.clickOnSubmitbtn();

		netFlixPage.clickOnStartWatchingBtn();
		netFlixPage.clickOnContinueBtnInDeviceSurvey();

		netFlixPage.enterNameFieldValues();
		netFlixPage.clickOnContinueBtnInDeviceSurvey();
		netFlixPage.clickOnContinueBtnInDeviceSurvey();

		netFlixPage.verifyNetflixHomePageAndClose();

		netFlixPage.getNetflixComfirmationPage(myTmoData);
		manageAddOnsConfirmationPage.verifyNetflixSigningUpMessage();
	}

	/**
	 *
	 * US489262 - Confirmation page - Page header
	 * 
	 * @throws InterruptedException
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "qqqq")
	public void verifyConfirmationPageInNewChangePlanFlowWhenSubmittedChangePlanWithAutopay(ControlTestData data,
			MyTmoData myTmoData) throws InterruptedException {

		logger.info(
				"verifyConfirmationPageInNewChangePlanFlowWhenSubmittedChangePlanWithAutopay method called in PlansConfigurePageTest");
		Reporter.log("Test Case : Autopay details should be displayed on Confirmation page.");
		Reporter.log("Test Data : PAH/Full Msisdn with AutoPay OFF and on any TE plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA | Plans Review Page Should be displayed");
		Reporter.log("6. Verify Plans Review Page |Plans Review Page Should be displayed");
		Reporter.log("7. Click on Agree & Submit CTA | Confirmation page should be displayed.");
		Reporter.log("8. Check Header on confirmation page | Header should be displayed.");
		Reporter.log("9. Check Thanks Message | Thanks message should be displayed.");
		Reporter.log("10. Check whether Order details is expanded or not | It should be expanded.");
		Reporter.log("11. Check Autopay details | Autopay detials should be displayed.");
		Reporter.log("12. Check First Autopay date | First Autopay date should be displayed.");
		Reporter.log("13. Check Account holder name | Account holder name should be displayed.");
		Reporter.log("14. Check BAN | Correct BAN should be displayed.");
		Reporter.log("15. Check Plan name | Plan name should be displayed.");
		Reporter.log("16. Check Effective Date | Effective Date should be displayed.");
		Reporter.log("17. Check Total Monthly blade with cost | It should be displayed.");
		Reporter.log("18. Check Next steps section with  Netflix message | It should be displayed.");
		Reporter.log("19. Check Back Nav arrow | Back Nav Arrow should be displayed.");
		Reporter.log("===========================================");

		navigateToAccountOverviewPage(myTmoData);

		// String planName = "T-Mobile ONE with ONE Plus";
		String planName = myTmoData.getplanname().trim();
		System.out.println("Passed plan name is " + planName);
		selectPlanOnPlansComparisonPage(myTmoData, "");

		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.checkWarningModalandPlanNameOnReviewPage(planName);
		changePlansReviewPage.verifyChangePlansReviewPage();
		changePlansReviewPage.turnAutoPayOFFToggle();
		String newMonthlyTotalCost = changePlansReviewPage.getCostFromNewMonthlyTotalBladeWithDollarAndMonthSign();

		changePlansReviewPage.clickOnAgreeAndSubmitCTA();

		ChangePlansConfirmation changePlansConfirmation = new ChangePlansConfirmation(getDriver());
		changePlansConfirmation.verifyPlansConfirmationPage();
		changePlansConfirmation.checkWhetherGreenCheckMarkIconDisplayedOrNot();
		changePlansConfirmation.checkWhetherThanksUserNameTextDisplayedOrNot();
		changePlansConfirmation.checkWhetherOrderIsCompleteTextDisplayedOrNot();
		changePlansConfirmation.checkChangePlanDetailsText();
		changePlansConfirmation.checkWhetherOrderDetailsIsExpandedOrNot();

		/*
		 * changePlansConfirmation.checkAutopayText();
		 * changePlansConfirmation.checkAutopayStatus();
		 * changePlansConfirmation.checkAutopayMethodText();
		 * changePlansConfirmation.usedAutopayMethod();
		 * changePlansConfirmation.textFirstAutopayDate();
		 * changePlansConfirmation.FirstAutopayDate();
		 */
		changePlansConfirmation.checkTextYourNewPlanText();
		changePlansConfirmation.verifyPlanName(planName);
		changePlansConfirmation.checkEffectiveDate();

		changePlansConfirmation.textNameOnAccount();
		changePlansConfirmation.nameOnAccount();
		changePlansConfirmation.textTMobileAccountNumber();
		changePlansConfirmation.tMobileAccountNumber();

		changePlansConfirmation.checkTextNewMonthlyTotal();
		changePlansConfirmation.checkNewMonthlyTotalCost(newMonthlyTotalCost);

		changePlansConfirmation.checkTextNextSteps();
		changePlansConfirmation.checkHeaderTextForNetflixSection();
		changePlansConfirmation.checkTextForNetflixSection();
		changePlansConfirmation.checkManageNetflixCTA();
		changePlansConfirmation.checkBackNavArrow();
	}

	/**
	 *
	 * US489262 - Confirmation page - Page header
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "sprint")
	public void verifyConfirmationPageInNewChangePlanFlowWhenSubmittedChangePlanWithoutAutopay(ControlTestData data,
			MyTmoData myTmoData) {

		logger.info(
				"verifyConfirmationPageInNewChangePlanFlowWhenSubmittedChangePlanWithoutAutopay method called in PlansConfigurePageTest");
		Reporter.log("Test Case : Autopay details should be displayed on Confirmation page.");
		Reporter.log("Test Data : PAH/Full Msisdn with AutoPay OFF and on any TE plan");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application | User should be able to login Successfully");
		Reporter.log("3. Verify Home page | Home Page Should be displayed");
		Reporter.log("4. Verify Account Overview page | Account Overview Page Should be displayed");
		Reporter.log("5. Click on Change Plan CTA | Plans Review Page Should be displayed");
		Reporter.log("6. Verify Plans Review Page |Plans Review Page Should be displayed");
		Reporter.log("7. Click on Agree & Submit CTA | Confirmation page should be displayed.");
		Reporter.log("8. Check Header on confirmation page | Header should be displayed.");
		Reporter.log("9. Check Thanks Message | Thanks message should be displayed.");
		Reporter.log("10. Check whether Order details is expanded or not | It should be expanded.");
		Reporter.log("11. Check Autopay details | Autopay details should not be displayed.");
		Reporter.log("13. Check Account holder name | Account holder name should be displayed.");
		Reporter.log("14. Check BAN | Correct BAN should be displayed.");
		Reporter.log("15. Check Plan name | Plan name should be displayed.");
		Reporter.log("16. Check Effective Date | Effective Date should be displayed.");
		Reporter.log("17. Check Total Monthly blade with cost | It should be displayed.");
		Reporter.log("18. Check Next steps section with  Netflix message | It should be displayed.");
		Reporter.log("19. Check Back Nav arrow | Back Nav Arrow should be displayed.");
		Reporter.log("===========================================");

		// navigateToPlansReviewPage(myTmoData);
		ChangePlansReviewPage changePlansReviewPage = new ChangePlansReviewPage(getDriver());
		changePlansReviewPage.clickOnAgreeAndSubmitCTA();
		ChangePlansConfirmation changePlansConfirmation = new ChangePlansConfirmation(getDriver());
		changePlansConfirmation.verifyPlansConfirmationPage();
		// changePlansConfirmation.checkWhetherSmilyIconDisplayedOrNot();
		changePlansConfirmation.checkWhetherThanksUserNameTextDisplayedOrNot();
		changePlansConfirmation.checkWhetherOrderIsCompleteTextDisplayedOrNot();
		changePlansConfirmation.checkOrderDetailsText();
		changePlansConfirmation.checkWhetherOrderDetailsIsExpandedOrNot();
		changePlansConfirmation.checkAutopayTextNotDisplayed();
		changePlansConfirmation.textNameOnAccount();
		changePlansConfirmation.nameOnAccount();
		changePlansConfirmation.textTMobileAccountNumber();
		changePlansConfirmation.tMobileAccountNumber();
		// changePlansConfirmation.verifyPlanName();
		changePlansConfirmation.checkEffectiveDate();
		changePlansConfirmation.checkTextNewMonthlyTotal();
		// changePlansConfirmation.checkNewMonthlyTotalCost();
		changePlansConfirmation.checkTextNextSteps();
		changePlansConfirmation.checkHeaderTextForNetflixSection();
		changePlansConfirmation.checkTextForNetflixSection();
		changePlansConfirmation.checkManageNetflixCTA();
		changePlansConfirmation.checkBackNavArrow();
	}

}
