package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.EmployeeVerificationPage;

public class EmployeeVerificationPageTest extends AccountsCommonLib {
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void VerifyEmployeeVerificatonNoPermissionPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("Test employee verification no perfmission page for standard and Restricted users");
		Reporter.log("Test Case : Verify with Saved payment method count in Profile-->billing & payments tab ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Profile page | Profile should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Verify Employee Verification page");
		Reporter.log("7. Check for standard and Restricted users");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		EmployeeVerificationPage employeeVerificationPage = navigateToEmployeeVerificationPage(myTmoData);
		employeeVerificationPage.verifyNoPermissionsPageLoaded();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void TestEmployeeVerificationDocumentUploadPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("US597252 : Test employee verification document upload page");
		Reporter.log("Test Case : Verify with Saved payment method count in Profile-->billing & payments tab ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Profile page | Profile should be displayed");
		Reporter.log("4. Click Profile | Profile Page should be displayed");
		Reporter.log("5. Verify Employee Verification page");
		Reporter.log(
				"7. Check for the document Upload page | Employee verification document upload page should be able to load");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		EmployeeVerificationPage employeeVerificationPage = navigateToEmployeeVerificationPage(myTmoData);
		employeeVerificationPage.verifyEmployeeDocumentUploagPage();
	}
}
