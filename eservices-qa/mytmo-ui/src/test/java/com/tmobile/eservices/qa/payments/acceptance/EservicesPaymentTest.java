package com.tmobile.eservices.qa.payments.acceptance;

import java.io.IOException;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.exception.FrameworkException;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.payments.NewAddCardPage;
import com.tmobile.eservices.qa.pages.payments.OTPAmountPage;
import com.tmobile.eservices.qa.pages.payments.OTPConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.PaymentCollectionPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author Srajana
 *
 */
public class EservicesPaymentTest extends PaymentCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(EservicesPaymentTest.class);

	/**
	 * TC005_Ebill_Make Bill payments_Partial payments_Checking account
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void verifyUserAbleTOMakePartialBillPaymentUsingCheckingAccount(ControlTestData data, MyTmoData myTmoData) {
		logger.info(
				"verifyUserAbleTOMakePartialBillPaymentUsingCheckingAccount method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make Partial BillPayment Using Checking Account");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Enter other amount | Other Amount TextBox should be displayed and can be able to enter value.");
		Reporter.log("6. Fill Checking Account details and click next | Review payment page should be displayed");
		Reporter.log(
				"7. Verify Checking Account Information | Entered Checking Account details should be displayed correctly.");
		Reporter.log("8. Click on Checking Account Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		Long accNo = addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifySavedBankAccount(accNo.toString());
	}

	/**
	 * TC006_Ebill_Make Bill payments_Partial payments_Credit card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void verifyUserAbleTOMakePartialBillPaymentUsingCreditCard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakePartialBillPaymentUsingCreditCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make Partial Bill Payment Using CreditCard");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Enter other amount | Other Amount TextBox should be displayed and can be able to enter value.");
		Reporter.log("6. Fill Credit Card details and click next | Review payment page should be loaded");
		Reporter.log("7. Verify Credit Card Information | Entered Credit card details should be displayed correctly.");
		Reporter.log("8. Click on Credit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);

		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(myTmoData.getPayment().getCardNumber());
	}

	/**
	 * TC007_Ebill_Make Bill payments_Full payments_Checking account
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyUserAbleTOMakeFullBillPaymentUsingCheckingAccount(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakeFullBillPaymentUsingCheckingAccount method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make FullBill Payment Using CheckingAccount");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Click balance amount radio button, Fill Checking Account details and click next | Review payment page should be displayed");
		Reporter.log(
				"6. Verify Checking Account Information | Entered Checking Account details should be displayed correctly.");
		Reporter.log("7. Click on Checking Account Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");

		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		Long accNo = addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifySavedBankAccount(accNo.toString());
	}

	/**
	 * TC008_Ebill_Make Bill payments_Full payments_Credit card
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws IOException 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyUserAbleTOMakeFullBillPaymentUsingCreditCard(ControlTestData data, MyTmoData myTmoData) throws IOException {
		logger.info("verifyUserAbleTOMakeFullBillPaymentUsingCreditCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make FullBill Payment Using CreditCard");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Click Balance radio button and Select New Credit Card Radio button| Credit Card section should be displayed.");
		Reporter.log("6. Fill Credit Card details and click next | Review payment page should be loaded");
		Reporter.log("7. Verify Credit Card Information | Entered Credit card details should be displayed correctly.");
		Reporter.log("8. Click on Credit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);
		NewAddCardPage addcard=  navigateToNewAddCArdPage(myTmoData);
		Map<String,String>cardinfo=getCardInfo("visa");
		addcard.addCardInformation(cardinfo);
		addcard.clickcontinueindialog();
		OneTimePaymentPage oneTimePaymentPage =new OneTimePaymentPage(getDriver());
	
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(cardinfo.get("cardnumber"));
	}

	/**
	 * TC011_E-Bill_make payments_Full payment_Debit Card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void verifyUserAbleTOMakeFullBillPaymentUsingDebitCard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakeFullBillPaymentUsingDebitCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make Full BillPayment Using DebitCard");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Click Balance radio button and Select New Debit Card Radio button| Debit Card section should be displayed.");
		Reporter.log("6. Fill Dedit Card details and click next | Review payment page should be loaded");
		Reporter.log("7. Verify Dedit Card Information | Entered Dedit card details should be displayed correctly.");
		Reporter.log("8. Click on Dedit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");

		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(myTmoData.getPayment().getCardNumber());
	}

	/**
	 * TC012_E-Bill_make payments_Partial payment_Debit Card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void verifyUserAbleTOMakePartialBillPaymentUsingDebitCard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakePartialBillPaymentUsingDebitCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make Partial Bill Payment Using DebitCard");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Enter other amount | Other Amount TextBox should be displayed and can be able to enter value.");
		Reporter.log(
				"6. Click Balance radio button and Select New Debit Card Radio button| Debit Card section should be displayed.");
		Reporter.log("7. Fill Dedit Card details and click next | Review payment page should be loaded");
		Reporter.log("8. Verify Dedit Card Information | Entered Dedit card details should be displayed correctly.");
		Reporter.log("9. Click on Dedit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(myTmoData.getPayment().getCardNumber());
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyCancelPaymentOnReviewPaymentPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyCancelPaymentOnReviewPaymentPage method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify Cancel Payment On Review PaymentPage");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("6. Click Add bank radio button | Checking Account section should be displayed.");
		Reporter.log("6. Fill Checking Account details and click next | Review payment page should be displayed");
		Reporter.log("7. Click on Cancel button | Cancel confirmation modal should be displayed.");
		Reporter.log("8. Click on Ok button for Cancel confirmation | Bill SUmmary Page should be displayed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.clickContinueBtnModal();
		verifyPage("Home Page", "Home");
	}

	/**
	 * Make Onetime payment using new MasterCard
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws IOException 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyUserAbleTOMakeOnetimePaymentUsingNewMasterCard(ControlTestData data, MyTmoData myTmoData) throws IOException {
		logger.info("verifyUserAbleTOMakeOnetimePaymentUsingNewMasterCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make One Time Payment Using New MasterCard");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Click Balance radio button and Select New Debit Card Radio button| Debit Card section should be displayed.");
		Reporter.log("6. Fill Dedit Card details and click next | Review payment page should be loaded");
		Reporter.log("7. Verify Dedit Card Information | Entered Dedit card details should be displayed correctly.");
		Reporter.log("8. Click on Dedit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);
		
		NewAddCardPage addcard=  navigateToNewAddCArdPage(myTmoData);
		Map<String,String>cardinfo=getCardInfo("testmaster");
		addcard.addCardInformation(cardinfo);
		addcard.clickcontinueindialog();
		OneTimePaymentPage oneTimePaymentPage =new OneTimePaymentPage(getDriver());
	
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(cardinfo.get("cardnumber"));
	}

	/**
	 * Make Onetime payment using new AMEX Card
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws IOException 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyUserAbleTOMakeOnetimePaymentUsingNewAMEXCard(ControlTestData data, MyTmoData myTmoData) throws IOException {
		logger.info("verifyUserAbleTOMakeOnetimePaymentUsingNewAMEXCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make One Time Payment Using New AMEX Card");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Click Balance radio button and Select New Debit Card Radio button| Debit Card section should be displayed.");
		Reporter.log("6. Fill Dedit Card details and click next | Review payment page should be loaded");
		Reporter.log("7. Verify Dedit Card Information | Entered Dedit card details should be displayed correctly.");
		Reporter.log("8. Click on Dedit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);
		
		NewAddCardPage addcard=  navigateToNewAddCArdPage(myTmoData);
		Map<String,String>cardinfo=getCardInfo("testamex");
		addcard.addCardInformation(cardinfo);
		addcard.clickcontinueindialog();
		OneTimePaymentPage oneTimePaymentPage =new OneTimePaymentPage(getDriver());
	
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(cardinfo.get("cardnumber"));
	}

	/**
	 * Make Onetime payment using new Discover Card
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws IOException 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyUserAbleTOMakeOnetimePaymentUsingNewDiscoverCard(ControlTestData data, MyTmoData myTmoData) throws IOException {
		logger.info("verifyUserAbleTOMakeOnetimePaymentUsingNewDiscoverCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make One Time Payment Using New Discover Card");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Click Balance radio button and Select New Debit Card Radio button| Debit Card section should be displayed.");
		Reporter.log("6. Fill Dedit Card details and click next | Review payment page should be loaded");
		Reporter.log("7. Verify Dedit Card Information | Entered Dedit card details should be displayed correctly.");
		Reporter.log("8. Click on Dedit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");

		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);
		
		NewAddCardPage addcard=  navigateToNewAddCArdPage(myTmoData);
		Map<String,String>cardinfo=getCardInfo("testdiscover");
		addcard.addCardInformation(cardinfo);
		addcard.clickcontinueindialog();
		OneTimePaymentPage oneTimePaymentPage =new OneTimePaymentPage(getDriver());
	
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(cardinfo.get("cardnumber"));
	}

	/**
	 * Make Onetime payment using new Debit Card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void verifyUserAbleTOMakeOnetimePaymentUsingNewDebitCard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakeOnetimePaymentUsingNewDebitCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make One Time Payment Using New Debit Card");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Click Balance radio button and Select New Debit Card Radio button| Debit Card section should be displayed.");
		Reporter.log("6. Fill Dedit Card details and click next | Review payment page should be loaded");
		Reporter.log("7. Verify Dedit Card Information | Entered Dedit card details should be displayed correctly.");
		Reporter.log("8. Click on Dedit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(myTmoData.getPayment().getCardNumber());
	}

	/**
	 * Make Onetime payment using new Hybrid Card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, Group.DUPLICATE })
	public void verifyUserAbleTOMakeOnetimePaymentUsingNewHybridCard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleTOMakeOnetimePaymentUsingNewHybridCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make One Time Payment Using New Hybrid Card");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Click Balance radio button and Select New Debit Card Radio button| Debit Card section should be displayed.");
		Reporter.log("6. Fill Dedit Card details and click next | Review payment page should be loaded");
		Reporter.log("7. Verify Dedit Card Information | Entered Dedit card details should be displayed correctly.");
		Reporter.log("8. Click on Dedit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewCard(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(myTmoData.getPayment().getCardNumber());
	}

	/**
	 * Make Onetime payment using STORED Credit Card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "to be fixed" })
	public void verifyOnetimepaymentStoredcreditCard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyOnetimepaymentStoredcreditCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Make Onetime payment using STORED Credit Card");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Select Radio button Stored credit Card | Radio button should be selected ");
		Reporter.log("6. Click next | Review payment page should be loaded");
		Reporter.log("7. Click on Credit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.isStoredCardEnabled();
		otpSpokePage.selectStoredCard();
		otpSpokePage.clickContinueButton();
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
	}

	/**
	 * Make Onetime payment using STORED Debit Card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "to be Fixed" })
	public void verifyOnetimepaymentStoreddebitCard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyOnetimepaymentStoreddebitCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case :  Make Onetime payment using STORED Debit Card	");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Select Radio button Stored debit Card | Radio button should be selected ");
		Reporter.log("6. Click next | Review payment page should be loaded");
		Reporter.log("7. Click on Dedit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.isStoredCardEnabled();
		otpSpokePage.selectStoredCard();
		otpSpokePage.clickContinueButton();
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
	}

	/**
	 * Make Onetime payment using STORED Hybrid Card
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "to be fixed" })
	public void verifyOnetimepaymentStoredhybdridCard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyOnetimepaymentStoredhybdridCard method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Make Onetime payment using STORED Hybrid Card");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Select Radio button Stored Hybdrid Card | Radio button should be selected ");
		Reporter.log("6. Click next | Review payment page should be loaded");
		Reporter.log("7. Click on Hybdrid Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.isStoredCardEnabled();
		otpSpokePage.selectStoredCard();
		otpSpokePage.clickContinueButton();
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
	}

	/**
	 * Make Onetime payment using STORED Checking account
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "to be fixed" })
	public void verifyOnetimepaymentStoredcheckingAccount(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyOnetimepaymentStoredcheckingAccount method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Make Onetime payment using STORED Checking account");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. Select Radio button Stored Checking Account | Radio button should be selected ");
		Reporter.log("6. Click next | Review payment page should be loaded");
		Reporter.log("7. Click on Checking Account Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.isStoredCheckingAccountEnabled();
		otpSpokePage.selectStoredCheckingAccount();
		otpSpokePage.clickBackButton();
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
	}

	/**
	 * Verify Balance updates after Onetime Payment Completion
	 * 
	 * @param data
	 * @param myTmoData
	 * Duplicate : testHomePageBalanceAfterOneTimePayment
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyBalanceupdatesInOnetimePaymentcompletion(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBalanceupdatesInOnetimePaymentcompletion method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : Verify Balance updates after Onetime Payment Completion");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log(
				"5. Click Balance radio button and Select New Debit Card Radio button| Debit Card section should be displayed.");
		Reporter.log("6. Fill Dedit Card details and click next | Review payment page should be loaded");
		Reporter.log("7. Verify Dedit Card Information | Entered Dedit card details should be displayed correctly.");
		Reporter.log("8. Click on Dedit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("9. Click Submit Button | Verify Successful Payment message should be displayed");
		Reporter.log("10. Navigate to One Time Payment Page | Balance updates should be displayed correctly");
		Reporter.log("================================");

		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		editAmountPage.setOtherAmount(generateRandomAmount().toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		Long accNo = addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifySavedBankAccount(accNo.toString());
	}

	/**
	 * 03_Desktop_MC 2 Series BIN_OneTimePayment_Set balance and Payment
	 * information_Validate system accepts new MC BIN series and identifies card
	 * brand_Validate Charge
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws IOException 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS})
	public void verifyUserAbleTOMakeBillPaymentUsingnewMCBINCARD(ControlTestData data, MyTmoData myTmoData) throws IOException {
		logger.info("verifyUserAbleTOMakeBillPaymentUsingnewMCBINCARD method called in EservicesPaymnetsTest");
		Reporter.log("Test Case : verify User Able TO Make Bill Payment Using new MC BIN CARD");
		Reporter.log(PaymentConstants.EXPECTED_TEST_STEPS);
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Paybill | One time payment page should be displayed");
		Reporter.log("5. click balance radio button |balance radio button shold be selected");
		Reporter.log("6. Fill Credit Card details and click next | Review payment page should be loaded");
		Reporter.log("7. Verify Credit Card Information | Entered Credit card details should be displayed correctly.");
		Reporter.log("8. Click on Credit Card Terms And Conditions checkbox | Submit button should be enabed.");
		Reporter.log("================================");
		Reporter.log(PaymentConstants.ACTUAL_TEST_STEPS);

		NewAddCardPage addcard=  navigateToNewAddCArdPage(myTmoData);
		Map<String,String>cardinfo=getCardInfo("mcbincard");
		addcard.addCardInformation(cardinfo);
		addcard.clickcontinueindialog();
		OneTimePaymentPage oneTimePaymentPage =new OneTimePaymentPage(getDriver());
	
		oneTimePaymentPage.verifyAgreeAndSubmitButtonEnabledOTPPage();
		oneTimePaymentPage.verifyStoredCardReplacedWithNewCard(cardinfo.get("cardnumber"));
	}

	/**
	 * US262366 Payment Defect Fixes - [SPIKE] Homepage balance not updating
	 * after making payment
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testHomePageBalanceAfterOneTimePayment(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(Constants.EXPECTED_TEST_STEPS);
		Reporter.log("Step 1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("Step 2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("Step 3: click on paybill | one time payment page should be displayed");
		Reporter.log("Step 4: click on amount Icon| amount spoke page should be displayed");
		Reporter.log("Step 5: setup other amount and update| amount should be updated");
		Reporter.log("Step 6: click add payment method| payment spoke page should be displayed");
		Reporter.log("Step 7: click on add card| Card information page should be displayed");
		Reporter.log("Step 8: Fill all card details and click continue button| OTP page should be displayed");
		Reporter.log("Step 9: click submit button| OTP confirmation page should be displayed");
		Reporter.log("Step 10: Click on 'Return to home' button| Home page should be displayed.");
		Reporter.log("Step 11: verify balance on home page| Paid amount should be deducted from the balance.");

		Reporter.log("================================");
		Reporter.log(Constants.ACTUAL_TEST_STEPS);
		launchAndPerformLogin(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		verifyPage("Home Page", "Home");
		displayBillDueDateAndAmount(homePage);
		String balanceAmount = homePage.getBalanceDueAmount();
		if (balanceAmount.equals("Error")) {
			throw new FrameworkException("Balance amount is not displayed on the home page");
		}
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double payAmount = generateRandomAmount();
		editAmountPage.setOtherAmount(payAmount.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage otpConfirmationPage = new OTPConfirmationPage(getDriver());
		otpConfirmationPage.verifyPageLoaded();
		otpConfirmationPage.clickReturnToHome();
		verifyPage("Home Page", "Home");
		String amountAfterPayment = homePage.getBalanceDueAmount();
		if (amountAfterPayment.equals("Error")) {
			throw new FrameworkException("Balance amount is not displayed on the home page after payment");
		}else if (amountAfterPayment.equals(balanceAmount)) {
			throw new FrameworkException("Balance amount is not updated on the home page after payment");
		}else{
			Double balance = Double.parseDouble(balanceAmount.replaceAll("\\$", ""));
			balance = balance - payAmount; 
			homePage.verifyBalanceAfterPayment(balance);
		}
		removedStoredBankAccounts();
	}

}