package com.tmobile.eservices.qa.global.api;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.PortinApiV1;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class PortinApiV1Test extends PortinApiV1 {

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PortinApiV1Test", "testGetPortin_8A", Group.GLOBAL,Group.SPRINT })
	public void testGetPortin_8A(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGetPortin");
		Reporter.log("Data Conditions:MyTmo registered misdn and Portin details configured.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Portin detils shoud be displayed");
		Reporter.log("Step 2: Verify Success response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		String operationName = "Portin_getPortin";
		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		Response response = getPortin(apiTestData,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonNode systemCode = jsonNode.path("systemCode");
				Assert.assertEquals("8A", systemCode.get(0).textValue());
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PortinApiV1Test", "testGetPortin_8C", Group.GLOBAL,Group.SPRINT })
	public void testGetPortin_8C(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGetPortin");
		Reporter.log("Data Conditions:MyTmo registered misdn and Portin details configured.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Portin detils shoud be displayed");
		Reporter.log("Step 2: Verify Success response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "Portin_getPortin";
		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		
		
		Response response = getPortin(apiTestData,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonNode systemCode = jsonNode.path("systemCode");
				Assert.assertEquals("8C", systemCode.get(0).textValue());
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PortinApiV1Test", "testGetPortin_8D", Group.GLOBAL,Group.SPRINT })
	public void testGetPortin_8D(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testGetPortin");
		Reporter.log("Data Conditions:MyTmo registered misdn and Portin details configured.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Portin detils shoud be displayed");
		Reporter.log("Step 2: Verify Success response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "Portin_getPortin";
		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		Response response = getPortin(apiTestData,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonNode systemCode = jsonNode.path("systemCode");
				Assert.assertEquals("8D", systemCode.get(0).textValue());
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}
}
