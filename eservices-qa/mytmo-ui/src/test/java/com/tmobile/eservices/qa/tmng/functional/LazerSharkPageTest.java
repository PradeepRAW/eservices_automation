package com.tmobile.eservices.qa.tmng.functional;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.LazerSharkPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

/**
 * @author sputti
 *
 */
public class LazerSharkPageTest extends TmngCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(LazerSharkPageTest.class);	
	/***
	 * FLEx - LazerShark Existing Customer Address Functional Testing 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })		
	public void verifyLSExistingCustAddress(TMNGData tMNGData) {
		logger.info("Existing Customer address process flow Testing of LazerShark Application");
		Reporter.log("Test Steps | Expected Test Results:");
		Reporter.log("===============================================================");
		Reporter.log("Launch TMNG LazerShark Application | Application Should be Launched");
		Reporter.log("1. Check and Select the Provider | Provider should be displayed and selected");
		Reporter.log("2. Select the Rate | Rate Field should be displayed and able to select");
		Reporter.log("3. Select the Business Option&Click Continue | Business Option Field should be displayed and able to select");
		Reporter.log("4. Enter Customer Details& click Continue | Customer Details should be entered");
		Reporter.log("5. Form2 Continue Button Clicked | Continue Button should be displayed, clicked and User should navigate to Form3");
		Reporter.log("6. Enter Existing Address&Click Submit | Existing Address should be entered and click submit");
		Reporter.log("7. Verify Existing Address Modal | Existing Address Modal should be displayed and close by clicking on OK button");
		Reporter.log("--------------------Actual Test Results:----------------------");
		loadTmngURL(tMNGData);
		getDriver().get("http://dev-tmo-publisher.corporate.t-mobile.com/content/t-mobile/consumer/offers/lazer-shark.html");
		LazerSharkPage lazersharkPage = new LazerSharkPage(getDriver());
		lazersharkPage.verifyLazerSharkPageLoaded();
		getDriver().manage().timeouts().setScriptTimeout(10000, TimeUnit.MINUTES);
		lazersharkPage.enterExistAddress();
	}
	/***
	 * FLEx - LazerShark Existing Customer Email Functional Testing 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })		
	public void verifyLSExistingCustEmail1(TMNGData tMNGData) {
		logger.info("Existing Customer Email process flow Testing of LazerShark Application");
		Reporter.log("Test Steps | Expected Test Results:");
		Reporter.log("===============================================================");
		Reporter.log("Launch TMNG LazerShark Application | Application Should be Launched");
		Reporter.log("1. Check and Select the Provider | Provider should be displayed and selected");
		Reporter.log("2. Select the Rate | Rate Field should be displayed and able to select");
		Reporter.log("3. Select the Business Option&Click Continue | Business Option Field should be displayed and able to select");
		Reporter.log("4. Enter Customer Details with existing email | Customer Details should be entered");
		Reporter.log("5. Verify existing customer email Modal | Existing Customer Email Modal should be displayed and close by clicking on OK Button");
		Reporter.log("--------------------Actual Test Results:----------------------");
		loadTmngURL(tMNGData);
		getDriver().get("http://dev-tmo-publisher.corporate.t-mobile.com/content/t-mobile/consumer/offers/lazer-shark.html");
		LazerSharkPage lazersharkPage = new LazerSharkPage(getDriver());
		lazersharkPage.verifyLazerSharkPageLoaded();
		getDriver().manage().timeouts().setScriptTimeout(10000, TimeUnit.MINUTES);
		lazersharkPage.enterExistEmail();
	}
	/***
	 * FLEx - US516986 - Lazershark - Customer Info Page: Alpha Characters Only 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })		
	public void verifyLSFirstAndLastName(TMNGData tMNGData) {
		logger.info("US516986 - Lazershark - Customer Info Page: Alpha Characters Only");
		Reporter.log("Test Steps | Expected Test Results:");
		Reporter.log("===============================================================");
		Reporter.log("Launch TMNG LazerShark Application | Application Should be Launched");
		Reporter.log("1. Check and Select the Provider | Provider should be displayed and selected");
		Reporter.log("2. Select the Rate | Rate Field should be displayed and able to select");
		Reporter.log("3. Select the Business Option&Click Continue | Business Option Field should be displayed and able to select");
		Reporter.log("4. Validate Customer First Name with out Alpha Characters  | Entered Text should not be allowed other than Alpha Characters");
		Reporter.log("5. Validate Customer Last Name with out Alpha Characters  | Entered Text should not be allowed other than Alpha Characters");
		Reporter.log("--------------------Actual Test Results:----------------------");
		loadTmngURL(tMNGData);
		getDriver().get("http://dev-tmo-publisher.corporate.t-mobile.com/content/t-mobile/consumer/offers/lazer-shark.html");
		LazerSharkPage lazersharkPage = new LazerSharkPage(getDriver());
		lazersharkPage.verifyLazerSharkPageLoaded();
		getDriver().manage().timeouts().setScriptTimeout(10000, TimeUnit.MINUTES);
		lazersharkPage.selectProviderOption();
		lazersharkPage.clickOnRate();
		lazersharkPage.clickOnBusinessOption();
		lazersharkPage.clickOnForm1ContinueBtn();		
		lazersharkPage.validateFirstNameAlphaChar();
		lazersharkPage.validateLastNameAlphaChar();
	}
	/***
	 * FLEx - LazerShark End to End Functional Testing 
	 * @param tMNGData
	 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })
		public void verifyLazerSharkPage(TMNGData tMNGData) {
		logger.info("End to End Automation Testing of LazerShark Application");
		Reporter.log("======================================================================");
		Reporter.log("Test Steps | Expected Test Results:");
		Reporter.log("Launch TMNG LazerShark Application | Application Should be Launched");
		Reporter.log("1. Verify Form Headers | Forms Headers should be displayed");
		Reporter.log("2. Verify Progress Numbers & Bars	| Forms Progress Number & Bar should be displayed");
		Reporter.log("3. Check and Select the Provider | Provider should be displayed and selected");
		Reporter.log("4. Select the Rate | Rate Field should be displayed and able to select");
		Reporter.log("5. Select the Business Option | Business Option Field should be displayed and able to select");
		Reporter.log("6. Verify Legal T&C | Legal T&C Message should displayed & T&C Modal should be work successfully");
		Reporter.log("7. Verify Continue Button Click | Continue Button Field should be displayed, clicked and able to move to Form2");
		Reporter.log("8. Verify First Name validations | First Name should be displayed and mandatory");
		Reporter.log("9. Verify Last Name validations | Last Name should be mandatory and Minimum of 2 consecutive characters");
		Reporter.log("10. Verify Phone validations | Phone Number should be mandatory, allow only 10 digits");
		Reporter.log("11. Verify Email validations | Email should be mandatory and allow to enter in format");
		Reporter.log("12. Verify Confirm Email validations | Confirm Email should be mandatory, allow to enter in format and equal to the Email");	
		Reporter.log("13. Verify Form2 IMEI | IMEI Field should be mandatory by selecting 'Yes' and allow only 15 digits");
		Reporter.log("14. Verify IMEI ToolTip | IMEI Tool Tip Message should be displayed");
		Reporter.log("15. Verify Email Error Modal | Email Error Modal should displayed on existing email");
		Reporter.log("16. Verify Form2 Continue Button | Continue Button should be displayed, clicked and User should navigate to Form3");
		Reporter.log("17. Verify Address validation	| Address1 should be mandatory");
		Reporter.log("18. Verify City | City Field should be displayed");
		Reporter.log("19. Verify State | State should be mandatory");
		Reporter.log("20. Verify Zip validation | Zip should allow only 5 digits");		
		Reporter.log("21. Verify Form3 Submit Modal Confirm	| Submit Modal should be displayed and Clicked Confirm Button");
		Reporter.log("22. Verify Soft Confirm Page Details | Soft Confirmation Page should be displayed with all details");
		Reporter.log("--------------------------------------------------Actual Test Results:--------------------------------------------------------");
		loadTmngURL(tMNGData);
		getDriver().get("http://dev-tmo-publisher.corporate.t-mobile.com/content/t-mobile/consumer/offers/lazer-shark.html");
		LazerSharkPage lazersharkPage = new LazerSharkPage(getDriver());
		lazersharkPage.verifyLazerSharkPageLoaded();
		getDriver().manage().timeouts().setScriptTimeout(10000, TimeUnit.MINUTES);
		lazersharkPage.verifyForm1Headers();
		lazersharkPage.verifyForm1Progress();
		lazersharkPage.selectProviderOption();
		lazersharkPage.clickOnRate();
		lazersharkPage.clickOnBusinessOption();
		lazersharkPage.verifyForm1LegalTC();
		lazersharkPage.clickOnForm1LegalTCModal();
		lazersharkPage.clickOnForm1ContinueBtn();		
		lazersharkPage.verifyForm2Headers();
		lazersharkPage.verifyForm2Progress();
		lazersharkPage.validateFirstName();
		lazersharkPage.validateLastName1();
		lazersharkPage.validateLastName2();
		lazersharkPage.validPhoneNumber1();
		lazersharkPage.validPhoneNumber2();
		lazersharkPage.validateEmail1();
		lazersharkPage.validateEmail2();
		lazersharkPage.validateConfirmEmail1();
		lazersharkPage.validateConfirmEmail2();
		lazersharkPage.validateConfirmEmail3();
		lazersharkPage.verifyForm2YesOption();
		lazersharkPage.verifyForm2IMEIError1();
		lazersharkPage.verifyForm2IMEIError2();
		lazersharkPage.verifyForm2IMEIIcon();
		lazersharkPage.verifyForm2LegalTC();
		lazersharkPage.clickOnForm2LegalTCLink();
		lazersharkPage.clickOnForm2ContinueBtn();
		lazersharkPage.verifyForm3Headers();
		lazersharkPage.verifyForm3Progress();
		lazersharkPage.verifyForm3Address();
		lazersharkPage.verifyForm3CityError();
		lazersharkPage.verifyForm3StateError();
		lazersharkPage.validZip();
		lazersharkPage.invalidZip();
		lazersharkPage.enterAddress();
		lazersharkPage.clickOnForm3SubmitBtn200();
		//lazersharkPage.clickOnForm3Model();
		//lazersharkPage.clickOnForm3ModelConfirmBtn();		
		lazersharkPage.verifySoftConfirmHeaders();
		lazersharkPage.verifyForm4Progress();
		lazersharkPage.verifySoftConfirmDetails();
	}
		/*@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, Group.TMNG })		
		public void setLSAuthPage(TMNGData tMNGData) {
			loadTmngURL(tMNGData);
			getDriver().get("http://dmo-tmo-author.corporate.t-mobile.com:4502/editor.html/content/t-mobile/consumer/offers/lazer-shark.html");
			LazerSharkAuthPage lazersharkAuth = new LazerSharkAuthPage(getDriver());
			lazersharkAuth.verifyLazerSharkAuthPageLoaded();
			getDriver().manage().timeouts().setScriptTimeout(10000, TimeUnit.MINUTES);
			lazersharkAuth.login();
			getDriver().close();
		}*/
		
		
		/***
		 * FLEx - LazerShark Existing Customer Email Functional Testing 
		 * @param tMNGData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT})		
		public void verifyLazerSharkPageFieldValidations(TMNGData tMNGData) {
			logger.info("Existing Customer Email process flow Testing of LazerShark Application");
			Reporter.log("Test Steps | Expected Test Results:");
			Reporter.log("===============================================================");
			Reporter.log("1.Launch TMNG LazerShark Application | LazerShark page should be displayed");
			Reporter.log("2.Verify First Name validations | First name text field data validation should be success");
			Reporter.log("3.Verify Last Name validations | Last name text field data validation should be success");
			Reporter.log("4.Verify Phone validations | Phone text field data validation should be success");
			Reporter.log("5.Verify Email validations | Email text field data validation should be success");
			Reporter.log("6.Verify Confirm Email validations | Confirm email text field data validation should be success");	
			Reporter.log("7.Click on continue button and verify Tmobile business message header | TMobile business header should be displayed");
			
			Reporter.log("--------------------Actual Test Results:----------------------");
			
			loadTmngURL(tMNGData);
			LazerSharkPage lazerSharkPage = new LazerSharkPage(getDriver());
			lazerSharkPage.verifyLazerSharkPage();
			lazerSharkPage.validateForm1FirstNameField();
			lazerSharkPage.validateForm1LastNameField();
			lazerSharkPage.validateForm1PhoneField();
			lazerSharkPage.validateForm1EmailField();
			lazerSharkPage.validateForm1ConfirmEmailField();
			lazerSharkPage.verifyForm2YesOption();
			lazerSharkPage.verifyForm2IMEIError1();
			lazerSharkPage.verifyForm2IMEIError2();
			lazerSharkPage.clickOnForm2ContinueBtn();
			lazerSharkPage.verifyTMobileBusineesHeader();
			
		}
		
		
		
		
}