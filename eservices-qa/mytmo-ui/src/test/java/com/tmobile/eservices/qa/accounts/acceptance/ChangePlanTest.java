///**
// * 
// */
//package com.tmobile.eservices.qa.accounts.acceptance;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.testng.Reporter;
//import org.testng.annotations.Test;
//
//import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
//import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
//import com.tmobile.eservices.qa.commonlib.Group;
//import com.tmobile.eservices.qa.data.MyTmoData;
//import com.tmobile.eservices.qa.pages.HomePage;
//import com.tmobile.eservices.qa.pages.accounts.FeaturedPlanPage;
//import com.tmobile.eservices.qa.pages.accounts.PlanPage;
//import com.tmobile.eservices.qa.pages.accounts.PlansComparisonPage;
//import com.tmobile.eservices.qa.pages.accounts.PlansConfigurePage;
//import com.tmobile.eservices.qa.pages.global.NewChooseAccountPage;
//
///**
// * @author prokarma
// *
// */
//public class ChangePlanTest extends AccountsCommonLib {
//	private static final Logger logger = LoggerFactory.getLogger(ChangePlanTest.class);
//
//	/**
//	 * Ensure user can post-date a rate plan change.
//	 * 
//	 * @param data
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
//			Group.IOS })
//	public void verifyUserCanPostDataARatePlanChange(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("verifyUserCanPostDataARatePlanChange method called in ChangePlanTest");
//		Reporter.log("Test Case Name : CPS - Verify user can Post Date a rate Plan change");
//		Reporter.log("Test Data : PAH/Full/Standard User on TMO ONE/Simple Choice plan");
//		Reporter.log("========================");
//		Reporter.log("Test Steps | Expected Results:");
//		Reporter.log("1. Launch the application | Application Should be Launched");
//		Reporter.log("2. Login to the application | User Should be login successfully");
//		Reporter.log("3. Verify Home page | Home page should be displayed");
//		Reporter.log("4. Click Plan Link in UNAV | Plan page should be displayed");
//		Reporter.log("5. Click on Change Plan | Plans comparison page should be displayed");
//		Reporter.log("6. Select Plan & continue | Plans Configure page should be displayed");
//		Reporter.log("========================");
//		Reporter.log("Actual Results:");
//
//		navigateToPlanConfigurationPageClickingOnBannerInPlanPage(myTmoData);
//
//		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
//		plansComparisonPage.clickSelectandContinueButton();
//		
//		PlansConfigurePage plansConfigurePage = new PlansConfigurePage(getDriver());
//		plansConfigurePage.verifyPlanConfigurePage();
//		/*plansConfigurePage.clickOnAgreeSubmitButton();
//		logStep(StepStatus.PASS, "click On Agree Submit Button", "");
//		ChangePlansConfirmation changePlansConfirmation = new ChangePlansConfirmation(getDriver());
//		Assert.assertTrue(changePlansConfirmation.getThankYouMessage()
//				.contains(AccountsConstants.CHANGE_PLANS_CONFIRMATION_MESSAGE));*/
//	}
//
//	/**
//	 * US183089#Change the title of ONE Plus fetaures from Unlimited to Feature
//	 * name in WPC
//	 * 
//	 * @param data
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
//			Group.IOS })
//	public void verifyPlansConfigurePageChangeDataOptionsDescription(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("verifyPlansConfigurePageChangeDataOptionsDescription method called in ChangePlanTest");
//		Reporter.log("Test Case : Verify Change Data options description on Plans Confirgure page.");
//		Reporter.log("Test Data : PAH/Full/Standard User on ONE Plus International data");
//		Reporter.log("========================");
//		Reporter.log("Test Steps | Expected Results:");
//		Reporter.log("1. Launch the application | Application Should be Launched");
//		Reporter.log("2. Login to the application | User Should be login successfully");
//		Reporter.log("3. Verify Home page | Home page should be displayed");
//		Reporter.log("4. Verify Change Plan | Plans Configure page should be displayed");
//		Reporter.log("5. Verify TMO One option | TMO One option should be displayed");
//		Reporter.log("6. Verify desription for Data option | Description should be displayed");
//		Reporter.log("========================");
//		Reporter.log("Actual Output:");
//
//		if (navigateToPlanComaprisionPage(myTmoData)) {
//			PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
//			plansComparisonPage.clickSelectandContinueButton();
//			PlansConfigurePage plansConfigurePage = new PlansConfigurePage(getDriver());
//			plansConfigurePage.verifyPlanConfigurePage();
//			// plansConfigurePage.verifyTMobileOneOptions();
//			plansConfigurePage.clickOnKickBackPopup();
//			plansConfigurePage.verifyONEPlusInternationalHeading("ONE Plus");
//			plansConfigurePage.verifyONEPlusDescription();
//		}
//	}
//
//	/**
//	 * US183089#Change the title of ONE Plus fetaures from Unlimited to Feature
//	 * name in WPC
//	 * 
//	 * @param data
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY})
//	public void testChooseAccountPageToPlansConfigurePageFlow(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("testChooseAccountPageToPlansConfigurePageFlow method called in ChangePlanTest");
//		Reporter.log("Test Case : Customer with multiple accounts flow to Plans Confirgure page.");
//		Reporter.log("Test Data : Customer with multiple accounts ");
//		Reporter.log("========================");
//		Reporter.log("Test Steps | Expected Results:");
//		Reporter.log("1. Launch the application | Application Should be Launched");
//		Reporter.log("2. Login to the application | Choose account page should be displayed");
//		Reporter.log("3. Choose other account | Home page should be displayed");
//		Reporter.log("4. Verify Home page | Home page should be displayed");
//		Reporter.log("5. Verify Change Plan | Plans Configure page should be displayed");
//		Reporter.log("6. Verify TMO One option | TMO One option should be displayed");
//		Reporter.log("========================");
//		Reporter.log("Actual Output:");
//
//		launchAndPerformLoginForChooseAccount(myTmoData);
//
//		NewChooseAccountPage accountPage = new NewChooseAccountPage(getDriver());
//		accountPage.verifyChooseAccountPage();
//		accountPage.chooseOtherAccount(myTmoData.getSwitchAcc());
//
//		HomePage homePage = new HomePage(getDriver());
//		homePage.verifyHomePage();
//
//		homePage.clickAccountLink();
//		int i = 0;
//		try {
//			while (getDriver().getTitle().contains("Home") && i < 4) {
//				homePage.clickAccountLink();
//				i++;
//			}
//		} catch (Exception e) {
//
//		}
//		PlanPage planPage = new PlanPage(getDriver());
//		planPage.verifyPlanpage();
//		planPage.clickOnchangePlanBtn();
//
//		FeaturedPlanPage featuredPlanPage = new FeaturedPlanPage(getDriver());
//		featuredPlanPage.verifyFeaturedPlanPage();
//		featuredPlanPage.clickPlanFeatureTaxInclusive();
//
//		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
//		plansComparisonPage.checkPageIsReady();
//		plansComparisonPage.clickSelectandContinueButton();
//
//		PlansConfigurePage plansConfigurePage = new PlansConfigurePage(getDriver());
//		plansConfigurePage.verifyPlanConfigurePage();
//
//	}
//
//	/**
//	 * Plans-configure page verify Service Agreement,Terms & Conditions and
//	 * Electronic Signature Terms
//	 * 
//	 * @param data
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
//			Group.IOS })
//	public void verifyPlansConfigureAgreementTermsAndConditions(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("VerifyPlansConfigureAgreementTerms method called in ChangePlanTest");
//		Reporter.log(
//				"Test Case : Plans-configure page verify Service Agreement,Terms & Conditions and Electronic Signature Terms.");
//		Reporter.log("Test Data : PAH/Full/Standard User on TMO ONE/Simple Choice plan");
//		Reporter.log("================================");
//		Reporter.log("Test Steps | Expected Results:");
//		Reporter.log("1. Launch the application | Application Should be Launched");
//		Reporter.log("2. Login to the application | User Should be login successfully");
//		Reporter.log("3. Verify Home page | Home page should be displayed");
//		Reporter.log("4. Click on Plan link | Plan page should be displayed");
//		Reporter.log("5. Click on Manage data add ons |Plans Configure page should display");
//		Reporter.log("6. Click on Service Agreement | Service Agreement overlay should be displayed");
//		Reporter.log("7. Click on Service Agreement close button | Service Agreement overlay should be closed");
//		Reporter.log("8. Click on Terms And Conditions | Terms And Conditions overlay should display");
//		Reporter.log("9. Click on Terms And Conditions close button| Terms And Conditions overlay should be closed");
//		Reporter.log(
//				"10. Click on Electronic Signature Terms | Electronic Signature Terms overlay should be displayed");
//		Reporter.log(
//				"11. Click on Electronic Signature Terms close button| Electronic Signature Terms overlay should be closed");
//		Reporter.log("================================");
//		Reporter.log("Actual results:");
//		// navigateToPlanChangeData(myTmoData);
//
//		if (navigateToPlanComaprisionPage(myTmoData)) {
//			PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
//			plansComparisonPage.clickSelectandContinueButton();
//			PlansConfigurePage plansConfigurePage = new PlansConfigurePage(getDriver());
//			plansConfigurePage.verifyPlanConfigurePage();
//			plansConfigurePage.clickOnServiceAgreement();
//			plansConfigurePage.verifyPrintServiceAgreementLink();
//			plansConfigurePage.clickOnCloseBtn();
//			plansConfigurePage.clickOnTermsAndConditions();
//			plansConfigurePage.verifyPrintTermsAndConditionsLink();
//			plansConfigurePage.clickOntermAndConditionscloseButton();
//			plansConfigurePage.clickOnElectronicSignatureTerms();
//			plansConfigurePage.verifyPrintElectronicSignatureTermsLink();
//			plansConfigurePage.clickOnElectronicSignaturemodalCloseButton();
//		}
//
//	}
//
//	/**
//	 * PAH/FULL GSM Account: Change Plan from SCNC to T1TI - ETE and validate
//	 * correct data options available
//	 * 
//	 * @param data
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
//			Group.IOS,Group.MULTIREG })
//	public void verifyChangePlanFromSCNCtoT1ValidateCorrectDataOptions(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("verifyChangePlanFromSCNCtoT1ValidateCorrectDataOptions method called in ChangePlanTest");
//		Reporter.log("Test Case Name:Change Plan from SCNC to T1TI And validate correct data options available");
//		Reporter.log("Test Data : PAH/Full/Standard User on TMO ONE/Simple Choice plan");
//		Reporter.log("========================");
//		Reporter.log("Test Steps | Expected Results:");
//		Reporter.log("1. Launch the application | Application Should be Launched");
//		Reporter.log("2. Login to the application | User Should be login successfully");
//		Reporter.log("3. Verify Home page | Home page should be displayed");
//		Reporter.log("4. Click on Plan link | Plan page should be displayed");
//		Reporter.log("5. Click on Tmobile one banner |Feature plan page should display");
//		Reporter.log("6. Click on Tax Inclusive |Plans Comparison page should display");
//		Reporter.log("7. Click on Cost break down |Cost break down section should display");
//		Reporter.log("8. Verify Effective Date Text |Effective Date Text should be display");
//		Reporter.log("9. Verify Cost Break Down Text |Cost Break Down Text should be display");
//		Reporter.log("10. Verify Plan Name text |Plan Name text should be display");
//		Reporter.log("11. Verify Plan Name |Plan Name should be display");
//		Reporter.log("12. Verify Dollars |Dollars should be display");
//		Reporter.log("================================");
//		Reporter.log("Actual Results:");
//
//		if (!navigateToPlanConfigurationPageClickingOnBannerInPlanPage(myTmoData)) {
//			PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
//
//			plansComparisonPage.clickSelectandContinueButton();
//			PlansConfigurePage plansConfigurePage = new PlansConfigurePage(getDriver());
//			plansConfigurePage.clickOnSeeCostBreakDown();
//			plansConfigurePage.verifyEffectiveDateText();
//
//			/*
//			 * plansConfigurePage.verifyPlanNameText();
//			 * plansConfigurePage.verifyPlanName();
//			 */
//			plansConfigurePage.verifyDollarsText();
//		}
//	}
//
//	/**
//	 * PAH/FULL GSM Account: Change Plan from T1TE to T1TI - ETE and validate
//	 * correct data options available
//	 * 
//	 * @param data
//	 * @param myTmoData
//	 */
//
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
//			Group.IOS,Group.MULTIREG })
//	public void verifyChangePlanFromT1TEtoT1ValidateCorrectDataOptions(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("verifyChangePlanFromT1TEtoT1ValidateCorrectDataOptions method called in ChangePlanTest");
//		Reporter.log("Test Case Name:Change Plan from T1TE to T1TI and validate correct data options available");
//		Reporter.log("Test Data : PAH/Full/Standard User on TMO ONE/Simple Choice plan");
//		Reporter.log("========================");
//		Reporter.log("Test Steps | Expected Results:");
//		Reporter.log("1. Launch the application | Application Should be Launched");
//		Reporter.log("2. Login to the application | User Should be login successfully");
//		Reporter.log("3. Verify Home page | Home page should be displayed");
//		Reporter.log("4. Click on Plan link | Plan page should be displayed");
//		Reporter.log("5. Click on Tmobile one banner |Feature plan page should display");
//		Reporter.log("6. Click on Tax Inclusive |Plans Comparison page should display");
//		Reporter.log("7. Click on Cost break down |Cost break down section should display");
//		Reporter.log("8. Verify Effective Date Text |Effective Date Text should be display");
//		Reporter.log("9. Verify Cost Break Down Text |Cost Break Down Text should be display");
//		Reporter.log("10. Verify Plan Name text |Plan Name text should be display");
//		Reporter.log("11. Verify Plan Name |Plan Name should be display");
//		Reporter.log("12. Verify Dollars |Dollars should be display");
//		Reporter.log("========================");
//		Reporter.log("Actual Results:");
//
//		if (navigateToPlanConfigurationPageClickingOnBannerInPlanPage(myTmoData)) {
//			PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
//			plansComparisonPage.clickSelectandContinueButton();
//			PlansConfigurePage plansConfigurePage = new PlansConfigurePage(getDriver());
//			plansConfigurePage.verifyPlanConfigurePage();
//			// plansComparisonPage.acceptKickBackWarning();
//			plansConfigurePage.clickOnSeeCostBreakDownLink();
//			plansConfigurePage.verifyEffectiveDateText();
//			plansConfigurePage.verifyTextPlanName();
//			plansConfigurePage.verifyPlanName();
//			plansConfigurePage.verifyDollarsText();
//		}
//	}
//
//	/**
//	 * MYTMO_CPS_IR_Change plan from T Mobile One TE to T Mobile One TE
//	 * 
//	 * @param data
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
//			Group.IOS,Group.MULTIREG })
//	public void verifyChangePlanFromTMOOneTEToTMOOneTI(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("verifyChangePlanFromTMOOneTEToTMOOneTI method called in ChangePlanTest");
//		Reporter.log("Test Case Name :CPS - Change plan from TMO One TE to TMO One");
//		Reporter.log("Test Data : PAH/Full User on TMO ONE TE Plan");
//		Reporter.log("========================");
//		Reporter.log("Test Steps | Expected Results:");
//		Reporter.log("1. Launch the application | Application Should be Launched");
//		Reporter.log("2. Login to the application | User Should be login successfully");
//		Reporter.log("3. Verify Home page | Home page should be displayed");
//		Reporter.log("4. Click Plan Link in UNAV | Plan page should be displayed");
//		Reporter.log("5. Click Change Plan | Plans comparison page is displayed");
//		Reporter.log("6. Select Plan & click on Continue | Plans Configure page is displayed");
//		Reporter.log("================================");
//		Reporter.log("Actual Results:");
//
//		if (navigateToPlanComaprisionPage(myTmoData)) {
//			PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
//			plansComparisonPage.verifyPlanComparisionPage();
//			plansComparisonPage.clickSelectandContinueButton();
//			PlansConfigurePage plansConfigurePage = new PlansConfigurePage(getDriver());
//			plansConfigurePage.verifyPlanConfigurePage();
//		}
//	}
//
//	// ***************sprint 6 spartans************
//
//	/**
//	 * US279817 Plans comparison re-skin - Desktop - Plan comparison page: load
//	 * the page with the benefit section expanded for current and available
//	 * plans
//	 * 
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP })
//	public void testVerifyBenefitSectionExpandedforCurrentandAvailablePlans(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("testVerifyBenefitSectionExpandedforCurrentandAvailablePlans method called in ChangePlanTest");
//		Reporter.log("Test Case Name :Verify Benefit Section Expanded for Current and Available Plans");
//		Reporter.log("Test Data : PAH/Full User ");
//		Reporter.log("Test Steps | Expected Results:");
//		Reporter.log("================================");
//		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
//		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
//		Reporter.log("3: Click On Plan link| Plan page should be displayed");
//		Reporter.log("4. Click on change plan  button |  old Plans Comparison Page should be displayed");
//		Reporter.log(
//				"5. verify the benefit plan expanded for current plan|benefit plan should be expanded for current plan");
//		Reporter.log("6. click on more details link|benefit section should be collapsed");
//		Reporter.log("================================");
//		Reporter.log("Actual Result:");
//
//		navigateToPlanComparisionPage(myTmoData);
//
//		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
//		plansComparisonPage.verifyBenifitSectionisExpanded();
//		plansComparisonPage.clickMoreBenefits();
//		plansComparisonPage.clickbackBtnCTA();
//	}
//
//	/**
//	 * US284777 Desktop and Mobile - Plan Comparison - Update legal verbiage
//	 * 
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
//			Group.IOS })
//	public void testVerifyUpdateLegalVerbiageOnPlansComparisionPageForSimpleChoiceUser(ControlTestData data,
//			MyTmoData myTmoData) {
//		logger.info(
//				"testVerifyUpdateLegalVerbiageOnPlansComparisionPageForSimpleChoiceUser method called in ChangePlanTest");
//		Reporter.log("Test Case Name :Verify Update Legal Verbiage On Plans Comparision Page For SimpleChoiceUser");
//		Reporter.log("Test Data : PAH/Full User with SimpleChoiceUser plan");
//		Reporter.log("Test Steps | Expected Results:");
//		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
//		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
//		Reporter.log("3: Click On Plan link| Plan page should be displayed");
//		Reporter.log("4. Click on change plan  button |  old Plans Comparison Page should be displayed");
//		Reporter.log(
//				"5. verify the updated legal verbiage in available plan section|updated legal verbiage in available plan section should be loaded");
//		Reporter.log("================================");
//		Reporter.log("Actual Result:");
//
//		navigateToPlanComparisionPage(myTmoData);
//		PlansComparisonPage plansComparisonPage = new PlansComparisonPage(getDriver());
//		plansComparisonPage.verifyDisplayLegalverbiageTextinAvailablePlanSectionForSimpleChoiceUser(
//				"Video@480p. Customers using >50GB/mo. may notice reduced speeds.");
//	}
//
//	/**
//	 * US260309
//	 * 
//	 * Plans comparison re-skin - Plan Details view: back button functionality -
//	 * to new plan comparison page
//	 * 
//	 * 
//	 * @param myTmoData
//	 */
//	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
//	public void testVerifyBackbtnFunctionalitytoNewPlanComparisionPage(ControlTestData data, MyTmoData myTmoData) {
//		logger.info("testVerifyBackbtnFunctionalitytoNewPlanComparisionPage method called in ChangePlanTest");
//		Reporter.log("Test Case Name :Verify Back button Functionality to New Plan ComparisionPage");
//		Reporter.log("Test Data : Any PAH/Full User");
//		Reporter.log("Test Steps | Expected Results:");
//		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
//		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
//		Reporter.log("3: Click On Plan link| Plan page should be displayed");
//		Reporter.log("4. Click on change plan  button |  New Plans Comparison Page should be displayed");
//		Reporter.log("5. click on view details in current plan section|old plans Comparison page should be loaded");
//		Reporter.log("6. click on Back navigation|New Plans Comparison Page should be displayed");
//
//		Reporter.log("================================");
//		Reporter.log("Actual Result:");
//
//		PlansComparisonPage plansComparisonPage = navigateToNewPlansComaprisonPage(myTmoData);
//		plansComparisonPage.clickCarrotOnNameOfThePlan();
//		plansComparisonPage.clickViewDetailsLinkInCurrentPlanPanel();
//		plansComparisonPage.verifyOldPlansComparisonPageHeader();
//
//		plansComparisonPage.clickbackbtnOnNavigationBar();
//		plansComparisonPage.verifyNewPlansComparisonPageHeader("Your current plan");
//	}
//}
