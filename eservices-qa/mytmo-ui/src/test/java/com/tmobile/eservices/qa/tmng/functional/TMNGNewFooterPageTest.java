package com.tmobile.eservices.qa.tmng.functional;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;
import com.tmobile.eservices.qa.pages.tmng.functional.TMNGNewFooterPage;

public class TMNGNewFooterPageTest extends TmngCommonLib {
	//private static final Logger logger = LoggerFactory.getLogger(TMNGNewFooterPageTest.class);

	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify TMNG Footer Title and English Link
	 * @param data
	 * @param myTmoData
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyEnglishLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Connect With T-Mobile text is displayed | Connect With T-Mobile Title text displayed successfully");
		Reporter.log("4.Verify English text is displayed | English text is displayed");
		Reporter.log("5.Click on English link | Clicked on English link successfully");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifyFooterTitle();
		unavPage.verifyEnglishLanguage();
		unavPage.clickEnglishlink();
	}
	
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Spanish Link Redirects To Spanish Page
	 * @param data
	 * @param myTmoData
	 */
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifySpanishLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Spanish text is displayed | Spanish text is displayed");
		Reporter.log("4.Click on Spanish link | Clicked on Spanish link successfully");
		Reporter.log("5.Verify Spanish page | Spanish Home page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifySpanishLanguage();
		unavPage.clickSpanishlink();
		unavPage.verifySpanishPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Instagram Redirects To Instagram Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyInstagramLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Instagram Link is displayed | Instagram link is displayed");
		Reporter.log("4.Click on Instagram link | Clicked on Instagram link successfully");
		Reporter.log("5.Verify Instagram page | Instagram page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickInstagramlink();
		unavPage.verifyInstagramPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Facebook Redirects To Facebook Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyFacebookLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Facebook Link is displayed | Facebook link is displayed");
		Reporter.log("4.Click on Facebook link | Clicked on Facebook link successfully");
		Reporter.log("5.Verify Facebook page | Facebook page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickFacebooklink();
		//unavPage.verifyFacebookPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Twitter Redirects To Twitter Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyTwitterLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Twitter Link is displayed | Twitter link is displayed");
		Reporter.log("4.Click on Twitter link | Clicked on Twitter link successfully");
		Reporter.log("5.Verify Twitter page | Twitter page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickTwitterlink();
		unavPage.verifyTwitterPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify YouTube Redirects To YouTube Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyYouTubeLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify YouTube Link is displayed | YouTube link is displayed");
		Reporter.log("4.Click on YouTube link | Clicked on YouTube link successfully");
		Reporter.log("5.Verify YouTube page | YouTube page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.clickYouTubelink();
		unavPage.verifyYouTubePage();
	}
	
	// Verify Phones and Devices Section
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Phones Redirects To Phones Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyPhonesLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Phones and Devices label is displayed | Phones and Devices is displayed");
		Reporter.log("4.Verify Phones label is displayed | Phones is displayed");
		Reporter.log("5.Click on Phones link | Clicked on Phones link successfully");
		Reporter.log("6.Verify Phones page | Phones page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifyPhoneDeviceLabel();
        unavPage.clickPhonesExpandButton();
		unavPage.verifyPhoneLabel();
		unavPage.clickPhoneslink();
		unavPage.verifyPhonesPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Tablets Redirects To Tablets Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyTabletsLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Tablets Link is displayed | Tablets link is displayed");
		Reporter.log("4.Click on Tablets link | Clicked on Tablets link successfully");
		Reporter.log("5.Verify Tablets page | Tablets page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickPhonesExpandButton();
		unavPage.verifyTabletsLabel();
		unavPage.clickTabletslink();
		unavPage.verifyTabletsPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify SmartWatches Redirects To SmartWatches Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifySmartWatchesLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify SmartWatches Link is displayed | SmartWatches link is displayed");
		Reporter.log("4.Click on SmartWatches link | Clicked on SmartWatches link successfully");
		Reporter.log("5.Verify SmartWatches page | SmartWatches page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickPhonesExpandButton();
		unavPage.verifySmartWatchesLabel();
		unavPage.clickSmartWatcheslink();
		unavPage.verifySmartWatchesPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Accessories Redirects To Accessories Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyAccessoriesLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Accessories Link is displayed | Accessories link is displayed");
		Reporter.log("4.Click on Accessories link | Clicked on Accessories link successfully");
		Reporter.log("5.Verify Accessories page | Accessories page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickPhonesExpandButton();
		unavPage.verifyAccessoriesLabel();
		unavPage.clickAccessorieslink();
		unavPage.verifyAccessoriesPage();
	}
	// Verify Apps and Connected Devices Section
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Family Mode Redirects To Family Mode Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyFamilyModeLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Apps and Connected devices is displayed | Apps and Connected devices is displayed");
		Reporter.log("4.Verify Familymode Link is displayed | Familymode link is displayed");
		Reporter.log("5.Click on Familymode link | Clicked on Familymode link successfully");
		Reporter.log("6.Verify Familymode page | Familymode page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifyAppsConnectDevicesLabel();
        unavPage.clickAppsExpandButton();
		unavPage.verifyFamilymodeLabel();
		unavPage.clickFamilymodelink();
		unavPage.verifyFamilymodePage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify DIGITS Redirects To DIGITS Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyDIGITSLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify DIGITS Link is displayed | DIGITS link is displayed");
		Reporter.log("4.Click on DIGITS link | Clicked on DIGITS link successfully");
		Reporter.log("5.Verify DIGITS page | DIGITS page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickAppsExpandButton();
		unavPage.verifyDIGITSLabel();
		unavPage.clickDIGITSlink();
		unavPage.verifyDIGITSPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify SyncUpDrive Redirects To SyncUpDrive Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifySyncUpDriveLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify SyncUpDrive Link is displayed | SyncUpDrive link is displayed");
		Reporter.log("4.Click on SyncUpDrive link | Clicked on SyncUpDrive link successfully");
		Reporter.log("5.Verify SyncUpDrive page | SyncUpDrive page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickAppsExpandButton();
		unavPage.verifySyncUpDriveLabel();
		unavPage.clickSyncUpDrivelink();
		unavPage.verifySyncUpDrivePage();
	}
	// Verify Plans and Information Section
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Plans Home Redirects To Plans Home Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyPlansHomeLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Plans and Information label is displayed | Plans and Information is displayed");
		Reporter.log("4.Verify PlansHome Link is displayed | PlansHome link is displayed");
		Reporter.log("5.Click on PlansHome link | Clicked on PlansHome link successfully");
		Reporter.log("6.Verify PlansHome page | PlansHome page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifyPlansInformationLabel();
        unavPage.clickPlansExpandButton();
		unavPage.verifyPlansHomeLabel();
		unavPage.clickPlansHomelink();
		unavPage.verifyPlansHomePage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify 55+ Redirects To 55+ Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verify55Link(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify 55 Link is displayed | 55 link is displayed");
		Reporter.log("4.Click on 55 link | Clicked on 55 link successfully");
		Reporter.log("5.Verify 55 page | 55 page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickPlansExpandButton();
		unavPage.verify55Label();
		unavPage.click55link();
		unavPage.verify55Page();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Military Redirects To Military Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyMilitaryLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Military Link is displayed | Military link is displayed");
		Reporter.log("4.Click on Military link | Clicked on Military link successfully");
		Reporter.log("5.Verify Military page | Military page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickPlansExpandButton();
		unavPage.verifyMilitaryLabel();
		unavPage.clickMilitarylink();
		unavPage.verifyMilitaryPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Prepaid Redirects To Prepaid Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyPrepaidLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Prepaid Link is displayed | Prepaid link is displayed");
		Reporter.log("4.Click on Prepaid link | Clicked on Prepaid link successfully");
		Reporter.log("5.Verify Prepaid page | Prepaid page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickPlansExpandButton();
		unavPage.verifyPrepaidLabel();
		unavPage.clickPrepaidlink();
		unavPage.verifyPrepaidPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify DeviceProtection Redirects To DeviceProtection Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyDeviceProtectionLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Device Protection Link is displayed | Device Protection link is displayed");
		Reporter.log("4.Click on Device Protection link | Clicked on Device Protection link successfully");
		Reporter.log("5.Verify Device Protection page | Device Protection page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickPlansExpandButton();
		unavPage.verifyDeviceProtectionLabel();
		unavPage.clickDeviceProtectionlink();
		unavPage.verifyDeviceProtectionPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * VVerify DataPass Redirects To DataPass Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyDataPassLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify DataPass Link is displayed | DataPass link is displayed");
		Reporter.log("4.Click on DataPass link | Clicked on DataPass link successfully");
		Reporter.log("5.Verify DataPass page | DataPass page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickPlansExpandButton();
		unavPage.verifyDataPassLabel();
		unavPage.clickDataPasslink();
		unavPage.verifyDataPassPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify MobileInternet Redirects To MobileInternet Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyMobileInternetLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify MobileInternet Link is displayed | MobileInternet link is displayed");
		Reporter.log("4.Click on MobileInternet link | Clicked on MobileInternet link successfully");
		Reporter.log("5.Verify MobileInternet page | MobileInternet page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickPlansExpandButton();
		unavPage.verifyMobileInternetLabel();
		unavPage.clickMobileInternetlink();
		unavPage.verifyMobileInternetPage();
	}
	// Verify Switch To T-Mobile Section
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify We’ll help you join Redirects To We’ll help you join Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyHelpJoinLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify Switch To T-Mobile Label is displayed | Switch To T-Mobile Label is displayed");
		Reporter.log("4.Verify We’ll help you join Link is displayed | We’ll help you join link is displayed");
		Reporter.log("5.Click on We’ll help you join link | Clicked on We’ll help you join link successfully");
		Reporter.log("6.Verify We’ll help you join page | We’ll help you join page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifySwitchTMobileLabel();
        unavPage.clickSwitchExpandButton();
		unavPage.verifyHelpJoinLabel();
		unavPage.clickHelpJoinlink();
		unavPage.verifyHelpJoinPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify SavingsCalculator Redirects To SavingsCalculator Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifySavingsCalculatorLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("4.Verify Savings Calculator link is displayed | Savings Calculator link is displayed");
		Reporter.log("5.Click on Savings Calculator link | Clicked on Savings Calculator link successfully");
		Reporter.log("6.Verify Savings Calculator page | Savings Calculator page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickSwitchExpandButton();
		unavPage.verifySavingsCalculatorLabel();
		unavPage.clickSavingsCalculatorlink();
		unavPage.verifySavingsCalculatorPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify Bring your own device Redirects To Bring your own device Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyBringOwnDeviceLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("4.Verify Bring Own Device link is displayed | Bring Own Device link is displayed");
		Reporter.log("5.Click on Bring Own Device link | Clicked on Bring Own Device link successfully");
		Reporter.log("6.Verify Bring Own Device page | Bring Own Device page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickSwitchExpandButton();
		unavPage.verifyBringOwnDeviceLabel();
		unavPage.clickBringOwnDevicelink();
		unavPage.verifyBringOwnDevicePage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify TradeInProgram Redirects To TradeInProgram Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyTradeInProgramLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("4.Verify Trade In Program link is displayed | Trade In Program link is displayed");
		Reporter.log("5.Click on Trade In Program  link | Clicked on Trade In Program  link successfully");
		Reporter.log("6.Verify Trade In Program  page | Trade In Program  page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickSwitchExpandButton();
		unavPage.verifyTradeInProgramLabel();
		unavPage.clickTradeInProgramlink();
		unavPage.verifyTradeInProgramPage();
	}
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify NumberPorting Redirects To NumberPorting Page
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
	public void verifyNumberPortingLink(TMNGData tMNGData){

		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("US599731 - UNAV | E2E Flow SDET");
		Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
		Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
		Reporter.log("US613371 - UNAV | Mobile Automation Testing");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("4.Verify NumberPorting link is displayed | NumberPorting link is displayed");
		Reporter.log("5.Click on NumberPorting link | Clicked on NumberPorting  link successfully");
		Reporter.log("6.Verify NumberPorting page | NumberPorting page should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
        unavPage.clickSwitchExpandButton();
		unavPage.verifyNumberPortingLabel();
		unavPage.clickNumberPortinglink();
		unavPage.verifyNumberPortingPage();
	}
	// Verify T-Mobile Perks Section
	/**
	 * US613371 - UNAV | Mobile Automation Testing
	 * US599731 - UNAV | E2E Flow SDET
	 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
	 * US589198 - UNAV | UI Header & footer in Dev prod 2
	 * US613371 - UNAV | Mobile Automation Testing
	 * Verify We’ll help you join Redirects To We’ll help you join Page
	 * @param data
	 * @param myTmoData
	 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyBenefitsLabel(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify TMobile Perks label is displayed | TMobilePerks label is displayed");
			Reporter.log("4.Verify Benefits link is displayed | Benefits link is displayed");
			Reporter.log("5.Click on Benefits link | Clicked on Benefits  link successfully");
			Reporter.log("6.Verify Benefits page | Benefits page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyTMobilePerksLabel();
	        unavPage.clickBenefitsExpandButton();
			unavPage.verifyBenefitsLabel();
			unavPage.clickBenefitslink();
			unavPage.verifyBenefitsPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify SavingsCalculator Redirects To SavingsCalculator Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyTravelLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Travel link is displayed | Travel link is displayed");
			Reporter.log("4.Click on Travel link | Clicked on Travel  link successfully");
			Reporter.log("5.Verify Travel page | Travel page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickBenefitsExpandButton();
			unavPage.verifyTravelLabel();
			unavPage.clickTravellink();
			unavPage.verifyTravelPage();
		}
		// Verify Order Info Section
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify We’ll help you join Redirects To We’ll help you join Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickCheckOrderStatuslink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Order Info label is displayed | Order Info label is displayed");
			Reporter.log("4.Verify Check Order Status link is displayed | Check Order Status link is displayed");
			Reporter.log("5.Click on Check Order Status link | Clicked on Check Order Status link successfully");
			Reporter.log("6.Verify Check Order Status page | Check Order Status page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyOrderInfoLabel();
	        unavPage.clickOrderInfoExpandButton();
			unavPage.verifyCheckOrderStatusLabel();
			unavPage.clickCheckOrderStatuslink();
			//unavPage.verifyCheckOrderStatusPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify View Return Policy Redirects To View Return Policy Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyViewReturnPolicyLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify ViewReturnPolicy link is displayed | ViewReturnPolicy link is displayed");
			Reporter.log("4.Click on ViewReturnPolicy link | Clicked on ViewReturnPolicy link successfully");
			Reporter.log("5.Verify ViewReturnPolicy page | ViewReturnPolicy page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickOrderInfoExpandButton();
			unavPage.verifyViewReturnPolicyLabel();
			unavPage.clickViewReturnPolicylink();
			unavPage.verifyViewReturnPolicyPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify RedeemRebate Redirects To RedeemRebate Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyRedeemRebateLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify RedeemRebate link is displayed | RedeemRebate link is displayed");
			Reporter.log("4.Click on RedeemRebate link | Clicked on RedeemRebate link successfully");
			Reporter.log("5.Verify RedeemRebate page | RedeemRebate page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickOrderInfoExpandButton();
			unavPage.verifyRedeemRebateLabel();
			unavPage.clickRedeemRebatelink();
			unavPage.verifyRedeemRebatePage();
		}
		// Verify Support Section
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Contact Us Redirects To Contact Us Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickContactUslink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Support label is displayed | Support labelis displayed");
			Reporter.log("4.Verify Contact Us link is displayed | Contact Us link is displayed");
			Reporter.log("5.Click on Contact Us link | Clicked on Contact Us link successfully");
			Reporter.log("6.Verify Contact Us page | Contact Us page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifySupportLabel();
	        unavPage.clickSupportExpandButton();
			unavPage.verifyContactUsLabel();
			unavPage.clickContactUslink();
			unavPage.verifyContactUsPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Phones Redirects To Phones Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyFooterPhonesLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Phones link is displayed | Phones link is displayed");
			Reporter.log("4.Click on Phones link | Clicked on Phones link successfully");
			Reporter.log("5.Verify Phones page | Phones page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickSupportExpandButton();
			unavPage.verifyFooterPhonesLabel();
			unavPage.clickFooterPhoneslink();
			unavPage.verifyFooterPhonesPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Plans Redirects To Plans Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyPlansLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Plans link is displayed | Plans link is displayed");
			Reporter.log("4.Click on Plans link | Clicked on Plans link successfully");
			Reporter.log("5.Verify Plans page | Plans page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickSupportExpandButton();
			unavPage.verifyPlansLabel();
			unavPage.clickPlanslink();
			unavPage.verifyPlansPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Billing Redirects To Billing Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyBillingLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Billing link is displayed | Billing link is displayed");
			Reporter.log("4.Click on Billing link | Clicked on Billing link successfully");
			Reporter.log("5.Verify Billing page | Billing page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickSupportExpandButton();
			unavPage.verifyBillingLabel();
			unavPage.clickBillinglink();
			unavPage.verifyBillingPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify International Redirects To International Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyInternationalLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify International link is displayed | International link is displayed");
			Reporter.log("4.Click on International link | Clicked on International link successfully");
			Reporter.log("5.Verify International page | International page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickSupportExpandButton();
			unavPage.verifyInternationalLabel();
			unavPage.clickInternationallink();
			unavPage.verifyInternationalPage();
		}
		// Verify My Account Section
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify PayMyBill Redirects To PayMyBill Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickPayMyBilllink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify My Account Label is displayed | MyAccount Label is displayed");
			Reporter.log("4.Verify Pay My Bill link is displayed | Pay My Bill link is displayed");
			Reporter.log("5.Click on Pay My Bill link | Clicked on Pay My Bill link successfully");
			Reporter.log("6.Verify Pay My Bill page | Pay My Bill page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyMyAccountLabel();
	        unavPage.clickMyAccountExpandButton();
			unavPage.verifyPayMyBillLabel();
			unavPage.clickPayMyBilllink();
			//unavPage.verifyPayMyBillPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Upgrade Redirects To Upgrade Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyUpgradeLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Upgrade link is displayed | Upgrade link is displayed");
			Reporter.log("4.Click on Upgrade link | Clicked on Upgrade link successfully");
			Reporter.log("5.Verify Upgrade page | Upgrade page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickMyAccountExpandButton();
			unavPage.verifyUpgradeLabel();
			unavPage.clickUpgradelink();
			unavPage.verifyUpgradePage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Add a Line Redirects To Add a Line Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyAddLineLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Add a line link is displayed | Add a line link is displayed");
			Reporter.log("4.Click on Add a line link | Clicked on Add a line link successfully");
			Reporter.log("5.Verify Add a line page | Add a line page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickMyAccountExpandButton();
			unavPage.verifyAddLineLabel();
			unavPage.clickAddLinelink();
			unavPage.verifyAddLinePage();
		}
		// Verify More than wireless Section
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Business Redirects To Business Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyBusinessLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify More Than Wireless label is displayed | More Than Wireless label is displayed");
			Reporter.log("4.Verify Business link is displayed | Business link is displayed");
			Reporter.log("5.Click on Business link | Clicked on Business link successfully");
			Reporter.log("6.Verify Business page | Business page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyMoreWirelessLabel();
	        unavPage.clickMoreThanWirelessExpandButton();
			unavPage.verifyBusinessLabel();
			unavPage.clickBusinesslink();
			unavPage.verifyBusinessPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify TVision Redirects To TVision Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyTVisionLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify TVision link is displayed | TVision link is displayed");
			Reporter.log("4.Click on TVision link | Clicked on TVision link successfully");
			Reporter.log("5.Verify TVision page | TVision page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickMoreThanWirelessExpandButton();
			unavPage.verifyTVisionLabel();
			unavPage.clickTVisionlink();
			unavPage.verifyTVisionPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify T-Mobile Money Redirects To T-Mobile Money Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyTMobileMoneyLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify T-Mobile Money link is displayed | T-Mobile Money link is displayed");
			Reporter.log("4.Click on T-Mobile Money link | Clicked on T-Mobile Money link successfully");
			Reporter.log("5.Verify T-Mobile Money page | T-Mobile Money page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickMoreThanWirelessExpandButton();
			unavPage.verifyTMobileMoneyLabel();
			unavPage.clickTMobileMoneylink();
			unavPage.verifyTMobileMoneyPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Home Internet Redirects To Home Internet Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyHomeInternetLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Home Internet link is displayed | Home Internet link is displayed");
			Reporter.log("4.Click on Home Internet link | Clicked on Home Internet link successfully");
			Reporter.log("5.Verify Home Internet page |Home Internet page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickMoreThanWirelessExpandButton();
			unavPage.verifyHomeInternetLabel();
			unavPage.clickHomeInternetlink();
			unavPage.verifyHomeInternetPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify IoT Redirects To IoT Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyIoTLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify IoT link is displayed | IoT link is displayed");
			Reporter.log("4.Click on IoT link | Clicked on IoT link successfully");
			Reporter.log("5.Verify IoT page | IoT page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
	        unavPage.clickMoreThanWirelessExpandButton();
			unavPage.verifyIoTLabel();
			unavPage.clickIoTlink();
			unavPage.verifyIoTPage();
		}
		// Verify About T-Mobile Section
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Our Story Redirects To Our Story Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickOurStorylink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About T-Mobile label is displayed | About T-Mobile label is displayed");
			Reporter.log("4.Verify Our Story link is displayed | Our Story link is displayed");
			Reporter.log("5.Click on Our Story link | Clicked on Our Story link successfully");
			Reporter.log("6.Verify Our Story page | Our Story page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyAboutTMobileLabel();
	        unavPage.clickAboutTMobileExpandButton();
			unavPage.verifyOurStoryLabel();
			unavPage.clickOurStorylink();
			unavPage.verifyOurStoryPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify News Room Redirects To Newsroom Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyNewsroomLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify AboutTMobile link is displayed | AboutTMobile link is displayed");
			Reporter.log("4.Click on AboutTMobile link | Clicked on AboutTMobile link successfully");
			Reporter.log("5.Verify AboutTMobile page | AboutTMobile page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
		    unavPage.clickAboutTMobileExpandButton();
			unavPage.verifyNewsroomLabel();
			unavPage.clickNewsroomlink();
			unavPage.verifyNewsroomPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Investor Relations Redirects To Investor Relations Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyInvestorRelationsLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify InvestorRelations link is displayed | InvestorRelations link is displayed");
			Reporter.log("4.Click on InvestorRelations link | Clicked on InvestorRelations link successfully");
			Reporter.log("5.Verify InvestorRelations page | InvestorRelations page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
		    unavPage.clickAboutTMobileExpandButton();
			unavPage.verifyInvestorRelationsLabel();
			unavPage.clickInvestorRelationslink();
			unavPage.verifyInvestorRelationsPage();
		}
		// Verify Corporate Responsibility Section
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Community Redirects To Community Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickCommunitylink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Corporate Responsibility label is displayed | Corporate Responsibility label is displayed");
			Reporter.log("4.Verify Community link is displayed | Community link is displayed");
			Reporter.log("5.Click on Community link | Clicked on Community link successfully");
			Reporter.log("6.Verify Community page | Community page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyCorporateResponsibilityLabel();
			unavPage.clickCorporateResponsibilityExpandButton();
			unavPage.verifyCommunityLabel();
			unavPage.clickCommunitylink();
			unavPage.verifyCommunityPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Sustainability Redirects To Sustainability Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifySustainabilityLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Sustainability link is displayed | Sustainability link is displayed");
			Reporter.log("4.Click on Sustainability link | Clicked on Sustainability link successfully");
			Reporter.log("5.Verify Sustainability page | Sustainability page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.clickCorporateResponsibilityExpandButton();
			unavPage.verifySustainabilityLabel();
			unavPage.clickSustainabilitylink();
			unavPage.verifySustainabilityPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Privacy Center Redirects To Privacy Center Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyPrivacyCenterLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Privacy Center link is displayed | Privacy Center link is displayed");
			Reporter.log("4.Click on Privacy Center link | Clicked on Privacy Center link successfully");
			Reporter.log("5.Verify Privacy Center page | Privacy Center page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.clickCorporateResponsibilityExpandButton();
			unavPage.verifyPrivacyCenterLabel();
			unavPage.clickPrivacyCenterlink();
			unavPage.verifyPrivacyCenterPage();
		}
		// Verify Careers Section
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify TMobileCareers To TMobileCareers Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickTMobileCareerslink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Careers label is displayed | Careers label is displayed");
			Reporter.log("4.Verify TMobile Careers link is displayed | TMobile Careers link is displayed");
			Reporter.log("5.Click on TMobile Careers link | Clicked on TMobile Careers link successfully");
			Reporter.log("6.Verify TMobile Careers page | TMobile Careers page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyCareersLabel();
			unavPage.clickCareersExpandButton();
			unavPage.verifyTMobileCareersLabel();
			unavPage.clickTMobileCareerslink();
			unavPage.verifyTMobileCareersPage();
		}
	
		// Verify Footer 2 - ABOUT Section
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify ABOUT Redirects To ABOUT Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickAboutlink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify ABOUT link is displayed | ABOUT link is displayed");
			Reporter.log("4.Click on ABOUT link | Clicked on ABOUT link successfully");
			Reporter.log("5.Verify ABOUT page | ABOUT page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyAboutLabel();
			unavPage.clickAboutlink();
			unavPage.verifyAboutPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify INVESTOR RELATIONS Redirects To INVESTOR RELATIONS Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickFooterInvestorRelationslink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify INVESTOR RELATIONS link is displayed | INVESTOR RELATIONS link is displayed");
			Reporter.log("4.Click on INVESTOR RELATIONS link | Clicked on INVESTOR RELATIONS link successfully");
			Reporter.log("5.Verify INVESTOR RELATIONS page | INVESTOR RELATIONS page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyFooterInvestorRelationsLabel();
			unavPage.clickFooterInvestorRelationslink();
			unavPage.verifyFooterInvestorRelationsPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify PRESS Redirects To PRESS Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickPresslink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify PRESS link is displayed | PRESS link is displayed");
			Reporter.log("4.Click on PRESS link | Clicked on PRESS link successfully");
			Reporter.log("5.Verify PRESS page | PRESS page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyPressLabel();
			unavPage.clickPresslink();
			unavPage.verifyPressPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify CAREERS Redirects To CAREERS Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickFooterCareerslink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify CAREERS link is displayed | CAREERS link is displayed");
			Reporter.log("4.Click on CAREERS link | Clicked on CAREERS link successfully");
			Reporter.log("5.Verify CAREERS page | CAREERS page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyFooterCareersLabel();
			unavPage.clickFooterCareerslink();
			unavPage.verifyFooterCareersPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify DEUTSCHE TELEKOM Redirects To DEUTSCHE TELEKOM Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickDeutscheTelekomlink(TMNGData tMNGData){
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify DEUTSCHE TELEKOM link is displayed | DEUTSCHE TELEKOM link is displayed");
			Reporter.log("4.Click on DEUTSCHE TELEKOM link | Clicked on DEUTSCHE TELEKOM link successfully");
			Reporter.log("5.Verify DEUTSCHE TELEKOM page | DEUTSCHE TELEKOM page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyDeutscheTelekomLabel();
			unavPage.clickDeutscheTelekomlink();
			unavPage.verifyDeutscheTelekomPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify PUERTO RICO Redirects To PUERTO RICO Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickPuertoRicolink(TMNGData tMNGData){
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify PUERTO RICO link is displayed | PUERTO RICO link is displayed");
			Reporter.log("4.Click on PUERTO RICO link | Clicked on PUERTO RICO link successfully");
			Reporter.log("5.Verify PUERTO RICO page | PUERTO RICO page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyPuertoRicoLabel();
			unavPage.clickPuertoRicolink();
			unavPage.verifyPuertoRicoPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify PRIVACY POLICY Redirects To PRIVACY POLICY Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickPrivacyPolicylink(TMNGData tMNGData){
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify PRIVACY POLICY link is displayed | PRIVACY POLICY link is displayed");
			Reporter.log("4.Click on PRIVACY POLICY link | Clicked on PRIVACY POLICY link successfully");
			Reporter.log("5.Verify PRIVACY POLICY page | PRIVACY POLICY page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyPrivacyPolicyLabel();
			unavPage.clickPrivacyPolicylink();
			unavPage.verifyPrivacyPolicyPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify INTEREST-BASED ADS Redirects To INTEREST-BASED ADS Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickInterestBasedAdslink(TMNGData tMNGData){
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify INTEREST-BASED ADS link is displayed | INTEREST-BASED ADS link is displayed");
			Reporter.log("4.Click on INTEREST-BASED ADS link | Clicked on INTEREST-BASED ADS link successfully");
			Reporter.log("5.Verify INTEREST-BASED ADS page | INTEREST-BASED ADS page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyInterestBasedAdsLabel();
			unavPage.clickInterestBasedAdslink();
			unavPage.verifyInterestBasedAdsPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify PRIVACY CENTER Redirects To PRIVACY CENTER Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickPrivacylink(TMNGData tMNGData){
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify PRIVACY CENTER link is displayed | PRIVACY CENTER link is displayed");
			Reporter.log("4.Click on PRIVACY CENTER link | Clicked on PRIVACY CENTER link successfully");
			Reporter.log("5.Verify PRIVACY CENTER page | PRIVACY CENTER page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyPrivacyLabel();
			unavPage.clickPrivacylink();
			unavPage.verifyPrivacyPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify CONSUMER INFORMATION Redirects To CONSUMER INFORMATION Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickConsumerlink(TMNGData tMNGData){
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify CONSUMER INFORMATION link is displayed | CONSUMER INFORMATION link is displayed");
			Reporter.log("4.Click on CONSUMER INFORMATION link | Clicked on CONSUMER INFORMATION link successfully");
			Reporter.log("5.Verify CONSUMER INFORMATION page | CONSUMER INFORMATION page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyConsumerLabel();
			unavPage.clickConsumerlink();
			unavPage.verifyConsumerPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify PUBLIC SAFETY/911 Redirects To PUBLIC SAFETY/911 Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickPublicSafetylink(TMNGData tMNGData){
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify PUBLIC SAFETY/911 link is displayed | PUBLIC SAFETY/911 link is displayed");
			Reporter.log("4.Click on PUBLIC SAFETY/911 link | Clicked on PUBLIC SAFETY/911 link successfully");
			Reporter.log("5.Verify PUBLIC SAFETY/911 page | PUBLIC SAFETY/911 page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyPublicSafetyLabel();
			unavPage.clickPublicSafetylink();
			unavPage.verifyPublicSafetyPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify TERMS & CONDITIONS Redirects To TERMS & CONDITIONS Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS})
		public void clickTermslink(TMNGData tMNGData){
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify TERMS & CONDITIONS link is displayed | TERMS & CONDITIONS link is displayed");
			Reporter.log("4.Click on TERMS & CONDITIONS link | Clicked on TERMS & CONDITIONS link successfully");
			Reporter.log("5.Verify TERMS & CONDITIONS page | TERMS & CONDITIONS page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyTermsLabel();
			unavPage.clickTermslink();
			unavPage.verifyTermsPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify TERMS OF USE Redirects To TERMS OF USE Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickTermsOfUselink(TMNGData tMNGData){
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify TERMS OF USE link is displayed | TERMS OF USE link is displayed");
			Reporter.log("4.Click on TERMS OF USE link | Clicked on TERMS OF USE link successfully");
			Reporter.log("5.Verify TERMS OF USE page | TERMS OF USE page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyTermsOfUseLabel();
			unavPage.clickTermsOfUselink();
			unavPage.verifyTermsOfUsePage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Accessibility Redirects To Accessibility Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickAccessibilitylink(TMNGData tMNGData){
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Accessibility link is displayed | Accessibility link is displayed");
			Reporter.log("4.Click on Accessibility link | Clicked on Accessibility link successfully");
			Reporter.log("5.Verify Accessibility page | Accessibility page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyAccessibilityLabel();
			unavPage.clickAccessibilitylink();
			unavPage.verifyAccessibilityPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Open Internet Redirects To OpenInternet Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void clickOpenInternetlink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Open Internet link is displayed | Open Internet link is displayed");
			Reporter.log("4.Click on Open Internet link | Clicked on Open Internet link successfully");
			Reporter.log("5.Verify Open Internet page | Open Internet page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyOpenInternetLabel();
			unavPage.clickOpenInternetlink();
			unavPage.verifyOpenInternetPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Footer2 Instagram Redirects To Instagram Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyFooter2InstagramLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Footer2 Instagram Link is displayed | Instagram link is displayed");
			Reporter.log("4.Click on Instagram link | Clicked on Instagram link successfully");
			Reporter.log("5.Verify Instagram page | Instagram page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.clickFooter2Instagramlink();
			unavPage.verifyInstagramPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Footer2 Facebook Redirects To Facebook Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.ANDROID, Group.IOS })
		public void verifyFooter2FacebookLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Facebook Link is displayed | Facebook link is displayed");
			Reporter.log("4.Click on Facebook link | Clicked on Facebook link successfully");
			Reporter.log("5.Verify Facebook page | Facebook page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.clickFooter2Facebooklink();
			//unavPage.verifyFacebookPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Footer2 Twitter Redirects To Twitter Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.ANDROID, Group.IOS })
		public void verifyFooter2TwitterLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Footer2 Twitter Link is displayed | Twitter link is displayed");
			Reporter.log("4.Click on Twitter link | Clicked on Twitter link successfully");
			Reporter.log("5.Verify Twitter page | Twitter page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.clickFooter2Twitterlink();
			unavPage.verifyTwitterPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Footer2 YouTube Redirects To YouTube Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.ANDROID, Group.IOS })
		public void verifyFooter2YouTubeLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Footer2 YouTube Link is displayed | YouTube link is displayed");
			Reporter.log("4.Click on YouTube link | Clicked on YouTube link successfully");
			Reporter.log("5.Verify YouTube page | YouTube page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.clickFooter2YouTubelink();
			unavPage.verifyYouTubePage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Footer2 Copy Right Text
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.ANDROID, Group.IOS })
		public void verifyFooter2CopyRight(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Footer2 Copy Right Text is displayed | Copy Right Text is displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyCopyRightLabel();
		}
		
		// Verify Contact Us Section in About Us Page
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Contact Information Redirects To Contact Information Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyContactInfoLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify Contact Us label is displayed | Contact Us is displayed");
			Reporter.log("5.Verify Contact Information label is displayed | Contact Information is displayed");
			Reporter.log("6.Click on Contact Information link | Clicked on Contact Information link successfully");
			Reporter.log("7.Verify Contact Information page | Contact Information page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
			unavPage.verifyContactusLabel();
	        unavPage.clickContactUsExpandButton();
			unavPage.verifyContactInfoLabel();
			unavPage.clickContactInfolink();
			unavPage.verifyContactInfoPage();
		}
		
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Check Order Status Redirects To Check Order Status Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyAboutUsCheckOrderStatusLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify Check Order Status label is displayed | Check Order Status is displayed");
			Reporter.log("5.Click on Check Order Status link | Clicked on Check Order Status link successfully");
			Reporter.log("6.Verify Check Order Status page | Check Order Status page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
	        unavPage.clickContactUsExpandButton();
			unavPage.verifyAboutUsCheckOrderStatusLabel();
			unavPage.clickAboutUsCheckOrderStatuslink();
			unavPage.verifyAboutUsCheckOrderStatusPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify View Return Policy Redirects To View Return Policy Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyAboutUsViewReturnPolicyLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify View Return Policy label is displayed | View Return Policy is displayed");
			Reporter.log("5.Click on View Return Policy link | Clicked on View Return Policy link successfully");
			Reporter.log("6.Verify View Return Policy page | View Return Policy page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
	        unavPage.clickContactUsExpandButton();
			unavPage.verifyAboutUsViewReturnPolicyLabel();
			unavPage.clickAboutUsViewReturnPolicylink();
			unavPage.verifyAboutUsViewReturnPolicyPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Get A Rebate Redirects To Get A Rebate Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyGetARebateLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify GetARebate label is displayed | GetARebate is displayed");
			Reporter.log("5.Click on GetARebate link | Clicked on GetARebate link successfully");
			Reporter.log("6.Verify GetARebate page | GetARebate page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
	        unavPage.clickContactUsExpandButton();
			unavPage.verifyGetARebateLabel();
			unavPage.clickGetARebatelink();
			unavPage.verifyGetARebatePage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Find A Store Redirects To Find A Store Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyFindAStoreLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify FindAStore label is displayed | FindAStore is displayed");
			Reporter.log("5.Click on FindAStore link | Clicked on FindAStore link successfully");
			Reporter.log("6.Verify FindAStore page | FindAStore page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
	        unavPage.clickContactUsExpandButton();
			unavPage.verifyFindAStoreLabel();
			unavPage.clickFindAStorelink();
			unavPage.verifyFindAStorePage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Trade In Program Redirects To Trade In Program Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyAboutUsTradeInProgramLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify Trade In Program label is displayed | Trade In Program is displayed");
			Reporter.log("5.Click on Trade In Program link | Clicked on Trade In Program link successfully");
			Reporter.log("6.Verify Trade In Program page | Trade In Program page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
	        unavPage.clickContactUsExpandButton();
			unavPage.verifyAboutUsTradeInProgramLabel();
			unavPage.clickAboutUsTradeInProgramlink();
			unavPage.verifyAboutUsTradeInProgramPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Device Support Redirects To Device Support Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyDeviceSupportLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify Device Support label is displayed | Device Support is displayed");
			Reporter.log("5.Click on Device Support link | Clicked on Device Support link successfully");
			Reporter.log("6.Verify Device Support page | Device Support page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
			unavPage.verifyAboutUsSupportLabel();
	        unavPage.clickAboutUsSupportExpandButton();
			unavPage.verifyDeviceSupportLabel();
			unavPage.clickDeviceSupportlink();
			unavPage.verifyDeviceSupportPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Questions about your bill Redirects To Questions about your bill Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyQuesAboutBillLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify Questions about your bill label is displayed | Questions about your bill is displayed");
			Reporter.log("5.Click on Questions about your bill link | Clicked on Questions about your bill link successfully");
			Reporter.log("6.Verify Questions about your bill page | Questions about your bill page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
	        unavPage.clickAboutUsSupportExpandButton();
			unavPage.verifyQuesAboutBillLabel();
			unavPage.clickQuesAboutBilllink();
			unavPage.verifyQuesAboutBillPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Plans & services Redirects To Plans & services Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyPlansServiceLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify Plans & services label is displayed | Plans & services is displayed");
			Reporter.log("5.Click on Plans & services link | Clicked on Plans & services link successfully");
			Reporter.log("6.Verify Plans & services page | Plans & services page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
	        unavPage.clickAboutUsSupportExpandButton();
			unavPage.verifyPlansServiceLabel();
			unavPage.clickPlansServicelink();
			unavPage.verifyPlansServicePage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Activate your prepaid phone or device Redirects To Activate your prepaid phone or device Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyActivePhoneLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify Activate your prepaid phone or device label is displayed | Activate your prepaid phone or device is displayed");
			Reporter.log("5.Click on Activate your prepaid phone or device link | Clicked on Activate your prepaid phone or device link successfully");
			Reporter.log("6.Verify Activate your prepaid phone or device page | Activate your prepaid phone or device page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
	        unavPage.clickAboutUsSupportExpandButton();
			unavPage.verifyActivePhoneLabel();
			unavPage.clickActivePhonelink();
			unavPage.verifyActivePhonePage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Refill your prepaid account Redirects To Refill your prepaid account Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyRefillAccountLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify Refill your prepaid account label is displayed | Refill your prepaid account is displayed");
			Reporter.log("5.Click on Refill your prepaid account link | Clicked on Refill your prepaid account link successfully");
			Reporter.log("6.Verify Refill your prepaid account page | Refill your prepaid account page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
	        unavPage.clickAboutUsSupportExpandButton();
			unavPage.verifyRefillAccountLabel();
			unavPage.clickRefillAccountlink();
			unavPage.verifyRefillAccountPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify International rates Redirects To International rates Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyInternationalRatesLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify International rates label is displayed | International rates is displayed");
			Reporter.log("5.Click on International rates link | Clicked on International rates link successfully");
			Reporter.log("6.Verify International rates page | International rates page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
	        unavPage.clickAboutUsSupportExpandButton();
			unavPage.verifyInternationalRatesLabel();
			unavPage.clickInternationalRateslink();
			unavPage.verifyInternationalRatesPage();
		}
		/**
		 * US613371 - UNAV | Mobile Automation Testing
		 * US599731 - UNAV | E2E Flow SDET
		 * US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)
		 * US589198 - UNAV | UI Header & footer in Dev prod 2
		 * US613371 - UNAV | Mobile Automation Testing
		 * Verify Internet of Things Redirects To Internet of Things Page
		 * @param data
		 * @param myTmoData
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.ANDROID, Group.DESKTOP, Group.IOS })
		public void verifyInternetOfThingsLink(TMNGData tMNGData){

			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("US599731 - UNAV | E2E Flow SDET");
			Reporter.log("US580048 - UNav | Types of Navigation |Desktop,Tablet & Mobile | Universal Links (New design)");
			Reporter.log("US589198 - UNAV | UI Header & footer in Dev prod 2");
			Reporter.log("US613371 - UNAV | Mobile Automation Testing");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify About Us page | About Us page should be displayed");
			Reporter.log("4.Verify Internet of Things label is displayed | Internet of Things is displayed");
			Reporter.log("5.Click on Internet of Things link | Clicked on Internet of Things link successfully");
			Reporter.log("6.Verify Internet of Things page | Internet of Things page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			getDriver().get(getDriver().getCurrentUrl().concat("/about-us"));
	        unavPage.clickTMobileBusinessExpandButton();
			unavPage.verifyInternetOfThingsLabel();
			unavPage.clickInternetOfThingslink();
			unavPage.verifyInternetOfThingsPage();
		}
		/**
		 * CDCDWR2-42 - CCPA WS4 | No Sell Web, Sell/No Sell App | DNS Page | UI
		 * @param data
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING})
		public void verifyDoNotSellLink(TMNGData tMNGData){
			Reporter.log("CDCDWR2-42 - CCPA WS4 | No Sell Web, Sell/No Sell App | DNS Page | UI");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1.Launch the application | Application Should be Launched");
			Reporter.log("2.Verify Home page | Home page should be displayed");
			Reporter.log("3.Verify Do Not Sell Link is displayed | Do Not Sell is displayed");
			Reporter.log("4.Click on Do Not Sell Link | Clicked on Do Not Sell successfully");
			Reporter.log("5.Verify DNS page | DNS page should be displayed");
			Reporter.log("========================");
			Reporter.log("Actual Results");
			TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
			navigateToTMobilePage(tMNGData);
			unavPage.verifyDoNotSellLabel();
			unavPage.clickDoNotSellLink();
			unavPage.verifyDoNotSellPage();
		}
		
		/**
		 * CDCDWR2-44 -CCPA WS2 | OFD UNAV | Footer Link for DNS Page
		 * @param data
		 */
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX_SPRINT})
		public void verifyFooterDNSLinkOnTMNGPages(TMNGData tMNGData){
			Reporter.log("CDCDWR2-44-CCPA WS2 | OFD UNAV | Footer Link for DNS Page");
			Reporter.log("TC-1441-Verify DNS link present on tmng pages when prospect use accessing it");
			Reporter.log("========================");
			Reporter.log("Test Steps | Expected Results:");
			Reporter.log("1. Launch the application | Application Should be Launched");
			Reporter.log("2. Verify T-Mobile Home page | T-Mobile Home page should be displayed");
			Reporter.log("3. Verify phones unav header menu | Phones unav header menu should be displayed");
			Reporter.log("4. Click on phones unav and verify cell phones menu in drop down | Cell phones menu should be displayed");
			Reporter.log("5. Click on cell phones menu | Click on cell phones menu should be success");
			Reporter.log("6. Scroll down to footer and verify DNS footer link | DNS footer link should be displayed");
			Reporter.log("7. Validate unav header search menu with any vaild search criteria | Search function should return the results as per search criteria");
			Reporter.log("8. Scroll down to footer and verify DNS footer link | DNS footer link should be displayed");
			Reporter.log("9. Verify plans unav header menu | Plans unav header menu should be displayed");
			Reporter.log("10.Click on plans unav and verify magneta menu in drop down | Magneta menu should be displayed");
			Reporter.log("11.Click on magneta menu | Click on magneta menu should be success");
			Reporter.log("12.Scroll down to footer and verify DNS footer link | DNS footer link should be displayed");
			
			Reporter.log("========================");
			Reporter.log("Actual Results");
			
			navigateToTMobilePage(tMNGData);
		}
}