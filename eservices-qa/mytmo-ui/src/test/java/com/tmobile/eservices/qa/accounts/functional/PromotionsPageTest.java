/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.accounts.LineDetailsPage;
import com.tmobile.eservices.qa.pages.accounts.PromotionsPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class PromotionsPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(PromotionsPageTest.class);

	/**
	 * US258011#Promo Summary: Create New Promo Details page US265803#Promo Summary:
	 * Add All promotions with summarized promo information US258023#Promo Summary:
	 * Deep linking to Promo Summary Tab
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void testPromotionDetailsAndPastPromotionsPage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testPromotionDetailsAndPastPromotionsPage method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify Promotion Details And Past Promotions");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify Promotion URL | Promotions URL should be displayed");
		Reporter.log("5. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("6. Verify View past Promotions Link | View past Promotions Link should be displayed");
		Reporter.log("7. Click on View past Promotions Link | View past Promotions page should be displayed");
		Reporter.log(
				"8. Verify View past Promotions message | Showing promotions for the past 6 months should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.verifyMyPromotionsPageURL();
		promotionsPage.verifyViewPastPromotionsLink();
		promotionsPage.clickOnViewPastPromotionsLink();
		promotionsPage.verifyPastPromotionsPage("Past Promotions");
		promotionsPage.verifyShowingPromotionsText("Showing promotions for the past 6 months.");
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void testPromotionsPageUpsellURL(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testPromotionsPageUpsellURL method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify Promotion page UpsellURL");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("========================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Load Promotions Upsell URL | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToFutureURLFromHome(myTmoData, "/account/promotions");

		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.verifyViewPastPromotionsLink();
	}

	/**
	 * US258017#Promo Summary: Support ability to deep link with a specific promo
	 * expanded US258026#Promo Eligibility: Promo eligibility steps US258020#Promo
	 * Summary: Expanded promo description display modal
	 * 
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending-SBD")
	public void testPromotionDetailsPage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testPromotionDetails method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify Promotion Details Page");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Click on Promotion right down button | Eligibility Criteria should be displayed");
		Reporter.log("6. Verify Promotion Eligibility Criteria | Promotion Eligibility Criteria should be displayed");
		Reporter.log("7. Verify Eligibility Criteria list | Eligibility Criteria list should be displayed");
		Reporter.log(
				"8. Verify Completed Or Incomplete Eligibility Criteria | Completed Or Incomplete Eligibility Criteria should be displayed");
		Reporter.log("9. Click on Information icon | Information overlay should be displayed");
		Reporter.log("10. Verify Information overlay info | Information overlay info should be verified");
		Reporter.log("11. Close Information overlay | Information overlay should be closed");
		Reporter.log(
				"12. Verify Completed Or Incomplete Eligibility Criteria | Completed Or Incomplete Eligibility Criteria should be displayed");
		Reporter.log("9. Click on Promotion right Up button  | Promotion is Collapse should be verified");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);

		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.clickOnRadndomPromotionsRightDownBtn();
		promotionsPage.verifyFirstPromoEligibilityCriteriaText("Promo criteria");
		promotionsPage.verifyEligibilityCriteriaList();
		promotionsPage.verifyCompletedORIncompleteEligibilityCriteria();
		promotionsPage.clickOnInfoIcon();

		promotionsPage.verifyPromoInfoInIcon();
		promotionsPage.clickOkButton();

		promotionsPage.clickOnFirstPromotionsRightUpBtn();
		// US260568
	}

	/**
	 * US262445#Promo Summary: Page Navigation
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending-SBD")
	public void verifyPromoDetailsPageNavigations(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyPromoDetailsPageNavigations method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify Promo Details Page Navigations");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Click on Contact US icon | Contact US Page should be displayed");
		Reporter.log("6. Click on back button | My Promotions page should be displayed");
		Reporter.log("7. Click on back icon | Home should be displayed");
		Reporter.log("8. Load Promotions Page | My Promotions page should be displayed");
		Reporter.log("9. Click on TMobile icon |Home should be should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnContactUsImg();

		// commonPage.verifyContactUSPage();
		commonPage.navigateBack();

		commonPage.checkPageIsReady();
		commonPage.clickOnBackBtn();

		commonPage.checkPageIsReady();
		getPromotionsPage();

		commonPage.checkPageIsReady();
		commonPage.clickOnTMobileIcon();
	}

	/**
	 * US265808#Promo Summary: Support expanded promo information US266343#Promo
	 * Summary: Credit details US257751#[Continued] Promo MS: UI integration for
	 * promos
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending-SBD")
	public void verifyPromoSummaryInformation(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyPromoSummaryInformation method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify Promo Summary Information");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Click on Promotion right down button | Eligibility Criteria should be displayed");
		Reporter.log("6. Verify Account Holder Type | Account Holder Type should be displayed");
		Reporter.log("7. Verify Promo Status And Details | Promo Status And Details should be displayed");
		Reporter.log("8. Verify Credit You Will Get text | Credit You Will Get text should be displayed");
		Reporter.log("9. Verify Promotional Savings message | Promotional Savings message should be displayed");
		Reporter.log("10. Verify Credit details | Credit details should be displayed");
		Reporter.log("11. Verify Status Desctriptions | Status Desctription should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);

		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.clickOnRadndomPromotionsRightDownBtn();
		promotionsPage.verifyAccountHolderType();
		promotionsPage.verifyPromoStatusAndDetails();
		promotionsPage.verifyFirstPromoEligibilityCriteriaText("Promo criteria");
		promotionsPage.verifyCreditYouWillGetText();

		promotionsPage.verifyPromoCode();
		promotionsPage.verifyCreditDetailsText();
		promotionsPage.verifyPromoCriteriaText();

		Assert.assertFalse(promotionsPage.verifyLegalTextIsEmpty(), "Legal Text is not displaying");

	}

	/**
	 * US258017#Promo Summary: Support ability to deep link with a specific promo
	 * expanded US258026#Promo Eligibility: Promo eligibility steps
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void testSeePromotionsDetailsInPromoDetailspage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testSeePromotionsDetailsInPromoDetailspage method called in PromotionsPageTest");

		Reporter.log("Test Case : Test See Promotions Details In Promo Details page");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Click on Promotion right down button | Eligibility Criteria should be displayed");
		Reporter.log("6. Verify Limited Time Offer Text | Limited Time Offer Text should be displayed");
		Reporter.log("7. Click on See Promotions Details link | Promotional offers page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());

		promotionsPage.clickOnFirstPromotionsRightDownBtn();
		promotionsPage.verifyLimitedTimeOfferText();
		promotionsPage.clickOnSeePromotionsDetailsLink();
		promotionsPage.verifyPromotionsDetailsUrl();

	}

	/**
	 * US258017#Promo Summary: Support ability to deep link with a specific promo
	 * expanded US258026#Promo Eligibility: Promo eligibility steps US258016#Promo
	 * Summary: Add Terms and Conditions Page US266817#Promo Summary : Default Legal
	 * text when not provided by API
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue") // 4702655666/Auto12345
	public void testTermsAndConditionsInPromoDetailspage(ControlTestData data, MyTmoData myTmoData) {

		logger.info("testTermsAndConditionsInPromoDetailspage method called in PromotionsPageTest");

		Reporter.log("Test Case : Test Terms And Conditions In Promo Details page");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Click on Promotion right down button | Eligibility Criteria should be displayed");
		Reporter.log("6. Verify Terms And Conditions legal text | Terms And Conditions legal text should be displayed");
		Reporter.log("7. Click on Terms And Conditions link | Terms & Conditions page should be displayed");
		Reporter.log("8. Verify Terms & Conditions page | Terms & Conditions page should be displayed");
		Reporter.log("9. Verify  Print this page | Print this page  should be displayed");
		Reporter.log("================================");
		Reporter.log("4702655666/Auto12345");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.clickOnFirstPromotionsRightDownBtn();
		promotionsPage.verifyLimitedTimeOfferText();

		promotionsPage.clickOnSeePromotionsDetailsLink();
		promotionsPage.acceptIOSAlert();
		promotionsPage.verifyPromotionsDetailsTCLegalHeaderText();
		promotionsPage.closeAllOtherWindows();

		promotionsPage.clickOnTermsAndConditionsLink();
		promotionsPage.verifyTCLegalHeaderText();
		// promotionsPage.verifyPrintThisPageLink();
	}

	/**
	 * US262800#Launch Next Steps: Update UI with Next Steps US284266#Launch Next
	 * Steps: Implement return to My Promotion
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyPromoCriteriaChangePlanFlow(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyPromoCriteriaChangePlanFlow method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify Promo Criteria Change Plan");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Click on Promotion right down button | Eligibility Criteria should be displayed");
		Reporter.log("6. Verify Change plan |Change plan link should display");
		Reporter.log("7. Click on Change plan | Plans comparison page should display");
		Reporter.log("8. Verify Plans comparison page | Plans comparison page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);

		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.clickOnRadndomPromotionsChangePlanBtn();
		promotionsPage.clickOnChangePlanBtn();
		promotionsPage.verifyPlansComparisonPage();
	}

	/**
	 * US269917#EFPE Promos: Promotion date handling US269922#EFPE Promos: Hide
	 * Credit details section US269923#EFPE Promos: Do not display promo description
	 * 
	 * 
	 * US278106#EFPE: Do not Display Legal Close Proximity Text
	 * 
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending-SBD")
	public void verifyEFPEPromotionsFlow(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyEFPEPromotionsFlow method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify EFPE Promotions Flow");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Click on Promotion right down button | Eligibility Criteria should be displayed");
		Reporter.log("6. Click On Change plan | Plans comparison page should be displayed");
		Reporter.log("7. Verify EFPE Promotions | EFPE Promotions page should be displayed");
		Reporter.log("8. Verify Start and End dates | Start and End dates should not displayed");
		Reporter.log("9. Verify Legal Close Proximity Text | Legal Close Proximity Text should not displayed");
		Reporter.log("10. Verify Promo description | promo description should not displayed");
		Reporter.log("11. Verify Credit details | Credit details should not displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.clickOnRadndomPromotionsCreditDetailsListTexts();
		Assert.assertTrue(promotionsPage.verifyCreditDetailsList(),
				"Total monthly credit,Duration and Total credit amount are not displaying");

		Assert.assertTrue(promotionsPage.verifyLegalTextIsEmpty(), "Legal Text is displaying for EFPEP");
	}

	/**
	 * US286314#EFPE Promos: Add values to Credit Details
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyPromotionsAddValuesToCreditDetails(ControlTestData data, MyTmoData myTmoData) {

		logger.info("VerifyPromotionsAddValuesToCreditDetails method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify Promotions Add Values To Credit Details");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Click on Promotion right down button | Eligibility Criteria should be displayed");
		Reporter.log("6. Click On Change plan | Plans comparison page should be displayed");
		Reporter.log("7. Verify EFPE Promotions | EFPE Promotions page should be displayed");
		Reporter.log("8. Verify Start and End dates | Start and End dates should not displayed");
		Reporter.log("9. Verify Legal Close Proximity Text | Legal Close Proximity Text should not displayed");
		Reporter.log("10. Verify Promo description | promo description should not displayed");
		Reporter.log("11. Verify Credit details | Credit details should not displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.clickOnFirstPromotionsRightDownBtn();
		Assert.assertTrue(promotionsPage.verifyCreditDetailsList(), "Credit details are not displaying");
		promotionsPage.clickGoToMyPromotionsBtn();
		verifyMyPromotionsPage();
	}

	public void getPromotionsPage() {

		System.out.println(getDriver().getCurrentUrl());
		String[] currentUrl = getDriver().getCurrentUrl().split("home");
		String promotions = currentUrl[0] + "promotions";
		getDriver().get(promotions);
	}

	/**
	 * US286496#Launch Next Steps: Restrict button display based on role
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyPromoCriteriaRestrictChangePlanButtonForPAH(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyPromoCriteriaRestrictChangePlanButtonForPAH method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify Promo Criteria Restrict Change Plan Button For PAH");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Click on Promotion right down button | Eligibility Criteria should be displayed");
		Reporter.log("6. Verify Change plan |Change plan button should not displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);

		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.clickOnRadndomPromotionsRightDownBtn();
		promotionsPage.verifyChangePlanButtonRestrictedCustomer();
	}

	/**
	 * US286496#Launch Next Steps: Restrict button display based on role
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyPromoCriteriaRestrictChangePlanButtonForSA(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyPromoCriteriaRestrictChangePlanButtonForSA method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify Promo Criteria Restrict Change Plan Button For SA");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Click on Promotion right down button | Eligibility Criteria should be displayed");
		Reporter.log("6. Verify Change plan |Change plan button should not displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.clickOnRadndomPromotionsRightDownBtn();
		promotionsPage.verifyChangePlanButtonRestrictedCustomer();
	}

	/**
	 * US286496#Launch Next Steps: Restrict button display based on role
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifyPromoCriteriaRestrictChangePlanButtonForRA(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyPromoCriteriaRestrictChangePlanButtonForRA method called in PromotionsPageTest");
		Reporter.log("Test Case : Verify Promo Criteria Restrict Change Plan Button For RA");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Click on Promotion right down button | Eligibility Criteria should be displayed");
		Reporter.log("6. Verify Change plan |Change plan button should not displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);

		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.clickOnRadndomPromotionsRightDownBtn();
		promotionsPage.verifyChangePlanButtonRestrictedCustomer();
	}

	/**
	 * US286596#Promo Summary: Add link to access My Promotions Page US286598#Promo
	 * Summary: Modify 'See Promo Details' link
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending")
	public void verifySeePromoDetails(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifySeePromoDetails method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify See Promo Details");
		Reporter.log("Test Data : Promotion data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("========================");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Plan link | Plan page should be displayed");
		Reporter.log("4. Verify View your promos link | View your promos link should be displayed");
		Reporter.log("5. Click View your promos link | My Promotions page should be displayed");
		Reporter.log("5. Verify My Promotions Page |Promotions should be shown");
		Reporter.log(
				"6. Verify My Promotions Page |Text No promotions to show at this time should be displayed if no promotions");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);

		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.clickOnRadndomPromotionsRightDownBtn();
		promotionsPage.clickOnSeePromotionsDetailsLink();
		promotionsPage.verifyPromotionsDetailsTCLegalHeaderText();
	}

	/**
	 * US292496#Convert Accordion UI on My Promotions page to list view
	 * US293927#Modify promotions page to segregate Pending and Active Promotions
	 * US293929#Default Behavior - display only Pending & Active Promotions on
	 * Promotions landing page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending-SBD")
	public void verifyPromotionsGroupedByPendingAndActive(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPromotionsGroupedByPendingAndActive method called in PromotionsPageTest");
		Reporter.log("Test Case : Verify Promotions Grouped By Pending And Active in list view");
		Reporter.log("Test Data : any PAH/Full/Standard/Restricted User");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Verify My Promotions Page |Promotions should be shown");
		Reporter.log(
				"6. Verify the summary on the promotions page|Promotions should be grouped by pending and Active promotions as a list view");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.verifyPendingText("Pending");
		promotionsPage.clickOnPromotion();
		promotionsPage.verifyGoToMyPromotionsBtn();
	}

	/**
	 * US291024#Implement new promotion details page US292502#Data integration for
	 * new promotion details page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending-SBD")
	public void verifyPromotionsDetailsDisplayedInNewPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPromotionsDetailsDisplayedInNewPage method called in PromotionsPageTest");
		Reporter.log("Test Case : Verify Promotions Details Displayed In NewPage ");
		Reporter.log("Test Data : any PAH/Full/Standard/Restricted User");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Verify My Promotions Page |Promotions should be shown");
		Reporter.log(
				"6. Verify the summary on the promotions page|Promotions should be grouped by pending and Active promotions as a list view");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);
		// PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		// US291024, //US292502
	}

	/**
	 * US269929#Filter promotions based on Line
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending-SBD")
	public void verifyLineSelectorForPA(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyLineSelectorForPA method called in PromotionsPageTest");
		Reporter.log("Test Case : Verify Promotions Details Displayed In NewPage ");
		Reporter.log("Test Data : any PAH/Full User");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Verify My Promotions Page |Promotions should be shown");
		Reporter.log("6. Select the desired line from dropdown list|should only view promotions for the selected line");
		Reporter.log("7. Verify the default  line shown in the dropdown|Should be the logged in line");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);

		// PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		// US269929
	}

	/**
	 * US269929#Filter promotions based on Line
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "pending-SBD")
	public void verifyLineSelectorForSA(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyLineSelectorForSA method called in PromotionsPageTest");
		Reporter.log("Test Case : Verify Promotions Details Displayed In NewPage ");
		Reporter.log("Test Data : any Standard/Restricted User");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Verify My Promotions Page |Promotions should be shown");
		Reporter.log("6. Select the desired line from dropdown list|should only view promotions for the selected line");
		Reporter.log("7. Verify the default  line shown in the dropdown| Line selecton dropdown should not be shown");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);
		// PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		// US269929
	}

	/**
	 * US293928#Modify PDE Micro Service to return Line Details for Line Selector
	 * US292865#Populate Line Selector drop-down values on My Promotions page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyPopulateLineSelectorDropDownValuesOnMyPromotionsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPopulateLineSelectorDropDownValuesOnMyPromotionsPage method called in PromotionsPageTest");
		Reporter.log("Test Case : Verify Populate Line Selector drop-down values on My Promotions page");
		Reporter.log("Test Data : Promotion Data");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Verify My Promotions header | My Promotions header should be displayed");
		Reporter.log("5. Verify My Promotions Page |Promotions should be shown");
		Reporter.log("6. Select the line selector dropdown| All of the lines from my account  should be displayed");
		Reporter.log(
				"7. Verify the line details | Name and phone numbers for all of the lines on account should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.clickSelectLineArrowDown();
		promotionsPage.selectLineDropDown();
		promotionsPage.verifyViewPastPromotionsLink();
	}

	/**
	 * 
	 * US293439#Create Past Promotions link US293450#Create Past Promotions page
	 * US293464#Implement Past Promotions Details page US293500#Create back button
	 * on Past Promotions page
	 * 
	 * @param data
	 * 
	 * @param myTmoData
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.MULTIREG, Group.ACCOUNTS_PHONEPAGE })
	public void verifyPastPromotionsDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPastPromotionsDetailsPage method called in PromotionsPageTest");
		Reporter.log(
				"Test Case : Verify past promotion details present in the past promotion page after cliking on the past promtoions link in  My Promotions page, also verify the back button functionality");
		Reporter.log("Test Data : Promotion Data");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Click on view past promotions link| Past promotions page should be displayed");
		Reporter.log(
				"5. Click on first promotion| Promotion details page should be displayed, change plan button should not be displayed");
		Reporter.log("6.Click on Go to my promotions link| Past promotions page should be displayed");
		Reporter.log("7.Click on Go to my promotions link|My promotions page should be displayed");
		Reporter.log("=============================");
		Reporter.log("Actual Results:");

		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.verifyViewPastPromotionsLink();
		promotionsPage.clickOnViewPastPromotionsLink();
		promotionsPage.verifyPastPromotionsPage("Past promotions");
		// promotionsPage.verifyShowingPromotionsText("Showing promotions for
		// the past 6 months.");
		// String promotionName = promotionsPage.getPromotionName();
		// promotionsPage.clickOnPromotion();
		// promotionsPage.verifySelectedPromotionDetails(promotionName);
		// Assert.assertTrue(promotionsPage.verifyNoChangePlanBtn(), "Change
		// plan button is displayed");
		// promotionsPage.verifyGoToMyPromotionsBtn();
		// promotionsPage.clickGoToMyPromotionsBtn();
		// promotionsPage.verifyPastPromotionsPage("Past Promotions");
		promotionsPage.clickGoToMyPromotionsBtn();
		promotionsPage.verifyMyPromotionsPage("My promotions");
	}

	/**
	 * 
	 * US284275#No promo available message for each status Promotions page
	 * 
	 * @param data
	 * 
	 * @param myTmoData
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE })
	public void verifyPromoMessagesDisplayedforNoPendingOrActiveStatus(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyPromoMessagesDisplayedforNoPendingOrActiveStatus method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify promo messages displayed for different status");
		Reporter.log("Test Data : Promotion Data");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. No Pending or active promotions text should be displayed");
		Reporter.log("=============================");
		Reporter.log("Actual Results:");
		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.verifyNoPendingOrActivePromotionsText();

	}

	/**
	 * 
	 * US284275#No promo available message for each status Promotions page
	 * 
	 * @param data
	 * 
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.ACCOUNTS_PHONEPAGE, Group.MULTIREG })
	public void verifyPromoMessagesDisplayedforNoPromotionsStatus(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyPromoMessagesDisplayedforNoPromotionsStatus method called in PromotionsPageTest");
		Reporter.log("Test Case : Verify promo messages displayed for different status");
		Reporter.log("Test Data : Promotion Data");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. No promotions available at this time. text should be displayed");
		Reporter.log("=============================");
		Reporter.log("Actual Results:");

		LineDetailsPage linedetailspage = navigateToLineDetailsPage(myTmoData);
		linedetailspage.clickOnMypromotions();

		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.verifyNoPromotionsText();
	}

	/**
	 * 
	 * US284275#No promo available message for each status Promotions page
	 * 
	 * @param data
	 * 
	 * @param myTmoData
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyPromoMesDisplayedforNoActivePromotionsStatus(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyPromoMesDisplayedforNoActivePromotionsStatus method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify promo messages displayed for different status");
		Reporter.log("Test Data : Promotion Data");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. No active promotions available at this time. text should be displayed");

		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.verifyNoActivePromotionsText();

	}

	/**
	 * 
	 * US284275#No promo available message for each status Promotions page
	 * 
	 * @param data
	 * 
	 * @param myTmoData
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyPendingHeaderIsNotDisplayedForNoPendingPromos(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyPendingHeaderIsNotDisplayedForNoPendingPromos method called in PromotionsPageTest");

		Reporter.log("Test Case : Verify promo messages displayed for different status");
		Reporter.log("Test Data : Promotion Data");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Pending promotions header should not be displayed");

		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.verifyPendingHeaderIsNotDisplayed();

	}

	/**
	 * 
	 * US293498#Create back button on Promotion Detail's page Promotions page
	 * 
	 * @param data
	 * 
	 * @param myTmoData
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = "dataissue")
	public void verifyBackButtonFunctionalityPromotionDetailsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBackButtonFunctionalityPromotionDetailsPage method called in PromotionsPageTest");
		Reporter.log("Test Case :Verify back button functionality in Promotion details page");
		Reporter.log("Test Data : Promotion Data");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Click on view past promotions link| Past promotions page should be displayed");
		Reporter.log("5. Click on first promotion| Promotion details page should be displayed");
		Reporter.log("6.Click on Go to my promotions link| Past promotions page should be displayed");
		Reporter.log("7.Click on Go to my promotions link|My promotions page should be displayed");
		Reporter.log("=============================");
		Reporter.log("Actual Results:");
		navigateToMyPromotionsPage(myTmoData);
		PromotionsPage promotionsPage = new PromotionsPage(getDriver());
		promotionsPage.verifyViewPastPromotionsLink();
		promotionsPage.clickOnViewPastPromotionsLink();
		promotionsPage.verifyPastPromotionsPage("Past promotions");
		// promotionsPage.clickOnPromotion();
		// promotionsPage.verifyGoToMyPromotionsBtn();
		promotionsPage.clickGoToMyPromotionsBtn();
		// promotionsPage.verifyPastPromotionsPage("Past promotions");
		// promotionsPage.clickGoToMyPromotionsBtn();
		promotionsPage.verifyMyPromotionsPage("My promotions");
	}

	/**
	 * 
	 * US302725#PDE - Show "Cancelled line" and MSIDN for cancelled lines appearing
	 * on My Promotions Page (Issue 32) Promotions page
	 * 
	 * @param data
	 * 
	 * @param myTmoData
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RELEASE_READY })
	public void verifyCancelledLineAndMISDnForCancelledLineIsDisplayed(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyCancelledLineAndMISDnForCancelledLineIsDisplayed method called in PromotionsPageTest");
		Reporter.log("Test Case :Verify Cancelled line and a MISDN for it is displayed in My Promotion page");
		Reporter.log("Test Data : Promotion Data");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToMyPromotionsPage(myTmoData);
		// PromotionsPage promotionsPage = new PromotionsPage(getDriver());

	}

	/**
	 * 
	 * US262896# Integrate PDL for All Promos
	 * 
	 * @param data
	 * @param myTmoData
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyUserInteractionsAreTrackedPromotionsPage(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyBackButtonFunctionalityPromotionDetailsPage method called in PromotionsPageTest");
		Reporter.log("Test Case :Verify back button functionality in Promotion details page");
		Reporter.log("Test Data : Promotion Data");
		Reporter.log("=============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Login to the application | User Should be login successfully");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Click on Promotion link | My Promotions page should be displayed");
		Reporter.log("4. Actions performed on this page should be tracked | Actions are tracked.");

		navigateToMyPromotionsPage(myTmoData);
		// PromotionsPage promotionsPage = new PromotionsPage(getDriver());
	}
}
