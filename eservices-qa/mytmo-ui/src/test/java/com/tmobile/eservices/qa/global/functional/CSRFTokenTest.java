package com.tmobile.eservices.qa.global.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;

public class CSRFTokenTest extends GlobalCommonLib {

	/**
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyCSRFTokenForHomePage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify CSRF token | CSRF token should be same for all services");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		// WebDriver driver=launchAndPerformLoginForCSRFTOKEN(myTmoData);

		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();
		homePage.clickShoplink();
		homePage.verifyCSRFToken(getDriver());
	}

	/**
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyCSRFTokenForShopPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify CSRF token | CSRF token should be same for all services");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		// WebDriver driver=launchAndPerformLoginForCSRFTOKEN(myTmoData);

		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();

		homePage.clickShoplink();

		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyShoppage();
		shopPage.verifyCSRFToken(getDriver());
	}

	/**
	 * @param data
	 * @param myTmoData
	 * @throws InterruptedException
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyCSRFTokenForAccountPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Verify CSRF token | CSRF token should be same for all services");

		Reporter.log("========================");
		Reporter.log("Actual Results");

		// WebDriver driver=launchAndPerformLoginForCSRFTOKEN(myTmoData);

		HomePage homePage = new HomePage(getDriver());
		homePage.verifyPageLoaded();

		homePage.clickShoplink();

		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyShoppage();
		shopPage.verifyCSRFToken(getDriver());
	}

}
