/**
 * 
 */
package com.tmobile.eservices.qa.tmng.functional;


import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.UNAVCommonPage;
import com.tmobile.eservices.qa.pages.tmng.functional.TMNGNewHeaderPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

/**
 * @author ksrivani
 *
 */
public class HomePageTest extends TmngCommonLib {
	/**
	 * US603774:[Continued] OFD | Search | UNAV | search dropdown | HTML Changes
	 * US609218:OFD | Search | UNAV | search text & hint text
	 * US546102:OFD | Search | UNAV | quick links
	 * US609216:OFD | Search | UNAV | top searches
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testSearchDropDownAlongWithQuickLinksAndTopSearches(TMNGData tMNGData) {
		Reporter.log("US603774:[Continued] OFD | Search | UNAV | search dropdown | HTML Changes");
		Reporter.log("US609218:OFD | Search | UNAV | search text & hint text");
		Reporter.log("US546102:OFD | Search | UNAV | quick links");
		Reporter.log("US609216:OFD | Search | UNAV | top searches");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify search button is displayed | Search button displayed successfully");
		Reporter.log("4.Click on search button | Clicked on Search button");
		Reporter.log("5.Verify search input field is displayed | Search input field is displayed");
		Reporter.log("6.Verify search hint text is displayed in text box | Search hint text is displayed in the text box");
		Reporter.log("7.Click on Search Close button | Clicked on Search close button successfully");
		Reporter.log("8.Click on search button | Clicked on Search button");
		Reporter.log("9.Verify search input field is displayed | Search input field is displayed");
		Reporter.log("10.Verify drop down display | Drop down is displayed upon clicking on Search");
		Reporter.log("11.Verify Quick links headline in the drop down | Quick links headline is displayed in the drop down");
		Reporter.log("12.Verify Quick links displayed in the drop down | Quick links are displayed in the drop down");
		Reporter.log("13.Verify Quick links are clickable | Quick links are clickable");
		Reporter.log("14.Verify Top searches headline in the drop down | Top searches headline is displayed in the drop down");
		Reporter.log("15.Verify Top searches displayed in the drop down | Top searches are displayed in the drop down");
		Reporter.log("16.Verify Top searches are clickable | Top searches are clickable");
		Reporter.log("17.Verify search icon beside each top search Top | Search icon is displayed beside each top search");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage homePage = new UNAVCommonPage(getDriver());

		navigateToTMobilePage(tMNGData);
		homePage.verifySearchButton();
		homePage.clickSearchButton();
		homePage.verifySearchInputField();
		homePage.verifySearchHintText();
		homePage.clickSearchCloseButton();
		homePage.clickSearchButton();
		homePage.verifySearchInputField();
		homePage.verifySearchDropDown();
		homePage.verifyQuickLinksHeadline();
		homePage.verifyQuickLinksInDropDown();
		homePage.clickQuickLink();
		homePage.verifyTopSearchesHeadline();		
		homePage.verifyTopSearchesInDropDown();
		homePage.clickOnTopSearch();
		homePage.verifySearchIconBesideTopSearch();
	}
	
	/**
	 * US602350:OFD | Search | UNAV | type-ahead search suggestions
	 * TC318482
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testUNAVTypeAheadSearchSuggestions(TMNGData tMNGData) {
		
		Reporter.log("US603774:[Continued] OFD | Search | UNAV | search dropdown | HTML Changes");
		Reporter.log("Step 1: Navigate to https://www.t-mobile.com/  | It should display t-mobile home page");
		Reporter.log("Step 2: verify  search icon | search icon should be displayed");
		Reporter.log("Step 3: click on Search link text or search icon | search text field should be expanded");
		Reporter.log("Step 4: enter a type ahead character in search field | maximum of 8 type-ahead predictive search suggestions should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		UNAVCommonPage homePage = new UNAVCommonPage(getDriver());

		navigateToTMobilePage(tMNGData);	
		homePage.verifySearchButton();
		homePage.clickSearchButton();
		homePage.verifySearchInputField();
		homePage.verifyTypeAheadSearch("Apple");
	}
	
	/**
	 * CDCDWR-332:UNAV | Cookied User to see 'Back to Myaccount' on Tmo
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testBackToMyAccountOnTMO(TMNGData tMNGData,MyTmoData myTmoData) {
		
		Reporter.log("CDCDWR-332:UNAV | Cookied User to see 'Back to Myaccount' on Tmo");
		Reporter.log("===============================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Navigate to https://www.t-mobile.com/  | T-mobile Home page should be displayed");
		Reporter.log("Step 2: Verify  my account link | My account link should be displayed");
		Reporter.log("Step 3: Click my account link | Click on my account link should be success");
		Reporter.log("Step 4: Verify TMO Home page| T-mobile Home page should be displayed");
		Reporter.log("Step 5: Verify back to my account link| Back to my account link should be displayed");
		
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		UNAVCommonPage homePage = new UNAVCommonPage(getDriver());
		launchAndPerformLogin(myTmoData);
		homePage.clickUtilityMyAccountlink();
		getDriver().get("https://uat2.digital.t-mobile.com");
		TMNGNewHeaderPage unavPage = new TMNGNewHeaderPage(getDriver());
		unavPage.verifyUtilityMyAccountLabel();
		unavPage.clickUtilityMyAccountlink();
		unavPage.verifyUtilityBacktoMyAccountLabel();
		unavPage.clickUtilityBacktoMyAccountlink();
		homePage.verifyBackToHomePage();
	}
}