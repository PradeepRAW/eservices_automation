/**
 * 
 */
package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.PrepaidFamilyControlsPage;
import com.tmobile.eservices.qa.pages.accounts.PrepaidProfileLandingPage;

/**
 * @author Sudheer Reddy Chilukuri
 *
 */
public class PrepaidFamilyControlPageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(PrepaidFamilyControlPageTest.class);

	/*
	 * CDCAM-864 [Prepaid Profile Page .NET Migration] Profile Page_ Family Control TAB
	 * CDCAM-1075  [Prepaid Profile Page .NET Migration] Family Control_Webguard
	 * 
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyFamilyControlPageAndSelectedWebGuardOption(ControlTestData data, MyTmoData myTmoData) {
		logger.info("checkFamilyControlPage");
		Reporter.log("Test Case : Check Family Control page url");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on T-Family Control tab | Family Control page should be displayed");
		Reporter.log("6. Verify URL. | It should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");
		
		navigateToProfilePage(myTmoData);
		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		/*navigateToHomePage(myTmoData);
		
		prepaidProfilePage.directURLForProfilePage();
		prepaidProfilePage.verifyPrePaidProfilePage();
		*/
		prepaidProfilePage.clickOnFamilyControlTab();

		PrepaidFamilyControlsPage prepaidFamilyControlPage = new PrepaidFamilyControlsPage(getDriver());
		prepaidFamilyControlPage.verifyFamilyControlsPage();
		String permissionDisplayedOnFamilyControlPage = prepaidFamilyControlPage.getCurrentWebGuardSelection();
		System.out.println("Displayed on Family Control page "+permissionDisplayedOnFamilyControlPage);
		prepaidFamilyControlPage.clickOnCaratSignOfWebGuardTab();
		prepaidFamilyControlPage.verifyWebGuardPage();
		prepaidFamilyControlPage.verifySelectedWebGuardOptionAndDisplayedOnPreviousPage(permissionDisplayedOnFamilyControlPage);		
	}

	/*
	 * CDCAM-1075  [Prepaid Profile Page .NET Migration] Family Control_Webguard
	 * 
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyUserAbleToSelectChildWebGuard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleToSelectChildWebGuard");
		Reporter.log("Test Case : Check whether user able to select Child Web Guard or not");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on T-Family Control tab | Family Control page should be displayed");
		Reporter.log("6. Click on Web Guard blade. | Web Guard page should be displayed");
		Reporter.log("7. Select Child Web Guard option. | Should be able to select Child Web Guard.");
		Reporter.log("8. Click on Save Changes CTA and check web guard value on Web Guard blade. | Selected Child web Guard value should be displayed on Web Guard blade.");		
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		
		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.clickOnFamilyControlTab();

		PrepaidFamilyControlsPage prepaidFamilyControlPage = new PrepaidFamilyControlsPage(getDriver());
		prepaidFamilyControlPage.verifyFamilyControlsPage();
		
		String permissionDisplayedOnFamilyControlPage = prepaidFamilyControlPage.getCurrentWebGuardSelection();
		System.out.println("Displayed on Family Control page "+permissionDisplayedOnFamilyControlPage);
		prepaidFamilyControlPage.clickOnCaratSignOfWebGuardTab();
		prepaidFamilyControlPage.verifyWebGuardPage();
		
		prepaidFamilyControlPage.selectChildWebGuardOption();
		
	}

	/*
	 * CDCAM-1075  [Prepaid Profile Page .NET Migration] Family Control_Webguard
	 * 
	 * 
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyUserAbleToSelectTeenWebGuard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleToSelectTeenWebGuard");
		Reporter.log("Test Case : Check whether user able to select Teen Web Guard or not");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on T-Family Control tab | Family Control page should be displayed");
		Reporter.log("6. Click on Web Guard blade. | Web Guard page should be displayed");
		Reporter.log("7. Select Teen Web Guard option. | Should be able to select Teen Web Guard.");
		Reporter.log("8. Click on Save Changes CTA and check web guard value on Web Guard blade. | Selected Teen web Guard value should be displayed on Web Guard blade.");		
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		
		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.clickOnFamilyControlTab();

		PrepaidFamilyControlsPage prepaidFamilyControlPage = new PrepaidFamilyControlsPage(getDriver());
		prepaidFamilyControlPage.verifyFamilyControlsPage();
		
		String permissionDisplayedOnFamilyControlPage = prepaidFamilyControlPage.getCurrentWebGuardSelection();
		System.out.println("Displayed on Family Control page "+permissionDisplayedOnFamilyControlPage);
		prepaidFamilyControlPage.clickOnCaratSignOfWebGuardTab();
		prepaidFamilyControlPage.verifyWebGuardPage();
		
		prepaidFamilyControlPage.selectTeenWebGuardOption();
		
	}

	/*
	 * CDCAM-1075  [Prepaid Profile Page .NET Migration] Family Control_Webguard
	 * 
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyUserAbleToSelectYoungAdultWebGuard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleToSelectTeenWebGuard");
		Reporter.log("Test Case : Check whether user able to select Teen Web Guard or not");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on T-Family Control tab | Family Control page should be displayed");
		Reporter.log("6. Click on Web Guard blade. | Web Guard page should be displayed");
		Reporter.log("7. Select YoungAdult Web Guard option. | Should be able to select YoungAdult Web Guard.");
		Reporter.log("8. Click on Save Changes CTA and check web guard value on Web Guard blade. | Selected YoungAdult web Guard value should be displayed on Web Guard blade.");		
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		
		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.clickOnFamilyControlTab();

		PrepaidFamilyControlsPage prepaidFamilyControlPage = new PrepaidFamilyControlsPage(getDriver());
		prepaidFamilyControlPage.verifyFamilyControlsPage();
		
		String permissionDisplayedOnFamilyControlPage = prepaidFamilyControlPage.getCurrentWebGuardSelection();
		System.out.println("Displayed on Family Control page "+permissionDisplayedOnFamilyControlPage);
		prepaidFamilyControlPage.clickOnCaratSignOfWebGuardTab();
		prepaidFamilyControlPage.verifyWebGuardPage();
		
		prepaidFamilyControlPage.selectYoungAdultWebGuardOption();
		
	}

	/*
	 * CDCAM-1075  [Prepaid Profile Page .NET Migration] Family Control_Webguard
	 * 
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyUserAbleToSelectNoRestrictionsWebGuard(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyUserAbleToSelectNoRestrictionsWebGuard");
		Reporter.log("Test Case : Check whether user able to select Teen Web Guard or not");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on T-Family Control tab | Family Control page should be displayed");
		Reporter.log("6. Click on Web Guard blade. | Web Guard page should be displayed");
		Reporter.log("7. Select NoRestrictions Web Guard option. | Should be able to select NoRestrictions Web Guard.");
		Reporter.log("8. Click on Save Changes CTA and check web guard value on Web Guard blade. | Selected NoRestrictions web Guard value should be displayed on Web Guard blade.");		
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		
		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.clickOnFamilyControlTab();

		PrepaidFamilyControlsPage prepaidFamilyControlPage = new PrepaidFamilyControlsPage(getDriver());
		prepaidFamilyControlPage.verifyFamilyControlsPage();
		
		String permissionDisplayedOnFamilyControlPage = prepaidFamilyControlPage.getCurrentWebGuardSelection();
		System.out.println("Displayed on Family Control page "+permissionDisplayedOnFamilyControlPage);
		prepaidFamilyControlPage.clickOnCaratSignOfWebGuardTab();
		prepaidFamilyControlPage.verifyWebGuardPage();
		
		prepaidFamilyControlPage.selectNoRestrictionsWebGuardOption();
		
	}

	/*
	 * CDCAM-1075  [Prepaid Profile Page .NET Migration] Family Control_Webguard
	 * 
	 * 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyWebGuardPageAndOptionsOnIt(ControlTestData data, MyTmoData myTmoData) {
		logger.info("verifyWebGuardPageAndOptionsOnIt");
		Reporter.log("Test Case : Check Web Guard page Header, options, Show Filter details, CTAs");
		Reporter.log("Test Data : Valid prepaid user");
		Reporter.log("==================================");
		Reporter.log("TestSteps | Expected Results:");
		Reporter.log("1. Launch application | Application should be launched");
		Reporter.log("2. Login to the application  |  User should be able to login Successfully");
		Reporter.log("3. Verify Home page  |  Home Page Should be displayed");
		Reporter.log("4. Click on Profile link | Profile page should be displayed");
		Reporter.log("5. Click on T-Family Control tab | Family Control page should be displayed");
		Reporter.log("6. Click on Web Guard blade. | Web Guard page should be displayed");
		Reporter.log("7. Check Header, Permission text and sub text. | Header, Permission text and sub text should be displayed.");
		Reporter.log("8. Check all Web Guard options. | Web Guard options Child,Teen,Young Adult and No Restrictions should be displayed.");		
		Reporter.log("9. Check Show Filter Details link. | It should be displayed and can be expanded.");
		Reporter.log("10. Check texts displayed for Child,Teen,Young Adult and No Restrictions. | It should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		navigateToProfilePage(myTmoData);
		
		PrepaidProfileLandingPage prepaidProfilePage = new PrepaidProfileLandingPage(getDriver());
		prepaidProfilePage.clickOnFamilyControlTab();

		PrepaidFamilyControlsPage prepaidFamilyControlPage = new PrepaidFamilyControlsPage(getDriver());
		prepaidFamilyControlPage.verifyFamilyControlsPage();
		
		prepaidFamilyControlPage.clickOnCaratSignOfWebGuardTab();
		prepaidFamilyControlPage.verifyWebGuardPage();
		
		prepaidFamilyControlPage.verifyHeaderAndPermissiontextWebGuardPage();
		prepaidFamilyControlPage.checkAllWebGuardOptions();

		
		prepaidFamilyControlPage.checkShowandHideFilterDetailsLinkFunctionality();
		
		prepaidFamilyControlPage.checkAllTextsUnderShowFilterDetailsSection();
		
	}

}