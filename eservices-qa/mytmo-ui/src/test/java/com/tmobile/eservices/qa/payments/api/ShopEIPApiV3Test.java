package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.ShopEIPAPICollectionV3;

import io.restassured.response.Response;

public class ShopEIPApiV3Test extends ShopEIPAPICollectionV3 {

	public Map<String, String> tokenMap;

	/**
	/**
	 * UserStory# US496500 SHOP-Modify EOS Payment to support processPayment for EIP, expose new endpoint
	 *          # US496502 SHOP-Define mapping from EIP processPayment request to managePayment processPayment within eos
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, priority = 0, groups= {"ShopEIPAPIV3","testShopEIPAPIV3",Group.SHOP,Group.SPRINT})
	public void testShopEIPAPIV3(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("US496500 SHOP-Modify EOS Payment to support processPayment for EIP, expose new endpoint");
		Reporter.log("Data Conditions:EIP eligible MSDSIN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1:Expose  EOS Payment - EIP payment with EOS Payment Manager V3 Tokenization  |  EIP Payment should  process successfully.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 3:If  EIP Payment is processed and Failed  |  Appropriate failure response code and response details should be returned in response");
		Reporter.log("Step 3: Verify the success message returned by the DPS team.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("Shop_EIP_PaymentsV3.txt");	

		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("chargeAmount", String.valueOf(Math.round(generateRandomAmount()* 100.0) / 100.0));

		String operationName="testShopEIPPayment";

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		ShopEIPAPICollectionV3 shopEIPAPICollectionV3 = new ShopEIPAPICollectionV3();

		Response response = shopEIPAPICollectionV3.shop_EIP_Payments(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode()==200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode,"messages.applicationName"),
						"DPS" , "application message is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"messages.uiMessage"), 
						"Your transaction has been successfully approved. Thank you.",
						"uiMessage is mismatched");
				Assert.assertEquals(getPathVal(jsonNode,"messages.uiAction"), 
						"Approved  Successfully ", "uiAction message is mismatched");
			}
		}else{
			failAndLogResponse(response, operationName);
		}
	}
}
