package com.tmobile.eservices.qa.shop.functional;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.InterstitialTradeInPage;
import com.tmobile.eservices.qa.pages.shop.TradeInAnotherDevicePage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import com.tmobile.eservices.qa.shop.ShopConstants;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

public class TradeInAnotherDevicePageTest extends ShopCommonLib {

    /**
     * US152924 - Trade-in another device : Whats this modal
     * US349130 : MyTmo > Trade - in Redesign > Line selector > Expand line >
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
            Group.IOS})
    public void testWhatsThiModelInTradeInAnotherDeviceFlow(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case : Test Whats this modal Trade In Another Device Page");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log(
                "1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
        Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
        Reporter.log("3. Select Feature device on shop page | PDP Page should be displayed");
        Reporter.log("4. Select EIP from payment option and click on Add to cart after verify the installment due amount  | Line Selector page should be displayed");
        Reporter.log("5. Select additional user device on LS page | Trade in Phone Selection page should be displayed");
        Reporter.log("6. Click on Trade in another device link | Trade in another device page should be displayed");
        Reporter.log("7. Click on What is this| Find your device IMEI page should be display");
        Reporter.log("8. Verify Find your device IMEI page | Find your device IMEI page should be verified");

        Reporter.log("========================");
        Reporter.log("Actual Results");

        TradeInAnotherDevicePage tradeInAnotherDevicePage = navigateToTradeInAnotherDevicePageByFeaturedDevicesFLow(
                myTmoData, myTmoData.getDeviceName());
        tradeInAnotherDevicePage.clickWhatIsThisLink();
        tradeInAnotherDevicePage.verifyWhatIsThisModelTitle();
        tradeInAnotherDevicePage.verifyWhatIsThisModelHeader();
        tradeInAnotherDevicePage.verifyWhatIsThisModelBody();
        Assert.assertTrue(tradeInAnotherDevicePage.getNumberToDial().equalsIgnoreCase(ShopConstants.DIALTONUMBER_IMEI),
                ShopConstants.WHATISTHISMODEL_DIANIN_NUMBER_ERROR);
        Reporter.log("What is this model dial in number is displayed");
        tradeInAnotherDevicePage.clickWhatIsThisModelCloseButton();
        tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
    }
    
    /**
     * US156496 - Trade-in another device : Error Conditions
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
            Group.IOS})
    public void testIMEIErrorConditionsInTradeInAnotherDeviceFlow(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case : Verify possible error conditions");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log(
                "1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
        Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
        Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
        Reporter.log("4. Click on the device on Device PLP page |Device PDP page should be displayed ");
        Reporter.log(
                "5. Select EIP from payment option and click on Add to cart after verify the installment due amount  | Line Selector page should be displayed");
        Reporter.log("6. Select additional user device on LS page | Trade in Phone Selection page should be displayed");
        Reporter.log("7. Click on Trade in another device link | Trade in another device page should be displayed");
        Reporter.log(
                "8. Select Carrier, make, model,enter invalid IMEI number & click continue | IMEI invalid number message should be displayed");

        Reporter.log("========================");
        Reporter.log("Actual Results");

        TradeInAnotherDevicePage tradeInAnotherDevicePage = navigateToTradeInAnotherDevicePageByFeaturedDevicesFLow(
                myTmoData, myTmoData.getDeviceName());
        tradeInAnotherDevicePage.selectCarrier(ShopConstants.CARRIER_DROPDOWN_OPTION);
        tradeInAnotherDevicePage.selectMake(ShopConstants.MAKE_DROPDOWN_OPTION);
        tradeInAnotherDevicePage.selectModel(ShopConstants.MODEL_DROPDOWN_OPTION);
        tradeInAnotherDevicePage.setIMEINumber(ShopConstants.INVALID_IMEI_1);
        tradeInAnotherDevicePage.clickContinueButton();
     
        Assert.assertTrue(tradeInAnotherDevicePage.getIMEIErrorMessage().equalsIgnoreCase("INVALID IMEI NUMBER"),
                ShopConstants.INVALID_IMEI_ERROR_MESSAGE);
        Reporter.log("IMEI invalid number message is displayed");
    }

    /**
     * US154092 - Trade-in another device : Carrier, Make, Model - Display Rules
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
            Group.IOS})
    public void testCarrierMakeModelInTradeInAnotherDeviceFlow(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case : Verify Carrier, Make and Model drop down functionality");
        Reporter.log("Data Condition | PAH User");
        Reporter.log("Test Steps | L̥Expected Results:");
        Reporter.log("Data Condition   |STD PAH Customer ");
        Reporter.log(
                "1. Log in to the application with valid PAH Standared customer  | Logged in Succesfully and landed on home page ");
        Reporter.log("2. Click on Shop on home page | Shop page should be displayed");
        Reporter.log("3. Click on See all phones on shop page | Device PLP Page should be displayed");
        Reporter.log("4. Click on the device on Device PLP page |Device PDP page should be displayed ");
        Reporter.log("5. Select payment option and click on Add to cart | Line Selector page should be displayed");
        Reporter.log("6. Select additional user device on LS page | Trade in Phone Selection page should be displayed");
        Reporter.log("7. Click on Trade in another device link | Trade in another device page should be displayed");
        Reporter.log("8. Select carrier | Make drop down options should be populated");
        Reporter.log("9. Select make | Model drop down options should be populated");
        Reporter.log(
                "10. Select carrier option 'Select' | Make and model drop down options should be defaulted to select");
        Reporter.log("11. Select make option as 'Select' | Model drop down option should be defaulted to select");

        Reporter.log("========================");
        Reporter.log("Actual Results");

        TradeInAnotherDevicePage tradeInAnotherDevicePage = navigateToTradeInAnotherDevicePageByFeaturedDevicesFLow(
                myTmoData, myTmoData.getDeviceName());
        tradeInAnotherDevicePage.selectCarrier(myTmoData.getCarrier());
        tradeInAnotherDevicePage.verifyMakeDropdownOptions();
        tradeInAnotherDevicePage.selectMake(myTmoData.getMake());
        tradeInAnotherDevicePage.verifyModelDropdownOptions();
        tradeInAnotherDevicePage.selectModel(myTmoData.getModel());
        tradeInAnotherDevicePage.selectCarrier(ShopConstants.DEFAULT_DROPDOWN_OPTION);
        Assert.assertTrue(tradeInAnotherDevicePage.getMakeDropdownDefaultOption().equalsIgnoreCase(
                ShopConstants.DEFAULT_DROPDOWN_OPTION), ShopConstants.MAKE_DROPDOWN_DEFAULTOPTION_ERROR);
        Reporter.log("Make drop down default option is populated");
        Assert.assertTrue(tradeInAnotherDevicePage.getModelDropdownDefaultOption().equalsIgnoreCase(
                ShopConstants.DEFAULT_DROPDOWN_OPTION), ShopConstants.MODEL_DROPDOWN_DEFAULTOPTION_ERROR);
        Reporter.log("Model drop down default option is populated");
        tradeInAnotherDevicePage.selectCarrier(myTmoData.getCarrier());
        tradeInAnotherDevicePage.selectMake(myTmoData.getMake());
        tradeInAnotherDevicePage.selectModel(myTmoData.getModel());
        tradeInAnotherDevicePage.selectMake(ShopConstants.DEFAULT_DROPDOWN_OPTION);
        Assert.assertTrue(tradeInAnotherDevicePage.getModelDropdownDefaultOption().equalsIgnoreCase(
                ShopConstants.DEFAULT_DROPDOWN_OPTION), ShopConstants.MODEL_DROPDOWN_DEFAULTOPTION_ERROR);
        Reporter.log("Model drop down default option is populated");
    }


    /**
     * US485305 :AAL- Trade In Device Page
     *
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
            Group.IOS, Group.AAL_REGRESSION})
    public void testAuthorablePageTitleInTradeInAnotherDevicePage(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case :US485305 :AAL- Trade In Device Page");
        Reporter.log("Data Condition | PAH User with AAL Eligible");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Shop link | Shop page should be displayed");
        Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
        Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
        Reporter.log("7. Click Contune button | PLP page should be displayed ");
        Reporter.log("8. Select device | PDP page should be displayed ");
        Reporter.log("9. Click Contune button | Interstitial page should be displayed ");
        Reporter.log("10. Verify Trade-In tile 'Yes, I want to trade in a device' | Trade-In tile should be displayed");
        Reporter.log("11. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
        Reporter.log("12. Verify Authorable title 'Tell us about your trade-in' | Authorable title 'Tell us about your trade-in' should be displayed");
        Reporter.log("13. Select Carrier, make, model and enter valid IMEI no | Details should be entered");
        Reporter.log("14. Verify 'What is this?' link | 'What is this?' link should be displayed");
        Reporter.log("15. Click on 'What is this?' link | IMEI details model window should be displayed");
        Reporter.log("16.Verify Device IMEI no | Device IMEI no should be displayed in IMEI model window");
        Reporter.log("17. Click on IMEI model window X icon | IMEI details model window should be closed");

        Reporter.log("================================");
        Reporter.log("Actual Output:");

        navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);

        InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
        interstitialTradeInPage.verifyInterstitialPageTitle();
        interstitialTradeInPage.verifyYesTradeinTile();
        interstitialTradeInPage.clickOnYesWantToTradeInCTA();

        TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
        tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
        tradeInAnotherDevicePage.verifyTradeInAnotherDeviceTitle();
        tradeInAnotherDevicePage.selectCarrier(myTmoData.getCarrier());
        tradeInAnotherDevicePage.selectMake(myTmoData.getMake());
        tradeInAnotherDevicePage.selectModel(myTmoData.getModel());
        tradeInAnotherDevicePage.setIMEINumber(myTmoData.getiMEINumber());
        tradeInAnotherDevicePage.clickWhatIsThisLink();
        tradeInAnotherDevicePage.verifyWhatsThisModal();
        tradeInAnotherDevicePage.verifyDialNumber();
        tradeInAnotherDevicePage.clickWhatIsThisModelCloseButton();
        tradeInAnotherDevicePage.verifyWhatsThisModalNotDisplayed();
        tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();

    }


    /**
     * US489008 :show EIP balance amount - IMEI section
     * US485298 :Consolidated Charges Page to Interstitial page/TRADE IN (Non byod)
     * @param data
     * @param myTmoData
     */
    @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
            Group.IOS, Group.AAL_REGRESSION})
    public void testShowEIPbalanceAmount(ControlTestData data, MyTmoData myTmoData) {
        Reporter.log("Test Case :US489008 :show EIP balance amount - IMEI section");
        Reporter.log("Data Condition | PAH User with AAL Eligible");
        Reporter.log("Test Steps | Expected Results:");
        Reporter.log("================================");
        Reporter.log("1. Launch the application | Application Should be Launched");
        Reporter.log("2. Login to the application | User Should be login successfully");
        Reporter.log("3. Verify Home page | Home page should be displayed");
        Reporter.log("4. Click on Shop link | Shop page should be displayed");
        Reporter.log("5. Click on Add a Line button | Customer intent page should be displayed");
        Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page  should be displayed");
        Reporter.log("7. Click Contune button | PLP page should be displayed ");
        Reporter.log("8. Select device | PDP page should be displayed ");
        Reporter.log("9. Click Contune button | Interstitial page should be displayed ");
        Reporter.log("10. Verify Authorable Image | Authorable Image should be displayed ");
        Reporter.log("11. Verify Trade-In tile 'Yes, I want to trade in a device' | Trade-In tile should be displayed");
        Reporter.log("12. Click on 'Yes, I want to trade in a device' tile | Trade-in another device page should be displayed");
        Reporter.log("13. Select Carrier, make, model and enter same BAN IMEI no | Device Remaining  balance shoudl be displayed");
        Reporter.log("14. Verify EIP balance price | EIP balance price should be displayed");

        Reporter.log("================================");
        Reporter.log("Actual Output:");

        navigateToInterstitialTradeInPageFromSeeAllPhones(myTmoData);
        InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
        interstitialTradeInPage.verifyAuthorableTradeInImage();
        interstitialTradeInPage.verifyYesTradeinTile();
        interstitialTradeInPage.clickOnYesWantToTradeInCTA();
        TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
        tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
        tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
        tradeInAnotherDevicePage.clickContinueButton();
        tradeInAnotherDevicePage.verifyRemainingEIPBalanceMessage();
        tradeInAnotherDevicePage.verifyRemainingEIPBalanceMessageContainsPrice();

    }

}
