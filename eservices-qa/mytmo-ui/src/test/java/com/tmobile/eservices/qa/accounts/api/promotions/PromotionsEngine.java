package com.tmobile.eservices.qa.accounts.api.promotions;
//package com.tmobile.eservices.qa.accounts.api;
//
//import static org.hamcrest.CoreMatchers.equalTo;
//import static org.hamcrest.CoreMatchers.hasItem;
//import static org.hamcrest.CoreMatchers.hasItems;
//import static org.junit.Assert.assertThat;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.json.JSONObject;
//import org.testng.Assert;
//import org.testng.Reporter;
//import org.testng.annotations.Test;
//
//import com.tmobile.eservices.qa.commonlib.Group;
//
//import io.restassured.RestAssured;
//import io.restassured.path.json.JsonPath;
//import io.restassured.response.Response;
//import io.restassured.specification.RequestSpecification;
//
//public class OnDeviceFulFillment_TE {
//
//	private static final String Object = null;
//	private static final List res = null;
//	static Response response = null;
//
////	@Test( groups = "ondevicefullfillment", priority=1) 
////	public void post_OnDeviceFulFillment_ONEPLUSINTERNATIONALDATAPLAN(){
//
//	public static void main(String args[]) throws Exception {
//
//		RestAssured.baseURI = "https://eos.corporate.t-mobile.com";
//
//		RequestSpecification request = RestAssured.given();
//
//		request.header("Authorization", "01.USR.0Ox5rozfztHjBOTmr");
//		request.header("X-B3-TraceId", "123456787");
//		request.header("X-B3-SpanId", "123456787");
//		request.header("channel_id", "DESKTOP");
//		request.header("msisdn", "6057281418");
//		request.header("ban", "962383459");
//		request.contentType("application/json");
//
//		JSONObject requestParams = new JSONObject();
//		requestParams.put("accountNumber", "962383459");	
//		requestParams.put("msisdn", "6057281418");
//		requestParams.put("dataServiceSoc", "ALL");
//		requestParams.put("optionalServiceSocs", "ALL");
//		requestParams.put("dataPassSoc", "ALL");
//		requestParams.put("userRole", "PAH");
//		
//		
//		System.out.println(requestParams.toString());
//
//		request.body(requestParams.toString());
//
//		request.keyStore(
//				"/Users/sivachaitanyakoorapati/deveServicesQA_eservices_automation/eservices-qa/mytmo-ui/src/test/resources/cacerts",
//				"changeit");
//		request.trustStore("/Library/Java/JavaVirtualMachines/jdk1.8.0_131.jdk/Contents/Home/jre/lib/security/cacerts",
//				"changeit");
//		response = request.post("/v1/cpslookup/ondevicefulfillment");
//
//		int statusCode = response.getStatusCode();
//		System.out.println("The status code recieved: " + statusCode);
//		
//		Reporter.log("Test Case : Validating CPSLookUp Operation Response ");
//		Reporter.log("Note : CPSlookUp Service Internally calls getEligiblePasses,getEligibleServices,getEligibleDataServices ");
//		Reporter.log("Test data : TMO ONE POOLED Msisdn with  One Plus International Data plan");
//
//		// System.out.println("Response body: " + response.prettyPrint());
//		if (response.body().asString() != null && response.getStatusCode() == 200) {
//			System.out.println(response.jsonPath().getList("servicesList.planServiceDetails.description").size());
//			System.out.println("output Success");
//			
//		}
//		else {
//		Assert.fail("invalid response");
//		}
//				
//	}
//	
//	@Test( groups = "ondevicefullfillment", priority=2)
//	public void verifyRatePlanOfTheBan() {
//
//		Assert.assertTrue(response.jsonPath().getString("planSoc").contains("FRLTULF2"),"Rate Plan Incorrect");
//	}
//	
//	@Test( groups = "ondevicefullfillment", priority=2)
//	public void verifyCurrentDataServiceDetails() {
//
//		Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.soc").contains("FRTMO1PLS"));
//		Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.name").contains("ONE Plus International"));
//		Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.price").contains("25"));
//		Assert.assertTrue(response.jsonPath().getString("currentDataServiceDetails.taxTreatment").contains("TI"));
//	}
//	
//	@Test( groups = "ondevicefullfillment", priority=2)
//	public void verifyAllCurrentActiveService() {
//
//		assertThat(response.jsonPath().getList("allCurrentServiceDetails.soc"), hasItems("FAMNFX13", "PHPT1", "FRTMO1PLS", "WEBGRDCH"));
//	}
//	
//	@Test( groups = "ondevicefullfillment", priority=2)
//	public void verifyELigibleDataPasses() {
//
//		List res = response.path("dataPassList.soc");
//		Assert.assertTrue(res.size() == 3);
//		assertThat(response.jsonPath().getList("dataPassList.soc"), hasItems("HDPASS0", "NAT7DAY", "SNPASS24"));
//	}
//
//	@Test( groups = "ondevicefullfillment", priority=2)
//	public void verifyAllAvailableDataServices() {
//
//		assertThat(response.jsonPath().getList("dataServiceList.soc"), hasItems("NODATA", "FRLTDATA", "TM1PLS2", "FRTMO1PLS"));
//		assertThat(response.jsonPath().getList("dataServiceList.name"), hasItems("ONE Plus International", "ONE Plus", "T-Mobile ONE", "No Data"));
//	}
//	
//	@Test( groups = "ondevicefullfillment", priority=2)
//	public void verifyAllAvailableSharedServices() {
//
//		assertThat(response.jsonPath().getList("sharedServicesList.soc"), hasItems("FAMALL10", "FAMLYMODE", "FAMNFX9", "FAMNFX13"));
//		assertThat(response.jsonPath().getList("sharedServicesList.name"), hasItems("Family Allowances", "Family Mode", "Netflix Standard $10.99 with Fam Allowances ($16 value)", "Netflix Premium $13.99 with Fam Allowances ($19 value)"));
//	}
//	
//	@Test( groups = "ondevicefullfillment", priority=2)
//	public void verifyServicesListDescriptionisPresent() {
//
//		Assert.assertNotNull(response.jsonPath().getList("servicesList.planServiceDetails.description"));
//	}
//	
//	@Test( groups = "ondevicefullfillment", priority=2)
//	public void verifyDataSericesListDescriptionisPresent() {
//
//		Assert.assertNotNull(response.jsonPath().getList("dataServiceList.planServiceDetails.description"));
//	}
//	
//	@Test( groups = "ondevicefullfillment", priority=2)
//	public void verifysharedServicesListDescriptionisPresent() {
//
//		Assert.assertNotNull(response.jsonPath().getList("sharedServicesList.planServiceDetails.description"));
//	}
//	
//	@Test( groups = "ondevicefullfillment", priority=2)
//	public void verifydataPassListDescriptionIsPresent() {
//
//		Assert.assertNotNull(response.jsonPath().getList("dataPassList.planServiceDetails.description"));
//	}
//	
//	@Test( groups = "ondevicefullfillment", priority=2)
//	public void verifygroupedServicesListDescriptionIsNotNull() {
//
//		Assert.assertNotNull(response.jsonPath().getList("groupedServicesList.planServiceDetails.description"));
//	}
//	
//}