package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.NotifyEipAgreement;

import io.restassured.response.Response;

public class NotifyEipAgreementTest extends NotifyEipAgreement {

	public Map<String, String> tokenMap;

	/**
	 * /** UserStory# US602669: Validate EIP Team of eSignature Completion; U
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "bhavya" })
	public void testVerifyEipSignature(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testVerifyEipSignature");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with Notify API.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testVerifyEipSignature";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("VerifyEipSignature.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);

		Response response = verifyEipSignature(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

			System.out.println(response.prettyPrint());
			logSuccessResponse(response, operationName);

		} else {

			failAndLogResponse(response, operationName);
		}
	}
}
