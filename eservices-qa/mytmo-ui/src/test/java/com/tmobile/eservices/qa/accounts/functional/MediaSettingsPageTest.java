package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.accounts.MediaSettingsPage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;

/**
 * @author rnallamilli
 * 
 */
public class MediaSettingsPageTest extends GlobalCommonLib {

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testBingeOnToggleInProfile(ControlTestData controlTestData, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with BingeOn profie");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile Link |  Profile page should be displayed");
		Reporter.log("5. Click on Media Settings link | Media Settings should be displayed");
		Reporter.log("6. verify BingeOn header | BingeOn header  should be displayed");
		Reporter.log("6. Toggle between Binge On/Off | BingeOn should be successfully toggled On/Off");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		MediaSettingsPage mediaSettingsPage = navigateToMediaSettingsPage(myTmoData, "Media Settings");
		mediaSettingsPage.verifyBingeOnHeader();
		mediaSettingsPage.clickBingeOnToggleAndVerifyBingeOnButtonOnOrOff();
	}

	/**
	 * US543629:MyTMO | Profile | Blocking Controls | HD Video Config US549927:MyTMO
	 * | Profile | Blocking Controls | HD Video Config | Testing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void testHDVideoToggleInProfile(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with HDVideo profile");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile Link |  Profile page should be displayed");
		Reporter.log("5. Click on Media Settings link | Media Settings should be displayed");
		Reporter.log("6. Verify HDVideo header| HDVideo header  should be displayed");
		Reporter.log("7. Verify HD video toggle status| HD Video toggle should be disabled");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		MediaSettingsPage mediaSettingsPage = navigateToMediaSettingsPage(myTmoData, "Media Settings");
		mediaSettingsPage.verifyHDVideoHeader();
		mediaSettingsPage.verifyHDVideoToggleDisableStatus();
	}

	/**
	 * US289347 (M&D) - Offer - Goat fell rate plan suppression
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testHDSupressedInProfilePageForGoatFellPlan(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with media settings blade");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1: Navigate to My T-Mobile.com | Login page should be displayed");
		Reporter.log("2: Login as a MyTMO Customer | Home page should be displayed");
		Reporter.log("3: Click On Profile link| Profile page should be displayed");
		Reporter.log(
				"4. Check  Media Settings Link is Not present | HD option & Media Settings Link Should not be present for Goat fell Plan Customer:");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		MediaSettingsPage mediaSettingsPage = navigateToMediaSettingsPage(myTmoData, "Media Settings");
		mediaSettingsPage.verifyMediaSettingsLinkNotDisplayedonProfilePage();
	}

	/**
	 * US385515:MyTMO - BingeON Toggle | Incorporate WPC flag into logic Testing
	 * efforts
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testBingeOnEnabledCustomerProfile(ControlTestData controlTestData, MyTmoData myTmoData) {
		Reporter.log("Test Case : US385515:BingeON Toggle | Incorporate WPC flag into logic Testing efforts");
		Reporter.log("================================");

		Reporter.log("Test Data Conditions: Msisdn configured with BingeOn profie");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile Link |  Profile page should be displayed");
		Reporter.log("5. Click on Media Settings link | Media Settings should be displayed");
		Reporter.log("6. verify BingeOn header | BingeOn header  should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		MediaSettingsPage mediaSettingsPage = navigateToMediaSettingsPage(myTmoData, "Media Settings");
		mediaSettingsPage.verifyMediaSettingsPage();
		mediaSettingsPage.verifyBingeOnHeader();
	}

	/**
	 * US385515:MyTMO - BingeON Toggle | Incorporate WPC flag into logic Testing
	 * efforts
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void testBingeOnDisabledCustomerProfile(ControlTestData controlTestData, MyTmoData myTmoData) {
		Reporter.log("Test Case : US385515:BingeON Toggle | Incorporate WPC flag into logic Testing efforts");
		Reporter.log("================================");

		Reporter.log("Test Data Conditions: Msisdn configured with BingeOn profie");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile Link |  Profile page should be displayed");
		Reporter.log("5. Click on Media Settings link | Media Settings should be displayed");
		Reporter.log(
				"6: Click on profile bread crumb and verify Profile bread crumb active status | Profile bread crumb should be active");
		Reporter.log("7: Verify profile page | Profile page should be displayed");
		Reporter.log("8. Click on Media Settings link | Media Settings should be displayed");
		Reporter.log("9. verify BingeOn header | BingeOn header  should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		MediaSettingsPage mediaSettingsPage = navigateToMediaSettingsPage(myTmoData, "Media Settings");
		mediaSettingsPage.clickProfileHomeBreadCrumb();

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
		profilePage.clickProfilePageLink("Media Settings");
		mediaSettingsPage.verifyBingeOnHeaderForDisabledCustomer();
	}

	/**
	 * US327336/US454528:Profile | Re-Launch Enhancements | Rename Home |
	 * Breadcrumbs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.ACCOUNTS, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.PROFILE })
	public void verifyMediaSettingsBreadCrumbsLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with BingeOn profie");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile Link |  Profile page should be displayed");
		Reporter.log("5. Click on Media Settings link | Media Settings should be displayed");
		Reporter.log(
				"6: Click on profile bread crumb and verify Profile bread crumb active status | Profile bread crumb should be active");
		Reporter.log("7: Verify profile page | Profile page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Result:");

		MediaSettingsPage mediaSettingsPage = navigateToMediaSettingsPage(myTmoData, "Media Settings");
		mediaSettingsPage.clickProfileHomeBreadCrumb();

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.FLEX })
	public void testHDVideoToggleForTM1PLS2DataPlan(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with HDVideo profile");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile Link |  Profile page should be displayed");
		Reporter.log("5. Click on Media Settings link | Media Settings should be displayed");
		Reporter.log("6. verify HDVideo header| HDVideo header  should be displayed");
		Reporter.log("6. Toggle between HD Video toggle| HD Video toggle should be successfully toggled On/Off");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		MediaSettingsPage mediaSettingsPage = navigateToMediaSettingsPage(myTmoData, "Media Settings");
		mediaSettingsPage.verifyHDVideoHeader();
		mediaSettingsPage.clickHDVideoToggleAndVerifyHDVideoOnOrOff();
	}

}
