package com.tmobile.eservices.qa.shop.npi;

import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.shop.*;

import io.appium.java_client.ios.IOSDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class StandardUpgradeValidator extends ShopCommonLib {

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular1" })
	public void testStandardUpgradeSkipTradeInForNewSkus3(MyTmoData myTmoData) {
		standardUpgradeSkipTradeIn(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeSkipTradeInForNewSkus7(MyTmoData myTmoData) {
		standardUpgradeSkipTradeIn(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeSkipTradeInForNewSkus11(MyTmoData myTmoData) {
		standardUpgradeSkipTradeIn(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeSkipTradeInForNewSkus15(MyTmoData myTmoData) {
		standardUpgradeSkipTradeIn(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeSkipTradeInForNewSkus19(MyTmoData myTmoData) {
		standardUpgradeSkipTradeIn(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeSkipTradeInForNewSkus23(MyTmoData myTmoData) {
		standardUpgradeSkipTradeIn(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeSkipTradeInForNewSkus27(MyTmoData myTmoData) {
		standardUpgradeSkipTradeIn(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeSkipTradeInForNewSkus31(MyTmoData myTmoData) {
		standardUpgradeSkipTradeIn(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeSkipTradeInForNewSkus35(MyTmoData myTmoData) {
		standardUpgradeSkipTradeIn(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeSkipTradeInForNewSkus39(MyTmoData myTmoData) {
		standardUpgradeSkipTradeIn(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular1" })
	public void testStandardUpgradeWithTradeInForNewSkus2(MyTmoData myTmoData) {
		standardUpgradeWithTradeInAndPHPWithAccessory(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeWithTradeInForNewSkus6(MyTmoData myTmoData) {
		standardUpgradeWithTradeInAndPHPWithAccessory(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeWithTradeInForNewSkus10(MyTmoData myTmoData) {
		standardUpgradeWithTradeInAndPHPWithAccessory(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeWithTradeInForNewSkus14(MyTmoData myTmoData) {
		standardUpgradeWithTradeInAndPHPWithAccessory(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeWithTradeInForNewSkus18(MyTmoData myTmoData) {
		standardUpgradeWithTradeInAndPHPWithAccessory(myTmoData);
	}

	// make these tests TradeIn Another Flow
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeWithTradeInAnotherDeviceForNewSku22(MyTmoData myTmoData) {
		standardUpgradeWithTradeInAnotherDevice(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeWithTradeInAnotherDeviceForNewSku26(MyTmoData myTmoData) {
		standardUpgradeWithTradeInAnotherDevice(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeWithTradeInAnotherDeviceForNewSku30(MyTmoData myTmoData) {
		standardUpgradeWithTradeInAnotherDevice(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeWithTradeInAnotherDeviceForNewSku34(MyTmoData myTmoData) {
		standardUpgradeWithTradeInAnotherDevice(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeWithTradeInAnotherDeviceForNewSku38(MyTmoData myTmoData) {
		standardUpgradeWithTradeInAnotherDevice(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "regular" })
	public void testStandardUpgradeWithTradeInAnotherDeviceForNewSku42(MyTmoData myTmoData) {
		standardUpgradeWithTradeInAnotherDevice(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "mobile" })
	public void testStandardUpgradeSkipTradeInForNewSkuInMobile(MyTmoData myTmoData) {
		standardUpgradeSkipTradeIn(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "mobile" })
	public void testStandardUpgradeWithTradeInForNewSkuInMobile(MyTmoData myTmoData) {
		standardUpgradeWithTradeInAndPHPWithAccessory(myTmoData);
	}

	public void standardUpgradeSkipTradeIn(MyTmoData myTmoData) {
		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyDeviceNameAndManufacturerName(myTmoData.getDeviceName());
		pdpPage.selectMemory(myTmoData.getDeviceMemory());
		pdpPage.chooseColor(myTmoData.getDeviceColor());
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectorDetailsPage();
		lineSelectorDetailsPage.clickOnSkipTradeInLink();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.clickPayMonthlyPopupCloseButton();
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();

		Double estimateDueTodayPrice = cartPage.getEstDueTodayCart();
		Reporter.log("Estimated Due today in cart page is Verified");
		Double estimateShippingPrice = cartPage.verifyAndGetEstimateShippingPrice();
		Reporter.log("Estimated Shipping Price in cart page is Verified");
		Double estimateSalesTaxPrice = cartPage.verifyAndGetEstimateSalexTaxPrice();
		Reporter.log("Estimated Due Sales Tax in cart page is Verified");
		Double dueTodayTotal = cartPage.getEstTotalDueTodayPriceOrderDetailsCartPage();
		Reporter.log("Estimated Due today in cart page is Verified");
		Double totalPrice = estimateDueTodayPrice + estimateShippingPrice + estimateSalesTaxPrice;
		cartPage.verifyTotalDueTodayPrices(dueTodayTotal, totalPrice);

		cartPage.clickContinueToShippingButton();
		cartPage.verifyUPSGroungShippingMethodOption();
		cartPage.verifyTwoDayShippingMethodOption();
		cartPage.verifyNextDayDeliveryBtn();
		cartPage.clickNextDayDeliveryBtn();

		if (getDriver() instanceof IOSDriver) {
			Reporter.log("Navigating to Payments page");
		} else {
			cartPage.clickEditShippingAddress();
			cartPage.fillWAAddressInfo();
		}
		cartPage.clickContinueToPaymentButton();
	//	cartPage.verifySalesTaxAmountForORInPaymentsTab();

		Double dueTodayOnCartPaymentPage = cartPage.getDueTodayCartPaymentPage();
		Double shippingAmountOnCartPaymentPage = cartPage.verifyAndGetEstimateShippingPricePaymentPage();
		Double salesTaxOnCartPaymentPage = cartPage.verifyAndGetEstimateSalexTaxPricePaymentPage();
		Double totalDuePrice = dueTodayOnCartPaymentPage + shippingAmountOnCartPaymentPage + salesTaxOnCartPaymentPage;
		Double totalDueTodayPaymentPage = cartPage.verifyAndGetDueTodayTotalPaymentPage();
		cartPage.verifyTotalDueTodayPrices(totalDueTodayPaymentPage, totalDuePrice);
		cartPage.clickBillingAndShippingCheckBox();
		cartPage.fillBillingAddressInfo();
		if (getDriver() instanceof IOSDriver) {
			Reporter.log("Standard upgrade with skip trade In flow completed successfully");
		} else {
			cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
			cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
			cartPage.enterCVV(myTmoData.getPayment().getCvv());
			Reporter.log("Added credit card details");
		}
		
        cartPage.clickAcceptAndPlaceOrder();
		ESignaturePage esignaturePage = new ESignaturePage(getDriver());
        esignaturePage.verifyESignaturePage();
        esignaturePage.selectESignatureCheckBox();
        esignaturePage.clickESignatureContinueCTA();
        esignaturePage.clickSignatureImage();
        esignaturePage.clickSelectStyleTab();
        esignaturePage.clickAdoptSignatureCTA();
        esignaturePage.clickSendTheOrderCTA();
        OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
        orderConfirmationPage.verifyOrderConfirmationPage();
        orderConfirmationPage.verifyOrderNumber();
        orderConfirmationPage.getOrderNumber();
        CommonPage commonPage = new CommonPage(getDriver());
		commonPage.takeScreenshot();
		
	}

	public void standardUpgradeWithTradeInAndPHPWithAccessory(MyTmoData myTmoData) {
		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyDeviceNameAndManufacturerName(myTmoData.getDeviceName());
		pdpPage.selectMemory(myTmoData.getDeviceMemory());
		pdpPage.chooseColor(myTmoData.getDeviceColor());
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		lineSelectorDetailsPage.clickOnTradeInThisPhone();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInConfirmationPage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInConfirmationPage.verifyTradeInDeviceConditionConfirmationPage();
		tradeInConfirmationPage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionURL();
		deviceProtectionPage.clickContinueButton();
		
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.selectNumberOfAccessoriesMoreThan49();
		accessoryPLPPage.clickContinueButton();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();

		Double estimateDueTodayPrice = cartPage.getEstDueTodayCart();
		Reporter.log("Estimated Due today in cart page is Verified");
		Double estimateShippingPrice = cartPage.verifyAndGetEstimateShippingPrice();
		Reporter.log("Estimated Shipping Price in cart page is Verified");
		Double estimateSalesTaxPrice = cartPage.verifyAndGetEstimateSalexTaxPrice();
		Reporter.log("Estimated Due Sales Tax in cart page is Verified");
		Double dueTodayTotal = cartPage.getEstTotalDueTodayPriceOrderDetailsCartPage();
		Reporter.log("Estimated Due today in cart page is Verified");
		/*Double totalPrice = estimateDueTodayPrice + estimateShippingPrice + estimateSalesTaxPrice;
		cartPage.verifyTotalDueTodayPrices(dueTodayTotal, totalPrice);*/

		cartPage.clickContinueToShippingButton();
		cartPage.verifyUPSGroungShippingMethodOption();
		cartPage.verifyTwoDayShippingMethodOption();
		cartPage.verifyNextDayDeliveryBtn();
		cartPage.clickNextDayDeliveryBtn();

		if (getDriver() instanceof IOSDriver) {
			Reporter.log("Navigating to Payments page");
		} else {
			cartPage.clickEditShippingAddress();
			cartPage.fillWAAddressInfo();
		}
		cartPage.clickContinueToPaymentButton();

		Double dueTodayOnCartPaymentPage = cartPage.getDueTodayCartPaymentPage();
		Double shippingAmountOnCartPaymentPage = cartPage.verifyAndGetEstimateShippingPricePaymentPage();
		Double salesTaxOnCartPaymentPage = cartPage.verifyAndGetEstimateSalexTaxPricePaymentPage();
		Double totalDuePrice = dueTodayOnCartPaymentPage + shippingAmountOnCartPaymentPage + salesTaxOnCartPaymentPage;
		Double totalDueTodayPaymentPage = cartPage.verifyAndGetDueTodayTotalPaymentPage();
		cartPage.verifyTotalDueTodayPrices(totalDueTodayPaymentPage, totalDuePrice);
		cartPage.clickBillingAndShippingCheckBox();
		cartPage.fillBillingAddressInfo();
		if (getDriver() instanceof IOSDriver) {
			Reporter.log("Standard upgrade with trade In flow completed successfully");
		} else {
			cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
			cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
			cartPage.enterCVV(myTmoData.getPayment().getCvv());
			Reporter.log("Credit card details added");
		}
		
		cartPage.clickAcceptAndPlaceOrder();
		ESignaturePage esignaturePage = new ESignaturePage(getDriver());
        esignaturePage.verifyESignaturePage();
        esignaturePage.selectESignatureCheckBox();
        esignaturePage.clickESignatureContinueCTA();
        esignaturePage.clickSignatureImage();
        esignaturePage.clickSelectStyleTab();
        esignaturePage.clickAdoptSignatureCTA();
        esignaturePage.clickSendTheOrderCTA();
        OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
        orderConfirmationPage.verifyOrderConfirmationPage();
        orderConfirmationPage.verifyOrderNumber();
        orderConfirmationPage.getOrderNumber();
        CommonPage commonPage = new CommonPage(getDriver());
		commonPage.takeScreenshot();
		 
	}

	public void standardUpgradeWithTradeInAnotherDevice(MyTmoData myTmoData) {
		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyDeviceNameAndManufacturerName(myTmoData.getDeviceName());
		pdpPage.selectMemory(myTmoData.getDeviceMemory());
		pdpPage.chooseColor(myTmoData.getDeviceColor());
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		lineSelectorDetailsPage.clickWantToTradeDifferentPhoneLink();

		PhoneSelectionPage phoneSelectionPage = new PhoneSelectionPage(getDriver());
		phoneSelectionPage.verifyPhoneSelectionPage();
		phoneSelectionPage.clickGotADifferentPhone();

		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.selectCarrier(myTmoData.getCarrier());
		tradeInAnotherDevicePage.selectMake(myTmoData.getMake());
		tradeInAnotherDevicePage.selectModel(myTmoData.getModel());
		tradeInAnotherDevicePage.setIMEINumber(myTmoData.getiMEINumber());
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.clickFindMyIphoneYesButton();
		tradeInDeviceConditionValuePage.clickAcceptableLCDYesButton();
		tradeInDeviceConditionValuePage.clickWaterDamageNoButton();
		tradeInDeviceConditionValuePage.clickDevicePowerONYesButton();
		tradeInDeviceConditionValuePage.clickContinueCTANew();
		TradeInDeviceConditionConfirmationPage tradeInConfirmationPage = new TradeInDeviceConditionConfirmationPage(
				getDriver());
		tradeInConfirmationPage.clickOnTradeInThisPhoneButton();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionURL();
		deviceProtectionPage.clickContinueButton();
		AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		accessoryPLPPage.verifyAccessoryPLPPage();
		accessoryPLPPage.clickSkipAccessoriesCTA();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();

		Double estimateDueTodayPrice = cartPage.getEstDueTodayCart();
		Reporter.log("Estimated Due today in cart page is Verified");
		Double estimateShippingPrice = cartPage.verifyAndGetEstimateShippingPrice();
		Reporter.log("Estimated Shipping Price in cart page is Verified");
		Double estimateSalesTaxPrice = cartPage.verifyAndGetEstimateSalexTaxPrice();
		Reporter.log("Estimated Due Sales Tax in cart page is Verified");
		Double dueTodayTotal = cartPage.getEstTotalDueTodayPriceOrderDetailsCartPage();
		Reporter.log("Estimated Due today in cart page is Verified");
		/*Double totalPrice = estimateDueTodayPrice + estimateShippingPrice + estimateSalesTaxPrice;
		cartPage.verifyTotalDueTodayPrices(dueTodayTotal, totalPrice);*/

		cartPage.clickContinueToShippingButton();
		cartPage.verifyUPSGroungShippingMethodOption();
		cartPage.verifyTwoDayShippingMethodOption();
		cartPage.verifyNextDayDeliveryBtn();
		cartPage.clickNextDayDeliveryBtn();

		if (getDriver() instanceof IOSDriver) {
			Reporter.log("Navigating to Payments page");
		} else {
			cartPage.clickEditShippingAddress();
			cartPage.fillWAAddressInfo();
		}
		cartPage.clickContinueToPaymentButton();

		Double dueTodayOnCartPaymentPage = cartPage.getDueTodayCartPaymentPage();
		Double shippingAmountOnCartPaymentPage = cartPage.verifyAndGetEstimateShippingPricePaymentPage();
		Double salesTaxOnCartPaymentPage = cartPage.verifyAndGetEstimateSalexTaxPricePaymentPage();
		Double totalDuePrice = dueTodayOnCartPaymentPage + shippingAmountOnCartPaymentPage + salesTaxOnCartPaymentPage;
		Double totalDueTodayPaymentPage = cartPage.verifyAndGetDueTodayTotalPaymentPage();
	/*	cartPage.verifyTotalDueTodayPrices(totalDueTodayPaymentPage, totalDuePrice);*/

		if (getDriver() instanceof IOSDriver) {
			Reporter.log("Standard upgrade with trade In flow completed successfully");
		} else {
			cartPage.enterFirstname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterLastname(myTmoData.getPayment().getNameOnCard());
			cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
			cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
			cartPage.enterCVV(myTmoData.getPayment().getCvv());
			Reporter.log("Standard upgrade with trade In flow completed successfully");
		}
		/*
		 * cartPage.placeOrderCTAEnabled(); cartPage.clickAcceptAndPlaceOrder();
		 */
		/*
		 * OrderConfirmationPage orderConfirmationPage = new
		 * OrderConfirmationPage(getDriver());
		 * orderConfirmationPage.verifyOrderConfirmationPage();
		 */
	}

}
