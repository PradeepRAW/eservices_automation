/**
 * 
 */
package com.tmobile.eservices.qa.payments.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.EIPDetailsPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentForwardPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentReviewCCPage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;
import com.tmobile.eservices.qa.payments.PaymentConstants;

/**
 * @author Sam Iurkov
 *
 */
public class EIPPaymentReviewCCPageTest extends PaymentCommonLib {

	/*	*//**
			 * US140907 Update fields on EIP details page, EIP payment credit
			 * card page, EIP review payment page, EIP payment confirmation
			 * page.
			 * 
			 * @param data
			 * @param myTmoData
			 */
	/*
	 * @Test(dataProvider = "byColumnName", enabled = true, groups = {
	 * Group.PAYMENTS, Group.DESKTOP, Group.ANDROID, Group.IOS }) public void
	 * eipPlanIdFieldUpdate(ControlTestData data, MyTmoData myTmoData) {
	 * logger.info("eipPlanIdFieldUpdate method called in EservicesPaymentTest"
	 * ); Reporter.log("Test Case : 'EIP Plan ID' authoring validation");
	 * Reporter.log("================================"); Reporter.log(
	 * "Test Steps | Expected Results:"); Reporter.log(
	 * "1. Launch the application | Application Should be Launched");
	 * Reporter.log(
	 * "2. Login to the application | User Should login successfully");
	 * Reporter.log("3. Verify Home page | Home page should be displayed");
	 * Reporter.log(
	 * "4. Click on billing link | Billing summary page should be displayed");
	 * Reporter.log(
	 * "5. Verify 'Installment Plan' section| 'Installment Plan' section should be displayed"
	 * ); Reporter.log(
	 * "6. Click on 'View details' link in Installment Plan section| View Details link in Installment Plan section should be displayed and clickable"
	 * ); Reporter.log(
	 * "7. Verify Installment Plans page for 'EIP Plan ID' text displayed| 'EIP Plan ID' text should be displayed in Installment Plans page"
	 * ); Reporter.log(
	 * "8. Click on 'Make a Payment' link in Installment Plans page| Make a Payment link should be displayed and clickable in Installment Plans page"
	 * ); Reporter.log(
	 * "9. Verify 'EIP Plan ID' text displayed in the Payment Page under 'Payment towards' section| 'EIP Plan ID' text should be displayed in the Payment Page under 'Payment towards' section"
	 * ); Reporter.log(
	 * "10. Enter new Card payment details and click on Next| User should be able to enter card details and click on Next"
	 * ); Reporter.log(
	 * "11. Verify 'EIP Plan ID' text displayed in the Review device payment page under 'Payment towards' section| 'EIP Plan ID' text should be displayed in the Review device payment Page under 'Payment towards' section"
	 * ); Reporter.log("========================"); Reporter.log(
	 * "Actual Results");
	 * 
	 * EIPDetailsPage eIPDetailsPage = navigateToEIPDetailsPage(myTmoData);
	 * eIPDetailsPage.verifyEIPPlanIdDisplayed();
	 * eIPDetailsPage.clickMakePayment(); EIPPaymentForwardPage eipPaymentPage =
	 * new EIPPaymentForwardPage(getDriver());
	 * eipPaymentPage.verifyPageLoaded();
	 * eipPaymentPage.enterPaymentamount(myTmoData.getPayment().getAmount());
	 * eipPaymentPage.clickNewRadioButton();
	 * 
	 * eipPaymentPage.fillPaymentInfo(myTmoData.getPayment(), "Card");
	 * eipPaymentPage.clickNextButton(); EIPPaymentReviewCCPage
	 * eIPPaymentReviewCCPage = new EIPPaymentReviewCCPage(getDriver());
	 * eIPPaymentReviewCCPage.verifyPageLoaded();
	 * eIPPaymentReviewCCPage.verifyEIPPlanIdDisplayed();
	 * eIPPaymentReviewCCPage.checkCreditCardTermsAndConditions(); }
	 * 
	 *//**
		 * Making EQUIPMENT INSTALLMENT PLAN partial Payment with STORED CREDIT
		 * CARD
		 * 
		 * @param data
		 * @param myTmoData
		 *//*
		 * @Test(dataProvider = "byColumnName", enabled = true, groups =
		 * {Group.PAYMENTS,Group.DESKTOP,Group.ANDROID,Group.IOS}) public void
		 * verifyEIPpartialPayment(ControlTestData data, MyTmoData myTmoData) {
		 * logger.info(
		 * "verifyEIPpartialPayment method called in BillingSummaryTest");
		 * Reporter.log(
		 * "Test Case : Making EQUIPMENT INSTALLMENT PLAN partial Payment with STORED CREDIT CARD"
		 * ); Reporter.log("================================"); Reporter.log(
		 * "Test Steps | Expected Results:"); Reporter.log(
		 * "1. Launch the application | Application Should be Launched");
		 * Reporter.log(
		 * "2. Login to the application | User Should be login successfully");
		 * Reporter.log("3. Verify Home page | Home page should be displayed");
		 * Reporter.log(
		 * "4. Click on Billing link | Billing summary page should be displayed"
		 * ); Reporter.log(
		 * "5. Verify EIP section | EIP section deatils should display");
		 * Reporter.log(
		 * "6. Verify View details section | View details section should be displayed"
		 * ); Reporter.log(
		 * "7. Click on View details | EIP details page should be displayed");
		 * Reporter.log(
		 * "8. Click Make Payment | Extra device payment should be displayed");
		 * Reporter.log(
		 * "9. Enter Payment Amount | Payment amount should be entered correctly"
		 * ); Reporter.log(
		 * "10. Select Stored Payment Radio button and Click Next button | Review Device payment should be displayed"
		 * ); Reporter.log(
		 * "11. Click on Terms And Conditions checkbox | Submit button should be enabed."
		 * ); Reporter.log(
		 * "12. Click Submit Button | Payment Confirmation message should be displayed"
		 * );
		 * 
		 * Reporter.log("================================"); Reporter.log(
		 * "Actual Test Tests");
		 * 
		 * EIPDetailsPage eIPDetailsPage = navigateToEIPDetailsPage(myTmoData);
		 * eIPDetailsPage.clickMakepayment(); EIPPaymentForwardPage
		 * eipPaymentPage = new EIPPaymentForwardPage(getDriver());
		 * eipPaymentPage.verifyPageLoaded();
		 * eipPaymentPage.enterPaymentamount(myTmoData.getPayment().getAmount())
		 * ; if (!eipPaymentPage.verifySavedCCisChecked()) {
		 * eipPaymentPage.fillPaymentInfo(myTmoData.getPayment(), "Card"); }
		 * eipPaymentPage.clickNextButton();
		 * eIPDetailsPage.verifyReviewdevicepaymentDisplayed();
		 * eIPDetailsPage.clickTermsandConditions();
		 * eIPDetailsPage.clickSubmitbtn(); EIPPaymentReviewCCPage
		 * reviewPaymentPage = new EIPPaymentReviewCCPage(getDriver());
		 * Assert.assertTrue(reviewPaymentPage.verifyEIPPartialPayemntSuccess(),
		 * "EIP Partial Payment - Test FAILED.");
		 * Assert.assertFalse(reviewPaymentPage.verifyPayemntdeclineDialogbox(),
		 * "EIP Partial Payment - Test FAILED."); logStep(StepStatus.PASS,
		 * Constants.PAYMENT_DECLINE_DISPALYED, getDriver().getTitle() +
		 * Constants.VERIFY_LANDING_PAGE); Reporter.log(
		 * "Payment Declined dialog box is displayed"); }
		 */

	/**
	 * US357215 EIP Payment Failure > Failure Modal Link Logic 
	 * US357216 EIP Payment Failure > Failure Modal Link Redirect
	 * 
	 * Flow: Regular EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentReturnToShopDisplayViewEIPDetailsLinkForRegularEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US357215 | US357216 EIP Payment Failure > Failure Modal Link Redirect");
		Reporter.log("Data Condition | JUMP2.0 customer who has not paid 50% of eip amount");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on any Featured Phone | Line Selector page should be displayed");
		Reporter.log("6. Click on Line eligible for EIP Payment | EIP payment page should be displayed");
		Reporter.log("7. Enter payment details and submit | Payment Failed modal should be displayed");
		Reporter.log("8. Click 'try again' link on Payment failed modal | EIP Payment page should be displayed");

		//TODO: Change navigation path from Regular flow to JUMP Shop flow 
		EIPPaymentReviewCCPage reviewCCPage = navigateToEIPPaymentReviewCCPage(myTmoData, "Card");
		reviewCCPage.clickCCTermsAndConditions();
		reviewCCPage.clickCreditCardSubmitButton();
		reviewCCPage.verifyPaymentFailedModal();
		reviewCCPage.clickPaymentFailedModal();
		EIPDetailsPage eIPDetailsPage = new EIPDetailsPage(getDriver());
		eIPDetailsPage.verifyPageLoaded();
		
		//TODO: verify read only fields and continue payment process
	}
	
	/*
	 * US356347  PII Masking > Variables on EIP Page
	 * US407816	PII masking missing for EIP Forward page
	 * 
	 * Flow: Regular EIP
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testPIIMaskingEIPpagesForRegularEIP(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("US356347 | US356347 PII Masking > Variables on EIP Page");
		Reporter.log("Data Condition | EIP Eligible");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Billing link | Billing summary page should be displayed");
		Reporter.log(
				"5.Verify Equipment Installation Plan Section | Equipment Installation Plan Section should be loaded ");
		Reporter.log("6. Click on Installment Plan blade | EIP details page should be displayed");
		Reporter.log("7. Verify PII Masking on EIP Details Page | Mobile Number and IMEI number PID's present");
		Reporter.log("8. Click on make payment button | EIP payment page should be displayed");
		Reporter.log("9. Enter Amount and Click on Next Button | EIP payment review page should be displayed");
		Reporter.log(
				"10. Verify PII Masking on EIP payment Page | Mobile Number,Customer Name,Card Info,Zip Code and BAN should be present");

		EIPDetailsPage eipDetailsPage = navigateToEIPDetailsPage(myTmoData);
		eipDetailsPage.verifyPIIPhoneNumber(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
		eipDetailsPage.verifyPIIImeiNumber(PaymentConstants.PII_CUSTOMER_IMEI_PID);
		eipDetailsPage.clickMakePayment();
		EIPPaymentForwardPage eIPPaymentForwardPage = new EIPPaymentForwardPage(getDriver());
		eIPPaymentForwardPage.verifyPageLoaded();
		eIPPaymentForwardPage.verifyPIImaskingUserName(PaymentConstants.PII_CUSTOMER_CUSTFIRSTNAME_PID);
		eIPPaymentForwardPage.verifyPIImaskingMsisdn(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
		eIPPaymentForwardPage.verifyPIImaskingSavedCard(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		eIPPaymentForwardPage.enterPaymentamount();
		eIPPaymentForwardPage.fillPaymentInfo(myTmoData.getPayment());
		eIPPaymentForwardPage.clickNextButton();
		EIPPaymentReviewCCPage eIPPaymentreviewCCPage = new EIPPaymentReviewCCPage(getDriver());
		eIPPaymentreviewCCPage.verifyPageLoaded();
		eIPPaymentreviewCCPage.verifyPIIforPhoneNumber(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
		eIPPaymentreviewCCPage.verifyPIIforCustomerName(PaymentConstants.PII_CUSTOMER_CUSTOMERNAME_PID);
		eIPPaymentreviewCCPage.verifyPIIforPaymentInfo(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		eIPPaymentreviewCCPage.verifyPIIforZipCode(PaymentConstants.PII_CUSTOMER_ZIPCODE_PID);
		eIPPaymentreviewCCPage.verifyPIIforBanNumber(PaymentConstants.PII_CUSTOMER_BANNUMBER_PID);
		
		/*		eIPPaymentreviewCCPage.clickCCTermsAndConditions();
		eIPPaymentreviewCCPage.clickCreditCardSubmitButton();
		EIPPaymentConfirmationCCPage eipPaymentConformationPage = new EIPPaymentConfirmationCCPage(getDriver());
		eipPaymentConformationPage.verifyPageLoaded();
		eipPaymentConformationPage.verifyPIIforCustomerFirstName(PaymentConstants.PII_CUSTOMER_CUSTFIRSTNAME_PID);
		eipPaymentConformationPage.verifyPIIforCustomerName(PaymentConstants.PII_CUSTOMER_CUSTOMERNAME_PID);
		eipPaymentConformationPage.verifyPIIforImeiNumber(PaymentConstants.PII_CUSTOMER_IMEI_PID);
		eipPaymentConformationPage.verifyPIIforPaymentInfo(PaymentConstants.PII_CUSTOMER_PAYMENTINFO_PID);
		eipPaymentConformationPage.verifyPIIforPhoneNumber(PaymentConstants.PII_CUSTOMER_MSISDN_PID);
		eipPaymentConformationPage.verifyPIIforBanNumber(PaymentConstants.PII_CUSTOMER_BANNUMBER_PID);
		eipPaymentConformationPage.verifyPIIforZipCode(PaymentConstants.PII_CUSTOMER_ZIPCODE_PID);
*/	
	}
}
