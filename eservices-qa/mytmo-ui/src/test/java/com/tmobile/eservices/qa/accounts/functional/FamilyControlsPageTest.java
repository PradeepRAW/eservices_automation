package com.tmobile.eservices.qa.accounts.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.accounts.FamilyControlsPage;
import com.tmobile.eservices.qa.pages.accounts.FamilyWherePage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.global.AppleStorePage;
import com.tmobile.eservices.qa.pages.global.FamilyAllowanceManagePage;
import com.tmobile.eservices.qa.pages.global.PlayStorePage;

public class FamilyControlsPageTest extends GlobalCommonLib {

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DUPLICATE })
	public void verifyFamilyWhere(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on family controls link | Family controls page should be displayed");
		Reporter.log("6. Click family where link | Family where page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData, "Family Controls");
		familyControlsPage.clickFamilyWhere();

		FamilyWherePage familyWherePage = new FamilyWherePage(getDriver());
		familyWherePage.verifyFamilyWherePage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void verifyFamilyAllowancesManageLink(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with family allowance manage link");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on family controls link | Family controls page should be displayed");
		Reporter.log("6. Click manage link | Family allowance manage page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData, "Family Controls");
		familyControlsPage.clickFamilyAllowances();
		familyControlsPage.clickManageLink();
		familyControlsPage.switchToWindow();
		familyControlsPage.acceptIOSAlert();

		FamilyAllowanceManagePage familyAllowanceManagePage = new FamilyAllowanceManagePage(getDriver());
		familyAllowanceManagePage.verifyFamilyAllowanceManagePage();
	}

	/**
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROFILE, Group.ACCOUNTS, Group.DESKTOP,
			Group.ANDROID, Group.IOS })
	public void verifyWebGuards(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on family controls link | Family controls page should be displayed");
		Reporter.log("6. Click on web guard and update filter | Family controls page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData, "Family Controls");
		familyControlsPage.clickWebGuard();
		familyControlsPage.clickChildRadioBtn();
		familyControlsPage.clickShowFilterBtn();
		familyControlsPage.verifyPermissionText();
		familyControlsPage.clickHideFilterBtn();
		familyControlsPage.clickSaveChangesBtn();
		familyControlsPage.verifyFamilyControlsPage();
	}

	/**
	 * Ensure user can edit family control settings.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROFILE, Group.ACCOUNTS, Group.DESKTOP,
			Group.ANDROID, Group.IOS })
	public void verifyEnsureUserCanEditFamilyControlSettings(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile Link |  Profile page should be displayed");
		Reporter.log("5. Click on family control blade | Family control page should be displayed");
		Reporter.log("6. Click on web guard |Web guard page should be clicked");
		Reporter.log("7. Change the permission level |Changing Permission level operation should be success");
		Reporter.log("8. Verify if save button is enabled |Save button should be enabled");

		Reporter.log("========================");
		Reporter.log("Actual Results:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData, "Family Controls");
		familyControlsPage.clickWebGuard();
		familyControlsPage.selectWebGuardPermissions();
		familyControlsPage.verifySaveBtnEnabled();
	}

	/**
	 * Ensure user is can edit family control settings.US196734
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROFILE, Group.ACCOUNTS, Group.DESKTOP,
			Group.ANDROID, Group.IOS })
	public void verifyCrazyLegRatePlanRestrictedProfilePageFamilyControls(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile Link | Profile page should be displayed");
		Reporter.log("5. Click on family controls link | Family controls page should be displayed");
		Reporter.log("6. Click on web guard | Web guard should be clicked successfully");
		Reporter.log("7. Select child radio button | Child radio button should be selected");
		Reporter.log("8. Click on save button | Save button should be clicked successfully");
		Reporter.log("9. Verify family controls page | Family controls page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Results:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData, "Family Controls");
		familyControlsPage.clickWebGuard();
		familyControlsPage.clickChildRadioBtn();
		familyControlsPage.clickSaveBtn();
		familyControlsPage.verifyFamilyControlsPage();
	}

	/**
	 * US334348:Bearmode - Profile Page Deployment and Testing
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyBearModeDiv(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on family controls link | Family controls page should be displayed");
		Reporter.log("6. Verify family mode divison | Family mode divison should be displayed");
		Reporter.log("7. Verify apple store icon | Apple store icon should be displayed");
		Reporter.log("8. Click apple store icon | Apple store icon should be clicked");
		Reporter.log("9. Verify apple store page | Apple store page should be displayed");
		Reporter.log("10.Verify play store icon| Play store icon should be displayed");
		Reporter.log("11.Click play store icon | Play store icon should be clicked");
		Reporter.log("12.Verify play store page| Play store page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData, "Family Controls");
		String MainWindow = getDriver().getWindowHandle();
		familyControlsPage.verifyBearModeDiv();
		familyControlsPage.verifyPlayStoreIcon();
		familyControlsPage.clickPlayStoreIcon();
		familyControlsPage.switchToWindow();

		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		PlayStorePage playStorePage = new PlayStorePage(getDriver());
		playStorePage.verifyPlayStorePage();
		playStorePage.closeOtherTabs(MainWindow);

		familyControlsPage.verifyAppleStoreIcon();
		familyControlsPage.clickAppleStoreIcon();

		familyControlsPage.switchToWindow();

		commonPage = new CommonPage(getDriver());
		commonPage.clickOnAllowPopUp();

		AppleStorePage AppleStorePage = new AppleStorePage(getDriver());
		AppleStorePage.verifyAppleStorePage();
	}

	/**
	 * DE171944:Prod: Updated failed error on web guard while editing NPAH lines
	 * DE232572:Prod - Updating WebGuard settings in My T-Mobile web or app fails
	 * with Update Operation Failed error DE234322:Web guard update failed for some
	 * users (date issue) - WEB fixes
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyWebGuardUpdateOperationFailedDailogBox(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile Link | Profile page should be displayed");
		Reporter.log("5. Click on family control blade | Family control page should be displayed");
		Reporter.log("6. Click on web guard | Web guard page should be clicked");
		Reporter.log("7. Verify Web Guard bread crumb active status| Web Guard bread crumb should be active");
		Reporter.log("8. Change the permission level | Changing permission level operation should be success");
		Reporter.log("9. Click on save button | Save button should be clicked successfully");
		Reporter.log(
				"10.Verify update operation failed dialog box | Update operation failed dialog box should not be displayed");
		Reporter.log("11.Verify web guard permission updated | Web guard permission update should be success");

		Reporter.log("========================");
		Reporter.log("Actual Results:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData, "Family Controls");
		familyControlsPage.clickWebGuard();
		familyControlsPage.verifyBreadCrumbActiveStatus("Web Guard");
		String selectedPermisson = familyControlsPage.selectWebGuardPermissions();
		familyControlsPage.clickSaveBtn();
		familyControlsPage.verifyUpdateOperationFailedDialogBox();
		familyControlsPage.verifyUpdatedWebGuardPermsission(selectedPermisson);
	}

	/***
	 * US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO -
	 * GLOBAL COD TC ID - TC250751
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PROFILE, Group.ACCOUNTS, Group.SPRINT })
	public void testLineSelectorSearchFieldValidations(MyTmoData myTmoData) {
		Reporter.log("US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO - GLOBAL COD");
		Reporter.log("Data Condition | Any Multi line MSISDN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Select a line from the drop down in the Profile page | Line should be selected from the drop down");
		Reporter.log(
				"6. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log("7. Verify presence of search icon | search icon should be displayed");
		Reporter.log(
				"8. Verify the search text field has First name, mobile number selected from the profile page | Search text field should have the first name, mobile number selected on profile page");
		Reporter.log("9. Verify Clear icon next the search field | Clear icon should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToLoginPage(myTmoData);
		FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
		familyControlsPage.login("4236179895", "Auto12345");
		familyControlsPage.navigateToProfilePage();
		ProfilePage profilePage = new ProfilePage(getDriver());
		String selectedLineInProfilePage = profilePage.returnSelectedLine();
		familyControlsPage.navigateToFamilyControlsPage();
		familyControlsPage.verifySearchIconOnLineSelectorSearchTextBar();
		familyControlsPage.verifyDefaultLineValueForLineSelectorSearchTextBar(selectedLineInProfilePage);
		familyControlsPage.verifyClearIcon();
	}

	/***
	 * US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO -
	 * GLOBAL COD TC ID - TC250775
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PROFILE, Group.ACCOUNTS, Group.SPRINT })
	public void testLineSelectorClearIconFunctionality(MyTmoData myTmoData) {
		Reporter.log("US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO - GLOBAL COD");
		Reporter.log("Data Condition | Any Multi line MSISDN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Select a line from the drop down in the Profile page | Line should be selected from the drop down");
		Reporter.log(
				"6. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log("7. Clear the existing line data| Line details should be cleared");
		Reporter.log("8. Type in alpha numeric characters| Entered Text should be allowed");
		Reporter.log(
				"9. Click on the Clear icon| Entered text should be cleared and place holder text \"Find a line\" should be displayed");
		Reporter.log(
				"10. Verify the cleared history| The last cleared text should appear first in the result and every result in the list should be present adjacent to the History symbol");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToLoginPage(myTmoData);
		FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
		familyControlsPage.login("4236179895", "Auto12345");
		familyControlsPage.navigateToProfilePage();
		ProfilePage profilePage = new ProfilePage(getDriver());
		String selectedLineInProfilePage = profilePage.returnSelectedLine();
		familyControlsPage.navigateToFamilyControlsPage();
		familyControlsPage.verifyDefaultLineValueForLineSelectorSearchTextBar(selectedLineInProfilePage);
		familyControlsPage.clickClearIcon();
		familyControlsPage.verifyNoTextInLineSelectorSearchTextBar();
		familyControlsPage.verifyClearedHistoryValue(selectedLineInProfilePage);
	}

	/***
	 * US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO -
	 * GLOBAL COD TC ID - TC250789
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PROFILE, Group.ACCOUNTS, Group.SPRINT })
	public void testAutocompleteFeautreInLineSelectorOnSearchByName(MyTmoData myTmoData) {
		Reporter.log("US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO - GLOBAL COD");
		Reporter.log("Data Condition | Any Multi line MSISDN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Select a line from the drop down in the Profile page | Line should be selected from the drop down");
		Reporter.log(
				"6. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log("7. Clear the existing line data| Line details should be cleared");
		Reporter.log(
				"8. Enter any character and verify autocomplete list of results having the matching character| All the results loaded should have the matching character");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToLoginPage(myTmoData);
		FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
		familyControlsPage.login("4236179895", "Auto12345");
		familyControlsPage.navigateToProfilePage();
		ProfilePage profilePage = new ProfilePage(getDriver());
		String selectedLineInProfilePage = profilePage.returnSelectedLine();
		familyControlsPage.navigateToFamilyControlsPage();
		familyControlsPage.verifyDefaultLineValueForLineSelectorSearchTextBar(selectedLineInProfilePage);
		familyControlsPage.clickClearIcon();
		familyControlsPage.setTextForLineSelectorSearchTextBar(selectedLineInProfilePage.substring(0, 1));
		familyControlsPage
				.verifyAutoCompleteSearchResultsForLineSelectorField(selectedLineInProfilePage.substring(0, 1));
	}

	/***
	 * US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO -
	 * GLOBAL COD TC ID - TC250807
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PROFILE, Group.ACCOUNTS, Group.SPRINT })
	public void testAutocompleteFeautreInLineSelectorOnSearchByNumber(MyTmoData myTmoData) {
		Reporter.log("US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO - GLOBAL COD");
		Reporter.log("Data Condition | Any Multi line MSISDN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Select a line from the drop down in the Profile page | Line should be selected from the drop down");
		Reporter.log(
				"6. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log("7. Clear the existing line data| Line details should be cleared");
		Reporter.log(
				"8. Enter any number and verify autocomplete list of results having the matching number| All the results loaded should have the matching number");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToLoginPage(myTmoData);
		FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
		familyControlsPage.login("4236179895", "Auto12345");
		familyControlsPage.navigateToProfilePage();
		ProfilePage profilePage = new ProfilePage(getDriver());
		String selectedLineInProfilePage = profilePage.returnSelectedLine();
		familyControlsPage.navigateToFamilyControlsPage();
		familyControlsPage.verifyDefaultLineValueForLineSelectorSearchTextBar(selectedLineInProfilePage);
		familyControlsPage.clickClearIcon();
		String numFetched[] = selectedLineInProfilePage.split(",");
		String numberToVerify = numFetched[1].trim().substring(1, 2);
		familyControlsPage.setTextForLineSelectorSearchTextBar(numberToVerify);
		familyControlsPage.verifyAutoCompleteSearchResultsForLineSelectorField(numberToVerify);
	}

	/***
	 * US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO -
	 * GLOBAL COD TC ID - TC250810
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PROFILE, Group.ACCOUNTS, Group.SPRINT })
	public void testHoverFunctionalityInLineSelectorSearchResults(MyTmoData myTmoData) {
		Reporter.log("US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO - GLOBAL COD");
		Reporter.log("Data Condition | Any Multi line MSISDN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Select a line from the drop down in the Profile page | Line should be selected from the drop down");
		Reporter.log(
				"6. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log("7. Clear the existing line data| Line details should be cleared");
		Reporter.log(
				"8. Enter any character/number and verify autocomplete list of results having the matching number/character| All the results loaded should have the matching number/character");
		Reporter.log(
				"9. Hover on any search result and verify the value in the search field| The value in the search field should have the hovered value");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToLoginPage(myTmoData);
		FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
		familyControlsPage.login("4236179895", "Auto12345");
		familyControlsPage.navigateToProfilePage();
		ProfilePage profilePage = new ProfilePage(getDriver());
		String selectedLineInProfilePage = profilePage.returnSelectedLine();
		familyControlsPage.navigateToFamilyControlsPage();
		familyControlsPage.verifyDefaultLineValueForLineSelectorSearchTextBar(selectedLineInProfilePage);
		familyControlsPage.clickClearIcon();
		familyControlsPage.setTextForLineSelectorSearchTextBar(selectedLineInProfilePage.substring(0, 1));
		familyControlsPage.verifyHoverFunctionalityInSearchResultsForLineSelectorField();
	}

	/***
	 * US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO -
	 * GLOBAL COD TC ID - TC250812
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PROFILE, Group.ACCOUNTS, Group.SPRINT })
	public void testAutocompleteFeautreInLineSelectorOnMobile(MyTmoData myTmoData) {
		Reporter.log("US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO - GLOBAL COD");
		Reporter.log("Data Condition | Any Multi line MSISDN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application in the mobile browser | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Select a line from the drop down in the Profile page | Line should be selected from the drop down");
		Reporter.log(
				"6. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log("7. Clear the existing line data| Line details should be cleared");
		Reporter.log(
				"8. Enter any number and verify autocomplete list of results having the matching number| All the results loaded should have the matching number");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToLoginPage(myTmoData);
		FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
		familyControlsPage.login("4236179895", "Auto12345");
		familyControlsPage.navigateToProfilePage();
		ProfilePage profilePage = new ProfilePage(getDriver());
		String selectedLineInProfilePage = profilePage.returnSelectedLine();
		familyControlsPage.navigateToFamilyControlsPage();
		familyControlsPage.verifyDefaultLineValueForLineSelectorSearchTextBar(selectedLineInProfilePage);
		familyControlsPage.clickClearIcon();
		familyControlsPage.setTextForLineSelectorSearchTextBar(selectedLineInProfilePage.substring(0, 1));
		familyControlsPage
				.verifyAutoCompleteSearchResultsForLineSelectorField(selectedLineInProfilePage.substring(0, 1));
	}

	/***
	 * US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO -
	 * GLOBAL COD TC ID - TC250814
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.RETIRED })
	public void testTabOutFunctionalityWithOutSelectingAnyLineInLineSelectorSearchResults(MyTmoData myTmoData) {
		Reporter.log("US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO - GLOBAL COD");
		Reporter.log("Data Condition | Any Multi line MSISDN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Select a line from the drop down in the Profile page | Line should be selected from the drop down");
		Reporter.log(
				"6. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log("7. Clear the existing line data| Line details should be cleared");
		Reporter.log(
				"8. Enter any character/number and verify autocomplete list of results having the matching number/character| All the results loaded should have the matching number/character");
		Reporter.log(
				"9. Tab out from the field without selecting any line from the results | The value should be reset to the line details on page load");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToLoginPage(myTmoData);
		FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
		familyControlsPage.login("4236179895", "Auto12345");
		familyControlsPage.navigateToProfilePage();
		ProfilePage profilePage = new ProfilePage(getDriver());
		String selectedLineInProfilePage = profilePage.returnSelectedLine();
		familyControlsPage.navigateToFamilyControlsPage();
		familyControlsPage.verifyDefaultLineValueForLineSelectorSearchTextBar(selectedLineInProfilePage);
		familyControlsPage.clickClearIcon();
		familyControlsPage.verifyTabOutFunctionalityForLineSelectorField(selectedLineInProfilePage);
	}

	/***
	 * US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO -
	 * GLOBAL COD TC ID - TC250817
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PROFILE, Group.ACCOUNTS, Group.SPRINT })
	public void testLineSelectorSearchFunctionalityForNoValidSearch(MyTmoData myTmoData) {
		Reporter.log("US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO - GLOBAL COD");
		Reporter.log("Data Condition | Any Single line MSISDN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log("6. Clear the existing line data| Line details should be cleared");
		Reporter.log(
				"6. Enter few characters/numbers that doesnt match the first name or the number of the lines | No results should be loaded and no error message should populate");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToLoginPage(myTmoData);
		FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
		familyControlsPage.login("4236179895", "Auto12345");
		familyControlsPage.navigateToProfilePage();
		ProfilePage profilePage = new ProfilePage(getDriver());
		String selectedLineInProfilePage = profilePage.returnSelectedLine();
		familyControlsPage.navigateToFamilyControlsPage();
		familyControlsPage.verifyDefaultLineValueForLineSelectorSearchTextBar(selectedLineInProfilePage);
		familyControlsPage.clickClearIcon();
		familyControlsPage.setTextForLineSelectorSearchTextBar("zzzzzzzzzzzzzzzz");
		familyControlsPage.verifyAutoCompleteSearchResultsForNoMathingSearchCriteria("zzzzzzzzzzzzzzzz");
	}

	/***
	 * US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO -
	 * GLOBAL COD TC ID - TC250815
	 * 
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = false, groups = { Group.PROFILE, Group.ACCOUNTS, Group.SPRINT })
	public void testLineSelectorSearchFunctionalityForSingleLineMSISDN(MyTmoData myTmoData) {
		Reporter.log("US414636: Family Allowances | Line Selector | Search Function (1/3) MYTMO - GLOBAL COD");
		Reporter.log("Data Condition | Any Single line MSISDN");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu and verify Profile page | Profile page should be displayed");
		Reporter.log(
				"5. Click on family controls link and verify Family Control page| Family controls page should be displayed");
		Reporter.log("6. Verify line selector field | Line selector field should not be present");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickProfilePageLink("Family Controls");
		FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
		familyControlsPage.verifyLineSelector();
	}

	/**
	 * US327336/US454528:Profile | Re-Launch Enhancements | Rename Home |
	 * Breadcrumbs
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROFILE, Group.ACCOUNTS, Group.DESKTOP,
			Group.ANDROID, Group.IOS })
	public void verifyFamilyControlsPageBreadCrumbsLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Msisdn");
		Reporter.log("Test Steps|Expected Result");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be logged in  successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Profile Link | Profile page should be displayed");
		Reporter.log("5. Click on family control blade | Family control page should be displayed");
		Reporter.log("6. Click on web guard | Web guard page should be clicked");
		Reporter.log("7: Verify Web Guard bread crumb active status| Web Guard bread crumb should be active");
		Reporter.log(
				"8: Click on Family Controls home bread crumb and verify Family Controls bread crumb active status | Family Controls bread crumb should be active");
		Reporter.log(
				"9: Click on profile bread crumb and verify profiel bread crumb active status | Profile bread crumb should be active");
		Reporter.log("10:Verify profile page | Profile page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData, "Family Controls");
		familyControlsPage.clickWebGuard();
		familyControlsPage.verifyBreadCrumbActiveStatus("Web Guard");
		familyControlsPage.clickBreadCrumb("Family Controls");
		familyControlsPage.verifyFamilyControlsPage();
		familyControlsPage.verifyBreadCrumbActiveStatus("Family Controls");
		familyControlsPage.clickProfileHomeBreadCrumb();
		familyControlsPage.verifyBreadCrumbActiveStatus("Profile");

		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
	}

	/**
	 * US519923:Family Allowance | Suspended Lines | Disable Profile Blade |
	 * Automation testing US504084:Family Allowance | Suspended Lines | Disable
	 * Profile Blade
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyFamilyAllowancesForSuspendLine(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Msisdn configured with family allowance manage link");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log(
				"5. Change to suspend line from line selector dropdown | Change to suspend line should be success");
		Reporter.log("6. Click on family controls link | Family controls page should be displayed");
		Reporter.log("7. Verify family allowance link for nonsuspended line | Family allowance link should be active");
		Reporter.log(
				"8. Change to suspend line from line selector dropdown | Change to suspend line should be success");
		Reporter.log("9. Verify family allowance link for suspended | Family allowance link should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.changeToSuspendedLine();
		profilePage.clickProfilePageLink("Family Controls");

		FamilyControlsPage familyControlsPage = new FamilyControlsPage(getDriver());
		familyControlsPage.verifyFamilyControlsPage();
		familyControlsPage.verifyFamilyAllowancesLink();
	}

	/**
	 * DE229841:nonmaster view list of lines is visible DE230007:Family Allowances |
	 * Non Master User: Line selector getting displayed on family control page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void verifyFamilyControlsPageForNonMasterData(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Non master data");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify home page | Home page should be displayed");
		Reporter.log("4. Click on profile menu | Profile page should be displayed");
		Reporter.log("5. Click on family controls link | Family controls page should be displayed");
		Reporter.log("6. Verify line selector dropdown | Line selector drop down should not be displayed");
		Reporter.log("7. Verify view list of lines link | List of lines link should not be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		FamilyControlsPage familyControlsPage = navigateToFamilyControlsPage(myTmoData, "Family Controls");
		familyControlsPage.verifyLineSelectorDropDownNotDisplayed();
		familyControlsPage.verifyViewListOfLinesNotDisplayed();
	}

}
