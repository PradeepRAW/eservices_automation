package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.EBillApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class EBillApiTest extends EBillApi{
	
	public Map<String, String> tokenMap;
	/**
	 * UserStory# US487798:eBill - Remediate HTTP 200 Status in Case of Error: Implement appropriate HTTP error codes
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true,priority = 1,  groups = { "EBillApi", "testBillList", Group.PAYMENTS,Group.SPRINT })
	public void testBillList(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testBillList");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testBillList";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("billNo", apiTestData.getSku());
		Response response = billList(apiTestData,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertNotNull(getPathVal(jsonNode,"amountDue"),"Invalid amountDue.");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory# US487798:eBill - Remediate HTTP 200 Status in Case of Error: Implement appropriate HTTP error codes
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true,priority = 1,  groups = { "EBillApi", "testBillList_204", Group.PAYMENTS,Group.SPRINT })
	public void testBillList_VerifyErrorCode_204(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testBillList");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 204 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testBillList_VerifyErrorCode_204";
		tokenMap = new HashMap<String, String>();
		tokenMap.put("billNo", "860820110");
		Response response = billList(apiTestData,tokenMap);
		if (response != null && "204".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	
	/**
	 * UserStory# US487798:eBill - Remediate HTTP 200 Status in Case of Error: Implement appropriate HTTP error codes
	 *  
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = {"EBillApi", "testPrintBill", Group.PAYMENTS,Group.SPRINT })
	public void testPrintBill(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: printBill for requested ban,misisdn");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results printBill details in response");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String, String> tokenMap= new HashMap<String,String>();
		tokenMap.put("billNo", apiTestData.getSku());
		tokenMap.put("statementId", "9787984403");
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ban", apiTestData.getBan());
		String operationName = "printBill";
		Response response = printBill(apiTestData, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertNotNull(getPathVal(jsonNode,"title"),"Invalid title.");
			Assert.assertNotNull(getPathVal(jsonNode,"billDateLabel"),"Invalid billDateLabel.");
			Assert.assertNotNull(getPathVal(jsonNode,"billDate"),"Invalid billDate.");
			Assert.assertNotNull(getPathVal(jsonNode,"billInfo"),"Invalid billInfo.");
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory# US487798:eBill - Remediate HTTP 200 Status in Case of Error: Implement appropriate HTTP error codes
	 *  
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = {"EBillApi", "testPrintBill_204", Group.PAYMENTS,Group.SPRINT })
	public void testPrintBill_VerifyErrorCode_204(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: printBill for requested ban,misisdn");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results printBill details in response");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 204 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String, String> tokenMap= new HashMap<String,String>();
		tokenMap.put("billNo", apiTestData.getSku());
		tokenMap.put("statementId", "978798440");
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ban", apiTestData.getBan());
		String operationName = "printBill_204";
		Response response = printBill(apiTestData, tokenMap);
		if (response != null && "204".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory#US487798:eBill - Remediate HTTP 200 Status in Case of Error: Implement appropriate HTTP error codes
	 *  
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = {"EBillApi", "testPrintsummary", Group.PAYMENTS,Group.SPRINT })
	public void testPrintsummary(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: printBillsummary for requested ban,misisdn");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results printBillSummary details in response");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String, String> tokenMap= new HashMap<String,String>();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("statementId", "9212816412");
		tokenMap.put("billNo", "473114962");
		String operationName = "printsummary";
		Response response = printSummary(apiTestData, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory#US487798:eBill - Remediate HTTP 200 Status in Case of Error: Implement appropriate HTTP error codes
	 *  
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = {"EBillApi", "testPrintsummary_204", Group.PAYMENTS,Group.SPRINT })
	public void testPrintsummary_VerifyErrorCode_204(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: printBillsummary for requested ban,misisdn");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results printBillSummary details in response");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 204 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String, String> tokenMap= new HashMap<String,String>();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("statementId", "921281641");
		tokenMap.put("billNo", "473114962");
		String operationName = "printsummary_204";
		Response response = printSummary(apiTestData, tokenMap);
		if (response != null && "204".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		} else {
			failAndLogResponse(response, operationName);
		}
	}
}
