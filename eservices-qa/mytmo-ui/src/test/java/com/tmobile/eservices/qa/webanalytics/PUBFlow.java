package com.tmobile.eservices.qa.webanalytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class PUBFlow extends PaymentCommonLib {
	AnalyticsLib al = new AnalyticsLib();
	
	
	// ----------Analytics start for karen request:PA-----------------
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "karenpa" })
		public void paymentArrangementPagePDL(ControlTestData data, MyTmoData myTmoData) {
			navigateToPaymentArrangementPage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "my/billing/summary.html");

			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			pageNotNullTags.add("v12");
			pageNotNullTags.add("v15");
			pageNotNullTags.add("v16");
			pageNotNullTags.add("v27");
			pageNotNullTags.add("v92");
			pageNotNullTags.add("v93");
			pageNotNullTags.add("v101");
			pageNotNullTags.add("v120");
			pageNotNullTags.add("v73");
			pageNotNullTags.add("v74");
			al.validateAnalyticTagsByScriptElement(getDriver(), pageTags, pageNotNullTags);
		}

		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "karenpa" })
		public void PaymentArrangementInstallmentDetailsPagePDL(ControlTestData data, MyTmoData myTmoData) {
			navigateToPAInstallmentDetailsPage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "my/billing/summary.html");

			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			pageNotNullTags.add("v12");
			pageNotNullTags.add("v15");
			pageNotNullTags.add("v16");
			pageNotNullTags.add("v27");
			pageNotNullTags.add("v92");
			pageNotNullTags.add("v93");
			pageNotNullTags.add("v101");
			pageNotNullTags.add("v120");
			pageNotNullTags.add("v73");
			pageNotNullTags.add("v74");
			al.validateAnalyticTagsByScriptElement(getDriver(), pageTags, pageNotNullTags);
		}

		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "karenpa" })
		public void paymentArrangementTermsAndConditionsPagePDL(ControlTestData data, MyTmoData myTmoData) {
			navigatePATncPage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "my/billing/summary.html");

			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			pageNotNullTags.add("v12");
			pageNotNullTags.add("v15");
			pageNotNullTags.add("v16");
			pageNotNullTags.add("v27");
			pageNotNullTags.add("v92");
			pageNotNullTags.add("v93");
			pageNotNullTags.add("v101");
			pageNotNullTags.add("v120");
			pageNotNullTags.add("v73");
			pageNotNullTags.add("v74");
			al.validateAnalyticTagsByScriptElement(getDriver(), pageTags, pageNotNullTags);
		}

		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT, "karenpa" })
		public void paymentArrangementFAQPagePDL(ControlTestData data, MyTmoData myTmoData) {
			navigatePAFAQPage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "my/billing/summary.html");

			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			pageNotNullTags.add("v12");
			pageNotNullTags.add("v15");
			pageNotNullTags.add("v16");
			pageNotNullTags.add("v27");
			pageNotNullTags.add("v92");
			pageNotNullTags.add("v93");
			pageNotNullTags.add("v101");
			pageNotNullTags.add("v120");
			pageNotNullTags.add("v73");
			pageNotNullTags.add("v74");
			al.validateAnalyticTagsByScriptElement(getDriver(), pageTags, pageNotNullTags);
		}
		
		
		//----------Analytics start for karen request:OTP-----------------
		
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT,"karenotp"})
		public void oneTimePaymentPagePDL(ControlTestData data, MyTmoData myTmoData) {
			
			navigateToOTPpage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "my/billing/summary.html");
			
			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			pageNotNullTags.add("v12");
			pageNotNullTags.add("v15");
			pageNotNullTags.add("v16");
			pageNotNullTags.add("v27");
			pageNotNullTags.add("v92");
			pageNotNullTags.add("v93");
			pageNotNullTags.add("v101");
			pageNotNullTags.add("v120");
			pageNotNullTags.add("v73");
			pageNotNullTags.add("v74");
			al.validateAnalyticTagsByScriptElement(getDriver(), pageTags,pageNotNullTags);
		}
		
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT,"karenotp"})
		public void oneTimePaymentTermAndConditionsPagePDL(ControlTestData data, MyTmoData myTmoData) {
			
			navigateOTPTncPage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "MyTMO | Billing : OTP : HUB");
			
			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			pageNotNullTags.add("v12");
			pageNotNullTags.add("v15");
			pageNotNullTags.add("v16");
			pageNotNullTags.add("v27");
			pageNotNullTags.add("v92");
			pageNotNullTags.add("v93");
			pageNotNullTags.add("v101");
			pageNotNullTags.add("v120");
			pageNotNullTags.add("v73");
			pageNotNullTags.add("v74");
			al.validateAnalyticTagsByScriptElement(getDriver(), pageTags,pageNotNullTags);
		}
		
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT,"karenotp" })
		public void editPaymentAmountPagePDL(ControlTestData data, MyTmoData myTmoData) {
			
			navigateToEditAmountPage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "MyTMO | Billing : OTP : HUB");
			
			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			pageNotNullTags.add("v12");
			pageNotNullTags.add("v15");
			pageNotNullTags.add("v16");
			pageNotNullTags.add("v27");
			pageNotNullTags.add("v92");
			pageNotNullTags.add("v93");
			pageNotNullTags.add("v101");
			pageNotNullTags.add("v120");
			pageNotNullTags.add("v73");
			pageNotNullTags.add("v74");
			al.validateAnalyticTagsByScriptElement(getDriver(), pageTags,pageNotNullTags);
		}
		
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT,"karenotp" })
		public void selectPaymentDatePagePDL(ControlTestData data, MyTmoData myTmoData) {
			
			navigateToOTPDatePage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "my/billing/summary.html");
			
			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			pageNotNullTags.add("v12");
			pageNotNullTags.add("v15");
			pageNotNullTags.add("v16");
			pageNotNullTags.add("v27");
			pageNotNullTags.add("v92");
			pageNotNullTags.add("v93");
			pageNotNullTags.add("v101");
			pageNotNullTags.add("v120");
			pageNotNullTags.add("v73");
			pageNotNullTags.add("v74");
			al.validateAnalyticTagsByScriptElement(getDriver(), pageTags,pageNotNullTags);
		}
		
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
		public void oTPConfirmationPagePDL(ControlTestData data, MyTmoData myTmoData) {
			
			navigateToEditAmountPage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "my/billing/summary.html");
			
			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			pageNotNullTags.add("v12");
			pageNotNullTags.add("v15");
			pageNotNullTags.add("v16");
			pageNotNullTags.add("v27");
			pageNotNullTags.add("v92");
			pageNotNullTags.add("v93");
			pageNotNullTags.add("v101");
			pageNotNullTags.add("v120");
			pageNotNullTags.add("v73");
			pageNotNullTags.add("v74");
			al.validateAnalyticTagsByScriptElement(getDriver(), pageTags,pageNotNullTags);
		}
		
		// ----------Analytics start for karen request:AutoPay-----------------
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT,"karenap" })
		public void spokePagePDL(ControlTestData data, MyTmoData myTmoData) {
			
			navigateToSpokePage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "my/billing/summary.html");
			
			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			pageNotNullTags.add("v12");
			pageNotNullTags.add("v15");
			pageNotNullTags.add("v16");
			pageNotNullTags.add("v27");
			pageNotNullTags.add("v92");
			pageNotNullTags.add("v93");
			pageNotNullTags.add("v101");
			pageNotNullTags.add("v120");
			pageNotNullTags.add("v73");
			pageNotNullTags.add("v74");
			al.validateAnalyticTagsByScriptElement(getDriver(), pageTags,pageNotNullTags);
		}
		
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT,"karenap" })
		public void addCardPagePDL(ControlTestData data, MyTmoData myTmoData) {
			
			navigateToAddCardPage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "my/billing/summary.html");
			
			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			pageNotNullTags.add("v12");
			pageNotNullTags.add("v15");
			pageNotNullTags.add("v16");
			pageNotNullTags.add("v27");
			pageNotNullTags.add("v92");
			pageNotNullTags.add("v93");
			pageNotNullTags.add("v101");
			pageNotNullTags.add("v120");
			pageNotNullTags.add("v73");
			pageNotNullTags.add("v74");
			al.validateAnalyticTagsByScriptElement(getDriver(), pageTags,pageNotNullTags);
		}
		
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT,"karenap" })
		public void addBankPagePDL(ControlTestData data, MyTmoData myTmoData) {
			
			navigateToAddBankPage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "my/billing/summary.html");
			
			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			pageNotNullTags.add("v12");
			pageNotNullTags.add("v15");
			pageNotNullTags.add("v16");
			pageNotNullTags.add("v27");
			pageNotNullTags.add("v92");
			pageNotNullTags.add("v93");
			pageNotNullTags.add("v101");
			pageNotNullTags.add("v120");
			pageNotNullTags.add("v73");
			pageNotNullTags.add("v74");
			al.validateAnalyticTagsByScriptElement(getDriver(), pageTags,pageNotNullTags);
		}
	//----------Analytics end for karen request-----------------
	
	//Account History page Analytics
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAccountHistoryPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToAccountHistoryPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}

	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAddBankPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToAddBankPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAddCardPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToAddCardPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutopayCancelConfirmationPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToAutoPayCancelConfirmationPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutopayConfirmationPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToAutoPayConfirmationPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testAutoPayPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToAutoPayPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testBillAndPaySummaryPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToBillAndPaySummaryPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "");
		pageTags.put("v6", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testBillDetailsPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToBillDetailsPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testBillingSummaryPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToBillingSummaryPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testCallDetailRecordsForwardPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToCallDetailRecordsForwardPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPDetailsPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToEIPDetailsPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentConfirmationCCPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		//navigateToEIPPaymentConfirmationCCPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentForwardPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToEIPPaymentForwardPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	

	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testEIPPaymentReviewCCPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToEIPPaymentReviewCCPage(myTmoData, "Card");
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testFAQpageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToAutoPayFAQPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void homePageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToHomePage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testLeaseDetailsPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToLeaseDetailsPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testOTPAmountPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToEditAmountPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testOTPConfirmationPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToOTPConfirmationPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testOTPDatePageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToOTPDatePage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAConfirmationPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToPAConfirmationPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPAInstallmentDetailsPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToPAInstallmentDetailsPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPaperlessBillingPageAnalytics(ControlTestData data, MyTmoData myTmoData) throws Exception {
		
		navigateToPaperlessBillingPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPaymentArrangementPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToPaymentArrangementPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPaymentErrorPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToPaymentErrorPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	//Navigation method should be written
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testProfilePageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToPaymentArrangementPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testSpokePageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToSpokePage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testTnCPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		
		navigateToAutoPayTnCPage(myTmoData);
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}
	
	
	//Navigational steps to be written
		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
		public void testUsageDetailsPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
			
			navigateToSpokePage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "my/billing/summary.html");
			
			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
		}

		@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
		public void testUsageOverviewPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
			
			navigateToUsageOverviewPage(myTmoData);
			HashMap<String, String> pageTags = new HashMap<String, String>();
			pageTags.put("v5", "my/billing/summary.html");
			
			List<String> pageNotNullTags = new ArrayList<>();
			pageNotNullTags.add("v10");
			al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
		}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testUsagePageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		navigateToUsagePage(myTmoData);
	
		HashMap<String, String> pageTags = new HashMap<String, String>();
		pageTags.put("v5", "my/billing/summary.html");
		
		List<String> pageNotNullTags = new ArrayList<>();
		pageNotNullTags.add("v10");
		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
	}

	

}
