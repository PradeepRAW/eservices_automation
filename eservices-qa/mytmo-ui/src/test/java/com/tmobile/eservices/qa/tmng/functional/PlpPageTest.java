package com.tmobile.eservices.qa.tmng.functional;


import com.tmobile.eservices.qa.pages.global.LoginPage;
import com.tmobile.eservices.qa.pages.tmng.functional.*;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class PlpPageTest extends TmngCommonLib {

	@Test(dataProvider = "byColumnName", enabled = true)
	public void testPhonesPage(TMNGData tMNGData) {
		navigateToPhonesPlpPage(tMNGData);

	}

	/**
	 * C478873 Validate home/Index page and UNAV tabs
	 * C347308 TMO- Verify Legal disclaimer
	 * US385690: TMNG Main PLP Migration to V2 - Product Grid: Product Image/Color
	 * C347307: TMO- Verify Footer links On PhonesPage
	 * Swatches
	 * @param data
	 * @param tMNGData
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyUNOTabs(ControlTestData data, TMNGData tMNGData) throws ClientProtocolException, IOException {
		Reporter.log("Test Case: C478873	Validate home/Index page and UNAV tabs");
		Reporter.log("Test Case:US386606 : TMNG Main PLP Migration to V2 - initial page load of Cellphones Main PLP");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | UNO Cell phones page should be displayed");
		Reporter.log("3. Verify Phones Images in PLP and color swaches | Phones Images and color swatches should be displayed for all the phones in PLP");
		Reporter.log("4. Verify legal disclaimer | Legal disclaimer should be displayed");
		Reporter.log("5. Verify Tablet Tab | Tablet Tab should be displayed");
		Reporter.log("6. Click Tablet Tab | Tablet PLP should be displayed");
		Reporter.log("7. Verify Watch Tab | Watch Tab should be displayed");
		Reporter.log("8. Click Watch Tab | Watch PLP should be displayed");
		Reporter.log("9. Verify Accessories Tab | Accessories Tab should be displayed");
		Reporter.log("10. Click Accessories Tab | Accessories PLP should be displayed");
		Reporter.log("11. Click on Footer links | Appropritae page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage plpPage = navigateToPhonesPlpPage(tMNGData);
		plpPage.verifyDeviceImg();
		plpPage.verifyColorSwatchDisplayed();
		plpPage.verifyLegalDisclaimer();
		plpPage.clickOnTabletsAndDevicesLink();
		plpPage.verifyTabletsAndDevicesPlpPageLoaded();
		plpPage.clickOnWatchesLink();
		plpPage.verifyWatchesPlpPageLoaded();
		plpPage.clickOnAccessoriesMenuLink();
		plpPage.verifyAccessoriesPlpPageLoaded();
		plpPage.clickFooterLink();
		plpPage.verifyFooterLinkPage();
		
	}

	/**
	 * US377624 : TMNG Main PLP Migration to V2 - Sort Orders for Internet Devices
	 * C494391 Validate that PHONES/TABLETS/WATCHES are able to be sorted by
	 * Featured, Price Low to High, Price High to Low and Highest Rating
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testSortingForTabletsAndDevices(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case :US377624 :	TMNG Main PLP Migration to V2 - Sort Orders for Internet Devices");
		Reporter.log("Test Case: C494391: Validate that PHONES/TABLETS/WATCHES are able to be sorted by Featured, Price Low to High, Price High to Low and Highest Rating");
		Reporter.log("================================");
		Reporter.log("Test steps:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Tablet&Devices | Internet Devices Main PLP should be displayed");
		Reporter.log("4. Verify default sort order | Default sort order - Featured should be displayed");
		Reporter.log("5. Select Sort order Low to High | Listed Internet devices should sorted by Price Low to high");
		Reporter.log("6. Select Sort order High to Low | Listed Internet devices should sorted by Price High to Low");
		Reporter.log("7. Select Sort order Low to High first and then select Manufacturer as Apple | Listed Internet devices should show all Apple products in the price Low to High order");
		Reporter.log("8. Clear filter and Manufacturer | Default list should be shown");
		Reporter.log("9. Select Manufacturer as Apple first and then select Sort order High to Low | Listed Internet devices should show all Apple products in the price High to Low order");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage tabletsAndDevicesPlpPage = navigateToTabletsAndDevicesPLP(tMNGData);
		tabletsAndDevicesPlpPage.verifySortingDefaultOptionText();
		tabletsAndDevicesPlpPage.clickOnSortDropdown();
		tabletsAndDevicesPlpPage.clickSortByPriceLowToHigh();
		tabletsAndDevicesPlpPage.verifyPriceLowToHigh();
		tabletsAndDevicesPlpPage.clickOnSortDropdown();
		tabletsAndDevicesPlpPage.clickSortByPriceHighToLow();
		tabletsAndDevicesPlpPage.verifyPriceHighToLow();
		tabletsAndDevicesPlpPage.clickOnSortDropdown();
		tabletsAndDevicesPlpPage.clickSortByPriceLowToHigh();
		tabletsAndDevicesPlpPage.clickFilterBrandsTab();
		tabletsAndDevicesPlpPage.selectFilterOption(tMNGData.getMakeName());
		tabletsAndDevicesPlpPage.verifyPriceLowToHigh();
		tabletsAndDevicesPlpPage.verifyFilteredDevicesInPLPPage(tMNGData.getMakeName());
		tabletsAndDevicesPlpPage.clickClearFilterCTA();
		tabletsAndDevicesPlpPage.selectFilterOption(tMNGData.getMakeName());
		tabletsAndDevicesPlpPage.clickOnSortDropdown();
		tabletsAndDevicesPlpPage.clickSortByPriceHighToLow();
		tabletsAndDevicesPlpPage.verifyFilteredDevicesInPLPPage(tMNGData.getMakeName());
		tabletsAndDevicesPlpPage.verifyPriceHighToLow();

	}

	/**
	 * C347306: TMO- Verify Filter And Sorting On PhonesPage US386606 : TMNG Main
	 * PLP Migration to V2 - initial page load of Cellphones Main PLP C443374 :
	 * Validate Filters on browse page C443377 : Validate Sort drop down
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyFilterAndSortingOnPhonesPage(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case: C347306 TMO - Verify Filter And Sorting On PhonesPage");
		Reporter.log("Test Case:US386606 : TMNG Main PLP Migration to V2 - initial page load of Cellphones Main PLP");
		Reporter.log("Test Case: C443374 : Validate Filters on browse page");
		Reporter.log("Test Case: C443377 : Validate Sort drop down");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Verify filters header | Filters header should be displayed");
		Reporter.log("4. Verify Sort Featured option | Sort option(Featured) should be displyed");
		Reporter.log("5. Click Brands filter and select manufacturer | Selected manufacturer devies should be displayed");
		Reporter.log("6. Click on Sort drop down and select price range as Low to High | Devices prices should be sorted");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPhonesPlpPage(tMNGData);
		PlpPage plpPage = new PlpPage(getDriver());
		plpPage.verifyFiltersHeader();
		plpPage.verifySortingDefaultOptionText();
		plpPage.clickFilterBrandsTab();
		plpPage.selectFilterOption(tMNGData.getMakeName());
		plpPage.verifyFilteredDevicesInPLPPage(tMNGData.getMakeName());
		plpPage.clickOnSortDropdown();
		plpPage.clickSortByPriceLowToHigh();
		plpPage.verifyPriceLowToHigh();
	}	

	/**
	 * US505047 TA1655282 : Prod : Filter not functional when $80 and above" filter
	 * is chosen for accessories who does not have any products around that price
	 * range. DE209566 : Prod : Filter not functional when $80 and above" filter is
	 * chosen for accessories who does not have any products around that price
	 * range.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testNoItemsFoundMessageForZaggAccessoryDevices(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case : TA1655282 : Prod : Filter not functional when $80 and above filter is chosen for accessories who does not have any products around that price range.");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on accessories link | accessory plp page should be display");
		Reporter.log("4. Click on Filter icon | Filters tab should be opened");
		Reporter.log("5. Select Manufactures as ZAGG | Manufacturer device ZAGG sholud be selected");
		Reporter.log("6. Select Price Range as $80 and above | Price Range as $80 and above sholud be selected");
		Reporter.log("7. Close X Filter Icon | Filters tab should be closed");
		Reporter.log("8. Verify No Items found message | No Items found message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToAccessoryPLP(tMNGData);
		PlpPage accessoriesMainPLPPage = new PlpPage(getDriver());
		accessoriesMainPLPPage.clickFilterBrandsTab();
		accessoriesMainPLPPage.clickmorelinkInFilter();
		accessoriesMainPLPPage.selectFilterOption("ZAGG");
		accessoriesMainPLPPage.clickFilterPriceRange();
		accessoriesMainPLPPage.selectFilterOption("$0 - $15");
		accessoriesMainPLPPage.verifyNoItemsFoundMessage();

	}

	/**
	 * US377627:TMNG Main PLP Migration to V2 - Product Grid: Promotion C478558
	 * :Ensure promotional text is in a link at the top of the product grid.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyProductPromotionAndCTAOnPhonesMainPLP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case :US377627:TMNG Main PLP Migration to V2 - Product Grid: Promotion");
		Reporter.log("TestCase: C478558 :Ensure promotional text is in a link at the top of the product grid");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | PLP page should be displayed");
		Reporter.log("3. Verify Promotions in PLP | Promotions should be displayed.");
		Reporter.log("4. Click on Promotion(If that is clickable) | Promotion modal pop up should be displayed.");
		Reporter.log(
				"5. Verify Device name on Promotion modal pop up| Device name in modal pop up should be displayed.");
		Reporter.log(
				"6. Verify Promotion title on Promotion modal pop up| Promotion title in modal pop up should be displayed.");
		Reporter.log(
				"7. Compare Promotion price on Promotion modal pop up and PLP| Promotion prices should matched in PLP and modal popup.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPage = navigateToPhonesPlpPage(tMNGData);
		phonesPage.verifyPromotionTextonPLP();		
		String PromotionPLP = phonesPage.clickPromotionTextonPLP();
		phonesPage.verifyPromotionModal();
		phonesPage.verifyDeviceNameOnPromotionModal();
		phonesPage.verifyPromotionTextOnPromotionModal();
		String PromotionModal = phonesPage.getPromotionAmountfromPromotionModal();
		phonesPage.ComparePromoText(PromotionPLP, PromotionModal);
	}	
	
	/**
	 * US385691: TMNG Main PLP Migration to V2 - Product Grid: Product Name
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyProductNameAndCTAOnPhonesMainPLP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US385691: TMNG Main PLP Migration to V2 - Product Grid: Product Name");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Verify Phones Names in PLP | Phone Name should be displayed for all the phones in PLP");
		Reporter.log("4.Click on Phone Name | Phones Main PDP page should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		PlpPage phonesPage = navigateToPhonesPlpPage(tMNGData);
		phonesPage.verifyAllPhoneNames();
	}

	/**
	 * US384349 :TMNG Main PLP Migration to V2 - Sort Orders for Cell Phones
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testSortingForPhonesPage(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US384349	:TMNG Main PLP Migration to V2 - Sort Orders for Cell Phones");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Verify default sort order | Default sort order - Featured should be displayed");
		Reporter.log("4. Select Sort order High to Low | Listed Phones should sorted by Price High to Low");
		Reporter.log("5. Verify page URL| Page url should contain 'sort=prr' for High to Low");
		Reporter.log("6. Select Sort order Low to High | Listed Phones should sorted by Price Low to High");
		Reporter.log("7. Verify page URL| Page url should contain 'sort=pr' for Low to High");
		Reporter.log("8. Select Sort order Best Seller| Listed Phones should sorted by best seller");
		Reporter.log("9. Verify page URL| Page url should contain 'sort=bs' for best seller");
		Reporter.log("10. Select Sort order Highest Rating| Listed Phones should sorted by Highest Rating");
		Reporter.log("11. Verify page URL| Page url should contain 'sort=r' for Highest rating");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPage = navigateToPhonesPlpPage(tMNGData);
		phonesPage.verifySortingDefaultOptionText();
		phonesPage.clickOnSortDropdown();
		phonesPage.clickSortByPriceHighToLow();
		phonesPage.verifyUrlContainsHighToLow();
		phonesPage.clickOnSortDropdown();
		phonesPage.clickSortByPriceLowToHigh();
		phonesPage.verifyUrlContainsLowToHigh();
		phonesPage.clickOnSortDropdown();
		phonesPage.clickSortByHighestRating();
		phonesPage.verifyUrlContainsHighlyRated();
	}

	/**
	 * US531727 TA1746037 : DI4_DMO_US385655: Price Range Filter Not Working For
	 * Accessories DE224734 : DI4_DMO_US385655: Price Range Filter Not Working For
	 * Accessories
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testPriceRangeFilterForAccessories(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : TA1746037:DI4_DMO_US385655: Price Range Filter Not Working For Accessories");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessories plp page should be displayed");
		Reporter.log("4. Click on Filter icon | Filters tab should be opened");
		Reporter.log("5. Select Manufactures as Apple | Manufacturer device Apple should be selected");
		Reporter.log("6. Select PriceRange as '$0 - $15' | PriceRange '$0 - $15' should be selected");
		Reporter.log("7. Select Categories as 'Cases' | Categories as 'Cases' should be selected");
		Reporter.log("8. Close filter model | Accessories plp page should be displayed");
		Reporter.log(
				"9. Verify Products based on filter condition or If there are no products in that particular category , appropriate informational message | "
						+ "Devices should be displayed on filter condition or appropriate information message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		PlpPage accessoriesPage = navigateToAccessoryPLP(tMNGData);
		accessoriesPage.clickFilterBrandsTab();
		accessoriesPage.selectFilterOption("Apple");
		accessoriesPage.clickFilterPriceRange();
		accessoriesPage.selectFilterOption("$0 - $15");
		// accessoriesPage.clickCategoryByname(tMNGData.getCategoryName());
		accessoriesPage.verifyAccessoriesPlpPageLoaded();
		// accessoriesPage.verifyPriceRangeFilterForAccessories();
	}

	/**
	 * US384340 : TMNG Main PLP Migration to V2 - Sort Orders for Accessories
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP})
	public void testSortingForAccessories(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case :US384340 :	TMNG Main PLP Migration to V2 - Sort Orders for Accessories");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessories Main PLP should be displayed");
		Reporter.log("4. Verify default sort order | Default sort order - Low to high should be displayed");
		Reporter.log("5. Select Sort order High to Low | Listed Accessories should sorted by Price High to Low");
		Reporter.log("6. Verify page URL| Page url should contain 'sort=prr' for High to Low");
		Reporter.log("7. Select Sort order Low to High | Listed Accessories should sorted by Price Low to High");
		Reporter.log("8. Verify page URL| Page url should contain 'sort=pr' for Low to High");
		Reporter.log(
				"9. Select Sort order Low to High first and then select Manufacturer as Apple | Listed Accessrios should show all Apple products in the price Low to High order");
		Reporter.log("10. Clear filter and Manufacturer | Default list should be shown");
		Reporter.log(
				"11. Select Manufacturer as Apple first and then select Sort order High to Low | Listed Accessrios should show all Apple products in the price High to Low order");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage accessariesPage = navigateToAccessoryPLP(tMNGData);
		accessariesPage.verifySortingDefaultOptionTextForAccessoriesTab();
		accessariesPage.clickOnSortDropdown();
		accessariesPage.clickSortByPriceHighToLow();
		accessariesPage.verifyPriceHighToLow();
		accessariesPage.verifyUrlContainsHighToLow();
		accessariesPage.clickOnSortDropdown();
		accessariesPage.clickSortByPriceLowToHigh();
		accessariesPage.verifyPriceLowToHigh();
		accessariesPage.verifyUrlContainsLowToHigh();
		accessariesPage.clickFilterBrandsTab();
		accessariesPage.selectFilterOption("Apple");
		accessariesPage.verifyAccessoriesAreFilteredCorrectly("Apple");
		accessariesPage.verifyPriceLowToHigh();
		accessariesPage.clickOnSortDropdown();
		accessariesPage.clickSortByPriceHighToLow();
		accessariesPage.verifyPriceHighToLow();
	}
	
	/**
	 * US383718: TMNG Main PLP Migration to V2 - Filters for Cellphones
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.NON_MOBILE })
	public void testVerifyFilterOtionsForPhones(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US383718:	TMNG Main PLP Migration to V2 - Filters for Cellphones");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Filter | Filter drop down should be expand");
		Reporter.log("4. Verify filters ordering | Filters should be located alphabetically");
		Reporter.log("5. Verify filter option - Manufacturer | Manufacturer Filter should contain Alcatel, Apple, LG, Motorola, Samsung and T-Mobile");
		Reporter.log("6. Verify filter option - Condition | Condition Filter should contain New and Certified Pre-Owned");
		Reporter.log("7. Verify filter option - OS | OS Filter should contain Android, iOS, OTHER");
		Reporter.log("8. Select Filter options Apple, Alctel and close the filter | Apple, alcatel Phones should be Listed");
		Reporter.log("9. Verify Sort option | Sort order should be Featured");
		Reporter.log("10. Verify selected filter options at below filter section | Apple, and alcatel filter should be displayed");
		Reporter.log("11. Click minus (-) icon of Apple at below filter section | Only alcatel filter should be displayed and apple devices should not display");
		Reporter.log("12. Verify Clear filter CTA | Clear filters should be displayed");
		Reporter.log("13. Click Clear filter CTA | Default list should be loaded");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPage = navigateToPhonesPlpPage(tMNGData);
		phonesPage.verifyFilterTabOptions();
		phonesPage.clickFilterBrandsTab();
		phonesPage.verifyBrandFilterOptionsSort();
		phonesPage.verifyBrandNameOnFilters();
		phonesPage.clickFilterBrandsTab();
		phonesPage.clickFilterOperationSystemTab();
		phonesPage.verifyOSOptionsFilters();
		phonesPage.clickFilterOperationSystemTab();
		phonesPage.clickFilterBrandsTab();
		phonesPage.selectFilterOption(tMNGData.getMakeName());
		phonesPage.clickFilterBrandsTab();
		phonesPage.verifyFilteredDevicesInPLPPage(tMNGData.getMakeName());
		phonesPage.verifySortingDefaultOptionText();
		phonesPage.verifySelectedFilterDisplayedOnTop(tMNGData.getMakeName());
		phonesPage.clickOnRemoveFilter();
		phonesPage.clickFilterBrandsTab();
		phonesPage.selectFilterOption("Alcatel");
		phonesPage.verifyRemoveFilteredDevicesNotListed(tMNGData.getMakeName());
	}
	
	/**
	 * US383718: TMNG Main PLP Migration to V2 - Filters for Cellphones
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.IOS, Group.ANDROID })
	public void testVerifyFilterOtionsForPhonesForMobile(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US383718:	TMNG Main PLP Migration to V2 - Filters for Cellphones");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Filter | Filter drop down should be expand");
		Reporter.log("4. Verify filter headers | Filter headers should be displayed");
		Reporter.log("5. Verify Sort option | Sort order should be Featured");
		Reporter.log("6. Select Filter options Apple and close the filter | Apple Phones should be Listed");
		Reporter.log("7. Verify Clear filter CTA | Clear filters should be displayed");
		Reporter.log("8. Click Clear filter CTA | Default list should be loaded");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PlpPage phonesPage = navigateToPhonesPlpPage(tMNGData);
		phonesPage.verifySortingDefaultOptionText();
		phonesPage.clickFilterBrandsTab();
		phonesPage.selectFilterOption("Apple");
		phonesPage.verifyPhonesPlpPageLoaded();
		phonesPage.clickOnRemoveFilter();
		phonesPage.verifyPhonesPlpPageLoaded();

	}
	/**
	 * US334964 [Continued] TEST ONLY: TMO Additional Terms - Accessory Catalog
	 * Promo (Consumer English site and @Work English site)
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyAccessoryCatalogPromotion(ControlTestData data, TMNGData tMNGData) {
		Reporter.log(
				"Test Case : US334964 -TEST ONLY: TMO Additional Terms - Accessory Catalog Promo (Consumer English site and @Work English site)");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. click on Phones Link |Cell Phones page should be displayed");
		Reporter.log("3. click on Accessories Link |Accessories PLP page should be displayed");
		Reporter.log("4. Verify FRP StrikeOut price | FRP StrikeOut price should be displayed");
		Reporter.log("5. Verify FRP price (After discount) | FRP(After discout) price should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToAccessoryPLP(tMNGData);

		PlpPage accessariesPage = new PlpPage(getDriver());
		Double OLDFRPALP = accessariesPage.getStrikeOutFRPForPromoDevice();
		Double NEWFRPALP = accessariesPage.getNewRPForPromoDevice();

	}	

	/**
	 * US505047 TA1655283 : PROD_Internet Devices PLP_'LG' Filter not working
	 * properly in PLP page DE209648 : PROD_Internet Devices PLP_'LG' Filter not
	 * working properly in PLP page
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testLGDevicesOnlyInPhonesPageWhenUserSelectFilterWithLG(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : TA1655283 : PROD_Internet Devices PLP_'LG' Filter not working properly in PLP page");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Filter icon | Filters tab should be opened");
		Reporter.log("4. Select Manufactures as LG | Manufacturer device LG sholud be selected");
		Reporter.log("5. Close X Filter Icon | Filters tab should be closed");
		Reporter.log("6. Verify LG Devices in PhonesPage | LG devices only displayed in PhonesPage");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPage = navigateToPhonesPlpPage(tMNGData);
		phonesPage.clickFilterBrandsTab();
		phonesPage.selectFilterOption(tMNGData.getMakeName());
		phonesPage.verifyFilteredDevicesInPLPPage(tMNGData.getMakeName());
	}

	/**
	 * US385655 :TMNG Main PLP Migration to V2 - Deeplink URL for Main PLP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testURLsBasedOnFiltersAndSortOrders(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US385655	:TMNG Main PLP Migration to V2 - Deeplink URL for Main PLP");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Filter icon | Filters tab should be opened");
		Reporter.log("4. Select Manufactures as Apple | Manufacturer device Apple sholud be selected");
		Reporter.log("5. Close X Filter Icon | Filters tab should be closed");
		Reporter.log("6. Verify URL | Page url should contain 'apple'");
		Reporter.log(
				"7. Click on Featured icon and Select Sort order as High to Low | Page url should contain 'sort=prr' for High to Low");
		Reporter.log(
				"8. Click on Featured icon and Select Sort order as Low to High | Page url should contain 'sort=pr' for Low to High");
		Reporter.log(
				"9. Click on Featured icon and Select Sort order as Highest rating | Page url should contain 'sort=r' for Highest rating");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		PlpPage phonesPage = navigateToPhonesPlpPage(tMNGData);
		phonesPage.clickFilterBrandsTab();
		phonesPage.selectFilterOption(tMNGData.getMakeName());
		phonesPage.verifyFilterOptionInPageUrl(tMNGData.getMakeName());
		phonesPage.clickOnSortDropdown();
		phonesPage.clickSortByPriceHighToLow();
		phonesPage.verifyUrlContainspriceHighToLow();
		phonesPage.clickOnSortDropdown();
		phonesPage.clickSortByPriceLowToHigh();
		phonesPage.verifyUrlContainspriceLowToHigh();
		phonesPage.clickOnSortDropdown();
		phonesPage.clickSortByHighestRating();
		phonesPage.verifyUrlContainsRating();

	}

	/**
	 * US385695: TMNG Main PLP Migration to V2 - Product Grid: Product Review
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyProductReviewCountAndCTAOnPhonesMainPLP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US385691: TMNG Main PLP Migration to V2 - Product Grid: Product Name");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Verify Reviews count in PLP | Review count should be displayed for all the phones in PLP");
		Reporter.log("4.Click on Reviews Count | the product's Main PDP Reviews section should be displayed.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPage = navigateToPhonesPlpPage(tMNGData);
		phonesPage.verifyAllRatingCount();
		phonesPage.clickOnRatingCount();
		PdpPage phoneDetailsPage = new PdpPage(getDriver());
		phoneDetailsPage.verifyPhonePdpPageLoaded();
		phoneDetailsPage.verifyReviewSectionIsDisplayed();
	}	

	/**
	 * US386609 : TMNG Main PLP Migration to V2 - initial page load of Accessories
	 * Main PLP
	 * US566077 - Issue no 15 : suppress SIMKIT on accessory PLP pages
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyAccessoriesMainPLP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case :US386605 : US386609 : TMNG Main PLP Migration to V2 - initial page load of Accessories Main PLP");
		Reporter.log("Test Case : US566077 - Issue no 15 : suppress SIMKIT on accessory PLP pages");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessories Main PLP should be displayed");
		Reporter.log("4. Verify SIMKit Presence On Accessory PLP Page | SIMKit should't be displayed");
		Reporter.log("5. Verify Accessories Devices | Accessories should be loaded in PLP");
		Reporter.log("6. Verify filter Options | No filters should be selected in PLP");
		Reporter.log("7. Verify Sort order | Default sort option(Price low to high) should be selected and verify products are listed as per the default sort order");
		Reporter.log("8. Verify Accessories count below the Sort order | Accessories count should be displayed below the sort order.");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage accessoriesPage = navigateToAccessoryPLP(tMNGData);
		accessoriesPage.verifySimKitNotListedOnAccessoriesPLP();
		accessoriesPage.verifySortingDefaultOptionTextForAccessoriesTab();
		accessoriesPage.verifyItemCountonPLP();
	}

	/**
	 * US383733: TMNG Main PLP Migration to V2 - Filters for Accessories
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.NON_MOBILE})
	public void testVerifyFilterOtionsForAccessories(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US383733: TMNG Main PLP Migration to V2 - Filters for Accessories");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessories Main PLP should be displayed");
		Reporter.log("4. Click on Filter | Filter drop down should be expand");
		Reporter.log("5. Verify filters ordering | Filters should be located alphabetically");
		Reporter.log(
				"6. Verify filter option - Manufacturer | Manufacturer Filter should contain different Manufacturers like Alcatel, Apple, LG, Motorola, Samsung and T-Mobile etc");
		Reporter.log(
				"7. Verify filter option - Price Range | Price Range Filter should contain different price ranges");
		Reporter.log(
				"8. Select Filter options Apple, Samsung, LG and close the filter | Apple, LG and Samsung Accessories should be Listed");
		Reporter.log("9. Verify number of results | Number of results should be displayed");
		Reporter.log(
				"10. Verify pagination is present if number of devices is greater than 'Results per page' | Pagination should be displayed if number is greater");
		Reporter.log(
				"11. Verify Sort option and order of devices | Sort order should be default - Price Low to High. Devices should be displayed in right order");
		Reporter.log(
				"12. Verify selected filter options at below filter section | Apple, LG and samsung filter should be displayed");
		Reporter.log(
				"13. Click minus (-) icon of Apple at below filter section | Only LG and samsung filter should be displayed and apple devices should not display");
		Reporter.log("14. Verify Clear filter CTA | Clear filters should be displayed");
		Reporter.log("15. Click Clear filter CTA | Default list should be loaded");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage accessoriesPlp = navigateToAccessoryPLP(tMNGData);
		accessoriesPlp.clickFilterBrandsTab();
		accessoriesPlp.verifyBrandFilterOptionsSort();
		accessoriesPlp.verifyBrandNameOnFilters();
		accessoriesPlp.clickFilterBrandsTab();
		accessoriesPlp.clickFilterPriceRange();
		accessoriesPlp.verifyPriceRangeFilterOptions();
		accessoriesPlp.clickFilterPriceRange();
		accessoriesPlp.clickFilterBrandsTab();
		accessoriesPlp.selectFilterOption("Apple");
		accessoriesPlp.clickFilterBrandsTab();
		accessoriesPlp.verifyFilteredDevicesInPLPPage("Apple");
		accessoriesPlp.verifyItemCountonPLP();
		accessoriesPlp.verifySortingDefaultOptionTextForAccessoriesTab();
		accessoriesPlp.clickFilterBrandsTab();
		accessoriesPlp.selectFilterOption("Amazon");
		accessoriesPlp.clickOnRemoveFilter();
		accessoriesPlp.verifyRemoveFilteredDevicesNotListed("Amazon");

	}
	
	/**
	 * US383733: TMNG Main PLP Migration to V2 - Filters for Accessories
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.IOS, Group.ANDROID})
	public void testVerifyFilterOtionsForAccessoriesForMobile(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test Case : US383733: TMNG Main PLP Migration to V2 - Filters for Accessories");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessories Main PLP should be displayed");
		Reporter.log("4. Click on Filter | Filter drop down should be expand");
		Reporter.log("5. Verify Sort option | Sort order should be Low to High");
		Reporter.log("6. Select Filter options Apple and Amazon and close the filter | Apple and Amazon Accessories should be Listed");
		Reporter.log("7. Verify Clear filter CTA | Clear filters should be displayed");
		Reporter.log("9. Click Clear filter CTA | Default list should be loaded");
		
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PlpPage accessoriesPlp = navigateToAccessoryPLP(tMNGData);
		accessoriesPlp.verifySortingDefaultOptionTextForAccessoriesTab();
		accessoriesPlp.clickFilterBrandsTab();
		accessoriesPlp.selectFilterOption("Apple");
		accessoriesPlp.clickOnFilterCTA();
		accessoriesPlp.selectFilterOption("Amazon");
		accessoriesPlp.clickOnRemoveFilter();
		accessoriesPlp.verifyAccessoriesPlpPageLoaded();
		
	}
	
	/**
	 * US580498: V2 - As a Product Manager, I want to be able to display list of
	 * accessory sub-categories on PLP, so that customers can select filter for
	 * their desired accessories.
	 *
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testSubCategoriesOfAccessoriesOnPLP(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Sub Category of Accessories should be displayed");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Accessories Link | Accessories page should be displayed");
		Reporter.log(
				"3. Verify sub-categories for accessories | sub-categories for accessories should be displayed in PLP ");
		Reporter.log(
				"4. Click on any Sub-Category | Specific List of that particular Category should be displayed in PLP");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage accessoriesPage = navigateToAccessoryPLP(tMNGData);
		accessoriesPage.verifyAccessoriesSubCategories();
		accessoriesPage.clickAccessorySubCategoryByName("Headphones");
		accessoriesPage.verifyURLContainsSelectedAccessorySubCategoryName("headphones");

	}

	/**
	 * US578172: As a user, I want to be able view coming soon products on PLP, so
	 * that I'm aware of the status.
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TPP, Group.DESKTOP})
	public void testComingSoonProductStatusOnDevicePLP(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Coming Soon Status on PLP");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones List page should be displayed");
		Reporter.log("3. Verify Promo Text not Displayed | Promo Text should not be displayed in PLP ");
		Reporter.log("4. Verify Color Swatches not Displayed |  Color Swatches should not be displayed in PLP ");
		Reporter.log("5. Verify Pricing not Displayed |  Pricing should not be displayed in PLP ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlpPage = navigateToPhonesPlpPage(tMNGData);		
		phonesPlpPage.verifyPromotionTextonPLP();
		phonesPlpPage.verifyColorSwatchDisplayed();
		phonesPlpPage.verifyPriceLowToHigh();

	}

	/**
	 * US578172: As a user, I want to be able view coming soon products on PLP, so
	 * that I'm aware of the status.
	 * 
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP,Group.TPP })
	public void testComingSoonProductStatusOnAccessoriesPLP(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Coming Soon Status on PLP");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Accessories Link | Accessories page should be displayed");
		Reporter.log("3. Verify Promo Text not Displayed | Promo Text should not be displayed in PLP ");
		Reporter.log("4. Verify Color Swatches not Displayed |  Color Swatches should not be displayed in PLP ");
		Reporter.log("5. Verify Pricing not Displayed |  Pricing should not be displayed in PLP ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage accessoriesPlpPage = navigateToAccessoryPLP(tMNGData);
		accessoriesPlpPage.verifyPromotionTextonPLP();
		accessoriesPlpPage.verifyColorSwatchDisplayed();
		accessoriesPlpPage.verifyPriceLowToHigh();
	}

	/**
	 * US578172: As a user, I want to be able view coming soon products on PLP, so
	 * that I'm aware of the status.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TPP })
	public void testComingSoonProductStatusOnTabletsPLP(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Coming Soon Status on PLP");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Tablets Link | Tablets page should be displayed");
		Reporter.log("3. Verify Promo Text not Displayed | Promo Text should not be displayed in PLP ");
		Reporter.log("4. Verify Color Swatches not Displayed |  Color Swatches should not be displayed in PLP ");
		Reporter.log("5. Verify Pricing not Displayed |  Pricing should not be displayed in PLP ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage tabletsAndDevicesPlpPage = navigateToTabletsAndDevicesPLP(tMNGData);
		tabletsAndDevicesPlpPage.verifyPromotionTextonPLP();
		tabletsAndDevicesPlpPage.verifyColorSwatchDisplayed();
		tabletsAndDevicesPlpPage.verifyPriceLowToHigh();
	}

	/**
	 * US578172: As a user, I want to be able view coming soon products on PLP, so
	 * that I'm aware of the status.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TPP })
	public void testComingSoonProductStatusOnWatchesPLP(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Coming Soon Status on PLP");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Watches Link | Watches page should be displayed");
		Reporter.log("3. Verify Promo Text not Displayed | Promo Text should not be displayed in PLP ");
		Reporter.log("4. Verify Color Swatches not Displayed |  Color Swatches should not be displayed in PLP ");
		Reporter.log("5. Verify Pricing not Displayed |  Pricing should not be displayed in PLP ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage watchesPlpPage = navigateToWatchesPLP(tMNGData);
		watchesPlpPage.verifyPromotionTextonPLP();
		watchesPlpPage.verifyColorSwatchDisplayed();
		watchesPlpPage.verifyPriceLowToHigh();
	}

	/**
	 * US591249: TEST ONLY - As a tester, I want to test functionality regarding
	 * Accessory sub-categories, so that I can ensure all accessory sub-categories
	 * functionality is working as expected when they're available via DCP.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION , Group.TPP })
	public void testSubCategoryAccessoriesSelectedAboveProductGrid(TMNGData tMNGData) {
		Reporter.log("Test Case :TMNG: Sub Category Accessories Selected Above Product Grid");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones page should be displayed");
		Reporter.log("3. Click On Accessories Link | Accessories Sub Category should be Displayed in PLP ");
		Reporter.log("4. Click on Top Accessory Sub Category |  Selected Sub Category Accessory Name should be displayed above product Grid ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PlpPage accessoriesPlpPage = navigateToAccessoryPLP(tMNGData);
		accessoriesPlpPage.verifyAccessoriesSubCategories();
		accessoriesPlpPage.clickAccessorySubCategoryByName(tMNGData.getCategoryName());
		accessoriesPlpPage.verifyURLContainsSelectedAccessorySubCategoryName(tMNGData.getCategoryName().toLowerCase());

	}
	
	/**
	 * US565404:TPP - Prescreen- load page for existing customer (CART/PLP/PDP).
	 * C543900 - PLP:Verify Find my price CTA - Existing customers
	 * US570115:[Test Only] TPP - Load PLP page with CRP 8/Awesome credit pricing as default (since no pre screens or hcc have been done)
	 * C539874 - Verify that the default price displayed in the PLP page is CRP 8/Awesome credit pricing -Cell Phones
	 * US585251:TPP -  Display  Pre verified Price version of PLP (verify price bar at top of page)
	 * C542141 - Validate "Verify your pricing" link is displayed on the PLP Page -Cell Phones
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testVerifyPrescreenLoadforEsistingCustomersForPhones(TMNGData tMNGData) {
		Reporter.log("US565404:TPP - Prescreen- load page for existing customer (CART/PLP/PDP).");
		Reporter.log("C543900 - PLP:Verify Find my price CTA - Existing customers");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing should be displayed on PLP ");
		Reporter.log("4. Verify 'Find your pricing' | 'Find your pricing' link should be displayed in PLP");
		Reporter.log("5. Click 'Find your pricing' | pre-screen intro page should be displayed.");
		Reporter.log("6. Click Let's Go CTA on pre-screen | User should navigate to the Prescreen FORM");
		Reporter.log("7. Verify the URL for the Prescreen form | URL should be 't-mobile.com/pre-screen/form'");
		Reporter.log("8. Verify the page Title | Page title should be 'Prescreen Form Page'");
		Reporter.log("9. Fill all manditory fileds of existing T-mobile customer and click 'Find my price' | pre screen loading page should be displayed");
		Reporter.log("10. Verify already T-Mobile customer modal for existing customer | 'Existing customer' page  should be displayed.");
		Reporter.log("11. Verify already T-Mobile customer modal header | Header should be Looks like you're already a T-Mobile customer.");
		Reporter.log("12. Verify already T-Mobile customer modal text | text should be 'Please log in to your account to make a purchase'");
		Reporter.log("13. Verify Log In CTA on modal | Log In CTA should be displayed");
		Reporter.log("14. Click Log In CTA on modal | User should navigate to my.t-mobile.com/shop");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PlpPage phonesPlp = navigateToPhonesPlpPage(tMNGData);
		phonesPlp.verifyCRPorAwesomePricesInPLP();
		phonesPlp.verifyFindYourPricingLink();
		phonesPlp.verifyYourPricingBoxTextInPLP();
		phonesPlp.clickFindYourPricingLink();
		TPPPreScreenIntro tppIntro = new TPPPreScreenIntro(getDriver());
		tppIntro.verifyPreScreenIntroPage();
		tppIntro.clickLetsGoCTAOnPreScreenIntroPage();
		  
		TPPPreScreenForm tppPreScreenform = new TPPPreScreenForm(getDriver());
		tppPreScreenform.verifyPreScreenForm();
		tppPreScreenform.verifyPreScreenFormPageURL();
		tppPreScreenform.verifyPreScreenFormPageTitle();
	    tppPreScreenform.fillAllFieldsOnPreScreenform(tMNGData);
		tppPreScreenform.clickFindMyPriceCTAOnPreScreenForm();
		tppPreScreenform.verifyPreScreenloadingPage();
		tppPreScreenform.verifyExistingCustomerNotifyModalHeader();
		tppPreScreenform.verifyExistingCustomerNotifyModalPageText();
		tppPreScreenform.clickLogInCTAonExistingCustomerNotifyModal();
		 
		HomePage myTMOShop = new HomePage(getDriver());
		myTMOShop.verifyMyTMOLoginPage();
		
	}

	/**
	 *
	 * US585096: TPP - Prescreen - Display Pre Screen Form intro Page
	 * C543869 - PLP: Validate Existing T-mobile Customer CTA
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.DESKTOP,  Group.TPP })
	public void verifyExistingCustomerOnPrescreenIntro(TMNGData tMNGData) {
		Reporter.log("US585096: TPP - Prescreen - Display Pre Screen Form intro Page");
		Reporter.log("C543869 - PLP: Validate Existing T-mobile Customer CTA");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing should be displayed on PLP ");
		Reporter.log("4. Verify 'Find your pricing' | 'Find your pricing' link should be displayed in PLP");
		Reporter.log("5. Click 'Find your pricing' | pre-screen intro page should be displayed.");
		Reporter.log("6. Verify cta's 'Lets do it' & No thanks | CTA's should be displayed.");
		Reporter.log("7. Verify hyperlinks under CTA's | Existing customer and Sprint customer hyperlinks should be displayed");
		Reporter.log("8. Click Log In link for Existing customer | User should navigate to my.t-mobile.com/shop");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlp = navigateToPhonesPlpPage(tMNGData);
		phonesPlp.verifyCRPorAwesomePricesInPLP();
		phonesPlp.verifyYourPricingBoxInPLP();
		phonesPlp.verifyYourPricingBoxTextInPLP();
		phonesPlp.verifyFindYourPricingLink();
		phonesPlp.clickFindYourPricingLink();
		TPPPreScreenIntro tppIntro = new TPPPreScreenIntro(getDriver());
		tppIntro.verifyPreScreenIntroPage();
		tppIntro.verifyLetsGoCTAOnPreScreenIntroPage();
		tppIntro.verifyNoThanksCtaOnPrescreenIntroPage();
		tppIntro.verifyExistingCustomerLink();
		tppIntro.verifySprintCusotmerLink();
		tppIntro.clickExistingCusotmerLink();
		HomePage myTMOShop = new HomePage(getDriver());
		myTMOShop.verifyMyTMOLoginPage();
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.performLoginAction(tMNGData.getLoginEmailOrPhone(), tMNGData.getPassword());
		loginPage.verifyLoginPageLoaded();
		loginPage.verifyLoginPage();
	}	
	
	/**
	 *  US588459 :TPP - Prescreen Fail - Display Pre Screen No Offer Page
	 * 	C543891 - PLP:Pre-screen Fail - Pre screen No Offer Page
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.DESKTOP ,  Group.TPP })
	public void testVerifyPrescreenFailNoOfferPage(TMNGData tMNGData) {
		Reporter.log(" US588459 :TPP - Prescreen Fail - Display Pre Screen No Offer Page");
		Reporter.log("C543891 - PLP:Pre-screen Fail - Pre screen No Offer Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Click 'Find your pricing' link | Pre-screen intro page should be displayed.");		
		Reporter.log("4. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log("5. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("6. Enter required information and  click on the 'Find my price' CTA | pre-screen processing page should be displayed.");
		Reporter.log("7. Verify processing page is displayed when the pre-screen is being done  | Processing page should be displayed after pre-screen");
		Reporter.log("8. Verify header on proceessing page  | 'Pre screen No Offer Page' header on Processing page should be displayed.");
		Reporter.log("9. Compare the page with the zepline | Page should include all requred fields");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlp = navigateToPhonesPlpPage(tMNGData);
		phonesPlp.verifyCRPorAwesomePricesInPLP();
		phonesPlp.verifyYourPricingBoxInPLP();
		phonesPlp.verifyYourPricingBoxTextInPLP();
		phonesPlp.verifyFindYourPricingLink();
		phonesPlp.clickFindYourPricingLink();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPrescreenform = new TPPPreScreenForm(getDriver());
		tppPrescreenform.verifyPreScreenForm();
		tppPrescreenform.fillAllFieldsOnPreScreenform(tMNGData);
		tppPrescreenform.clickFindMyPriceCTAOnPreScreenForm();
		tppPrescreenform.verifyPreScreenloadingPage();
		TPPNoOfferPage tppNoOfferPage = new TPPNoOfferPage(getDriver());
		tppNoOfferPage.verifyPreScreenNoOfferPageURL();
		tppNoOfferPage.verifyPrescreenNoOfferPage();
		tppNoOfferPage.verifyPrescreenNoOfferPageTextFields();
	}

	/**
	 * US575533:TPP - Prescreen - Display Prescreen form page
	 * C543872 - PLP:UI Validation for Pre-Screen form page
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.DESKTOP,  Group.TPP })
	public void verifyPrescreenFormPageFromPLP(TMNGData tMNGData) {
		Reporter.log("US575533:TPP - Prescreen - Display Prescreen form page");
		Reporter.log("C543872 - PLP:UI Validation for Pre-Screen form page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing" +
				" should be displayed on PLP ");
		Reporter.log("4. Verify 'Find your pricing' link | 'Find your pricing' link should be displayed in PLP");
		Reporter.log("5. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("6. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log("7. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("8. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("9. Verify the page header | 'This isn't a credit check...' text should be displayed");
		Reporter.log("10. Verify the text under page header | Text fields- 'We just need a little bit of info...' should be displayed");
		Reporter.log("11. Verify the personal info section | Editable fields First Name,Last name, Email address Address, Zipcode, Birth date MM/DD/YYYY, Last 4 of SSN should be displayed");
		Reporter.log("12. Verify placeholder texts for all fields | Respective texts should be displayed");
		Reporter.log("13. Verify the Primary CTA 'Find my price' | 'Find my price' should be in grayed out state until all fields are entered");
		Reporter.log("14. Verify secondary CTA - 'Skip for now' | 'Skip for now' cta should be displayed");
		Reporter.log("15. Verify legal text below the CTA | legal text should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		PlpPage phonesPlp = navigateToPhonesPlpPage(tMNGData);
		phonesPlp.verifyFindYourPricingLink();
		phonesPlp.clickFindYourPricingLink();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPrescreenform = new TPPPreScreenForm(getDriver());
		tppPrescreenform.verifyPreScreenForm();
		tppPrescreenform.verifyPreScreenFormPageTitle();
		tppPrescreenform.verifyPrescreenFormPageHeader();
		tppPrescreenform.verifyTextUnderPrescreenFormPageHeader();
		tppPrescreenform.verifyEditableFieldsSection();
		tppPrescreenform.verifyFindMyPriceCta();
		tppPrescreenform.verifySkipForNowCta();
		tppPrescreenform.verifyLegalTextatBottom();
	}

	/**
	 * US575533:TPP - Prescreen - Display Prescreen form page
	 * C543883 - PLP:Verify Find my price CTA - Manual review Page
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP , Group.TPP })
	public void verifyManualReviewPageForFindMyPriceCTAFromPLP(TMNGData tMNGData) {
		Reporter.log("US575533:TPP - Prescreen - Display Prescreen form page");
		Reporter.log("C543883 - PLP:Verify Find my price CTA - Manual review Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing" +
				" should be displayed on PLP ");
		Reporter.log("4. Verify 'Find your pricing' link on PLP | 'Find your pricing' link should be displayed");
		Reporter.log("5. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("6. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log("7. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("8. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("9. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("10. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log("11. Verify pre screen page after loading page | Manual review should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlp = navigateToPhonesPlpPage(tMNGData);
		phonesPlp.verifyCRPorAwesomePricesInPLP();
		phonesPlp.verifyYourPricingBoxInPLP();
		phonesPlp.verifyYourPricingBoxTextInPLP();
		phonesPlp.verifyFindYourPricingLink();
		phonesPlp.clickFindYourPricingLink();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPrescreenform = new TPPPreScreenForm(getDriver());
		tppPrescreenform.verifyPreScreenForm();
		tppPrescreenform.verifyPreScreenFormPageTitle();
		tppPrescreenform.verifyPrescreenFormPageHeader();
		tppPrescreenform.verifyTextUnderPrescreenFormPageHeader();
		tppPrescreenform.fillAllFieldsOnPreScreenform(tMNGData);
		tppPrescreenform.clickFindMyPriceCTAOnPreScreenForm();
		tppPrescreenform.verifyPreScreenloadingPage();
		TPPManualorFraudReviewPage tppManualorFraudReviewPage = new TPPManualorFraudReviewPage(getDriver());
		tppManualorFraudReviewPage.verifyManualRevieworFraudDetectionPage();
	}

	/**
	 * US565403:TPP - Prescreen- Load manual review/fraud page (CART/PLP/PDP)
	 * C543915 - PLP:Pre-screen Fail - "Call 1-800-T-Mobile"
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT , Group.TPP })
	public void testCall1800TMobileCtaOnManualReviewPageFromPLP(TMNGData tMNGData) {
		Reporter.log("US565403:TPP - Prescreen- Load manual review/fraud page (CART/PLP/PDP)");
		Reporter.log("C543915 - PLP:Pre-screen Fail - 'Call 1-800-T-Mobile' ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing" +
				" should be displayed on PLP ");
		Reporter.log("4. Verify 'Find your pricing' link on PLP | 'Find your pricing' link should be displayed");
		Reporter.log("5. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("6. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log("7. Click on 'Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("8. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("9. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("10. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log("11. Verify pre screen page after loading page | Manual review/Fraud test page should be displayed");
		Reporter.log("12. Click on CTA : 'Call 1-800-T-Mobile' | dialer on mobile, or skype for desktop should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlp = navigateToPhonesPlpPage(tMNGData);
		phonesPlp.verifyCRPorAwesomePricesInPLP();
		phonesPlp.verifyYourPricingBoxInPLP();
		phonesPlp.verifyYourPricingBoxTextInPLP();
		phonesPlp.verifyFindYourPricingLink();
		phonesPlp.clickFindYourPricingLink();
		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPrescreenform = new TPPPreScreenForm(getDriver());
		tppPrescreenform.verifyPreScreenForm();
		tppPrescreenform.verifyPreScreenFormPageTitle();
		tppPrescreenform.verifyPrescreenFormPageHeader();
		tppPrescreenform.verifyTextUnderPrescreenFormPageHeader();
		tppPrescreenform.fillAllFieldsOnPreScreenform(tMNGData);
		tppPrescreenform.clickFindMyPriceCTAOnPreScreenForm();
		tppPrescreenform.verifyPreScreenloadingPage();
		TPPManualorFraudReviewPage tppManualorFraudReviewPage = new TPPManualorFraudReviewPage(getDriver());
		tppManualorFraudReviewPage.verifyManualRevieworFraudDetectionPage();
		tppManualorFraudReviewPage.verifyCallTMobileCTA();
		tppManualorFraudReviewPage.clickCallTMobileCTA();
		//tppManualorFraudReviewPage.verifyDialerOnMobile();

	}
	
	/**
	 * US585762 : TPP - Display Base Customer experience on PLP for customers who are cookied as a MyTMO customer (logged in or not logged in)
	 * US602128 : [Continued] As a cookied/base-customer user, I'd like to see pricing based on my CRP on PLP, so that I get the most accurate pricing.
	 * C547640 : Verify for cookied customer or MY TMO customer TPP functionality is hidden
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.DESKTOP , Group.TPP })
	public void testVerifyCookedUserTPPFlowinPLP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test  US585762 : TPP - Display Base Customer experience on PLP for customers who are cookied as a MyTMO customer (logged in or not logged in)");
		Reporter.log("Test  US602128 : [Continued] As a cookied/base-customer user, I'd like to see pricing based on my CRP on PLP, so that I get the most accurate pricing.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Phones Link | Phones page should be displayed");
		Reporter.log("5. Verify 'Check your price' in PLP | 'Check your price' should not be displayed for cooked user.");
		Reporter.log("6. Verify EIP downpayment, monthly payment and loan term length that corresponds to customers CRP in PLP | EIP downpayment, monthly payment and loan term length that corresponds to customers CRP should be displayed for cooked user.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToMyTMOHome(tMNGData);		
		PlpPage phonesPage = navigateToPhonesPlpPage(tMNGData);
		phonesPage.verifyFindYourPricingDisable();
		phonesPage.verifyEIPPayment();
		phonesPage.verifyLoanTermLength();
		phonesPage.verifyFRPPrice();		
		phonesPage.verifyDownPaymentPrice();
	}
	
	/**
	 * US585762 : TPP - Display Base Customer experience on PLP for customers who are cookied as a MyTMO customer (logged in or not logged in)
	 * US602128 : [Continued] As a cookied/base-customer user, I'd like to see pricing based on my CRP on PLP, so that I get the most accurate pricing.
	 * C547640 : Verify for cookied customer or MY TMO customer TPP functionality is hidden
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.DESKTOP, Group.TPP })
	public void testVerifyCookedUserTPPFlowinTabletPLP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test  US585762 : TPP - Display Base Customer experience on PLP for customers who are cookied as a MyTMO customer (logged in or not logged in)");
		Reporter.log("Test  US602128 : [Continued] As a cookied/base-customer user, I'd like to see pricing based on my CRP on PLP, so that I get the most accurate pricing.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Phones Link | Phones page should be displayed");
		Reporter.log("5. Verify 'Check your price' in PLP | 'Check your price' should not be displayed for cooked user.");
		Reporter.log("6. Verify EIP downpayment, monthly payment and loan term length that corresponds to customers CRP in PLP | EIP downpayment, monthly payment and loan term length that corresponds to customers CRP should be displayed for cooked user.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToMyTMOHome(tMNGData);		
		PlpPage tabletPage = navigateToTabletsAndDevicesPLP(tMNGData);
		tabletPage.verifyFindYourPricingDisable();
		tabletPage.verifyEIPPayment();
		tabletPage.verifyLoanTermLength();
		tabletPage.verifyFRPPrice();		
		tabletPage.verifyDownPaymentPrice();
	}
	
	/**
	 *	US520907:  As a user (ANY)  I would like to see the promotion offered for each device as I browse the PLP page based on the transaction type, so that I can shop for devices based on promotions.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP , Group.TPP })
	public void testVerifyPromoOfferForCookedUserInPLP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test  US520907:  As a user (ANY)  I would like to see the promotion offered for each device as I browse the PLP page based on the transaction type, so that I can shop for devices based on promotions.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Phones Link | Phones page should be displayed");
		Reporter.log("5. Verify Promo Offer for devices in PLP |  Promo Offer should be displayed for Phones in PLP for cooked user.");
		Reporter.log("6. Click on Promo Offer link for on of the device in PLP |  Promo Offer modal should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToMyTMOHome(tMNGData);

		PlpPage phonesPage = navigateToPhonesPlpPage(tMNGData);
		phonesPage.verifyPromotionTextonPLP();
		phonesPage.clickPromotionTextonPLP();
		phonesPage.verifyPromotionModal();
		phonesPage.verifyDeviceNameOnPromotionModal();
		phonesPage.verifyPromotionTextOnPromotionModal();

	}
	
	/**
	 *	US520907:  As a user (ANY)  I would like to see the promotion offered for each device as I browse the PLP page based on the transaction type, so that I can shop for devices based on promotions.
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP , Group.TPP })
	public void testVerifyPromoOfferForCookedUserInTabletPLP(ControlTestData data, TMNGData tMNGData) {
		Reporter.log("Test  US520907:  As a user (ANY)  I would like to see the promotion offered for each device as I browse the PLP page based on the transaction type, so that I can shop for devices based on promotions.");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the MyTmo application | MyTmo Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Launch TMO URL  | Application Should be Launched");
		Reporter.log("4. Click on Phones Link | Phones page should be displayed");
		Reporter.log("5. Verify Promo Offer for devices in PLP |  Promo Offer should be displayed for Phones in PLP for cooked user.");
		Reporter.log("6. Click on Promo Offer link for on of the device in PLP |  Promo Offer modal should be displayed.");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToMyTMOHome(tMNGData);

		PlpPage tabletsPage = navigateToTabletsAndDevicesPLP(tMNGData);
		tabletsPage.verifyPromotionTextonPLP();
		tabletsPage.clickPromotionTextonPLP();
		tabletsPage.verifyPromotionModal();
		tabletsPage.verifyDeviceNameOnPromotionModal();
		tabletsPage.verifyPromotionTextOnPromotionModal();

	}
	/**
	 * US609291: TPP - Prescreen - Display Prescreen form page (Updates)
	 * C586071 : Pre-screen form validation
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP , Group.TPP })
	public void testVerifyPreScreenFormValidationsFromPLP(TMNGData tMNGData) {
		Reporter.log("US609291: TPP - Prescreen - Display Prescreen form page (Updates)");
		Reporter.log("C586071 : Pre-screen form validation");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing" +
				" should be displayed on PLP ");
		Reporter.log("4. Verify 'Find your pricing' link on PLP | 'Find your pricing' link should be displayed");
		Reporter.log("5. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("6. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log("7. Click on 'Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("8. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("9. For DOB - for incorrect format validation | Error message should be displayed for Incorrect DOB format");
		Reporter.log("10. For DOB - DOB will be greater than 18 years | Error message should be displayed DOB for below 18 years");
		Reporter.log("11. Check error messages for all fields for data missing validation | Error message should be displayed for all fields for data missing");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlp = navigateToPhonesPlpPage(tMNGData);
		phonesPlp.verifyCRPorAwesomePricesInPLP();
		phonesPlp.verifyYourPricingBoxInPLP();
		phonesPlp.verifyYourPricingBoxTextInPLP();
		phonesPlp.verifyFindYourPricingLink();
		phonesPlp.clickFindYourPricingLink();

		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPrescreenform = new TPPPreScreenForm(getDriver());
		tppPrescreenform.verifyPreScreenForm();
		
		tppPrescreenform.verifyErrorMessagesforAllFieldsForDataMissing();
		tppPrescreenform.verifyErrorMessageForDOBBelow18andIncorrectFormat();

	}
	
	/**
	 * US618022 :TPP - Prescreen- Hide CRID on manual review/fraud page (CART/PLP/PDP)
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.TPP , Group.DESKTOP})
	public void verifyManualReviewPageCTAsFromPLP(TMNGData tMNGData) {
		Reporter.log("US618022 :TPP - Prescreen- Hide CRID on manual review/fraud page (CART/PLP/PDP)");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Verify default CRP 8/Awesome credit pricing is displayed for each device in the PLP  | CRP 8/Awesome credit pricing" +
				" should be displayed on PLP ");
		Reporter.log("4. Verify 'Find your pricing' link on PLP | 'Find your pricing' link should be displayed");
		Reporter.log("5. Click 'Find your pricing' link | pre-screen intro page should be displayed");
		Reporter.log("6. Verify 'Lets Go' cta | Lets Go cta should be displayed");
		Reporter.log("7. Click on Lets Go' cta | User should be navigated to Prescreen FORM(t-mobile.com/pre-screen-form)");
		Reporter.log("8. Verify the page Title | Prescreen Form Page should be displayed");
		Reporter.log("9. Enter all the details in the form | Find my price cta should be enabled");
		Reporter.log("10. Click on 'Find my price' cta | 'pre screen loading page' should be displayed");
		Reporter.log("11. Verify pre screen page after loading page | Manual review should be displayed");
		Reporter.log("12. Verify Schedule an in-store visit CTA | Schedule an in-store visit CTA  should be displayed");
		Reporter.log("13. Verify Call 1-800-TMobile CTA | 1-800-TMobile CTA should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PlpPage phonesPlp = navigateToPhonesPlpPage(tMNGData);
		phonesPlp.verifyCRPorAwesomePricesInPLP();
		phonesPlp.verifyYourPricingBoxInPLP();
		phonesPlp.verifyYourPricingBoxTextInPLP();
		phonesPlp.verifyFindYourPricingLink();
		phonesPlp.clickFindYourPricingLink();

		TPPPreScreenIntro tppPreScreenIntro = new TPPPreScreenIntro(getDriver());
		tppPreScreenIntro.verifyPreScreenIntroPage();
		tppPreScreenIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPrescreenform = new TPPPreScreenForm(getDriver());
		tppPrescreenform.verifyPreScreenForm();
		tppPrescreenform.verifyPreScreenFormPageTitle();
		tppPrescreenform.verifyPrescreenFormPageHeader();
		tppPrescreenform.verifyTextUnderPrescreenFormPageHeader();
		tppPrescreenform.fillAllFieldsOnPreScreenform(tMNGData);
		tppPrescreenform.clickFindMyPriceCTAOnPreScreenForm();
		tppPrescreenform.verifyPreScreenloadingPage();

		TPPManualorFraudReviewPage tppManualorFraudReviewPage = new TPPManualorFraudReviewPage(getDriver());
		tppManualorFraudReviewPage.verifyManualRevieworFraudDetectionPage();
		
		tppManualorFraudReviewPage.verifyscheduleStoreVisitCta();
		tppManualorFraudReviewPage.verifyCallTMobileCTA();

	}
	
	/**
	 *	CDCSM-155 US639721 - TPP R2 - Switch Legal Text position on TPP Form Page
	 * TC-1426 CDCSM-155:TPP R2 - Switch Legal Text position on TPP Form Page
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP })
	public void testVerifyLegalTextPositionOnTPPFormPage ( TMNGData tMNGData) {
		Reporter.log("US639721 - TPP R2 - Switch Legal Text position on TPP Form Page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Click on 'See what i qualify for' link | pre-screen intro page should be displayed");
		Reporter.log("4. Click on 'Let's do it' CTA in pre-screen intro page |pre-screen form page should be displayed ");
		Reporter.log("5. Verify legal text in pre-screen form page |legal text should displayed in pre-screen form page ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PlpPage phonesPlp =navigateToPhonesPlpPage(tMNGData);
		
		phonesPlp.verifyFindYourPricingLink();
		phonesPlp.clickFindYourPricingLink();
		TPPPreScreenIntro tppIntro = new TPPPreScreenIntro(getDriver());
		tppIntro.verifyPreScreenIntroPage();
		tppIntro.clickLetsGoCTAOnPreScreenIntroPage();
		TPPPreScreenForm tppPreScreenform = new TPPPreScreenForm(getDriver());
		tppPreScreenform.verifyPreScreenForm();
		tppPreScreenform.verifyPreScreenFormPageURL();
		tppPreScreenform.verifyLegalTextatBottom();
	}
	
	/**
	 * CDCSM-581: TPP P2 - Legal Compliance - Add Prescreen Opt-out disclaimer on Offer page
	 * TC-2296: Pre-Screen Offer Page : legal message not shown on AEM
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testVerifyLegalComplianceOnOfferPage ( TMNGData tMNGData) {
		Reporter.log("CDCSM-581: TPP P2 - Legal Compliance - Add Prescreen Opt-out disclaimer on Offer page");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Phones PLp page should be displayed");
		Reporter.log("3. Click on 'See what i qualify for' link | pre-screen intro page should be displayed");
		Reporter.log("4. Click on 'Let's do it' CTA in pre-screen intro page | Pre-screen form page should be displayed ");
		Reporter.log("5. Fill pre-screen form page and click 'See your payments' | Verify Offer page is loaded ");
		Reporter.log("6. Verify Opt-out disclaimer on Offer page | VOpt-out disclaimer on Offer page should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		TPPPreScreenForm tPPPreScreenForm =navigateToTPPPreScreenFormPage(tMNGData);
		tPPPreScreenForm.fillAllFieldsOnPreScreenform(tMNGData);
		tPPPreScreenForm.clickFindMyPriceCTAOnPreScreenForm();
		TPPOfferPage tPPOfferPage = new TPPOfferPage(getDriver());
		tPPOfferPage.verifyTPPOfferPage();
		tPPOfferPage.verifyDisclaimerText();
	}
}