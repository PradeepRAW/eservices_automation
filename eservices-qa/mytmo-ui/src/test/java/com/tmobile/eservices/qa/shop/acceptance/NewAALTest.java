package com.tmobile.eservices.qa.shop.acceptance;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.pages.shop.ConsolidatedRatePlanPage;
import com.tmobile.eservices.qa.pages.shop.DeviceIntentPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.InterstitialTradeInPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.pages.shop.TradeInAnotherDevicePage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionValuePage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class NewAALTest extends ShopCommonLib {

	// We dont need this for now ignore
	// @Test(dataProvider = "byColumnName", enabled = true, groups = {
	// Group.PROGRESSION, "AAL01" })
	public void verifyAALKnownIntentEIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify EIP Order Place With AAL Known Intent EIP");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line button in shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product display page should be displayed");
		Reporter.log("8. Select EIP as buying option | EIP should be selected");
		Reporter.log("9. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("10. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log(
				"11. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be dispayed");
		Reporter.log(
				"12. Click on Continue to shiping button | Ship to different address link should be displayed in cart page");
		Reporter.log("13. Click on Continue to payment button | Payment information should be displayed");
		Reporter.log("14. Fill Card details and click continue button | Order conformation page should be displayed");
		Reporter.log("15. Verify order header | Order header should be displayed");
		Reporter.log("16. Verify order Number | Order Number should be displayed");

		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "AAL01" })
	public void verifyAALKnownIntentFRP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify EIP Order Place With AAL Known Intent FRP");

		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line button in shop page | Device intent page should be displayed");
		Reporter.log("6. Click on 'Buy a new Phone' option | Product List page should be displayed");
		Reporter.log("7. Select a product from Product list page | Product details page should be displayed");
		Reporter.log("8. Select FRP as buying option | FRP should be selected");
		Reporter.log("9. Click on 'Add to Cart' button on product display page | Rate plan page should be displayed");
		Reporter.log("10. Click 'Choose a phone' button on Rate plan page | DeviceProtection page should be displayed");
		Reporter.log(
				"11. Click 'Yes protect my Phone' button on Device Protection Page | Cart Page should be dispayed");
		Reporter.log(
				"12. Click on Continue to shiping button | Ship to different address link should be displayed in cart page");
		Reporter.log("13. Click on Continue to payment button | Payment information should be displayed");
		Reporter.log("14. Fill Card details and click continue button | Order conformation page should be displayed");
		Reporter.log("15. Verify order header | Order header should be displayed");
		Reporter.log("16. Verify order Number | Order Number should be displayed");
		Reporter.log("================================");

		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToShippingPageFromAALBuyNewPhoneFlow(myTmoData);
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();

	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "AAL01" })
	public void VerifyOrderConfirmationforAALTradeInKnownIntent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify AAL TradeIn Order Place With AAL UNKnown Intent BYOD");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User should login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on Add a Line button from quicklinks | Customer intent page should be displayed");
		Reporter.log("6. Select on 'Buy a new phone' option | Consolidated Rate Plan Page should be displayed");
		Reporter.log("7. Click Contune button | PLP page should be displayed ");
		Reporter.log("8. Select device | PDP page should be displayed ");
		Reporter.log("9. Click Continue button | Interstitial page should be displayed ");
		Reporter.log(
				"10. Click on 'Yes, I want to trade in a device' | Trade-in another device page should be displayed");
		Reporter.log("11. Verify Authorable title | Authorable title should be displayed");
		Reporter.log("12. Select Carrier, make, model and enter valid IMEI no | Continue button should be enabled");
		Reporter.log("13. Click on Continue button | Device condition page should be displayed");
		Reporter.log(
				"14. Verify good condition authorable title | Good condition authorable title should be displayed");
		Reporter.log("15. Select good condition option | Device value page should be displayed");
		Reporter.log("16. Verify Trade-In this Phone CTA | Trade-In this Phone CTA should be displayed");
		Reporter.log("17. Click on Trade-In this Phone CTA | Device protection page should be displayed");
		Reporter.log("18. Click on Continue button | Accessories Plp page should be displayed");
		Reporter.log("19. Click on Skip Accessories button | Cart page should be displayed");
		Reporter.log("20. Click on 'Continue to Shipping' button | Shipping details should be displayed");
		Reporter.log("21. Click on 'Continue to Payment' button | Payment details should be displayed");
		Reporter.log(
				"22. Verify 'DRP Customer Agreement' & 'DRP Terms & Conditions' links avaialbility | 'DRP Customer Agreement' & 'DRP Terms & Conditions' links should"
						+ " be displayed and clickable");
		Reporter.log("23. Fill Card details and click continue button | Order conformation page should be displayed");
		Reporter.log("24. Verify order header | Order header should be displayed");
		Reporter.log("25. Verify order Number | Order Number should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToInterstitialTradeInPageFromAALBuyNewPhoneFlow(myTmoData);
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.clickOnYesWantToTradeInCTA();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();
		tradeInAnotherDevicePage.verifyTradeInAnotherDeviceTitle();
		tradeInAnotherDevicePage.selectTradeInDeviceOptionsBasedOnDevice(myTmoData);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage TradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		TradeInDeviceConditionValuePage.verifyGoodTitleTextOnDeviceConditionAAL();
		TradeInDeviceConditionValuePage.clickOnGoodCondition();
		TradeInDeviceConditionValuePage tradeInDeviceConditionValuePage = new TradeInDeviceConditionValuePage(
				getDriver());
		tradeInDeviceConditionValuePage.verifyTradeInDeviceConditionValuePage();
		tradeInDeviceConditionValuePage.verifyContinueTradeInButton();
		tradeInDeviceConditionValuePage.clickContinueTradeInButton();
		DeviceProtectionPage deviceProtectionpage = new DeviceProtectionPage(getDriver());
		deviceProtectionpage.verifyDeviceProtectionPage();
		deviceProtectionpage.clickOnYesProtectMyPhoneButton();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();

		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "AAL02" })
	public void testAALUnknownEIP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify EIP Order Place With AAL UNKnown Intent EIP");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select device | Product details page should be displayed");
		Reporter.log("6. Select EIP Price as payment selection dropdown | Dropdown item should be selected");
		Reporter.log("7. Click on Add a Line button | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("8. Click Contune button | Interstitial page should be displayed ");
		Reporter.log("9. Select on 'No thanks, skip trade-in' | PHP page should be displayed");
		Reporter.log("10. Click Continue button | Accessories list should be displayed");
		Reporter.log("11. Click Skip accessories button | Cart Page should be displayed");
		Reporter.log("12. Click on continue button  | Cart page should be displayed");
		Reporter.log(
				"13. Click on Continue to shiping button | Ship to different address link should be displayed in cart page");
		Reporter.log("14. Click on Continue to payment button | Payment information should be displayed");
		Reporter.log("14. Fill Card details and click continue button | Order conformation page should be displayed");
		Reporter.log("15. Verify order header | Order header should be displayed");
		Reporter.log("16. Verify order Number | Order Number should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}

	// We dont need this for now ignore
	// @Test(dataProvider = "byColumnName", enabled = true, groups =
	// {Group.PROGRESSION, "AAL02"})
	public void testAALDeposit(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify EIP Order Place With AAL Deposit");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select device | Product details page should be displayed");
		Reporter.log("6. Select EIP Price as payment selection dropdown | Dropdown item should be selected");
		Reporter.log("7. Click on Add a Line button | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("8. Click Contune button | Interstitial page should be displayed ");
		Reporter.log("9. Select on 'No thanks, skip trade-in' | PHP page should be displayed");
		Reporter.log("10. Click Continue button | Accessories list should be displayed");
		Reporter.log("11. Click Skip accessories button | Cart Page should be displayed");
		Reporter.log("12. Click on continue button  | Cart page should be displayed");
		Reporter.log(
				"13. Click on Continue to shiping button | Ship to different address link should be displayed in cart page");
		Reporter.log("14. Click on Continue to payment button | Payment information should be displayed");
		Reporter.log("14. Fill Card details and click continue button | Order conformation page should be displayed");
		Reporter.log("15. Verify order header | Order header should be displayed");
		Reporter.log("16. Verify order Number | Order Number should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "AAL02" })
	public void testAALUnknownFRP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify FRP Order Place With AAL UNKnown Intent FRP");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select device | Product details page should be displayed");
		Reporter.log("6. Select FRP Price as payment selection dropdown | Dropdown item should be selected");
		Reporter.log("7. Click on Add a Line button | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("8. Click Contune button | Interstitial page should be displayed ");
		Reporter.log("9. Select on 'No thanks, skip trade-in' | PHP page should be displayed");
		Reporter.log("10. Click Continue button | Accessories list should be displayed");
		Reporter.log("11. Click Skip accessories button | Cart Page should be displayed");
		Reporter.log("12. Click on continue button  | Cart page should be displayed");
		Reporter.log(
				"13. Click on Continue to shiping button | Ship to different address link should be displayed in cart page");
		Reporter.log("14. Click on Continue to payment button | Payment information should be displayed");
		Reporter.log("15. Fill Card details and click continue button | Order conformation page should be displayed");
		Reporter.log("16. Verify order header | Order header should be displayed");
		Reporter.log("17. Verify order Number | Order Number should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickOnAddALineButton();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (getContentFlagsvalue("byPassInsuranceMigrationPage") == "false") {
			if (deviceProtectionPage.verifyDeviceProtectionURL()) {
				deviceProtectionPage.clickContinueButton();
			}
		}
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION, "AAL03" })
	public void testBYODOrderConfirmation(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify BYOD Order Place With AAL UNKnown Intent BYOD");
		Reporter.log("Data Condition |  PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on 'Add A LINE' button | Customer intent page should be displayed");
		Reporter.log("5. Select on 'Buy my own device' option | Consolidated Rate Plan Page  should be displayed");
		Reporter.log("6. Click Continue button on Consolidated Rate Plan Page | PHP page should be displayed");
		Reporter.log("7. Click Continue button on PHP page | Accessory PLP Page should be displayed");
		Reporter.log("8. Click Skip accessories button on Accessory PLP page | Cart Page should be displayed");
		Reporter.log(
				"9. Click on Continue to shiping button | Ship to different address link should be displayed in cart page");
		Reporter.log("10. Click on Continue to payment button | Payment information should be displayed");
		Reporter.log("11. Fill Card details and click continue button | Order conformation page should be displayed");
		Reporter.log("12. Verify order header | Order header should be displayed");
		Reporter.log("13. Verify order Number | Order Number should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.clickQuickLinks("ADD A LINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickUseMyPhoneOption();

		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();

		/*
		 * DeviceProtectionPage deviceProtectionPage = new
		 * DeviceProtectionPage(getDriver());
		 * deviceProtectionPage.verifyDeviceProtectionPage();
		 * deviceProtectionPage.clickDeviceProtectionSoc();
		 * deviceProtectionPage.clickContinueButton();
		 * 
		 * AccessoryPLPPage accessoryPLPPage = new AccessoryPLPPage(getDriver());
		 * accessoryPLPPage.clickPayMonthlyPopupCloseButton();
		 * accessoryPLPPage.verifyAccessoryPLPPage();
		 * accessoryPLPPage.clickSkipAccessoriesCTA();
		 */

		CartPage cartPage = new CartPage(getDriver());
		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}
}
