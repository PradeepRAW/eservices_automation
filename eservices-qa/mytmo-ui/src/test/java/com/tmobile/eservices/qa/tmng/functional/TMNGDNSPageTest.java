package com.tmobile.eservices.qa.tmng.functional;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;
import com.tmobile.eservices.qa.pages.tmng.functional.TMNGNewFooterPage;
import com.tmobile.eservices.qa.pages.DNSCommonPage;

public class TMNGDNSPageTest extends TmngCommonLib {
	//private static final Logger logger = LoggerFactory.getLogger(TMNGNewHeaderPageTest.class);
	/**
	 * CDCDWR2-42 - CCPA WS4 | No Sell Web, Sell/No Sell App | DNS Page | UI
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void verifyDNSPage(TMNGData tMNGData){
		Reporter.log("CDCDWR2-42 - CCPA WS4 | No Sell Web, Sell/No Sell App | DNS Page | UI");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify DNS Link is displayed | DNS Link is displayed");
		Reporter.log("4.Click on DNS Link | Clicked on DNS Link successfully");
		Reporter.log("5.Verify DNS page | DNS page should be displayed");
		Reporter.log("6.Verify DNS Page Title is displayed | DNS Page Title is displayed");
		Reporter.log("7.Verify DNS Legal Content | DNS Legal Content is displayed");
		Reporter.log("8.Verify Learn More | Learn More should be displayed");
		Reporter.log("9.Verify DNS Global Toggle | Verify Global Toggle is displayed");
		Reporter.log("10.Verify Local Toggle | Verify Local Toggle should be displayed");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifyDoNotSellLabel();
		unavPage.clickDoNotSellLink();
		unavPage.verifyDoNotSellPage();
		DNSCommonPage dnsPage = new DNSCommonPage(getDriver());
	    dnsPage.verifyDNSPage();
		dnsPage.verifyDNSTitle();
		dnsPage.verifyDNSLegalContent();
		dnsPage.verifyDNSCopyMessage();
		dnsPage.verifyDNSLearnMoreLink();
		dnsPage.verifyDNSBrandToggle();
		dnsPage.verifyDNSLocalToggle();
		dnsPage.verifyDNSLoginLink();
	}
	/**
	 * CDCDWR2-33 - CCPA WS2 | All Sites & Apps | Receive Parameters from Originating Site | Authenticated Customer
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void VerifyLocalDNSCookiesInDNSPage(TMNGData tMNGData){
		Reporter.log("CDCDWR2-33 - CCPA WS2 | All Sites & Apps | Receive Parameters from Originating Site | Authenticated Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify DNS Link is displayed | DNS Link is displayed");
		Reporter.log("4.Click on DNS Link | Clicked on DNS Link successfully");
		Reporter.log("5.Verify DNS page | DNS page should be displayed");
		Reporter.log("6.Verify DNS Cookie values for Autenticated Customer | DNS Cookie Values should be set");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifyDoNotSellLabel();
		unavPage.clickDoNotSellLink();
		unavPage.verifyDoNotSellPage();
		DNSCommonPage dnsPage = new DNSCommonPage(getDriver());
	    dnsPage.verifyDNSPage();
	    dnsPage.getAuthorizationValueFromDNSCookie();
	    dnsPage.getUserIDValueFromDNSCookie();
	    dnsPage.getIDTypeValueFromDNSCookie();
	}
	
	/**
	 * CDCDWR2-34 - CCPA WS2 | Sell Web | Receive Parameters from Originating Site | All Customers
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING })
	public void VerifySellWebDNSCookiesInDNSPage(TMNGData tMNGData){
		Reporter.log("CDCDWR2-34 - CCPA WS2 | Sell Web | Receive Parameters from Originating Site | All Customers");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify DNS Link is displayed | DNS Link is displayed");
		Reporter.log("4.Click on DNS Link | Clicked on DNS Link successfully");
		Reporter.log("5.Verify DNS page | DNS page should be displayed");
		Reporter.log("6.Verify DNS Cookie for Sell Web | DNS Cookie Values should be set");
		Reporter.log("========================");
		Reporter.log("Actual Results");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifyDoNotSellLabel();
		unavPage.clickDoNotSellLink();
		unavPage.verifyDoNotSellPage();
		DNSCommonPage dnsPage = new DNSCommonPage(getDriver());
	    dnsPage.verifyDNSPage();
	    dnsPage.getDNSSettingValueFromDNSCookie();
	    dnsPage.getSiteValueFromDNSCookie();
	    dnsPage.getBrandValueFromDNSCookie();
	}
	
	
	/**
	 * CDCDWR2-242 - CCPA | WS2 | TMNG Properties | Send Parameters to DNS Page
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testMagentaBrandingOnDNSPage(TMNGData tMNGData){
		Reporter.log("CDCDWR2-242 - CCPA | WS2 | TMNG Properties | Send Parameters to DNS Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1.Launch the application | Application Should be Launched");
		Reporter.log("2.Verify Home page | Home page should be displayed");
		Reporter.log("3.Verify DNS Link is displayed | DNS Link is displayed");
		Reporter.log("4.Click on DNS Link | Clicked on DNS Link successfully");
		Reporter.log("5.Verify DNS page | DNS page should be displayed");
		Reporter.log("6.Verify DNS Cookie for Sell Web | DNS Cookie Values should be set");
		Reporter.log("7.Verify toggle and links appropriate to magenta brand | Toggle and links should be appropriate to magenta brand");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		navigateToTMobilePage(tMNGData);
		unavPage.verifyDoNotSellLabel();
		unavPage.clickDoNotSellLink();
		unavPage.verifyDoNotSellPage();
		DNSCommonPage dnsPage = new DNSCommonPage(getDriver());
	    dnsPage.verifyDNSPage();
	    dnsPage.getBrandValueFromDNSCookie();
	    dnsPage.getDNSSettingValueFromDNSCookie();
	    dnsPage.getSiteValueFromDNSCookie();

	    
	}
	
	
	/**
	 * CDCDWR2-325 - CCPA | WS2 | All Sites & Apps | Originating Web Page | Integration Support
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testDNSPageIntegrationForUnauthenticated(TMNGData tMNGData,MyTmoData myTmoData){
		Reporter.log("CDCDWR2-242 - CCPA | WS2 | TMNG Properties | Send Parameters to DNS Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Verify DNS Link is displayed | DNS Link is displayed");
		Reporter.log("3. Click on DNS Link | Clicked on DNS Link successfully");
		Reporter.log("4. Verify DNS page | DNS page should be displayed");
		Reporter.log("5. Click on login account link | Click on login account link should be sucess");
		Reporter.log("6. Verify DNS page for unauthenticated | Verifying DNS page for unauthenticated should be success");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		getDriver().navigate().to(" https://uat2.digital.t-mobile.com");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		unavPage.verifyDoNotSellLabel();
		unavPage.clickDoNotSellLink();
		
		DNSCommonPage dnsPage = new DNSCommonPage(getDriver());
	    dnsPage.verifyDNSPage();
	    dnsPage.clickLoginAccountLink();
	    
	    launchAndPerformLogin(myTmoData);
	    
	    dnsPage.verifyDNSUnAuthneticatedContent();
	    
	}
	
	/**
	 * CDCDWR2-325 - CCPA | WS2 | All Sites & Apps | Originating Web Page | Integration Support
	 * @param data
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testDNSPageIntegrationForAuthenticated(TMNGData tMNGData,MyTmoData myTmoData){
		Reporter.log("CDCDWR2-242 - CCPA | WS2 | TMNG Properties | Send Parameters to DNS Page");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Verify DNS Link is displayed | DNS Link is displayed");
		Reporter.log("3. Click on DNS Link | Clicked on DNS Link successfully");
		Reporter.log("4. Verify DNS page | DNS page should be displayed");
		Reporter.log("5. Click on login account link | Click on login account link should be sucess");
		Reporter.log("6. Verify DNS page for unauthenticated | Verifying DNS page for unauthenticated should be success");
		
		Reporter.log("========================");
		Reporter.log("Actual Results");
		
		getDriver().navigate().to(" https://uat2.digital.t-mobile.com");
		TMNGNewFooterPage unavPage = new TMNGNewFooterPage(getDriver());
		unavPage.verifyDoNotSellLabel();
		unavPage.clickDoNotSellLink();
		
		DNSCommonPage dnsPage = new DNSCommonPage(getDriver());
	    dnsPage.verifyDNSPage();
	    dnsPage.clickLoginAccountLink();
	    
	    launchAndPerformLogin(myTmoData);
	    
	    dnsPage.verifyDNSUnAuthneticatedContent();
	    
	}
}