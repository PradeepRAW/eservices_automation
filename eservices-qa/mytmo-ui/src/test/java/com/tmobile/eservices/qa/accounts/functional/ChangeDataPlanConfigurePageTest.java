package com.tmobile.eservices.qa.accounts.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.accounts.PlansConfigurePage;

public class ChangeDataPlanConfigurePageTest extends AccountsCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(ChangeDataPlanConfigurePageTest.class);

	/**
	 * TMO One - Add on ONE Plus - See Cost Breakdown
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyPlanConfigureCostBreakDown(ControlTestData data, MyTmoData myTmoData) {
		logger.info("VerifyCostBreakDown method called in ChangeDataPlanConfigurePageTest");

		Reporter.log("Test Case : Verify Cost break down functionality in Change Data Plan Configure Page");
		Reporter.log("Test Data : Any PAH/Standard Msisdn");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be logged in successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click Plan Link | Plan page should be displayed");
		Reporter.log("5. Click on Change data | Plans configure page should be displayed");
		Reporter.log("6. Click on See Cost Break Down | See Cost Break Down should be displayed");
		Reporter.log("7. Verify Cost Break Down | Cost Break Down should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		if (!navigateToChangeDataPlansConfigurationPage(myTmoData)) {
			PlansConfigurePage plansConfigurePage = new PlansConfigurePage(getDriver());
			plansConfigurePage.verifyPlanConfigurePage();

			// plansConfigurePage.clickOnSeeCostBreakDown();
			plansConfigurePage.verifyHideCostBreakDownText();
		}
	}
}
