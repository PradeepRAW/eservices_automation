package com.tmobile.eservices.qa.shop.acceptance;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.CartPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

/**
 * @author Suresh
 *
 */
public class ShopOrderPlaceTest extends ShopCommonLib {

	/**
	 * Verify EIP Order Place With Skip Trade-In flow through Select feature device
	 * in shop page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, "Standard01" })
	public void testEIPOrderPlaceWithSkipTradeInFlow(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : Verify EIP Order Place With Skip Trade-In flow");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select device | Product details page should be displayed");
		Reporter.log("6. Select EIP Price as payment selection dropdown | Dropdown item should be selected");
		Reporter.log("7. Click Add to cart button | Line selector page should be displayed");
		Reporter.log("8. Select line | Phone selection page should be displayed");
		Reporter.log("9. Click on skip trade-in link | Insurance migration page should be displayed");
		Reporter.log("10. Click on continue button  | Cart page should be displayed");
		Reporter.log(
				"11. Click on Continue to shiping button | Ship to different address link should be displayed in cart page");
		Reporter.log("12. Click on Continue to payment button | Payment information should be displayed");
		Reporter.log("13. Fill Card details and click continue button | Order conformation page should be displayed");
		Reporter.log("14. Verify order header | Order header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByFeatureDevicesWithSKipTradeIn(myTmoData, myTmoData.getDeviceName());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();

	}

	/**
	 * Verify FRP Order Place With Skip Trade-In flow through SeeAllPhones Link in
	 * Shop page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, "AAL04" })
	public void testFRPOrderPlaceWithSkipTradeInFlow(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : Verify FRP Order Place With Skip Trade-In flow");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on SeeAllPhones link | PLP page should be displayed");
		Reporter.log("6. Select device | Product details page should be displayed");
		Reporter.log("7. Select FRP Price as payment selection dropdown | Dropdown item should be selected");
		Reporter.log("8. Click Add to cart button | Line selector page should be displayed");
		Reporter.log("9. Select line | Phone selection page should be displayed");
		Reporter.log("10. Click on skip trade-in link | Insurance migration page should be displayed");
		Reporter.log("11. Click on continue button  | Cart page should be displayed");
		Reporter.log(
				"12. Click on Continue to shiping button | Ship to different address link should be displayed in cart page");
		Reporter.log("13. Click on Continue to payment button | Payment information should be displayed");
		Reporter.log("14. Fill Card details and click continue button | Order conformation page should be displayed");
		Reporter.log("15. Verify order header | Order header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		CartPage cartPage = navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
//		cartPage.clickAcceptAndPlaceOrder();
//		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
//		orderConfirmationPage.verifyOrderConfirmationPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AAL04" })
	public void testFRPWithTradeInFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify test FRP with trade in flow");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Phone page should be displayed");
		Reporter.log("5. Click on select device | PLP page should be displayed");
		Reporter.log("6. Click on the device | PDP page should be displayed");
		Reporter.log("7. Clcik on Add to cart | Line slector page should be displayed");
		Reporter.log("8. Click on any line|Phone selection page should be displayed");
		Reporter.log("9. Click on any line|Phone selection page should be displayed");
		Reporter.log("10. Click on got a different phone|Trade in another device page should be displayed");
		Reporter.log("11. Select trade in info & click continue|Device condition page should be displayed");
		Reporter.log("12. Click its good radio button & click continue|Trade in value page should be displayed");
		Reporter.log("13. click continue|Product list page should be displayed");
		Reporter.log("14. click on device|Product details page should be displayed");
		Reporter.log("15. Select Full Retial Price & Click on add to cart |Cart page should be displayed");
		Reporter.log(
				"16. Enter payment info & click accept & place order | Order confirmation page should be displayed");
		Reporter.log("17. Verify order success message | Order success message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByFeaturedDevicesWithTradeInFlow(myTmoData);

		cartPage.clickContinueToShippingButton();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
//		cartPage.clickAcceptAndPlaceOrder();
//		OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
//		orderConfirmationPage.verifyOrderConfirmationPage();
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING, "AAL03" })
	public void testEIPOrderWithPHP(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : Verify EIP Order Place With Skip Trade-In flow");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Select device | Product details page should be displayed");
		Reporter.log("6. Select EIP Price as payment selection dropdown | Dropdown item should be selected");
		Reporter.log("7. Click Add to cart button | Line selector page should be displayed");
		Reporter.log("8. Select line | Phone selection page should be displayed");
		Reporter.log("9. Click on skip trade-in link | Insurance migration page should be displayed");
		Reporter.log("10. Click on continue button  | Cart page should be displayed");
		Reporter.log(
				"11. Click on Continue to shiping button | Ship to different address link should be displayed in cart page");
		Reporter.log("12. Click on Continue to payment button | Payment information should be displayed");
		Reporter.log("13. Fill Card details and click continue button | Order conformation page should be displayed");
		Reporter.log("14. Verify order header | Order header should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageByFeatureDevicesWithSKipTradeIn(myTmoData, myTmoData.getDeviceName());
		cartPage.verifyCartPage();
		cartPage.clickContinueToShippingButton();
		cartPage.verifyShipToDifferentAddressLink();
		cartPage.clickContinueToPaymentButton();
		cartPage.verifyPaymentInformationForm();
		cartPage.enterFirstname(myTmoData.getFirstName());
		cartPage.enterLastname(myTmoData.getLastName());
		cartPage.enterCardNumber(myTmoData.getCardNumber());
		cartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		cartPage.enterCVV(myTmoData.getPayment().getCvv());
		cartPage.verifyAcceptAndContinue();
		// cartPage.clickAcceptAndPlaceOrder();
		// OrderConfirmationPage orderConfirmationPage = new
		// OrderConfirmationPage(getDriver());
		// orderConfirmationPage.verifyOrderConfirmationPage();
	}
}