package com.tmobile.eservices.qa.accounts.api.LineDetails.responses.line_details;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Line {
	private String msisdn;
	private String imei;
	private String productType;

	@JsonProperty
	private Boolean isLineSuspended;

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public Boolean getLineSuspended() {
		return isLineSuspended;
	}

	public void setLineSuspended(Boolean lineSuspended) {
		isLineSuspended = lineSuspended;
	}
}
