package com.tmobile.eservices.qa.shop.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.shop.ShopConstants;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.path.json.JsonPath;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.AccessoriesApi;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;


public class AccessoriesAPITest extends AccessoriesApi {
	public Map<String, String> tokenMap;
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AccessoriesAPITest","testAccessoriesList",Group.SHOP,Group.APIREG })
	public void testAccessoriesList(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get Accessaries List");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the accessories|Accessaries list should get from service response");
		Reporter.log("Step 2: Verify expected accessories|Accessaries should be in the response list");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 5: Verify accessories list is not null|Accessaries list should display some number.");
		Reporter.log("Step 6: Verify accessories per page |Accessaries should be 12 per page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap=new HashMap<String,String>();
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_ACCESSORIES+ "getAccessoryList.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		//String requestBody="{\"ban\" : \""+apiTestData.getBan()+"\",\"accountType\" : \"I\",\"accountStatus\" : \"OPERATIONAL\",\"billingState\" : \"WA\",\"creditClass\" : \"\",\"resultsPerPage\" : \"\",\"pageNumber\" : \"\",\"manufacturer\" : [ ],\"priceRange\" : [ ],\"catagoryName\" : \"\",\"deviceName\" : \"\",\"sorting\" : \"DESC\"}";
		//String requestBody="{\"resultsPerPage\": 10,\"pageNumber\": 1,\"ban\":\""+apiTestData.getBan()+"\",\"accountType\": \"I\",\"accountStatus\": \"R\",\"sorting\": \"ASC\"}";
		Response response = getAccessoryList(apiTestData,updatedRequest,tokenMap);
/*		Assert.assertTrue(response.statusCode()==200);
		Assert.assertTrue(JsonPath.from(response.asString()).get("totalItems").toString()!=null,"No accessries found");
		Reporter.log("Total Accessaries found:"+JsonPath.from(response.asString()).get("totalItems").toString());
		Assert.assertTrue(JsonPath.from(response.asString()).get("pageSize").toString().equals("10"),"Number of accessories per page is null");
		Reporter.log("Accessaries displaying per page:"+JsonPath.from(response.asString()).get("pageSize").toString());
		Reporter.log("Accessaries list in page1:"+response.jsonPath().getList("accessories.accessoryName"));*/
		String operationName="getAccessoryList";
		logRequest(updatedRequest, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
	}
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AccessoriesAPITest","getAccessoryDetails",Group.SHOP,Group.APIREG })
	public void testAccessoriesDetails(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName:getAccessoryDetails");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the accessories|Accessaries details should get from service response");
		Reporter.log("Step 2: Verify expected accessories|Accessaries details should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap=new HashMap<String,String>();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		String requestBody="{\"ban\" : \""+apiTestData.getBan()+"\",\"accountType\" : \"SPECIAL\",\"accountStatus\" : \"OPERATIONAL\",\"billingState\" : \"WA\",\"creditClass\" : \"E\",\"resultsPerPage\" : \"12\",\"pageNumber\" : \"1\",\"manufacturer\" : [ ],\"priceRange\" : [ ],\"catagoryName\" : \"\",\"deviceName\" : \"\",\"sorting\" : \"DESC\"}";
		String operationName="getAccessoryDetails";
		logRequest(requestBody, operationName);
		Response response =getAccessoryDetails(apiTestData,requestBody,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AccessoriesAPITest","getAccessoryFilter",Group.SHOP,Group.APIREG })
	public void testAccessoriesByDeviceFilter(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get Accessaries By Device Filter");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the accessories|Accessaries list should get from service response");
		Reporter.log("Step 2: Verify expected accessories|Accessaries should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap=new HashMap<String,String>();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		String requestBody="{\"ban\" : \""+apiTestData.getBan()+"\",\"accountType\" : \"SPECIAL\",\"accountStatus\" : \"OPERATIONAL\",\"billingState\" : \"WA\",\"creditClass\" : \"E\",\"resultsPerPage\" : \"12\",\"pageNumber\" : \"1\",\"manufacturer\" : [T-Mobile ],\"priceRange\" : [ ],\"catagoryName\" : \"\",\"deviceName\" : \"\",\"sorting\" : \"DESC\"}";
		Response response =getAccessoryFilter(apiTestData,requestBody,"", tokenMap);
		String operationName="Get Accessaries By Device Filter";
		logRequest(requestBody, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals("0", getPathVal(jsonNode, "statusCode"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
	}
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"AccessoriesAPITest","getAccessariesByCatergory",Group.SHOP,Group.APIREG })
	public void testAccessoriesByCatergory(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getAccessoriesByCatergory");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the accessories|Accessaries list should get from service response");
		Reporter.log("Step 2: Verify expected accessories|Accessaries should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap=new HashMap<String,String>();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		String requestBody="{\"ban\" : \""+apiTestData.getBan()+"\",\"accountType\" : \"SPECIAL\",\"accountStatus\" : \"OPERATIONAL\",\"billingState\" : \"WA\",\"creditClass\" : \"E\",\"resultsPerPage\" : \"12\",\"pageNumber\" : \"1\",\"manufacturer\" : [ ],\"priceRange\" : [ ],\"catagoryName\" : \"Wearable Tech\",\"deviceName\" : \"\",\"sorting\" : \"DESC\"}";
		Response response =getAccessoryFilter(apiTestData,requestBody,"Wearable Tech", tokenMap);
		String operationName="getAccessariesByCatergory Filter";
		logRequest(requestBody, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Reporter.log(" <b>Response :</b> ");
			Reporter.log(response.body().asString());
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals("0", getPathVal(jsonNode, "statusCode"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory# Description:
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AccessoriesAPITest","getAccessoriesByManufacturer",Group.SHOP,Group.APIREG })
	public void testAccessoriesByManufacturer(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get Accessories List");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the accessories|Accessaries list should get from service response");
		Reporter.log("Step 2: Verify expected accessories|Accessaries should be in the response list");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		tokenMap=new HashMap<String,String>();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		String requestBody="{\"ban\" : \""+apiTestData.getBan()+"\",\"accountType\" : \"SPECIAL\",\"accountStatus\" : \"OPERATIONAL\",\"billingState\" : \"WA\",\"creditClass\" : \"E\",\"resultsPerPage\" : \"12\",\"pageNumber\" : \"1\",\"manufacturer\" : [Samsung],\"priceRange\" : [ ],\"catagoryName\" : \"\",\"deviceName\" : \"\",\"sorting\" : \"DESC\"}";
		Response response =getAccessoryFilter(apiTestData,requestBody,"Samsung",tokenMap);
		String operationName="getAccessariesByManufacturer Filter";
		logRequest(requestBody, operationName);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals("0", getPathVal(jsonNode, "statusCode"));
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
		
	}
	
	/**
	 * UserStory#US611338   Description:only accessories for chosen phone
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "AccessoriesAPITest","testAccessoriesList",Group.SHOP,Group.APIREG })
	public void testAccessoriesListDifference(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: Get Accessaries List");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get all the accessories|Accessaries list should get from service response");
		Reporter.log("Step 2: Verify expected accessories|Accessaries should be in the response list");
		Reporter.log("Step 4: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("Step 5: Verify accessories list is not null|Accessaries list should display some number.");
		Reporter.log("Step 6: Verify accessories per page |Accessaries should be 12 per page.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		
		tokenMap=new HashMap<String,String>();
		String requestBody = new ServiceTest().getRequestFromFile(ShopConstants.SHOP_ACCESSORIES+ "getAccessoryListDifference.txt");
		tokenMap.put("ban", apiTestData.getBan());
		String operationName = "Get Accessory";
		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, operationName);
		Response response =getAccessoryList1(apiTestData, updatedRequest,tokenMap);

		
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertTrue(response.statusCode()==200);
				JsonPath jsonPath= new JsonPath(response.asString());
				Assert.assertNotNull(jsonPath.get("statusMessage"),"Accessories not retrieved successfully");	
				Reporter.log("Accessories retrieved successfully");
				
			}
		} else {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("Response : " + response.body().asString());
			failAndLogResponse(response, operationName);
		}
	}

}
