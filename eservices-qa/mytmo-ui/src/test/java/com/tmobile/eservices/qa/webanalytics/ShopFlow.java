package com.tmobile.eservices.qa.webanalytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.shop.LineSelectorDetailsPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.TradeInAnotherDevicePage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionConfirmationPage;
import com.tmobile.eservices.qa.pages.shop.TradeInDeviceConditionValuePage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import com.tmobile.eservices.qa.shop.ShopConstants;

public class ShopFlow extends ShopCommonLib {
	
	AnalyticsLib al = new AnalyticsLib();
	SoftAssert softAssert = new SoftAssert();
	
	/***
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"analytics"})
	public void testShopPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5", "MyTMO | Shop : Lander");
		pageTags.put("pageName", "MyTMO | Shop : Lander");
		pageTags.put("v12", "Standard Upgrade");
		pageTags.put("v120", "Standard Upgrade");
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"demo"})
	public void testShopPageAnalyticsFAILScenario(ControlTestData data, MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5", "MyTMO | Shop : Lander");
		pageTags.put("pageName", "MyTMO | Shop : Lander");
		pageTags.put("v12", "Standard Upgrade");
		pageTags.put("v120", "Standard Upgrade");
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
	/***
	 * just for demo
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"demo"})
	public void testShopPageAnalyticsPASSScenario(ControlTestData data, MyTmoData myTmoData) {
		navigateToShopPage(myTmoData);
		
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5", "MyTMO | Shop : Lander");
		pageTags.put("pageName", "MyTMO | Shop : Lander");
		pageTags.put("v12", "Standard Upgrade");
		//pageTags.put("v120", "Standard Upgrade");
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
	/***
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"analytics"})
	public void testProductListPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		navigateToPLPPageBySeeAllPhones(myTmoData);
		
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5", "MyTMO | Shop : Devices : Phones : Browse");
		pageTags.put("pageName", "MyTMO | Shop : Devices : Phones : Browse");
		pageTags.put("v12", "Standard Upgrade");
		pageNotNullTags.add("v10");
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
	/***
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"analytics"})
	public void testProductDetailsPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		navigateToPDPPageBySeeAllPhones(myTmoData);
		
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5", "MyTMO | Shop : Devices : Phones : Samsung Galaxy S9 ");
		pageTags.put("pageName", "MyTMO | Shop : Devices : Phones : Samsung Galaxy S9 ");
		pageTags.put("v12", "Standard Upgrade");
		//TODO products
		pageNotNullTags.add("v10");
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
	/***
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"analytics"})
	public void testLineSelectionPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSelectorPageBySeeAllPhones(myTmoData);
		
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5", "MyTMO | Shop : Upgrade Line Selector");
		pageTags.put("pageName", "MyTMO | Shop : Upgrade Line Selector");
		pageTags.put("v12", "Standard Upgrade");
		pageNotNullTags.add("v10");
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
	/***
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"analytics"})
	public void testPhoneSelectionPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		navigateToLineSelectorDetailsPageBySeeAllPhones(myTmoData);
		
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5", "MyTMO | Shop : Device Trade-in Selector");
		pageTags.put("pageName", "MyTMO | Shop : Device Trade-in Selector");
		pageTags.put("v12", "Standard Upgrade");
		pageNotNullTags.add("v10");
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
	/***
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"analytics"})
	public void testTradeInDeviceConditionPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		LineSelectorPage lineSelectorPage = navigateToLineSelectorPageByFeatureDevices(myTmoData);
		lineSelectorPage.clickDevice();
		LineSelectorDetailsPage lineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
		lineSelectorDetailsPage.verifyLineSelectionDetailsPage();
		lineSelectorDetailsPage.clickOnWantToTradeInDifferentPhone();
		TradeInAnotherDevicePage tradeInAnotherDevicePage = new TradeInAnotherDevicePage(getDriver());
		tradeInAnotherDevicePage.verifyTradeInAnotherDevicePage();

		tradeInAnotherDevicePage.selectCarrier(ShopConstants.CARRIER_DROPDOWN_OPTION);
		tradeInAnotherDevicePage.selectMake(ShopConstants.MAKE_DROPDOWN_OPTION);
		tradeInAnotherDevicePage.selectModel(ShopConstants.MODEL_DROPDOWN_OPTION);
		tradeInAnotherDevicePage.setIMEINumber(ShopConstants.VALID_IMEI);
		tradeInAnotherDevicePage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5", "MyTMO | Shop : Device Trade-In Condition");
		pageTags.put("pageName", "MyTMO | Shop : Device Trade-In Condition");
		pageTags.put("v12", "Standard Upgrade");
		pageNotNullTags.add("v10");
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
	/***
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"analytics"})
	public void testTradeInValuePageAnalytics(ControlTestData data, MyTmoData myTmoData) {
		TradeInDeviceConditionConfirmationPage tradeInDeviceConditionConfirmationPage = navigateToTradeInDeviceConditionConfirmationPageByFeatureDevicesWithTradeInFlow(myTmoData,
				myTmoData.getDeviceName());		
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5", "MyTMO | Shop : Device Trade-In Value");
		pageTags.put("pageName", "MyTMO | Shop : Device Trade-In Value");
		pageTags.put("v12", "Standard Upgrade");
		pageNotNullTags.add("v101");
		pageNotNullTags.add("v10");
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
	/***
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"analytics"})
	public void testCartPageAnalyticsWhenViewOrder(ControlTestData data, MyTmoData myTmoData) {
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5", "MyTMO | Shop : Cart : Order Detail");
		pageTags.put("v12", "Standard Upgrade");
		pageNotNullTags.add("v101");
		pageNotNullTags.add("v10");
		pageNotNullTags.add("v107");
		pageNotNullTags.add("v114");
		//TODO products
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
	/***
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"analytics"})
	public void testCartPageAnalyticsWhenViewShippingInfo(ControlTestData data, MyTmoData myTmoData) {
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5", "MyTMO | Shop : Cart : Shipping Options");
		pageTags.put("v12", "Standard Upgrade");
		pageNotNullTags.add("v10");
		pageNotNullTags.add("v27");
		pageNotNullTags.add("v107");
		pageNotNullTags.add("v114");
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
	/***
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {"analytics"})
	public void testCartPageAnalyticsWhenViewPaymentInfo(ControlTestData data, MyTmoData myTmoData) {
		navigateToCartPageBySeeAllPhonesWithSkipTradeIn(myTmoData);
		
		HashMap<String, String> pageTags = new HashMap<String, String>();
		List<String> pageNotNullTags = new ArrayList<>();
		pageTags.put("v5", "MyTMO | Shop : Cart : Payment Method");
		pageTags.put("v12", "Standard Upgrade");
		pageNotNullTags.add("v10");
		pageNotNullTags.add("v27");
		pageNotNullTags.add("v107");
		pageNotNullTags.add("v114");
		al.verifyPagePdl(getDriver(), pageTags,pageNotNullTags);
	}
	
//	@Test(dataProvider = "byColumnName", enabled = true, groups = {"analyticsDebug"})
//	public void testPhoneSelectionPageAnalytics(ControlTestData data, MyTmoData myTmoData) {
//		navigateToPhoneSelectionPageBySeeAllPhones(myTmoData);
//		
//		//al.verifyPagePdl(getDriver(), "clickEvent");
//		
////		HashMap<String, String> pageTags = new HashMap<String, String>();
////		List<String> pageNotNullTags = new ArrayList<>();
////		pageTags.put("v5", "MyTMO | Shop : Device Trade-in Selector");
////		pageTags.put("pageName", "MyTMO | Shop : Device Trade-in Selector");
////		pageTags.put("v12", "Standard Upgrade");
////		pageNotNullTags.add("v10");
////		al.verifyPagePdlTags(getDriver(), pageTags,pageNotNullTags);
//	}

}
