package com.tmobile.eservices.qa.shop.functional;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class UnlockDataTest extends ShopCommonLib {

	@Test(dataProvider = "byColumnName", enabled = true)
	public void unlockTestData(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Unlock Test Data");
		navigateToShopPage(myTmoData);
	}

}
