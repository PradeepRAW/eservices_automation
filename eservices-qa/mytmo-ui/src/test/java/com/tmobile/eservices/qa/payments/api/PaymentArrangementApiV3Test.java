package com.tmobile.eservices.qa.payments.api;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.pages.payments.api.PaymentArrangementApiV3;
import com.tmobile.eservices.qa.pages.payments.api.SedonaApiCollectionV1;

import io.restassured.response.Response;

public class PaymentArrangementApiV3Test extends PaymentArrangementApiV3 {

	public Map<String, String> tokenMap;

	/**
	 * /** UserStory# US415315: MyTMO - PAYMENTS - Tokenization - NEW PA API
	 * Endpoint EOS-PAYMENTS #
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentArrangementApiV3",
			"testCreatePaymentArrangementWithCardNonSedona", Group.PAYMENTS, Group.SPRINT })
	public void testCreatePaymentArrangementWithCardNonSedona(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testCreatePaymentArrangementWithCardNonSedona");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with CreatePaymentArrangement.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testCreatePaymentArrangementWithCardNonSedona";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("generatequote_OTP_Sedona.txt");
		String quoteId = null;
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("chargeAmount", apiTestData.getChargeAmount());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		SedonaApiCollectionV1 SedonaApiCollectionV1_Service = new SedonaApiCollectionV1();

		Response response = SedonaApiCollectionV1_Service.generatequote_OTP(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				quoteId = getPathVal(jsonNode, "quoteId");
			}
		}

		if (quoteId != null) {
			requestBody = new ServiceTest().getRequestFromFile("paymentarrangementCreateandUpdatewithCard.txt");

			tokenMap.put("quoteId", quoteId);

			updatedRequest = prepareRequestParam(requestBody, tokenMap);

			response = createPaymentArrangement(apiTestData, updatedRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				Assert.assertTrue(response.statusCode() == 200);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {

				}
			} else {

				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * /** UserStory# US415315: MyTMO - PAYMENTS - Tokenization - NEW PA API
	 * Endpoint EOS-PAYMENTS #
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentArrangementApiV3",
			"testUpdatePaymentArrangementWithCardNonSedona", Group.PAYMENTS, Group.SPRINT })
	public void testUpdatePaymentArrangementWithCardNonSedona(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testUpdatePaymentArrangementWithCardNonSedona");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with CreatePaymentArrangement.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testUpdatePaymentArrangementWithCardNonSedona";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("generatequote_OTP_Sedona.txt");
		String quoteId = null;
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("chargeAmount", apiTestData.getChargeAmount());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		SedonaApiCollectionV1 SedonaApiCollectionV1_Service = new SedonaApiCollectionV1();

		Response response = SedonaApiCollectionV1_Service.generatequote_OTP(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				quoteId = getPathVal(jsonNode, "quoteId");
			}
		}

		if (quoteId != null) {
			requestBody = new ServiceTest().getRequestFromFile("paymentarrangementCreateandUpdatewithCard.txt");

			tokenMap.put("quoteId", quoteId);

			updatedRequest = prepareRequestParam(requestBody, tokenMap);

			response = createPaymentArrangement(apiTestData, updatedRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				Assert.assertTrue(response.statusCode() == 200);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {

				}
			} else {

				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * /** UserStory# US415315: MyTMO - PAYMENTS - Tokenization - NEW PA API
	 * Endpoint EOS-PAYMENTS #
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentArrangementApiV3",
			"testCreatePaymentArrangementWithBankNonSedona", Group.PAYMENTS, Group.SPRINT })
	public void testCreatePaymentArrangementWithBankNonSedona(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testCreatePaymentArrangementWithBankNonSedona");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with CreatePaymentArrangement.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testCreatePaymentArrangementWithBankNonSedona";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("generatequote_OTP_Sedona.txt");
		String quoteId = null;
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("chargeAmount", apiTestData.getChargeAmount());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		SedonaApiCollectionV1 SedonaApiCollectionV1_Service = new SedonaApiCollectionV1();

		Response response = SedonaApiCollectionV1_Service.generatequote_OTP(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				quoteId = getPathVal(jsonNode, "quoteId");
			}
		}

		if (quoteId != null) {
			requestBody = new ServiceTest().getRequestFromFile("paymentarrangementCreateandUpdatewithBank.txt");

			tokenMap.put("quoteId", quoteId);

			updatedRequest = prepareRequestParam(requestBody, tokenMap);

			response = createPaymentArrangement(apiTestData, updatedRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				Assert.assertTrue(response.statusCode() == 200);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {

				}
			} else {

				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * /** UserStory# US415315: MyTMO - PAYMENTS - Tokenization - NEW PA API
	 * Endpoint EOS-PAYMENTS #
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentArrangementApiV3",
			"testUpdatePaymentArrangementWithBankNonSedona", Group.PAYMENTS, Group.SPRINT })
	public void testUpdatePaymentArrangementWithBankNonSedona(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testUpdatePaymentArrangementWithBankNonSedona");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with CreatePaymentArrangement.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testUpdatePaymentArrangementWithBankNonSedona";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("generatequote_OTP_Sedona.txt");
		String quoteId = null;
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("chargeAmount", apiTestData.getChargeAmount());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		SedonaApiCollectionV1 SedonaApiCollectionV1_Service = new SedonaApiCollectionV1();

		Response response = SedonaApiCollectionV1_Service.generatequote_OTP(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				quoteId = getPathVal(jsonNode, "quoteId");
			}
		}

		if (quoteId != null) {
			requestBody = new ServiceTest().getRequestFromFile("paymentarrangementCreateandUpdatewithBank.txt");

			tokenMap.put("quoteId", quoteId);

			updatedRequest = prepareRequestParam(requestBody, tokenMap);

			response = createPaymentArrangement(apiTestData, updatedRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				Assert.assertTrue(response.statusCode() == 200);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {

				}
			} else {

				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * /** UserStory# US415315: MyTMO - PAYMENTS - Tokenization - NEW PA API
	 * Endpoint EOS-PAYMENTS #
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentArrangementApiV3",
			"testCreatePaymentArrangementWithNoPaymentMethodNonSedona", Group.PAYMENTS, Group.SPRINT })
	public void testCreatePaymentArrangementWithNoPaymentMethodNonSedona(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testCreatePaymentArrangementWithNoPaymentMethodNonSedona");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with CreatePaymentArrangement.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testCreatePaymentArrangementWithNoPaymentMethodNonSedona";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("generatequote_OTP_Sedona.txt");
		String quoteId = null;
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("chargeAmount", apiTestData.getChargeAmount());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		SedonaApiCollectionV1 SedonaApiCollectionV1_Service = new SedonaApiCollectionV1();

		Response response = SedonaApiCollectionV1_Service.generatequote_OTP(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				quoteId = getPathVal(jsonNode, "quoteId");
			}
		}

		if (quoteId != null) {
			requestBody = new ServiceTest()
					.getRequestFromFile("paymentarrangementCreateandUpdatewithNoPaymentMethod.txt");

			tokenMap.put("quoteId", quoteId);

			updatedRequest = prepareRequestParam(requestBody, tokenMap);

			response = createPaymentArrangement(apiTestData, updatedRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				Assert.assertTrue(response.statusCode() == 200);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {

				}
			} else {

				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * /** UserStory# US415315: MyTMO - PAYMENTS - Tokenization - NEW PA API
	 * Endpoint EOS-PAYMENTS #
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentArrangementApiV3",
			"testUpdatePaymentArrangementWithNoPaymentMethodNonSedona", Group.PAYMENTS, Group.SPRINT })
	public void testUpdatePaymentArrangementWithNoPaymentMethodNonSedona(ControlTestData data, ApiTestData apiTestData)
			throws Exception {
		Reporter.log("TestName: testUpdatePaymentArrangementWithNoPaymentMethodNonSedona");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with CreatePaymentArrangement.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testUpdatePaymentArrangementWithNoPaymentMethodNonSedona";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("generatequote_OTP_Sedona.txt");
		String quoteId = null;
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("paactionCode", apiTestData.getChargeAmount());
		tokenMap.put("paissecure", apiTestData.getChargeAmount());

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		SedonaApiCollectionV1 SedonaApiCollectionV1_Service = new SedonaApiCollectionV1();

		Response response = SedonaApiCollectionV1_Service.generatequote_OTP(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				quoteId = getPathVal(jsonNode, "quoteId");
			}
		}

		if (quoteId != null) {
			requestBody = new ServiceTest()
					.getRequestFromFile("paymentarrangementCreateandUpdatewithNoPaymentMethod.txt");

			tokenMap.put("quoteId", quoteId);

			updatedRequest = prepareRequestParam(requestBody, tokenMap);

			response = createPaymentArrangement(apiTestData, updatedRequest, tokenMap);

			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				logSuccessResponse(response, operationName);
				Assert.assertTrue(response.statusCode() == 200);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.asString());
				if (!jsonNode.isMissingNode()) {

				}
			} else {

				failAndLogResponse(response, operationName);
			}
		} else {
			failAndLogResponse(response, operationName);
		}

	}

	/**
	 * /** UserStory# US544097: Secure to Unsecure PA # US544096: Unsecure to Secure
	 * PA # US545233: TECH - Secure PA - Payment Instrument Change - EOS Payments to
	 * DPS Change.
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentArrangementApiV3Update",
			"testUpdatePAWithUnsecureToSecureBank", Group.PAYMENTS, Group.SPRINT })
	public void testUpdatePAWithUnsecureToSecureBank(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testUpdatePAWithUnsecureToSecureBank");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with UpdatePaymentArrangement.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testUpdatePAWithUnsecureToSecureBank";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("paymentarrangementCreateandUpdatewithBank.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("paactionCode", apiTestData.getpaActionCode());
		tokenMap.put("email", apiTestData.getMsisdn() + "@yopmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = updatePaymentArrangement(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "payment.statusCode"), "A", "Status Code Mis Matched");
				Assert.assertEquals(getPathVal(jsonNode, "payment.reasonCode"), "2491", "reason Code Mis Matched");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# US544097: Secure to Unsecure PA # US544096: Unsecure to Secure
	 * PA # US545233: TECH - Secure PA - Payment Instrument Change - EOS Payments to
	 * DPS Change.
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentArrangementApiV3Update",
			"testUpdatePAWithUnsecureToSecureCard", Group.PAYMENTS, Group.SPRINT })
	public void testUpdatePAWithUnsecureToSecureCard(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testUpdatePAWithUnsecureToSecureBank");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with UpdatePaymentArrangement.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testUpdatePAWithUnsecureToSecureCard";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("paymentarrangementCreateandUpdatewithCard.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("paactionCode", apiTestData.getpaActionCode());
		tokenMap.put("email", apiTestData.getMsisdn() + "@yopmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = updatePaymentArrangement(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "payment.statusCode"), "A", "Status Code Mis Matched");
				Assert.assertEquals(getPathVal(jsonNode, "payment.reasonCode"), "2491", "reason Code Mis Matched");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# US544097: Secure to Unsecure PA # US544096: Unsecure to Secure
	 * PA # US545233: TECH - Secure PA - Payment Instrument Change - EOS Payments to
	 * DPS Change.
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentArrangementApiV3Update",
			"testUpdatePAWithSecureBankToSecureCard", Group.PAYMENTS, Group.SPRINT })
	public void testUpdatePAWithSecureBankToSecureCard(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testUpdatePAWithSecureBankToSecureCard");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with UpdatePaymentArrangement.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testUpdatePAWithSecureBankToSecureCard";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("paymentarrangementCreateandUpdatewithCard.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("paactionCode", apiTestData.getpaActionCode());
		tokenMap.put("email", apiTestData.getMsisdn() + "@yopmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = updatePaymentArrangement(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "payment.statusCode"), "A", "Status Code Mis Matched");
				Assert.assertEquals(getPathVal(jsonNode, "payment.reasonCode"), "2491", "reason Code Mis Matched");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# US544097: Secure to Unsecure PA # US544096: Unsecure to Secure
	 * PA # US545233: TECH - Secure PA - Payment Instrument Change - EOS Payments to
	 * DPS Change.
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentArrangementApiV3Update",
			"testUpdatePAWithSecureCardToSecureBank", Group.PAYMENTS, Group.SPRINT })
	public void testUpdatePAWithSecureCardToSecureBank(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testUpdatePAWithSecureCardToSecureBank");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with UpdatePaymentArrangement.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testUpdatePAWithSecureCardToSecureBank";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest().getRequestFromFile("paymentarrangementCreateandUpdatewithBank.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("paactionCode", apiTestData.getpaActionCode());
		tokenMap.put("email", apiTestData.getMsisdn() + "@yopmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = updatePaymentArrangement(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "payment.statusCode"), "A", "Status Code Mis Matched");
				Assert.assertEquals(getPathVal(jsonNode, "payment.reasonCode"), "2491", "reason Code Mis Matched");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# US544097: Secure to Unsecure PA # US544096: Unsecure to Secure
	 * PA # US545233: TECH - Secure PA - Payment Instrument Change - EOS Payments to
	 * DPS Change.
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentArrangementApiV3Update",
			"testUpdatePAWithSecureCardToUnsecure", Group.PAYMENTS, Group.SPRINT })
	public void testUpdatePAWithSecureCardToUnsecure(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testUpdatePAWithSecureCardToUnsecure");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with UpdatePaymentArrangement.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testUpdatePAWithSecureCardToUnsecure";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest()
				.getRequestFromFile("paymentarrangementCreateandUpdatewithNoPaymentMethod.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("paactionCode", apiTestData.getpaActionCode());
		tokenMap.put("email", apiTestData.getMsisdn() + "@yopmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = updatePaymentArrangement(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "payment.statusCode"), "A", "Status Code Mis Matched");
				Assert.assertEquals(getPathVal(jsonNode, "payment.reasonCode"), "2491", "reason Code Mis Matched");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * /** UserStory# US544097: Secure to Unsecure PA # US544096: Unsecure to Secure
	 * PA # US545233: TECH - Secure PA - Payment Instrument Change - EOS Payments to
	 * DPS Change.
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "PaymentArrangementApiV3Update",
			"testUpdatePAWithSecureBankToUnsecure", Group.PAYMENTS, Group.SPRINT })
	public void testUpdatePAWithSecureBankToUnsecure(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testUpdatePAWithSecureBankToUnsecure");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with UpdatePaymentArrangement.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testUpdatePAWithSecureBankToUnsecure";

		tokenMap = new HashMap<String, String>();

		String requestBody = new ServiceTest()
				.getRequestFromFile("paymentarrangementCreateandUpdatewithNoPaymentMethod.txt");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("paactionCode", apiTestData.getpaActionCode());
		tokenMap.put("email", apiTestData.getMsisdn() + "@yopmail.com");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		Response response = updatePaymentArrangement(apiTestData, updatedRequest, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "payment.statusCode"), "A", "Status Code Mis Matched");
				Assert.assertEquals(getPathVal(jsonNode, "payment.reasonCode"), "2491", "reason Code Mis Matched");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US580564 Validate/Fix Sedona Vs NoSedona logic for Payment Arrangement
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "testPALandingDetailsNonSedona", Group.SPRINT })
	public void testPALandingDetailsNonSedona(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testPALandingDetailsNonSedona");
		Reporter.log("Data Conditions:MyTmo registered PA elgible non sedona misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with palandingdetails.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testPALandingDetailsNonSedona";

		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("isSedona", "false");
		Response response = getPALandingDetails(apiTestData, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "paymentInfo.isSedonaCustomer"), "false",
						"Is a Sedona customer");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * US580564 Validate/Fix Sedona Vs NoSedona logic for Payment Arrangement
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "testPALandingDetailsSedona", Group.SPRINT })
	public void testPALandingDetailsSedona(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testPALandingDetailsSedona");
		Reporter.log("Data Conditions:MyTmo registered PA elgible sedona misdn and BAN.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with palandingdetails.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testPALandingDetailsSedona";
		Map<String, String> tokenMap = new HashMap<String, String>();
		tokenMap.put("isSedona", "true");
		Response response = getPALandingDetails(apiTestData, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertEquals(getPathVal(jsonNode, "paymentInfo.isSedonaCustomer"), "true",
						"Is a Non Sedona customer");
			}
		} else {

			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * CDCDWG2-635 Migration of eos-pa
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "testMigrationOfEOSPA", "testMigrationOfEOSPA",
			Group.PAYMENTS, Group.SPRINT })
	public void testMigrationOfEOSPA(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testMigrationOfEOSPA");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN: Payment Arrangement");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with testMigrationOfEOSPA.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testMigrationOfEOSPA";

		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v1");
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("msisdn", apiTestData.getMsisdn());

		Response response = migrationEosPA(apiTestData, tokenMap);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertNotNull(jsonNode.get("paymentArrangement"),
						"paymentArrangement node is empty in response");
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}

	/**
	 * CDCDWG2-634 Retire EOS Service Quote
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "testRetireEOSServiceQuote", "testRetireEOSServiceQuote",
			Group.PAYMENTS, Group.SPRINT })
	public void testRetireEOSServiceQuote(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: testRetireEOSServiceQuote");
		Reporter.log("Data Conditions:MyTmo registered misdn and BAN: Payment Arrangement");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response with testRetireEOSServiceQuote.");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "testRetireEOSServiceQuote";

		tokenMap = new HashMap<String, String>();
		tokenMap.put("version", "v1");
		tokenMap.put("ban", apiTestData.getBan());
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
        Calendar calCurr = Calendar.getInstance();
        calCurr.setTime(date);
        calCurr.add(Calendar.DATE, 1);
        Date oneDayAheadDate = calCurr.getTime();
        tokenMap.put("startdate", sdf.format(oneDayAheadDate));
        
		String requestBody = new ServiceTest()
				.getRequestFromFile("generatequote_PA.txt");

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);

		
		Response response = generateQuote(apiTestData, tokenMap, updatedRequest);

		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			Assert.assertTrue(response.statusCode() == 200);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				Assert.assertNotNull(jsonNode.get("accounts"),
						"accounts node is empty in response");
				Assert.assertNotNull(jsonNode.get("quoteId"),
						"quoteId node is empty in response");
				checkexpectedvalues(response, "accounts.accountNumber", apiTestData.getBan());
			}
		} else {
			failAndLogResponse(response, operationName);
		}
	}
}
