package com.tmobile.eservices.qa.payments.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.api.eos.BillingApiV1;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class BillingApiV1Test extends BillingApiV1{
	
	public Map<String, String> tokenMap;
	/**
	 * UserStory# Description: Billing getDataSet Request
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true,priority = 1,  groups = { "BillingApiV1", "testGetDataSet", Group.PAYMENTS,Group.APIREG })
	public void testGetDataSet(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetDataSet");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results the dataset of requested ban,msisdn");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		String operationName = "getDataSet";
		tokenMap = new HashMap<String, String>();
		Map<String, String> tokenMap= new HashMap<String,String>();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		Response response = getDataSet(apiTestData,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Assert.assertEquals("100", getPathVal(jsonNode,"statusCode"));
			/*
			 * Assert.assertNotNull(getPathVal(jsonNode,"wallet.expiredCards"));
			 * Assert.assertNotNull(getPathVal(jsonNode,"wallet.expiringCards"));
			 * Assert.assertNotNull(getPathVal(jsonNode,"wallet.instrumentsOnNegativeFile"))
			 * ; Assert.assertNotNull(getPathVal(jsonNode,"wallet.savedPaymentMethods"));
			 */
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	
	/**
	 * UserStory# Description: getBillList
	 * 
	 * @param data
	 * @param apiTestData
	 * @throws Exception
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = {"BillingApiV1", "testGetBillist", Group.PAYMENTS,Group.APIREG })
	public void testGetBillist(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: GetBillist for requested ban,misisdn");
		Reporter.log("Data Conditions:MyTmo registered misdn.");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Results billList details in response");
		Reporter.log("Step 2: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String, String> tokenMap= new HashMap<String,String>();
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("version", "v1");
		String operationName = "getBillList";
		Response response = getBillList(apiTestData,tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
		} else {
			failAndLogResponse(response, operationName);
		}
	}
	

}
