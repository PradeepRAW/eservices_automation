/**
 * 
 */
package com.tmobile.eservices.qa.monitors;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.data.ApiTestData;

/**
 * @author csudheer
 *
 */
public class AccountsAPIMonitors extends AccountsAPIMonitorsHelper {

	// Hybrid TE to TI Migration end to end flow with signing up for Autopay
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testhybridTEtoTIMigrationFlow(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			tokenMap.put("password", apiTestData.getPassword());
			tokenMap.put("version", "v1");
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Hybrid TE to TI Migration Flow</b> ");

			// Step 1 - Login to My TMO

			// invokeGetSubScriberLines(apiTestData, tokenMap);
			// invokeGetSubScriberAccount(apiTestData, tokenMap);

			// Step 2 - Select PLAN Tab

			invokePlanTabServices(apiTestData, tokenMap);

			// Step 3 - Select Change Plan at account level

			invokeChangePlanAtAccountLevelServices(apiTestData, tokenMap);

			// Step 4 - Select change plan on the featured plans page
			invokeChangePlanOnTheFeaturedPlansPage(apiTestData, tokenMap);

			// Step 5 - Select “With Netflix on us” plan on plans comparison
			// page

			invokePlanOnPlansComparisonPageSelectPlan(apiTestData, tokenMap);

			// Ignoring Confirmation page validations

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	// Change data plan(backdate)/add data pass(immediate)/add a future dated
	// service
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testChangeDataPlanFlow(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Hybrid TE to TI Migration Flow</b> ");

			// Step 1 - Login to My TMO
			// invokeGetSubScriberLines(apiTestData, tokenMap);
			// invokeGetSubScriberAccount(apiTestData, tokenMap);

			// Step 2 - Select PLAN Tab
			invokePlanTabServices(apiTestData, tokenMap);

			// Step 3 - Select Manage Add ons

			invokeSelectManageAddOnsServices(apiTestData, tokenMap,"Automation");

			// Step 4 - From the line picker select a msisdn

			invokeLinePickerSelectAMsisdn(apiTestData, tokenMap,"Automation");

			// Ignoring Confirmation page validations

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	// Check Device unlock status
	@Test(dataProvider = "byColumnName", enabled = true)
	public void testCheckDeviceUnlockStatus(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			Reporter.log("**********************************************************************************");
			Reporter.log("<b>Hybrid TE to TI Migration Flow</b> ");

			// Step 1 - Login to My TMO
			// invokeGetSubScriberLines(apiTestData, tokenMap);
			// invokeGetSubScriberAccount(apiTestData, tokenMap);

			// Step 2 - Select PHONE Tab
			invokePhoneTabServices(apiTestData, tokenMap,"CR");

			// Step 3 & 4- Click on “Device Unlock” Status & Verify status
			invokeDeviceUnlockServices(apiTestData, tokenMap);

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}

	// Verify Report device as lost or stolen functionality for a subline
	@Test(dataProvider = "byColumnName", enabled = true)
	public void verifyReportDeviceAsLostOrStolen(ApiTestData apiTestData) {
		try {
			Map<String, String> tokenMap = new HashMap<String, String>();
			tokenMap.put("ban", apiTestData.getBan());
			tokenMap.put("msisdn", apiTestData.getMsisdn());
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Hybrid TE to TI Migration Flow</b> ");

			// Step 1 - Login to My TMO
			 invokeGetSubScriberLines(apiTestData, tokenMap);
			// invokeGetSubScriberAccount(apiTestData, tokenMap);

			// Step 2 - Select PHONE Tab
			invokePhoneTabServices(apiTestData, tokenMap,"SNQ1");

			// Step 3 - Select a Subline

			invokeSelectASublineServices(apiTestData, tokenMap);

			// Step 4 - Select “Report Lost or Stolen” link

			invokeSelectReportLostOrStolenLinkServices(apiTestData, tokenMap,"Automation");

			// Step 5 - On the Report Device as Lost or stolen page, select Next
			// button
			// Step 6 - Select any of Device is lost or stolen option

			invokeSuspendOrKeepTheLineServices(apiTestData, tokenMap);

		} catch (RuntimeException re) {
			Assert.fail("<b>Exception Response :</b> " + re);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + re);
		} catch (Exception e) {
			Assert.fail("<b>Exception Response :</b> " + e);
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
	}
}
