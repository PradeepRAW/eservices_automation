package com.tmobile.eservices.qa.shop.npi;

import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.shop.*;
import org.testng.Reporter;
import org.testng.annotations.Test;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

public class JumpValidator extends ShopCommonLib {

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "jump1" })
	public void testJumpFlowForNewSkus4(MyTmoData myTmoData) {
		jumpFlow(myTmoData);
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "jump" })
	public void testJumpFlowForNewSkus8(MyTmoData myTmoData) {
		jumpFlow(myTmoData);
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "jump" })
	public void testJumpFlowForNewSkus12(MyTmoData myTmoData) {
		jumpFlow(myTmoData);
	}
	// Need data for 7 tests
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "jump" })
	public void testJumpFlowForNewSkus16(MyTmoData myTmoData) {
		jumpFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "jump" })
	public void testJumpFlowForNewSkus20(MyTmoData myTmoData) {
		jumpFlow(myTmoData);
	}

	@Test(dataProvider = "byColumnName", enabled = true, groups = { "jump" })
	public void testJumpFlowForNewSkus24(MyTmoData myTmoData) {
		jumpFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "jump" })
	public void testJumpFlowForNewSkus28(MyTmoData myTmoData) {
		jumpFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "jump" })
	public void testJumpFlowForNewSkus32(MyTmoData myTmoData) {
		jumpFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "jump" })
	public void testJumpFlowForNewSkus36(MyTmoData myTmoData) {
		jumpFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "jump" })
	public void testJumpFlowForNewSkus40(MyTmoData myTmoData) {
		jumpFlow(myTmoData);
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "mobile" })
	public void testJumpFlowForNewSkuInMobile(MyTmoData myTmoData) {
		jumpFlow(myTmoData);
	}

	public void jumpFlow(MyTmoData myTmoData) {
		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.verifyDeviceNameAndManufacturerName(myTmoData.getDeviceName());
		pdpPage.selectMemory(myTmoData.getDeviceMemory());
		pdpPage.chooseColor(myTmoData.getDeviceColor());
		pdpPage.selectPaymentOption(myTmoData.getpaymentOptions());
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.verifyLineSelectorPage();
		lineSelectorPage.clickOnGivenLine(myTmoData.getLineNumber());

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		if (deviceProtectionPage.verifyJumpProtection()) {
			deviceProtectionPage.clickContinueButton();
		}

		JumpConditionPage newJumpConditionPage = new JumpConditionPage(getDriver());
		newJumpConditionPage.verifyNewJumpConditionPage();
		newJumpConditionPage.selectGoodComponents();
		newJumpConditionPage.clickContinueButton();
		TradeInDeviceConditionValuePage deviceConditionsPage = new TradeInDeviceConditionValuePage(getDriver());
		//deviceConditionsPage.verifyTradeInDeviceConditionValuePage();
		deviceConditionsPage.clickAcceptAndContinueButton();
		JumpCartPage jumpCartPage = new JumpCartPage(getDriver());
		jumpCartPage.verifyJumpCartPage();
		jumpCartPage.verifyShippingAddress();
		jumpCartPage.verifyShippingPriceIsDisplayed();
		jumpCartPage.verifyShippingMethods();
		jumpCartPage.verifyEstimateShippingDetails();

		Double downPayment = jumpCartPage.getDownPaymentIntegerJumpCartPage();
		Reporter.log("Down Payment is Displayed and Verified on Jump cart Page");
		Double shippingPrice = jumpCartPage.getShippingpriceIntegerJumpCartPage();
		Reporter.log("Shipping Price is Displayed and Verified on Jump cart Page");
		Double taxesAndFees = jumpCartPage.getTaxesFeesAndPriceIntegerJumpCartPage();
		Reporter.log("Taxes and Fees is Displayed and Verified on Jump cart Page");
		Double dueTodayTotal = jumpCartPage.getOrderTotalPriceIntegerJumpCartPage();
		Reporter.log("Down Payment is Displayed and Verified on Jump cart Page");
		Double totalPrice = downPayment + shippingPrice + taxesAndFees;
		jumpCartPage.verifyTotalDueTodayPrices(dueTodayTotal,totalPrice);
		
		jumpCartPage.clickBillingAndShippingCheckBox();
		jumpCartPage.fillBillingAddressInfo();
		jumpCartPage.enterCardHoldername(myTmoData.getPayment().getNameOnCard());
		jumpCartPage.enterCardNumber(myTmoData.getPayment().getCardNumber());
		jumpCartPage.enterCardExpiryDate(myTmoData.getPayment().getExpiryDate());
		jumpCartPage.enterCVV(myTmoData.getPayment().getCvv());
		jumpCartPage.clickAcceptAndPlaceOrder();
		ESignaturePage esignaturePage = new ESignaturePage(getDriver());
        esignaturePage.verifyESignaturePage();
        esignaturePage.selectESignatureCheckBox();
        esignaturePage.clickESignatureContinueCTA();
        esignaturePage.clickSignatureImage();
        esignaturePage.clickSelectStyleTab();
        esignaturePage.clickAdoptSignatureCTA();
        esignaturePage.clickSendTheOrderCTA();
        OrderConfirmationPage orderConfirmationPage = new OrderConfirmationPage(getDriver());
        orderConfirmationPage.verifyOrderConfirmationPage();
        orderConfirmationPage.verifyOrderNumber();
        orderConfirmationPage.getOrderNumber();
        CommonPage commonPage = new CommonPage(getDriver());
		commonPage.takeScreenshot();
		
		Reporter.log("Jump Upgrade flow completed successfully");
	}

}