package com.tmobile.eservices.qa.payments.api;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;

import com.jayway.jsonpath.JsonPath;

public class invalidtestdataidentifier {

	// @Test
	public void tesdatagapidentifier() throws IOException {
		// List<String> invalidadta = JsonPath.parse(new
		// URL("http://172.28.49.103:3000/testdata")).read("$.[?(@.login=='invalid')].misdin");
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String datatostring = JsonPath.parse(new URL("http://172.28.49.103:3000/testdata")).read("$.[*]").toString();
		List<String> invalidadta = JsonPath.parse(datatostring).read("$.[?(@.login=='invalid')].misdin");
		// File testdatfile=new File(
		// System.getProperty("user.dir")+"/src/test/resources/testdata/db.json");
		// List<String> invalidadta=JsonPath.read(testdatfile,
		// "$.[?(@.login=='invalid')].misdin");

		// JsonPath.parse(new
		// URL("http://172.28.49.103:3000/testdata")).read("$.[?(@.auto=='true')].misdin");
		Reporter.log("<table><tbody><tr><td>");
		Reporter.log("<font size=\"4\" color=\"blue\">msidns required to be check logins</font>");
		for (String msidn : invalidadta) {
			Reporter.log(msidn.split("/")[0] + "-" + msidn.split("/")[1]);

		}

		Reporter.log("</td></tr>");

		File folder = new File(System.getProperty("user.dir") + "/src/test/resources/testdata");
		File[] listOfFiles = folder.listFiles();

		for (File eachfile : listOfFiles) {
			String filename = eachfile.getName().toString();
			if (filename.contains("xls") || filename.contains("xlsx")) {

				Reporter.log("<tr><td><font size=\"4\" color=\"green\">Data sheet name is:" + filename + "</font>");

				Workbook workbook;
				FileInputStream excelFile = new FileInputStream(eachfile);

				if (filename.contains("xlsx")) {
					workbook = new XSSFWorkbook(excelFile);
				} else {
					workbook = new HSSFWorkbook(excelFile);
				}

				Sheet datatypeSheet = workbook.getSheet("common");
				if (datatypeSheet == null)
					continue;
				Iterator<Row> iterator = datatypeSheet.iterator();

				int tesnamecolid = -1;
				int filterid = -1;
				while (iterator.hasNext()) {

					Row currentRow = iterator.next();

					if (currentRow.getRowNum() == 0) {

						tesnamecolid = getcolumid(currentRow, "TestName");
						filterid = getcolumid(currentRow, "Filter");

						if (tesnamecolid == -1 || filterid == -1) {
							break;
						}

					} else {
						if (currentRow.getCell(filterid) != null && currentRow.getCell(tesnamecolid) != null) {

							try {

								List<String> getdata = JsonPath.parse(datatostring).read(
										"$.[?(" + currentRow.getCell(filterid).getStringCellValue() + ")].misdin");
								// List<String> getdata=JsonPath.parse(new
								// URL("http://172.28.49.103:3000/testdata")).read("$.[?("+currentRow.getCell(filterid).getStringCellValue()+")].misdin");
								// List<String> getdata=JsonPath.read(testdatfile,
								// "$.[?("+currentRow.getCell(filterid).getStringCellValue()+")].misdin");
								if (getdata.isEmpty()) {
									Reporter.log(currentRow.getCell(tesnamecolid).getStringCellValue() + "-"
											+ currentRow.getCell(filterid).getStringCellValue());
								}
							} catch (Exception e) {

							}
						} // if cells are not null

					} // else for remaining rows
					workbook.close();

				} // while

				Reporter.log("</td></tr>");
			} // if excel

		} // forloop
		Reporter.log("</tbody></tale>");
	}// test method

	public int getcolumid(Row currentRow, String header) {
		int retu = -1;

		Iterator<Cell> column = currentRow.iterator();
		while (column.hasNext()) {
			Cell currentcell = column.next();

			if (currentcell.toString().equalsIgnoreCase(header))
				return currentcell.getColumnIndex();
		}

		return retu;
	}

}
