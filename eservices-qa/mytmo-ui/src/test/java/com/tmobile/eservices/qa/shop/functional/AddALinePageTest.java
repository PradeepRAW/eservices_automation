package com.tmobile.eservices.qa.shop.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.global.ContactUSPage;
import com.tmobile.eservices.qa.pages.shop.ConsolidatedRatePlanPage;
import com.tmobile.eservices.qa.pages.shop.DeviceIntentPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.InterstitialTradeInPage;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.PLPPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;

/**
 * @author charshavardhana
 *
 */
public class AddALinePageTest extends ShopCommonLib {

	private final static Logger logger = LoggerFactory.getLogger(AddALinePageTest.class);

	/**
	 * Verify Add A line button for No Credit check customer
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION })
	public void verifyNCCustomerAddALineButton(ControlTestData data, MyTmoData myTmoData) {

		logger.info("verifyNCCustomerAddALineButton method called in AALTest");
		Reporter.log("Verify Add A line button for No Credit check customer");
		Reporter.log("Data Condition | IR STD NC Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login as NCCustomer into the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click On Shop | Add a Line button should be displayed in Shop page to the NCCustomer");
		Reporter.log("================================");
		Reporter.log("Actual Result:");

		navigateToShopPage(myTmoData);
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyPageLoaded();
		shopPage.verifyPageUrl();
		shopPage.clickQuickLinks("Add a line");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
	}

	/**
	 * US369973 : AAL - Device Intent Page -Trade In Warning sticky
	 * US369742 : AAL - Device Intent Page display (HTML Update)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PENDING_REGRESSION })
	public void testTradeInWarningStickyMessageInDeviceIntentPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case#US369973 : AAL - Device Intent Page -Trade In Warning sticky");
		Reporter.log("Data Condition | User must be Eligible for Add a line");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line button in shop page | Device Intent page should be displayed");
		Reporter.log("6. Verify Page Title as 'Are you adding a new phone or using your own?'| "
				+ "Page Title 'Are you adding a new phone or using your own?' should be displayed");
		Reporter.log("7. Verify Sticky message | Stickymessage should be displayed");
		Reporter.log("8. Verify Phone Icon in Sticky message banner | "
				+ "Phone Icon in Sticky message banner should be displayed");
		Reporter.log("9. Verify Authorable text 'Want to trade in a device?' in Sticky message banner | "
				+ "Authorable text 'Want to trade in a device?' in Sticky message banner should be displayed");
		Reporter.log("10. Verify Hyper link 'Call 1-800-T-Mobile' in Sticky message banner | "
				+ "Hyper link 'Call 1-800-T-Mobile' in Sticky message banner should be displayed");
		Reporter.log("11. Verify 'X' icon in Sticky message banner | "
				+ "'X' icon in Sticky message banner should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");

		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyTitle();
		deviceIntentPage.verifyWantToTradeInADeviceText();
		deviceIntentPage.verifyCallHyperLink();
		deviceIntentPage.verifyCancelIconInStickyBanner();
	}

	/**
	 * US340859 : AAL: Entry points for SHOP PAGE/Banner CTAs (known intent) -
	 * error messages
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION})
	public void testAALEligibilityCheckForPuertoRicoCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case#US340859 : AAL: Entry points for SHOP PAGE/Banner CTAs (known intent) - error messages");
		Reporter.log("Data Condition | User must be puerto rico customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log(
				"5. Click Add a line button in shop page |Authorable ineligibility message for puerto rico customer should be displayed");
		Reporter.log(
				"6. Verify authorable ineligibility message |Authorable ineligibility message for puerto rico customer should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");	
		
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickShopLinkForPuertoCustomer();
		ShopPage shopPage = new ShopPage(getDriver());
		shopPage.verifyPageLoaded();		
		shopPage.clickQuickLinks("Agrega una línea");
		shopPage.verifyAALIneligibleModalWindow();
		shopPage.verifyAALIneligibleMessageForPuertoRicoCustomer();
	}

	/**
	 * US340859 : AAL: Entry points for SHOP PAGE/Banner CTAs (known intent) -
	 * error messages
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION })
	public void testAALEligibilityCheckForVoiceLines(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case#US340859 : AAL: Entry points for SHOP PAGE/Banner CTAs (known intent) - error messages");
		Reporter.log("Data Condition | User should be eligible for voice lines and eligible MBB lines is zero");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line button in shop page |Authorable ineligibilty message should be displayed");
		Reporter.log(
				"6. Verify authorable ineligibility message |Authorable ineligibility message for zero mbb lines should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		shopPage.verifyAALIneligibleModalWindow();
		shopPage.verifyAALIneligibleMessageForMBBLines();
	}

	/**
	 * US340859 : AAL: Entry points for SHOP PAGE/Banner CTAs (known intent) -
	 * error messages
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION})
	public void testAALEligibilityCheckForFullOrSingleLineUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case#US340859 : AAL: Entry points for SHOP PAGE/Banner CTAs (known intent) - error messages");
		Reporter.log("Data Condition | User should full or single line user and role is not among PAH ");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line button in shop page |Authorable ineligibility message should be displayed");
		Reporter.log(
				"6. Verify authorable ineligibility message |Authorable ineligibility message for user role not among pah should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		shopPage.verifyAALIneligibleModalWindow();
		shopPage.verifyAALIneligibleMessageForUserRoleNotAmongPAH();
	}

	/**
	 * US340859 : AAL: Entry points for SHOP PAGE/Banner CTAs (known intent) -
	 * error messages
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION })
	public void testAALEligibilityCheckForSuspended(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case#US340859 : AAL: Entry points for SHOP PAGE/Banner CTAs (known intent) - error messages");
		Reporter.log("Data Condition | Suspended account");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application should be Launched");
		Reporter.log("2. Login to the application | User should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line button in shop page |Authorable ineligibility message should be displayed");

		Reporter.log(
				"6. Verify authorable ineligibility message |Authorable ineligibility message for suspended account should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		shopPage.verifyAALIneligibleModalWindow();
		shopPage.verifyAALIneligibleMessageForSuspendedAccount();
	}


	/**
	 * US377605 - AAL - unKnown intent Blocked path - lead Non Eligible plan
	 * customers to Modal(LINE SELECTOR)
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION})
	public void verifyAALwithFullCustomerWhoisNotonTMOONEVoicePlan(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case name :AAL - unKnown intent Blocked path - lead Non Eligible plan customers to Modal(LINE SELECTOR)");
		Reporter.log("Test Data : User should be PAH or  FULL customer who is not on TMO ONE voice plan ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Click on a device in PLP | Product details page should be displayed");
		Reporter.log("7. Click add to cart button | Line selectior page should be displayed");
		Reporter.log("8. Click add a line button in shop page | Modal pop up should be dsiplayed");
		Reporter.log("9. Verify Authorable Title as 'Let's talk' | Authorable Title('Let's talk') should be displayed");
		Reporter.log("10.Verify Authorable text line1 | Authorable text line1 should be displayed");
		Reporter.log(
				"11.Verify Authorable text line2('To a line') message | Authorable text line2('To a line') message should be displayed");
		Reporter.log("12.Verify Contact us CTA | Contact us CTA should be displayed");
		Reporter.log("13.Click on Contact US | Contact Page should be displayed ");

		Reporter.log("Actual Result:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.clickAddToCart();
		LineSelectorPage lineSelectorPage = new LineSelectorPage(getDriver());
		lineSelectorPage.dismissPopup();
		lineSelectorPage.clickOnAALCTA();
		lineSelectorPage.verifyAALModalPopup();
		lineSelectorPage.verifyAALModalPopupTitleAndText();
		lineSelectorPage.verifyContactusOnModalpopup();
		lineSelectorPage.clickContactUsBtnInModalpopup();

		ContactUSPage contactUsPage = new ContactUSPage(getDriver());
		contactUsPage.verifyContactUSPage();
	}

	/**
	 * US377607	AAL - Known intent Blocked path - lead NON Non Eligible plan customers to Modal(quick links/banner CTA) - AC2
	 * US388893 - [Continued] AAL - Known intent Blocked path - lead max voice/non max MBB to Modal(quick links/banner CTA) - AC2
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION})
	public void verifyAALwithTMOONEVoicePlanCustomerFromQuickLinks(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case name :AAL - unKnown intent Blocked path - lead Non Eligible plan customers to Modal(LINE SELECTOR) and "
				+ "AAL - Known intent Blocked path - lead max voice/non max MBB to Modal(quick links/banner CTA) ");
		
		Reporter.log("Test Data : User should be TMO ONE voice plan customer that is not maxed out on SOC plan");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Click on Shop option | User Should be navigated to Shop home page");
		Reporter.log("4. Click Add a line button in shop page | Customer intent page should be displayed");
		Reporter.log("5. Click on 'Buy a new phone' option | RatePlan page should be displayed");
		Reporter.log("6. Click on continue button | PLP page should be displayed");
		Reporter.log("7. Select device | PDP page should be displayed");
		Reporter.log("8. Click Add to cart button | Interstitial page should be displayed");
		Reporter.log("9. Click skip trade in option | Device protection page should be displayed");
		Reporter.log("================================");

		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.clickOnBuyaNewPhone();
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickDeviceByName(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.clickContinueBtn();		
		InterstitialTradeInPage interstitialTradeInPage = new InterstitialTradeInPage(getDriver());
		interstitialTradeInPage.verifyInterstitialTradeInPage();
		interstitialTradeInPage.clickOnSkipTradeInCTA();
		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();

	}
	

	/**
	 * US377607 - AAL - Known intent Blocked path - lead NON Non Eligible plan
	 * customers to Modal(quick links/banner CTA)
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION})
	public void verifyAALwithPAHuser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case name : AAL - Known intent Blocked path - lead NON Non Eligible plan customers to Modal(quick links/banner CTA)");
		Reporter.log("Test Data : User should be PAH or  FULL customer who is not on TMO ONE voice plan ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click Add a line button in shop page | Modal pop up should be dsiplayed");
		Reporter.log("6. Verify Authorable Title as 'Let's talk' | Authorable Title('Let's talk') should be displayed");
		Reporter.log("7. Verify Authorable text line1 | Authorable text line1 should be displayed");
		Reporter.log(
				"8. Verify Authorable text line2('To a line') message | Authorable text line2('To a line') message should be displayed");
		Reporter.log("9. Verify Contact us CTA | Contact us CTA should be displayed");
		Reporter.log("10. Click on Contact US | Contact Page should be displayed ");

		Reporter.log("Actual Result:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		shopPage.verifyAALModalPopup();
		shopPage.verifyAALModalPopupTitleAndText();
		shopPage.verifyContactusOnModalpopup();
		shopPage.clickContactUsBtnInModalpopup();

		ContactUSPage contactUsPage = new ContactUSPage(getDriver());
		contactUsPage.verifyContactUSPage();
	}

	/**
	 * US377613 - AAL - Known intent Blocked path - lead Eligible (max soc) plan
	 * customers to modal (quick links/banner CTA)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS, Group.AAL_REGRESSION})
	public void verifyAALStatusWithKnownIntentforEligibleUserWhenSOCLimitMaxedOut(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("VerifyAALStatusWithKnownIntentforUserWhenSOCLimitMaxedOut");
		Reporter.log("================================");

		Reporter.log("Test Data Conditions: TMO one voice plan but SOC limit is maxed out");
		Reporter.log("Test Data Conditions: Passed the eligibility checks from F37104");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Click on Shop tab | User Should be navigated to Shop home page");
		Reporter.log(
				"4. Click on ADD a Line(From Quick links) | 'Let's talk...Sorry, we're not able add a line to your account online...' pop up should be	displayed");
		Reporter.log(
				"5. Verify contents on 'Let's talk...' popup | User friendly message with Customer care details and 'Contact Us' button should be displayed on thepop-up");
		Reporter.log(
				"6. Click 'Contact US' button on 'Let's talk...' popup | User should be navigated to Contact Us page");
		Reporter.log("================================");

		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");
		shopPage.verifyAALModalPopup();
		shopPage.verifyAALModalPopupTitleAndText();
		shopPage.verifyContactusOnModalpopup();
		shopPage.clickContactUsBtnInModalpopup();
		ContactUSPage contactUsPage = new ContactUSPage(getDriver());
		contactUsPage.verifyContactUSPage();
	}

}

/**
 * Retired US In this Page: US340859: Data Dependency,
 *  US168516 Old functionality,
 *  US340859- Invalid data condition for regression
 *   */
