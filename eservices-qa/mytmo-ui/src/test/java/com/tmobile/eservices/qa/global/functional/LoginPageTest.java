/**
 * 
 */
package com.tmobile.eservices.qa.global.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.global.GlobalCommonLib;
import com.tmobile.eservices.qa.pages.NewHomePage;

/**
 * @author Slalithambigai
 *
 */
public class LoginPageTest extends GlobalCommonLib {

	private static final Logger logger = LoggerFactory.getLogger(LoginPageTest.class);

	/**
	 * Verify SL - defaultTMOID cookie is present after login
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifySLdefaultTMOIDAndWebCookie(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any single line ssisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify defaultTMOID cookie is present| defaultTMOID cookie should be present");
		Reporter.log("4. Verify es_web_an cookie is present| es_web_an cookie should be present");

		Reporter.log("==============================");
		Reporter.log("Actual Result:");

		launchAndPerformLogin(myTmoData);
		Assert.assertNotNull(getDriver().manage().getCookieNamed("defaultTMOID"), "defaultTMOID cookie is present");
		Reporter.log("defaultTMOID cookie is available after login");
		Assert.assertNotNull(getDriver().manage().getCookieNamed("es_web_an_new"), "es_web_an cookie is present");
		Reporter.log("es_web_an cookie is available after login");
	}

	/**
	 * Verify SL - es_web_an_new cookie is present after login
	 * 
	 * @param data
	 * @param myTmoData
	 */

	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.GLOBAL, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyMLdefaultTMOIDAndWebCookie(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Data Conditions: Any Multi line ssisdn");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Verify Home page | Home page should be displayed");
		Reporter.log("3. Verify es_web_an_new cookie is present| es_web_an_new should be present");
		Reporter.log("4. Verify defaultTMOID cookie is present| defaultTMOID cookie should be present");

		Reporter.log("==============================");
		Reporter.log("Actual Result:");

		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		Assert.assertNotNull(getDriver().manage().getCookieNamed("es_web_an_new"), "es_web_an_new cookie is present");
		Reporter.log("es_web_an_new cookie is available after login");
		Assert.assertNotNull(getDriver().manage().getCookieNamed("defaultTMOID"), "defaultTMOID cookie is present");
		Reporter.log("defaultTMOID cookie is available after login");
	}
}
