package com.tmobile.eservices.qa.global.api;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.CustomerAccountApiV1;
import com.tmobile.eservices.qa.api.eos.DNSApiV1;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class DNSApiV1Test extends DNSApiV1 {
	/**
	 * UserStory# Description:CDCDWR2-43:CCPA WS2 | Web No Sell, App Sell/No Sell | Integrate with CHUB GetCustomerDoNotSell API
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "DNSApi","testGetAndSetAccountID_DNSSettings",Group.GLOBAL,Group.SPRINT  })
	public void testGetAndSetAccountID_DNSSettings(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getDNSSettings test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Use input idType as AccountID,brand as TVision,id as suneelatest@105");
		Reporter.log("Step 1: Get the response getDNSSettings details.");
		Reporter.log("Step 2: Verify getDNSSettings details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String,String> tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("Msisdn", apiTestData.getMsisdn());
		tokenMap.put("idType", "AccountID");
		tokenMap.put("brand", "TVision");
		tokenMap.put("id", "suneelatest@105");
		
		Map<String,String> responseDNSMap=getDNSSettingsResponse(apiTestData,tokenMap);
		Assert.assertNotNull(responseDNSMap.get("localDnsValue"),"Invalid localDnsValue ");
		Assert.assertNotNull(responseDNSMap.get("globalDnsValue"),"Invalid globalDnsValue ");
		
		tokenMap.put("dnsSetting", responseDNSMap.get("localDnsValue").equalsIgnoreCase("Sell")?"DoNotSell":"Sell");
		tokenMap.put("dnsType", "Global");
		
		Map<String,String> responseSetDNSMap=setDNSSettingsResponse(apiTestData,tokenMap);
		Assert.assertEquals(responseSetDNSMap.get("status"),"success","Invalid status");
		
		Map<String,String> responseUpdatedDNSMap=getDNSSettingsResponse(apiTestData,tokenMap);
		Assert.assertNotEquals(responseUpdatedDNSMap.get("localDnsValue"),responseDNSMap.get("localDnsValue"),"Updating dnsValue failed.");
		Assert.assertNotEquals(responseUpdatedDNSMap.get("globalDnsValue"),responseDNSMap.get("globalDnsValue"),"Updating dnsValue failed.");
	}
	
	public Map<String,String> getDNSSettingsResponse(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {
		String requestBody = new ServiceTest().getRequestFromFile("getDNSSettings.txt");
		String operationName="testGetDNSSettings";
		Response response =getDNSSettings(apiTestData, requestBody,tokenMap);
		ObjectMapper mapper = new ObjectMapper();
		Map<String,String> responseMap=new HashMap<String, String>();
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String localDnsValue= getPathVal(jsonNode, "localDnsValue");
				String globalDnsValue= getPathVal(jsonNode, "globalDnsValue");
				responseMap.put("localDnsValue", localDnsValue);
				responseMap.put("globalDnsValue", globalDnsValue);
				Reporter.log("response localDnsValue = "+localDnsValue+" - globalDnsValue ="+globalDnsValue);
			}
		}else{
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode= getPathVal(jsonNode, "statusCode");
				String error= getPathVal(jsonNode, "error");
				responseMap.put("statusCode", statusCode);
				responseMap.put("error", error);
			}
		}
		return responseMap;
	}
	
	public  Map<String,String> setDNSSettingsResponse(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception {
		String operationName="testSetDNSSettings";
		String requestBody = new ServiceTest().getRequestFromFile("setDNSSettings.txt");
		Response response =setDNSSettings(apiTestData, requestBody,tokenMap);
		Map<String,String> responseMap=new HashMap<String, String>();
		ObjectMapper mapper = new ObjectMapper();
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String status= getPathVal(jsonNode, "status");
				responseMap.put("status", status);
			}
		}else{
			JsonNode jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				String statusCode= getPathVal(jsonNode, "statusCode");
				String error= getPathVal(jsonNode, "error");
				responseMap.put("statusCode", statusCode);
				responseMap.put("error", error);
			}
		}
		return responseMap;
	}
		
	/**
	 * UserStory# Description:CDCDWR2-43:CCPA WS2 | Web No Sell, App Sell/No Sell | Integrate with CHUB GetCustomerDoNotSell API
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "DNSApi","testGetAndSetSubscriberID_DNSSettings",Group.GLOBAL,Group.SPRINT  })
	public void testGetAndSetSubscriberID_DNSSettings(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getDNSSettings test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Use input idType as SubscriberID,brand as MbyT,id as 3472998170(msisdn)");
		Reporter.log("Step 1: Get the response getDNSSettings details.");
		Reporter.log("Step 2: Verify getDNSSettings details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String,String> tokenMap=new HashMap<String,String>();
		tokenMap.put("idType", "SubscriberID");
		tokenMap.put("brand", "MbyT");
		tokenMap.put("id", "3472998170");
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("ban", apiTestData.getBan());
		
		Map<String,String> responseDNSMap=getDNSSettingsResponse(apiTestData,tokenMap);
		Assert.assertNotNull(responseDNSMap.get("localDnsValue"),"Invalid localDnsValue ");
		Assert.assertNotNull(responseDNSMap.get("globalDnsValue"),"Invalid globalDnsValue ");
		
		tokenMap.put("dnsSetting", responseDNSMap.get("localDnsValue").equalsIgnoreCase("Sell")?"DoNotSell":"Sell");
		tokenMap.put("dnsType", responseDNSMap.get("globalDnsValue").equalsIgnoreCase("Sell")?"Global":"Global");
		
		Map<String,String> responseSetDNSMap=setDNSSettingsResponse(apiTestData,tokenMap);
		Assert.assertEquals(responseSetDNSMap.get("status"),"success","Invalid status");
		
		Map<String,String> responseUpdatedDNSMap=getDNSSettingsResponse(apiTestData,tokenMap);
		Assert.assertNotEquals(responseUpdatedDNSMap.get("localDnsValue"),responseDNSMap.get("localDnsValue"),"Updating dnsValue failed.");
		Assert.assertNotEquals(responseUpdatedDNSMap.get("globalDnsValue"),responseDNSMap.get("globalDnsValue"),"Updating dnsValue failed.");
	}
	
	/**
	 * UserStory# Description:CDCDWR2-43:CCPA WS2 | Web No Sell, App Sell/No Sell | Integrate with CHUB GetCustomerDoNotSell API
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "DNSApi","testGetAndSetSubscriberID_TFB_DNSSettings",Group.GLOBAL,Group.SPRINT  })
	public void testGetAndSetSubscriberID_TFB_DNSSettings(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getDNSSettings test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Use input idType as SubscriberID,brand as TFB,id as 3472998170(msisdn)");
		Reporter.log("Step 1: Get the response getDNSSettings details.");
		Reporter.log("Step 2: Verify getDNSSettings details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String,String> tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("Msisdn", apiTestData.getMsisdn());
		tokenMap.put("idType", "SubscriberID");
		tokenMap.put("brand", "TFB");
		tokenMap.put("id", "3472998170");
		
		Map<String,String> responseDNSMap=getDNSSettingsResponse(apiTestData,tokenMap);
		Assert.assertNotNull(responseDNSMap.get("localDnsValue"),"Invalid localDnsValue ");
		Assert.assertNotNull(responseDNSMap.get("globalDnsValue"),"Invalid globalDnsValue ");
	}
	
	/**
	 * UserStory# Description:CDCDWR2-43:CCPA WS2 | Web No Sell, App Sell/No Sell | Integrate with CHUB GetCustomerDoNotSell API
	 * 
	 * @param data
	 * @param myTmoData
	 * @throws Exception 
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "DNSApi","testGetAndSetTMOID_DNSSettings",Group.GLOBAL,Group.SPRINT  })
	public void testGetAndSetTMOID_DNSSettings(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getDNSSettings test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Use input idType as TMOID,brand as Magenta,id as U-23cfeec2-2a91-4e5e-89a0-65922367a582(IAM UserID)");
		Reporter.log("Step 1: Get the response getDNSSettings details.");
		Reporter.log("Step 2: Verify getDNSSettings details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String,String> tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("Msisdn", apiTestData.getMsisdn());
		tokenMap.put("idType", "TMOID");
		tokenMap.put("brand", "Magenta");
		tokenMap.put("localDnsValue", "Sell");
		tokenMap.put("dnsType", "Global");
		tokenMap.put("id", "U-23cfeec2-2a91-4e5e-89a0-65922367a582");
		
		CustomerAccountApiV1 accountApiV1=new CustomerAccountApiV1();
		Map<String,String> responseDNSMap=accountApiV1.getCustomerDNSDetailsResponseMap(apiTestData,tokenMap);
		Assert.assertNotNull(responseDNSMap.get("localDnsValue"),"Invalid localDnsValue ");
		Assert.assertNotNull(responseDNSMap.get("globalDnsValue"),"Invalid globalDnsValue ");
		
		tokenMap.put("dnsSetting", responseDNSMap.get("localDnsValue").equalsIgnoreCase("Sell")?"DoNotSell":"Sell");
		Map<String,String> responseSetDNSMap=setDNSSettingsResponse(apiTestData,tokenMap);
		Assert.assertEquals(responseSetDNSMap.get("status"),"success","Invalid status");
		
		Map<String,String> responseUpdatedDNSMap=accountApiV1.getCustomerDNSDetailsResponseMap(apiTestData,tokenMap);
		Assert.assertNotEquals(responseUpdatedDNSMap.get("localDnsValue"),responseDNSMap.get("localDnsValue"),"Updating dnsValue failed.");
		Assert.assertNotEquals(responseUpdatedDNSMap.get("globalDnsValue"),responseDNSMap.get("globalDnsValue"),"Updating dnsValue failed.");
	}
	
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "DNSApi","testGetDNSSettings_400",Group.GLOBAL,Group.SPRINT  })
	public void testGetDNSSettings_400(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getDNSSettings test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response getDNSSettings details.");
		Reporter.log("Step 2: Verify getDNSSettings details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String,String> tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("Msisdn", apiTestData.getMsisdn());
		tokenMap.put("idType", "AccountID1");
		tokenMap.put("brand", "TVision1");
		tokenMap.put("id", "suneelatest@105");
		tokenMap.put("dnsSetting", "Sell1");
		tokenMap.put("dnsType", "Global1");
		Map<String,String> responseDNSMap=getDNSSettingsResponse(apiTestData,tokenMap);
		Assert.assertEquals(responseDNSMap.get("statusCode"),"400","Invalid statusCode");
		Assert.assertEquals(responseDNSMap.get("error"),"Bad Request","Invalid error");
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "DNSApi","testSetDNSSettings_400",Group.GLOBAL,Group.SPRINT  })
	public void testSetDNSSettings_400(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getDNSSettings test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response getDNSSettings details.");
		Reporter.log("Step 2: Verify getDNSSettings details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String,String> tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("Msisdn", apiTestData.getMsisdn());
		tokenMap.put("idType", "AccountID");
		tokenMap.put("brand", "TVision");
		tokenMap.put("id", "suneelatest@105");
		tokenMap.put("dnsSetting", "Sell1");
		tokenMap.put("dnsType", "Global1");
		
		Map<String,String> responseSetDNSMap=setDNSSettingsResponse(apiTestData,tokenMap);
		Assert.assertEquals(responseSetDNSMap.get("statusCode"),"400","Invalid statusCode");
		Assert.assertEquals(responseSetDNSMap.get("error"),"Bad Request","Invalid error");
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "DNSApi","testGetDNSSettings_400",Group.GLOBAL,Group.SPRINT  })
	public void testGetDNSSettings_503(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getDNSSettings test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response getDNSSettings details.");
		Reporter.log("Step 2: Verify getDNSSettings details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String,String> tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("Msisdn", apiTestData.getMsisdn());
		tokenMap.put("idType", "AccountID");
		tokenMap.put("brand", "TVision");
		tokenMap.put("id", "suneelatest@105");
		tokenMap.put("dnsSetting", "Sell");
		tokenMap.put("dnsType", "Global");
		Map<String,String> responseDNSMap=getDNSSettingsResponse(apiTestData,tokenMap);
		Assert.assertEquals(responseDNSMap.get("statusCode"),"503","Invalid statusCode");
		Assert.assertEquals(responseDNSMap.get("error"),"Service Unavailable","Invalid error");
	}
	@Test(dataProvider = "byColumnName", enabled = true, groups = { "DNSApi","testSetDNSSettings_400",Group.GLOBAL,Group.SPRINT  })
	public void testSetDNSSettings_503(ControlTestData data, ApiTestData apiTestData) throws Exception {
		Reporter.log("TestName: getDNSSettings test");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("Step 1: Get the response getDNSSettings details.");
		Reporter.log("Step 2: Verify getDNSSettings details returned successfully or not");
		Reporter.log("Step 3: Verify Success services response code|Response code should be 200 OK.");
		Reporter.log("================================");
		Reporter.log("Actual Result:");
		Map<String,String> tokenMap=new HashMap<String,String>();
		tokenMap.put("ban", apiTestData.getBan());
		tokenMap.put("Msisdn", apiTestData.getMsisdn());
		tokenMap.put("idType", "AccountID");
		tokenMap.put("brand", "TVision");
		tokenMap.put("id", "suneelatest@105");
		tokenMap.put("dnsSetting", "Sell");
		tokenMap.put("dnsType", "Global");
		
		Map<String,String> responseSetDNSMap=setDNSSettingsResponse(apiTestData,tokenMap);
		Assert.assertEquals(responseSetDNSMap.get("statusCode"),"503","Invalid statusCode");
		Assert.assertEquals(responseSetDNSMap.get("error"),"Service Unavailable","Invalid error");
	}
}

