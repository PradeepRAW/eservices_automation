package com.tmobile.eservices.qa.tmng.acceptance;

import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.functional.CartPage;
import com.tmobile.eservices.qa.pages.tmng.functional.CheckOutPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PdpPage;
import com.tmobile.eservices.qa.pages.tmng.functional.PlpPage;
import com.tmobile.eservices.qa.tmng.TmngCommonLib;

public class OrderPlacementTest  extends TmngCommonLib {
	
	/**
	 *	Magenta Plan EIP:  add Phone, Tablet, Wearable, BYOD Phone , Tablet and accessory
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutMaxLinesForMixedProductsWithMagentaPlanEIP(TMNGData tMNGData) {
		Reporter.log("End To End Flow: Magenta Plan EIP:  add Phone, Tablet, Wearable, BYOD Phone , Tablet and accessory");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Select monthly payment and Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Add Tablet, Wearable, BYOD Phone, BYOD Tablet and Accessory to cart |All type of products should be added into cart");
		Reporter.log("6. Verify Error message below Add More Lines on Cart | 'Want more lines? Please give us a call at 1-844-889-4983' should be displayed.");
		Reporter.log("7. Verify Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's on Cart | Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's should be disabled");
		Reporter.log("8. Verify Add a Accessories img's on Cart | Add a Accessories img  should be enabled");
		Reporter.log("9. Verify Duplicate CTa for all lines | Duplicate CTA  should be disables for all lines in the cart");
		Reporter.log("10. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("11. Fill all personal and shipping information and click on next | Payment and credit page should be displayed");
		Reporter.log("12. Fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("13. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.verifyCartPageLoaded();
		
		cartPage.addATablettoCartFromCartPage(tMNGData);		
		cartPage.addAWearabletoCartFromCartPage(tMNGData);		
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);		
		
		cartPage.clickOnMagentaPlan();
		cartPage.verifyMagentaPlanSelected();
		
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);	
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());	

	}
	
	/**
	 *	Magenta Plus Plan EIP:  add Phone, Tablet, Wearable, BYOD Phone , Tablet and accessory
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutMaxLinesForMixedProductsWithMagentaPlusPlanEIP(TMNGData tMNGData) {
		Reporter.log("End To End Flow: Magenta Plus Plan EIP:  add Phone, Tablet, Wearable, BYOD Phone , Tablet and accessory");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Select monthly payment and Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Add Tablet, Wearable, BYOD Phone, BYOD Tablet and Accessory to cart |All type of products should be added into cart");
		Reporter.log("6. Verify Error message below Add More Lines on Cart | 'Want more lines? Please give us a call at 1-844-889-4983' should be displayed.");
		Reporter.log("7. Verify Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's on Cart | Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's should be disabled");
		Reporter.log("8. Verify Add a Accessories img's on Cart | Add a Accessories img  should be enabled");
		Reporter.log("9. Verify Duplicate CTa for all lines | Duplicate CTA  should be disables for all lines in the cart");
		Reporter.log("10. Select magenta plus plan Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("11. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log("12. fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("13. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		
		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.addAWearabletoCartFromCartPage(tMNGData);
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
		
		cartPage.clickOnMagentaPlusPlan();
		cartPage.verifyMagentaPlusPlanSelected();
		
		cartPage.completeTPPInCartPage(tMNGData);
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesSCC(tMNGData);
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());	

	}
	
	/**
	 *	Essentials Plan EIP:  add Phone, Phone, Tablet, BYOD Phone , BYOD Tablet and accessory
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutMaxLinesForMixedProductsWithEssentialPlanEIP(TMNGData tMNGData) {
		Reporter.log("End To End Flow: Essentials Plan EIP:  add Phone, Phone, Tablet, BYOD Phone , BYOD Tablet and accessory");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Select monthly payment and Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Add Tablet, phone, BYOD Phone, BYOD Tablet and Accessory to cart |All type of products should be added into cart");
		Reporter.log("6. Verify Error message below Add More Lines on Cart | 'Want more lines? Please give us a call at 1-844-889-4983' should be displayed.");
		Reporter.log("7. Verify Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's on Cart | Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's should be disabled");
		Reporter.log("8. Verify Add a Accessories img's on Cart | Add a Accessories img  should be enabled");
		Reporter.log("9. Verify Duplicate CTa for all lines | Duplicate CTA  should be disables for all lines in the cart");
		Reporter.log("10. Select Essential plan Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("11. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log("12. fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("13. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		
		cartPage.addATablettoCartFromCartPage(tMNGData);
		cartPage.addAnAccessorytoCartFromCartPage(tMNGData);
		
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.addATablettoCartFromCartPage(tMNGData);
		
		cartPage.clickOnTMobileEssentialsPlan();
		cartPage.verifyTMobileEssentialsPlanSelected();

		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);	
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());	

	}
	
	/**
	 *  Magenta Plan Mix:  add Phone EIP + add-on, Tablet EIP, Wearable FRP, BYOD Phone , Tablet FRP, Accessory
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutMaxLinesForMixedProductsandMixedPaymentTypesWithMagentaPlan(TMNGData tMNGData) {
		Reporter.log("End To End Flow: Magenta Plan Mix:  add Phone EIP + add-on, Tablet EIP, Wearable FRP, BYOD Phone , Tablet FRP, Accessory");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Select pay in full payment and Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Add add on for Phone EIP, Tablet EIP, Wearable FRP, BYOD Phone, Tablet FRP and Accessory to cart |All type of products should be added into cart");
		Reporter.log("6. Verify Error message below Add More Lines on Cart | 'Want more lines? Please give us a call at 1-844-889-4983' should be displayed.");
		Reporter.log("7. Verify Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's on Cart | Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's should be disabled");
		Reporter.log("8. Verify Add a Accessories img's on Cart | Add a Accessories img  should be enabled");
		Reporter.log("9. Verify Duplicate CTa for all lines | Duplicate CTA  should be disables for all lines in the cart");
		Reporter.log("10. Select Essential plan Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("11. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log("12. fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("13. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnMagentaPlan();
		cartPage.verifyMagentaPlanSelected();			
		cartPage.clickFirstAddOn();
		
		cartPage.addATablettoCartFromCartPage(tMNGData);
		
		cartPage.clickOnAddAWearableLinkOnCart();
		
		PlpPage wearablePlpPage = new PlpPage(getDriver());
		wearablePlpPage.verifyWatchesPlpPageLoaded();
		wearablePlpPage.clickDeviceWithAvailability(tMNGData.getWatchName());
		
		PdpPage wearablePdpPage =new PdpPage(getDriver());
		wearablePdpPage.verifyWatchesPdpPageLoaded();
		wearablePdpPage.clickPayInFullPaymentOption();
		wearablePdpPage.clickOnAddToCartBtn();			
		cartPage.verifyCartPageLoaded();

//		cartPage.addAByodPhoneToCartFromCartPageFromPLP(tMNGData);
 
		cartPage.clickOnAddATabletLinkOnCart();
		PlpPage tabletPlpPage = new PlpPage(getDriver());
		tabletPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		tabletPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		
		PdpPage tabletPdpPage = new PdpPage(getDriver());
		tabletPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletPdpPage.clickPayInFullPaymentOption();
		tabletPdpPage.clickOnAddToCartBtn();			
		cartPage.verifyCartPageLoaded();			
		
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		
		PlpPage accessroyPlp = new PlpPage(getDriver());
		accessroyPlp.verifyAccessoriesPlpPageLoaded();
		accessroyPlp.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryPdp = new PdpPage(getDriver());
		accessoryPdp.verifyAccessoriesPdpPageLoaded();
		accessoryPdp.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);	
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());	

	}
	
	/**
	 * Magenta Plus Plan Mix:  add Phone EIP + add-on, Tablet EIP, Wearable FRP, BYOD Phone , Tablet FRP, Accessory
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutMaxLinesForMixedProductsandMixedPaymentTypesWithMagentaPlusPlan(TMNGData tMNGData) {
		Reporter.log("End To End Flow: Magenta plus Plan Mix:  add Phone EIP + add-on, Tablet EIP, Wearable FRP, BYOD Phone , Tablet FRP, Accessory");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Select pay in full payment and Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Add add on for Phone EIP, Tablet EIP, Wearable FRP, BYOD Phone, Tablet FRP and Accessory to cart |All type of products should be added into cart");
		Reporter.log("6. Verify Error message below Add More Lines on Cart | 'Want more lines? Please give us a call at 1-844-889-4983' should be displayed.");
		Reporter.log("7. Verify Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's on Cart | Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's should be disabled");
		Reporter.log("8. Verify Add a Accessories img's on Cart | Add a Accessories img  should be enabled");
		Reporter.log("9. Verify Duplicate CTa for all lines | Duplicate CTA  should be disables for all lines in the cart");
		Reporter.log("10. Select Essential plan Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("11. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log("12. fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("13. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnMagentaPlusPlan();
		cartPage.verifyMagentaPlusPlanSelected();			
		cartPage.clickFirstAddOn();
		cartPage.clickNextCTAOnZipCodeModal();
		
		cartPage.clickOnAddATabletLinkOnCart();
		
		PlpPage tabletPlpPage = new PlpPage(getDriver());
		tabletPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		tabletPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		
		PdpPage tabletPdpPage =new PdpPage(getDriver());
		tabletPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletPdpPage.clickOnAddToCartBtn();			
		cartPage.verifyCartPageLoaded();
		
		cartPage.clickOnAddAWearableLinkOnCart();
		
		PlpPage wearablePlpPage = new PlpPage(getDriver());
		wearablePlpPage.verifyWatchesPlpPageLoaded();
		wearablePlpPage.clickDeviceWithAvailability(tMNGData.getWatchName());
		
		PdpPage wearablePdpPage =new PdpPage(getDriver());
		wearablePdpPage.verifyWatchesPdpPageLoaded();
		wearablePdpPage.clickPayInFullPaymentOption();
		wearablePdpPage.clickOnAddToCartBtn();			
		cartPage.verifyCartPageLoaded();

		//cartPage.addAByodPhoneToCartFromCartPageFromPLP(tMNGData);
		
		cartPage.clickOnAddATabletLinkOnCart();
		
		tabletPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		tabletPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		
		tabletPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletPdpPage.clickPayInFullPaymentOption();
		tabletPdpPage.clickOnAddToCartBtn();			
		cartPage.verifyCartPageLoaded();			
		
		cartPage.clickOnAddAnAccessoryLinkOnCart();

		PlpPage accessroyPlp = new PlpPage(getDriver());
		accessroyPlp.verifyAccessoriesPlpPageLoaded();
		accessroyPlp.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryPdp = new PdpPage(getDriver());
		accessoryPdp.verifyAccessoriesPdpPageLoaded();
		accessoryPdp.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		
		/*cartPage.verifyLineMaxLimitReachedMessageOnCart();
		cartPage.verifyAddAPhonebtnDisabled();
		cartPage.verifyAddAWearablebtnDisabled();
		cartPage.verifyAddATabletbtnDisabled();
		cartPage.verifyBYODbtnDisabled();
		cartPage.verifyAddAAcessorybtnEnabled();
		cartPage.verifyDuplicateCTADisabledForAllLines();*/
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);	
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());	

	}
	
	/**
	 *	Essentials Plan FRP:  add Phone, Phone, Tablet, BYOD Phone , BYOD Tablet and accessory
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutMaxLinesForMixedProductsandMixedPaymentTypesWithEssentialPlan(TMNGData tMNGData) {
		Reporter.log("End To End Flow: Essentials Plan Mix:  add Phone EIP + add-on, Phone FRP, Tablet EIP, BYOD Phone , BYOD Tablet, Accessory");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Add add-on for phone EIP, add phone FRP, Tablet EIP, BYOD Phone, BYOD Tablet and Accessory to cart | All type of products should be added into cart");
		Reporter.log("6. Verify Error message below Add More Lines on Cart | 'Want more lines? Please give us a call at 1-844-889-4983' should be displayed.");
		Reporter.log("7. Verify Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's on Cart | Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's should be disabled");
		Reporter.log("8. Verify Add a Accessories img's on Cart | Add a Accessories img  should be enabled");
		Reporter.log("9. Verify Duplicate CTa for all lines | Duplicate CTA  should be disables for all lines in the cart");
		Reporter.log("10. Select Essential plan Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("11. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log("12. fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("13. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		cartPage.clickFirstAddOn();
		
		cartPage.addATablettoCartFromCartPage(tMNGData);
		
		cartPage.clickOnAddAPhoneLinkOnCart();
		
		PlpPage phonesPlpPage = new PlpPage(getDriver());
		phonesPlpPage.verifyPhonesPlpPageLoaded();
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getSecondDeviceName());

		PdpPage phonesPdpPage = new PdpPage(getDriver());
		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.clickPayInFullPaymentOption();
		phonesPdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();

		/*cartPage.addAByodPhoneToCartFromCartPageFromPLP(tMNGData);
		cartPage.addAByodTabletToCartFromCartPageFromPLP(tMNGData);*/
		
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		
		PlpPage accessroyPlp = new PlpPage(getDriver());
		accessroyPlp.verifyAccessoriesPlpPageLoaded();
		accessroyPlp.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryPdp = new PdpPage(getDriver());
		accessoryPdp.verifyAccessoriesPdpPageLoaded();
		accessoryPdp.clickPayInFullPaymentOption();
		accessoryPdp.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		
		cartPage.clickOnTMobileEssentialsPlan();
		cartPage.verifyTMobileEssentialsPlanSelected();
		
		/*cartPage.verifyLineMaxLimitReachedMessageOnCart();
		cartPage.verifyAddAPhonebtnDisabled();
		cartPage.verifyAddAWearablebtnDisabled();
		cartPage.verifyAddATabletbtnDisabled();
		cartPage.verifyBYODbtnDisabled();
		cartPage.verifyAddAAcessorybtnEnabled();
		cartPage.verifyDuplicateCTADisabledForAllLines();*/
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);	
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());	

	}
	
	/**
	 *	Magenta Plan FRP:  add Phone, Tablet, Wearable, BYOD Phone , Tablet and accessory
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutMaxLinesForMixedProductsWithMagentaPlanFRP(TMNGData tMNGData) {
		Reporter.log("End To End Flow: Magenta Plan FRP:  add Phone, Tablet, Wearable, BYOD Phone , Tablet and accessory");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Select pay in full payment and Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Add Tablet, Wearable, BYOD Phone, BYOD Tablet and Accessory to cart |All type of products should be added into cart");
		Reporter.log("6. Verify Error message below Add More Lines on Cart | 'Want more lines? Please give us a call at 1-844-889-4983' should be displayed.");
		Reporter.log("7. Verify Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's on Cart | Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's should be disabled");
		Reporter.log("8. Verify Add a Accessories img's on Cart | Add a Accessories img  should be enabled");
		Reporter.log("9. Verify Duplicate CTa for all lines | Duplicate CTA  should be disables for all lines in the cart");
		Reporter.log("10. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("11. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log("12. fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("13. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickPayInFullPaymentOption();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		
		cartPage.clickOnAddATabletLinkOnCart();
		
		PlpPage tabletPlpPage = new PlpPage(getDriver());
		tabletPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		tabletPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		
		PdpPage tabletPdpPage =new PdpPage(getDriver());
		tabletPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletPdpPage.clickPayInFullPaymentOption();
		tabletPdpPage.clickOnAddToCartBtn();			
		cartPage.verifyCartPageLoaded();
		
		cartPage.clickOnAddAWearableLinkOnCart();
		
		PlpPage wearablePlpPage = new PlpPage(getDriver());
		wearablePlpPage.verifyWatchesPlpPageLoaded();
		wearablePlpPage.clickDeviceWithAvailability(tMNGData.getWatchName());
		
		PdpPage wearablePdpPage =new PdpPage(getDriver());
		wearablePdpPage.verifyWatchesPdpPageLoaded();
		wearablePdpPage.clickPayInFullPaymentOption();
		wearablePdpPage.clickOnAddToCartBtn();			
		cartPage.verifyCartPageLoaded();

/*		cartPage.addAByodPhoneToCartFromCartPageFromPLP(tMNGData);
		cartPage.addAByodTabletToCartFromCartPageFromPLP(tMNGData);*/
		
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		PlpPage accessroyPlp = new PlpPage(getDriver());
		accessroyPlp.verifyAccessoriesPlpPageLoaded();
		accessroyPlp.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryPdp = new PdpPage(getDriver());
		accessoryPdp.verifyAccessoriesPdpPageLoaded();
		accessoryPdp.clickPayInFullPaymentOption();
		accessoryPdp.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		
		cartPage.clickOnMagentaPlan();
		cartPage.verifyMagentaPlanSelected();
		
		/*cartPage.verifyLineMaxLimitReachedMessageOnCart();
		cartPage.verifyAddAPhonebtnDisabled();
		cartPage.verifyAddAWearablebtnDisabled();
		cartPage.verifyAddATabletbtnDisabled();
		cartPage.verifyBYODbtnDisabled();
		cartPage.verifyAddAAcessorybtnEnabled();
		cartPage.verifyDuplicateCTADisabledForAllLines();*/
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);	
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());		

	}
	
	/**
	 *	Magenta Plus Plan FRP:  add Phone, Tablet, Wearable, BYOD Phone , Tablet and accessory
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutMaxLinesForMixedProductsWithMagentaPlusPlanFRP(TMNGData tMNGData) {
		Reporter.log("End To End Flow: Magenta Plus Plan FRP:  add Phone, Tablet, Wearable, BYOD Phone , Tablet and accessory");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Select pay in full payment and Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Add Tablet, Wearable, BYOD Phone, BYOD Tablet and Accessory to cart |All type of products should be added into cart");
		Reporter.log("6. Verify Error message below Add More Lines on Cart | 'Want more lines? Please give us a call at 1-844-889-4983' should be displayed.");
		Reporter.log("7. Verify Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's on Cart | Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's should be disabled");
		Reporter.log("8. Verify Add a Accessories img's on Cart | Add a Accessories img  should be enabled");
		Reporter.log("9. Verify Duplicate CTa for all lines | Duplicate CTA  should be disables for all lines in the cart");
		Reporter.log("10. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("11. fill all personal and shipping information and cl+ick on next | payment and credit page should be displayed");
		Reporter.log("12. fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("13. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickPayInFullPaymentOption();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		
		cartPage.clickOnAddATabletLinkOnCart();
		
		PlpPage tabletPlpPage = new PlpPage(getDriver());
		tabletPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		tabletPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		
		PdpPage tabletPdpPage =new PdpPage(getDriver());
		tabletPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletPdpPage.clickPayInFullPaymentOption();
		tabletPdpPage.clickOnAddToCartBtn();			
		cartPage.verifyCartPageLoaded();
		
		cartPage.clickOnAddAWearableLinkOnCart();
		
		PlpPage wearablePlpPage = new PlpPage(getDriver());
		wearablePlpPage.verifyWatchesPlpPageLoaded();
		wearablePlpPage.clickDeviceWithAvailability(tMNGData.getWatchName());

		PdpPage wearablePdpPage =new PdpPage(getDriver());
		wearablePdpPage.verifyWatchesPdpPageLoaded();
		wearablePdpPage.clickPayInFullPaymentOption();
		wearablePdpPage.clickOnAddToCartBtn();			
		cartPage.verifyCartPageLoaded();

		/*cartPage.addAByodPhoneToCartFromCartPageFromPLP(tMNGData);
		cartPage.addAByodTabletToCartFromCartPageFromPLP(tMNGData);*/
		
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		
		PlpPage accessroyPlp = new PlpPage(getDriver());
		accessroyPlp.verifyAccessoriesPlpPageLoaded();
		accessroyPlp.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryPdp = new PdpPage(getDriver());
		accessoryPdp.verifyAccessoriesPdpPageLoaded();
		accessoryPdp.clickPayInFullPaymentOption();
		accessoryPdp.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		
		cartPage.clickOnMagentaPlusPlan();
		cartPage.verifyMagentaPlusPlanSelected();
		
		/*cartPage.verifyLineMaxLimitReachedMessageOnCart();
		cartPage.verifyAddAPhonebtnDisabled();
		cartPage.verifyAddAWearablebtnDisabled();
		cartPage.verifyAddATabletbtnDisabled();
		cartPage.verifyBYODbtnDisabled();
		cartPage.verifyAddAAcessorybtnEnabled();
		cartPage.verifyDuplicateCTADisabledForAllLines();*/
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);	
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());	

	}
	
	/**
	 *	Essentials Plan FRP:  add Phone, Phone, Tablet, BYOD Phone , BYOD Tablet and accessory
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutMaxLinesForMixedProductsWithEssentialPlanFRP(TMNGData tMNGData) {
		Reporter.log("End To End Flow: Essentials Plan FRP:  add Phone, Phone, Tablet, BYOD Phone , BYOD Tablet and accessory");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO   | TMO home page should be launched");
		Reporter.log("2. Click  On Phones | Phones PLP page should be displayed");
		Reporter.log("3. Select a phone from main PLP | Phones PDP should  be displayed");
		Reporter.log("4. Select pay in full payment and Click Add to Cart in PDP | Cart Page should  be displayed");
		Reporter.log("5. Add Tablet, phone, BYOD Phone, BYOD Tablet and Accessory to cart |All type of products should be added into cart");
		Reporter.log("6. Verify Error message below Add More Lines on Cart | 'Want more lines? Please give us a call at 1-844-889-4983' should be displayed.");
		Reporter.log("7. Verify Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's on Cart | Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's should be disabled");
		Reporter.log("8. Verify Add a Accessories img's on Cart | Add a Accessories img  should be enabled");
		Reporter.log("9. Verify Duplicate CTa for all lines | Duplicate CTA  should be disables for all lines in the cart");
		Reporter.log("10. Select Essential plan Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("11. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log("12. fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("13. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickPayInFullPaymentOption();
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.selectContinueAsGuest();

		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		
		cartPage.clickOnAddATabletLinkOnCart();
		
		PlpPage tabletPlpPage = new PlpPage(getDriver());
		tabletPlpPage.verifyTabletsAndDevicesPlpPageLoaded();
		tabletPlpPage.clickDeviceWithAvailability(tMNGData.getTabletName());
		
		PdpPage tabletPdpPage =new PdpPage(getDriver());
		tabletPdpPage.verifyTabletsAndDevicesPdpPageLoaded();
		tabletPdpPage.clickPayInFullPaymentOption();
		tabletPdpPage.clickOnAddToCartBtn();			
		cartPage.verifyCartPageLoaded();
		
		cartPage.clickOnAddAPhoneLinkOnCart();
		
		PlpPage phonesPlpPage = new PlpPage(getDriver());
		phonesPlpPage.verifyPhonesPlpPageLoaded();
		phonesPlpPage.clickDeviceWithAvailability(tMNGData.getSecondDeviceName());

		phonesPdpPage.verifyPhonePdpPageLoaded();
		phonesPdpPage.clickPayInFullPaymentOption();
		phonesPdpPage.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		
		/*cartPage.addAByodPhoneToCartFromCartPageFromPLP(tMNGData);
		cartPage.addAByodTabletToCartFromCartPageFromPLP(tMNGData);*/
		
		cartPage.clickOnAddAnAccessoryLinkOnCart();
		
		PlpPage accessroyPlp = new PlpPage(getDriver());
		accessroyPlp.verifyAccessoriesPlpPageLoaded();
		accessroyPlp.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryPdp = new PdpPage(getDriver());
		accessoryPdp.verifyAccessoriesPdpPageLoaded();
		accessoryPdp.clickPayInFullPaymentOption();
		accessoryPdp.clickOnAddToCartBtn();
		cartPage.verifyCartPageLoaded();
		
		cartPage.clickOnTMobileEssentialsPlan();
		cartPage.verifyTMobileEssentialsPlanSelected();
		
	/*	cartPage.verifyLineMaxLimitReachedMessageOnCart();
		cartPage.verifyAddAPhonebtnDisabled();
		cartPage.verifyAddAWearablebtnDisabled();
		cartPage.verifyAddATabletbtnDisabled();
		cartPage.verifyBYODbtnDisabled();
		cartPage.verifyAddAAcessorybtnEnabled();
		cartPage.verifyDuplicateCTADisabledForAllLines();*/
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);	
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());	

	}
	
	/**
	 *	Accessories with FRP:  add accessory with less than 65 cart value
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutAccessoriesWithLessThan49FRP(TMNGData tMNGData) {
		Reporter.log("End To End Flow: Accessories with FRP:  add accessory with less than 65 cart value");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessories should be displayed");
		Reporter.log("3. Select Accessories | PAccessories should be selected");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("6. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log("7. fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("8. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		navigateToAccessoryMainPDP(tMNGData);
		PdpPage accessoryDetailsPage = new PdpPage(getDriver());
		accessoryDetailsPage.clickPayInFullPaymentOption();
		accessoryDetailsPage.clickOnAddToCartBtn();
		accessoryDetailsPage.selectContinueAsGuest();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);	
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());	
	}
	
	/**
	 *	Accessories with FRP:  add accessory with more than 65 cart value
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutAccessoriesWithMoreThan49FRP(TMNGData tMNGData) {
		Reporter.log("End To End Flow: Accessories with FRP:  add accessory with more than 65 cart value");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO  | Application Should be Launched");
		Reporter.log("2. Click on Phones Link | Cell Phones page should be displayed");
		Reporter.log("3. Click on Accessories | Accessories should be displayed");
		Reporter.log("3. Select Accessories | Accessories should be selected");
		Reporter.log("4. Click on Add to Cart button | Cart page should be displayed");
		Reporter.log("5. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("6. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log("7. fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("8. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");
		
		PlpPage accessoryPlpPage = navigateToAccessoryPLP(tMNGData);
		accessoryPlpPage.clickOnSortDropdown();
		accessoryPlpPage.clickSortByPriceHighToLow();
		accessoryPlpPage.clickDeviceWithAvailability(tMNGData.getAccessoryName());
		PdpPage accessoryDetailsPage = new PdpPage(getDriver());
		accessoryDetailsPage.verifyAccessoriesPdpPageLoaded();
		accessoryDetailsPage.clickPayInFullPaymentOption();
		accessoryDetailsPage.clickOnAddToCartBtn();
		accessoryDetailsPage.selectContinueAsGuest();
		
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		
		cartPage.addAPhonetoCartFromCartPage(tMNGData);
		cartPage.clickOnCartContinueBtn();
		
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);	
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());	
	}
	
	/**
	 * CheckOut Limit number of lines on TMNG - Phones with FRP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutMaxLinesForPhoneswithFRP(TMNGData tMNGData) {
		Reporter.log("Test Case : CheckOut Limit number of lines on TMNG - Phones");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Phone link on Cart | Credit options modal should  be displayed");
		Reporter.log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("4. Select any Phones in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5. Select FRP proce and Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("6. Add 4 more Tablets to cart | 5 tablets should be added into cart");
		Reporter.log("7. Verify Error message below Add More Lines on Cart | 'Want more lines? Please give us a call at 1-844-889-4983' should be displayed.");
		Reporter.log("8. Verify Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's on Cart | Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's should be disabled");
		Reporter.log("9. Verify Add a Accessories img's on Cart | Add a Accessories img  should be enabled");
		Reporter.log("10. Verify Duplicate CTa for all lines | Duplicate CTA  should be disables for all lines in the cart");
		Reporter.log("11. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("12. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log("13. fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("14. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PdpPage phonesPdpPage = navigateToPhonesPDP(tMNGData);
		phonesPdpPage.clickOnAddToCartBtn();
		phonesPdpPage.clickPayInFullPaymentOption();
		phonesPdpPage.selectContinueAsGuest();
		CartPage cartPage = new CartPage(getDriver());
		cartPage.verifyCartPageLoaded();
		
		cartPage.clickOnDuplicateNTimes(4);
		cartPage.verifyDuplicateLinesCountOnCart(5);
		cartPage.verifyLineMaxLimitReachedMessageOnCart();
		cartPage.verifyAddAPhonebtnDisabled();
		cartPage.verifyAddAWearablebtnDisabled();
		cartPage.verifyAddATabletbtnDisabled();
		cartPage.verifyBYODbtnDisabled();
		cartPage.verifyAddAAcessorybtnEnabled();
		cartPage.verifyDuplicateCTADisabledForAllLines();
		
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);	
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());	

	}
	
	/**
	 * CheckOut Limit number of lines on TMNG - Phones with EIP
	 * 
	 * @param data
	 * @param tMNGData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.DESKTOP, Group.TPP })
	public void testE2ECheckOutMaxLinesForPhoneswithEIP(TMNGData tMNGData) {
		Reporter.log("Test Case : CheckOut Limit number of lines on TMNG - Phones");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch TMO empty cart  | empty Cart page should be displayed");
		Reporter.log("2. Click  Add a Phone link on Cart | Credit options modal should  be displayed");
		Reporter.log("3. Select Awesome credit options and click Done | Phones Mini PLP should  be displayed");
		Reporter.log("4. Select any Phones in mini PLP | Mini PDP Page should  be displayed");
		Reporter.log("5.Select EIP price and Click Add to Cart in mini PDP | Cart Page should  be displayed");
		Reporter.log("6. Add 4 more Tablets to cart | 5 tablets should be added into cart");
		Reporter.log("7. Verify Error message below Add More Lines on Cart | 'Want more lines? Please give us a call at 1-844-889-4983' should be displayed.");
		Reporter.log("8. Verify Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's on Cart | Add a Phone, Add a Wearable, Add a Tablet and BYOD CTA's should be disabled");
		Reporter.log("9. Verify Add a Accessories img's on Cart | Add a Accessories img  should be enabled");
		Reporter.log("10. Verify Duplicate CTa for all lines | Duplicate CTA  should be disables for all lines in the cart");
		Reporter.log("11. Click Continue button in Cart page | Checkout page should be displayed");
		Reporter.log("12. fill all personal and shipping information and click on next | payment and credit page should be displayed");
		Reporter.log("13. fill all payment and credit information , set security pin  | Review & submit page  should be displayed");
		Reporter.log("14. Click on Agree and Submit button | Conformation header should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		CartPage cartPage = navigateToCartPageBySelectingPhone(tMNGData);
		
		cartPage.verifyCartPageLoaded();
		cartPage.clickOnDuplicateNTimes(4);
		cartPage.verifyDuplicateLinesCountOnCart(5);
		cartPage.verifyLineMaxLimitReachedMessageOnCart();
		cartPage.verifyAddAPhonebtnDisabled();
		cartPage.verifyAddAWearablebtnDisabled();
		cartPage.verifyAddATabletbtnDisabled();
		cartPage.verifyBYODbtnDisabled();
		cartPage.verifyAddAAcessorybtnEnabled();
		cartPage.verifyDuplicateCTADisabledForAllLines();
		
		cartPage.clickOnCartContinueBtn();
		
		CheckOutPage checkOutPage = new CheckOutPage(getDriver());
		checkOutPage.completeCheckOutProcessForDevicesHCC(tMNGData);	
		checkOutPage.clickAgreeAndSubmitBtnInReviewSubmit();
		checkOutPage.verifyConfirmationPageHeaderContainsName(tMNGData.getFirstName());		

	}

}
