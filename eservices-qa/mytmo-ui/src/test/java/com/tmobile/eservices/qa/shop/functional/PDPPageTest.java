package com.tmobile.eservices.qa.shop.functional;

import com.tmobile.eservices.qa.pages.accounts.AccountOverviewPage;
import com.tmobile.eservices.qa.pages.global.LoginPage;
import org.testng.Reporter;
import org.testng.annotations.Test;
import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.global.ContactUSPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.shop.ConsolidatedRatePlanPage;
import com.tmobile.eservices.qa.pages.shop.DeviceIntentPage;
import com.tmobile.eservices.qa.pages.shop.DeviceProtectionPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.PLPPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;
import com.tmobile.eservices.qa.shop.ShopCommonLib;
import com.tmobile.eservices.qa.shop.ShopConstants;

public class PDPPageTest extends ShopCommonLib {

	/**
	 * US342770: PDP V2 Migration - Primary Area - Reviews
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP })
	public void testDeviceReviewRating(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case:US342770: PDP V2 Migration - Primary Area - Reviews");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones | Product list page should be displayed");
		Reporter.log("6. Select Device | Product details page should be displayed");
		Reporter.log("7. Verify review star rate icon | Star rate icon should be displayed");
		Reporter.log("8. Verify total number of review counts | Total review counts number should be displayed");
		Reporter.log(
				"9. Click Review Star icon or the review count text | Should open review section divison at the bottom of the PDP page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyReviewsStarRating();
		pdpPage.verifyReviewTab();
		pdpPage.clickReviewTab();
		pdpPage.verifyPositiveReviewCountForPDP();
		pdpPage.verifyNegativeReviewCountForPDP();
	}

	/**
	 * US492483 : V3 Gaps - plp/pdp Device, plp/pdp accessories, shop.
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testDeviceNameonPDPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("US492483 : V3 Gaps - plp/pdp Device, plp/pdp accessories, shop.");
		Reporter.log("Data Condition | IR PAH User");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on Shop tab on home page | User should be navigated to Shop Page ");
		Reporter.log("3. Click on the 'All Phones' link | PLP should be displayed");
		Reporter.log("4. Click on a Device | PDP Page should be displayed");
		Reporter.log(
				"5. Validate Device Name and Manufacture name | Device Name and Manufacture name should be displayed");
		Reporter.log("6. Verify Footers | Footers should not be displayed in Pdp page");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyDeviceNameAndManufacturerName(myTmoData.getDeviceName());
		pdpPage.verifyPDPFooter();
	}

	/**
	 * US380445 California Proposition 65 legal requirements : Modal(Standard, Jump
	 * and Std alone accessories) US380437 Standard Upgrade : California Proposition
	 * 65 legal requirements
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testCaliforniaPropositionLegalRequirementsModalStandardUpgradeFlow(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test Case :  US380445 California Proposition 65 legal requirements  : Modal(Standard, Jump and Std alone accessories)");
		Reporter.log("Test Case :  US380437 Standard Upgrade : California Proposition 65 legal requirements");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on Deive | PDP page should be displayed");
		Reporter.log(
				"6. verify text 'California Residents: See the California Proposition 65 WARNING' below device specification section | 'California Residents: See the California Proposition 65 WARNING' should be displayed");
		Reporter.log(
				"7. click on 'California Proposition 65 WARNING' link | California Proposition 65 Warning modal should be displayed");
		Reporter.log(
				"8.verify 'California Proposition 65 Warning ' header |  'California Proposition 65 Warning ' header should be displayed");
		Reporter.log(
				"9. verify ' WARNING: Cancer and Reproductive Harm - www.P65warnings.ca.gov' message | ' WARNING: Cancer and Reproductive Harm - www.P65warnings.ca.gov' message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyCaliforniaResidentsWarning();

		pdpPage.clickonCaliforniaPropositionLink();
		pdpPage.verifyCaliforniaPropositionWarningModal();
		pdpPage.clickCaliforniaPropositionWarningModelCloseButton();
	}

	/**
	 * US306680: MyTMO - Additional Terms - Standard Upgrade Flow - Device PLP
	 * Pricing Display US306681: MyTMO - Additional Terms - Standard Upgrade Flow -
	 * Device PDP Pricing Display US351480: [Unfinished] MyTMO - Additional Terms -
	 * Standard Upgrade Flow - Device PLP Pricing Display US351465: [Unfinished]
	 * MyTMO - Additional Terms - Standard Upgrade Flow - Device PDP Pricing Display
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testEIPdevicesPricingInPDPpage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case:US306680, US351480: MyTMO - Additional Terms - Standard Upgrade Flow - Device PLP Pricing Display");
		Reporter.log(
				"Test Case:US306681, US351465: MyTMO - Additional Terms - Standard Upgrade Flow - Device PDP Pricing Display");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click on see all phones | Product list  page should be displayed");
		Reporter.log("6. Verify device rating stars | Device rating stars should be displayed");
		Reporter.log("7. Verify EIP price with dollar symbol | EIP price with dollar symbol should be displayed");
		Reporter.log("8. Verify EIP 'Monthly' text | EIP 'Monthly' text should be displayed");
		Reporter.log("9. Verify Installment months | Installment months should be displayed");
		Reporter.log("5. Verify color picker | Color picker should be displayed");
		Reporter.log("10. Select device | Product details  page should be displayed");
		Reporter.log("11. Verify EIP price with dollar symbol | EIP price with dollar symbol should be displayed");
		Reporter.log("12. Verify EIP 'Monthly' text | EIP 'Monthly' text should be displayed");
		Reporter.log("13. Verify Installment months | Installment months should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PLPPage plpPage = new PLPPage(getDriver());
		navigateToPLPPageBySeeAllPhones(myTmoData);
		plpPage.verifyDeviceStars();
		plpPage.verifyDollarSign();
		plpPage.verifyEIPPrice();
		plpPage.verifyDeviceEIPMonthlyText();
		plpPage.verifyInstallmentTerm();
		plpPage.verifyFRPPriceText();
		//String downPayment = plpPage.getDeviceDownPaymentInPLP();
		String monthlyPayment = plpPage.getDeviceMonthlyPaymentInPLP();
		String loanTerm = plpPage.getDeviceLoanTermInPLP();
		plpPage.clickDeviceByName("Apple iPhone XS Max");
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.verifyColorPicker();
		pdpPage.verifyEIPDollarsSign();
		pdpPage.verifyEIPPrice();
		pdpPage.verifyDeviceEIPMonthlyText();
		pdpPage.verifyDeviceEIPMonthlyInstallmentTerms();
		// pdpPage.compareDownPaymentIsSameForPLPAndPDP(pdpPage.getDeviceDownPaymentInPDP(),
		// downPayment);
		// Need To Replace
	//	pdpPage.compareMonthlyPaymentIsSameForPLPAndPDP(pdpPage.getDeviceMonthlyPaymentInPDPNoCents(), monthlyPayment);
		pdpPage.compareLoanTermIsSameForPLPAndPDP(pdpPage.getDeviceLoanTermInPDP(), loanTerm);
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption("Full Retail Price");
		pdpPage.verifyFRPDollarsSign();
		pdpPage.verifyDeviceFRPPrice();
	}

	/**
	 * US354452 MyTMO - AT - Standard Upgrade Flow Device PDP: Pull out FRP from
	 * Legal Text and Display as Standalone Fields
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testFRPfromLegalTextandDisplayasStandaloneFields(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test US354452: MyTMO - AT - Standard Upgrade Flow Device PDP: Pull out FRP from Legal Text and Display as Standalone Fields ");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on Shop link | Shop page should be displayed");
		Reporter.log("5. Click on See all phones | Product list page should be displayed");
		Reporter.log("6. Select Device | Product details page should be displayed");
		Reporter.log(
				"7. verify Full Retail Price label for the device separated from the legal text| Full Retail Price label for the device should be separated from the legal text");
		Reporter.log(
				"8. verify legal text placed under  EIP pricing / loan term length data| legal text should be placed under  EIP pricing / loan term length data");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyLegalText();
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption(ShopConstants.PAYMENT_OPTION_FRP);
		pdpPage.verifyLegalTextNotDisplayed();
	}

	/**
	 * US123754 [Mock data] Product information : As a mytmo.com customer, I want to
	 * see information about the device I'm viewing, so that I can learn more and
	 * move down the purchase funnel. US140013 Legal text (Desktop) : As a mytmo.com
	 * customer, I want to see legal text on the PDP, so I am fully informed on the
	 * device. * US141661 Upgrade CTA - Edit (only applies to single PDP) :
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testLegalTextandProductInformationInPDPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : Verify product information PDP page");
		Reporter.log("Test Case : Verify legal text in PDP page");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on Deive | PDP page should be displayed");
		Reporter.log("6. Verify legal text| Legal text should be displayed");
		Reporter.log("7. Verify see more link| See more link should be displayed");
		Reporter.log("8. Click see more link| See less link should be displayed");
		Reporter.log("9. Click see less link| See more link should be displayed");
		Reporter.log("10.Verify continue button | Continue button should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyLegalText();
		pdpPage.verifySeeMoreLink();
		pdpPage.clickSeeMoreLink();
		pdpPage.verifySeeLessLink();
		pdpPage.clickSeeLessLink();
		pdpPage.verifyContinueButton();

	}

	/**
	 * US421121: Trade in Redesign > Account level error > Custom message
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testAccountLevelCustomErrorMessage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US421121: Trade in Redesign > Account level error > Custom message ");
		Reporter.log("Data Condition | STD PAH With Past-Due or Delinquent Customer ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Deive | PDP page should be displayed");
		Reporter.log("6. Verify Past-due sticky message | Past-due message should be displayed in PDP page");
		Reporter.log("7. Verify Add to cart button | Add to cart button should be disable mode");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyPastDueStickyMessageDisplayed();
		pdpPage.verifyAddtoCartButtonisDisabled();
	}

	/**
	 * US140897 PDP Due Today modal : As a mytmo.com user on the PDP, I want to be
	 * able to click on the "Due Today" amount and see an explanation of what this
	 * number includes, so that I better understand what I'm paying for. US123747
	 * [Mock data] Memory variant - As a mytmo.com customer, I want to select a
	 * memory variant of my device, so that I can purchase the one I want
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testMemoryVarientandDueTodayModelInPDPPage(ControlTestData data, MyTmoData myTmoData) {

		Reporter.log("Test Case : Verify due today model in PDP page");
		Reporter.log("Data Condition | PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on Deive | PDP page should be displayed");
		Reporter.log("6. Verify Shipping date | Shipping date should be displayed");
		Reporter.log("7. Verify memory header| Memory header should be displayed");
		Reporter.log("8. Click today button| Down payment model header should be displayed");
		Reporter.log("9. Verify down payment model text| Down payment model should be displayed");
		Reporter.log("10. Verify down payment model note| Down payment model note should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyEstimatedShipDate();
		pdpPage.verifyMemoryHeader();
		pdpPage.clickTodayButton();
		pdpPage.verifyDownPaymentModelHeader();
		pdpPage.verifyDownPaymentModelText();
		pdpPage.verifyDownPaymentModelNote();
	}

	/**
	 * US503824: [TECHNICAL] V3 Gaps - PDP UI gaps from MPM response(show missing
	 * data)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testSpecificationsInPdpPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US503824: [TECHNICAL] V3 Gaps - PDP UI gaps from MPM response(show missing data)");
		Reporter.log("Data Condition | IR STD PAH Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click Select Device | PDP Page is Displayed");
		Reporter.log(
				"6. Verify Specification section under add to cart button | Specifications section should be displayed");
		Reporter.log("7. Verify hide Device Tier | Device Tier should not be displayed");
		Reporter.log("8. Verify WEA Capable hyper link  | WEA Capable hyper link should be displayed");
		Reporter.log("9. Click WEA Capable hyper link  | WEA Capable new window should be opened");
		Reporter.log("9. Click WEA Capable hyper link  | WEA Capable new window should be opened");
		Reporter.log("10. Verify Camera value needs to be concatenated  | Camera value should be concatenated");
		Reporter.log(
				"11. Verify Battery Talk Time Starts with 'Up to' and standby time before value  | Battery Talk Time Starts with 'Up to' should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Results:");
		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifySpecificationsLinkOnPDP();
		pdpPage.verifyWEACapableSpecifcationValueOnPDP();
		pdpPage.verifyWEACapableHyperLinkValueOnPDP();
		pdpPage.clickWEACapableHyperLinkValueOnPDP();
		pdpPage.verifyCameraSpecificationOnPDP();
		pdpPage.verifyTierSpecifcationValueOnPDP();
		pdpPage.verifyBatterySpecifcationOnPDP();
		pdpPage.verifyBatterySpecifcationValueOnPDP();

	}

	/**
	 * P1_TC_Regression_Desktop_B2B Upgrade Error Message_ Non-Master Permission
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void verifyUpgradeErrorMessage_NonMasterPermission(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : B2B Customer non master permission - not be eligible for upgrade");
		Reporter.log("Data Condition | Non Master Customer");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click Select Device| PDP Page is Displayed");
		Reporter.log("6. Verify InEligible Message| InEligible Message should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Results:");

		PDPPage pdpPage = navigateToPDPPageByFeatureDevices(myTmoData, myTmoData.getDeviceName());
		pdpPage.verifyPDPPage();
		pdpPage.verifyIneligibilityTxt();
	}

	/**
	 * TA1655236 : PDP Page(DE165409) Header(Above past due sticky) missing in PDP
	 * page when the user returns to the PDP page by canceling OTP
	 * 
	 * TA1655269 : Offline : Past due sticky on PDP page is not appearing when the
	 * user gets back to PDP from the OTP (Not clearing the past due ) DE170480 :
	 * Offline : Past due sticky on PDP page is not appearing when the user gets
	 * back to PDP from the OTP (Not clearing the past due )
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID,
			Group.IOS })
	public void testPdpHeaderForPastDueCustomerAfterCancellingOTP(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case : TA1655236 : PDP Page");
		Reporter.log("Data Condition | IR PAH PAST DUE User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click See all phones link | PLP page should be displayed");
		Reporter.log("6. Select device | PDP page should be displayed");
		Reporter.log("7. Verify Past due banner | Past due banner should be displayed");
		Reporter.log("8. Click on Past due banner | Past due banner should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPastDueStickyMessageDisplayed();
		pdpPage.clickPastDueBanner();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageUrl();
		oneTimePaymentPage.clickCancelBtn();
		oneTimePaymentPage.clickOnYesLeaveCTA();
		pdpPage.verifyPDPPage();
		pdpPage.verifyPastDueStickyMessageDisplayed();
	}

	/**
	 * US377925 - AAL - UnKnown intent Blocked path - lead Eligible (max soc) plan
	 * customers to modal (PDP)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testAALEligibilityCheckForPAHCustomerWithTMOONEPlanMaxSOCLimit(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"Test Data: PAH Customer Without TMO ONE Plan & PAH Customer With TMO ONE Plan SOC limit maxed out");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Navigate PDP page | Product display page should be displayed");
		Reporter.log(
				"4. Click on 'Add a Line' button on PDP page | 'Let's talk...Sorry, we're not able add a line to your account online...' pop up should be displayed");
		Reporter.log(
				"5. Verify contents on 'Let's talk...' popup | User friendly message with Customer care details and 'Contact Us' button should be displayed on the	pop-up");
		Reporter.log(
				"6. Click 'Contact US' button on 'Let's talk...' popup | User should be navigated to Contact Us page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickSeeAllPhones();

		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickDeviceByName(myTmoData.getDeviceName());

		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.clickOnAddALineButton();
		pdpPage.verifyLetsTalkModel();
		pdpPage.verifyLetsTalkModelDescription();
		pdpPage.clickOnContactUsButton();

		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
	}

	/**
	 * US377671 - AAL - UnKnown intent Blocked path - lead Non Eligible plan
	 * customers to Modal (PDP)
	 * 
	 * US377925 - AAL - UnKnown intent Blocked path - lead Eligible (max soc) plan
	 * customers to modal (PDP)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testAALEligibilityCheckForPAHCustomerWithoutTMOONEPlan(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Data: PAH Customer Without TMO ONE Plan & PAH Customer With TMO ONE Plan SOC limit maxed out");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Navigate PDP page | Product display page should be displayed");
		Reporter.log(
				"4. Click on 'Add a Line' button on PDP page | 'Let's talk...Sorry, we're not able add a line to your account online...' pop up should be displayed");
		Reporter.log(
				"5. Verify contents on 'Let's talk...' popup | User friendly message with Customer care details and 'Contact Us' button should be displayed on the	pop-up");
		Reporter.log(
				"6. Click 'Contact US' button on 'Let's talk...' popup | User should be navigated to Contact Us page");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickSeeAllPhones();

		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickDeviceByName(myTmoData.getDeviceName());

		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.clickOnAddALineButton();
		pdpPage.verifyLetsTalkModel();
		pdpPage.verifyLetsTalkModelDescription();
		pdpPage.clickOnContactUsButton();

		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
	}

	/**
	 * US377671 - AAL - UnKnown intent Blocked path - lead Non Eligible plan
	 * customers to Modal (PDP)
	 * 
	 * US377925 - AAL - UnKnown intent Blocked path - lead Eligible (max soc) plan
	 * customers to modal (PDP)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testAALFlowForTMOOneCustomerSOCNotMaxedOut(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Data: PAH Customer Without TMO ONE Plan & PAH Customer With TMO ONE Plan SOC limit maxed out");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Navigate PDP page | Product display page should be displayed");
		Reporter.log("4. Click on 'Add a Line' button on PDP page | Rate plan page should be disaplyed");
		Reporter.log("5. Click on continue | Device Protection page should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.clickOnAddALineButton();

		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.verifyConsolidatedRatePlanPage();
		consolidatedRatePlanPage.clickOnContinueCTA();

		DeviceProtectionPage deviceProtectionPage = new DeviceProtectionPage(getDriver());
		deviceProtectionPage.verifyDeviceProtectionPage();
	}

	/**
	 * US349036: AAL: PDP Page CTA display scenarios (unknown intent) - both CTAs
	 * hidden
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testPDPPageAALCTADisabledForNonPAHUser(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US349036: AAL: PDP Page CTA display scenarios (unknown intent) - both CTAs hidden");
		Reporter.log("Data Condition | STD PAH not among PAH,Full user or single line Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on Deive | PDP page should be displayed");
		Reporter.log("6. Verify UPGRADE CTA | UPGRADE button  should be HIDDEN");
		Reporter.log("7. Verify Add a line CTA | Add a line button should be HIDDEN");
		Reporter.log(
				"8. Verify ineligibility message | Please check with your primary account holder to add a line message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyupgradeCTAHidden();
		pdpPage.verifyAddaLineCTAHidden();
		pdpPage.verifyInEligibleMessage("Contact your Primary Account Holder to upgrade.");
	}

	/**
	 * US349036: AAL: PDP Page CTA display scenarios (unknown intent) - both CTAs
	 * hidden
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testPDPPageAALCTADisabledForSuspendedCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US349036: AAL: PDP Page CTA display scenarios (unknown intent) - both CTAs hidden");
		Reporter.log("Data Condition | STD PAH suspended Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on Deive | PDP page should be displayed");
		Reporter.log("6. Verify UPGRADE CTA | UPGRADE button  should be HIDDEN");
		Reporter.log("7. Verify Add a line CTA | Add a line button should be HIDDEN");
		Reporter.log(
				"8. Verify ineligibility message | Your account has been temporarily suspended. Please call 1-800-937-8997. message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyupgradeCTAHidden();
		pdpPage.verifyAddaLineCTAHidden();
		pdpPage.verifyInEligibleMessage("Your account has been temporarily suspended. Please call 1-800-937-8997.");
	}

	/**
	 * US257098 :Display promotions, including badge, pricing and promo text, on PDP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROMO_REGRESSION })
	public void testPDPDeviceBadges(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US257098 :Display promotions, including badge, pricing and promo text, on PDP");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Verify promo badge | Promo badge should be displayed");
		Reporter.log("6. Verify device header | Device header should be displayed");
		Reporter.log("7. Verify promo link text | Promo link text should be displayed");
		Reporter.log("8.Click promo link text | PDP promo window should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage newShopPage = navigateToShopPage(myTmoData);
		newShopPage.clickOnSeeAllPhonesBottomBtn();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickFilterDropdown();
		plpPage.selectOnSaleFilterValue();
		plpPage.clickOnCloseFilter();
		plpPage.clickSelectedPromoDevice();
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.clickOnPaymentOption();
		pdpPage.selectPaymentOption("Full Retail Price");

		pdpPage.verifyPromoBadge();
		pdpPage.verifyDeviceFRPPrice();
		pdpPage.verifypromoLinkText();
		pdpPage.clickPromoLinkText();
		pdpPage.verifyPDPPromoWindow();

	}

	/**
	 * US335144:AAL: PDP Page CTA display scenarios (unknown intent) - only AAL
	 * disabled
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testPDPPageAALCTADisabledFortotalOfEligibleVoiceLinesAndEligibleMBBLinesIsZero(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test US335144: AAL: PDP Page CTA display scenarios (unknown intent) - only AAL disabled");
		Reporter.log("Data Condition | STD PAH total of eligible Voice Lines and eligible MBB Lines is zero Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on Device | PDP page should be displayed");
		Reporter.log("6. Verify UPGRADE CTA | UPGRADE button  should be enable");
		Reporter.log("7. Verify Add a line CTA | Add a line button should GRAYED OUT");
		Reporter.log(
				"8. Verify ineligibility message | You’ve reached the maximum number of lines you can add to this account message should display");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyUpGradeCTAEnabled();
		pdpPage.verifyAddaLineCTADisabled();
		pdpPage.verifyInEligibleMessage("You’ve reached the maximum number of lines you can add to this account");

	}

	/**
	 * US349036:AAL: PDP Page CTA display scenarios (unknown intent) - both CTAs
	 * hidden
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testPDPPageAALCTADisabledForPuertoRicoCustomer(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US349036: AAL: PDP Page CTA display scenarios (unknown intent) - both CTAs hidden");
		Reporter.log("Data Condition | STD PAH PuertoRico Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Click on Deive | PDP page should be displayed");
		Reporter.log("6. Verify UPGRADE CTA | UPGRADE button  should be HIDDEN");
		Reporter.log("7. Verify Add a line CTA | Add a line button should be HIDDEN");
		Reporter.log(
				"8. Verify ineligibility message | Please visit a retail location to add a line for a Puerto Rico account. message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyupgradeCTAHidden();
		pdpPage.verifyAddaLineCTAHidden();
		pdpPage.verifyInEligibleMessage("Contact Customer Care.");
	}

	/**
	 * US388894 AAL - UnKnown intent Blocked path - lead max voice/non max MBB to
	 * Modal (PDP)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testAALDigitalFlowStoppedforCustomerwithMaxVoiceLinesInPDPPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log("Test US388894: AAL - UnKnown intent Blocked path - lead max voice/non max MBB to Modal (PDP)");
		Reporter.log(
				"Data Condition |  PAH or FULL customer who has the max voice lines on their BAN, but not max MBB ");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select on Deive | PDP page should be displayed");
		Reporter.log("6. click on 'AAL CTA' | Customer Modal should be displayed ");
		Reporter.log("7. Verify 'Let's talk' title on the modal| 'Let's talk' title should be displayed");
		Reporter.log(
				"8. Verify 'Sorry - we're not able to add a line to your account online.' text on the modal| 'Sorry - we're not able to add a line to your account online.' text should be displayed");
		Reporter.log(
				"9. Verify 'To add a line, please call Customer Care at 1-800-937-8997, or by dialing 611 from your T-Mobile phone.' text on the modal|  'To add a line, please call Customer Care at 1-800-937-8997, or by dialing 611 from your T-Mobile phone.' text should be displayed");
		Reporter.log("10. click on 'CONTACT US' CTA | Contact Us page should be displayed ");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.clickOnAddALineButton();
		pdpPage.verifyLetsTalkModel();
		pdpPage.verifyLetsTalkModelDescription();
		pdpPage.clickonContactUsCTA();

		ContactUSPage contactUSPage = new ContactUSPage(getDriver());
		contactUSPage.verifyContactUSPage();
	}

	/**
	 * US393839: AAL - Consolidated Charges Page: Remove SIM elements from PDP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SHOP, Group.DESKTOP, Group.ANDROID, Group.IOS,
			Group.AAL_REGRESSION })
	public void testRemoveTheSIMCardTextInPDPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test US393839: AAL - Consolidated Charges  Page: Remove SIM elements from PDP");
		Reporter.log("Data Condition | STD PAH account Customer");
		Reporter.log("================================");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | Home page should be displayed");
		Reporter.log("3. Click on shop | shop page should be displayed");
		Reporter.log("4. Click on see all phones | PLP page should be displayed");
		Reporter.log("5. Select a device | PDP page should be displayed");
		Reporter.log(
				"6. Verify remove the sim card text message under cta | Remove the sim card text message should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		PDPPage pdpPage = navigateToPDPPageBySeeAllPhones(myTmoData);
		pdpPage.verifyPDPPage();
		pdpPage.verifyRemoveSimCardTextMessage();
	}

	/**
	 * US340319:MyTMO - Additional Terms - AAL - Device PDP Pricing Display (AAL
	 * intent)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testDevicePricesInPLPPageAALIntent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case: US340319:MyTMO - Additional Terms - AAL - Device PDP Pricing Display (AAL intent)");
		Reporter.log("Data Condition | STD PAH Customer with AAL eligibility");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on Shop tab on home page | Application Navigated to Shop Page ");
		Reporter.log("3. Click on Add a line button | Device-intent page should be displayed");
		Reporter.log("4. Click on 'Buy a new phone' option | PLP page Should be Displayed ");
		Reporter.log("5. Select device in PLP page | PDP Page should be displayed");
		Reporter.log("6. Verify Device, Today price and Save price in V1  | Today price should be displayed ");
		Reporter.log("7. Verify Device, Monthly price and Save price in V2  | Monthly price should be displayed ");
		Reporter.log(
				"8. Verify Device, Installment months and Save Months in V3  | Installment months should be displayed ");
		Reporter.log(
				"9. Verify Device, Full retail price and Save price in V4  | Full retail price should be displayed ");
		Reporter.log("10. Compare Price(V4 = (V1 + V2 * V3))  | Device price should be equal");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickQuickLinks("ADD A lINE");

		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.verifyDeviceIntentPage();
		deviceIntentPage.verifyBuyANewPhoneTileIsDisplayed();
		deviceIntentPage.clickBuyANewPhoneOption();

		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickDeviceByName(myTmoData.getDeviceName());

		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.verifyDeviceDownPaymentPrice();
		Double V1 = Double.parseDouble(pdpPage.getDeviceDownPaymentInPDP());
		pdpPage.verifyEIPPrice();
		Double V2 = Double.parseDouble(pdpPage.getDeviceMonthlyPaymentInPDP());
		pdpPage.verifyDeviceEIPMonthlyInstallmentTerms();
		Double V3 = Double.parseDouble(pdpPage.getDeviceLoanTermInPDP());
		pdpPage.verifyDeviceFRPPrice();
		Double V4 = pdpPage.getNewFRPPDPPage();
		Double calcFRP = (V1 + (V2 * V3));
		pdpPage.compareDevicePrice(calcFRP, V4);
	}

	/**
	 * US338637: MyTMO - Additional Terms - AAL - Device PDP Pricing Display
	 * (UNKNOWN and UPGRADE intent)
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testDevicePricesInPDPPageUnknownOrUpgradeIntent(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"Test Case: US338637: MyTMO - Additional Terms - AAL - Device PDP Pricing  Display (UNKNOWN and UPGRADE intent)");
		Reporter.log("Data Condition | STD PAH Customer");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Click on Shop tab on home page | Application Navigated to Shop Page ");
		Reporter.log("4. Click on See All Phones | PLP page Should be Displayed ");
		Reporter.log("5. Select device in PLP page | PDP Page should be displayed");
		Reporter.log("6. Verify Device, Today price and Save price in V1  | Today price should be displayed ");
		Reporter.log("7. Verify Device, Monthly price and Save price in V2  | Monthly price should be displayed ");
		Reporter.log(
				"8. Verify Device, Installment months and Save Months in V3  | Installment months should be displayed ");
		Reporter.log(
				"9. Verify Device, Full retail price and Save price in V4  | Full retail price should be displayed ");
		Reporter.log("10. Compare Price(V4 = (V1 + V2 * V3))  | Device price should be equal");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickSeeAllPhones();

		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickDeviceByName(myTmoData.getDeviceName());

		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.verifyDeviceDownPaymentPrice();
		Double V1 = Double.parseDouble(pdpPage.getDeviceDownPaymentInPDP());
		pdpPage.verifyEIPPrice();
		Double V2 = Double.parseDouble(pdpPage.getDeviceMonthlyPaymentInPDP());
		pdpPage.verifyDeviceEIPMonthlyInstallmentTerms();
		Double V3 = Double.parseDouble(pdpPage.getDeviceLoanTermInPDP());
		pdpPage.verifyDeviceFRPPrice();
		Double V4 = pdpPage.getNewFRPPDPPage();
		Double calcFRP = (V1 + (V2 * V3));
		pdpPage.compareDevicePrice(calcFRP, V4);
	}

	/**
	 * US474171: Standalone accessories: Enable "Helpful" and "Unhelpful" for
	 * Reviews on PDP
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testEnableHelpfulAndUnhelpfulOptionsOnReviewsOnPDPPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :US474171: Standalone accessories: Enable Helpful and Unhelpful for Reviews on PDP");
		Reporter.log("Data Condition | STD PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click See all phones link | PLP page should be displayed");
		Reporter.log("6. Select device | PDP page should be displayed");
		Reporter.log("7. Verify device Reviews count | Device Reviews count should be displayed");
		Reporter.log("8. Verify Review Tab in below device | In below device review tab should be displayed");
		Reporter.log("9. Click Review Tab in below device | Filter options should be displayed");
		Reporter.log(
				"10. Verify 'HELPFUL' link for each review count | 'HELPFUL' link for each review should be displayed");
		Reporter.log(
				"11. Get any 'HELPFUL' link review count and Store V1  | 'HELPFUL' link review count should be Stored in V1");
		Reporter.log(
				"12. Click 'HELPFUL' review link and Get link count store in V2  | 'HELPFUL' link review count should be Stored in V2 ");
		Reporter.log(
				"13. Compare 'HELPFUL' review link count, saved in V1 and V2 | 'HELPFUL' review link count values(V1 and V2) should not matched");
		Reporter.log(
				"14. Verify 'UNHELPFUL' link for each review count | 'UNHELPFUL' link for each review should be displayed");
		Reporter.log(
				"15. Get any 'UNHELPFUL' link review count and Store V3  | 'UNHELPFUL' link review count should be Stored in V3");
		Reporter.log(
				"16. Click 'UNHELPFUL' review link and Get link count store in V4  | 'UNHELPFUL' link review count should be Stored in V4 ");
		Reporter.log(
				"17. Compare 'UNHELPFUL' review link count, saved in V3 and V4 | 'UNHELPFUL' review link count values(V3 and V4) should not matched");

		Reporter.log("================================");
		Reporter.log("Actual Output:");
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickSeeAllPhones();

		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageLoaded();
		plpPage.clickDeviceByName(myTmoData.getDeviceName());

		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.verifyReviewTab();
		pdpPage.clickReviewTab();
		pdpPage.verifyPositiveReviewCountForPDP();
		pdpPage.verifyNegativeReviewCountForPDP();
		String v1 = pdpPage.getPositiveReviewCountForPDP();
		pdpPage.clickPositiveReviewCountForPDP();
		String v2 = pdpPage.getPositiveReviewCountForPDP();
		pdpPage.compareReviewCount(v1, v2);
		String v3 = pdpPage.getNegativeReviewCountForPDP();
		pdpPage.clickNegativeReviewCountForPDP();
		String v4 = pdpPage.getNegativeReviewCountForPDP();
		pdpPage.compareReviewCount(v3, v4);

	}

	/**
	 * US524728: V3 Gaps - MPM Response: Specifications : set SKU level details
	 * first and then Product (EOS) TA1651841 : PDP Page
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testFeaturesAndSpecificationsInPdpPage(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log("Test Case :TA1651841 : PDP Page");
		Reporter.log(
				"Test Case :US524728: V3 Gaps - MPM Response: Specifications : set SKU level details first and then Product (EOS)");
		Reporter.log("Data Condition | IR PAH User");
		Reporter.log("Test Steps | Expected Results:");
		Reporter.log("================================");
		Reporter.log("1. Launch the application | Application Should be Launched");
		Reporter.log("2. Login to the application | User Should be login successfully");
		Reporter.log("3. Verify Home page | Home page should be displayed");
		Reporter.log("4. Click on shop link | Shop page should be displayed");
		Reporter.log("5. Click See all phones link | PLP page should be displayed");
		Reporter.log("6. Select device | PDP page should be displayed");
		Reporter.log("7. Verify Fetures section | Fetaures available for the product should be displayed");
		Reporter.log("8. Verify Specifications section | Specifications should be displayed");
		Reporter.log(
				"9. Verify each specification provided under product and SKU details | Respective specifications should be displayed");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyFeaturesTab();
		pdpPage.verifyFeaturesSection();
		pdpPage.verifySpecificationSection();
		pdpPage.verifyDisplaySpecificationAndValue();
		pdpPage.verifyDisplayResolutionSpecificationAndValue();
		pdpPage.verifyWeightSpecificationAndValue();
		pdpPage.verifyLengthHeightWidthSpecificationAndValue();
		pdpPage.verifyBatteryDescriptionSpecificationAndValue();
		pdpPage.verifyBatteryTalkTimeSpecificationAndValue();
		pdpPage.verifyPortsSpecificationAndValue();
		pdpPage.verifyConnectivitySpecificationAndValue();
		pdpPage.verifyProcessorSpecificationAndValue();
		pdpPage.verifyOSSpecificationAndValue();
		pdpPage.verifyRamSpecificationAndValue();
		pdpPage.verifyMemorySpecificationAndValue();
		pdpPage.verifyWirelessNetworkTechSpecificationAndValue();
		pdpPage.verifySupportedEmailSpecificationAndValue();
		pdpPage.verifyWEACapableSpecificationAndValue();
		pdpPage.verifyHearingAidSpecificationAndValue();
		pdpPage.verifyMobileHotspotSpecificationAndValue();
		pdpPage.verifyCamersSpecificationAndValue();
		pdpPage.verifyFrequencySpecificationAndValue();
	}

	/**
	 * US521900 : myTMO release - As an authenticated user that has specified my
	 * intent to ADD A LINE, I'd like the only CTA displayed to be ADD TO CART, so I
	 * am not confused by other options.
	 *
	 * US535101 : myTMO Release - As an authenticated user that  is in the CONSOLIDATED
	 * CHARGES page and has specified my intent to ADD A LINE, I would like to be taken
	 * to the PHONES PLP upon selecting CONTINUNE so that I may continue shopping.
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION  })
	public void testPresenceOfAddToCartCtaOnPDPPageForAAlIntentFlow(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"US521900 : myTMO release - As an authenticated user that has specified my intent to ADD A LINE, I'd like the only CTA displayed to be ADD TO CART, "
						+ "so I am not confused by other options.");
		Reporter.log("Data Condition | IR PAH User");
		Reporter.log("1. Login to the application | Home page should be displayed");
		Reporter.log("2. Click on Shop tab on home page | User should be navigated to Shop Page ");
		Reporter.log("3. Click on the 'Add a line' from quick links | Device intent page should be displayed");
		Reporter.log("4. Select Buy new Phone option | Rate plan page should be displayed");
		Reporter.log("5. Click 'Continue' cta | UNO PLP should be displayed and also Phones category should be selected");
		Reporter.log("6. Select a Device from UNO PLP | PDP Page should be displayed");
		Reporter.log("7. Verify Add to Cart is displayed | 'Add to Cart' cta should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToRatePlanPageFromAALBuyNewPhoneFlow(myTmoData);
		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.clickOnContinueCTA();
		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.verifyPageUrl();
		plpPage.verifySelectedCategory("Phones");
		plpPage.clickDeviceByName(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyAddtoCartButtonIsEnabled();
	}

	/**
	 * US521900 : myTMO release - As an authenticated user that has specified my
	 * intent to ADD A LINE, I'd like the only CTA displayed to be ADD TO CART, so I
	 * am not confused by other options.
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPresenceOfAddToCartCtaOnPDPPageForAAlIntentFlowFromAccountsPage(ControlTestData data,
			MyTmoData myTmoData) {
		Reporter.log(
				"US521900 : myTMO release - As an authenticated user that has specified my intent to ADD A LINE, I'd like the only CTA displayed to be ADD TO CART, "
						+ "so I am not confused by other options.");
		Reporter.log("Data Condition | IR PAH User");
		Reporter.log("1. Login to the application | Home page should be displayed");
		Reporter.log("2. Click on Accounts tab on home page | User should be navigated to Account-Overview Page ");
		Reporter.log(
				"3. Click on the 'Add a person or device to my account' from Account-Overview Page | Device intent page should be displayed");
		Reporter.log("4. Select Buy new Phone option | Rate plan page should be displayed");
		Reporter.log("5. Click 'Continue' cta | PLP should be displayed");
		Reporter.log("6. Select a Device from PLP | PDP Page should be displayed");
		Reporter.log("5. Verify Add to Cart is displayed | 'Add to Cart' cta should be displayed");

		Reporter.log("================================");
		Reporter.log("Actual Output:");

		AccountOverviewPage accountOverviewPage = new AccountOverviewPage(getDriver());
		accountOverviewPage.clickOnAddaLine();

		DeviceIntentPage deviceIntentPage = new DeviceIntentPage(getDriver());
		deviceIntentPage.clickBuyANewPhoneOption();

		ConsolidatedRatePlanPage consolidatedRatePlanPage = new ConsolidatedRatePlanPage(getDriver());
		consolidatedRatePlanPage.clickOnContinueCTA();

		PLPPage plpPage = new PLPPage(getDriver());
		plpPage.clickDeviceByName(myTmoData.getDeviceName());

		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyAddtoCartButtonIsEnabled();
	}

	/**
	 * US540508 : myTMO release - As a cookied user, I would like the UPGRADE and
	 * ADD A LINE CTAs to be disabled if my account is identified as not eligible,
	 * so that I can't buy products that I am not eligible for. 
	 * US540509: myTMO
	 * release - As a Product manager, I want to stop cookied/authenticated
	 * customers from adding a line on PDP if they're not eligible, so that
	 * customer's are aware why they cannot add a line on their account.
	 * 
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PROGRESSION })
	public void testUpgradeAALCtaStatusOnPDPPageForIneligibleUsers(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"US540508 : myTMO release - As a cookied user, I would like the UPGRADE and ADD A LINE CTAs to be disabled if my account is identified "
						+ "as not eligible, so that I can't buy products that I am not eligible for.");
		Reporter.log("Data Condition | Ineligible User");
		Reporter.log("1. Login to the application | Home page should be displayed");
		Reporter.log("2. Click on Shop tab on home page | User should be navigated to Shop Page ");
		Reporter.log("3. Select a Device from PLP | PDP Page should be displayed");
		Reporter.log("4. Verify Upgrade Button is displayed | 'Upgrade Button' cta should be disabled");
		Reporter.log("5. Verify Add a Line is displayed | 'Add a Line' cta should be disabled");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		navigateToPDPPageBySeeAllPhones(myTmoData);
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyUpgradeButtonIsDisabled();
		pdpPage.verifyAddaLineCTADisabled();
		pdpPage.verifyInEligibleMessage("Please make a payment to become eligible to upgrade or add a line.");
	}

	/**
	 * US572786 - MyTMO Release - As a Product Manager, I want to redirect uncookied/unauthenticated customers back to the PDP page, so
	 * that they can view contextual information based on their CRP.
	 *
	 * @param data
	 * @param myTmoData
	 */
	@Test(dataProvider = "byColumnName", enabled = true, groups = { Group.SPRINT })
	public void testPDPPageForUncookiedUserFromUNO(ControlTestData data, MyTmoData myTmoData) {
		Reporter.log(
				"US572786 - MyTMO Release - As a Product Manager, I want to redirect uncookied/unauthenticated customers back to the PDP page, so\n" +
						"\t * that they can view contextual information based on their CRP.");
		Reporter.log("Data Condition | Uncookied user");
		Reporter.log("1. Launch TMNG url | TMO home page should be displayed");
		Reporter.log("2. Click on Phone tab on home page | UNO PLP should be displayed");
		Reporter.log("3. Select a Device from PLP | PDP Page should be displayed");
		Reporter.log("4. Click on 'Add to cart' cta | Login popup should be displayed");
		Reporter.log("5. Continue as Exisitng customer(Select Log in cta) | User should be redirected to MYTmo login page");
		Reporter.log("5. Provide login credentials and select login | User should be navigated to PDP");
		Reporter.log("================================");
		Reporter.log("Actual Output:");

		LaunchTMNGAndNavigateToMyTmoLogin(myTmoData);
		LoginPage loginPage = new LoginPage(getDriver());
		loginPage.performLoginAction(myTmoData.getLoginEmailOrPhone(), myTmoData.getLoginPassword());

		PDPPage pdpPage= new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		pdpPage.verifyAddtoCartButtonIsEnabled();

	}
}

/*
 * Removed retired US - US123747
 */