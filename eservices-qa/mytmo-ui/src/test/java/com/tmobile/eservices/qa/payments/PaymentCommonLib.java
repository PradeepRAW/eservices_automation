/**
 * 
 */
package com.tmobile.eservices.qa.payments;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eqm.testfrwk.ui.core.exception.FrameworkException;
import com.tmobile.eservices.qa.api.EOSCommonLib;
import com.tmobile.eservices.qa.commonlib.CommonLibrary;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.Payment;
import com.tmobile.eservices.qa.pages.CommonPage;
import com.tmobile.eservices.qa.pages.HomePage;
import com.tmobile.eservices.qa.pages.NewHomePage;
import com.tmobile.eservices.qa.pages.accounts.ProfilePage;
import com.tmobile.eservices.qa.pages.payments.AccountHistoryPage;
import com.tmobile.eservices.qa.pages.payments.AddBankPage;
import com.tmobile.eservices.qa.pages.payments.AddCardPage;
import com.tmobile.eservices.qa.pages.payments.AutoPayCancelConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.AutoPayConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.AutoPayPage;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.BillDetailsPage;
import com.tmobile.eservices.qa.pages.payments.BillingPaymentsPage;
import com.tmobile.eservices.qa.pages.payments.BillingSummaryPage;
import com.tmobile.eservices.qa.pages.payments.CallDetailRecordsForwardPage;
import com.tmobile.eservices.qa.pages.payments.ChargeEquipmentPage;
import com.tmobile.eservices.qa.pages.payments.ChargeFeaturesPage;
import com.tmobile.eservices.qa.pages.payments.ChargeOneTimePage;
import com.tmobile.eservices.qa.pages.payments.ChargePlanPage;
import com.tmobile.eservices.qa.pages.payments.DocuSignPage;
import com.tmobile.eservices.qa.pages.payments.EIPDetailsPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentConfirmationCCPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentForwardPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentPage;
import com.tmobile.eservices.qa.pages.payments.EIPPaymentReviewCCPage;
import com.tmobile.eservices.qa.pages.payments.EipJodPage;
import com.tmobile.eservices.qa.pages.payments.FAQPage;
import com.tmobile.eservices.qa.pages.payments.HistoricBillsPage;
import com.tmobile.eservices.qa.pages.payments.JODEndingSoonOptionsPage;
import com.tmobile.eservices.qa.pages.payments.LeaseDetailsPage;
import com.tmobile.eservices.qa.pages.payments.MaintenancePage;
import com.tmobile.eservices.qa.pages.payments.NewAddBankPage;
import com.tmobile.eservices.qa.pages.payments.NewAddCardPage;
import com.tmobile.eservices.qa.pages.payments.NewAutoPayConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayCancelConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayFAQPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayLandingPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayPage;
import com.tmobile.eservices.qa.pages.payments.NewAutopayTncPage;
import com.tmobile.eservices.qa.pages.payments.NewPaymentArrangementPage;
import com.tmobile.eservices.qa.pages.payments.OTPAmountPage;
import com.tmobile.eservices.qa.pages.payments.OTPConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.OTPDatePage;
import com.tmobile.eservices.qa.pages.payments.OTPTncPage;
import com.tmobile.eservices.qa.pages.payments.OneTimePaymentPage;
import com.tmobile.eservices.qa.pages.payments.PAConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.PAFAQPage;
import com.tmobile.eservices.qa.pages.payments.PAInstallmentDetailsPage;
import com.tmobile.eservices.qa.pages.payments.PATncPage;
import com.tmobile.eservices.qa.pages.payments.PaperlessBillingPage;
import com.tmobile.eservices.qa.pages.payments.PaymentArrangementPage;
import com.tmobile.eservices.qa.pages.payments.PaymentErrorPage;
import com.tmobile.eservices.qa.pages.payments.PaymentCollectionPage;
import com.tmobile.eservices.qa.pages.payments.PoipConfirmationPage;
import com.tmobile.eservices.qa.pages.payments.PreviousChargePage;
import com.tmobile.eservices.qa.pages.payments.PurchaseOptionInstallmentPlanPage;
import com.tmobile.eservices.qa.pages.payments.ShopHomePage;
import com.tmobile.eservices.qa.pages.payments.ShopLineSectorDetailsPage;
import com.tmobile.eservices.qa.pages.payments.SpokePage;
import com.tmobile.eservices.qa.pages.payments.TnCPage;
import com.tmobile.eservices.qa.pages.payments.UpdateBankPage;
import com.tmobile.eservices.qa.pages.payments.UpdateCardPage;
import com.tmobile.eservices.qa.pages.payments.UsageDetailsPage;
import com.tmobile.eservices.qa.pages.payments.UsageOverviewPage;
import com.tmobile.eservices.qa.pages.payments.UsagePage;
import com.tmobile.eservices.qa.pages.payments.WalletPage;
import com.tmobile.eservices.qa.pages.payments.api.EOSAutopayV3;
import com.tmobile.eservices.qa.pages.payments.api.EOSBillingFilters;
import com.tmobile.eservices.qa.pages.shop.LineSelectorPage;
import com.tmobile.eservices.qa.pages.shop.PDPPage;
import com.tmobile.eservices.qa.pages.shop.ShopPage;

import io.restassured.response.Response;

/**
 * @author charshavardhana
 *
 */
public class PaymentCommonLib extends CommonLibrary {
	private static final String PLATFORM_TYPE = "platform-type";
	private static String amount = null;

	/**
	 * Navigate To Billing Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public CommonLibrary navigateToBillingPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickBillingLink();
		return this;
	}

	/**
	 * Navigate to Activity and alerts page through Ebill
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected AccountHistoryPage navigateToActivityAndAlertPageThroughEbill(MyTmoData myTmoData) {
		navigateToEIPDetailsPage(myTmoData);
		EIPDetailsPage eIPDetailsPage = new EIPDetailsPage(getDriver());
		eIPDetailsPage.clickAccountActivities();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		return accountHistoryPage;
	}

	/**
	 * Navigate to Activity and alerts page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected AccountHistoryPage navigateToActivityAndAlertsPage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickOnAccountHistoryLink();
		AccountHistoryPage accountHistoryPage = new AccountHistoryPage(getDriver());
		accountHistoryPage.verifyPageLoaded();
		return accountHistoryPage;
	}

	/**
	 * Navigate To Add BAnk page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected AddBankPage navigateToAddBankPage(MyTmoData myTmoData) {
		SpokePage otpSpokePage = navigateToSpokePage(myTmoData);
		otpSpokePage.clickAddBank();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		addBankPage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);
		return addBankPage;
	}

	/**
	 * Navigate To Add Card page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected AddCardPage navigateToAddCardPage(MyTmoData myTmoData) {
		navigateToSpokePage(myTmoData);
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		return addCardPage;
	}

	/**
	 * Navigate to Autopay Cancel Confirmation page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected AutoPayCancelConfirmationPage navigateToAutoPayCancelConfirmationPage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		/*
		 * if (!homePage.verifyAutoPay("ON")) { setupAutopay(myTmoData); }
		 */

		if (!homePage.verifyAutopayisscheduled()) {
			setupAutopay(myTmoData);
		}
		cancelonlyAutoPay(homePage, myTmoData);
		AutoPayCancelConfirmationPage autoPayCancelConfirmationPage = new AutoPayCancelConfirmationPage(getDriver());
		autoPayCancelConfirmationPage.verifyPageLoaded();
		return autoPayCancelConfirmationPage;
	}

	/**
	 * Navigate to Autopay Confirmation page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected AutoPayConfirmationPage navigateToAutoPayConfirmationPage(MyTmoData myTmoData) {
		navigateToAutoPayPage(myTmoData);
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyStoredPaymentMethodOrAddBank(myTmoData);
		autoPayPage.clickAgreeAndSubmitBtn();
		AutoPayConfirmationPage autoPayConfirmationPage = new AutoPayConfirmationPage(getDriver());
		autoPayConfirmationPage.verifyPageLoaded();
		return autoPayConfirmationPage;
	}

	/**
	 * Login To MyTmo, Cancel Autopay and Set it Up again
	 * 
	 * @param myTmoData
	 */
	protected AutoPayPage navigateToAutoPayPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		// cancelAutoPay(homePage);
		// homePage.clickEasyPay();
		homePage.clickBillingLink();
		if (getDriver().getCurrentUrl().contains("/billandpay")) {
			BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
			billAndPaySummaryPage.verifyPageLoaded();
			billAndPaySummaryPage.clickAutopayShortcut();
		} else {
			BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
			billingSummaryPage.verifyPageLoaded();
			billingSummaryPage.clickAutopayShortcut();
		}
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyPageLoaded();
		return autoPayPage;
	}

	/**
	 * Login To MyTmo, Cancel Autopay and Set it Up again
	 * 
	 * @param myTmoData
	 */
	protected AutoPayPage navigateToAutoPayPagefromHomePage(MyTmoData myTmoData) {
		HomePage homePage = new HomePage(getDriver());

		homePage.clickBillingLink();
		if (getDriver().getCurrentUrl().contains("/billandpay")) {
			BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
			billAndPaySummaryPage.verifyPageLoaded();
			billAndPaySummaryPage.clickAutopayShortcut();

		} else {

			BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
			billingSummaryPage.verifyPageLoaded();
			billingSummaryPage.clickAutopayShortcut();

		}
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyPageLoaded();
		return autoPayPage;
	}

	/**
	 * Navigate To Billing Summary Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected BillingSummaryPage navigateToBillingSummaryPage(MyTmoData myTmoData) {
		navigateToBillingPage(myTmoData);
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.waitforSpinner();
		waitForPageLoad(getDriver());
		commonPage.verifyAjaxSpinnerInvisible();
		BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
		billingSummaryPage.verifyPageLoaded();
		billingSummaryPage.changeBillCycle();
		return billingSummaryPage;
	}

	/**
	 * Navigate to Bill And Pay (brite bill) Summary Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected BillAndPaySummaryPage navigateToBillAndPaySummaryPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickBillingLink();
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.waitforSpinner();
		BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
		billAndPaySummaryPage.verifyPageLoaded();
		return billAndPaySummaryPage;
	}

	/**
	 * Navigate to Bill details Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected BillDetailsPage navigateToBillDetailsPage(MyTmoData myTmoData) {
		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.waitForSpinners();
		billingSummaryPage.clickViewBillDetails();
		BillDetailsPage billDetailsPage = new BillDetailsPage(getDriver());
		billDetailsPage.verifyPageLoaded();
		return billDetailsPage;
	}

	/**
	 * Navigate To Paperless Billing Summary Page
	 *
	 * @param myTmoData
	 * @return
	 * @throws Exception 
	 */
	protected PaperlessBillingPage navigateToPaperlessBillingPage(MyTmoData myTmoData) throws Exception {
		BillingSummaryPage billingSummaryPage = navigateToEbillpagethroughBBpage(myTmoData);;
		billingSummaryPage.clickPaperLessBilling();
		PaperlessBillingPage paperlessBillingPage = new PaperlessBillingPage(getDriver());
		paperlessBillingPage.verifyPageLoaded();
		return paperlessBillingPage;
	}

	/**
	 * Navigate to Call Detail Records Forward Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected CallDetailRecordsForwardPage navigateToCallDetailRecordsForwardPage(MyTmoData myTmoData) {
		navigateToUsageOverviewPage(myTmoData);
		UsageOverviewPage usageOverviewPage = new UsageOverviewPage(getDriver());
		usageOverviewPage.clickViewAllUsageDetails();
		CallDetailRecordsForwardPage callDetailRecordsForwardPage = new CallDetailRecordsForwardPage(getDriver());
		callDetailRecordsForwardPage.verifyPageLoaded();
		return callDetailRecordsForwardPage;
	}

	/**
	 * Navigate to Charge Equipment Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected ChargeEquipmentPage navigateToChargeEquipmentPage(MyTmoData myTmoData, String categoryOrLine) {

		BillAndPaySummaryPage billingSummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billingSummaryPage.clickChargeCategory(categoryOrLine);
		ChargeEquipmentPage chargeEquipmentpage = new ChargeEquipmentPage(getDriver());
		chargeEquipmentpage.verifyPageLoaded();
		return chargeEquipmentpage;
	}

	/**
	 * Navigate to Charge Features Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected ChargeFeaturesPage navigateToChargeFeaturesPage(MyTmoData myTmoData, String categoryOrLine) {

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickChargeCategory(categoryOrLine);
		ChargeFeaturesPage chargeFeaturesPage = new ChargeFeaturesPage(getDriver());
		chargeFeaturesPage.verifyPageLoaded();
		return chargeFeaturesPage;
	}

	/**
	 * Navigate to Charge One Time Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected ChargeOneTimePage navigateToChargeOneTimePage(MyTmoData myTmoData) {
		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickonetimecharges();
		ChargeOneTimePage chargeOneTimePage = new ChargeOneTimePage(getDriver());
		chargeOneTimePage.verifyPageLoaded();

		return chargeOneTimePage;
	}

	/**
	 * Navigate to Charge Plan Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected ChargePlanPage navigateToChargePlanPage(MyTmoData myTmoData) {
		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickPalns();
		ChargePlanPage chargePlanPage = new ChargePlanPage(getDriver());
		chargePlanPage.verifyPageLoaded();
		return chargePlanPage;
	}

	/**
	 * Navigate to EIP Details Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected EIPDetailsPage navigateToEIPDetailsPage(MyTmoData myTmoData) {
		navigateToBillingPage(myTmoData);
		waitForPageLoad(getDriver());
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.checkPageIsReady();
		if (getDriver().getCurrentUrl().contains("/billandpay")) {
			BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
			billAndPaySummaryPage.verifyPageLoaded();
			billAndPaySummaryPage.clickEquipmentInstallmentPlansShortcut();
		} else {
			BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
			billingSummaryPage.changeBillCycle();
			billingSummaryPage.clickViewDetails();

		}
		EIPDetailsPage eIPDetailsPage = new EIPDetailsPage(getDriver());
		eIPDetailsPage.verifyPageLoaded();
		return eIPDetailsPage;
	}

	/**
	 * Navigate to EIP Payment Confirmation Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected EIPPaymentConfirmationCCPage navigateToEIPPaymentConfirmationCCPage(MyTmoData myTmoData,
			String paymentType) {
		EIPPaymentReviewCCPage reviewCCPage = navigateToEIPPaymentReviewCCPage(myTmoData, paymentType);
		reviewCCPage.clickCCTermsAndConditions();
		reviewCCPage.clickCreditCardSubmitButton();
		EIPPaymentConfirmationCCPage eIPPaymentConfirmationCCPage = new EIPPaymentConfirmationCCPage(getDriver());
		eIPPaymentConfirmationCCPage.verifyPageLoaded();
		eIPPaymentConfirmationCCPage.verifyPaymentConfirmationDetails(amount, myTmoData.getPayment().getZipCode(),
				myTmoData.getPayment().getCardNumber());
		eIPPaymentConfirmationCCPage.Validatepaymentdate();
		eIPPaymentConfirmationCCPage.verifyPrintConfirmationLink();
		return eIPPaymentConfirmationCCPage;
	}

	/**
	 * Navigate to EIP Payment Forward Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected EIPPaymentForwardPage navigateToEIPPaymentForwardPage(MyTmoData myTmoData) {
		EIPDetailsPage eipDetailsPage = navigateToEIPDetailsPage(myTmoData);
		eipDetailsPage.clickMakePayment();
		EIPPaymentForwardPage eIPPaymentForwardPage = new EIPPaymentForwardPage(getDriver());
		eIPPaymentForwardPage.verifyPageLoaded();
		return eIPPaymentForwardPage;
	}

	/**
	 * Navigate to EIP Payment Review Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected EIPPaymentReviewCCPage navigateToEIPPaymentReviewCCPage(MyTmoData myTmoData, String paymentType) {
		EIPPaymentForwardPage eipPaymentForwardPage = navigateToEIPPaymentForwardPage(myTmoData);
		amount = eipPaymentForwardPage.enterPaymentamount();
		if (!eipPaymentForwardPage.verifySavedCCisChecked()) {
			eipPaymentForwardPage.fillPaymentInfo(myTmoData.getPayment());
		}
		eipPaymentForwardPage.clickNextButton();
		EIPPaymentReviewCCPage eIPPaymentReviewCCPage = new EIPPaymentReviewCCPage(getDriver());
		eIPPaymentReviewCCPage.verifyPageLoaded();
		return eIPPaymentReviewCCPage;
	}

	/**
	 * Navigate to AutoPay FAQ Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected FAQPage navigateToAutoPayFAQPage(MyTmoData myTmoData) {
		AutoPayPage autoPayPage = navigateToAutoPayPage(myTmoData);
		autoPayPage.clickAutoPayFAQsLink();
		FAQPage fAQPage = new FAQPage(getDriver());
		fAQPage.verifyPageLoaded();
		return fAQPage;
	}

	/**
	 * Navigate to Lease Details Page Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected LeaseDetailsPage navigateToLeaseDetailsPage(MyTmoData myTmoData) {

		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickBillingLink();
		BillingSummaryPage billSummaryPage = new BillingSummaryPage(getDriver());
		billSummaryPage.verifyPageLoaded();
		billSummaryPage.clickViewLeaseDeatils();
		LeaseDetailsPage leaseDetailsPage = new LeaseDetailsPage(getDriver());
		leaseDetailsPage.verifyPageLoaded();
		return leaseDetailsPage;
	}

	/**
	 * Navigate To One Payment Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected OneTimePaymentPage navigateToOTPpage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		if (System.getProperty("environment").contains("qata03")) {
			getDriver().get(System.getProperty("environment") + "/onetimepayment");
		} else {
			homePage.clickPayBillbutton();
		}
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		if (oneTimePaymentPage.verifyReviewPAModalIsDisplayedOrNot()) {
			oneTimePaymentPage.clickContinueWithPaymentCTA();
		}
		return oneTimePaymentPage;
	}

	/**
	 * Navigate to OTP Amount Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected OTPAmountPage navigateToEditAmountPage(MyTmoData myTmoData) {
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage oTPAmountPage = new OTPAmountPage(getDriver());
		oTPAmountPage.verifyPageLoaded();
		return oTPAmountPage;
	}

	public OTPTncPage navigateOTPTncPage(MyTmoData myTmoData) {
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickTermsAndConditions();
		OTPTncPage OTPTncPage = new OTPTncPage(getDriver());
		OTPTncPage.verifyPageLoaded();
		return OTPTncPage;
	}

	public PATncPage navigatePATncPage(MyTmoData myTmoData) {
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickTermsAndConditionsLink();
		PATncPage PATncPage = new PATncPage(getDriver());
		PATncPage.verifyPageLoaded();
		return PATncPage;
	}

	public PAFAQPage navigatePAFAQPage(MyTmoData myTmoData) {
		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickSeeFAQS();
		PAFAQPage PAFAQPage = new PAFAQPage(getDriver());
		PAFAQPage.verifyPageLoaded();
		return PAFAQPage;
	}

	/**
	 * Navigate to OTP Confirmation Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected OTPConfirmationPage navigateToOTPConfirmationPage(MyTmoData myTmoData) {
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickamountIcon();
		OTPAmountPage editAmountPage = new OTPAmountPage(getDriver());
		editAmountPage.verifyPageLoaded();
		editAmountPage.clickAmountRadioButton("Other");
		Double payAmount = generateRandomAmount();
		editAmountPage.setOtherAmount(payAmount.toString());
		editAmountPage.clickUpdateAmount();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		addNewBank(myTmoData.getPayment(), otpSpokePage);
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.agreeAndSubmit();
		OTPConfirmationPage oTPConfirmationPage = new OTPConfirmationPage(getDriver());
		oTPConfirmationPage.verifyPageLoaded();
		return oTPConfirmationPage;
	}

	/**
	 * Navigate to PA Confirmation Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected PAConfirmationPage navigateToPAConfirmationPage(MyTmoData myTmoData) {

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.clickinstallmentDetails();
		paymentArrangementPage.clickDatePicker();
		paymentArrangementPage.pickDate();
		paymentArrangementPage.clickUpdateButton();
		paymentArrangementPage.clickpaymentMethod();
		paymentArrangementPage.verifyPaymentSpokePageDispalyed();
		paymentArrangementPage.clickAddCard();
		paymentArrangementPage.verifyCardInfoHeader();
		paymentArrangementPage.fillCardDetails(myTmoData.getPayment());
		paymentArrangementPage.clickContinueAddCard();
		paymentArrangementPage.verifyAgreeAndSubmitButton();
		paymentArrangementPage.clickAgreeNSubmitBtn();
		PAConfirmationPage paConfirmationPage = new PAConfirmationPage(getDriver());
		paConfirmationPage.verifyPageLoaded();
		return paConfirmationPage;
	}

	/**
	 * Navigate to OTP Date Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected OTPDatePage navigateToOTPDatePage(MyTmoData myTmoData) {
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.verifyDateBlade();
		oneTimePaymentPage.clickDateIcon();
		OTPDatePage oTPDatePage = new OTPDatePage(getDriver());
		oTPDatePage.verifyPageLoaded();
		return oTPDatePage;
	}

	/**
	 * Navigate to PA installment details Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected PAInstallmentDetailsPage navigateToPAInstallmentDetailsPage(MyTmoData myTmoData) {

		PaymentArrangementPage paymentArrangementPage = navigateToPaymentArrangementPage(myTmoData);
		paymentArrangementPage.verifyPageLoaded();
		paymentArrangementPage.clickinstallemtBlade();
		PAInstallmentDetailsPage pAInstallmentDetailsPage = new PAInstallmentDetailsPage(getDriver());
		pAInstallmentDetailsPage.verifyPageLoaded();
		return pAInstallmentDetailsPage;
	}

	/**
	 * Navigate To Payment Arrangement page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected PaymentArrangementPage navigateToPaymentArrangementPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		displayBillDueDateAndAmount(homePage);
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		/*
		 * if (oneTimePaymentPage.verifyReviewPAModalIsDisplayedOrNot()) {
		 * oneTimePaymentPage.clickReviewPABtnOnModal(); } else if
		 * (oneTimePaymentPage.verifyPaymentArrangementLinkDisplayed()) {
		 * oneTimePaymentPage.clickpaymentArrangement(); } else {
		 * 
		 * oneTimePaymentPage.clickCancelBtn();
		 * oneTimePaymentPage.clickContinueBtnModal(); waitForPageLoad(getDriver());
		 * homePage.clickAccountHistoryHeader(); AccountHistoryPage alertsActivitespage
		 * = new AccountHistoryPage(getDriver());
		 * alertsActivitespage.verifyPageLoaded();
		 * alertsActivitespage.clickAddpaymentmethod(); }
		 */
		oneTimePaymentPage.verifyPaymentArrangementLinkDisplayed();
		oneTimePaymentPage.clickpaymentArrangement();
 
		getDriver().navigate().to(System.getProperty("environment") + "/paymentarrangement");
		paymentArrangementPage.verifyPageLoaded();
		return paymentArrangementPage;
	}

	/**
	 * Navigate To Payment Arrangement page directly from Home page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected PaymentArrangementPage navigateDirectlyToPaymentArrangementPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		if (!homePage.clickSetupPaymentArrangementLink()) {
			homePage.clickPayBillbutton();
			OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
			oneTimePaymentPage.verifyPageLoaded();
			oneTimePaymentPage.verifyPaymentArrangementLinkDisplayed();
			oneTimePaymentPage.clickpaymentArrangement();
		}
		PaymentArrangementPage paymentArrangementPage = new PaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
		return paymentArrangementPage;
	}

	/**
	 * Navigate To Billing Summary Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected PaymentErrorPage navigateToPaymentErrorPage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		getDriver().get("https://deva0b.eservice.t-mobile.com/testPaymentError");
		PaymentErrorPage paymenterrorpage = new PaymentErrorPage(getDriver());
		paymenterrorpage.verifyPageLoaded();
		Reporter.log("payment Error  page is displayed");
		return paymenterrorpage;
	}

	/**
	 * Navigate to Previous Charge Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected PreviousChargePage navigateToPreviousChargePage(MyTmoData myTmoData, String categoryOrLine) {

		BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
		billAndPaySummaryPage.clickViewByLineSelector();
		billAndPaySummaryPage.clickChargeCategory(categoryOrLine);
		PreviousChargePage previousChargePage = new PreviousChargePage(getDriver());
		previousChargePage.verifyPageLoaded();
		return previousChargePage;
	}

	/**
	 * Navigate to spoke page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected SpokePage navigateToSpokePage(MyTmoData myTmoData) {
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyPageLoaded();
		return otpSpokePage;
	}

	/**
	 * Navigate to Terms and Conditions Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected TnCPage navigateToAutoPayTnCPage(MyTmoData myTmoData) {
		AutoPayPage autoPayPage = navigateToAutoPayPage(myTmoData);
		autoPayPage.clicktermsNconditions();
		TnCPage tnCPage = new TnCPage(getDriver());
		tnCPage.verifyPageLoaded();
		return tnCPage;

	}

	/**
	 * Navigate to Usage Overview page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected UsageOverviewPage navigateToUsageOverviewPage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickUsageLink();
		UsageOverviewPage usageOverview = new UsageOverviewPage(getDriver());
		usageOverview.verifyUsageOverviewPage();
		return usageOverview;
	}

	/**
	 * Navigate to Usage page
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected UsagePage navigateToUsagePage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.clickUsageLink();
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.waitforSpinner();
		UsagePage usage = new UsagePage(getDriver());
		usage.verifyPageLoaded();
		return usage;
	}

	/**
	 * Click Download PDF On Billing Page
	 */
	public void clickDownloadPdfOnBillingPage(MyTmoData myTmoData) {
		BillingSummaryPage billingSummaryPage = navigateToBillingSummaryPage(myTmoData);
		billingSummaryPage.verifyBillSummaryPageHeader();
		billingSummaryPage.clickDownLoadPDF();
		Reporter.log("pdf downloaded");
	}

	/**
	 * Cancel Auto Pay
	 * 
	 */
	protected void cancelAutoPay(MyTmoData myTmoData) {

		HomePage homePage = new HomePage(getDriver());
		if (homePage.verifyAutopayisscheduled()) {
			AutoPayPage autoPayPage = navigateToAutoPayPagefromHomePage(myTmoData);

			autoPayPage.verifyPageLoaded();
			autoPayPage.clickCancelAutoPayLink();
			autoPayPage.verifycancelAutoPayModal();
			autoPayPage.clickYesCancelAutoPay();
			AutoPayCancelConfirmationPage autoPayCancelConfirmationPage = new AutoPayCancelConfirmationPage(
					getDriver());
			autoPayCancelConfirmationPage.verifyPageLoaded();
			autoPayCancelConfirmationPage.clickreturnToHome();
			homePage.verifyPageLoaded();

		}

	}

	/**
	 * Cancel Auto Pay
	 * 
	 * @param homePage
	 */
	protected void cancelonlyAutoPay(HomePage homePage, MyTmoData myTmoData) {

		// if (!homePage.verifyAutopayisscheduled()) {

		navigateToAutoPayPagefromHomePage(myTmoData);

		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyPageLoaded();
		autoPayPage.clickCancelAutoPayLink();
		autoPayPage.verifycancelAutoPayModal();
		autoPayPage.clickYesCancelAutoPay();

		// }
	}

	/**
	 * common method to add card information for auto pay tests
	 * 
	 * @param payment
	 * @param otpSpokePage
	 */
	protected void addNewCard(Payment payment, SpokePage otpSpokePage) {
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.fillCardInfo(payment);
		addCardPage.clickContinueAddCard();
	}

	/**
	 * common method to add bank information for auto pay tests
	 * 
	 * @param payment
	 * @param otpSpokePage
	 */
	protected Long addNewBank(Payment payment, SpokePage otpSpokePage) {
		otpSpokePage.verifyPageLoaded();
		otpSpokePage.clickAddBank();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		addBankPage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);
		addBankPage.fillBankInfo(payment);
		Long accNo = addBankPage.clickContinueAddBank();
		return accNo;
	}

	/**
	 * Go to AutoPayPage from Home Page
	 *
	 */
	protected AutoPayPage clickSetUpAutoPay() {
		HomePage homePage = new HomePage(getDriver());
		homePage.clickEasyPay();
		AutoPayPage autoPayPage = new AutoPayPage(getDriver());
		autoPayPage.verifyAutoPayLandingPageFields();
		return autoPayPage;
	}

	/**
	 * generate a unique amount value
	 * 
	 * @return double - amount
	 */
	protected Double generateRandomAmount() {
		double min = 1.01;
		double max = 2.00;
		Random r = new Random();
		return r.nextDouble() * (max - min) + min;
	}

	protected void setupAutopay(MyTmoData myTmoData) {
		HomePage homePage = new HomePage(getDriver());
		homePage.clickBillingLink();
		if (getDriver().getCurrentUrl().contains("/billandpay")) {
			BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
			billAndPaySummaryPage.verifyPageLoaded();
			billAndPaySummaryPage.clickAutopayShortcut();

		} else {

			BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
			billingSummaryPage.verifyPageLoaded();
			billingSummaryPage.clickAutopayShortcut();

		}
		AutoPayPage autopayLandingPage = new AutoPayPage(getDriver());
		autopayLandingPage.verifyAutoPayLandingPageFields();
		if (!autopayLandingPage.verifyIfPaymentMethodIsSaved()) {
			autopayLandingPage.addBankToPaymentMethod(myTmoData);
		}
		autopayLandingPage.clickAgreeAndSubmitBtn();
		AutoPayConfirmationPage confirmationPage = new AutoPayConfirmationPage(getDriver());
		confirmationPage.verifyPageLoaded();
		confirmationPage.verifySuccessHeader();
		confirmationPage.clickreturnToHome();
		homePage.verifyPageLoaded();
	}

	protected void displayBillDueDateAndAmount(HomePage homePage) {
		// homePage.getBalanceDueAmount();
		// homePage.getDueDate();
	}

	/**
	 * This method is to remove stored bank accounts in OTP
	 */
	public void removedStoredBankAccounts() {
		HomePage homePage = new HomePage(getDriver());
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage spokePage = new SpokePage(getDriver());
		spokePage.verifyPageLoaded();
		spokePage.removeStoredBankAccount();
	}

	/**
	 * Navigate To New Shop Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public ShopPage navigateToShopPage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		HomePage homePage = new HomePage(getDriver());
		homePage.clickShoplink();
		ShopPage newShopPage = new ShopPage(getDriver());
		newShopPage.verifyNewShoppage();
		return newShopPage;
	}

	public EIPPaymentForwardPage navigatefromShoptoEIPpaymentForwardPage(MyTmoData myTmoData) {
		ShopPage shopPage = navigateToShopPage(myTmoData);
		shopPage.clickFeaturedDeviceByName(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		// pdpPage.clickUpgradeButton();
		pdpPage.clickAddToCart();
		LineSelectorPage linePage = new LineSelectorPage(getDriver());
		linePage.verifyLineSelectorPage();
		linePage.clickonPaynowLink();
		EIPPaymentForwardPage paymentPage = new EIPPaymentForwardPage(getDriver());
		paymentPage.verifyPageLoaded();
		return paymentPage;
	}

	/**
	 * Navigate from Shop Page to EIP payment review page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public EIPPaymentReviewCCPage navigatefromShoptoEIPpaymentReviewPage(MyTmoData myTmoData) {
		EIPPaymentForwardPage paymentPage = navigatefromShoptoEIPpaymentForwardPage(myTmoData);
		paymentPage.verifyChangeButton();
		paymentPage.verifyEIPfullCheckBox();
		paymentPage.clickNewRadioButton();
		paymentPage.fillPaymentInfo(myTmoData.getPayment());
		paymentPage.clickNextButton();
		EIPPaymentReviewCCPage paymentReviewPage = new EIPPaymentReviewCCPage(getDriver());
		paymentReviewPage.verifyPageLoaded();
		return paymentReviewPage;
	}

	/**
	 * Navigate from Shop Page to EIP payment Confirmation page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public EIPPaymentConfirmationCCPage navigatefromShoptoEIPpaymentConfirmationPage(MyTmoData myTmoData) {
		EIPPaymentReviewCCPage paymentReviewPage = navigatefromShoptoEIPpaymentReviewPage(myTmoData);
		paymentReviewPage.verifyPageLoaded();
		paymentReviewPage.clickCCTermsAndConditions();
		paymentReviewPage.clickCreditCardSubmitButton();
		EIPPaymentConfirmationCCPage confirmationCCPage = new EIPPaymentConfirmationCCPage(getDriver());
		confirmationCCPage.verifyPageLoaded();
		return confirmationCCPage;
	}

	public HomePage verifyHomePageBalanceDue(String balanceAmount, String payAmount, String amountAfterPayment,
			HomePage homePage) {

		if (amountAfterPayment.equals("Error")) {
			throw new FrameworkException("Balance amount is not displayed on the home page after payment");
		} else if (amountAfterPayment.equals(balanceAmount)) {
			throw new FrameworkException("Balance amount is not updated on the home page after payment");
		} else {
			Double balance = Double.parseDouble(balanceAmount.replaceAll("\\$", ""));
			Double paymentAmount = Double.parseDouble(payAmount);
			balance = balance - paymentAmount;
			homePage.verifyBalanceAfterPayment(balance);
			return homePage;
		}
	}

	public UsagePage checkmonthlyspecificViewbycatagory(MyTmoData myTmoData, String Catagory) {

		UsagePage usage = navigateToUsagePage(myTmoData);
		UsageDetailsPage usageDeatilsPage = new UsageDetailsPage(getDriver());
		usage.selectrequiredmonthforcheck(Catagory);
		boolean irun = true;
		int exec = 0;
		while (irun) {
			WebElement ele = usage.getitemsviewByCatagory(Catagory);
			usage.expandcatagory(ele, Catagory);
			List<WebElement> chele = usage.getItemsViewByCatagoryAccounts(ele);
			int clesize = chele.size();
			usage.clickOnCatogryAccountBlade(chele, exec);
			usageDeatilsPage.verifyUsageDetailsPage();
			usageDeatilsPage.clickBackButton();
			usage.verifyPageLoaded();
			exec++;
			if (exec == clesize)
				irun = false;

		}

		return usage;
	}

	public void staticwait(int waittime) {
		try {
			Thread.sleep(waittime);
		} catch (Exception e) {

		}
	}

	public UsagePage checkallMonthsViewbycatagory(MyTmoData myTmoData, String Catagory, String Header) {
		UsagePage usage = navigateToUsagePage(myTmoData);
		UsageDetailsPage usageDeatilsPage = new UsageDetailsPage(getDriver());
		List<WebElement> months = usage.getbillingCycles();
		int maxwait = 0;
		while (months.size() == 0) {
			staticwait(1000);
			months = usage.getbillingCycles();
			maxwait++;
			if (maxwait == 20)
				Assert.fail("Usage graph is not loaded");

		}
		// usageDeatilsPage.verifyPageLoaded();

		if (months.size() == 0)
			Assert.fail();
		for (int mnt = 0; mnt < months.size(); mnt++) {
			usage.clickMonths(months, mnt);

			boolean irun = true;
			int exec = 0;
			while (irun) {

				WebElement ele = usage.getitemsviewByCatagory(Catagory);

				usage.expandcatagory(ele, Catagory);
				List<WebElement> chele = usage.getItemsViewByCatagoryAccounts(ele);
				int clesize = chele.size();
				usage.clickOnCatogryAccountBlade(chele, exec);
				usageDeatilsPage.verifyUsageDetailsPage(Header);
				usageDeatilsPage.clickBackButton();
				usage.verifyPageLoaded();

				exec++;
				if (exec == clesize)
					irun = false;

			}

		}

		return usage;
	}

	public UsagePage checkallMonthsViewbyLine(MyTmoData myTmoData, String Catagory, String header) {
		UsagePage usage = navigateToUsagePage(myTmoData);
		UsageDetailsPage usageDeatilsPage = new UsageDetailsPage(getDriver());

		List<WebElement> months = usage.getbillingCycles();
		for (int mnt = 0; mnt < months.size(); mnt++) {
			usage.clickMonths(months, mnt);

			usage.clickViewByLine();
			List<WebElement> Lineitems = usage.getcountviewbyLineitems();
			for (int iexec = 0; iexec < Lineitems.size(); iexec++) {
				usage.expandlineitemarrows(Lineitems, iexec);
				usage.clickOnItemByLineItems(Catagory, Lineitems, iexec);
				usageDeatilsPage.verifyUsageDetailsPage(header);
				usageDeatilsPage.clickBackButton();
				usage.verifyPageLoaded();
				usage.contrastlineitemarrows(Lineitems, iexec);
			}

		}

		return usage;
	}

	public UsagePage checkspecificMonthViewbyLine(MyTmoData myTmoData, String Catagory) {
		UsagePage usage = navigateToUsagePage(myTmoData);
		UsageDetailsPage usageDeatilsPage = new UsageDetailsPage(getDriver());
		usage.selectrequiredmonthforcheck(Catagory);

		usage.clickViewByLine();
		List<WebElement> Lineitems = usage.getcountviewbyLineitems();
		for (int iexec = 0; iexec < Lineitems.size(); iexec++) {
			usage.expandlineitemarrows(Lineitems, iexec);
			usage.clickOnItemByLineItems(Catagory, Lineitems, iexec);
			usageDeatilsPage.verifyUsageDetailsPage();
			usageDeatilsPage.clickBackButton();
			usage.verifyPageLoaded();
			usage.contrastlineitemarrows(Lineitems, iexec);
		}

		return usage;
	}

	public UsagePage checkslinescountinbothviews(MyTmoData myTmoData, String Catagory) {
		UsagePage usage = navigateToUsagePage(myTmoData);

		List<WebElement> months = usage.getbillingCycles();
		for (int mnt = 0; mnt < months.size(); mnt++) {
			usage.clickMonths(months, mnt);
			usage.clickViewByCategory();
			WebElement ele = usage.getitemsviewByCatagory(Catagory);
			usage.expandcatagory(ele, Catagory);
			List<WebElement> chele = usage.getItemsViewByCatagoryAccounts(ele);
			usage.clickViewByLine();
			List<WebElement> Lineitems = usage.getcountviewbyLineitems();

			if (chele.size() == Lineitems.size())
				Reporter.log("Line count is matching in both views");
			else
				Assert.fail("Line count is not matching in both views");

		}
		return usage;
	}

	public EipJodPage navigateToEIPJODpage(MyTmoData myTmoData) {
		// navigateToNewHomePage(myTmoData);
		// homepage.clickBillingLink();
		// getDriver().get(System.getProperty("environment") + "/eipjod");//eip-jod-poip
		navigateToBillingPage(myTmoData);
		if (getDriver().getCurrentUrl().contains("/billandpay")) {
			BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
			billAndPaySummaryPage.verifyPageLoaded();
			billAndPaySummaryPage.verifyEquipmentInstallmentPlansShortcut();
			billAndPaySummaryPage.clickEquipmentInstallmentPlansShortcut();

		} else {// if (getDriver().getCurrentUrl().contains("//bill")){

			BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
			billingSummaryPage.verifyPageLoaded();
			billingSummaryPage.verifyEipText();
			billingSummaryPage.verifyEipSubAlertText();
			billingSummaryPage.clickInstallemntArrow();
		}

		EipJodPage eipJodPage = new EipJodPage(getDriver());
		eipJodPage.verifyPageLoaded();
		return eipJodPage;
	}

	public EipJodPage navigateToActiveJODDeeplinkingPage(MyTmoData myTmoData) {
		navigateToNewHomePage(myTmoData);
		// homepage.clickBillingLink();
		getDriver().get(System.getProperty("environment") + "/eipjod?device=leaseActive_0");
		EipJodPage eipJodPage = new EipJodPage(getDriver());
		eipJodPage.verifyPageLoaded();
		return eipJodPage;
	}

	public AddBankPage navigateToNewAddBankPage(MyTmoData myTmoData) {
		HomePage homepage = navigateToHomePage(myTmoData);
		getDriver().get(System.getProperty("environment") + "/payments/paymentblade");
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.clickAddBank();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		return addBankPage;
		
	}

	public AddCardPage navigateToNewAddCardPage(MyTmoData myTmoData) {
		HomePage homepage = navigateToHomePage(myTmoData);
		getDriver().get(System.getProperty("environment") + "/payments/paymentblade");
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		return addCardPage;
	}

	public OneTimePaymentPage navigateToNewpaymentBladePage(MyTmoData myTmoData) {
		HomePage homepage = navigateToHomePage(myTmoData);
		getDriver().get(System.getProperty("environment") + "/payments/paymentblade");
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		return oneTimePaymentPage;
	}

	public SpokePage navigateToNewSpokePage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		getDriver().get(System.getProperty("environment") + "/payments/paymentblade");
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.clickPaymentMethodBlade();
		SpokePage otpSpokePage = new SpokePage(getDriver());
		otpSpokePage.verifyNewPageLoaded();
		return otpSpokePage;
	}

	/**
	 * @param myTmoData
	 * @return
	 */
	public WalletPage navigateToMyWalletPage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		WalletPage walletPage = new WalletPage(getDriver());
		getDriver().navigate().to(System.getProperty("environment") + "/mywallet");
		walletPage.verifyPageLoaded();
		walletPage.clickCloseModal();
		return walletPage;
	}

	public HistoricBillsPage navigateToHistoricBillsPage(MyTmoData myTmoData) {
		BillAndPaySummaryPage bbpage = navigateToBillAndPaySummaryPage(myTmoData);
		bbpage.clickViewHistoricalBillsLink();
		HistoricBillsPage historicbillpage = new HistoricBillsPage(getDriver());
		historicbillpage.verifyPageLoaded();
		return historicbillpage;
	}

	public UsageDetailsPage NavigatetoUsagedetailspage(MyTmoData myTmoData, String Catagory, String Header) {
		UsagePage usage = navigateToUsagePage(myTmoData);
		UsageDetailsPage usageDeatilsPage = new UsageDetailsPage(getDriver());
		List<WebElement> months = usage.getbillingCycles();
		boolean identified = false;
		for (int mnt = 0; mnt < months.size(); mnt++) {
			usage.clickMonths(months, mnt);

			WebElement ele = usage.getitemsviewByCatagory(Catagory);
			if (usage.checkvalidnodewithdata(ele)) {
				usage.expandcatagory(ele, Catagory);
				if (usage.clickOnNodeWithDataExist(ele)) {
					usageDeatilsPage.verifyUsageDetailsPage(Header);
					return usageDeatilsPage;
				} else {
					Assert.fail("Data is not showing in sub nodes");
				}
			}

		}
		if (!identified)
			Assert.fail("there is no data for given msisdn");
		return usageDeatilsPage;
	}

	public UsageDetailsPage navigatetoUsagedetailspageFromUsagePage(MyTmoData myTmoData, String Catagory,
			String Header) {
		UsagePage usage = new UsagePage(getDriver());
		UsageDetailsPage usageDeatilsPage = new UsageDetailsPage(getDriver());
		List<WebElement> months = usage.getbillingCycles();
		boolean identified = false;
		for (int mnt = 0; mnt < months.size(); mnt++) {
			usage.clickMonths(months, mnt);

			WebElement ele = usage.getitemsviewByCatagory(Catagory);
			if (usage.checkvalidnodewithdata(ele)) {
				usage.expandcatagory(ele, Catagory);
				if (usage.clickOnNodeWithDataExist(ele)) {
					usageDeatilsPage.verifyUsageDetailsPage(Header);
					return usageDeatilsPage;
				} else {
					Assert.fail("Data is not showing in sub nodes");
				}
			}

		}
		if (!identified)
			Assert.fail("there is no data for given msisdn");
		return usageDeatilsPage;
	}

	/**
	 * Navigate to Billing and Payments page in profile
	 * 
	 * @param myTmoData
	 * @return
	 */
	public BillingPaymentsPage navigateToBillingPaymentsPage(MyTmoData myTmoData) {
		ProfilePage profile = navigateToProfilePage(myTmoData);
		profile.clickBillingPayments();
		BillingPaymentsPage billingandpaymentspage = new BillingPaymentsPage(getDriver());
		billingandpaymentspage.verifyPageLoaded();
		return billingandpaymentspage;
	}

	/**
	 * Navigate to Billing and Payments page in profile
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected SpokePage navigateToPaymentsCollectionPage(MyTmoData myTmoData) {
		ProfilePage profilePage = navigateToProfilePage(myTmoData);
		profilePage.clickBillingPayments();
		BillingPaymentsPage billingAndPaymentsPage = new BillingPaymentsPage(getDriver());
		billingAndPaymentsPage.verifySavedPaymentMethodLink();
		billingAndPaymentsPage.clickSavedPaymentMethodLink();
		SpokePage paymentsCollectionPage = new SpokePage(getDriver());
		paymentsCollectionPage.verifyNewPageLoaded();
		return paymentsCollectionPage;
	}

	/**
	 * Login To MyTmo,Navigate to new Autopay page
	 * 
	 * @param myTmoData
	 */
	protected NewAutopayPage navigateToNewAutoPayPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		getDriver().get(System.getProperty("environment") + "/payments/autopay");
		NewAutopayPage newautoPayPage = new NewAutopayPage(getDriver());
		newautoPayPage.verifyPageLoaded();
		return newautoPayPage;
	}

	public EIPPaymentPage navigatefromShoptoEIPPaymentAmountPage(MyTmoData myTmoData) {
		ShopPage shopPage = navigateToShopPage(myTmoData);
		ShopHomePage shophomePage = new ShopHomePage(getDriver());
		shophomePage.verifyforWaitFeaturedPhoneTitle();
		// shopPage.clickFeaturedDeviceByName(myTmoData.getDeviceName());
		shopPage.clickOnFeaturedDevices(myTmoData.getDeviceName());
		PDPPage pdpPage = new PDPPage(getDriver());
		pdpPage.verifyPDPPage();
		// pdpPage.clickUpgradeButton();
		pdpPage.clickAddToCart();
		LineSelectorPage linePage = new LineSelectorPage(getDriver());
		linePage.verifyLineSelectorPage();
		// linePage.clickonPaynowLink();
		linePage.clickDevice();
		ShopLineSectorDetailsPage lineDetailsPage = new ShopLineSectorDetailsPage(getDriver());
		lineDetailsPage.verifyPageLoaded();
		lineDetailsPage.clickContinueButton();
		EIPPaymentPage paymentLandingPage = new EIPPaymentPage(getDriver());
		paymentLandingPage.verifyPageLoaded();
		return paymentLandingPage;
	}

	/**
	 * Navigate to Billing and Payments page in profile
	 * 
	 * @param myTmoData
	 * @return
	 * @throws Exception
	 */
	public BillingSummaryPage navigateToEbillpagethroughBBpage(MyTmoData myTmoData) throws Exception {

		HistoricBillsPage historybillpage = navigateToHistoricBillsPage(myTmoData);
		EOSCommonLib ecl = new EOSCommonLib();
		String[] getjwt = ecl.getauthorizationandregisterinapigee(myTmoData.getLoginEmailOrPhone(),
				myTmoData.getLoginPassword());
		EOSBillingFilters billing = new EOSBillingFilters();
		Response billlistrespons = billing.getResponsebillList(getjwt);
		String ebilldate = billing.getebillstatementmonth(billlistrespons);
		if (ebilldate == null)
			Assert.fail("No ebill is identified for given data");
		LocalDate date = LocalDate.parse(ebilldate);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMMM d, yyyy");
		String reqdate = formatter.format(date).toString();
		historybillpage.Clickongivenbillcycle(reqdate);
		BillingSummaryPage billSummaryPage = new BillingSummaryPage(getDriver());
		billSummaryPage.verifyAjaxSpinnerInvisible();
		billSummaryPage.verifyPageLoaded();
		return billSummaryPage;

	}

	/**
	 * Navigate to POIP DocusignPage
	 * 
	 * @param myTmoData
	 * @return
	 * @throws Exception
	 */
	public DocuSignPage navigateToPoipDocUsignPage(MyTmoData myTmoData) {
		// EipJodPage eipJodPage = navigateToEIPJODpage(myTmoData);
		EipJodPage eipJodPage = navigateToNewEIPJODpage(myTmoData);
		eipJodPage.verifyTakeActionCTA();
		eipJodPage.clickTakeActionCTA();
		JODEndingSoonOptionsPage optionsPage = new JODEndingSoonOptionsPage(getDriver());
		optionsPage.verifyPageLoaded();
		optionsPage.verifyOption1BladeDetails();
		optionsPage.verifyLegalText();
		optionsPage.clickSetupInstallementPlan();
		PurchaseOptionInstallmentPlanPage poipPage = new PurchaseOptionInstallmentPlanPage(getDriver());
		poipPage.verifyPageLoaded();
		poipPage.verifyPaymentMethodBlade();
		poipPage.clickPaymentMethodBlade();
		SpokePage sPaymentsCollectionPage = new SpokePage(getDriver());
		sPaymentsCollectionPage.verifyNewPageLoaded();
		sPaymentsCollectionPage.verifyNewPageUrl();
		sPaymentsCollectionPage.clickAddCard();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.fillCardDetails(myTmoData.getPayment());
		addCardPage.clickContinueAddCard();
		poipPage.clickCTAButton();
		DocuSignPage sDocSignPage = new DocuSignPage(getDriver());
		sDocSignPage.verifyPageLoaded();
		return sDocSignPage;

	}

	public EipJodPage navigateToNewEIPJODpage(MyTmoData myTmoData) {
		navigateToNewHomePage(myTmoData);
		// homepage.clickBillingLink();
		getDriver().get(System.getProperty("environment") + "/eipjod");// eip-jod-poip
		EipJodPage eipJodPage = new EipJodPage(getDriver());
		eipJodPage.verifyPageLoaded();
		return eipJodPage;
	}

	/**
	 * Navigate to POIP Payment ConfirmationPage
	 * 
	 * @param myTmoData
	 * @return
	 * @throws Exception
	 */
	public PoipConfirmationPage navigateToPoipPaymentConfirmationPage(MyTmoData myTmoData) {
		DocuSignPage sDocSignPage = navigateToPoipDocUsignPage(myTmoData);
		sDocSignPage.clickDisclosureCheckBox();
		sDocSignPage.clickContinueButton();
		sDocSignPage.clickSignImageIcon();
		sDocSignPage.editSignature();
		sDocSignPage.clickAdoptSignButton();
		sDocSignPage.clickSubmitOrderButton();
		PoipConfirmationPage popSuccessPage = new PoipConfirmationPage(getDriver());
		popSuccessPage.verifyPageUrl();
		return popSuccessPage;

	}

	public EipJodPage navigateToNewEIPpage(MyTmoData myTmoData) {
		navigateToBillingPage(myTmoData);
		waitForPageLoad(getDriver());
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.waitforSpinner();
		commonPage.checkPageIsReady();

		if (getDriver().getCurrentUrl().contains("/billandpay/")) {
			BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
			billAndPaySummaryPage.verifyPageLoaded();
			billAndPaySummaryPage.clickEquipmentInstallmentPlansShortcut();
		} else {
			BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
			commonPage.verifyAjaxSpinnerInvisible();
			billingSummaryPage.changeBillCycle();
			billingSummaryPage.clickViewDetails();
		}
		EipJodPage eipJodPage = new EipJodPage(getDriver());
		eipJodPage.verifyEIPPage();
		return eipJodPage;
	}

	/**
	 * Login To MyTmo,Navigate to new Autopay page
	 * 
	 * @param myTmoData
	 */
	protected NewAutopayLandingPage navigateToNewAutoPayLandingPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		getDriver().get(System.getProperty("environment") + "/payments/autopay");

		NewAutopayLandingPage newAutopayLandingPage = new NewAutopayLandingPage(getDriver());
		newAutopayLandingPage.verifyPageLoaded();
		return newAutopayLandingPage;
	}

	/**
	 * Navigate To Profile Page
	 * 
	 * @param myTmoData
	 * @return
	 */
	public ProfilePage navigateToProfilePage(MyTmoData myTmoData) {
		NewHomePage homePage = navigateToNewHomePage(myTmoData);
		homePage.clickProfileMenu();
		ProfilePage profilePage = new ProfilePage(getDriver());
		profilePage.verifyProfilePage();
		return profilePage;
	}

	/**
	 * return due date
	 * 
	 * @param myTmoData
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> getDueDate(MyTmoData myTmoData) throws Exception {

		EOSCommonLib ecl = new EOSCommonLib();
		EOSBillingFilters billing = new EOSBillingFilters();
		EOSAutopayV3 ap = new EOSAutopayV3();

		Map<String, String> hmap = new HashMap<String, String>();

		String[] getjwt = ecl.getauthorizationandregisterinapigee(myTmoData.getLoginEmailOrPhone(),
				myTmoData.getLoginPassword());
		Response billlistrespons = billing.getResponsedataset(getjwt);
		String dueDate = billing.getdueDate(billlistrespons);

		if (dueDate == null) {
			Assert.fail("No due date is identified for given data");
		}
		String nickname = billing.getUserNickname(billlistrespons);
		Response apSearch = ap.getResponseAutopaySearch(getjwt);
		String autopaymethod = ap.AutopayPaymentmethod(apSearch);
		hmap.put("autopaymethod", autopaymethod);
		hmap.put("duedate", dueDate);
		hmap.put("nickname", nickname);
		hmap.put("accountnumber", getjwt[3]);
		return hmap;

	}

	public Map<String, String> getPaymentInfo(MyTmoData myTmoData) throws Exception {

		EOSCommonLib ecl = new EOSCommonLib();
		String[] getjwt = ecl.getauthorizationandregisterinapigee(myTmoData.getLoginEmailOrPhone(),
				myTmoData.getLoginPassword());
		EOSAutopayV3 searchAutopay = new EOSAutopayV3();
		Response responseAutoPaysearch = searchAutopay.getResponseAutopaySearch(getjwt);
		Map<String, String> Paymentvals = searchAutopay.getautopayinformation(responseAutoPaysearch);
		Paymentvals.put("accountnumber", getjwt[3]);
		return Paymentvals;

	}

	public String getrequireddates(String duedatetype, String duedate, String balance) {
		String returndate;
		LocalDate dt = LocalDate.parse(duedate);
		LocalDate ct = LocalDate.now();

		switch (duedatetype.toLowerCase()) {
		case "duedatepast":
			returndate = (balance.equalsIgnoreCase("true")) ? dt.plusMonths(1).minusDays(2).toString()
					: dt.plusMonths(1).minusDays(2).toString();
			break;
		case "outsideblackoutperiod":
			returndate = (balance.equalsIgnoreCase("true")) ? dt.minusDays(2).toString()
					: dt.plusMonths(1).minusDays(2).toString();
			break;
		case "insideblackoutperiod":
			returndate = (balance.equalsIgnoreCase("true")) ? ct.plusDays(1).toString()
					: dt.plusMonths(1).minusDays(2).toString();
			break;
		case "duedate":
			returndate = (balance.equalsIgnoreCase("true")) ? dt.plusMonths(1).minusDays(2).toString()
					: dt.plusMonths(1).minusDays(2).toString();
			break;

		default:
			returndate = (balance.equalsIgnoreCase("true")) ? dt.plusMonths(1).minusDays(2).toString()
					: dt.plusMonths(1).minusDays(2).toString();
			break;
		}

		return returndate;
	}

	public NewAutopayLandingPage setupautopaywithBank(Map<String, String> getBankinfo) throws IOException {
		NewAutopayLandingPage newaplandingPage = new NewAutopayLandingPage(getDriver());
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.clickAddPaymentmethod();
		PaymentCollectionPage pcp = new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickAddBank();
		NewAddBankPage nab = new NewAddBankPage(getDriver());
		nab.verifyBankPageLoaded();
		nab.addNewBankDetails(getBankinfo);
		nab.clickContinueCTA();
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.ClickAgreeandSubmit();
		NewAutoPayConfirmationPage apconfPage = new NewAutoPayConfirmationPage(getDriver());
		apconfPage.verifyPageLoaded();
		apconfPage.ClickBackToHomebutton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		home.URLredirecttoAutopay();
		// home.clickonAutopayLink();
		newaplandingPage.verifyPageLoaded();

		return newaplandingPage;
	}

	public NewAutopayLandingPage setupautopaywithCard(Map<String, String> cardinfo) throws IOException {
		NewAutopayLandingPage newaplandingPage = new NewAutopayLandingPage(getDriver());
		newaplandingPage.clickAddPaymentmethod();
		PaymentCollectionPage pcp = new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		pcp.clickAddCard();
		NewAddCardPage nac = new NewAddCardPage(getDriver());
		nac.verifyCardPageLoaded();
		nac.addCardInformation(cardinfo);
		newaplandingPage.verifyPageLoaded();
		newaplandingPage.ClickAgreeandSubmit();
		NewAutoPayConfirmationPage apconfPage = new NewAutoPayConfirmationPage(getDriver());
		apconfPage.verifyPageLoaded();
		apconfPage.ClickBackToHomebutton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		// home.clickonAutopayLink();
		home.URLredirecttoAutopay();
		newaplandingPage.verifyPageLoaded();

		return newaplandingPage;
	}

	public NewAutopayLandingPage cancelAutopay() throws InterruptedException {
		NewAutopayLandingPage newaplandingPage = new NewAutopayLandingPage(getDriver());
		newaplandingPage.clickCancelAutopayLink();
		newaplandingPage.checkcancelautopaytextinModel();
		newaplandingPage.clickyescancelautopayonModel();
		NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
		cancelconfPage.verifyPageLoaded();
		cancelconfPage.clickBacktoHomeButton();
		HomePage home = new HomePage(getDriver());
		home.verifyPageLoaded();
		Thread.sleep(2000);
		getDriver().get(System.getProperty("environment") + "/payments/autopay");
		newaplandingPage.verifyPageLoaded();
		return newaplandingPage;
	}

	/**
	 * Login To MyTmo, Cancel Autopay and Set it Up again
	 * 
	 * @param myTmoData
	 */
	protected NewAutopayLandingPage navigateToAutoPayPagefromBilling(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		// cancelAutoPay(homePage);
		// homePage.clickEasyPay();
		homePage.clickBillingLink();
		waitForPageLoad(getDriver());
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.waitforSpinner();
		commonPage.checkPageIsReady();
		if (getDriver().getCurrentUrl().contains("/billandpay/")) {
			BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
			billAndPaySummaryPage.verifyPageLoaded();
			billAndPaySummaryPage.clickAutopayShortcut();

		} else {

			BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
			billingSummaryPage.verifyPageLoaded();
			billingSummaryPage.clickAutopayShortcut();

		}
		NewAutopayLandingPage NewAutopayLandingPage = new NewAutopayLandingPage(getDriver());
		NewAutopayLandingPage.verifyPageLoaded();
		return NewAutopayLandingPage;
	}

	public NewAutopayTncPage navigateNewAutopayTncpage(MyTmoData myTmoData) {
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);
		newaplandingPage.clicktermsAndConditions();
		NewAutopayTncPage newAPtncpage = new NewAutopayTncPage(getDriver());
		newAPtncpage.verifyPageLoaded();
		return newAPtncpage;
	}

	public NewAutopayFAQPage navigateNewAutopayFaqpage(MyTmoData myTmoData) {
		NewAutopayLandingPage newaplandingPage = navigateToNewAutoPayLandingPage(myTmoData);
		newaplandingPage.clickseeFaqLink();
		NewAutopayFAQPage newAPfaqpage = new NewAutopayFAQPage(getDriver());
		newAPfaqpage.verifyPageLoaded();
		return newAPfaqpage;
	}

	public void deleteStoredCard(String storedCardNo, String version) {
		UpdateCardPage editCardPage = new UpdateCardPage(getDriver());
		editCardPage.verifyCardPageLoaded(PaymentConstants.Edit_card_Header);
		editCardPage.verifyEditCardPageElements(storedCardNo, version);
		editCardPage.clickRemoveFromMyWalletLink();
		editCardPage.verifyRemoveFromMyWalletModal();
		editCardPage.clickYesDeleteOnModal();
	}

	public void deleteStoredBank() {
		UpdateBankPage editBankPage = new UpdateBankPage(getDriver());
		editBankPage.verifyBankPageLoaded(PaymentConstants.Edit_bank_Header);
		editBankPage.verifyEditBankPageElements();
		editBankPage.clickRemoveFromMyWalletLink();
		editBankPage.verifyRemoveFromMyWalletModal();
		editBankPage.clickYesDeleteOnModal();
	}

	/**
	 * @param myTmoData
	 * @param walletPage
	 * @return
	 */
	public AddBankPage addBankWallet(MyTmoData myTmoData, WalletPage walletPage) {
		walletPage.clickAddBankBlade();
		AddBankPage addBankPage = new AddBankPage(getDriver());
		addBankPage.verifyBankPageLoaded(PaymentConstants.Add_bank_Header);
		addBankPage.verifySavetoMyWalletCTA();
		String accNo = addBankPage.fillBankInfo(myTmoData.getPayment());
		accNo = addBankPage.clickSaveToMyWalletCTA();
		walletPage.verifyPageLoaded();
		walletPage.verifyStoredBank(accNo);
		return addBankPage;
	}

	/**
	 * @param myTmoData
	 * @param walletPage
	 * @return
	 */
	public AddCardPage addCardWallet(MyTmoData myTmoData, WalletPage walletPage) {
		walletPage.clickAddCardBlade();
		AddCardPage addCardPage = new AddCardPage(getDriver());
		addCardPage.verifyCardPageLoaded(PaymentConstants.Add_card_Header);
		addCardPage.verifySavetoMyWalletCTA();
		addCardPage.fillCardDetails(myTmoData.getPayment());
		addCardPage.clickSetAsDefaultCheckBox();
		addCardPage.clickSaveToMyWalletCTA();
		walletPage.verifyPageLoaded();
		walletPage.verifyStoredCard(myTmoData.getPayment().getCardNumber());
		return addCardPage;
	}

	/**
	 * navigate to payments/paymentcollection page after logging in.
	 */
	public void navigateToAngular6PaymentCollectionsPage() {
		if (System.getProperty("environment").contains("qata03")) {
			getDriver().get(System.getProperty("environment") + "/payments/paymentblade");
			OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
			oneTimePaymentPage.verifyNewOTPPageLoaded();
			oneTimePaymentPage.clickPaymentMethodBlade();
		} else {
			getDriver().get(System.getProperty("environment") + "/payments/autopay");
			NewAutopayLandingPage newAutopayLandingPage = new NewAutopayLandingPage(getDriver());
			newAutopayLandingPage.verifyPageLoaded();
			newAutopayLandingPage.clickAddPaymentmethod();
		}
	}

	/**
	 * @param walletPage
	 */
	public void deleteAndVerifyStoredCardWallet(WalletPage walletPage) {
		String storedCardNo = walletPage.verifyAndClickStoredCard();
		deleteStoredCard(storedCardNo, "version");
		walletPage.verifyPageLoaded();
		walletPage.verifyDeletedPaymentMethodIsNotDisplayed(storedCardNo);
	}

	/**
	 * @param walletPage
	 */
	protected void deleteAndVerifyStoredBankWallet(WalletPage walletPage) {
		String storedBankNo = walletPage.verifyAndClickStoredBank();
		deleteStoredBank();
		walletPage.verifyPageLoaded();
		walletPage.verifyDeletedPaymentMethodIsNotDisplayed(storedBankNo);
	}

	/**
	 * @param myTmoData
	 */
	public MaintenancePage navigateToMaintenancePage(MyTmoData myTmoData) {
		navigateToHomePage(myTmoData);
		getDriver().get(System.getProperty("environment") + "/billandpay/maintenance");
		MaintenancePage maintenancePage = new MaintenancePage(getDriver());
		return maintenancePage;
	}

	public HomePage Autopayswitch(String status) throws IOException {
		getDriver().get(System.getProperty("environment") + "/payments/autopay");
		NewAutopayLandingPage newaplandingPage = new NewAutopayLandingPage(getDriver());
		HomePage home = new HomePage(getDriver());
		newaplandingPage.verifyPageLoaded();
		if (status.equalsIgnoreCase("ON")) {
			if (!newaplandingPage.checkispageAutopayON()) {
				newaplandingPage.clickAddPaymentmethod();
				PaymentCollectionPage pcp = new PaymentCollectionPage(getDriver());
				pcp.verifyPageLoaded();
				pcp.clickAddCard();
				NewAddCardPage nac = new NewAddCardPage(getDriver());
				nac.verifyCardPageLoaded();
				Map<String, String> cardinfo = getCardInfo("master");
				nac.addCardInformation(cardinfo);
				newaplandingPage.verifyPageLoaded();
				newaplandingPage.ClickAgreeandSubmit();
				NewAutoPayConfirmationPage apconfPage = new NewAutoPayConfirmationPage(getDriver());
				apconfPage.verifyPageLoaded();
				//apconfPage.NewCreditcardconfirmationpageValidations(cardinfo);
				apconfPage.ClickBackToHomebutton();
				home.verifyPageLoaded();
			} else {
				newaplandingPage.clickBackbutton();
				newaplandingPage.clickYesPleaseExitbutton();
				home.verifyPageLoaded();
			}
		}else if (status.equalsIgnoreCase("OFF")) {
			   if (!newaplandingPage.checkispageAutopayOFF()) {
				   newaplandingPage.clickCancelAutopayLink();
					newaplandingPage.checkcancelautopaytextinModel();
					newaplandingPage.clickyescancelautopayonModel();
					NewAutopayCancelConfirmationPage cancelconfPage = new NewAutopayCancelConfirmationPage(getDriver());
					cancelconfPage.verifyPageLoaded();
					cancelconfPage.clickBacktoHomeButton();
				
			  }else {
				  newaplandingPage.clickBackbutton();
					newaplandingPage.clickYesPleaseExitbutton();
					home.verifyPageLoaded(); 
			  }
		}

		return home;
	}

	public BillingSummaryPage navigateTopreviousEbillfromBBAccount(MyTmoData myTmoData) throws Exception {
		navigateToHomePage(myTmoData);
		EOSCommonLib ecl = new EOSCommonLib();
		String[] getjwt = ecl.getauthorizationandregisterinapigee(myTmoData.getLoginEmailOrPhone(),
				myTmoData.getLoginPassword());
		EOSBillingFilters billing = new EOSBillingFilters();
		Response billlistrespons = billing.getResponsebillList(getjwt);
		String statementID = billing.getebillstatemenid(billlistrespons);
		if (statementID == null)
			Assert.fail("No ebill is identified for given data");
		String newurl = System.getProperty("environment") + "/billing/summary.html?statementId=" + statementID + "&";
		getDriver().get(newurl);
		BillingSummaryPage billSummaryPage = new BillingSummaryPage(getDriver());
		billSummaryPage.verifyAjaxSpinnerInvisible();
		billSummaryPage.verifyPageLoaded();
		return billSummaryPage;

	}

	public BillingSummaryPage TopreviousEbillfromBBAccount(MyTmoData myTmoData) throws Exception {
		
		EOSCommonLib ecl = new EOSCommonLib();
		String[] getjwt = ecl.getauthorizationandregisterinapigee(myTmoData.getLoginEmailOrPhone(),
				myTmoData.getLoginPassword());
		EOSBillingFilters billing = new EOSBillingFilters();
		Response billlistrespons = billing.getResponsebillList(getjwt);
		String statementID = billing.getebillstatemenid(billlistrespons);
		if (statementID == null)
			Assert.fail("No ebill is identified for given data");
		String newurl = System.getProperty("environment") + "/billing/summary.html?statementId=" + statementID + "&";
		getDriver().get(newurl);
		BillingSummaryPage billSummaryPage = new BillingSummaryPage(getDriver());
		billSummaryPage.verifyAjaxSpinnerInvisible();
		billSummaryPage.verifyPageLoaded();
		return billSummaryPage;

	}
	
	
	
	
	/**
	 * Navigate To New Payment Arrangement page directly from Home page by clicking
	 * on Setup Payment Arrangement link
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected NewPaymentArrangementPage navigateToNewPaymentArrangementPage(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		homePage.clickSetupPaymentArrangementLink();
		NewPaymentArrangementPage paymentArrangementPage = new NewPaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
		return paymentArrangementPage;
	}

	/**
	 * Navigate To New Payment Arrangement page via OTP
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected NewPaymentArrangementPage navigateToNewPAPageViaOTP(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		homePage.clickPayBillbutton();
		OneTimePaymentPage oneTimePaymentPage = new OneTimePaymentPage(getDriver());
		oneTimePaymentPage.verifyPageLoaded();
		NewPaymentArrangementPage paymentArrangementPage = new NewPaymentArrangementPage(getDriver());

		if (oneTimePaymentPage.verifyReviewPAModalIsDisplayedOrNot()) {
			oneTimePaymentPage.clickReviewPABtnOnModal();
		} else if (oneTimePaymentPage.verifyPaymentArrangementLinkDisplayed()) {
			oneTimePaymentPage.clickpaymentArrangement();
		} 
		paymentArrangementPage.verifyPageLoaded();
		return paymentArrangementPage;
	}

	/**
	 * Login To MyTmo, Navigate to PA page through billing for Unsecure PA scenarios
	 * 
	 * @param myTmoData
	 */
	protected NewPaymentArrangementPage navigateToNewPAPageViaBilling(MyTmoData myTmoData) {
		HomePage homePage = navigateToHomePage(myTmoData);
		homePage.verifyPageLoaded();
		homePage.clickBillingLink();
		waitForPageLoad(getDriver());
		CommonPage commonPage = new CommonPage(getDriver());
		commonPage.waitforSpinner();
		commonPage.checkPageIsReady();
		if (getDriver().getCurrentUrl().contains("/billandpay/")) {
			BillAndPaySummaryPage billAndPaySummaryPage = new BillAndPaySummaryPage(getDriver());
			billAndPaySummaryPage.verifyPageLoaded();
			billAndPaySummaryPage.clickPaymentArrangementShortcut();
		} else {
			BillingSummaryPage billingSummaryPage = new BillingSummaryPage(getDriver());
			billingSummaryPage.verifyPageLoaded();
			billingSummaryPage.clickPAArrow();
		}
		NewPaymentArrangementPage paymentArrangementPage = new NewPaymentArrangementPage(getDriver());
		paymentArrangementPage.verifyPageLoaded();
		return paymentArrangementPage;
	}
	
	/**
	 * Navigate To New Payment Arrangement page directly from Home page by clicking
	 * on Setup Payment Arrangement link
	 * 
	 * @param myTmoData
	 * @return
	 */
	protected PaymentCollectionPage navigateToPaymentcollectionPage(MyTmoData myTmoData) {
		OneTimePaymentPage oneTimePaymentPage = navigateToOTPpage(myTmoData);
		oneTimePaymentPage.clickPaymentMethodBlade();
		PaymentCollectionPage pcp=new PaymentCollectionPage(getDriver());
		pcp.verifyPageLoaded();
		return pcp;
	}
	
	protected NewAddBankPage navigateToNewAddBAnkPage(MyTmoData myTmoData) {
		PaymentCollectionPage pcp=navigateToPaymentcollectionPage(myTmoData);
		pcp.clickAddBank();
		NewAddBankPage addnewbank=new NewAddBankPage(getDriver());
		return addnewbank;
	}

	protected NewAddCardPage navigateToNewAddCArdPage(MyTmoData myTmoData) {
		PaymentCollectionPage pcp=navigateToPaymentcollectionPage(myTmoData);
		pcp.clickAddCard();
		NewAddCardPage addnewcard=new NewAddCardPage(getDriver());
		return addnewcard;
	}
	

}