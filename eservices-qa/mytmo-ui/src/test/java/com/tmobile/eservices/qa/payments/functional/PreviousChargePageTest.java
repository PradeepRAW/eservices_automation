package com.tmobile.eservices.qa.payments.functional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.tmobile.eqm.testfrwk.ui.core.data.ControlTestData;
import com.tmobile.eservices.qa.commonlib.Group;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.payments.BillAndPaySummaryPage;
import com.tmobile.eservices.qa.pages.payments.BillingSummaryPage;
import com.tmobile.eservices.qa.pages.payments.PreviousChargePage;
import com.tmobile.eservices.qa.payments.PaymentCommonLib;

public class PreviousChargePageTest extends PaymentCommonLib{
	
    private static final Logger logger = LoggerFactory.getLogger(PreviousChargePageTest.class);

    @Test(dataProvider = "byColumnName", enabled = true, groups = { Group.PAYMENTS, Group.DESKTOP })
        public void testPreviousChargePageAccountBalance(ControlTestData data, MyTmoData myTmoData) {
    		logger.info("testPreviousChargePageAccountBalance method called in PreviousChargePageTest");
            Reporter.log("Test Case : Test Brite Bill Charge Category Show More Details");
            Reporter.log("Data Condition - Regular account - Brite Bill");
            Reporter.log("================================");
            Reporter.log("Test Steps | Expected Results:");
            Reporter.log("1: Launch Application | Application should be launched");
            Reporter.log("2. Login to the application | User Should be login successfully");
            Reporter.log("3. Verify Home page | Home page should be displayed");
            Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
            Reporter.log("5: click 'View By line' | Should be able to select the View by line");
            Reporter.log("6: Click on Balance option | Previous charge page should be loaded.");
            Reporter.log("7: Check the Account Balance | Account Balance should be seen.");
            Reporter.log("8. Check the total Amount | Total amount should be displayed");
            Reporter.log("================================");
            Reporter.log("Actual Result:");
            
            PreviousChargePage previouschargePage = navigateToPreviousChargePage(myTmoData,"Balance");
            previouschargePage.verifyBalanceInSlider();
            previouschargePage.verifyTotalAmountLabel();
        }

        @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT})
        public void testPreviousChargePageUnpaidAccountBalance(ControlTestData data, MyTmoData myTmoData) {
    		logger.info("testPreviousChargePageUnpaidAccountBalance method called in PreviousChargePageTest");
            Reporter.log("Test Case : Test Brite Bill Charge Category Show More Details");
            Reporter.log("Data Condition - Regular account - Brite Bill");
            Reporter.log("================================");
            Reporter.log("Test Steps | Expected Results:");
            Reporter.log("1: Launch Application | Application should be launched");
            Reporter.log("2. Login to the application | User Should be login successfully");
            Reporter.log("3. Verify Home page | Home page should be displayed");
            Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
            Reporter.log("5: click 'View By line' | Should be able to select the View by line");
            Reporter.log("6: Click on UnpaidBalance option | Previous charge page should be loaded.");
            Reporter.log("7: Check the Account Balance | Account Balance should be seen.");
            Reporter.log("8. Check the total Amount | Total amount should be displayed");
            Reporter.log("================================");
            Reporter.log("Actual Result:");

            PreviousChargePage previouschargePage = navigateToPreviousChargePage(myTmoData,"Unpaid Balance");
            previouschargePage.verifyBalanceInSlider();
            previouschargePage.verifyTotalAmountLabel();
        }

        @Test(dataProvider = "byColumnName", enabled = true, groups = {Group.SPRINT})
        public void testEbillFlowFromBriteBill(ControlTestData data, MyTmoData myTmoData) {
    		logger.info("testEbillFlowFromBriteBill method called in PreviousChargePageTest");
            Reporter.log("Test Case : Verify Ebill summary page from Brite - Historical Bills");
            Reporter.log("Data Condition - Regular account - Brite Bill");
            Reporter.log("================================");
            Reporter.log("Test Steps | Expected Results:");
            Reporter.log("1: Launch Application | Application should be launched");
            Reporter.log("2. Login to the application | User Should be login successfully");
            Reporter.log("3. Verify Home page | Home page should be displayed");
            Reporter.log("4: Click on Billing link | Brite Bill page should be displayed");
    		Reporter.log("5: Click on view Historical Bills Link | Select Billing Period as Previous Year(i.e 2017)");
    		Reporter.log("6: Verify Billing Summary Page | Billing Summary Page Should be displayed");
            Reporter.log("================================");
            Reporter.log("Actual Result:");

            BillAndPaySummaryPage billAndPaySummaryPage = navigateToBillAndPaySummaryPage(myTmoData);
            billAndPaySummaryPage.clickPreviousBillCycle("Jun", "2017");
            BillingSummaryPage billSummaryPage = new BillingSummaryPage(getDriver());
            billSummaryPage.verifyPageLoaded();
        }

    }


