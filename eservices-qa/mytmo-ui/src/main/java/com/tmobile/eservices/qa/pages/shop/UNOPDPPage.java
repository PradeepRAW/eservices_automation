package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

public class UNOPDPPage extends CommonPage {
	
	@FindBy(css = "p[class*='text-legal ng-star-inserted']")
	private WebElement legalText;
	
	@FindBy(css = "	div.text-legal")
	private WebElement legalTextFooter;

	@FindBy(css = "div[class*='full-price']  span")
	private List<WebElement> deviceFRPPrice;
	
	@FindBy(css = "a[class*='text-small promo-link']")
	private WebElement promotionText;
	
	@FindBy(css = "img[class*='product-image']")
	private WebElement deviceImg;
	
	@FindBy(xpath = "//span[contains(text(), 'Upgrade')] | //span[contains(text(), 'Add to Cart')]")
	private WebElement upgradeCTA;	
	
	@FindBy(xpath = "//span[contains(text(), 'Add to a new line')]")
	private WebElement addaLineCTA;	
	
	@FindBy(css = "div[class*='finance-prices'] span[class='fx-column'] h2")
	private List<WebElement> eipMonthlyPrice;
	
	@FindBy(css = "backButton")
	private WebElement backButton;
	
	@FindBy(css = "div.fx-row-wrap.ng-star-inserted img")
	private List<WebElement> selectColor;
	
	@FindBy(xpath = "//tmo-sku-picker//h5[contains(.,'Color')]")
	private WebElement deviceColor;
	
	@FindBy(css = "div#idPdpMemoryVariant div.dropdown")
	private WebElement memoryDropdown;
	
	@FindBy(css = "button.memory-option span")
	private List<WebElement> memoryDropdownOptions;
	
	@FindBy(css = "button.memory-option[aria-label*='Currently selected memory']")
	private WebElement deviceMemory;
	
	@FindBy(css = "span[class*='legal-verbiage-text'] span[ng-bind*='offerDollars']")
	private WebElement deviceFRPPriceDollers;
	
	@FindBy(css = "span.legal-verbiage-text span[ng-bind*='offerCents']")
	private WebElement deviceFRPPriceCents;
	
	@FindBy(css = "[data-analytics-value='Reviews']")
	private WebElement reviewsTab;
	
	@FindBy(css = "div[class*='full-price-toggle-container'], div[class*='instant-discount-price']")
	private WebElement accessoryFRPPrice;
	
	@FindBy(css ="span.text-strike-through")
	private List<WebElement> strikeOutPrice;

	public UNOPDPPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verify UNO PLP Page
	 *
	 * @return
	 */
	public UNOPDPPage verifyPageLoaded() {
		waitforSpinner();
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.urlContains("cell-phone/"));
			Reporter.log("UNOPDP page is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to load UNOPDP Page");
		}
		return this;
	}
	
	
	/**
	 * Verify Legal Text
	 * 
	 * @return
	 */
	public UNOPDPPage verifyLegalText() {
		try {			
			Assert.assertTrue(legalText.getText()
					.contains("If you cancel wireless service, remaining balance on device becomes due"));
			Reporter.log("Legal text is displayed");
		} catch (Exception e) {
			Assert.fail("Legal text is not displayed");
		}
		return this;
	}
	
	/**
	 * Verify Legal Text
	 * 
	 * @return
	 */
	public UNOPDPPage verifyLegalTextFooter() {
		try {			
			Assert.assertTrue(isElementDisplayed(legalTextFooter),
					"Legal text is not diplayed on main PDP Page footer.");
			Reporter.log("Footer Legal text is displayed In PDP Page");
		} catch (Exception e) {
			Assert.fail("Footer Legal text is not displayed");
		}
		return this;
	}
	
	/**
	 * Click bannner
	 */
	public UNOPDPPage verifyBanner() {		
		try {
			
		} catch (Exception e) {
			
		}
		return this;
	}
	
	/**
	 * Get total Device FRP on PDP
	 * 
	 * @return
	 */
	public String getDeviceFRP() {
		String frp = "";
		try {
			frp = deviceFRPPrice.get(1).getText().replace("$", "");			
			Reporter.log(" Got total Device FRP on PDP");
		} catch (Exception e) {
			Assert.fail("Failed to get total Device FRP on PDP");
		}
		return frp;
	}
	
	/**
	 * Compare Two Values does not match
	 *
	 */
	public UNOPDPPage compareTwoStringValues(String firstValue, String secondValue) {
		try {
			Assert.assertEquals(firstValue,secondValue, "The prices are same" );	
			Reporter.log("Prices are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare 2 prices");
		}
		return this;
	}
	
	/**
	 * Verify that Device Price are equal
	 */
	public UNOPDPPage compareDeviceFRPPrice(String frpPriceFromUNOPDP, String frpPriceFromUNOPLP) {
		try {
			Assert.assertEquals(frpPriceFromUNOPDP, frpPriceFromUNOPLP, " Device Price are not equal");
			Reporter.log("Device Pric  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Device Price");
		}
		return this;
	}
	
	/**
	 * Verify Device Promotion text
	 */
	public UNOPDPPage verifyPromotionText() {
		try {
			Assert.assertTrue(promotionText.isDisplayed(), "Failed to display devices promotion text");
			Reporter.log("Promotion text is Displayed for devices");
		} catch (Exception e) {
			Assert.fail("Failed to display devices promotion text");
		}
		return this;
	}
	
	/**
	 * Verify device Image in PDP
	 *
	 */
	public UNOPDPPage verifyDeviceImageAtPDP() {

		try {
			Assert.assertFalse(deviceImg.getAttribute("src").isEmpty(),"Device Image is not found at PDP");
			Reporter.log("Device Image is displayed at PDP");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Image in PDP");
		}
		return this;
	}
	
	/**
	 * verify Upgrade button is enabled
	 */
	public UNOPDPPage verifyUpGradeCTAEnabled() {
		try {
			checkPageIsReady();
			moveToElement(upgradeCTA);
			Assert.assertTrue(upgradeCTA.isEnabled(), "upgrade cta is enabled");
			Reporter.log("upgrade cta not enabled");
		} catch (Exception e) {
			Assert.fail("upgrade cta is enabled");
		}
		return this;
	}
	
	/**
	 * Verify add a line button is enabled
	 */
	public UNOPDPPage verifyAddaLineCTAEnabled() {
		try {
			checkPageIsReady();
			moveToElement(addaLineCTA);
			Assert.assertTrue(addaLineCTA.isEnabled(), "add a Line button is disabled");
			Reporter.log("add a Line button is enabled");
		} catch (Exception e) {
			Assert.fail("add a Line button is disabled");
		}
		return this;
	}
	
	/**
	 * Verify Device FRPPrice
	 * 
	 * @return
	 */
	public UNOPDPPage verifyDeviceFRPPrice() {
		try {
			Assert.assertTrue(deviceFRPPrice.get(1).isDisplayed());
			Reporter.log("Device FRP is displayed");
		} catch (Exception e) {
			Assert.fail("Device FRP Price is not displayed");
		}
		return this;
	}
	
	/**
	 * Verify Device EIP Price
	 *
	 * @return
	 */
	public UNOPDPPage verifyDeviceEIPPrice() {
		try {
			waitforSpinner();
			for (WebElement eipMonthlyPrices : eipMonthlyPrice) {
				Assert.assertTrue(eipMonthlyPrices.isDisplayed(), "EIP Monthly price is not displayed");
			}
			Reporter.log("EIP Price is displayed for the devices");
		} catch (Exception e) {
			Assert.fail("Failed to display EIP Price for the devices");
		}
		return this;
	}
	
	/**
	 * Click on Back Button
	 */
	public UNOPDPPage clickBackButton() {
		try {
			clickElementWithJavaScript(backButton);
			Reporter.log("Clicked on Back Button of Line Selector Details Page");
		} catch (Exception e) {
			Assert.fail("Failed to click on Back Button.");
		}
		return this;
	}
	
	/**
	 * Click on Add to Cart CTA button
	 */
	public UNOPDPPage clickOnAddToCartCTA() {
		try {
			checkPageIsReady();
			moveToElement(upgradeCTA);
			clickElementWithJavaScript(upgradeCTA);
			Reporter.log("Add to cart button is clickable");
		} catch (Exception e) {
			Assert.fail("Failed to display Add to cart cta button");
		}
		return this;
	}
	
	/**
	 * Click Add A Line Button
	 * 
	 * @return
	 */
	public UNOPDPPage clickOnAddALineButton() {
		try {
			waitforSpinner();						
			clickElementWithJavaScript(addaLineCTA);
			Reporter.log("Add a Line button is clickable");
		} catch (Exception e) {
			Assert.fail("Add a line button is not displayed");
		}
		return this;
	}
	
	/**
	 * Select the color based on data
	 * 
	 * @param color
	 * @return
	 */
	public UNOPDPPage chooseColor(String color) {
		try {
			if (selectColor.size() > 4) {
				selectColor.get(0).click();
				waitforSpinner();
				getDriver().findElement(By.xpath("//button/img[@title='" + color + "']")).click();
				Reporter.log("Device Color " + color + " is selected");
			} else {
				getDriver().findElement(By.xpath("//button/img[@title='" + color + "']")).click();
				Reporter.log("Device Color " + color + " is selected");
			}
		} catch (Exception e) {
			Assert.fail("Unable to select the respective color");
		}
		return this;
	}
	
	/**
	 * getDeviceColor
	 */
	public String getDeviceColor() {
		return deviceColor.getText();
	}
	
	/**
	 * Select the memory variant
	 * 
	 * @param memoryValue
	 */
	public void selectMemoryOnUNOPDP(String memoryValue) {
		try {
			for (WebElement webElement : memoryDropdownOptions) {
				if (memoryValue.equalsIgnoreCase(webElement.getText())) {
					webElement.click();
					Reporter.log("Memory variant: " +memoryValue+ "is selected");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Memory variant is not clickable");
		}
	}
	
	/**
	 * getDeviceMemory
	 */
	public String getDeviceMemory() {
		return deviceMemory.getText();
	}
	
	/**
	 * Get New FRP price
	 */
	public String getFRPPrice() {
		String FRPamountDollars = deviceFRPPriceDollers.getText();
		String FRPamountCents = deviceFRPPriceCents.getText();
		String TotlaFRP = FRPamountDollars + "." + FRPamountCents;
		return TotlaFRP;
	}
	
	/**
	 * Click Upgrade button
	 */
	public UNOPDPPage ClickOnUpGradeCTA() {
		try {
			checkPageIsReady();
			waitforSpinner();
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(reviewsTab);
				upgradeCTA.click();
				Reporter.log("upgrade cta is clickable");
			} else {
				upgradeCTA.click();
				Reporter.log("upgrade cta is clickable");
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on upgrade cta");
		}
		return this;
	}
	
	/**
	 * Verify Accessory FRP Less Than $49
	 * 
	 * @return
	 */
	public UNOPDPPage verifyAccessoryFRPLessThan49() {
		try {
			checkPageIsReady();
			Double accessoryFRP = getAccessoryFRPPRiceInPDPValue();
			Assert.assertTrue(accessoryFRP < 49.00, "Accessory FRP Not less than $49");
			Reporter.log("Accessory FRP is less than $49");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessory FRP is less than $49");
		}
		return this;
	}
	
	/**
	 * Get FRP Price of Accessory
	 * 
	 * @return
	 */
	public Double getAccessoryFRPPRiceInPDPValue() {
		String[] frpPrice;
		Double price;
		waitFor(ExpectedConditions.visibilityOf(accessoryFRPPrice), 5);
		frpPrice = accessoryFRPPrice.getText().split(" ");
		String[] frpPrice1 = frpPrice[2].split("\n");
		if (!strikeOutPrice.isEmpty()) {
			price = Double.parseDouble(frpPrice1[2].replace("$", ""));
		} else {
			price = Double.parseDouble(frpPrice1[1].replace("$", ""));
		}
		return price;

	}

	
}
