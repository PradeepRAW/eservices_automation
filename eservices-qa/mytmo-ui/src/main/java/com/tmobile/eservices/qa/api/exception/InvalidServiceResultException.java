package com.tmobile.eservices.qa.api.exception;

public class InvalidServiceResultException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidServiceResultException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }	

}
