package com.tmobile.eservices.qa.pages;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.paulhammant.ngwebdriver.ByAngular;
import com.paulhammant.ngwebdriver.NgWebDriver;

/**
 * 
 * @author pshiva
 *
 */
public class NGCommonPage extends CommonPage {
	private NgWebDriver ngWebDriver = null;

	/**
	 * 
	 * @param webDriver
	 */
	public NGCommonPage(WebDriver webDriver) {
		super(webDriver);
		ngWebDriver = new NgWebDriver((JavascriptExecutor) webDriver);
	}

	/**
	 * find my angular model
	 * 
	 * @param modelName
	 * @param data
	 */
	public void findByAngularModel(String modelName, String data) {
		ngWebDriver.waitForAngularRequestsToFinish();
		WebElement webElement = getDriver().findElement(ByAngular.model(modelName));
		webElement.clear();
		webElement.sendKeys(data);

	}

	/**
	 * find all by angular options
	 * 
	 * @param options
	 * @return
	 */
	public List<WebElement> findAllByAngularOptions(String options) {
		ngWebDriver.waitForAngularRequestsToFinish();
		return getDriver().findElements(ByAngular.options(options));
	}

	/**
	 * find by angular button text
	 * @param buttonText
	 */
	public void findByAngularButtonText(String buttonText) {
		ngWebDriver.waitForAngularRequestsToFinish();
		getDriver().findElement(ByAngular.buttonText(buttonText)).click();
	}

	/**
	 * find by angular partial button text
	 * @param buttonText
	 */
	public void findByAngularPartialButtonText(String buttonText) {
		ngWebDriver.waitForAngularRequestsToFinish();
		getDriver().findElement(ByAngular.partialButtonText(buttonText)).click();
	}

	/**
	 * find by angular CSS containing button text
	 * @param cssSelector
	 * @param searchText
	 * @return
	 */
	public List<WebElement> findByAngularCssContainingText(String cssSelector, String searchText) {
		ngWebDriver.waitForAngularRequestsToFinish();
		return getDriver().findElements(ByAngular.cssContainingText(cssSelector, searchText));
	}

}
