/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author csudheer
 *
 */
public class CrazyLegsBenefitsPage extends CommonPage {

	// Headers & Body content
	@FindBy(css = "p[class*='body-copy-description']")
	private WebElement messageText;

	@FindBy(css = "span.body")
	private List<WebElement> bodyDescription;

	@FindAll({ @FindBy(css = "p.body-copy-description"), @FindBy(css = "span.body") })
	private List<WebElement> netflixRegisteredContent;

	@FindBy(css = "span.Display4")
	private List<WebElement> netflixRegisteredContentTitle;

	@FindBy(css = "button[ng-click*='onClickButton']")
	private WebElement addNetflixToMyAccountBtn;

	@FindBy(xpath = "//span[contains(text(),'Contact Care')]")
	private WebElement contactcare;

	@FindBy(xpath = "//span[contains(text(),'Make a payment')]")
	private WebElement makePayment;

	@FindBy(css = "img[src*='tmo-logo']")
	private WebElement tMobileId;

	@FindBy(css = "[ng-bind*='benefitsContentData']")
	private WebElement signUpNetflixbtn;

	@FindBy(xpath = "//*[contains(text(), 'recover it here')]")
	private WebElement recoveritHerelink;

	@FindBy(css = "span.Display4")
	private WebElement headerMessage;

	@FindBy(css = "div.legal.padding-top-large")
	private WebElement checkLegalease;

	// netflix mail id

	@FindBy(css = "span[class*='PrimaryCTA']")
	private WebElement setUpNetflixOnMyPlanBtn;

	@FindBy(css = "img[class='modal-img']")
	private WebElement tMobileOneNetflixImgmobile;

	// @FindAll({ @FindBy(css = "div.hidden-xs.text-center.ng-binding"),
	// @FindBy(css = "div[ng-bind-html*='crazyLegNetflixImgUrl']") })

	@FindBy(css = "div.text-center.padding-top-small img")
	private WebElement tMobileOneNetflixImg;

	@FindBy(css = "span[class*='PrimaryCTA']")
	private WebElement goHomeBtn;

	@FindBy(xpath = "//span[text()='Contact Care']")
	private WebElement contactCareBtn;

	@FindBy(css = "span[class*='btn PrimaryCTA-flat-Normal-sm PrimaryCTA-flat-Normal-xs ng-binding']")
	private WebElement signuptext;

	@FindBy(css = "")
	private WebElement benefitsPageAngular;

	//private final String pageUrl = "/plan-benefits";
	
	private final String pageUrl = "benefit-redemption";

	@FindBy(css = "div[class='row'] span")
	private List<WebElement> headersMessages;

	@FindBy(xpath = "//button[text()='Contact care']")
	private WebElement contactCareButton;

	@FindBy(css = "div.legal.padding-top-large span")
	private WebElement checkLegaleasee;

	public CrazyLegsBenefitsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify message text
	 * 
	 * @return
	 */
	public CrazyLegsBenefitsPage verifyBenifitsPage() {
		try {
			checkPageIsReady();
			waitforSpinner();
			verifyPageLoaded();
			Reporter.log("Benefits page is displayed");
		} catch (Exception e) {
			Assert.fail("Benefits page is not displayed");
		}
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public CrazyLegsBenefitsPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			waitFor(ExpectedConditions
					.javaScriptThrowsNoExceptions("return (document.readyState == 'complete' && jQuery.active == 0)"));
			/*
			 * waitFor(ExpectedConditions.invisibilityOfElementLocated(
			 * By.cssSelector(
			 * "i[class*='fa fa-spin fa-spinner interceptor-spinner']")));
			 * waitFor(ExpectedConditions.invisibilityOfElementLocated(
			 * By.cssSelector("i[class*='spinner']")));
			 */

		} catch (Exception e) {
			Assert.fail("Benefits page is not displayed");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public CrazyLegsBenefitsPage verifyPageUrl() {
		try {
			checkPageIsReady();

			waitFor(ExpectedConditions.urlContains(pageUrl));
		} catch (Exception e) {
			Assert.fail("Crazy Legs Benefits page is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage verifyContactCarebutton() {
		try {
			contactcare.click();
			Reporter.log("Clicked on contact care button");
		} catch (Exception e) {
			Assert.fail("Not Clicked on contact care button");
		}
		return this;
	}

	public CrazyLegsBenefitsPage verifyMakeAPaymentButton() {
		try {
			checkPageIsReady();
			makePayment.click();
			Reporter.log("Clicked on make payment button");
		} catch (Exception e) {
			Assert.fail("Not clicked on make payment button");
		}
		return this;
	}

	public CrazyLegsBenefitsPage verifySignUpMyNetflixAccount() {
		try {
			checkPageIsReady();
			Assert.assertTrue(signUpNetflixbtn.isDisplayed());
			Reporter.log("Sign up Netflix To My Account Button  is Displayed");
		} catch (Exception e) {
			Assert.fail("Sign up Netflix To My Account button  not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage clickSignUpMyNetflixAccount() {
		try {

			signUpNetflixbtn.click();
			Reporter.log("Clicked on Set-up my  Netflix To My Account Button");
		} catch (Exception e) {
			Assert.fail("Not clicked on Set-up my  Netflix To My Account Button");
		}
		return this;
	}

	public CrazyLegsBenefitsPage clickRecoveritHerelink() {
		try {
			recoveritHerelink.click();
			Reporter.log("Clicked on Recover it here link");
		} catch (Exception e) {
			Assert.fail("Not clicked on Recover it here link");
		}
		return this;
	}

	public CrazyLegsBenefitsPage businessCustomerheaderMessage(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(headerMessage.getText().contains(msg));
			Reporter.log("" + msg + "is displayed");
		} catch (Exception e) {
			Assert.fail("" + msg + "is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage unAuthorizedheaderMessage(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(headerMessage.getText().contains(msg));
			Reporter.log("" + msg + " is displayed");
		} catch (Exception e) {
			Assert.fail("" + msg + " is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage businessUserbodyDescription(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(bodyDescription.get(0).getText().contains(msg));
			Reporter.log("" + msg + " is displayed");
		} catch (Exception e) {
			Assert.fail("" + msg + " is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage suspendedUserbodyDescription(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(bodyDescription.get(0).getText().contains(msg));
			Reporter.log("" + msg + " is displayed");
		} catch (Exception e) {
			Assert.fail("" + msg + " is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage unAuthorizedbodyDescription(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(bodyDescription.get(0).getText().contains(msg));
			Reporter.log("" + msg + "is displayed");
		} catch (Exception e) {
			Assert.fail("" + msg + " is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage checkLegalease(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(checkLegalease.getText().contains(msg));
			Reporter.log("Legalese Terms is Displayed");
		} catch (Exception e) {
			Assert.fail("Legalese terms is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage verifyNetflixPage() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				String currentUrl = getDriver().getCurrentUrl();
				Assert.assertTrue(currentUrl.contains("netflix"), " Netflix page is not displayed");
			} else {
				switchToSecondWindow();
				String currentUrl = getDriver().getCurrentUrl();
				Assert.assertTrue(currentUrl.contains("netflix"), "Netflix page is not displayed");
			}
			Reporter.log("Netflix page is displayed");
		} catch (Exception e) {
			Assert.fail("Netflix page is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage clickOnSetUpNetFlixBtn() {
		try {
			verifyPageLoaded();
			checkPageIsReady();
			setUpNetflixOnMyPlanBtn.click();
			Reporter.log("Clicked on set up netflix on my plan");
		} catch (Exception e) {
			Assert.fail("Not clicked on set up netflix on my plan");
		}
		return this;
	}

	public CrazyLegsBenefitsPage verifyThrowingInNetflixText(String msg) {
		checkPageIsReady();
		try {
			Assert.assertTrue(verifyListOfElementsBytext(netflixRegisteredContentTitle, msg));
			Reporter.log("" + msg + "text is  displayed");
		} catch (Exception e) {
			Assert.fail("" + msg + "text is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage verifyNetflixSubscriptionText(String msg) {
		try {
			Assert.assertTrue(verifyListOfElementsBytext(netflixRegisteredContent, msg),
					"" + msg + " text is not displayed");
			Reporter.log("" + msg + " text is displayed");
		} catch (Exception e) {
			Assert.fail("" + msg + " text is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage verifyAddToYourAccountText(String msg) {
		try {
			Assert.assertTrue(verifyListOfElementsBytext(netflixRegisteredContent, msg));
			Reporter.log("" + msg + " Text is  displayed");
		}

		catch (Exception e) {
			Assert.fail("" + msg + " Text is not displayed");
		}
		return this;

	}

	public CrazyLegsBenefitsPage verifytextnotpresent(String msg) {
		checkPageIsReady();
		try {
			Assert.assertFalse(verifyListOfElementsBytext(netflixRegisteredContent, msg));
			Reporter.log("" + msg + " Text is not  displayed");
		}

		catch (Exception e) {
			Assert.fail("" + msg + " Text is  displayed");
		}
		return this;

	}

	public CrazyLegsBenefitsPage verifySignUpWithNetflixText(String msg) {
		checkPageIsReady();
		try {
			Assert.assertTrue(verifyListOfElementsBytext(netflixRegisteredContent, msg));
			Reporter.log("" + msg + " Text is displayed");
		} catch (Exception e) {
			Assert.fail("" + msg + " Text is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage verifyTMobileOneNetflixImg() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(tMobileOneNetflixImgmobile.isDisplayed(), "Netflix Image is not displayed");
			} else {
				Assert.assertTrue(tMobileOneNetflixImg.isDisplayed(), "Netflix image is not displayed");
			}
			Reporter.log("Netflix image is displayed");
		} catch (Exception e) {
			Assert.fail("Netflix image is not displayed");
		}
		return this;
	}



	public CrazyLegsBenefitsPage verifyContactUsBtn() {
		try {
			checkPageIsReady();
			Assert.assertTrue(contactCareBtn.isDisplayed(), "Contact care button is not displayed");
			Reporter.log("Contact care button is displayed");
		} catch (Exception e) {
			Assert.fail("Contact care button is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage clickOnContactUsBtn() {
		try {
			contactCareBtn.click();
			Reporter.log("Contact care button is clicked");
		} catch (Exception e) {
			Assert.fail("Contact care button is not clicked");
		}
		return this;
	}

	// Angular 5 upgrade

	public CrazyLegsBenefitsPage inEligibleRatePlanHeaders(String msg) {
		try {
			checkPageIsReady();

			Assert.assertTrue(verifyElementBytext(headersMessages, msg), "" + msg + " is not displayed");
			Reporter.log("" + msg + " is displayed");
		} catch (Exception e) {
			Assert.fail("" + msg + " is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage verifyButton(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(contactCareButton.getText().contains(msg), "" + msg + " is not displayed");
			Reporter.log("" + msg + " is displayed");
		} catch (Exception e) {
			Assert.fail("" + msg + " is not displayed");
		}
		return this;
	}

	public CrazyLegsBenefitsPage clickOnContactCareButton() {
		try {
			checkPageIsReady();
			contactCareButton.click();
			Reporter.log(" button is clicked");
		} catch (Exception e) {
			Assert.fail("button is  not clicked");
		}
		return this;
	}

	public CrazyLegsBenefitsPage checkLegaleasee(String msg) {
		try {
			checkPageIsReady();
			moveToElement(checkLegaleasee);
			Assert.assertTrue(checkLegaleasee.getText().contains(msg), "" + msg + " is not displayed");
			Reporter.log("Legalese Terms is Displayed");
		} catch (Exception e) {
			Assert.fail("Legalese terms is not displayed");
		}
		return this;
	}
	
}
