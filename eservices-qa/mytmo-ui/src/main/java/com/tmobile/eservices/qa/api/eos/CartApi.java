package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class CartApi extends ApiCommonLib{
	
	public static String accessToken;
	Map<String, String> headers;
	/***
	 * This is the API for the EOS Cart API
	 * parameters:
     *   - $ref: '#/parameters/oAuth'
     *   - $ref: '#/parameters/transactionId'
     *   - $ref: '#/parameters/correlationId'
     *   - $ref: '#/parameters/applicationId'
     *   - $ref: '#/parameters/channelId'
     *   - $ref: '#/parameters/clientId'
     *   - $ref: '#/parameters/transactionBusinessKey'
     *   - $ref: '#/parameters/transactionBusinessKeyType'
     *   - $ref: '#/parameters/transactionType'
	 * @return
	 * @throws Exception 
	 */
	public Response getCartLines(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v3/cart/listLines", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}
    
    
    public Response getCart(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v3/cart/" + tokenMap.get("cartId"), RestCallType.GET);
		System.out.println(response.asString());
		return response;
    }  
    
    public Response deleteCart(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v3/cart/" + tokenMap.get("cartId"), RestCallType.DELETE);
		System.out.println(response.asString());
		return response;
    }  
    
    public Response addToCart(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v3/cart/" + tokenMap.get("cartId"), RestCallType.PUT);
		System.out.println(response.asString());
		return response;
    }
    
    public Response removeFromCart(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v3/cart/" + tokenMap.get("cartId"), RestCallType.PUT);
		System.out.println(response.asString());
		return response;
    }
    
    public Response updateCart(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v3/cart/" + tokenMap.get("cartId"), RestCallType.PUT);
		System.out.println(response.asString());
		return response;
    }
    
    public Response createCart(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildCartAPIHeader(apiTestData, tokenMap);
		System.out.println("base request body" + requestBody);
		RestService service = new RestService(requestBody, eos_base_url);
		System.out.println("Base Url : " + eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		System.out.println("Resource Url :" + "/v3/cart/");
		Response response = service.callService("v3/cart/", RestCallType.POST);
		System.out.println(response.asString());
		return response;
    }
    
    private Map<String, String> buildCartAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
    	headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("Authorization", checkAndGetAccessToken());
		headers.put("correlationId", createPlattoken(apiTestData));
		headers.put("applicationId", "MYTMO");
		headers.put("channelId", "web");
		headers.put("clientId", "e-serviceUI");
		headers.put("transactionBusinessKey", "BAN");
		headers.put("transactionBusinessKeyType", apiTestData.getBan());
		headers.put("transactionType", "UPGRADE");
		headers.put("dealerCode", "000000");
		headers.put("usn", "testUSN");
		headers.put("ban", apiTestData.getBan());
		headers.put("msisdn",apiTestData.getMsisdn());
		headers.put("phoneNumber", apiTestData.getMsisdn());
		headers.put("Cache-Control", "no-cache");
		headers.put("X-B3-TraceId", "ShopAPITest123");
		headers.put("X-B3-SpanId", "ShopAPITest456");
		headers.put("transactionid","ShopAPITest");
		return headers;
	}

}
