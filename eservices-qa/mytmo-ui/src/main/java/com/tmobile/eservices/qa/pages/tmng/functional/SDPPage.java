package com.tmobile.eservices.qa.pages.tmng.functional;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

import io.appium.java_client.AppiumDriver;

public class SDPPage extends TmngCommonPage {

	public SDPPage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(css = "#map")
	private WebElement storeLocatorMap;

	@FindBy(css = "button[aria-label='Zoom in']")
	private WebElement zoomInButtonOnMap;

	@FindBy(css = "span[ng-bind='vm.showHideLabel | parseHtml']")
	private WebElement showHideItemsCTAOnSDP;

	@FindBy(css = "span[ng-bind*='statusCode.statusValue']")
	private List<WebElement> itemsInStoreDisplayedOnSDP;

	@FindBy(css = "button[ng-click*='vm.close']")
	private WebElement closeCTASDP;

	@FindBy(css = "[ng-bind*='Go Back']")
	private WebElement goBackMobileView;

	@FindBy(css = "#address")
	private WebElement storeDetails;

	@FindBy(css = "div.store-locator-breadcrumb")
	private List<WebElement> breadCrumb;

	@FindBy(css = "[ng-bind='vm.availabilityMsg | parseHtml']")
	private WebElement inventoryTextSDP;

	@FindBy(css = "#storeDescription")
	private WebElement storeInfoTextOnStoreDetailsPage;

	/**
	 * Validate Store map page in Store locator list modal popup
	 *
	 */
	public SDPPage verifyStoreMapPage() {
		try {
			checkPageIsReady();
			getDriver().switchTo().frame("pccIFrame");
			Assert.assertTrue(storeLocatorMap.isDisplayed(), "Map is not displaying beside store list");
			getDriver().switchTo().parentFrame();
			Reporter.log("Map is displaying beside store list");
		} catch (Exception e) {
			Assert.fail("Failed to verify map page");
		}
		return this;
	}

	/**
	 * Click Zoom In Button on Map
	 *
	 */
	public SDPPage clickZoomInButtonOnMap() {
		try {
			waitForSpinnerInvisibility();
			getDriver().switchTo().frame("pccIFrame");
			zoomInButtonOnMap.click();
			getDriver().switchTo().parentFrame();
			Reporter.log("Map Zoom In button is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Map Zoom In button");
		}
		return this;
	}

	/**
	 * Verify Inventory Status on Store Details Modal on Cart page
	 *
	 */
	public boolean verifyOutofStockMsgOnSDP() {
		try {
			//System.out.println(inventoryTextSDP.getText());
			if (inventoryTextSDP.getText().contains("out of stock"))
				return true;
			else
				return false;
		} catch (Exception e) {
			Assert.fail("Failed to display Inventory status on SDP");
		}
		return false;
	}

	/**
	 * Verify display of Show Hide CTA on Store Details Modal on Cart page
	 *
	 */
	public SDPPage verifyShowHideCTADisplayedOnSDPCartPage() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(showHideItemsCTAOnSDP),60);
			Assert.assertTrue(isElementDisplayed(showHideItemsCTAOnSDP), "Show Hide CTA is not Displayed on SDP");
			Reporter.log("Show Hide CTA is Displayed on SDP");
		} catch (Exception e) {
			Assert.fail("Failed to display Show Hide CTA on SDP");
		}
		return this;
	}

	/**
	 * Click on Show/Hide Items for selected store
	 *
	 */
	public SDPPage clickShowHideItemsOnSDP() {
		try {
			waitForSpinnerInvisibility();
			clickElementWithJavaScript(showHideItemsCTAOnSDP);
			Reporter.log("Show Hide button clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Show Hide CTA");
		}
		return this;
	}

	/**
	 * Verify items in Store displayed on Store Details Modal on Cart page
	 *
	 */
	public SDPPage verifyItemsInStoreDisplayedOnSDPCartPage() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(itemsInStoreDisplayedOnSDP.get(0)),60);
			Assert.assertTrue(isElementDisplayed(itemsInStoreDisplayedOnSDP.get(0)),
					"Items available in store is not Displayed on SDP");
			Reporter.log("Items available in store is Displayed on SDP");

		} catch (Exception e) {
			Assert.fail("Failed to display Items available in store on SDP");
		}
		return this;
	}

	/**
	 * Verify items in Store is not displayed when clicked on Hide CTA
	 *
	 */
	public SDPPage verifyItemsInStoreNotDisplayedOnSDPCartPage() {
		try {
			waitForSpinnerInvisibility();
			Assert.assertTrue(itemsInStoreDisplayedOnSDP.isEmpty(), "Items available in store is Displayed on SDP");
			Reporter.log("Items available in store is not Displayed on SDP");
		} catch (Exception e) {
			Assert.fail("Failed to display Items available in store on SDP");
		}
		return this;
	}

	/**
	 * Click on Close CTA of SDP Modal
	 *
	 */
	public SDPPage clickSDPModalClose() {
		try {
			if(getDriver() instanceof AppiumDriver){
				clickElement(goBackMobileView);
				Reporter.log("Unable to close");
			}else{
				clickElementWithJavaScript(closeCTASDP);
				Reporter.log("Close CTA is clicked");
			}
		} catch (Exception e) {
			Assert.fail("Failed to click Close CTA");
		}
		return this;
	}

	/**
	 * verify Store details page
	 */
	public SDPPage verifyStoredetailsPage() {
		checkPageIsReady();
		try {
			if(getDriver() instanceof AppiumDriver){
				waitFor(ExpectedConditions.visibilityOf(storeDetails),30);
				Assert.assertTrue(storeDetails.isDisplayed(), "Store Details are not displayed");
				Reporter.log("Store details are displayed");
			}else{
				waitFor(ExpectedConditions.visibilityOf(storeDetails),60);
				Assert.assertTrue(storeDetails.isDisplayed(), "Store Details are not displayed");
				Reporter.log("Store details are displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify store details");
		}
		return this;
	}

	/**
	 * validate Breadcrumb is hidden/suppressed on SDP
	 *
	 */
	public SDPPage verifyBreadCrumbHidden() {
		try {
			Assert.assertEquals(breadCrumb.size(), 0, "Search Box on Map is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Breadcrumb on SDP");
		}

		return this;
	}

	/**
	 * verify inventory status
	 */
	public SDPPage verifyInventoryTextOnSDPPage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(inventoryTextSDP),60);
			Assert.assertTrue(inventoryTextSDP.isDisplayed(), "Store status text is not displayed");
			Reporter.log("Inventory status is displayed");
		} catch (Exception e) {
			Assert.fail("failed to verify inventory status");
		}
		return this;
	}

	/**
	 * Verify Store Info text On Store Details Page
	 *
	 */
	public SDPPage storeInfoTextOnStoreDetailsPage() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(storeInfoTextOnStoreDetailsPage),60);
			Assert.assertTrue(storeInfoTextOnStoreDetailsPage.isDisplayed(),"Store Info text On Store Details Page is not displayed");
			Reporter.log("Store Info text On Store Details Page is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Store Info text On Store Details Page");
		}
		return this;

	}

}