package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class PrepaidProfileLandingPage extends CommonPage {

	private static final String pageUrl = "account/profile/prepaidlanding";
	// private static final String blockingPageUrl = "blocking";

	@FindBy(xpath = "//p[@class='Display3 text-center']")
	private WebElement profilePageHeader;

	@FindBy(xpath = "//p[contains(text(),'Billing & Payments')]")
	private WebElement billingandPaymentsTab;

	@FindBy(xpath = "(//*[@u1st-aria-label='Billing & Payments']//../p[2])")
	private WebElement descriptionOfbillingandPaymentsTab;

	@FindBy(xpath = "//p[@class='Display6'][contains(text(),'T-Mobile ID')]")
	private WebElement tmobileIDTab;

	@FindBy(xpath = "(//*[@u1st-aria-label='T-Mobile ID']//../p[2])")
	private WebElement descriptionOfTmobileIDTab;

	@FindBy(xpath = "//p[contains(text(),'Line Settings')]")
	private WebElement lineSettingsTab;

	@FindBy(xpath = "(//*[@u1st-aria-label='Line Settings']//../p[2])")
	private WebElement descriptionOfLineSettingsTab;

	@FindBy(xpath = "//p[contains(text(),'Privacy & Notifications')]")
	private WebElement privacyAndNotificationsTab;

	@FindBy(xpath = "(//p[contains(text(),'Privacy & Notifications')]//../p[2])")
	private WebElement descriptionOfPrivacyAndNotificationsTab;

	@FindBy(xpath = "//p[contains(text(),'Family Controls')]")
	private WebElement familyControlTab;

	@FindBy(xpath = "(//p[contains(text(),'Family Control')]//../p[2])")
	private WebElement descriptionOfFamilyControlTab;

	@FindBy(xpath = "//p[contains(text(),'Multiple Devices')]")
	private WebElement multipleDevicesTab;

	@FindBy(xpath = "(//p[contains(text(),'Multiple Devices')]//../p[2])")
	private WebElement descriptionOfMultipleDevicesTab;

	// ##################### to be deleted

	@FindBy(css = "p[class='Display6']")
	private List<WebElement> profilePageLinks;

	@FindBy(css = "span[ng-model='selectedVal']")
	private WebElement selectedDropDownValue;

	@FindBy(xpath = "//*[contains(text(),'Blocking')]")
	private WebElement blockingBlade;

	@FindBy(xpath = "//*[@class='switch mt-1 stateEnabled' and @id='Block International Roaming']//*[@class='slider round']")
	private WebElement blockInternationalRoamingEnabled;

	@FindBy(xpath = "//*[@class='switch mt-1' and @id='Block International Roaming']//*[@class='slider round']")
	private WebElement blockInternationalRoamingDisabled;

	@FindBy(css = "div#model.dropdown-toggle")
	private WebElement lineHeader;

	@FindBys(@FindBy(css = "div#test ul li"))
	private List<WebElement> lineList;

	@FindBy(xpath = "//*[contains(text(),'Employment verification')]")
	private WebElement EmployeVerification;

	@FindBy(xpath = "//p[text()='Payment methods']")
	private WebElement PaymnetMethod;

	@FindBy(xpath = "//p[text()='Save up to 10 payment methods per line to secure for secure,easy paymnet methods ]")
	private WebElement paymentbody;

	@FindBy(xpath = "//p[text()='Permission']")
	private WebElement permissionsBlade;

	@FindBy(xpath = "//p[contains(text(),'Profile') and contains(@class,'text-center')]")
	private WebElement profileHeader;

	/*	*//**
			 * Verify Blocking page.
			 *
			 * @return the blocking Page under Profile page.
			 *//*
				 * public PrepaidProfileLandingPage verifyBlockingPage() { try {
				 * waitforSpinnerinProfilePage(); checkPageIsReady();
				 * verifyPageUrl(blockingPageUrl); Reporter.log("Blocking page is displayed"); }
				 * catch (NoSuchElementException e) {
				 * Assert.fail("Blocking page not displayed"); } return this; }
				 */
	/*
	 * Verify whether International Blocking is Enabled or not
	 * 
	 */
	public boolean verifyInternationalBlockingStatus() {
		boolean statusOfInternationalBlocking = false;
		try {
			if (blockInternationalRoamingEnabled.isDisplayed())
				statusOfInternationalBlocking = true;
		} catch (Exception e) {
			Reporter.log("Block International Roaming is disabled");
		}

		return statusOfInternationalBlocking;

	}

	

	// #####################################################################################

	public PrepaidProfileLandingPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Prepaid Profile page.
	 *
	 * @return the ProfilePage class instance.
	 */
	public PrepaidProfileLandingPage verifyPrePaidProfilePage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			isElementDisplayed(profilePageHeader);
			Reporter.log("Prepaid Profile page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Prepaid Profile page not displayed");
		}
		return this;
	}

	// Hit direct url for Prepaid Profile page
	public PrepaidProfileLandingPage directURLForPrepaidProfilePage() {
		try {
			String urls[] = getDriver().getCurrentUrl().split("home");
			String profileURL = urls[0].concat("/account/profile/prepaidlanding");
			getDriver().get(profileURL);
		} catch (Exception e) {
			Assert.fail("Failed to enter url for Profile page for Prepaid users");
		}
		return this;
	}


	// Verify Billing and Payments Tab is displayed or not
	public PrepaidProfileLandingPage checkBillingAndPaymentsTabAndDescriptionUnderIt() {
		try {
			checkPageIsReady();
			billingandPaymentsTab.isDisplayed();
			Assert.assertEquals(billingandPaymentsTab.getText().trim(), "Billing & Payments", "Text Billing & Payments is mismatched on blade on Profile page.");
			Reporter.log("Billing and Payment tab is displayed");
		} catch (Exception e) {
			Assert.fail("Billing and payments tab is not displayed");
		}
		try {
			descriptionOfbillingandPaymentsTab.isDisplayed();
			Assert.assertEquals(descriptionOfbillingandPaymentsTab.getText().trim(), "Manage billing address, AutoPay, and paperless billing", "Subtext of Billing & Payments tab is mismatched on blade on Profile page.");
			Reporter.log("Description of Billing and Payment tab is displayed");
		} catch (Exception e) {
			Assert.fail("Description of Billing and payments tab is not displayed");
		}
		return this;
	}

	// Click on Billing and Payments Tab
	public PrepaidProfileLandingPage clickBillingAndPaymentsTab() {
		try {
			checkPageIsReady();
			billingandPaymentsTab.click();
			Reporter.log("Billing and Payment tab is clicked");
		} catch (Exception e) {
			Assert.fail("Billing and payments tab not clicked");
		}
		return this;
	}

	// Verify Billing and Payments Tab is displayed or not
	public PrepaidProfileLandingPage checkTMobileIDTabAndDescriptionUnderIt() {
		try {
			checkPageIsReady();
			tmobileIDTab.isDisplayed();
			Assert.assertEquals(tmobileIDTab.getText().trim(), "T-Mobile ID", "Text on T-Mobile ID tab is mismatched on blade on Profile page.");
			Reporter.log("TMobile ID tab is displayed");
		} catch (Exception e) {
			Assert.fail("TMobile ID tab is not displayed");
		}
		try {
			descriptionOfTmobileIDTab.isDisplayed();
			Assert.assertEquals(descriptionOfTmobileIDTab.getText().trim(), "Edit your name, email, password, security questions", "Subtext of T-Mobile ID tab is mismatched on blade on Profile page.");
			Reporter.log("Description of TMobile ID tab is displayed");
		} catch (Exception e) {
			Assert.fail("Description of TMobile ID tab is not displayed");
		}
		return this;
	}

	// Click on T-Mobile ID Tab
	public PrepaidProfileLandingPage clickOnTMobileIDTab() {
		try {

			checkPageIsReady();
			tmobileIDTab.click();
			Reporter.log("T-Mobile ID tab is clicked");
		} catch (Exception e) {
			Assert.fail("T-Mobile ID tab not clicked");
		}
		return this;
	}

	// Verify Line Settings tab and description under it.
	public PrepaidProfileLandingPage checkLineSettingsTabAndDescriptionUnderIt() {
		try {
			checkPageIsReady();
			lineSettingsTab.isDisplayed();
			Assert.assertEquals(lineSettingsTab.getText().trim(), "Line Settings", "Text on Line Settings tab is mismatched on blade on Profile page.");
			Reporter.log("Line Settings tab is displayed");
		} catch (Exception e) {
			Assert.fail("Line Settings tab is not displayed");
		}
		try {
			descriptionOfLineSettingsTab.isDisplayed();
			Assert.assertEquals(descriptionOfLineSettingsTab.getText().trim(), "Edit your nickname, E911 address, usage address and site permissions", "Subtext of Line Settings tab is mismatched on blade on Profile page.");
			Reporter.log("Description of Line Settings tab is displayed");
		} catch (Exception e) {
			Assert.fail("Description of Line Settings tab is not displayed");
		}
		return this;
	}

	// Click on Line Settings Tab
	public PrepaidProfileLandingPage clickOnLineSettingsTab() {
		try {

			checkPageIsReady();
			lineSettingsTab.click();
			Reporter.log("Line Settings tab is clicked");
		} catch (Exception e) {
			Assert.fail("Line Settings tab not clicked");
		}
		return this;
	}

	// Click on Privacy & Notifications Tab
	public PrepaidProfileLandingPage clickOnPrivacyAndNotificationsTab() {
		try {
			checkPageIsReady();
			privacyAndNotificationsTab.click();
			Reporter.log("Privacy and Notifications tab is clicked");
		} catch (Exception e) {
			Assert.fail("Privacy and Notifications tab not clicked");
		}
		return this;
	}

	// Verify Family Control tab and description under it.
	public PrepaidProfileLandingPage checkPrivacyAndNotificationsTabAndDescriptionUnderIt() {
		try {
			checkPageIsReady();
			privacyAndNotificationsTab.isDisplayed();
			Assert.assertEquals(privacyAndNotificationsTab.getText().trim(), "Privacy and Notifications", "Text on Privacy and Notifications tab is mismatched on blade on Profile page.");
			Reporter.log("Privacy and Notifications tab is displayed");
		} catch (Exception e) {
			Assert.fail("Privacy and Notifications tab is not displayed");
		}
		try {
			descriptionOfPrivacyAndNotificationsTab.isDisplayed();
			Assert.assertEquals(descriptionOfPrivacyAndNotificationsTab.getText().trim(), "Manage service alerts, marketing and third-party communications and apps that use T-Mobile ID", "Subtext of Privacy and Notifications tab is mismatched on blade on Profile page.");
			Reporter.log("Description of Privacy and Notifications tab is displayed");
		} catch (Exception e) {
			Assert.fail("Description of Privacy and Notifications tab is not displayed");
		}
		return this;
	}

	// Verify Family Control tab and description under it.
	public PrepaidProfileLandingPage checkFamilyControlTabAndDescriptionUnderIt() {
		try {
			checkPageIsReady();
			familyControlTab.isDisplayed();
			Assert.assertEquals(familyControlTab.getText().trim(), "Family Controls", "Text on Family Controls tab is mismatched on blade on Profile page.");
			Reporter.log("Family Control tab is displayed");
		} catch (Exception e) {
			Assert.fail("Family Control tab is not displayed");
		}
		try {
			descriptionOfFamilyControlTab.isDisplayed();
			Assert.assertEquals(descriptionOfFamilyControlTab.getText().trim(), "Restrict access to adult content", "Subtext of Family Control tab is mismatched on blade on Profile page.");
			Reporter.log("Description of Family Control tab is displayed");
		} catch (Exception e) {
			Assert.fail("Description of Family Control tab is not displayed");
		}
		return this;
	}

	// Click on Family Control tab
	public PrepaidProfileLandingPage clickOnFamilyControlTab() {
		try {
			Thread.sleep(10000);
			checkPageIsReady();
			familyControlTab.click();
			Reporter.log("Family Control tab is clicked");
		} catch (Exception e) {
			Assert.fail("Family Control tab not clicked");
		}
		return this;
	}

	// Verify Multiple Devices tab and description under it.
	public PrepaidProfileLandingPage checkMultipleDevicesTabAndDescriptionUnderIt() {
		try {
			checkPageIsReady();
			multipleDevicesTab.isDisplayed();
			Assert.assertEquals(multipleDevicesTab.getText().trim(), "Multiple Devices", "Text on Multiple Devices tab is mismatched on blade on Profile page.");
			Reporter.log("Multiple Devices tab is displayed");
		} catch (Exception e) {
			Assert.fail("Multiple Devices tab is not displayed");
		}
		try {
			descriptionOfMultipleDevicesTab.isDisplayed();
			Assert.assertEquals(descriptionOfMultipleDevicesTab.getText().trim(), "Allows you to extend your number’s voice calling and messaging to multiple device", "Subtext of Multiple Devices tab is mismatched on blade on Profile page.");
			Reporter.log("Description of Family Control tab is displayed");
		} catch (Exception e) {
			Assert.fail("Description of Family Control tab is not displayed");
		}
		return this;
	}

	// Click on Multiple devices Tab
	public PrepaidProfileLandingPage clickOnMultipleDevicesTab() {
		try {

			checkPageIsReady();
			multipleDevicesTab.click();
			Reporter.log("Multiple Devices tab is clicked");
		} catch (Exception e) {
			Assert.fail("Multiple Devices tab not clicked");
		}
		return this;
	}

	//#################
	// Hit direct url for Profile page
	public PrepaidProfileLandingPage directURLForProfilePage() {
		try {
			String urls[] = getDriver().getCurrentUrl().split("home");
			String profileURL = urls[0].concat("/profile");
			getDriver().get(profileURL);
		} catch (Exception e) {
			Assert.fail("Failed to enter url for Profile page for Prepaid users");
		}
		return this;
	}
//#################
}
