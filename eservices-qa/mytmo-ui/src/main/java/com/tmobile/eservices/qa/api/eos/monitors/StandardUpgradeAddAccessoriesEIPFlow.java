package com.tmobile.eservices.qa.api.eos.monitors;

import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
import com.tmobile.eservices.qa.api.exception.ServiceFailureException;
import com.tmobile.eservices.qa.common.ShopConstants;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class StandardUpgradeAddAccessoriesEIPFlow extends OrderProcess {

	/*
	 * 
	 * Steps For Standard Upgrade Add Accessories EIP Flow,
	 * 1. getCatalogProducts
	 * 2. Create Cart
	 * 3. Get Accessories
	 * 4. Update Cart with Accessories
	 * 5. Create Quote
	 * 6. Update Quote
	 * 7. Make Payment
	 * 8. Place Order
	 * 
	 * */
	
	@Override
	public void createCart(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "createCartAPI_EIP_forOrder.txt");
			invokeCreateCart(apiTestData, requestBody);
		} catch (Exception e) {
			//Reporter.log("Service issue while making Create Cart : " + e);
			throwServiceFailException();
		}
	}

	@Override
	public void getAccessories(ApiTestData apiTestData) throws ServiceFailureException {
		try{
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "getAccessoryListV4ForMonitors.txt");
			String updatedRequest = prepareRequestParam(requestBody, serviceResultsMap);
			logLargeRequest(updatedRequest, "getAccessories");
			Response response = catalogApi.retrieveProductsUsingPOST(apiTestData, updatedRequest, serviceResultsMap);
			boolean isSuccessResponse = checkAndReturnResponseStatusAndLogInfo(response, "getAccessories");
			if(isSuccessResponse){
				if (isResponseEmpty(response)) {
					failAndLogResponse(response, "getAccessories");				
				}
				getTokenMapForGetAccessories(response);
			}
			}catch(Exception e){
				Reporter.log("Service issue while invoking the getAccessories : "+e);
				throwServiceFailException();
			}
				
	}
	
	private void getTokenMapForGetAccessories(Response response) throws Exception {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		boolean accessoryFound = false;
		String prodStatus = "";
		if (!jsonNode.isMissingNode() && jsonNode.has("data")) {

			JsonNode parentNode = jsonNode.path("data");
			if (!parentNode.isMissingNode() && parentNode.isArray()) {
				for (int i = 0; i < parentNode.size(); i++) {
					// JsonNode dataNode = parentNode.get(i);
					if (null != parentNode.get(i).get("productAvailability")) {
						if ("Available".equalsIgnoreCase(
								parentNode.get(i).get("productAvailability").get("availabilityStatus").asText())) {

							serviceResultsMap.put("skuAccessory", getPathVal(parentNode.get(i), "skuNumber"));
							serviceResultsMap.put("FRPAccessory", getPathVal(parentNode.get(i), "pricing.offerPrice"));
							serviceResultsMap.put("estimatedShipFromDateAccessory",
									getPathVal(parentNode.get(i), "productAvailability.estimatedShipDateFrom"));
							serviceResultsMap.put("ShipDateToAccessory",
									getPathVal(parentNode.get(i), "productAvailability.estimatedShipDateTo"));
							accessoryFound = true;
							break;
						}
					} else {
						throw new Exception(
								"productAvailability attribute in GetAccessories Response is null or empty");
					}
					prodStatus = parentNode.get(i).get("productAvailability").get("availabilityStatus").asText();
				}

				if (!accessoryFound) {
					serviceResultsMap.put("skuAccessory", "");
					serviceResultsMap.put("FRPAccessory", "");
					serviceResultsMap.put("estimatedShipFromDateAccessory", "");
					serviceResultsMap.put("ShipDateToAccessory", "");
					serviceResultsMap.put("exceptionMessage", "No Valid SKU found in AccessoryList because of " + prodStatus);
					throw new Exception("No Valid SKU with Available status was found in AccessoryList");
				}

			}
		} else {
			// failAndLogResponse(response, "getAccessories");
			throw new Exception("Empty Response from getAccessories");
		}
		
	}
	
	@Override
	public void updateCart(ApiTestData apiTestData) throws ServiceFailureException {
		try{
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_Accessories_EIP_req.txt");
			invokeUpdateCart(apiTestData, requestBody);
			}catch(Exception e){
				Reporter.log("Service issue while updating Cart with Accessories: "+e);
				throwServiceFailException();
			}
	}

}
