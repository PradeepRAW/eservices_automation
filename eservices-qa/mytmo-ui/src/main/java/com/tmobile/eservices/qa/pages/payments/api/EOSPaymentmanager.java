package com.tmobile.eservices.qa.pages.payments.api;
import java.time.Duration;
import java.time.LocalDate;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSPaymentmanager extends EOSCommonLib {






public Response getPAinformationresponse(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	
	    RestService restService = new RestService("", eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("Authorization",alist[2]);
	    restService.addHeader("msisdn", alist[0]);
	    restService.addHeader("ban", alist[3]);
	    response = restService.callService("v1/paymentmanager/paymentarrangementdetails",RestCallType.GET);
	    
	}

	    return response;
}




public String isPAAlreadysetup(Response response) {
	String pasetup=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("isPAalreadySetUp")!=null) {
        	return jsonPathEvaluator.get("isPAalreadySetUp").toString();
	}}
		
	
	return pasetup;
}


public String ispaeligible(Response response) {
	String autopayeligible=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("paymentArrangementEligibilityType.paymentArrangementStatus")!=null) {
			if(jsonPathEvaluator.get("paymentArrangementEligibilityType.paymentArrangementStatus").toString().equalsIgnoreCase("[S, S, S, S]"))
				return "true" ;
				else
				return "false";
		}
		
		if(jsonPathEvaluator.get("paymentArrangementData.paymentArrangementType")!=null) {
		
			if(jsonPathEvaluator.get("paymentArrangementData.paymentArrangementType").toString().equalsIgnoreCase("[Y, Y]"))
			return "true" ;
			else
			return "false";
	}}
		
	return autopayeligible;
}




/**********************************************************************************************************************
 * 
 * @param alist
 * @return
 * @throws Exception
 *************************************************************************************************************************/

public Response getsubscriberinforesponse(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	
	    RestService restService = new RestService("", eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("Authorization",alist[2]);
	    restService.addHeader("msisdn", alist[0]);
	    restService.addHeader("ban", alist[3]);
	    restService.addHeader("isSubcriber", "true");
	    restService.addHeader("isSedona", "");
	    restService.addHeader("isEasyPay", "");
	    response = restService.callService("v1/paymentmanager/subcriberpaymentdetails",RestCallType.GET);
	    
	}

	    return response;
}



public String isautopayeligible(Response response) {
	String autopayeligible=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("subscriberDetails.autoPayStatus.isEligibleForAutoPay")!=null) {
			if(jsonPathEvaluator.get("subscriberDetails.isSCNC").toString().equalsIgnoreCase("true")) {
				return "false";
			}
			return jsonPathEvaluator.get("subscriberDetails.autoPayStatus.isEligibleForAutoPay").toString();
	}}
		
	return autopayeligible;
}


public String autopayblackoutperiod(Response response) {
	String autopayblackout=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("subscriberDetails.autoPayStatus.isInsideBlackoutPeriod")!=null) {
		
			return jsonPathEvaluator.get("subscriberDetails.autoPayStatus.isInsideBlackoutPeriod").toString();
	}}
		
	return autopayblackout;
}



public String getcreditclass(Response response) {
	String creditclass=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("subscriberDetails.creditClass")!=null) {
		
			return jsonPathEvaluator.get("subscriberDetails.creditClass").toString();
	}}
		
	return creditclass;
}


public String isfdpeligible(Response response) {
	String fdpeligible=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("subscriberDetails.balance.pastDueAmount")!=null && jsonPathEvaluator.get("subscriberDetails.balance.dueDate")!=null) {
			String pstdue=jsonPathEvaluator.get("subscriberDetails.balance.pastDueAmount").toString();
			if(Float.parseFloat(pstdue)<=0.0) {
				if(jsonPathEvaluator.get("subscriberDetails.balance.dueDate").toString().trim().length()<1) {return "false";}
				LocalDate today = LocalDate.now();
                LocalDate duedate = LocalDate.parse(jsonPathEvaluator.get("subscriberDetails.balance.dueDate").toString());
               long duration= Duration.between(today.atStartOfDay(), duedate.atStartOfDay()).toDays();
			if(duration>0) return "true";
			else return "false";
			}else return "false";
	}}
		
	return fdpeligible;
}



/**********************************************************************************************************************
 * 
 * @param alist
 * @return
 * @throws Exception
 *************************************************************************************************************************/

public Response getsschedulepaymentresponse(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	
	    RestService restService = new RestService("", eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("Authorization",alist[2]);
	    restService.addHeader("msisdn", alist[0]);
	    restService.addHeader("ban", alist[3]);
	    response = restService.callService("v1/paymentmanager/schedulepaymentdetails",RestCallType.GET);
	    
	}

	    return response;
}


public String checkschedulepayment(Response response) {
	String paymentindicator=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("paymentIndicator")!=null) {
        	return jsonPathEvaluator.get("paymentIndicator").toString();
	}}
		
	
	return paymentindicator;
}

}