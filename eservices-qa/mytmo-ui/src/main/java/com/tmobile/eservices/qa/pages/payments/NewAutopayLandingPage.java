package com.tmobile.eservices.qa.pages.payments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

public class NewAutopayLandingPage extends CommonPage {

	private final String pageUrl = "payments/autopay";

	@FindBy(css = "p.body-bold.black.padding-bottom-small")
	private WebElement nextAutopaydate;

	@FindBy(css = "p.legal")
	private List<WebElement> subsequentPaymentDate;

	@FindBy(xpath = "//span[text()='Terms and Conditions']")
	private WebElement termsAndConditions;

	@FindBy(xpath = "//span[text()='AutoPay FAQ.']")
	private WebElement seeFaq;

	@FindBy(xpath = "//p[text()='AutoPay is OFF']")
	private List<WebElement> txtautopayOFF;

	@FindBy(xpath = "//p[text()='AutoPay is ON']")
	private List<WebElement> txtautopayON;

	@FindBy(id = "paymentIcon")
	private WebElement spriteIcon;

	@FindBy(xpath = "//p[contains(text(),'First AutoPay would be')]")
	private WebElement firstautopaytext;

	@FindBy(xpath = "//p[contains(text(),'Next AutoPay payment:')]")
	private WebElement NextAutopayment;

	@FindBy(xpath = "//span[@aria-label='bank']")
	private List<WebElement> bankdefault;

	@FindBy(xpath = "//span[@aria-label='card']")
	private List<WebElement> carddefault;

	@FindBy(xpath = "//p[contains(text(),'Your bill due')]")
	private WebElement txtBilldue;

	@FindBy(xpath = "//p[contains(text(),'Next bill due')]")
	private WebElement Nextbilldue;

	@FindBy(xpath = "//p[contains(text(),'Payments will process on')]")
	private WebElement everymonthprocesspayment;

	@FindBy(id = "acceptButton")
	private WebElement btnAgreeAndSubmit;

	@FindBy(id = "cancelButton")
	private WebElement btnBack;

	@FindBy(id = "autopayCancelModel")
	private WebElement cancelAutopayLink;

	@FindBy(xpath = "//span[contains(text(),'Are you sure you want to cancel AutoPay?')]")
	private WebElement txtcancelmodelAutopay;

	@FindBy(xpath = "//button[contains(text(),'Yes, Cancel')]")
	private WebElement btnYescancelmodelAutopaytext;

	@FindBy(xpath = "//button[contains(text(),'No thanks')]")
	private WebElement btnNocancelmodelAutopaytext;

	@FindBy(xpath = "//span[contains(text(),'Are you sure you want to exit AutoPay?')]")
	private WebElement txtExitmodelAutopay;

	@FindBy(xpath = "//button[contains(text(),'Yes, please exit')]")
	private WebElement btnYesExitmodelAutopaytext;

	@FindBy(xpath = "//button[contains(text(),'No thanks')]")
	private WebElement btnNoExitmodelAutopaytext;

	@FindBy(xpath = "//p[contains(text(),'Save')]")
	private List<WebElement> savediscounttext;

	@FindAll({ @FindBy(id = "paymentDefaultText"), @FindBy(id = "paymentNumber") })
	private WebElement addPaymentmethod;

	@FindAll({ @FindBy(xpath = "//span[contains(@class,'sprite')]/following-sibling::p"),
			@FindBy(css = "span[pid='cust_crd0']") })
	private WebElement storedPaymentCardNumber;

	private By landingpageSpinner = By.cssSelector("div.circle");

	@FindBy(css = "")
	private WebElement nextDuedate;

	@FindBy(css = "")
	private WebElement autopayAlert;

	public NewAutopayLandingPage(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public NewAutopayLandingPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public NewAutopayLandingPage verifyPageLoaded() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				waitforSpinner();
				Thread.sleep(2000);
				verifyPageUrl();
			} else {
				checkPageIsReady();
				waitFor(ExpectedConditions.invisibilityOfElementLocated(landingpageSpinner));

				verifyPageUrl();
				Reporter.log("Autopay page displayed");
			}
		} catch (Exception e) {
			Assert.fail("Autopay page not displayed");
		}
		return this;
	}

	public NewAutopayLandingPage checkHeaderAutopayOFF() {
		try {
			boolean isautopayoff = txtautopayOFF.size() > 0;
			if (isautopayoff)
				Reporter.log("Discount message shown");
			else
				Verify.fail("Discount message not shown");

		} catch (Exception e) {
			Verify.fail("Autopay off header not located");
		}
		return this;
	}

	public NewAutopayLandingPage ClickAgreeandSubmit() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(btnAgreeAndSubmit));
			btnAgreeAndSubmit.click();
			Reporter.log("Agree and submit button clicked");

		} catch (Exception e) {
			Verify.fail("Agree and submit button not located");
		}
		return this;
	}

	public NewAutopayLandingPage checkdiscounttextonAutopayOFF(boolean discount) {
		try {
			int disccount = savediscounttext.size();

			if (discount) {
				if (disccount > 0)
					Reporter.log("Discount message shown");
				else
					Verify.fail("Discount message not shown");
			} else {
				if (disccount < 1)
					Reporter.log("Discount message not shown");
				else
					Verify.fail("Discount message is shown");
			}
		} catch (Exception e) {
			Verify.fail("Saved payment element not located");
		}
		return this;
	}

	public NewAutopayLandingPage checkFirstautopaytextAutopayOFF(String strdate) {
		try {
			LocalDate dt = LocalDate.parse(strdate);
			// DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM dd yyyy hh:mm
			// a");
			DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM d");
			String landing = dt.format(format);

			String txtfirstautopay = firstautopaytext.getText();
			String firstdate = "First AutoPay would be: " + landing;
			if (txtfirstautopay.equalsIgnoreCase(firstdate))
				Reporter.log("First autopay date is correct");
			else
				Verify.fail("First autopay date is not correct");

		} catch (Exception e) {
			Verify.fail("First auto pay date element is not locating ");
		}
		return this;
	}

	public NewAutopayLandingPage checkBilldueAutopayOFF(String strdate) {
		try {
			LocalDate dt = LocalDate.parse(strdate);
			// DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM dd yyyy hh:mm
			// a");
			DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM d");
			String landing = dt.format(format);

			String billDueText = txtBilldue.getText();
			String billdue = "Your bill due: " + landing;
			if (billDueText.equalsIgnoreCase(billdue))
				Reporter.log("First autopay date is correct");
			else
				Verify.fail("First autopay date is not correct");

		} catch (Exception e) {
			Verify.fail("Billdue element is not locating ");
		}
		return this;
	}

	public NewAutopayLandingPage clickAddPaymentmethod() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addPaymentmethod));
			waitFor(ExpectedConditions.elementToBeClickable(addPaymentmethod));
			clickElementWithJavaScript(addPaymentmethod);
			Reporter.log("Add payment method link clicked");

		} catch (Exception e) {
			Verify.fail("Add payment method link is not located");
		}
		return this;
	}

	/**
	 * verify Scheduled Autopay Alert Inside Blackout Period
	 */
	public NewAutopayLandingPage verifyScheduledAutopayAlertsOnAutopayLandingPage() {
		try {
			checkPageIsReady();

			Verify.assertTrue(autopayAlert.isDisplayed(),
					" Scheduled Autopay Alert Inside Blackout Period not dispalyed");
			Verify.assertTrue(
					nextAutopaydate.getText()
							.contains("Next AutoPay Payment Date: (bill due date + 1 calendar month -2 days)"),
					" Scheduled Autopay Alert Inside Blackout Period not dispalyed");
			/*
			 * Verify.assertTrue(subsequentPaymentDate.getText().contains(
			 * "Subsequent payments will be processed on the (day of bill due date, e.g. 13th) of every month."
			 * ), " Scheduled Autopay Alert Inside Blackout Period not dispalyed");
			 */
			Verify.assertTrue(nextDuedate.getText()
					.contains("Your next bill due date is on Bill due date, e.g. March 16th, 2016)"));
		} catch (Exception e) {
			Verify.fail("Fail to validate  Scheduled Autopay Alert Inside Blackout Period not dispalyed");
		}
		return this;
	}

	public NewAutopayLandingPage verifyScheduledAutopayAlertInsideBlackoutPeriod(String Duedate, int calanderMonth,
			int days) {

		SimpleDateFormat standardFormat = new SimpleDateFormat("yyyy-MM-DD");
		Date billDueDate = null;

		try {

			billDueDate = standardFormat.parse(Duedate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(billDueDate);

			cal.add(Calendar.MONTH, calanderMonth);
			cal.add(Calendar.DAY_OF_MONTH, days);
			Date nextAutopayPaymentdate = cal.getTime();
			Verify.assertTrue(nextAutopaydate.getText().contains("Next AutoPay Payment Date:" + nextAutopayPaymentdate),
					"Next AutoPay Payment Date not dispalyed");
			/*
			 * Verify.assertTrue( subsequentPaymentDate.getText()
			 * .contains("Subsequent payments will be processed on the"+ billDueDate +
			 * "of every month."), " Subsequent payments date not dispalyed");
			 */
			Verify.assertTrue(
					nextDuedate.getText().contains("Your next bill due date is on Bill due date," + billDueDate),
					"Your next bill due date not dispalyed");

		} catch (ParseException e) {
			Verify.fail("Fail to validate  Scheduled Autopay Alert Inside Blackout Period not dispalyed");
		}

		return this;

	}

	public NewAutopayLandingPage verifyScheduledAutopayAlertOutsideBlackoutPeriod(String Duedate, int calanderMonth,
			int days) throws ParseException {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(sdf.parse(Duedate));
			Date nextBillDue = cal.getTime();
			String[] nextBillDuedate = nextBillDue.toString().split(" ");
			cal.add(Calendar.MONTH, calanderMonth);
			cal.add(Calendar.DAY_OF_MONTH, -2);
			Date nextAutopayPaymentdate = cal.getTime();

			String[] paymentdate = nextAutopayPaymentdate.toString().split(" ");// Sat Jan 19 00:07:00 IST 2019
			Verify.assertTrue(
					nextAutopaydate.getText()
							.contains("Next AutoPay payment: " + paymentdate[1] + " " + paymentdate[2]),
					"Next AutoPay Payment Date not dispalyed");
			Verify.assertTrue(
					verifyElementBytext(subsequentPaymentDate, "Payments will process on the " + paymentdate[2]),
					" Subsequent payments date not dispalyed");
			Verify.assertTrue(verifyElementBytext(subsequentPaymentDate, "Next bill due: " + nextBillDuedate[1]),
					"Your next bill due date not dispalyed");

		} catch (ParseException e) {
			Verify.fail("Fail to validate  Scheduled Autopay Alert Inside Blackout Period not dispalyed");
		}

		return this;

	}

	public NewAutopayLandingPage verifyScheduledAutopayAlertOutsideBlackoutInvalidDuedate(String Duedate,
			int calanderMonth, int days) {

		SimpleDateFormat standardFormat = new SimpleDateFormat("yyyy-MM-DD");
		Date billDueDate = null;

		try {

			billDueDate = standardFormat.parse(Duedate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(billDueDate);

			cal.add(Calendar.MONTH, calanderMonth);
			Date nextBillDuedate = cal.getTime();
			cal.add(Calendar.DAY_OF_MONTH, days);
			Date nextAutopayPaymentdate = cal.getTime();

			Verify.assertTrue(nextAutopaydate.getText().contains("Next AutoPay Payment Date:" + nextAutopayPaymentdate),
					"Next AutoPay Payment Date not dispalyed");
			/*
			 * Verify.assertTrue( subsequentPaymentDate.getText()
			 * .contains("Subsequent payments will be processed on the" + billDueDate +
			 * "of every month."), " Subsequent payments date not dispalyed");
			 */
			Verify.assertTrue(
					nextDuedate.getText().contains("Your next bill due date is on Bill due date," + nextBillDuedate),
					"Your next bill due date not dispalyed");

		} catch (ParseException e) {
			Verify.fail("Fail to validate  Scheduled Autopay Alert outside Blackout Period not dispalyed");
		}

		return this;

	}

	public NewAutopayLandingPage verifyScheduledAutopayAlertOutsideBlackoutInvalidDuedateBalanceLessThanZero(
			String Duedate, int calanderMonth, int days) {

		SimpleDateFormat standardFormat = new SimpleDateFormat("yyyy-MM-DD");
		Date billDueDate = null;

		try {

			billDueDate = standardFormat.parse(Duedate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(billDueDate);

			cal.add(Calendar.MONTH, calanderMonth);

			cal.add(Calendar.DAY_OF_MONTH, days);
			Date nextAutopayPaymentdate = cal.getTime();

			Verify.assertTrue(nextAutopaydate.getText().contains("Next AutoPay Payment Date:" + nextAutopayPaymentdate),
					"Next AutoPay Payment Date not dispalyed");
			/*
			 * Verify.assertTrue( subsequentPaymentDate.getText()
			 * .contains("Subsequent payments will be processed on the" + billDueDate +
			 * "of every month."), " Subsequent payments date not dispalyed");
			 */

		} catch (ParseException e) {
			Verify.fail("Fail to validate  Scheduled Autopay Alert outside Blackout Period not dispalyed");
		}

		return this;

	}

	public NewAutopayLandingPage verifyScheduledAutopayAlertwhenBillDueDateNotReturned() {

		try {

			Verify.assertTrue(autopayAlert.getText().contains(
					"AutoPay drafts your full balance balance and processes the payment 3 days before your monthly bill due date"));
			Verify.assertTrue(autopayAlert.getText().contains("Save $X/mo. by setting up AutoPay"));

		} catch (Exception e) {
			Verify.fail("Fail to validate  Scheduled Autopay Alert when bill duedate not returned");
		}

		return this;

	}

	public NewAutopayLandingPage clicktermsAndConditions() {
		try {
			termsAndConditions.click();
			Reporter.log("Terms and Conditions link clicked");
		} catch (Exception e) {
			Assert.fail("Terms and conditions is not located");
		}

		return this;

	}

	public NewAutopayLandingPage clickseeFaqLink() {
		try {
			seeFaq.click();
			Reporter.log("See FAQ link clicked");
		} catch (Exception e) {
			Assert.fail("See FAQ link is not located");
		}

		return this;

	}

	public NewAutopayLandingPage ValidateLandingpagedatesonAutopayOFF(String firstday, String duedate) {

		checkFirstautopaytextAutopayOFF(firstday);
		checkBilldueAutopayOFF(duedate);

		return this;

	}

	public NewAutopayLandingPage Submitpaymentwhenbankisdefaultmethod() {
		try {
			if (Verifydefaultisbank()) {
				btnAgreeAndSubmit.click();
			} else {
				Assert.fail("No saved bank");
			}
		} catch (Exception e) {
			Assert.fail("bankdefault elment ");
		}
		return this;
	}

	public boolean Verifydefaultiscard() {
		try {

			if (carddefault.size() > 0) {

				return true;
			} else {

				return false;
			}
		} catch (Exception e) {
			Assert.fail("card default elment is not located");
		}
		return false;
	}

	public boolean Verifydefaultisbank() {
		try {

			if (bankdefault.size() > 0) {

				return true;
			} else {

				return false;
			}
		} catch (Exception e) {
			Assert.fail("card default elment is not located");
		}
		return false;
	}

	public NewAutopayLandingPage checkFirstautopaytextAutopayON(String strdate) {
		try {
			LocalDate dt = LocalDate.parse(strdate);
			// DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM dd yyyy hh:mm
			// a");
			DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM d");
			String landing = dt.format(format);

			String txtfirstautopay1 = NextAutopayment.getText().trim();
			String txtfirstautopay = txtfirstautopay1.substring(0, txtfirstautopay1.length() - 2);
			String firstdate = "Next AutoPay payment: " + landing;
			if (txtfirstautopay.equalsIgnoreCase(firstdate))
				Reporter.log("First autopay date is correct");
			else
				Verify.fail("First autopay date is not correct");

		} catch (Exception e) {
			Verify.fail("First auto pay date element is not locating ");
		}
		return this;
	}

	public NewAutopayLandingPage checkBilldueAutopayON(String strdate) {
		try {
			LocalDate dt = LocalDate.parse(strdate);
			// DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM dd yyyy hh:mm
			// a");
			DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM d");
			String landing = dt.format(format);

			String billDueText1 = Nextbilldue.getText().trim();
			String billDueText = billDueText1.substring(0, billDueText1.length() - 2);
			String billdue = "Next bill due: " + landing;
			if (billDueText.equalsIgnoreCase(billdue))
				Reporter.log("Next Bill due date is correct");
			else
				Verify.fail("Next bill due date is not correct");

		} catch (Exception e) {
			Verify.fail("Billdue element is not locating ");
		}
		return this;
	}

	public NewAutopayLandingPage checkmontlyPaymentDateAutopayON(String strdate) {
		try {
			LocalDate dt = LocalDate.parse(strdate);
			// DateTimeFormatter format = DateTimeFormatter.ofPattern("MMM dd yyyy hh:mm
			// a");
			DateTimeFormatter format = DateTimeFormatter.ofPattern("d");
			String landing = dt.format(format);

			String billDueText1 = everymonthprocesspayment.getText().trim();
			String billDueText = billDueText1.substring(0, billDueText1.length() - 16);
			String billdue = "Payments will process on the " + landing;
			if (billDueText.equalsIgnoreCase(billdue))
				Reporter.log("everymonth paymnet date is correct");
			else
				Verify.fail("everymonth paymnet date is not correct");

		} catch (Exception e) {
			Verify.fail("everymonth paymnet date element is not locating ");
		}
		return this;
	}

	public NewAutopayLandingPage clickCancelAutopayLink() {
		try {

			cancelAutopayLink.click();
			Reporter.log("Cancel Auto pay button clicked");
		} catch (Exception e) {
			Assert.fail("CancelAutopaylinknot clicked ");
		}
		return this;
	}

	public NewAutopayLandingPage checkcancelautopaytextinModel() {
		try {

			if (txtcancelmodelAutopay.isDisplayed())
				Reporter.log("Are you sure you want to cancel Autopay? text displyed");
			else
				Verify.fail("Are you sure you want to cancel Autopay? text is not displyed");
		} catch (Exception e) {
			Assert.fail("Are you sure you want to cancel Autopay? element not located ");
		}
		return this;
	}

	public NewAutopayLandingPage checkautopayOFFtext() {
		try {

			if (txtautopayOFF.get(0).isDisplayed())
				Reporter.log("Autopay OFF  text displyed");
			else
				Verify.fail("Autopay OFF text  is not displyed");
		} catch (Exception e) {
			Assert.fail("Autopay OFF text element not located ");
		}
		return this;
	}

	public boolean checkispageAutopayOFF() {

		if (txtautopayOFF.size() > 0)
			return true;
		else
			return false;

	}

	public boolean checkispageAutopayON() {

		if (txtautopayON.size() > 0)
			return true;
		else
			return false;

	}

	public boolean checkstroredpaymentmethods() {

		if (bankdefault.size() > 0)
			return true;
		else if (carddefault.size() > 0)
			return true;
		else
			return false;

	}

	public NewAutopayLandingPage clickBackbutton() {
		try {

			btnBack.click();
			Reporter.log("Back button clicked");
		} catch (Exception e) {
			Assert.fail("Back button elment is not located");
		}
		return this;
	}

	public NewAutopayLandingPage clickYesPleaseExitbutton() {
		try {

			btnYesExitmodelAutopaytext.click();
			Reporter.log("Yes Please Exit button clicked");
		} catch (Exception e) {
			Assert.fail("Yes Please Exit button elment is not located");
		}
		return this;
	}

	public NewAutopayLandingPage CheckSpriteiconisDisplayed() {
		try {

			if (spriteIcon.isDisplayed())
				Reporter.log("Sprite Icon displayed");
			else
				Verify.fail("Sprite Icon is not displayed");
		} catch (Exception e) {
			Assert.fail("Sprite Icon is not located");
		}
		return this;
	}

	public NewAutopayLandingPage clickyescancelautopayonModel() {
		try {

			btnYescancelmodelAutopaytext.click();
			Reporter.log("Yes Cancel Auto pay button clicked");
		} catch (Exception e) {
			Assert.fail("Yes Cancel Auto pay elment is not located");
		}
		return this;
	}

	/**
	 * get payment card/account number on blade
	 */
	public String getStoredPaymentMethodNumber() {
		String cardNumber = null;
		try {
			cardNumber = storedPaymentCardNumber.getText().replace(" ", "").replace("*", "").trim();
		} catch (Exception e) {
			Assert.fail("Payment method number not found");
		}
		return cardNumber;
	}

	public NewAutopayLandingPage Checklastfourdigitsofpaymentmethod(String lastdigits) {
		try {

			if (addPaymentmethod.getText().contains(lastdigits))
				Reporter.log("Last 4digits of card is verified");
			else
				Verify.fail("Last 4 digits of code is not verified");

		} catch (Exception e) {
			Assert.fail("addPaymentmethod elment is not located");
		}
		return this;
	}

}
