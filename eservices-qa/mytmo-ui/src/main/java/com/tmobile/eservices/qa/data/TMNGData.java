package com.tmobile.eservices.qa.data;

import com.tmobile.eqm.testfrwk.ui.core.annotations.Data;

/**
 * @author csudheer
 * 
 */
public class TMNGData {

	@Data(name = "Env")
	private String env;

	@Data(name = "URL")
	private String url;
	
	@Data(name="TestName")
	private String testName;

	@Data(name="retriveCartEmail")
	private String retriveCartEmail;
	
	@Data(name="firstName")
	private String firstName;
	
	@Data(name="middleName")
	private String middleName;
	
	@Data(name="lastName")
	private String lastName;
	
	@Data(name="email")
	private String email;
	
	@Data(name="phoneNumber")
	private String phoneNumber;
	
	@Data(name="shippingAddress")
	private String shippingAddress;
	
	@Data(name="city")
	private String city;
	
	@Data(name="state")
	private String state;
	
	@Data(name="zipcode")
	private String zipcode;
	
	@Data(name="nameOnCard")
	private String nameOnCard;
	
	@Data(name="cardNumber")
	private String cardNumber;
	
	@Data(name="expiryDate")
	private String expiryDate;
	
	@Data(name="cvv")
	private String cvv;
	
	@Data(name="idType")
	private String idType;
	
	@Data(name="idNumber")
	private String idNumber;
	
	@Data(name="idExpiryDate")
	private String idExpiryDate;
	
	@Data(name="SSN")
	private String SSN;
	
	@Data(name="lastFourDigitSSN")
	private String lastFourDigitSSN;
	
	@Data(name="DOB")
	private String DOB;
	
	@Data(name="securityPin")
	private String securityPin;
	
	@Data(name="DeviceName")
	private String deviceName;
	
	@Data(name="DeviceSKU")
	private String deviceSKU;
	
	@Data(name="SecondDeviceSKU")
	private String secondDeviceSKU;
	
	@Data(name="ThirdDeviceSKU")
	private String thirdDeviceSKU;
	
	@Data(name="DeviceColor")
	private String deviceColor;
	
	@Data(name="DeviceMemory")
	private String deviceMemory;
	
	@Data(name="Carrier")
	private String carrierOtions;

	@Data(name="Make")
	private String makeOtions;
	
	@Data(name="Modal")
	private String modalOtions;
	
	@Data(name="iMEINumber")
	private String imeiName;
	
	@Data(name="appointmentDate")
	private String appointmentDate;
	
	@Data(name="appointmentTime")
	private String appointmentTime;
	
	@Data(name="reason")
	private String reason;
	
	@Data(name="TabletName")
	private String tabletName;
	
	@Data(name="WatchName")
	private String watchName;
	
	@Data(name="AccessoryName")
	private String accessoryName;
	
	@Data(name="invalidEmail")
	private String invalidEmail;
	
	@Data(name="category")
	private String category;
	
	@Data(name="destinationName")
	private String destinationName;
	
	@Data(name="cruiseName")
	private String cruiseName;
		
	@Data(name="deviceCarrier")
	private String deviceCarrier;
	
	@Data(name="deviceManufacturer")
	private String deviceManufacturer;
	
	@Data(name="deviceModel")
	private String deviceModel;	
	
	@Data(name = "loginEmailOrPhone")
	private String loginEmailOrPhone;
	
	@Data(name="loginPassword")
	private String password;
	
	@Data(name="sapId")
	private String sapId;
	
	@Data(name="startDate")
	private String startDate;
	
	@Data(name="endDate")
	private String endDate;
	
	@Data(name="openingTime")
	private String openingTime;
	
	@Data(name="closingTime")
	private String closingTime;
	
	@Data(name="closingEarly")
	private String closingEarly;
	
	@Data(name="openingEarly")
	private String openingEarly;
	
	@Data(name="description")
	private String description;
	
	@Data(name="deviceColorCount")
	private String deviceColorCount;
	
	//NPI - begin
	
	@Data(name="SecondDeviceName")
	private String secondDeviceName;
	
	@Data(name="ThirdDeviceName")
	private String thirdDeviceName;
	
	@Data(name="SecondDeviceMemory")
	private String secondDeviceMemory;
	
	@Data(name="ThirdDeviceMemory")
	private String thirdDeviceMemory;
	
	@Data(name="SecondDeviceColor")
	private String secondDeviceColor;
	
	@Data(name="ThirdDeviceColor")
	private String thirdDeviceColor;
	
	@Data(name="TabletMemory")
	private String tabletMemory;
	
	@Data(name="TabletColor")
	private String tabletColor;
	
	@Data(name="WatchMemory")
	private String watchMemory;
	
	@Data(name="WatchColor")
	private String watchColor;
	
	@Data(name="WatchSKU")
	private String watchSKU;
	
	@Data(name="AccessoryColor")
	private String accessoryColor;
	
	@Data(name="SecondDeviceColorCount")
	private String secondDeviceColorCount;
	
	@Data(name="ThirdDeviceColorCount")
	private String thirdDeviceColorCount;
	
	@Data(name="SecondAccessoryName")
	private String secondAccessoryName;
	
	
	/**
	 * @return the Device Name
	 */
	public String getSecondDeviceName() {
		return secondDeviceName;
	}
	
	/**
	 * @return the Device Name
	 */
	public String getThirdDeviceName() {
		return thirdDeviceName;
	}
	
	/**
	 * @return the Memory 
	 */
	public String getSecondDeviceMemoryOption() {
		return secondDeviceMemory;
	}
	
	/**
	 * @return the Memory 
	 */
	public String getThirdDeviceMemoryOption() {
		return thirdDeviceMemory;
	}
	
	/**
	 * @return the Color 
	 */
	public String getSecondDeviceColorOption() {
		return secondDeviceColor;
	}
	
	/**
	 * @return the Color 
	 */
	public String getThirdDeviceColorOption() {
		return thirdDeviceColor;
	}
	
	/**
	 * @return the Memory 
	 */
	public String getTabletMemoryOption() {
		return tabletMemory;
	}
	
	/**
	 * @return the Color 
	 */
	public String getTabletColorOption() {
		return tabletColor;
	}
	
	/**
	 * @return the Memory 
	 */
	public String getWatchMemoryOption() {
		return watchMemory;
	}
	
	/**
	 * @return the Color 
	 */
	public String getWatchColorOption() {
		return watchColor;
	}
	
	/**
	 * @return the Watch SKU 
	 */
	public String getWatchSKU() {
		return watchSKU;
	}
	
	/**
	 * @return the Color 
	 */
	public String getAccessoryColorOption() {
		return accessoryColor;
	}
	
	/**
	 * @return the Device color count
	 */
	public String getSecondDeviceColorCount() {
		return secondDeviceColorCount;
	}
	
	/**
	 * @return the Device color count
	 */
	public String getThirdDeviceColorCount() {
		return thirdDeviceColorCount;
	}
	
	//NPI - end
	
	public String getDescription() {
		return description;
	}
	
	public String getOpeningTime() {
		return openingTime;
	}

	

	public String getClosingTime() {
		return closingTime;
	}

	

	public String getClosingEarly() { 
		return closingEarly;
	}



	public String getOpeningEarly() {
		return openingEarly;
	}

	


	
	
	public String getEmailForRetriveCart() {
		return retriveCartEmail;
	}
	
	/**
	 * @return the env
	 */
	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * @return the city
	 */
	public String getZipcode() {
		return zipcode;
	}
	
	
	
	/**
	 * @return the city
	 */
	public String getState() {
		return state;
	}
	
	
	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}
	
	
	/**
	 * @return the shippingAddress
	 */
	public String getShippingAddress() {
		return shippingAddress;
	}
	
	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	/**
	 * @return the LastName
	 */
	public String getLastName() {
		return lastName;
	}
	

	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the securityPin
	 */
	public String getsecurityPin() {
		return securityPin;
	}
	
	/**
	 * @return the DOB
	 */
	public String getDOB() {
		return DOB;
	}
	
	/**
	 * @return the SSN
	 */
	public String getSSN() {
		return SSN;
	}
	
	/**
	 * @return the SSN
	 */
	public String getSSNLastFour() {
		return lastFourDigitSSN;
	}
	
	/**
	 * @return the idexpiryDate
	 */
	public String getIdExpiryDate() {
		return idExpiryDate;
	}
	
	/**
	 * @return the idnumber
	 */
	public String getIdNumber() {
		return idNumber;
	}
	
	/**
	 * @return the idType
	 */
	public String getIdType() {
		return idType;
	}
	
	/**
	 * @return the cvv
	 */
	public String getCvv() {
		return cvv;
	}
	
	/**
	 * @return the expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}
	
	/**
	 * @return the nameOnCard
	 */
	public String getNameOnCard() {
		return nameOnCard;
	}
	
	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}
	
	/**
	 * @return the Device Name
	 */
	public String getDeviceName() {
		return deviceName;
	}
	
	/**
	 * @return the Device color count
	 */
	public String getDeviceColorCount() {
		return deviceColorCount;
	}
	
	/**
	 * @return the second SKU 
	 */
	public String getSecondDeviceSKU() {
		return secondDeviceSKU;
	}
	
	/**
	 * @return the SKU 
	 */
	public String getThirdDeviceSKU() {
		return thirdDeviceSKU;
	}
	
	/**
	 * @return the SKU 
	 */
	public String getDeviceSKU() {
		return deviceSKU;
	}
	
	/**
	 * @return the Color 
	 */
	public String getDeviceColorOption() {
		return deviceColor;
	}
	
	/**
	 * @return the Memory 
	 */
	public String getDeviceMemoryOption() {
		return deviceMemory;
	}
	
	/**
	 * @return the Carrier Name
	 */
	public String getCarrierName() {
		return carrierOtions;
	}
	
	/**
	 * @return the Make Name
	 */
	public String getMakeName() {
		return makeOtions;
	}
	/**
	 * @return the Modal Name
	 */
	public String getModalName() {
		return modalOtions;
	}
	/**
	 * @return the Imei Name
	 */
	public String getImeiName() {
		return imeiName;
	}
	/**
	 * @return the Appointment Date
	 */
	public String getAppointmentDate() {
		return appointmentDate;
	}
	
	/**
	 * @return the Appointment time
	 */
	public String getAppointmentTime() {
		return appointmentTime;
	}
	
	/**
	 * @return the Reason
	 */
	public String getReason() {
		return reason;
	}
	
	/**
	 * @return the AccessoryName
	 */
	public String getAccessoryName() {
		return accessoryName;
	}
	
	/**
	 * @return Tablet name
	 */
	public String getTabletName() {
		return tabletName;
	}
	
	/**
	 * @return Watch name
	 */
	public String getWatchName() {
		return watchName;
	}
	
	
	
	/**
	 * @return InvalidEmail
	 */
	public String getInvalidEmail() {
		return invalidEmail;
	}
	
	/**
	 * @return category
	 */
	public String getCategoryName() {
		return category;
	}
	
	/**
	 * @return DestinationCountryName
	 */
	public String getDestinationName() {
		return destinationName;
	}
	
	/**
	 * @return cruiseOrFerryName
	 */
	public String getCruiseName() {
		return cruiseName;
	}
	
	/**
	 * @return device Carrier
	 */
	public String getDeviceCarrier() {
		return deviceCarrier;
	}
	
	/**
	 * @return device Manufacturer
	 */
	public String getDeviceManufacturer() {
		return deviceManufacturer;
	}
	
	/**
	 * @return device Model
	 */
	public String getDeviceModel() {
		return deviceModel;
	}
	
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * @return the SAP ID
	 */
	public String getSAPId() {
		return sapId;
	}
	
	/**
	 * @return the start date
	 */
	public String getStartDate() {
		return startDate;
	}
	
	/**
	 * @return the end date
	 */
	public String getEndDate() {
		return endDate;
	}
	
	/**
	 * @return the email
	 */
	public String getLoginEmailOrPhone() {
		return loginEmailOrPhone;
	}
	
	/**
	 * @return the firstName
	 */
	public String getMiddleName() {
		return middleName;
	}
	
	/**
	 * @return the AccessoryName
	 */
	public String getSecondAccessoryName() {
		return secondAccessoryName;
	}
	
}
