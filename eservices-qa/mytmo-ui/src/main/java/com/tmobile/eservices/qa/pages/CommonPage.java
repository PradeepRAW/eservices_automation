package com.tmobile.eservices.qa.pages;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.exception.FrameworkException;
import com.tmobile.eqm.testfrwk.ui.core.web.WebPage;
import com.tmobile.eservices.qa.api.RestServiceHelper;
import com.tmobile.eservices.qa.api.unlockdata.RequestData;
//import com.tmobile.eservices.qa.accounts.AccountsCommonLib;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.listeners.CustomizedEmailableReport;
import com.tmobile.eservices.qa.listeners.WebEventListener;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsConfirmationPage;
import com.tmobile.eservices.qa.pages.accounts.ManageAddOnsSelectionPage;
import com.tmobile.eservices.qa.pages.accounts.ODFDataPassReviewPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

/**
 * @author ksrinivas
 *
 */
public class CommonPage extends WebPage {

	public static final Logger logger = LoggerFactory.getLogger(CommonPage.class);
	private static final Logger L = LoggerFactory.getLogger(CustomizedEmailableReport.class);

	private List<String> listDropdown = new ArrayList<>();
	Actions action;
	Select select;
	static JavascriptExecutor javaScript;
	EventFiringWebDriver eventDriver = null;
	WebEventListener eventListener = null;
	RestServiceHelper restServiceHelper;

	@FindBy(id = "overridelink")
	private WebElement btnCertificateButton;

	protected By loadingImageProfile = By.id("modal_message_loading");
	protected By spinner = By.id("updated_spinner");
	protected By waitForMessage = By.id("divProgressBar");
	@FindBy(css = "div.acsActionShell a.acsDeclineButton")
	private WebElement noThanks;

	@FindBy(id = "bgoverlay")
	protected List<WebElement> spinners;

	@FindBy(id = "android:id/button1")
	private WebElement securitycontinuebtn;

	@FindBy(id = "overridelink")
	private List<WebElement> continueToWebsiteButton;

	@FindBy(css = "i[class*='fa-phn-touch-area']")
	private WebElement contactUsImg;

	@FindBy(id = "logout-Ok")
	private WebElement logOutBtn;

	@FindBy(xpath = "//h5[contains(text(), 'Connect with')]")
	private WebElement contactWithUSPage;

	@FindBy(css = "div.TMO-HEADER-COMPONENT span")
	private WebElement backBtn;

	@FindBy(css = "img[class*='icon-size']")
	private WebElement tmobileIcon;

	@FindBy(css = ".animation-in")
	private WebElement updateOperationDialogBox;

	@FindBy(css = "#city")
	private WebElement city;

	@FindBy(css = "#line1")
	private WebElement addressLine1;

	@FindBy(css = "#line2")
	private WebElement addressLine2;

	@FindBy(css = "div#model.dropdown-toggle")
	private WebElement stateDropDownToggle;

	@FindBy(css = "#zip")
	private WebElement zipCode;

	@FindBy(xpath = "//p[@class='Display6'and contains(text(),'Address')]/../p[2]")
	private WebElement updatedAddressLine2;

	@FindBy(css = "a.text-black")
	private WebElement profileBreadCrumb;

	@FindBy(css = ".breadcrumb-item.active")
	private WebElement activeBreadCrumb;

	@FindBy(css = ".d-inline.show-on-sm-modal")
	private WebElement callUsIcon;

	@FindBy(css = "i.pull-left.ico-backArrowIcon2")
	private WebElement headerBackButton;

	/**
	 * @param webDriver
	 */
	public CommonPage(WebDriver webDriver) {
		super(webDriver);
		eventDriver = new EventFiringWebDriver(getDriver());
		eventListener = new WebEventListener(getDriver());
		eventDriver.register(eventListener);
	}

	/**
	 * invisibility
	 * 
	 * @return
	 */
	public void waitForDataPassSpinnerInvisibility() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("ajax-loader_spinner")));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("loader spin-image")));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(
					By.cssSelector("i[class='fa fa-spin fa-spinner interceptor-spinner']")));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("loader spinner-image")));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("spin-loader")));
			waitFor(ExpectedConditions
					.javaScriptThrowsNoExceptions("return (document.readyState == 'complete' && jQuery.active == 0)"));
		} catch (Exception e) {
			Reporter.log("Spinner not visible");
		}
	}

	/**
	 * invisibility
	 * 
	 * @return
	 */
	public void waitForImageSpinnerInvisibility() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("modal_message_loading")));
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("ajax_loader")));
	}

	/**
	 * 
	 * @param element
	 */
	public void elementClick(WebElement element) {
		try {
			eventListener.beforeClickOn(element, eventDriver);
			moveToElement(element);
			element.click();
		} catch (Exception e) {
			e.getMessage().contains("NoSuchElementException");
			Reporter.log(">>FAILED::Not found" + "'" + element.getText() + "'" + " Can not proceed further");
			Reporter.log(e.getMessage().trim());
		}
	}

	/**
	 * 
	 * @param element
	 */
	public void selectCheckbox(WebElement element) {
		waitFor(ExpectedConditions.visibilityOf(element));
		try {
			if (!element.isSelected()) {
				waitFor(ExpectedConditions.elementToBeClickable(element));
				elementClick(element);
				Reporter.log("Select check box is checked");
			}
		} catch (Exception e) {
			Reporter.log("Select check box not displayed");
		}
	}

	/**
	 * Send Text Data
	 * 
	 * @param element
	 * @param data
	 */
	public void sendTextData(WebElement element, String data) {
		try {
			element.clear();
			element.sendKeys(data);
		} catch (Exception e) {
			Assert.fail("Unable to enter the text");
		}
	}

	/**
	 * To Verify the Certificate of the Browser
	 */
	public void verifyCertificate() {
		waitFor(ExpectedConditions.visibilityOf(btnCertificateButton));
		if (btnCertificateButton.isDisplayed()) {
			elementClick(btnCertificateButton);
		}
	}

	/**
	 * To Select the values from the drop down
	 * 
	 * @param topicListDropdown
	 * @return
	 */
	public List<String> getAllOptionsInDropdown(WebElement topicListDropdown) {
		select = new Select(topicListDropdown);
		List<WebElement> optionsList = select.getOptions();
		for (WebElement optionList : optionsList) {
			listDropdown.add(optionList.getText());
		}
		return listDropdown;
	}

	/**
	 * Select Element From The Drop Down
	 * 
	 * @param element [WebElement Of Select]
	 * @param param   [Value or Text or Index]
	 * @param data    [Element To Be Selected]
	 */
	public void selectElementFromDropDown(WebElement element, String param, String data) {
		waitFor(ExpectedConditions.visibilityOf(element));
		select = new Select(element);
		switch (param) {
		case Constants.SELECT_BY_VALUE:
			select.selectByValue(data);
			break;
		case Constants.SELECT_BY_TEXT:
			select.selectByVisibleText(data);
			break;
		case Constants.SELECT_BY_INDEX:
			select.selectByIndex(Integer.parseInt(data));
			break;
		default:
			break;
		}
	}

	/**
	 * Get Selected Element
	 * 
	 * @param element
	 * @return
	 */
	public String getSelectedOption(WebElement element) {
		waitFor(ExpectedConditions.visibilityOf(element));
		select = new Select(element);
		return select.getFirstSelectedOption().getText();
	}

	/**
	 * @param tabName
	 * @param webElementList
	 */
	public void clickMenuLinks(final String tabName, final List<WebElement> webElementList) {
		for (WebElement link : webElementList) {
			if (tabName.equalsIgnoreCase(link.getText())) {
				elementClick(link);
				break;
			}
		}
	}

	/**
	 * @param tabName
	 * @param webElementList
	 * @return
	 */
	public boolean verifyLinksAreDisplaying(final String tabName, final List<WebElement> webElementList) {
		boolean linkText = false;
		for (WebElement link : webElementList) {
			if (tabName.equalsIgnoreCase(link.getText())) {
				link.isDisplayed();
				linkText = true;
				break;
			}
		}
		return linkText;
	}

	/**
	 * Gets the URL for current Page.
	 * 
	 * @return String
	 */
	public String currentUrl() {
		return getDriver().getCurrentUrl();
	}

	/**
	 * Switch To Window
	 */
	public void switchToWindow() {
		if (!(getDriver() instanceof IOSDriver)) {
			checkPageIsReady();
			String currentWindow = getDriver().getWindowHandle();
			for (String winHandle : getDriver().getWindowHandles()) {
				if (!winHandle.equalsIgnoreCase(currentWindow)) {
					getDriver().switchTo().window(winHandle);
					break;
				}
			}
		}
	}

	/**
	 * Wait For Spinner Invisible
	 */
	public void waitForSpinnerInvisibility() {
		waitFor(ExpectedConditions
				.invisibilityOfElementLocated(By.cssSelector("i[class='fa fa-spin fa-spinner interceptor-spinner']")));
	}

	/**
	 * Clicking On An Element With JavaScript
	 * 
	 * @param element
	 */
	public void clickElementWithJavaScript(WebElement element) {
		try {
			javaScript = (JavascriptExecutor) getDriver();
			javaScript.executeScript("arguments[0].click();", element);
		} catch (TimeoutException e) {
			Assert.fail("Element not found to perform click operation");
		}
	}

	/**
	 * Clicking On An Element With JavaScriptScaling
	 *
	 * @param element
	 */
	public void clickElementWithJavaScriptScaling(WebElement element) {
		try {
			javaScript = (JavascriptExecutor) getDriver();
			javaScript.executeScript("arguments[0].style.transform='scale(1)';", element);
		} catch (TimeoutException e) {
			Assert.fail("Element not found to perform click operation");
		}
	}

	/**
	 * Enter value in an Element With JavaScript
	 * 
	 * @param element
	 * @param inputValue
	 */
	public void sendDataWithJavaScript(WebElement element, String inputValue) {
		waitFor(ExpectedConditions.visibilityOf(element));
		javaScript = (JavascriptExecutor) getDriver();
		javaScript.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');",
				element);
		javaScript.executeScript("arguments[0].value = '" + inputValue + "';", element);
	}

	/**
	 * This common method is used Delete all cookies in browser
	 */
	public void deleteCookies() {
		getDriver().manage().deleteAllCookies();
	}

	/**
	 * MoveToElement With Actions Class
	 * 
	 * @param element
	 */
	public void moveToElement(WebElement element) {
		waitFor(ExpectedConditions.visibilityOf(element));
		action = new Actions(getDriver());
		action.moveToElement(element).build().perform();
	}

	/**
	 * Get Element Attribute
	 * 
	 * @param element
	 * @param attribute
	 * @return
	 */
	public String getAttribute(WebElement element, String attribute) {
		waitFor(ExpectedConditions.visibilityOf(element));
		return element.getAttribute(attribute);
	}

	/**
	 * We are using this method to verifying the page load status Check Page Is
	 * Ready
	 */
	public void checkPageIsReady() {
		javaScript = (JavascriptExecutor) getDriver();
		try {
			for (int i = 0; i < 30; i++) {
				Thread.sleep(1000);
				if ("complete".equalsIgnoreCase(javaScript.executeScript("return document.readyState").toString())) {
					break;
				}
			}
		} catch (Exception ex) {
			Reporter.log(getDriver().getCurrentUrl() + "Page is not loaded properly");
		}
	}

	/**
	 * Return Elements Text [List Of Elements]
	 * 
	 * @param elements
	 * @return
	 */
	public List<String> getElementsByText(List<WebElement> elements) {
		waitFor(ExpectedConditions.visibilityOfAllElements(elements));
		List<String> listElements = new ArrayList<>();
		for (WebElement element : elements) {
			listElements.add(element.getText());
		}
		return listElements;
	}

	/**
	 * Click Element By Text
	 * 
	 * @param elements
	 * @param link
	 */
	public void clickElementBytext(List<WebElement> elements, String link) {
		for (WebElement webElement : elements) {
			if (link.equalsIgnoreCase(webElement.getText())) {
				try {
					clickElementWithJavaScript(webElement);
				} catch (Exception e) {
					e.getMessage().contains("NoSuchElementException");
					Reporter.log(">>FAILED::Not found " + webElement.getText() + " can not proceed further");
					Reporter.log(e.getMessage().trim());
				}
				break;
			}
		}
	}

	/**
	 * Click Element [List Of elements]
	 * 
	 * @param elements
	 */
	public void clickElement(List<WebElement> elements) {
		waitFor(ExpectedConditions.visibilityOfAllElements(elements));
		for (WebElement webElement : elements) {
			if (webElement.isDisplayed()) {
				try {
					elementClick(webElement);
				} catch (Exception e) {
					e.getMessage().contains("NoSuchElementException");
					Reporter.log(">>FAILED::Not found " + webElement.getText() + " Can not proceed further");
					Reporter.log(e.getMessage().trim());
				}
				break;
			}
		}
	}

	/**
	 * Click Element [List Of elements]
	 * 
	 * @param elements
	 */
	public void clickElementUponDisplay(List<WebElement> elements) {

		for (WebElement webElement : elements) {
			if (webElement.isDisplayed()) {
				try {
					elementClick(webElement);
				} catch (Exception e) {
					e.getMessage().contains("NoSuchElementException");
					Reporter.log(">>FAILED::Not found " + webElement.getText() + "can not proceed further");
					Reporter.log(e.getMessage().trim());
				}
				break;
			}
		}

	}

	public boolean verifyElementBytext(List<WebElement> elements, String text) {
		boolean isElementDisplayed = false;
		for (WebElement webElement : elements) {
			if (webElement.getText().contains(text)) {
				isElementDisplayed = true;
				break;
			}
		}
		return isElementDisplayed;
	}

	public ArrayList<Double> getNumbersFromList(List<WebElement> elements) {

		String cost;
		double totalMonthlyCostValue;
		ArrayList<Double> al = new ArrayList<>();

		for (WebElement webElement : elements) {
			cost = webElement.getText();
			cost = cost.substring(1, cost.length());
			totalMonthlyCostValue = Double.parseDouble(cost);
			al.add(totalMonthlyCostValue);
		}
		return al;
	}

	public boolean verifyElementStyleAttribute(WebElement element) {
		boolean isDisabled = false;
		String eleAttr;
		eleAttr = element.getAttribute("style");
		if (eleAttr.contains("none")) {
			isDisabled = true;
		}
		return isDisabled;
	}

	/**
	 * Switch To Frame [WebElement]
	 * 
	 * @param element
	 */
	public void switchToFrame(WebElement element) {
		if (getDriver() instanceof AppiumDriver) {
			getDriver().get(getAttribute(element, "src"));
		} else {
			getDriver().switchTo().frame(element);
		}

	}

	/**
	 * Switch To Default Content
	 */
	public void switchToDefaultContent() {
		getDriver().switchTo().defaultContent();
	}

	/**
	 * verify login page title
	 * 
	 * @return boolean
	 */
	public CommonPage verifyLoginPageTitle() {
		waitFor(ExpectedConditions.titleContains("T-Mobile"));
		return this;
	}

	public void verifyDomRendered() {
		try {
			waitFor(ExpectedConditions
					.javaScriptThrowsNoExceptions("return (document.readyState == 'complete' && jQuery.active == 0)"));
			Thread.sleep(2000);
		} catch (Exception e) {
			Assert.fail("JQuery calls not completed with in max time limit");
		}
	}

	/**
	 * Get Alert Text
	 * 
	 * @return
	 */
	public String getAlertText() {
		return getDriver().switchTo().alert().getText();
	}

	/**
	 * Accept Alert
	 */
	public void acceptIOSAlert() {
		if (getDriver() instanceof IOSDriver) {
			if (isAlertPresent()) {
				Alert alert = getDriver().switchTo().alert();
				alert.accept();
			}
		}
	}

	public boolean isAlertPresent() {
		boolean foundAlert = false;
		try {
			waitFor(ExpectedConditions.alertIsPresent());
			foundAlert = true;
		} catch (TimeoutException eTO) {
			foundAlert = false;
		}
		return foundAlert;
	}

	/**
	 * Dismiss Alert
	 */
	public void dismissAlert() {
		getDriver().switchTo().alert().dismiss();
	}

	/**
	 * Navigate Back
	 */
	public void navigateBack() {
		checkPageIsReady();
		getDriver().navigate().back();
		getDriver().navigate().refresh();
	}

	/**
	 * Close Other Tabs
	 * 
	 * @param mainWindow
	 */
	public void closeOtherTabs(String mainWindow) {
		// Do something to open new tabs
		if (getDriver() instanceof AppiumDriver) {
			getDriver().close();
		} else {
			for (String handle : getDriver().getWindowHandles()) {
				if (!handle.equals(mainWindow)) {
					getDriver().switchTo().window(handle);
					getDriver().close();
				}
			}
			getDriver().switchTo().window(mainWindow);
		}
	}

	/**
	 * Click On Security Alert
	 */
	@SuppressWarnings("rawtypes")
	public void clickOnSecurityAlertAccept() {
		if (getDriver() instanceof AndroidDriver) {
			String currentContext = ((AppiumDriver) getDriver()).getContext();
			try {
				((AppiumDriver) getDriver()).context("NATIVE_APP");
				waitFor(ExpectedConditions.visibilityOf(securitycontinuebtn));
				securitycontinuebtn.click();
				((AppiumDriver) getDriver()).context(currentContext);
			} catch (Exception e) {
				((AppiumDriver) getDriver()).context(currentContext);
				logger.info("Security certificate is not appeared in Android");
			}
		} else if (getDriver() instanceof AppiumDriver) {
			String currentIosContext = ((AppiumDriver) getDriver()).getContext();
			try {
				((AppiumDriver) getDriver()).context("NATIVE_APP");
				waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@label='Continue']")));
				((AppiumDriver) getDriver()).findElement(By.xpath("//*[@label='Continue']")).click();
				((AppiumDriver) getDriver()).context(currentIosContext);
			} catch (Exception e) {
				((AppiumDriver) getDriver()).context(currentIosContext);
				logger.info("Security certificate is not appeared in IOS");
			}
		}
		Capabilities caps = ((RemoteWebDriver) getDriver()).getCapabilities();
		String browserName = caps.getBrowserName();
		if ("internet explorer".equalsIgnoreCase(browserName))
			checkPageIsReady();
		if (CollectionUtils.isNotEmpty(continueToWebsiteButton)
				&& !CollectionUtils.sizeIsEmpty(continueToWebsiteButton)) {
			clickElement(continueToWebsiteButton);
		}
	}

	public void clickOnAllowPopUp() {
		if (getDriver() instanceof IOSDriver) {
			String currentIosContext = ((AppiumDriver<?>) getDriver()).getContext();
			try {
				((AppiumDriver<?>) getDriver()).context("NATIVE_APP");
				waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@label='Allow']")));
				((AppiumDriver<?>) getDriver()).findElement(By.xpath("//*[@label='Allow']")).click();
				((AppiumDriver<?>) getDriver()).context(currentIosContext);
				Set<String> contextView = ((AppiumDriver) getDriver()).getContextHandles();
				ArrayList<String> s = new ArrayList<String>(contextView);
				((AppiumDriver<?>) getDriver()).context(s.get(contextView.size() - 1));
			} catch (Exception e) {
				((AppiumDriver<?>) getDriver()).context(currentIosContext);
				logger.info("Allow Poup is not appeared in IOS");
			}
		}
	}

	/**
	 * Refresh A page
	 * 
	 * 
	 */
	public void refreshPage() {
		getDriver().navigate().refresh();
		Reporter.log("Page have been refreshed");
	}

	public void scrollToElement(WebElement element) {
		((JavascriptExecutor) getDriver()).executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public void doubleClick(WebElement element) {
		try {
			action = new Actions(getDriver());
			action.doubleClick(element).build().perform();
		} catch (Exception e) {
			e.getMessage().contains("NoSuchElementException");
			Reporter.log(">>FAILED::Not found " + element.getText());
		}
	}

	public void acceptCertificateIE() {
		try {
			if ((getDriver() instanceof RemoteWebDriver)) {
				checkPageIsReady();
				if ("Certificate Error: Navigation Blocked".equalsIgnoreCase(getDriver().getTitle())
						&& btnCertificateButton.isDisplayed()) {
					waitFor(ExpectedConditions.visibilityOf(btnCertificateButton));
					btnCertificateButton.click();
				}
			}
		} catch (Exception e) {
		}
	}

	public boolean verifyElementByText(WebElement element, String message) {
		boolean elementText = false;
		waitFor(ExpectedConditions.visibilityOf(element));
		String elementTxt = element.getText();
		if (elementTxt.contains(message)) {
			elementText = true;
		}
		return elementText;
	}

	/**
	 * verify Element By Text
	 * 
	 * @param elements
	 * @param link
	 * @return
	 */
	public boolean verifyListOfElementsBytext(List<WebElement> elements, String text) {
		boolean isElementDisplayed = false;
		for (WebElement webElement : elements) {
			if (webElement.getText().contains(text)) {
				isElementDisplayed = true;
				break;
			}
		}
		return isElementDisplayed;
	}

	public String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd");
		Calendar calobj = Calendar.getInstance();
		calobj.add(Calendar.DATE, 1);
		return dateFormat.format(calobj.getTime());

	}

	public void waitforSpinner() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("circle")));
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("circle-clipper left")));
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("circle-clipper right")));
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("i[class*='fa fa-spin fa-spinner']")));
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("ajax_loader")));
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("alertsLoader")));
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("spinner-layer")));
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("bb-loader-icon")));
	}

	/**
	 * Generating random number
	 * 
	 * @return value
	 * @param elements
	 */
	public static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

	/**
	 * Get Text from Element [List Of elements]
	 * 
	 * @return value
	 * @param elements
	 */
	public String getTextUponDisplay(List<WebElement> elements) {
		for (WebElement webElement : elements) {
			if (webElement.isDisplayed()) {
				return webElement.getText();
			}
		}
		return StringUtils.EMPTY;
	}

	public boolean verifyCurrentUrl(String url) {
		checkPageIsReady();
		String currentUrl = getDriver().getCurrentUrl();
		if (currentUrl.contains(url)) {
			Reporter.log("Navigated to expected URL " + url);
			return true;
		} else {
			Assert.fail("Failed to navigate to expected URL");
			return false;
		}

	}

	public void switchToSecondWindow() {
		if (!(getDriver() instanceof IOSDriver)) {
			String mainWindow = getDriver().getWindowHandle();
			for (String handle : getDriver().getWindowHandles()) {
				if (!handle.equals(mainWindow)) {
					getDriver().switchTo().window(handle);
				}
			}
		}
	}

	public void switchToSecondWindowInMobile() {
		String mainWindow = getDriver().getWindowHandle();
		for (String handle : getDriver().getWindowHandles()) {
			if (!handle.equals(mainWindow)) {
				getDriver().switchTo().window(handle);
				break;
			}

		}
	}

	public void switchToHomeWindow() {
		for (String handle : getDriver().getWindowHandles()) {
			getDriver().switchTo().window(handle);
			String title = getDriver().switchTo().window(handle).getTitle();
			if (title.contains("My T-Mobile | Home"))
				getDriver().switchTo().window(handle);
		}
	}

	public void verifyCurrentPageURL(String pageName, String urlTextToVerify) {
		try {
			waitFor(ExpectedConditions.urlContains(urlTextToVerify));
			Assert.assertTrue(getDriver().getCurrentUrl().contains(urlTextToVerify), pageName + " is not loaded");
			Reporter.log(pageName + " is displayed");

		} catch (Exception e) {
			Reporter.log(pageName + " is not displayed");
			Assert.fail(pageName + " is not displayed");
		}
	}

	/**
	 * Click On Security Alert
	 */
	@SuppressWarnings("rawtypes")
	public void siteAttemptingPopUpAlertAccept() {
		if (getDriver() instanceof AppiumDriver) {
			String currentIosContext = ((AppiumDriver) getDriver()).getContext();
			try {
				((AppiumDriver) getDriver()).context("NATIVE_APP");
				waitFor(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@label='Allow']")));
				((AppiumDriver) getDriver()).findElement(By.xpath("//*[@label='Allow']")).click();
				((AppiumDriver) getDriver()).context(currentIosContext);
			} catch (Exception e) {
				((AppiumDriver) getDriver()).context(currentIosContext);
				logger.info("Pop up is not appeared in IOS");
			}
		}
	}

	public boolean verifyElementText(WebElement element, String message) {
		boolean text = false;
		waitFor(ExpectedConditions.visibilityOf(element));
		String Txt = element.getText();
		if (Txt.equalsIgnoreCase(message)) {
			text = true;
		}
		return text;
	}

	/**
	 * Click On Contact us Img
	 * 
	 * @return
	 */
	public CommonPage clickOnContactUsImg() {
		checkPageIsReady();
		try {
			contactUsImg.click();
			Reporter.log("Clicked on Contact US icon");
		} catch (Exception e) {
			Reporter.log("Contact US icon is not clicked");
		}
		return this;
	}

	/**
	 * Click on back button
	 * 
	 * @return
	 */
	public CommonPage clickOnBackBtn() {
		try {
			backBtn.click();
			Reporter.log("Back arrow is displayed on Review Page");
		} catch (Exception e) {
			Assert.fail("Back arrow is not displayed on Review Page");
		}
		return this;
	}

	/**
	 * Click On T-Mobile Icon
	 */
	public CommonPage clickOnTMobileIcon() {
		try {
			tmobileIcon.click();
			Reporter.log("Clicked on T-Mobile Icon on Data Pass Review page");
		} catch (Exception e) {
			Assert.fail("T-Mobile Icon is not found");
		}
		return this;
	}

	/**
	 * Get dynamic locator resolved to normal one
	 *
	 * @param locator     - locator that needs to be replaced
	 * @param dynamicText - text that is dynamic in the locator
	 * @return By - new locator after placing required text in the locator string
	 */

	/**
	 * Current Window Title
	 */
	public String getCurrentWindowTitle() {
		checkPageIsReady();
		String currentWindow = getDriver().getWindowHandle();
		for (String winHandle : getDriver().getWindowHandles()) {
			if (!winHandle.equalsIgnoreCase(currentWindow)) {
				getDriver().switchTo().window(winHandle);
				break;
			}
		}
		return getDriver().getTitle();
	}

	/**
	 * return webElement text count
	 */
	public int getWebElementTextCount(WebElement element) {
		checkPageIsReady();
		String actualText = element.getText();
		System.out.println(actualText);
		return actualText.length();
	}

	/**
	 * boolean text count
	 */
	public boolean verifyHeaderCharacterLimit(int actuals, int Expected) {
		return actuals <= Expected;
	}

	public WebElement findElement(By locater) {

		try {
			return getDriver().findElement(locater);
		} catch (NoSuchElementException e) {
			logger.error(e.getMessage());
			throw new FrameworkException("No Such element element{}" + e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new FrameworkException("Exception occured during finding element{}" + e.getMessage());
		}
	}

	/**
	 * Get dynamic locator resolved to normal one
	 *
	 * @param locator     - locator that needs to be replaced
	 * @param dynamicText - list of dynamic texts that need to be substituted in
	 *                    dynamic locator
	 * @return By - new locator after placing required texts in the locator string
	 */
	public static By getNewLocator(By locator, String... dynamicText) {
		String locatorType = locator.toString().split(": ")[0].split("\\.")[1];
		String newLocatorString = String.format(locator.toString().split(": ")[1], dynamicText);
		switch (locatorType) {
		case "xpath":
			locator = By.xpath(newLocatorString);
			break;
		case "cssSelector":
			locator = By.cssSelector(newLocatorString);
			break;
		case "id":
			locator = By.id(newLocatorString);
			break;
		case "className":
			locator = By.className(newLocatorString);
			break;
		case "name":
			locator = By.name(newLocatorString);
			break;
		case "linkText":
			locator = By.linkText(newLocatorString);
			break;
		case "partialLinkText":
			locator = By.partialLinkText(newLocatorString);
			break;
		case "tagName":
			locator = By.tagName(newLocatorString);
			break;
		}
		return locator;
	}

	/**
	 * Click Element By Text
	 * 
	 * @param elements
	 * @param
	 */
	public void randomElementClick(List<WebElement> elements) {
		try {
			for (WebElement webElement : elements) {
				elementClick(webElement);
			}
		} catch (Exception e) {
			Reporter.log("FAILED::Exception occured waiting for the element " + elements.toString());
		}
	}

	/**
	 * Check Element is enabled or not
	 * 
	 * @param element
	 * @return
	 */
	public boolean isElementEnabled(WebElement element) {
		try {
			waitFor(ExpectedConditions.visibilityOf(element));
			return element.isEnabled();
		} catch (Exception e) {
			Reporter.log("webelement is in disable state" + element.toString());
			return false;
		}
	}

	public boolean waitforSpinnerinMarketingCommunications() {
		List<WebElement> elements = getDriver().findElements(By.cssSelector(".spinner-loader"));
		try {
			waitFor(ExpectedConditions.invisibilityOfAllElements(elements), 120);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean waitforSpinnerinProfilePage() {
		if (!(getDriver() instanceof AppiumDriver)) {
			List<WebElement> elements = getDriver().findElements(By.cssSelector(".circle"));
			try {
				waitFor(ExpectedConditions.invisibilityOfAllElements(elements), 120);
				return true;
			} catch (Exception e) {
				Reporter.log("Spinner reached to inifinite state");
				Assert.fail("Spinner reached to inifinite state " + e.getMessage());
				return false;
			}
		} else {
			return true;
		}
	}

	public boolean waitforSpinnerinTMobileIDPage() {
		List<WebElement> elements = getDriver().findElements(By.cssSelector("img.spinner.whiteBg"));
		try {
			waitFor(ExpectedConditions.invisibilityOfAllElements(elements), 120);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * Click On Element
	 * 
	 * @param element
	 */
	public void clickElement(WebElement element) {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(element));
			elementClick(element);
		} catch (Exception e) {
			if (e.getMessage().contains("NoSuchElementException"))
				Reporter.log(">>FAILED::Not found " + "'" + element.getText() + "'");
		}
	}

	/**
	 * Check Element Is Displayed Or Not
	 * 
	 * @param element
	 * @return
	 */
	public boolean isElementDisplayed(WebElement element) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(element));
			}
			waitFor(ExpectedConditions.visibilityOf(element), 3);
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public String changeDateFormate(String date) {
		String mdy = null;
		try {
			SimpleDateFormat outFormat = new SimpleDateFormat("MMM d, yyyy");
			SimpleDateFormat inFormat = new SimpleDateFormat("MM/dd/yyyy");
			Date dateString = inFormat.parse(date);
			System.out.println(date);
			mdy = outFormat.format(dateString);
			System.out.println("mdy" + mdy);
		} catch (Exception ex) {
		}
		return mdy;
	}

	/**
	 * Wait For Spinner
	 * 
	 * @return
	 */
	public boolean waitForNewSpinner() {
		List<WebElement> elements = getDriver().findElements(By.cssSelector("div.circle-clipper.right div.circle"));
		try {
			waitFor(ExpectedConditions.invisibilityOfAllElements(elements), 120);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * verify login page title
	 * 
	 * @return boolean
	 */
	public CommonPage verifyCurrentPageTitle(String title) {
		checkPageIsReady();
		Assert.assertTrue(getDriver().getTitle().contains(title), " " + title + " is not displayed");
		Reporter.log("" + title + "is displayed");
		return this;
	}

	public CommonPage clickOnlogOutBtn() {
		logOutBtn.click();
		Reporter.log("Clicked on logOut button");
		return this;
	}

	/**
	 * Navigate to previous page.
	 */
	public CommonPage navigateToPreviousPage() {
		try {
			waitforSpinnerinProfilePage();
			getDriver().navigate().back();
			Reporter.log("Navigation to previous page is success");
		} catch (Exception e) {
			Assert.fail("Navigation to previous page is success" + e.toString());
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public CommonPage verifyPageUrl(String pageUrl) {
		try {
			waitFor(ExpectedConditions.urlContains(pageUrl));
		} catch (Exception e) {
			Reporter.log("URL containing " + pageUrl + " page not loaded ");
			Assert.fail("URL containing " + pageUrl + " page not loaded ");
		}
		return this;
	}

	/**
	 * Verify toggle status.
	 */
	public String getToggleStatus(WebElement element1, WebElement element2, WebElement element3) {
		waitforSpinnerinProfilePage();
		if (isElementDisplayed(element1)) {
			return "ON";
		}
		if (isElementDisplayed(element2)) {
			return "OFF";
		}
		if (isElementDisplayed(element3)) {
			return "DISABLED";
		}
		return null;
	}

	/**
	 * Verify toggle operations.
	 */
	public void verifyToggleOpeartions(WebElement element1, WebElement element2, WebElement element3,
			String toggleStatus, WebElement toggle) {
		switch (toggleStatus) {
		case "ON":
			toggle.click();
			waitforSpinnerinProfilePage();
			String toggleStatus2 = getToggleStatus(element1, element2, element3);
			Assert.assertNotEquals(toggleStatus, toggleStatus2, "Status not updated");
			Reporter.log("Toggle is turned OFF and status is updated successfully");
			break;
		case "OFF":
			toggle.click();
			waitforSpinnerinProfilePage();
			String toggleStatus3 = getToggleStatus(element1, element2, element3);
			Assert.assertNotEquals(toggleStatus, toggleStatus3, "Status not updated");
			Reporter.log("Toggle is turned ON and status is updated successfully");
			break;
		case "DISABLED":
			Reporter.log("Toggle is in disabled state");
		}
	}

	/**
	 * Compare the pii Name and its Id Sequence Number
	 * 
	 * @param pid        list Element
	 * @param pii        Masking Element
	 * @param true/false
	 */
	public boolean comparePidandIndex(WebElement pidAttribute, String pidName) {
		boolean bFlag = false;
		try {

			if (pidAttribute.getAttribute("pid").contains(pidName) && Character
					.isDigit(pidAttribute.getAttribute("pid").charAt(pidAttribute.getAttribute("pid").length() - 1))) {
				bFlag = true;
			}
		} catch (Exception e) {
			Assert.fail("PII element verfitcaion fail to validate");
		}
		return bFlag;
	}

	/**
	 * Check PII Masking for all personal and payment information
	 * 
	 * @param pidElement
	 * @param piiMasking
	 * @return true/false
	 */
	public boolean checkElementisPIIMasked(WebElement pidElement, String piiMasking) {
		boolean isMasked = false;
		try {
			String pidAttr = pidElement.getAttribute("pid");
			if (!pidAttr.isEmpty() && pidAttr.contains(piiMasking) && pidAttr.matches(piiMasking + "\\d+")) {
				isMasked = true;
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking");
		}
		return isMasked;
	}

	public boolean retryingFindClick(WebElement ele) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 5) {
			try {
				checkPageIsReady();
				clickElement(ele);
				result = true;
				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}
		return result;
	}

	/***
	 * get price value
	 * 
	 */
	public String getPriceValue(WebElement element) {
		String priceValue = null;
		String tradeInHeaderText = element.getText();
		String[] trimmedText = tradeInHeaderText.split(" ");
		for (String value : trimmedText) {
			if (value.contains("$")) {
				priceValue = value;
				break;
			}
		}
		return priceValue.trim();
	}

	/**
	 * verify update operation failed dialog box
	 */
	public CommonPage verifyUpdateOperationFailedDialogBox() {
		try {
			Assert.assertFalse(isElementDisplayed(updateOperationDialogBox), "Update operation dialog box displayed");
			Reporter.log("Update operation dialog box not displayed");
		} catch (AssertionError e) {
			Reporter.log("Update operation dialog box displayed");
			Assert.fail("Update operation dialog box displayed");
		}
		return this;
	}

	/**
	 * Verify updated BillingAddress.
	 */
	public CommonPage verifyUpdatedBillingAddress(String addressLine, String address) {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			if (getDriver() instanceof AppiumDriver) {
				WebElement spinner = getDriver().findElement(By.cssSelector(".overlay:not(.overlay-out)"));
				waitFor(ExpectedConditions.invisibilityOf(spinner));
				moveToElement(activeBreadCrumb);
				WebElement webElement = getDriver().findElement(By.xpath(
						"//p[@class='Display6'and contains(text()," + "'" + address + " Address" + "'" + ")]/../p[2]"));
				scrollToElement(webElement);
				Assert.assertTrue(webElement.getText().contains(addressLine), "Updating billing address failed");
			} else {
				String updatedAddressLine = getDriver().findElement(By.xpath(
						"//p[@class='Display6'and contains(text()," + "'" + address + " Address" + "'" + ")]/../p[2]"))
						.getText();

				Assert.assertTrue(updatedAddressLine.contains(addressLine), "Updating billing address failed");
			}
			Reporter.log(address + " address updated successfully");
		} catch (Exception e) {
			Reporter.log("Updating " + address + "address failed");
			Assert.fail("Updating " + address + "address failed");
		}
		return this;
	}

	/**
	 * Update e911BillingAddress.
	 */
	public String updateBillingAddress(MyTmoData myTmoData) {
		try {
			// checkPageIsReady();
			waitforSpinnerinProfilePage();
			String s1 = addressLine1.getAttribute("value").trim();
			String s2 = myTmoData.getAddressLine2().trim();
			if (!(s1.contains(s2))) {
				addressLine1.clear();
				addressLine1.sendKeys(myTmoData.getAddressLine2());
				city.clear();
				city.sendKeys(myTmoData.getCity2());
				// waitForElement(stateDropDownToggle);
				stateDropDownToggle.click();
				// clickElement(getDriver().findElement(By.xpath("//a[text()=' "
				// + myTmoData.getState2() + " ']")));
				zipCode.clear();
				zipCode.sendKeys(myTmoData.getZipCode2());
				return myTmoData.getAddressLine2();
			} else {
				addressLine1.clear();
				addressLine1.sendKeys(myTmoData.getAddressLine1());
				city.clear();
				city.sendKeys(myTmoData.getCity1());
				// waitForElement(stateDropDownToggle);
				stateDropDownToggle.click();
				// Because of below line build is failing. Please fix it
				// clickElement(getDriver().findElement(By.xpath("//a[text()=' "
				// + myTmoData.getState2() + " ']")));
				zipCode.clear();
				zipCode.sendKeys(myTmoData.getZipCode1());
				return myTmoData.getAddressLine1();
			}
		} catch (Exception e) {
			Assert.fail("Updating billing address failed" + e.toString());
		}
		return null;
	}

	/**
	 * Click bread crumb
	 **/
	public void clickBreadCrumb(String string) {
		try {
			WebElement webElement = getDriver().findElement(By.xpath("//*[contains(text()," + "string" + ")]"));
			waitFor(ExpectedConditions.visibilityOf(webElement));
			clickElementWithJavaScript(getDriver().findElement(By.xpath("//*[contains(text()," + "string" + ")]")));
		} catch (Exception e) {
			Reporter.log("Click on bread crumb " + string + " failed");
			Assert.fail("Click on bread crumb " + string + " failed");
		}
	}

	/**
	 * Click bread crumb
	 **/
	public void clickProfileHomeBreadCrumb() {
		try {
			waitFor(ExpectedConditions.visibilityOf(profileBreadCrumb));
			clickElementWithJavaScript(profileBreadCrumb);
			waitforSpinnerinProfilePage();
		} catch (Exception e) {
			Reporter.log("Click on profile bread crumb failed");
			Assert.fail("Click on profile bread crumb failed");
		}
	}

	/**
	 * Click bread crumb
	 **/
	public void verifyBreadCrumbActiveStatus(String breadCrumb) {
		try {
			waitFor(ExpectedConditions.visibilityOf(activeBreadCrumb));
			activeBreadCrumb.isDisplayed();
			Reporter.log(breadCrumb + "bread crumb is active");
		} catch (Exception e) {
			Reporter.log(breadCrumb + " is not available");
			Assert.fail(breadCrumb + " is not available");
		}
	}

	/**
	 * Verify CSRF token for all services
	 * 
	 * @return
	 */
	public CommonPage verifyCSRFToken(WebDriver driver) {
		try {
			List<String> t_values = new ArrayList<String>();
			List<LogEntry> entries = driver.manage().logs().get(LogType.PERFORMANCE).getAll();
			System.out.println("entries---------" + entries.size());
			for (LogEntry entry : entries) {
				String service_String = entry.getMessage();
				String token_Value;
				ObjectMapper mapper = new ObjectMapper();
				if (service_String.contains("eservice/servlet") && service_String.contains("requestWillBeSent")) {

					System.out.println("Request " + service_String);
					JsonNode jsonNode = mapper.readTree(service_String);
					JsonNode deviceFamilyInfoPathNode = jsonNode.path("message").path("params").path("request")
							.path("headers");
					System.out.println("request token is " + deviceFamilyInfoPathNode.get("x-xsrf-token"));
					if (deviceFamilyInfoPathNode.get("x-xsrf-token") == null) {
						token_Value = deviceFamilyInfoPathNode.get("X-XSRF-TOKEN").toString();
					} else {
						token_Value = deviceFamilyInfoPathNode.get("x-xsrf-token").toString();
					}
					t_values.add(token_Value);
				} else if (service_String.contains("eservice/servlet") && service_String.contains("responseReceived")) {
					System.out.println("Response " + service_String);
					JsonNode jsonNode = mapper.readTree(service_String);
					JsonNode deviceFamilyInfoPathNode = jsonNode.path("message").path("params").path("response")
							.path("requestHeaders");
					System.out.println("response token is " + deviceFamilyInfoPathNode.get("x-xsrf-token"));
					if (deviceFamilyInfoPathNode.get("x-xsrf-token") == null) {
						token_Value = deviceFamilyInfoPathNode.get("X-XSRF-TOKEN").toString();
					} else {
						token_Value = deviceFamilyInfoPathNode.get("x-xsrf-token").toString();
					}
					t_values.add(token_Value);
				}
				int j = t_values.size();
				while (j > 1) {
					if (!((t_values.get(j - 1)).equals(t_values.get(j - 2)))) {
						Reporter.log("CSRF token is not same for all the services");
					}
					j--;
				}
			}
		} catch (Exception e) {
			Assert.fail("CSRF token is not same for all the services ");
		}
		return this;
	}

	public CommonPage isFramePresent(WebElement frame) {
		try {
			checkPageIsReady();
			waitforSpinnerinTMobileIDPage();
			WebDriverWait wait = new WebDriverWait(getDriver(), 300);
			wait.ignoring(StaleElementReferenceException.class).ignoring(WebDriverException.class)
					.until(ExpectedConditions.visibilityOf(frame));
		} catch (Exception e) {
			Reporter.log("TMobileID page frame not loaded");
			Assert.fail("TMobileID page frame not loaded ");
		}
		return this;
	}

	public void closeNewWindow() {
		if (!(getDriver() instanceof IOSDriver)) {
			String currentWindow = getDriver().getWindowHandle();
			for (String winHandle : getDriver().getWindowHandles()) {
				if (winHandle.equalsIgnoreCase(currentWindow)) {
					getDriver().switchTo().window(winHandle).close();
				}
			}
		}
	}

	public boolean removeNetflixSocByName(List<WebElement> activeServiceList, String netFlixSocName) {
		boolean isSocSelected = true;

		if (!activeServiceList.isEmpty()) {
			ManageAddOnsSelectionPage manageAddOnsSelectionPage = new ManageAddOnsSelectionPage(getDriver());
			manageAddOnsSelectionPage.uncheckActiveChekBox();
			// manageAddOnsSelectionPage.unCheckCheckBox(netFlixSocName);
			manageAddOnsSelectionPage.verifyWarningMsgRemovingNetFlixisDisplayed(
					"Removing Netflix will result in the instant removal of T-Mobile as your method of payment unless you move to a new Netflix streaming plan. We also suggest checking any previous methods of payment on file with Netflix since this change could cause potential charges.");

			manageAddOnsSelectionPage.verifyContinueRemovalBtnisDisplayed();
			// manageAddOnsSelectionPage.clickOnConflictContinue();
			manageAddOnsSelectionPage.clickOnContinueBtn();

			ODFDataPassReviewPage oDFDataPassReviewPage = new ODFDataPassReviewPage(getDriver());
			oDFDataPassReviewPage.verifyReviewPageOfManageDataAndAddOnsPage();
			oDFDataPassReviewPage.agreeAndSubmitButton();

			ManageAddOnsConfirmationPage newODFConfirmationPage = new ManageAddOnsConfirmationPage(getDriver());
			newODFConfirmationPage.verifyODFConfirmationPage();
			isSocSelected = false;
		}
		return isSocSelected;
	}

	/**
	 * This will navigate to bookmark url
	 * 
	 * @return
	 */
	public CommonPage navigateToBookMarkedDashBoardURL(String bookmarkURL) {
		try {
			getDriver().navigate().to(bookmarkURL);
		} catch (Exception e) {
			Assert.fail("Unable to navigate to bookmark url");
		}
		return this;
	}

	/**
	 * Click on call me phone icon
	 */
	public CommonPage clickCallUsMenu() {
		try {
			checkPageIsReady();
			callUsIcon.click();
			Reporter.log("Click on call us is success");
		} catch (Exception e) {
			Reporter.log("Click on call us failed");
		}
		return this;
	}

	public void verifyCurrentPageTitle(String pageName, String urlTextToVerify) {
		try {
			waitFor(ExpectedConditions.titleContains(urlTextToVerify));
			Assert.assertTrue(getDriver().getTitle().contains(urlTextToVerify), pageName + " is not loaded");
			Reporter.log(pageName + " is displayed");

		} catch (Exception e) {
			Reporter.log(e.getMessage().trim());
			Reporter.log(pageName + " is not displayed");
		}
	}

	public boolean waitforSpinnerinPrepaidPage() {
		if (!(getDriver() instanceof AppiumDriver)) {
			List<WebElement> elements = getDriver().findElements(By.cssSelector("div.fa.fa-pulse"));
			try {
				waitFor(ExpectedConditions.invisibilityOfAllElements(elements), 120);
				return true;
			} catch (Exception e) {
				Reporter.log("Spinner reached to inifinite state");
				Assert.fail("Spinner reached to inifinite state " + e.getMessage());
				return false;
			}
		} else {
			return true;
		}
	}

	/**
	 * Click on Header Back arrow button
	 * 
	 * @return
	 */
	public CommonPage clickOnHeaderBackBtn() {
		try {
			headerBackButton.click();
			Reporter.log("Back arrow button is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Back arrow button");
		}
		return this;
	}

	/**
	 * MoveToElement With Actions Class
	 * 
	 * @param element
	 */
	public void scrollUp() {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		js.executeScript("window.scrollBy(0,1000)");
	}

	public String generateRandomString() {
		String alphabet = "abcdefghijklmnopqrstuvwxyz";
		String s = "";
		Random random = new Random();
		int randomLen = 1 + random.nextInt(15);
		for (int i = 0; i < randomLen; i++) {
			char c = alphabet.charAt(random.nextInt(26));
			s += c;
		}

		return s;
	}

	public void verifyAjaxSpinnerInvisible() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#spinner")));
			Reporter.log("Ajax Spinner is invisible");
		} catch (Exception e) {
			Assert.fail("Page has not loaded");
		}
	}

	/**
	 * Method verifying duplicates of webElements from classes
	 * 
	 * @param objClass
	 */
	public void verifyDuplicatedElements(Class<?> objClass) {
		try {
			Map<WebElement, String> elemnts = new HashMap<WebElement, String>();
			List<String> elementsNotFound = new ArrayList<String>();
			Field[] fields = objClass.getDeclaredFields();

			Reporter.log("-----------------------------------------------------------------------------");
			Reporter.log("Test Class: " + getClass().getSimpleName());
			Reporter.log("-----------------------------------------------------------------------------");

			for (Field field : fields) {
				String name = field.getName();
				field.setAccessible(true);
				if (field.getGenericType().toString().contains("WebElement")) {
					if (field.getGenericType().toString().contains("List")) {
						List<WebElement> listOfWebElements = (List<WebElement>) field.get(this);
						if (listOfWebElements.size() > 0) {
							WebElement elemfromList = listOfWebElements.get(0);
							if (!elemnts.containsKey(elemfromList)) {
								elemnts.put(elemfromList, name);
							} else {
								Reporter.log("Duplicates are: <i>" + name + "</i> and <i>" + elemnts.get(elemfromList)
										+ "</i>");
//								System.out.println("duplicates are :" + name + ":  " + elemnts.get(elemfromList));
							}
						} else {
							elementsNotFound.add(name);
						}
					} else {
						WebElement w = getelement((WebElement) field.get(this));
						if (w != null) {
							if (!elemnts.containsKey(w)) {
								elemnts.put(w, name);
							} else {
								Reporter.log("Duplicates are: <i>" + name + "</i> and <i>" + elemnts.get(w) + "</i>");
//								System.out.println("duplicates are :" + name + ":  " + elemnts.get(w));
							}
						} else {
							elementsNotFound.add(name);
						}
					}
				}
			}
			Reporter.log("List of elements, which are not found in the page:");
//			elementsNotFound.forEach(Reporter::log);
			for (String elemName : elementsNotFound) {
				Reporter.log(" -" + elemName);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public WebElement getelement(WebElement w) {
		try {
			w.isDisplayed();
		} catch (Exception e) {
			return null;
		}
		return w;
	}

	public String getTextMessagePin(String msidn) {
		logger.info("accountVerification method called");

		String smsPin = null;
		RequestData requestData = new RequestData();
		String environment = System.getProperty(Constants.IAM_SERVER_ENVIRONEMT).toLowerCase();
		requestData.setMsisdn(msidn);
		requestData.setEnvironment(environment);
		requestData.setProfileType("TMO");

		long startTime = System.currentTimeMillis();
		logger.info("startTime::::::{}", startTime);
		try {
			smsPin = new RestServiceHelper().getTextMessagePin(requestData);
		} catch (Exception e) {
			System.out.println("Unable to get Text message pin from service");
		}
		long stopTime = System.currentTimeMillis();
		logger.info("stopTime::::::{}", stopTime);
		long elapsedTime = stopTime - startTime;
		logger.info("elapsedTime::::::{}", elapsedTime);
		return smsPin;
	}

	public void takeScreenshot() {
		try {
			String url = getDriver().getCurrentUrl();
			File scrFile = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
			scrFile = appendUrlToImage(url, scrFile);
			String encodedFileString = encodeFileToBase64Binary(scrFile);
			String imgSrc = "data:image/png;base64," + encodedFileString;
			Reporter.log("<img width='100' height='100' src=" + imgSrc + " onclick=toggleImage(this.src);> ");
		} catch (Exception e) {
			L.error("Error while capturing screenshot: " + e.getCause());
		}

	}

	private static String encodeFileToBase64Binary(File file) {
		String encodedfile = null;
		try {
			FileInputStream fileInputStreamReader = new FileInputStream(file);
			byte[] bytes = new byte[(int) file.length()];
			fileInputStreamReader.read(bytes);
			encodedfile = new String(Base64.encodeBase64(bytes), "UTF-8");
			fileInputStreamReader.close();
		} catch (FileNotFoundException e) {
			L.info(e.getMessage());
		} catch (IOException e) {
			L.info(e.getMessage());
		}
		return encodedfile;
	}

	private File appendUrlToImage(String url, File scrFile) throws Exception {
		final BufferedImage image = ImageIO.read(scrFile);

		Graphics g = image.getGraphics();
		g.setFont(g.getFont().deriveFont(20f));
		g.setColor(Color.BLACK);
		g.drawString(url, 50, 50);
		g.dispose();
		ImageIO.write(image, "png", scrFile);
		return scrFile;
	}
}