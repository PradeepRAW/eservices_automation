package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class DeviceUnlockStatus extends ApiCommonLib {
	Response response = null;

	public Response getDeviceUnlockResponse(String requestBody) throws Exception {
		Map<String, String> headers = deviceUnlockStatusHeaders();
		RestService service = new RestService(requestBody, "https://eos.corporate.t-mobile.com");
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		reqSpec.setTrustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("/myPhone/deviceUnlockStatus", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}

	public Response getDeviceDetailsResponse(String requestBody) throws Exception {
		Map<String, String> headers = deviceUnlockStatusHeaders();
		RestService service = new RestService(requestBody, "https://eos.corporate.t-mobile.com");
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		reqSpec.setTrustStore(System.getProperty("user.dir") + "/src/test/resources/cacerts", "changeit");
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("/myPhone/getDevices", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}

	private Map<String, String> buildPhonePageAPIHeader(ApiTestData apiTestData, String jwtToken) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("Authorization", "Bearer " + jwtToken);
		headers.put("interactionId", "1234");
		headers.put("application-id", "MYTMO");

		return headers;
	}

	public Response getDeviceDtlsIMEI(ApiTestData apiTestData, String jwtToken) throws Exception {
		Map<String, String> headers = buildPhonePageAPIHeader(apiTestData, jwtToken);
		String resourceURL = "https://eos.corporate.t-mobile.com/myPhone/deviceDetails";
		Response response = invokeRestServiceCall(headers, "", resourceURL, "", RestServiceCallType.GET);
		return response;
	}

	private Map<String, String> deviceUnlockStatusHeaders() throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", "abc");
		headers.put("application_id", "ESERVICE");
		headers.put("channel_id", "WEB");
		headers.put("interactionId", "123456787");
		headers.put("sender_id", "MYTMO");
		headers.put("Content-Type", "application/json");
		return headers;
	}

	public DeviceUnlockStatus checkjsontagitems(Response response, String tag) {

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();
			try {
				Assert.assertTrue(jsonPathEvaluator.get(tag) != null, tag + " value is " + tag + " is existing");
				Reporter.log(tag + " is existing");
			} catch (Exception e) {
				Assert.fail(tag + " is not existing");
			}
		}

		return this;
	}

	public DeviceUnlockStatus checkMutipleExpectedvalues(Response response, Map<String, String> tagAndExpectedValues) {
		Boolean check = false;
		JsonPath jsonPathEvaluator = response.jsonPath();
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			Set<String> key = tagAndExpectedValues.keySet();
			Iterator<String> itr = key.iterator();
			while (itr.hasNext()) {
				String tagName = itr.next();
				if (jsonPathEvaluator.get(tagName) != null) {
					Boolean checkExpectedValue = tagAndExpectedValues.get(tagName).contains("!");
					if (checkExpectedValue) {
						int size = tagAndExpectedValues.get(tagName).length();
						if (!jsonPathEvaluator.get(tagName).toString()
								.contains(tagAndExpectedValues.get(tagName).substring(1, size))) {
							Assert.assertTrue(true, tagName + " value is " + jsonPathEvaluator.get(tagName).toString());
							Reporter.log(tagName + " value is " + jsonPathEvaluator.get(tagName).toString());
							check = true;
							break;
						}
					} else {
						if (jsonPathEvaluator.get(tagName).toString().contains(tagAndExpectedValues.get(tagName))) {
							Assert.assertTrue(true, tagName + " value is " + jsonPathEvaluator.get(tagName).toString());
							Reporter.log(tagName + " value is " + jsonPathEvaluator.get(tagName).toString());
							check = true;
							break;
						}
					}
				}
			}
		}

		if (!check) {
			Assert.fail("Tag value doesnot match");
		}

		return this;
	}

}
