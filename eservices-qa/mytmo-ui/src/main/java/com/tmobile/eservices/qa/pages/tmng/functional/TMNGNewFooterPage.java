package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.UNAVCommonPage;
import io.appium.java_client.AppiumDriver;

/**
 * This page contains all the UNAV methods
 * 
 * @author sputti2
 *
 */
public class TMNGNewFooterPage extends UNAVCommonPage {

	/**
	 * 
	 * @param webDriver
	 */
	public TMNGNewFooterPage(WebDriver webDriver) {
		super(webDriver);
	}
	// Verify UNAV Footer - Top

	/**
	 * Verify Footer Title
	 * 
	 * @return
	 */
	public TMNGNewFooterPage verifyFooterTitle() {
		try {
			Assert.assertTrue(isElementDisplayed(tmobile_title));
			Assert.assertTrue(tmobile_title.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_tmobile_title));
			Reporter.log(tmobile_title.getText()+" is displayed");
		} catch (Exception e) {
			Assert.fail(tmobile_title.getText()+" is not dispalyed");
		}
		return this;
	}
	
	/**
	 * Verify English Language
	 * 
	 * @return
	 */
	public TMNGNewFooterPage verifyEnglishLanguage() {
		try {
			moveToElement(footer_link_top_0);
			Assert.assertTrue(isElementDisplayed(language_0));
			Assert.assertTrue(language_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_language_0));
			Reporter.log(language_0.getText()+" is displayed");
		} catch (Exception e) {
			Assert.fail(language_0.getText()+" is not dispalyed");
		}
		return this;
	}

	/**
	 * Click English link
	 */
	public TMNGNewFooterPage clickEnglishlink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(language_0),60);
			clickElementWithJavaScript(language_0);
			Reporter.log("Clicked on English Language Link");
		} catch (Exception e) {
			Verify.fail("Click on English link failed ");
		}
		return this;
	}
	/**
	 * Verify Spanish Language
	 * 
	 * @return
	 */
	public TMNGNewFooterPage verifySpanishLanguage() {
		try {
			moveToElement(footer_link_top_0);
			Assert.assertTrue(isElementDisplayed(language_1));
			Assert.assertTrue(language_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_language_1));
			Reporter.log(language_1.getText()+" is displayed");
		} catch (Exception e) {
			Assert.fail(language_1.getText()+" is not dispalyed");
		}
		return this;
	}

	/**
	 * Click Spanish link
	 */
	public TMNGNewFooterPage clickSpanishlink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(language_1),60);
			clickElementWithJavaScript(language_1);
			Reporter.log("Clicked on Spanish Link");
		} catch (Exception e) {
			Verify.fail("Click on Spanish link failed ");
		}
		return this;
	}
	/**
	 * Verify Spanish T-Mobile page.
	 *
	 * @return the SpanishPage class instance.
	 */
	public TMNGNewFooterPage verifySpanishPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER_language_1_LINK);
			Reporter.log("T-Mobile Spanish page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("T-Mobile Spanish page not displayed");
		}
		return this;
	}	
	
	// Verify UNAV Footer - Social Links
	
	/**
	 * Click Instagram link
	 */
	public TMNGNewFooterPage clickInstagramlink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(socialLink0),60);
			clickElementWithJavaScript(socialLink0);
			Reporter.log("Clicked on Instagram Link");
		} catch (Exception e) {
			Verify.fail("Click on Instagram link failed ");
		}
		return this;
	}
	/**
	 * Verify Instagram T-Mobile page.
	 *
	 * @return the InstagramPage class instance.
	 */
	public TMNGNewFooterPage verifyInstagramPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER_Instagram_LINK);
			Reporter.log("T-Mobile Instagram page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("T-Mobile Instagram page not displayed");
		}
		return this;
	}
	/**
	 * Click Facebook link
	 */
	public TMNGNewFooterPage clickFacebooklink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(socialLink1),60);
			clickElementWithJavaScript(socialLink1);
			Reporter.log("Clicked on Facebook Link");
		} catch (Exception e) {
			Verify.fail("Click on Facebook link failed ");
		}
		return this;
	}
	/**
	 * Verify Facebook T-Mobile page.
	 *
	 * @return the FacebookPage class instance.
	 */
	public TMNGNewFooterPage verifyFacebookPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER_Facebook_LINK);
			Reporter.log("T-Mobile Facebook page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("T-Mobile Facebook page not displayed");
		}
		return this;
	}
	/**
	 * Click Twitter link
	 */
	public TMNGNewFooterPage clickTwitterlink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(socialLink2),60);
			clickElementWithJavaScript(socialLink2);
			Reporter.log("Clicked on Twitter Link");
		} catch (Exception e) {
			Verify.fail("Click on Twitter link failed ");
		}
		return this;
	}
	/**
	 * Verify Twitter T-Mobile page.
	 *
	 * @return the TwitterPage class instance.
	 */
	public TMNGNewFooterPage verifyTwitterPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER_Twitter_LINK);
			Reporter.log("T-Mobile Twitter page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("T-Mobile Facebook page not displayed");
		}
		return this;
	}
	/**
	 * Click YouTube link
	 */
	public TMNGNewFooterPage clickYouTubelink() {
		try {
			moveToElement(footer_link_top_0);
			waitFor(ExpectedConditions.visibilityOf(socialLink3),60);
			clickElementWithJavaScript(socialLink3);
			Reporter.log("Clicked on YouTube Link");
		} catch (Exception e) {
			Verify.fail("Click on YouTube link failed ");
		}
		return this;
	}
	/**
	 * Verify YouTube T-Mobile page.
	 *
	 * @return the YouTubePage class instance.
	 */
	public TMNGNewFooterPage verifyYouTubePage() {
		try {
			checkPageIsReady();
			verifyPageUrl(Constants.UNAV_FOOTER_Youtube_LINK);
			Reporter.log("T-Mobile YouTube page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("T-Mobile YouTube page not displayed");
		}
		return this;
	}
	// Verify UNAV Wireless Footer

		/**
		 * Verify Footer Phones and Devices Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPhoneDeviceLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_0));
				Assert.assertTrue(footerLabel_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_footerLabel_0));
				Reporter.log(footerLabel_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer Phones Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPhoneLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_0_0));
				Assert.assertTrue(subFooterLink_0_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_0_0));
				Reporter.log(subFooterLink_0_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_0_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Phones link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPhoneslink() {
			try {
				checkPageIsReady();
				subFooterLink_0_0.click();
				Reporter.log("Clicked on Phones Link");
			} catch (Exception e) {
				Reporter.log("Click on Phones failed");
				Assert.fail("Click on Phones failed");
			}
			return this;
		}

		/**
		 * Verify Phones page.
		 *
		 * @return the Phones Page class instance.
		 */
		public TMNGNewFooterPage verifyPhonesPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_0_0_LINK);
				Reporter.log("Phones Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Phones Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Tablets Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyTabletsLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_0_1));
				Assert.assertTrue(subFooterLink_0_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_0_1));
				Reporter.log(subFooterLink_0_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_0_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Tablets link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickTabletslink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_0_1);
				Reporter.log("Clicked on Tablets Link");
			} catch (Exception e) {
				Reporter.log("Click on Tablets failed");
				Assert.fail("Click on Tablets failed");
			}
			return this;
		}

		/**
		 * Verify Tablets page.
		 *
		 * @return the Tablets Page class instance.
		 */
		public TMNGNewFooterPage verifyTabletsPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_0_1_LINK);
				Reporter.log("Tablets Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Tablets Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Smart Watches Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifySmartWatchesLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_0_2));
				Assert.assertTrue(subFooterLink_0_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_0_2));
				Reporter.log(subFooterLink_0_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_0_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Smart Watches link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickSmartWatcheslink() {
			try {
				checkPageIsReady();
				subFooterLink_0_2.click();
				Reporter.log("Clicked on Smart Watches Link");
			} catch (Exception e) {
				Reporter.log("Click on Smart Watches failed");
				Assert.fail("Click on Smart Watches failed");
			}
			return this;
		}

		/**
		 * Verify Smart Watches page.
		 *
		 * @return the Smart Watches Page class instance.
		 */
		public TMNGNewFooterPage verifySmartWatchesPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_0_2_LINK);
				Reporter.log("Smart Watches Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Smart Watches Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Accessories Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyAccessoriesLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_0_3));
				Assert.assertTrue(subFooterLink_0_3.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_0_3));
				Reporter.log(subFooterLink_0_3.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_0_3.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Accessories link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickAccessorieslink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_0_3);
				Reporter.log("Clicked on Accessories Link");
			} catch (Exception e) {
				Reporter.log("Click on Accessories failed");
				Assert.fail("Click on Accessories failed");
			}
			return this;
		}

		/**
		 * Verify Accessories page.
		 *
		 * @return the Accessories Page class instance.
		 */
		public TMNGNewFooterPage verifyAccessoriesPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_0_3_LINK);
				Reporter.log("Accessories Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Accessories Page not displayed");
			}
			return this;
		}
		
		/**
		 * Verify Apps & Connected Devices Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyAppsConnectDevicesLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_1));
				Assert.assertTrue(footerLabel_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_footerLabel_1));
				Reporter.log(footerLabel_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer Family mode Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyFamilymodeLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_1_0));
				Assert.assertTrue(subFooterLink_1_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_1_0));
				Reporter.log(subFooterLink_1_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_1_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Family mode link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickFamilymodelink() {
			try {
				checkPageIsReady();
				subFooterLink_1_0.click();
				Reporter.log("Clicked on Familymode Link");
			} catch (Exception e) {
				Reporter.log("Click on Familymode failed");
				Assert.fail("Click on Familymode failed");
			}
			return this;
		}

		/**
		 * Verify Family mode page.
		 *
		 * @return the Family mode Page class instance.
		 */
		public TMNGNewFooterPage verifyFamilymodePage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_1_0_LINK);
				Reporter.log("Familymode Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Familymode Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer DIGITS Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyDIGITSLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_1_1));
				Assert.assertTrue(subFooterLink_1_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_1_1));
				Reporter.log(subFooterLink_1_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_1_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click DIGITS link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickDIGITSlink() {
			try {
				checkPageIsReady();
				subFooterLink_1_1.click();
				Reporter.log("Clicked on DIGITS Link");
			} catch (Exception e) {
				Reporter.log("Click on DIGITS failed");
				Assert.fail("Click on DIGITS failed");
			}
			return this;
		}

		/**
		 * Verify DIGITS page.
		 *
		 * @return the DIGITS Page class instance.
		 */
		public TMNGNewFooterPage verifyDIGITSPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_1_1_LINK);
				Reporter.log("DIGITS Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("DIGITS Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer SyncUp Drive Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifySyncUpDriveLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_1_2));
				Assert.assertTrue(subFooterLink_1_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_1_2));
				Reporter.log(subFooterLink_1_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_1_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click SyncUp Drive link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickSyncUpDrivelink() {
			try {
				checkPageIsReady();
				subFooterLink_1_2.click();
				Reporter.log("Clicked on SyncUp Drive Link");
			} catch (Exception e) {
				Reporter.log("Click on SyncUp Drive failed");
				Assert.fail("Click on SyncUp Drive failed");
			}
			return this;
		}

		/**
		 * Verify SyncUp Drive page.
		 *
		 * @return the SyncUp Drive Page class instance.
		 */
		public TMNGNewFooterPage verifySyncUpDrivePage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_1_2_LINK);
				Reporter.log("SyncUp Drive Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("SyncUp Drive Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Plans & Information Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPlansInformationLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_2));
				Assert.assertTrue(footerLabel_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_footerLabel_2));
				Reporter.log(footerLabel_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer Plans Home Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPlansHomeLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_2_0));
				Assert.assertTrue(subFooterLink_2_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_2_0));
				Reporter.log(subFooterLink_2_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_2_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Plans Home link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPlansHomelink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_2_0);
				Reporter.log("Clicked on Plans Home Link");
			} catch (Exception e) {
				Reporter.log("Click on Plans Home failed");
				Assert.fail("Click on Plans Home failed");
			}
			return this;
		}

		/**
		 * Verify Plans Home page.
		 *
		 * @return the Plans Home Page class instance.
		 */
		public TMNGNewFooterPage verifyPlansHomePage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_2_0_LINK);
				Reporter.log("Plans Home Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Plans Home Page not displayed");
			}
			return this;
		}
		/**
		 * Verify 55+ Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verify55Label() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_2_1));
				Assert.assertTrue(subFooterLink_2_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_2_1));
				Reporter.log(subFooterLink_2_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_2_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click 55+ link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage click55link() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_2_1);
				Reporter.log("Clicked on 55+ Link");
			} catch (Exception e) {
				Reporter.log("Click on 55+ failed");
				Assert.fail("Click on 55+ failed");
			}
			return this;
		}

		/**
		 * Verify 55+ page.
		 *
		 * @return the 55+ Page class instance.
		 */
		public TMNGNewFooterPage verify55Page() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_2_1_LINK);
				Reporter.log("55+ Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("55+ Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Military Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyMilitaryLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_2_2));
				Assert.assertTrue(subFooterLink_2_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_2_2));
				Reporter.log(subFooterLink_2_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_2_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Military link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickMilitarylink() {
			try {
				checkPageIsReady();
				subFooterLink_2_2.click();
				Reporter.log("Clicked on Military Link");
			} catch (Exception e) {
				Reporter.log("Click on Military failed");
				Assert.fail("Click on Military failed");
			}
			return this;
		}

		/**
		 * Verify Military page.
		 *
		 * @return the Military Page class instance.
		 */
		public TMNGNewFooterPage verifyMilitaryPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_2_2_LINK);
				Reporter.log("Military Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Military Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Prepaid Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPrepaidLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_2_3));
				Assert.assertTrue(subFooterLink_2_3.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_2_3));
				Reporter.log(subFooterLink_2_3.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_2_3.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Prepaid link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPrepaidlink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_2_3);
				Reporter.log("Clicked on Prepaid Link");
			} catch (Exception e) {
				Reporter.log("Click on Prepaid failed");
				Assert.fail("Click on Prepaid failed");
			}
			return this;
		}

		/**
		 * Verify Prepaid page.
		 *
		 * @return the Prepaid Page class instance.
		 */
		public TMNGNewFooterPage verifyPrepaidPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_2_3_LINK);
				Reporter.log("Prepaid Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Prepaid Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Device Protection Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyDeviceProtectionLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_2_4));
				Assert.assertTrue(subFooterLink_2_4.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_2_4));
				Reporter.log(subFooterLink_2_4.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_2_4.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Device Protection link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickDeviceProtectionlink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_2_4);
				Reporter.log("Clicked on Device Protection Link");
			} catch (Exception e) {
				Reporter.log("Click on Device Protection failed");
				Assert.fail("Click on Device Protection failed");
			}
			return this;
		}

		/**
		 * Verify Device Protection page.
		 *
		 * @return the Device Protection Page class instance.
		 */
		public TMNGNewFooterPage verifyDeviceProtectionPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_2_4_LINK);
				Reporter.log("Device Protection Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Device Protection Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Data Pass Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyDataPassLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_2_5));
				Assert.assertTrue(subFooterLink_2_5.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_2_5));
				Reporter.log(subFooterLink_2_5.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_2_5.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Data Pass link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickDataPasslink() {
			try {
				checkPageIsReady();
				subFooterLink_2_5.click();
				Reporter.log("Clicked on Data Pass Link");
			} catch (Exception e) {
				Reporter.log("Click on Data Pass failed");
				Assert.fail("Click on Data Pass failed");
			}
			return this;
		}

		/**
		 * Verify Data Pass page.
		 *
		 * @return the Data Pass Page class instance.
		 */
		public TMNGNewFooterPage verifyDataPassPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_2_5_LINK);
				Reporter.log("Data Pass Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Data Pass Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Mobile Internet Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyMobileInternetLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_2_6));
				Assert.assertTrue(subFooterLink_2_6.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_2_6));
				Reporter.log(subFooterLink_2_6.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_2_6.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Mobile Internet link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickMobileInternetlink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_2_6);
				Reporter.log("Clicked on Mobile Internet Link");
			} catch (Exception e) {
				Reporter.log("Click on Mobile Internet failed");
				Assert.fail("Click on Mobile Internet failed");
			}
			return this;
		}

		/**
		 * Verify Mobile Internet page.
		 *
		 * @return the Mobile Internet Page class instance.
		 */
		public TMNGNewFooterPage verifyMobileInternetPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_2_6_LINK);
				Reporter.log("Mobile Internet Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Mobile Internet Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Switch to T-Mobile Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifySwitchTMobileLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_3));
				Assert.assertTrue(footerLabel_3.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_footerLabel_3));
				Reporter.log(footerLabel_3.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_3.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer We’ll help you join Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyHelpJoinLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_3_0));
				Assert.assertTrue(subFooterLink_3_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_3_0));
				Reporter.log(subFooterLink_3_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_3_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click We’ll help you join link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickHelpJoinlink() {
			try {
				checkPageIsReady();
				subFooterLink_3_0.click();
				Reporter.log("Clicked on We’ll help you join Link");
			} catch (Exception e) {
				Reporter.log("Click on We’ll help you join failed");
				Assert.fail("Click on We’ll help you join failed");
			}
			return this;
		}

		/**
		 * Verify We’ll help you join page.
		 *
		 * @return the We’ll help you join Page class instance.
		 */
		public TMNGNewFooterPage verifyHelpJoinPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_3_0_LINK);
				Reporter.log("We’ll help you join Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("We’ll help you join Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Savings Calculator Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifySavingsCalculatorLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_3_1));
				Assert.assertTrue(subFooterLink_3_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_3_1));
				Reporter.log(subFooterLink_3_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_3_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Savings Calculator link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickSavingsCalculatorlink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_3_1);
				Reporter.log("Clicked on SavingsCalculator Link");
			} catch (Exception e) {
				Reporter.log("Click on Savings Calculator failed");
				Assert.fail("Click on Savings Calculator failed");
			}
			return this;
		}

		/**
		 * Verify Savings Calculator page.
		 *
		 * @return the Savings Calculator Page class instance.
		 */
		public TMNGNewFooterPage verifySavingsCalculatorPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_3_1_LINK);
				Reporter.log("Savings Calculator Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Savings Calculator Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Bring your own device Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyBringOwnDeviceLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_3_2));
				Assert.assertTrue(subFooterLink_3_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_3_2));
				Reporter.log(subFooterLink_3_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_3_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Bring your own device link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickBringOwnDevicelink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_3_2);
				Reporter.log("Clicked on Bring your own device Link");
			} catch (Exception e) {
				Reporter.log("Click on Bring your own device failed");
				Assert.fail("Click on Bring your own device failed");
			}
			return this;
		}

		/**
		 * Verify Bring your own device page.
		 *
		 * @return the Bring your own device Page class instance.
		 */
		public TMNGNewFooterPage verifyBringOwnDevicePage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_3_2_LINK);
				Reporter.log("Bring your own device Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Bring your own device Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Trade-In Program Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyTradeInProgramLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_3_3));
				Assert.assertTrue(subFooterLink_3_3.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_3_3));
				Reporter.log(subFooterLink_3_3.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_3_3.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Trade-In Program link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickTradeInProgramlink() {
			try {
				checkPageIsReady();
				subFooterLink_3_3.click();
				Reporter.log("Clicked on Trade-In Program Link");
			} catch (Exception e) {
				Reporter.log("Click on Trade-In Program failed");
				Assert.fail("Click on Trade-In Program failed");
			}
			return this;
		}

		/**
		 * Verify Trade-In Program page.
		 *
		 * @return the Trade-In Program Page class instance.
		 */
		public TMNGNewFooterPage verifyTradeInProgramPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_3_3_LINK);
				Reporter.log("Trade-In Program Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Trade-In Program Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Number Porting Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyNumberPortingLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_3_4));
				Assert.assertTrue(subFooterLink_3_4.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_3_4));
				Reporter.log(subFooterLink_3_4.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_3_4.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Number Porting link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickNumberPortinglink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_3_4);
				Reporter.log("Clicked on Number Porting Link");
			} catch (Exception e) {
				Reporter.log("Click on Number Porting failed");
				Assert.fail("Click on Number Porting failed");
			}
			return this;
		}

		/**
		 * Verify Number Porting page.
		 *
		 * @return the Number Porting Page class instance.
		 */
		public TMNGNewFooterPage verifyNumberPortingPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_3_4_LINK);
				Reporter.log("Number Porting Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Number Porting Page not displayed");
			}
			return this;
		}
		/**
		 * Verify T-Mobile Perks Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyTMobilePerksLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_4));
				Assert.assertTrue(footerLabel_4.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_footerLabel_4));
				Reporter.log(footerLabel_4.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_4.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer Benefits Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyBenefitsLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_4_0));
				Assert.assertTrue(subFooterLink_4_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_4_0));
				Reporter.log(subFooterLink_4_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_4_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Benefits link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickBenefitslink() {
			try {
				checkPageIsReady();
				subFooterLink_4_0.click();
				Reporter.log("Clicked on Benefits Link");
			} catch (Exception e) {
				Reporter.log("Click on Benefits failed");
				Assert.fail("Click on Benefits failed");
			}
			return this;
		}

		/**
		 * Verify Benefits page.
		 *
		 * @return the Benefits Page class instance.
		 */
		public TMNGNewFooterPage verifyBenefitsPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_4_0_LINK);
				Reporter.log("Benefits Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Benefits Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Travel Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyTravelLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_4_1));
				Assert.assertTrue(subFooterLink_4_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_4_1));
				Reporter.log(subFooterLink_4_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_4_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Travel link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickTravellink() {
			try {
				checkPageIsReady();
				subFooterLink_4_1.click();
				Reporter.log("Clicked on Travel Link");
			} catch (Exception e) {
				Reporter.log("Click on Travel failed");
				Assert.fail("Click on Travel failed");
			}
			return this;
		}

		/**
		 * Verify Travel page.
		 *
		 * @return the Travel Page class instance.
		 */
		public TMNGNewFooterPage verifyTravelPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_4_1_LINK);
				Reporter.log("Travel Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Travel Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Order Info Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyOrderInfoLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_5));
				Assert.assertTrue(footerLabel_5.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_footerLabel_5));
				Reporter.log(footerLabel_5.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_5.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer Check Order Status Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyCheckOrderStatusLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_5_0));
				Assert.assertTrue(subFooterLink_5_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_5_0));
				Reporter.log(subFooterLink_5_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_5_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Check Order Status link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickCheckOrderStatuslink() {
			try {
				checkPageIsReady();
				subFooterLink_5_0.click();
				Reporter.log("Clicked on Check Order Status Link");
			} catch (Exception e) {
				Reporter.log("Click on Check Order Status failed");
				Assert.fail("Click on Check Order Status failed");
			}
			return this;
		}

		/**
		 * Verify Check Order Status page.
		 *
		 * @return the Check Order Status Page class instance.
		 */
		public TMNGNewFooterPage verifyCheckOrderStatusPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_5_0_LINK);
				Reporter.log("Check Order Status Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Check Order Status Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer View Return Policy Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyViewReturnPolicyLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_5_1));
				Assert.assertTrue(subFooterLink_5_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_5_1));
				Reporter.log(subFooterLink_5_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_5_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click View Return Policy link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickViewReturnPolicylink() {
			try {
				checkPageIsReady();
				subFooterLink_5_1.click();
				Reporter.log("Clicked on View Return Policy Link");
			} catch (Exception e) {
				Reporter.log("Click on View Return Policy failed");
				Assert.fail("Click on View Return Policy failed");
			}
			return this;
		}

		/**
		 * Verify View Return Policy page.
		 *
		 * @return the View Return Policy Page class instance.
		 */
		public TMNGNewFooterPage verifyViewReturnPolicyPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_5_1_LINK);
				Reporter.log("View Return Policy Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("View Return Policy Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Redeem a Rebate Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyRedeemRebateLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_5_2));
				Assert.assertTrue(subFooterLink_5_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_5_2));
				Reporter.log(subFooterLink_5_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_5_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Redeem a Rebate link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickRedeemRebatelink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_5_2);
				Reporter.log("Clicked on Redeem a Rebate Link");
			} catch (Exception e) {
				Reporter.log("Click on Redeem a Rebate failed");
				Assert.fail("Click on Redeem a Rebate failed");
			}
			return this;
		}

		/**
		 * Verify Redeem a Rebate page.
		 *
		 * @return the Redeem a Rebate Page class instance.
		 */
		public TMNGNewFooterPage verifyRedeemRebatePage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_5_2_LINK);
				Reporter.log("Redeem a Rebate Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Redeem a Rebate Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Support Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifySupportLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_6));
				Assert.assertTrue(footerLabel_6.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_footerLabel_6));
				Reporter.log(footerLabel_6.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_6.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer Contact Us Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyContactUsLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_6_0));
				Assert.assertTrue(subFooterLink_6_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_6_0));
				Reporter.log(subFooterLink_6_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_6_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Check Contact Us link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickContactUslink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_6_0);
				Reporter.log("Clicked on Contact Us Link");
			} catch (Exception e) {
				Reporter.log("Click on Contact Us failed");
				Assert.fail("Click on Contact Us failed");
			}
			return this;
		}

		/**
		 * Verify Contact Us page.
		 *
		 * @return the Contact Us Page class instance.
		 */
		public TMNGNewFooterPage verifyContactUsPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_6_0_LINK);
				Reporter.log("Contact Us Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Contact Us Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Phones Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyFooterPhonesLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_6_1));
				Assert.assertTrue(subFooterLink_6_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_6_1));
				Reporter.log(subFooterLink_6_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_6_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Phones link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickFooterPhoneslink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_6_1);
				Reporter.log("Clicked on Phones Link");
			} catch (Exception e) {
				Reporter.log("Click on Phones failed");
				Assert.fail("Click on Phones failed");
			}
			return this;
		}

		/**
		 * Verify Phones page.
		 *
		 * @return the Phones Page class instance.
		 */
		public TMNGNewFooterPage verifyFooterPhonesPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_6_1_LINK);
				Reporter.log("Phones Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Phones Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Plans Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPlansLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_6_2));
				Assert.assertTrue(subFooterLink_6_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_6_2));
				Reporter.log(subFooterLink_6_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_6_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Plans link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPlanslink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_6_2);
				Reporter.log("Clicked on Plans Link");
			} catch (Exception e) {
				Reporter.log("Click on Plans failed");
				Assert.fail("Click on Plans failed");
			}
			return this;
		}

		/**
		 * Verify Plans page.
		 *
		 * @return the Plans Page class instance.
		 */
		public TMNGNewFooterPage verifyPlansPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_6_2_LINK);
				Reporter.log("Plans Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Plans Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Billing Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyBillingLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_6_3));
				Assert.assertTrue(subFooterLink_6_3.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_6_3));
				Reporter.log(subFooterLink_6_3.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_6_3.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Billing link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickBillinglink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_6_3);
				Reporter.log("Clicked on Billing Link");
			} catch (Exception e) {
				Reporter.log("Click on Billing failed");
				Assert.fail("Click on Billing failed");
			}
			return this;
		}

		/**
		 * Verify Billing page.
		 *
		 * @return the Billing Page class instance.
		 */
		public TMNGNewFooterPage verifyBillingPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_6_3_LINK);
				Reporter.log("Billing Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Billing Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer International Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyInternationalLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_6_3));
				Assert.assertTrue(subFooterLink_6_4.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_6_4));
				Reporter.log(subFooterLink_6_4.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_6_4.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click International link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickInternationallink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_6_4);
				Reporter.log("Clicked on International Link");
			} catch (Exception e) {
				Reporter.log("Click on International failed");
				Assert.fail("Click on International failed");
			}
			return this;
		}

		/**
		 * Verify International page.
		 *
		 * @return the International Page class instance.
		 */
		public TMNGNewFooterPage verifyInternationalPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_6_4_LINK);
				Reporter.log("International Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("International Page not displayed");
			}
			return this;
		}
		/**
		 * Verify My Account Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyMyAccountLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_7));
				Assert.assertTrue(footerLabel_7.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_footerLabel_7));
				Reporter.log(footerLabel_7.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_7.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer Pay My Bill Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPayMyBillLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_7_0));
				Assert.assertTrue(subFooterLink_7_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_7_0));
				Reporter.log(subFooterLink_7_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_7_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Check Pay My Bill link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPayMyBilllink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_7_0);
				Reporter.log("Clicked on Pay My Bill Link");
			} catch (Exception e) {
				Reporter.log("Click on Pay My Bill failed");
				Assert.fail("Click on Pay My Bill failed");
			}
			return this;
		}

		/**
		 * Verify Pay My Bill page.
		 *
		 * @return the Pay My Bill Page class instance.
		 */
		public TMNGNewFooterPage verifyPayMyBillPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_7_0_LINK);
				Reporter.log("Pay My Bill Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Pay My Bill Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Upgrade Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyUpgradeLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_7_1));
				Assert.assertTrue(subFooterLink_7_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_7_1));
				Reporter.log(subFooterLink_7_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_7_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Check Upgrade link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickUpgradelink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_7_1);
				Reporter.log("Clicked on Upgrade Link");
			} catch (Exception e) {
				Reporter.log("Click on Upgrade failed");
				Assert.fail("Click on Upgrade failed");
			}
			return this;
		}

		/**
		 * Verify Upgrade page.
		 *
		 * @return the Upgrade Page class instance.
		 */
		public TMNGNewFooterPage verifyUpgradePage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_7_1_LINK);
				Reporter.log("Upgrade Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Upgrade Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Add a Line Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyAddLineLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_7_2));
				Assert.assertTrue(subFooterLink_7_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_7_2));
				Reporter.log(subFooterLink_7_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_7_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Check Add a Line link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickAddLinelink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_7_2);
				Reporter.log("Clicked on Add a Line Link");
			} catch (Exception e) {
				Reporter.log("Click on Add a Line failed");
				Assert.fail("Click on Add a Line failed");
			}
			return this;
		}

		/**
		 * Verify Add a Line page.
		 *
		 * @return the Add a Line Page class instance.
		 */
		public TMNGNewFooterPage verifyAddLinePage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_7_2_LINK);
				Reporter.log("Add a Line Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Add a Line Page not displayed");
			}
			return this;
		}
		/**
		 * Verify More than wireless Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyMoreWirelessLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_8));
				Assert.assertTrue(footerLabel_8.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_footerLabel_8));
				Reporter.log(footerLabel_8.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_8.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer Business Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyBusinessLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_8_0));
				Assert.assertTrue(subFooterLink_8_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_8_0));
				Reporter.log(subFooterLink_8_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_8_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Business link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickBusinesslink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_8_0);
				Reporter.log("Clicked on Business Link");
			} catch (Exception e) {
				Reporter.log("Click on Business failed");
				Assert.fail("Click on Business failed");
			}
			return this;
		}

		/**
		 * Verify Business page.
		 *
		 * @return the Business Page class instance.
		 */
		public TMNGNewFooterPage verifyBusinessPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_8_0_LINK);
				Reporter.log("Business Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Business Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer TVision Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyTVisionLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_8_2));
				Assert.assertTrue(subFooterLink_8_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_8_2));
				Reporter.log(subFooterLink_8_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_8_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click TVision link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickTVisionlink() {
			try {
				checkPageIsReady();
				subFooterLink_8_2.click();
				Reporter.log("Clicked on TVision Link");
			} catch (Exception e) {
				Reporter.log("Click on TVision failed");
				Assert.fail("Click on TVision failed");
			}
			return this;
		}

		/**
		 * Verify TVision page.
		 *
		 * @return the TVision Page class instance.
		 */
		public TMNGNewFooterPage verifyTVisionPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_8_2_LINK);
				Reporter.log("TVision Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("TVision Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer T-Mobile Money Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyTMobileMoneyLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_8_3));
				Assert.assertTrue(subFooterLink_8_3.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_8_3));
				Reporter.log(subFooterLink_8_3.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_8_3.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click T-Mobile Money link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickTMobileMoneylink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_8_3);
				Reporter.log("Clicked on T-Mobile Money Link");
			} catch (Exception e) {
				Reporter.log("Click on T-Mobile Money failed");
				Assert.fail("Click on T-Mobile Money failed");
			}
			return this;
		}

		/**
		 * Verify T-Mobile Money page.
		 *
		 * @return the T-Mobile Money Page class instance.
		 */
		public TMNGNewFooterPage verifyTMobileMoneyPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_8_3_LINK);
				Reporter.log("T-Mobile Money Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("T-Mobile Money Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Home Internet Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyHomeInternetLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_8_4));
				Assert.assertTrue(subFooterLink_8_4.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_8_4));
				Reporter.log(subFooterLink_8_4.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_8_4.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Home Internet link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickHomeInternetlink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_8_4);
				Reporter.log("Clicked on Home Internet Link");
			} catch (Exception e) {
				Reporter.log("Click on Home Internet failed");
				Assert.fail("Click on Home Internet failed");
			}
			return this;
		}

		/**
		 * Verify Home Internet page.
		 *
		 * @return the Home Internet Page class instance.
		 */
		public TMNGNewFooterPage verifyHomeInternetPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_8_4_LINK);
				Reporter.log("Home Internet Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Home Internet Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer IoT Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyIoTLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_8_5));
				Assert.assertTrue(subFooterLink_8_5.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_8_5));
				Reporter.log(subFooterLink_8_5.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_8_5.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click IoT link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickIoTlink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_8_5);
				Reporter.log("Clicked on IoT Link");
			} catch (Exception e) {
				Reporter.log("Click on IoT failed");
				Assert.fail("Click on IoT failed");
			}
			return this;
		}

		/**
		 * Verify IoT page.
		 *
		 * @return the IoT Page class instance.
		 */
		public TMNGNewFooterPage verifyIoTPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_8_5_LINK);
				Reporter.log("IoT Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("IoT Page not displayed");
			}
			return this;
		}
		/**
		 * Verify About T-Mobile Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyAboutTMobileLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_9));
				Assert.assertTrue(footerLabel_9.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_footerLabel_9));
				Reporter.log(footerLabel_9.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_9.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer Our Story Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyOurStoryLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_9_0));
				Assert.assertTrue(subFooterLink_9_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_9_0));
				Reporter.log(subFooterLink_9_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_9_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our Story link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickOurStorylink() {
			try {
				checkPageIsReady();
				subFooterLink_9_0.click();
				Reporter.log("Clicked on Our Story Link");
			} catch (Exception e) {
				Reporter.log("Click on Our Story failed");
				Assert.fail("Click on Our Story failed");
			}
			return this;
		}

		/**
		 * Verify Our Story page.
		 *
		 * @return the Our Story Page class instance.
		 */
		public TMNGNewFooterPage verifyOurStoryPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_9_0_LINK);
				Reporter.log("Our Story Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Our Story Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Newsroom Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyNewsroomLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_9_1));
				Assert.assertTrue(subFooterLink_9_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_9_1));
				Reporter.log(subFooterLink_9_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_9_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Newsroom link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickNewsroomlink() {
			try {
				checkPageIsReady();
				subFooterLink_9_1.click();
				Reporter.log("Clicked on Newsroom Link");
			} catch (Exception e) {
				Reporter.log("Click on Newsroom failed");
				Assert.fail("Click on Newsroom failed");
			}
			return this;
		}

		/**
		 * Verify Newsroom page.
		 *
		 * @return the Newsroom Page class instance.
		 */
		public TMNGNewFooterPage verifyNewsroomPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_9_1_LINK);
				Reporter.log("Newsroom Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Newsroom Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Investor Relations Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyInvestorRelationsLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_9_2));
				Assert.assertTrue(subFooterLink_9_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_9_2));
				Reporter.log(subFooterLink_9_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_9_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Investor Relations link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickInvestorRelationslink() {
			try {
				checkPageIsReady();
				subFooterLink_9_2.click();
				Reporter.log("Clicked on Investor Relations Link");
			} catch (Exception e) {
				Reporter.log("Click on Investor Relations failed");
				Assert.fail("Click on Investor Relations failed");
			}
			return this;
		}

		/**
		 * Verify Investor Relations page.
		 *
		 * @return the Investor Relations Page class instance.
		 */
		public TMNGNewFooterPage verifyInvestorRelationsPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_9_2_LINK);
				Reporter.log("Investor Relations Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Investor Relations Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Corporate Responsibility Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyCorporateResponsibilityLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_10));
				Assert.assertTrue(footerLabel_10.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_footerLabel_10));
				Reporter.log(footerLabel_10.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_10.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer Community Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyCommunityLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_10_0));
				Assert.assertTrue(subFooterLink_10_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_10_0));
				Reporter.log(subFooterLink_10_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_10_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our Community link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickCommunitylink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_10_0);
				Reporter.log("Clicked on Community Link");
			} catch (Exception e) {
				Reporter.log("Click on Community failed");
				Assert.fail("Click on Community failed");
			}
			return this;
		}

		/**
		 * Verify Community page.
		 *
		 * @return the Community Page class instance.
		 */
		public TMNGNewFooterPage verifyCommunityPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_10_0_LINK);
				Reporter.log("Community Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Community Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Sustainability Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifySustainabilityLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_10_1));
				Assert.assertTrue(subFooterLink_10_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_10_1));
				Reporter.log(subFooterLink_10_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_10_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our Sustainability link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickSustainabilitylink() {
			try {
				checkPageIsReady();
				subFooterLink_10_1.click();
				Reporter.log("Clicked on Sustainability Link");
			} catch (Exception e) {
				Reporter.log("Click on Sustainability failed");
				Assert.fail("Click on Sustainability failed");
			}
			return this;
		}

		/**
		 * Verify Sustainability page.
		 *
		 * @return the Sustainability Page class instance.
		 */
		public TMNGNewFooterPage verifySustainabilityPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_10_1_LINK);
				Reporter.log("Sustainability Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Sustainability Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Privacy Center Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPrivacyCenterLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_10_2));
				Assert.assertTrue(subFooterLink_10_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_10_2));
				Reporter.log(subFooterLink_10_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_10_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our Privacy Center link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPrivacyCenterlink() {
			try {
				checkPageIsReady();
				subFooterLink_10_2.click();
				Reporter.log("Clicked on Privacy Center Link");
			} catch (Exception e) {
				Reporter.log("Click on Privacy Center failed");
				Assert.fail("Click on Privacy Center failed");
			}
			return this;
		}

		/**
		 * Verify Privacy Center page.
		 *
		 * @return the Privacy Center Page class instance.
		 */
		public TMNGNewFooterPage verifyPrivacyCenterPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_10_2_LINK);
				Reporter.log("Privacy Center Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Privacy Center Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Careers Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyCareersLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_11));
				Assert.assertTrue(footerLabel_11.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_footerLabel_11));
				Reporter.log(footerLabel_11.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_11.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer T-Mobile Careers Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyTMobileCareersLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_11_0));
				Assert.assertTrue(subFooterLink_11_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_subFooterLink_11_0));
				Reporter.log(subFooterLink_11_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_11_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our T-Mobile Careers link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickTMobileCareerslink() {
			try {
				checkPageIsReady();
				clickElement(subFooterLink_11_0);
				Reporter.log("Clicked on T-Mobile Careers Link");
			} catch (Exception e) {
				Reporter.log("Click on T-Mobile Careers failed");
				Assert.fail("Click on T-Mobile Careers failed");
			}
			return this;
		}

		/**
		 * Verify T-Mobile Careers page.
		 *
		 * @return the T-Mobile Careers Page class instance.
		 */
		public TMNGNewFooterPage verifyTMobileCareersPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_11_0_LINK);
				Reporter.log("T-Mobile Careers Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("T-Mobile Careers Page not displayed");
			}
			return this;
		}
		
		// Verify UNAV Footer - Bottom
		
		/**
		 * Verify Footer ABOUT Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyAboutLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_top_0));
				Assert.assertTrue(footer_link_top_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_top_0));
				Reporter.log(footer_link_top_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_top_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our About link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickAboutlink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_top_0);
				Reporter.log("Clicked on ABOUT Link");
			} catch (Exception e) {
				Reporter.log("Click on ABOUT failed");
				Assert.fail("Click on ABOUT failed");
			}
			return this;
		}

		/**
		 * Verify ABOUT page.
		 *
		 * @return the ABOUT Page class instance.
		 */
		public TMNGNewFooterPage verifyAboutPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_top_0_LINK);
				Reporter.log("ABOUTs Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("ABOUT Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer INVESTOR RELATIONS Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyFooterInvestorRelationsLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_top_1));
				Assert.assertTrue(footer_link_top_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_top_1));
				Reporter.log(footer_link_top_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_top_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our Investor Relations link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickFooterInvestorRelationslink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_top_1);
				Reporter.log("Clicked on Invester Relations Link");
			} catch (Exception e) {
				Reporter.log("Click on Invester Relations failed");
				Assert.fail("Click on Invester Relations failed");
			}
			return this;
		}

		/**
		 * Verify Investor Relations page.
		 *
		 * @return the Investor Relations Page class instance.
		 */
		public TMNGNewFooterPage verifyFooterInvestorRelationsPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_top_1_LINK);
				Reporter.log("Investor Relations Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Investor Relations Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer PRESS Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPressLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_top_2));
				Assert.assertTrue(footer_link_top_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_top_2));
				Reporter.log(footer_link_top_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_top_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our PRESS link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPresslink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_top_2);
				Reporter.log("Clicked on PRESS Link");
			} catch (Exception e) {
				Reporter.log("Click on PRESS failed");
				Assert.fail("Click on PRESS failed");
			}
			return this;
		}

		/**
		 * Verify PRESS page.
		 *
		 * @return the PRESS Page class instance.
		 */
		public TMNGNewFooterPage verifyPressPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_top_2_LINK);
				Reporter.log("PRESS Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("PRESS Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer CAREERS Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyFooterCareersLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_top_3));
				Assert.assertTrue(footer_link_top_3.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_top_3));
				Reporter.log(footer_link_top_3.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_top_3.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our CAREERS link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickFooterCareerslink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_top_3);
				Reporter.log("Clicked on CAREERS Link");
			} catch (Exception e) {
				Reporter.log("Click on CAREERS failed");
				Assert.fail("Click on CAREERS failed");
			}
			return this;
		}

		/**
		 * Verify CAREERS page.
		 *
		 * @return the CAREERS Page class instance.
		 */
		public TMNGNewFooterPage verifyFooterCareersPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_top_3_LINK);
				Reporter.log("CAREERS Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("CAREERS Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer DEUTSCHE TELEKOM Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyDeutscheTelekomLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_top_4));
				Assert.assertTrue(footer_link_top_4.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_top_4));
				Reporter.log(footer_link_top_4.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_top_4.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our DEUTSCHE TELEKOM link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickDeutscheTelekomlink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_top_4);
				Reporter.log("Clicked on DEUTSCHE TELEKOM Link");
			} catch (Exception e) {
				Reporter.log("Click on DEUTSCHE TELEKOM failed");
				Assert.fail("Click on DEUTSCHE TELEKOM failed");
			}
			return this;
		}

		/**
		 * Verify DEUTSCHE TELEKOM page.
		 *
		 * @return the DEUTSCHE TELEKOM Page class instance.
		 */
		public TMNGNewFooterPage verifyDeutscheTelekomPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_top_4_LINK);
				Reporter.log("DEUTSCHE TELEKOM Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("DEUTSCHE TELEKOM Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer PUERTO RICO Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPuertoRicoLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_top_5));
				Assert.assertTrue(footer_link_top_5.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_top_5));
				Reporter.log(footer_link_top_5.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_top_5.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our PUERTO RICO link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPuertoRicolink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_top_5);
				Reporter.log("Clicked on PUERTO RICO Link");
			} catch (Exception e) {
				Reporter.log("Click on PUERTO RICO failed");
				Assert.fail("Click on PUERTO RICO failed");
			}
			return this;
		}

		/**
		 * Verify PUERTO RICO page.
		 *
		 * @return the PUERTO RICO Page class instance.
		 */
		public TMNGNewFooterPage verifyPuertoRicoPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_top_5_LINK);
				Reporter.log("PUERTO RICO Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("PUERTO RICO Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer PRIVACY POLICY Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPrivacyPolicyLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_bottom_0));
				Assert.assertTrue(footer_link_bottom_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_0));
				Reporter.log(footer_link_bottom_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_bottom_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our PRIVACY POLICY link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPrivacyPolicylink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_bottom_0);
				Reporter.log("Clicked on PRIVACY POLICY Link");
			} catch (Exception e) {
				Reporter.log("Click on PRIVACY POLICY failed");
				Assert.fail("Click on PRIVACY POLICY failed");
			}
			return this;
		}

		/**
		 * Verify PUERTO RICO page.
		 *
		 * @return the PUERTO RICO Page class instance.
		 */
		public TMNGNewFooterPage verifyPrivacyPolicyPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_bottom_0_LINK);
				Reporter.log("PRIVACY POLICY Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("PRIVACY POLICY Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer INTEREST-BASED ADS Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyInterestBasedAdsLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_bottom_1));
				Assert.assertTrue(footer_link_bottom_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_1));
				Reporter.log(footer_link_bottom_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_bottom_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our INTEREST-BASED ADS link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickInterestBasedAdslink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_bottom_1);
				Reporter.log("Clicked on INTEREST-BASED ADS Link");
			} catch (Exception e) {
				Reporter.log("Click on INTEREST-BASED ADS failed");
				Assert.fail("Click on INTEREST-BASED ADS failed");
			}
			return this;
		}

		/**
		 * Verify INTEREST-BASED ADS page.
		 *
		 * @return the INTEREST-BASED ADS Page class instance.
		 */
		public TMNGNewFooterPage verifyInterestBasedAdsPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_bottom_1_LINK);
				Reporter.log("INTEREST-BASED ADS Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("INTEREST-BASED ADS Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer PRIVACY CENTER Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPrivacyLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_bottom_2));
				Assert.assertTrue(footer_link_bottom_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_2));
				Reporter.log(footer_link_bottom_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_bottom_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our PRIVACY CENTER link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPrivacylink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_bottom_2);
				Reporter.log("Clicked on PRIVACY CENTER Link");
			} catch (Exception e) {
				Reporter.log("Click on PRIVACY CENTER failed");
				Assert.fail("Click on PRIVACY CENTER failed");
			}
			return this;
		}

		/**
		 * Verify PRIVACY CENTER page.
		 *
		 * @return the PRIVACY CENTER Page class instance.
		 */
		public TMNGNewFooterPage verifyPrivacyPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_bottom_2_LINK);
				Reporter.log("PRIVACY CENTER Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("PRIVACY CENTER Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer CONSUMER INFORMATION Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyConsumerLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_bottom_3));
				Assert.assertTrue(footer_link_bottom_3.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_3));
				Reporter.log(footer_link_bottom_3.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_bottom_3.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our CONSUMER INFORMATION link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickConsumerlink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_bottom_3);
				Reporter.log("Clicked on CONSUMER INFORMATION Link");
			} catch (Exception e) {
				Reporter.log("Click on CONSUMER INFORMATION failed");
				Assert.fail("Click on CONSUMER INFORMATION failed");
			}
			return this;
		}

		/**
		 * Verify CONSUMER INFORMATION page.
		 *
		 * @return the CONSUMER INFORMATION Page class instance.
		 */
		public TMNGNewFooterPage verifyConsumerPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_bottom_3_LINK);
				Reporter.log("CONSUMER INFORMATION Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("CONSUMER INFORMATION Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer PUBLIC SAFETY/911 Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPublicSafetyLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_bottom_4));
				Assert.assertTrue(footer_link_bottom_4.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_4));
				Reporter.log(footer_link_bottom_4.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_bottom_4.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our PUBLIC SAFETY/911 link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPublicSafetylink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_bottom_4);
				Reporter.log("Clicked on PUBLIC SAFETY/911 Link");
			} catch (Exception e) {
				Reporter.log("Click on PUBLIC SAFETY/911 failed");
				Assert.fail("Click on PUBLIC SAFETY/911 failed");
			}
			return this;
		}

		/**
		 * Verify PUBLIC SAFETY/911 page.
		 *
		 * @return the PUBLIC SAFETY/911 Page class instance.
		 */
		public TMNGNewFooterPage verifyPublicSafetyPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_bottom_4_LINK);
				Reporter.log("PUBLIC SAFETY/911 Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("PUBLIC SAFETY/911 Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer TERMS & CONDITIONS Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyTermsLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_bottom_5));
				Assert.assertTrue(footer_link_bottom_5.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_5));
				Reporter.log(footer_link_bottom_5.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_bottom_5.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our TERMS & CONDITIONS link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickTermslink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_bottom_5);
				Reporter.log("Clicked on TERMS & CONDITIONS Link");
			} catch (Exception e) {
				Reporter.log("Click on TERMS & CONDITIONS failed");
				Assert.fail("Click on TERMS & CONDITIONS failed");
			}
			return this;
		}

		/**
		 * Verify TERMS & CONDITIONS page.
		 *
		 * @return the TERMS & CONDITIONS Page class instance.
		 */
		public TMNGNewFooterPage verifyTermsPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_bottom_5_LINK);
				Reporter.log("TERMS & CONDITIONS Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("TERMS & CONDITIONS Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer TERMS OF USE Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyTermsOfUseLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_bottom_6));
				Assert.assertTrue(footer_link_bottom_6.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_6));
				Reporter.log(footer_link_bottom_6.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_bottom_6.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our TERMS OF USE link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickTermsOfUselink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_bottom_6);
				Reporter.log("Clicked on TERMS OF USE Link");
			} catch (Exception e) {
				Reporter.log("Click on TERMS OF USE failed");
				Assert.fail("Click on TERMS OF USE failed");
			}
			return this;
		}

		/**
		 * Verify TERMS OF USE page.
		 *
		 * @return the TERMS OF USE Page class instance.
		 */
		public TMNGNewFooterPage verifyTermsOfUsePage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_bottom_6_LINK);
				Reporter.log("TERMS OF USE Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("TERMS OF USE Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer ACCESSIBILITY Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyAccessibilityLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_bottom_7));
				Assert.assertTrue(footer_link_bottom_7.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_7));
				Reporter.log(footer_link_bottom_7.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_bottom_7.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our ACCESSIBILITY link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickAccessibilitylink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_bottom_7);
				Reporter.log("Clicked on ACCESSIBILITY Link");
			} catch (Exception e) {
				Reporter.log("Click on ACCESSIBILITY failed");
				Assert.fail("Click on ACCESSIBILITY failed");
			}
			return this;
		}

		/**
		 * Verify ACCESSIBILITY page.
		 *
		 * @return the ACCESSIBILITY Page class instance.
		 */
		public TMNGNewFooterPage verifyAccessibilityPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_bottom_7_LINK);
				Reporter.log("ACCESSIBILITY Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("ACCESSIBILITY Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer OPEN INTERNET Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyOpenInternetLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_bottom_8));
				Assert.assertTrue(footer_link_bottom_8.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_8));
				Reporter.log(footer_link_bottom_8.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footer_link_bottom_8.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Our OPEN INTERNET link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickOpenInternetlink() {
			try {
				checkPageIsReady();
				clickElement(footer_link_bottom_8);
				Reporter.log("Clicked on OPEN INTERNET Link");
			} catch (Exception e) {
				Reporter.log("Click on OPEN INTERNET failed");
				Assert.fail("Click on OPEN INTERNET failed");
			}
			return this;
		}

		/**
		 * Verify OPEN INTERNET page.
		 *
		 * @return the OPEN INTERNET Page class instance.
		 */
		public TMNGNewFooterPage verifyOpenInternetPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER2_bottom_8_LINK);
				Reporter.log("OPEN INTERNET Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("OPEN INTERNET Page not displayed");
			}
			return this;
		}
		// Footer2 Social links
		/**
		 * Click Footer2 Instagram link
		 */
		public TMNGNewFooterPage clickFooter2Instagramlink() {
			try {
				moveToElement(footer_link_top_0);
				waitFor(ExpectedConditions.visibilityOf(bottom_socailLink0));
				clickElementWithJavaScript(bottom_socailLink0);
				Reporter.log("Clicked on Instagram Link");
			} catch (Exception e) {
				Verify.fail("Click on Instagram link failed ");
			}
			return this;
		}
		/**
		 * Click Footer2 Facebook link
		 */
		public TMNGNewFooterPage clickFooter2Facebooklink() {
			try {
				moveToElement(footer_link_top_0);
				waitFor(ExpectedConditions.visibilityOf(bottom_socailLink1));
				clickElementWithJavaScript(bottom_socailLink1);
				Reporter.log("Clicked on footer2 Facebook Link");
			} catch (Exception e) {
				Verify.fail("Click on Footer2 Facebook link failed ");
			}
			return this;
		}
		/**
		 * Click Footer2 Twitter link
		 */
		public TMNGNewFooterPage clickFooter2Twitterlink() {
			try {
				moveToElement(footer_link_top_0);
				waitFor(ExpectedConditions.visibilityOf(bottom_socailLink2));
				clickElementWithJavaScript(bottom_socailLink2);
				Reporter.log("Clicked on Footer 2 Twitter Link");
			} catch (Exception e) {
				Verify.fail("Click on Footer2 Twitter link failed ");
			}
			return this;
		}
		/**
		 * Click Footer2 YouTube link
		 */
		public TMNGNewFooterPage clickFooter2YouTubelink() {
			try {
				moveToElement(footer_link_top_0);
				waitFor(ExpectedConditions.visibilityOf(bottom_socailLink3));
				clickElementWithJavaScript(bottom_socailLink3);
				Reporter.log("Clicked on Footer2 YouTube Link");
			} catch (Exception e) {
				Verify.fail("Click on Footer2 YouTube link failed ");
			}
			return this;
		}
		/**
		 * Verify Footer2 Copy Right Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyCopyRightLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footer_link_bottom_8));
				Assert.assertTrue(copyright.getText().equalsIgnoreCase(Constants.UNAV_FOOTER2_bottom_COPYRIGHT));
				Reporter.log(copyright.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(copyright.getText()+" is not dispalyed");
			}
			return this;
		}
		// Mobile Clicks on Wireless Page
		/**
		 * Click on Phones and devices Expand link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPhonesExpandButton() {
			try {
				if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton0.click();
				Reporter.log("Clicked on Phone and devices Expnad Button");
				}
			} catch (Exception e) {
				Reporter.log("Click on Phone and devices Expnad Button failed");
				Assert.fail("Click on Phone and devices Expnad Button failed");
			}
			return this;
		}
		/**
		 * Click on  Apps And Connected devices Expand link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickAppsExpandButton() {
			try {
				if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton1.click();
				Reporter.log("Clicked on Apps And Connected devices Expnad Button");
				}
			} catch (Exception e) {
				Reporter.log("Click on Apps And Connected devices Expnad Button failed");
				Assert.fail("Click on  Apps And Connected devices Expnad Button failed");
			}
			return this;
		}
		/**
		 * Click on Plans and information Expand link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPlansExpandButton() {
			try {
				if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton2.click();
				Reporter.log("Clicked on Plans and information Expnad Button");
				}
			} catch (Exception e) {
				Reporter.log("Click on Plans  and information Expnad Button failed");
				Assert.fail("Click on Plans  and information Expnad Button failed");
			}
			return this;
		}
		/**
		 * Click on Switch to T-Mobile Expand link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickSwitchExpandButton() {
			try {
				if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton3.click();
				Reporter.log("Clicked on Switch to T-Mobile Expnad Button");
				}
			} catch (Exception e) {
				Reporter.log("Click on Switch to T-Mobile Expnad Button failed");
				Assert.fail("Click on Switch to T-Mobile Expnad Button failed");
			}
			return this;
		}
		/**
		 * Click on T-Mobile Benefits Expand link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickBenefitsExpandButton() {
			try {
				if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton4.click();
				Reporter.log("Clicked on T-Mobile Benefits Expnad Button");
				}
			} catch (Exception e) {
				Reporter.log("Click on T-Mobile Benefits Expnad Button failed");
				Assert.fail("Click on T-Mobile Benefits Expnad Button failed");
			}
			return this;
		}
		/**
		 * Click on Order Info Expand link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickOrderInfoExpandButton() {
			try {
				if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton5.click();
				Reporter.log("Clicked on Order Info Expnad Button");
				}
			} catch (Exception e) {
				Reporter.log("Click on Order Info Expnad Button failed");
				Assert.fail("Click on Order Info Expnad Button failed");
			}
			return this;
		}
		/**
		 * Click on Support Expand link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickSupportExpandButton() {
			try {
				if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton6.click();
				Reporter.log("Clicked on Support Expnad Button");
				}
			} catch (Exception e) {
				Reporter.log("Click on Support Expnad Button failed");
				Assert.fail("Click on Support Expnad Button failed");
			}
			return this;
		}
		/**
		 * Click on My Account Expand link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickMyAccountExpandButton() {
			try {
				if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton7.click();
				Reporter.log("Clicked on  My Account Expnad Button");
				}
			} catch (Exception e) {
				Reporter.log("Click on  My Account Expnad Button failed");
				Assert.fail("Click on  My Account Expnad Button failed");
			}
			return this;
		}
		/**
		 * Click on More Than Wireless Expand link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickMoreThanWirelessExpandButton() {
			try {
				if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton8.click();
				Reporter.log("Clicked on  More Than Wireless Expnad Button");
				}
			} catch (Exception e) {
				Reporter.log("Click on  More Than Wireless Expnad Button failed");
				Assert.fail("Click on  More Than Wireless Expnad Button failed");
			}
			return this;
		}
		/**
		 * Click on About T-Mobile Expand link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickAboutTMobileExpandButton() {
			try {
				if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton9.click();
				Reporter.log("Clicked on About T-Mobile Expnad Button");
				}
			} catch (Exception e) {
				Reporter.log("Click on About T-Mobile Expnad Button failed");
				Assert.fail("Click on About T-Mobile Expnad Button failed");
			}
			return this;
		}
		/**
		 * Click on Corporate Responsibility Expand link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickCorporateResponsibilityExpandButton() {
			try {
				if (getDriver() instanceof AppiumDriver) {				
				checkPageIsReady();
				expandButton10.click();
				Reporter.log("Clicked on Corporate Responsibility Expnad Button");
				}
			} catch (Exception e) {
				Reporter.log("Click on Corporate Responsibility Expnad Button failed");
				Assert.fail("Click on Corporate Responsibility Expnad Button failed");
			}
			return this;
		}
		/**
		 * Click on Careers Expand link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickCareersExpandButton() {
			try {
				if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				expandButton11.click();
				Reporter.log("Clicked on Careers Expnad Button");
				}
			} catch (Exception e) {
				Reporter.log("Click on Careers Expnad Button failed");
				Assert.fail("Click on Careers Expnad Button failed");
			}
			return this;
		}
		// Verify UNAV About Us Footer

		/**
		 * Verify Footer Contact Us Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyContactusLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_0));
				Assert.assertTrue(footerLabel_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_CONTACTUS_LABEL));
				Reporter.log(footerLabel_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer Contact Information Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyContactInfoLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_0_0));
				Assert.assertTrue(subFooterLink_0_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_CONTACTINFO));
				Reporter.log(subFooterLink_0_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_0_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Contact Information link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickContactInfolink() {
			try {
				checkPageIsReady();
				subFooterLink_0_0.click();
				Reporter.log("Clicked on Contact Information Link");
			} catch (Exception e) {
				Reporter.log("Click on Contact Information failed");
				Assert.fail("Click on Contact Information failed");
			}
			return this;
		}

		/**
		 * Verify Contact Information page.
		 *
		 * @return the Contact Information Page class instance.
		 */
		public TMNGNewFooterPage verifyContactInfoPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_CONTACTINFO_LINK);
				Reporter.log("Contact Information Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Contact Information Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Check Order Status Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyAboutUsCheckOrderStatusLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_0_1));
				Assert.assertTrue(subFooterLink_0_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_CHECKORDERSTAT));
				Reporter.log(subFooterLink_0_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_0_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Check Order Status link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickAboutUsCheckOrderStatuslink() {
			try {
				checkPageIsReady();
				subFooterLink_0_1.click();
				Reporter.log("Clicked on Check Order Status Link");
			} catch (Exception e) {
				Reporter.log("Click on Check Order Status failed");
				Assert.fail("Click on Check Order Status failed");
			}
			return this;
		}

		/**
		 * Verify Check Order Status page.
		 *
		 * @return the Check Order Status Page class instance.
		 */
		public TMNGNewFooterPage verifyAboutUsCheckOrderStatusPage() {
			try {
				checkPageIsReady();
				//verifyPageUrl(Constants.UNAV_FOOTER_CHECKORDERSTAT_LINK);
				verifyPageUrl(Constants.UNAV_FOOTER_subFooterLink_7_1_LINK);
				Reporter.log("Check Order Status Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Check Order Status Page not displayed");
			}
			return this;
		}
		/**
		 * Verify View Return Policy Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyAboutUsViewReturnPolicyLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_0_2));
				Assert.assertTrue(subFooterLink_0_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_VIEWRETURN));
				Reporter.log(subFooterLink_0_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_0_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click View Return Policy link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickAboutUsViewReturnPolicylink() {
			try {
				checkPageIsReady();
				subFooterLink_0_2.click();
				Reporter.log("Clicked on View Return Policy Link");
			} catch (Exception e) {
				Reporter.log("Click on View Return Policy failed");
				Assert.fail("Click on View Return Policy failed");
			}
			return this;
		}

		/**
		 * Verify View Return Policy page.
		 *
		 * @return the View Return Policy Page class instance.
		 */
		public TMNGNewFooterPage verifyAboutUsViewReturnPolicyPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_VIEWRETURN_LINK);
				Reporter.log("View Return Policy Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("View Return Policy Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Get A Rebate Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyGetARebateLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_0_3));
				Assert.assertTrue(subFooterLink_0_3.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_GETAREBATE));
				Reporter.log(subFooterLink_0_3.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_0_3.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Get A Rebate link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickGetARebatelink() {
			try {
				checkPageIsReady();
				subFooterLink_0_3.click();
				Reporter.log("Clicked on Get A Rebate Link");
			} catch (Exception e) {
				Reporter.log("Click on Get A Rebate failed");
				Assert.fail("Click on Get A Rebate failed");
			}
			return this;
		}

		/**
		 * Verify Get A Rebate page.
		 *
		 * @return the Get A Rebate Page class instance.
		 */
		public TMNGNewFooterPage verifyGetARebatePage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_GETAREBATE_LINK);
				Reporter.log("Get A Rebate Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Get A Rebate Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Find A Store Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyFindAStoreLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_0_4));
				Assert.assertTrue(subFooterLink_0_4.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_FINDASTORE));
				Reporter.log(subFooterLink_0_4.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_0_4.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Find A Store link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickFindAStorelink() {
			try {
				checkPageIsReady();
				subFooterLink_0_4.click();
				Reporter.log("Clicked on Find A Store Link");
			} catch (Exception e) {
				Reporter.log("Click on Find A Store failed");
				Assert.fail("Click on Find A Store failed");
			}
			return this;
		}

		/**
		 * Verify Find A Store page.
		 *
		 * @return the Find A Store Page class instance.
		 */
		public TMNGNewFooterPage verifyFindAStorePage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_FINDASTORE_LINK);
				Reporter.log("Find A Store Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Find A Store Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Trade In Program Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyAboutUsTradeInProgramLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_0_5));
				Assert.assertTrue(subFooterLink_0_5.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_TRADEINPRG));
				Reporter.log(subFooterLink_0_5.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_0_5.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Trade In Program link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickAboutUsTradeInProgramlink() {
			try {
				checkPageIsReady();
				subFooterLink_0_5.click();
				Reporter.log("Clicked on Trade In Program Link");
			} catch (Exception e) {
				Reporter.log("Click on Trade In Program failed");
				Assert.fail("Click on Trade In Program failed");
			}
			return this;
		}

		/**
		 * Verify Trade In Program page.
		 *
		 * @return the Trade In Program Page class instance.
		 */
		public TMNGNewFooterPage verifyAboutUsTradeInProgramPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_TRADEINPRG_LINK);
				Reporter.log("Trade In Program Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Trade In Program Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Footer Support Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyAboutUsSupportLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_1));
				Assert.assertTrue(footerLabel_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_SUPPORT_LABEL));
				Reporter.log(footerLabel_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Footer Device Support Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyDeviceSupportLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_1_0));
				Assert.assertTrue(subFooterLink_1_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_DEVICESUPPORT));
				Reporter.log(subFooterLink_1_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_1_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Device Support link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickDeviceSupportlink() {
			try {
				checkPageIsReady();
				subFooterLink_1_0.click();
				Reporter.log("Clicked on Device Support Link");
			} catch (Exception e) {
				Reporter.log("Click on Device Support failed");
				Assert.fail("Click on Device Support failed");
			}
			return this;
		}

		/**
		 * Verify Device Support page.
		 *
		 * @return the Device Support Page class instance.
		 */
		public TMNGNewFooterPage verifyDeviceSupportPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_DEVICESUPPORT_LINK);
				Reporter.log("Device Support Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Device Support Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Questions about your bill Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyQuesAboutBillLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_1_1));
				Assert.assertTrue(subFooterLink_1_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_QUES));
				Reporter.log(subFooterLink_1_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_1_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Questions about your bill link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickQuesAboutBilllink() {
			try {
				checkPageIsReady();
				subFooterLink_1_1.click();
				Reporter.log("Clicked on Questions about your bill Link");
			} catch (Exception e) {
				Reporter.log("Click on Questions about your bill failed");
				Assert.fail("Click on Questions about your bill failed");
			}
			return this;
		}

		/**
		 * Verify Questions about your bill page.
		 *
		 * @return the Questions about your bill Page class instance.
		 */
		public TMNGNewFooterPage verifyQuesAboutBillPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_QUES_LINK);
				Reporter.log("Questions about your bill Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Questions about your bill Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Plans & services Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyPlansServiceLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_1_2));
				Assert.assertTrue(subFooterLink_1_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_PLANSSERVICE));
				Reporter.log(subFooterLink_1_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_1_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Plans & services link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickPlansServicelink() {
			try {
				checkPageIsReady();
				subFooterLink_1_2.click();
				Reporter.log("Clicked on Plans & services Link");
			} catch (Exception e) {
				Reporter.log("Click on Plans & services failed");
				Assert.fail("Click on Plans & services failed");
			}
			return this;
		}

		/**
		 * Verify Plans & services page.
		 *
		 * @return the Plans & services Page class instance.
		 */
		public TMNGNewFooterPage verifyPlansServicePage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_PLANSSERVICE_LINK);
				Reporter.log("Plans & services Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Plans & services Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Activate your prepaid phone or device Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyActivePhoneLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_1_3));
				Assert.assertTrue(subFooterLink_1_3.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_ACTIVE));
				Reporter.log(subFooterLink_1_3.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_1_3.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Activate your prepaid phone or device link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickActivePhonelink() {
			try {
				checkPageIsReady();
				subFooterLink_1_3.click();
				Reporter.log("Clicked on Activate your prepaid phone or device Link");
			} catch (Exception e) {
				Reporter.log("Click on Activate your prepaid phone or device failed");
				Assert.fail("Click on Activate your prepaid phone or device failed");
			}
			return this;
		}

		/**
		 * Verify Activate your prepaid phone or device page.
		 *
		 * @return the Activate your prepaid phone or device Page class instance.
		 */
		public TMNGNewFooterPage verifyActivePhonePage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_ACTIVE_LINK);
				Reporter.log("Activate your prepaid phone or device Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Activate your prepaid phone or device Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Refill your prepaid account Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyRefillAccountLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_1_4));
				Assert.assertTrue(subFooterLink_1_4.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_REFILL));
				Reporter.log(subFooterLink_1_4.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_1_4.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Refill your prepaid account link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickRefillAccountlink() {
			try {
				checkPageIsReady();
				subFooterLink_1_4.click();
				Reporter.log("Clicked on Refill your prepaid account Link");
			} catch (Exception e) {
				Reporter.log("Click on Refill your prepaid account failed");
				Assert.fail("Click on Refill your prepaid account failed");
			}
			return this;
		}

		/**
		 * Verify Refill your prepaid account page.
		 *
		 * @return the Refill your prepaid account Page class instance.
		 */
		public TMNGNewFooterPage verifyRefillAccountPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_REFILL_LINK);
				Reporter.log("Refill your prepaid account Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Refill your prepaid account Page not displayed");
			}
			return this;
		}
		/**
		 * Verify International rates Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyInternationalRatesLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_1_5));
				Assert.assertTrue(subFooterLink_1_5.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_INTERRATES));
				Reporter.log(subFooterLink_1_5.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_1_5.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click International rates link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickInternationalRateslink() {
			try {
				checkPageIsReady();
				subFooterLink_1_5.click();
				Reporter.log("Clicked on International rates Link");
			} catch (Exception e) {
				Reporter.log("Click on International rates failed");
				Assert.fail("Click on International rates failed");
			}
			return this;
		}

		/**
		 * Verify International rates page.
		 *
		 * @return the International rates Page class instance.
		 */
		public TMNGNewFooterPage verifyInternationalRatesPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_INTERRATES_LINK);
				Reporter.log("International rates Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("International rates Page not displayed");
			}
			return this;
		}
		/**
		 * Verify T-Mobile for Business Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyTMobileBusinessLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(footerLabel_2));
				Assert.assertTrue(footerLabel_2.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_BUSINESS_LABEL));
				Reporter.log(footerLabel_2.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(footerLabel_2.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Verify Business plans Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyBusinessPlansLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_2_0));
				Assert.assertTrue(subFooterLink_2_0.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_BUSINESSPLAN));
				Reporter.log(subFooterLink_2_0.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_2_0.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Business plans link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickBusinessPlanslink() {
			try {
				checkPageIsReady();
				subFooterLink_2_0.click();
				Reporter.log("Clicked on Business plans Link");
			} catch (Exception e) {
				Reporter.log("Click on Business plans failed");
				Assert.fail("Click on Business plans failed");
			}
			return this;
		}

		/**
		 * Verify Business plans page.
		 *
		 * @return the Business plans Page class instance.
		 */
		public TMNGNewFooterPage verifyBusinessPlansPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_BUSINESSPLAN_LINK);
				Reporter.log("Business plans Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Business plans Page not displayed");
			}
			return this;
		}
		/**
		 * Verify Internet of Things Label
		 * 
		 * @return
		 */
		public TMNGNewFooterPage verifyInternetOfThingsLabel() {
			try {
				Assert.assertTrue(isElementDisplayed(subFooterLink_2_1));
				Assert.assertTrue(subFooterLink_2_1.getText().equalsIgnoreCase(Constants.UNAV_FOOTER_INTERNET));
				Reporter.log(subFooterLink_2_1.getText()+" is displayed");
			} catch (Exception e) {
				Assert.fail(subFooterLink_2_1.getText()+" is not dispalyed");
			}
			return this;
		}
		/**
		 * Click Internet of Things link
		 * 
		 * @return
		 */
		public TMNGNewFooterPage clickInternetOfThingslink() {
			try {
				checkPageIsReady();
				subFooterLink_2_1.click();
				Reporter.log("Clicked on Internet of Things Link");
			} catch (Exception e) {
				Reporter.log("Click on Internet of Things failed");
				Assert.fail("Click on Internet of Things failed");
			}
			return this;
		}

		/**
		 * Verify Internet of Things page.
		 *
		 * @return the Internet of Things Page class instance.
		 */
		public TMNGNewFooterPage verifyInternetOfThingsPage() {
			try {
				checkPageIsReady();
				verifyPageUrl(Constants.UNAV_FOOTER_INTERNET_LINK);
				Reporter.log("Internet of Things Page is displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Internet of Things Page not displayed");
			}
			return this;
		}

		// Mobile Clicks on About Us Page
				/**
				 * Click on Contact Us Expand link
				 * 
				 * @return
				 */
				public TMNGNewFooterPage clickContactUsExpandButton() {
					try {
						if (getDriver() instanceof AppiumDriver) {
						checkPageIsReady();
						expandButton0.click();
						Reporter.log("Clicked on Contact Us Expnad Button");
						}
					} catch (Exception e) {
						Reporter.log("Click on Contact Us Expnad Button failed");
						Assert.fail("Click on Contact Us Expnad Button failed");
					}
					return this;
				}
				/**
				 * Click on Support Expand link
				 * 
				 * @return
				 */
				public TMNGNewFooterPage clickAboutUsSupportExpandButton() {
					try {
						if (getDriver() instanceof AppiumDriver) {
						checkPageIsReady();
						expandButton1.click();
						Reporter.log("Clicked on Support Expnad Button");
						}
					} catch (Exception e) {
						Reporter.log("Click on Support Expnad Button failed");
						Assert.fail("Click on Support Expnad Button failed");
					}
					return this;
				}
				/**
				 * Click on T-Mobile for Business Expand link
				 * 
				 * @return
				 */
				public TMNGNewFooterPage clickTMobileBusinessExpandButton() {
					try {
						if (getDriver() instanceof AppiumDriver) {
						checkPageIsReady();
						expandButton2.click();
						Reporter.log("Clicked on T-Mobile for Business Expnad Button");
						}
					} catch (Exception e) {
						Reporter.log("Click on T-Mobile for Business Expnad Button failed");
						Assert.fail("Click on T-Mobile for Business Expnad Button failed");
					}
					return this;
				}
}