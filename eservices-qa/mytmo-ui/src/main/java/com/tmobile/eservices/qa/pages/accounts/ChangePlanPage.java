/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class ChangePlanPage extends CommonPage {

	@FindAll({ @FindBy(xpath = "//span[text()='Mobile Internet 2GB']//..//..//input"),
			@FindBy(xpath = "//span[text()='Mobile Internet 6GB']//..//..//input") })
	private WebElement mobileInternet;

	@FindBy(id = "nextButton_id")
	private WebElement nextButton;

	@FindBy(id = "changeNoticeContinueBtn")
	private WebElement continueButton;

	@FindAll({ @FindBy(css = "#reviewPageContainer_id > div.ui_headline.pt30"),
			@FindBy(css = "#reviewPageContainer_id > div.co_review") })
	private WebElement reviewChangesheader;

	@FindBy(id = "addedServiceName_id")
	private WebElement mobileInternetTwoGBAdded;

	@FindBy(xpath = "//*[@id='dynamicData_id']//..//input[@value='3']")
	private WebElement tMobileonePlan;

	@FindAll({ @FindBy(id = "currentMonthlyTotal_id"),
			@FindBy(css = "#reviewPageContainer_id > div.monthly-total > div.current-monthly-total") })
	private WebElement currentMonthlytotal;

	@FindAll({ @FindBy(id = "newTotal_id"),
			@FindBy(css = "#reviewPageContainer_id > div.monthly-total > div:nth-child(1)") })
	private WebElement newMonthlytotal;

	@FindBy(id = "removedServiceName_id")
	private WebElement removedExistingservice;

	/**
	 * 
	 * @param webDriver
	 */
	public ChangePlanPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Click Mobile Intenet 2G
	 */
	public ChangePlanPage clickMobileinternetTwoGB() {
		checkPageIsReady();
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.elementToBeClickable(mobileInternet));
			mobileInternet.click();
			Reporter.log("Selected 2 Gb Mobile Internet");
		} catch (Exception e) {
			Assert.fail("2 Gb Mobile Internet Check box is not selected");
		}
		return this;
	}

	/**
	 * Click Next button
	 */
	public ChangePlanPage clickNextbutton() {
		try {
			checkPageIsReady();
			nextButton.click();
			Reporter.log("Clicked on Next Button");
		} catch (Exception e) {
			Assert.fail("Not clicked on the next Button");
		}
		return this;
	}

	/**
	 * Click Continue Button
	 */
	public ChangePlanPage clickContinuebtn() {
		try {
			checkPageIsReady();
			continueButton.click();
			Reporter.log("Clicked on Change Notice Pop Up Continue Button");
		} catch (Exception e) {
			Assert.fail("Not clicked on the Continue Button");
		}
		return this;
	}

	/**
	 * Verify Review Changes is displayed
	 * 
	 * @return
	 */
	public ChangePlanPage verifyReviewChangespage() {
		checkPageIsReady();
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("plans-review"));
			//Assert.assertTrue(reviewChangesheader.isDisplayed(), "Review Change page is not displayed");
			Reporter.log("Review Change page is displayed");
		} catch (Exception e) {
			Assert.fail("Review Change page is not displayed");
		}
		return this;
	}

	/**
	 * Get a Text of Mobile Internet of Two GB
	 * 
	 * @return
	 */
	public ChangePlanPage getTextMobileinternetTwoGB(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(mobileInternetTwoGBAdded.getText().contains(msg),"Mobile Internet 2GB is  not Matched");
			Reporter.log("Mobile Internet 2GB is Matched");
		} catch (Exception e) {
			Assert.fail("Mobile Internet 2GB is not Matched");
		}
		return this;
	}

	/**
	 * Click T-Mobile One Wearable plan
	 */
	public ChangePlanPage clickTmobileOneWearable() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(tMobileonePlan));
			tMobileonePlan.click();
			Reporter.log("Clicked on TMobile one plan");
		} catch (Exception e) {
			Assert.fail("Not clicked on TMobile one plan");
		}
		return this;
	}

	/**
	 * Verify New monthly total is displayed
	 * 
	 * @return
	 */
	public ChangePlanPage verifycurrentmonthlyTotaldisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(currentMonthlytotal.isDisplayed());
			Reporter.log("Current Monthly tab is displayed");
		} catch (Exception e) {
			Assert.fail("Current Monthly tab is not displayed");
		}
		return this;
	}

	/**
	 * Verify Current monthly total is displayed
	 * 
	 * @return
	 */
	public ChangePlanPage verifyNewmonthlyTotaldisplayed() {
		try {
			checkPageIsReady();
			Assert.assertTrue(newMonthlytotal.isDisplayed());
			Reporter.log("New Monthly tab is displayed");
		} catch (Exception e) {
			Assert.fail("New Monthly tab is not displayed");
		}
		return this;
	}

	/**
	 * Get Tex from removed service name
	 * 
	 * @return
	 */
	public ChangePlanPage getTextremovedServices(String msg) {
		try {
			checkPageIsReady();
			Assert.assertTrue(removedExistingservice.getText().contains(msg),
					"Mobile Internet 6GB text is not present in Removed text");
			Reporter.log("Mobile Internet 6GB text is present in Removed text");
		} catch (Exception e) {
			Assert.fail("" + msg + " text is not displayed in Removed text");
		}
		return this;
	}
}
