package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.common.ShopConstants;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class JWTTokenApi extends ApiCommonLib {

	/***
	 * summary: post the request to create a new token description: 'This method
	 * is used to create DCP Token ' Headers:* Authorization accept
	 * applicationid cache-control channelid clientid content-type usn
	 * interactionid transactionBusinessKey phoneNumber
	 * transactionbusinesskeytype correlationid transactionid transactiontype
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getCode(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		String accessToken = "";
		String requestBody = "";
		String operationName = "JWT_generateCode";
		Response response = null;
		requestBody = new ServiceTest().getRequestFromFile("JWT_generateCode.txt");

		String misdinpwd = getMsisdnAndPaswordFromFilter(apiTestData);
		if (misdinpwd == null) {
			Assert.fail("No valid data identified");
		}

		String misdin = misdinpwd.split("/")[0]; // msisdn
		String pwd = misdinpwd.split("/")[1]; // password

		apiTestData.setMsisdn(misdin);
		apiTestData.setPassword(pwd);

		// invokeClearCounters(apiTestData);

		tokenMap.put("pahMsisdn", apiTestData.getMsisdn());
		tokenMap.put("msisdn", apiTestData.getMsisdn());
		tokenMap.put("password", apiTestData.getPassword());
		tokenMap.put("redirect_uri", getRedirectURL(backend));
		tokenMap.put("scope", getScopeInfo());

		if (backend.equalsIgnoreCase("stage") || backend.equalsIgnoreCase("prod")
				|| backend.equalsIgnoreCase("stage2")) {
			tokenMap.put("client_id", "MYTMO");
			tokenMap.put("client_secret", "tFKWQodJkjqcEx7VeHVP3eZrvqo8mVYzq9ed6P37P5F2PUFujn");
			tokenMap.put("version", "v1");
		} else if (backend.equalsIgnoreCase("qlab01")) {
			tokenMap.put("client_id", "MYTMO3");
			tokenMap.put("client_secret", "mytmo3Password");
			tokenMap.put("version", "v3");
		} else {
			tokenMap.put("client_id", "MYTMO");
			tokenMap.put("client_secret", "MYTMOSecret");
			tokenMap.put("version", "v3");
		}

		String baseURI = getJwtUri();
		String resourceURI = getBrassCodeResourceUrl();

		String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logLargeRequest(updatedRequest, operationName);

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");

		response = invokeRestServiceCall(headers, baseURI, resourceURI, updatedRequest, RestServiceCallType.POST);

		if (response.statusCode() == 200) {
			logLargeResponse(response, operationName);

			if (null != JsonPath.from(response.asString()).get("code")) {
				System.out.println("Code:" + JsonPath.from(response.asString()).get("code").toString());
				return JsonPath.from(response.asString()).get("code").toString();
			} else {
				return "";
			}
		} else {

			Reporter.log("<b>" + operationName + " Exception Response :</b> ");
			Reporter.log("Response status code : " + response.getStatusCode());
			Reporter.log("<b>" + operationName + " Response : </b>" + response.body().asString());
			accessToken = "getCodeException : with <b>" + baseURI + "/" + resourceURI + "</b> "
					+ response.body().asString();
			exceptionMsgs.put(ShopConstants.ERROR_MSG, accessToken);
		}
		return accessToken;
	}

	public Map<String, String> getJwtAuthToken(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {

		String authCode = null;

		String emailVal = "";

		authCode = getCode(apiTestData, tokenMap);
		/*
		 * if (StringUtils.isEmpty(authCode)) { throw new
		 * Exception("Null or Empty code from getCode"); } else if
		 * (authCode.contains("getCodeException")) { if
		 * (authCode.contains("locked")) { if (flagForClearCounters) {
		 * //invokeClearCounters(apiTestData); //authCode = getCode(apiTestData,
		 * tokenMap); if (StringUtils.isEmpty(authCode)) { throw new
		 * Exception("Null or Empty code from getCode"); } else if
		 * (authCode.contains("getCodeException")) { throw new
		 * Exception(authCode); } } else { throw new Exception(authCode); } }
		 * else { throw new Exception(authCode); } }
		 */
		// prepare for userToken call
		tokenMap.put("code", authCode);

		String operationName = "usertoken";
		Response response = null;		
		try {

			response = getUserToken(tokenMap);
			if (response.statusCode() == 200) {
				logLargeResponse(response, operationName);

				JsonPath jsonPathEvaluator = response.jsonPath();

				String ban = "";
				String userType="";

				if (null != jsonPathEvaluator.get("userInfo.associatedLines.findAll{it.msisdn=='"
						+ apiTestData.getMsisdn() + "'}[0].primaryBillingAccountCode")) {
					if (null != jsonPathEvaluator.get("userInfo.associatedLines.findAll{it.msisdn=='"
							+ apiTestData.getMsisdn() + "'}[0].primaryBillingAccountCode").toString()) {
						ban = jsonPathEvaluator.get("userInfo.associatedLines.findAll{it.msisdn=='"
								+ apiTestData.getMsisdn() + "'}[0].primaryBillingAccountCode").toString();
					}
				}
				
				if (null != jsonPathEvaluator.get("userInfo.associatedLines.findAll{it.msisdn=='" + apiTestData.getMsisdn()
						+ "'}[0].multiLineCustomerType")) {
					if (null != jsonPathEvaluator.get("userInfo.associatedLines.findAll{it.msisdn=='" + apiTestData.getMsisdn()
					+ "'}[0].multiLineCustomerType").toString()) {
						userType = jsonPathEvaluator.get("userInfo.associatedLines.findAll{it.msisdn=='" + apiTestData.getMsisdn()
						+ "'}[0].multiLineCustomerType").toString();
					}
				}
				
				if (StringUtils.isBlank(ban)) {
					ban = apiTestData.getBan();
				}

				if (StringUtils.isNoneEmpty(ban)) {
					apiTestData.setBan(ban);
					tokenMap.put("ban", apiTestData.getBan());
				} else {
					Assert.fail("No Valid BAN idetntified");
				}

				String accessToken = JsonPath.from(response.asString()).get("access_token").toString();

				if (jsonPathEvaluator.get("id_tokens[0].'id_token.direct'").toString() != null) {
					System.out.println(jsonPathEvaluator.get("id_tokens[0].'id_token.direct'").toString());
					String jwtToken = JsonPath.from(response.asString()).get("id_tokens[0].'id_token.direct'")
							.toString();

					if (null != JsonPath.from(response.asString()).get("userInfo.iamProfile")) {
						if (null != JsonPath.from(response.asString()).get("userInfo.iamProfile.email")) {
							emailVal = JsonPath.from(response.asString()).get("userInfo.iamProfile.email").toString();
						}
					} else {
						emailVal = apiTestData.getMsisdn() + "@yopmail.com"; // default
																				// email
					}

					Map<String, String> jwtTokens = new HashMap<String, String>();
					jwtTokens.put("jwtToken", jwtToken.trim());
					jwtTokens.put("accessToken", accessToken.trim());
					jwtTokens.put("emailVal", emailVal);
					jwtTokens.put("code", authCode);

					System.out.println("jwtToken =" + jwtToken);
					System.out.println("accessToken =" + accessToken);
					tokenMapCommon = new HashMap<String, String>();
					tokenMapCommon.put("jwtToken", jwtToken.trim());
					tokenMapCommon.put("accessToken", accessToken.trim());
					tokenMapCommon.put("emailVal", emailVal);
					tokenMapCommon.put("userType", userType);
					tokenMap.put("email", emailVal);
					tokenMap.put("userType", userType);
					
					//Map<String, String> userDetails = getUserDetailsFromUsertokenResponse(response,tokenMap.get("msisdn"));
					//tokenMap.putAll(userDetails);
					return jwtTokens;
				}
			} else {
				Reporter.log("<b>" + operationName + " Exception Response :</b> ");
				Reporter.log("Response status code : " + response.getStatusCode());
				Reporter.log("<b>" + operationName + " Response : </b>" + response.body().asString());
				failAndLogResponse(response, operationName);
			}

		} catch (Exception e) {
			System.out.println(e);
			Reporter.log("Exception : occurred." + e.getStackTrace());
			failAndLogResponse(response, operationName);
		}

		return null;
	}

	public Response getUserToken(Map<String, String> tokenMap) {
		String operationName = "usertoken";
		Response response = null;
		try {
			String requestBody = new ServiceTest().getRequestFromFile("JWT_usertoken.txt");

			if ("qlab01".equalsIgnoreCase(environment)||"qlab02".equalsIgnoreCase(environment)||"qlab03".equalsIgnoreCase(environment)||"qlab06".equalsIgnoreCase(environment)||"stage".equalsIgnoreCase(environment)||"stage2".equalsIgnoreCase(environment)||"prod".equalsIgnoreCase(environment)) {
				tokenMap.put("scope", "TMO_ID_profile associated_lines extended_lines");
			}

			String updatedRequest = prepareRequestParam(requestBody, tokenMap);
			logLargeRequest(updatedRequest, operationName);
			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Content-Type", "application/json");
			response = invokeRestServiceCall(headers, getUserTokenUrl(), getBrassUserTokenResourceUrl(), updatedRequest,
					RestServiceCallType.POST);
		} catch (Exception e) {
			Reporter.log("Exception : occurred." + e.getStackTrace());
			failAndLogResponse(response, operationName);
		}

		return response;
	}

	public void registerJWTTokenforAPIGEE(String apigeeproxy, String jwtToken, Map<String, String> tokenMap)
			throws Exception {
		String version = "";
		String requestBody = "";
		RestService service = null;
		Response response = null;
		String operationName = "registerJWTTokenforAPIGEE";
		tokenMap.put("version", "v1");
		try {

			if (StringUtils.isNoneEmpty(tokenMap.get("version"))) {
				version = tokenMap.get("version");

			}
			if ("v1".equals(version)) {
				// Register in ONPREM
				service = new RestService(requestBody, getEOSEndPoint("ms_" + backend));
				service.addHeader("authorization", jwtToken);
				service.addHeader("Content-Type", "application/json");
				service.addHeader("apigee-proxy", apigeeproxy);
				response = service.callService("v1/eosoauthmanager/usertoken", RestCallType.POST);
				System.out.println("check point2");

			} else {
				service = new RestService(requestBody, oAuth.get(backend));
				service.addHeader("authorization", jwtToken);
				response = service.callService("tms/v3/usertoken", RestCallType.POST);
			}

			if (response.statusCode() == 200) {
				logLargeResponse(response, operationName);
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(response.body().asString());
				if (!jsonNode.isMissingNode()) {
					Assert.assertTrue(response.statusCode() == 200);
					String status = jsonNode.get("status").asText();
					Assert.assertEquals(status, "success", "Invalid register status.");
				}

			} else {
				Reporter.log("<b>" + operationName + " Exception Response :</b> ");
				Reporter.log("Response status code : " + response.getStatusCode());
				Reporter.log("<b>" + operationName + " Response : </b>" + response.body().asString());
			}
		} catch (Exception e) {
			System.out.println(e);
			Reporter.log("Exception : occurred." + e.getStackTrace());
		}
	}

}
