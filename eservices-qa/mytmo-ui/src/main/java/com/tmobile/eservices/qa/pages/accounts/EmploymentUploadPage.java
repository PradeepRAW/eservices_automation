package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class EmploymentUploadPage extends CommonPage {

	private static final String pageUrl = " account/employment/upload";

	// private static final String noPermissionsPageUrl = "
	// profile/employee/nopermission";

	private static final String TITLE = "We need more information";

	/**
	 * @param webDriver
	 */
	public EmploymentUploadPage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(css = "div.TMO-SCHEDULED-ACTIVITY-PAGE")
	private WebElement pageLoadElement;

	@FindBy(css = "button.btn.btn-primary.padding-bottom-large")
	private WebElement yesCancelAutopay;

	@FindBy(xpath = "//*[contains(text(),'Employment verification')]")
	private WebElement EmploymentVerfication;

	/**
	 * Wait For Spinner
	 *
	 * @return
	 */
	public boolean waitForNewSpinner() {
		List<WebElement> elements = getDriver().findElements(By.cssSelector("div.circle-clipper.right div.circle"));
		try {
			waitFor(ExpectedConditions.invisibilityOfAllElements(elements), 120);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 */
	public EmploymentUploadPage verifyPageTitle() {
		waitFor(ExpectedConditions.titleContains(TITLE));
		return this;
	}

	/**
	 * Verify page loaded completely.
	 *
	 */
	public EmploymentUploadPage verifyPageLoaded() {

		try {
			waitForNewSpinner();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			verifyPageTitle();
			Reporter.log("Employement Verification  page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Employement Verification page is  not displayed");
		}
		return this;
	}

	/**
	 * Verify Employee Document Uploag Page
	 *
	 */
	public EmploymentUploadPage verifyEmployeeDocumentUploagPage() {
		try {
			waitForNewSpinner();
			checkPageIsReady();
			verifyPageLoaded();
			Reporter.log("Employement Verification document upload is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Employement Verification document upload page is not displayed");
		}
		return this;
	}
}
