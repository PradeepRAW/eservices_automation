package com.tmobile.eservices.qa.pages.global;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * 
 * @author rnallamilli
 *
 */
public class NewContactUSPage extends CommonPage {

	public static final String contactUsPageUrl = "/contact-us";
	public static final String contactUsPageLoadText = "Connect with your team";

	@FindAll({ @FindBy(xpath = "//div[@class='assigned'] //h5[contains(text(), 'Connect with')]"),
			@FindBy(id = "lpButtonSupportMobilels") })
	private WebElement contactWithUSPage;

	@FindBy(xpath = "//div[contains(text(), 'Get in touch')]")
	private WebElement getInTouch;

	@FindBy(linkText = "Check order status")
	private WebElement checkOrderStatus;

	@FindBy(xpath = "//h2[contains(text(), 'Contact Us')]")
	private WebElement legacyContactUsPage;

	@FindBy(css = "div[data-target='#hideShowAccordianAccountProfile'] p.padding-vertical-large ")
	private WebElement accountProfile;

	@FindBy(css = "button.scheduleCallButton")
	private WebElement messageUsButton;

	@FindBy(css = ".PrimaryCTA")
	private WebElement messageTeamBtn;

	@FindBy(css = ".SecondaryCTA")
	private WebElement callTeamBtn;

	@FindBy(id = "disabledMessageUs")
	private WebElement messageUsButtonDisabled;

	@FindBy(id = "callMe")
	private WebElement callMeButton;

	@FindBy(id = "callUs")
	private WebElement callUSButton;

	@FindBy(id = "setUpCallModal")
	private WebElement setUpcalldiv;

	@FindBy(id = "category_dd")
	private WebElement categoryTopicSelect;

	private By callScheduleSpinner = By.id("callSchdule_waitcursor");

	@FindBy(css = "button#confirmCallButton div.call-me")
	private WebElement callMeNowButton;

	@FindBy(id = "confirmCallMeModal")
	private WebElement confirmCallMeDiv;

	@FindBy(id = "confirmCallMeDoneButton")
	private WebElement confirmCallMeDoneButton;

	@FindBy(id = "scheduleCallBackButton")
	private WebElement scheduleCallBackButton;

	@FindBy(id = "selectCallModal")
	private WebElement scheduleCallDiv;

	@FindBy(id = "availableSlotsScreen")
	private WebElement scheduleCallDivMobile;

	@FindBy(id = "date_dd")
	private WebElement selectDate;

	@FindBy(css = "div#findDateDropDown select")
	private WebElement selectDateMobile;

	@FindBy(id = "time_dd")
	private WebElement selectTime;

	@FindBy(css = "div#findTimeDropDown select")
	private WebElement selectTimeMobile;

	@FindBy(id = "select_zone_dd")
	private WebElement selectZone;

	@FindBy(css = "div#findTimeZoneDropDown select")
	private WebElement selectZoneMobile;

	@FindBy(css = "div#callback_times_id button")
	private WebElement selectATimeSlotButton;

	@FindBy(id = "findATime")
	private WebElement findATimeButton;

	@FindBy(css = "div#findATimeButton button")
	private WebElement findATimeButtonMobile;

	@FindBy(id = "scheduleModalConfirmButton")
	private WebElement scheduleModalConfirmButton;

	@FindBy(id = "availableSlotConfirm")
	private WebElement scheduleModalConfirmButtonMobile;

	@FindBy(id = "confirmCallScheduleModal")
	private WebElement confirmCallScheduleDiv;

	@FindBy(id = "scheduleConfirmation")
	private WebElement confirmCallScheduleDivMobile;

	@FindBy(id = "CallConfirmDoneButton")
	private WebElement scheduledCallConfirmDoneButton;

	@FindBy(id = "doneButton")
	private WebElement scheduledCallConfirmDoneButtonMobile;

	@FindBy(css = "div.lp_location_center")
	private WebElement chatPopUp;

	@FindBy(css = "div.lp_input_area textarea")
	private WebElement chatTextInput;

	@FindBy(css = "button.lp_send_button div img")
	private WebElement sendTextMsg;

	@FindBy(css = "span.lp_title_text.lp_ltr")
	private WebElement sentChatMsg;

	@FindBy(css = "div.lp_maximized div.lp_close-icon")
	private WebElement closeMessagingWindow;

	@FindBy(css = "div.lp_maximized button.lp_confirm_button")
	private List<WebElement> closeChatConfirmYes;

	@FindBy(css = "div a.lp_csat_rater_star")
	private WebElement chatRatingStar;

	@FindBy(css = "button.lp_submit_button")
	private WebElement submitRating;

	@FindBy(id = "confirmCallModal")
	private List<WebElement> scheduledCallBack;

	@FindAll({ @FindBy(id = "setUpCallModal"), @FindBy(id = "setupNewCallScreen") })
	private WebElement callModalHeaderText;

	@FindBy(id = "makeachange_btn")
	private WebElement makeAchangeBtn;

	@FindBy(id = "makeAChangeSchedule")
	private WebElement makeAChangeScheduleBtn;

	@FindBy(css = "div.error")
	private WebElement errorMsg;

	@FindBy(css = "div.hideShowAccordianParent p")
	private List<WebElement> topLevelLinks;

	@FindBy(linkText = "AutoPay settings")
	private WebElement autopaySettingsLink;

	@FindBy(linkText = "Add family allowances")
	private WebElement addFamilyAllowances;

	@FindBy(linkText = "View app purchases")
	private WebElement viewAppPurchases;

	@FindBy(linkText = "Lost/Stolen device")
	private WebElement lostOrStolenDevice;

	@FindBy(linkText = "Trade in value")
	private WebElement tradeInValue;

	@FindBy(linkText = "Device unlock status")
	private WebElement deviceUnlockStatus;

	@FindBy(linkText = "Upgrade device")
	private WebElement upgradeDevice;

	@FindBy(linkText = "Access voicemail")
	private WebElement accessVoicemail;

	@FindBy(linkText = "File a damage claim")
	private WebElement fileADamageClaim;

	@FindBy(css = "div[data-target='#hideShowAccordianDeviceCoverage']")
	private WebElement accordianDeviceCoverage;

	@FindBy(css = "div[data-target='#hideShowAccordianPlansUsage']")
	private WebElement plansAndUsage;

	@FindBy(css = "#generalCare")
	private WebElement teamOfExpertsDiv;

	@FindBy(css = "#managerImage")
	private WebElement teamPicture;

	@FindBy(css = "#callMe")
	private WebElement callTeamCTA;

	@FindBy(css = ".scheduleCallButton")
	private WebElement messageCTA;

	@FindBys(@FindBy(xpath = "//*[contains(text(),' PM') or contains(text(),' pm')]"))
	private List<WebElement> hoursAvailabiltyDiv;

	@FindBy(css = "#setUpCallModal")
	private WebElement setUpCallPopUp;

	@FindBy(css = "#headline")
	private WebElement setUpCallPopUpHeaderText;

	@FindBy(css = "div.Display3.text-center")
	private WebElement setUpCallPopUpHeaderTextForIOS;

	@FindBy(css = "img.d-lg-block")
	private WebElement teamPictureOnNewAngularPage;

	@FindBy(css = "img.d-block.d-lg-none")
	private WebElement teamPictureOnNewAngularPageForIOS;

	@FindBy(css = ".Display3")
	private WebElement teamOfExpertsDivOnNewAngularPage;

	@FindBy(xpath = "//*[contains(text(),'Make a change')]")
	private WebElement makeAChangeBtn;

	@FindBy(xpath = "//*[text()='Cancel']")
	private WebElement cancelBtn;

	@FindBy(xpath = "//*[text()='Schedule a call']")
	private WebElement scheduleACallBtn;

	@FindBy(xpath = "//*[text()='Confirm']")
	private WebElement confirmBtn;

	@FindBys(@FindBy(css = "button#slot"))
	private List<WebElement> timeSlots;

	@FindBy(xpath = "//*[text()='Done']")
	private WebElement doneBtn;

	@FindBy(css = ".d-none-xxs")
	private WebElement cancelCallBack;

	@FindBy(css = "a[ng-bind='$ctrl.aalWarningModalContent.ctaLabel']")
	private WebElement contactUsButton;

	@FindBy(xpath = "//*[text()='Device & Coverage']")
	private WebElement deviceCoverageHeader;

	@FindBy(xpath = "//*[text()='Plans & Usage ']")
	private WebElement plansUsageHeader;

	@FindBy(css = "[aria-label='click to call us']")
	private WebElement phoneCallIcon;

	@FindBy(css = "[aria-label='click to message us']")
	private WebElement messageUsIcon;

	@FindBy(css = ".PrimaryCTA-accent")
	private WebElement errorModalPopUp;

	@FindBy(css = ".SecondaryCTA.d-md-block")
	private WebElement findATimeBtn;

	public NewContactUSPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Legacy Contact US Page
	 * 
	 * @return
	 */
	public NewContactUSPage verifyLegacyContactUSPage() {
		try {
			Assert.assertTrue(isElementDisplayed(legacyContactUsPage), "Legacy Contact Us page is not displayed");
			Reporter.log("Legacy Contact US page is displayed.");
		} catch (Exception e) {
			Assert.fail("Legacy Contact Us page failed to load");
		}
		return this;
	}

	/**
	 * Verify Contact US Page
	 * 
	 * @return
	 */
	public NewContactUSPage verifyContactUSPageDisplayed() {
		try {
			Assert.assertTrue(isElementDisplayed(contactWithUSPage), "Contact Us page is not displayed");
			Reporter.log("Contact US page is displayed.");
		} catch (Exception e) {
			Assert.fail("Contact Us page failed to load");
		}
		return this;
	}

	/**
	 * Verify Get In Touch
	 * 
	 * @return
	 */
	public NewContactUSPage verifyGetInTouch() {
		try {
			Assert.assertTrue(isElementDisplayed(getInTouch), "Get in touch is not displayed");
			Reporter.log("Get in touch is displayed.");
		} catch (Exception e) {
			Assert.fail("Get in touch failed to load");
		}
		return this;

	}

	/**
	 * Click Check Order Status
	 */
	public NewContactUSPage clickCheckOrderStatus() {
		checkPageIsReady();
		if (getDriver() instanceof AppiumDriver) {
			clickElement(accountProfile);
			clickElement(checkOrderStatus);
		} else {
			clickElement(checkOrderStatus);
		}
		Reporter.log("Clicked on check order status");
		return this;
	}

	/**
	 * click on message US button
	 */
	public NewContactUSPage clickMessageUsBtn() {
		try {
			clickElementWithJavaScript(messageUsButton);
			Reporter.log("Clicked on message us cta");
		} catch (Exception e) {
			Reporter.log("Click on message us button failed ");
		}
		return this;
	}

	/**
	 * click on Call Me button
	 */
	public NewContactUSPage clickCallMeBtn() {
		if (getDriver() instanceof AppiumDriver) {
			clickElement(callMeButton);
		} else {
			clickElement(callMeButton);
		}
		Reporter.log("Clicked on call me button");
		return this;
	}

	/**
	 * select category topic for Call
	 */
	public NewContactUSPage selectCategoryTopicForCall() {
		try {
			Select select = new Select(categoryTopicSelect);
			WebElement selectdOption = select.getFirstSelectedOption();
			for (WebElement options : select.getOptions()) {
				if (!options.equals(selectdOption)) {
					options.click();
				}
			}

			Reporter.log("Clicked on Category Topic For Call");
		} catch (Exception e) {
			Assert.fail("Category Topic For Call is not clicked");
		}
		return this;
	}

	/**
	 * click on Call Me Now button
	 */
	public NewContactUSPage clickCallMeNowBtn() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(callScheduleSpinner));
			callMeNowButton.click();
			Reporter.log("Clicked on call me now button.");
		} catch (NoSuchElementException e) {
			Assert.fail("Unable to click Call Me now button.");
		}
		return this;
	}

	/**
	 * click on scheduled Callback button
	 */
	public NewContactUSPage clickScheduledCallback() {
		clickElement(scheduleCallBackButton);
		Reporter.log("Clicked on schedule Call Back Button");
		return this;
	}

	/**
	 * verify Confirm CallMe modal
	 * 
	 * @return true/false
	 */
	public NewContactUSPage verifyConfirmCallMeDiv() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(callScheduleSpinner));
			Assert.assertTrue(confirmCallMeDiv.isDisplayed());
			Reporter.log("Confirm Call Me modal is displayed.");
		} catch (Exception e) {
			Assert.fail("Confirm Call Me modal is not displayed.");
		}
		return this;
	}

	/**
	 * click on Call Me confirm Done button
	 */
	public NewContactUSPage clickCallMeDoneBtn() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(callScheduleSpinner));
		clickElement(confirmCallMeDoneButton);
		Reporter.log("Clicked on confirm Call Me Done Button");
		return this;
	}

	/**
	 * verify if messaging chat Pop up is displayed or not
	 * 
	 * @return true/false
	 */
	public NewContactUSPage verifyChatPopUp() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("img[alt='Loading']")));
			chatPopUp.isDisplayed();
			Reporter.log("Message us chat pop up is displayed");
		} catch (Exception e) {
			Assert.fail("Message us chat pop up not displayed");
		}
		return this;
	}

	/**
	 * send some text to in chat window
	 */
	public NewContactUSPage sendMessageinChatWindow() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.lp_rotator")));
		sendTextData(chatTextInput, Constants.SETUP_CALL_TEXT);
		clickElement(sendTextMsg);
		return this;
	}

	/**
	 * verify sent messages are displayed in chat Pop up
	 * 
	 * @return true/false
	 */
	public NewContactUSPage verifySentTextInChatPopUp() {
		try {
			Assert.assertTrue(isElementDisplayed(sentChatMsg));
			Reporter.log("Sent messages are displayed in chat pop up.");
		} catch (Exception e) {
			Assert.fail("Sent messages are not displayed in chat pop up.");
		}
		return this;
	}

	/**
	 * close messaging window
	 * 
	 * @return true/false
	 */
	public NewContactUSPage closeChatWindow() {
		clickElement(closeMessagingWindow);
		if (!closeChatConfirmYes.isEmpty() && isElementDisplayed(closeChatConfirmYes.get(0))) {
			clickElement(closeChatConfirmYes);
			if (closeMessagingWindow.isDisplayed()) {
				clickElement(closeMessagingWindow);
			}
		}
		return this;
	}

	/**
	 * submit ratings for the chat
	 */
	public NewContactUSPage submitChatRating() {
		clickElement(chatRatingStar);
		clickElement(submitRating);
		return this;
	}

	/**
	 * select Schedule Date
	 */
	public NewContactUSPage selectScheduleDate() {
		if (getDriver() instanceof AppiumDriver) {
			selectElementFromDropDown(selectDateMobile, Constants.SELECT_BY_INDEX, "1");
		} else {
			selectElementFromDropDown(selectDate, Constants.SELECT_BY_INDEX, "1");
		}
		return this;

	}

	/**
	 * select Schedule Time
	 */
	public NewContactUSPage selectScheduleTime() {
		if (getDriver() instanceof AppiumDriver) {
			selectElementFromDropDown(selectTimeMobile, Constants.SELECT_BY_INDEX, "1");
		} else {
			selectElementFromDropDown(selectTime, Constants.SELECT_BY_INDEX, "1");
		}
		return this;
	}

	/**
	 * select Schedule Zone
	 */
	public NewContactUSPage selectScheduleZone() {
		if (getDriver() instanceof AppiumDriver) {
			selectElementFromDropDown(selectZoneMobile, Constants.SELECT_BY_INDEX, "1");
		} else {
			selectElementFromDropDown(selectZone, Constants.SELECT_BY_INDEX, "1");
		}
		return this;
	}

	/**
	 * click Schedule Slot
	 */
	public NewContactUSPage clickFindATimeButton() {
		if (getDriver() instanceof AppiumDriver) {
			clickElement(findATimeButtonMobile);
		} else {
			clickElement(findATimeButton);
		}
		return this;
	}

	/**
	 * click Schedule Slot
	 */
	public NewContactUSPage clickScheduleSlot() {
		clickElement(selectATimeSlotButton);
		return this;
	}

	/**
	 * click Schedule Call Confirm Button
	 */
	public NewContactUSPage clickScheduleCallConfirmButton() {
		if (getDriver() instanceof AppiumDriver) {
			clickElement(scheduleModalConfirmButtonMobile);
		} else {
			clickElement(scheduleModalConfirmButton);
		}
		return this;
	}

	/**
	 * verify Scheduled CallBack modal
	 * 
	 * @return
	 */
	public NewContactUSPage verifyScheduledCallBackDiv() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				isElementDisplayed(scheduleCallDivMobile);
			} else {
				isElementDisplayed(scheduleCallDiv);
			}
			Reporter.log("Scheduled Callback modal is displayed.");
		} catch (Exception e) {
			Assert.fail("Scheduled Callback modal is not being displayed.");
		}
		return this;

	}

	/**
	 * verify Scheduled CallBack confirm modal
	 * 
	 * @return
	 */
	public NewContactUSPage verifyScheduledCallBackConfirmDiv() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				isElementDisplayed(confirmCallScheduleDivMobile);
			} else {
				isElementDisplayed(confirmCallScheduleDiv);
			}
			Reporter.log("Scheduled Callback confirm modal is displayed.");
		} catch (Exception e) {
			Assert.fail("Scheduled Callback confirm modal is not being displayed.");
		}

		return this;
	}

	/**
	 * click Schedule Call Done Button
	 */
	public NewContactUSPage clickScheduleCallDoneButton() {
		if (getDriver() instanceof AppiumDriver) {
			clickElement(scheduledCallConfirmDoneButtonMobile);
		} else {
			clickElement(scheduledCallConfirmDoneButton);
		}
		return this;
	}

	/**
	 * verify if call back is already scheduled.
	 * 
	 * @return true/false
	 */
	public boolean verifyAlreadyScheduledCallBackAndChange() {
		boolean isDisplayed = false;
		boolean callBackText = callModalHeaderText.isDisplayed();
		if (!callBackText) {
			isDisplayed = true;
		}
		return isDisplayed;
	}

	/**
	 * click Make a Change button
	 */
	public NewContactUSPage clickMakeAChangeButton() {
		if (getDriver() instanceof AppiumDriver) {
			clickElement(makeAChangeScheduleBtn);
		} else {
			clickElement(makeAChangeBtn);
		}
		return this;
	}

	/**
	 * verify Message US button is enabled
	 * 
	 * @return true/false
	 */
	public NewContactUSPage verifyMessageUsBtn() {
		try {
			Assert.assertTrue(isElementDisplayed(messageUsButton));
			Reporter.log("Message US button is enabled.");
		} catch (Exception e) {
			Assert.fail("Message US button is not enabled");
		}
		return this;
	}

	/**
	 * verify Error Text For Message Us Button if disabled
	 * 
	 * @return true/false
	 */
	public NewContactUSPage verifyErrorTextForMessageUsBtnDisablity() {
		try {
			Assert.assertTrue(isElementDisplayed(errorMsg) && errorMsg.getText().contains(Constants.MESSAGE_US_ERROR));
			Reporter.log(
					"Verified that the Messaging US Button is enabled or Error Text is displayed when Message US button is disabled.");
		} catch (Exception e) {
			Assert.fail(
					"Unable to verify that the Messaging US Button is enabled or Error Text is displayed when Message US button is disabled.");
		}
		return this;
	}

	/**
	 * verify Error Text For Call Us Button if disabled
	 * 
	 * @return true/false
	 */
	public NewContactUSPage verifyErrorTextForCallUsBtnDisablity() {
		try {
			Assert.assertTrue(isElementDisplayed(errorMsg) && errorMsg.getText().contains(Constants.CALL_US_ERROR));
			Reporter.log(
					"Verified that the Call US Button is enabled or Error Text is displayed when Call US button is disabled.");
		} catch (Exception e) {
			Assert.fail(
					"Unable to to verify that the Call US Button is enabled or Error Text is displayed when Call US button is disabled.");
		}
		return this;
	}

	/**
	 * verify Call Us button
	 * 
	 * @return true/false
	 */
	public boolean verifyCallUsBtn() {
		return isElementDisplayed(callMeButton);
	}

	/**
	 * Verify message US button is disabled
	 * 
	 * @return true/false
	 */
	public boolean verifyMessageUsBtnDisabled() {
		return messageUsButtonDisabled.isDisplayed();
	}

	/**
	 * Verify Error Text For Message Us And Call Us Buttons Disablity
	 * 
	 * @return true/false
	 */
	public NewContactUSPage verifyErrorTextForMessageUsAndCallUsBtnsDisablity() {
		try {
			Assert.assertTrue(isElementDisplayed(errorMsg)
					&& errorMsg.getText().contains(Constants.MESSAGE_US_AND_CALL_US_ERROR));
			Reporter.log(
					"Verified that the Messaging US & Call US Buttons are enabled or Error Text is displayed when both buttons are disabled.");
		} catch (Exception e) {
			Assert.fail(
					"Unable to verify that the Messaging US & Call US Buttons are enabled or Error Text is displayed when both buttons are disabled.");
		}
		return this;
	}

	/**
	 * click on autopay settings link
	 */
	public NewContactUSPage clickAutopaySettingsLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement topLink : topLevelLinks) {
					if (topLink.equals("Billing & Payments")) {
						clickElement(topLink);
						WebElement autopayLink = getDriver().findElement(By.linkText("AutoPay settings"));
						clickElement(autopayLink);
						break;
					}
				}
			} else {
				clickElement(autopaySettingsLink);
			}
		} catch (Exception e) {
			Verify.fail("autopaySettingsLink not found");
		}
		return this;
	}

	/**
	 * Click on add Family Allowances
	 */
	public NewContactUSPage clickOnAddFamilyAllowances() {
		checkPageIsReady();
		if (getDriver() instanceof AppiumDriver) {
			moveToElement(plansUsageHeader);
			clickElementWithJavaScript(plansUsageHeader);
			clickElementWithJavaScript(addFamilyAllowances);
		} else {
			clickElement(addFamilyAllowances);
			Reporter.log("Clicked on Add Family Allowances link was successful");
		}
		return this;
	}

	/**
	 * Click on view App Purchases
	 */
	public NewContactUSPage clickOnViewAppPurchases() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(plansUsageHeader);
				clickElementWithJavaScript(plansUsageHeader);
				clickElementWithJavaScript(viewAppPurchases);
			} else {
				clickElement(viewAppPurchases);
			}
		} catch (Exception e) {
			Assert.fail("Click on view app purchase link failed");
		}
		return this;
	}

	/**
	 * Click on lost Or Stolen Device
	 */
	public NewContactUSPage clickOnlostOrStolenDevice() {
		checkPageIsReady();
		if (getDriver() instanceof AppiumDriver) {
			moveToElement(deviceCoverageHeader);
			clickElementWithJavaScript(deviceCoverageHeader);
			clickElementWithJavaScript(lostOrStolenDevice);
			Reporter.log("Clicked on Lost/Stolen device");
		}
		clickElement(lostOrStolenDevice);
		Reporter.log("Clicked on Lost/Stolen device");
		return this;
	}

	/**
	 * Click on trade In Value
	 */
	public NewContactUSPage clickOntradeInValue() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(deviceCoverageHeader);
				clickElementWithJavaScript(deviceCoverageHeader);
				clickElementWithJavaScript(tradeInValue);
			} else {
				clickElementWithJavaScript(tradeInValue);
			}
		} catch (Exception e) {
			Reporter.log("Click on trade in value failed");
		}
		return this;
	}

	/**
	 * Click on device Unlock Status
	 */
	public NewContactUSPage clickOnDeviceUnlockStatus() {
		checkPageIsReady();
		if (getDriver() instanceof AppiumDriver) {
			moveToElement(deviceCoverageHeader);
			clickElementWithJavaScript(deviceCoverageHeader);
			clickElementWithJavaScript(deviceUnlockStatus);
			Reporter.log(" Clicked on Device unlock status.");
		} else {
			clickElement(deviceUnlockStatus);
			Reporter.log(" Clicked on Device unlock status.");
		}
		return this;
	}

	/**
	 * Click on file A Damage Claim
	 */
	public NewContactUSPage clickOnFileADamageClaim() {
		checkPageIsReady();
		if (getDriver() instanceof AppiumDriver) {
			moveToElement(deviceCoverageHeader);
			clickElementWithJavaScript(deviceCoverageHeader);
			clickElementWithJavaScript(fileADamageClaim);
			Reporter.log("Clicked on file a damage claim");
		} else {
			clickElement(fileADamageClaim);
			Reporter.log("Clicked on file a damage claim");
		}
		return this;
	}

	/**
	 * Access MyPhP Info Page
	 */

	public NewContactUSPage verifyMyphpinfoPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyCurrentUrl("myphpinfo"));
			Reporter.log("My php info page is loading.");
		} catch (Exception e) {
			Assert.fail("My php info page is not loading.");
		}
		return this;
	}

	/**
	 * Click on upgrade Device
	 */
	public NewContactUSPage clickOnUpgradeDevice() {
		checkPageIsReady();

		if (getDriver() instanceof AppiumDriver) {
			moveToElement(deviceCoverageHeader);
			clickElementWithJavaScript(deviceCoverageHeader);
			clickElementWithJavaScript(upgradeDevice);
			Reporter.log("Clicked on Upgrade device link");
		} else {
			clickElementWithJavaScript(upgradeDevice);
			Reporter.log("Clicked on Upgrade device link");
		}
		return this;
	}

	/**
	 * Click on Access Voicemail
	 */
	public NewContactUSPage clickOnAccessVoicemail() {
		checkPageIsReady();
		if (getDriver() instanceof AppiumDriver) {
			moveToElement(deviceCoverageHeader);
			clickElementWithJavaScript(deviceCoverageHeader);
			clickElementWithJavaScript(accessVoicemail);
			Reporter.log("Clicked on Access Voicemail link");
		} else {
			clickElementWithJavaScript(accessVoicemail);
			Reporter.log("Clicked on Access Voicemail link");
		}
		return this;
	}

	/**
	 * Access Voicemail Page
	 */

	public NewContactUSPage verifyVoicemailPage() {
		checkPageIsReady();
		try {
			switchToSecondWindow();
			Assert.assertTrue(verifyCurrentUrl("wrcpro"));
			Reporter.log("Voice mail page is accessible");
		} catch (Exception e) {
			Assert.fail("Voice mail page is not accessible");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Contact US Page class instance.
	 */

	public NewContactUSPage verifyPageLoaded() {
		checkPageIsReady();
		getDriver().getPageSource().contains(contactUsPageLoadText);
		return this;
	}

	/**
	 * Verify Contact us page.
	 *
	 * @return the ContactUSPage class instance.
	 */
	public NewContactUSPage verifyContactUSPage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.ajax_loader")));
			verifyPageUrl(contactUsPageUrl);
			Reporter.log("Contact us page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Contact us page not displayed");
		}
		return this;
	}

	/**
	 * Verify team picture .
	 */
	public NewContactUSPage verifyTeamPicture() {
		try {
			teamPicture.isDisplayed();
			Reporter.log("Team picture displayed");
		} catch (Exception e) {
			Assert.fail("Team picture not displayed");
		}
		return this;
	}

	/**
	 * Verify team of experts description divison.
	 */
	public NewContactUSPage verifyTeamofExpertsDescription() {
		try {
			teamOfExpertsDiv.isDisplayed();
			Reporter.log("Team of experts description divison is displayed");
		} catch (Exception e) {
			Assert.fail("Team of experts description divison is displayed");
		}
		return this;
	}

	/**
	 * Verify call team cta.
	 */
	public NewContactUSPage verifyCallTeamCTA() {
		try {
			callTeamCTA.isDisplayed();
			Reporter.log("Call team cta displayed");
		} catch (Exception e) {
			Assert.fail("Call team cta not displayed");
		}
		return this;
	}

	/**
	 * Verify message cta.
	 */
	public NewContactUSPage verifyMessageCTA() {
		try {
			messageCTA.isDisplayed();
			Reporter.log("Message cta displayed");
		} catch (Exception e) {
			Assert.fail("Message cta not displayed");
		}
		return this;
	}

	/**
	 * Verify hours of availability div.
	 */
	public NewContactUSPage verifyHoursOfAvailabilityDiv() {
		try {
			Assert.assertFalse(hoursAvailabiltyDiv.size() == 0, "Hours of availability div not displayed");
			Reporter.log("Hours of availability div is displayed");
		} catch (Exception e) {
			Assert.fail("Hours of availability div not displayed");
		}
		return this;
	}

	/**
	 * Verify set up call popup window.
	 */
	public NewContactUSPage verifySetUpCallPopUp() {
		try {
			setUpCallPopUp.isDisplayed();
			Reporter.log("Set up call popup window displayed");
		} catch (Exception e) {
			Assert.fail("Set up call popup window not displayed");
		}
		return this;
	}

	/**
	 * Verify set up call popup window.
	 */
	public NewContactUSPage verifySetUpCallPopUpForNewAngular() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				waitforSpinnerinProfilePage();
				setUpCallPopUpHeaderTextForIOS.isDisplayed();
				Reporter.log("Set up call popup window displayed");
			} else {
				waitFor(ExpectedConditions.visibilityOf(setUpCallPopUpHeaderText));
				setUpCallPopUpHeaderText.isDisplayed();
				Reporter.log("Set up call popup window displayed");
			}
		} catch (Exception e) {
			Assert.fail("Set up call popup window not displayed");
		}
		return this;
	}

	/**
	 * Click Block Charge toggle.
	 */
	public NewContactUSPage clickCallTeamCTA() {
		try {
			callTeamCTA.click();
		} catch (Exception e) {
			Assert.fail("Click on call team cta failed");
		}
		return this;
	}

	/**
	 * click on message team button
	 */
	public NewContactUSPage clickMessageTeamBtn() {
		try {
			clickElementWithJavaScript(messageTeamBtn);
			Reporter.log("Clicked on message team cta");
		} catch (Exception e) {
			Reporter.log("Click on message team button failed ");
		}
		return this;
	}

	/**
	 * verify if messaging chat Pop up is displayed or not
	 * 
	 * @return true/false
	 */
	public NewContactUSPage verifyAngularChatPopUp() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("img[alt='Loading']")));
			chatPopUp.isDisplayed();
			Reporter.log("Message us chat pop up is displayed");
		} catch (Exception e) {
			Assert.fail("Message us chat pop up not displayed");
		}
		return this;
	}

	/**
	 * Verify team picture .
	 */
	public NewContactUSPage verifyTeamPictureOnNewAngularPage() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				teamPictureOnNewAngularPageForIOS.isDisplayed();
				Reporter.log("Team picture displayed");
			} else {
				teamPictureOnNewAngularPage.isDisplayed();
				Reporter.log("Team picture displayed");
			}
		} catch (Exception e) {
			Assert.fail("Team picture not displayed");
		}
		return this;
	}

	/**
	 * Verify team of experts description divison.
	 */
	public NewContactUSPage verifyTeamofExpertsDescriptionOnNewAngularPage() {
		try {
			teamOfExpertsDivOnNewAngularPage.isDisplayed();
			Reporter.log("Team of experts description divison is displayed");
		} catch (Exception e) {
			Assert.fail("Team of experts description divison is displayed");
		}
		return this;
	}

	/**
	 * click on call team button
	 */
	public NewContactUSPage clickCallTeamBtn() {
		try {
			waitFor(ExpectedConditions.visibilityOf(callTeamBtn));
			callTeamBtn.click();
			Reporter.log("Clicked on call team button");
		} catch (Exception e) {
			Reporter.log("Click on call team button failed " + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify call team cta.
	 */
	public NewContactUSPage verifyCallTeamBtn() {
		try {
			waitFor(ExpectedConditions.visibilityOf(callTeamBtn));
			callTeamBtn.isDisplayed();
			Reporter.log("Call team button displayed");
		} catch (Exception e) {
			Assert.fail("Call team button not displayed");
		}
		return this;
	}

	/**
	 * Verify make a change button.
	 */
	public NewContactUSPage verifyMakeAChangeBtn() {

		try {
			waitFor(ExpectedConditions.visibilityOf(makeAChangeBtn));
			Assert.assertTrue(isElementDisplayed(makeAChangeBtn));
			Reporter.log("Make a change button displayed");
		} catch (AssertionError e) {
			Assert.fail("Make a change button not displayed");
		}
		return this;
	}

	/**
	 * click on cancel button cta
	 */
	public NewContactUSPage clickCancelBtn() {
		try {
			callTeamBtn.click();
			Reporter.log("Clicked on call team button");
		} catch (Exception e) {
			Reporter.log("Click on call team button failed " + e.getMessage());
		}
		return this;
	}

	/**
	 * click on schedule a call button
	 */
	public NewContactUSPage clickScheduleACallBtn() {
		try {
			waitFor(ExpectedConditions.visibilityOf(scheduleACallBtn));
			scheduleACallBtn.click();
			Reporter.log("Clicked on schedule a call button");
		} catch (Exception e) {
			Reporter.log("Click on schedule a call button failed " + e.getMessage());
		}
		return this;
	}

	/**
	 * click on confirm button
	 */
	public NewContactUSPage clickConfirmBtn() {
		try {
			moveToElement(confirmBtn);
			confirmBtn.click();
			Reporter.log("Clicked on confirm button");
		} catch (Exception e) {
			Reporter.log("Click on confirm button failed " + e.getMessage());
		}
		return this;
	}

	/**
	 * click on schedule a call button
	 */
	public NewContactUSPage selectScheduledTime() {
		try {
			checkPageIsReady();
			waitforSpinnerinProfilePage();
			for (WebElement webElement : timeSlots) {
				clickElementWithJavaScript(webElement);
				Reporter.log("CLicked on time slot");
				break;
			}

		} catch (Exception e) {
			Reporter.log("Click on time slot button failed " + e.getMessage());
		}
		return this;
	}

	/**
	 * Verify confirmation window.
	 */
	public NewContactUSPage verfiyConfirmationWindow() {
		try {
			waitFor(ExpectedConditions.visibilityOf(doneBtn));
			Assert.assertTrue(isElementDisplayed(doneBtn));
			Reporter.log("Confirmation window displayed");
		} catch (AssertionError e) {
			Assert.fail("Confirmation window not displayed");
		}
		return this;
	}

	/**
	 * Click confirmation window.
	 */
	public NewContactUSPage clickConfirmationWindow() {
		try {
			waitFor(ExpectedConditions.visibilityOf(doneBtn));
			doneBtn.click();
			Reporter.log("Clicked on confirmation window");
		} catch (Exception e) {
			Assert.fail("Click on confirmation window failed");
		}
		return this;
	}

	/**
	 * Click cancel call back button.
	 */
	public NewContactUSPage clickCancelCallBack() {
		try {
			waitFor(ExpectedConditions.visibilityOf(cancelCallBack));
			cancelCallBack.click();
			Reporter.log("Clicked on cancel call back button");
		} catch (Exception e) {
			Assert.fail("Click on  cancel call back button failed ");
		}
		return this;
	}

	/**
	 * Verify cancel call back button.
	 */
	public NewContactUSPage verfiyCancelCallBack() {

		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.circle")));
			cancelCallBack.isDisplayed();
			Reporter.log("Cancel call back cta displayed");
		} catch (Exception e) {
			Assert.fail("Cancel call back cta not displayed");
		}
		return this;
	}

	/**
	 * Click on browser back button.
	 */

	public NewContactUSPage clickOnBrowserBackButton() {
		try {
			getDriver().navigate().back();
			Reporter.log("Clicked on Contact Us Browser back button");

		} catch (Exception e) {
			Assert.fail("Failed to click on Browser back button");
		}
		return this;
	}

	/**
	 * Verify call us icon
	 */
	public NewContactUSPage verifyCallUsButtonNotDispalyed() {
		try {
			Assert.assertFalse(isElementDisplayed(phoneCallIcon));
			Reporter.log("Call us button not displayed");
		} catch (Exception e) {
			Assert.fail("Call us button displayed");
		}
		return this;
	}

	/**
	 * Verify message us icon
	 */
	public NewContactUSPage verifyMessageUSButtonNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(messageUsIcon));
			Reporter.log("Message us button not displayed");
		} catch (Exception e) {
			Assert.fail("Message us button displayed");
		}
		return this;
	}

	/**
	 * Verify team picture
	 */
	public NewContactUSPage verifyTeamPictureNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(teamPicture));
			Reporter.log("Team picture not displayed");
		} catch (Exception e) {
			Assert.fail("Team picture displayed");
		}
		return this;
	}

	/**
	 * Verify team content
	 */
	public NewContactUSPage verifyTeamContentNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(teamOfExpertsDiv));
			Reporter.log("Team content not displayed");
		} catch (Exception e) {
			Assert.fail("Team content displayed");
		}
		return this;
	}

	/**
	 * Click error modal popup
	 */
	public NewContactUSPage clickErrorModalPopUp() {
		try {
			errorModalPopUp.click();
		} catch (Exception e) {
			Assert.fail("Click on error modal popup failed");
		}
		return this;
	}
}
