package com.tmobile.eservices.qa.pages.payments;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author pshiva
 *
 */
public class NewAddCardPage extends CommonPage {

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	private By waitSpinner = By.cssSelector("div.circle");

	@FindBy(id = "cardName")
	private WebElement cardName;

	@FindBy(id = "creditCardNumber")
	private WebElement cardNumber;

	@FindBy(id = "expirationDate")
	private WebElement expirationDate;

	@FindBy(id = "cvvNumber")
	private WebElement cvvNumber;

	@FindBy(id = "zipCode")
	private WebElement zipCode;

	@FindBy(xpath = "//p[text()='Card information']")
	private WebElement headerCardInformation;

	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	private WebElement btnContinue;

	@FindBy(xpath = "//button[contains(text(),'Back')]")
	private WebElement btnBack;

	@FindBy(xpath = "//p[contains(text(),'Enter a valid card number')]")
	private List<WebElement> Entervalidcard;
	
	@FindBy(xpath="//input[@formcontrolname='saveCardToggle']")
	private WebElement Savecheckbox;
	
	
	@FindBy(xpath="//div[@class='d-flex']/i")
	private WebElement exclametionalreadyhavecard;
	
	@FindBy(xpath="//div[@class='d-flex']/span")
	private WebElement txtalradyhavecard;
	
	@FindBy(xpath="//input[contains(@class,'saveCheckbox ng')]")
	private WebElement chkboxsavepaymentmethod;	
	

	@FindBy(xpath = "//div[contains(@class,'dialog animation')]//button[text()='Continue']")
	private WebElement dialogcontinue;	
	
	@FindBy(xpath = "//div[contains(@class,'dialog animation')]//button[text()='Continue']")
	private List<WebElement> btnsdialogcontinue;	
	
	
	@FindBy(xpath="//label[@for='cardName']")
	private WebElement placeholderNameoncard;	
	
	@FindBy(xpath="//label[@for='creditCardNumber']")
	private WebElement placeholderCardNumber;	
		
	@FindBy(xpath="//label[@for='expirationDate']")
	private WebElement placeholderExpirationdate;
	
	@FindBy(xpath="//label[@for='cvvNumber']")
	private WebElement placeholderCVVnumber;
	
	@FindBy(xpath="//label[@for='zipCode']")
	private WebElement placeholderZipCode;
	
	@FindBy(id="cvvHelpLink")
	private WebElement cvvHelpLink;
	
	@FindBy(xpath = "//span[contains(text(),'Save this payment method')]")
	private List<WebElement> txtsavepayment;
	
	@FindBy(xpath = "//i[contains(@class,'tooltip-icon')]")
	private List<WebElement> tooltipSavePayment;
	
	
	@FindBy(xpath = "//span[contains(@class,'padding-horizontal-xsmall')]")
	private List<WebElement> alreadysavedMetod;
	
	@FindBy(css="img.ccv_amexImg")
	private List<WebElement> popupCVV;
	
	@FindBy(xpath = "//div[@class='discover-icon']")
	private WebElement discoverSprite;
	
	@FindBy(xpath = "//div[@class='visaImg']")
	private WebElement visaSprite;
	
	@FindBy(xpath = "//div[@class='masterImg']")
	private WebElement masterSprite;
	
	@FindBy(xpath = "//div[@class='expressImg expressImgs']")
	private WebElement amexSprite;
	
	@FindBy(xpath="//p[contains(@class,'errormessage')]")
	private List<WebElement> errormessages;
	
	private final String pageUrl = "payments/addcard";

	private WebElement saveChangesBtn;

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public NewAddCardPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public NewAddCardPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * verify card information header
	 *
	 * @return boolean
	 */
	public NewAddCardPage verifyCardPageLoaded() {
		try {

			checkPageIsReady();
			// waitFor(ExpectedConditions.visibilityOf(pageSpinner));
			waitFor(ExpectedConditions.elementToBeClickable(btnBack));
			verifyPageUrl();

			Reporter.log("Verified the Card page is loaded");
		} catch (Exception e) {
			Assert.fail("Card page is not loaded");
		}
		return this;
	}

	public NewAddCardPage Entervalidcardnumber() {
		try {
			if (Entervalidcard.size() == 1)
				Reporter.log("Enter valid card number error displayed");
			else
				Assert.fail("Enter valid card number error is not displayed");
		} catch (Exception e) {
			Assert.fail("Enter valid card number element is not located");
		}
		return this;
	}

	public NewAddCardPage clickBackBtn() {
		try {
			btnBack.click();
		} catch (Exception e) {
			Assert.fail("Failed to click back button");
		}
		return this;
	}

	/**
	 * click continue button for add Card
	 */
	public NewAddCardPage clickContinueCTA() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.spinner")));
			waitFor(ExpectedConditions.invisibilityOfElementLocated(waitSpinner));
			waitFor(ExpectedConditions.elementToBeClickable(btnContinue));
			clickElementWithJavaScript(btnContinue);
			Reporter.log("Clicked on Continue button");

		} catch (Exception e) {
			Assert.fail("Failed to verify Card info Header");
		}
		return this;
	}

	public NewAddCardPage enterNameOnCard(String name) {
		try {
			cardName.clear();
			cardName.sendKeys(name);
			Reporter.log("Card holder name entered");

		} catch (Exception e) {
			Assert.fail("card name is not located");
		}
		return this;
	}

	public NewAddCardPage enterCardNumber(String number) {
		try {
			cardNumber.clear();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(waitSpinner));
			cardNumber.sendKeys(number);
			Reporter.log("cardNumber entered");

		} catch (Exception e) {
			Assert.fail("cardNumber is not located");
		}
		return this;
	}

	public NewAddCardPage ClickonExpirationDate() {
		try {
		
			expirationDate.click();
			;
			Reporter.log("expirationDate clicked");

		} catch (Exception e) {
			Assert.fail("expirationDate is not located");
		}
		return this;
	}

	public NewAddCardPage enterExpirationDate(String expirydate) {
		try {
			expirationDate.clear();
			expirationDate.sendKeys(expirydate);
			Reporter.log("expirationDate entered");

		} catch (Exception e) {
			Assert.fail("expirationDate is not located");
		}
		return this;
	}

	public NewAddCardPage enterCVV(String cvv) {
		try {
			cvvNumber.clear();
			
			cvvNumber.sendKeys(cvv);
			Reporter.log("cvvNumber entered");

		} catch (Exception e) {
			Assert.fail("cvvNumber is not located");
		}
		return this;
	}

	public NewAddCardPage enterZipCode(String zip) {
		try {
			zipCode.clear();
			zipCode.sendKeys(zip);
			Reporter.log("zipCode entered");

		} catch (Exception e) {
			Assert.fail("zipCode is not located");
		}
		return this;
	}

	public NewAddCardPage addCardInformation(Map<String, String> cardinfo) {
		enterNameOnCard(cardinfo.get("nameoncard"));
		enterCardNumber(cardinfo.get("cardnumber"));
		// ClickonExpirationDate();
		waitFor(ExpectedConditions.invisibilityOfElementLocated(waitSpinner));
		enterExpirationDate(cardinfo.get("expiry"));
		enterCVV(cardinfo.get("cvv"));
		enterZipCode(cardinfo.get("zip"));
		clickContinueCTA();
		return this;
	}
	
	public NewAddCardPage addCardInformationforOTP(Map<String, String> cardinfo) {
		enterNameOnCard(cardinfo.get("nameoncard"));
		enterCardNumber(cardinfo.get("cardnumber"));
		// ClickonExpirationDate();
		waitFor(ExpectedConditions.invisibilityOfElementLocated(waitSpinner));
		enterExpirationDate(cardinfo.get("expiry"));
		enterCVV(cardinfo.get("cvv"));
		enterZipCode(cardinfo.get("zip"));
		//clickContinueCTA();
		return this;
	}
	
	

	public NewAddCardPage Checkexclamationalreadyhavecard() {
		try {
			if(exclametionalreadyhavecard.isDisplayed())Reporter.log("Exclamation mark for already have card displayed");
			else Verify.fail("Exclamation mark for already have card is not displayed");

		} catch (Exception e) {
			Assert.fail("exclametionalreadyhavecard element is not located");
		}
		return this;
	}
	

	public NewAddCardPage Checktxtalreadyhavecard() {
		try {
			if(txtalradyhavecard.isDisplayed())Reporter.log("Text for already have card displayed");
			else Verify.fail("Text for already have card is not displayed");

		} catch (Exception e) {
			Assert.fail("exclametionalreadyhavecard element is not located");
		}
		return this;
	}
	

	public boolean ispaymentmethodChecked() {
		
		if(chkboxsavepaymentmethod.getAttribute("checked")!=null) return true;
		else return false;
	
	}
	
	public NewAddCardPage savepaymentmethodOFF() {
		try {
		if(ispaymentmethodChecked())chkboxsavepaymentmethod.click();
		Reporter.log("Save payment method un selected");
		
		}
		catch(Exception e) {
			Assert.fail("Check box is not located");
		}
		return this;
	
}
	
	public NewAddCardPage savepaymentmethodON() {
		try {
		if(!ispaymentmethodChecked())chkboxsavepaymentmethod.click();
		Reporter.log("Save payment method  selected");
		
		}
		catch(Exception e) {
			Assert.fail("Check box is not located");
		}
		return this;
	}
	

	public NewAddCardPage ClickContinueinDialog() {
		try {
			dialogcontinue.click();
		Reporter.log("Continue button is clicked");
		
		}
		catch(Exception e) {
			Assert.fail("Continue button is not located");
		}
		return this;
	
}
	
	
	
	public NewAddCardPage addcardwithsaveoption(Map<String, String> cardinfo) {
		addCardInformationforOTP(cardinfo);
		savepaymentmethodON();
		clickContinueCTA();
		ClickContinueinDialog();
		return this;
		
	}
	
	
	public NewAddCardPage CheckNameonCardPalceholder() {
		try {
			if(placeholderNameoncard.getText().trim().equalsIgnoreCase("Name on card")) Reporter.log("name on Card place holder is displayed");
			else Verify.fail("name on Card place holder is not displayed");
			
			}
			catch(Exception e) {
				Assert.fail("placeholderNameoncard is not located");
			}
			return this;
	}
	
	
	public NewAddCardPage CheckCardNumberPalceholder() {
		try {
			if(placeholderCardNumber.getText().trim().equalsIgnoreCase("Credit card number")) Reporter.log("Card number place holder is displayed");
			else Verify.fail("Card number place holder is not displayed");
			
			}
			catch(Exception e) {
				Assert.fail("placeholderCardNumber is not located");
			}
			return this;
	}
	public NewAddCardPage CheckExpirationDatePalceholder() {
		try {
			if(placeholderExpirationdate.getText().trim().equalsIgnoreCase("Expiration date")) Reporter.log("Expiration date place holder is displayed");
			else Verify.fail("Expiration date place holder is not displayed");
			
			}
			catch(Exception e) {
				Assert.fail("placeholderExpirationdate is not located");
			}
			return this;
	}
	public NewAddCardPage CheckCVVNumberPalceholder() {
		try {
			if(placeholderCVVnumber.getText().trim().equalsIgnoreCase("CVV")) Reporter.log("CVV number place holder is displayed");
			else Verify.fail("CVV number place holder is not displayed");
			
			}
			catch(Exception e) {
				Assert.fail("placeholderCVVnumber is not located");
			}
			return this;
	}
	public NewAddCardPage CheckZipPalceholder() {
		try {
			if(placeholderZipCode.getText().trim().equalsIgnoreCase("Zip")) Reporter.log("Zip place holder is displayed");
			else Verify.fail("Zip place holder is not displayed");
			
			}
			catch(Exception e) {
				Assert.fail("placeholderZipCode is not located");
			}
			return this;
	}
	
	
	public NewAddCardPage CheckCVVHelpLink() {
		try {
			cvvHelpLink.click(); 
			Reporter.log("Cvv Help link is Clicked");
			
			
			}
			catch(Exception e) {
				Assert.fail("placeholderAccountNumber is not located");
			}
			return this;
	}
	
	
	
	
	public NewAddCardPage CheckSavePaymentmethodtext(boolean val) {
		try {
			
			boolean res=txtsavepayment.size()>0?true:false;
			if(val==res) Reporter.log("Save check box text is "+res);
			else Verify.fail("Save check box text expected is:"+val+" actual is:"+res);
			
			}
			catch(Exception e) {
				Assert.fail("txtsavethispaymentmethod is not located");
			}
			return this;
	}
	
	
	public NewAddCardPage CheckAlreadyhavepaymentmethodtext(boolean val) {
		try {
			
			boolean res=alreadysavedMetod.size()>0?true:false;
			if(val==res) Reporter.log("already have paymentmethod text is "+res);
			else Verify.fail("already have paymentmethod text expected is:"+val+" actual is:"+res);
			
			}
			catch(Exception e) {
				Assert.fail("txtalreadyhavepaymentmethod is not located");
			}
			return this;
	}
	
	public NewAddCardPage Checktooltipforsaveoptional(boolean val) {
		try {
			
			boolean res=tooltipSavePayment.size()>0?true:false;
			if(val==res) Reporter.log("tooltip for save account checkbox "+res);
			else Verify.fail("tooltip for save account checkbox expected is:"+val+" actual is:"+res);
			
			}
			catch(Exception e) {
				Assert.fail("txtalreadyhavepaymentmethod is not located");
			}
			return this;
	}
	
	
	
	public NewAddCardPage CheckCvvHelppopupDsiplayed(boolean val) {
		
try {
	        List<WebElement> popupcvv1=getDriver().findElements(By.cssSelector("img.amex_Img"));		
			boolean res=popupcvv1.size()>0?true:false;
			if(val==res) Reporter.log("popup for CVV number "+res);
			else Verify.fail("popup for CVV number expected is:"+val+" actual is:"+res);
			
			}
			catch(Exception e) {
				Assert.fail("popupCVV is not located");
			}
			return this;
}

	public NewAddCardPage CheckdiscoverSpriteDisplayed() {
		
		try {
			        if(discoverSprite.isDisplayed())Reporter.log("Discover Sprite diplayed");
			        else Verify.fail("Discover Sprite is not diplayed");
					
					}
					catch(Exception e) {
						Assert.fail("discoverSprite is not located");
					}
					return this;
		}	
	
	
public NewAddCardPage CheckmasterSpriteDisplayed() {
		
		try {
			        if(masterSprite.isDisplayed())Reporter.log("Master Sprite diplayed");
			        else Verify.fail("Master Sprite is not diplayed");
					
					}
					catch(Exception e) {
						Assert.fail("masterSprite is not located");
					}
					return this;
		}	
	
public NewAddCardPage CheckvisaSpriteDisplayed() {
	
	try {
		        if(visaSprite.isDisplayed())Reporter.log("Visa Sprite diplayed");
		        else Verify.fail("Visa Sprite is not diplayed");
				
				}
				catch(Exception e) {
					Assert.fail("visaSprite is not located");
				}
				return this;
	}	
public NewAddCardPage CheckamexSpriteDisplayed() {
	
	try {
		        if(amexSprite.isDisplayed())Reporter.log("Amex Sprite diplayed");
		        else Verify.fail("Amex Sprite is not diplayed");
				
				}
				catch(Exception e) {
					Assert.fail("amexSprite is not located");
				}
				return this;
	}	




public boolean verifyErrorTextMessage(String message) {
	checkPageIsReady();
	boolean display = false;
	try {
		for (WebElement error : errormessages) {
			if (error.isDisplayed() && error.getText().contains(message)) {
				display = true;
			}
		}
	} catch (Exception e) {

	}
	return display;
}



public NewAddCardPage checkmaximumcharbyfield(WebElement ele, int stringlength) {
	try {
		if (ele.getText().length() > stringlength)
			Verify.fail("Name on account is accepting more than " + stringlength + " characters");
	} catch (Exception e) {
		Assert.fail("Account Name field not found");
	}
	return this;
}


public void entercarderrorcheck(String CardType,String cardnumber) {

	String cvv1="23",cvv2="234";
	refreshpage();
	enterCardNumber(cardnumber);
	//ClickonExpirationDate();
	waitFor(ExpectedConditions.invisibilityOfElementLocated(waitSpinner));
	
	
	
	switch(CardType)
	{
	  
	   case "visa" :
		   CheckvisaSpriteDisplayed();
		   Verify.assertFalse(verifyErrorTextMessage("Enter a valid card number"),"Visa card is not accepted");
		   cvv1="23";
		   cvv2="234";
		   break; 
	   
	   case "master" :
		   CheckmasterSpriteDisplayed();
			Verify.assertFalse(verifyErrorTextMessage("Enter a valid card number"),"Master card is not accepted");
			 cvv1="23";
			 cvv2="234";
			break; 
	 
	   case "amex" :
		   CheckamexSpriteDisplayed();
		   Verify.assertFalse(verifyErrorTextMessage("Enter a valid card number"),"Amex card is not accepted");
		   cvv1="234";
		   cvv2="2345";
		    break; 
	   case "discover" :
		   CheckdiscoverSpriteDisplayed();
			Verify.assertFalse(verifyErrorTextMessage("Enter a valid card number"),"Discover card is not accepted");
			 cvv1="23";
			 cvv2="234";
		     break; 
	  
	}
	
	enterCVV(cvv1);
	Verify.assertTrue(verifyErrorTextMessage("Enter a valid CVV"),"cvv check");
	enterCVV(cvv2);
	Verify.assertFalse(verifyErrorTextMessage("Enter a valid CVV"),"cvv check");
}

public void refreshpage() {
	getDriver().navigate().refresh();
	OneTimePaymentPage otp=new OneTimePaymentPage(getDriver());
	otp.verifyPageLoaded();
	otp.clickPaymentMethodBlade();
	PaymentCollectionPage pcp=new PaymentCollectionPage(getDriver());
	pcp.verifyPageLoaded();
	pcp.clickAddCard();
	verifyCardPageLoaded();
}

/**
 * validate all bank page elements
 */
public NewAddCardPage validateAddBankAllFields() {
	try {
		// continueAddBank.click();
		/*
		 * Verify.assertTrue(verifyErrorTextMessage("Enter a valid name."),
		 * "Blank Account name can be accepted");
		 * Verify.assertTrue(verifyErrorTextMessage("Enter a valid routing number."),
		 * "Blank Routing Number can be accepted");
		 * Verify.assertTrue(verifyErrorTextMessage("Enter a valid account number."),
		 * "Blank Account Number can be accepted");
		 */
		enterNameOnCard("T");
		Thread.sleep(1000);
	
		Verify.assertTrue(verifyErrorTextMessage("Enter a valid name"),
				"Account name with one character can be accepted");
		

		enterNameOnCard("characters limit for account name fieldis");
		Thread.sleep(1000);
		checkmaximumcharbyfield(cardName, 40);

	
		enterCardNumber("23");
		
		
		Thread.sleep(1000);
		
		Verify.assertTrue(verifyErrorTextMessage("Enter a valid card number"),
				"card number with 2 characters can not be accepted");
		
		
		refreshpage();
		enterCardNumber("4111111111111111");
		//ClickonExpirationDate();
		waitFor(ExpectedConditions.invisibilityOfElementLocated(waitSpinner));
		LocalDate dt=LocalDate.now();
		LocalDate pastdate=dt.minusMonths(1);
		LocalDate futuredate=dt.plusMonths(1);
		
		
		
		
		DateTimeFormatter customFormatter = DateTimeFormatter.ofPattern("MMyy");
		String currmonth = customFormatter.format(dt);
		String pastmonth = customFormatter.format(pastdate);
		String futuremonth = customFormatter.format(futuredate);
		
		enterExpirationDate(pastmonth);
		Verify.assertTrue(verifyErrorTextMessage("Enter a valid expiration date"),
				"Expiration date verification check");
		
		
		enterExpirationDate(futuremonth);
		Verify.assertFalse(verifyErrorTextMessage("Enter a valid expiration date"),
				"Expiration date verification check");
		
		
		entercarderrorcheck("visa","4111111111111111");
		entercarderrorcheck("master","5555555555554444");
		entercarderrorcheck("discover","6011000990139424");
		entercarderrorcheck("amex","371449635398431");

		
		enterZipCode("9802");
		Thread.sleep(1000);
		
		Verify.assertTrue(verifyErrorTextMessage("Enter a valid zip code"),
				"Zip code with less then 5 characters can not be accepted");
		enterZipCode("98027");
		Thread.sleep(1000);
		Verify.assertFalse(verifyErrorTextMessage("Enter a valid zip code"),
				"5 numbers zip code ");
	} catch (Exception e) {
		Verify.fail("Failed to verify all the bank validations");
	}
	return this;
}

public NewAddCardPage clickcontinueindialog() {
	if(btnsdialogcontinue.size()>0) btnsdialogcontinue.get(0).click();
	return this;
}



}