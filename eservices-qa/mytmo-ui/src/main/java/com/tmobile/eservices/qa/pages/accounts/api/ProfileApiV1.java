package com.tmobile.eservices.qa.pages.accounts.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class ProfileApiV1 extends ApiCommonLib{
	
	/***
	 * Profile API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response getProfile(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildProfileAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v1/profile/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
    }     
    
    /***
	 * Profile API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response updateProfile(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildProfileAPIHeader(apiTestData, tokenMap);
    	String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, "updateProfile");
    	String resourceURL = "v1/profile/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, updatedRequest ,RestServiceCallType.PUT);
		return response;
    }   
    

	/***
	 * Profile API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response getProfilePreferences(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildProfileAPIHeader(apiTestData, tokenMap);
    	String resourceURL = "v1/profile/preferences/" + apiTestData.getMsisdn();
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
    }     
    
    /***
	 * Profile API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response updateProfilePreferences(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildProfileAPIHeader(apiTestData, tokenMap);
    	String updatedRequest = prepareRequestParam(requestBody, tokenMap);
    	logRequest(updatedRequest, "updateProfilePaperlessBillingDetails");
    	String resourceURL = "v1/profile/preferences/" + apiTestData.getMsisdn();
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, updatedRequest ,RestServiceCallType.PUT);
		return response;
    } 
    

	/***
	 * Profile API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response getProfileFeatures(ApiTestData apiTestData,String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildProfileAPIHeader(apiTestData, tokenMap);
    	String updatedRequest = prepareRequestParam(requestBody, tokenMap);
    	logRequest(updatedRequest,"getProfileFeatures");
    	String resourceURL = "v1/profile/profilefeatures?category="+tokenMap.get("category");
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, updatedRequest ,RestServiceCallType.POST);
		return response;
    }     
    
    /***
	 * Profile API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response updateProfileFeatures(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildProfileAPIHeader(apiTestData, tokenMap);
    	String updatedRequest = prepareRequestParam(requestBody, tokenMap);
		logRequest(updatedRequest, "updateProfileFeatures");
    	String resourceURL = "v1/profile/profilefeatures";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, updatedRequest ,RestServiceCallType.PUT);
		return response;
    }    
    

	/***
	 * Profile API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response getProfilePaperlessBillingDetails(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildProfileAPIHeader(apiTestData, tokenMap);
    	String resourceURL = "v1/profile/billing/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
    }     
    
    /***
	 * Profile API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response updateProfilePaperlessBillingDetails(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildProfileAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v1/profile/billing/" + apiTestData.getMsisdn();
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
		return response;
    }   
    
    

  	/***
  	 * Profile API.
  	 * parameters:
  	 *	-  $ref: '#/parameters/Accept
  	 *	-  $ref: '#/parameters/Content-Type
  	 *	-  $ref: '#/parameters/X-B3-TraceId
  	 *	-  $ref: '#/parameters/X-B3-SpanId
  	 *	-  $ref: '#/parameters/channel_id
  	 *	-  $ref: '#/parameters/Authorization
  	 *	-  $ref: '#/parameters/application_id
  	 *	-  $ref: '#/parameters/user-token
  	 *	-  $ref: '#/parameters/tmo-id
  	 *	-  $ref: '#/parameters/msisdn
  	 *	-  $ref: '#/parameters/ban
  	 *	-  $ref: '#/parameters/access_token
  	 *	-  $ref: '#/parameters/application_client
  	 *	-  $ref: '#/parameters/application_version_code
  	 *	-  $ref: '#/parameters/device_os
  	 *	-  $ref: '#/parameters/os_version
  	 *	-  $ref: '#/parameters/os_language
  	 *	-  $ref: '#/parameters/device_manufacturer
  	 *	-  $ref: '#/parameters/device_model
  	 *	-  $ref: '#/parameters/marketing_cloud_id
  	 *	-  $ref: '#/parameters/sd_id
  	 *	-  $ref: '#/parameters/adobe_uuid

  	 * @return
  	 * @throws Exception 
  	 */
      public Response getProfileDigits(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
      	Map<String, String> headers = buildProfileAPIHeader(apiTestData, tokenMap);
      	String resourceURL = "v1/profile/digits/count/"+apiTestData.getMsisdn();
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
        return response;
      } 
      
      /***
  	 * Profile API.
  	 * parameters:
  	 *	-  $ref: '#/parameters/Accept
  	 *	-  $ref: '#/parameters/Content-Type
  	 *	-  $ref: '#/parameters/X-B3-TraceId
  	 *	-  $ref: '#/parameters/X-B3-SpanId
  	 *	-  $ref: '#/parameters/channel_id
  	 *	-  $ref: '#/parameters/Authorization
  	 *	-  $ref: '#/parameters/application_id
  	 *	-  $ref: '#/parameters/user-token
  	 *	-  $ref: '#/parameters/tmo-id
  	 *	-  $ref: '#/parameters/msisdn
  	 *	-  $ref: '#/parameters/ban
  	 *	-  $ref: '#/parameters/access_token
  	 *	-  $ref: '#/parameters/application_client
  	 *	-  $ref: '#/parameters/application_version_code
  	 *	-  $ref: '#/parameters/device_os
  	 *	-  $ref: '#/parameters/os_version
  	 *	-  $ref: '#/parameters/os_language
  	 *	-  $ref: '#/parameters/device_manufacturer
  	 *	-  $ref: '#/parameters/device_model
  	 *	-  $ref: '#/parameters/marketing_cloud_id
  	 *	-  $ref: '#/parameters/sd_id
  	 *	-  $ref: '#/parameters/adobe_uuid

  	 * @return
  	 * @throws Exception 
  	 */
      public Response updateMlCustType(ApiTestData apiTestData,String mlCustType, Map<String, String> tokenMap) throws Exception{
      	Map<String, String> headers = buildProfileAPIHeader(apiTestData, tokenMap);
      	headers.put("mlCustType", mlCustType);
      	String resourceURL = "/v1/profile/updatemlcusttype";
      	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.POST);
  		return response;
      } 
      
    private Map<String, String> buildProfileAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
    	tokenMap.put("version","v1");
		Map<String, String> headers = new HashMap<String, String>();				
		headers.put("Accept", "application/json");
		headers.put("Content-Type", "application/json");
		headers.put("X-B3-TraceId", "FlexAPITest123");
		headers.put("X-B3-SpanId", "FlexAPITest456");
		headers.put("channel_id", "DESKTOP");
		headers.put("Authorization", checkAndGetJWTToken(apiTestData, tokenMap).get("jwtToken"));
		headers.put("application_id", "MYTMO");
		headers.put("user-token", "user-token");
		headers.put("tmo-id", "MYTMO");
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("ban", apiTestData.getBan());
		headers.put("usn", "TestUSN");
		headers.put("application_client", "{{application_client}}");
		headers.put("application_version_code", "{{application_version_code}}");
		headers.put("device_os", "{{device_os}}");
		headers.put("os_version", "{{os_version}}");
		headers.put("os_language", "{{os_language}}");
		headers.put("device_manufacturer", "{{device_manufacturer}}");
		headers.put("device_model", "{{device_model}}");
		headers.put("marketing_cloud_id", "{{marketing_cloud_id}}");
		headers.put("sd_id", "{{sd_id}}");
		headers.put("adobe_uuid", "{{adobe_uuid}}");
		headers.put("access_token", checkAndGetJWTToken(apiTestData, tokenMap).get("accessToken"));
		headers.put("Cache-Control", "no-chache");
		//headers.put("dealerCode", "000002");
		return headers;
	}

    public Map<String,String>  updateMlCustTypeRespElementsMap(Map<String,String> tokenMap,ApiTestData apiTestData,List<String> elements,String updateMlCustType) throws Exception {
		String operationName="updateMlCustType";
		Map<String, String> elementsMap = new HashMap<String, String>();
		Response response = updateMlCustType(apiTestData,updateMlCustType, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			Boolean updateSuccessful = jsonNode.path("updateSuccessful").asBoolean();
			elementsMap.put("updateSuccessful", updateSuccessful.toString());
		}else {
			failAndLogResponse(response, operationName);
		}
		return elementsMap;
	}
	
	public Map<String,String>  getProfileRespElementsMap(Map<String,String> tokenMap,ApiTestData apiTestData,List<String> elements) throws Exception {
		String operationName="getProfile";
		Map<String, String> elementsMap = new HashMap<String, String>();
		Response response = getProfile(apiTestData, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			String govAcNonMasterPermission = jsonNode.path("govAcNonMasterPermission").textValue();
			elementsMap.put("govAcNonMasterPermission", govAcNonMasterPermission!=null ? govAcNonMasterPermission : "");
			JsonNode lines = jsonNode.path("lines");
			for (int i = 0; i < lines.size(); i++) {
				if (!lines.isMissingNode() && lines.isArray()) {
					JsonNode line = lines.get(i);
					String permission = line.path("permission").asText();
					//elementsMap.put("permission", permission);
					elementsMap.put(line.path("msisdn").asText(), permission);
				}
				
			}
			
		}else {
			failAndLogResponse(response, operationName);
		}
		return elementsMap;
	}
	
	public Map<String,String>  getProfileRespElementsMap1(Map<String,String> tokenMap,ApiTestData apiTestData,List<String> elements) throws Exception {
		String operationName="getProfile";
		Map<String, String> elementsMap = new HashMap<String, String>();
		Response response = getProfile(apiTestData, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = mapper.readTree(response.asString());
			//String govAcNonMasterPermission = jsonNode.path("govAcNonMasterPermission").textValue();
			//elementsMap.put("govAcNonMasterPermission", govAcNonMasterPermission);
			JsonNode lines = jsonNode.path("lines");
			if (!lines.isMissingNode() && lines.isArray()) {
				JsonNode line = lines.get(0);
				String permission = line.path("permission").asText();
				elementsMap.put("permission", permission);
			}
		}else {
			failAndLogResponse(response, operationName);
		}
		return elementsMap;
	}
}
