package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

public class ShopPage extends CommonPage {
	private static final Logger logger = LoggerFactory.getLogger(ShopPage.class);

	private static final String pageLoadedText = "Plus, get a bill credit for the trade-in value of your old phone After rebate and qualifying trade-in";
	private static final String pageUrl = "shop";
	private static final int fullBannerEyeBrowTextCount = 25;
	private static final int fullBannerHeadlineTextCount = 60;
	private static final int fullBannerParagraphTextCount = 200;
	private static final int fullBannnerLegalTextCount = 100;
	private static final int fullBannerPrimaryCTACount = 20;
	private static final int fullBannerSecondaryCTACount = 20;
	private static final int halfBannerEyeBrowTextCount = 25;
	private static final int halfBannerHeadlineTextCount = 60;
	private static final int halfBannerParagraphTextCount = 120;
	private static final int halfBannnerLegalTextCount = 100;
	private static final int halfBannerPrimaryCTACount = 20;
	private static final int halfBannerSecondaryCTACount = 20;

	public static final String PROMOFLOW = "promoflow";
	public static final String SEEALLPHONES = "seeAllPhones";
	public static final String SELECTDEVICE = "selectDevice";

	@FindBy(xpath = "//button[text()='Add']")
	private WebElement promoBanner;

	@FindBy(css = ".stickshowStyle p.legal")
	private WebElement topUpgradeBanner;

	@FindBy(css = ".stickshowStyle div span")
	private WebElement topUpgradeBannerTitle;

	@FindBy(css = ".stickshowStyle div span")
	private WebElement topUpgradeBannerText;

	@FindBy(css = ".stickshowStyle a.legal")
	private WebElement topUpgradeBannerSeeDetailsLink;

	@FindBy(css = "#seeLegalModal div.no-padding span")
	private WebElement topUpgradeBannerPopUpHeader;

	@FindBy(css = "#seeLegalModal div.text-center span")
	private WebElement topUpgradeBannerPopUpText;

	@FindBy(css = "i.fa.fa-spin.fa-spinner.interceptor-spinner")
	private WebElement spinner;

	@FindBy(css = "#seeLegalModal button[class='PrimaryCTA-accent pull-right']")
	private WebElement popupCloseButton;

	@FindBy(xpath = "//span[text()='Promotional Ineligible...']")
	private WebElement promotionInelgibleText;

	@FindBy(css = "div#bannerDiv")
	private WebElement heroBanner;

	@FindBy(css = "div#bannerDiv div.section-content div.col-12")
	private List<WebElement> heroBannerDetails;

	@FindBy(css = "div#bannerDiv button.PrimaryCTA")
	private WebElement heroBannerBuyBtn;

	@FindBy(css = "div#bannerDiv button.SecondaryCTA")
	private WebElement heroBannerSellBtn;

	@FindBy(css = "div.container div.center-content a.nav-element")
	private List<WebElement> heroBannerBelowButtons;

	@FindBy(xpath = "//p[contains(text(), 'Featured Devices')]")
	private WebElement featuredDeviceSection;

	@FindBy(xpath = "//p[contains(text(), 'Featured phones')]")
	private WebElement featuredPhonesSection;

	@FindBy(css = "div#latestDeals")
	private WebElement latestDealsBanner;

	@FindBy(css = "div#latestDeals div.section-content div.col-sm-12")
	private List<WebElement> latestDealsDetails;

	@FindBy(css = "div#latestDeals button.PrimaryCTA")
	private List<WebElement> latestDealsBannerBuyButton;

	@FindBy(css = "div#imgDiv div.d-inline-grid-ms")
	private List<WebElement> popularPhones;

	@FindBy(xpath = "//button[text()='See all phones']")
	private WebElement seeAllPhonesBottomBtn;

	@FindBy(css = "div.dialog.animation-in span.pull-left")
	private WebElement aalErrorModel;

	@FindBy(css = "#idFeatureDeviceHdr")
	private WebElement featureDevicetitle;

	int latestDealsBannerSizeBeforeClickOnLoadMore;

	int latestDealsBannerSizeAfterClickOnLoadMore;

	@FindBy(css = "div.divCursor.padding-vertical-medium.bg-white")
	private List<WebElement> holidayDealPromoDeviceList;

	@FindBy(xpath = "(//*[contains(text(),'Featured')]/../../div[@class='col-12 no-padding flex-prop']/div)[1]/div/div/div/p/span[@class='legal']")
	private WebElement holidayDealpromoDevicesPromoTitle;

	@FindBy(xpath = "(//div[contains(@class,' col-12 no-padding  divCursor padding-vertical-medium bg-white')])[1]/div/div/div/p[@class='H6-heading mb-0  ']")
	private WebElement holidayDealpromoDevicesDeviceTitle;

	@FindBy(xpath = "(//div[contains(@class,' col-12 no-padding  divCursor padding-vertical-medium bg-white')])[1]/div/div/div[3]/div[@class='border-f2f2f2 pr-3']")
	private WebElement holidayDealpromoDevicesTodayAndMonthlyPricing;

	@FindBy(xpath = "(//div[contains(@class,' col-12 no-padding  divCursor padding-vertical-medium bg-white')])[1]/div/div/div/p[@class='mb-0 legal ']")
	private WebElement holidayDealpromoDeviceFRP;

	@FindBy(xpath = "//div[contains(@class,'aal-ineligibility-modal')]")
	private WebElement aalIneligibleModalWindow;

	@FindBy(css = "#seeLegalModal")
	private WebElement legalTermsModalWindow;

	@FindBy(css = "div#bannerDiv a.tmo-modal-link")
	private WebElement legalTermsLink;

	@FindBy(css = "div#bannerDiv button.PrimaryCTA")
	private WebElement heroBannersContactUsButton;

	@FindBy(css = "div.dialog-container div.dialog.animation-out.animation-in span")
	private WebElement addALineEligibilityMessage;

	@FindBys(@FindBy(css = "p[ng-bind-html*='deviceDetail.deviceName']"))
	private List<WebElement> featuredeviceNames;

	@FindBys(@FindBy(css = "a[ng-bind*='selectDeviceTxt']"))
	private List<WebElement> selectDevice;

	@FindBy(id = "lnkDoneAddingServices")
	private WebElement addServiceContinueBtn;

	@FindBy(xpath = "//button[contains(text(), 'Get the deal') or contains(text(),'Get the details') or contains(text(),'Get started')]")
	private WebElement marqueeBannerCTA;

	@FindBy(xpath = "//button[contains(text(), 'Get this deal') or contains(text(),'Get the details')]")
	private WebElement buyNowCTA;

	@FindBy(css = "div.text-center a[href*='/shop/accessories.html']")
	private WebElement shopAccessoriesButton;

	@FindBy(css = "p[ng-bind='$ctrl.objAALEligibility.aalErrorMsg']")
	private WebElement puertoRicoCustomerEligibilityMes;

	@FindBy(css = "a[ng-bind*='$ctrl.contentData.addALineBtnLabel']")
	private WebElement addAlineButton;

	@FindBy(css = ".featureDivName.col-12.no-padding p")
	private List<WebElement> featuredDevices;

	@FindBy(css = "div.featureDivName.col-12.no-padding")
	private List<WebElement> featuredDevicesList;

	@FindBy(css = ".border-f2f2f2.pr-3 p:first-child")
	private List<WebElement> EipTodayText;

	@FindBy(css = ".border-f2f2f2 .body-bold.mb-0.black")
	private List<WebElement> EipTodayPrice;

	@FindBy(css = ".pl-3 p:first-child")
	private List<WebElement> EipMonthlyText;

	@FindBy(css = ".pl-3 p.body-bold.black.mb-0")
	private List<WebElement> EipMonthlyPrice;

	@FindBy(css = ".border-f2f2f2.pr-3 p:last-child")
	private List<WebElement> EipDownTaxText;

	@FindBy(css = "div.pl-3 p:last-child")
	private List<WebElement> EipInstallmentsMonthsText;

	@FindBy(css = "span.legal")
	private List<WebElement> featuredFRPText;

	@FindBy(css = ".padding-horizontal-medium.pt-1.pb-1 p.mb-0.legal")
	private List<WebElement> holidayFRPText;

	@FindBy(css = "span.promoTxt")
	private List<WebElement> promoOfferLinks;

	@FindBy(css = "#seeLegalModal.animation-in")
	private WebElement promoModal;

	@FindBy(css = "#seeLegalModal.animation-in span.Display5")
	private WebElement promoModalTitle;

	@FindBy(css = "#seeLegalModal.animation-in div.body span")
	private WebElement promoModalText;

	@FindBy(css = ".overlay-in")
	private WebElement overlayOutside;

	@FindBy(css = "#featuredDevices :nth-child(7) .featureDivName p")
	private WebElement tradeInPromoDevicePDPFlow;

	@FindBy(css = "button[class*='TeritiaryCTA ']")
	private List<WebElement> bannerLinksToLS;

	@FindBy(css = "div[class='hero_banner'] button[class='PrimaryCTA full-width-md']")
	private WebElement promoBannerCTA;

	// @FindBy(xpath = "//p[contains(text(),'Apple iPhone
	// 8')]/../../../div[1]/div[2]/div[2]/p/span[2]/s")
	@FindBy(xpath = "//p[contains(text(),'Apple iPhone X')]/../../../div[1]/div[2]/div[2]/p/span[2]/s")
	private WebElement catelogPromoStrikethoughPrice;

	// @FindBy(xpath = "//p[contains(text(),'Apple iPhone
	// 8')]/../../../div[1]/div[2]/div[2]/p/span[2]")
	@FindBy(xpath = "//p[contains(text(),'Apple iPhone X') and not(contains(text(),'Apple iPhone XS'))]/../../../div[1]/div[2]/div[2]/p/span[2]")
	private WebElement newFRPAfterCatelogPromo;

	// @FindBy(xpath = "//p[contains(text(),'Apple iPhone
	// 8')]/../../../div[1]/div[2]/div/div[@class='border-f2f2f2 pr-3']/p[2]")
	@FindBy(xpath = "//p[contains(text(),'Apple iPhone X')and not(contains(text(),'Apple iPhone XS'))]/../../../div[1]/div[2]/div/div[@class='border-f2f2f2 pr-3']/p[2]")
	private WebElement downPaymentOfCatelogPromo;

	// @FindBy(xpath = "//p[contains(text(),'Apple iPhone
	// 8')]/../../../div[1]/div[2]/div/div[@class='pl-3']/p[2]")
	@FindBy(xpath = "//p[contains(text(),'Apple iPhone X')and not(contains(text(),'Apple iPhone XS'))]/../../../div[1]/div[2]/div/div[@class='pl-3']/p[2]")
	private WebElement eipPriceOfCatelogPromo;

	// @FindBy(xpath = "//p[contains(text(),'Apple iPhone
	// 8')]/../../../div[1]/div[2]/div/div[@class='pl-3']/p[3]")
	@FindBy(xpath = "//p[contains(text(),'Apple iPhone X')and not(contains(text(),'Apple iPhone XS'))]/../../../div[1]/div[2]/div/div[@class='pl-3']/p[3]")
	private WebElement epiTermsOfCatelogPromo;

	@FindBy(css = "button[class='TeritiaryCTA margin-left-small no-padding-lr text-theme']")
	private WebElement readyToSwitchButton;

	// @FindBy(css = "#seeLegalModal.dialog.animation-out")
	@FindBy(css = ".dialog.animation-out span.text-center")
	private WebElement modalPopupHeader;

	// @FindBy(css = "p[ng-bind='$ctrl.aalWarningModalContent.header']")
	@FindBy(css = ".dialog.animation-out span.padding-vertical-header")
	private WebElement modalPopupDescription;

	@FindBy(css = ".dialog.animation-out span.padding-vertical-message")
	private WebElement modalPopupDescription1;

	@FindBy(css = ".")
	private WebElement modalCloseCTA;

	@FindBy(xpath = "//button[contains(text(),'Contact Us')]")
	private WebElement contactusOnPopup;

	// @FindBy(css = "span[class='col-12 no-padding pull-left body
	// padding-vertical-medium']")
	@FindBy(xpath = "//span[contains(text(),'Puerto Rico Account')]")
	private WebElement aalIneligibleMessageForPuertoRicoCustomer;

	@FindBy(css = "button.btn-dialog")
	private WebElement returnToShoppingForPuertoCustomer;

	@FindBy(css = "button[class='TeritiaryCTA margin-left-small no-padding-lr text-theme']")
	private WebElement aalIneligibleMessageForMBBLines;

	@FindBy(css = "button[class='TeritiaryCTA margin-left-small no-padding-lr text-theme']")
	private WebElement aalIneligibleMessageForDelinquentAcc;

	// @FindBy(css = "button[class='TeritiaryCTA margin-left-small no-padding-lr
	// text-theme']")
	@FindBy(css = ".aal-ineligibility-modal span:nth-child(2)")
	private WebElement aalIneligibleMessageForSuspendedAccount;

	@FindBy(css = "button[class='TeritiaryCTA margin-left-small no-padding-lr text-theme']")
	private WebElement aalIneligibleMessageForUserRoleNotAmongPAH;

	@FindBy(css = "button[class='TeritiaryCTA margin-left-small no-padding-lr text-theme']")
	private WebElement aalIneligibleMessageForBankRuptcyAccount;

	@FindBy(css = "span.eyebrow.visible")
	private List<WebElement> catalogPromoDevices;

	@FindBy(css = "")
	private WebElement seeDetailsCTA;

	@FindBy(css = "")
	private WebElement seeDetailsModal;

	@FindBy(css = "")
	private List<WebElement> featuredTodayPrice;

	@FindBy(css = "a[href*='support']")
	private WebElement supportLink;

	@FindBy(css = "a[href*='contact-us']")
	private WebElement contactUsLink;

	@FindBy(css = "a[href*='coverage']")
	private WebElement coverageLink;

	@FindBy(css = "a[href*='store-locator']")
	private WebElement storeLocatorLink;

	@FindBy(css = "a[href='https://www.t-mobile.com/']")
	private WebElement tmobileLink;

	@FindBy(css = "div.co_footer-nav a#logoutfoot-Ok")
	private WebElement logoutLink;

	@FindBy(css = "button[class='PrimaryCTA-accent pull-right']")
	private List<WebElement> aalModalCloseCTA;

	@FindBy(xpath = "//span[text()='Pending Rate Plan']")
	private WebElement pendingRatePlanModelHeader;

	@FindBy(xpath = "//button[text()='Shop now']")
	private WebElement shopNowButton;

	@FindBy(css = "span[class*='aal-modal-header']")
	private WebElement aalIneligibleMessageForMaxVoiceLinesAndMaxMBB;

	@FindBy(css = "#acsMainInvite .acsCloseButton--container>span>a")
	private List<WebElement> mobileFeedBackAlert;

	@FindBy(css = "[id='user-links-dropdown']")
	private WebElement userName;

	@FindBy(css = "a#user-links-dropdown-mobile span.show-on-sm-modal")
	private WebElement mobileUserName;

	@FindBy(css = ".mdi-menu")
	private WebElement hamburgerMenu;

	@FindBy(css = "button.ntm-navbar__hamburger-close span.mdi")
	private WebElement mobilecloseIcon;

	public ShopPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the ShopPage class instance.
	 */
	public ShopPage verifyPageLoaded() {
		waitforSpinner();
		checkPageIsReady();
		try {
			verifyPageUrl();
		} catch (NoSuchElementException e) {
			Assert.fail("Shop page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the ShopPage class instance.
	 */
	public ShopPage verifyPageUrl() {
		try {
			if (mobileFeedBackAlert.size() == 1) {
				mobileFeedBackAlert.get(0).click();
			}
			Assert.assertTrue(getDriver().getCurrentUrl().contains(pageUrl), "Shop Page is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Shop page");
		}
		return this;
	}

	/**
	 * Click buy button in the hero banner section.
	 *
	 * @return the ShopPage class instance.
	 */
	public ShopPage clickOnHeroBannerBuyButton() {
		try {
			heroBannerBuyBtn.click();
			Reporter.log("Clicked on Hero Banner Buy Button on Shop Page");
		} catch (Exception e) {
			Assert.fail("HeroBannerBuyButton is not available to click");
		}
		return this;
	}

	/**
	 * Click sell button in the hero banner section.
	 *
	 * @return the ShopPage class instance.
	 */
	public ShopPage clickOnHeroBannerSellButton() {
		try {
			clickElementWithJavaScript(heroBannerSellBtn);
			Reporter.log("Clicked on Hero Banner Sell Button on Shop Page");
		} catch (Exception e) {
			Assert.fail("HeroBannerSellButton is not available to click");
		}
		return this;
	}

	/**
	 * Click Quick Links {Apple, Samsung, All phones, Latest Deals, Add A Line,}
	 * 
	 * @param option
	 * @return
	 */
	public ShopPage clickQuickLinks(String option) {
		waitforSpinner();
		moveToElement(featureDevicetitle);
		try {
			// waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.loader")));
			for (WebElement webElement : heroBannerBelowButtons) {
				if (webElement.getText().equalsIgnoreCase(option)) {
					moveToElement(webElement);
					clickElement(webElement);
					break;
				}
			}
			Reporter.log("Clicked on " + option + "  Button from Shop Page");
		} catch (NoSuchElementException e) {
			Assert.fail("Link " + option + " not found in shop page");
		}
		return this;
	}

	/**
	 * verify AAL ineligible popup.
	 *
	 * @return the ShopPage class instance.
	 */
	public ShopPage verifyAALIneligiblePopup() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(aalErrorModel));
			aalErrorModel.isDisplayed();
		} catch (Exception e) {
			Assert.fail("AALIneeligiblePopup is not Displayed");
		}
		return this;
	}

	/**
	 * Click Accessories button under hero banner section.
	 *
	 * @return the ShopPage class instance.
	 */
	public ShopPage clickOnLatestDealsBannerBuyButton() {
		try {
			moveToElement(latestDealsBannerBuyButton.get(0));
			latestDealsBannerBuyButton.get(0).click();
			Reporter.log("Clicked on Latest Deals Banner Buy Button");
			checkPageIsReady();
		} catch (Exception e) {
			Reporter.log("Latest Deals Banner Buy Button is not Dispalyed to Click");
		}

		return this;
	}

	/**
	 * Click seeAllPhonesBottomBtn.
	 *
	 * @return the ShopPage class instance.
	 */
	public ShopPage clickOnSeeAllPhonesBottomBtn() {
		try {
			clickElementWithJavaScript(seeAllPhonesBottomBtn);
			Reporter.log("Clicked on see All Phones Button At the Bottom of the Page");
		} catch (NoSuchElementException e) {
			e.addInfo(getCurrentWindowTitle(), "See All Phones button not found");
		}
		return this;
	}

	/**
	 * Verify new shop page is displayed
	 * 
	 * @return
	 */
	public ShopPage verifyNewShoppage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			getDriver().getCurrentUrl().contains("shop");
		} catch (Exception e) {
			Assert.fail("NewShoppage is not available");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible modal window
	 * 
	 * @return
	 */
	public ShopPage verifyAALIneligibleModalWindow() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(aalIneligibleModalWindow));
			aalIneligibleModalWindow.isDisplayed();
			Reporter.log("AALIneligibleModalWindow is displayed");
		} catch (Exception e) {
			Assert.fail("AALIneligibleModalWindow is not displayed");
		}
		return this;
	}

	/**
	 * Verify AAL ineligible modal window
	 * 
	 * @return
	 */
	public ShopPage verifyLegalTermsModalWindow() {
		try {
			legalTermsModalWindow.isDisplayed();
			Reporter.log("Legal Terms Model Window is Displayed");

		} catch (Exception e) {
			Assert.fail("Legal Terms Window Model is not Displayed");
		}
		return this;
	}

	/**
	 * Click legal terms link
	 * 
	 * @return
	 */
	public ShopPage clickLegalTermsLink() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				moveToElement(featuredPhonesSection);
				legalTermsLink.click();
				Reporter.log("Clicked onLegal Terms Link");
			} else {
				legalTermsLink.click();
				Reporter.log("Clicked onLegal Terms Link");
			}

		} catch (Exception e) {
			Assert.fail("Legal Terms Link is not Dispalyed to click");
		}
		return this;

	}

	/**
	 * Click On HeroBanners ContactUs Button
	 */
	public ShopPage clickOnHeroBannersContactUsButton() {
		try {
			waitforSpinner();
			heroBannersContactUsButton.click();
			Reporter.log("Clicked on Hero Banner Contact Us Button");
		} catch (Exception e) {
			Assert.fail("BannersContactUsButton is not available to click");
		}
		return this;
	}

	/**
	 * Verify Add A Line Eligibility
	 * 
	 * @return
	 */
	public ShopPage verifyAddALineEligibility() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addALineEligibilityMessage));
			addALineEligibilityMessage.isDisplayed();
		} catch (Exception e) {
			Assert.fail("Add a line eligibility message is not displayed");
		}
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 */
	public ShopPage verifyShoppage() {
		try {
			verifyPageUrl();
			waitFor(ExpectedConditions.visibilityOf(seeAllPhonesBottomBtn), 10);
			Thread.sleep(3000);
			Reporter.log(Constants.SHOP_PAGE_HEADER);
		} catch (Exception e) {
			Assert.fail("Shop Page is Not Loaded");
		}
		return this;
	}

	/**
	 * Click See All Phones
	 */
	public ShopPage clickSeeAllPhones() {
		waitforSpinner();
		try {
			clickElement(seeAllPhonesBottomBtn);
			Reporter.log("Clicked on See All Phone cta");
		} catch (Exception e) {
			Assert.fail("See all phones button is not clickable");
		}
		return this;
	}

	/**
	 * Click Marquee Banner CTA
	 */
	public ShopPage clickMarqueeBannerCTA() {
		try {
			marqueeBannerCTA.click();
			Reporter.log("Marquee Banner is clickable");
		} catch (Exception e) {
			Assert.fail("Marquee Banner is not clickable");
		}
		return this;
	}

	/**
	 * Click Order Now Button
	 */
	public ShopPage clickOrderNowButton() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(buyNowCTA);
			Reporter.log("Order now button is clickable");
		} catch (Exception e) {
			Assert.fail("Order now button is not clickable");
		}
		return this;
	}

	/**
	 * Verify Add A Line Is Enabled Or Disabled
	 * 
	 * @return
	 */
	public ShopPage verifyAddALineIsEnabledOrDisabled() {
		try {
			addAlineButton.isEnabled();
			Reporter.log("Add a line is enabled");
		} catch (Exception e) {
			Assert.fail("Add a line is disabled");
		}
		return this;
	}

	/**
	 * Verify Add A Line Is Enabled Or Disabled
	 * 
	 * @return
	 */
	public ShopPage verifyAddALineIsDisabled() {
		try {
			Assert.assertFalse(addAlineButton.isEnabled(), "Add a line is enabled");
			Reporter.log("Add a line is disabled");
		} catch (Exception e) {
			Assert.fail("Add a line is enabled");
		}
		return this;
	}

	/**
	 * Click on Featured Devices
	 * 
	 * @param device
	 * @return
	 */
	public ShopPage clickOnFeaturedDevices(String device) {
		waitforSpinner();
		checkPageIsReady();
		try {
			for (WebElement webElement : featuredDevices) {
				if (webElement.getText().trim().equalsIgnoreCase(device)) {
					((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,700)");
					waitFor(ExpectedConditions.elementToBeClickable(webElement));
					clickElementWithJavaScript(webElement);
					Reporter.log("Clicked on Featured Devices");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Featuread devices are not displayed");
		}
		return this;
	}

	/** Click on device in featured device_Generalized */

	public ShopPage clickFeaturedDeviceByName(String deviceName) {
		try {
			waitforSpinner();
			if (getDriver().getCurrentUrl().contains("shop")) {
				for (WebElement webElement : featuredDevices) {
					if (webElement.getText().contains(deviceName) || webElement.getText().contains("Apple iPhone 7")
							|| webElement.getText().contains("Apple iPhone 8")) {
						webElement.click();
						break;
					}
				}
			}
			Reporter.log("Selected apple device");
		} catch (Exception e) {
			Assert.fail("Unable to select apple device");
		}
		return this;
	}

	/**
	 * Verify Today text is displayed for all devices on shop landing page
	 * 
	 * @return
	 */

	public ShopPage verifyTodayTextForDevices() {
		try {
			waitforSpinner();
			for (WebElement eipTodayText : EipTodayText) {
				Assert.assertTrue(eipTodayText.getText().equalsIgnoreCase("TODAY"),
						"EIP Today Text for devices is not displayed");
			}
			Reporter.log("EIP Today Text is present for all Devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Today Text");
		}
		return this;
	}

	/**
	 * Verify Today price is displayed for featured devices
	 * 
	 * @return
	 */

	public ShopPage verifyTodayPriceForDevices() {
		try {
			waitforSpinner();
			for (WebElement eipTodayPrice : EipTodayPrice) {
				Assert.assertTrue(eipTodayPrice.isDisplayed(), "EIP Today Price for featured devices is displayed");
			}
			Reporter.log("EIP Today Price is present for all devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP Today Price");
		}
		return this;
	}

	/** Verify EIP DownPayment+Tax text for featured devices */

	public ShopPage verifyEIPDownPaymentTaxTextForDevices() {
		try {
			waitforSpinner();
			for (WebElement eipDownPaymentTaxText : EipDownTaxText) {
				Assert.assertTrue(eipDownPaymentTaxText.getText().equalsIgnoreCase("down + tax"),
						"EIP DownPayment and Tax text for featured devices is displayed");
			}
			Reporter.log("EIP DownPayment and Tax text is displayed for all devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP DownPayment and Tax text");
		}
		return this;
	}

	/**
	 * Verify Monthly text is displayed for featured devices
	 * 
	 * @return
	 */

	public ShopPage verifyMonthlyTextForDevices() {
		try {
			waitforSpinner();
			for (WebElement eipMonthlyText : EipMonthlyText) {
				Assert.assertTrue(eipMonthlyText.getText().equalsIgnoreCase("MONTHLY"),
						"EIP monthly text for featured devices is displayed");
			}
			Reporter.log("EIP monthly text is displayed for all devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP monthly text");
		}
		return this;
	}

	/**
	 * Verify Monthly price is displayed for featured devices
	 * 
	 * @return
	 */

	public ShopPage verifyMonthlyPriceForDevices() {
		try {
			waitforSpinner();
			for (WebElement eipMonthlyPrice : EipMonthlyPrice) {
				Assert.assertTrue(eipMonthlyPrice.getText().contains("$"),
						"EIP monthly price for featured devices is displayed");
			}
			Reporter.log("EIP monthly price for featured devices is displayed for all devices");

		} catch (Exception e) {
			Assert.fail("Filed to verify EIP monthly price");
		}
		return this;
	}

	/** Verify loan length */

	public ShopPage verifyLoanTermLength() {
		try {
			waitforSpinner();
			for (WebElement loanLength : EipInstallmentsMonthsText) {
				Assert.assertTrue(loanLength.isDisplayed(),
						"EIP monthly installments text for featured devices is displayed");
				Assert.assertTrue(
						loanLength.getText().contains("9") | loanLength.getText().contains("12")
								| loanLength.getText().contains("24") | loanLength.getText().contains("36"),
						"EIP Loan Term is not displayed correctly");
			}
			Reporter.log("EIP monthly installments text for featured devices is displayed for all devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP loan length term");
		}
		return this;
	}

	/** Verify FRP price for featured devices */

	public ShopPage verifyFRPPriceForFeaturedDevices() {
		try {
			waitforSpinner();
			for (WebElement verifyFRPPrice : featuredFRPText) {
				Assert.assertTrue(verifyFRPPrice.getText().contains("$"), "FRP Price is displayed");
			}
			Reporter.log("FRP price for devices is displayed for all devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP price for devices");
		}
		return this;
	}

	/** Verify FRP text for featured devices */

	public ShopPage verifyFRPTextForFeaturedDevices() {
		try {
			waitforSpinner();
			for (WebElement verifyFRPText : featuredFRPText) {
				Assert.assertTrue(verifyFRPText.getText().contains("Full retail"), "FRP text is displayed");
			}
			Reporter.log("FRP text for devices is displayed for all devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP text for devices");
		}
		return this;
	}

	/** Verify FRP Price for holiday devices */

	public ShopPage verifyFRPPriceForHolidayDevices() {
		try {
			waitforSpinner();
			for (WebElement hFRPPrice : holidayFRPText) {
				Assert.assertTrue(hFRPPrice.getText().contains("$"), "FRP Price is not displayed for holiday devices");
			}
			Reporter.log("FRP price for holiday devices is displayed for all devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP price for holiday devices");
		}
		return this;

	}

	/** Verify FRP text for holiday devices */

	public ShopPage verifyFRPTextForHolidayDevices() {
		try {
			waitforSpinner();
			for (WebElement hFRPText : holidayFRPText) {
				Assert.assertTrue(hFRPText.getText().contains("Full retail"),
						"FRP text for holiday devices is not displayed");
			}
			Reporter.log("FRP text for holiday devices is displayed for all devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP text for holiday devices");
		}
		return this;

	}

	/**
	 * Click Offer Promo Link
	 */
	public ShopPage clickFirstOfferPromo() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(promoOfferLinks.get(0));
			Reporter.log("Promo Offer Banner Link is clickable");
		} catch (Exception e) {
			Assert.fail("Promo Offer Banner Link is not clickable");
		}
		return this;
	}

	/**
	 * Verify Offer Promo Modal
	 * 
	 * @return
	 */
	public ShopPage verifyOfferPromoModal() {
		try {
			waitforSpinner();
			Assert.assertTrue(promoModal.isDisplayed(), "Offer Promo Modal Not Displayed");
			Reporter.log("Offer Promo Modal is Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display offer Promo Modal");
		}
		return this;
	}

	/**
	 * Verify Offer Promo Modal Title
	 * 
	 * @return
	 */
	public ShopPage verifyOfferPromoModalTitle() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(promoModalTitle));
			Assert.assertTrue(promoModalTitle.isDisplayed(), "Offer Promo Modal Title is Not Displayed");
			Reporter.log("Offer Promo Modal Title is Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Offer Promo Modal Title");
		}
		return this;
	}

	/**
	 * Verify Offer Promo Modal Title
	 * 
	 * @return
	 */
	public ShopPage verifyOfferPromoModalText() {
		try {
			waitforSpinner();
			Assert.assertTrue(promoModalText.isDisplayed(), "Offer Promo Modal Text is Not Displayed");
			Reporter.log("Offer Promo Modal Text is Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Offer Promo Modal Text");
		}
		return this;
	}

	/**
	 * Click outside Offer Promo Link
	 */
	public ShopPage clickOutsideOfferPromo() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(overlayOutside);
			Reporter.log("Overlay outside Promo Modal is clickable");
		} catch (Exception e) {
			Assert.fail("Overlay outside Promo Modal is not clickable");
		}
		return this;
	}

	/**
	 * Verify Offer Promo Modal Not Displayed
	 * 
	 * @return
	 */
	public ShopPage verifyOfferPromoModalNotDisplayed() {
		try {
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(promoModal), "Offer Promo Modal is Displayed");
			Reporter.log("Offer Promo Modal is Not Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify offer Promo Modal not Displayed");
		}
		return this;
	}

	/**
	 * Click on Promo Banner in Shop page
	 */
	public ShopPage clickTopUpgradeBannerSeeDetailsLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(topUpgradeBannerSeeDetailsLink));
			clickElementWithJavaScript(topUpgradeBannerSeeDetailsLink);
			Reporter.log("Upgrade Banner is clickable");
		} catch (Exception e) {
			Assert.fail("Upgrade Banner is not clickable");
		}
		return this;
	}

	/**
	 * Verify on Top Banner popup in Shop page
	 */
	public ShopPage verifyTopUpgradeBannerPopUp() {
		try {
			Assert.assertTrue(topUpgradeBannerPopUpHeader.isDisplayed(),
					"Upgrade Top Banner pop Up Header in shop page is not displayed");
			Reporter.log("Upgrade Top Banner Pop Up header in shop pgae is displayed");
			/*
			 * Assert.assertTrue(topUpgradeBannerPopUpText.getText(). contains(
			 * "See details Modal"),
			 * "Upgrade Top Banner pop Up Description in shop page is not displayed" );
			 * Reporter. log(
			 * "Upgrade Top Banner Pop Up Description in shop page is displayed" );
			 */
		} catch (Exception e) {
			Assert.fail("Upgrade Top Banner Pop Up in shop page is not displayed");
		}
		return this;
	}

	/**
	 * Verify on Top Banner in Shop page
	 */
	public ShopPage verifyTopUpgradeBanner() {
		try {
			waitFor(ExpectedConditions.visibilityOf(topUpgradeBanner));
			Assert.assertTrue(topUpgradeBanner.getText().contains("DID YOU KNOW?"),
					"Upgrade Top Banner in shop pgae is not displayed");
			Reporter.log("Upgrade Top Banner in shop page is displayed");

			Assert.assertTrue(topUpgradeBannerText.getText().contains("You’ll save"),
					"Upgrade Top Banner not contains upgrading text.");
			Reporter.log("Upgrade Top Banner contains title and authorable text.");
		} catch (Exception e) {
			Assert.fail("Upgrade Top Banner in shop page is not displayed");
		}
		return this;
	}

	/**
	 * Verify catelog promo device is present
	 *
	 */
	public ShopPage verifyCatelogPromoDevice() {
		try {
			waitFor(ExpectedConditions.visibilityOf(catelogPromoStrikethoughPrice));
			Assert.assertTrue(isElementDisplayed(catelogPromoStrikethoughPrice), "catelog promo device is not present");
			Reporter.log("catelog promo device is present");
		} catch (Exception e) {
			Assert.fail("Failed to verify catelog promo device");
		}
		return this;
	}

	/**
	 * Get New FRP After Promo
	 *
	 */
	public Double newFRPCatelogPromoDevice() {

		waitFor(ExpectedConditions.visibilityOf(newFRPAfterCatelogPromo));
		String[] xyz = newFRPAfterCatelogPromo.getText().split(":");
		String ab[] = xyz[1].trim().split(" ");
		String pq[] = ab[1].split("\\$");
		Reporter.log("Get New FRP of catelog promo");
		return Double.parseDouble(pq[1]);
	}

	/**
	 * Get New FRP After Promo
	 *
	 */
	public Double downPaymentCatelogPromoDevice() {

		waitFor(ExpectedConditions.visibilityOf(downPaymentOfCatelogPromo));
		String[] xyz = downPaymentOfCatelogPromo.getText().split("\\$");
		Reporter.log("Get down payment of catelog promo");
		return Double.parseDouble(xyz[1]);
	}

	/**
	 * Get New monthly price after promo
	 *
	 */
	public Double monthlyPaymentCatelogPromoDevice() {

		waitFor(ExpectedConditions.visibilityOf(eipPriceOfCatelogPromo));
		String[] xyz = eipPriceOfCatelogPromo.getText().split("\\$");
		Reporter.log("Get monthly payment of catelog promo");
		return Double.parseDouble(xyz[1]);
	}

	/**
	 * Verify catelog promo device EIP is present
	 *
	 */
	public ShopPage verifyEipTermCatelogPromoDevice() {
		try {
			waitFor(ExpectedConditions.visibilityOf(epiTermsOfCatelogPromo));
			Assert.assertTrue(isElementDisplayed(epiTermsOfCatelogPromo),
					"catelog promo device EIP term is not present");
			Reporter.log("catelog promo device EIP term is present");
		} catch (Exception e) {
			Assert.fail("Failed to verify catelog promo device EIP term");
		}
		return this;
	}

	/**
	 * Get New monthly term price after promo
	 *
	 */
	public Double getEipTermCatelogPromoDevice() {

		waitFor(ExpectedConditions.visibilityOf(epiTermsOfCatelogPromo));
		String[] xyz = epiTermsOfCatelogPromo.getText().split(" ");
		Reporter.log("Get term monthly payment of catelog promo");
		return Double.parseDouble(xyz[1]);
	}

	/**
	 * Verify that Promo FRP value on PDP and PLP Page are equal
	 */
	public ShopPage compareFRPwithTotal(double firstValue, double secondValue) {
		try {
			Assert.assertEquals(firstValue, secondValue, " Promo FRP values PDP and PLP are not equal");
			Reporter.log("Promo FRP values onPDP and PLP  are equal");
		} catch (Exception e) {
			Assert.fail("Failed to compare Promo FRP values on PDP and PLP  page");
		}
		return this;
	}

	/**
	 * Click ReadyToSwitch Button
	 */
	public ShopPage clickReadyToSwitchButton() {
		try {
			readyToSwitchButton.click();
			Reporter.log("Ready to Switch button is clickable");
		} catch (Exception e) {
			Assert.fail("Ready to Switch button is not clickable");
		}
		return this;
	}

	/**
	 * Verify Modal popup
	 *
	 */
	public ShopPage verifyAALModalPopup() {
		try {
			waitFor(ExpectedConditions.visibilityOf(modalPopupHeader));
			Assert.assertTrue(isElementDisplayed(modalPopupHeader), "Modal Pop up is not displayed. ");
			Reporter.log("Modal Pop up is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify modal pop up");
		}
		return this;
	}

	/**
	 * Verify Modal pop up title and text
	 *
	 */
	public ShopPage verifyAALModalPopupTitleAndText() {
		try {
			Assert.assertTrue(isElementDisplayed(modalPopupHeader), "Let's talk title was not displayed.");
			Assert.assertTrue(modalPopupHeader.getText().contains("Let's talk"), "Let's talk title was not displayed.");
			Reporter.log("Let's talk title is displayed.");
			Assert.assertTrue(isElementDisplayed(modalPopupDescription),
					" Sorry - we're not able to add a line was not displayed ");
			Assert.assertTrue(
					modalPopupDescription.getText()
							.contains("Sorry - we're not able to add a line to your account online"),
					" Sorry - we're not able to add a line was not displayed.");
			Reporter.log("Sorry - we're not able to add a line to your account online is displayed.");
			Assert.assertTrue(isElementDisplayed(modalPopupDescription1),
					"To add a line, please call Customer Care at 1-800-937-8997, or by dialing 611 from your T-Mobile phone is not displayed.");
			Reporter.log(
					"To add a line, please call Customer Care at 1-800-937-8997, or by dialing 611 from your T-Mobile phone is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to verify Title and Description on Modal pop up");
		}
		return this;
	}

	/**
	 * Verify Contact US on Modal pop up.
	 *
	 */
	public ShopPage verifyContactusOnModalpopup() {
		try {
			waitFor(ExpectedConditions.visibilityOf(contactusOnPopup));
			Assert.assertTrue(isElementDisplayed(contactusOnPopup), "Contact US not displayed on Modal pop up. ");
			Reporter.log("Contact Us is displayed on Modal Pop up.");
		} catch (Exception e) {
			Assert.fail("Faile to verify Contact Us on modal pop up");
		}
		return this;
	}

	/**
	 * Verify Contact US on Modal pop up.
	 *
	 */
	public ShopPage clickContactUsBtnInModalpopup() {
		try {
			contactusOnPopup.click();
			Reporter.log("Contact Us button is clickable");
		} catch (Exception e) {
			Assert.fail("Contact Us button is not clickable");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible message for puerto rico customer
	 * 
	 * @return
	 */
	public ShopPage verifyAALIneligibleMessageForPuertoRicoCustomer() {
		try {
			waitFor(ExpectedConditions.visibilityOf(aalIneligibleMessageForPuertoRicoCustomer));
			Assert.assertTrue(aalIneligibleMessageForPuertoRicoCustomer.isDisplayed());
			Reporter.log("Authorable ineligibility message for puerto rico customer displayed");
		} catch (Exception e) {
			Assert.fail("Authorable ineligibility message for puerto rico customer not displayed");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL Return to shopping CTA for puerto customer
	 * 
	 * @return
	 */
	public ShopPage verifyReturnToShoppingCTAForPuertoRicoCustomer() {
		try {
			waitFor(ExpectedConditions.visibilityOf(returnToShoppingForPuertoCustomer));
			Assert.assertTrue(returnToShoppingForPuertoCustomer.isDisplayed());
			Reporter.log("Return to Shopping CTA for puerto rico customer displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display return to Shopping CTA for puerto rico customer ");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible for zero mbb lines
	 * 
	 * @return
	 */
	public ShopPage verifyAALIneligibleMessageForMBBLines() {
		try {
			waitFor(ExpectedConditions.visibilityOf(aalIneligibleMessageForMBBLines));
			aalIneligibleMessageForMBBLines.isDisplayed();
			Reporter.log("AAL ineligible for zero mbb lines is displayed");
		} catch (Exception e) {
			Assert.fail("AAL ineligible for zero mbb lines not displayed");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible message for user role not among pah
	 * 
	 * @return
	 */
	public ShopPage verifyAALIneligibleMessageForUserRoleNotAmongPAH() {
		try {
			aalIneligibleMessageForUserRoleNotAmongPAH.isDisplayed();
			Reporter.log("AAL ineligible message for user role not among pah is displayed");
		} catch (Exception e) {
			Assert.fail("AAL ineligible message for user role not among pah not displayed");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible message for suspended account
	 * 
	 * @return
	 */
	public ShopPage verifyAALIneligibleMessageForSuspendedAccount() {
		try {
			waitFor(ExpectedConditions.visibilityOf(aalIneligibleMessageForSuspendedAccount));
			Assert.assertTrue(aalIneligibleMessageForSuspendedAccount.isDisplayed());
			Reporter.log("Proper AAL ineligible message for suspended account is displayed");
		} catch (Exception e) {
			Assert.fail("Proper AAL ineligible message for suspended account not displayed");
		}
		return this;
	}

	/**
	 * Click on Catalog Promo Device
	 * 
	 * @return
	 */
	public ShopPage clickOnCatalogPromoDevice() {
		try {
			for (WebElement webElement : catalogPromoDevices) {
				clickElementWithJavaScript(webElement);
				break;
			}
		} catch (Exception e) {
			Assert.fail("Catalog Promo devices are not displayed");
		}
		return this;
	}

	/** Verify Today price for featured devices */

	public ShopPage verifyTodayPriceForFeaturedDevices() {
		try {
			waitforSpinner();
			for (WebElement verifyTodayPrice : featuredTodayPrice) {
				Assert.assertTrue(verifyTodayPrice.getText().contains("$"), "Today Price is displayed");
			}
			Reporter.log("Today price for devices is displayed for all devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify Today price for devices");
		}
		return this;
	}

	/**
	 * Verify AAL ineligibility Modal pop up title and text for Past Due
	 *
	 */
	public ShopPage verifyAALModalPopupTitleAndTextPastDue() {
		try {
			Assert.assertTrue(isElementDisplayed(modalPopupHeader), "Past Due title was not displayed.");
			Assert.assertTrue(modalPopupHeader.getText().contains("Past Due"), "Past Due title was not displayed.");
			Reporter.log("Past Due title is displayed.");

			Assert.assertTrue(isElementDisplayed(modalPopupDescription1),
					"Your account is past due. To add a line, please make a payment is not displayed.");
			Assert.assertTrue(modalPopupDescription1.getText()
					.contains("Your account is past due. To add a line, please make a payment"));
			Reporter.log("Your account is past due. To add a line, please make a payment is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to verify Title and Description on Modal pop up");
		}
		return this;
	}

	/**
	 * Verify footer links
	 */
	public ShopPage verifyFooterLinks() {
		waitforSpinner();
		try {
			moveToElement(supportLink);
			Assert.assertTrue(isElementDisplayed(supportLink), "Support link is not displayed.");
			Assert.assertTrue(isElementDisplayed(contactUsLink), "Contact us link is not displayed.");
			Assert.assertTrue(isElementDisplayed(storeLocatorLink), "Store Locator link is not displayed.");
			Assert.assertTrue(isElementDisplayed(coverageLink), "Coverage link is not displayed.");
			Assert.assertTrue(isElementDisplayed(tmobileLink), "TMobile link is not displayed.");
			Reporter.log("Verified Footer links");
		} catch (Exception e) {
			Assert.fail("Failed to verify Footer links");
		}
		return this;
	}

	/**
	 * Verify Manufacturer Name on Shop Page
	 */
	public ShopPage verifyAllDeviceManufacturerName() {

		waitforSpinner();
		checkPageIsReady();
		try {

			for (WebElement deviceName : featuredDevices) {
				String deviceNameText = deviceName.getText();
				Assert.assertTrue(
						deviceNameText.contains("Apple") || deviceNameText.contains("LG")
								|| deviceNameText.contains("Samsung") || deviceNameText.contains("T-Mobile®")
								|| deviceNameText.contains("Alcatel") || deviceNameText.contains("Motorola")
								|| deviceNameText.contains("OnePlus"),
						"Manufacturer name is not displayed for a Device");
				moveToElement(deviceName);
			}
			Reporter.log("Manufacturer name is displayed for devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify Manufacturer name for devices");
		}
		return this;
	}

	/**
	 * Verify Device Names for all Devices on Shop Page
	 */
	public ShopPage verifyAllDeviceName() {
		try {

			for (WebElement deviceName : featuredDevices) {
				Assert.assertTrue(deviceName.isDisplayed(), "Device name is not displayed for a Device");
				moveToElement(deviceName);
			}
			Reporter.log("Device name is displayed for devices");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device name for devices");
		}
		return this;
	}

	/**
	 * Verify Pending Rate Plan Model Header
	 * 
	 * @return
	 */
	public ShopPage verifyPendingRatePlanModelHeader() {
		try {
			waitforSpinner();
			Assert.assertTrue(pendingRatePlanModelHeader.isDisplayed(),
					"Pending Rate Plan Model header is not displayed");
			Reporter.log("Pending Rate Plan Model header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Pending RatePlan mode header");
		}
		return this;
	}

	/**
	 * Click on Shop now Button
	 *
	 */
	public ShopPage clickOnShopNowButton() {
		try {
			shopNowButton.click();
			Reporter.log("Shop Now button is clickable");
		} catch (Exception e) {
			Assert.fail("Shop Now button is not clickable");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL In Eligibility Message For MaxLines And MaxMBB
	 * 
	 * @return
	 */
	public ShopPage verifyAALInEligibilityMessageForMaxLinesAndMaxMBB() {
		try {
			Assert.assertTrue(aalIneligibleMessageForMaxVoiceLinesAndMaxMBB.isDisplayed());
			Assert.assertTrue(aalIneligibleMessageForMaxVoiceLinesAndMaxMBB.getText() != null,
					"The ineligibility message for max voice and Mbb lines is not displayed");
			Reporter.log("AAl Ineligibility Message for Max voice Lines and Max MBB is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display AAl Ineligibility Message for Max voice Lines and Max MBB is displayed");
		}
		return this;
	}

	/**
	 * Verify Offer Promo Modal Title
	 * 
	 * @return
	 */
	public String getUserName() {
		String name = null;
		try {
			if (getDriver() instanceof AppiumDriver) {
				hamburgerMenu.click();
				name = mobileUserName.getText();
				mobilecloseIcon.click();
			} else {
				waitforSpinner();
				moveToElement(userName);
				name = userName.getText();
			}
		} catch (Exception e) {
			Assert.fail("Failed to display Offer Promo Modal Text");
		}
		return name;
	}

}
