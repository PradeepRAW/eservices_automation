/**
 * 
 */
package com.tmobile.eservices.qa.pages.global;

import java.util.List;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class StoreLocatorPage extends CommonPage {

	public static final String storeLocatorPageUrl = "store-locator";
	public static final String storeLocatorPageLoadText = "Store Locator";
	private int timeout = 15;

	@FindBy(xpath = "//h2[contains(text(), 'Store Locator')]")
	private WebElement legacyStoreLocatorPage;

	@FindBy(css = "a[ng-repeat*='store in vm.model.storeList track by $index']")
	private List<WebElement> storesList;

	@FindBy(css = "span[ng-bind-html*='vm.storeDetailsAuthoring.getInLineLabel']")
	private WebElement getInLineLink;

	@FindBy(xpath = "//span[text()='Appointment']")
	private WebElement appointmentLink;

	@FindBy(css = "a[ng-bind-html*='vm.store.telephone']")
	private WebElement callStoreLink;

	/**
	 * 
	 * @param webDriver
	 */
	public StoreLocatorPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Legacy Support Page
	 * 
	 * @return
	 */
	public StoreLocatorPage verifyStoreLocatorPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(getDriver().getCurrentUrl().contains("store-locator"));
			verifyPageUrl(storeLocatorPageUrl);
			Reporter.log("Store Locator page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Store Locator page not displayed");
		}
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the StoreLocatorPage class instance.
	 */
	public StoreLocatorPage verifyPageLoaded() {
		checkPageIsReady();
		getDriver().getPageSource().contains(storeLocatorPageLoadText);
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the StoreLocatorPage class instance.
	 */
	public StoreLocatorPage verifyPageUrl() {
		checkPageIsReady();
		getDriver().getCurrentUrl().contains(storeLocatorPageUrl);
		return this;
	}

	/**
	 * select first store from stores list
	 */
	public StoreLocatorPage selectFirstStore() {
		try {
			storesList.get(0).click();
			Reporter.log("Selected first store on Store locator Page");
		} catch (Exception e) {
			Assert.fail("Stores are not available to click");
		}
		return this;
	}

	/**
	 * verify get in line link
	 */
	public StoreLocatorPage verifyGetInLineLink() {
		try {
			Assert.assertTrue(getInLineLink.isDisplayed(), "Get In Line link is not displayed");
			Reporter.log("Get In Line link is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Get in Line link");
		}
		return this;
	}

	/**
	 * verify appointment link
	 */
	public StoreLocatorPage verifyAppointmentLink() {
		try {
			Assert.assertTrue(appointmentLink.isDisplayed(), "Appointment Link is not displayed");
			Reporter.log("Appointment Link is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Appointment link");
		}
		return this;
	}

	/**
	 * verify Call Us link
	 */
	public StoreLocatorPage verifyCallUsLink() {
		try {
			Assert.assertTrue(callStoreLink.isDisplayed(), "Call US Link is not displayed");
			Reporter.log("Call US Link is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Call us link");
		}
		return this;
	}
}
