package com.tmobile.eservices.qa.accessibility.scanner;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ApplicationProperties {
	protected static final Logger logger = LoggerFactory.getLogger(ApplicationProperties.class);

	Properties properties;

	public ApplicationProperties(String fileName) throws IOException {
		this.properties = readFile(fileName);
	}

	public Properties readFile(String fileName) {
		Properties properties = new Properties();
		try {
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream(fileName);
			if (inputStream != null) {
				properties.load(inputStream);
			}
			inputStream.close();
		} catch (FileNotFoundException e) {
			logger.debug(e.getMessage());
		} catch (IOException e) {
			logger.debug(e.getMessage());
		}
		return properties;
	}

	public String getProperty(String property) {
		return this.properties.getProperty(property);
	}

}