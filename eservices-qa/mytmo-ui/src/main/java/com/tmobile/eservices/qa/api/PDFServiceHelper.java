package com.tmobile.eservices.qa.api;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tmobile.eservices.qa.api.unlockdata.CommonUtils;
import com.tmobile.eservices.qa.api.unlockdata.PDFService;
import com.tmobile.eservices.qa.api.unlockdata.PDFServiceImpl;
import com.tmobile.eservices.qa.api.unlockdata.ServiceException;

/**
 * This class handles to perform operation in pdfs
 * 
 * @author blakshminarayana
 *
 */
public class PDFServiceHelper {
	private static final Logger logger = LoggerFactory.getLogger(PDFServiceHelper.class);

	/**
	 * To save pdf in local
	 * 
	 * @param pdfURL
	 */
	public String savePDfInLocal(String pdfURL) {
		logger.info("savePDfInLocal method called in PDFServiceHelper");
		String localDirectory = CommonUtils.getPdfLocalDirectory();
		String fileName = FilenameUtils.getName(pdfURL);
		savePDF(fileName, localDirectory, pdfURL);
		return fileName;
	}

	/**
	 * * To save pdf in saucelab
	 * 
	 * @param pdfURL
	 * @param filename
	 */
	public String savePDfInSauceLab(String pdfURL) {
		logger.info("savePDfInSauceLab method called in PDFServiceHelper");
		String saucelab = CommonUtils.getSauceLabPDFDirectory();
		String fileName = FilenameUtils.getName(pdfURL);
		savePDF(fileName, saucelab, pdfURL);
		return fileName;
	}

	/**
	 * common method to save pdf files
	 * 
	 * @param filename
	 * @param localOrsaucelab
	 * @param pdfURL
	 */
	private void savePDF(String filename, String localOrsaucelab, String pdfURL) {
		StringBuilder fullPath = new StringBuilder();
		try {
			UrlValidator urlValidator = new UrlValidator();
			boolean valid = urlValidator.isValid(pdfURL);
			if (valid) {
				if (localOrsaucelab.contains(filename)) {
					FileUtils.forceDelete(new File(filename));
				}
				fullPath.append(localOrsaucelab).append("/").append(filename);
				PDFService pdfService = new PDFServiceImpl();
				pdfService.savePDF(pdfURL, fullPath.toString());
			}
		} catch (IOException ex) {
			logger.error("Exception occured while saving pdf file{}", ex);
			throw new ServiceException("Exception occured while saving pdf file{}", ex);
		}
	}

	/**
	 * to validate content input in pdf files
	 * 
	 * @param filename
	 * @param content
	 * @return boolean
	 */
	public boolean validateContent(String filename, String content) {
		String name = FilenameUtils.getName(filename);
		PDFService pdfService = new PDFServiceImpl();
		return pdfService.validateContentInPDF(name, content);
	}
}
