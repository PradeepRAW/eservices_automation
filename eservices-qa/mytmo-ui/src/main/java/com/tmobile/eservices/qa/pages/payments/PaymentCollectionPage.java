package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

public class PaymentCollectionPage extends CommonPage {

	private final String pageUrl = "/paymentcollection";

	@FindAll({ @FindBy(css = "div#back button"), @FindBy(css = "button.SecondaryCTA") })
	private WebElement pageLoadElement;

	@FindAll({@FindBy(xpath = "//span[contains(text(),'Select a payment method')]"), @FindBy(css = "div.Display4 span")})
	private WebElement pageHeader;

	@FindBy(xpath = "//span[contains(text(),'Add a card')]")
	private WebElement addCard;

	@FindBy(xpath = "//span[contains(text(),'Add a bank')]")
	private WebElement addBank;

	@FindBy(xpath = "//label[contains(@for,'card0')]")
	private WebElement radiobuttonStoredCard;

	@FindBy(xpath = "//label[contains(@for,'card')]//ancestor::div/div[contains(@class, 'text-right')]/p[@class='Display6']")
	private WebElement storedCardValue;

	@FindBy(xpath = "//label[contains(@for,'bank0')]")
	private WebElement radiobuttonStoredBank;

	@FindBy(xpath = "//label[contains(@for,'bank')]//ancestor::div/div[contains(@class, 'text-right')]/p[@class='Display6']")
	private WebElement storedBankValue;

	@FindBy(xpath = "//p[contains(text(),'later')]//../..//label")
	private WebElement choosePaymentMethodLaterRadioBtn;

	@FindBy(css = "button.PrimaryCTA")
	private WebElement selectPaymentMethodCTA;

	@FindBy(xpath = "//label[contains(@for,'card0')]/ancestor::div[contains(@class,'row padding-vertical-medium')]//span[text()='Delete']")
	private WebElement deletecard;
	
	@FindBy(xpath = "//label[contains(@for,'bank0')]/ancestor::div[contains(@class,'row padding-vertical-medium')]//span[text()='Delete']")
	private WebElement deletebank;
	
	@FindBy(xpath = "//label[contains(@for,'card')]")
	private List<WebElement> listradiobuttonStoredcard;
	
	@FindBy(xpath = "//label[contains(@for,'bank')]")
	private List<WebElement> listradiobuttonStoredbank;
	

	@FindBy(xpath = "//span[text()='Add a bank ']/ancestor::div[contains(@class,'cursor disableBlade')]")
	private List<WebElement> disabledbanks;
	
	@FindBy(xpath = "//button[text()='Cancel']")
	private WebElement buttonCancel;
	
	@FindBy(xpath = "//span[text()='In Session ']/ancestor::div[contains(@class,'padding-vertical-medium')]")
	private WebElement insessionPaymentrow;	

	@FindBy(xpath = "//span[text()='In Session ']/ancestor::div[contains(@class,'padding-vertical-medium')]//span[text()='Edit']")
	private WebElement insessionEditpaymentlink;	
	
	@FindBy(xpath = "//span[text()='In Session ']/ancestor::div[contains(@class,'padding-vertical-medium')]//label[contains(@for,'card')]")
	private WebElement insessionCardRadiobutton;	
	
	@FindBy(xpath = "//span[text()='In Session ']/ancestor::div[contains(@class,'padding-vertical-medium')]//label[contains(@for,'bank')]")
	private WebElement insessionBankRadiobutton;	
	
	@FindBy(xpath = "//span[text()='In Session ']/ancestor::div[contains(@class,'padding-vertical-medium')]//span[contains(@pid,'cust_crd')]")
	private WebElement inseeeionlastdigitsofpayment;	
	
	
	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public PaymentCollectionPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * 
	 * 
	 * /** Verify that current page URL matches the expected URL.
	 */
	public PaymentCollectionPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public PaymentCollectionPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			Reporter.log("Payment spoke page is loaded and header is verified ");
		} catch (Exception e) {

			Assert.fail("Failed to load payment spoke page");
		}
		return this;
	}

	/**
	 * verify page header is displayed
	 * 
	 * @return boolean
	 */
	public PaymentCollectionPage verifyPageHeader() {
		try {
			pageHeader.isDisplayed();
			Assert.assertTrue(pageHeader.getText().equals("Select a payment method"), "Page Header is not displayed");
			Reporter.log("Page Header is displayed");
		} catch (Exception e) {
			Assert.fail("failed to verify page header");
		}
		return this;
	}

	/**
	 * 
	 * click on add bank
	 */
	public PaymentCollectionPage clickAddBank() {
		try {
			addBank.click();
			Reporter.log("Clicked on add bank");
		} catch (Exception e) {
			Assert.fail("Add bank button not found");
		}
		return this;
	}

	/**
	 * 
	 * verify add bank blade is displayed or not
	 */
	public PaymentCollectionPage verifyAddBankBlade() {
		try {
			addBank.isDisplayed();
			Reporter.log("add a bank blade is diaplayed");
		} catch (Exception e) {
			Assert.fail("failed to verify add a bank blade");
		}
		return this;
	}

	/**
	 * 
	 * verify add card blade is displayed or not
	 */
	public PaymentCollectionPage verifyAddCardBlade() {
		try {
			addCard.isDisplayed();
			Reporter.log("add a card blade is diaplayed");
		} catch (Exception e) {
			Assert.fail("failed to verify add a card blade");
		}
		return this;
	}

	/**
	 * click on add card
	 */
	public PaymentCollectionPage clickAddCard() {
		try {
			addCard.click();
			Reporter.log("Add card button clicked");
		} catch (Exception e) {
			Assert.fail("Add card not found");
		}
		return this;
	}

	public PaymentCollectionPage clickStoredCardRadiobutton() {
		try {
			radiobuttonStoredCard.click();
			Reporter.log("card radiobutton clicked");
		} catch (Exception e) {
			Assert.fail("card radio button not found");
		}
		return this;
	}

	/**
	 * verify if the CTA is enabled or disabled
	 */
	public void verifySelectpaymentMethodCTAEnabledOrDisabled(boolean enabledOrNot) {
		try {
			Assert.assertEquals(selectPaymentMethodCTA.isEnabled(), enabledOrNot);
			Reporter.log("Verified Select payment method CTA");
		} catch (Exception e) {
			Assert.fail("Failed to verify Select payment method CTA");
		}
	}

	/**
	 * verify and select Choose payment method later Radio button
	 */
	public void clickChoosePaymentMethodLater() {
		try {
			choosePaymentMethodLaterRadioBtn.click();
			Reporter.log("'Choose payment method later' radio button is selected");
		} catch (Exception e) {
			Assert.fail("Failed to select 'Choose Payment Method Later' radio button");
		}
	}

	/**
	 * click Select payment method CTA
	 */
	public void clickSelectPaymentMethodCTA() {
		try {
			selectPaymentMethodCTA.click();
			Reporter.log("Clicked on 'Select Payment method' CTA");
		} catch (Exception e) {
			Assert.fail("Failed to click on 'Select Payment method' CTA");
		}
	}

	/**
	 * click radio button of stored bank
	 * 
	 * @return
	 */
	public String clickStoredBankRadiobutton() {
		try {
			radiobuttonStoredBank.click();
			Reporter.log("Clicked on saved bank account radio button");
		} catch (Exception e) {
			Assert.fail(
					"Failed to select Saved Bank account or there might not be any saved bank accounts on this MSISDN.");
		}
		return storedBankValue.getText();
	}

	/**
	 * check if there are any stored banks available
	 * 
	 * @return
	 */
	public boolean verifyAndSelectStoredPayment() {
		boolean isSelected = false;
		try {
			if (radiobuttonStoredBank.isDisplayed()) {
				radiobuttonStoredBank.click();
				isSelected = true;
			} else if (radiobuttonStoredCard.isDisplayed()) {
				radiobuttonStoredCard.click();
				isSelected = true;
			}
			Reporter.log("Selected Stored payment method");
			clickSelectPaymentMethodCTA();
		} catch (Exception e) {
			Reporter.log("There are no Stored Bank accounts on this MSISDN");
		}
		return isSelected;
	}

	
	public PaymentCollectionPage clickDeleteCard() {
		try {
			deletecard.click();
			Reporter.log("Clicked on Delete card link");
		} catch (Exception e) {
			Assert.fail("Failed to locate Delet card Link");
		}
		return this;
	}
	
	
	public PaymentCollectionPage clickDeleteBank() {
		try {
			deletebank.click();
			Reporter.log("Clicked on Delete Bank link");
		} catch (Exception e) {
			Assert.fail("Failed to locate Delet Bank Link");
		}
		return this;
	}
	
	public PaymentCollectionPage checkstoredcardsremoved() {
		try {
			if(listradiobuttonStoredcard.size()<=0) Reporter.log("Stored Card is deleted");
			else Assert.fail("Stored Card is not removed");
		} catch (Exception e) {
			Assert.fail("unable locate stored cards lables ");
		}
		return this;
	}
	
	public PaymentCollectionPage checkstoredbankremoved() {
		try {
			if(listradiobuttonStoredbank.size()<=0) Reporter.log("Stored Bank is deleted");
			else Assert.fail("Stored Bank is not removed");
		} catch (Exception e) {
			Assert.fail("unable locate stored bank lables ");
		}
		return this;
	}
	
	public PaymentCollectionPage CheckbankisDisabled() {
		try {
			if(disabledbanks.size()>0) Reporter.log("Bank is disabled");
			else Assert.fail("Bank is not disabled");
		} catch (Exception e) {
			Assert.fail("unable to locate disabled bank element ");
		}
		return this;
	}
	
	
	public PaymentCollectionPage CheckPCPcancelbutton() {
		try {
			buttonCancel.click();
			Reporter.log("Clicked on Cancel button");
		} catch (Exception e) {
			Assert.fail("Failed to locate Cancel button");
		}
		return this;
	}
	
	public PaymentCollectionPage CheckInsessionPaymentmethod() {
		try {
			if(insessionPaymentrow.isDisplayed())Reporter.log("InsessionPaymentmethod exists");
			else Verify.fail("InsessionPaymentmethod not xists");
			
		} catch (Exception e) {
			Assert.fail("Failed to locate insessionPaymentrow");
		}
		return this;
	}
	
	public PaymentCollectionPage CheckInsessionPaymentmethodEditLink() {
		try {
			if(insessionEditpaymentlink.isDisplayed())Reporter.log("InsessionPaymentmethod edit link exists");
			else Verify.fail("InsessionPaymentmethod edit link  not xists");
			
		} catch (Exception e) {
			Assert.fail("Failed to locate insessionEditpaymentlink");
		}
		return this;
	}
	
	public PaymentCollectionPage CheckInsessionPaymentmethodradiobutton() {
		try {
			if(insessionCardRadiobutton.isDisplayed())Reporter.log("InsessionPaymentmethod radio button exists");
			else Verify.fail("InsessionPaymentmethod radio button  not xists");
			
		} catch (Exception e) {
			Assert.fail("Failed to locate insessionCardRadiobutton");
		}
		return this;
	}
	
	public PaymentCollectionPage CheckbankInsessionPaymentmethodradiobutton() {
		try {
			if(insessionBankRadiobutton.isDisplayed())Reporter.log("InsessionPaymentmethod radio button exists");
			else Verify.fail("InsessionPaymentmethod radio button  not xists");
			
		} catch (Exception e) {
			Assert.fail("Failed to locate insessionBankRadiobutton");
		}
		return this;
	}
	
	
	
	public PaymentCollectionPage CheckInsessionPaymentmethodlastdigits(String card) {
		try {
			String last4digits=inseeeionlastdigitsofpayment.getText().trim();
			String lastFourDigits = card.substring(card.length() - 4);
			if(last4digits.contains(lastFourDigits))Reporter.log("Checked last 4 digits");
			else Assert.fail("Last 4 digits are not showing");
		
		} catch (Exception e) {
			Assert.fail("Failed to locate inseeeionlastdigitsofpayment");
		}
		return this;
	}
	
}