package com.tmobile.eservices.qa.pages.shop;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class LineSelectorPage extends CommonPage {

	public static final String LINE_SELECTION_URL = "lineselector";
	public static final String FIRSTLINE = "FirstLine";
	public static final String SECONDLINE = "SecondLine";
	public static final String JUMPLINE = "JumpLine";
	public static final String ADDALINE = "AddALine";

	@FindBy(css = "div#idShopLineSelector div.row.lineTabDivCls")
	private WebElement lineSelectorPage;

	@FindBys(@FindBy(css = "p[href='txt_NoPHPCartLink']"))
	private List<WebElement> upgradeLink;

	@FindBy(css = "i[class=\"fa fa-close uib-close p-15-xs-landScape\"]")
	private WebElement dismissPopup;

	@FindBys(@FindBy(css = "div[ng-click*='ctrl.onClickLineTabNavigation(line)'] p[ng-bind='line.UpgradeEligibleTxt']"))
	private List<WebElement> eligibilityCheck;

	//@FindBy(xpath = "//div[@ng-if='line.displayMsisdn']//p[contains(@class, 'legal') and not(contains(@class, 'name-hidden'))]")
	@FindBy(xpath = "//div[@ng-if='line.displayMsisdn']")
	private List<WebElement> lineToSelect;

	@FindBys(@FindBy(css = ".col-xs-12.col-sm-12.col-md-12.div-box-shadow.p-t-20.no-padding.lineselector-height.overflow-hidden.disable-image"))
	private List<WebElement> greyedLineToSelect;

	@FindBy(css = "button[ng-click*='ctrl.onClickWarningModalContinueCTA'] span[ng-bind*='ctrl.contentData.primaryCTAText']")
	private List<WebElement> continueStopUpgrade;

	@FindBys(@FindBy(css = "a.jump-logo"))
	private List<WebElement> jumpUpgradeLink;

	@FindBys(@FindBy(css = "p.text-bold.ng-binding"))
	private List<WebElement> nickName;

	@FindBys(@FindBy(css = "div.display-table-cell.vertical-align-middle.small.link-alignment p"))
	private List<WebElement> ineligibilityText;

	// @FindBy(css="div[ng-repeat='line in $ctrl.lineDetailsArr']")
	//@FindBys(@FindBy(css = "div[ng-repeat='line in $ctrl.lineDetails']"))
	@FindBy(css = "div[ng-repeat*='ctrl.accountInfo.lines']")
	private List<WebElement> lines;

	@FindBy(css = "div.text-center.header-box-shadow.ng-scope")
	private WebElement sedonaHeader;

	@FindBy(css = "span[ng-bind-html*='$ctrl.objUserEligibility.inEligibleMessage']")
	private WebElement ineligibilityTxt;

	@FindBy(css = "div button[ng-click*='$ctrl.jumpModularityUtil.closeJumpTransactionFailedModal();']")
	private WebElement jumpCatalogWarningModal;


	@FindBy(xpath = "//span[contains(text(), 'Add a line')]")
	private WebElement addALinkLink;

	@FindBy(css = "div.modal-content.modal-background div.row.no-margin")
	private WebElement pastDueModalContainer;

	@FindBy(css = "div#idpastDueDelinquentModal h2.h2-title.text-black")
	private WebElement pastDueModalTitle;

	@FindBy(css = "div#idpastDueDelinquentModal p.body-copy-description")
	private WebElement pastDueModalDescription;

	@FindBy(css = "div#idpastDueDelinquentModal button.btn.glueButton.glueButtonPrimary")
	private WebElement pastDueModalMakePaymentCTA;

	@FindBy(css = "button.close.pull-right.uib-close i.fa.fa-close.uib-close.p-15-xs-landScape")
	private WebElement pastDueModalCloseCTA;

	@FindBy(css = "i.fa.fa-spin.fa-spinner.interceptor-spinner")
	private WebElement pageSpinner;

	@FindBy(css = ".fine-print-body.text-red.ng-binding")
	private WebElement redMessage;

	@FindBy(css = "div.col-xs-12.critical-icon-div.wrapper")
	private WebElement stickyBannerWarningImageIcon;

	@FindBy(css = ".notificationClose.pdp-sticky-display-flex span.break-xs")
	private WebElement stickyBannerHeader;

	@FindBy(css = ".notificationClose.pdp-sticky-display-flex span.body-copy-custom")
	private WebElement stickyBannerBody;

	@FindBy(css = "i#ico")
	private WebElement stickyBannerNextIcon;

	@FindBy(css = "p.body-copy-description strong.ng-binding")
	private WebElement pastDueAmount;

	@FindBy(css = ".body-copy-caption.text-red")
	private WebElement suspendedMessage;

	@FindBy(xpath = "//a[contains(text(),'Pay Now')]")
	private List<WebElement> payNowLinks;

	@FindBy(css = "jumpInEligibleMessage")
	private WebElement jumpInEligibleMessage;

	@FindBy(css = "p[ng-bind='$ctrl.aalWarningModalContent.header']")
	private WebElement modalPopupDescription;

	@FindBy(css = "p[ng-bind='$ctrl.aalWarningModalContent.message']")
	private WebElement modalPopupDescription1;

	@FindBy(css = "span[class='add-new-line ng-binding']")
	private WebElement aalCTA;

	@FindBy(linkText = "Contact Us")
	private WebElement contactUs;

	@FindBy(css = "h2[ng-bind='$ctrl.aalWarningModalContent.title']")
	private WebElement letsTalkModel;

	@FindBy(css = "a[ng-bind = '$ctrl.aalWarningModalContent.ctaLabel']")
	private WebElement contactusOnPopup;

	@FindBy(css = "h2[ng-bind='$ctrl.aalWarningModalContent.title']")
	private WebElement modalPopup;

	@FindBy(css = "button[class='TeritiaryCTA margin-left-small no-padding-lr text-theme']")
	private WebElement aalIneligibleMessageForPuertoRicoCustomer;

	@FindBy(css = "button[class='TeritiaryCTA margin-left-small no-padding-lr text-theme']")
	private WebElement aalIneligibleMessageForMBBLines;

	@FindBy(xpath = "//div[contains(@ng-if,'isUserInEligibleToUpgrade')]")
	private WebElement aalIneligibleMessageForDelinquentAcc;

	@FindBy(css = "button[class='TeritiaryCTA margin-left-small no-padding-lr text-theme']")
	private WebElement aalIneligibleMessageForAALServiceDown;

	@FindBy(xpath = "//div[contains(@ng-if,'isUserInEligibleToUpgrade')]")
	private WebElement aalIneligibleMessageForAccountTenureDaysLessThan60;

	@FindBy(css = "//div[contains(@ng-if,'isUserInEligibleToUpgrade')]")
	private WebElement aalIneligibleMessageForSuspendedAccount;

	@FindBy(css = "button[class='TeritiaryCTA margin-left-small no-padding-lr text-theme']")
	private WebElement aalIneligibleMessageForUserRoleNotAmongPAH;

	@FindBy(css = "button[class='TeritiaryCTA margin-left-small no-padding-lr text-theme']")
	private WebElement aalIneligibleMessageForBankRuptcyAccount;
	
	@FindBy(xpath = "//button[contains(@ng-bind,'.storeLocatorCTATxt')]")
	private List<WebElement> findStore;
	
	@FindBy(css = "i.backArrowLarge")
	private WebElement backArrow;
	
	@FindBy(css = "#object-8 div div.no-padding.col-xs-8")
	private WebElement tradeInLine;

	@FindBy(css = "#lineSelector-title p")
	private WebElement headerLine;

	@FindBy(css = "[ng-repeat*='lines'] img")
	private List<WebElement> deviceImages;

	@FindBy(css = "span[ng-bind*='customerName']")
	private List<WebElement> lineNameText;

	@FindBy(css = "p[pid*='cust_msisdn']")
	private List<WebElement> lineNumber;

	@FindBy(css = "p[pid*='cust_name']")
	private List<WebElement> deviceName;

	@FindBy(css = "[ng-click='$ctrl.onClickAddALineLink()']")
	private WebElement addaLinelink;

	@FindBy(css = "span[class*='disable-links']")
	private WebElement disabledAddaLineLink;
	
	@FindBy(css = "div.modal-content.modal-background div.row.no-margin")
	private WebElement noLinesToUpgradeErrorModalContainer;

	@FindBy(css = "i[ng-click*='close']")
	private WebElement noLinesToUpgradeErrorModalCloseCTA;
	
	@FindBy(css = "i[ng-click*='close']")
	private WebElement jumpLine;
		
	@FindBy(css = "i[ng-click*='close']")
	private WebElement jumpIneligibelityMessage;		
	
	@FindBy(css = "inEligiblityMessage")
	private WebElement inEligiblityMessage;

	public LineSelectorPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Line Selector Page
	 * 
	 * @return
	 */
	public LineSelectorPage verifyLineSelectorPage() {
		waitforSpinner();
		try {
			verifyPageUrl();
			Reporter.log(Constants.LINE_SELECTOR_PAGE);
		} catch (Exception e) {
			Assert.fail("Line Selector Page is Not Loaded");
		}
		return this;

	}
	
	/**
	 * Verify Line Selector Page URL
	 * 
	 * @return
	 */
	public LineSelectorPage waitForLineSelectorPage() {
		checkPageIsReady();
	
		try {
			waitFor(ExpectedConditions.urlContains("lineselector"));
		} catch (Exception e) {
			Assert.fail("Line selector URL did not appeared");
		}
		return this;

	}
	
	

	/**
	 * Verify Line Selector Page URL
	 * 
	 * @return
	 */
	public LineSelectorPage verifyPageUrl() {
		checkPageIsReady();
		waitFor(ExpectedConditions.urlContains(LINE_SELECTION_URL));
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains(LINE_SELECTION_URL),"Verified Line Selector page");
			Reporter.log("Verified Line Selector page");

			// Do not change this URL. It is for regression(E3 & Prod). If required
			// in lower env please use different variable or method.
		}catch (Exception e){
			Assert.fail("Line Selection Page is not displayed");
		}
		return this;
	}

	/**
	 * Click Device
	 */
	public LineSelectorPage clickDevice() {
		checkPageIsReady();
		waitforSpinner();
		try {
			for (int i = 0; i < lineToSelect.size(); i++) {
				clickElement(lineToSelect.get(i));
				if (findStore.size() == 1) {
					clickElement(backArrow);
					Reporter.log("Redirected back to Line Selection Page");
					waitforSpinner();
				} else {
					break;
				}
			}
			Reporter.log("Clicked on a eligible line in line selector page");
		} catch (Exception e) {
			Assert.fail("Eligible lines are not present");
		}
		return this;
	}

	/**
	 * Click MBB Device
	 */
	public LineSelectorPage clickMBBDevice() {
		checkPageIsReady();
		waitforSpinner();
		try {
			for (int i = 0; i < lineToSelect.size(); i++) {
				clickElementWithJavaScript(lineToSelect.get(i));
				if (findStore.size() == 1) {
					Reporter.log("Redirected back to Line Selection Page");
					waitforSpinner();
				} else {
					break;
				}
			}
			Reporter.log("Clicked on a MBB line in line selector page");
		} catch (Exception e) {
			Assert.fail("MBB lines are not available");
		}
		return this;
	}
	
	/**
	 * Click UnKnown Device
	 */
	public LineSelectorPage selectUnKnownDevice() {
		waitforSpinner();
		checkPageIsReady();
		try {
			for (int i = 0; i < lineToSelect.size(); i++) {
					clickElementWithJavaScript(lineToSelect.get(i));
					if (findStore.size() == 1) {
						backArrow.click();
						Reporter.log("Redirected back to Line Selection Page");
						waitforSpinner();
					} else {
						break;
					}
				}
				Reporter.log("Clicked on 1st UnKnown Device on Line Selector Page");
			} catch (Exception e) {
				Assert.fail("UnKnown Device not displayed in Line Selector Page");
			}
			return this;				
	}

	/**
	 * Click Upgrade Link
	 */
	public LineSelectorPage clickJumpUpgradeLink() {
		try {
			for (WebElement webElement : jumpUpgradeLink) {
				clickElementWithJavaScript(webElement);
				Reporter.log("Clicked on Jump link on Line Selector Page");
				break;
			}
		} catch (Exception e) {
			Assert.fail(
					" Jump Device is not clickable or Failed due to not able to Proceed to Phone Selection after clicking the Device");
		}
		return this;
	}


	/**
	 * Click SecondLine
	 */
	public LineSelectorPage clickSecondLine() {
		try {
			getDriver().manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			waitforSpinner();
			for (WebElement webElement : lineToSelect) {
				clickElementWithJavaScript(webElement);
				Reporter.log("Clicked on Second Device on Line Selector Page");
				break;
			}
			waitforSpinner();
			getDriver().manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			if (continueStopUpgrade.size() > 0 && !continueStopUpgrade.isEmpty()) {
				clickElementWithJavaScript(continueStopUpgrade.get(1));
				Reporter.log("Clicked on Second Device on Line Selector Page");
			}
		} catch (Exception e) {
			Assert.fail(
					"Second Device is not clickable or Failed due to not able to Proceed to Phone Selection after clicking the Device");
		}
		return this;

	}

	/**
	 * Verify Puerto rico customer eligibility mesasge is displayed
	 */
	public LineSelectorPage verifyIneligibilityTxt() {
		try {
			Assert.assertTrue(ineligibilityTxt.isDisplayed());
			Reporter.log("Puerto rico customer eligibility mesasge is displayed");
		} catch (Exception e) {
			Assert.fail("Puerto rico customer eligibility mesasge is not displayed");
		}
		return this;
	}

	/**
	 * Verify Jump Catalog Warning Modal
	 */
	public LineSelectorPage verifyJUMPCatalogWarningModal() {
		try {
			Assert.assertTrue(jumpCatalogWarningModal.isDisplayed());
			Reporter.log("Jump catalog warning modal is displayed");
		} catch (Exception e) {
			Assert.fail("Jump Catalog warning modal is not displayed");
		}
		return this;
	}

	/**
	 * Click Jump Catalog Warning Modal
	 */
	public LineSelectorPage clickJUMPCatalogWarningModal() {
		try {
			jumpCatalogWarningModal.click();
		} catch (Exception e) {
			Assert.fail("Jump Catalog warning modal is not dispalyed or Not able to navigate to ");
		}
		return this;
	}


	/**
	 * Click On First Line
	 */
	public LineSelectorPage clickOnFirstLine() {
		waitforSpinner();
		try {
			Assert.assertTrue(lineToSelect.get(0).isDisplayed(), "First Line is not displayed");
			clickElementWithJavaScript(lineToSelect.get(0));
		} catch (Exception e) {
			Assert.fail("Failed to click on the first device ");
		}
		return this;

	}

	/**
	 * Verify Display of Modal for past due customer and delinquent
	 */
	public LineSelectorPage verifyModalPastDueCustomer() {
		try {
			waitFor(ExpectedConditions.visibilityOf(pastDueModalContainer), 10);
			Assert.assertTrue(pastDueModalContainer.isDisplayed(), "Modal for past due customer is not displayed");
			Reporter.log("Modal for past due customer is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Modal for past due customer");
		}
		return this;
	}

	/**
	 * Verify Display of Modal Title for past due customer and delinquent
	 */
	public LineSelectorPage verifyModalPastDueTitleText() {
		String title;
		try {
			title = pastDueModalTitle.getText();
			Assert.assertEquals(title, "Your account is past due!!!", "Past due Modal Title is not displayed correctly");
			Reporter.log("Past due Modal Title is displayed correctly");
		} catch (Exception e) {
			Assert.fail("Failed to display Past Due Modal title");
		}
		return this;
	}

	/**
	 * Verify Display of Modal Description for past due customer and delinquent
	 */
	public LineSelectorPage verifyModalPastDueDescriptionText() {
		try {
			Assert.assertTrue(pastDueModalDescription.getText().contains("This account is not eligible for upgrade"),
					"Past due Modal description text is not displayed correctly");
			Reporter.log("Past due Modal description text is displayed correctly");
		} catch (Exception e) {
			Assert.fail("Failed to display Past due Modal description text");
		}
		return this;
	}

	/**
	 * Verify Display Make A Payment CTA of Modal for past due customer and
	 * delinquent
	 */
	public LineSelectorPage verifyModalPastDueMakePaymentCTA() {
		try {
			Assert.assertTrue(pastDueModalMakePaymentCTA.isDisplayed(),
					"Make a payment CTA for Modal of past due customer is not displayed");
			Reporter.log("Make a payment CTA for Modal of past due customer is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Make a payment CTA for Modal of past due customer");
		}
		return this;
	}

	/**
	 * Click Close CTA of Past Due Modal
	 */
	public LineSelectorPage clickCloseCTAPastDueModal() {
		try {
			clickElementWithJavaScript(pastDueModalCloseCTA);
			Reporter.log("Modal Close button is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Modal Close button");
		}
		return this;
	}

	/**
	 * Verify Display of Modal for past due customer and delinquent
	 */
	public LineSelectorPage verifyPastDueModalNotVisible() {
		try {
			waitFor(ExpectedConditions.visibilityOfAllElements(lineToSelect));
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(pastDueModalContainer), "Modal for past due customer is displayed");
			Reporter.log("Modal for past due customers is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to hide Modal for past due customer");
		}
		return this;
	}

	/**
	 * Verify that the Line Selection page is grayed out
	 */

	public LineSelectorPage verifyLineSelectorPageGrayedOut() {
		try {
			for (WebElement webElement : greyedLineToSelect) {
				Assert.assertTrue(isElementDisplayed(webElement),
						"Line Selector page does not have grayed out elements");
			}
			Reporter.log("Line Selector page is grayed out");
		} catch (Exception e) {
			Assert.fail("Failed to verify Line Selector page as grayed out");
		}
		return this;
	}

	/**
	 * Verify Sticky Banner Warning Icon
	 */
	public LineSelectorPage verifyStickyBannerWarningImageIcon() {
		try {
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(stickyBannerWarningImageIcon));
			Assert.assertTrue(stickyBannerWarningImageIcon.isDisplayed(),
					"Sticky banner Warning Icon is not displayed");
			Reporter.log("Sticky banner Warning Icon is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Sticky banner Warning Icon");
		}
		return this;
	}

	/**
	 * Verify Sticky Banner Header
	 */
	public LineSelectorPage verifyStickBannerHeader() {
		try {
			waitforSpinner();
			Assert.assertTrue(stickyBannerHeader.isDisplayed(), "Sticky header is not displayed");
			Reporter.log("Sticky banner header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Sticky banner Header");
		}
		return this;
	}

	/**
	 * Verify Sticky Banner Body
	 */
	public LineSelectorPage verifyStickBannerBody() {
		try {
			waitforSpinner();
			Assert.assertTrue(stickyBannerBody.isDisplayed(), "Sticky banner Body is not displayed");
			Reporter.log("Sticky banner Body is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Sticky banner Body");
		}
		return this;
	}

	/**
	 * Verify Sticky Banner next Icon
	 */
	public LineSelectorPage verifyStickBannerNextIcon() {
		try {
			waitforSpinner();
			Assert.assertTrue(stickyBannerNextIcon.isDisplayed(), "Sticky banner next icon is not displayed");
			Reporter.log("Sticky banner next icon is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Sticky banner next icon");
		}
		return this;
	}

	/**
	 * Click Sticky Banner Next ICON
	 * @return
	 */
	public LineSelectorPage clickStickBannerNextIcon() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(stickyBannerNextIcon);
			Reporter.log("Sticky Banner Next icon is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Sticky Banner Next icon");
		}
		return this;
	}

	/**
	 * Verify Display of Modal for past due customer and delinquent
	 */
	public LineSelectorPage verifyStickyBannerPopUpNotVisible() {
		try {
			Assert.assertFalse(isElementDisplayed(stickyBannerHeader), "Sticky Banner Pop Up is displayed");
			Reporter.log("Sticky Banner Pop Up is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Sticky Banner Pop Up not displayed");
		}
		return this;
	}

	/**
	 * Verify Display of Modal Description for suspended customer
	 */
	public LineSelectorPage verifySuspendedCustomerMessageText() {
		try {
			String suspendedMessageText = suspendedMessage.getText();
			Assert.assertTrue(suspendedMessage.isDisplayed(),
					"Suspended customer message is not present in Past due Modal");
			Assert.assertTrue(
					suspendedMessageText
							.equals("Your account has been temporarily suspended. Please call 1-800-937-8997."),
					"Suspended customer message text not verified");
			Reporter.log("Suspended customer message is displayed and verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Suspended customer message text");
		}
		return this;
	}

	/**
	 * Click on Paynow Link in Line Selector Page(If First Link Appears , it
	 * will click first link and skips for next Loop)
	 */
	public LineSelectorPage clickonPaynowLink() {
		try {
			for (int linkCount = 0; linkCount <= payNowLinks.size() - 1;) {
				if (payNowLinks.get(linkCount).isDisplayed())
					payNowLinks.get(linkCount).click();
				break;
			}

			Reporter.log("Pay Now Link is displayed and Clicked successfully");
		} catch (Exception e) {
			Assert.fail("Failed to verify Pay Now link");
		}
		return this;
	}

	/**
	 * Dismiss Pop up
	 */
	public LineSelectorPage dismissPopup() {
		try {
			Thread.sleep(10000);
			checkPageIsReady();
			dismissPopup.click();
			Reporter.log("Pop up was dismissed");
		} catch (Exception e) {
			Assert.fail("Popup was not dismissed");
		}
		return this;

	}

	/**
	 * Verify Jump InEligibility message
	 */
	public LineSelectorPage verifyJumpInEligiblityMessage() {
		try {
			Assert.assertTrue(jumpInEligibleMessage.isDisplayed());
			Reporter.log("Jump InEligible message is displayed correctly");
		} catch (Exception e) {
			Assert.fail("Jump InEligible message not displayed");
		}
		return this;

	}

	/**
	 * Click on AAL CTA
	 * 
	 * @return
	 */
	public LineSelectorPage clickOnAALCTA() {
		try {
			checkPageIsReady();
			waitforSpinner();
			waitFor(ExpectedConditions.visibilityOf(aalCTA), 10);
			clickElementWithJavaScript(aalCTA);
			Reporter.log("Clicked on AAl CTA");
		} catch (Exception e) {
			Assert.fail("AAl CTA not clickable");
		}
		return this;
	}

	/**
	 * Verify Lets Talk Model
	 * 
	 * @return
	 */
	public LineSelectorPage verifyLetsTalkModel() {
		try {
			waitFor(ExpectedConditions.visibilityOf(letsTalkModel), 10);
			Assert.assertTrue(letsTalkModel.isDisplayed());
			Reporter.log("Lets talk model is displayed");
		} catch (Exception e) {
			Assert.fail("Lets talk modal not displayed");
		}
		return this;

	}

	/**
	 * Verify Lets Talk Model Warning Message
	 * 
	 * @return
	 */
	public LineSelectorPage verifyLetsTalkModelWarningMessage() {
		try {
			Assert.assertEquals(modalPopupDescription.getText(),
					"Sorry - we're not able to add a line to your account online.");
			Assert.assertEquals(modalPopupDescription1.getText(),
					"To add a line, please call Customer Care at 1-800-937-8997, or by dialing 611 from your T-Mobile phone.");
			Reporter.log("modal text is displayed");
		} catch (Exception e) {
			Assert.fail("modal text not displayed");
		}
		return this;

	}

	public LineSelectorPage clickOnContactUsCTA() {
		try {
			contactUs.click();
			Reporter.log("Clicked on contact us CTA");
		} catch (Exception e) {
			Assert.fail("contact us CTA not displayed");
		}
		return this;

	}

	/**
	 * Verify Modal popup
	 *
	 */
	public LineSelectorPage verifyAALModalPopup() {
		try {
			waitFor(ExpectedConditions.visibilityOf(modalPopup));
			Assert.assertTrue(isElementDisplayed(modalPopup), "Modal Pop up is not displayed. ");
			Reporter.log("Modal Pop up is displayed.");
		} catch (Exception e) {
			Assert.fail("Failed to verify modal pop up");
		}
		return this;
	}

	/**
	 * Verify Modal pop up title and text
	 *
	 */
	public LineSelectorPage verifyAALModalPopupTitleAndText() {

		try {
			Assert.assertTrue(isElementDisplayed(letsTalkModel), "Let's talk title was not displayed.");
			Assert.assertTrue(letsTalkModel.getText().contains("Let's talk"), "Let's talk title was not displayed.");
			Reporter.log("Let's talk title is displayed.");
			Assert.assertTrue(isElementDisplayed(modalPopupDescription),
					" Sorry - we're not able to add a line was not displayed ");
			Assert.assertTrue(modalPopupDescription.getText().contains("not able to add a line to your account"),
					" Sorry - we're not able to add a line was not displayed.");
			Reporter.log("Sorry - we're not able to add a line to your account online is displayed.");
			Assert.assertTrue(isElementDisplayed(modalPopupDescription1),
					"To add a line, please call Customer Care at 1-800-937-8997, or by dialing 611 from your T-Mobile phone is not displayed.");
			Assert.assertTrue(modalPopupDescription1.getText().contains("please call Customer Care"));
			Reporter.log(
					"To add a line, please call Customer Care at 1-800-937-8997, or by dialing 611 from your T-Mobile phone is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to verify Title and Description on Modal pop up");
		}
		return this;
	}

	/**
	 * Verify Contact US on Modal pop up.
	 *
	 */
	public LineSelectorPage verifyContactusOnModalpopup() {
		try {
			waitFor(ExpectedConditions.visibilityOf(contactusOnPopup));
			Assert.assertTrue(isElementDisplayed(contactusOnPopup), "Contact US displayed on Modal pop up. ");
			Reporter.log("Contact Us is displayed on Modal Pop up.");
		} catch (Exception e) {
			Assert.fail("Faile to verify Contact Us on modal pop up");
		}
		return this;
	}

	/**
	 * Verify Contact US on Modal pop up.
	 *
	 */
	public LineSelectorPage clickContactUsBtnInModalpopup() {
		try {
			contactusOnPopup.click();
			Reporter.log("Contact Us button is clickable");
		} catch (Exception e) {
			Assert.fail("Contact Us button is not clickable");
		}

		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible message for puerto rico customer
	 * 
	 * @return
	 */
	public LineSelectorPage verifyAALIneligibleMessageForPuertoRicoCustomer() {
		try {
			aalIneligibleMessageForPuertoRicoCustomer.isDisplayed();
			Reporter.log("Authorable ineligibility message for puerto rico customer displayed");
		} catch (Exception e) {
			Assert.fail("Authorable ineligibility message for puerto rico customer not displayed");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible for zero mbb lines
	 * 
	 * @return
	 */
	public LineSelectorPage verifyAALIneligibleMessageForMBBLines() {
		try {
			aalIneligibleMessageForMBBLines.isDisplayed();
			Reporter.log("AAL ineligible for zero mbb lines is displayed");
		} catch (Exception e) {
			Assert.fail("AAL ineligible for zero mbb lines not displayed");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible message for user role not among pah
	 * 
	 * @return
	 */
	public LineSelectorPage verifyAALIneligibleMessageForUserRoleNotAmongPAH() {
		try {
			aalIneligibleMessageForUserRoleNotAmongPAH.isDisplayed();
			Reporter.log("AAL ineligible message for user role not among pah is displayed");
		} catch (Exception e) {
			Assert.fail("AAL ineligible message for user role not among pah not displayed");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible message for bankruptcy account
	 * 
	 * @return
	 */
	public LineSelectorPage verifyAALIneligibleMessageForBankRuptcyAccount() {
		try {
			aalIneligibleMessageForBankRuptcyAccount.isDisplayed();
			Reporter.log("AAL ineligible message for bankruptcy account is displayed");
		} catch (Exception e) {
			Assert.fail("AAL ineligible message for bankruptcy account not displayed");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible message for suspended account
	 * 
	 * @return
	 */
	public LineSelectorPage verifyAALIneligibleMessageForSuspendedAccount() {
		try {
			aalIneligibleMessageForSuspendedAccount.isDisplayed();
			Reporter.log("AAL ineligible message for suspended account is displayed");
		} catch (Exception e) {
			Assert.fail("AAL ineligible message for suspended account not displayed");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible message for account tenure days less than 60
	 * 
	 * @return
	 */
	public LineSelectorPage verifyAALIneligibleMessageForAccountTenureDaysLessThan60() {
		try {
			aalIneligibleMessageForAccountTenureDaysLessThan60.isDisplayed();
			Reporter.log("AAL ineligible message for account tenure days less than 60 is displayed");
		} catch (Exception e) {
			Assert.fail("AAL ineligible message for account tenure days less than 60 not displayed");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible message for delinquent account down
	 * 
	 * @return
	 */
	public LineSelectorPage verifyAALIneligibleMessageForDelinquentAcc() {
		try {
			aalIneligibleMessageForDelinquentAcc.isDisplayed();
			Reporter.log("AAL ineligible message for delinquent account down is displayed");
		} catch (Exception e) {
			Assert.fail("AAL ineligible message for delinquent account down not displayed");
		}
		return this;
	}

	/**
	 * 
	 * Verify AAL ineligible message for AAL service down
	 * 
	 * @return
	 */
	public LineSelectorPage verifyAALIneligibleMessageForAALServiceDown() {
		try {
			aalIneligibleMessageForAALServiceDown.isDisplayed();
			Reporter.log("AAL ineligible message for AAL service down is displayed");
		} catch (Exception e) {
			Assert.fail("AAL ineligible message for AAL service down not displayed");
		}
		return this;
	}

	/**
	 * Verify header in line selection page
	 */
	public LineSelectorPage verifyHeaderText() {
		try {
			String headerText = headerLine.getText();
			Assert.assertTrue(headerLine.isDisplayed(), "Header is not present in Line selector page to verify");
			Assert.assertTrue(headerText.contains("Who's getting this cool new phone?"), "Header text is not verified");
			Reporter.log("Header text is displayed and verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify Header text");
		}
		return this;
	}

	/**
	 * verify Device image in Line selection page
	 */
	public LineSelectorPage verifyDeviceImageDisplay() {
		try {
			for (WebElement deviceImage : deviceImages) {
				Assert.assertTrue(deviceImage.isDisplayed(),
						"Device Image is not present in Line selector page to verify");
			}
			Reporter.log("Device Images are displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Images");
		}
		return this;
	}

	/**
	 * Verify Device Line Name in Line selection page
	 */
	public LineSelectorPage verifyDeviceLineNameDisplay() {
		try {
			for (WebElement lineName : lineNameText) {
				Assert.assertTrue(lineName.isDisplayed(),
						"Device line name is not present in Line selector page to verify");
			}
			Reporter.log("Device line names are displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device line names");
		}
		return this;
	}

	/**
	 * Verify Device Line Number in Line selection page
	 */
	public LineSelectorPage verifyDeviceLineNumberDisplay() {
		try {
			for (WebElement number : lineNumber) {
				Assert.assertTrue(number.isDisplayed(),
						"Device line number is not present in Line selector page to verify");
			}
			Reporter.log("Device line numbers are displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device line number");
		}
		return this;
	}

	/**
	 * Verify Device name in Line selection page
	 */
	public LineSelectorPage verifyDeviceNameDisplay() {
		try {
			for (WebElement name : deviceName) {
				Assert.assertTrue(name.isDisplayed(), "Device name is not present in Line selector page to verify");
			}
			Reporter.log("Device Names are displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Name");
		}
		return this;
	}

	/**
	 * Verify device information for all lines
	 * 
	 */
	public LineSelectorPage verifyDeviceInformation() {
		try {
			verifyDeviceImageDisplay();
			verifyDeviceLineNameDisplay();
			verifyDeviceLineNumberDisplay();
			verifyDeviceNameDisplay();// this method is failing due to
										// application issue
			Reporter.log("All Device Information is displayed for all lines");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Information");
		}
		return this;
	}

	/**
	 * Verify lines in Line selection page
	 */
	public LineSelectorPage verifyAllLinesClickable() {
		try {
			LineSelectorDetailsPage newLineSelectorDetailsPage = new LineSelectorDetailsPage(getDriver());
			for (WebElement webElement : lines) {
				clickElementWithJavaScript(webElement);
				Reporter.log("Clicked on First Device on Line Selector Page");
				waitforSpinner();
				newLineSelectorDetailsPage.verifyPageUrl();
				newLineSelectorDetailsPage.verifyLineSelectionDetailsPage();
				Reporter.log("Line selector details page is displayed");
				newLineSelectorDetailsPage.clickOnBackLink();
				LineSelectorPage LineSelectorPage = new LineSelectorPage(getDriver());
				LineSelectorPage.verifyLineSelectorPage();
			}
			waitforSpinner();
		} catch (Exception e) {
			Assert.fail("Failed to check lines in Line selector page");
		}
		return this;
	}

	/**
	 * 
	 * verify Add a line link is present
	 */
	public LineSelectorPage verifyAddaLineLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(addaLinelink));
			Assert.assertTrue(addaLinelink.isDisplayed(), "Add a Line link is not displayed");
			Reporter.log("Add a new line link is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Add a new line link");
		}
		return this;
	}

	/**
	 * click on Add a line link
	 */
	public LineSelectorPage clickAddaLineLink() {
		waitforSpinner();
		try {
			Assert.assertTrue(addaLinelink.isDisplayed(), "Add a Line link is not displayed");
			clickElement(addaLinelink);
			Reporter.log("Add a new line link is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Add a line link");
		}
		return this;
	}

	/**
	 * verify Add a line link is disabled
	 * 
	 * @return
	 */
	public LineSelectorPage verifyAddaLineLinkisDisabled() {
		try {
			if (isElementDisplayed(addaLinelink)) {
				Assert.fail("'Add a new line' link is displayed");
			} else if(isElementDisplayed(disabledAddaLineLink)) {
				Reporter.log("'Add a new line' link is disabled");
			} else {
				Reporter.log("'Add a new line' link is not present");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Add a new line' link");
		}
		return this;
	}

	/**
	 * click on Given line
	 */
	public LineSelectorPage clickOnGivenLine(String line) {
		checkPageIsReady();
		waitforSpinner();
		try {
			if(line == null) {
				lines.get(0).click();
				Reporter.log("Clicked on First line");
			}else	switch (line) {
			case "First_Line":
				lines.get(0).click();
				Reporter.log("Clicked on First line");
				break;
			case "Second_Line":
				lines.get(1).click();
				Reporter.log("Clicked on Second line");
				break;
			case "Third_Line":
				lines.get(2).click();
				Reporter.log("Clicked on Third line");
				break;
			case "Fourth_Line":
				lines.get(3).click();
				Reporter.log("Clicked on Fourth line");
				break;
			case "Fifth_Line":
				lines.get(4).click();
				Reporter.log("Clicked on Fifth line");
				break;
			case "Sixth_Line":
				lines.get(5).click();
				Reporter.log("Clicked on Sixth line");
				break;
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on line");
		}
		return this;
	}

	/**
	 * Verify Display of Modal for no lines to upgrade
	 */
	public LineSelectorPage verifyModalNoLinesToUpgrade() {
		try {
			waitFor(ExpectedConditions.visibilityOf(noLinesToUpgradeErrorModalContainer), 10);
			Assert.assertTrue(noLinesToUpgradeErrorModalContainer.isDisplayed(),
					"Modal for no lines to upgrade is not displayed");
			Reporter.log("Modal for no lines to upgrade is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Modal for no lines to upgrade");
		}
		return this;
	}

	/**
	 * Verify Display Close CTA of Modal for No lines to upgrade
	 */
	public LineSelectorPage verifyModalNoLinesToUpgradeModelCloseCTA() {
		try {
			Assert.assertTrue(noLinesToUpgradeErrorModalCloseCTA.isDisplayed(),
					"Close CTA for Modal no lines to upgrade is not displayed");
			Reporter.log("Close CTA for Modal no lines to upgrade is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Close CTA for Modal of no lines to upgrade");
		}
		return this;
	}

	/**
	 * Click Close CTA of no lines to upgrade Model
	 */
	public LineSelectorPage clickNoLinesToUpgradeModelCloseCTA() {
		try {
			waitforSpinner();
			clickElementWithJavaScript(noLinesToUpgradeErrorModalCloseCTA);
			Reporter.log("Modal Close button is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Modal Close button");
		}
		return this;
	}
	
	/**
	 * Verify Jump line
	 */

	public LineSelectorPage verifyJumpLine() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(jumpLine), "Jump line message displayed");
			Reporter.log("Jump line message not displayed");
		} catch (AssertionError e) {
			Assert.fail("Jump line message displayed");
		}
		return this;
	}
	
	/**
	 * click on Select jump line
	 */
	public LineSelectorPage selectJumpLine() {
		try {
			Assert.assertTrue(jumpLine.isDisplayed(), "Failed to select Add a line option");
			jumpLine.click();
			Reporter.log("Add a new line link is selected");
		} catch (Exception e) {
			Assert.fail("Failed to select Add a line option");
		}
		return this;
	}
	

	/**
	 * Verify Jump line ineligibelity message
	 */

	public LineSelectorPage verifyJumpIneligibelityMessage() {
		try {
			waitforSpinner();
			Assert.assertTrue(isElementDisplayed(jumpIneligibelityMessage), "Jump line message displayed");
			Reporter.log("ump line Ineligibelity message is displayed");
		} catch (AssertionError e) {
			Assert.fail("Jump line Ineligibelity message is notdisplayed");
		}
		return this;
	}
	
	/**
	 * Get Per Page Results count
	 *
	 */
	public String getCustomerName() {
		String cname = "";
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(lineNameText.get(0)));
			cname = (lineNameText.get(0).getText());
		} catch (Exception e) {
			Assert.fail("Failed to get total count of Accessories");
		}
		return cname;
	}
	
	/**
	 * Verifies InEligiblity Message Model
	 */
	public LineSelectorPage clickInEligiblityMessageModel() {
		try {
			Assert.assertTrue(inEligiblityMessage.isDisplayed());
			Reporter.log("InEligiblity message clickable");

		} catch (Exception e) {
			Assert.fail("Failed to display InEligiblity message");
		}
		return this;
	}
	
	/**
	 * Verify PID for Msisdn
	 * 
	 * @param custMsisdnPID
	 */
	public void verifyPIIMaskingForMsisdn(String custMsisdnPID) {
		try {
			for (WebElement line : lineNumber){
				Assert.assertTrue(checkElementisPIIMasked(line, custMsisdnPID),
						"No PII masking for Customer Msisdn");
				Reporter.log("Verified PII masking for Msisdn: " + line);
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Msisdn");
		}
	}
	
	/**
	 * Verify PID for Name
	 * 
	 * @param custNamePID
	 */
	public void verifyPIIMaskingForName(String custNamePID) {
		try {
			for (WebElement name : deviceName){
				Assert.assertTrue(checkElementisPIIMasked(name, custNamePID),
						"No PII masking for Customer Name");
				Reporter.log("Verified PII masking for Name: " + name);
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify PII masking for Name");
		}
	}
	
	
}
