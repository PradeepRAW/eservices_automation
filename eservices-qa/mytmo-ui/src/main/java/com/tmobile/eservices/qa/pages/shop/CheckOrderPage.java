package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

//import javax.validation.constraints.AssertTrue;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author blakshminarayana
 *
 */
public class CheckOrderPage extends CommonPage {
	
	public static final String checkOrderPageUrl = "checkorder";

	@FindBy(css = "div.orderStatusHeader div.ui_mobile_headline")
	private WebElement orderStatusTextMobile;

	@FindBy(css = ".pagetitle_txt .ui_headline")
	private WebElement orderStatusText;

	@FindBys(@FindBy(css = "div[id*='overview'] div.ui_small_label"))
	private List<WebElement> orderStatusItemsList;

	@FindBy(css = "a[href='#collapse0'] .moreDetailSpan")
	private WebElement moreDetailsOrderStatusBYOD;
	
	@FindBy(css = "a[href='#collapse1'] .moreDetailSpan")
	private WebElement moreDetailsOrderStatusNonBYOD;

	@FindBy(css = "#orderStatusContainer0 .newDeviceTrackItem")
	private WebElement trackItemsLinkBYOD;
	
	@FindBy(css = "#orderStatusContainer1 .newDeviceTrackItem")
	private WebElement trackItemsLinkNonBYOD;

	@FindBy(css = "#orderStatusContainer0 #orderTotalError .ui_caps_headline")
	private WebElement orderTotalTextBYOD;

    @FindBy(css = "#orderStatusContainer0 #orderTotalError .js-device-price")
	private WebElement orderTotalAmountBYOD;
    
    @FindBy(css = "#orderStatusContainer1 #orderTotalError .ui_caps_headline")
	private WebElement orderTotalTextNonBYOD;

    @FindBy(css = "#orderStatusContainer1 #orderTotalError .js-device-price")
	private WebElement orderTotalAmountNonBYOD;
   
   @FindBy(css = "[href='#collapse0'] .lessDetailSpan")
  	private WebElement lessDetailsOrderStatus;
   
   @FindBy(css = "[href='#collapse1'] .lessDetailSpan")
 	private WebElement lessDetailsOrderStatusNonBYOD;
   
   @FindBy(css = "#collapse0 div.pull-left.ui_caps_headline")
 	private List<WebElement> paymentShippedBilledDeliverySection;
   
   @FindBy(xpath = "//div[@id='collapse0']//div[contains(text(),'SHIPPED TO')]")
  	private WebElement shippedToDeliverySection;
   
   @FindBy(css = "#collapse0 div.span3 div.pull-left")
 	private WebElement billedToDeliverySection;
   
   @FindBy(css = "#collapse0 div.ui_darkbody_bold")
 	private List<WebElement> paymentCustomerDetailSection;

	@FindBy(css = "#collapse0 span.ui_darkbody.js-payment-tmobileAccountNumber")
	private WebElement accountNumberCustomerDetailSection;

	@FindBy(css = "#collapse0 span.ui_darkbody.js-payment-phone-no")
	private WebElement phoneNumberCustomerDetailSection;

	@FindBy(css = "#collapse0 span.ui_darkbody.js-payment-email-id")
	private WebElement emailCustomerDetailSection;

	@FindBy(css = "#collapse0 span.ui_darkbody.js-payment-credit-card")
	private WebElement creditCardCustomerDetailSection;

	@FindBy(css = "#collapse0 span.ui_darkbody.js-payment-paid-date")
	private WebElement paidDateCustomerDetailSection;

	@FindBy(css = "#collapse0 div.ui_darkbody.js-customer-address1")
	private List<WebElement> address1InPaymentAndBilled;

	@FindBy(css = "#collapse0 div.ui_darkbody.js-customer-address2")
	private List<WebElement> address2InPaymentAndBilled;

	@FindBy(css = "#collapse0 div.prd-clm-2 div.js-customer-name")
	private WebElement shippedTocustomerNameInPaymentAndBilled;
	
	@FindBy(css = "#collapse0 div.prd-clm-3 div.js-customer-name")
	private WebElement billedTocustomerNameInPaymentAndBilled;

	@FindBy(css = "#collapse0 .ui_small_label_uppercase span")
	private WebElement transactionHeader;

	@FindBy(xpath = "//div[@id='collapse0']//span[contains(text(),'Insurance deductible')]")
	private WebElement insuranceDeductibleText;
	
	@FindBy(xpath = "//div[@id='collapse1']//span[contains(text(),'Insurance deductible')]")
	private WebElement insuranceDeductibleTextNonBYOD;

	@FindBy(css = "#collapse0 .insuranceDeductible")
	private WebElement insuranceDeductiblePrice;
	
	@FindBy(css = "#collapse1 .insuranceDeductible")
	private WebElement insuranceDeductiblePriceNonBYOD;
	
	@FindBy(css = "#new_device_detail_wrapper00 .deviceSalePrice")
	private WebElement simPrice;

	@FindBy(css = "#collapse0 .deposit-section .ui_darkbody")
	private WebElement depositeText;
	
	@FindBy(css = "#collapse1 .deposit-section .ui_darkbody")
	private WebElement depositeTextNonBYOD;

	@FindBy(css = "#collapse0 .deposit-section .depositamount")
	private WebElement depositeAmount;
	
	@FindBy(css = "#collapse1 .deposit-section .depositamount")
	private WebElement depositeAmountNonBYOD;

	@FindBy(xpath = "//div[@id='collapse0']//span[contains(text(),'Taxes and fees')]")
	private WebElement taxesAndFeesText;

	//@FindBy(css = "//div[@id='collapse1']//span[contains(text(),'Taxes and fees')]")
	// private WebElement taxesAndFeesTextNonBYOD; 
	
	
	@FindBy(css = "#collapse1 .trade-inner:nth-child(5) span")
	private List<WebElement> taxesAndFeesTextNonBYOD;
	

	@FindBy(css = "#orderStatusContainer0 #ordersStatusSameTonFromDate .js-device-estShipDateTOnFROMSame")
	private WebElement shippingDateBYOD;
	
	@FindBy(css = "#orderStatusContainer1 #ordersStatusSameTonFromDate .js-device-estShipDateTOnFROMSame")
	private WebElement shippingDateNonBYOD;

	@FindBy(css = "#collapse0 .taxesAndFees")
	private WebElement taxesAndFeesAmount;
	
	@FindBy(css = "#collapse1 .taxesAndFees")
	private WebElement taxesAndFeesAmountNonBYOD;

	@FindBy(xpath = "//div[@id='collapse0']//span[contains(text(),'Shipping')]")
	private WebElement shippingText;
	
	@FindBy(xpath = "//div[@id='collapse1']//span[contains(text(),'Shipping')]")
	private WebElement shippingTextNonBYOD;

	@FindBy(css = "#collapse0 .shippingDetails")
	private WebElement shippingAmount;
	
	@FindBy(css = "#collapse1 .shippingDetails")
	private WebElement shippingAmountNonBYOD;

	@FindBy(xpath = "//div[@id='collapse0']//span[contains(text(),'Total due today')]")
	private WebElement totaldueToday;
	
	@FindBy(xpath = "//div[@id='collapse1']//span[contains(text(),'Total due today')]")
	private WebElement totaldueTodayNonBYOD;

	@FindBy(css = "#collapse0 .totalDetailsdueToday")
	private WebElement totalDueTodayAmount;
	
	@FindBy(css = "#collapse1 .totalDetailsdueToday")
	private WebElement totalDueTodayAmountNonBYOD;

	@FindBy(css = "#collapse0 .legal-text-aal")
	private WebElement monthlyLegalText;
	
	@FindBy(css = "#collapse1 .legal-text-aal")
	private WebElement monthlyLegalTextNonBYOD;

	@FindBy(css = "#orderStatusContainer0 div.js-aal-order-type")
	private WebElement byodOrderType;

	@FindBy(css = "#orderStatusContainer1 div.js-aal-order-type")
	private WebElement nonByodOrderType;

	@FindBy(css = " ")
	private WebElement nonByodOrderStatus;
	
	@FindBy(css = "#new_device_overview00 .pt10 .pull-left")
	private WebElement deviceType;

	@FindBy(css = "#new_device_overview00 .js-orderdetails_status")
	private WebElement orderStatusItems;

	@FindBy(css = "#orderStatus")
	private List<WebElement> orderStatusOverall;

	@FindBy(css = "#new_device_overview00 .devacc_moredetails")
	private WebElement moreDetailsItems;
	
	@FindBy(css = "#new_device_overview11 .devacc_moredetails")
	private WebElement moreDetailsItemsNonBYOD;

	@FindBy(css = "#new_device_detail_wrapper00 .deviceSalePrice")
	private WebElement itemPrice;
	
	@FindBy(css = "#orderStatusContainer1 .ui_subhead.js-order-status")
	private List<WebElement> nonBYODorderStatusOverall;
	
	@FindBy(css = "#new_device_overview11 .pt10 .pull-left")
	private WebElement nonBYODdeviceType;
	
	@FindBy(css = "#new_device_overview11 .js-orderdetails_status")
	private WebElement nonBYODorderStatusItems;
	
	@FindBy(css = "#new_device_overview11 .devacc_moredetails")
	private WebElement nonBYODmoreDetailsItems;
	
	@FindBy(css = "#new_device_detail_wrapper10 .deviceSalePrice")
	private WebElement nonBYODitemPrice;
	
	@FindBy(css = "#new_device_detail_wrapper11 .deviceSalePrice")
	private WebElement nonBYODSIMPrice;
	
	@FindBy(css = "#collapse1 .ui_small_label_uppercase span")
	private WebElement transactionHeaderNonBYOD;
	
	@FindBy(css = "#new_device_detail_wrapper00 .collapseDetailName")
	private WebElement simStarterKitBYOD;
	
	@FindBy(css = "#new_device_detail_wrapper11 .collapseDetailName")
	private WebElement simStarterKitNONBYOD;
	
	@FindBy(css = "#orderStatusContainer1 #tradeInContainerId0 a[action*='ShowDetails']")
	private WebElement moreDetailsLink;
	
	@FindBy(css = "#orderStatusContainer1 #tradeInContainerId0 div[class*='tradeInDetailsBlock'] div[class*='ui_small_label_uppercase']")
	private WebElement tradeInDetailsTitle;
	
	@FindBy(css = "#orderStatusContainer1 #tradeInContainerId0 [class*='tradeInDownloadDRPLinkforWeb'] span[class*='pull-left ui_secondary_link']")
	private WebElement drpLink;
	
	@FindBy(css = "#orderStatusContainer1 #tradeInContainerId0 div[class*='tradeInPrintShippingLabelLink']")
	private WebElement printShippingLabel;
	
	@FindBy(css = "#orderStatusContainer1 #tradeInContainerId0 div[class*='ui_subhead']")
	private WebElement offerLabelText;
	
	@FindBy(css = "#orderStatusContainer3  #tradeInContainerId0 div[class*='ui_subhead'] span[class*='hide assurant_received_device']")
	private WebElement receivedAccessingText;
	
	@FindBy(css = "#orderStatusContainer2 #tradeInContainerId0 div[class*='ui_returnn clearfix'] span[class*='hide customer_accepted_exception']")
	private WebElement processingCreditText;
	
	@FindBy(css = "#orderStatusContainer5 #tradeInContainerId0 div[class*='ui_returnn clearfix'] span[class*='hide bill_credit_posted_status']")
	private WebElement completeCreditApplied;
	
	@FindBy(css = "#orderStatusContainer6 #tradeInContainerId0 div[class*='ui_returnn clearfix'] span[class*='hide customer_rejected_exception']")
	private WebElement processingReturn;
	
	@FindBy(css = "#orderStatusContainer1 #tradeInContainerId0 div[class*='tradeInAssessment'] div[class*='ui_small_label_uppercase']")
	private WebElement tradeInAssessmentTitle;
	
	@FindBy(css = "#orderStatusContainer1 #tradeInContainerId0 div[class*='tradeInAssessment'] div[class*='tradeInOffer']")
	private WebElement originalEstimatedValue;
	
	@FindBy(css = "#orderStatusContainer1 #tradeInContainerId0 div[class*='tradeInHistory'] div[class*='js-tradeInHistory-detail']")
	private WebElement orderPlaced;
	
	@FindBy(css = "#orderStatusContainer1 #tradeInContainerId0 div[class*='tradeInHistory'] div[class*='ui_small_label_uppercase']")
	private WebElement tradeInHistory;
	
	@FindBy(css = "#orderStatusContainer1 #tradeInContainerId0 div.js-trade-in-details-upgrad-aal")
	private WebElement tradeInSection;
	
	/**
	 * 
	 * @param webDriver
	 */
	public CheckOrderPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Check Order Page
	 * 
	 * @return
	 */
	public CheckOrderPage verifyCheckOrderPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(checkOrderPageUrl);
			Reporter.log("Check order page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Check order page not displayed");
		}
		return this;
	}

	/**
	 * Verify Check Order Page
	 * 
	 * @return
	 */
	public CheckOrderPage verifyNewDeviceTextUpdate() {
		waitforSpinner();
		boolean flag = false;
		if (orderStatusItemsList.size() == 0) {
			Assert.assertFalse(orderStatusItemsList.isEmpty(), "New device webelement is not displayed");
		} else {
			for (WebElement webElement : orderStatusItemsList) {
				flag = false;
				Assert.assertTrue(webElement.getText().contains("item"), "New device text not updated");
				flag = true;
			}
			if (flag)
				Reporter.log("New device text updated");
		}
		return this;
	}

	/**
	 * Verify more details link BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyMoreDetailsLinkBYOD() {
		try {

			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(moreDetailsOrderStatusBYOD));
			Assert.assertTrue(moreDetailsOrderStatusBYOD.isDisplayed(), "more details link is not visible");
			Reporter.log("more details link is displayed");
		}

		catch (Exception e) {
			Assert.fail("Failed to view  on More details link ");
		}
		return this;
	}
	
	
	/**
	 * Verify more details link Non BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyMoreDetailsLinkNonBYOD() {
		try {

			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(moreDetailsOrderStatusNonBYOD));
			Assert.assertTrue(moreDetailsOrderStatusNonBYOD.isDisplayed(), "more details link is not visible");
			Reporter.log("more details link is displayed");
		}

		catch (Exception e) {
			Assert.fail("Failed to view  on More details link ");
		}
		return this;
	}

	/**
	 * Verify Order Total Text BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyOrderTotalTextBYOD() {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(orderTotalTextBYOD));
			Assert.assertTrue(orderTotalTextBYOD.isDisplayed(), "Order Total Text is not visible");
			Reporter.log("Order Total Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  Order Total Text ");
		}
		return this;
	}
	
	/**
	 * Verify Order Total Text Non BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyOrderTotalTextNonBYOD() {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(orderTotalTextNonBYOD));
			Assert.assertTrue(orderTotalTextNonBYOD.isDisplayed(), "Order Total Text is not visible");
			Reporter.log("Order Total Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  Order Total Text ");
		}
		return this;
	}

	/**
	 * Verify more details link BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyOrderTotalAmountBYOD() {
		try {
			Assert.assertTrue(orderTotalAmountBYOD.isDisplayed(), "Order Total Amount is not visible");	
			Assert.assertTrue(orderTotalAmountBYOD.getText().contains("$"));
			Reporter.log("Order Total Amount is displayed with Dollar sign");}
		catch(Exception e) {
			Assert.fail("Failed to view  Order Total Amount ");
		}
			return this;
	}
	
	/**
	 * Verify more details link Non BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyOrderTotalAmountNonBYOD() {
		try {
			Assert.assertTrue(orderTotalAmountNonBYOD.isDisplayed(), "Order Total Amount is not visible");	
			Assert.assertTrue(orderTotalAmountNonBYOD.getText().contains("$"));
			Reporter.log("Order Total Amount is displayed with Dollar sign");}
		catch(Exception e) {
			Assert.fail("Failed to view  Order Total Amount ");
		}
			return this;
	}
	
	/**
	 * Verify Track items link BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTrackItemsLinkBYOD() {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(trackItemsLinkBYOD));
			Assert.assertTrue(trackItemsLinkBYOD.isDisplayed(), "more details link is not visible");
			Reporter.log("Track Items link is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  track items link ");
		}
		return this;
	}
	
	/**
	 * Verify Track items link Non BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTrackItemsLinkNonBYOD() {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(trackItemsLinkNonBYOD));
			Assert.assertTrue(trackItemsLinkNonBYOD.isDisplayed(), "more details link is not visible");
			Reporter.log("Track Items link is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  track items link ");
		}
		return this;
	}

	/**
	 * Click Track Items Link BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage clickTrackItemsLinkBYOD() {
		try {
			trackItemsLinkBYOD.click();
			Reporter.log("Track Items link is clicked ");}
		catch(Exception e) {
			Assert.fail("Failed to click  track items link ");
		}
			return this;
	}
	
	/**
	 * Click Track Items Link Non BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage clickTrackItemsLinkNonBYOD() {
		try {
			trackItemsLinkNonBYOD.click();
			Reporter.log("Track Items link is clicked ");}
		catch(Exception e) {
			Assert.fail("Failed to click  track items link ");
		}
			return this;
	}
	
	/**
	 * click more details link
	 * 
	 * @return
	 */
	public CheckOrderPage clickMoreDetailsLinkBYOD() {
		try {
			moveToElement(moreDetailsOrderStatusBYOD);
			clickElementWithJavaScript(moreDetailsOrderStatusBYOD);
			Reporter.log("More details link is not clickable");
		} catch (Exception e) {
			Assert.fail("Failed to click on More details link ");
		}
		return this;
	}
	
	/**
	 * Verify less details link
	 * 
	 * @return
	 */
	public CheckOrderPage verifyLessDetailsLinkNonBYOD() {
		try {
			waitFor(ExpectedConditions.visibilityOf(lessDetailsOrderStatusNonBYOD));
			Assert.assertTrue(lessDetailsOrderStatusNonBYOD.isDisplayed(), "less details link is not visible");
			Reporter.log("Less details is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  on less details link ");
		}
		return this;
	}

	/**
	 * click less details link
	 * 
	 * @return
	 */
	public CheckOrderPage clickLessDetailsLink() {
		try {
			clickElementWithJavaScript(lessDetailsOrderStatus);
			Reporter.log("More details link is not clickable");
		} catch (Exception e) {
			Assert.fail("Failed to click on More details link ");
		}
		return this;
	}

	/**
	 * click Non BYOD less details link
	 * 
	 * @return
	 */
	public CheckOrderPage clickLessDetailsLinkNonBYOD() {
		try {
			moveToElement(lessDetailsOrderStatusNonBYOD);
			clickElementWithJavaScript(lessDetailsOrderStatusNonBYOD);
			Reporter.log("Less details link is clickable");
		} catch (Exception e) {
			Assert.fail("Failed to click on Less details link ");
		}
		return this;
	}
	
	/**
	 * Verify PaymentAuthorableText
	 * 
	 * @return
	 */
	public CheckOrderPage verifyPaymentAuthorableTextInMoreDetails() {
		try {
			waitFor(ExpectedConditions.visibilityOf(paymentShippedBilledDeliverySection.get(0)));
			Assert.assertTrue(paymentShippedBilledDeliverySection.get(0).isDisplayed(), "payment text in not visible ");
			Reporter.log("payment text is  displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  on payment text ");
		}
		return this;
	}

	/**
	 * Verify Shipped TO AuthorableText
	 * 
	 * @return
	 */
	public CheckOrderPage verifyShippedToAuthorableTextInMoreDetails() {
		try {
			waitFor(ExpectedConditions.visibilityOf(shippedToDeliverySection));
			Assert.assertTrue(isElementDisplayed(shippedToDeliverySection),
					"ShippedTo text in not visible ");
			Reporter.log("ShippedTo text is displayed on clicking more details");
		} catch (Exception e) {
			Assert.fail("Failed to view  on ShippedTo text ");
		}
		return this;
	}

	/**
	 * Verify Billed To AuthorableText
	 * 
	 * @return
	 */
	public CheckOrderPage verifyBilledToAuthorableTextInMoreDetails() {
		try {
			waitFor(ExpectedConditions.visibilityOf(billedToDeliverySection));
			Assert.assertTrue(billedToDeliverySection.isDisplayed(), "Billed To text in not visible ");
			Reporter.log("billed text is displayed on clicking more details");
		} catch (Exception e) {
			Assert.fail("Failed to view  on billed text ");
		}
		return this;
	}

	/**
	 * Verify Customer To Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyCustomerTextUnderPaymentTextInMoreDetails() {
		try {
			waitFor(ExpectedConditions.visibilityOf(paymentCustomerDetailSection.get(0)));
			Assert.assertTrue(paymentCustomerDetailSection.get(0).isDisplayed(), "Customer Text in not visible ");
			Reporter.log("Customer text is displayed on clicking more details");
		} catch (Exception e) {
			Assert.fail("Failed to view  customer text in payment  ");
		}
		return this;
	}

	/**
	 * Verify account Number text To Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTAccountNumberTextUnderPaymentTextInMoreDetails() {
		try {
			waitFor(ExpectedConditions.visibilityOf(paymentCustomerDetailSection.get(1)));
			Assert.assertTrue(paymentCustomerDetailSection.get(1).isDisplayed(), "account Number text in not visible ");
			Reporter.log("account Number text  Details is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view  account Number in payment  ");
		}
		return this;
	}
   
	/**
	 * Verify account Number To Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTAccountNumberUnderPaymentTextInMoreDetails() {
		try {
			
			Assert.assertTrue(isElementDisplayed(accountNumberCustomerDetailSection), "account Number  in not visible ");
			Reporter.log("account Number  Details is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view  account Number in payment  ");
		}
		return this;
	}
	
	/**
	 * Verify phone Number Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyPhoneNumberTextUnderPaymentTextInMoreDetails() {
		try {
			Assert.assertTrue(paymentCustomerDetailSection.get(2).isDisplayed(), "phone Number text in not visible ");
			Reporter.log("PhoneNumber text and number Details is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view  PhoneNumber in payment  ");
		}
		return this;
	}
	
	/**
	 * Verify phone Number Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyPhoneNumberUnderPaymentTextInMoreDetails() {
		try {
			
			Assert.assertTrue(isElementDisplayed(phoneNumberCustomerDetailSection), "phone Number  in not visible ");
			Reporter.log("PhoneNumber Details is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view  PhoneNumber in payment  ");
		}
		return this;
	}

	/**
	 * Verify email Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyEmailUnderPaymentTextInMoreDetails() {
		try {
			Assert.assertTrue(emailCustomerDetailSection.isDisplayed(), "email id in not visible ");
			String email = emailCustomerDetailSection.getText();
			Assert.assertTrue(email.contains("@"), "email text is verified");
			Reporter.log("email    idDetails is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view email text in payment  ");
		}
		return this;
	}
	
	/**
	 * Verify email Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyEmailTextUnderPaymentTextInMoreDetails() {
		try {
			Assert.assertTrue(paymentCustomerDetailSection.get(3).isDisplayed(), "email text in not visible ");
			Reporter.log("email  text is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view email text in payment  ");
		}
		return this;
	}

	/**
	 * Verify credit card Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyCreditCardUnderPaymentTextInMoreDetails() {
		try {
			Assert.assertTrue(creditCardCustomerDetailSection.isDisplayed(), "creditCard number in not visible ");
			String cardNumber = creditCardCustomerDetailSection.getText();
			String pattern = "^(.*)[0-9]{4}$";
			Assert.assertTrue(cardNumber.matches(pattern), "card number last 4 digit are numeric");
			Reporter.log("creditCard id Details is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view creditCard text in payment  ");
		}
		return this;
	}
	
	/**
	 * Verify credit card Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyCreditCardTextUnderPaymentTextInMoreDetails() {
		try {
			Assert.assertTrue(paymentCustomerDetailSection.get(5).isDisplayed(), "creditCard text in not visible ");
			Reporter.log("creditCard text Details is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view creditCard text in payment  ");
		}
		return this;
	}

	/**
	 * Verify Transaction Header
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTransactionHeader() {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(transactionHeader));
			Assert.assertTrue(transactionHeader.isDisplayed(), "Transaction Header is not visible");
			Reporter.log("Transaction Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Transaction Header");
		}
		return this;
	}
	
	/**
	 * Verify Transaction Header
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTransactionHeaderNonBYOD() {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(transactionHeaderNonBYOD));
			Assert.assertTrue(transactionHeaderNonBYOD.isDisplayed(), "Transaction Header is not visible");
			Reporter.log("Transaction Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Transaction Header");
		}
		return this;
	}

	/*
	 * Verify PaidDate Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyPaidDateUnderPaymentTextInMoreDetails() {
		try {
			
			Assert.assertTrue(paidDateCustomerDetailSection.isDisplayed(), "PaidDate in not visible ");
			String paidDate = paidDateCustomerDetailSection.getText();
			String pattern = "\\s*[a-zA-Z]{3,9}\\s+\\d{1,2}\\s*,?\\s*\\d{4}";
            Assert.assertTrue(paidDate.matches(pattern), "Paid date not verified");
			Reporter.log("PaidDatetext and date Details is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view PaidDatetext in payment  ");
		}
		return this;
	}

	/*
	 * Verify PaidDate Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyPaidDateTextUnderPaymentTextInMoreDetails() {
		try {
			Assert.assertTrue(paymentCustomerDetailSection.get(5).isDisplayed(), "PaidDatetext in not visible ");
		    Reporter.log("PaidDatetext is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view PaidDatetext in payment  ");
		}
		return this;
	}
	/**
	 * Verify CustomerName Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyCustomerNameUnderBilledToTextInMoreDetails() {
		try {
			Assert.assertTrue(billedTocustomerNameInPaymentAndBilled.isDisplayed(),
					"CustomerName Under BilledTo in not visible ");

			Reporter.log("CustomerName Under BilledTo is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view CustomerName Under Billed To in payment  ");
		}
		return this;
	}

	/**
	 * Verify CustomerName in shipped Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyCustomerNameUnderShippedToTextInMoreDetails() {
		try {
			Assert.assertTrue(shippedTocustomerNameInPaymentAndBilled.isDisplayed(),
					"CustomerName Under Shipped To in not visible ");

			Reporter.log("CustomerName Under shipped To is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view CustomerName Under shipped To in payment  ");
		}
		return this;
	}

	/**
	 * Verify CustomerAddress in shipped to Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyCustomerAdddressUnderShippedToTextInMoreDetails() {
		try {
			Assert.assertTrue(address1InPaymentAndBilled.get(0).isDisplayed(),
					"Customeraddress street city Under Shipped To in not visible ");
			Assert.assertTrue(address2InPaymentAndBilled.get(0).isDisplayed(),
					"Customeraddress sate and zip Under Shipped To in not visible ");
			Reporter.log("CustomerAddress Under shipped To is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view CustomerAddress Under shipped To in payment  ");
		}
		return this;
	}

	/**
	 * Verify CustomerAddress in billed to Authorable Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyCustomerAddressUnderBilledToTextInMoreDetails() {
		try {
			Assert.assertTrue(address1InPaymentAndBilled.get(1).isDisplayed(),
					"Customeraddress street city Under billed  To in not visible ");
			Assert.assertTrue(address2InPaymentAndBilled.get(1).isDisplayed(),
					"Customeraddress state and zip Under billed to in not visible ");
			Reporter.log("CustomerAddress Under shipped To is displayed ");

			Reporter.log("CustomerAddress Under Billed To is displayed ");
		} catch (Exception e) {
			Assert.fail("Failed to view CustomerAddress Under Billed To in payment  ");
		}
		return this;
	}

	/**
	 * Verify Order Type is displayed as AAL -- BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyAALorderTypeBYOD() {
		try {
			waitforSpinner();
			Assert.assertTrue(byodOrderType.getText().contains("Add A Line"), "Order Type is not displayed as AAL");
			Reporter.log("Order Type is displayed as AAL");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to display Order Type as AAL");
		}
		return this;
	}

	/**
	 * Verify Order Type is displayed as AAL -- NON BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyAALorderTypeNonBYOD() {
		try {
			waitforSpinner();
			Assert.assertTrue(nonByodOrderType.getText().contains("Add A Line"), "Order Type is not displayed as AAL");
			Reporter.log("Order Type is displayed as AAL");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to display Order Type as AAL");
		}
		return this;
	}

	/**
	 * Verify Order Status is displayed -- NON BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyOrderStatusDisplayedNonBYOD() {
		try {
			waitforSpinner();
			Assert.assertTrue(orderStatusOverall.get(2).isDisplayed(), "Order Status is not displayed");
			Reporter.log("Order Status is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to display Order Status");
		}
		return this;
	}

	/**
	 * Verify Insurance deductible title
	 * 
	 * @return
	 */
	public CheckOrderPage verifyInsuranceDeductibleTitle() {
		try {
			waitFor(ExpectedConditions.visibilityOf(insuranceDeductibleText));
			Assert.assertTrue(isElementDisplayed(insuranceDeductibleText),
					"'Insurance Deductible' title is not visible");
			Reporter.log("'Insurance Deductible' title is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view 'Insurance Deductible' title");
		}
		return this;
	}
	
	
	/**
	 * Verify Insurance deductible title
	 * 
	 * @return
	 */
	public CheckOrderPage verifyInsuranceDeductibleTitleNonBYOD() {
		try {
			waitFor(ExpectedConditions.visibilityOf(insuranceDeductibleTextNonBYOD));
			Assert.assertTrue(isElementDisplayed(insuranceDeductibleTextNonBYOD),
					"'Insurance Deductible' title is not visible");
			Reporter.log("'Insurance Deductible' title is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view 'Insurance Deductible' title");
		}
		return this;
	}

	/**
	 * Verify Insurance deductible amount
	 * 
	 * @return
	 */
	public CheckOrderPage verifyInsuranceDeductibleAmount() {
		try {
			waitFor(ExpectedConditions.visibilityOf(insuranceDeductiblePrice));
			Assert.assertTrue(isElementDisplayed(insuranceDeductiblePrice),
					"'Insurance Deductible' amount is not visible");
			Reporter.log("'Insurance Deductible' amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view 'Insurance Deductible' amount");
		}
		return this;
	}
	
	/**
	 * Verify Insurance deductible amount
	 * 
	 * @return
	 */
	public CheckOrderPage verifyInsuranceDeductibleAmountNonBYOD() {
		try {
			waitFor(ExpectedConditions.visibilityOf(insuranceDeductiblePriceNonBYOD));
			Assert.assertTrue(isElementDisplayed(insuranceDeductiblePriceNonBYOD),
					"'Insurance Deductible' amount is not visible");
			Reporter.log("'Insurance Deductible' amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view 'Insurance Deductible' amount");
		}
		return this;
	}

	/**
	 * Get Insurance deductible amount
	 * 
	 * @return
	 */
	public Double getInsuranceDeductibleAmount() {

		String amt = insuranceDeductiblePrice.getText();
		String amount = amt.substring(amt.lastIndexOf("$") + 1);
		return Double.parseDouble(amount);
	}
	
	/**
	 * Get Insurance deductible amount
	 * 
	 * @return
	 */
	public Double getInsuranceDeductibleAmountNonBYOD() {

		String amt = insuranceDeductiblePriceNonBYOD.getText();
		String amount = amt.substring(amt.lastIndexOf("$") + 1);
		return Double.parseDouble(amount);
	}

	/**
	 * Verify Order Status is displayed -- BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage orderShippingDateBYOD() {
		try {
			waitforSpinner();
			Assert.assertTrue(shippingDateBYOD.isDisplayed(), "Order Status is not displayed");
			String shipDate = shippingDateBYOD.getText();
			String pattern = "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d$";
            Assert.assertTrue(shipDate.matches(pattern), "Shipping date not verified");
			Reporter.log("Shipping Date is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to display Shipping Date");
		}
		return this;
	}
	
	
	/**
	 * Verify Order Status is displayed -- BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage orderShippingDateNonBYOD() {
		try {
			waitforSpinner();
			Assert.assertTrue(shippingDateNonBYOD.isDisplayed(), "Order Status is not displayed");
			String shipDate = shippingDateNonBYOD.getText();
			String pattern = "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d$";
            Assert.assertTrue(shipDate.matches(pattern), "Shipping date not verified");
			Reporter.log("Shipping Date is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to display Shipping Date");
		}
		return this;
	}

	/**
	 * Verify Deposit text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyDepositeText() {
		try {
			waitFor(ExpectedConditions.visibilityOf(depositeText));
			Assert.assertTrue(isElementDisplayed(depositeText), "Deposite text is not visible");
			Reporter.log("Deposite text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Deposite text");
		}
		return this;
	}
	
	/**
	 * Verify Deposit text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyDepositeTextNonBYOD() {
		try {
			waitFor(ExpectedConditions.visibilityOf(depositeTextNonBYOD));
			Assert.assertTrue(isElementDisplayed(depositeTextNonBYOD), "Deposite text is not visible");
			Reporter.log("Deposite text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Deposite text");
		}
		return this;
	}

	/**
	 * Verify Deposit amount
	 * 
	 * @return
	 */
	public CheckOrderPage verifyDepositAmount() {
		try {
			waitFor(ExpectedConditions.visibilityOf(depositeAmount));
			Assert.assertTrue(isElementDisplayed(depositeAmount), "Deposit Amount is not visible");
			Reporter.log("Deposit Amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Deposite amount");
		}
		return this;
	}
	
	/**
	 * Verify Deposit amount
	 * 
	 * @return
	 */
	public CheckOrderPage verifyDepositAmountNonBYOD() {
		try {
			waitFor(ExpectedConditions.visibilityOf(depositeAmountNonBYOD));
			Assert.assertTrue(isElementDisplayed(depositeAmountNonBYOD), "Deposit Amount is not visible");
			Reporter.log("Deposit Amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Deposite amount");
		}
		return this;
	}

	/**
	 * Get Deposit amount
	 * 
	 * @return
	 */
	public Double getDepositAmount() {

		String amt = depositeAmount.getText();
		String amount = amt.substring(amt.lastIndexOf("$") + 1);
		return Double.parseDouble(amount);
	}
	
	/**
	 * Get Deposit amount
	 * 
	 * @return
	 */
	public Double getDepositAmountNonBYOD() {

		String amt = depositeAmountNonBYOD.getText();
		String amount = amt.substring(amt.lastIndexOf("$") + 1);
		return Double.parseDouble(amount);
	}

	/**
	 * Verify Taxes and Fees text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTaxesAndFeesText() {
		try {
			waitFor(ExpectedConditions.visibilityOf(taxesAndFeesText));
			Assert.assertTrue(isElementDisplayed(taxesAndFeesText), "Taxes and fees text is not visible");
			Reporter.log("Taxes and fees text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Taxes and fees text");
		}
		return this;
	}
	
	/**
	 * Verify Taxes and Fees text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTaxesAndFeesTextNonBYOD() {
		try {
			waitFor(ExpectedConditions.visibilityOf(taxesAndFeesTextNonBYOD.get(0)));
			Assert.assertTrue(isElementDisplayed(taxesAndFeesTextNonBYOD.get(0)), "Taxes and fees text is not visible");
			Reporter.log("Taxes and fees text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Taxes and fees text");
		}
		return this;
	}

	/**
	 * Verify Taxes and Fees amount
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTaxesAndFeesAmount() {
		try {
			waitFor(ExpectedConditions.visibilityOf(taxesAndFeesAmount));
			Assert.assertTrue(isElementDisplayed(taxesAndFeesAmount), "Taxes and fees amount is not visible");
			Reporter.log("Taxes and fees amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Taxes and fees amount");
		}
		return this;
	}
	
	/**
	 * Verify Taxes and Fees amount
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTaxesAndFeesAmountNonBYOD() {
		try {
			waitFor(ExpectedConditions.visibilityOf(taxesAndFeesAmountNonBYOD));
			Assert.assertTrue(isElementDisplayed(taxesAndFeesAmountNonBYOD), "Taxes and fees amount is not visible");
			Reporter.log("Taxes and fees amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Taxes and fees amount");
		}
		return this;
	}

	/**
	 * Get Taxes and Fees amount
	 * 
	 * @return
	 */
	public Double getTaxesAndFeesAmount() {

		String amt = taxesAndFeesAmount.getText();
		String amount = amt.substring(amt.lastIndexOf("$") + 1);
		return Double.parseDouble(amount);
	}
	
	/**
	 * Get Taxes and Fees amount
	 * 
	 * @return
	 */
	public Double getTaxesAndFeesAmountNonBYOD() {

		String amt = taxesAndFeesAmountNonBYOD.getText();
		String amount = amt.substring(amt.lastIndexOf("$") + 1);
		return Double.parseDouble(amount);
	}

	/**
	 * Verify Shipping text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyShippingText() {
		try {
			waitFor(ExpectedConditions.visibilityOf(shippingText));
			Assert.assertTrue(isElementDisplayed(shippingText), "Shipping text is not visible");
			Reporter.log("Shipping text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Shipping text");
		}
		return this;
	}
	
	/**
	 * Verify Shipping text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyShippingTextNonBYOD() {
		try {
			waitFor(ExpectedConditions.visibilityOf(shippingTextNonBYOD));
			Assert.assertTrue(isElementDisplayed(shippingTextNonBYOD), "Shipping text is not visible");
			Reporter.log("Shipping text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Shipping text");
		}
		return this;
	}

	/**
	 * Verify Shipping amount
	 * 
	 * @return
	 */
	public CheckOrderPage verifyShippingAmount() {
		try {
			waitFor(ExpectedConditions.visibilityOf(shippingAmount));
			Assert.assertTrue(isElementDisplayed(shippingAmount), "Shipping Amount is not visible");
			Reporter.log("Shipping Amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Shipping amount");
		}
		return this;
	}
	
	/**
	 * Verify Shipping amount
	 * 
	 * @return
	 */
	public CheckOrderPage verifyShippingAmountNonBYOD() {
		try {
			waitFor(ExpectedConditions.visibilityOf(shippingAmountNonBYOD));
			Assert.assertTrue(isElementDisplayed(shippingAmountNonBYOD), "Shipping Amount is not visible");
			Reporter.log("Shipping Amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Shipping amount");
		}
		return this;
	}

	/**
	 * Get SHipping amount
	 * 
	 * @return
	 */
	public Double getShippingAmount() {

		String amt = shippingAmount.getText();
		String amount = amt.substring(amt.lastIndexOf("$") + 1);
		return Double.parseDouble(amount);
	}
	
	/**
	 * Get SHipping amount
	 * 
	 * @return
	 */
	public Double getShippingAmountNonBYOD() {

		String amt = shippingAmountNonBYOD.getText();
		String amount = amt.substring(amt.lastIndexOf("$") + 1);
		return Double.parseDouble(amount);
	}

	/**
	 * Verify Total due today text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTotalDueText() {
		try {
			waitFor(ExpectedConditions.visibilityOf(totaldueToday));
			Assert.assertTrue(isElementDisplayed(totaldueToday), "Total due today text is not visible");
			Reporter.log("Total due today text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Total due today text");
		}
		return this;
	}
	
	/**
	 * Verify Total due today text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTotalDueTextNonBYOD() {
		try {
			waitFor(ExpectedConditions.visibilityOf(totaldueTodayNonBYOD));
			Assert.assertTrue(isElementDisplayed(totaldueTodayNonBYOD), "Total due today text is not visible");
			Reporter.log("Total due today text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Total due today text");
		}
		return this;
	}

	/**
	 * Verify Total due today amount
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTotalDueAmount() {
		try {
			waitFor(ExpectedConditions.visibilityOf(totalDueTodayAmount));
			Assert.assertTrue(isElementDisplayed(totalDueTodayAmount), "Total due today amount is not visible");
			Reporter.log("Total due today amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Total due today amount");
		}
		return this;
	}
	
	/**
	 * Verify Total due today amount
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTotalDueAmountNonBYOD() {
		try {
			waitFor(ExpectedConditions.visibilityOf(totalDueTodayAmountNonBYOD));
			Assert.assertTrue(isElementDisplayed(totalDueTodayAmountNonBYOD), "Total due today amount is not visible");
			Reporter.log("Total due today amount is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Total due today amount");
		}
		return this;
	}
	
	/**
	 * Get Get Total Due amount
	 * 
	 * @return
	 */
	public Double getTotalDueAmount() {

		String amt = totalDueTodayAmount.getText();
		String amount = amt.substring(amt.lastIndexOf("$") + 1);
		return Double.parseDouble(amount);
	}
	
	/**
	 * Get Get Total Due amount
	 * 
	 * @return
	 */
	public Double getTotalDueAmountNonBYOD() {

		String amt = totalDueTodayAmountNonBYOD.getText();
		String amount = amt.substring(amt.lastIndexOf("$") + 1);
		return Double.parseDouble(amount);
	}
	
	/**
	 * Compare Total due today amount
	 * 
	 * @return
	 */
	public CheckOrderPage compareTotalDueAmountWithAllAmounts(Double V1,Double V2,Double V3,Double V4,Double V6,Double V5) {
		try {
			
			Assert.assertEquals(Math.round(V5),Math.round(V1+V2+V3+V4+V6), "Total due today amount is not equal");
			Reporter.log("Total due today amount is Equal");
		} catch (Exception e) {
			Assert.fail("Failed to Calculate Total due today amount");
		}
		return this;
	}

	/**
	 * Verify Monthly Legal Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyMonthlyLegalText() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyLegalText));
			Assert.assertTrue(isElementDisplayed(monthlyLegalText), "Monthly Legal Text is not visible");
			Reporter.log("Monthly Legal Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Monthly Legal Text");
		}
		return this;
	}
	
	/**
	 * Verify Monthly Legal Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyMonthlyLegalTextNonBYOD() {
		try {
			waitFor(ExpectedConditions.visibilityOf(monthlyLegalTextNonBYOD));
			Assert.assertTrue(isElementDisplayed(monthlyLegalTextNonBYOD), "Monthly Legal Text is not visible");
			Reporter.log("Monthly Legal Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Monthly Legal Text");
		}
		return this;
	}

	 								 
	/**
	 * Verify Device Type
	 * 
	 * @return
	 */
	public CheckOrderPage verifyDeviceType() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(deviceType.isDisplayed(), "Device Type is not visible");
			Reporter.log("Device Type is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  Device Type");
		}
		return this;
	}
	
	/**
	 * Verify Device Type Non BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyDeviceTypeNonBYOD() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(nonBYODdeviceType.isDisplayed(), "Device Type is not visible");
			Reporter.log("Device Type is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  Device Type");
		}
		return this;
	}

	/**
	 * Verify Order Status Sim
	 * 
	 * @return
	 */
	public CheckOrderPage verifyOrderStatusItem() {
		try {
			Assert.assertTrue(orderStatusItems.isDisplayed(), "Order Status for Items is not visible");
			Reporter.log("Order Status for Items is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  Order Status for Items");
		}
		return this;
	}
	
	/**
	 * Verify Order Status Sim Non BYOD
	 * 
	 * @return
	 */
	public CheckOrderPage verifyOrderStatusItemNonBYOD() {
		try {
			Assert.assertTrue(nonBYODorderStatusItems.isDisplayed(), "Order Status for Items is not visible");
			Reporter.log("Order Status for Items is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  Order Status for Items");
		}
		return this;
	}

	/**
	 * Verify Order Status Overall
	 * 
	 * @return
	 */
	public CheckOrderPage verifyOrderStatusOverallBYOD() {
		try {
			Assert.assertTrue(orderStatusOverall.get(1).isDisplayed(), "Order Status overall is not visible");
			Reporter.log("Order Status overall is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  Order Status overall");
		}
		return this;
	}
	
	/**
	 * Verify Order Status Overall
	 * 
	 * @return
	 */
	public CheckOrderPage verifyOrderStatusOverallNonBYOD() {
		try {
			Assert.assertTrue(nonBYODorderStatusOverall.get(0).isDisplayed(), "Order Status overall is not visible");
			Reporter.log("Order Status overall is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  Order Status overall");
		}
		return this;
	}

	/**
	 * Verify More Details for Items
	 * 
	 * @return
	 */
	public CheckOrderPage verifyMoreDetailsItems() {
		try {
			Assert.assertTrue(moreDetailsItems.isDisplayed(), "More Details for items is not visible");
			Reporter.log("More Details for items is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view More Details for items");
		}
		return this;
	}
	
	/**
	 * Verify More Details for Items
	 * 
	 * @return
	 */
	public CheckOrderPage verifyMoreDetailsItemsNonBYOD() {
		try {
			Assert.assertTrue(nonBYODmoreDetailsItems.isDisplayed(), "More Details for items is not visible");
			Reporter.log("More Details for items is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view More Details for items");
		}
		return this;
	}

	/**
	 * click more details link for items
	 * 
	 * @return
	 */
	public CheckOrderPage clickMoreDetailsLinkForItems() {
		try {
			clickElementWithJavaScript(moreDetailsItems);
			Reporter.log("More details link for items Clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on More details link for Items ");
		}
		return this;
	}
	/**
	 * click more details link for items
	 * 
	 * @return
	 */
	public CheckOrderPage clickMoreDetailsLinkForItemsNonBYOD() {
		try {
			clickElementWithJavaScript(moreDetailsItemsNonBYOD);
			Reporter.log("More details link for items Clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on More details link for Items ");
		}
		return this;
	}

	/**
	 * Verify More Details for Items
	 * 
	 * @return
	 */
	public CheckOrderPage verifyItemPrice() {
		try {
			Assert.assertTrue(itemPrice.isDisplayed(), "Item Price is not visible");
			Reporter.log("Item Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Item Price");
		}
		return this;
	}
	/**
	 * Verify More Details for Items
	 * 
	 * @return
	 */
	public CheckOrderPage verifyItemPriceNonBYOD() {
		try {
			Assert.assertTrue(nonBYODitemPrice.isDisplayed(), "Item Price is not visible");
			Reporter.log("Item Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Item Price");
		}
		return this;
	}
	
	/**
	 * Verify More Details for Items
	 * 
	 * @return
	 */
	public CheckOrderPage verifyDevicePriceNonBYOD() {
		try {
			Assert.assertTrue(nonBYODitemPrice.isDisplayed(), "Item Price is not visible");
			Reporter.log("Item Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view Item Price");
		}
		return this;
	}
	
	/**
	 * Verify More Details for Items
	 * 
	 * @return
	 */
	public CheckOrderPage verifySIMPriceNonBYOD() {
		try {
			Assert.assertTrue(nonBYODSIMPrice.isDisplayed(), "sim Price is not visible");
			Reporter.log("sim Price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view  sim Price");
		}
		return this;
	}
	/**
	 * click more details link
	 * 
	 * @return
	 */
	public CheckOrderPage clickMoreDetailsLinkNonBYOD() {
		try {
			moveToElement(moreDetailsOrderStatusNonBYOD);
			clickElementWithJavaScript(moreDetailsOrderStatusNonBYOD);
			Reporter.log("More details link is not clickable");
		} catch (Exception e) {
			Assert.fail("Failed to click on More details link ");
		}
		return this;
	}
    
	
	/**
	 * Verify sim starter kit for Items
	 * 
	 * @return
	 */
	public CheckOrderPage verifySIMStarterKItBYOD() {
		try {
			Assert.assertTrue(simStarterKitBYOD.isDisplayed(), "sim starter kit is not visible");
			Reporter.log("sim starter kit is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view sim starter kit");
		}
		return this;
	}
	
	/**
	 * Verify sim starter kit for Items
	 * 
	 * @return
	 */
	public CheckOrderPage verifySIMStarterKItNONBYOD() {
		try {
			Assert.assertTrue(simStarterKitNONBYOD.isDisplayed(), "sim starter kit is not visible");
			Reporter.log("sim starter kit is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to view sim starter kit");
		}
		return this;
	}
	
	/**
	 * Get Device Price
	 */
	public Double getDevicePrice() {
		String amt = itemPrice.getText();
		String amount = amt.substring(amt.lastIndexOf("$") + 1);
		return Double.parseDouble(amount);
	}
	
	/**
	 * Click More Details Link
	 * 
	 * @return
	 */
	public CheckOrderPage clickMoreDetailsLink() {
		try {
			waitforSpinner();
			moreDetailsLink.click();
			Reporter.log("Clicked on More Details Link");
		} catch (Exception e) {
			Assert.fail("Failed to click on More Details Link");
		}
		return this;
	}
	
	/**
	 * Verify trade in details
	 * 
	 * @return
	 */
	public CheckOrderPage verifyTradeInDetails() {
		try {
			waitforSpinner();
			Assert.assertTrue(tradeInDetailsTitle.isDisplayed(), "Trade in details title is not visible");
			Assert.assertTrue(tradeInAssessmentTitle.isDisplayed(), "Trade in assessment is not visible");
			Assert.assertTrue(originalEstimatedValue.isDisplayed(), "Original estimated value is not visible");
			Assert.assertTrue(orderPlaced.isDisplayed(), "Order placed is not visible");
			Assert.assertTrue(tradeInHistory.isDisplayed(), "Trade in history is not visible");
			Reporter.log("Trade in details are displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Trade in details");
		}
		return this;
	}
	
	/**
	 * Verify DRP Link
	 * 
	 * @return
	 */
	public CheckOrderPage verifyDrpLink() {
		try {
			Assert.assertTrue(drpLink.isDisplayed(), "DRP link not visible");
			Reporter.log("DRP link is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display DRP link");
		}
		return this;
	}
	
	/**
	 * Verify Print Shipping Label
	 * 
	 * @return
	 */
	public CheckOrderPage verifyPrintShippingLabel() {
		try {
			Assert.assertTrue(printShippingLabel.isDisplayed(), "Print Shipping Label not visible");
			Reporter.log("Print Shipping Label is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Print Shipping Label");
		}
		return this;
	}
	
	/**
	 * Verify Offer Label
	 * 
	 * @return
	 */
	public CheckOrderPage verifyOfferLabel() {
		try {
			Assert.assertTrue(offerLabelText.isDisplayed(), "Offer Label not visible");
			Reporter.log("Offer is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Offer Label");
		}
		return this;
	}
	
	/**
	 * Verify Received Accessing Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyReceivedAccessingText() {
		try {
			Assert.assertTrue(receivedAccessingText.isDisplayed(), "received accessing text not visible");
			Reporter.log("received accessing text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display received accessing text");
		}
		return this;
	}
	
	/**
	 * Verify processing credit Text
	 * 
	 * @return
	 */
	public CheckOrderPage verifyProcessingCreditText() {
		try {
			Assert.assertTrue(processingCreditText.isDisplayed(), "processing credit text not visible");
			Reporter.log("processing credit text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display processing credit text");
		}
		return this;
	}
	
	/**
	 * Verify complete credit applied
	 * 
	 * @return
	 */
	public CheckOrderPage verifyCompleteCreditApplied() {
		try {
			Assert.assertTrue(completeCreditApplied.isDisplayed(), "Complete Credit Applied not visible");
			Reporter.log("Complete Credit Applied is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Complete Credit Applied");
		}
		return this;
	}
	
	/**
	 * Verify Processing Return
	 * 
	 * @return
	 */
	public CheckOrderPage verifyProcesingReturn() {
		try {
			Assert.assertTrue(processingReturn.isDisplayed(), "Processing Return not visible");
			Reporter.log("Processing Return is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Processing Return");
		}
		return this;
	}
	
	/**
	 *verifyTradeInDetailsSection
	 * 
	 * @return
	 */
	
	public CheckOrderPage verifyTradeInDetailsSection() {
	try {
		waitFor(ExpectedConditions.visibilityOf(tradeInSection));
		Assert.assertTrue(tradeInSection.isDisplayed(), "Trade in section is not displayed");
		Reporter.log("Trade in section is displayed");
	} catch (Exception e) {
		Assert.fail("Failed to display Trade in section ");
	}
	return this;
}

}
