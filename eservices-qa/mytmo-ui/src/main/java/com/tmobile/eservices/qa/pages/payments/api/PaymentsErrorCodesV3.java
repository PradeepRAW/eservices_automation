package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class PaymentsErrorCodesV3 extends ApiCommonLib{
	
	/***
	 * Summary: This API is used to test the Error Codes for Payments API
	 * Headers:Authorization,
	 * 	-Content-Type ,
	 * 	-activityId,
	 * 	-interactionId ,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-application_id , 
	 * 	
	 *  
	 *  
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	
	
	private Map<String, String> buildErrorCodesHeader(ApiTestData apiTestData) throws Exception {
		//clearCounters(apiTestData);
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Postman-Token","9d782f3b-76c7-4c75-a2ae-d2322847451c");
		headers.put("Authorization", getAccessToken());
		headers.put("Content-Type", "application/json");
		headers.put("cache-control","no-cache");
		
	
		return headers;
	}
	    
	public Response paymentErrorCodes(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildErrorCodesHeader(apiTestData);
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("payments/v3/fdp", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}
    
    
}
