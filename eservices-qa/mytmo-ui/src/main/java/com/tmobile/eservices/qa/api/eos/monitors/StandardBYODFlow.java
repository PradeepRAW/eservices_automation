package com.tmobile.eservices.qa.api.eos.monitors;

import org.testng.Reporter;

import com.tmobile.eservices.qa.api.exception.ServiceFailureException;
import com.tmobile.eservices.qa.common.ShopConstants;
import com.tmobile.eservices.qa.data.ApiTestData;

public class StandardBYODFlow extends OrderProcess {

	/*
	 * 
	 * Steps For BYODFlow Flow, 1. getCatalogProducts 2. Create Cart 3. Update
	 * Cart 4. Create Quote 5. Update Quote 6. Make Payment 7. Place Order
	 * 
	 */

	@Override
	public void createCart(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(
					ShopConstants.SHOP_MONITORS + "createCartAPI_BYOD_For_Monitors.txt");
			invokeCreateCart(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while making Create Cart : " + e);
			throwServiceFailException();
		}
	}

	@Override
	public void updateCart(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "UpdateCart_BYOD_Monitors.txt");
			invokeUpdateCart(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue during Update Cart : " + e);
			throwServiceFailException();
		}
	}

	@Override
	public void updateQuote(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "UpdateQuote_FRP_req.txt");
			invokeUpdateQuote(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while invoking the Update Quote : " + e);
			throwServiceFailException();
		}
	}

}
