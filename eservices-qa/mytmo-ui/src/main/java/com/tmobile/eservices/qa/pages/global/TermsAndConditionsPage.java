/**
 * 
 */
package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 *
 */
public class TermsAndConditionsPage extends CommonPage {

	public static final String termsAndConditionPageUrl = "/terms-and-conditions";

	/**
	 * 
	 * @param webDriver
	 */
	public TermsAndConditionsPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
	 * Verify Terms And Conditions Page.
	 */
	public TermsAndConditionsPage verifyTermsAndConditionsPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(termsAndConditionPageUrl);
			Reporter.log("Terms & Conditions page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Terms & Conditions page is not displayed");
		}
		return this;

	}
}
