
package com.tmobile.eservices.qa.pages.tmng.functional;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class DealsPage extends TmngCommonPage {
	private Map<String, String> data;
	private WebDriver driver;
	private int timeout = 30;

	private final String pageUrl = "/offers/deals-hub";

	@FindBy(css = "#slick-slide00 button")
	private WebElement _1;

	@FindBy(css = "#slick-slide01 button")
	private WebElement _2;

	@FindBy(css = "#slick-slide02 button")
	private WebElement _3;

	@FindBy(css = "a[href='//www.t-mobile.com/about-us']")
	private WebElement about;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/accessibility-policy']")
	private WebElement accessibility;

	@FindBy(css = "a[href='https://www.t-mobile.com/hub/accessories-hub?icid=WMM_TM_Q417UNAVME_DJSZDFWU45Q11780']")
	private WebElement accessories1;

	@FindBy(css = "#b42e02d8a899638fc5cf51bbd84d1d3092cba6a9 div:nth-of-type(2) ul li:nth-of-type(4) button")
	private WebElement accessories2;

	@FindBy(css = "a[href='//prepaid-phones.t-mobile.com/prepaid-activate']")
	private WebElement activateYourPrepaidPhoneOrDevice;

	@FindBy(css = "#150-and-under div:nth-of-type(1) div.slick-list.draggable div.slick-track div:nth-of-type(2) div.large-grid.light-bg a.product-link-block")
	private WebElement alcatelA30Tablet8inch144001;

	@FindBy(css = "#150-and-under div:nth-of-type(1) div.slick-list.draggable div.slick-track div:nth-of-type(9) div.large-grid.light-bg a.product-link-block")
	private WebElement alcatelA30Tablet8inch144002;

	@FindBy(css = "#150-and-under div:nth-of-type(1) div.slick-list.draggable div.slick-track div:nth-of-type(3) div.large-grid.dark-bg a.product-link-block")
	private WebElement alcatelGoFlip75001;

	@FindBy(css = "#150-and-under div:nth-of-type(1) div.slick-list.draggable div.slick-track div:nth-of-type(10) div.large-grid.dark-bg a.product-link-block")
	private WebElement alcatelGoFlip75002;

	@FindBy(css = "#150-and-under div:nth-of-type(1) div.slick-list.draggable div.slick-track div:nth-of-type(6) div.large-grid.light-bg a.product-link-block")
	private WebElement alcatelLinkzone4gLteMobile1;

	@FindBy(css = "#150-and-under div:nth-of-type(1) div.slick-list.draggable div.slick-track div:nth-of-type(13) div.large-grid.light-bg a.product-link-block")
	private WebElement alcatelLinkzone4gLteMobile2;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/bring-your-own-phone?icid=WMM_TM_Q417UNAVME_2JDVKVMA5T12838']")
	private WebElement bringYourOwnPhone;

	@FindBy(css = "a[href='//business.t-mobile.com/']")
	private WebElement business1;

	@FindBy(css = "a[href='https://business.t-mobile.com/?icid=WMM_TM_Q417UNAVME_I44PB47UAS11783']")
	private WebElement business2;

	@FindBy(css = "a.tat17237_TopA.tmo_tfn_number")
	private WebElement callUs;

	@FindBy(css = "a[href='//www.t-mobile.com/careers']")
	private WebElement careers;

	@FindBy(css = "a[href='/offers/apple-iphone-deals?icid=WMM_TM_18APPL6S_D215T2G8ES013949']")
	private WebElement checkItOut1;

	@FindBy(css = "a[href='/cell-phone/samsung-galaxy-note8?icid=WMD_TMNG_18BCK2SCHL_Q4EGULACA3913979']")
	private WebElement checkItOut2;

	@FindBy(css = "a[href='/cell-phone/samsung-galaxy-s8?icid=WMD_TMNG_18BCK2SCHL_NHZIVKJZSNW13975']")
	private WebElement checkItOut3;

	@FindBy(css = "a[href='/accessories/beats?icid=WMM_TMNG_BEATSXWITH_CX6L0BTJBWH13986#navBar']")
	private WebElement checkItOut4;

	@FindBy(css = "a[href='/offers/apple-watch-deals?icid=WMD_TMNG_18APPLEW_DOJ34LCAKJ312136']")
	private WebElement checkItOut5;

	@FindBy(css = "a[href='/cell-phones/lg?icid=WMD_TM_18Q3LGBOGO_ET172G5TEUW13906#lshop_menu_1']")
	private WebElement checkItOut6;

	@FindBy(css = "a[href='/internet-device/lg-g-pad-x2-80-plus?memory=32gb&icid=WMD_TM_Q3LGGPAD_JP35JHD75N713883']")
	private WebElement checkItOut7;

	@FindBy(css = "a[href='http://www.t-mobile.com/orderstatus/default.aspx']")
	private WebElement checkOrderStatus;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/4g-lte-network?icid=WMM_TM_Q417UNAVME_FPAO3ZN8J3811775']")
	private WebElement checkOutTheCoverage;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info']")
	private WebElement consumerInformation;

	@FindBy(css = "a[href='http://www.t-mobile.com/contact-us.html']")
	private WebElement contactInformation;

	@FindBy(css = "a[href='//www.t-mobile.com/offers/deals-hub?icid=WMM_TMNG_Q416DLSRDS_O8RLYN4ZJ4I6275']")
	private WebElement deals1;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/deals-hub?icid=WMM_TM_Q417UNAVME_QXMF61Q49FA11771']")
	private WebElement deals2;

	@FindBy(css = "a[href='//www.telekom.com/']")
	private WebElement deutscheTelekom;

	@FindBy(css = "a[href='//support.t-mobile.com/community/phones-tablets-devices']")
	private WebElement deviceSupport;

	@FindBy(id = "3cc7fe53ccda9c02c4b6cc07b33d65859e530691-email")
	private WebElement email;

	@FindBy(css = "a[href='https://es.t-mobile.com/?icid=WMM_TM_Q417UNAVME_KG7W6OINJH11785']")
	private WebElement espanol;

	@FindBy(css = "a[href='//es.t-mobile.com/']")
	private WebElement espaol;

	@FindBy(css = "a.lang-toggle-wrapper")
	private WebElement espaolEspaol;

	@FindBy(css = "a.btn-secondary.btn-brand.btn")
	private WebElement findAStore1;

	@FindBy(css = "a[href='http://www.t-mobile.com/store-locator.html']")
	private WebElement findAStore2;

	@FindBy(id = "3cc7fe53ccda9c02c4b6cc07b33d65859e530691-firstName")
	private WebElement firstName;

	@FindBy(css = "a[href='http://www.t-mobile.com/promotions']")
	private WebElement getARebate;

	@FindBy(css = "a[href='/offers/apple-iphone-bogo?icid=WMD_TMNG_18APPLBOGO_14WEUNI2EF13390']")
	private WebElement getTheDetails;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx#choicesaboutadvertising']")
	private WebElement interestbasedAds;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/international-calling?icid=WMM_TM_Q118UNAVIN_9ZQGKL9LLZD12592']")
	private WebElement internationalCalling;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global.html']")
	private WebElement internationalRates;

	@FindBy(css = "a[href='http://iot.t-mobile.com/']")
	private WebElement internetOfThings;

	@FindBy(css = "a[href='http://investor.t-mobile.com/CorporateProfile.aspx?iid=4091145']")
	private WebElement investorRelations;

	@FindBy(id = "3cc7fe53ccda9c02c4b6cc07b33d65859e530691-lastName")
	private WebElement lastName;

	@FindBy(css = ".marketing-page.ng-scope header.global-header div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(3) a.tmo_tfn_number")
	private WebElement letsTalk1800tmobile;

	@FindBy(css = "a.tat17237_TopA.tat17237_CustLogInIconCont")
	private WebElement logIn;

	@FindBy(css = "button.hamburger.hamburger-border")
	private WebElement menuMenu;

	@FindBy(css = "a[href='//my.t-mobile.com/?icid=WMD_TMNG_HMPGRDSGNU_S6FV9BEA3L36130']")
	private WebElement myTmobile;

	@FindBy(css = "button.slick-next.slick-arrow")
	private WebElement nextSlide;

	@FindBy(name = "customer")
	private List<WebElement> no;

	private final String noValue = "0";

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/policies/internet-service']")
	private WebElement openInternet;

	@FindBy(id = "3cc7fe53ccda9c02c4b6cc07b33d65859e530691-phone")
	private WebElement phoneNumber;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phones?icid=WMM_TM_HMPGRDSGNA_P7QGF8N5L3B5877']")
	private WebElement phones1;

	@FindBy(css = "#b42e02d8a899638fc5cf51bbd84d1d3092cba6a9 div:nth-of-type(2) ul li:nth-of-type(2) button")
	private WebElement phones2;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q117TMO1PL_H85BRNKTDO37510']")
	private WebElement plans;

	@FindBy(css = "a[href='//support.t-mobile.com/community/plans-services']")
	private WebElement plansServices;

	@FindBy(css = ".marketing-page.ng-scope header.global-header div:nth-of-type(1) ul:nth-of-type(1) li:nth-of-type(2) a")
	private WebElement prepaid1;

	@FindBy(css = "#overlay-menu ul:nth-of-type(2) li:nth-of-type(2) a.tat17237_BotA")
	private WebElement prepaid2;

	@FindBy(css = "a[href='//www.t-mobile.com/news']")
	private WebElement press;

	@FindBy(css = "button.slick-prev.slick-arrow.ng-scope.slick-disabled")
	private WebElement previousSlide;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/privacy']")
	private WebElement privacyCenter;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx']")
	private WebElement privacyStatement;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/safety/9-1-1']")
	private WebElement publicSafety911;

	@FindBy(css = "a[href='http://www.t-mobilepr.com/']")
	private WebElement puertoRico;

	@FindBy(css = "a[href='//support.t-mobile.com/community/billing']")
	private WebElement questionsAboutYourBill;

	@FindBy(css = "a[href='https://prepaid-phones.t-mobile.com/prepaid-refill-api-bridging']")
	private WebElement refillYourPrepaidAccount;

	@FindBy(css = ".marketing-page.ng-scope div:nth-of-type(8) button")
	private WebElement scrollToTop;

	@FindBy(id = "searchText")
	private WebElement search1;

	@FindBy(id = "searchText")
	private WebElement search2;

	@FindBy(css = "#a223cce7b48f81504fb14140255e14806822db5b div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms1;

	@FindBy(css = "#d91ce2bdfa294430985978f5892531f3377f4e93 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms2;

	@FindBy(css = "#699c225e560134e4b06bd9f379225864a7386739 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms3;

	@FindBy(css = "#cdc74cf3109b1961652c337598da502a1d5e7025 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms4;

	@FindBy(css = "#c81cb0ee2132dddbc9d21910a1aac88002881baa div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms5;

	@FindBy(css = "#4033b69018cc49c842b8b715b8d42d38927c2193 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms6;

	@FindBy(css = "#33cfb5cc4bafebe9c1e25adf2b05775381841c96 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms7;

	@FindBy(css = "#e51da22ad1b43693f00cf78e24fbdd0102f7ab1a div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) p.section-legal.col-xs-12 a.tmo-modal-link")
	private WebElement seeFullTerms8;

	@FindBy(css = "a[href='https://www.t-mobile.com/switch-to-t-mobile?icid=WMM_TM_Q417UNAVME_QAE0VIGZ14T11772']")
	private WebElement seeHowMuchYouCouldSave;

	@FindBy(css = "a[href='/accessories/category-on-sale?icid=WMD_TMNG_Q118ACCESS_B1D9QUT0EPN11738#navBar']")
	private WebElement shopNow;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phones?icid=WMM_TM_Q417UNAVME_OXNEXWO4KCB11769']")
	private WebElement shopPhones;

	@FindBy(css = "a[href='#divfootermain']")
	private WebElement skipToFooter;

	@FindBy(css = "a[href='#maincontent']")
	private WebElement skipToMainContent;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-of-things?icid=WMM_TM_Q417UNAVME_SZBCRCV105G11778']")
	private WebElement smartDevices;

	@FindBy(css = "a[href='//www.t-mobile.com/store-locator.html']")
	private WebElement storeLocator;

	@FindBy(css = "a[href='https://www.t-mobile.com/store-locator?icid=WMM_TM_Q417UNAVME_JP4FGXM0A6211900']")
	private WebElement stores;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/student-teacher-smartphone-tablet-discounts?icid=WMM_TM_CAMPUSEXCL_UHTZZ7GR1RT11028']")
	private WebElement studentteacherDiscount;

	@FindBy(css = "button[class*='btn-primary']")
	private WebElement submit;

	@FindBy(css = ".marketing-page.ng-scope header.global-header nav.main-nav.content-wrap.clearfix div:nth-of-type(2) div:nth-of-type(3) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch1;

	@FindBy(css = ".marketing-page.ng-scope header.global-header div:nth-of-type(4) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch2;

	@FindBy(css = "a[href='//support.t-mobile.com/']")
	private WebElement supportHome;

	@FindBy(css = "#b42e02d8a899638fc5cf51bbd84d1d3092cba6a9 div:nth-of-type(2) ul li:nth-of-type(3) button")
	private WebElement tablets;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions&print=true']")
	private WebElement termsConditions;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsOfUse&print=true']")
	private WebElement termsOfUse;

	@FindBy(css = "a[href='https://business.t-mobile.com']")
	private WebElement tmobileForBusiness;

	@FindBy(css = "a[href='/cell-phone/t-mobile-linelink-home-phone-adapter?icid=WMD_TMNG_Q118ECOMME_IGUJ017H0A611791']")
	private WebElement tmobileLinelinkHomePhoneAdapter;

	@FindBy(css = "#150-and-under div:nth-of-type(1) div.slick-list.draggable div.slick-track div:nth-of-type(5) div.large-grid.dark-bg a.product-link-block")
	private WebElement tmobileRevvl150001;

	@FindBy(css = "#150-and-under div:nth-of-type(1) div.slick-list.draggable div.slick-track div:nth-of-type(12) div.large-grid.dark-bg a.product-link-block")
	private WebElement tmobileRevvl150002;

	@FindBy(css = "#150-and-under div:nth-of-type(1) div.slick-list.draggable div.slick-track div:nth-of-type(4) div.large-grid.dark-bg a.product-link-block")
	private WebElement tmobileRevvlCertifiedPreowned96001;

	@FindBy(css = "#150-and-under div:nth-of-type(1) div.slick-list.draggable div.slick-track div:nth-of-type(11) div.large-grid.dark-bg a.product-link-block")
	private WebElement tmobileRevvlCertifiedPreowned96002;

	@FindBy(css = "#150-and-under div:nth-of-type(1) div.slick-list.draggable div.slick-track div:nth-of-type(1) div.large-grid.dark-bg a.product-link-block")
	private WebElement tmobileSyncupDrive120001;

	@FindBy(css = "#150-and-under div:nth-of-type(1) div.slick-list.draggable div.slick-track div:nth-of-type(8) div.large-grid.dark-bg a.product-link-block")
	private WebElement tmobileSyncupDrive120002;

	@FindBy(css = "a[href='http://www.t-mobile.com/cell-phone-trade-in.html']")
	private WebElement tradeInProgram;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global?icid=WMM_TM_Q417UNAVME_AE1B0D6WUNH11777']")
	private WebElement travelingAbroad;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q417UNAVME_GHBARMBPJEO11768']")
	private WebElement unlimitedPlan;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/t-mobile-digits?icid=WMM_TM_Q417UNAVME_VEBBQDCSN9W11779']")
	private WebElement useMultipleNumbersOnMyPhone;

	@FindBy(css = "button.active")
	private WebElement viewAll;

	@FindBy(css = "a[href='http://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_ReturnPolicy&print=true']")
	private WebElement viewReturnPolicy;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-devices?icid=WMM_TM_Q417UNAVME_KX7JJ8E0YH11770']")
	private WebElement watchesTablets;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/how-to-join-us?icid=WMM_TM_Q417UNAVME_7ZKBD6XWQ712837']")
	private WebElement wellHelpYouJoin;

	@FindBy(name = "customer")
	private List<WebElement> yes;

	private final String yesValue = "1";

	@FindBy(id = "3cc7fe53ccda9c02c4b6cc07b33d65859e530691-zip")
	private WebElement zip;
	
	@FindBy(css = "div.input-wrapper [ng-model='vm.formData.firstName']")
	private WebElement firstname;
	
	@FindBy(css = "div.input-wrapper [ng-model='vm.formData.lastName']")
	private WebElement lastname;
	
	@FindBy(xpath = "//p[contains(text(),'Please enter a correct email')]")
	private WebElement emailErrorMessage;
	

	public DealsPage(WebDriver webDriver) {
		super(webDriver);
	}

	public DealsPage(WebDriver driver, Map<String, String> data) {
		this(driver);
		this.data = data;
	}

	public DealsPage(WebDriver driver, Map<String, String> data, int timeout) {
		this(driver, data);
		this.timeout = timeout;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage verifyPageLoaded() {
		try {
			verifyPageUrl();
			Reporter.log("Deals page loaded");

		} catch (Exception ex) {
			Assert.fail("Deals Page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		return this;
	}

	/**
	 * Click on About Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickAboutLink() {
		about.click();
		return this;
	}

	/**
	 * Click on Accessibility Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickAccessibilityLink() {
		accessibility.click();
		return this;
	}

	/**
	 * Click on Accessories Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickAccessories1Button() {
		accessories1.click();
		return this;
	}

	/**
	 * Click on Accessories Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickAccessories2Button() {
		accessories2.click();
		return this;
	}

	/**
	 * Click on Activate Your Prepaid Phone Or Device Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickActivateYourPrepaidPhoneOrDeviceLink() {
		activateYourPrepaidPhoneOrDevice.click();
		return this;
	}

	/**
	 * Click on Alcatel A30 Tablet 8inch 144.00 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickAlcatelA30Tablet8inch144001Link() {
		alcatelA30Tablet8inch144001.click();
		return this;
	}

	/**
	 * Click on Alcatel A30 Tablet 8inch 144.00 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickAlcatelA30Tablet8inch144002Link() {
		alcatelA30Tablet8inch144002.click();
		return this;
	}

	/**
	 * Click on Alcatel Go Flip 75.00 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickAlcatelGoFlip75001Link() {
		alcatelGoFlip75001.click();
		return this;
	}

	/**
	 * Click on Alcatel Go Flip 75.00 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickAlcatelGoFlip75002Link() {
		alcatelGoFlip75002.click();
		return this;
	}

	/**
	 * Click on Alcatel Linkzone 4g Lte Mobile Hotspot 72.00 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickAlcatelLinkzone4gLteMobile1Link() {
		alcatelLinkzone4gLteMobile1.click();
		return this;
	}

	/**
	 * Click on Alcatel Linkzone 4g Lte Mobile Hotspot 72.00 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickAlcatelLinkzone4gLteMobile2Link() {
		alcatelLinkzone4gLteMobile2.click();
		return this;
	}

	/**
	 * Click on Bring Your Own Phone Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickBringYourOwnPhoneLink() {
		bringYourOwnPhone.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickBusiness1Link() {
		business1.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickBusiness2Link() {
		business2.click();
		return this;
	}

	/**
	 * Click on 1 Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickButton1() {
		_1.click();
		return this;
	}

	/**
	 * Click on 2 Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickButton2() {
		_2.click();
		return this;
	}

	/**
	 * Click on 3 Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickButton3() {
		_3.click();
		return this;
	}

	/**
	 * Click on Call Us Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickCallUsLink() {
		callUs.click();
		return this;
	}

	/**
	 * Click on Careers Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickCareersLink() {
		careers.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickCheckItOut1Link() {
		checkItOut1.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickCheckItOut2Link() {
		checkItOut2.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickCheckItOut3Link() {
		checkItOut3.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickCheckItOut4Link() {
		checkItOut4.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickCheckItOut5Link() {
		checkItOut5.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickCheckItOut6Link() {
		checkItOut6.click();
		return this;
	}

	/**
	 * Click on Check It Out Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickCheckItOut7Link() {
		checkItOut7.click();
		return this;
	}

	/**
	 * Click on Check Order Status Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickCheckOrderStatusLink() {
		checkOrderStatus.click();
		return this;
	}

	/**
	 * Click on Check Out The Coverage Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickCheckOutTheCoverageLink() {
		checkOutTheCoverage.click();
		return this;
	}

	/**
	 * Click on Consumer Information Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickConsumerInformationLink() {
		consumerInformation.click();
		return this;
	}

	/**
	 * Click on Contact Information Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickContactInformationLink() {
		contactInformation.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickDeals1Link() {
		deals1.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickDeals2Link() {
		deals2.click();
		return this;
	}

	/**
	 * Click on Deutsche Telekom Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickDeutscheTelekomLink() {
		deutscheTelekom.click();
		return this;
	}

	/**
	 * Click on Device Support Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickDeviceSupportLink() {
		deviceSupport.click();
		return this;
	}

	/**
	 * Click on Espanol Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickEspanolLink() {
		espanol.click();
		return this;
	}

	/**
	 * Click on Espaol Espaol Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickEspaolEspaolLink() {
		espaolEspaol.click();
		return this;
	}

	/**
	 * Click on Espaol Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickEspaolLink() {
		espaol.click();
		return this;
	}

	/**
	 * Click on Find A Store Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickFindAStore1Link() {
		findAStore1.click();
		return this;
	}

	/**
	 * Click on Find A Store Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickFindAStore2Link() {
		findAStore2.click();
		return this;
	}

	/**
	 * Click on Get A Rebate Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickGetARebateLink() {
		getARebate.click();
		return this;
	}

	/**
	 * Click on Get The Details Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickGetTheDetailsLink() {
		getTheDetails.click();
		return this;
	}

	/**
	 * Click on Interestbased Ads Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickInterestbasedAdsLink() {
		interestbasedAds.click();
		return this;
	}

	/**
	 * Click on International Calling Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickInternationalCallingLink() {
		internationalCalling.click();
		return this;
	}

	/**
	 * Click on International Rates Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickInternationalRatesLink() {
		internationalRates.click();
		return this;
	}

	/**
	 * Click on Internet Of Things Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickInternetOfThingsLink() {
		internetOfThings.click();
		return this;
	}

	/**
	 * Click on Investor Relations Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickInvestorRelationsLink() {
		investorRelations.click();
		return this;
	}

	/**
	 * Click on Lets Talk 1800tmobile Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickLetsTalk1800tmobileLink() {
		letsTalk1800tmobile.click();
		return this;
	}

	/**
	 * Click on Log In Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickLogInLink() {
		logIn.click();
		return this;
	}

	/**
	 * Click on Menu Menu Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickMenuMenuButton() {
		menuMenu.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickMyTmobileLink() {
		myTmobile.click();
		return this;
	}

	/**
	 * Click on Next Slide Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickNextSlideButton() {
		nextSlide.click();
		return this;
	}

	/**
	 * Click on Open Internet Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickOpenInternetLink() {
		openInternet.click();
		return this;
	}

	/**
	 * Click on Phones Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickPhones1Button() {
		phones1.click();
		return this;
	}

	/**
	 * Click on Phones Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickPhones2Button() {
		phones2.click();
		return this;
	}

	/**
	 * Click on Plans Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickPlansLink() {
		plans.click();
		return this;
	}

	/**
	 * Click on Plans Services Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickPlansServicesLink() {
		plansServices.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickPrepaid1Link() {
		prepaid1.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickPrepaid2Link() {
		prepaid2.click();
		return this;
	}

	/**
	 * Click on Press Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickPressLink() {
		press.click();
		return this;
	}

	/**
	 * Click on Previous Slide Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickPreviousSlideButton() {
		previousSlide.click();
		return this;
	}

	/**
	 * Click on Privacy Center Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickPrivacyCenterLink() {
		privacyCenter.click();
		return this;
	}

	/**
	 * Click on Privacy Statement Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickPrivacyStatementLink() {
		privacyStatement.click();
		return this;
	}

	/**
	 * Click on Public Safety911 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickPublicSafety911Link() {
		publicSafety911.click();
		return this;
	}

	/**
	 * Click on Puerto Rico Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickPuertoRicoLink() {
		puertoRico.click();
		return this;
	}

	/**
	 * Click on Questions About Your Bill Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickQuestionsAboutYourBillLink() {
		questionsAboutYourBill.click();
		return this;
	}

	/**
	 * Click on Refill Your Prepaid Account Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickRefillYourPrepaidAccountLink() {
		refillYourPrepaidAccount.click();
		return this;
	}

	/**
	 * Click on Scroll To Top Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickScrollToTopButton() {
		scrollToTop.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSeeFullTerms1Link() {
		seeFullTerms1.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSeeFullTerms2Link() {
		seeFullTerms2.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSeeFullTerms3Link() {
		seeFullTerms3.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSeeFullTerms4Link() {
		seeFullTerms4.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSeeFullTerms5Link() {
		seeFullTerms5.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSeeFullTerms6Link() {
		seeFullTerms6.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSeeFullTerms7Link() {
		seeFullTerms7.click();
		return this;
	}

	/**
	 * Click on See Full Terms Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSeeFullTerms8Link() {
		seeFullTerms8.click();
		return this;
	}

	/**
	 * Click on See How Much You Could Save Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSeeHowMuchYouCouldSaveLink() {
		seeHowMuchYouCouldSave.click();
		return this;
	}

	/**
	 * Click on Shop Now Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickShopNowLink() {
		shopNow.click();
		return this;
	}

	/**
	 * Click on Shop Phones Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickShopPhonesLink() {
		shopPhones.click();
		return this;
	}

	/**
	 * Click on Skip To Footer Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSkipToFooterLink() {
		skipToFooter.click();
		return this;
	}

	/**
	 * Click on Skip To Main Content Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSkipToMainContentLink() {
		skipToMainContent.click();
		return this;
	}

	/**
	 * Click on Smart Devices Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSmartDevicesLink() {
		smartDevices.click();
		return this;
	}

	/**
	 * Click on Store Locator Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickStoreLocatorLink() {
		storeLocator.click();
		return this;
	}

	/**
	 * Click on Stores Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickStoresLink() {
		stores.click();
		return this;
	}

	/**
	 * Click on Studentteacher Discount Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickStudentteacherDiscountLink() {
		studentteacherDiscount.click();
		return this;
	}

	/**
	 * Click on Submit Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSubmitButton() {	
		try {
			checkPageIsReady();
			submit.click();
			Reporter.log("Clicked submit button");
		}catch (Exception e) {
			Assert.fail("Failed to click Submit button");
		}
	
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSubmitSearch1Button() {
		submitSearch1.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSubmitSearch2Button() {
		submitSearch2.click();
		return this;
	}

	/**
	 * Click on Support Home Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickSupportHomeLink() {
		supportHome.click();
		return this;
	}

	/**
	 * Click on Tablets Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTabletsButton() {
		tablets.click();
		return this;
	}

	/**
	 * Click on Terms Conditions Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTermsConditionsLink() {
		termsConditions.click();
		return this;
	}

	/**
	 * Click on Terms Of Use Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTermsOfUseLink() {
		termsOfUse.click();
		return this;
	}

	/**
	 * Click on Tmobile For Business Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTmobileForBusinessLink() {
		tmobileForBusiness.click();
		return this;
	}

	/**
	 * Click on Tmobile Linelink Home Phone Adapter 29.99 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTmobileLinelinkHomePhoneAdapterLink() {
		tmobileLinelinkHomePhoneAdapter.click();
		return this;
	}

	/**
	 * Click on Tmobile Revvl 150.00 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTmobileRevvl150001Link() {
		tmobileRevvl150001.click();
		return this;
	}

	/**
	 * Click on Tmobile Revvl 150.00 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTmobileRevvl150002Link() {
		tmobileRevvl150002.click();
		return this;
	}

	/**
	 * Click on Tmobile Revvl Certified Preowned 96.00 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTmobileRevvlCertifiedPreowned96001Link() {
		tmobileRevvlCertifiedPreowned96001.click();
		return this;
	}

	/**
	 * Click on Tmobile Revvl Certified Preowned 96.00 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTmobileRevvlCertifiedPreowned96002Link() {
		tmobileRevvlCertifiedPreowned96002.click();
		return this;
	}

	/**
	 * Click on Tmobile Syncup Drive 120.00 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTmobileSyncupDrive120001Link() {
		tmobileSyncupDrive120001.click();
		return this;
	}

	/**
	 * Click on Tmobile Syncup Drive 120.00 Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTmobileSyncupDrive120002Link() {
		tmobileSyncupDrive120002.click();
		return this;
	}

	/**
	 * Click on Trade In Program Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTradeInProgramLink() {
		tradeInProgram.click();
		return this;
	}

	/**
	 * Click on Traveling Abroad Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickTravelingAbroadLink() {
		travelingAbroad.click();
		return this;
	}

	/**
	 * Click on Unlimited Plan Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickUnlimitedPlanLink() {
		unlimitedPlan.click();
		return this;
	}

	/**
	 * Click on Use Multiple Numbers On My Phone Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickUseMultipleNumbersOnMyPhoneLink() {
		useMultipleNumbersOnMyPhone.click();
		return this;
	}

	/**
	 * Click on View All Button.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickViewAllButton() {
		viewAll.click();
		return this;
	}

	/**
	 * Click on View Return Policy Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickViewReturnPolicyLink() {
		viewReturnPolicy.click();
		return this;
	}

	/**
	 * Click on Watches Tablets Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickWatchesTabletsLink() {
		watchesTablets.click();
		return this;
	}

	/**
	 * Click on Well Help You Join Link.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage clickWellHelpYouJoinLink() {
		wellHelpYouJoin.click();
		return this;
	}

	/**
	 * Fill every fields in the page.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage fill() {
		setSearch1TextField();
		setSearch2TextField();
		setFirstNameTextField();
		setLastNameTextField();
		setEmailEmailField();
		setZipNumberField();
		setYesRadioButtonField();
		setNoRadioButtonField();
		setPhoneNumberTelField();
		return this;
	}

	/**
	 * Fill every fields in the page and submit it to target page.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage fillAndSubmit() {
		fill();
		return submit();
	}

	/**
	 * Set default value to Email Email field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setEmailEmailField() {
		return setEmailEmailField(data.get("EMAIL"));
	}

	/**
	 * Set value to Email Email field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setEmailEmailField(String emailValue) {
		email.sendKeys(emailValue);
		return this;
	}

	/**
	 * Set default value to First Name Text field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setFirstNameTextField() {
		return setFirstNameTextField(data.get("FIRST_NAME"));
	}

	/**
	 * Set value to First Name Text field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setFirstNameTextField(String firstNameValue) {
		firstName.sendKeys(firstNameValue);
		return this;
	}

	/**
	 * Set default value to Last Name Text field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setLastNameTextField() {
		return setLastNameTextField(data.get("LAST_NAME"));
	}

	/**
	 * Set value to Last Name Text field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setLastNameTextField(String lastNameValue) {
		lastName.sendKeys(lastNameValue);
		return this;
	}

	/**
	 * Set default value to No Radio Button field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setNoRadioButtonField() {
		for (WebElement el : no) {
			if (el.getAttribute("value").equals(noValue)) {
				if (!el.isSelected()) {
					el.click();
				}
				break;
			}
		}
		return this;
	}

	/**
	 * Set default value to Phone Number Tel field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setPhoneNumberTelField() {
		return setPhoneNumberTelField(data.get("PHONE_NUMBER"));
	}

	/**
	 * Set value to Phone Number Tel field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setPhoneNumberTelField(String phoneNumberValue) {
		phoneNumber.sendKeys(phoneNumberValue);
		return this;
	}

	/**
	 * Set default value to Search Text field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setSearch1TextField() {
		return setSearch1TextField(data.get("SEARCH_1"));
	}

	/**
	 * Set value to Search Text field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setSearch1TextField(String search1Value) {
		search1.sendKeys(search1Value);
		return this;
	}

	/**
	 * Set default value to Search Text field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setSearch2TextField() {
		return setSearch2TextField(data.get("SEARCH_2"));
	}

	/**
	 * Set value to Search Text field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setSearch2TextField(String search2Value) {
		search2.sendKeys(search2Value);
		return this;
	}

	/**
	 * Set default value to Yes Radio Button field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setYesRadioButtonField() {
		for (WebElement el : yes) {
			if (el.getAttribute("value").equals(yesValue)) {
				if (!el.isSelected()) {
					el.click();
				}
				break;
			}
		}
		return this;
	}

	/**
	 * Set default value to Zip Number field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setZipNumberField() {
		return setZipNumberField(data.get("ZIP"));
	}

	/**
	 * Set value to Zip Number field.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage setZipNumberField(String zipValue) {
		zip.sendKeys(zipValue);
		return this;
	}

	/**
	 * Submit the form to target page.
	 *
	 * @return the DealsPage class instance.
	 */
	public DealsPage submit() {
		// clickSubmitSearchButton();
		DealsPage target = new DealsPage(driver, data, timeout);
		PageFactory.initElements(driver, target);
		return target;
	}
	
	/**
	 * Enter value in FirstName
	 *
	 */
	public DealsPage enterFirstName(String firstName) {
		try {								
			waitFor(ExpectedConditions.visibilityOf(firstname), 10);
			sendTextData(firstname, firstName);
		} catch (Exception e) {
			Assert.fail("FirstName field is not displayed ");
		}
		return this;
	}
	
	/**
	 * Enter value in LastName
	 *
	 */
	public DealsPage enterLastName(String LastName) {
		try {								
			waitFor(ExpectedConditions.visibilityOf(lastname), 10);
			sendTextData(lastname, LastName);
		} catch (Exception e) {
			Assert.fail("FirstName field is not displayed ");
		}
		return this;
	}
	
	/**
	 * Verify Email Error Message
	 */
	public DealsPage verifyEmailErrorMessage() {
		try {
			Assert.assertTrue(emailErrorMessage.isDisplayed(),"Email error message is not displayed");
			Reporter.log("Email error message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Email Error Message");
		}
		return this;
	}

}
