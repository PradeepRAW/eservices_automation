package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.data.Payment;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author pshiva
 *
 */
public class AddBankPage extends CommonPage {

	private Long accNumber = 0L;

	@FindAll({ @FindBy(css = "addbank div h4"), @FindBy(css = "p.Display3"), @FindBy(css = "div.bank-heading span") })
	private WebElement bankHeader;

	@FindBy(css = "i.ml-2")
	private WebElement toolTip;

	@FindBy(css = "span.tooltip-save")
	private WebElement toolTipText;

	@FindAll({ @FindBy(xpath = "//label[contains(@for,'account_name')]"), @FindBy(css = "label[for='accountName']"),
			@FindBy(css = "label[for='nameonCard']") })
	private WebElement nameOnAccountHeader;

	@FindAll({ @FindBy(xpath = "//label[contains(@for,'routing_number')]"), @FindBy(css = "label[for='routingNum']"),
			@FindBy(css = "label[for='routingNumber']") })
	private WebElement routingNumberHeader;

	@FindAll({ @FindBy(xpath = "//label[contains(@for,'account_number')]"), @FindBy(css = "label[for='accountNum']"),
			@FindBy(css = "label[for='accountNumber']") })
	private WebElement accountNumberHeader;

	@FindAll({ @FindBy(css = "#account_name"), @FindBy(css = "#accountName"), @FindBy(css = "#nameonCard"),
			@FindBy(id = "nameonCard") })
	private WebElement accountName;

	@FindBy(css = "div.overlay-in")
	private WebElement outOfModel;

	@FindBy(css = "span.mt-lg-1")
	private WebElement saveThispaymentErrorMsg;

	@FindBy(css = "div.xsmall.fine-print-body.ng-binding")
	private List<WebElement> replaceStoredPaymentAlert;

	@FindBy(className = "imgwidth")
	private WebElement howToFindModel;

	@FindAll({ @FindBy(css = "#routing_number"), @FindBy(css = "#routingNum"), @FindBy(css = "#routingNumber") })
	private WebElement routingNum;

	@FindAll({ @FindBy(css = "#account_number"), @FindBy(css = "#accountNum"), @FindBy(css = "#accountNumber") })
	private WebElement bankAccountNumber;

	@FindBy(linkText = "How to find my routing or account number")
	private WebElement howToFindLink;

	/*
	 * @FindBy(css = "span.invalid.text-error") private List<WebElement> errorMsg;
	 */

	@FindAll({ @FindBy(css = "span.invalid.text-error"),
			@FindBy(xpath = "//div[contains(@class,'errormsg tab-pressed ng-binding')]") })
	private List<WebElement> errorMsg;

	// div[contains(@class,'errormsg tab-pressed ng-binding')]

	@FindAll({ @FindBy(css = "button.btn.btn-primary.padding-bottom-large.primaryCTA.no-padding.width-md.ng-binding"),
			@FindBy(css = "button.PrimaryCTA") })
	private WebElement continueAddBank;

	@FindAll({ @FindBy(css = "div#back button"), @FindBy(css = "button.SecondaryCTA") })
	private WebElement backBtn;

	@FindAll({ @FindBy(css = "span.check1"), @FindBy(css = "span.slider") })
	private WebElement savePaymentCheckBox;

	@FindBy(id = "checkbox2")
	private WebElement savePaymentValue;

	@FindBy(css = "div.row div.Display5")
	private WebElement replaceModelHeader;

	@FindBy(css = "div.div.row div.black")
	private WebElement replaceModelSubHeader;

	@FindBy(css = "div.row span.p-l-10")
	private WebElement replaceModelCardNumber;

	@FindBy(css = "div.padding-horizontal-xsmall-md button.ml-2")
	private WebElement replaceModelContinueBtn;

	@FindBy(id = "cardImageModalButton")
	private WebElement toolTipLink;

	@FindBy(id = "cardImageModal")
	private WebElement toolTipInfoDiv;

	@FindBy(css = "button.uib-close")
	private WebElement toolTipInfoDivClose;

	@FindBy(css = ".paymentProcessing")
	private WebElement paymentProcessing;

	@FindAll({ @FindBy(css = "div[class='d-flex'] i+span"), @FindBy(css = "div.fine-print-body.ng-binding") })
	private WebElement errWalletsizereached;

	private final String pageUrl = "/addbank";

	@FindBy(css = "input#nickName")
	private WebElement nickName;

	@FindAll({ @FindBy(css = "label[for='nickName']") })
	private WebElement nickNameLabel;

	@FindAll({ @FindBy(css = "a[aria-label='optional field']") })
	private WebElement optionalLabel;

	@FindBy(css = "div.legal.indicator>span")
	private WebElement defaultCheckbox;

	@FindBy(xpath = "//span[contains(text(),'Set as default')]")
	private WebElement defaultCheckboxLabel;

	@FindBy(css = "button.PrimaryCTA")
	private WebElement saveToMyWalletCTA;

	@FindAll({ @FindBy(css = "input[formcontrolname='saveBankToggle']"), @FindBy(css = "#storedPaymentBankCheckbox") })
	private WebElement savePaymentCheckboxInput;

	/**
	 * Constructor
	 *
	 * @param webDriver
	 */
	public AddBankPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public AddBankPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @param addBankHeader
	 * 
	 * @throws InterruptedException
	 */
	public AddBankPage verifyBankPageLoaded(String addBankHeader) {
		try {
			checkPageIsReady();
			verifyPageUrl();
			// waitFor(ExpectedConditions.visibilityOf(paymentProcessing));
			waitFor(ExpectedConditions.elementToBeClickable(backBtn));
			bankHeader.isDisplayed();
			Assert.assertTrue(bankHeader.getText().equals(addBankHeader), "Bank page header mismatch");
			Reporter.log("Bank page is loaded and header is verified");
		} catch (Exception e) {
			Assert.fail("Bank page is  not loaded and Header is failed to verified");

		}
		return this;
	}

	/**
	 * Verify bank page header is displayed or not.
	 * 
	 */
	public AddBankPage verifyBankPageHeader() {
		try {

			bankHeader.isDisplayed();
			Assert.assertTrue(bankHeader.getText().equals("Bank information"));
			Reporter.log("Bank page header is verified");
		} catch (Exception e) {
			Assert.fail("Bank page header is failed to verified");
		}
		return this;
	}

	public AddBankPage verifyAddbankPageElements(String savePaymentErrorMsg, String toolTipMsg) {
		try {
			verifyAddBanklabels();
			verifyContinueCTAAddBank();
			verifyBackCTA();
			verifyHowToFindLink();
			clickHowToFindLink();
			verifyHowToFindModel();
			clickOutModel();
			verifySavePaymentMethodCheckBox();
			verifySavePaymentMethodCheckBoxIsUnSelected();
			selectSavePaymentOptionCheckBox();
			verifySaveThisPaymentErrorMessage(savePaymentErrorMsg);
			mouseHoverOnToolTip();
			verifyToolTipMsg(toolTipMsg);

			Reporter.log("Add bank page elements is verified");
		} catch (Exception e) {
			Assert.fail("Add bank page elements is failed to verified");
		}
		return this;
	}

	/**
	 * Fill all the bank details and click on continue
	 *
	 * @param payment
	 */
	public String fillBankInfo(Payment payment) {
		try {
			checkPageIsReady();
			accountName.sendKeys(payment.getCheckingName());
			routingNum.sendKeys(payment.getRoutingNumber());
			accNumber = generateRandomAccNumber();
			bankAccountNumber.sendKeys(accNumber.toString());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("circle")));
			nickName.sendKeys(generateRandomString());
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("circle")));
			Reporter.log("Filled the bank info details");
		} catch (Exception e) {
			Assert.fail("Failed to fill bank info details ");
		}
		return accNumber.toString();
	}

	public AddBankPage verifyAndSelectSavePaymentOption() {
		try {
			savePaymentCheckBox.isDisplayed();
			if (!savePaymentValue.isSelected()) {
				savePaymentCheckBox.click();
			}
			Reporter.log("Save payment method option is selected");
		} catch (Exception e) {
			Reporter.log("Save payment method option is not displayed");
		}
		return this;
	}

	public AddBankPage selectSavePaymentOptionCheckBox() {
		try {
			savePaymentCheckBox.isDisplayed();
			if (!savePaymentCheckBox.isSelected()) {
				savePaymentCheckBox.click();
			}

			Reporter.log("Save payment method option is selected");
		} catch (Exception e) {
			Verify.fail("Save payment method option is not displayed");
		}
		return this;
	}

	public AddBankPage clickOutModel() {

		try {
			Actions builder = new Actions(getDriver());
			// builder.moveToElement(outOfModel).doubleClick().build().perform();

			builder.moveByOffset(150, 200).doubleClick().build().perform();

			Reporter.log("clicked on out of model");

		} catch (Exception e) {
			Assert.fail("failed to click out of model");
		}
		return this;

	}

	/**
	 * click continue button for add bank
	 */
	public Long clickContinueAddBank() {
		try {
			checkPageIsReady();
			clickElement(continueAddBank);
			// continueAddBank.click();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("i.fa-spinner")));
			if (verifyErrorMsgIsDisplayed()) {
				accNumber = generateRandomAccNumber() + accNumber;
				bankAccountNumber.clear();
				bankAccountNumber.sendKeys(accNumber.toString());
				clickContinueAddBank();
			}
			Reporter.log("Clicked on continue Add Bank and set the Bank account number");
		} catch (Exception e) {
			Assert.fail("Failed to click on Continue Add Bank");
		}
		return accNumber;
	}

	private boolean verifyErrorMsgIsDisplayed() {
		try {
			for (WebElement error : errorMsg) {
				if (error.isDisplayed()) {
					return true;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * verify all bank page elements
	 */
	public AddBankPage verifyAddBankAllElements() {
		try {
			verifyNameOnAccountHeader();
			// getPlaceholder(accountName, "Name on Account");
			verifyRoutingNumberHeader();
			// getPlaceholder(routingNum, "9 digits");
			verifyAccountNumberHeader();
			// getPlaceholder(bankAccountNumber, "Up to 17 digits");
			verifyBankInfoToolTipLink("How to find my routing or account number");
			verifyContinueCTAAddBank();
			verifyBackCTA();
			Reporter.log("All elements are present on AddBank page");
		} catch (Exception e) {
			Verify.fail("Failed to verify all Bank elements");

		}
		return this;
	}

	/**
	 * validate all bank page elements
	 */
	public AddBankPage validateAddBankAllFields(String addBankHeader) {
		try {
			// continueAddBank.click();
			/*
			 * Verify.assertTrue(verifyErrorTextMessage("Enter a valid name."),
			 * "Blank Account name can be accepted");
			 * Verify.assertTrue(verifyErrorTextMessage("Enter a valid routing number."),
			 * "Blank Routing Number can be accepted");
			 * Verify.assertTrue(verifyErrorTextMessage("Enter a valid account number."),
			 * "Blank Account Number can be accepted");
			 */
			setAccountName("T");
			// continueAddBank.click();
			Verify.assertTrue(verifyErrorTextMessage("Enter a valid name."),
					"Account name with one character can be accepted");
			refreshandNavigateToAddBankPage(addBankHeader);

			setAccountName("characters limit for account name fieldis");

			checkmaximumcharbyfield(accountName, 40);

			// continueAddBank.click();
			/*
			 * Verify.assertFalse(verifyErrorTextMessage("Enter a valid name."),
			 * "Account name with more then 40 character can be accepted");
			 */
			setRoutingNumber("99999999");
			// continueAddBank.click();
			Verify.assertTrue(verifyErrorTextMessage("Enter a valid routing number."),
					"Routing Number with less then 9 characters can not be accepted");
			setRoutingNumber("125008547");
			// continueAddBank.click();
			Verify.assertFalse(verifyErrorTextMessage("Enter a valid routing number."),
					"Routing Number with more then 9 characters can be accepted");
			setAccountNumber("55555");
			// waitFor(ExpectedConditions.invisibilityOfElementLocated(bankhidepage));
			// continueAddBank.click();
			Verify.assertTrue(verifyErrorTextMessage("Enter a valid account number."),
					"Account Number with less then 6 characters can not be accepted");
			setAccountNumber("999999999999999999");
			// continueAddBank.click();
			/*
			 * checkPageIsReady(); OneTimePaymentPage oneTimePaymentPage = new
			 * OneTimePaymentPage(getDriver()); oneTimePaymentPage.verifyPageLoaded();
			 */
			/*
			 * Verify.assertFalse(verifyErrorTextMessage("Enter a valid account number."),
			 * "Account Number with more then 17 characters can be accepted");
			 * Reporter.log("Fields on AddBank page are successfully validated");
			 */
			checkmaximumcharbyfield(bankAccountNumber, 17);
		} catch (Exception e) {
			Verify.fail("Failed to verify all the bank validations");
		}
		return this;
	}

	/**
	 * click add bank icon
	 */
	public AddBankPage refreshandNavigateToAddBankPage(String addBankHeader) {
		try {
			/*
			 * getDriver().navigate().refresh(); OneTimePaymentPage oneTimePaymentPage = new
			 * OneTimePaymentPage(getDriver());
			 * oneTimePaymentPage.clickPaymentMethodBlade();
			 */

			clickBackButton();
			SpokePage otpSpokePage = new SpokePage(getDriver());
			otpSpokePage.verifyPageLoaded();
			otpSpokePage.clickAddBank();
			AddBankPage addBankPage = new AddBankPage(getDriver());
			addBankPage.verifyBankPageLoaded(addBankHeader);
			Reporter.log("Add bank page loaded");
		} catch (Exception e) {
			Assert.fail("Failed to load Add bank page");
		}
		return this;

	}

	/**
	 * click add bank icon
	 */
	public AddBankPage clickBackButton() {
		try {
			// moveToElement(backBtn);
			clickElementWithJavaScript(backBtn);
			// backBtn.click();
			Reporter.log("Clicked on back button");
		} catch (Exception e) {
			Verify.fail("Failed to click on back button");
		}
		return this;

	}

	public AddBankPage verifyReplaceModelHeader() {
		try {
			replaceModelHeader.isDisplayed();
			Reporter.log("replace model header is verified");
		} catch (Exception e) {
			Assert.fail("Failed to verify replace model header");
		}
		return this;

	}

	public AddBankPage clickReplaceModelContinueBtn() {
		try {
			replaceModelContinueBtn.isDisplayed();
			replaceModelContinueBtn.click();
			;

			Reporter.log("replace model continue button  is clciked");
		} catch (Exception e) {
			Assert.fail("Failed to click replace model continue button");
		}
		return this;

	}

	/**
	 * verify replacing existing stored payment alert
	 */
	public AddBankPage verifySaveThisPaymentErrorMessage(String msg) {
		try {
			checkPageIsReady();
			verifyElementBytext(replaceStoredPaymentAlert, msg);
			Reporter.log("verified  replacing  existing stored payment alert");
		} catch (Exception e) {
			Assert.fail("replacing  existing stored payment alert not found");
		}
		return this;

	}

	public AddBankPage mouseHoverOnToolTip() {
		try {
			toolTip.isDisplayed();
			Actions builder = new Actions(getDriver());
			Action mouseOver = builder.moveToElement(toolTip).build();
			mouseOver.perform();
			Reporter.log("mouse hover on tool tip");
		} catch (Exception e) {
			Assert.fail("tool tip is not found");
		}
		return this;
	}

	/**
	 * verify Tool Tip message
	 *
	 * @return boolean
	 */
	public AddBankPage verifyToolTipMsg(String msg) {
		try {
			Assert.assertTrue(toolTipText.isDisplayed(), "Tool tip link text is not displayed");
			Assert.assertTrue(toolTipText.getAttribute("tooltip-text").equals(msg), "Tool tip link text is incorrect");
		} catch (Exception e) {
			Assert.fail("Tool tip not found");
		}
		return this;
	}

	/**
	 * verify Account Name Placeholder
	 * 
	 * @param namePlaceHolder
	 * @return
	 */
	public AddBankPage verifyAccountNamePlaceholder(String namePlaceHolder) {
		try {
			getPlaceholder(accountName, namePlaceHolder);
			Reporter.log("Verified account name place holder");
		} catch (Exception e) {
			Verify.fail("Account name place holder is not displayed");
		}
		return this;
	}

	/**
	 * verify Routing Number Placeholder
	 * 
	 * @param routingNoPLaceHolder
	 * @return
	 */
	public AddBankPage verifyRoutingNumberPlaceholder(String routingNoPLaceHolder) {
		try {
			getPlaceholder(routingNum, routingNoPLaceHolder);
			Reporter.log("Verified Routing number place holder");
		} catch (Exception e) {
			Verify.fail("Routing number place holder is not displayed");
		}
		return this;
	}

	/**
	 * verify bank account number place holder
	 * 
	 * @param accNumberPlaceHolder
	 * @return
	 */
	public AddBankPage verifyBankAccNumberPlaceholder(String accNumberPlaceHolder) {
		try {
			getPlaceholder(bankAccountNumber, accNumberPlaceHolder);
			Reporter.log("Verified bank acc number place holder");
		} catch (Exception e) {
			Verify.fail("Bank Account number place holder is not displayed");
		}
		return this;
	}

	public AddBankPage getPlaceholder(WebElement element, String msg) {
		try {
			/*
			 * Verify.assertTrue(element.getAttribute("placeholder").equals(msg),
			 * "Placeholder text is incorrect for " + element.getAttribute("placeholder"));
			 */

			if (element.findElement(By.xpath("./parent::div/label")).getText().contains(msg))
				Reporter.log("Place holder is:" + msg);
			else
				Verify.fail("Place holder is not :" + msg);
		} catch (Exception e) {
			Verify.fail("Place holder not found");
		}
		return this;
	}

	public Long getAccNumber() {
		return accNumber;
	}

	/**
	 * generate a unique amount value
	 *
	 * @return double - amount
	 */
	private Long generateRandomAccNumber() {
		return (long) ((Math.random() * 9000000) + 1000000);
	}

	/**
	 * verify tool tips for routing number and account number
	 * 
	 * @return
	 */
	public AddBankPage verifyBankInfoToolTips() {
		try {
			toolTipLink.click();
			waitFor(ExpectedConditions.visibilityOf(toolTipInfoDiv));
			Assert.assertTrue(toolTipInfoDiv.isDisplayed(), "Tool tip modal is not displayed");
			Reporter.log("Tool Tip verified");
		} catch (Exception e) {
			Assert.fail("Fail to verify bank info tool tips");
		}
		return this;
	}

	public AddBankPage verifyNameOnAccountHeader() {
		try {
			Verify.assertTrue(nameOnAccountHeader.isDisplayed(), "Name on Account header is not displayed");
			Verify.assertTrue(nameOnAccountHeader.getText().equals("Name on Account"),
					"Name on Account header text is incorrect");
		} catch (Exception e) {
			Verify.fail("Name on Account header missing");
		}
		return this;
	}

	public AddBankPage verifyRoutingNumberHeader() {
		try {
			Verify.assertTrue(routingNumberHeader.isDisplayed(), "Routing Number header is not displayed");
			Verify.assertTrue(routingNumberHeader.getText().equals("Routing Number"),
					"Routing Number header text is incorrect");
		} catch (Exception e) {
			Verify.fail("Routing number header not found");
		}
		return this;
	}

	public AddBankPage verifyAccountNumberHeader() {
		try {
			Verify.assertTrue(accountNumberHeader.isDisplayed(), "Account Number header is not displayed");
			Verify.assertTrue(accountNumberHeader.getText().equals("Account Number"),
					"Account Number header text is incorrect");
		} catch (Exception e) {
			Verify.fail("Account number header not found");
		}
		return this;
	}

	/**
	 * Verify add bank labels is displayed or not
	 */
	public AddBankPage verifyAddBanklabels() {
		try {
			verifyNameOnAccountHeader();
			verifyRoutingNumberHeader();
			verifyAccountNumberHeader();

		} catch (Exception e) {
			Assert.fail("add bank labels are not found");

		}
		return this;
	}

	/**
	 * verify Tool Tip link
	 *
	 * @return boolean
	 */
	public AddBankPage verifyBankInfoToolTipLink(String msg) {
		try {
			Verify.assertTrue(toolTipLink.isDisplayed(), "Tool tip link text is not displayed");
			Verify.assertTrue(toolTipLink.getText().equals(msg), "Tool tip link text is incorrect");
		} catch (Exception e) {
			Verify.fail("Tool tip not found");
		}
		return this;
	}

	/**
	 * verify Back CTA is displayed
	 *
	 * @return boolean
	 */
	public AddBankPage verifyBackCTA() {
		try {
			Verify.assertTrue(backBtn.isDisplayed(), "Back CTA is not displayed");
		} catch (Exception e) {
			Verify.fail("Fail verifying the CTA Back button");
		}
		return this;
	}

	/**
	 * verify Continue CTA is displayed
	 *
	 * @return boolean
	 */
	public AddBankPage verifyContinueCTAAddBank() {
		try {
			Verify.assertTrue(continueAddBank.isDisplayed(), "Continue CTA is not displayed");
		} catch (Exception e) {
			Verify.fail("Failed to verify Continue CTA Add Bank");
		}
		return this;
	}

	public AddBankPage setAccountName(String accName) {
		try {
			accountName.sendKeys(accName);
		} catch (Exception e) {
			Assert.fail("Account Name field not found");
		}
		return this;
	}

	public AddBankPage checkmaximumcharbyfield(WebElement ele, int stringlength) {
		try {
			if (ele.getText().length() > stringlength)
				Verify.fail("Name on account is accepting more than " + stringlength + " characters");
		} catch (Exception e) {
			Assert.fail("Account Name field not found");
		}
		return this;
	}

	public AddBankPage setRoutingNumber(String routingNumber) {
		try {
			routingNum.clear();
			routingNum.sendKeys(routingNumber);
		} catch (Exception e) {
			Assert.fail("Fail to set the Routing Number");
		}
		return this;
	}

	public AddBankPage setAccountNumber(String accNumber) {
		try {
			bankAccountNumber.clear();
			bankAccountNumber.sendKeys(accNumber);
		} catch (Exception e) {
			Assert.fail("Fail to set the Account number");
		}
		return this;
	}

	/**
	 * verify how to find my routing or account number is displayed or not
	 *
	 */
	public AddBankPage verifyHowToFindLink() {
		try {
			howToFindLink.isDisplayed();
			Assert.assertTrue(howToFindLink.getText().equals("How to find my routing or account number"),
					"how to find link is not found");

		} catch (Exception e) {
			Assert.fail("Fail to verify the how to find link");
		}
		return this;
	}

	/**
	 * click how to find my routing or account number link
	 *
	 */
	public AddBankPage clickHowToFindLink() {
		try {
			howToFindLink.isDisplayed();
			howToFindLink.click();
			Reporter.log("how to find link is clicked");

		} catch (Exception e) {
			Assert.fail("Fail to verify the how to find link");
		}
		return this;
	}

	/**
	 * click how to find my routing or account number model is displayed or not
	 *
	 */
	public AddBankPage verifyHowToFindModel() {
		try {
			howToFindModel.isDisplayed();
			Reporter.log("How to find my routing or account number model is displayed");

		} catch (Exception e) {
			Assert.fail("Fail to verify the how to find model");
		}
		return this;
	}

	/**
	 * verify error messages for empty fields
	 * 
	 * @param message
	 * @return boolean
	 */
	public boolean verifyErrorTextMessage(String message) {
		checkPageIsReady();
		boolean display = false;
		try {
			for (WebElement error : errorMsg) {
				if (error.isDisplayed() && error.getText().contains(message)) {
					display = true;
				}
			}
		} catch (Exception e) {

		}
		return display;
	}

	/**
	 * Verify that Save Payment checkbox is present
	 */
	public AddBankPage verifySavePaymentMethodCheckBox() {
		try {
			Verify.assertTrue(savePaymentCheckBox.isDisplayed(), "Save Payment Checkbox is not present");
			Reporter.log("Save Payment Checkbox is present");
		} catch (Exception e) {
			Verify.fail("Failed to verify Save Payement CheckBox. Checkbox should be present");
		}
		return this;
	}

	/**
	 * Verify that Save Payment checkbox unselect State or not
	 */
	public AddBankPage verifySavePaymentMethodCheckBoxIsUnSelected() {
		try {
			Assert.assertTrue(!savePaymentCheckBox.isSelected(), "Save Payment Checkbox is selected state");
			Reporter.log("Save Payment Checkbox is unselected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Save Payement CheckBox");
		}
		return this;
	}

	/**
	 * Verify that Save Payment checkbox select State or not
	 */
	public AddBankPage verifySavePaymentMethodCheckBoxIsSelected() {
		try {
			savePaymentCheckBox.isSelected();
			Reporter.log("Save Payment Checkbox is selected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Save Payement CheckBox");
		}
		return this;
	}

	public AddBankPage verifyWalletsizereachedtext(String reqText) {
		try {

			if (errWalletsizereached.getText().contains(reqText))
				Reporter.log("Wallet size reached text is:" + reqText);
			else
				Assert.fail("Wallet size reached text is:" + errWalletsizereached.getText());
		} catch (Exception e) {
			Assert.fail("Wallet size reached text is not displayed ");
		}
		return this;
	}

	public AddBankPage Checkdisabilityofsavepayment() {
		try {

			if (savePaymentValue.isEnabled())
				Assert.fail("Save payment mehod is enble");
			else
				Reporter.log("Save payment method check box is disable");

		} catch (Exception e) {
			Assert.fail("Save payment method check box is not displayed ");
		}
		return this;
	}

	/**
	 * Verify that Save Payment checkbox is NOT present
	 */
	public AddBankPage verifyNoSavePaymentMethodCheckBox() {
		try {
			waitFor(ExpectedConditions.visibilityOf(backBtn));
			boolean isSaveCheckBoxDisplayed = false;
			if (isElementDisplayed(savePaymentCheckBox)) {
				isSaveCheckBoxDisplayed = true;
			}
			Assert.assertFalse(isSaveCheckBoxDisplayed, "Save Payment CheckBox is visisble for non-PAH account");
		} catch (Exception e) {
			Assert.fail("Failed to verify Save Payement CheckBox");
		}
		return this;
	}

	public AddBankPage verifyInSessionSavedPaymentDetails(Long accountNumber, Payment payment) {
		try {
			Verify.assertTrue(accountName.getAttribute("value").equals(payment.getCheckingName()),
					"Account Number header text is incorrect");
			Verify.assertTrue(bankAccountNumber.getAttribute("value").equals(String.valueOf(accountNumber)),
					"Account Number header text is incorrect");
			Verify.assertTrue(routingNum.getAttribute("value").equals(payment.getRoutingNumber()),
					"Account Number header text is incorrect");

		} catch (Exception e) {
			Reporter.log("Save payment method option is not displayed");
		}
		return this;
	}

	/**
	 * Fill all the bank details and click on continue
	 *
	 * @param payment
	 */
	public void enterAccountNumber() {
		try {
			checkPageIsReady();
			bankAccountNumber.clear();
			String accountNumber = String.valueOf(generateRandomAccNumber());
			bankAccountNumber.sendKeys(accountNumber);
			accNumber = Long.parseLong(accountNumber);
			Reporter.log("Failed to fill the Account number");
		} catch (Exception e) {
			Assert.fail("Failed to fill the Account number");
		}

	}

	/**
	 * verify NickName Field is displayed or not
	 * 
	 *
	 */
	public AddBankPage verifyNickNameHeader() {
		try {
			nickNameLabel.isDisplayed();
			nickName.isDisplayed();
			optionalLabel.isDisplayed();
			Reporter.log("nickName field,label,Optional Label is displayed");
		} catch (Exception e) {
			Verify.fail("Failed to Verify the nickName Header elements");
		}
		return this;
	}

	/**
	 * verify NickName Field is Enabled or not
	 * 
	 *
	 */
	public AddBankPage verifyNickNameDisplay() {
		try {
			if (nickName.isDisplayed()) {
				Reporter.log("nickName field is displayed");
			} else {
				Reporter.log("nickName field is Surpressed");
			}

		} catch (Exception e) {
			Verify.fail("nickName Field is not Found ");
		}
		return this;
	}

	/**
	 * verify Set as Default checkbox Field is displayed or not
	 * 
	 *
	 */
	public AddBankPage verifyDefaultCheckBox() {
		try {
			if (defaultCheckbox.isDisplayed()) {
				Reporter.log("Default Check Box field is Disaplyed");
			} else {
				Reporter.log("Default Checkbox field is not Displayed");
			}

		} catch (Exception e) {
			Verify.fail("Default checkbox Field is not Found ");
		}
		return this;
	}

	/**
	 * verify Set as Default checkbox Field label is displayed or not
	 * 
	 *
	 */
	public AddBankPage verifyDefaultCheckBoxLabel() {
		try {
			if (defaultCheckboxLabel.isDisplayed()) {
				Reporter.log("Default Check Box field Label is Disaplyed");
			} else {
				Reporter.log("Default Checkbox field Label is not Displayed");
			}

		} catch (Exception e) {
			Assert.fail("Default checkbox Field Label is not Found ");
		}
		return this;
	}

	/**
	 * Click checkbox Field is Enabled or not
	 * 
	 *
	 */
	public AddBankPage ClickDefaultCheckBox() {
		try {
			defaultCheckbox.click();
			Reporter.log("default Check box field is Clicked");

		} catch (Exception e) {
			Verify.fail("default CheckBox is not Found ");
		}
		return this;
	}

	/**
	 * verify NickName Field is Enabled or not
	 * 
	 *
	 */
	public AddBankPage verifyNoNickNameandDefaultCheckBoxDisplay() {
		try {
			if (nickName.isDisplayed() && defaultCheckboxLabel.isDisplayed() && defaultCheckbox.isDisplayed()) {
				Verify.fail("nickName Field,Default Check Box and Default CheckBox Label is Displayed");
			}

		} catch (Exception e) {
			Reporter.log("nickName Field,Default Check Box and Default CheckBox Label is not found");
		}
		return this;
	}

	/**
	 * Enter Nick Name in Bank Page
	 *
	 * @param payment
	 */
	public void enterNickName(String nicName) {
		try {
			checkPageIsReady();
			nickName.clear();
			nickName.sendKeys(nicName);
			Reporter.log("Nick Name is Enterd successfully");
		} catch (Exception e) {
			Verify.fail("Failed to fill the Nick Name");
		}

	}

	/**
	 * click save to my wallet button for wallet flow
	 */
	public String clickSaveToMyWalletCTA() {
		try {
			checkPageIsReady();
			saveToMyWalletCTA.click();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".circle")));
			if (verifyErrorMsgIsDisplayed()) {
				accNumber = generateRandomAccNumber();
				bankAccountNumber.clear();
				bankAccountNumber.sendKeys(accNumber.toString());
				saveToMyWalletCTA.click();
			}
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".circle")));
			Reporter.log("Clicked on Save to my wallet CTA and set the Bank account number");
		} catch (Exception e) {
			Assert.fail("Failed to click on  Save to my wallet CTA");
		}
		return accNumber.toString();

	}

	/**
	 * Verify that Save Payment checkbox is suppressed
	 */
	public AddBankPage verifySavePaymentMethodCheckBoxSuppressed(MyTmoData myTmoData) {
		try {
			waitFor(ExpectedConditions.visibilityOf(backBtn));
			boolean isSaveCheckBoxDisplayed = false;
			if (isElementDisplayed(savePaymentCheckBox)) {
				isSaveCheckBoxDisplayed = true;
			}

			Assert.assertFalse(isSaveCheckBoxDisplayed, "Save Payment CheckBox is visisble for non-PAH account for "
					+ myTmoData.getPayment().getUserType());
			Reporter.log("saved payments suppressed for" + myTmoData.getPayment().getUserType());
			System.out.println("saved payments suppressed for" + myTmoData.getPayment().getUserType());
		} catch (Exception e) {
			Assert.fail("Failed to verify Save Payement CheckBox for " + myTmoData.getPayment().getUserType());
		}
		return this;
	}

	/**
	 * method to verify CTA Save to my wallet for wallet - add bank
	 */
	public void verifySavetoMyWalletCTA() {
		try {
			Assert.assertTrue(saveToMyWalletCTA.isDisplayed(), "Primary CTA -Save to wallet is not found");
			Assert.assertEquals(saveToMyWalletCTA.getText().trim(), "Save to my wallet");
			Reporter.log("Save to my wallet is displayed");
		} catch (Exception e) {
			Assert.fail("Primary CTA -Save to wallet is not found");
		}

	}

	/**
	 * verify that the save payment method checkbox is disabled for wallet full
	 */
	public void verifySavePaymentCheckboxDisabled() {
		try {
			Assert.assertFalse(savePaymentCheckboxInput.isEnabled(), "Save payment checkbox is not disabled");
			Reporter.log("Save payment checkbox is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify Save payment checkbox");
		}
	}

	public void verifyPiiMasking(String custNamePID, String custPaymentPID) {
		try {
			Assert.assertTrue(checkElementisPIIMasked(accountName, custNamePID), "Name on Account is not PII masked");
			Assert.assertTrue(checkElementisPIIMasked(routingNum, custPaymentPID), "Routing Number is not PII masked");
			Assert.assertTrue(checkElementisPIIMasked(bankAccountNumber, custPaymentPID),
					"Account Number is not PII masked");
			Reporter.log("Verified PII masking for Add bank Page");
		} catch (Exception e) {
			Assert.fail("Failed to Verify: PII masking for Add Bank page");
		}
	}

}
