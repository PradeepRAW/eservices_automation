package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author pshiva
 *
 */
public class PaymentsHubPage extends CommonPage {

	private final String pageUrl = "/paymenthub";

	@FindBy(css = "div.Display3 span")
	private WebElement pageHeader;

	@FindBy(css = "")
	private WebElement manageMyPaymentMethodsBlade;

	@FindBy(css = "span[aria-label*='Equipment']")
	private WebElement eipBlade;

	@FindBy(css = "span[aria-label*='Payment arrangement']")
	private WebElement paBlade;
	
	@FindBy(css = "span[aria-label*='Autopay']")
	private WebElement autopayBlade;

	@FindBy(css = "")
	private WebElement otpBlade;

	private WebElement backCTA;

	
	public PaymentsHubPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PaymentsHubPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public PaymentsHubPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			Reporter.log("Payments Hub Page is loaded.");
		} catch (Exception e) {
			Assert.fail("Payments Hub page is not loaded");
		}
		return this;
	}

	/**
	 * verify OTP blade is displayed
	 */
	public void verifyOTPBlade() {
		try {
			otpBlade.isDisplayed();
			Reporter.log("OTP blade is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify OTP blade");
		}
	}

	/**
	 * verify Autopay blade is displayed
	 */
	public void verifyAutopayBlade() {
		try {
			autopayBlade.isDisplayed();
			Reporter.log("Autopay blade is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Autopay blade");
		}
	}

	/**
	 * verify PA blade is displayed
	 */
	public void verifyPABlade() {
		try {
			paBlade.isDisplayed();
			Reporter.log("PA blade is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify PA blade");
		}
	}

	/**
	 * verify EIP blade is displayed
	 */
	public void verifyEIPBlade() {
		try {
			eipBlade.isDisplayed();
			Reporter.log("EIP blade is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify EIP blade");
		}
	}

	/**
	 * verify Manage my payment methods blade is displayed
	 */
	public void verifyManageMyPaymentMethodsBlade() {
		try {
			manageMyPaymentMethodsBlade.isDisplayed();
			Reporter.log("Manage My Payment Methods Blade is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Manage my payments blade");
		}
	}

	/**
	 * verify back CTA
	 */
	public void verifyBackCTA() {
		try {
			backCTA.isDisplayed();
			Reporter.log("Back CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Back CTA");
		}
	}

	/**
	 * Click on Back CTA
	 */
	public void clickBackCTA() {
		try {
			backCTA.click();
			Reporter.log("Clicked on Back CTA");
		} catch (Exception e) {
			Assert.fail("Failed to click Back CTA");
		}
	}

}
