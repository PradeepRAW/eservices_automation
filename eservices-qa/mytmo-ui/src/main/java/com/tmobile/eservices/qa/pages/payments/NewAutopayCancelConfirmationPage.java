package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Sreekanth
 *
 */
public class NewAutopayCancelConfirmationPage extends CommonPage {
	
	
	@FindBy(xpath = "//p[text()='AutoPay is now OFF']")
	private WebElement autopayoff;
	
	@FindBy(id = "cancelButton")
	private WebElement BacktoHome;
	
	@FindBy(id = "acceptButton")
	private WebElement makeAOTPPayment;
	
	
	@FindBy(id = "span.check-icon")
	private WebElement successIcon;
	
	@FindBy(xpath = "//p[contains(text(),'A one-time payment will be required for any outstanding balance due on')]")
	private WebElement otpText;
	
	
	private By landingpageSpinner = By.cssSelector("div.circle");
	private final String pageUrl = "autopay/cancelConfirmation";
	
	
	
//	private final String pageUrlPA = "/tnc";

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public NewAutopayCancelConfirmationPage(WebDriver webDriver) {
		super(webDriver);
	}
	/**
     * Verify that current page URL matches the expected URL.
     */
    public NewAutopayCancelConfirmationPage verifyPageUrl() {
    	waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }
	
	/**
     * Verify that the page loaded completely.
	 * @throws InterruptedException 
     */
    public NewAutopayCancelConfirmationPage verifyPageLoaded(){
    	try{
    		checkPageIsReady();
    		waitFor(ExpectedConditions.invisibilityOfElementLocated(landingpageSpinner));
    		verifyPageUrl();
    		waitFor(ExpectedConditions.visibilityOf(autopayoff));
    		waitFor(ExpectedConditions.visibilityOf(BacktoHome));
			
    	}catch(Exception e){
    		Assert.fail("Autopay Cancel Confirmation page is not loaded");
    	}
        return this;
    }
    

	
	public NewAutopayCancelConfirmationPage clickBacktoHomeButton() {
	    try{
	    	BacktoHome.click();
            Reporter.log("Clicked on Back to Home Button");
        }catch (Exception e){
        	Verify.fail("Fail to click on Back to Home Button");
        }
		return this;
	}

	public NewAutopayCancelConfirmationPage clickOnmakeOTP() {
	    try{
	    	makeAOTPPayment.click();
            Reporter.log("Clicked on makeonetimepayment Button");
        }catch (Exception e){
        	Verify.fail("Fail to click on makeonetimepayment Button");
        }
		return this;
	}
	
	
	public NewAutopayCancelConfirmationPage VerifyOTPtextDisplayed() {
	    try{
	    	otpText.isDisplayed();
            Reporter.log("OTP text is diplayed");
        }catch (Exception e){
        	Verify.fail("Failed to locate otpText eleent");
        }
		return this;
	}
	
	
}
