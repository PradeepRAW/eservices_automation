package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class MobileLandingPage extends CommonPage {

	private static final String pageUrl = "staging";
	
	@FindBy(css = "img.gad-cursor:not(.btn-details)")
	private WebElement menuIcon;
	
	@FindBy(css = "[href*='terms-of-use']")
	private WebElement termsOfUseLink;
	
	@FindBy(css = "[href*='privacypolicy']")
	private WebElement privacyPolicyLink;
	
	@FindBy(css = "[href*='advert']")
	private WebElement internetBasedAdsLink;

	public MobileLandingPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Blocking page.
	 *
	 * @return the BlockingPage class instance.
	 */
	public MobileLandingPage verifyMobileLandingPage() {
		try {
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Mobile landing page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Mobile landing page not displayed");
		}
		return this;

	}
	
	
	/**
	 * Click menu icon
	 */
	public MobileLandingPage clickMenuIcon() {
		try{	
		waitforSpinner();
			checkPageIsReady();
			menuIcon.click();
			Reporter.log("Click on menu is success");
		}
		catch(Exception e)
		{
			Reporter.log("Click on menu failed");
		}
		return this;
	}
	
	/**
	 * Verify terms of use link
	 * 
	 * @return
	 */
	public MobileLandingPage verifyTermsOfUseLink() {
		try {
			termsOfUseLink.isDisplayed();
			Reporter.log("Terms of use link displayed");
		} catch (Exception e) {
			Assert.fail("Terms of use link not displayed");
		}
		return this;
	}
	
	/**
	 * Verify privacy policy link
	 * 
	 * @return
	 */
	public MobileLandingPage verifyPrivacyPolicyLink() {
		try {
			privacyPolicyLink.isDisplayed();
			Reporter.log("Privacy policy link displayed");
		} catch (Exception e) {
			Assert.fail("Privacy policy link not displayed");
		}
		return this;
	}
	
	/**
	 * Verify internet based ads link
	 * 
	 * @return
	 */
	public MobileLandingPage verifyInternetBasedAdsLink() {
		try {
			internetBasedAdsLink.isDisplayed();
			Reporter.log("Internet based ads link displayed");
		} catch (Exception e) {
			Assert.fail("Internet based ads link not displayed");
		}
		return this;
	}

	public MobileLandingPage launchMobileLandingPage(MyTmoData myTmoData){

		return this;
	}

	}
