package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.Payment;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author pshiva
 *
 */
public class EditBankPage1_5 extends CommonPage {

	@FindBy(css = "addbank div h4")
	private WebElement bankHeader;

	@FindAll({ @FindBy(xpath = "//label[contains(@for,'account_name')]"), @FindBy(css = "label[for='accountName']") })
	private WebElement nameOnAccountHeader;

	@FindAll({ @FindBy(xpath = "//label[contains(@for,'routing_number')]"), @FindBy(css = "label[for='routingNum']") })
	private WebElement routingNumberHeader;

	@FindAll({ @FindBy(xpath = "//label[contains(@for,'account_number')]"), @FindBy(css = "label[for='accountNum']") })
	private WebElement accountNumberHeader;

	@FindAll({ @FindBy(css = "#account_name"), @FindBy(css = "#accountName") })
	private WebElement accountName;

	@FindAll({ @FindBy(css = "#routing_number"), @FindBy(css = "#routingNum") })
	private WebElement routingNum;

	@FindAll({ @FindBy(css = "#account_number"), @FindBy(css = "#accountNum") })
	private WebElement bankAccountNumber;

	@FindBy(linkText = "How to find my routing or account number")
	private WebElement howToFindLink;

	@FindAll({ @FindBy(css = "div#back button"), @FindBy(css = "button.SecondaryCTA") })
	private WebElement backBtn;

	private final String pageUrl = "/editbank";

	@FindBy(css = "input#nickName")
	private WebElement nickName;

	@FindAll({ @FindBy(css = "label[for='nickName']") })
	private WebElement nickNameLabel;

	@FindAll({ @FindBy(css = "a[aria-label='optional field']") })
	private WebElement optionalLabel;

	@FindBy(css = "div.legal.indicator>span")
	private WebElement defaultCheckbox;

	@FindBy(xpath = "//span[contains(text(),'Set as default')]")
	private WebElement defaultCheckboxLabel;

	@FindBy(id = "acceptButton")
	private WebElement saveChangesCTA;

	@FindBy(id = "deleteModelLink")
	private WebElement removeFromMyWalletLink;

	@FindBy(id = "deleteModal")
	private WebElement deleteModal;

	@FindBy(css = "#deleteModal div span.Display3")
	private WebElement deleteModalHeader;

	@FindBy(css = "#deleteModal div.padding-top-medium span")
	private WebElement deleteModalContent;

	@FindBy(css = "#deleteModal button.PrimaryCTA-accent")
	private WebElement deleteModalYesBtn;

	@FindBy(css = "#deleteModal #deleteModalCancelBtn")
	private WebElement deleteModalCancelBtn;

	/**
	 * Constructor
	 *
	 * @param webDriver
	 */
	public EditBankPage1_5(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public EditBankPage1_5 verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public EditBankPage1_5 verifyBankPageLoaded() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			// waitFor(ExpectedConditions.visibilityOf(paymentProcessing));
			waitFor(ExpectedConditions.elementToBeClickable(backBtn));
			bankHeader.isDisplayed();
			Reporter.log("Bank page is loaded and header is verified");
		} catch (Exception e) {
			Assert.fail("Bank page is  not loaded and Header is failed to verified");

		}
		return this;
	}

	/**
	 * Verify bank page header is displayed or not.
	 * 
	 */
	public EditBankPage1_5 verifyBankPageHeader() {
		try {

			bankHeader.isDisplayed();
			Assert.assertTrue(bankHeader.getText().equals("Bank information"));
			Reporter.log("Bank page header is verified");
		} catch (Exception e) {
			Assert.fail("Bank page header is failed to verified");
		}
		return this;
	}

	/**
	 * click add bank icon
	 */
	public EditBankPage1_5 clickBackButton() {
		try {
			backBtn.click();
			Reporter.log("Clicked on back button");
		} catch (Exception e) {
			Verify.fail("Failed to click on back button");
		}
		return this;

	}

	/**
	 * verify Account Name Placeholder
	 * 
	 * @param namePlaceHolder
	 * @return
	 */
	public EditBankPage1_5 verifyAccountNamePlaceholder(String namePlaceHolder) {
		try {
			getPlaceholder(accountName, namePlaceHolder);
			Reporter.log("Verified account name place holder");
		} catch (Exception e) {
			Verify.fail("Account name place holder is not displayed");
		}
		return this;
	}

	/**
	 * verify Routing Number Placeholder
	 * 
	 * @param routingNoPLaceHolder
	 * @return
	 */
	public EditBankPage1_5 verifyRoutingNumberPlaceholder(String routingNoPLaceHolder) {
		try {
			getPlaceholder(routingNum, routingNoPLaceHolder);
			Reporter.log("Verified Routing number place holder");
		} catch (Exception e) {
			Verify.fail("Routing number place holder is not displayed");
		}
		return this;
	}

	/**
	 * verify bank account number place holder
	 * 
	 * @param accNumberPlaceHolder
	 * @return
	 */
	public EditBankPage1_5 verifyBankAccNumberPlaceholder(String accNumberPlaceHolder) {
		try {
			getPlaceholder(bankAccountNumber, accNumberPlaceHolder);
			Reporter.log("Verified bank acc number place holder");
		} catch (Exception e) {
			Verify.fail("Bank Account number place holder is not displayed");
		}
		return this;
	}

	public EditBankPage1_5 getPlaceholder(WebElement element, String msg) {
		try {
			Verify.assertTrue(element.getAttribute("placeholder").equals(msg),
					"Placeholder text is incorrect for " + element.getAttribute("placeholder"));
		} catch (Exception e) {
			Verify.fail("Place holder not found");
		}
		return this;
	}

	public EditBankPage1_5 verifyNameOnAccountHeader() {
		try {
			Verify.assertTrue(nameOnAccountHeader.isDisplayed(), "Name on Account header is not displayed");
			Verify.assertTrue(nameOnAccountHeader.getText().equals("Name on Account"),
					"Name on Account header text is incorrect");
		} catch (Exception e) {
			Verify.fail("Name on Account header missing");
		}
		return this;
	}

	public EditBankPage1_5 verifyRoutingNumberHeader() {
		try {
			Verify.assertTrue(routingNumberHeader.isDisplayed(), "Routing Number header is not displayed");
			Verify.assertTrue(routingNumberHeader.getText().equals("Routing Number"),
					"Routing Number header text is incorrect");
		} catch (Exception e) {
			Verify.fail("Routing number header not found");
		}
		return this;
	}

	public EditBankPage1_5 verifyAccountNumberHeader() {
		try {
			Verify.assertTrue(accountNumberHeader.isDisplayed(), "Account Number header is not displayed");
			Verify.assertTrue(accountNumberHeader.getText().equals("Account Number"),
					"Account Number header text is incorrect");
		} catch (Exception e) {
			Verify.fail("Account number header not found");
		}
		return this;
	}

	/**
	 * Verify add bank labels is displayed or not
	 */
	public EditBankPage1_5 verifyAddBanklabels() {
		try {
			verifyNameOnAccountHeader();
			verifyRoutingNumberHeader();
			verifyAccountNumberHeader();

		} catch (Exception e) {
			Assert.fail("add bank labels are not found");

		}
		return this;
	}

	/**
	 * verify Back CTA is displayed
	 *
	 * @return boolean
	 */
	public EditBankPage1_5 verifyBackCTA() {
		try {
			Verify.assertTrue(backBtn.isDisplayed(), "Back CTA is not displayed");
		} catch (Exception e) {
			Verify.fail("Fail verifying the CTA Back button");
		}
		return this;
	}

	public EditBankPage1_5 setAccountName(String accName) {
		try {
			accountName.sendKeys(accName);
		} catch (Exception e) {
			Assert.fail("Account Name field not found");
		}
		return this;
	}

	public EditBankPage1_5 setRoutingNumber(String routingNumber) {
		try {
			routingNum.clear();
			routingNum.sendKeys(routingNumber);
		} catch (Exception e) {
			Assert.fail("Fail to set the Routing Number");
		}
		return this;
	}

	public EditBankPage1_5 setAccountNumber(String accNumber) {
		try {
			bankAccountNumber.clear();
			bankAccountNumber.sendKeys(accNumber);
		} catch (Exception e) {
			Assert.fail("Fail to set the Account number");
		}
		return this;
	}

	/**
	 * verify how to find my routing or account number is displayed or not
	 *
	 */
	public EditBankPage1_5 verifyHowToFindLink() {
		try {
			howToFindLink.isDisplayed();
			Assert.assertTrue(howToFindLink.getText().equals("How to find my routing or account number"),
					"how to find link is not found");

		} catch (Exception e) {
			Assert.fail("Fail to verify the how to find link");
		}
		return this;
	}

	/**
	 * click how to find my routing or account number link
	 *
	 */
	public EditBankPage1_5 clickHowToFindLink() {
		try {
			howToFindLink.isDisplayed();
			howToFindLink.click();
			Reporter.log("how to find link is clicked");

		} catch (Exception e) {
			Assert.fail("Fail to verify the how to find link");
		}
		return this;
	}

	/**
	 * verify NickName Field is displayed or not
	 * 
	 *
	 */
	public EditBankPage1_5 verifyNickNameHeader() {
		try {
			nickNameLabel.isDisplayed();
			nickName.isDisplayed();
			optionalLabel.isDisplayed();
			Reporter.log("nickName field,label,Optional Label is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify the nickName Header elements");
		}
		return this;
	}

	/**
	 * verify NickName Field is Enabled or not
	 * 
	 *
	 */
	public EditBankPage1_5 verifyNickNameDisplay() {
		try {
			if (nickName.isDisplayed()) {
				Reporter.log("nickName field is displayed");
			} else {
				Reporter.log("nickName field is Surpressed");
			}

		} catch (Exception e) {
			Assert.fail("nickName Field is not Found ");
		}
		return this;
	}

	/**
	 * verify Set as Default checkbox Field is displayed or not
	 * 
	 *
	 */
	public EditBankPage1_5 verifyDefaultCheckBox() {
		try {
			if (defaultCheckbox.isDisplayed()) {
				Reporter.log("Default Check Box field is Disaplyed");
			} else {
				Reporter.log("Default Checkbox field is not Displayed");
			}

		} catch (Exception e) {
			Assert.fail("Default checkbox Field is not Found ");
		}
		return this;
	}

	/**
	 * verify Set as Default checkbox Field label is displayed or not
	 * 
	 *
	 */
	public EditBankPage1_5 verifyDefaultCheckBoxLabel() {
		try {
			if (defaultCheckboxLabel.isDisplayed()) {
				Reporter.log("Default Check Box field Label is Disaplyed");
			} else {
				Reporter.log("Default Checkbox field Label is not Displayed");
			}

		} catch (Exception e) {
			Assert.fail("Default checkbox Field Label is not Found ");
		}
		return this;
	}

	/**
	 * Click checkbox Field is Enabled or not
	 * 
	 *
	 */
	public EditBankPage1_5 ClickDefaultCheckBox() {
		try {
			defaultCheckbox.click();
			Reporter.log("default Check box field is Clicked");

		} catch (Exception e) {
			Assert.fail("default CheckBox is not Found ");
		}
		return this;
	}

	/**
	 * verify NickName Field is Enabled or not
	 * 
	 *
	 */
	public EditBankPage1_5 verifyNoNickNameandDefaultCheckBoxDisplay() {
		try {
			if (nickName.isDisplayed() && defaultCheckboxLabel.isDisplayed() && defaultCheckbox.isDisplayed()) {
				Assert.fail("nickName Field,Default Check Box and Default CheckBox Label is Displayed");
			}

		} catch (Exception e) {
			Reporter.log("nickName Field,Default Check Box and Default CheckBox Label is not found");
		}
		return this;
	}

	/**
	 * Enter Nick Name in Bank Page
	 *
	 * @param payment
	 */
	public void enterNickName(String nicName) {
		try {
			checkPageIsReady();
			nickName.clear();
			nickName.sendKeys(nicName);
			Reporter.log("Nick Name is Enterd successfully");
		} catch (Exception e) {
			Assert.fail("Failed to fill the Nick Name");
		}

	}

	public void verifyEditBankPageElements() {
		try {
			verifyNameOnAcocuntNotNull();
			verifyRoutingNumberMasked();
			verifyAcocuntNumberMasked();
			verifyBackCTA();
			verifySaveChangesCTA();
			Reporter.log("Verified Edit bank page elements");
		} catch (Exception e) {
			Assert.fail("Failed to verify Edit bank page elements");
		}
	}

	private void verifySaveChangesCTA() {
		try {
			saveChangesCTA.isDisplayed();
			Reporter.log("Verified Save changes CTA");
		} catch (Exception e) {
			Assert.fail("Failed to verify Save changes CTA");
		}
	}

	private void verifyAcocuntNumberMasked() {
		try {
			bankAccountNumber.getAttribute("value").matches("[*]+");
			Reporter.log("Verified that the acocunt number is masked");
		} catch (Exception e) {
			Assert.fail("Failed to verify that the account number is masked");
		}
	}

	private void verifyRoutingNumberMasked() {
		try {
			routingNum.getAttribute("value").matches("[*]+");
			Reporter.log("Verified that the routing number is masked");
		} catch (Exception e) {
			Assert.fail("Failed to verify that the routing number is masked");
		}
	}

	private void verifyNameOnAcocuntNotNull() {
		try {
			Assert.assertFalse(accountName.getAttribute("value").isEmpty(), "Account Name is NULL");
			Reporter.log("Verified that the Account Name is not NULL");
		} catch (Exception e) {
			Assert.fail("Failed to verify that the Account Name is not NULL");
		}
	}

	public void updateBankDetailsAndSave(Payment payment) {
		try {
			accountName.clear();
			accountName.sendKeys(payment.getCheckingName());
			nickName.clear();
			nickName.sendKeys(payment.getNickName());
			saveChangesCTA.click();
			Reporter.log("Updated new Bank details and clicked on save");
		} catch (Exception e) {
			Assert.fail("Failed to update Bank details");
		}
	}

	public void verifyUpdatedBankDetails(Payment payment) {
		try {
			Assert.assertTrue(accountName.getAttribute("value").equals(payment.getCheckingName()));
			Assert.assertTrue(nickName.getAttribute("value").equals(payment.getNickName()));
			Reporter.log("Verified the updated bank details");
		} catch (Exception e) {
			Assert.fail("Failed to verify updated Bank details");
		}
	}

	public EditBankPage1_5 clickRemoveFromMyWalletLink() {
		try {
			removeFromMyWalletLink.click();
			Reporter.log("Clicked on Remove from my Wallet link");
		} catch (Exception e) {
			Assert.fail("Failed to click on Remove from my Wallet link");
		}
		return this;
	}

	public EditBankPage1_5 verifyRemoveFromMyWalletModal() {
		try {
			deleteModal.isDisplayed();
			deleteModalHeader.isDisplayed();
			deleteModalContent.isDisplayed();
			deleteModalYesBtn.isDisplayed();
			deleteModalCancelBtn.isDisplayed();
			Reporter.log("Verified Delete modal");
		} catch (Exception e) {
			Assert.fail("Failed to verify Delete modal");
		}
		return this;
	}

	public EditBankPage1_5 clickYesDeleteOnModal() {
		try {
			deleteModalYesBtn.click();
			Reporter.log("Clicked 'Yes, Delete button' on Delete modal");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Yes, Delete button' on Delete modal");
		}
		return this;
	}

	public EditBankPage1_5 clickCancelOnModal() {
		try {
			deleteModalCancelBtn.click();
			Reporter.log("Clicked 'Cancel button' on Delete modal");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Cancel button' on Delete modal");
		}
		return this;
	}

}
