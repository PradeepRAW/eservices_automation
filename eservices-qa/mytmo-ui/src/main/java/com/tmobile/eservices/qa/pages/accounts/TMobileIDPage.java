package com.tmobile.eservices.qa.pages.accounts;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author rnallamilli
 * 
 */
public class TMobileIDPage extends CommonPage {

	private static final String pageUrl = "profile";

	@FindBy(css = "[ng-reflect-title-text='T-Mobile ID'] .Display6")
	private WebElement tMobileIDLink;

	@FindBy(css = "p[ng-click*='UpdateName']")
	private WebElement updateNameEditBtn;

	@FindBy(css = "div.text-center[ng-if*='showNameEdit']")
	private WebElement updateNameEditBtnForIOS;

	@FindBy(css = "p[ng-click*='EmailUpdate']")
	private WebElement updateEmailEditBtn;

	@FindBy(css = "p.icon-gt-arrow.pull-left.arrow-alignment.ng-scope")
	private WebElement updateEmailEditBtnForIOS;

	@FindBy(css = "p[ng-click*='Security']")
	private WebElement updateSecurityQstnsEditBtn;

	@FindBy(xpath = "//div[contains(text(),'Security questions')]")
	private WebElement updateSecurityQstnsEditBtnForIOS;

	@FindBy(css = "p[ng-click*='Step']")
	private WebElement updateStepVerificationEditBtn;

	@FindBy(css = "a[ng-click*='Phone']")
	private WebElement updatePhoneNumberEditBtn;

	@FindBy(xpath = "//div/b[contains(text(),'Phone numbers')]")
	private WebElement updatePhoneNumberEditBtnForIOS;

	@FindBy(id = "newEmail_value")
	private WebElement emailField;

	@FindBy(id = "reEnteredEmail_value")
	private WebElement reEnterEmailField;

	@FindBy(id = "firstName")
	private WebElement firstNameTxtField;

	@FindBy(id = "lastName")
	private WebElement lastNameTxtField;

	@FindBy(css = ".btn-primary")
	private WebElement saveBtn;

	@FindBy(id = "logout-Ok")
	private WebElement logoutButton;

	@FindBy(css = "div[ng-show='isPhoneOrTabletEmail']:not(.edit-current-email)")
	private WebElement email;

	@FindBy(css = "p[ng-show='isCurentEmail']:not(.ng-hide)")
	private WebElement emailErrorMessage;

	@FindBy(css = "[name='answer0']")
	private WebElement securityQstn1;

	@FindBy(css = "[name='answer1']")
	private WebElement securityQstn2;

	@FindBy(css = "[name='answer2']")
	private WebElement securityQstn3;

	@FindBy(css = ".number-list .ui_billingpastdue")
	private WebElement linkedPhoneNumber;

	@FindBy(css = "span#on")
	private WebElement twoStepVerificationToggle;

	@FindBy(xpath = "//span[@id='on']")
	private WebElement toggleButton;

	@FindBy(css = "div[ng-if*='showPhoneEdit']")
	private WebElement phoneNumbers;

	@FindBy(css = "[ng-model*='updateNickName']")
	private WebElement nickNameCheckBox;

	@FindBy(css = ".save_cancel button")
	private WebElement securityQstnssaveBtn;

	@FindBy(id = "contentFrame")
	private WebElement contentFrame;

	/**
	 * @param webDriver
	 */
	public TMobileIDPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Profile page.
	 *
	 * @return the ProfilePage class instance.
	 */
	public TMobileIDPage verifyTMobileIDPage() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("spinner.whiteBg")));
			waitforSpinnerinTMobileIDPage();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("TMobileID page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("TMobileID page not displayed");
		}
		return this;

	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public TMobileIDPage clickNameEditBtn() {
		waitforSpinnerinTMobileIDPage();
		if (getDriver() instanceof AppiumDriver) {
			getDriver().get(getAttribute(contentFrame, "src"));
			try {
				checkPageIsReady();
				updateNameEditBtnForIOS.click();
				Reporter.log("Clicked on nick name");
			} catch (Exception e) {
				Assert.fail("Click on nick name failed");
			}
		} else {
			try {
				isFramePresent(contentFrame);
				getDriver().switchTo().frame("contentFrame");
				updateNameEditBtn.click();
				Reporter.log("Clicked on nick name");
			} catch (Exception e) {
				Assert.fail("Click on nick name failed");
			}
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public TMobileIDPage clickEmailEditBtn() {
		if (getDriver() instanceof AppiumDriver) {
			getDriver().get(getAttribute(contentFrame, "src"));
			try {
				checkPageIsReady();
				waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("img.spinner.whiteBg")));
				updateEmailEditBtnForIOS.click();
				Reporter.log("Clicked on email");
			} catch (Exception e) {
				Assert.fail("Click on email failed");
			}
		} else {
			try {
				isFramePresent(contentFrame);
				getDriver().switchTo().frame("contentFrame");
				updateEmailEditBtn.click();
				Reporter.log("Clicked on email");
			} catch (Exception e) {
				Assert.fail("Click on email failed");
			}
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public TMobileIDPage clickSecurityQstnsEditBtn() {
		if (getDriver() instanceof AppiumDriver) {
			getDriver().get(getAttribute(contentFrame, "src"));
			try {
				checkPageIsReady();
				waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("img.spinner.whiteBg")));
				updateSecurityQstnsEditBtnForIOS.click();
				Reporter.log("Clicked on security question");
			} catch (Exception e) {
				Assert.fail("Click on security question failed");
			}
		} else {
			try {
				isFramePresent(contentFrame);
				getDriver().switchTo().frame("contentFrame");
				updateSecurityQstnsEditBtn.click();
				Reporter.log("Clicked on security question");
			} catch (Exception e) {
				Assert.fail("Click on security question failed");
			}
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public TMobileIDPage clickPhoneNumberEditBtn() {
		if (getDriver() instanceof AppiumDriver) {
			getDriver().get(getAttribute(contentFrame, "src"));
			try {
				checkPageIsReady();
				waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("img.spinner.whiteBg")));
				updatePhoneNumberEditBtnForIOS.click();
				Reporter.log("Clicked on phone number");
			} catch (Exception e) {
				Assert.fail("Click on phone number failed");
			}
		} else {
			try {
				isFramePresent(contentFrame);
				getDriver().switchTo().frame("contentFrame");
				updatePhoneNumberEditBtn.click();
				Reporter.log("Clicked on phone number");
			} catch (Exception e) {
				Assert.fail("Click on phone number failed");
			}
		}
		return this;
	}

	/**
	 * Update name and click save.
	 */
	public TMobileIDPage updateNameAndClickSave() {
		waitforSpinnerinTMobileIDPage();
		checkPageIsReady();
		try {
			sendTextData(firstNameTxtField, "test");
			sendTextData(lastNameTxtField, "test");
			saveBtn.click();
			Reporter.log("Clicked on save button");
		} catch (Exception e) {
			Assert.fail("Click on save button failed");
		}
		return this;
	}

	/**
	 * Update security questions and click save.
	 */
	public TMobileIDPage updateSecurityQstnsAndClickSave() {
		waitforSpinnerinTMobileIDPage();
		checkPageIsReady();
		waitFor(ExpectedConditions.visibilityOf(securityQstn1));
		try {
			sendTextData(securityQstn1, "test");
			sendTextData(securityQstn2, "test");
			sendTextData(securityQstn3, "test");
			securityQstnssaveBtn.click();
		} catch (Exception e) {
			Assert.fail("Update security questions failed");
		}
		return this;
	}

	/**
	 * Update email and click save.
	 */
	public TMobileIDPage updateEmail() {
		waitforSpinnerinTMobileIDPage();
		checkPageIsReady();
		sendTextData(emailField, email.getText());
		sendTextData(reEnterEmailField, email.getText());
		return this;
	}

	/**
	 * Verify Email message
	 * 
	 * @return the TMobileIDPage class instance.
	 */
	public TMobileIDPage verifyEmailErrorMesage() {
		try {
			waitforSpinnerinTMobileIDPage();
			checkPageIsReady();
			Assert.assertTrue(isElementDisplayed(emailErrorMessage), "Email error message not displayed");
			Reporter.log("Email error message displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Email error message not displayed");
		}
		return this;

	}

	/**
	 * Verify linked phone number
	 * 
	 * @return the TMobileIDPage class instance.
	 */
	public TMobileIDPage verifyLinkedPhoneNumber() {
		if (getDriver() instanceof AppiumDriver) {
			// getDriver().get(getAttribute(contentFrame, "src"));
			try {
				checkPageIsReady();
				waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("img.spinner.whiteBg")));
				linkedPhoneNumber.isDisplayed();
				Reporter.log("Linked  number  displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Linked  number not displayed");
			}
		}
		if (getDriver() instanceof ChromeDriver) {
			try {
				checkPageIsReady();
				waitforSpinnerinTMobileIDPage();
				linkedPhoneNumber.isDisplayed();
				Reporter.log("Linked  number  displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Linked  number not displayed");
			}
		}
		return this;

	}

	/**
	 * Verify linked phone number
	 * 
	 * @return the TMobileIDPage class instance.
	 */
	public TMobileIDPage verifyTwoStepVerificationToggle() {
		if (getDriver() instanceof AppiumDriver) {
			getDriver().get(getAttribute(contentFrame, "src"));
			try {
				checkPageIsReady();
				waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("img.spinner.whiteBg")));
				waitFor(ExpectedConditions.visibilityOf(twoStepVerificationToggle));
				Assert.assertTrue(twoStepVerificationToggle.isDisplayed());
				Reporter.log("Two step verification toogle displayed");
			} catch (NoSuchElementException e) {
				Assert.fail("Two step verification toogle not displayed");
			}
		} else {
			try {
				isFramePresent(contentFrame);
				getDriver().switchTo().frame("contentFrame");
				Assert.assertTrue(twoStepVerificationToggle.isDisplayed());
				Reporter.log("Two step verification toogle displayed");
			} catch (AssertionError e) {
				Assert.fail("Two step verification toogle not displayed");
			}
		}
		return this;
	}

	public TMobileIDPage verifyNicknameCheckboxIsChecked() {
		waitforSpinnerinTMobileIDPage();
		checkPageIsReady();
		try {
			Assert.assertTrue(nickNameCheckBox.getAttribute("class").contains("not-empty"));
			Reporter.log("Nickname check box is selected");
		} catch (Exception e) {
			Assert.fail("Check box is not displayed");
		}
		return this;
	}

}
