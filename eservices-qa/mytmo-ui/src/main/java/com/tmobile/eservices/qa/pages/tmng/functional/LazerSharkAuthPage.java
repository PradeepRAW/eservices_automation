package com.tmobile.eservices.qa.pages.tmng.functional;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

/**
 * @author sputti
 *
 */
public class LazerSharkAuthPage extends TmngCommonPage {
	
	private final String pageUrl = "http://dev-tmo-author.corporate.t-mobile.com:4502/editor.html/content/t-mobile/consumer/offers/lazer-shark.html";
	
	@FindBy(xpath = "//input[@id='username']")
	private WebElement user;

	@FindBy(xpath = "//input[@id='password']")
	private WebElement pwd;
	
	@FindBy(xpath = "//button[@id='submit-button']")
	private WebElement signin;
	
	@FindBy(xpath = "//button[contains(text(),'Edit')]")
	private WebElement edit;
	
	@FindBy(xpath = "//button[@data-layer='Preview']")
	private WebElement Preview;
	
	@FindBy(xpath = "//div[@title='Lazer Shark']")
	private WebElement LS;

	@FindBy(xpath = "//textarea[@name='./global/tncCopyText']")
	private WebElement LegalTC;	
	
	@FindBy(xpath = "//input[@name='./global/tncLinkText']")
	private WebElement LegalTCLink;

	@FindBy(xpath = "//input[@name='./global/tncModalHeader']")
	private WebElement LegalTCModelHeader;
	
	@FindBy(xpath = "//input[@name='./global/tncPrimaryCta']")
	private WebElement LegalTCModelButton;
	
	//------------Survery------------
	@FindBy(xpath = "//coral-tab[@id='coral-id-497']")
	private WebElement Survery;
	
	@FindBy(xpath = "//input[@name='./survey/header']")
	private WebElement SurveyHeader;
	
	@FindBy(xpath = "//input[@name='./survey/statusHeader']")
	private WebElement SurveySubHeader;
	
	@FindBy(xpath = "//input[@name='./survey/carrierHeader']")
	private WebElement SurveyCarrierHeader;
	
	@FindBy(xpath = "//button[contains(text(),'Add field')]")
	private WebElement SurveyAddField;
	
	@FindBy(xpath = "//input[@name='./survey/wirelessProviders']")
	private WebElement SurveyProvider;
	
	@FindBy(xpath = "//input[@name='./survey/ratingHeader']")
	private WebElement SurveyUserRatingHeader;
	
	@FindBy(xpath = "//input[@name='./survey/tfbHeader']")
	private WebElement SurveytfbHeader;
	
	@FindBy(xpath = "//input[@name='./survey/yesLabel']")
	private WebElement SurveytfbYes;
	
	@FindBy(xpath = "//input[@name='./survey/noLabel']")
	private WebElement SurveytfbNo;
	
	@FindBy(xpath = "//input[@name='./survey/primaryCta']")
	private WebElement SurveyContinueButton;
	
	//----------- Customer Info ------------
	@FindBy(xpath = "//coral-tab[@id='coral-id-498']")
	private WebElement CustInfo;
	
	@FindBy(xpath = "//input[@name='./customer/header']")
	private WebElement CustInfoHeader;
	
	@FindBy(xpath = "//input[@name='./customer/statusHeader']")
	private WebElement CustInfoSubHeader;
	
	@FindBy(xpath = "//input[@name='./customer/firstNameError']")
	private WebElement CustInfoFirstNameError;
	
	@FindBy(xpath = "//input[@name='./customer/lastNameError']")
	private WebElement CustInfoLastNameError;
	
	@FindBy(xpath = "//input[@name='./customer/emailError']")
	private WebElement CustInfoEmailError;
	
	@FindBy(xpath = "//input[@name='./customer/confirmEmailError']")
	private WebElement CustInfoCEmailError;
	
	@FindBy(xpath = "//input[@name='./customer/phoneError']")
	private WebElement CustInfoPhoneError;
	
	@FindBy(xpath = "//input[@name='./customer/imeiToolTip']")
	private WebElement CustInfoIMEIToolTipMessage;
	
	@FindBy(xpath = "//input[@name='./customer/imeiError']")
	private WebElement CustInfoIMEIError;
	
	@FindBy(xpath = "//input[@name='./customer/primaryCta']")
	private WebElement CustInfoContinueButton;
	
	@FindBy(xpath = "//input[@name='./customer/secondaryCta']")
	private WebElement CustInfoBackButton;
	
	//-------Shipping Info-------
	
	@FindBy(xpath = "//coral-tab[@id='coral-id-499']")
	private WebElement ShipInfo;
	
	@FindBy(xpath = "//input[@name='./shipping/header']")
	private WebElement ShipInfoHeader;
	
	@FindBy(xpath = "//input[@name='./shipping/statusHeader']")
	private WebElement ShipInfoSubHeader;
	
	@FindBy(xpath = "//input[@name='./shipping/addressOneErrorHint']")
	private WebElement ShipInfoAddress1Error;
	
	@FindBy(xpath = "//input[@name='./shipping/cityErrorHint']")
	private WebElement ShipInfoCityError;
	
	@FindBy(xpath = "//input[@name='./shipping/stateErrorHint']")
	private WebElement ShipInfoStateError;
	
	@FindBy(xpath = "//input[@name='./shipping/zipCodeErrorHint']")
	private WebElement ShipInfoZipError;
	
	@FindBy(xpath = "//input[@name='./shipping/primaryCta']")
	private WebElement ShipInfoSubmitButton;
	
	@FindBy(xpath = "//input[@name='./shipping/secondaryCta']")
	private WebElement ShipInfoBackButton;
	
	@FindBy(xpath = "//input[@name='./shipping/modalHeading']")
	private WebElement ShipInfoModalHeader;
	
	@FindBy(xpath = "//input[@name='./shipping/modalBody']")
	private WebElement ShipInfoModalBody;
	
	@FindBy(xpath = "//input[@name='./shipping/modalPrimaryCta']")
	private WebElement ShipInfoConfirmButton;
	
	@FindBy(xpath = "//input[@name='./shipping/modalSecondaryCta']")
	private WebElement ShipInfoModalBackButton;
	
	//-------Error Message-------
	
	@FindBy(xpath = "//coral-tab[@id='coral-id-500']")
	private WebElement ErrorMessage;
	
	@FindBy(xpath = "//input[@name='./error/phoneError/heading']")
	private WebElement PhoneErrorHead;
	
	@FindBy(xpath = "//textarea[@name='./error/phoneError/body']")
	private WebElement PhoneErrorbody;
	
	@FindBy(xpath = "//input[@name='./error/emailCurrentCustomer/heading']")
	private WebElement EmailErrorHead;
	
	@FindBy(xpath = "//textarea[@name='./error/emailCurrentCustomer/body']")
	private WebElement EmailErrorbody;
	
	@FindBy(xpath = "//input[@name='./error/emailInactiveCustomer/heading']")
	private WebElement InactiveEmailErrorHead;
	
	@FindBy(xpath = "//textarea[@name='./error/emailInactiveCustomer/body']")
	private WebElement InactiveEmailErrorbody;
	
	@FindBy(xpath = "//input[@name='./error/shippingCurrentCustomer/heading']")
	private WebElement ShipErrorHead;
	
	@FindBy(xpath = "//textarea[@name='./error/shippingCurrentCustomer/body']")
	private WebElement ShipErrorbody;
	
	@FindBy(xpath = "//input[@name='./error/shippingOrderedAlready/heading']")
	private WebElement ExistShipErrorHead;
	
	@FindBy(xpath = "//textarea[@name='./error/shippingOrderedAlready/body']")
	private WebElement ExistShipErrorbody;
	
	@FindBy(xpath = "//input[@name='./error/shippingPOError/heading']")
	private WebElement ShipPOErrorHead;
	
	@FindBy(xpath = "//textarea[@name='./error/shippingPOError/body']")
	private WebElement ShipPOErrorbody;
	
	@FindBy(xpath = "//input[@name='./error/zipcodeError/heading']")
	private WebElement ZipErrorHead;
	
	@FindBy(xpath = "//textarea[@name='./error/zipcodeError/body']")
	private WebElement ZipErrorbody;
	
	@FindBy(xpath = "//input[@name='./error/genericError/heading']")
	private WebElement GenericErrorHead;
	
	@FindBy(xpath = "//textarea[@name='./error/genericError/body']")
	private WebElement GenericErrorbody;
	
	@FindBy(xpath = "//input[@name='./error/ctaLabel']")
	private WebElement ErrorCloseButton;
	
	@FindBy(xpath = "//button[@title='Done']")
	private WebElement DoneButton;
	
	@FindBy(xpath = "//a[@title='Page Information']")
	private WebElement PublishIcon;
	
	@FindBy(xpath = "//button[@title='Publish Page']")
	private WebElement PublishButton;
	
	/**
	 * @param webDriver
	 */
	public LazerSharkAuthPage(WebDriver webDriver) {
		super(webDriver);
	}
	
//   ------------------------------------------------------- LazerShark Page Load Verification --------------------------------
	/**
	 * Verify that the page loaded completely.
	 *.
	 */
	public LazerSharkAuthPage verifyLazerSharkAuthPageLoaded() {
		try {
			if (getDriver().getCurrentUrl().contains(pageUrl)) {
				Reporter.log("PASS - LazerShark page loaded");
			}
		} catch (NoSuchElementException e) {
			Assert.fail("FAIL - LazerShark page not loaded");
		}
		return this;
	}  
	/**
	 * Login
	 * 
	 */
	public LazerSharkAuthPage login() {
		try {
			sendTextData(user, Constants.LS_AUTH_LOGIN_USER);
			sendTextData(pwd, Constants.LS_AUTH_LOGIN_PWD);
			signin.click();
			waitFor(ExpectedConditions.visibilityOf(edit),5);
			Preview.click();
			waitFor(ExpectedConditions.visibilityOf(edit),5);
			edit.click();
			waitFor(ExpectedConditions.visibilityOf(edit),5);
			LS.click();
			Reporter.log("Click on Lazer Shark Preview Layout");
			doubleClick(LS);
			Reporter.log("Double Click on Lazer Shark Preview Layout");
			doubleClick(LS);
			waitFor(ExpectedConditions.visibilityOf(LegalTC),5);
			sendTextData(LegalTC, Constants.LS_AUTH_LEGAL_TEXT);	
			sendTextData(LegalTCLink, Constants.LS_AUTH_LEGAL_LINK);	
			sendTextData(LegalTCModelHeader, Constants.LS_AUTH_LEGAL_MODEL);	
			sendTextData(LegalTCModelButton, Constants.LS_AUTH_LEGAL_MODEL_BUTTON);
			//------------- Survery----------
			Survery.click();
			sendTextData(SurveyHeader, Constants.LAZERSHARK_FORM1_HEADER);
			sendTextData(SurveySubHeader, Constants.LAZERSHARK_FORM1_SUBHEADER);
			sendTextData(SurveyCarrierHeader, Constants.LAZERSHARK_CARRIER_HEADER);
			SurveyAddField.click();
			sendTextData(SurveyProvider, Constants.LAZERSHARK_PROVIDER);
			sendTextData(SurveyUserRatingHeader, Constants.LAZERSHARK_FORM1_UR_HEADER);
			sendTextData(SurveytfbHeader, Constants.LAZERSHARK_FORM1_TFD_HEADER);
			sendTextData(SurveytfbYes, Constants.LAZERSHARK_FORM1_TFB_YES);
			sendTextData(SurveytfbNo, Constants.LAZERSHARK_FORM1_TFB_NO);
			sendTextData(SurveyContinueButton, Constants.LAZERSHARK_FORM1_CONTINUE);
			//------------- Cust Info----------
			CustInfo.click();
			sendTextData(CustInfoHeader, Constants.LAZERSHARK_FORM2_HEADER);
			sendTextData(CustInfoSubHeader, Constants.LAZERSHARK_FORM2_SUBHEADER);
			sendTextData(CustInfoFirstNameError, Constants.LAZERSHARK_CUST_FNERROR);
			sendTextData(CustInfoLastNameError, Constants.LAZERSHARK_CUST_LNERROR);
			sendTextData(CustInfoEmailError, Constants.LAZERSHARK_CUST_EMAILERROR);
			sendTextData(CustInfoCEmailError, Constants.LAZERSHARK_CUST_CEMAILERROR);
			sendTextData(CustInfoPhoneError, Constants.LAZERSHARK_CUST_PHERROR);
			sendTextData(CustInfoIMEIToolTipMessage, Constants.LAZERSHARK_CUST_IMEITIPMESS);
			sendTextData(CustInfoIMEIError, Constants.LAZERSHARK_CUST_IMEIERROR);
			sendTextData(CustInfoContinueButton, Constants.LAZERSHARK_CUST_CONTINUE_BUTTON);
			sendTextData(CustInfoBackButton, Constants.LAZERSHARK_CUST_BACK_BUTTON);
			//-------------ShipInfo-------
			waitFor(ExpectedConditions.visibilityOf(ShipInfo),5);
			ShipInfo.click();
			sendTextData(ShipInfoHeader, Constants.LAZERSHARK_FORM3_HEADER);
			sendTextData(ShipInfoSubHeader, Constants.LAZERSHARK_FORM3_SUBHEADER);
			sendTextData(ShipInfoAddress1Error, Constants.LAZERSHARK_SHIP_ADDR1ERROR);
			sendTextData(ShipInfoCityError, Constants.LAZERSHARK_SHIP_CITYERROR);
			sendTextData(ShipInfoStateError, Constants.LAZERSHARK_SHIP_STATEERROR);
			waitFor(ExpectedConditions.visibilityOf(ShipInfo),5);
			sendTextData(ShipInfoZipError, Constants.LAZERSHARK_SHIP_ZIPERROR);
			sendTextData(ShipInfoSubmitButton, Constants.LAZERSHARK_SHIP_SUBMIT_BUTTON);
			sendTextData(ShipInfoBackButton, Constants.LAZERSHARK_SHIP_BACK_BUTTON);
			sendTextData(ShipInfoModalHeader, Constants.LAZERSHARK_SHIP_MODAL_HEADER);
			sendTextData(ShipInfoModalBody, Constants.LAZERSHARK_SHIP_MODAL_BODY);
			sendTextData(ShipInfoConfirmButton, Constants.LAZERSHARK_SHIP_CONFIRM_BUTTON);
			sendTextData(ShipInfoModalBackButton, Constants.LAZERSHARK_SHIP_MODAL_BACK_BUTTON);
			
			//-------Error Messages-------			
			waitFor(ExpectedConditions.visibilityOf(ErrorMessage),5);
			ErrorMessage.click();
			sendTextData(PhoneErrorHead, Constants.LAZERSHARK_PH_ERRORHEAD);
			sendTextData(PhoneErrorbody, Constants.LAZERSHARK_PH_ERRORBODY);

			sendTextData(EmailErrorHead, Constants.LAZERSHARK_EMAIL_ERRORHEAD);
			sendTextData(EmailErrorbody, Constants.LAZERSHARK_EMAIL_ERRORBODY);
			sendTextData(InactiveEmailErrorHead, Constants.LAZERSHARK_INACTIVEEMAIL_ERRORHEAD);
			sendTextData(InactiveEmailErrorbody, Constants.LAZERSHARK_INACTIVEEMAIL_ERRORBODY);					
			
			sendTextData(ShipErrorHead, Constants.LAZERSHARK_SHIP_ERRORHEAD);
			sendTextData(ShipErrorbody, Constants.LAZERSHARK_SHIP_ERRORBODY);
			sendTextData(ExistShipErrorHead, Constants.LAZERSHARK_EXISTSHIP_ERRORHEAD);
			sendTextData(ExistShipErrorbody, Constants.LAZERSHARK_EXISTSHIP_ERRORBODY);
			sendTextData(ShipPOErrorHead, Constants.LAZERSHARK_SHIPPO_ERRORHEAD);
			sendTextData(ShipPOErrorbody, Constants.LAZERSHARK_SHIPPO_ERRORBODY);
			
			sendTextData(ZipErrorHead, Constants.LAZERSHARK_ZIP_ERRORHEAD);
			sendTextData(ZipErrorbody, Constants.LAZERSHARK_ZIP_ERRORBODY);
						
			sendTextData(GenericErrorHead, Constants.LAZERSHARK_GENERIC_ERRORHEAD);
			sendTextData(GenericErrorbody, Constants.LAZERSHARK_GENERIC_ERRORBODY);
			
			sendTextData(ErrorCloseButton, Constants.LAZERSHARK_CLOSE_BUTTON_ERROR);
			DoneButton.click();
			waitFor(ExpectedConditions.visibilityOf(PublishIcon),5);
			PublishIcon.click();
			clickElement(PublishButton);
			waitFor(ExpectedConditions.visibilityOf(Preview),5);
			Preview.click();
			waitFor(ExpectedConditions.visibilityOf(edit),5);
			
		} catch (Exception e) {
			Assert.fail("Not able to login" + e.getMessage());
		}
		return this;
	}
}