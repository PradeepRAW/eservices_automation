package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class OpenInternetPolicyPage extends CommonPage {

	public static final String openInternetPolicyPageUrl = "/responsibility/consumer-info/policies/internet-service";
	public static final String openInternetPolicyPageLoadText = "Internet Services";

	@FindBy(xpath = "//h1[contains(text(),'Open Internet')]")
	private WebElement openInternetPolicyPageHeader;

	/**
	 * 
	 * @param webDriver
	 */
	public OpenInternetPolicyPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Privacy Policy Page
	 * 
	 * @return
	 */
	public OpenInternetPolicyPage verifyOpenInternetPrivacyPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageUrl(openInternetPolicyPageUrl);
			Reporter.log("Open internet policy page is displayed");
		}
		catch(Exception e)
		{
			Assert.fail("Open internet policy pagee not displayed");
		}
		return this;
	}
}
