package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class ShopEIPAPICollectionV3 extends ApiCommonLib {

	/***
	 * Summary: This API is used do the payments  through Shp EIP API
	 * Headers:Authorization,
	 * 	-Content-Type,
	 * 	-Accept,
	 * 	-interactionId,
	 *	-Authorization,
	 * 	-channel_id,
	 * 	-activityId,
	 * 	-application_id
	 * 	
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */

	private Map<String, String> buildShopEIPCollectionHeader(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", checkAndGetAccessToken());
		headers.put("Content-Type", "application/json");
		headers.put("Accept", "application/json");
		headers.put("interactionId", "123456787");
		headers.put("channel_id", "WEB");
		headers.put("activityId", "123456");
		headers.put("application_id", "ESERVICE");
		headers.put("X-B3-TraceId", "PaymentsAPITest123");
		headers.put("X-B3-SpanId", "PaymentsAPITest456");
		headers.put("transactionid","PaymentsAPITest");

		return headers;
	}

	public Response shop_EIP_Payments(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildShopEIPCollectionHeader(apiTestData);
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("payments/v3/eip", RestCallType.POST);
		return response;
	}	
}