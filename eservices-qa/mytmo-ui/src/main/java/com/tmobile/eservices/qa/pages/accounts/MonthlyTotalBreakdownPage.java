
/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author
 *
 */
public class MonthlyTotalBreakdownPage extends CommonPage {

	// Headers

	@FindBy(css = "P[class*='Display5']")
	private List<WebElement> totalMonthlyCost;

	@FindBy(xpath = "//p[contains(@class,'black Display4')]//following::p[@class='text-right']")
	private List<WebElement> costOfEachItemUnderEachLine;

	// Others
	@FindBy(css = ".Display3")
	private WebElement headingOfNewMonthlyTotalBreakDownPage;

	@FindBy(css = ".body.padding-top-small")
	private WebElement subHeadingOfNewMonthlyTotalBreakDownPage;

	@FindBy(xpath = "//*[@class='float-left']")
	private WebElement textNewMonthlyTotal;

	@FindBy(xpath = "(//span[@class='H6-heading black float-right'])")
	private WebElement totalCostAtAccountLevel;

	@FindBy(xpath = "(//*[@class='text-right']/span)")
	private List<WebElement> costOfEachItemUnderAccountLevel;

	@FindBy(xpath = "//*[@class='float-left']")
	private WebElement totalMonthlyDeviceCostAmount;

	@FindBy(xpath = "(//p[@class='Display6 text-right'])")
	private List<WebElement> costAtEachLineLevel;

	@FindBy(css = "(.SecondaryCTA.blackCTA.text-balck.full-btn-width-xs)")
	private WebElement backToSummaryCTA;

	@FindBy(xpath = "//*[@class='float-left']")
	private WebElement lineNameOnDeviceBreakdownPage;

	@FindBy(xpath = "//*[@class='float-left']")
	private WebElement lineNumberOnDeviceBreakdownPage;

	@FindBy(xpath = "//*[@class='float-left']")
	private WebElement priceOnLineLevelOnDeviceBreakdownPage;

	@FindBy(xpath = "//*[@class='SecondaryCTA blackCTA text-balck full-btn-width-xs']")
	private WebElement backToSummaryCTAOnDeviceBreakdownPage;

	@FindBy(xpath = "(//*[@class='black Display4 float-left'])/following::*[@class='text-right']")
	private List<WebElement> amountsAtEachLineLevel;

	@FindBy(xpath = "")
	private List<WebElement> namesOfEachLine;

	@FindBy(xpath = "")
	private List<WebElement> msisdnOfEachLine;

	@FindBy(xpath = "")
	private WebElement accountSectionLabel;

	@FindBy(xpath = "")
	private WebElement impactedSectionLabel;

	@FindBy(xpath = "")
	private WebElement nonImpactedSectionLabel;

	/**
	 * 
	 * @param webDriver
	 */
	public MonthlyTotalBreakdownPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Device Breakdown page is displayed
	 * 
	 * @return
	 */
	public MonthlyTotalBreakdownPage verifyNewMonthlyTotalBreakdownPage() {
		checkPageIsReady();
		waitForDataPassSpinnerInvisibility();
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("total-breakdown"));
			Reporter.log("New Monthly Total Breakdown page is displayed");
		} catch (Exception e) {
			Assert.fail("New Monthly Total Breakdown page is not displayed");
		}
		return this;
	}

	/**
	 * Check header on New Monthly Total Breakdown page
	 * 
	 * @return
	 */
	public MonthlyTotalBreakdownPage checkHeaderOfNewMonthlyTotalBreakDownPage() {
		try {
			Assert.assertTrue(headingOfNewMonthlyTotalBreakDownPage.getText().equals("New monthly total charges"),
					"Header of New Monthly Total Breakdown page is mismatched");
			Reporter.log("Header of New Monthly Total Breakdown page is matched.");
		} catch (Exception e) {
			Assert.fail("Header of New Monthly Total Breakdown page is not displayed.");
		}
		return this;
	}

	/**
	 * Check Subheader on New Monthly Total Breakdown page
	 * 
	 * @return
	 */
	public MonthlyTotalBreakdownPage checkSubHeaderOfNewMonthlyTotalBreakDownPage() {
		try {
			Assert.assertTrue(subHeadingOfNewMonthlyTotalBreakDownPage.getText().trim().contains("Here's how"),
					"SubHeader of New Monthly Total Breakdown page is mismatched");
			Assert.assertTrue(
					subHeadingOfNewMonthlyTotalBreakDownPage.getText().trim().contains("applies to your account."),
					"SubHeader of New Monthly Total Breakdown page is mismatched");
			Reporter.log("SubHeader of New Monthly Total Breakdown page is matched.");
		} catch (Exception e) {
			Assert.fail("SubHeader of New Monthly Total Breakdown page is not displayed.");
		}
		return this;
	}

	/**
	 * Check label "New Monthly Total"
	 * 
	 * @return
	 */
	public MonthlyTotalBreakdownPage checkLabelNewMonthlyTotal() {
		try {
			Assert.assertTrue(textNewMonthlyTotal.getText().trim().contains("New monthly total charges"),
					"Label New Monthly Total Charges is mismatched");
			Reporter.log("Label New Monthly Total Charges is matched");
		} catch (Exception e) {
			Assert.fail("Label New Monthly Total Charges is not displayed.");
		}
		return this;
	}

	// ################## Calculate for Total Monthly Cost ##################

	/**
	 * Read Total Monthly Cost
	 * 
	 * @return
	 */
	public double verifyTotalMonthlyCostAtTopAndBottom() {
		double monthlyTotalCost = 0.00;
		String costAtTop = null;
		String costAtBottom = null;

		try {
			totalMonthlyCost.get(0).isDisplayed();
			costAtTop = totalMonthlyCost.get(0).getText().trim();
		} catch (Exception e) {
			Reporter.log("Total Monthly cost is not displayed on top on New Monthly Total Breakdown page.");
		}
		try {
			totalMonthlyCost.get(1).isDisplayed();
			costAtBottom = totalMonthlyCost.get(1).getText().trim();
		} catch (Exception e) {
			Reporter.log("Total Monthly cost is not displayed at bottom top on New Monthly Total Breakdown page.");
		}
		try {
			if (costAtTop.equals(costAtBottom)) {
				Reporter.log("Total Monthly cost at Top and Bottom of New Monthly Total page is matched");
			} else
				Assert.fail("Total Monthly cost at Top and Bottom of New Monthly Total page is mismatched");
		} catch (Exception e) {
			Reporter.log(
					"Not able to calculate Total Monthly cost at top and bottom which displayed on New Monthly Total Breakdown page.");
		}
		try {
			String cost = totalMonthlyCost.get(0).getText().trim().replace("$", "");
			monthlyTotalCost = monthlyTotalCost + Double.parseDouble(cost);
		} catch (Exception e) {
			Reporter.log(
					"Not able to calculate Total Monthly cost which displayed on New Monthly Total Breakdown page.");
		}

		return monthlyTotalCost;
	}

	// ################## Calculate costs for Account section ##################
	/**
	 * Calculate amount for each item under Account Level
	 * 
	 * @return
	 */
	public double checkAndCalculateAmountForEachItemUnderAccountSection() {
		double costOfEachItemAtAccountLevel = 0.00;
		try {
			boolean disc = false;
			for (WebElement element : costOfEachItemUnderAccountLevel) {
				double costAtLineLevel = 0.00;
				String cost = element.getText().trim();
				String realCost;

				if (cost.contains("$")) {
					realCost = cost.replace("$", "");
					costOfEachItemAtAccountLevel = costOfEachItemAtAccountLevel + Double.parseDouble(realCost);

				} else if (cost.equalsIgnoreCase("Included")) {
					costOfEachItemAtAccountLevel = costOfEachItemAtAccountLevel + costAtLineLevel;
				} else if (disc == false) {
					realCost = cost.replace("$", "");
					costOfEachItemAtAccountLevel = costOfEachItemAtAccountLevel + Double.parseDouble(realCost);
				}
			}
		} catch (Exception e) {
			Assert.fail("Not able to calculate cost under each item.");
		}
		return costOfEachItemAtAccountLevel;
	}

	/**
	 * Read Account Level cost
	 * 
	 * @return
	 */
	public double checkCostAtAccountLevel() {
		double accountLevelCost = 0.00;
		try {
			String cost = totalCostAtAccountLevel.getText().trim().replace("$", "");
			accountLevelCost = accountLevelCost + Double.parseDouble(cost);
		} catch (Exception e) {
			Reporter.log("Not able to calculate Account level Addons cost.");
		}
		return accountLevelCost;

	}

	/**
	 * Verify Account Level cost
	 * 
	 * @return
	 */
	public double checkAndVerifyCostAtAccountLevel() {
		double totalOfEachItem;
		double costatAccountLevel;

		totalOfEachItem = checkAndCalculateAmountForEachItemUnderAccountSection();
		costatAccountLevel = checkCostAtAccountLevel();
		if (totalOfEachItem == costatAccountLevel)
			Reporter.log("Cost at Account level is matching");
		else
			Reporter.log("There is a mismatch in cost at Account level and items under account section");
		return costatAccountLevel;
	}

	// ########### Calculations for Line Level Section ##################

	/**
	 * Calculate amount for each item under each line
	 * 
	 * @return
	 */
	public double calculateTotalAmountForEachItemUnderAllLines() {
		double totalCostForEachItemForAllLines = 0.00;
		try {
			for (WebElement element : costOfEachItemUnderEachLine) {
				String cost = element.getText().trim();
				String realCost;

				if (!cost.equalsIgnoreCase("Included") && cost.contains("$") && !cost.equalsIgnoreCase("Free")) {
					realCost = cost.replace("$", "");
					totalCostForEachItemForAllLines = totalCostForEachItemForAllLines + Double.parseDouble(realCost);
				}
			}
		} catch (Exception e) {
			Assert.fail("Not able to calculate cost for each item under each line.");
		}
		return totalCostForEachItemForAllLines;
	}

	/**
	 * Calculate amount for each item under each line
	 * 
	 * @return
	 */
	public double calculateTotalAmountAtLineLevelForAllLines() {
		double totalCostAtLineLevelForAllLines = 0.00;
		try {

			for (WebElement element : costAtEachLineLevel) {

				String cost = element.getText().trim();
				String realCost;
				if (!cost.equalsIgnoreCase("Included") && cost.contains("$") && !cost.equalsIgnoreCase("Free")) {
					realCost = cost.replace("$", "");
					totalCostAtLineLevelForAllLines = totalCostAtLineLevelForAllLines + Double.parseDouble(realCost);
				}

			}
		} catch (Exception e) {
			Assert.fail("Not able to calculate cost for each item under each line.");
		}
		return totalCostAtLineLevelForAllLines;
	}

	/**
	 * Verify Costs of All Lines and Each item under each line
	 * 
	 * @return
	 */
	public double calculateAndVerifyTotalCostsAtLineLevel() {
		double totalCostAtLineLevelForAllLines;
		double totalCostForEachItemForAllLines;

		totalCostAtLineLevelForAllLines = calculateTotalAmountAtLineLevelForAllLines();
		totalCostForEachItemForAllLines = calculateTotalAmountForEachItemUnderAllLines();

		if (totalCostAtLineLevelForAllLines == totalCostForEachItemForAllLines)
			Reporter.log("Cost at Line level is matching");
		else
			Reporter.log("There is a mismatch in cost at Line level and items under each Line level section");
		return totalCostForEachItemForAllLines;
	}

	// Verify cost at Account Level and Line level
	public double checkAndVerifyTotalMonthlyLevelCostAgainstAccountLevelAndLineLevel() {

		double accountAndLineCost = 0.00;
		double totalMontlyCost = 0.00;

		totalMontlyCost = verifyTotalMonthlyCostAtTopAndBottom();

		accountAndLineCost = checkAndVerifyCostAtAccountLevel() + calculateAndVerifyTotalCostsAtLineLevel();

		if (totalMontlyCost == accountAndLineCost)
			Reporter.log(
					"On New Monthly Total breakdown page Cost at Total Monthly level is matching with Account level plus Line level");
		else
			Reporter.log(
					"On New Monthly Total breakdown page there is a mismatch in cost at Total Monthly level and Account level plus Line level");

		return accountAndLineCost;
	}

	// get Total Monthly Cost after verifying all costs at Account and Line
	// level
	public String getTotalMonthlyCostWithFormatOfDollarSign() {
		String formattedCost = null;
		double totalMontlyCost = 0.00;

		totalMontlyCost = checkAndVerifyTotalMonthlyLevelCostAgainstAccountLevelAndLineLevel();
		formattedCost = "$" + String.format("%.2f", totalMontlyCost);
		return formattedCost;
	}

	// get Total Monthly Cost without $ sign
	public double getTotalMonthlyCostWithoutDollarSign() {
		double totalMontlyCost = 0.00;
		totalMontlyCost = checkAndVerifyTotalMonthlyLevelCostAgainstAccountLevelAndLineLevel();
		return totalMontlyCost;
	}

	// #################################################

	/**
	 * Read Total Monthly Cost only
	 * 
	 * @return
	 */
	public int verifyTotalMonthlyCost() {
		int totalMonthlyCostValue = 0;
		String monthlyCost = totalMonthlyDeviceCostAmount.getText();
		try {
			String cost[] = totalMonthlyDeviceCostAmount.getText().split("$");
			totalMonthlyCostValue = Integer.parseInt(cost[0]);
		} catch (Exception e) {
			Assert.fail("Not able to calculate Total Monthly cost.");
		}
		return totalMonthlyCostValue;
	}

	// Verify Total Monthly cost for Single EIP Device
	public MonthlyTotalBreakdownPage verifyAndCheckTotalMonthlyCostWhenAccountHasOnlyOneDeviceOnEIP() {
		int totalMonthlyCost = 0;
		int totalCostAtLineLevel = 0;

		totalMonthlyCost = verifyTotalMonthlyCost();

		// totalCostAtLineLevel = calculateTotalMonthlyCostForSingleDevice();

		if (totalMonthlyCost == totalCostAtLineLevel)
			Reporter.log("Prices are matching in Total Monthly section");
		else
			Assert.fail("Prices are not matching in Total Monthly Section");

		return this;
	}

	// Check Name of each line
	public MonthlyTotalBreakdownPage verifyNamesOfEachLine() {
		String lineName = null;
		int count = namesOfEachLine.size();
		for (int i = 0; i < count; i++) {
			lineName = namesOfEachLine.get(i).getText();
			if (lineName.equals(""))
				Reporter.log("Name not displayed for line");
			else
				Reporter.log("Name displayed for line");
		}
		return this;
	}

	// Check MSISDN of each line
	public MonthlyTotalBreakdownPage verifyMSISDNOfEachLine() {
		String lineNumber = null;
		int count = msisdnOfEachLine.size();
		for (int i = 0; i < count; i++) {
			lineNumber = msisdnOfEachLine.get(i).getText();
			if (lineNumber.equals(""))
				Reporter.log("MSISDN is not displayed for line");
			else
				Reporter.log("MSISDN  displayed for line");
		}
		return this;
	}

	/**
	 * Check Total cost for each line
	 * 
	 * @return
	 */
	public int calculateTotalCostForEachLine() {
		int sum = 0;
		try {
			boolean disc = false;
			for (WebElement element : amountsAtEachLineLevel) {
				int costAtLineLevel = 0;
				String cost = element.getText();
				String realCost[];

				if (cost.contains("-"))
					disc = true;

				if (element.getText().equalsIgnoreCase("Included")) {
					costAtLineLevel = 0;
					sum = sum + costAtLineLevel;
				} else if (disc = true) {
					realCost = cost.split("$");
					sum = sum + Integer.parseInt(realCost[1]);
				} else if (disc = false) {
					realCost = cost.split("$");
					sum = sum + Integer.parseInt(realCost[0]);
				}
			}
		} catch (Exception e) {
			Assert.fail("Not able to calculate cost.");
		}
		return sum;
	}

	/**
	 * Click on Back to Summary CTA
	 * 
	 * @return
	 */
	public MonthlyTotalBreakdownPage clickOnBackToSummaryCTA() {
		try {
			backToSummaryCTAOnDeviceBreakdownPage.isDisplayed();
			backToSummaryCTAOnDeviceBreakdownPage.click();
			Reporter.log("Back to Summary CTA is clicked on Device Breakdown page.");
		} catch (Exception e) {
			Assert.fail("Back to Summary CTA is not displayed on Device Breakdown page.");
		}
		return this;
	}

	// Check Account section is displayed or not
	public MonthlyTotalBreakdownPage verifyAccountSection() {
		try {
			accountSectionLabel.isDisplayed();
			Reporter.log("Account section is displayed.");
		} catch (Exception e) {
			Assert.fail("Label Account is not displayed");
		}
		return this;
	}

	// Check Impacted section is displayed or not
	public MonthlyTotalBreakdownPage verifyImpactedSection() {
		try {
			impactedSectionLabel.isDisplayed();
			Reporter.log("Impacted label is displayed.");
		} catch (Exception e) {
			Assert.fail("Impacted Label is not displayed");
		}
		return this;
	}

	// Check Non Impacted section is displayed or not
	public MonthlyTotalBreakdownPage verifyNonImpactedSectionSection() {
		try {
			nonImpactedSectionLabel.isDisplayed();
			Reporter.log("Non Impacted label is displayed.");
		} catch (Exception e) {
			Assert.fail("Non Impacted Label is not displayed");
		}
		return this;
	}

	// Verify cost On New Monthly Total Blade
	public MonthlyTotalBreakdownPage verifyCostOnNewMonthlyTotalBlade(String cost1, String cost2) {
		try {
			Assert.assertTrue(cost1.equals(cost2),
					"Cost on New Monthly Total Blade is matched with Monthly Cost on New Monthly breakdown page is not matched");
			Reporter.log(
					"Cost on New Monthly Total Blade is matched with Monthly Cost on New Monthly breakdown page is matched");
		} catch (Exception e) {
			Assert.fail(
					"Cost on New Monthly Total Blade is matched with Monthly Cost on New Monthly breakdown page is not matched");
			Reporter.log(
					"Cost on New Monthly Total Blade is matched with Monthly Cost on New Monthly breakdown page is not matched");
		}
		return this;
	}

}