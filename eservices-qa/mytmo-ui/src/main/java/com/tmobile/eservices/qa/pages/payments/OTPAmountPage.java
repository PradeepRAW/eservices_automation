package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * URL : onetimepayment/amount
 * 
 * @author pshiva
 *
 */
public class OTPAmountPage extends CommonPage {

	private By otpSpinner = By.cssSelector("i.fa.fa-spinner");

	@FindAll({ @FindBy(css = "div[ng-click*='Amount']"), @FindBy(xpath = "//span[contains(text(),'Amount')]") })
	private WebElement amountIcon;

	@FindBy(css = "[ng-click='vm.updateAmount()']")
	private WebElement updateAmount;

	@FindBy(css = "div.other-amount-alert")
	private List<WebElement> otherAmountAlert;

	@FindBy(css = "div#continueAmountAlert button")
	private WebElement yesOtherAmountAlert;

	@FindBy(css = "input#otherAmount")
	private WebElement otherAmountTextBox;

	@FindBy(xpath = "//p[contains(text(),'Other')]//..//..//input")
	private WebElement otherAmountcheckbox;

	@FindBy(css = "i.fa-warning")
	private WebElement warningLogo;

	@FindBy(css = "p.inline.text-error")
	private List<WebElement> inlineErrorMsgs;

	@FindBy(css = ".recommendedDIV .xsmall.pull-right")
	private List<WebElement> viewDetailsLink;

	@FindBy(css = ".h2-Uppercase-mob.h2-Uppercase")
	private WebElement breakdownHeader;

	@FindBy(css = ".ico.ico-closeIcon")
	private WebElement breakdownCloseIcon;

	@FindBy(css = "div.margin-left-errormsg")
	private WebElement errorMsgOtherAmount;

	@FindBy(css = "div.fine-print-body.text-black.ng-binding")
	private List<WebElement> alertMsgSedona;

	@FindBy(css = "div.col-12.col-md-10 app-dynamic-component")
	private WebElement autopayAlertNonSuspended;

	@FindBy(css = "#otp-paAlerts div p")
	private WebElement warningtextDueAmount;

	@FindBy(css = "#otp-paAlerts div div.exclamation-circle")
	private WebElement highSeverityWarningLogo;

	@FindAll({ @FindBy(css = "p.button-title.ng-binding"), @FindBy(css = "span.Display3") })
	private WebElement editAmountPageHeader;

	@FindAll({ @FindBy(css = "span.padding-horizontal-xsmall.Display6"),
			@FindBy(css = ".body-copy-description.text-nowrap.ng-binding") })
	private List<WebElement> amountRadioButtons;

	@FindBy(xpath = "//span[contains(text(),'Total')]/following-sibling::span")
	private WebElement balanceDueAmount;
	
	

	@FindBy(css = "button.SecondaryCTA.glueButton")
	private WebElement cancelBtn;

	@FindBy(linkText = "review previous payments")
	private WebElement reviewPreviouspayment;

	@FindBy(css = "i.fa.fa-spinner")
	private WebElement pageSpinner;

	@FindBy(linkText = "button.SecondaryCTA.glueButton")
	private WebElement pageLoadElement;

	private final String pageUrl = "/amount";

	@FindBy(css = "p.description-red-subline")
	private WebElement otherAmountWarningText;

	@FindBy(css = "div.fine-print-body.ng-binding")
	private WebElement warningText;

	@FindBy(xpath = "//p[contains(text(),'Total Balance') or contains(text(),'Pay in Full')]/following-sibling::p[contains(@class,'ng-scope')]")
	private WebElement textUnderTotalBal;

	@FindBy(xpath = "//p[contains(text(),'Past due') or contains(text(),'Due Today')]/following-sibling::p[contains(@class,'ng-scope')]")
	private WebElement textUnderPastDue;

	@FindBy(xpath = "//p[contains(text(),'Other')]/following-sibling::p[contains(@class,'ng-scope')]")
	private WebElement textUnderOther;

	@FindBy(css ="div.fine-print-body.text-black")
	private WebElement paymentArrangementAlert;
	
	@FindBy(xpath = "//p[contains(text(),'Total')]/../../../../following-sibling::div//span[contains(@class,'ng-binding')]")
	private WebElement totalBalanceDue;
	
	@FindBy(xpath = "//p[contains(text(),'Past due')]/../../../../following-sibling::div//span[contains(@class,'ng-binding')]")
	private WebElement pastDueBalanceDue;

	
	
	/**
	 * page constructor
	 * 
	 * @param webDriver
	 */
	public OTPAmountPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public OTPAmountPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public OTPAmountPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			verifyPageUrl();
			editAmountPageHeader.isDisplayed();
			Reporter.log("Verified Edit Amount page as displyaed");
		} catch (Exception e) {
			Assert.fail("Edit Amount page is not displayed - OTP");
		}

		return this;
	}

	/**
	 * Click edit button for amount
	 */
	public OTPAmountPage clickamountIcon() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(amountIcon));
			clickElementWithJavaScript(amountIcon);
			Reporter.log("Clicked on Amount icon");
		} catch (Exception e) {
			Assert.fail("Amount icon not found");
		}
		return this;
	}

	/**
	 * Click update amount button
	 */
	public OTPAmountPage clickUpdateAmount() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(updateAmount));
			clickElementWithJavaScript(updateAmount);
			waitFor(ExpectedConditions.invisibilityOfElementLocated(otpSpinner));
			if (!otherAmountAlert.isEmpty() && otherAmountAlert.get(0).isDisplayed()) {
				yesOtherAmountAlert.click();
			}
			Reporter.log("Clicked on update button");
		} catch (Exception e) {
			Assert.fail("Update button not found");
		}
		return this;
	}

	/**
	 * Set Other Amount
	 */
	public OTPAmountPage setOtherAmount(String data) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(otherAmountTextBox));
			waitFor(ExpectedConditions.elementToBeClickable(otherAmountTextBox));
			otherAmountTextBox.click();
			otherAmountTextBox.clear();
			otherAmountTextBox.sendKeys(data);
			otherAmountTextBox.sendKeys(Keys.ENTER);
			Reporter.log("Entered Other amount");
		} catch (Exception e) {
			Assert.fail("Other amount field not found");
		}
		return this;
	}

	/**
	 * Verify Inline Error Msgs OTP page on click of Agg
	 * 
	 * @param radioBtn
	 */
	public OTPAmountPage verifyInlineErrorMsgs(String errorMsg) {
		try {
			for (WebElement inlineErr : inlineErrorMsgs) {
				inlineErr.isDisplayed();
				Assert.assertTrue(inlineErr.getText().equals(errorMsg));
			}
			Reporter.log("Verified inline error Messages");
		} catch (Exception e) {
			Assert.fail("Inline error message component missing");
		}
		return this;
	}

	/**
	 * click view details link
	 * 
	 * @return
	 */
	public OTPAmountPage clickViewDetails() {
		try {
			viewDetailsLink.get(0).click();
			Reporter.log("clicked on view details link ");
		} catch (Exception e) {
			Assert.fail("view details link not found");
		}
		return this;
	}

	/**
	 * verify breakdown modal
	 * 
	 * @return
	 */
	public OTPAmountPage verifyBreakdownModal() {
		try {
			Assert.assertTrue(breakdownHeader.getText().equals("BREAKDOWNS"));
			breakdownCloseIcon.click();
			Reporter.log("Verified breakdown header and clicked on breakdown button");
		} catch (Exception e) {
			Assert.fail("Break down model not found");
		}
		return this;
	}

	/***
	 * verify view details link
	 * 
	 * @return
	 */
	public OTPAmountPage verifyViewDetailsLink() {
		try {
			if (viewDetailsLink.size() > 1) {
				viewDetailsLink.get(0).isDisplayed();
				viewDetailsLink.get(1).isDisplayed();
			}
			viewDetailsLink.get(0).isDisplayed();
			Reporter.log("Verified view details link");
		} catch (Exception e) {
			Assert.fail("View details link missing");
		}
		return this;
	}

	/***
	 * verify amount radio button
	 * 
	 * @param radioBtnTxt
	 * @return
	 */
	public OTPAmountPage verifyAmountRadioButton(String radioBtnTxt) {
		boolean isDisplayed = false;
		try {
			for (WebElement radioButton : amountRadioButtons) {
				if (radioButton.isDisplayed() && radioButton.getText().contains(radioBtnTxt)) {
					isDisplayed =  true;
					Reporter.log("Verified amount radio button");
				}
			}
			if (!isDisplayed) {
				Reporter.log("Verified amount radio button" + radioBtnTxt +" is not displayed");
			}
		} catch (Exception e) {
			Assert.fail(radioBtnTxt + "- Amount radio button not found");
		}
		return this;
	}

	/**
	 * verify update CTA is displayed
	 * 
	 * @return boolean
	 */
	public OTPAmountPage verifyUpdateAmountCTA() {
		try {
			updateAmount.isDisplayed();
			Reporter.log("Verified update amount CTA");
		} catch (Exception e) {
			Assert.fail("Update amount CTA not found");
		}
		return this;
	}

	/***
	 * verify amount is populated
	 * 
	 * @param bal
	 * @return
	 */
	public OTPAmountPage verifyotherAmountIsPopulated(String bal) {
		try {
			checkPageIsReady();
			otherAmountTextBox.click();
			Assert.assertTrue(otherAmountTextBox.getAttribute("value").equals(bal));
			Reporter.log("Verified other amount");
		} catch (Exception e) {
			Assert.fail("Other amount field not found");
		}
		return this;
	}

	/**
	 * click amount radio buttons
	 * 
	 * @param radioBtnTxt
	 */
	public OTPAmountPage clickAmountRadioButton(String radioBtnTxt) {
		try {
			for (WebElement radioButton : amountRadioButtons) {
				if (radioButton.isDisplayed() && radioButton.getText().equals(radioBtnTxt)) {
					radioButton.click();
					break;
				}
			}
			Reporter.log("Clicked on Amount radio button");
		} catch (Exception e) {
			Assert.fail("Amount radio button not found");
		}
		return this;
	}

	/**
	 * verify amount field value is null
	 * 
	 * @return boolean
	 */
	public OTPAmountPage verifyOtherAmountCacheCleared() {
		try {
			if (otherAmountTextBox.getAttribute("placeholder").equals("0.00")
					|| otherAmountTextBox.getAttribute("value").isEmpty())
				Reporter.log("Clicked on AutoPay SignUP button");
			else
				Assert.fail("Verified other amount cache cleared");
		} catch (Exception e) {
			Assert.fail("Other amount cache not cleared");
		}
		return this;
	}

	/***
	 * Verify payment amount sedon alert
	 * 
	 * @param warningText
	 * @return
	 */
	public OTPAmountPage verifyPaymentAmountSedonaAlert(String warningText) {
		try {
			errorMsgOtherAmount.isDisplayed();
			Assert.assertTrue(errorMsgOtherAmount.getText().contains(warningText));
			Reporter.log("Verified payment amount in sedona alert");
		} catch (Exception e) {
			Assert.fail("Sedona alert not found");
		}
		return this;
	}

	/***
	 * verify payment amount sedona alert message
	 * 
	 * @param message
	 * @return
	 */
	public OTPAmountPage verifyPaymentAmountSedonaAlertMsg(String message) {
		try {
			checkPageIsReady();
			verifyElementBytext(alertMsgSedona, message);
			//alertMsgSedona.isDisplayed();
			//Assert.assertTrue(alertMsgSedona.getText().contains(message));
			Reporter.log("Verified Sedona alert message");
		} catch (Exception e) {
			Assert.fail("Sedona alert message not found");
		}
		return this;
	}

	/***
	 * Verify payment amount sedona alert
	 * 
	 * @param message
	 * @return
	 */
	public OTPAmountPage verifyPaymentAmountSedonaAlertInRed(String message) {
		try {
			errorMsgOtherAmount.isDisplayed();
			Assert.assertTrue(errorMsgOtherAmount.getText().contains(message));
			Assert.assertTrue(errorMsgOtherAmount.getAttribute("class").contains("color-red"));
			Reporter.log("Verified payment amount Alert in Red");
		} catch (Exception e) {
			Assert.fail("Sedona alert not found");
		}
		return this;
	}

	/**
	 * verify if the default radio button is selected
	 * 
	 * @param radioBtnTxt
	 * @return true/false
	 */
	public Boolean verifyAmountRadioButtonIsSelected(String radioBtnTxt) {
		Boolean isRadioBtnSelected = false;
		try {
			String elementText;
			for (WebElement radioButton : amountRadioButtons) {
				if (radioButton.isDisplayed() && radioButton.getText().contains(radioBtnTxt)) {
					elementText = radioButton.getText();
					String radioBtnXpath = "//span[contains(text(),'" + elementText
							+ "')]//..//..//div//input[@name='otp-amount']";
					WebElement radioBtn;
					try {
						radioBtn = getDriver().findElement(By.xpath(radioBtnXpath));
					} catch (Exception e) {
						radioBtnXpath = "//p[contains(text(),'" + elementText + "')]//..//..//..//label//input";
						radioBtn = getDriver().findElement(By.xpath(radioBtnXpath));;
					}
					isRadioBtnSelected = radioBtn.isSelected();
				}
			}
			Assert.assertTrue(isRadioBtnSelected, radioBtnTxt + " :Radio button is not selected");
			Reporter.log("Radio button selected");
		} catch (Exception e) {
			Assert.fail("Exception occurred while verifying if the " + radioBtnTxt
					+ " is selected by default - Step Failed");
		}
		return isRadioBtnSelected;
	}

	/**
	 * 
	 * verify if warning message is displayed when a lesser amount than the due
	 * amount is entered into other amount text box.
	 */
	public OTPAmountPage verifyWarningTextForLessAmount() {
		try {
			String otherAmount = otherAmountTextBox.getAttribute("value");
			Double lessAmount = Double.parseDouble(otherAmount) - 1;
			setOtherAmount(lessAmount.toString());
			Assert.assertTrue(warningtextDueAmount.getText()
					.contains("You need to have paid $" + otherAmount + " toward your Payment Arrangement by"));
			Assert.assertTrue(warningtextDueAmount.getText()
					.contains("to avoid suspension & fees. Increase amount or review previous payments."));
			if (warningtextDueAmount.isDisplayed() && highSeverityWarningLogo.isDisplayed())
				Reporter.log("Verified warning text for less amount");
		} catch (Exception e) {
			Assert.fail("Warning text is not displayed as expected when less than due amount is entered.");
		}
		return this;
	}

	/***
	 * Get balance amount
	 * 
	 * @return
	 */
	public String getBalanceDueAmount() {
		String bal = "";
		try {
			if (balanceDueAmount.isDisplayed()) {
				bal = balanceDueAmount.getText().replace("$", "");
			}

		} catch (Exception balDueNotCaptured) {
			Assert.fail("Balance Due could NOT be captured. ");
			bal = "Error";
		}
		return bal;
	}

	/***
	 * Click cancel button
	 * 
	 * @return
	 */
	public OTPAmountPage clickCancelBtn() {
		try {
			cancelBtn.click();
			Reporter.log("Clicked on Cancel button");
		} catch (Exception e) {
			Assert.fail("Cancel button not found");
		}
		return this;
	}

	/***
	 * Click review previous payments
	 * 
	 * @return
	 */
	public OTPAmountPage clickReviewPreviouspayments() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.elementToBeClickable(reviewPreviouspayment));
			clickElementWithJavaScript(reviewPreviouspayment);
			Reporter.log("Clicked on review previous payments");
		} catch (Exception e) {
			Assert.fail("Review payments not found");
		}
		return this;
	}

	/***
	 * Verified cancel button
	 * 
	 * @return
	 */
	public OTPAmountPage verifyCancelCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(cancelBtn));
			Assert.assertTrue(cancelBtn.isDisplayed(), "Cancel button CTA is not found");
		} catch (Exception e) {
			Assert.fail("Cancel button CTA is not found");
		}
		return this;
	}

	/***
	 * Verify the default selection
	 * 
	 * @return
	 */
	public OTPAmountPage verifyTotalOrPastDueButtonIsSelected() {
		try {
			Assert.assertTrue(verifyRadioButtonSelected("Total") || verifyRadioButtonSelected("Past"));
		} catch (Exception e) {
			Assert.fail("Total/Past due radio butotn is not selected");
		}
		return this;
	}

	/***
	 * Verify the message below Other radio button verify the notification when
	 * custom Amount lesser than the past due Amount
	 * 
	 * @return
	 */
	public OTPAmountPage verifyWarningTextsWhenLessAmountEntered() {
		try {
			Assert.assertTrue(otherAmountWarningText.isDisplayed(), "Other Warning Text is not Displayed");
			Assert.assertTrue(otherAmountWarningText.getText().contains("required to upgrade"),
					"Other Amount Warning Text is not Correct");
			Assert.assertTrue(warningText.isDisplayed(), "Warning Text when less Amount is entered is not Displayed");
			Assert.assertTrue(warningText.getText().equals("This payment will not make you eligible to upgrade"),
					"Warning Text when less Amount is entered is not Correct");
			Reporter.log("Verified inline error Message when other amount is less than required pastdue");
		} catch (Exception e) {
			Assert.fail("Inline error message component missing");
		}
		return this;
	}

	/**
	 * verify if the default radio button is selected
	 * 
	 * @param radioBtnTxt
	 */
	public Boolean verifyRadioButtonSelected(String radioBtnTxt) {
		Boolean isRadioBtnSelected = false;
		try {
			String elementText;
			for (WebElement radioButton : amountRadioButtons) {
				if (radioButton.isDisplayed() && radioButton.getText().contains(radioBtnTxt)) {
					elementText = radioButton.getText();
					String radioBtnXpath = "//p[contains(text(),'" + elementText + "')]//..//..//..//..//div//input";
					WebElement radioBtn = getDriver().findElement(By.xpath(radioBtnXpath));
					isRadioBtnSelected = radioBtn.isSelected();
				}
			}
		} catch (Exception e) {
			Assert.fail("Exception occurred while verifying if the " + radioBtnTxt
					+ " is selected by default - Step Failed");
		}
		return isRadioBtnSelected;
	}

	/**
	 * verify if the default radio button is selected
	 * 
	 * @param radioBtnTxt
	 */
	public OTPAmountPage clickUnselectedRadioButton() {
		try {
			String elementText;
			for (WebElement radioButton : amountRadioButtons) {
				if (radioButton.isDisplayed()
						&& (radioButton.getText().contains("Total") || radioButton.getText().contains("Past"))) {
					elementText = radioButton.getText();
					String radioBtnXpath = "//p[contains(text(),'" + elementText + "')]//..//..//..//..//div//input";
					WebElement radioBtn = getDriver().findElement(By.xpath(radioBtnXpath));
					if (!radioBtn.isSelected()) {
						radioButton.click();
						break;
					}
				}
			}
		} catch (Exception e) {
			Assert.fail("Exception occurred while clicking the radio button - Step Failed");
		}
		return this;
	}

	/***
	 * Verify the message below total Balance radio button
	 * 
	 * @return
	 */
	public OTPAmountPage verifyTextUnderTotalBal(String totalBlatext) {
		try {
			Assert.assertTrue(textUnderTotalBal.isDisplayed(), "Message below Total balance is not Displayed");
			Assert.assertTrue(
					textUnderTotalBal.getText().contains(totalBlatext),
					"Message under Total Balance is not correct");
			Reporter.log("Verified inline Message under Total balance");
		} catch (Exception e) {
			Assert.fail("Inline message component missing under Total balance");
		}
		return this;
	}

	/***
	 * Verify the message below Past Due Balance radio button
	 * 
	 * @return
	 */
	public OTPAmountPage verifyTextUnderPastDue(String pastDueText) {
		try {
			Thread.sleep(1000);
			Assert.assertTrue(textUnderPastDue.isDisplayed(), "Message below Past Due balance is not Displayed");
			Assert.assertTrue(textUnderPastDue.getText().contains(pastDueText),"Message under Past Due is not correct");
			Reporter.log("Verified inline Message under Past due balance");
		} catch (Exception e) {
			Assert.fail("Inline message component missing under Past due balance");
		}
		return this;
	}

	/**
	 * verify text under Other amount field
	 * 
	 * @return page
	 */
	public OTPAmountPage verifyTextUnderOther(String otherAmountText) {
		try {
			Assert.assertTrue(textUnderOther.isDisplayed(), "Text Under Other is not Displayed");
			String value = textUnderOther.getText();
			Assert.assertTrue(textUnderOther.getText().contains(otherAmountText),"Text Under Other is not Correct");
			Reporter.log("Verified inline Message under Past due balance");
		} catch (Exception e) {
			Assert.fail("Inline message component missing under Past due balance");
		}
		return this;
	}

	/***
	 * Verify the notification when Amount Greater is Entered Verify the
	 * notification when Amount Equal is Entered
	 * 
	 * @return
	 */
	public OTPAmountPage verifyWarningText(String text) {
		try {
			Assert.assertTrue(warningText.isDisplayed(), "Warning Text is not Displayed");
			String value = warningText.getText();
			Assert.assertTrue(warningText.getText().equalsIgnoreCase(text), "Warning Text is not correct");
		} catch (Exception e) {
			Assert.fail("Warning text is not displayed as expected when less than due amount is entered.");
		}
		return this;
	}

	/**
	 * Get other amount text box value
	 * 
	 * @return value
	 */
	public String getOtherAmount() {
		try {
			return otherAmountTextBox.getAttribute("value");
		} catch (Exception e) {
			Assert.fail("Failed getting other amount value");
		}
		return null;
	}

	public void verifyPAAlert(String paAlert) {	try {
		paymentArrangementAlert.isDisplayed();
		Assert.assertTrue(paymentArrangementAlert.getText().contains(paAlert),"Payment Arrangement Notification on OTP Edit amount page is not displayed/as expected");
	} catch (Exception e) {
		Assert.fail("Payment Arrangement Notification on OTP Edit amount page is not displayed");
	}}
	
	public OTPAmountPage clickReviewPaymentsMade() {
		try {
			reviewPreviouspayment.click();
			Reporter.log("Clicked on review payment made button");
		} catch (Exception e) {
			Assert.fail("Review payment made button not found");
		}
		return this;
	}

	/**
	 * verify Digital PA messaging
	 * @param digitalPaMsg
	 */
	public void verifyDigitalPAAlert(String digitalPaMsg) {
		try {
			paymentArrangementAlert.isDisplayed();
			Assert.assertTrue(paymentArrangementAlert.getText().contains(digitalPaMsg),"Digital PA Notification on OTP Edit amount page is not displayed/as expected");
		} catch (Exception e) {
			Assert.fail("DIgital PA Notification on OTP Edit amount page is not displayed");
		}		
	}
	
	
	/***
	 * verify amount radio button
	 * 
	 * @param radioBtnTxt
	 * @return
	 */
	public boolean verifyTotalBalanceequaltoPastDueBal() {
		boolean isDisplayed = false;
		try {
			if ( totalBalanceDue.getText().equals(pastDueBalanceDue.getText())) {
					isDisplayed =  true;
					Reporter.log("total balance equal to past due balance");
				}		
			
		} catch (Exception e) {
			
		}
		return isDisplayed;
	}
	/***
	 * verify Total Balance" & "Past Due Balance radio buttons are suppressed
	 * 
	 * @param radioBtnTxt
	 * @return 
	 */
	public OTPAmountPage verifyTotalandPastDueBalanceRadioButtonSupressedforNonMasterUser(MyTmoData myTmoData) {
	
		try {
			
			Assert.assertEquals(amountRadioButtons.size(), 1);
			Assert.assertTrue(amountRadioButtons.get(0).getText().contains("Other"), "Total Balance & Past Due Balance radio buttons are not suppressed");	
			Reporter.log("Total Balance & Past Due Balance radio buttons are  suppressed for "+myTmoData.getPayment().getUserType());
		System.out.println("Total Balance & Past Due Balance radio buttons are  suppressed for "+myTmoData.getPayment().getUserType());
		} catch (Exception e) {
			Assert.fail("Total Balance & Past Due Balance radio buttons are not suppressed for "+myTmoData.getPayment().getUserType());
		}
		return this;
	}
	
	/**
	 * verify other amount divison
	 */
	public void verifyOtherAmountDiv() {
		try {
			Assert.assertFalse(isElementDisplayed(otherAmountTextBox),"Other amount div displayed");
		} catch (Exception e) {
			Assert.fail("Other amount div not displayed");
		}		
	}
}