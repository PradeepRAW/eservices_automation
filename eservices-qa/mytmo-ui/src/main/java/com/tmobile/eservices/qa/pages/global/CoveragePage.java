/**
 * 
 */
package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class CoveragePage extends CommonPage {
	public static final String coverragePageUrl = "coverage";
	public static final String coveragePageLoadText = "";

	@FindBy(id = "pccSearchButton")
	private WebElement legacyCoveragePage;

	/**
	 * 
	 * @param webDriver
	 */
	public CoveragePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Legacy Coverage Page
	 * 
	 * @return
	 */
	public CoveragePage verifyCoveragePage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageLoaded();
			verifyPageUrl(coverragePageUrl);
			Reporter.log("Coverage page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Coverage page not displayed");
		}
		return this;

	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the Coverage Page class instance.
	 */

	public CoveragePage verifyPageLoaded() {
		try {
			checkPageIsReady();
			getDriver().getPageSource().contains(coveragePageLoadText);

		} catch (Exception e) {
			Reporter.log("Coverage page not loaded " + e.getMessage());
		}
		return this;
	}

}
