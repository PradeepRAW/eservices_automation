/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class TradeInAnotherDevicePage extends CommonPage {

	@FindBy(css = "div h3.h3-title")
	private WebElement tradeInAnotherDevice;

	@FindBy(css = "div#isRadioBad span.radio-ht")
	private WebElement itsGotIssuesRadioBtn;

	@FindBy(css = "div#isRadioGud span.radio-ht")
	private WebElement itsGoodRadioBtn;

	@FindBy(css = "span.radio-ht")
	private List<WebElement> radioButtons;

	@FindBy(linkText = "What is this?")
	private WebElement whatIsThis;

	@FindBy(css = "div.col-xs-12 img")
	private WebElement deviceImage;

	@FindBy(css = "h2[ng-bind*='ctrl.whatThisContentData.findyourdevice']")
	private WebElement whatIsThisModelTitle;

	@FindBy(css = "h2[ng-bind*='findyourdevice']")
	private WebElement whatIsThisModelHeader;

	@FindBy(css = "p[ng-bind*='bodytext']")
	private WebElement whatIsThisModelBody;

	@FindBys(@FindBy(css = "div.display-flex span div.ussd-code-border"))
	private List<WebElement> numberToDial;

	@FindBy(css = "button[class='close pull-right uib-close']")
	private WebElement closeWhatIsThisModel;

	@FindBy(css = "button[ng-click*='ValidateIMEI']")
	private WebElement continueButton;

	@FindBy(css = "button.btn.PrimaryCTA-Normal")
	private WebElement tradeInDevicecontinueButton;

	@FindBy(css = "p.body-copy-description")
	private WebElement promotionModel;

	@FindBy(xpath = "//span[contains(@ng-show, '!$ctrl.showCarrier')]//..//..//div[@id='model']")
	private WebElement carrier;

	@FindBys(@FindBy(css = "ul[aria-labelledby='model'] li[ng-repeat*='$ctrl.CarrierList'] a"))
	private List<WebElement> cairrerOptions;

	@FindBy(xpath = "//label[text()=' Carrier']//..//div//a[text()='ATT']")
	private WebElement mobileCarrierOption;
	
	@FindBy(xpath = "//label[text()=' Carrier']//..//div//a[text()='Select']")
	private WebElement mobileCarrierOptionAsSelect;
	
	@FindBy(xpath = "//span[contains(@ng-show, '$ctrl.showMake')]//..//..//div[@id='model']")
	private WebElement make;

	@FindBy(xpath = "//label[text()='Make']//..//div//a[text()='Apple']")
	private WebElement mobileMakeOption;
	
	@FindBy(xpath = "//label[text()='Make']//..//div//a[text()='Select']")
	private WebElement mobileMakeOptionAsSelect;
	
	@FindBy(css = "ul[ng-show*='$ctrl.makes.length'] li[role='menuitem'] a")
	private List<WebElement> makeOptions;

	@FindBy(xpath = "//span[contains(@ng-show, '!$ctrl.showModel')]//..//..//div[@id='model']")
	private WebElement model;

	@FindBy(css = "ul[ng-show*='$ctrl.models.length'] li[role='menuitem'] a")
	private List<WebElement> modelOptions;

	@FindBy(xpath = "//label[text()='Model']//..//div//a[contains(text(), 'iPad 2')]")
	private List<WebElement> mobileModelOptions;

	@FindBy(css = "span[ng-show='!$ctrl.showMake']")
	private WebElement makeDefaultOption;

	@FindBy(css = "span[ng-show='!$ctrl.showModel']")
	private WebElement modelDefaultOption;

	@FindBy(css = "i.backArrowLarge")
	private WebElement backArrow;

	@FindBy(id = "imeiTB")
	private WebElement imei;

	@FindBy(css = "div.fine-print-body.color-6A6A6A.text-center.ng-binding")
	private WebElement imeiNumber;

	@FindBy(css = "label.ng-binding.imei-red")
	private WebElement invalidImeiMessage;

	@FindBy(css = "label.ng-binding.imei-red")
	private List<WebElement> invalidIMEIMessage;

	@FindBy(css = "label.ng-binding.imei-red")
	private WebElement imeiErrorMessage;

	@FindBys(@FindBy(css = "div.Border-Invalid span.fine-print-body"))
	private List<WebElement> imeiIneligibleMessage;

	@FindBy(css = "div.Border-warning")
	private WebElement imeiEIPErrorMessage;

	@FindBy(css = "[class='fine-print-body p-t-2-xs p-t-4-md text-black text-bold ng-binding']")
	private WebElement remainingBalance;

	@FindBy(css = "span[class*='fine-print-body']")
	private WebElement deviceIsNotEligibleError;

	@FindBy(css = "button[ng-click*='$ctrl.gotoCART']")
	private WebElement skipTradeInBtn;

	@FindBy(css = "div.col-sm-offset-1 li.body-copy-description.gray-light")
	private List<WebElement> goodDevicePoints;

	@FindBy(css = "div.padding-bottom-medium-sm li.body-copy-description.gray-light")
	private List<WebElement> badDevicePoints;

	@FindBy(css = "i.fa.fa-comments")
	private List<WebElement> chatBoxIcon;

	@FindBy(css = "div [ng-class*='$ctrl.getImeiLabelClass()']")
	private WebElement imeiText;

	@FindBy(css = "div.display-flex")
	private WebElement dialNumber;

	@FindBy(css = "div.container_width.container")
	private WebElement whatsThismodal;

	public TradeInAnotherDevicePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Trade In Another Device Page
	 * 
	 * @return
	 */
	public TradeInAnotherDevicePage verifyTradeInAnotherDevicePage() {
		checkPageIsReady();
		waitforSpinner();
		try {
			Assert.assertTrue(getDriver().getCurrentUrl().contains("tradeInAnotherDevice"));
			Reporter.log("Navigated to Trade-In another device page");
		} catch (Exception e) {
			Assert.fail("Trade In Another Device Page is Not Displayed");
		}
		return this;
	}

	/**
	 * Click What Is This Link
	 */
	public TradeInAnotherDevicePage clickWhatIsThisLink() {
		try {
			whatIsThis.click();
		} catch (Exception e) {
			Assert.fail("What is This Link is Not Displayed to click");

		}
		return this;
	}

	/**
	 * Enter
	 */
	public TradeInAnotherDevicePage selectTradeInDeviceOptionsBasedOnDevice(MyTmoData myTmoData) {
		waitforSpinner();
		try {
			selectCarrier(myTmoData.getCarrier());
			selectMake(myTmoData.getMake());
			selectModel(myTmoData.getModel());
			setIMEINumber(myTmoData.getiMEINumber());
		} catch (Exception e) {
			Assert.fail("Options in Trade In Another Device Page are not Displayed to Choose");
		}
		return this;
	}

	/**
	 * Verify What Is This Model Title
	 * 
	 * @return
	 */
	public TradeInAnotherDevicePage verifyWhatIsThisModelTitle() {
		try {
			whatIsThisModelTitle.isDisplayed();
			Reporter.log("What is this model title is displayed");

		} catch (Exception e) {
			Assert.fail("What is this model title is not displayed");

		}
		return this;
	}

	/**
	 * Verify What Is This Model Header
	 * 
	 * @return
	 */
	public TradeInAnotherDevicePage verifyWhatIsThisModelHeader() {
		try {
			whatIsThisModelHeader.isDisplayed();
			Reporter.log("What is this model header is displayed");

		} catch (Exception e) {
			Assert.fail("What is this model Header is not displayed");

		}
		return this;
	}

	/**
	 * Verify What Is This Model Body
	 * 
	 * @return
	 */
	public TradeInAnotherDevicePage verifyWhatIsThisModelBody() {
		try {
			whatIsThisModelBody.isDisplayed();
			Reporter.log("What is this model body is displayed");
		} catch (Exception e) {
			Assert.fail("What is this model body is not displayed");

		}
		return this;

	}

	/**
	 * Get Following Number To Dial[To Retrieve IMEI]
	 * 
	 * @return
	 */
	public String getNumberToDial() {
		StringBuilder number = new StringBuilder();
		for (WebElement webElement : numberToDial) {
			number.append(webElement.getText());
		}
		return number.toString();
	}

	/**
	 * Click What Is This Model Close Button
	 */
	public TradeInAnotherDevicePage clickWhatIsThisModelCloseButton() {
		try {
			closeWhatIsThisModel.click();
		} catch (Exception e) {
			Assert.fail("What is this model body is not Clickable");

		}
		return this;
	}

	/**
	 * Click Continue Button
	 */
	public TradeInAnotherDevicePage clickContinueButton() {
		checkPageIsReady();
		waitforSpinner();
		try {
			clickElement(continueButton);
			waitforSpinner();
		} catch (Exception e) {
			Assert.fail("Continue Button is not Displayed to Click");
		}
		return this;
	}

	/**
	 * Select Carrier
	 * 
	 * @param option
	 */
	public TradeInAnotherDevicePage selectCarrier(String option) {
		try {
			carrier.click();
			if (getDriver() instanceof AppiumDriver) {
				if (option.equalsIgnoreCase("Select")) {
					mobileCarrierOptionAsSelect.click();
				} else {
					waitFor(ExpectedConditions.visibilityOf(mobileCarrierOption));
					mobileCarrierOption.click();
				}
			} else {
				for (WebElement webElement : cairrerOptions) {
					if (webElement.getText().equalsIgnoreCase(option)) {
						webElement.click();
						break;
					}
				}
			}
		} catch (Exception e) {
			Assert.fail("Selecting Carrier Option Is Failed");
		}
		return this;
	}

	/**
	 * Select Make
	 * 
	 * @param option
	 */
	public TradeInAnotherDevicePage selectMake(String option) {
			try {
			make.click();
			if (getDriver() instanceof AppiumDriver) {
				if (option.equalsIgnoreCase("Select")) {
					mobileMakeOptionAsSelect.click();
				} else {
				waitFor(ExpectedConditions.visibilityOf(mobileMakeOption));
				mobileMakeOption.click();
				}
			} else {
				for (WebElement webElement : makeOptions) {
					if (webElement.getText().equalsIgnoreCase(option)) {
						webElement.click();
						break;
					}
				}
			}
		} catch (Exception e) {
			Assert.fail("Selecting Make Option Is Failed");
		}
		return this;
	}

	/**
	 * Select Model
	 * 
	 * @param option
	 * @return
	 */
	public TradeInAnotherDevicePage selectModel(String option) {
		waitforSpinner();
		try {
			model.click();
			if (getDriver() instanceof AppiumDriver) {
				mobileModelOptions.get(0).click();
			} else {
				waitFor(ExpectedConditions.elementToBeClickable(modelOptions.get(0)), 30);
				for (WebElement webElement : modelOptions) {
					if (webElement.getText().trim().equalsIgnoreCase(option)) {
						moveToElement(webElement);
						webElement.click();
						break;
					}
				}
			}
		} catch (Exception e) {
			Assert.fail("Selecting Model Option Is Failed");
		}
		return this;
	}

	public TradeInAnotherDevicePage selectModel1(String option) {
		waitforSpinner();
		try {
			model.click();
		} catch (Exception e) {
			Assert.fail("Selecting Model Option Is Failed");
		}
		return this;

	}

	/**
	 * Set IMEI NUmber
	 * 
	 * @param imeiNumber
	 */
	public TradeInAnotherDevicePage setIMEINumber(String imeiNumber) {
		waitforSpinner();
		try {
			sendTextData(imei, imeiNumber);
			imei.sendKeys(Keys.TAB);
			if (getDriver() instanceof AppiumDriver) {
				imei.click();
			}
		} catch (Exception e) {
			Assert.fail("Setting IMEI Is Failed");
		}
		return this;
	}

	/**
	 * Get IMEI Error Message[Invalid IMEI Number]
	 * 
	 * @return
	 */
	public String getIMEIErrorMessage() {
		waitforSpinner();
		return invalidImeiMessage.getText();
	}

	/**
	 * Get IMEI Error Message[Invalid IMEI Number]
	 * 
	 * @return
	 */
	public boolean verifyIMEIErrorMessage() {
		waitforSpinner();
		boolean isElement = false;

		if (invalidIMEIMessage.size() > 0) {
			isElement = false;
		}
		return isElement;
	}

	/**
	 * Verify Make Drop-down Options Populated
	 * 
	 * @return
	 */
	public TradeInAnotherDevicePage verifyMakeDropdownOptions() {
		try {
			waitforSpinner();
			boolean isMakeOptionsPopulated = false;
			if (makeOptions.size() > 0) {
				isMakeOptionsPopulated = true;
			}
			Assert.assertTrue(isMakeOptionsPopulated, "Make Drop down options is not displayed");
			Reporter.log("Make Drop down options is displayed");
		} catch (Exception e) {
			Assert.fail("Make Drop down options is not displayed");
		}
		return this;
	}

	/**
	 * Verify Model Drop-down options Populated
	 * 
	 * @return
	 */
	public TradeInAnotherDevicePage verifyModelDropdownOptions() {
		try {
			if (getDriver() instanceof AppiumDriver) {
			} else {
				waitforSpinner();
				boolean isModelOptionsPopulated = false;
				if (modelOptions.size() > 0) {
					isModelOptionsPopulated = true;
				}
				Assert.assertTrue(isModelOptionsPopulated, "Model dropdown options are not populated");
				Reporter.log("Model drop down options are populated");
			}
		} catch (Exception e) {
			Assert.fail("Model dropdown options are not populated");
		}
		return this;
	}

	/**
	 * Get Make Drop-down Default Option
	 * 
	 * @return
	 */
	public String getMakeDropdownDefaultOption() {
		waitforSpinner();
		return makeDefaultOption.getText();
	}

	/**
	 * Get Model Drop-down Default Option
	 * 
	 * @return
	 */
	public String getModelDropdownDefaultOption() {
		waitforSpinner();
		return modelDefaultOption.getText();
	}

	/**
	 * Click on Skip Trade In Button
	 */
	public TradeInAnotherDevicePage clickOnSkipTradeInBtn() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.className("spinner")));
			skipTradeInBtn.click();
		} catch (Exception e) {
			Assert.fail("Skip trade in Button is not Displayed to CLick");
		}
		return this;
	}

	/**
	 * verify Continue Button Enabled
	 */
	public TradeInAnotherDevicePage verifyContinueButtonEnabled() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(continueButton));
			Reporter.log("Continue Button is enabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify if continue button is enabled");

		}
		return this;
	}

	/**
	 * Verify invalid error message displayed
	 **/
	public TradeInAnotherDevicePage verifyInvalidImeiErrMsg() {
		try {

			waitFor(ExpectedConditions.visibilityOf(invalidImeiMessage));
			Assert.assertTrue(invalidImeiMessage.isDisplayed(), "Invalid IMEI error message not displayed");
			Reporter.log("Invalid IMEI error message displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display invalid imei error message");
		}
		return this;
	}

	/**
	 * Click on imei text
	 */
	public TradeInAnotherDevicePage clickOnImeiText() {

		try {
			waitforSpinner();
			imeiText.click();
			Reporter.log("Imei text is clicked");

		} catch (Exception e) {
			Assert.fail("Failed to click imei text");

		}
		return this;
	}

	/** verify authorable title Tell us about your trade-in **/

	public TradeInAnotherDevicePage verifyTradeInAnotherDeviceTitle() {
		try {

			waitFor(ExpectedConditions.visibilityOf(tradeInAnotherDevice));
			Assert.assertTrue(tradeInAnotherDevice.isDisplayed(),
					"authorable title Tell us about your trade-in not displayed");
			Reporter.log("authorable title Tell us about your trade-in displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display authorable title Tell us about your trade-in");
		}
		return this;
	}

	/** verify what's this link is displayed **/

	public TradeInAnotherDevicePage verifyWhatIsThisLink() {
		try {

			waitFor(ExpectedConditions.visibilityOf(whatIsThis));
			Assert.assertTrue(whatIsThis.isDisplayed(), "what's this link is not displayed");
			Reporter.log("what's this link displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display what's this link");
		}
		return this;
	}

	/** verify Dial number is present **/

	public TradeInAnotherDevicePage verifyDialNumber() {
		try {

			waitFor(ExpectedConditions.visibilityOf(dialNumber));
			Assert.assertTrue(dialNumber.isDisplayed(), "Imei dialNumber is not displayed");
			Reporter.log("Imei dialNumber displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display imei dialNumber");
		}
		return this;
	}

	/** verify What's this modal displayed **/

	public TradeInAnotherDevicePage verifyWhatsThisModal() {
		try {

			waitFor(ExpectedConditions.visibilityOf(whatsThismodal));
			Assert.assertTrue(whatsThismodal.isDisplayed(), "whatsThismodal is not displayed");
			Reporter.log("whatsThismodal is  displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display whatsThismodal");
		}
		return this;
	}

	/** verify What's this modal not displayed **/

	public TradeInAnotherDevicePage verifyWhatsThisModalNotDisplayed() {
		try {
			waitforSpinner();
			Assert.assertFalse(isElementDisplayed(whatsThismodal), "whatsThismodal is displayed");
			Reporter.log("whatsThismodal is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to hide whatsThismodal");
		}
		return this;
	}

	/**
	 * verify Remaining EIP balance Message contains price
	 *
	 **/

	public TradeInAnotherDevicePage verifyRemainingEIPBalanceMessageContainsPrice() {
		try {
			waitFor(ExpectedConditions.visibilityOf(imeiEIPErrorMessage));
			Assert.assertTrue(imeiEIPErrorMessage.getText().contains("$"),
					"Remaining EIP balance Message does not contain price");
			Reporter.log("Remaining EIP balance Message price is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Remaining EIP balance Message contains price");
		}
		return this;
	}

	/**
	 * verify Remaining EIP balance Message is displayed
	 *
	 **/

	public TradeInAnotherDevicePage verifyRemainingEIPBalanceMessage() {
		try {
			waitFor(ExpectedConditions.visibilityOf(remainingBalance));
			Assert.assertTrue(remainingBalance.isDisplayed(), "Remaining EIP balance Message is not displayed");
			Reporter.log("Remaining EIP balance Message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Remaining EIP balance Message");
		}
		return this;
	}

}
