package com.tmobile.eservices.qa.pages.payments.api;
import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSAutopayDiscount extends  EOSCommonLib {

	
public Response getResponseAutopayDiscountEligible(String alist[]) throws Exception {
	Response response=null;   
	//String alist[]=getauthorizationandregisterinapigee(misdin,pwd);
	    if(alist!=null) {
	    	
	   // RestService restService = new RestService("", "https://stage2.eos.corporate.t-mobile.com");
	    RestService restService = new RestService("", eep.get(environment));
	    restService.setContentType("application/json");
	    restService.addHeader("X-B3-SpanId", "43214124");
	    restService.addHeader("ban", alist[3]);
	    restService.addHeader("X-B3-TraceId", "1234132");
	    restService.addHeader("activityid", "123");
	    restService.addHeader("Authorization",alist[2]);
	    restService.addHeader("interactionId", "4324312");
	  
	    response = restService.callService("v1/autopaydiscount/eligibility/"+alist[3],RestCallType.GET);
	    
	}

	    return response;
}





public String discounteligible(Response response) {
	String discounteligibility=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("eligible")!=null) {
        	return jsonPathEvaluator.get("eligible").toString();
	}}
		
	
	return discounteligibility;
}

}




	


