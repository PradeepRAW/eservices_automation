package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class AUCTermsAndConditionsPage extends CommonPage {

	private static final String pageUrl = "linechanged/disclaimer";
	
	@FindBy(css = ".PrimaryCTA")
	private WebElement saveButton;
	
	@FindBy(css = ".legal.check1")
	private WebElement legalCheckBox;

	public AUCTermsAndConditionsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify AUC terms and conditions page
	 */
	public AUCTermsAndConditionsPage verifyAUCTermsAndConditionsPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("AUC terms and conditions page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("AUC terms and conditions page not displayed");
		}
		return this;
	}
	
	/**
	 * Click permission header
	 */
	public AUCTermsAndConditionsPage clickSaveBtn() {
		try {
			waitforSpinnerinProfilePage();
			saveButton.click();		
			Reporter.log("Clicked on save button");
		} catch (Exception e) {
			Assert.fail("Click on save button failed ");
		}
		return this;
	}
	
	/**
	 * Click legal check box
	 */
	public AUCTermsAndConditionsPage clickCheckBox() {
		try {
			waitforSpinnerinProfilePage();
			legalCheckBox.click();		
		} catch (Exception e) {
			Assert.fail("Select check box failed ");
		}
		return this;
	}
}
