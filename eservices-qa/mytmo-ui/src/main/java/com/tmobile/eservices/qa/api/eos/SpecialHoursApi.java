package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class SpecialHoursApi extends ApiCommonLib {

	/***
	 * Gets all the events info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getSpecialHoursEvents(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildGetSpecialHoursEvents(apiTestData);
		String resourceURL = "events/get-special-hours-events";
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, "", RestServiceCallType.GET);
		return response;
	}

	/***
	 * Create an event
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response createSpecialHoursEvents(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildGetSpecialHoursEvents(apiTestData);
		String resourceURL = "events/create-special-hours-event";
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, requestBody,
				RestServiceCallType.POST);
		return response;
	}

	/***
	 * Delete an event
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response deleteSpecialHoursEvent(ApiTestData apiTestData, String eventID) throws Exception {
		Map<String, String> headers = buildGetSpecialHoursEvents(apiTestData);
		String resourceURL = "events/delete-special-hours-event?eventId=" + eventID;
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, "", RestServiceCallType.DELETE);
		return response;
	}

	/***
	 * Gets all the Stores info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getStoresByCityState(ApiTestData apiTestData, String state, String city) throws Exception {
		Map<String, String> headers = buildGetSpecialHoursEvents(apiTestData);
		String resourceURL = "getStoresInCity?state=" + state + "&city=" + city;
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, "", RestServiceCallType.GET);
		return response;
	}

	private Map<String, String> buildGetSpecialHoursEvents(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		return headers;
	}

}