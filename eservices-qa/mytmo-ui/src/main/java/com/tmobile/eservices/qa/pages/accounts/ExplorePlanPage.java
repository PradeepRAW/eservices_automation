package com.tmobile.eservices.qa.pages.accounts;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author Sudheer Reddy Chilukuri
 *
 */
public class ExplorePlanPage extends CommonPage {

	// headers
	@FindAll({ @FindBy(css = ".ui_headline.planHeadline"),
			@FindBy(css = "div#PlanServicesDiv_id div.ui_mobile_headline") })
	private WebElement planPageHeader;

	@FindBy(css = "div.H6-heading")
	private List<WebElement> monthlyTotalDetails;

	@FindBy(css = "div.Display3")
	private WebElement accountHeader;

	@FindBy(css = "p.Display3.padding-bottom-small")
	private WebElement pendingHeding;

	@FindBy(css = "span.H4-heading")
	private List<WebElement> mbbPlan;

	@FindBy(css = "p.body.padding-top-xsmall")
	private WebElement notificationError;

	// Buttons

	@FindBy(css = "span.legal-bold-link")
	private WebElement seeFullDetails;

	@FindBy(linkText = "See more plans")
	private List<WebElement> seeMorePlansLink;

	@FindBy(linkText = "Hide Plans")
	private List<WebElement> hidePlansLink;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement backCta;

	@FindBy(css = "button.PrimaryCTA.full-btn-width.float-md-none")
	private List<WebElement> backAndManageAddonCta;

	@FindBy(xpath = "//button[contains(text(),'Back')]")
	private WebElement bcakCta;

	@FindBy(xpath = "//button[contains(text(),'Contact Us')]")
	private WebElement contactUscta;

	// others
	@FindBy(css = "span[class*='body'] img")
	private WebElement planName;

	@FindBy(css = "div.col-12.Display3.padding-bottom-medium.padding-top-medium")
	private WebElement verifyExplore;

	@FindBy(css = "span.legal")
	private WebElement legalText;

	@FindBy(css = "div.Display5")
	private WebElement verifyLegalPage;

	@FindBy(css = "div.row.cursor.padding-top-small")
	private List<WebElement> clickOnFeaturePlan;

	@FindBy(css = "p.Display3.padding-top-small.text-center")
	private WebElement comparePlan;

	@FindBy(xpath = "//*[contains(text(),'Take an extra $5')]")
	private WebElement autoPayerrorMessage;

	@FindBy(xpath = "//span[@aria-label='T-Mobile ONE Military']")
	private WebElement militaryPlan;

	@FindBy(xpath = "//span[@aria-label='T-Mobile ONE Unlimited 55']")
	private WebElement slect55plus;

	@FindBy(xpath = "//span[contains(text(),'with Autopay + taxes and fees included')]")
	private List<WebElement> enrolledAutopay;

	@FindBy(css = "p.body.padding-top-xsmall")
	private WebElement bestPlan;

	@FindAll({ @FindBy(css = "span.H4-heading"),
			@FindBy(xpath = "//span[contains(text(),'T-Mobile ONE with ONE Plus')]") })
	private WebElement tmobileOnePlus;

	@FindBy(xpath = "//img[@role=\"presentation\"]//following::span[contains(text(),'$130.00')]")
	private List<WebElement> TmobileOne;

	@FindBy(css = "div.col-12.Display5")
	private List<WebElement> verifyComponent;

	@FindBy(css = "img.phone-image")
	private List<WebElement> calculateLIne;

	@FindBy(css = "div.col-12.Display5.padding-vertical-medium")
	private List<WebElement> exploreComponent;

	@FindBy(css = "div.row.cursor.padding-top-small")
	private List<WebElement> availablePlan;

	@FindBy(css = "div.row.cursor.padding-top-small")
	private List<WebElement> tmobileOne;

	@FindBy(xpath = "//p[contains(text(),'Manage Data and Add-ons')]")
	private WebElement manageAddon;

	@FindBy(xpath = "//span[@aria-label=\"T-Mobile Essentials\"]")
	private WebElement tmobileEssent;

	@FindBy(xpath = "//p[contains(text(),'You already have a pending rate plan or service change in your account. Please try again later or contact us for help changing plans.')]")
	private WebElement pendingError;

	@FindBy(css = "div[aria-label='Current Plan']")
	private WebElement currentPlan;

	@FindBy(css = "span[class*='Display4']")
	private List<WebElement> planheaders;

	// Plan details

	@FindBy(xpath = "//span[@aria-label='T-Mobile ONE']//..//..")
	private WebElement tMobileONEPlanSection;

	@FindBy(xpath = "//span[@aria-label='T-Mobile ONE']//..//..//span[@class='Display5']")
	private WebElement tMobileONEPlanPriceSection;

	@FindBy(xpath = "//span[@aria-label='Magenta®']//..//..")
	private WebElement magentaPlanSection;

	@FindBy(xpath = "//span[@aria-label='Magenta®']//..//..//span[@class='Display5']")
	private WebElement magentaPlanPriceSection;

	@FindBy(xpath = "//span[@aria-label='Magenta® Plus Military']//..//..")
	private WebElement magentaMilitaryPlanSection;

	@FindBy(xpath = "//span[@aria-label='Magenta® Plus Military']//..//..//span[@class='Display5']")
	private WebElement magentaMilitaryPlanPriceSection;

	@FindBy(xpath = "//span[@aria-label='Magenta® Plus']//..//..//..")
	private WebElement magentaPlusPlanSection;

	@FindBy(xpath = "//span[@aria-label='Magenta® Plus']//..//..//span[@class='Display5']")
	private WebElement magentaPlusPlanPriceSection;

	@FindBy(xpath = "//span[@aria-label='Magenta® Military']//..//..//..")
	private WebElement magentaTMMilitaryPlanSection;

	@FindBy(xpath = "//span[@aria-label='Magenta® Military']//..//..//span[@class='Display5']")
	private WebElement magentaTMMilitaryPlanPriceSection;

	@FindBy(xpath = "//span[@aria-label='T-Mobile Essentials ']//..//..//..")
	private WebElement tMobileEssentialsPlanSection;

	@FindBy(xpath = "//span[@aria-label='T-Mobile Essentials ']//..//..//span[@class='Display5']")
	private WebElement tMobileEssentialsPlanPriceSection;

	@FindBy(css = "span[class*='Display4']")
	private WebElement currentPlanName;

	@FindBy(css = "p.mb-0.Display3.padding-top-large")
	private WebElement manageAddonPage;

	public ExplorePlanPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * verify Plan Details Header
	 * 
	 */
	public ExplorePlanPage verifyPlanDetailsHeader(WebElement planSection, String planName) {
		try {
			String name = planSection.getText();
			String planNAme = name.replaceAll("[^\\x00-\\x7f]", "");
			Assert.assertTrue(planNAme.contains(planName), planName + " header is not displayed");
			Reporter.log(planName + " header is displayed");
		}

		catch (Exception e) {
			Assert.fail(planName + " Header is not displayed");
		}
		return this;
	}

	/**
	 * verify plan Price With Comments
	 * 
	 */
	public ExplorePlanPage verifyplanPriceWithComments(WebElement planSectionElement, String priceTextDetails) {
		try {
			Assert.assertTrue(verifyElementByText(planSectionElement, priceTextDetails),
					priceTextDetails + " header is not displayed");
			Reporter.log(priceTextDetails + " header is displayed");

		} catch (Exception e) {
			Assert.fail(priceTextDetails + " header is not displayed");
		}
		return this;
	}

	/**
	 * verify Montly Plan Details
	 * 
	 */
	public ExplorePlanPage verifyMontlyPlanDetails(WebElement planSectionElement, String monthlyPlanDetails) {
		try {
			Assert.assertTrue(verifyElementByText(planSectionElement, monthlyPlanDetails),
					monthlyPlanDetails + " Plan header is not displayed");
			Reporter.log(monthlyPlanDetails + " Plan header is displayed");

		} catch (Exception e) {
			Assert.fail(monthlyPlanDetails + " Plan header is not displayed");
		}
		return this;
	}

	/**
	 * verify Plan Details Description
	 * 
	 */
	public ExplorePlanPage verifyPlanDetailsDescription(WebElement planSectionElement, String description) {
		try {
			Assert.assertTrue(planSectionElement.getText().contains(description), description + " is not displayed");
			Reporter.log(description + " is displayed");

		} catch (Exception e) {
			Assert.fail(description + " is not displayed");
		}
		return this;
	}

	/**
	 * verify Plan Details Section
	 * 
	 */
	public ExplorePlanPage verifyPlanDetailsSection(WebElement planSectionElement, WebElement planSectionPriceElement,
			String planName, String priceNumber, String priceTextDetails, String monthlyPlanDetails, String desc) {
		waitforSpinner();
		try {
			verifyPlanDetailsHeader(planSectionElement, planName);
			// verifyPlanPriceDetails(planSectionPriceElement, priceNumber);
			verifyplanPriceWithComments(planSectionElement, priceTextDetails);
			verifyMontlyPlanDetails(planSectionElement, monthlyPlanDetails);
			verifyPlanDetailsDescription(planSectionElement, desc);
			Reporter.log(planName + " Complete section is displayed");

		} catch (Exception e) {
			Assert.fail(planName + " Details are is not displayed");
		}
		return this;
	}

	/**
	 * verify TMobileOne Plan Details Section
	 * 
	 */
	public ExplorePlanPage verifyTMobileOnePlanDetailsSection() {
		try {
			verifyPlanDetailsSection(tMobileONEPlanSection, tMobileONEPlanPriceSection, "T-Mobile ONE", "$130.00",
					"Taxes and fees included.", "Monthly total with current plan:",
					"Includes unlimited talk, text, and high-speed data, plus Netflix™ On Us. All taxes and fees are included.");
		} catch (Exception e) {
			Assert.fail(" t Mobile One Plan Details are is not displayed");
		}
		return this;
	}

	/**
	 * verify Magent Plan Details Section
	 * 
	 */
	public ExplorePlanPage verifyMagentPlanDetailsSection() {
		try {
			verifyPlanDetailsSection(magentaPlanSection, magentaPlanPriceSection, "Magenta", "$120.00",
					"with AutoPay. Taxes and fees included.", "Monthly total with this plan:",
					"Unlimited talk, text, high-speed data, plus Netflix, and high-speed mobile hotspot. All taxes and fees are included.");
		} catch (Exception e) {
			Assert.fail(" Magenta Plan Details are is not displayed");
		}
		return this;
	}

	/**
	 * verify Magenta Plus Plan Details Section
	 * 
	 */
	public ExplorePlanPage verifyMagentaPlusPlanDetailsSection() {
		try {
			verifyPlanDetailsSection(magentaPlusPlanSection, magentaPlusPlanPriceSection, "Magenta Plus", "$140.00",
					"with AutoPay. Taxes and fees included.", "Monthly total with this plan:",
					"Unlimited talk, text, high-speed data, plus Netflix, HD video streaming, high-speed mobile hotspot, and much more. All taxes and fees are included.");
		} catch (Exception e) {
			Assert.fail("Magenta Plus Plan Details are is not displayed");
		}
		return this;
	}

	/**
	 * verify TMobile Essentials Plan Details Section
	 * 
	 */
	public ExplorePlanPage verifyTMobileEssentialsPlanDetailsSection() {
		try {
			verifyPlanDetailsSection(tMobileEssentialsPlanSection, tMobileEssentialsPlanPriceSection,
					"T-Mobile Essentials", "$90.00", "with AutoPay. Taxes and fees additional.",
					"Monthly total with this plan:",
					"Get just the essentials: unlimited talk, text, and high-speed data.");
		} catch (Exception e) {
			Assert.fail(" T-Mobile Essentials Plan Details are is not displayed");
		}
		return this;
	}

	/**
	 * verify Magenta Plus Military Plan Details Section
	 * 
	 */
	public ExplorePlanPage verifyMagentaPlusMilitaryPlanDetailsSection() {
		try {
			verifyPlanDetailsSection(magentaMilitaryPlanSection, magentaMilitaryPlanPriceSection,
					"Magenta Plus Military", "$100.00", "with AutoPay. Taxes and fees included.",
					"Monthly total with this plan:",
					"Unlimited talk, text, high-speed data, plus Netflix, HD video streaming, and much more for our customers with a qualifying U.S. Military affiliation. All taxes and fees are included.");
		} catch (Exception e) {
			Assert.fail(" Magenta® Plus Military Plan Details are is not displayed");
		}
		return this;
	}

	/**
	 * verify Magenta TMMilitary Plan Details Section
	 * 
	 */
	public ExplorePlanPage verifyMagentaTMMilitaryPlanDetailsSection() {
		try {
			verifyPlanDetailsSection(magentaTMMilitaryPlanSection, magentaTMMilitaryPlanPriceSection,
					"Magenta Military", "$80.00", "with AutoPay. Taxes and fees included.",
					"Monthly total with this plan:",
					"Unlimited talk, text, high-speed data, plus Netflix, and high-speed mobile hotspot for our customers with a qualifying U.S. Military affiliation. All taxes and fees are included.");
		} catch (Exception e) {
			Assert.fail("Magenta Military Plan Details are is not displayed");
		}
		return this;
	}

	/**
	 * verify Change Plan Heading
	 * 
	 */
	public ExplorePlanPage verifyChangePlanHeading() {
		try {
			checkPageIsReady();
			Assert.assertTrue(pendingHeding.getText().contains("Pending Changes"));
			Reporter.log("Pending Changes heading is available on the Error Page");
		} catch (Exception e) {
			Assert.fail("Pending Changes heading is not available on the Error Page");
		}
		return this;
	}

	/**
	 * verify Pending Error Message
	 * 
	 */
	public ExplorePlanPage verifyPendingErrorMessage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(pendingError.isDisplayed(), "You already have a pending rate message is not displaying");
			Reporter.log(
					"You already have a pending rate plan or service change in your account. Please try again later or contact us for help changing plans notification Message is available on the Error Page");
		} catch (Exception e) {
			Assert.fail("Notification Message is not available on the Error Page");
		}
		return this;
	}

	/**
	 * verify Back Cta for Pending Plan
	 * 
	 */
	public ExplorePlanPage verifyBackCtaforPendingPlan() {
		try {
			checkPageIsReady();
			Assert.assertTrue(bcakCta.isDisplayed());
			Reporter.log("Back Button is available on Peding Changes Page");
			bcakCta.click();
			Reporter.log("Clciked on Back Button and navigated to Account Overview Page");
		} catch (Exception e) {
			Assert.fail("Back Button is not available on Peding Changes Page");
		}
		return this;
	}

	/**
	 * verify ContactUs Page
	 * 
	 */
	public ExplorePlanPage verifyContactUsPage() {
		checkPageIsReady();
		waitForDataPassSpinnerInvisibility();
		try {
			checkPageIsReady();
			String currentUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(currentUrl.contains("/contact-us"));
			Reporter.log("Contact us Page is loaded");
		} catch (Exception e) {
			Assert.fail("Contact us Page is not loaded");
		}
		return this;
	}

	/**
	 * verify Line Details Page
	 * 
	 */
	public ExplorePlanPage verifyLineDetailsPage() {
		checkPageIsReady();
		waitForDataPassSpinnerInvisibility();
		try {
			checkPageIsReady();
			String currentUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(currentUrl.contains("/line-details"));
			Reporter.log("Line Details Page is loaded");
		} catch (Exception e) {
			Assert.fail("Line Details Page is not loaded");
		}
		return this;
	}

	/**
	 * verify OOPs Error
	 * 
	 */
	public ExplorePlanPage verifyOOPsError() {
		try {
			checkPageIsReady();
			Assert.assertTrue(pendingHeding.getText().contains("Oops! Something went wrong."));
			Reporter.log("Oops! Something went wrong. is available on Noticaiton Page");
		} catch (Exception e) {
			Assert.fail("Oops! Something went wrong. is not available on Noticaiton Page");
		}
		return this;
	}

	/**
	 * verify Notification Error Message
	 * 
	 */
	public ExplorePlanPage verifyNotificationErrorMessage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(notificationError.getText().contains(
					"You don’t have permission to perform this change. Please contact your Primary Account Holder for assistance."));
			Reporter.log(notificationError.getText() + "Is availble");
		} catch (Exception e) {
			Assert.fail("notification Error message is not available");
		}
		return this;
	}

	/**
	 * verify Notification Message
	 * 
	 */
	public ExplorePlanPage verifyNotificationMessage(String text) {
		try {
			checkPageIsReady();
			Assert.assertTrue(notificationError.getText().contains(text),
					text + " Notification message is not displaying");
			Reporter.log(notificationError.getText() + "Is availble");
		} catch (Exception e) {
			Assert.fail("Notification message is not available");
		}
		return this;
	}

	/**
	 * verify Backt cta
	 * 
	 */
	public ExplorePlanPage verifyBacktcta() {
		try {
			checkPageIsReady();
			Assert.assertTrue(bcakCta.isDisplayed());
			Reporter.log("Back Button is availble in Notification Page");
			bcakCta.click();
			Reporter.log("Clicked on Back Button and navigate back to Line details Page");
			verifyLineDetailsPage();
		} catch (Exception e) {
			Assert.fail("Back Button is not availble in Notification Page");
		}
		return this;
	}

	/**
	 * verify ContactUs for Pending Plan
	 * 
	 */
	public ExplorePlanPage verifyContactUsforPendingPlan() {
		try {
			checkPageIsReady();
			Assert.assertTrue(contactUscta.isDisplayed(), "Contact Us Button is not available on Pending Changes Page");
			Reporter.log("Contact Us Button is available on Pending Changes Page");
			contactUscta.click();
			Reporter.log("Clicked on Contact Us Button and navigated to Contact Us Page");
			verifyContactUsPage();
		} catch (Exception e) {
			Assert.fail("Contact Us Button is not available on Pending Changes Page");
		}
		return this;
	}

	/**
	 * slect New Plan
	 * 
	 */
	public ExplorePlanPage slectNewPlan() {
		try {
			checkPageIsReady();
			mbbPlan.get(1).click();
		} catch (Exception e) {
			Assert.fail("No Plans Available to select");
		}
		return this;
	}

	/**
	 * verif Available Plans in Explore Page for Wearable
	 * 
	 */
	public ExplorePlanPage verifyAvailablePlansinExplorePageforWearable() {
		try {
			checkPageIsReady();
			ArrayList<String> sample = new ArrayList<String>();
			sample.add(
					"Simple Choice North America for Wearables with Separate Number: Unlimited Talk + Text + 500MB High-Speed Data");
			sample.add("T-Mobile ONE Wearable TE with Separate Number");
			for (int i = 0; i < sample.size(); i++) {
				Assert.assertTrue(sample.contains(mbbPlan.get(i).getText()));
				Reporter.log(sample.get(i) + "is Presented");
			}
		} catch (Exception e) {
			Assert.fail("No Plans Available to select");
		}
		return this;
	}

	/**
	 * Click on TMobileONE
	 * 
	 */
	public ExplorePlanPage clickonTMobileONE() {
		try {
			checkPageIsReady();
			TmobileOne.get(1).click();
			Reporter.log("Clicked on TMobile ONE and navigated to PlanComparison Page");
		} catch (Exception e) {
			Reporter.log("TMobile ONE plan is not available in Explore Page");
		}
		return this;
	}

	/**
	 * Click on TMobileONE with ONE Plus
	 * 
	 */
	public ExplorePlanPage clickonTMobileONEwithONEPlus() {
		try {
			checkPageIsReady();
			tmobileOnePlus.click();
			Reporter.log("Clicked on TMobileONEwithONEPlus plan and navigated to PlanComparison Page");
		} catch (Exception e) {
			Reporter.log("TMobileONEwithONEPlus plan is not available in Explore Page");
		}
		return this;
	}

	/**
	 * navigate to Plan Comparision Page
	 * 
	 */
	public ExplorePlanPage navigateToPlanComparisionPage(String planName) {
		try {
			waitforSpinner();
			checkPageIsReady();
			clickOnSeeMorePlansLink();
			checkPageIsReady();
			if (!planName.isEmpty() && !CollectionUtils.sizeIsEmpty(planheaders)) {
				for (WebElement webElement : planheaders) {
					if (planName.equalsIgnoreCase(webElement.getText().replaceAll("[^\\x00-\\x7f]", ""))) {
						webElement.click();
						break;
					}
				}
			} else {
				availablePlan.get(1).click();
			}
			Reporter.log("Clicked on Available plan and navigated to PlanComparison Page");
		} catch (Exception e) {
			Reporter.log("No Plans Available");
		}
		return this;
	}

	/**
	 * verify Current and Choose Plan Component
	 * 
	 */
	public ExplorePlanPage verifyCurrentandChoosePlanComponent() {
		try {
			checkPageIsReady();
			int n = verifyComponent.size();
			if (n > 0) {
				for (int i = 0; i < n; i++) {
					String name = verifyComponent.get(i).getText();
					Reporter.log(name + "  Component is presented on Explore Page");
				}
			}

		} catch (Exception e) {
			Assert.fail("There is no component is presnted on explore plan page ");
		}
		return this;
	}

	/**
	 * verify Back Cta on ExplorePage
	 * 
	 */
	public ExplorePlanPage verifyBackCtaonExplorePage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(backAndManageAddonCta.get(0).getText().contains("Back"));
			Reporter.log("BACK Cta is available on ExplorePage");
			backAndManageAddonCta.get(0).click();
			checkPageIsReady();
			String currentUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(currentUrl.contains("/account-overview"));
			Reporter.log("Clicked on BACK Cta and navigated back to Account Overview Page");
		} catch (Exception e) {
			Assert.fail("BACK Cta is not available on ExplorePage ");
		}
		return this;
	}

	/**
	 * verify Manage Addon Page
	 * 
	 */
	public ExplorePlanPage verifyManageAddonPage() {
		try {
			checkPageIsReady();
			backAndManageAddonCta.get(1).click();
			Assert.assertTrue(manageAddonPage.getText().contains("Manage Data and Add-ons"));
			Reporter.log("Manage Addon page is loaded");
		} catch (Exception e) {
			Assert.fail("Manage Addon page is not loaded");
		}
		return this;
	}

	/**
	 * verify Manage Addon CTA
	 * 
	 */
	public ExplorePlanPage verifyManageAddonCta() {
		try {
			checkPageIsReady();
			Assert.assertTrue(backAndManageAddonCta.get(1).getText().contains("Manage Data & Add-ons"));
			Reporter.log("Manage Data & Add-ons Cta is available on ExplorePage");
			/*
			 * backAndManageAddonCta.get(1).click(); checkPageIsReady();
			 * manageAddon.isDisplayed(); Reporter.log(
			 * "Clicked on Manage Data & Add-ons Cta and navigated to add ons page" );
			 * getDriver().navigate().back(); Reporter.log(
			 * "Clicked BrowserBack cta and navigated to Explore Page");
			 */
		} catch (Exception e) {
			Assert.fail("Manage Data & Add-ons Cta is not available on ExplorePage ");
		}
		return this;
	}

	/**
	 * verify Error Message for Best Plan
	 * 
	 */
	public ExplorePlanPage verifyErrorMessageforBestPlan() {
		try {
			checkPageIsReady();
			Assert.assertTrue(bestPlan.getText().contains(
					"You are not eligible for any other rate plans at this time. Amp up your plan with add-ons"));
			Reporter.log("Expcted bestPlan Error Message is available on ExplorePage");
		} catch (Exception e) {
			Assert.fail("Expcted bestPlan Error Message is not available on ExplorePage ");
		}
		return this;
	}

	/**
	 * verify Auto Pay Error Message
	 * 
	 */
	public ExplorePlanPage verifyAutoPayErrorMessage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(autoPayerrorMessage.isDisplayed(),
					"AutoPayError Message is not available on ExplorePage");
			Reporter.log("Expcted Auot Pay Error Message is available on ExplorePage");
		} catch (Exception e) {
			Assert.fail("AutoPayError Message is not available on ExplorePage ");
		}
		return this;
	}

	/**
	 * Click On feature Plan
	 * 
	 */
	public ExplorePlanPage clickOnfeaturePlan() {
		try {
			checkPageIsReady();
			clickOnFeaturePlan.get(1).click();
			Reporter.log("clicked on FeaturePlan and navigated to PlanCompare Page");
		} catch (Exception e) {
			Reporter.log("FeaturePlan is not available to select");
		}
		return this;
	}

	/**
	 * Verify Back Cta
	 * 
	 */
	public ExplorePlanPage verifyBackCta() {
		try {
			checkPageIsReady();
			Assert.assertTrue(backCta.getText().contains("Back"));
			Reporter.log("backCta is available on legal page");
			backCta.click();
			Reporter.log("Clciked on backCta and navigated back to Previous page");

		} catch (Exception e) {
			Reporter.log("backCta is available on legal page");
		}
		return this;
	}

	/**
	 * Verify That Legal Page
	 * 
	 */
	public ExplorePlanPage verifyThatLegalPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(verifyLegalPage.getText().contains("Important Rate Plan Information"));
			Reporter.log("Legal page is loaded successfully");
		} catch (Exception e) {
			Reporter.log("Legal page is NOT loaded successfully");
		}
		return this;
	}

	/**
	 * click on see Full Details
	 * 
	 */
	public ExplorePlanPage seeFullDetailsLinkClick() {
		try {
			checkPageIsReady();
			Assert.assertTrue(seeFullDetails.getText().contains("See full details"));
			Reporter.log("See Full Details link is available to click");
			seeFullDetails.click();
			Reporter.log("See Full Details link is clicked and navigated to Legal page");
		} catch (Exception e) {
			Reporter.log("See Full Details link is NOT available to click");
		}
		return this;
	}

	/**
	 * verify Legal Text On Explore
	 * 
	 */
	public ExplorePlanPage verifyLegalTextOnExplore() {
		try {
			checkPageIsReady();
			legalText.isDisplayed();
			Reporter.log("Legal text in Explore page is available");
		} catch (Exception e) {
			Reporter.log("Legal text in Explore page is NOT available");
		}
		return this;
	}

	/**
	 * verify Explore Page for Home Internet
	 * 
	 */
	public ExplorePlanPage verifyExplorePageforHomeInternet() {
		try {
			checkPageIsReady();
			String currentUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(currentUrl.contains("/explore-plans"));
			Reporter.log("ExplorePlans Page is loaded");
		} catch (Exception e) {
			Assert.fail("ExplorePlans Page is not loaded");
		}
		return this;

	}

	/**
	 * verify Explore Page
	 * 
	 */
	public ExplorePlanPage verifyExplorePage() {
		waitforSpinner();
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.urlContains("explore-plans"));
			Reporter.log("ExplorePlane Heading is available");
		} catch (Exception e) {
			Assert.fail("ExplorePlane Heading is not available");
		}
		return this;
	}

	/**
	 * navigate To Explore Plan Page from AccountOverviewPage
	 * 
	 */

	public ExplorePlanPage navigateToExplorePlanPagefromAccountOverviewPage() {

		String currentURL[] = getDriver().getCurrentUrl().split("account-overview");
		try {
			currentURL[0] = currentURL[0] + "change-plan/explore-plans";
			getDriver().get(currentURL[0]);
			checkPageIsReady();
		} catch (Exception e) {
		}
		return this;
	}

	/**
	 * click On See More Plans Link
	 * 
	 */
	public ExplorePlanPage clickOnSeeMorePlansLink() {
		checkPageIsReady();
		waitforSpinner();
		try {
			if (!seeMorePlansLink.isEmpty()) {
				clickElementWithJavaScript(seeMorePlansLink.get(0));
			}
			Reporter.log("Clicked on See More Plans Link");
		} catch (Exception e) {
			Assert.fail("Failed to click on See More Plans Link");
		}
		return this;
	}

	public String getCurrentPlanName() {
		return currentPlanName.getText();
	}

}
