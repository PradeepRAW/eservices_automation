package com.tmobile.eservices.qa.api.eos.monitors;

import org.testng.Reporter;

import com.fasterxml.jackson.databind.JsonNode;
import com.tmobile.eservices.qa.api.exception.ServiceFailureException;
import com.tmobile.eservices.qa.common.ShopConstants;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class StandardUpgradeTradeInEIPFlow extends OrderProcess {

	/*
	 * 
	 * Steps For Standard Upgrade TradeIn EIP Flow, 1. getCatalogProducts 2.
	 * Create Cart 3. Get Account Devices Eligible For TradeIn 4. Invoke TradeIn
	 * 5. Update Cart with Device Trade In Details 6. Create Quote 7. Update
	 * Quote 8. Make Payment 9. Place Order
	 * 
	 */

	@Override
	public void createCart(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "createCartAPI_EIP_forOrder.txt");
			invokeCreateCart(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while making Create Cart : " + e);
			throwServiceFailException();
		}
	}

	@Override
	public void getAccountDevicesEligibleForTradeIn(ApiTestData apiTestData) throws ServiceFailureException {				
		try {
			String requestBody = extractPayLoadFromFile(
					ShopConstants.SHOP_MONITORS + "getAccountDeviceListForTradeinAPI.txt");
			String updatedRequest = prepareRequestParam(requestBody, serviceResultsMap);
			logLargeRequest(updatedRequest, "getAccountDevicesEligibleForTradeIn");
			Response response = deviceTradeInApi.getAccountDeviceListEligibleForTradeIn(apiTestData, updatedRequest,
					serviceResultsMap);
			boolean isSuccessResponse = checkAndReturnResponseStatusAndLogInfo(response,
					"getAccountDevicesEligibleForTradeIn");
			if (isSuccessResponse) {
				if(isResponseEmpty(response)){
					failAndLogResponse(response, "getAccountDevicesEligibleForTradeIn");
				}
				getAccountDevicesEligibleForTradeInFromResponse(response);
			}
		} catch (Exception e) {
			//Reporter.log("Service issue while invoking getAccountDevicesEligibleForTradeIn : " + e);
			throwServiceFailException();
		}

	}

	@Override
	public void invokeTradeIn(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "tradeIn_req_eip_monitors.txt");
			String updatedRequest = prepareRequestParam(requestBody, serviceResultsMap);
			logLargeRequest(updatedRequest, "createQuoteTradeIn");
			Response response = deviceTradeInApi.createQuoteTradeIn(apiTestData, updatedRequest, serviceResultsMap);
			boolean isSuccessResponse = checkAndReturnResponseStatusAndLogInfo(response, "createQuoteTradeIn");
			if (isSuccessResponse) {
				if(isResponseEmpty(response)){
					failAndLogResponse(response, "createQuoteTradeIn");
				}
				getTokenMapforCreateQuoteTradeIn(response);
			}
		} catch (Exception e) {
			Reporter.log("Service issue while invoking createQuoteTradeIn : " + e);
			throwServiceFailException();
		}
	}

	

	@Override
	public void updateCart(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(
					ShopConstants.SHOP_MONITORS + "updateCartDeviceTradeIn_Monitors.txt");
			invokeUpdateCart(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while updating Cart with Device TradeIn Details: " + e);
			throwServiceFailException();
		}
	}

	private void getAccountDevicesEligibleForTradeInFromResponse(Response response) {

		JsonNode jsonNode = getParentNodeFromResponse(response);

		if (!jsonNode.isMissingNode() && jsonNode.has("accountDevices")) {
			JsonNode parentNode = jsonNode.path("accountDevices");
			if (!parentNode.isMissingNode() && parentNode.isArray()) {
				for (int i = 0; i < parentNode.size(); i++) {
					if ("true".equalsIgnoreCase(parentNode.get(i).get("isEligible").asText())
							&& (null != parentNode.get(i).get("device")
									&& (!parentNode.get(i).get("device").isMissingNode()))) {
						serviceResultsMap.put("isEligibleForTradeIn", getPathVal(parentNode.get(i), "isEligible"));
						serviceResultsMap.put("imei", getPathVal(parentNode.get(i), "imei"));
						serviceResultsMap.put("displayName", getPathVal(parentNode.get(i), "displayName"));
						serviceResultsMap.put("makeName",
								getPathVal(parentNode.get(i), "device.carrier[0].make[0].makeName"));
						serviceResultsMap.put("TradeInModelName",
								getPathVal(parentNode.get(i), "device.carrier[0].make[0].model[0].modelName"));
					}
				}
			}

		}

	}

	private void getTokenMapforCreateQuoteTradeIn(Response response) {
		JsonNode jsonNode = getParentNodeFromResponse(response);
		if (!jsonNode.isMissingNode()) {
			serviceResultsMap.put("quoteId", getPathVal(jsonNode, "quoteId"));
			serviceResultsMap.put("quotePrice", getPathVal(jsonNode, "quotePrice"));
			serviceResultsMap.put("deviceIMEI", getPathVal(jsonNode, "device.imei"));
			serviceResultsMap.put("deviceName", getPathVal(jsonNode, "device.carrier[0].make[0].model[0].modelName"));
			serviceResultsMap.put("deviceCarrier", getPathVal(jsonNode, "device.carrier[0].carrierName"));
			serviceResultsMap.put("deviceMake", getPathVal(jsonNode, "device.carrier[0].make[0].makeName"));
		}
	}

}
