package com.tmobile.eservices.qa.pages.tmng.functional;

import static org.testng.Assert.assertTrue;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.google.common.collect.Ordering;
import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

public class PlpPage extends TmngCommonPage {

	private final String cellPhonesPlpPageUrl = "/cell-phones";
	private final String accessoriesPlpPageUrl = "/accessories";
	private final String tabletsAndDevicesPlpPageUrl = "/tablets";
	private final String watchesPlpPageUrl = "/smart-watches";

	private final String accessoryMenuOption = "Accessories";
	private final String tabletsAndDevicesMenuOption = "Tablets & Devices";
	private final String watchesMenuOption = "Watches";

	private final String sortByFeatured = "Featured";
	private final String sortByPriceLowToHigh = "Price Low to High";
	private final String sortByPriceHighToLow = "Price High to Low";
	private final String sortByHighestRating = "Highest Rating";
	private final String contactUSPageUrl = "contact-us";

	private final String dealsFilterOption = "Deals";
	private final String brandsFilterOption = "Brands";
	private final String osFilterOption = "Operating System";
	private final String pricerangeFilterOption = "Price Range";
	
	@FindBy(css = "span[class*='small-card'] div[class*='image-container'] img")
	private List<WebElement> phonesListwithImages;

	@FindBy(css = "img[class='color-swatch ng-star-inserted']")
	private List<WebElement> colorSwatchImage;

	@FindBy(css = ".mobile .category-button")
	private WebElement mobileDeviceTypeDropdown;

	@FindBy(css = ".cdk-global-overlay-wrapper .categories .only-mobile span")
	private List<WebElement> menuLinksMobile;

	@FindBy(css = "button span[data-asset-type='link']")
	private List<WebElement> menuLinks;

	@FindBy(xpath = "//a[contains(text(),'+ more')]")
	private WebElement moreFilterOption;
	
	@FindBy(css="a[data-e2e='desktop-filter-toggle-expansion']")
	private WebElement moreFilterOptionsMobile;
	
	@FindBy(css = "p.product-name")
	private List<WebElement> productName;

	@FindBy(css = "div.manufacturer span[data-e2e='product-card-manufacturer']")
	private List<WebElement> productManufacturer;

	@FindBy(xpath = "//h3[contains(text(),'No Items found.')]")
	private WebElement noItemsFoundMessage;

	@FindBy(css = "div[class*='price-lockup-wrapper']")
	private List<WebElement> priceRangeVerification;

	@FindBy(css = "span[class='filter-display-name']")
	private List<WebElement> filterOptions;

	@FindBy(css = "button[title*='Remove']")
	private List<WebElement> appliedFilters;

	@FindBy(css = ".shop-title-container .sort-menu-button span div span")
	private WebElement sortDropdown;
	
	@FindBy(css="mat-expansion-panel-header#mat-expansion-panel-header-22")
	private WebElement sortDropdownForMobile;
	
	@FindBy(css = ".mobile .item-count")
	private WebElement itemsCountMobile;

	@FindBy(css = ".desktop .item-count")
	private WebElement itemsCountDesktop;
	
	@FindBy(css = ".mat-menu-content button")
	private List<WebElement> sortDropdownOptions;
	
	@FindBy(css = "div.mat-radio-label-content")
	private List<WebElement> sortDropdownOptionsForMobile;

	@FindBy(css = "span[class='body--small']")
	private List<WebElement> newFRPDollars;

	@FindBy(css = "span[class='list-price ng-star-inserted']")
	private List<WebElement> strikeOutFRPDollars;

	@FindBy(css = "span.text-legal")
	private List<WebElement> actualOutFRPDollars;

	@FindBy(css = "tmo-bottom-sheet-header[title='Filters']")
	private WebElement filtersHeader;
	
	@FindBy(xpath="//tmo-product-filters[@class='ng-star-inserted']//div[@class='heading-5 sidenav-title']")
	private WebElement filtersHeaderForMobile;
	
	@FindBy(css = "span[data-e2e='desktop-filter-group-name']")
	private List<WebElement> filterTabs;

	@FindBy(css = "span.ratings-count")
	private List<WebElement> ratingCount;

	@FindBy(css = "span>tmo-star-rating")
	private List<WebElement> ratingStars;

	@FindBy(css = "h2.footer__header--copy")
	private WebElement pageFooter;

	@FindBy(css="button[class='expansion__panel--button ng-tns-c12-27 ng-star-inserted']")
	private WebElement supportLinkExpand; 
	
	@FindBy(xpath = "//a[contains(text(),'Contact us')]")
	private WebElement contactUSInfo;

	@FindBy(css = "div.text-legal.fx-flex-100.fx-column.fx-layout-align-center-center")
	private WebElement legalDisclaimer;
	
	@FindBy(css="div.text-legal.has-mobile-margin")
	private WebElement legalDisclaimerforMobile;

	@FindBy(css = "button[title*='Remove']")
	private WebElement clearFilterCTA;
	
	@FindBy(css="span[data-asset-type='button']")
	private List<WebElement> clearFilterCTAMobile;
	
	@FindBy(css = "div[data-analytics-details*='Offer Modal']")
	private List<WebElement> promotionText;

	@FindBy(css = "mat-dialog-container[aria-modal='true']")
	private WebElement promotionModal;

	@FindBy(css = "h6.promo-title")
	private WebElement deviceNameOnPromotionModal;

	@FindBy(css = "h4.heading-4")
	private WebElement promotionTextOnPromotionModal;

	@FindBy(css = "div[class*='sub-category']")
	private List<WebElement> accessorySubCategory;

	@FindBy(css = "div[class*='fx-column fx-layout-align-center-start']")
	private List<WebElement> crporAwesomePrices;

	@FindBy(css = "")
	private List<WebElement> newPricesBasedOnCreditClass;

	@FindBy(css = ".verify-pricing-banner.hide-on-mobile span")
	private WebElement findYourPricing;

	@FindBy(css = ".verify-pricing-banner.hide-on-mobile")
	private WebElement yourPricingBox;

	@FindBy(css = ".verify-pricing-banner.hide-on-mobile p")
	private WebElement yourPricingBoxText;

	@FindBy(css = "div[class*='monthly-price'] div div")
	private List<WebElement> eipPriceBreakDown;

	@FindBy(css = "span[class*='sub-category']")
	private WebElement morelinkforsubcategory;

	@FindBy(css = "h1.display-4")
	private WebElement deviceName;
	
	@FindBy(css="h2#digital-footer-tmobile-icon-title")
	private WebElement connectWithTMobile;
	
	@FindBy(css="button.text-small")
	private WebElement filterCTA;
	
	@FindBy(css="mat-icon[data-event-type='cta']")
	private List<WebElement> closeFilterIcon;	

	@FindBy(xpath="//span[text()='Sort By']")
	private List<WebElement> sortByTab;
	
	public PlpPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 */
	public PlpPage verifyPhonesPlpPageLoaded() {
		try {
			verifyPlpPageUrl(cellPhonesPlpPageUrl);
			Reporter.log("Phones PLP page is loaded");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Cell-Phones PLP Page");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PlpPage verifyPlpPageUrl(String pageUrl) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains(pageUrl),60);
		} catch (Exception ex) {
			Assert.fail("Failed to verify PLP Page url");
		}
		return this;
	}

	/**
	 * Verify that Tablets and Devices page loaded completely.
	 */
	public PlpPage verifyTabletsAndDevicesPlpPageLoaded() {
		try {
			verifyPlpPageUrl(tabletsAndDevicesPlpPageUrl);
			Reporter.log("Tablets and Devices PLP page is loaded");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Tablets and Devices PLP Page");
		}
		return this;
	}

	/**
	 * Verify that Watches page loaded completely.
	 */
	public PlpPage verifyWatchesPlpPageLoaded() {
		try {
			verifyPlpPageUrl(watchesPlpPageUrl);
			Reporter.log("Watches PLP page is loaded");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Watches PLP Page");
		}
		return this;
	}

	/**
	 * Verify Accessory Image in PLP
	 *
	 */
	public PlpPage verifyDeviceImagesInMainPLP() {
		try {
			for (WebElement img : phonesListwithImages) {
				moveToElement(img);
				Assert.assertFalse(img.getAttribute("src").isEmpty(), "Device Image is not found at PLP");
			}
			Reporter.log("Device Image is displayed at PLp");
		} catch (Exception e) {
			Assert.fail("Failed to verify Device Image in PLP");
		}
		return this;
	}

	/**
	 * Verify that Accessories page loaded completely.
	 */
	public PlpPage verifyAccessoriesPlpPageLoaded() {
		try {
			verifyPlpPageUrl(accessoriesPlpPageUrl);
			Reporter.log("Accessories PLP page is loaded");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Accessories PLP Page");
		}
		return this;
	}

	/**
	 * Select Filter Option
	 */
	public PlpPage selectFilterOption(String option) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement filterDropDownOption : filterOptions) {
					if (filterDropDownOption.getText().contains(option)) {
						waitforSpinner();
						clickElementWithJavaScript(filterDropDownOption);
						closeFilterIcon.get(2).click();
						break;
					}
				}
			} else {
				for (WebElement filterDropDownOption : filterOptions) {
					moveToElement(filterDropDownOption);
					if (filterDropDownOption.getText().contains(option)) {
						clickElementWithJavaScript(filterDropDownOption);
						waitforSpinner();
						break;
					}
				}
			}
			Reporter.log(option + " Filter Option is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on " + option + " Filter Option");
		}
		return this;
	}
	
	

	/**
	 * verify Accessories Listed
	 */
	public PlpPage verifyAccessoriesAreFilteredCorrectly(String name) {
		try {
			waitFor(ExpectedConditions.visibilityOf(productName.get(0)),60);
			for (WebElement webElement : productName) {
				Assert.assertTrue(webElement.getText().toLowerCase().contains(name.toLowerCase()),
						"Filtered List of accessories not contain" + name);
			}
			Reporter.log("Filtered accessories list contains only " + name);
		} catch (Exception e) {
			Assert.fail("Failed to verify filtered accessories accessories");
		}
		return this;
	}

	/**
	 * Click on menu Link.
	 *
	 */
	public PlpPage clickOnMenuLink(String option) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				clickElementWithJavaScript(mobileDeviceTypeDropdown);
				waitFor(ExpectedConditions.visibilityOf(menuLinksMobile.get(0)), 20);
				for (WebElement link : menuLinksMobile) {
					String linkText = link.getText().trim().toLowerCase();
					if (option.equalsIgnoreCase(linkText)) {
						clickElementWithJavaScript(link);
						break;
					}
				}
			} else {
				waitFor(ExpectedConditions.visibilityOf(menuLinks.get(0)), 20);
				for (WebElement link : menuLinks) {
					String linkText = link.getText().trim().toLowerCase();
					if (option.equalsIgnoreCase(linkText)) {
						clickElementWithJavaScript(link);
						break;
					}
				}
			}
			Reporter.log("Clicked on '" + option + "' link");
		} catch (Exception e) {
			Assert.fail("Failed to click on '" + option + "' link");
		}
		return this;
	}

	/**
	 * Click on menu Link.
	 *
	 */
	public PlpPage clickOnMenuLinkTPP(String option) {
		try {
			waitFor(ExpectedConditions.visibilityOf(menuLinks.get(0)), 20);
			for (WebElement link : menuLinks) {
				String linkText = link.getText().trim().toLowerCase();
				if (option.equalsIgnoreCase(linkText)) {
					clickElementWithJavaScript(link);
					break;
				}
			}
			Reporter.log("Clicked on '" + option + "' link");
		} catch (Exception e) {
			Assert.fail("Failed to click on '" + option + "' link");
		}
		return this;
	}

	/**
	 * Click On Accessories Link
	 * 
	 * @return
	 */
	public PlpPage clickOnAccessoriesMenuLink() {
		try {
			checkPageIsReady();
			clickOnMenuLink(accessoryMenuOption);
			waitForSpinnerInvisibility();
		} catch (Exception e) {
			Assert.fail("Failed to click on '" + accessoryMenuOption + "' link");
		}
		return this;
	}

	/**
	 * Click On Accessories Link
	 * 
	 * @return
	 */
	public PlpPage clickOnAccessoriesMenuLinkTPP() {
		try {
			clickOnMenuLinkTPP(accessoryMenuOption);
			waitForSpinnerInvisibility();
		} catch (Exception e) {
			Assert.fail("Failed to click on '" + accessoryMenuOption + "' link");
		}
		return this;
	}

	/**
	 * Click on Tablets and Devices Link.
	 */
	public PlpPage clickOnTabletsAndDevicesLink() {
		try {
			clickOnMenuLink(tabletsAndDevicesMenuOption);
		} catch (Exception e) {
			Assert.fail("Failed to click on '" + accessoryMenuOption + "' link");
		}
		return this;
	}

	/**
	 * Click on Watches Link.
	 */
	public PlpPage clickOnWatchesLink() {
		try {
			clickOnMenuLink(watchesMenuOption);
		} catch (Exception e) {
			Assert.fail("Failed to click on '" + watchesMenuOption + "' link");
		}
		return this;
	}
	/**
	 * Click on filter CTA.
	 */
	public PlpPage clickOnFilterCTA() {
		try {
			if(getDriver() instanceof AppiumDriver){
				filterCTA.click();
			}	
		}
		catch (Exception e) {
			Assert.fail("Failed to click on filter CTA");
		}
		return this;
	}

	/**
	 * Verify SimKit Not listed on Accessories PLP on all paginations pages
	 */
	public PlpPage verifySimKitNotListedOnAccessoriesPLP() {
		try {
			String accessoryName;
			String[] rowsCount = null;
			if (getDriver() instanceof AppiumDriver) {
				Reporter.log("Mobile - Not verified in automation - SimKit is not displayed");
			} else {
				rowsCount = itemsCountDesktop.getText().split(" ");

				int count = Integer.parseInt(rowsCount[0].trim());
				for (int i = 0; i <= count / 3; i++) {
					for (WebElement webElement : productName) {
						accessoryName = webElement.getText();
						Assert.assertFalse(accessoryName.contains("SIM"), "Sim Kit is displayed");
					}
					if (getDriver().getCurrentUrl().contains("accessories")) {
						((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,700)");
						checkPageIsReady();
					} else {
						break;
					}
					Reporter.log("SimKit is not displayed");
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify SimKit presence on Accessories PLP");
		}
		return this;
	}

	/**
	 * Verify Sort drop down default Text
	 */
	public PlpPage verifySortingDefaultOptionText() {
		try {
			if(getDriver() instanceof AppiumDriver){
				filterCTA.click();
				sortByTab.get(1).click();
				Assert.assertTrue(sortDropdownOptionsForMobile.get(0).isDisplayed(), "Sort dropdown is not displayed");
				Assert.assertEquals(sortDropdownOptionsForMobile.get(0).getText().trim(), "Featured",
						"Featured text does not matched in Sort Dropdown");
				closeFilterIcon.get(2).click();
			}else {
				scrollDownToelement(sortDropdown);
				Assert.assertTrue(sortDropdown.isDisplayed(), "Sort dropdown is not displayed");
				Assert.assertEquals(sortDropdown.getText().trim(), "Sort by: Featured",
						"Featured text does not matched in Sort Dropdown");
			}
			Reporter.log("Sort dropdown Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Sort dropdown default Text");
		}
		return this;
	}

	/**
	 * Verify Sort dropdown default Text
	 */
	public PlpPage verifySortingDefaultOptionTextForAccessoriesTab() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				filterCTA.click();
				sortByTab.get(1).click();
				Assert.assertTrue(sortDropdownOptionsForMobile.get(0).isDisplayed(), "Sort dropdown is not displayed");
				Assert.assertEquals(sortDropdownOptionsForMobile.get(0).getText().trim(), "Price Low to High",
						"Price Low to High text does not matched in Sort Dropdown");
				closeFilterIcon.get(2).click();
			} else {
				checkPageIsReady();
				scrollDownToelement(sortDropdown);
				Assert.assertTrue(sortDropdown.isDisplayed(), "Sort dropdown is not displayed");
				Assert.assertEquals(sortDropdown.getText().trim(), "Sort by: Price Low to High",
						"Price Low to High text does not matched in Sort Dropdown");
			}
			Reporter.log("Sort dropdown Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Verify Sort dropdown default Text");
		}
		return this;
	}

	/**
	 * verify item count on plp
	 */
	public PlpPage verifyItemCountonPLP() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				Assert.assertTrue(itemsCountMobile.isDisplayed(), "Item count is not displayed for Mobile");
			} else {
				Assert.assertTrue(itemsCountDesktop.isDisplayed(), "Item count is not displayed for Desktop");
			}
			Reporter.log("Item count is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify item count on plp");
		}
		return this;
	}

	/**
	 * Click Sort Drop-down
	 */
	public PlpPage clickOnSortDropdown() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				filterCTA.click();
				sortByTab.get(1).click();	
			} else {
			checkPageIsReady();
			clickElementWithJavaScript(sortDropdown);
			waitFor(ExpectedConditions.elementToBeClickable(sortDropdownOptions.get(0)),30);	
			}Reporter.log("Clicked on 'Sort by:' dropdown");
		} catch (Exception e) {
			Assert.fail("Failed to click on 'Sort by:' dropdown");
		}
		return this;
	}

	/**
	 * Verify that current page URL contains priceHighToLow.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PlpPage verifyUrlContainspriceHighToLow() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("priceHighToLow"),30);
			Reporter.log("priceHighToLow page URL verified");
		} catch (Exception ex) {
			Assert.fail("Failed to verify prr url contains priceHighToLow");
		}
		return this;
	}

	/**
	 * Verify that current page URL contains priceLowToHigh.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PlpPage verifyUrlContainspriceLowToHigh() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("priceLowToHigh"),30);
			Reporter.log("priceLowToHigh page URL verified");
		} catch (Exception ex) {
			Assert.fail("Failed to verify priceLowToHigh url");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PlpPage verifyFilterOptionInPageUrl(String option) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains(option.toLowerCase()),30);
			Reporter.log("Page url contains " + option);
		} catch (Exception ex) {
			Assert.fail("Failed to verify Page URL after filter with brand " + option);
		}
		return this;
	}

	/**
	 * Select Sort option Drop-down
	 */
	public PlpPage selectDropDownOption(String option) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				for (WebElement listItem : sortDropdownOptionsForMobile) {
					String itemText = listItem.getText().trim();
					if (itemText.equalsIgnoreCase(option)) {
						clickElementWithJavaScript(listItem);
						Reporter.log("Selected '" + option + "' from Sort dropdown");
						break;
					}
				}
			} else 
			for (WebElement listItem : sortDropdownOptions) {
				String itemText = listItem.getText().trim();
				if (itemText.equalsIgnoreCase(option)) {
					clickElementWithJavaScript(listItem);
					Reporter.log("Selected '" + option + "' from Sort dropdown");
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to select '" + option + "' from Sort dropdown");
		}
		return this;
	}

	/**
	 * Verify that current page URL contains High to low.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PlpPage verifyUrlContainsHighToLow() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("sort=priceHighToLow"),30);
			Reporter.log("prr page URL verified");
		} catch (Exception ex) {
			Assert.fail("Failed to verify High to Low URL");
		}
		return this;
	}

	/**
	 * Verify that current page URL contains Low to High.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PlpPage verifyUrlContainsLowToHigh() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("sort=priceLowToHigh"),30);
			Reporter.log("prr page URL verified");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Low to High Url");
		}
		return this;
	}

	/**
	 * Verify that current page URL contains High to low.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PlpPage verifyUrlContainsHighlyRated() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("sort=rating"),30);
			Reporter.log("Highly Rated page URL verified");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Highly Rated  url");
		}
		return this;
	}

	/**
	 * Select Featured Sort option from drop-down
	 */
	public PlpPage clickSortByFeatured() {
		try {
			selectDropDownOption(sortByFeatured);
		} catch (Exception e) {
			Assert.fail("Failed to select Sort dropdown option");
		}
		return this;
	}

	/**
	 * Select Price Low to High Sort option from drop-down
	 */
	public PlpPage clickSortByPriceLowToHigh() {
		try {
			if(getDriver() instanceof AppiumDriver){
				selectDropDownOption(sortByPriceLowToHigh);
				clickElementWithJavaScript(closeFilterIcon.get(2));
			}else
			{
			selectDropDownOption(sortByPriceLowToHigh);
	} 
		}catch (Exception e) {
			Assert.fail("Failed to select Sort dropdown option");
		}
		return this;
	}

	/**
	 * Select Price High to Low Sort option from drop-down
	 */
	public PlpPage clickSortByPriceHighToLow() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				selectDropDownOption(sortByPriceHighToLow);
				clickElementWithJavaScript(closeFilterIcon.get(2));				
			} else {
				selectDropDownOption(sortByPriceHighToLow);
			}			
		} catch (Exception e) {
			Assert.fail("Failed to select Sort dropdown option");
		}
		return this;
	}

	/**
	 * Select Price High to Low Sort option from drop-down
	 */
	public PlpPage clickSortByHighestRating() {
		try {
			selectDropDownOption(sortByHighestRating);
		} catch (Exception e) {
			Assert.fail("Failed to select Sort dropdown option");
		}
		return this;
	}

	/**
	 * Verify that current page URL contains rating.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PlpPage verifyUrlContainsRating() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains("rating"),30);
			Reporter.log("Highest rating page URL verified");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Highest rating url");
		}
		return this;
	}

	/**
	 * Verify Price Low To High
	 * 
	 * @return
	 */
	public PlpPage verifyPriceLowToHigh() {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			boolean lowToHigh = false;
			List<Float> frpPrices = new ArrayList<>();
			for (WebElement deviceFrp : newFRPDollars) {
				String frp = deviceFrp.getText().substring(1);
				float frpPrice = Float.parseFloat(frp.replace(".", ""));
				frpPrices.add(frpPrice);
			}
			lowToHigh = Ordering.natural().isOrdered(frpPrices);
			Assert.assertTrue(lowToHigh, "Sorting works incorrectly for 'Prices low to high'");
			Reporter.log("Sorting works correctly for 'Prices low to high'");
		} catch (Exception e) {
			Assert.fail("Failed to verify Prices low to high");
		}
		return this;
	}

	/**
	 * Verify Filters header
	 *
	 * @return the PhonesPage class instance.
	 */
	public PlpPage verifyFiltersHeader() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				checkPageIsReady();
				filterCTA.click();
				Assert.assertTrue(isElementDisplayed(filtersHeaderForMobile), "Filter header not displayed");
				closeFilterIcon.get(2).click();
			} else {
				checkPageIsReady();
				Assert.assertTrue(isElementDisplayed(filtersHeader), "Filter header not displayed");
				Assert.assertEquals(filtersHeader.getText(), "Filters", "Filter text does not matched");
			}
			Reporter.log("Filter Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Filters Header");
		}
		return this;
	}

	/**
	 * Click filter tabs Expand Icon
	 */
	public PlpPage clickFilterTabs(String option) {
		try {
			if (getDriver() instanceof AppiumDriver) {
				for (int i = 1; i < filterTabs.size(); i++) {
					if (filterTabs.get(i).getText().toLowerCase().equalsIgnoreCase(option.toLowerCase())) {
						clickElementWithJavaScript(filterTabs.get(i));
						Reporter.log("Clicked on " + option + " in filter tabs");
						break;
					}
				}
			} else {
				moveToElement(filterTabs.get(1));
				for (int i = 1; i < filterTabs.size(); i++) {
					if (filterTabs.get(i).getText().toLowerCase().equalsIgnoreCase(option.toLowerCase())) {
						moveToElement(filterTabs.get(i));
						clickElementWithJavaScript(filterTabs.get(i));
						Reporter.log("Clicked on " + option + " in filter tabs");
						break;
					}
				}
			}
			Reporter.log("Clicked on " + option + " filter");
		} catch (Exception e) {
			Assert.fail("Failed to click " + option + " filter");
		}
		return this;
	}

	/**
	 * Click Deals filter tabs Expand Icon
	 */
	public PlpPage clickFilterDealTab() {
		try {
			clickFilterTabs(dealsFilterOption);
		} catch (Exception e) {
			Assert.fail("Failed to click " + dealsFilterOption + " filter header");
		}
		return this;
	}

	/**
	 * Click Price Range filter tabs Expand Icon
	 */
	public PlpPage clickFilterPriceRange() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				filterCTA.click();
				clickFilterTabs(pricerangeFilterOption);
			}
				else {
				clickFilterTabs(pricerangeFilterOption);
			}
		} catch (Exception e) {
			Assert.fail("Failed to click " + brandsFilterOption + " filter header");
		}
		return this;
	}

	/**
	 * Click more link filter tabs Expand Icon
	 */
	public PlpPage clickmorelinkInFilter() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				clickElementWithJavaScript(moreFilterOptionsMobile);
			} else {
				clickElementWithJavaScript(moreFilterOption);
			}
		} catch (Exception e) {
			Reporter.log("Failed to click " + brandsFilterOption + " filter header");
		}
		return this;
	}

	/**
	 * Click Brands filter tabs Expand Icon
	 */
	public PlpPage clickFilterBrandsTab() {
		try {
			if(getDriver() instanceof AppiumDriver){
				filterCTA.click();
				clickFilterTabs(brandsFilterOption);
			}else
			{
			clickFilterTabs(brandsFilterOption);
			}
		} catch (Exception e) {
			Assert.fail("Failed to click " + brandsFilterOption + " filter header");
		}
		return this;
	}
	/**
	 * Click OS filter tabs Expand Icon
	 */
	public PlpPage clickFilterOperationSystemTab() {
		try {
			clickFilterTabs(osFilterOption);
		} catch (Exception e) {
			Assert.fail("Failed to click " + osFilterOption + " filter header");
		}
		return this;
	}

	/**
	 * Verify Filter Devices In PLPPage
	 */
	public PlpPage verifyFilteredDevicesInPLPPage(String option) {
		try {
			checkPageIsReady();
			waitForSpinnerInvisibility();
			boolean isFilterDevices = false;
			for (WebElement webElement : productManufacturer) {
				String manufacturer = webElement.getText();
				manufacturer = manufacturer.toLowerCase();
				if (manufacturer.contains(option.toLowerCase())) {
					isFilterDevices = true;
				}
			}
			Assert.assertTrue(isFilterDevices, "Selected " + option + " is not filtered");
			Reporter.log("All filtered devices contains '" + option + "' in title in PLP Page");
		} catch (Exception e) {
			Assert.fail("Devices are not pre filtered in PLP Page");
		}
		return this;
	}

	/**
	 * Click On Rating Count for a Phone
	 * 
	 * 
	 */
	public PlpPage clickOnRatingCount() {
		try {
			waitForSpinnerInvisibility();
			for (WebElement count : ratingCount) {
				if (count.isDisplayed()) {
					moveToElement(count);
					clickElementWithJavaScript(count);
					Reporter.log("Clicked on First Rating Count Visible");
					break;
				}

			}
		} catch (Exception e) {
			Assert.fail("Unable to click the Rating Count for all phones on product list page");
		}
		return this;
	}

	/**
	 * Verify Rating Count for All Phones
	 * 
	 * 
	 */
	public PlpPage verifyAllRatingCount() {
		try {
			waitForSpinnerInvisibility();
			scrollToNextLine();
		Assert.assertTrue(ratingCount.size()>0,"Rating count is not diaplying");
				Reporter.log("Verified Rating Count for All Phones");
	
		} catch (Exception e) {
			Assert.fail("Unable to verify the Rating Count for all phones on product list page");
		}
		return this;
	}

	/**
	 * Verify Rating Stars Image for All products
	 * 
	 * 
	 */
	public PlpPage verifyAllRatingStarsImage() {

		try {
			waitForSpinnerInvisibility();
			if (ratingStars.size() > 0) {
				Reporter.log("Review Star img is displayed on PLP");
			} else {
				scrollToNextLine();
				waitForSpinnerInvisibility();
			}
			for (WebElement star : ratingStars) {
				if (star.isDisplayed()) {
					moveToElement(star);
				} else {
					Assert.fail("Unable to verify one of the Star image");
				}
			}
			Reporter.log("Verified star image for All Phones");
		} catch (Exception e) {
			Assert.fail("Unable to verify the star image for all phones on product list page");
		}
		return this;
	}

	/**
	 * Click On Rating Stars Image for a Phone
	 * 
	 * 
	 */
	public PlpPage clickOnRatingStarsImage() {
		try {
			waitForSpinnerInvisibility();
			for (WebElement star : ratingStars) {
				if (star.isDisplayed()) {
					moveToElement(star);
					clickElementWithJavaScript(star);
					Reporter.log("Clicked on First Star Image Visible");
					break;
				}

			}
		} catch (Exception e) {
			Assert.fail("Unable to click the star image for all phones on product list page");
		}
		return this;
	}

	/**
	 * Click Footer link
	 */
	public PlpPage clickFooterLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				supportLinkExpand.click();
				clickElementWithJavaScript(contactUSInfo);
			} else {
				scrollDownToelement(pageFooter);
				clickElementWithJavaScript(contactUSInfo);
			}
			Reporter.log("Clicked on Footer link");
		} catch (Exception e) {
			Assert.fail("Failed to click Footer links");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PlpPage verifyContactUSPageUrl() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.urlContains(contactUSPageUrl),30);
			Reporter.log("Contact US page URL verified");
		} catch (Exception ex) {
			Assert.fail("Failed to verify ContactUS Page");
		}
		return this;
	}

	/**
	 * Verify No Items found message
	 */
	public PlpPage verifyNoItemsFoundMessage() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(itemsCountMobile), 30);
				assertTrue(itemsCountMobile.getText().equalsIgnoreCase("0 items"),
						"'No Items Found' message is not displayed");
			} else {
				waitFor(ExpectedConditions.visibilityOf(itemsCountDesktop), 30);

				// assertTrue(noItemsFoundMessage.isDisplayed(), "'No Items Found'
				// message is not displayed");
				assertTrue(itemsCountDesktop.getText().equalsIgnoreCase("0 items"),
						"'No Items Found' message is not displayed");
			}
			Reporter.log("'No Items Found' message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'No Items Found' message");
		}
		return this;
	}
	
	/**
	 * Verify that the footer link page loded - contactUS.
	 * 
	 */
	public PlpPage verifyFooterLinkPage() {
		try {
			checkPageIsReady();
			verifyContactUSPageUrl();
			Reporter.log("ContactUS page is loaded");
		} catch (Exception ex) {
			Assert.fail("Failed to verify contact US Page");
		}
		return this;
	}

	/**
	 * Verify Promotion Text Displayed
	 * 
	 */
	public PlpPage verifyPromotionTextonPLP() {
		try {
			checkPageIsReady();
			assertTrue(promotionText.get(0).isDisplayed(), "Promotion Text is not diplayed for any device");
			Reporter.log("Promotion Text is diplayed");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Promotion Text for any device");
		}
		return this;
	}

	/**
	 * Verify legal disclaimer on Phones page.
	 * 
	 */
	public PlpPage verifyLegalDisclaimer() {
		checkPageIsReady();
		try {
			if (getDriver() instanceof AppiumDriver) {
				assertTrue(legalDisclaimerforMobile.isDisplayed(),
						"LegalDisclaimer text is not diplayed on phones page.");
			} else {
				assertTrue(legalDisclaimer.isDisplayed(), "LegalDisclaimer text is not diplayed on phones page.");
			}
			Reporter.log("LegalDisclaimer text is diplayed on phones page.");
		} catch (Exception ex) {
			Assert.fail("Failed to verify LegalDisclaimer text on phones page.");
		}
		return this;
	}

	/**
	 * Verify Device Images are displayed
	 *
	 * @return
	 */
	public PlpPage verifyDeviceImg() {
		try {
			waitforSpinner();
			for (WebElement deviceImgElement : phonesListwithImages) {
				moveToElement(deviceImgElement);
				Assert.assertTrue(deviceImgElement.isDisplayed(), "Device images are not displayed");
				if (deviceImgElement.getAttribute("src").isEmpty() || deviceImgElement.getAttribute("src") == null) {
					Reporter.log("Image is broken for " + deviceImgElement.getAttribute("alt").substring(9));
					Assert.fail("Device images for '" + deviceImgElement.getAttribute("alt").substring(9)
							+ "' does not have SRC attribute ");
				}
			}
			Reporter.log("Device Image is present for all Devices");
		} catch (Exception e) {
			Assert.fail("Failed to display device image for all devices");
		}
		return this;
	}

	/**
	 * Verify color options are displayed in product grid
	 *
	 * @return
	 */
	public PlpPage verifyColorSwatchDisplayed() {
		try {
			waitforSpinner();
			int i = 0;
			for (WebElement colorElement : colorSwatchImage) {
				i++;
				moveToElement(colorElement);
				Assert.assertTrue(colorElement.isDisplayed(), "Color option is not displayed on phone grid");
				if (colorElement.getAttribute("src").isEmpty() || colorElement.getAttribute("src").isEmpty() == true) {
					Reporter.log("Color swatch image is broken for swatch number " + i);
					Assert.fail("Color swatch image number " + i + " doesnt have SRC attribute");
				}
			}
			Reporter.log("Color options are displayed on phone grid");
		} catch (Exception e) {
			Assert.fail("Failed to display color option on phone grid");
		}
		return this;
	}

	/*
	 * Get the device color from PLP*
	 * 
	 * @return
	 **/

	public String getDeviceColorPLP() {
		String color = null;
		try {
			color = getAttribute(colorSwatchImage.get(2), "title");
			Reporter.log("PLP color is obtained");
		} catch (Exception ex) {
			Assert.fail("Failed to get device color on PLP");
		}
		return color.toLowerCase();
	}

	/**
	 * Click On Color Swatch
	 *
	 * @return
	 */
	public PlpPage clickOnColorSwatch() {
		try {
			waitforSpinner();
			String clr = getDeviceColorPLP();
			clickElementWithJavaScript(colorSwatchImage.get(2));
			Reporter.log(clr + " Color option is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on color option on phone grid");
		}
		return this;
	}

	/**
	 * Verify All Phone Names
	 * 
	 * @return
	 */
	public PlpPage verifyAllPhoneNames() {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			for (WebElement device : productName) {
				if (device.getText() != null) {
					moveToElement(device);
				} else {
					Assert.fail("Unable to verify one of the phone names");
					break;
				}
				Reporter.log("Verified All Phone Names");
			}
		} catch (Exception e) {
			Assert.fail("Unable to verify the phone names on product list page");
		}
		return this;
	}

	/**
	 * Click Clear Filter CTA
	 */
	public PlpPage clickClearFilterCTA() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				filterCTA.click();
				clearFilterCTAMobile.get(2).click();
				waitforSpinner();
			} else {
				clickElementWithJavaScript(clearFilterCTA);
				waitforSpinner();
			}
			Reporter.log("Clear Filter CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Clear Filter CTA");
		}
		return this;
	}

	/**
	 * Click Promotion Text for device
	 * 
	 */
	public String clickPromotionTextonPLP() {
		String promoTextAmount = null;
		try {
			for (int i = 0; i <= 2; i++) {
				for (WebElement webElement : promotionText) {
					if (webElement.getText().contains("$")) {
						String PromoTextPLP = webElement.getText();
						String[] a = PromoTextPLP.split("\\$");
						String[] b = a[1].split(" ");
						promoTextAmount = b[0];
						webElement.click();
						i = 4;
						break;
					}
				}
				scrollToNextLine();
			}

		} catch (Exception ex) {
			Assert.fail("Failed to click Promotion Text for any device");

		}
		return promoTextAmount;
	}

	/**
	 * Verify Promotion Modal
	 * 
	 */
	public PlpPage verifyPromotionModal() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(promotionModal),30);
			assertTrue(promotionModal.isDisplayed(), "Promotion Modal is not diplayed");
			Reporter.log("Promotion Modal is diplayed");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Promotion Modal");

		}
		return this;
	}

	/**
	 * Verify Device name On Promotion Modal
	 * 
	 */
	public PlpPage verifyDeviceNameOnPromotionModal() {
		try {
			checkPageIsReady();
			assertTrue(deviceNameOnPromotionModal.isDisplayed(), "Device name on Promotion Modal is not diplayed");
			Reporter.log("Device name on Promotion Modal is diplayed");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Device name on Promotion Modal");
		}
		return this;
	}

	/**
	 * Verify Promotion Text On Promotion Modal
	 * 
	 */
	public PlpPage verifyPromotionTextOnPromotionModal() {
		try {
			checkPageIsReady();
			assertTrue(promotionTextOnPromotionModal.isDisplayed(),
					"Promotion Text on Promotion Modal is not diplayed");
			Reporter.log("Promotion Text on Promotion Modal is diplayed");
		} catch (Exception ex) {
			Assert.fail("Failed to verify Promotion Text on Promotion Modal");

		}
		return this;
	}

	/**
	 * Get Promotion Text from Promotion Modal
	 * 
	 */
	public String getPromotionAmountfromPromotionModal() {
		String PromoTextAmount = null;
		try {
			String PromoTextModal = promotionTextOnPromotionModal.getText();
			String[] a = PromoTextModal.split("\\$");
			String[] b = a[1].split(" ");
			PromoTextAmount = b[0];
			Reporter.log("Promotion Text on Promo Modal is taken in a variable");
		} catch (Exception e) {
			Assert.fail("Failed to get Promotion Text on Promo Modal in a variable");
		}
		return PromoTextAmount;
	}

	/**
	 * Compare Promo Text on PLP and Promo Modal
	 * 
	 */
	public PlpPage ComparePromoText(String PromoOne, String PromoTwo) {
		try {
			Assert.assertEquals(PromoOne, PromoTwo, "Promotion Text on Modal and PLP does not match");
			Reporter.log("Promotion Text on Promotion Modal and PLP matched");
		} catch (Exception ex) {
			Assert.fail("Failed to Compare promotion Text");

		}
		return this;
	}

	/**
	 * Get Promotion Text From PLP
	 * 
	 */
	public String getPromotionAmountfromPLP() {
		String PromoTextAmount = null;
		try {
			String PromoTextPLP = promotionText.get(1).getText();
			String[] a = PromoTextPLP.split("\\$");
			String[] b = a[1].split(" ");
			PromoTextAmount = b[0];
			Reporter.log("Promotion Text on PLP is taken in a variable");
		} catch (Exception e) {
			Assert.fail("Failed to get Promotion Text on PLP  in a variable");
		}
		return PromoTextAmount;
	}

	/**
	 * Verify that DEALS, BRANDS and OPERATING SYSTEM is present as filter
	 * options
	 */
	public PlpPage verifyFilterTabOptions() {
		try {
			List<String> al = new ArrayList<String>();
			for (WebElement a : filterTabs) {
				moveToElement(filterTabs.get(filterTabs.size() - 1));
				al.add(a.getText().toString());
			}
			String[] aa = new String[al.size()];
			aa = al.toArray(aa);
			if (!Arrays.asList(aa).contains(dealsFilterOption))
				Assert.fail("Next option is not present: " + dealsFilterOption);
			if (!Arrays.asList(aa).contains(brandsFilterOption))
				Assert.fail("Next option is not present: " + brandsFilterOption);
			if (!Arrays.asList(aa).contains(osFilterOption))
				Assert.fail("Next option is not present: " + osFilterOption);
			Reporter.log("'Deals, Brands and Operating System' are present as filters in Phones PLP");
		} catch (Exception e) {
			Assert.fail("Failed to verify filter tabs");
		}
		return this;
	}

	/**
	 * Verify Price Low To High
	 * 
	 * @return
	 */
	public PlpPage verifyPriceHighToLow() {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			boolean lowToHigh = false;
			List<Float> frpPrices = new ArrayList<>();
			for (WebElement deviceFrp : newFRPDollars) {
				String frp = deviceFrp.getText().substring(1);
				float frpPrice = Float.parseFloat(frp.replace(".", ""));
				frpPrices.add(frpPrice);
			}
			lowToHigh = Ordering.natural().reverse().isOrdered(frpPrices);
			Assert.assertTrue(lowToHigh, "Sorting works incorrectly for 'Prices high to low'");
			Reporter.log("Sorting works correctly for 'Prices high to low'");
		} catch (Exception e) {
			Assert.fail("Failed to verify Prices high to low");
		}
		return this;
	}

	/**
	 * 
	 * Get Strike out FRP for promo device
	 * 
	 */
	public Double getStrikeOutFRPForPromoDevice() {
		Double strikeOutFRPValue = null;
		String strikeOutPricetext = "";
		try {
			for (WebElement strikeOutPrice : strikeOutFRPDollars) {
				strikeOutPricetext = strikeOutPrice.getText();
				break;
			}
			strikeOutPricetext = strikeOutPricetext.replace("$", "");
			strikeOutFRPValue = Double.parseDouble(strikeOutPricetext);
		} catch (Exception e) {
			Assert.fail("Failed to Get Strike out FRP for promo device");
		}

		return strikeOutFRPValue;
	}

	/**
	 * 
	 * Get New FRP for promo device
	 * 
	 */
	public Double getNewRPForPromoDevice() {
		Double newFRPValue = null;
		String actualPricetext = "";
		try {
			for (WebElement actualPrice : actualOutFRPDollars) {
				actualPricetext = actualPrice.getText();
				break;
			}
			actualPricetext = actualPricetext.replace("$", "");
			newFRPValue = Double.parseDouble(actualPricetext);
		} catch (Exception e) {
			Assert.fail("Failed to Get New FRP for promo device");
		}

		return newFRPValue;

	}

	/**
	 * Navigate to https://dmo.digital.t-mobile.com/accessories?sort=prr
	 * successfully
	 */
	public PlpPage navigatetoTMOPRR() {
		try {
			getDriver().navigate().to(getDriver().getCurrentUrl() + "?sort=prr");
			Reporter.log("Navigate to " + System.getProperty("environment") + "accessories?sort=prr successfully");
		} catch (Exception e) {
			Assert.fail("Failed to navigate " + System.getProperty("environment") + "accessories?sort=prr");
		}
		return this;
	}

	/**
	 * Navigate to https://dmo.digital.t-mobile.com/accessories/apple?sort=prr
	 * successfully
	 */
	public PlpPage navigatetoTMOApplePRR() {
		try {
			getDriver().navigate().to(getDriver().getCurrentUrl() + "/apple?sort=prr");
			Reporter.log(
					"Navigate to " + System.getProperty("environment") + "/accessories/apple?sort=prr successfully");
		} catch (Exception e) {
			Assert.fail("Failed to navigate " + System.getProperty("environment") + "/accessories/apple?sort=prr");
		}
		return this;
	}

	/**
	 * verify Brand filter options are sorted
	 */
	public PlpPage verifyBrandFilterOptionsSort() {
		try {
			String previous = "";
			for (WebElement webElement : filterOptions) {
				if (webElement.getText().compareTo(previous) < 0)
					Reporter.log("Brand Filter Options are not sorted alphabetically");
				previous = webElement.getText();
			}
			Reporter.log("Brand Filter Options are sorted alphabetically");
		} catch (Exception e) {
			Assert.fail("Failed to verify Brand Filter Options sorting order");
		}
		return this;
	}

	/**
	 * Verify Brand Names on Manufacturer Filter
	 */
	public PlpPage verifyBrandNameOnFilters() {
		try {
			checkPageIsReady();
			for (WebElement filterName : filterOptions) {
				String deviceNameText = filterName.getText();

				if (deviceNameText == "Apple" || deviceNameText == "LG" || deviceNameText == "Samsung"
						|| deviceNameText == "T-Mobile" || deviceNameText == "Alcatel"
						|| deviceNameText == "Motorola") {
					Assert.assertTrue(true, "Brand name is not displayed in Filters");
					break;
				}
			}
			Reporter.log("Brand name is displayed in Filters");
		} catch (Exception e) {
			Assert.fail("Failed to verify Brand name in Filters");
		}
		return this;
	}

	/**
	 * verify PriceRange Filter Options
	 */
	public PlpPage verifyPriceRangeFilterOptions() {
		try {
			checkPageIsReady();
			for (WebElement filterName : filterOptions) {
				String deviceNameText = filterName.getText();
				if (deviceNameText == "$0 - $15" || deviceNameText == "$15 - $30" || deviceNameText == "$30 - $50"
						|| deviceNameText == "$50 - $80" || deviceNameText == "$80+") {
					Assert.assertTrue(true, "Price range option name is not displayed in Filters");
					break;
				}
			}
			Reporter.log("Price range name is displayed in Filters");
		} catch (Exception e) {
			Assert.fail("Failed to verify PriceRange Filter Options");
		}
		return this;
	}

	/**
	 * Verify options for OS Filter
	 */
	public PlpPage verifyOSOptionsFilters() {
		try {
			checkPageIsReady();
			for (WebElement conditionName : filterOptions) {
				String filterNameText = conditionName.getText();
				if (filterNameText == "ANDROID" || filterNameText == "iOS" || filterNameText == "OTHER") {
					Assert.assertTrue(true, "OS filter option is not displayed in Filters");
					break;
				}
			}
			Reporter.log("OS filter name is displayed in Filters");
		} catch (Exception e) {
			Assert.fail("Failed to verify OS filter name in Filters");
		}
		return this;
	}

	/**
	 * verify selected filter on clear filter section on top
	 */
	public PlpPage verifySelectedFilterDisplayedOnTop(String appliedFilterName) {
		try {
			waitforSpinner();
			checkPageIsReady();
			for (WebElement webElement : appliedFilters) {
				String deviceName = webElement.getAttribute("title").toLowerCase();
				Assert.assertTrue(deviceName.contains(appliedFilterName.toLowerCase()),
						appliedFilterName + "filters are not displayed & applied");
			}
			Reporter.log(appliedFilterName + " filter is applied & displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify selected filter on clear filter section on top ");
		}
		return this;
	}

	/**
	 * Click On Remove Filter
	 * 
	 * @return
	 */
	public PlpPage clickOnRemoveFilter() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				filterCTA.click();
				clearFilterCTAMobile.get(2).click();
				closeFilterIcon.get(2).click();
			} else {
				clickElementWithJavaScript(appliedFilters.get(0));
				
			}Reporter.log("Minus icon is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Minus icon");
		}
		return this;
	}

	/**
	 * verify remove filtered Devices are not Listed
	 */
	public PlpPage verifyRemoveFilteredDevicesNotListed(String brand) {
		try {
			waitforSpinner();
			checkPageIsReady();
			for (WebElement webElement : productName) {
				String deviceName = webElement.getText().toLowerCase();
				Assert.assertFalse(deviceName.contains(brand.toLowerCase()), brand + " devices are displayed");
			}
			Reporter.log("verified remove filtered Devices are not Listed on PLP for filter: " + brand);
		} catch (Exception e) {
			Assert.fail("Failed to verify remove filtered Devices are not Listed");
		}
		return this;
	}

	/**
	 * Verify the CTAs and Inventory Status of Products
	 * 
	 * @param deviceName
	 */
	public PlpPage verifyInventoryStatusAndCTAs(String deviceName) {
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			PdpPage phonesPdpPage = new PdpPage(getDriver());
			if (deviceName == null) {
				for (WebElement product : productName) {
					moveToElement(product);
					Reporter.log("Clicked on device on the PLP page: " + product.getText() + "'");
					clickElementWithJavaScript(product);
					waitForSpinnerInvisibility();
					checkPageIsReady();
					phonesPdpPage.verifyInventoryStatusandAllCTAs();
					getDriver().navigate().back();
					waitForSpinnerInvisibility();
					checkPageIsReady();
				}
			} else {
				String devices[] = deviceName.split(",");
				int devicesCount = devices.length;
				PlpPage phonesPlpPage = new PlpPage(getDriver());
				int i = 0;
				if (devicesCount >= 1) {
					do {
						phonesPlpPage.clickDeviceWithAvailability(devices[i]);
						waitForSpinnerInvisibility();
						checkPageIsReady();
						phonesPdpPage.verifyInventoryStatusandAllCTAs();
						getDriver().navigate().back();
						waitForSpinnerInvisibility();
						checkPageIsReady();
						i++;
					} while (i < devicesCount);
				} else {
					phonesPlpPage.clickDeviceWithAvailability(devices[0]);
					waitForSpinnerInvisibility();
					checkPageIsReady();
					phonesPdpPage.verifyInventoryStatusandAllCTAs();
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to click device in PLP Page");
		}
		return this;
	}

	/**
	 * verify Accessory Sub Categories
	 *
	 */
	public PlpPage verifyAccessoriesSubCategories() {
		try {
			if(getDriver() instanceof AppiumDriver){
				clickElementWithJavaScript(mobileDeviceTypeDropdown);
				Assert.assertTrue(accessorySubCategory.get(22).isDisplayed(), "Accessory Sub Categories is Not Dispayed");
			}else
			{			
			Assert.assertTrue(accessorySubCategory.get(0).isDisplayed(), "Accessory Sub Categories is Not Dispayed");
			}	Reporter.log("Accessory Sub Categories is Dispayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Accessory Sub Categories");
		}
		return this;
	}

	/**
	 * click Accessory Sub Category By Name
	 * 
	 * @param subCategoryname
	 * @return
	 */
	public PlpPage clickAccessorySubCategoryByName(String subCategoryname) {
		try {
			if (getDriver() instanceof AppiumDriver) {
			} else {
				morelinkforsubcategory.click();
			}
			for (WebElement webElement : accessorySubCategory) {
				if (webElement.getText().equalsIgnoreCase(subCategoryname)) {
					webElement.click();
					checkPageIsReady();
					break;
				}
			}
			Reporter.log("clicked  on accessories sub category");
		} catch (Exception e) {
			Assert.fail("Failed to click device in PLP Page");
		}
		return this;
	}

	/**
	 * Verify that current page URL contains Selected Accessory Name.
	 *
	 * @return the PhonesPage class instance.
	 */
	public PlpPage verifyURLContainsSelectedAccessorySubCategoryName(String Name) {
		try {
			waitFor(ExpectedConditions.urlContains("category"),30);
			Assert.assertTrue(getDriver().getCurrentUrl().contains(Name),
					"Accessories With Sub Category Name " + Name + " are not displayed");
			Reporter.log("Accessories With Sub Category Name " + Name + " are displayed");
		} catch (Exception ex) {
			Assert.fail("Failed to verify sub Category Accessories URL");
		}
		return this;
	}

	/**
	 * verify CRP or Awesome Prices in PLP for all devices
	 *
	 */
	public PlpPage verifyCRPorAwesomePricesInPLP() {

		try {
			waitForSpinnerInvisibility();

			for (WebElement price : crporAwesomePrices) {
				if (price.isDisplayed()) {
					moveToElement(price);
					Assert.assertTrue(price.isDisplayed(), "CRP or Awesome Prices are not displaying in PLP");
				} else {
					Assert.fail("Failed to verify CRP or Awesome Prices in PLP for all devices");
					break;
				}
				Reporter.log("CRP or Awesome Prices are displaying in PLP");
			}
		} catch (Exception e) {
			Assert.fail("Unable to verify CRP or Awesome Prices are displaying in PLP");
		}
		return this;
	}

	/**
	 * verify Your Pricing Box in PLP
	 *
	 */
	public PlpPage verifyYourPricingBoxInPLP() {
		try {
			Assert.assertTrue(yourPricingBox.isDisplayed(), "Your Pricing Box is not displaying in PLP");
			Reporter.log("Find Your Pricing Box is displaying in PLP");
		} catch (Exception e) {
			Assert.fail("Failed to verify Find Your Pricing Box in PLP");
		}
		return this;
	}

	/**
	 * verify Your Pricing Box text in PLP
	 *
	 */
	public PlpPage verifyYourPricingBoxTextInPLP() {
		try {
			Assert.assertTrue(yourPricingBoxText.isDisplayed(), "Your Pricing Box text is not displaying in PLP");
			Assert.assertTrue(
					yourPricingBoxText.getText()
							.contains("Down/monthly payments shown are for well-qualified customers."),
					"Your pricing box text is not correct");
			Reporter.log("Find Your Pricing Box text is displaying in PLP");
		} catch (Exception e) {
			Assert.fail("Failed to verify Find Your Pricing Box text in PLP");
		}
		return this;
	}

	/**
	 * verify Find Your Pricing Link in PLP
	 *
	 */
	public PlpPage verifyFindYourPricingLink() {
		try {
			if(isElementDisplayed(findYourPricing)) {
				Assert.assertTrue(findYourPricing.isDisplayed(), "Find Your Pricing Link is not displaying in PLP");
				Reporter.log("Find Your Pricing Link is displaying in PLP");
			}else {
				getDriver().navigate().to(getDriver().getCurrentUrl()+"?mboxDisable=true");
				verifyPhonesPlpPageLoaded();
				Assert.assertTrue(findYourPricing.isDisplayed(), "Find Your Pricing Link is not displaying in PLP");
				Reporter.log("Find Your Pricing Link is displaying in PLP");
			}
			
		} catch (Exception e) {
			Assert.fail("Failed to verify Find Your Pricing Link in PLP");
		}
		return this;
	}

	/**
	 * Click Find Your Pricing Link in PLP
	 *
	 */
	public PlpPage clickFindYourPricingLink() {
		try {
			if(isElementDisplayed(findYourPricing)) {
				clickElementWithJavaScript(findYourPricing);
				Reporter.log("Clicked on Find Your Pricing Link");
			}else {
				getDriver().navigate().to(getDriver().getCurrentUrl()+"?mboxDisable=true");
				verifyPhonesPlpPageLoaded();
				clickElementWithJavaScript(findYourPricing);
				Reporter.log("Clicked on Find Your Pricing Link");
			}
		} catch (Exception e) {
			Assert.fail("Failed to Click Find Your Pricing Link in PLP");
		}
		return this;
	}

	/**
	 * verify New Prices based on credit class in PLP for all devices
	 *
	 */
	public PlpPage verifyNewPriceBasedOnCreditClass() {
		try {
			Assert.assertTrue(newPricesBasedOnCreditClass.get(0).isDisplayed(),
					"New Prices are not displaying in PLP after credit check");
			Reporter.log("New Prices are updated in PLP after credit check");
		} catch (Exception e) {
			Assert.fail("Failed to verify New Prices based on credit class in PLP for all devices");
		}
		return this;
	}

	/**
	 * verify Find Your Pricing disable in PLP
	 *
	 */
	public PlpPage verifyFindYourPricingDisable() {
		try {
			checkPageIsReady();
			Assert.assertFalse(isElementDisplayed(findYourPricing), "Find Your Pricing is displaying in PLP");
		} catch (Exception e) {
			Assert.fail("Find Your Pricing is displaying in PLP.");
		}
		return this;
	}

	/**
	 * verify Monthly payment is displayed
	 */
	public PlpPage verifyEIPPayment() {
		try {
			Assert.assertTrue(eipPriceBreakDown.get(0).isDisplayed(), "Monthly payment is not displayed");
			Reporter.log("Monthly payment is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Monthly payment");
		}
		return this;
	}

	/**
	 * verify Loan Term Length
	 */
	public PlpPage verifyLoanTermLength() {
		try {
			Assert.assertTrue(eipPriceBreakDown.get(1).isDisplayed(),
					"Monthly payment loan term length is not displayed");
			Reporter.log("Monthly payment loan term length is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Loan Term Length");
		}
		return this;
	}

	/**
	 * verify FRP Price
	 */
	public PlpPage verifyFRPPrice() {
		try {
			Assert.assertTrue(newFRPDollars.get(0).isDisplayed(), "FRP is not displayed");
			Reporter.log("FRP is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify FRP");
		}
		return this;
	}

	/**
	 * get device Name from PDP
	 */
	public String getDeviceName() {
		String deviceNameStr = "";
		try {
			deviceNameStr = deviceName.getText();
			Reporter.log("Device name is : " + deviceNameStr);
		} catch (Exception e) {
			Assert.fail("Failed to  get device Name from PDP");
		}
		return deviceNameStr;
	}

	/**
	 * verify Down-payment
	 */
	public PlpPage verifyDownPaymentPrice() {
		try {
			Assert.assertTrue(eipPriceBreakDown.get(2).isDisplayed(), "Downpayment price is not displayed");
			Reporter.log("Downpayment price is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Down-payment.");
		}
		return this;
	}

	/**
	 * Click device containing availability
	 *
	 * @param pName
	 *
	 */
	public void clickDeviceWithAvailability(String pName) {
		try {
//			String url = getDriver().getCurrentUrl();
//			ProductsAPIV3 productsAPIV3 = new ProductsAPIV3();
//			List<String> products = productsAPIV3.getProducts(url);
			List<String> products = readDataFromAvailableDevices();

			if (pName != null && products.toString().contains(pName + ",")) {
				pName = pName.trim();
				Boolean elementClick = true;
				do {
					for (int i = 0; i < productName.size(); i++) {
						String currentProductName = productName.get(i).getText();
						String productNameWithManufacturer = productManufacturer.get(i).getText() + " "
								+ currentProductName;
						if (currentProductName.trim().endsWith(pName) || productNameWithManufacturer.endsWith(pName)) {
							// moveToElement(productName.get(i));
							clickElementWithJavaScript(productName.get(i));
							Reporter.log("Clicked on product: " + currentProductName);
							elementClick = false;
							break;
						}
					}
					waitForSpinnerInvisibility();
					if (getDriver().getCurrentUrl().contains("cell-phones")
							| getDriver().getCurrentUrl().contains("smart-watches")
							| getDriver().getCurrentUrl().contains("accessories")
							| getDriver().getCurrentUrl().contains("tablets")) {
						scrollToNextLine();
					} else {
						break;
					}
				} while (elementClick);
			} else {
				String[] rowsCount = null;
				if (getDriver() instanceof AppiumDriver) {
					rowsCount = itemsCountMobile.getText().split(" ");
				} else {
					rowsCount = itemsCountDesktop.getText().split(" ");
				}
				int count = Integer.parseInt(rowsCount[0].trim());
				for (int i = 0; i <= count / 3; i++) {
					for (WebElement product : productName) {
						if (products.toString().contains(product.getText() + ",")) {
							String productName = product.getText();
							clickElementWithJavaScript(product);
							Reporter.log("Clicked on product: " + productName);
							break;
						}
					}
					waitForSpinnerInvisibility();
					if (getDriver().getCurrentUrl().contains("cell-phones")
							| getDriver().getCurrentUrl().contains("smart-watches")
							| getDriver().getCurrentUrl().contains("accessories")
							| getDriver().getCurrentUrl().contains("tablets")) {
						scrollToNextLine();
					} else {
						break;
					}
				}
			}
		} catch (Exception e) {
			Assert.fail("Fail to click on next device: " + pName);
		}
	}

	/**
	 * Scroll to next line in PLP
	 *
	 */
	public void scrollToNextLine() {
		try {
			if (getDriver() instanceof IOSDriver) {
				scrollDownMobileIOS();
			} else {
				((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,700)");
			}
		} catch (Exception e) {
			Assert.fail("Failed to scroll to next line");
		}
	}

	/*
	 * Select SIM Kit in PLP
	 * 
	 * @param deviceName
	 */
	public void selectSimCardFromPlp() {
		try {
			
			String[] rowsCount = null;
			if(getDriver() instanceof AppiumDriver){
			rowsCount = itemsCountMobile.getText().split(" ");
			}else
			{
			rowsCount = itemsCountDesktop.getText().split(" ");
			}
			int count = Integer.parseInt(rowsCount[0].trim());
			for (int i = 0; i <= count / 3; i++) {
				for (WebElement product : productName) {
					
					if (product.getText().toLowerCase().contains("sim card")) {
						waitFor(ExpectedConditions.elementToBeClickable(product),30);
						waitforSpinner();
						product.click();						
						break;
					}
				}
				waitForSpinnerInvisibility();
				if (getDriver().getCurrentUrl().contains("cell-phones")
						| getDriver().getCurrentUrl().contains("smart-watches")
						| getDriver().getCurrentUrl().contains("accessories")
						| getDriver().getCurrentUrl().contains("tablets")) {
					((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,700)");
				} else {
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Unable to select SIM Card in product list page");
		}
	}

	/**
	 * click Accessory Sub Category By Name
	 *
	 * @return
	 */
	public PlpPage verifyAccessorySubCategoryByName() {
		try {
			morelinkforsubcategory.click();
			boolean isAccessorySubCategoryDisplayed = false;

			List<String> accessorySubCategorydetails = new ArrayList<>(
					Arrays.asList("Cases & covers", "Chargers & adapters", "Charging", "Headphones",
							"Mounts, docks & stands", "Others", "Screen protectors", "Smart home", "Speakers"));

			for (String detail : accessorySubCategorydetails) {
				for (WebElement webElement : accessorySubCategory) {
					if (webElement.isDisplayed()) {
						if (detail.equals(webElement.getText())) {
							isAccessorySubCategoryDisplayed = true;
							break;
						} else {
							isAccessorySubCategoryDisplayed = false;
						}
					}
				}
				if (!isAccessorySubCategoryDisplayed) {
					break;
				}
			}
			Assert.assertTrue(isAccessorySubCategoryDisplayed, "Accessory Sub category name is not dispalyd");
			Reporter.log("Accessory Sub category name is displayed");

		} catch (Exception e) {
			Assert.fail("Failed to display sub category device in PLP Page");
		}
		return this;
	}

	/**
	 * Verify color options are displayed in product grid for family name
	 *
	 * @return
	 */
	public PlpPage verifyColorSwatchForGivenFamilyName(String familyname, String color) {
		try {
			// For IOS
			if (getDriver() instanceof IOSDriver) {
				scrollDownMobileIOS();
				String deviceName = familyname.replace(" ", "-");
				List<WebElement> colorOptions = getDriver()
						.findElements(By.cssSelector("a[href*='" + deviceName.toLowerCase()
								+ "?'] div[class*='swatch-container'] img[src*='" + deviceName + "-']"));
				if (colorOptions.size() == 0) {
					scrollUpMobileIOS();
					colorOptions = getDriver().findElements(By.cssSelector("a[href*='" + deviceName.toLowerCase()
							+ "?'] div[class*='swatch-container'] img[src*='" + deviceName + "-']"));
				}
				int colorCount = Integer.parseInt(color);
				Assert.assertTrue(colorCount == colorOptions.size(),
						"Device Color options are not proper for " + familyname);
				Reporter.log("Color options are displayed on phone grid");
				Reporter.log("Expected nember of colors: " + color + ", found: " + colorCount);
			}
			// For Android
			else if (getDriver() instanceof AndroidDriver) {
				((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,700)");
				String deviceName = familyname.replace(" ", "-");
				List<WebElement> colorOptions = getDriver()
						.findElements(By.cssSelector("a[href*='" + deviceName.toLowerCase()
								+ "?'] div[class*='swatch-container'] img[src*='" + deviceName + "-']"));
				if (colorOptions.size() == 0) {
					((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,-700)");
					colorOptions = getDriver().findElements(By.cssSelector("a[href*='" + deviceName.toLowerCase()
							+ "?'] div[class*='swatch-container'] img[src*='" + deviceName + "-']"));
				}
				int colorCount = Integer.parseInt(color);
				Assert.assertTrue(colorCount == colorOptions.size(),
						"Device Color options are not proper for " + familyname);
				Reporter.log("Color options are displayed on phone grid");
				Reporter.log("Expected nember of colors: " + color + ", found: " + colorCount);
			}
			// For Windows
			else {
				String deviceName = familyname.replace(" ", "-");
				List<WebElement> colorOptions = getDriver()
						.findElements(By.cssSelector("a[href*='" + deviceName.toLowerCase()
								+ "?'] div[class*='swatch-container'] img[src*='" + deviceName + "-']"));
				int colorCount = Integer.parseInt(color);
				Assert.assertTrue(colorCount == colorOptions.size(),
						"Device Color options are not proper for " + familyname);
				Reporter.log("Color options are displayed on phone grid");
				Reporter.log("Expected nember of colors: " + color + ", found: " + colorCount);
			}
		} catch (Exception e) {
			Assert.fail("Failed to display color option on phone grid");
		}
		return this;
	}

	/**
	 * Verify device family name in plp page
	 */
	public PlpPage verifyDevicesFamilyName(String familyname) {
		try {
			boolean isFamilyNameAvailable = false;
			String[] rowsCount;
			if (getDriver() instanceof AppiumDriver) {
				rowsCount = itemsCountMobile.getText().split(" ");
				int countMobile = Integer.parseInt(rowsCount[0].trim());
				for (int i = 0; i <= countMobile / 2; i++) {
					for (WebElement product : productName) {
						if (familyname.equals(product.getText().trim())) {
							Reporter.log("Verified Family name successfully:" + familyname);
							isFamilyNameAvailable = true;
							break;
						}

					}
					if (isFamilyNameAvailable) {
						break;
					} else {
						if (getDriver() instanceof AndroidDriver) {
							((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,700)");
							waitForSpinnerInvisibility();
						} else {
							scrollDownMobileIOS();
						}
					}
				}

			} else {
				rowsCount = itemsCountDesktop.getText().split(" ");
				int count = Integer.parseInt(rowsCount[0].trim());
				for (int i = 0; i <= count / 3; i++) {
					for (WebElement product : productName) {
						if (familyname.equals(product.getText().trim())) {
							waitFor(ExpectedConditions.elementToBeClickable(product),30);
							Reporter.log("Verified Family name successfully: " + familyname);
							isFamilyNameAvailable = true;
							break;
						}
					}
					if (isFamilyNameAvailable) {
						break;
					} else {
						((JavascriptExecutor) getDriver()).executeScript("window.scrollBy(0,725)");
						waitForSpinnerInvisibility();
					}
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify '" + familyname + "' device family name in PLP page");
		}
		return this;
	}

	/**
	 * Click device containing availability
	 *
	 * @param pName
	 *
	 */
	public void clickDevice(String pName) {
		try {
			pName = pName.trim();
			Boolean elementClick = true;
			do {
				for (int i = 0; i < productName.size(); i++) {
					String productNameWithManufacturer = productManufacturer.get(i).getText() + " "
							+ productName.get(i).getText();
					if (productName.get(i).getText().trim().endsWith(pName)
							|| productNameWithManufacturer.endsWith(pName)) {
						String deviceName = productName.get(i).getText();
						// moveToElement(productName.get(i));
						clickElementWithJavaScript(productName.get(i));
						Reporter.log("Clicked on product: " + deviceName);
						elementClick = false;
						break;
					}
				}
				waitForSpinnerInvisibility();
				if (getDriver().getCurrentUrl().contains("cell-phones")
						| getDriver().getCurrentUrl().contains("smart-watches")
						| getDriver().getCurrentUrl().contains("accessories")
						| getDriver().getCurrentUrl().contains("tablets")) {
					scrollToNextLine();
				} else {
					break;
				}
			} while (elementClick);
		} catch (Exception e) {
			Assert.fail("Fail to click on next device: " + pName);
		}
	}

	public List<String> readDataFromAvailableDevices(){
		String fileName = "src/test/resources/testdata/tmngAvailiableDevices.txt";
		List<String> lines = new ArrayList<>();
		try{
			//lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
			Scanner s = new Scanner(new FileReader(fileName));
			while (s.hasNext()){
				lines.add(s.nextLine());
			}
			s.close();
			return lines;
		}catch (Exception e){
			Reporter.log("Unable to read available device list");
		}
		return null;
	}
	
	
}
