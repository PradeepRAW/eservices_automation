/**
 * 
 */
package com.tmobile.eservices.qa.data;

import com.tmobile.eqm.testfrwk.ui.core.annotations.Data;

/**
 * @author charshavardhana
 *
 */
public class Payment {
	
	@Data(name = "NameOnCard")
	private String nameOnCard;
	@Data(name = "CardNumber")
	private String cardNumber;
	@Data(name = "ExpiryMonth")
	private String expiryMonth;
	@Data(name = "ExpiryYear")
	private String expiryYear;
	@Data(name = "ExpiryYearMobile")
	private String expiryYearMobile;

	@Data(name = "ExpiryMonthandYear")
	private String expiryMonthAndYear;
	@Data(name = "ZipCode")
	private String zipCode;
	@Data(name = "CVV")
	private String cvv;
	@Data(name = "CardHoldersName")
	private String cardHoldersName;
	@Data(name = "ExpiryDate")
	private String expiryDate;
	@Data(name = "OrderNumber")
	private String orderNumber;
	@Data(name = "Amount")
	private String amount;
	@Data(name = "CheckingName")
	private String checkingName;
	@Data(name = "RoutingNumber")
	private String routingNumber;
	@Data(name = "AccountNumber")
	private String accountNumber;

	@Data(name = "State")
	private String state;

	@Data(name = "NickName")
	private String nickName;
	
	@Data(name = "UserType")
	private String userType;
	
	/**
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * @return the nameOnCard
	 */
	public String getNameOnCard() {
		return nameOnCard;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @return the expiryMonth
	 */
	public String getExpiryMonth() {
		return expiryMonth;
	}

	/**
	 * @return the expiryYear
	 */
	public String getExpiryYear() {
		return expiryYear;
	}
	
	/**
	 * @return the expiryYear
	 */
	public String getExpiryYearMobile() {
		return expiryYearMobile;
	}
	
	
	
	
	/**
	 * @return the expiryYear
	 */
	public String getExpiryMonthYear() {
		return expiryMonthAndYear;
	}


	/**
	 * @return the zipCode
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * @return the cvv
	 */
	public String getCvv() {
		return cvv;
	}

	/**
	 * @return the cardHoldersName
	 */
	public String getCardHoldersName() {
		return cardHoldersName;
	}

	/**
	 * @return the expiryDate
	 */
	public String getExpiryDate() {
		return expiryDate;
	}

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getCheckingName() {
		return checkingName;
	}
	
	public String getState() {
		return state;
	}
	
	/**
	 * @return the NickName
	 */
	public String getNickName() {
		return nickName;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public String getUserType() {
		return userType;
	}

	@Override
	public String toString() {
		return "Payment [nameOnCard=" + nameOnCard + ", cardNumber=" + cardNumber + ", expiryMonth=" + expiryMonth
				+ ", expiryYear=" + expiryYear + ", expiryYearMobile=" + expiryYearMobile + ", expiryMonthAndYear="
				+ expiryMonthAndYear + ", zipCode=" + zipCode + ", cvv=" + cvv + ", cardHoldersName=" + cardHoldersName
				+ ", expiryDate=" + expiryDate + ", orderNumber=" + orderNumber + ", amount=" + amount
				+ ", checkingName=" + checkingName + ", routingNumber=" + routingNumber + ", accountNumber="
				+ accountNumber + ", state=" + state + "]";
	}
}
