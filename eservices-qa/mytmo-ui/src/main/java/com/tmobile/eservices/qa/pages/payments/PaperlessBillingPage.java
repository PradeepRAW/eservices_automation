package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.tmobile.eservices.qa.pages.CommonPage;

public class PaperlessBillingPage extends CommonPage {

    @FindBy(css = "div.TMO-BILLING-AND-PAYMENTS-PAPERLESS-UAT-NEW")
    private WebElement pageLoadElement;

    @FindBy(css = "i.fa.fa-spinner")
    private WebElement pageSpinner;

    @FindBy(css = "p.Display3")
    private WebElement pageHeader;
    
    private final String pageUrl = "billing_payment/paperless_billing";

    private By paperlessSpinner = By.className("TMO-LOADER-SPINNER");
    /**
     * Verify that current page URL matches the expected URL.
     */


    /**
     * Constructor
     *
     * @param webDriver
     */
    public PaperlessBillingPage(WebDriver webDriver) {
        super(webDriver);
    }

    public PaperlessBillingPage verifyPageUrl() {
        waitFor(ExpectedConditions.urlContains(pageUrl));
        return this;
    }

    /**
     * Verify that the page loaded completely.
     * @throws InterruptedException
     */
    public PaperlessBillingPage verifyPageLoaded(){
        try{
            checkPageIsReady();
         //   waitFor(ExpectedConditions.invisibilityOfElementLocated(paperlessSpinner));
            waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
            verifyPageUrl();
            verifyPageHeader();
        }catch(Exception e){
            Assert.fail("PaperlessBillingPage page not loaded");
        }
        return this;
    }

    /**
     * verify page header is displayed
     */
	private void verifyPageHeader() {
        try{
        	pageHeader.isDisplayed();
        }catch(Exception e){
            Assert.fail("Paperless Billing Header is not displayed");
        }
	}

    }

