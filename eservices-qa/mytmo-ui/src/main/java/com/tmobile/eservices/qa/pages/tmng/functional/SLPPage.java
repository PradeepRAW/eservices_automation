package com.tmobile.eservices.qa.pages.tmng.functional;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.TMNGData;
import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class SLPPage extends TmngCommonPage {

	public SLPPage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(css = ".device-section [ng-click*='vm.openStoreLocatorModal()']")
	private WebElement nearbyStoresHeader;

	@FindBy(css = "div[class*='filterDescription'] span.ng-binding")
	private WebElement checkBoxDescription;

	@FindBy(css = "button[class*='store-result-item']")
	private List<WebElement> storeListOnSLP;

	@FindBy(css = ".check1")
	private WebElement checkBoxInStoreLocatorPage;

	@FindBy(css = "[ng-bind*='checkboxMessage']")
	private WebElement checkBoxMessage;

	@FindBy(css = "label[for='cartFilterCheckbox']")
	private WebElement checkBoxStatus;

	@FindBy(id = "backButton")
	private WebElement goBack;

	@FindBy(css = "*[ng-bind='vm.getItemsCtaText()']")
	private WebElement showHideItemsCTAOnSLP;

	@FindBy(id = "storeName")
	private WebElement storeNameInFilteredSearch;

	@FindBy(css = "span[ng-bind*='inventory.deviceName']")
	private List<WebElement> showItemsAreDisplayedOnStoreListPage;

	@FindBy(css = "#map")
	private WebElement storeLocatorMap;

	@FindBy(css = "button[aria-label='Zoom in']")
	private WebElement zoomInButtonOnMap;

	@FindBy(xpath = "//span[contains(text(),'Appointment')]")
	private List<WebElement> appointmentInStoreListModal;

	@FindBy(css = "[ng-class*='vm.oneInStock']")
	private List<WebElement> itemsInStockTextOnStoreListPage;

	@FindBy(css = "b[id*='store-name']")
	private List<WebElement> storesInStoreListPage;

	@FindBy(xpath = "//a[contains(text(),'Hide items')]")
	private WebElement hideItemsCTAOnStoreListPage;

	@FindBy(css = "button[ng-class*='vm.model.searchingStore ?']")
	private WebElement searchCTAOnMap;

	@FindBy(css = "b[class='ng-binding']")
	private List<WebElement> storeNameOnSLPList;

	@FindBy(xpath = "//button[contains(@class,'store-locator-search-submit')]")
	private WebElement searchImg;

	@FindBy(xpath = "//span[@ng-if='vm.item.todayHours']/span[@class='ng-binding']")
	private WebElement storeTimings;

	@FindBy(id = "get-inline-cta")
	private List<WebElement> getInLineCtaInStoreListModal;

	@FindBy(id = "call-us-cta")
	private List<WebElement> callUsCtaInStoreListModal;

	@FindBy(css = "[ng-click='vm.openFinalizeInStoreOnCart(vm.item)']")
	private List<WebElement> saveCartLinkSLP;

	@FindBy(css = "input[id*='storeSearchField']")
	private WebElement searchField;

	@FindBy(css = ".check1")
	private WebElement checkBoxOnSaveYourCartModel;

	@FindBy(xpath = "//button[@aria-label='Search']")
	private WebElement clickSearchIcon;

	@FindBy(css = "p[ng-bind*='vm.authoring.digitalToRetail.noNearbyStoreMessage']")
	private WebElement noStoreInventoryMessage;

	@FindBy(css = "span[ng-bind = 'vm.item.inventoryMessage']")
	private List<WebElement> inventoryStatusInStoreModalPopup;

	@FindBy(css = "button[ng-click*='closeModal']")
	private WebElement appointmentTroubleConnectingAlertOk;

	@FindBy(css = ".store-locator-overlay b")
	private List<WebElement> appointmentTroubleConnectingAlert;

	@FindBy(css = ".store-locator-panel.store-locator-interstitial")
	private WebElement dateTimeModal;

	@FindBy(css = "[ng-repeat*='slot in vm.model.selectedDateAndDay.timeSlots track by $index'] button span")
	private List<WebElement> timeList;

	@FindBy(css = "[ng-click*='vm.submitSelectedAppointment()']")
	private WebElement nextCTA;

	@FindBy(css = "[ng-class*='vm.model.storeLocatorInModal'] div[ng-class*='vm.model.storeLocatorInModal']")
	private WebElement appointmentForm;

	@FindBy(css = "[class*='firstName']")
	private WebElement firstName;

	@FindBy(css = "[class*='lastName']")
	private WebElement lastName;

	@FindBy(css = "#email")
	private WebElement emailId;

	@FindBy(css = "#phone")
	private WebElement phoneNo;

	@FindBy(css = "#reason")
	private WebElement reasonTextBox;

	@FindBy(css = "#reason option")
	private WebElement reasonOption;

	@FindBy(css = "div.sl-form-group.ng-scope [for*='English']")
	private WebElement englishRadioButton;

	@FindBy(css = "[for*='userConsent'] p")
	private WebElement userConsentCheckbox;

	@FindBy(css = "#reason option")
	private List<WebElement> selectReason;

	@FindBy(css = "")
	private WebElement appointmentButton;

	@FindBy(xpath = "//button[contains(text(),'Keep the original appointment')]")
	private List<WebElement> keepOriginalAppointment;

	@FindBy(css = ".store-locator-confirmation-heading h1")
	private WebElement appointmentConfirmantionText;

	@FindBy(css = ".store-locator-confirmation-store-details p")
	private List<WebElement> scheduledAppointmentDetails;

	/**
	 * Verify Store Locator list page
	 *
	 */
	public SLPPage verifyStoreLocatorListPage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(goBack), 60);
			Assert.assertTrue(goBack.isDisplayed(), "Store locator list is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Store locator list");
		}
		return this;
	}

	/**
	 * Verify Check Box Description in Store Locator Page is Displayed
	 *
	 */
	public SLPPage verifyCheckBoxDescriptionInStoreLocatorPageIsdisplayed() {
		try {
			Assert.assertTrue(checkBoxDescription.isDisplayed(), "Check Box Description is not displayed");
			Reporter.log("Check Box Description is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Display Store Locator Check Box Description");
		}
		return this;
	}

	/**
	 * verify change Store locator Cta
	 *
	 */
	public SLPPage verifyGoBack() {
		try {
			waitFor(ExpectedConditions.visibilityOf(goBack), 60);
			Assert.assertTrue(goBack.isDisplayed(), "Go Back link at header is not displaying");
		} catch (Exception e) {
			Assert.fail("Failed to verify Go back header link at top header");

		}
		return this;
	}

	/**
	 * Verify If Check Box is not selected by Default
	 *
	 */
	public SLPPage verifyCheckBoxNotSelectedByDefault() {
		try {
			Assert.assertFalse(checkBoxInStoreLocatorPage.isSelected(), "Check Box is Selected");
			Reporter.log("Check Box is Not Selected");
		} catch (Exception e) {
			Assert.fail("Failed to see unselected Check Box");
		}
		return this;
	}

	/**
	 * MessageUnderCheckBox
	 */
	public SLPPage verifyMessageUnderCheckBoxMessageDetailsInStoreLocatorPage() {
		try {
			String displayedMessage = checkBoxMessage.getText();
			Assert.assertTrue(displayedMessage.contains("Only show"));
			Reporter.log("only show products with all products in stock is not displayed");
		} catch (Exception e) {
			Assert.fail("only show products with all products in stock is not displayed");
		}
		return this;
	}

	/**
	 * verify Description of the checkbox
	 *
	 */
	public SLPPage verifyDefaultStateOfCheckBox() {
		try {
			waitFor(ExpectedConditions.visibilityOf(checkBoxStatus), 60);
			Assert.assertTrue(checkBoxStatus.isDisplayed(), "Checkbox status is unchecked by defaulty");
		} catch (Exception e) {
			Assert.fail("Failed to verify check box status ");
		}
		return this;
	}

	/**
	 * Select second Filtered stores
	 */
	public SLPPage selectSecondFilteredStore() {
		try {
			waitFor(ExpectedConditions.visibilityOfAllElements(storeListOnSLP), 60);
			clickElementWithJavaScript(storeListOnSLP.get(1));
			Reporter.log("Store selected successfully.");
		} catch (Exception e) {
			Assert.fail("Unable to select Store from the list");
		}
		return this;
	}

	/**
	 * verify StoreList On SLP from SaveCart Confirmation page
	 *
	 */
	public SLPPage verifyStoreListOnSLP() {
		try {
			checkPageIsReady();
			for (WebElement store : storeListOnSLP) {
				Assert.assertTrue(store.isDisplayed(), "Store list is not displayed on SLP");
			}
			Reporter.log("Store list is displayed on SLP");
		} catch (Exception e) {
			Assert.fail("Failed to verify StoreList On SLP from SaveCart Confirmation page");
		}
		return this;
	}

	/**
	 * Verify Search Box on Map
	 *
	 */
	public SLPPage verifySearchThisAreaBoxOnMapNotDisplayed() {
		try {
			waitForSpinnerInvisibility();
			Assert.assertFalse(isElementDisplayed(searchCTAOnMap), "Search Box on Map is displayed");
			Reporter.log("Search Box on Map is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed not to display Search Box on Map");
		}
		return this;
	}

	/**
	 * Click on Show/Hide Items for selected store
	 *
	 */
	public SLPPage clickShowHideItemsOnSLP() {
		try {
			if (itemsInStockTextOnStoreListPage.get(0).getText().equalsIgnoreCase("Contact store for availability")) {
				Reporter.log("Items are out of stock");
			} else {
				waitForSpinnerInvisibility();
				clickElementWithJavaScript(showHideItemsCTAOnSLP);
				Reporter.log("Show Hide button clicked");
			}

		} catch (Exception e) {
			Assert.fail("Failed to click Show Hide CTA");
		}
		return this;
	}

	/**
	 * Verify display of Show Hide CTA on Store Details Modal on Cart page
	 *
	 */
	public SLPPage verifyShowHideCTADisplayedOnSLPCartPage() {
		try {
			waitForSpinnerInvisibility();
			if (itemsInStockTextOnStoreListPage.get(0).getText().equalsIgnoreCase("Contact store for availability")) {
				Reporter.log("Items are out of stock");
			} else {
				waitFor(ExpectedConditions.visibilityOf(showHideItemsCTAOnSLP), 60);
				Assert.assertTrue(isElementDisplayed(showHideItemsCTAOnSLP), "Show Hide CTA is not Displayed on SLP");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display Show Hide CTA on SLP");
		}
		return this;
	}

	/**
	 * Verify Store name in filtered Search results
	 *
	 */
	public SLPPage verifySelectedStoreInFilteredSearch(String storeName) {
		try {
			checkPageIsReady();
			Assert.assertTrue(storeNameInFilteredSearch.getText().equals(storeName),
					"Selected store is not displaying");
			Reporter.log("Store name value successfully verified");
		} catch (Exception e) {
			Assert.fail("Unable to verify Store name value");
		}
		return this;
	}

	/**
	 * To verify item list on clicking show Items CTA on Store List page
	 *
	 */
	public SLPPage verifyItemListonClickingShowItemsCTAOnStoreListPage() {
		try {
			if (itemsInStockTextOnStoreListPage.get(0).getText().equalsIgnoreCase("Contact store for availability")) {
				Reporter.log("Items are out of stock");
			} else {
				waitForSpinnerInvisibility();
				waitFor(ExpectedConditions.visibilityOfAllElements(showItemsAreDisplayedOnStoreListPage), 60);
				Assert.assertTrue(showItemsAreDisplayedOnStoreListPage.get(0).isDisplayed(),
						"Show items are not displayed on Store List Page");
				Reporter.log("Show items are displayed on Store List Page");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Items List");
		}
		return this;
	}

	/**
	 * Validate Store map page in Store locator list modal popup
	 *
	 */
	public SLPPage verifyStoreMapPage() {
		try {
			getDriver().switchTo().frame("pccIFrame");
			Assert.assertTrue(storeLocatorMap.isDisplayed(), "Map is not displaying beside store list");
			getDriver().switchTo().parentFrame();
			Reporter.log("Map is displaying beside store list");
		} catch (Exception e) {
			Assert.fail("Failed to verify map page");
		}
		return this;
	}

	/**
	 * Click Zoom In Button on Map
	 *
	 */
	public SLPPage clickZoomInButtonOnMap() {
		try {
			waitForSpinnerInvisibility();
			getDriver().switchTo().frame("pccIFrame");
			waitFor(ExpectedConditions.elementToBeClickable(zoomInButtonOnMap), 60);
			zoomInButtonOnMap.click();
			getDriver().switchTo().parentFrame();
			Reporter.log("Map Zoom In button is clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click on Map Zoom In button");
		}
		return this;
	}

	/**
	 * Verify Appointment Cta in Store locator map
	 *
	 */
	public SLPPage verifyAppointmentCTA() {
		try {
			waitFor(ExpectedConditions.visibilityOf(inventoryStatusInStoreModalPopup.get(0)), 60);
			Assert.assertTrue(appointmentInStoreListModal.get(0).isDisplayed(), "Appointment Cta is not displaying");

			Reporter.log("Appointment Cta is  displaying");

		} catch (Exception e) {
			Assert.fail("Failed to verify verify Appointment Cta");
		}
		return this;
	}

	/**
	 * Click Appointment Cta in Store locator map
	 *
	 */
	public SLPPage clickAppointmentCTA() {
		try {
			appointmentInStoreListModal.get(0).click();
			Reporter.log("Appointment Cta is  clicked");
		} catch (Exception e) {
			Assert.fail("Failed to click Appointment Cta");
		}
		return this;
	}

	/**
	 * To verify 'items in stock' text on Store List page
	 *
	 */
	public SLPPage verifyItemsInStockTextOnStoreListPage() {
		try {
			waitFor(ExpectedConditions.visibilityOfAllElements(itemsInStockTextOnStoreListPage), 60);
			for (WebElement text : itemsInStockTextOnStoreListPage) {
				Assert.assertTrue(text.isDisplayed(), "Inventory Status text is Not displayed");
				Reporter.log("Inventory status text is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to display Inventory status text");
		}
		return this;
	}

	/**
	 * verify Store locator map
	 */
	public SLPPage verifySelectedStore(String storeName) {
		try {
			String displayedStore = null;
			for (WebElement name : storesInStoreListPage) {
				displayedStore = name.getText();
				Assert.assertTrue(displayedStore.equals(storeName), "Selected store is not displaying");
				break;
			}
			Reporter.log("Selected store is  displaying");
		} catch (Exception e) {
			Assert.fail("Failed to verify selected store");
		}
		return this;
	}

	/**
	 * Verify Hide Items CTA is Displayed
	 */
	public SLPPage verifyhideItemsCTAOnStoreListPage() {
		try {
			if (itemsInStockTextOnStoreListPage.get(0).getText().equalsIgnoreCase("Contact store for availability")) {
				Reporter.log("Items are out of stock");
			} else {
				waitFor(ExpectedConditions.visibilityOf(hideItemsCTAOnStoreListPage), 60);
				Assert.assertTrue(hideItemsCTAOnStoreListPage.isDisplayed(), "Hide Items CTA is not displaying");
				Reporter.log("Hide Items CTA  is displayed");
			}
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to display Hide Items CTA");
		}
		return this;
	}

	/**
	 * Verify Search Box on Map
	 *
	 */
	public SLPPage verifySearchThisAreaBoxOnMap() {
		try {
			waitFor(ExpectedConditions.visibilityOf(searchCTAOnMap), 60);
			Assert.assertTrue(searchCTAOnMap.isDisplayed(), "Search Box on Map is not displayed");
			Reporter.log("Search Box on Map is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Search Box on Map");
		}
		return this;
	}

	/**
	 * Verify Store Locator list page
	 *
	 */
	public String selectAnyStore() {
		String storeName = null;
		try {
			waitForSpinnerInvisibility();
			checkPageIsReady();
			for (WebElement store : storeNameOnSLPList) {
				if (store.isDisplayed()) {
					storeName = store.getText();
					clickElementWithJavaScript(store);
					break;
				}
			}
			Reporter.log("Store is not selected");
		} catch (Exception e) {
			Assert.fail("Store is not selected");
		}
		return storeName;
	}

	/**
	 * Verify get In Line Cta
	 *
	 */
	public SLPPage verifyGetInLineCTA() {
		try {
			Calendar now = Calendar.getInstance(TimeZone.getTimeZone("PST"));
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			sdf.setTimeZone(TimeZone.getTimeZone("PST"));
			String currentTime = sdf.format(now.getTime());

			String storeTimigs = storeTimings.getText();
			String storeTimigsArray[] = storeTimigs.split("-");

			String startTime = storeTimigsArray[0].replaceAll("[^0-9?!\\.-:]", "");
			String endTime = storeTimigsArray[1].replaceAll("[^0-9?!\\.-:]", "");
			String currentTimestr = currentTime.replaceAll("[^0-9?!\\.]", "");

			startTime = manageStoreStartTime(startTime);
			endTime = manageStoreClosingTime(endTime);

			int storeOpeningTime = Integer.parseInt(startTime);
			int storeClosingTime = Integer.parseInt(endTime);
			int localTime = Integer.parseInt(currentTimestr);

			if (localTime >= storeOpeningTime && localTime <= storeClosingTime) {
				Assert.assertTrue(getInLineCtaInStoreListModal.get(0).isDisplayed(),
						"Get In Line Cta is not displaying");
				Reporter.log("Verified Get In Line Cta");
			} else {
				Reporter.log("Store is closed and Get In Line CTA is disabled.");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Get In Line Cta");
		}
		return this;
	}

	/**
	 * Verify Call us Cta
	 *
	 */

	public SLPPage verifycallUsCta() {
		try {
			waitFor(ExpectedConditions.visibilityOfAllElements(callUsCtaInStoreListModal), 60);
			Assert.assertTrue(callUsCtaInStoreListModal.get(1).isDisplayed(), "Call us Cta is not displaying");
			Reporter.log("Call us Cta is  displaying");
		} catch (Exception e) {
			Assert.fail("Failed to verify Call US Cta");
		}
		return this;
	}

	/**
	 * Click on Get In Line Button
	 * 
	 * @return
	 */
	public SLPPage clickOnGetInLineCTA() {
		try {
			getInLineCtaInStoreListModal.get(0).isEnabled();
			clickElementWithJavaScript(getInLineCtaInStoreListModal.get(0));
			Reporter.log("'Get in line' CTA was clicked successfully.");
		} catch (Exception e) {
			Assert.fail("Failed to click 'Get in line' CTA");
		}
		return this;
	}

	/**
	 * Verify Save Cart Cta on SLP
	 *
	 */
	public SLPPage verifySaveCartCTAonSLP() {
		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOfAllElements(saveCartLinkSLP), 60);
			Assert.assertTrue(saveCartLinkSLP.get(0).isDisplayed(), "Save Cart Link is not displaying");
			Reporter.log("Save Cart Link is  displaying");
		} catch (Exception e) {
			Assert.fail("Failed to verify Save Cart Link");
		}
		return this;
	}

	/**
	 * search for a location where no inventory available
	 */
	public SLPPage searchForALocationWithNoInventoryAndVerifyMessage(String zipCode) {
		try {
			searchField.sendKeys(zipCode);
			checkBoxOnSaveYourCartModel.click();
			clickElementWithJavaScript(clickSearchIcon);
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(noStoreInventoryMessage), 60);
			Assert.assertTrue(noStoreInventoryMessage.isDisplayed(),
					"No stores nearby with inventory message is not displayed");
			Reporter.log("No stores nearby with inventory message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify No stores nearby with inventory message");
		}
		return this;
	}

	/**
	 * search for a location
	 */
	public SLPPage searchForALocation(String zipCode) {
		try {
			searchField.sendKeys(zipCode);
			clickElementWithJavaScript(clickSearchIcon);
			waitForSpinnerInvisibility();
			Reporter.log("Location is searched in SLP page");
		} catch (Exception e) {
			Assert.fail("Failed to search for a location ");
		}
		return this;
	}

	/**
	 * Click Go back CTA
	 */
	public SLPPage clickGobackCTA() {
		try {
			goBack.click();
			waitForSpinnerInvisibility();
			Reporter.log("Clicked on Go back CTA in SLP page");
		} catch (Exception e) {
			Assert.fail("Failed to Click Go back CTA");
		}
		return this;
	}

	/**
	 * To verify and select checkbox on Save Your Cart Model
	 *
	 */
	public SLPPage verifyAndSelectCheckBoxOnSaveYourCartModel() {
		try {
			Assert.assertTrue(isElementDisplayed(checkBoxOnSaveYourCartModel),
					"Check box on save Cart Model is not displayed");
			clickElementWithJavaScript(checkBoxOnSaveYourCartModel);
			Reporter.log("Check box on save Cart Model is displayed and Selected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Check box on save Cart Model.");
		}
		return this;
	}

	/**
	 * Select Check Box in Store Locator Page
	 *
	 */
	public SLPPage selectCheckBoxInStoreLocatorPage() {
		try {
			checkBoxInStoreLocatorPage.click();
			Reporter.log("Check Box is Selected");
			checkPageIsReady();
			if (isElementDisplayed(noStoreInventoryMessage)) {
				checkBoxInStoreLocatorPage.click();
			}

		} catch (Exception e) {
			Assert.fail("Failed to select Store Locator Check Box");
		}
		return this;
	}

	/**
	 * Validate Checkbox on Save Cart Modal
	 */
	public SLPPage verifyCheckBoxOnSaveCartModal() {
		try {
			waitforSpinner();
			Assert.assertTrue(checkBoxOnSaveYourCartModel.isDisplayed(), "Check box is not displaying");
			Reporter.log("Check box is displaying");
		} catch (NoSuchElementException e) {
			Assert.fail("Failed to verify Check box");
		}
		return this;
	}

	/**
	 * Validate Inventory status message in store list modal popup
	 *
	 */
	public SLPPage verifyInventoryStatusInStoreLocatorListPage() {
		try {
			if (isElementDisplayed(noStoreInventoryMessage)) {
				Reporter.log("There are no stores with all items in stock");
			} else {
				Assert.assertTrue(inventoryStatusInStoreModalPopup.get(0).isDisplayed(),
						"Inventory status message is not displaying");
				Reporter.log("Inventory status message is  displaying");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify inventory status message");
		}
		return this;
	}

	/**
	 * Verify Check Box Message in Store Locator Page is Displayed
	 *
	 */
	public SLPPage verifyCheckBoxMessageInStoreLocatorPageisDisplayed() {
		try {
			Assert.assertTrue(checkBoxMessage.isDisplayed(), "Check Box Message is not displayed");
			Reporter.log("Check Box Message is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Display Store Locator Check Box Message");
		}
		return this;
	}

	/**
	 * click on Appointment Form modal ok
	 */
	public SLPPage clickOnAppointmentTroubleConnectingAlertOk() {
		try {
			waitForSpinnerInvisibility();
			appointmentTroubleConnectingAlertOk.click();
			Reporter.log("Clicked on Appointment Form modal ok button");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay Appointment Form modal.");
		}
		return this;
	}

	/**
	 * Verify Appointment Trouble Connecting Alert Modal pop up
	 */
	public boolean verifyAppointmentTroubleConnectingAlert() {
		boolean isElement = false;
		try {
			isElement = appointmentTroubleConnectingAlert.isEmpty();
			Reporter.log("Appointment Trouble Connecting Modal pop up is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay Appointment Trouble Connecting Alert.");
		}
		return isElement;
	}

	/**
	 * Verify date Time Modal pop up
	 */
	public SLPPage verifyDateTimeModalModal() {
		try {
			Assert.assertTrue(dateTimeModal.isDisplayed(), "Date and Time Modal pop up is not displayed");
			Reporter.log("Date and Time Modal pop up is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay Date and Time Modal pop up.");
		}
		return this;
	}

	/**
	 * Select Time
	 * 
	 * @param option
	 */
	public SLPPage selectTime(String option) {
		try {
			waitForSpinnerInvisibility();
			for (WebElement webElement : timeList) {
				if (webElement.getText().equalsIgnoreCase(option)) {
					webElement.click();
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Time List Element not clicked");
		}
		return this;
	}

	/**
	 * Click on Next CTA
	 * 
	 */
	public SLPPage clickNextCTA() {
		try {
			waitForSpinnerInvisibility();
			clickElementWithJavaScript(nextCTA);
			Reporter.log("Clicked on Next button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Next Button");
		}
		return this;
	}

	/**
	 * Verify Appointment Form modal
	 */
	public SLPPage verifyAppointmentForm() {
		try {
			waitForSpinnerInvisibility();
			Assert.assertTrue(appointmentForm.isDisplayed(), "Appointment Form modal is not displayed");
			Reporter.log("Appointment Form modal is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to dipslay Appointment Form modal.");
		}
		return this;
	}

	/**
	 * Enter the Appointment Details
	 * 
	 * @param
	 * @return
	 */
	public SLPPage selectAppointmentDetails(TMNGData tMNGData) {
		try {
			waitForSpinnerInvisibility();
			setFirstName(tMNGData.getFirstName());
			setLastName(tMNGData.getLastName());
			setEmail(tMNGData.getEmail());
			setPhoneNumber(tMNGData.getPhoneNumber());
			selectReasonOption(tMNGData.getReason());
			clickOnEnglishRadioButton();
			clickOnUserConsentCheckbox();
		} catch (Exception e) {
			Assert.fail("Options in Trade In Another Device Page are not Displayed to Choose");
		}
		return this;
	}

	/**
	 * Set First Name
	 * 
	 * @param firstNameValue
	 */
	public SLPPage setFirstName(String firstNameValue) {
		try {
			waitForSpinnerInvisibility();
			sendTextData(firstName, firstNameValue);
			firstName.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			Assert.fail("Failed to send data to FirtName");
		}
		return this;
	}

	/**
	 * Set Last Name
	 * 
	 * @param lastNameValue
	 */
	public SLPPage setLastName(String lastNameValue) {
		try {
			waitForSpinnerInvisibility();
			sendTextData(lastName, lastNameValue);
			lastName.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			Assert.fail("Failed to send data to LastName");
		}
		return this;
	}

	/**
	 * Set Email
	 * 
	 * @param email
	 */
	public SLPPage setEmail(String email) {
		try {
			waitForSpinnerInvisibility();
			sendTextData(emailId, email);
			emailId.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			Assert.fail("Failed to send data to Email");
		}
		return this;
	}

	/**
	 * Set Phone Number
	 * 
	 * @param phoneNumber
	 */
	public SLPPage setPhoneNumber(String phoneNumber) {
		try {
			sendTextData(phoneNo, phoneNumber);
			phoneNo.sendKeys(Keys.ENTER);
		} catch (Exception e) {
			Assert.fail("Failed to send data to PhoneNumber");
		}
		return this;
	}

	/**
	 * Click on English Radio Button
	 *
	 */
	public SLPPage clickOnEnglishRadioButton() {
		try {
			clickElementWithJavaScript(englishRadioButton);
			Reporter.log("Clicked on English radio button");
		} catch (Exception e) {
			Assert.fail("Failed to click English radio button");
		}
		return this;
	}

	/**
	 * Click on User Consent Checkbox
	 *
	 */
	public SLPPage clickOnUserConsentCheckbox() {
		try {
			clickElementWithJavaScript(userConsentCheckbox);
			Reporter.log("Clicked on user Consent Checkbox");
		} catch (Exception e) {
			Assert.fail("Failed to click User Consent Checkbox.");
		}
		return this;
	}

	/**
	 * Select Reason
	 * 
	 * @param reason
	 */
	public SLPPage selectReasonOption(String reason) {
		try {
			waitForSpinnerInvisibility();
			clickElementWithJavaScript(reasonTextBox);
			for (WebElement webElement : selectReason) {
				if (webElement.getText().equals(reason)) {
					webElement.click();
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Reason Options Field is not Displayed");
		}
		return this;
	}

	/**
	 * Click on Schedule Appointment Button
	 * 
	 * @return
	 */
	public SLPPage clickOnScheduleAppointmentButton() {
		try {
			clickElementWithJavaScript(appointmentButton);
			Reporter.log("Clicked on Schedule Appointment Button");
		} catch (Exception e) {
			Assert.fail("Failed to click Schedule Appointment Button.");
		}
		return this;
	}

	/**
	 * Verify You have already have appointment pop up
	 */
	public boolean verifyAlreadyHaveAnAppointment() {
		boolean isPopUpdisplayed = false;
		try {
			checkPageIsReady();
			if (keepOriginalAppointment.isEmpty()) {
				Reporter.log("Failed to dipslay You have already have appointment pop up.");
			} else {
				isPopUpdisplayed = keepOriginalAppointment.get(0).isDisplayed();
				Reporter.log("You have already have appointment Modal pop up is displayed");
			}
		} catch (Exception e) {
			Assert.fail("Failed to dipslay You have already have appointment pop up.");
		}
		return isPopUpdisplayed;
	}

	/**
	 * Click on Schedule Appointment Button
	 *
	 */
	public SLPPage clickKeepOriginalAppointment() {
		try {
			clickElementWithJavaScript(keepOriginalAppointment.get(0));
			checkPageIsReady();
			Reporter.log("Clicked on Keep original Appointment Button");
		} catch (Exception e) {
			Assert.fail("Failed to click Keep original Appointment Button.");
		}
		return this;
	}

	/**
	 * Verify appointment Confirmantion Text
	 *
	 */
	public SLPPage verifyappointmentConfirmantionText() {
		try {
			waitFor(ExpectedConditions.visibilityOf(appointmentConfirmantionText), 60);
			Assert.assertTrue(appointmentConfirmantionText.isDisplayed(),
					"appointment Confirmantion Text is not displayed");
			Reporter.log("appointment Confirmantion Text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display appointment Confirmantion Text");
		}
		return this;
	}

	/**
	 * Verify Scheduled Date And Time
	 *
	 */
	public SLPPage verifyScheduledDateAndTime(TMNGData tMNGData) {
		try {
			Assert.assertTrue(scheduledAppointmentDetails.get(0).isDisplayed(),
					"Scheduled Date And Time is not displayed");
			Assert.assertTrue(scheduledAppointmentDetails.get(0).getText().contains(tMNGData.getAppointmentDate()),
					"Date is not verified");
			Assert.assertTrue(scheduledAppointmentDetails.get(0).getText().contains(tMNGData.getAppointmentTime()),
					"Time is not verified");
			Reporter.log("Scheduled Date And Time is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Scheduled Date And Time");
		}
		return this;
	}

	/**
	 * Verify Inventory Status on Store Details Modal on Cart page
	 *
	 */
	public boolean verifyshowItemsInStockSlp() {
		try {
			if (isElementDisplayed(showHideItemsCTAOnSLP))
				return true;
			else
				return false;
		} catch (Exception e) {
			Assert.fail("Failed to display 'Show items' in SLP");
		}
		return false;
	}
}
