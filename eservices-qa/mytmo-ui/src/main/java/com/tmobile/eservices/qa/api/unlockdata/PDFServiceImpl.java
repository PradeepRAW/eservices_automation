package com.tmobile.eservices.qa.api.unlockdata;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;

/**
 * This class handles to perform operations in pdfs
 * @author blakshminarayana
 *
 */
public class PDFServiceImpl implements PDFService {
	private static final Logger logger =LoggerFactory.getLogger(PDFServiceImpl.class);

	/**
	 *This Method is used to save PDF in particular location (localOr sauceLab) 
	 *@param pdfURL
	 *@param filename
	 */
	@Override
	public void savePDF(String pdfURL,String filename) {
		logger.info("savePdf method called in PDFServiceImpl");
		try {
			URL url = new URL(pdfURL);
			File file = new File(filename);
			FileUtils.copyURLToFile(url,file);
		} catch (IOException ex) {
			throw new ServiceException("Unable to save Pdf in specific Location", ex);
		}
	}

	/** This Method to validate the content in pdf file
	 *@param pdfURL
	 *@param filename
	 */
	@Override
	public boolean validateContentInPDF(String filename,String content) {
		logger.info("validateContentInPDF method called in PDFServiceImpl");
		boolean verifyContent = false;
		try {
			PdfReader pdfReader = new PdfReader(filename);
			PdfReaderContentParser parser = new PdfReaderContentParser(pdfReader);
			for (int page = 1; page <= pdfReader.getNumberOfPages(); page++) {
				TextExtractionStrategy  extractionStrategy = parser.processContent(page,  new SimpleTextExtractionStrategy());
				String resultantText = extractionStrategy.getResultantText();
				if(resultantText.contains(content)){
					verifyContent = true;
				}
			}
		} catch (IOException ex) {
			throw new ServiceException("Unable to read content in pdf", ex);
		}
		return verifyContent;
	}
}
