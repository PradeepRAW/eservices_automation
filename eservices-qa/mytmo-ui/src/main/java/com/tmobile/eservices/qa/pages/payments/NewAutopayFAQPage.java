package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Sreekanth
 *
 */
public class NewAutopayFAQPage extends CommonPage {

	@FindBy(xpath = "//div[text()='Frequently Asked Questions']")
	private WebElement faqheader;

	@FindBy(id = "acceptButton")
	private WebElement Backbutton;

	private final String pageUrl = "/autopay/faq";

//	private final String pageUrlPA = "/tnc";

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public NewAutopayFAQPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public NewAutopayFAQPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public NewAutopayFAQPage verifyPageLoaded() {
		try {
			checkPageIsReady();

			waitFor(ExpectedConditions.visibilityOf(faqheader));
			waitFor(ExpectedConditions.visibilityOf(Backbutton));
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Terms and Conditions page not laoded");
		}
		return this;
	}

	public NewAutopayFAQPage clickBackButton() {
		try {
			Backbutton.click();
			Reporter.log("Clicked on Back Button");
		} catch (Exception e) {
			Verify.fail("Fail to click on Back Button");
		}
		return this;
	}

}
