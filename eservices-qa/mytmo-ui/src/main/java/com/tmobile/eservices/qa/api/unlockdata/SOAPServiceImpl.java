package com.tmobile.eservices.qa.api.unlockdata;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.stream.StreamSource;
import org.jdom2.Document;
import org.apache.commons.io.IOUtils;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is class is responsible for all soap calls that can be used by multiple
 * services like agency, coverage.
 * 
 * @author blakshminarayana
 *
 */
public class SOAPServiceImpl implements SOAPService {
	private static final Logger logger = LoggerFactory.getLogger(SOAPServiceImpl.class);

	/**
	 * This method is used to construct the soap request and returns SAOP message
	 * 
	 * @param document
	 * @param soapAction
	 * @return SOAPMessage
	 */
	@SuppressWarnings("deprecation")
	@Override
	public SOAPMessage createRequest(Document document, String soapAction) {
		logger.info("createRequest method called in SOAPHandlerImpl");
		SOAPMessage message = null;
		try {
			message = MessageFactory.newInstance().createMessage();
			message.getMimeHeaders().addHeader("SOAPAction", "\"" + soapAction + "\"");
			SOAPPart soapPart = message.getSOAPPart();
			String outputString = new XMLOutputter().outputString(document);
			InputStream inputStream = IOUtils.toInputStream(outputString);
			soapPart.setContent(new StreamSource(inputStream));
			message.saveChanges();
		} catch (SOAPException se) {
			logger.error("Exception while creating soap Meassge from Stream {}", se);
			throw new ServiceException("Exception raised while creating soap request", se);
		}
		return message;
	}

	/**
	 * This method to invoke the all soap services
	 * 
	 * @param endpoint
	 * @param SOAP     message Request
	 */
	@Override
	public SOAPMessage invokeService(String endpoint, SOAPMessage request) {
		logger.info("invokeService method called in SOAPHandlerImpl");
		HttpsURLConnection httpsConnection = null;
		SOAPConnection soapConnection = null;
		SOAPMessage response = null;
		try {
			SSLContext sslContext = SSLContext.getInstance("SSL");
			TrustManager[] trustAll = new TrustManager[] { new TrustAllCertificates() };
			sslContext.init(null, trustAll, new java.security.SecureRandom());

			// Set trust all certificates context to HttpsURLConnection
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

			// Open HTTPS connection
			URL url = new URL(endpoint);
			httpsConnection = (HttpsURLConnection) url.openConnection();
			// Trust all hosts
			httpsConnection.setHostnameVerifier(new TrustAllHosts());
			httpsConnection.connect();
			if (httpsConnection.getURL() != null) {
				logger.info("Connected to the server{}", httpsConnection.getURL());
			} else {
				logger.info("HTTPS Connection Failed");
			}
			soapConnection = SOAPConnectionFactory.newInstance().createConnection();
			response = soapConnection.call(request, endpoint);
		} catch (SOAPException | IOException | NoSuchAlgorithmException | KeyManagementException ex) {
			logger.info("send request failed while calling the soap service{}", ex);
			throw new ServiceException("Unable to connect to the server ", ex);
		} finally {
			if (httpsConnection != null) {
				httpsConnection.disconnect();
			}
		}
		return response;
	}

	private static class TrustAllCertificates implements X509TrustManager {

		/**
		 * This method to check the trusted issuers or not
		 */
		@Override
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return Collections.emptyList().toArray(new X509Certificate[0]);
		}

		/**
		 * This method is verifying checkClientTrusted This is override method which is
		 * not implemented
		 */
		@Override
		public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
				throws CertificateException {
			// override method no need to write any logic here
		}

		/**
		 * This is override method which is not implemented
		 */
		@Override
		public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
				throws CertificateException {
			// override method no need to write any logic here
		}

	}

	private static class TrustAllHosts implements HostnameVerifier {
		@Override
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	}

}
