package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class PaymentManagerApiV1 extends ApiCommonLib{
	
	/***
	 * Summary: This API is used to get scheduled payment details through PaymentManager API
	 * Headers:Authorization,
	 * 	-transactionId,
	 * 	-transactionType,
	 * 	-applicationId,
	 * 	-channelId,(This indicates whether the original request came from Mobile / web / retail / mw  etc)
	 * 	-correlationId, 
	 * 	-clientId,Unique identifier for the client (could be e-servicesUI ,MWSAPConsumer etc )
	 * 	-transactionBusinessKey,
	 * 	-transactionBusinessKeyType,(Indicates the kind of transaction eg AAL , CIHU , inventoryfeed etc)
	 * 	-usn,required=true
	 * 	-phoneNumber,required=true
	 *  -Content-Type
	 *  -activityId 
	 *  -application_userid 
	 *  
	 *  
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	
	
    private Map<String, String> buildPaymentManagerHeader(ApiTestData apiTestData) throws Exception {
    	//clearCounters(apiTestData);
    	final String oauth = checkAndGetAccessToken();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization",oauth);
		headers.put("Content-Type", "application/json");
		headers.put("ban",apiTestData.getBan());
		headers.put("msisdn",apiTestData.getMsisdn());	
		return headers;
	}
    
    public Response schedulePaymentDetails(ApiTestData apiTestData) throws Exception{
    	Map<String, String> headers = buildPaymentManagerHeader(apiTestData);

    	RestService service = new RestService("", eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("v1/paymentmanager/schedulepaymentdetails",RestCallType.GET);
         System.out.println(response.asString());
         return response;
    }
    
    

    public Response SubcriberPaymentDetails(ApiTestData apiTestData) throws Exception{
    	Map<String, String> headers = buildPaymentManagerHeader(apiTestData);
    	headers.put("isSubcriber","true");
		headers.put("isSedona","true");
		headers.put("isEasyPay","true");

    	RestService service = new RestService("", eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("v1/paymentmanager/subcriberpaymentdetails",RestCallType.GET);
         System.out.println(response.asString());
         return response;
    }
    
    public Response PaymentarrangementDetails(ApiTestData apiTestData) throws Exception{
    	Map<String, String> headers = buildPaymentManagerHeader(apiTestData);
    	headers.put("isSubcriber","true");
		headers.put("isSedona","false");
		headers.put("isEasyPay","true");

    	RestService service = new RestService("", eos_base_url);
         RequestSpecBuilder reqSpec = service.getRequestbuilder();
 			reqSpec.addHeaders(headers);
 			service.setRequestSpecBuilder(reqSpec);
         Response response = service.callService("v1/paymentmanager/paymentarrangementdetails",RestCallType.GET);
         System.out.println(response.asString());
         return response;
    }
    
    public Response autopayLandingDetails(ApiTestData apiTestData,Map<String,String> tokenMap) throws Exception{
    	Map<String, String> headers = buildPaymentManagerHeader(apiTestData, tokenMap);
    	headers.put("isSubcriber","true");
		headers.put("isSedona","false");
		headers.put("isEasyPay","true");
    	//String updatedRequest = prepareRequestParam("", null);
		//logRequest(updatedRequest, "PaymentManager-autopaylandingdetails");
    	String resourceURL = "v1/paymentmanager/autopaylandingdetails";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
    }

	private Map<String, String> buildPaymentManagerHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization","Bearer "+checkAndGetJWTToken(apiTestData, tokenMap).get("jwtToken"));
		headers.put("Content-Type","application/json");
		headers.put("MSISDN",apiTestData.getMsisdn());
		headers.put("access_token","Bearer "+checkAndGetJWTToken(apiTestData, tokenMap).get("accessToken"));
		headers.put("applicationId","MYTMO");
		headers.put("ban",apiTestData.getBan());
		headers.put("cache-control","no-cache");
		headers.put("channelId","DESKTOP");
		headers.put("channelVersion","");
		headers.put("clientId","eService");
		headers.put("transactionId","cfbb77bb-e205-4627-a0c3-c01ab1425e4b");
		headers.put("usn","1111");
		return headers;
	}

}
