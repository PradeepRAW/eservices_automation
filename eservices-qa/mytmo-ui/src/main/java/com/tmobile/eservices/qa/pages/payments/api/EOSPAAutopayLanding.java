package com.tmobile.eservices.qa.pages.payments.api;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.response.Response;

public class EOSPAAutopayLanding extends EOSCommonLib {

	public Response getResponsePAAPLanding(String alist[]) throws Exception {
		Response response = null;
		// String alist[]=getauthorizationandregisterinapigee(misdin,pwd);
		if (alist != null) {

			RestService restService = new RestService("", "https://stage2.eos.corporate.t-mobile.com");
			// RestService restService = new RestService("", eep.get(environment));
			restService.setContentType("application/json");

			restService.addHeader("ban", alist[3]);
			restService.addHeader("msisdn", alist[0]);

			restService.addHeader("usn", "usn_100");

			restService.addHeader("Authorization", "Bearer " + alist[2]);
			restService.addHeader("access_token", "Bearer " + alist[1]);

			response = restService.callService("v1/paymentmanager/autopaylandingdetails", RestCallType.GET);

		}

		return response;
	}

}
