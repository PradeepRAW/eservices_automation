package com.tmobile.eservices.qa.pages.payments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindAll;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author pshiva
 *
 */
public class EipJodPage extends CommonPage {

	@FindBy(css = "div.Display3 div")
	private List<WebElement> pageHeader;

	@FindBy(css = "div.col-12.Display3.text-center div")
	private WebElement header;

	private final String pageUrl = "eipjod";

	@FindBy(css = "div.H6-heading")
	private WebElement docAndRecieptsHeader;

	@FindBy(css = "div.body.px-lg-2")
	private WebElement docAndRecieptsText;

	@FindBy(css = "div.body.px-lg-2 a")
	private WebElement accountActivitiesLink;

	@FindBy(css = "label[for='activeTab']")
	private WebElement activeSectionTab;

	@FindBy(css = "label[for='completedTab']")
	private WebElement completedSectionTab;

	@FindBy(css = "p.H6-heading")
	private WebElement eipBladeHeader;

	@FindBy(css = "div[id^='eipCompleted'] span.arrow-down-disable")
	private List<WebElement> completedEipDownArrow;

	@FindBy(css = "div[id^='leaseCompleted'] span.arrow-down-disable")
	private List<WebElement> completedJodDownArrow;

	@FindBy(css = "span.arrow-up")
	private WebElement completedEipUpArrow;

	@FindBy(css = "app-rollup-totals div span[class='body-bold black']")
	private WebElement monthlyPaymentsHeader;

	@FindBy(xpath = "//span[contains(text(),'Monthly payments')]/following-sibling::span[contains(@class,'body-bold black float-right')]")
	private WebElement monthlyPaymentsAmount;

	@FindBy(xpath = "//span[contains(text(),'Plan start date:')]/following-sibling::span")
	private List<WebElement> planStartDates;

	@FindBy(css = "span.body-bold.black.payment-size")
	private List<WebElement> activeEIPsRemainingAmount;

	@FindBy(css = "span.body.black.float-md-right")
	private List<WebElement> activeEIPsMonthlyAmount;

	@FindBy(linkText = "Plan balance")
	private WebElement planBalanceHeader;

	@FindBy(css = "div[id^='eipActive']")
	private List<WebElement> activeEIPPlans;

	@FindBy(css = "div[id^='leaseActive']")
	private List<WebElement> activeJODPlans;

	@FindBy(css = "div[id^='eipActive'] div.TMO-PROGRESS-BAR")
	private List<WebElement> progressBars;

	@FindBy(css = "div[id^='eipActive'] img")
	private List<WebElement> phoneImages;

	@FindBy(css = "div[id^='eipActive'] p.Display6")
	private List<WebElement> friendlyNames;

	@FindBy(css = "div[id^='eipActive'] p.legal")
	private List<WebElement> msisdns;

	@FindBy(css = "div[id^='eipActive'] p.body")
	private List<WebElement> deviceNames;

	@FindBy(css = "div[id^='eipActive'] span.body.pr-md-2.pr-1")
	private List<WebElement> remainingBal;

	@FindBy(css = "div[id^='eipActive'] span[class*='arrow-down']")
	private List<WebElement> activeEIPArrowDown;

	@FindBy(css = "div[id^='eipActive'] span.arrow-up.arrowup_rotation")
	private WebElement activeEIPArrowUp;

	@FindBy(css = "div[id^='eipActive'] span.arrow-up.arrowup_rotation")
	private List<WebElement> activeEIPArowUp;

	@FindAll({ @FindBy(css = "div[id^='eipActive'] div.accordian-in"),
			@FindBy(css = "div[id^='eipCompleted'] div.accordian-in") })
	private WebElement eipExpandedView;

	@FindBy(css = "div[id^='leaseActive'] div.accordian-in")
	private WebElement jodExpandedView;

	@FindBy(css = "div[id^='eipActive'] div.accordian-in button.PrimaryCTA")
	private WebElement makeAPaymentBtn;

	@FindBy(linkText = "See the payment estimator")
	private WebElement seeThePaymentEstimatorLink;

	@FindBy(css = "div.accordian-in span[class='body'],div.accordian-in span[class='body ']")
	private List<WebElement> activeEipExpandedViewHeaders;

	@FindBy(css = "div.accordian-in span.body.float-right")
	private List<WebElement> activeEipExpandedViewValues;

	private WebElement leaseDetailsAmount;

	private WebElement leaseDetailsHeader;

	@FindBy(xpath = "//a[contains(text(),'Plan balance')]//../span")
	private WebElement planBalanceAmount;

	@FindBy(linkText = "Plan balance")
	private WebElement planDetailsHeader;

	@FindBy(css = "#eipSection div.text-center.body")
	private WebElement noActiveEIPorJODtext;

	@FindBy(css = "#eipSection div button")
	private WebElement shopNowCTA;

	@FindBy(css = "div[id^='eipCompleted']")
	private List<WebElement> completedEIPPlans;

	@FindBy(css = "div[id^='leaseCompleted']")
	private List<WebElement> completedJODPlans;

	@FindBy(xpath = "//div[contains(text(),' Active EIP ')]//../div//div[@class='TMO-EIP-JOD-BLADE']/div//span[@class='body-bold ']']")
	private List<WebElement> remainingBalAmount;

	@FindBy(xpath = "//div[contains(text(),' Active EIP ')]//../div//div[@class='TMO-EIP-JOD-BLADE']/div//span[@class='body black']']")
	private List<WebElement> monthlyAmounts;

	@FindBy(css = "div.accordian-in span[class='body']")
	private List<WebElement> completedEipExpandedViewHeaders;

	@FindBy(css = "div.accordian-in span[class='body']")
	private List<WebElement> completedJODExpandedViewHeaders;

	@FindBy(css = "div.accordian-in span.body.float-right")
	private List<WebElement> completedEipExpandedViewValues;

	@FindBy(css = "div[id^=eipCompleted] img")
	private List<WebElement> completedEipDeviceImages;

	@FindBy(css = "div[id^=eipCompleted] p.Display6")
	private List<WebElement> completedEipFriendlyNames;

	@FindBy(css = "div[id^=eipCompleted] p.legal")
	private List<WebElement> completedEipMisdins;

	@FindBy(css = "div[id^=eipCompleted] p.body")
	private List<WebElement> completedEipDeviceNames;

	@FindBy(css = "div[id^=eipCompleted] span.body-bold")
	private List<WebElement> completedEipCompletedStatus;

	@FindBy(css = "div[id^='leaseCompleted'] img")
	private List<WebElement> completedJodDeviceImages;

	@FindBy(css = "div[id^='leaseCompleted'] p.Display6")
	private List<WebElement> completedJodFriendlyNames;

	@FindBy(css = "div[id^='leaseCompleted'] p.legal")
	private List<WebElement> completedJodMisdins;

	@FindBy(css = "div[id^='leaseCompleted'] p.body")
	private List<WebElement> completedJodDeviceNames;

	@FindBy(css = "div[id^='leaseCompleted'] span.body-bold")
	private List<WebElement> completedJodCompletedStatus;

	private List<String> expandedViewHeaders = new ArrayList<>(Arrays.asList("Payments remaining:", "Original price:",
			"Down payment:", "Plan start date:", "Plan status:", "EIP Plan ID:", "IMEI:"));

	@FindBy(xpath = "//span[contains(text(),'EIP Plan ID:')]/following-sibling::span")
	private List<WebElement> eipPlanId;

	@FindBy(css = "div[id^='leaseActive'] img")
	private List<WebElement> jodPhoneImages;

	@FindBy(css = "div[id^='leaseActive'] p.Display6")
	private List<WebElement> jodFriendlyNames;

	@FindBy(css = "div[id^='leaseActive'] p.legal")
	private List<WebElement> activeJODMsisdns;

	@FindBy(css = "div[id^='leaseActive'] p.body")
	private List<WebElement> activeJODDeviceNames;

	@FindBy(css = "div[id^='leaseActive'] span.body.pr-md-2.pr-1")
	private List<WebElement> activeJODRemainingPayments;

	@FindBy(css = "div[id^='leaseActive'] span.body-bold.black.payment-size")
	private List<WebElement> activeJODsRemainingPayments;

	@FindBy(css = "div[id^='leaseActive'] span.body.black.float-md-right")
	private List<WebElement> activeJODsMonthlyAmount;

	@FindBy(css = "div[id^='leaseActive'] div.TMO-PROGRESS-BAR")
	private List<WebElement> activeJODprogressBars;

	private List<String> expandedJODViewHeadersHardcoded = new ArrayList<>(
			Arrays.asList("Monthly payment amount:", "Total lease amount:", "Purchase option price:",
					"Lease start date:", "Lease status:", "Payments remaining:", "Plan ID:", "IMEI:"));

	@FindBy(css = "div[id^='leaseActive'] div.accordian-in span[class='body'],div[id^='leaseActive'] div.accordian-in span[class='body ']")
	private List<WebElement> activeJODExpandedViewHeaders;

	@FindBy(css = "div[id^='leaseActive'] div.accordian-in span.body.float-right")
	private List<WebElement> activeJODExpandedViewValues;

	@FindBy(css = "div[id^='leaseActive'] span.arrow-up")
	private WebElement activeJODArrowUp;

	@FindBy(css = "div[id^='leaseActive'] span[class*='arrow-down']")
	private List<WebElement> activeJODArrowDown;

	@FindBy(css = "div[id^='leaseActive_0'] span.arrow-up")
	private WebElement activeFirstJODArrowUp;

	@FindBy(css = "div.body.white.d-inline-block.padding-horizontal-xsmall")
	private WebElement jodNotificationRibbons;

	@FindBy(css = "button.full-btn-width.PrimaryCTA.w-100")
	private WebElement takeActionCTA;

	@FindBy(css = "p.body.black.padding-top-small-xs.padding-top-small-sm")
	private WebElement takeActionText;

	@FindBy(css = "div[id^='leaseActive'] p.padding-top-small-xs")
	private List<WebElement> poiptext;

	@FindBy(css = "span[aria-label='Collapsed Press enter to Expand']")
	private List<WebElement> ExpandArrowIcon;

	private By landingpageSpinner = By.cssSelector("div.circle");

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public EipJodPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public EipJodPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public EipJodPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitforSpinner();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(landingpageSpinner));
			verifyPageUrl();
			verifyEIPJODPageHeader();
		} catch (Exception e) {
			Assert.fail("EIP JOD - page not found.");
		}
		return this;
	}

	/**
	 * verify Documents and Receipts header and body text are displayed as
	 * expected
	 * 
	 * @param header
	 * @param bodyText
	 * @return
	 */
	public EipJodPage verifyDocAndRecieptsSection(String header, String bodyText) {
		try {
			docAndRecieptsHeader.isDisplayed();
			Assert.assertTrue(docAndRecieptsHeader.getText().equals(header),
					"Documents and Receipts Header is not displayed as expected");
			docAndRecieptsText.isDisplayed();
			// String bodyTextConcat =
			// docAndRecieptsText.getText().concat(accountActivitiesLink.getText());
			// System.out.println(bodyTextConcat);
			Assert.assertTrue(docAndRecieptsText.getText().equals(bodyText),
					"Documents and Receipts Body text is not displayed as expected");
			Reporter.log("Verified Docuemnts and Receitps Section");
		} catch (Exception e) {
			Assert.fail("Documents and Receipts Section not found");
		}
		return this;
	}

	/**
	 * verify and click account activities link
	 * 
	 * @return
	 */
	public EipJodPage clickAccountHistoryLink() {
		try {
			accountActivitiesLink.isDisplayed();
			accountActivitiesLink.click();
			Reporter.log("Clicked on Account activities link");
		} catch (Exception e) {
			Assert.fail("Account Activities link not found");
		}
		return this;
	}

	/**
	 * Verify Active and Completed EIP plans section
	 * 
	 * @return
	 */
	public EipJodPage verifyEipSection() {
		try {
			activeSectionTab.isDisplayed();
			Assert.assertTrue(activeSectionTab.getAttribute("class").contains("black body-bold"),
					"Active EIP plans section is not Active tab");
			completedSectionTab.isDisplayed();
			Assert.assertTrue(!completedSectionTab.getAttribute("class").contains("black body-bold"),
					"Compelted EIP plans section is Active tab");
			Reporter.log("Verified Active and Completed EIP plans sections");
		} catch (Exception e) {
			Assert.fail("Active and Completed EIP plans sections not found");
		}
		return this;
	}

	/**
	 * CLick on Active EIP plans section
	 * 
	 * @return
	 */
	public EipJodPage clickActiveEIPPlansTab() {
		try {
			activeSectionTab.isDisplayed();
			activeSectionTab.click();
			Reporter.log("Clicked on Active EIP plans section");
		} catch (Exception e) {
			Assert.fail("Active EIP plans section not found");
		}
		return this;
	}

	/**
	 * CLick on Completed EIP plans section
	 * 
	 * @return
	 */
	public EipJodPage clickCompletedPlansTab() {
		try {
			completedSectionTab.isDisplayed();
			completedSectionTab.click();
			Reporter.log("Clicked on Comepleted EIP plans section");
		} catch (Exception e) {
			Assert.fail("Completed EIP plans section not found");
		}
		return this;
	}

	/**
	 * Verify Active EIP plans section
	 * 
	 * @return
	 */
	public EipJodPage verifyActiveEipTabHighlighted() {
		try {
			activeSectionTab.isDisplayed();
			Assert.assertTrue(activeSectionTab.getAttribute("class").contains("black body-bold"),
					"Active EIP plans section is not Active");
			completedSectionTab.isDisplayed();
			Assert.assertTrue(!completedSectionTab.getAttribute("class").contains("black body-bold"),
					"Compelted EIP plans section is Active");
			Reporter.log("Verified Active EIP plans sections");
		} catch (Exception e) {
			Assert.fail("Active EIP plans sections not found");
		}
		return this;
	}

	/**
	 * Verify Completed EIP plans section
	 * 
	 * @return
	 */
	public EipJodPage verifyCompletedEipTabHighlighted() {
		try {
			completedSectionTab.isDisplayed();
			Assert.assertTrue(completedSectionTab.getAttribute("class").contains("black body-bold"),
					"Compelted EIP plans section is not Active");
			activeSectionTab.isDisplayed();
			Assert.assertTrue(!activeSectionTab.getAttribute("class").contains("black body-bold"),
					"Active EIP plans section is Active");
			Reporter.log("Verified Completed EIP plans sections");
		} catch (Exception e) {
			Assert.fail("Completed EIP plans sections not found");
		}
		return this;
	}

	// TODO: Need to change verification for active and inactive EIP pla tabs
	// after code deployment.

	/**
	 * verify monthly payments header and its corresponding amount
	 */
	public void verifyMonthlyPayments() {
		try {
			verifyMonthlyPaymentsHeader();
			verifyMonthlyPaymentsAmount();
		} catch (Exception e) {
			Assert.fail("Monthly payments info not found");
		}
	}

	/**
	 * verify monthly payments header
	 */
	public void verifyMonthlyPaymentsHeader() {
		try {
			monthlyPaymentsHeader.isDisplayed();
			Reporter.log("Monhtly payments header is displayed");
		} catch (Exception e) {
			Assert.fail("Monthly payments Header not found");
		}
	}

	/**
	 * verify monthly payments amount (Sum of active EIP's)
	 */
	public void verifyMonthlyPaymentsAmount() {
		try {
			monthlyPaymentsAmount.isDisplayed();
			String monthlyTotalAmount = monthlyPaymentsAmount.getText().replace("$", "");
			Double sumOfMonthlyAmounts = getTotalOfActiveEIPsMonthlyAmount();
			Assert.assertEquals(sumOfMonthlyAmounts, Double.parseDouble(monthlyTotalAmount), 0.001);
			Reporter.log("Monhtly payments amount calculation is verified");
		} catch (Exception e) {
			Assert.fail("Monthly payments amount not found");
		}
	}

	/**
	 * get sum of active monthly EIP payment amounts
	 * 
	 * @return amountTotal
	 */
	public Double getTotalOfActiveEIPsMonthlyAmount() {
		Double amountTotal = 0.0;
		try {
			String monthlyAmountval;
			for (WebElement monthlyAmount : activeEIPsMonthlyAmount) {
				monthlyAmountval = monthlyAmount.getText();
				amountTotal = amountTotal + Double.parseDouble(
						monthlyAmountval.substring(monthlyAmountval.indexOf("$") + 1, monthlyAmountval.indexOf("/")));
			}
			Reporter.log("Monhtly payments amount calculation is verified");
		} catch (Exception e) {
			Assert.fail("Monthly payments amount not found");
		}
		return amountTotal;
	}

	/**
	 * verify lease details header and its corresponding amount
	 */
	public void verifyLeaseDetails() {
		try {
			verifyLeaseDetailsHeader();
			verifyLeaseDetailsAmount();
		} catch (Exception e) {
			Assert.fail("Lease Details are not found");
		}
	}

	/**
	 * click lease details link
	 */
	public void clickLeaseDetails() {
		try {
			leaseDetailsHeader.click();
			Reporter.log("Clicked on Lease Details");
		} catch (Exception e) {
			Assert.fail("Lease Details Header not found");
		}
	}

	/**
	 * verify lease details header
	 */
	public void verifyLeaseDetailsHeader() {
		try {
			leaseDetailsHeader.isDisplayed();
			Reporter.log("Lease Details Header is verified");
		} catch (Exception e) {
			Assert.fail("Lease Details Header not found");
		}
	}

	/**
	 * verify lease details amount
	 */
	public void verifyLeaseDetailsAmount() {
		try {
			leaseDetailsAmount.isDisplayed();
			Reporter.log("Lease Details Amount is verified");
		} catch (Exception e) {
			Assert.fail("Lease Details Amount not found");
		}
	}

	public void clickActiveArrowUp() {
		try {
			if (activeEIPArrowUp.isDisplayed()) {

				activeEIPArrowUp.click();
			}

			Reporter.log("clicked on active arrow up");
		} catch (Exception e) {
			Assert.fail("failed to click active arrow up");
		}
	}

	/**
	 * verify plan balance header and its corresponding amount
	 */
	public void verifyPlanBalance() {
		try {
			verifyPlanBalanceHeader();
			verifyPlanBalanceAmount();
		} catch (Exception e) {
			Assert.fail("Plan balance details not found");
		}
	}

	/**
	 * click plan balance
	 */
	public void clickPlanBalance() {
		try {
			planDetailsHeader.click();
			Reporter.log("Clicked on Plan Details Header");
		} catch (Exception e) {
			Assert.fail("Plan Details Header not found");
		}
	}

	/**
	 * verify plan balance header
	 */
	public void verifyPlanBalanceHeader() {
		try {
			planDetailsHeader.isDisplayed();
			Reporter.log("Plan Details Header is displayed");
		} catch (Exception e) {
			Assert.fail("Plan Details Header not found");
		}
	}

	/**
	 * verify plan balance amount (Sum of remaining balances of active plans)
	 */
	public void verifyPlanBalanceAmount() {
		try {
			planBalanceAmount.isDisplayed();
			String planBalTotalAmount = planBalanceAmount.getText().replaceAll("[$,]", "");
			Double sumOfactiveRemainingAmounts = getTotalOfActiveEIPsRemainingAmount();
			Assert.assertEquals(sumOfactiveRemainingAmounts, Double.parseDouble(planBalTotalAmount), 0.001);
			Reporter.log("Remaining amount calculation is verified");
		} catch (Exception e) {
			Assert.fail("Remaining amount not found");
		}
	}

	/**
	 * get sum of active monthly EIP payment amounts
	 * 
	 * @return amountTotal
	 */
	public Double getTotalOfActiveEIPsRemainingAmount() {
		Double amountTotal = 0.0;
		try {
			for (WebElement remainingAmount : activeEIPsRemainingAmount) {
				amountTotal = amountTotal + Double.parseDouble(remainingAmount.getText().replace("$", ""));
			}
			Reporter.log("Monhtly payments amount calculation is verified");
		} catch (Exception e) {
			Assert.fail("Monthly payments amount not found");
		}
		return amountTotal;
	}

	public void verifyJODSection() {
		// TODO Auto-generated method stub

	}

	/**
	 * Verify Upsell message for No activeEIP or JOD users
	 * 
	 * @param noActiveEip
	 */
	public void verifyNoActiveEIPorJODMessage(String noActiveEip) {
		try {
			noActiveEIPorJODtext.isDisplayed();
			Assert.assertTrue(noActiveEIPorJODtext.getText().trim().equals(noActiveEip));
			Reporter.log("Upsell message is displayed for No active EIP or JOD Users");
		} catch (Exception e) {
			Assert.fail("Upsell message is not found");
		}
	}

	/**
	 * verify Shop Now CTA is displayed
	 */
	public void verifyShopNowCTA() {
		try {
			shopNowCTA.isDisplayed();
			Reporter.log("Shop Now CTA is displayed");
		} catch (Exception e) {
			Assert.fail("Shop Now CTA is not found");
		}
	}

	/**
	 * click on Shop Now CTA
	 */
	public void clickSHopNowCTA() {
		try {
			shopNowCTA.click();
			Reporter.log("CLicked on Shop Now CTA");
		} catch (Exception e) {
			Assert.fail("Shop Now CTA is not found");
		}
	}

	/**
	 * Verify EIPJOD Header is displayed or not
	 * 
	 * @return
	 */
	public EipJodPage verifyEIPJODPageHeader() {
		try {

			StringBuilder headerText = new StringBuilder();
			for (WebElement header : pageHeader) {
				headerText.append(header.getText().concat(StringUtils.SPACE));
			}
			Assert.assertTrue(headerText.toString().trim().equals("Equipment Installment Plans & Leases"),
					"Page header not found");
			Reporter.log("Verified EIPJOD page Header is displayed");
		} catch (Exception e) {
			Assert.fail("EIPJOD page Header is not displayed");
		}
		return this;
	}

	public EipJodPage verifyEIPPage() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(landingpageSpinner));
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.urlContains("eipjod"), 10);
			}
			Reporter.log("Verified EIP page Header is displayed");
		} catch (Exception e) {
			Assert.fail("EIP page Header is not displayed");
		}
		return this;
	}

	/**
	 * Verify EIP blade header is displayed or not
	 * 
	 * @return
	 */
	public EipJodPage verifyEIPBladeHeader() {
		try {
			eipBladeHeader.isDisplayed();
			Assert.assertTrue(eipBladeHeader.getText().contains("Equipment Installment Plans"),
					"eip blade header not found");
			Reporter.log("Verified EIP blade Header is displayed");
		} catch (Exception e) {
			Assert.fail("EIP blade Header is displayed");
		}
		return this;
	}

	/**
	 * verify Progress bar is displayed for every active EIP/JOD plan for both
	 * expanded and collapsed view.
	 */
	public void verifyProgressBarExpandedAndCollapsedViews() {
		try {
			for (int i = 0; i < activeEIPPlans.size(); i++) {
				if (i != 0) {
					activeEIPArrowDown.get(i).click();
				}
				progressBars.get(i).isDisplayed();
				waitFor(ExpectedConditions.visibilityOf(activeEIPArrowUp));
				activeEIPArrowUp.click();
				progressBars.get(i).isDisplayed();
			}
			Reporter.log("Verified Progress bar is displayed for ALL Active EIP plans");
			/*
			 * for (WebElement activeJOD : activeJODPlans) { progressBar =
			 * activeJOD.findElement(By.className("TMO-PROGRESS-BAR"));
			 * progressBar.isDisplayed();
			 * activeJOD.findElement(By.cssSelector("span[class*='arrow-down']")
			 * ).click(); progressBar.isDisplayed(); } Reporter.log(
			 * "Verified Progress bar is displayed for ALL Active JOD plans");
			 */
		} catch (Exception e) {
			Assert.fail("Failed to verify Progress Bar for Active EIP & JOD plans");
		}

	}

	/**
	 * verify all the elements of Active EIP plans Collapsed section
	 */
	public void verifyActiveEIPPlans() {
		try {
			for (int i = 0; i < activeEIPPlans.size(); i++) {
				verifyActiveEIPPhoneImageCollapsedView(phoneImages.get(i));
				verifyActiveEIPFriendlyNameCollapsedView(friendlyNames.get(i));
				verifyActiveEIPMsisdnCollapsedView(msisdns.get(i));
				verifyActiveEIPDeviceNameCollapsedView(deviceNames.get(i));
				verifyActiveEIPRemainingBalCollapsedView(remainingBal.get(i), activeEIPsRemainingAmount.get(i));
				verifyActiveEIPMonthlyAmountCollapsedView(activeEIPsMonthlyAmount.get(i));
				verifyActiveEIPProgressBarCollapsedView(progressBars.get(i));
				if (i == 0) {
					clickActiveArrowUp();
				}
				verifyActiveEIPProgressBarCollapsedView(progressBars.get(i));
			}
			Reporter.log("Verified ALL Active EIP plans are displayed as Expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Active EIP plans");
		}
	}

	/**
	 * click on arrow chevron
	 * 
	 * @param arrowChevron
	 */
	public void clickActiveEIPArrow(int i) {
		try {
			activeEIPArrowDown.get(i).click();
			Reporter.log("Clicked on Arrow Chevron");
		} catch (Exception e) {
			Assert.fail("Arrow Chevron is not found");
		}
	}

	/**
	 * verify phone image is displayed
	 * 
	 * @param phoneImage
	 */
	public void verifyActiveEIPPhoneImageCollapsedView(WebElement phoneImage) {
		try {
			phoneImage.isDisplayed();
			Reporter.log("Phone Image is displayed");
		} catch (Exception e) {
			Assert.fail("Phone Image is not found");
		}
	}

	/**
	 * verify friendly name is displayed
	 * 
	 * @param friendlyName
	 */
	public void verifyActiveEIPFriendlyNameCollapsedView(WebElement friendlyName) {
		try {
			friendlyName.isDisplayed();
			Reporter.log("Friendly Name is displayed");
		} catch (Exception e) {
			Assert.fail("Friendly Name is not found");
		}
	}

	/**
	 * verify msisdn is displayed
	 * 
	 * @param msisdn
	 */
	public void verifyActiveEIPMsisdnCollapsedView(WebElement msisdn) {
		try {
			msisdn.isDisplayed();
			Reporter.log("Msisdn is displayed");
		} catch (Exception e) {
			Assert.fail("Msisdn is not found");
		}
	}

	/**
	 * verify device name is displayed
	 * 
	 * @param deviceName
	 */
	public void verifyActiveEIPDeviceNameCollapsedView(WebElement deviceName) {
		try {
			deviceName.isDisplayed();
			Reporter.log("Device Name is displayed");
		} catch (Exception e) {
			Assert.fail("Device Name is not found");
		}
	}

	/**
	 * verify Progress Bar is displayed
	 * 
	 * @param progressBar
	 */
	public void verifyActiveEIPProgressBarCollapsedView(WebElement progressBar) {
		try {
			progressBar.isDisplayed();
			Reporter.log("Progress Bar is displayed");
		} catch (Exception e) {
			Assert.fail("Progress Bar is not found");
		}
	}

	/**
	 * verify Remaining Balance is displayed
	 * 
	 * @param remainingBal
	 * @param remainingBalAmount
	 */
	public void verifyActiveEIPRemainingBalCollapsedView(WebElement remainingBal, WebElement remainingBalAmount) {
		try {
			remainingBal.isDisplayed();
			Reporter.log("Remaining Balance Header is displayed");
			remainingBalAmount.isDisplayed();
			Reporter.log("Remaining Balance Amount is displayed");
		} catch (Exception e) {
			Assert.fail("Msisdn is not found");
		}
	}

	/**
	 * verify Monthly amount is displayed
	 * 
	 * @param monthlyAmount
	 */
	public void verifyActiveEIPMonthlyAmountCollapsedView(WebElement monthlyAmount) {
		try {
			monthlyAmount.isDisplayed();
			Reporter.log("Monthly Amount is displayed");
		} catch (Exception e) {
			Assert.fail("Monthly Amount is not found");
		}
	}

	/**
	 * verify all the elements of Active EIP plans Expanded section
	 */
	public void verifyActiveEIPPlansExpandedView() {
		boolean paymentDetailsDisplayed = false;
		try {
			for (int i = 0; i < activeEIPPlans.size(); i++) {
				if (i != 0) {
					clickEIPDownwardPointingChevron(i);
				}
				verifyActiveEIPPhoneImageCollapsedView(phoneImages.get(i));
				verifyActiveEIPFriendlyNameCollapsedView(friendlyNames.get(i));
				verifyActiveEIPMsisdnCollapsedView(msisdns.get(i));
				verifyActiveEIPDeviceNameCollapsedView(deviceNames.get(i));
				verifyActiveEIPRemainingBalCollapsedView(remainingBal.get(i), activeEIPsRemainingAmount.get(i));
				verifyActiveEIPMonthlyAmountCollapsedView(activeEIPsMonthlyAmount.get(i));
				verifyActiveEIPProgressBarCollapsedView(progressBars.get(i));
				for (String detail : expandedViewHeaders) {
					for (int j = 0; j <= activeEipExpandedViewHeaders.size(); j++) {
						if (activeEipExpandedViewHeaders.get(j).isDisplayed()
								&& detail.equals(activeEipExpandedViewHeaders.get(j).getText())
								&& activeEipExpandedViewValues.get(j).isDisplayed()) {
							paymentDetailsDisplayed = true;
							break;
						}
					}
					if (!paymentDetailsDisplayed) {
						Assert.fail("Expected Headers are not found in Active EIP plans Expanded view");
					}
				}
				verifyActiveEIPProgressBarCollapsedView(progressBars.get(i));
				clickActiveArrowUp();
			}
			Reporter.log("Verified ALL Active EIP plans are displayed as Expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Active EIP plans");
		}
	}

	/**
	 * CLick on Active EIP down arrow
	 * 
	 * @param count
	 * 
	 * @return
	 */
	public EipJodPage clickEIPDownwardPointingChevron(int count) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(activeEIPArrowDown.get(count)));
			scrollToElement(activeEIPArrowDown.get(count));

			activeEIPArrowDown.get(count).click();
			waitFor(ExpectedConditions.visibilityOf(eipExpandedView));
			Reporter.log("Clicked on Active EIP downward  arrow");
		} catch (Exception e) {
			Assert.fail("Active EIP downward  arrow not found");
		}
		return this;
	}

	/**
	 * CLick on completed EIP down arrow
	 * 
	 * @return
	 */
	public EipJodPage clickCompletedDownwardPointingChevron(WebElement completedEipDownArrow) {
		try {

			completedEipDownArrow.isDisplayed();
			completedEipDownArrow.click();

			Reporter.log("Clicked on completed EIP downward  arrow");
		} catch (Exception e) {
			Assert.fail("completed EIP downward  arrow not found");
		}
		return this;
	}

	/**
	 * CLick on completed EIP Up arrow section
	 * 
	 * @return
	 */
	public EipJodPage clickCompletedUpwardPointingChevron() {
		try {
			completedEipUpArrow.isDisplayed();
			completedEipUpArrow.click();
			Reporter.log("Clicked on completed EIP up arrow");
		} catch (Exception e) {
			Assert.fail("completed EIP up arrow not found");
		}
		return this;
	}

	/**
	 * CLick on EIP payment estimator link
	 * 
	 * @return
	 */
	public EipJodPage clickEipPaymentEstimatorlink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(eipExpandedView));
			waitFor(ExpectedConditions.visibilityOf(seeThePaymentEstimatorLink));
			seeThePaymentEstimatorLink.isDisplayed();
			seeThePaymentEstimatorLink.click();
			Reporter.log("Clicked on  EIP Payment estimator link");
		} catch (Exception e) {
			Assert.fail("EIP payment estimatorlink not found");
		}
		return this;
	}

	/**
	 * Verify completed EIP Device image is displayed or not
	 * 
	 * @return
	 */
	public EipJodPage verifyCompletedDeviceImg(WebElement completedEipDeviceImage) {
		try {
			completedEipDeviceImage.isDisplayed();
			Reporter.log("Verified Completed EIP device image is displayed");
		} catch (Exception e) {
			Assert.fail("Completed EIP device image is not displayed");
		}
		return this;
	}

	/**
	 * Verify completed EIP Device name is displayed or not
	 * 
	 * @return
	 */
	public EipJodPage verifyCompletedDeviceName(WebElement completedEipDeviceName) {
		try {

			completedEipDeviceName.isDisplayed();

			Reporter.log("Verified Completed EIP device name is displayed");
		} catch (Exception e) {
			Assert.fail("Completed EIP device name is not displayed");
		}
		return this;
	}

	/**
	 * Verify completed EIP status is displayed or not
	 * 
	 * @return
	 */
	public EipJodPage verifyCompletedStatus(WebElement completedStatus) {
		try {
			completedStatus.isDisplayed();

			Reporter.log("Verified Completed EIP status is displayed");
		} catch (Exception e) {
			Assert.fail("Completed EIP status is not displayed");
		}
		return this;
	}

	/**
	 * Verify completed EIP misdin is displayed or not
	 * 
	 * @return
	 */
	public EipJodPage verifyCompletedMisdin(WebElement completedEipMisdin) {
		try {

			completedEipMisdin.isDisplayed();

			Reporter.log("Verified Completed EIP misdin is displayed");
		} catch (Exception e) {
			Assert.fail("Completed EIP misdin is not displayed");
		}
		return this;
	}

	/**
	 * Verify completed EIP friendly name is displayed or not
	 * 
	 * @return
	 */
	public EipJodPage verifyCompletedFriendlyName(WebElement completedEipFriendlyName) {
		try {

			completedEipFriendlyName.isDisplayed();

			Reporter.log("Verified Completed EIP friendly name is displayed");
		} catch (Exception e) {
			Assert.fail("Completed EIP friendly name is not displayed");
		}
		return this;
	}

	/**
	 * Verify progress bar is displayed or not
	 * 
	 * @return
	 */
	public EipJodPage verifyProgressBarNotDisplayed() {
		try {
			for (WebElement progressBar : progressBars)
				progressBar.isDisplayed();
		} catch (Exception e) {
			if (e.getMessage().contains("Unable to locate element")) {
				Reporter.log("progress bar is not displayed");
			} else {
				Assert.fail("Completed progress bar is displayed");
			}
		}
		return this;
	}

	/**
	 * verify all the elements of completed EIP plans and blade extended section
	 */
	public void verifyCompletedEIPPlans() {
		try {
			for (int i = 0; i < completedEIPPlans.size(); i++) {

				verifyCompletedDeviceImg(completedEipDeviceImages.get(i));
				verifyCompletedDeviceName(completedEipDeviceNames.get(i));
				verifyCompletedFriendlyName(completedEipFriendlyNames.get(i));
				verifyCompletedMisdin(completedEipMisdins.get(i));
				verifyCompletedStatus(completedEipCompletedStatus.get(i));
				verifyProgressBarNotDisplayed();
				clickCompletedDownwardPointingChevron(completedEipDownArrow.get(i));
				verifyIsEIpBladeElementsDisplayed();
				clickCompletedUpwardPointingChevron();
			}
			Reporter.log("Verified ALL Completed EIP plans and balde elements are displayed as Expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify competed EIP plans and blade elements");
		}
	}

	/**
	 * verify all the elements of completed EIP plans and blade extended section
	 */
	public void verifyCompletedEIPPlansAndBladeElements() {
		try {
			for (int i = 0; i < completedEIPPlans.size(); i++) {
				verifyCompletedDeviceImg(completedEipDeviceImages.get(i));
				verifyCompletedDeviceName(completedEipDeviceNames.get(i));
				verifyCompletedFriendlyName(completedEipFriendlyNames.get(i));
				verifyCompletedMisdin(completedEipMisdins.get(i));
				verifyCompletedStatus(completedEipCompletedStatus.get(i));
				verifyProgressBarNotDisplayed();
				clickCompletedDownwardPointingChevron(completedEipDownArrow.get(i));
				verifyIsEIpBladeElementsDisplayed();
				verifyEIpBladeElementsText();
				clickCompletedUpwardPointingChevron();
			}
			Reporter.log("Verified ALL Completed EIP plans and balde elements are displayed as Expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify competed EIP plans and blade elements");
		}
	}

	/**
	 * verify Eip blade elements is dispalyed or not
	 */
	public void verifyIsEIpBladeElementsDisplayed() {
		try {
			eipExpandedView.isDisplayed();
			Reporter.log("Eip blade  elements is displayed");
		} catch (Exception e) {
			Assert.fail("Eip Blade elements is  not found");
		}
	}

	/**
	 * verify JOD blade elements is displayed or not
	 */
	public void verifyIsJodBladeElementsDisplayed() {
		try {
			jodExpandedView.isDisplayed();
			Reporter.log("JOD blade elements is displayed");
		} catch (Exception e) {
			Assert.fail("JOD Blade elements is not found");
		}
	}

	/**
	 * verify Eip blade elements text
	 */
	public void verifyEIpBladeElementsText() {
		try {
			for (int i = 0; i < completedEipExpandedViewHeaders.size(); i++) {

				Assert.assertTrue(completedEipExpandedViewHeaders.get(i).getText().equals(expandedViewHeaders.get(i)));
			}
			Reporter.log("Eip blade elements text is verified");
		} catch (Exception e) {
			Assert.fail("Eip Blade elements text is  not found");
		}
	}

	/**
	 * verify JOD blade elements text
	 */
	public void verifyJODBladeElementsText() {
		try {
			for (int i = 0; i < completedJODExpandedViewHeaders.size(); i++) {

				Assert.assertTrue(completedJODExpandedViewHeaders.get(i).getText().equals(expandedViewHeaders.get(i)));
			}
			Reporter.log("JOD blade elements text is verified");
		} catch (Exception e) {
			Assert.fail("JOD Blade elements text is not found");
		}
	}

	public void verifyActiveEIPPlansSorting() {
		try {
			List<Date> planDates = new ArrayList<Date>();
			SimpleDateFormat sortedDateFormat = new SimpleDateFormat("MM/dd/yy");
			Date planDate;
			for (int i = 0; i < activeEIPPlans.size(); i++) {
				if (!activeEIPArowUp.get(i).isDisplayed()) {
					clickEIPDownwardPointingChevron(i);
				}
				waitFor(ExpectedConditions.visibilityOf(planStartDates.get(0)));
				planDate = sortedDateFormat.parse(planStartDates.get(0).getText());
				planDates.add(planDate);
				clickActiveArrowUp();
			}
			Assert.assertTrue(isSorted(planDates));
			Reporter.log(
					"Verified First EIP Active EIP plan is displayed in Expanded view and all other are in collapsed view.");
		} catch (Exception e) {
			Assert.fail("Failed to verify Active EIP plans Sorting Order");
		}
	}

	/**
	 * get friendly name of particular active eip
	 * 
	 * @param i
	 */
	public String getActiveEIPFriendlyName(int i) {
		checkPageIsReady();
		String name = null;
		try {
			name = friendlyNames.get(i).getText();
		} catch (Exception e) {
			Assert.fail("Friendly Name is not found");
		}
		return name;
	}

	/**
	 * get msisdn of active EIP
	 * 
	 * @param i
	 */
	public String getActiveEIPMsisdn(int i) {
		String msisdn = null;
		try {
			msisdn = msisdns.get(i).getText();
		} catch (Exception e) {
			Assert.fail("Msisdn is not found");
		}
		return msisdn;
	}

	/**
	 * get device name of active EIP
	 * 
	 * @param i
	 */
	public String getActiveEIPDeviceName(int i) {
		String deviceName = null;
		try {
			deviceName = deviceNames.get(i).getText();
		} catch (Exception e) {
			Assert.fail("Device Name is not found");
		}
		return deviceName;
	}

	/**
	 * get Monthly amount of active EIP
	 * 
	 * @param i
	 */
	public String getActiveEIPMonthlyAmount(int i) {
		String monthlyAmount = null;

		try {
			monthlyAmount = activeEIPsMonthlyAmount.get(i).getText();
		} catch (Exception e) {
			Assert.fail("Monthly Amount is not found");
		}
		return monthlyAmount;
	}

	/**
	 * click make a payment button of active EIP
	 */
	public void clickMakeAPaymentBtn() {
		try {
			checkPageIsReady();
			if (!activeEIPArowUp.get(0).isDisplayed()) {
				clickEIPDownwardPointingChevron(0);
			}
			scrollToElement(makeAPaymentBtn);
			makeAPaymentBtn.click();
			Reporter.log("Failed to click on Make a Payment button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Make a Payment button");
		}
	}

	public int getActiveEIPPlansCount() {
		int count = 0;
		try {
			count = activeEIPPlans.size();
		} catch (Exception e) {
			Assert.fail("Error getting active EIP plans size");
		}
		return count;
	}

	public boolean isSorted(List<Date> planDates) {
		boolean sorted = true;
		try {
			for (int i = 1; i < planDates.size(); i++) {
				if (planDates.get(i - 1).compareTo(planDates.get(i)) > 0)
					sorted = false;
			}
		} catch (Exception e) {
			Assert.fail("Failed while verifying the sort order of active EIP records");
		}
		return sorted;
	}

	/**
	 * verify PII masking on required elements of Active EIP plans Expanded
	 * section
	 */
	public void verifyActiveEIPPlansPiiMasking(String custName, String custMsisdn, String custImei) {
		try {
			for (int i = 0; i < activeEIPPlans.size(); i++) {
				if (i != 0) {
					clickEIPDownwardPointingChevron(i);
				}
				Assert.assertTrue(checkElementisPIIMasked(friendlyNames.get(i), custName));
				Assert.assertTrue(checkElementisPIIMasked(msisdns.get(i), custMsisdn));
				for (int j = 0; j <= activeEipExpandedViewHeaders.size(); j++) {
					if (activeEipExpandedViewHeaders.get(j).isDisplayed()
							&& activeEipExpandedViewHeaders.get(j).getText().equals("IMEI:")
							&& activeEipExpandedViewValues.get(j).isDisplayed()) {
						Assert.assertTrue(checkElementisPIIMasked(activeEipExpandedViewValues.get(j), custImei),
								"IMEI number is not PII Masked");
						break;
					}
				}
				clickActiveArrowUp();
			}
			Reporter.log("Verified ALL Active EIP plans are displayed as Expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Active EIP plans");
		}
	}

	/**
	 * verify PII masking on required elements of Completed EIP plans Expanded
	 * section
	 * 
	 * @param piiCustomerImeiPid
	 * @param piiCustomerMsisdnPid
	 * @param piiCustomerCustfirstnamePid
	 */
	public void verifyCompletedEIPPlansPiiMasking(String piiCustomerCustfirstnamePid, String piiCustomerMsisdnPid,
			String piiCustomerImeiPid) {
		try {
			for (int i = 0; i < completedEIPPlans.size(); i++) {
				Assert.assertTrue(
						checkElementisPIIMasked(completedEipFriendlyNames.get(i), piiCustomerCustfirstnamePid));
				Assert.assertTrue(checkElementisPIIMasked(completedEipMisdins.get(i), piiCustomerMsisdnPid));
				clickCompletedDownwardPointingChevron(completedEipDownArrow.get(i));
				verifyIsEIpBladeElementsDisplayed();
				for (int j = 0; i < completedEipExpandedViewHeaders.size(); j++) {
					if (completedEipExpandedViewHeaders.get(j).isDisplayed()
							&& completedEipExpandedViewHeaders.get(j).getText().equals("IMEI:")
							&& completedEipExpandedViewValues.get(j).isDisplayed()) {
						Assert.assertTrue(
								checkElementisPIIMasked(completedEipExpandedViewValues.get(j), piiCustomerImeiPid),
								"IMEI number is not PII Masked");
						break;
					}
				}
				clickCompletedUpwardPointingChevron();
			}
			Reporter.log("Verified ALL Completed EIP plans and balde elements are displayed as Expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify competed EIP plans and blade elements");
		}
	}

	/**
	 * 
	 * verify Completed JOD plans and Blade elements
	 */
	public void verifyCompletedJODPlansAndBladeElements() {
		try {
			for (int i = 0; i < completedJODPlans.size(); i++) {
				verifyCompletedDeviceImg(completedJodDeviceImages.get(i));
				verifyCompletedDeviceName(completedJodDeviceNames.get(i));
				verifyCompletedFriendlyName(completedJodFriendlyNames.get(i));
				verifyCompletedMisdin(completedJodMisdins.get(i));
				verifyCompletedStatus(completedJodCompletedStatus.get(i));
				verifyProgressBarNotDisplayed();
				clickCompletedDownwardPointingChevron(completedJodDownArrow.get(i));
				verifyIsJodBladeElementsDisplayed();
				verifyJODBladeElementsText();
				clickCompletedUpwardPointingChevron();
			}
			Reporter.log("Verified ALL Completed JOD plans and blade elements are displayed as Expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify competed JOD plans and blade elements");
		}
	}

	/**
	 * verify all the elements of completed JOD plans and blade extended section
	 */
	public void verifyCompletedJODPlans() {
		try {
			for (int i = 0; i < completedJODPlans.size(); i++) {

				verifyCompletedDeviceImg(completedJodDeviceImages.get(i));
				verifyCompletedDeviceName(completedJodDeviceNames.get(i));
				verifyCompletedFriendlyName(completedJodFriendlyNames.get(i));
				verifyCompletedMisdin(completedJodMisdins.get(i));
				verifyCompletedStatus(completedJodCompletedStatus.get(i));
				verifyProgressBarNotDisplayed();
				clickCompletedDownwardPointingChevron(completedJodDownArrow.get(i));
				verifyIsJodBladeElementsDisplayed();
				clickCompletedUpwardPointingChevron();
			}
			Reporter.log("Verified ALL Completed JOD plans and blade elements are displayed as Expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify competed JOD plans and blade elements");
		}
	}

	/**
	 * get active EIP remaining bal
	 * 
	 * @return
	 */
	public String getRemainingBal(int i) {
		try {
			return remainingBal.get(i).getText();
		} catch (Exception e) {
			Assert.fail("Failed to get Remaining Balance amount");
		}
		return null;
	}

	public String getActiveEIPPlanID(int i) {
		try {
			return eipPlanId.get(i).getText();
		} catch (Exception e) {
			Assert.fail("Failed to get EIP Plan ID");
		}
		return null;
	}

	public EipJodPage verifyIfJODBladeExpanded() {
		try {
			activeFirstJODArrowUp.isDisplayed();
			// activeEipSection.isSelected();
			Reporter.log("Verified Active Tab is selected in EIP JOD page");
		} catch (Exception e) {
			Assert.fail("Active Tab is not selected in EIP JOD page");
		}
		return this;

	}

	/**
	 * Verify If Active Tab is Selected
	 * 
	 * @return
	 */
	public EipJodPage verifyActiveTabIsSelected() {
		try {
			activeSectionTab.isDisplayed();
			activeSectionTab.isSelected();
			Reporter.log("Verified Active Tab is selected in EIP JOD page");
		} catch (Exception e) {
			Assert.fail("Active Tab is not selected in EIP JOD page");
		}
		return this;
	}

	/**
	 * verify all the elements of Active JOD plans Expanded section
	 */
	public void verifyActiveJODPlansExpandedView() {
		boolean paymentDetailsDisplayed = false;
		checkPageIsReady();
		try {
			for (int i = 0; i < activeJODPlans.size(); i++) {
				if (i != 0) {
					clickJODDownwardPointingChevron(i);
				}
				verifyActiveJODPhoneImageCollapsedView(jodPhoneImages.get(i));
				verifyActiveJODFriendlyNameCollapsedView(jodFriendlyNames.get(i));
				verifyActiveJODMsisdnCollapsedView(activeJODMsisdns.get(i));
				verifyActiveJODDeviceNameCollapsedView(activeJODDeviceNames.get(i));
				verifyActiveJODRemainingPaymentsCollapsedView(activeJODRemainingPayments.get(i),
						activeJODsRemainingPayments.get(i));
				verifyActiveJODMonthlyAmountCollapsedView(activeJODsMonthlyAmount.get(i));
				verifyActiveJODProgressBarCollapsedView(activeJODprogressBars.get(i));
				for (String detail : expandedJODViewHeadersHardcoded) {
					for (int j = 0; j <= activeJODExpandedViewHeaders.size(); j++) {
						if (activeJODExpandedViewHeaders.get(j).isDisplayed()
								&& detail.equals(activeJODExpandedViewHeaders.get(j).getText())
								&& activeJODExpandedViewValues.get(j).isDisplayed()) {
							paymentDetailsDisplayed = true;
							break;
						}
					}
					if (!paymentDetailsDisplayed) {
						Assert.fail("Expected Headers are not found in Active JOD plans Expanded view");
					}
				}
				verifyActiveJODProgressBarCollapsedView(activeJODprogressBars.get(i));
				clickActiveArrowUp();
			}
			Reporter.log("Verified ALL Active JOD plans are displayed as Expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Active JOD plans");
		}
	}

	/**
	 * verify all the elements of Active JOD plans Collapsed section
	 */
	public void verifyActiveJODPlansCollapsedView() {
		try {
			for (int i = 0; i < activeJODPlans.size(); i++) {
				verifyActiveJODPhoneImageCollapsedView(jodPhoneImages.get(i));
				verifyActiveJODFriendlyNameCollapsedView(jodFriendlyNames.get(i));
				verifyActiveJODMsisdnCollapsedView(activeJODMsisdns.get(i));
				verifyActiveJODDeviceNameCollapsedView(activeJODDeviceNames.get(i));
				verifyActiveJODProgressBarCollapsedView(activeJODprogressBars.get(i));
				verifyActiveJODRemainingPaymentsCollapsedView(activeJODRemainingPayments.get(i),
						activeJODsRemainingPayments.get(i));
				verifyActiveJODMonthlyAmountCollapsedView(activeJODsMonthlyAmount.get(i));

			}
			Reporter.log("Verified ALL Active JOD plans are displayed as Expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify Active JOD plans");
		}
	}

	/**
	 * verify all the elements of Active JOD plans Progress Bar
	 */
	public void verifyActiveJODPlansProgressBars() {
		try {
			for (int i = 0; i < activeJODPlans.size(); i++) {
				verifyActiveJODProgressBarCollapsedView(activeJODprogressBars.get(i));
			}
			Reporter.log("Verified ALL Active JOD plans progress bars are displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Progress bars for Active JOD plans");
		}
	}

	/**
	 * CLick on Active JOD down arrow
	 * 
	 * @param count
	 * 
	 * @return
	 */
	public EipJodPage clickJODDownwardPointingChevron(int count) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(activeJODArrowDown.get(count)));
			scrollToElement(activeJODArrowDown.get(count));
			activeJODArrowDown.get(count).click();
			waitFor(ExpectedConditions.visibilityOf(eipExpandedView));
			Reporter.log("Clicked on Active EIP downward  arrow");
		} catch (Exception e) {
			Assert.fail("Active EIP downward  arrow not found");
		}
		return this;
	}

	/**
	 * verify phone image is displayed
	 * 
	 * @param phoneImage
	 */
	public void verifyActiveJODPhoneImageCollapsedView(WebElement phoneImage) {
		try {
			phoneImage.isDisplayed();
			Reporter.log("Phone Image is displayed in Active JOD");
		} catch (Exception e) {
			Assert.fail("Phone Image is not found in Active JOD");
		}
	}

	/**
	 * verify friendly name is displayed
	 * 
	 * @param friendlyName
	 */
	public void verifyActiveJODFriendlyNameCollapsedView(WebElement friendlyName) {
		try {
			friendlyName.isDisplayed();
			Reporter.log("Friendly Name is displayed");
		} catch (Exception e) {
			Assert.fail("Friendly Name is not found");
		}
	}

	/**
	 * verify msisdn is displayed
	 * 
	 * @param msisdn
	 */
	public void verifyActiveJODMsisdnCollapsedView(WebElement msisdn) {
		try {
			msisdn.isDisplayed();
			Reporter.log("Msisdn is displayed");
		} catch (Exception e) {
			Assert.fail("Msisdn is not found");
		}
	}

	/**
	 * verify device name is displayed
	 * 
	 * @param deviceName
	 */
	public void verifyActiveJODDeviceNameCollapsedView(WebElement deviceName) {
		try {
			deviceName.isDisplayed();
			Reporter.log("Device Name is displayed");
		} catch (Exception e) {
			Assert.fail("Device Name is not found");
		}
	}

	/**
	 * verify Remaining Balance is displayed
	 * 
	 * @param remainingBal
	 * @param remainingBalAmount
	 */
	public void verifyActiveJODRemainingPaymentsCollapsedView(WebElement remainingBal, WebElement remainingBalAmount) {
		try {
			remainingBal.isDisplayed();
			Reporter.log("Remaining Balance Header is displayed");
			remainingBalAmount.isDisplayed();
			Reporter.log("Remaining Balance Payments is displayed");
		} catch (Exception e) {
			Assert.fail("Remaining Balance Header (or) Payments is not found");
		}
	}

	/**
	 * verify if JOD Monthly amount is displayed
	 * 
	 * @param monthlyAmount
	 */
	public void verifyActiveJODMonthlyAmountCollapsedView(WebElement monthlyAmount) {
		try {
			monthlyAmount.isDisplayed();
			Reporter.log("Monthly Amount is displayed");
		} catch (Exception e) {
			Assert.fail("Monthly Amount is not found");
		}
	}

	/**
	 * verify Progress Bar is displayed
	 * 
	 * @param progressBar
	 */
	public void verifyActiveJODProgressBarCollapsedView(WebElement progressBar) {
		try {
			progressBar.isDisplayed();
			Reporter.log("Progress Bar is displayed");
		} catch (Exception e) {
			Assert.fail("Progress Bar is not found");
		}
	}

	/**
	 * 
	 * verify Ribbons are displayed
	 * 
	 * @param value
	 */
	public void verifyEndingSoonRibbon(String value) {
		try {
			jodNotificationRibbons.isDisplayed();
			Assert.assertTrue(jodNotificationRibbons.getText().equals(value), "Incorrect notification displayed");
			Reporter.log("JOD Notification ribbons are displayed");
		} catch (Exception e) {
			Assert.fail("Error: JOD Notification ribbons not found");
		}
	}

	/**
	 * verify Yake action CTA is displayed
	 */
	public void verifyTakeActionCTA() {
		try {
			if (takeActionCTA.isDisplayed()) {
				takeActionText.isDisplayed();
				Reporter.log("Take Action CTA is displayed");
			} else {
				Reporter.log("Take Action CTA is not displayed");
			}

		} catch (Exception e) {
			Assert.fail("Error: Take Action CTA not found");
		}
	}

	/**
	 * Click on Take Action CTA
	 */
	public void clickTakeActionCTA() {
		try {
			takeActionCTA.click();
			Reporter.log("Take Action CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Error: Take Action CTA not found");
		}
	}

	/**
	 * verify Poip Text for Future Poip - Paid ones
	 */

	public void verifyNoElementDisplay(String elementPath, String ReporterText) {
		try {
			if (getDriver().findElement(By.xpath(elementPath)).isDisplayed())
				Assert.fail(ReporterText + " is Displayed");
		} catch (Exception e) {
			Reporter.log(ReporterText + " is not Displayed");
		}
	}

	/**
	 * verify Poip Future Options
	 */
	public void verifyFututreOptions(String DeviceName, String EndingSoonBanner, String PoipText) {
		try {
			for (int i = 0; i < jodFriendlyNames.size(); i++) {
				if (jodFriendlyNames.get(i).getText().contains(DeviceName)) {
					verifyNoElementDisplay("//div[contains(@id,'leaseActive_" + i
							+ "')]/div//div[@class='d-inline-block']/following-sibling::div", EndingSoonBanner);
					verifyNoElementDisplay(
							"//div[contains(@id,'leaseActive_" + i
									+ "')]/div//div[starts-with(@class,'d-flex')]/following-sibling::div//div/button",
							"Take Action Button");
					verifyPoipText(
							"//div[contains(@id,'leaseActive_" + i
									+ "')]//div[starts-with(@class,'d-flex')]/following-sibling::div//div/p[@role='term']",
							PoipText);
					Reporter.log("Verified Future POIP options");
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Poip Future POIP options");
		}
	}

	/**
	 * verify Poip Text for Future Poip - Paid one's
	 */
	public void verifyPoipText(String elementPath, String PoipText) {
		try {
			if (getDriver().findElement(By.xpath(elementPath)).getText().contains(PoipText)) {
				Reporter.log("Verified Poip Text and displayed as" + PoipText);
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify Poip Text for Poip Completed transaction");
		}
	}

	/**
	 * Clicking on Expanding all the Lease Details
	 */
	public void clickExpandAllLeaseDetails() {
		try {
			for (int i = 0; i < ExpandArrowIcon.size(); i++) {
				ExpandArrowIcon.get(i).click();
			}
		} catch (Exception e) {
			Assert.fail("ExpandArrow icon Not Found");
		}
	}

}