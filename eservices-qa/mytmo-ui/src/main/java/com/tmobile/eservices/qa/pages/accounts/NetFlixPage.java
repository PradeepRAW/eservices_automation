package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author Sudheer Reddy Chilukuri.
 *
 */
public class NetFlixPage extends CommonPage {

	public NetFlixPage(WebDriver webDriver) {
		super(webDriver);
	}

	@FindBy(id = "id_email")
	private WebElement emailText;

	@FindBy(id = "id_password")
	private WebElement passwordText;

	@FindBy(css = "button[data-uia='submitBtn']")
	private WebElement submitBtn;

	@FindBy(css = "button[id='mainButton']")
	private WebElement startWatchingBtn;

	@FindBy(css = "button[type='submit']")
	private WebElement continueBtnInDeviceSurvey;

	@FindBy(css = "input[name='profile1Name']")
	private WebElement nameFieldOne;

	@FindBy(css = "input[name='profile2Name']")
	private WebElement nameFieldTwo;

	@FindBy(css = "span[class='thumb-up'] svg")
	private List<WebElement> listOfImgsInonRamp;

	/**
	 * Enter Email address * @return
	 */
	public NetFlixPage enterEmailAddress(String data) {
		checkPageIsReady();
		try {
			sendTextData(emailText, data + getRandomNumberInRange(1000, 10000) + "@t-mobile.com");
			Reporter.log("Entered Email address");
		} catch (Exception e) {
			Assert.fail("Email address not displayed");
		}
		return this;
	}

	/**
	 * Enter Password address
	 * 
	 * @return
	 */
	public NetFlixPage enterPassword() {

		try {
			waitFor(ExpectedConditions.visibilityOf(passwordText));
			sendTextData(passwordText, "tmobil1");
			Reporter.log("Entered password Text");
		} catch (Exception e) {
			Assert.fail("Password filed is not displayed");
		}
		return this;
	}

	/**
	 * Click on Submit button
	 * 
	 * @return
	 */
	public NetFlixPage clickOnSubmitbtn() {
		try {
			submitBtn.click();
			Reporter.log("Clicked on Netflix Continue button");
		} catch (Exception e) {
			Assert.fail("Netflix Continue button is not displayed");
		}
		return this;
	}

	/**
	 * Click on Start Watching Button
	 * 
	 * @return
	 */
	public NetFlixPage clickOnStartWatchingBtn() {
		checkPageIsReady();
		try {
			startWatchingBtn.click();
			Reporter.log("Clicked on Start Watching button");
		} catch (Exception e) {
			Assert.fail("Start Watching button is not displayed");
		}
		return this;
	}

	/**
	 * Click on continue Button In Device Survey page
	 * 
	 * @return
	 */
	public NetFlixPage clickOnContinueBtnInDeviceSurvey() {
		checkPageIsReady();
		waitFor(ExpectedConditions.visibilityOf(continueBtnInDeviceSurvey));
		try {
			continueBtnInDeviceSurvey.click();
			Reporter.log("Clicked on continue Button");
		} catch (Exception e) {
			Assert.fail("Continue Button is not displayed");
		}
		return this;
	}

	/**
	 * Enter Name Field values
	 * 
	 * @return
	 */
	public NetFlixPage enterNameFieldValues() {
		checkPageIsReady();
		waitFor(ExpectedConditions.visibilityOf(nameFieldOne));
		try {
			sendTextData(nameFieldOne, "" + getRandomNumberInRange(10000, 100000));
			sendTextData(nameFieldTwo, "" + getRandomNumberInRange(10000, 100000));
			Reporter.log("Entered name one and two names ");
		} catch (Exception e) {
			Assert.fail("Name one and two fields are not displayed");
		}
		return this;
	}

	/**
	 * verify Netflix Home Page And Close
	 * 
	 * @return
	 */
	public NetFlixPage verifyNetflixHomePageAndClose() {
		checkPageIsReady();
		try {
			verifyCurrentPageURL("netflix.com", "netflix.com");
			Reporter.log("Netflix home page is displayed ");

			closeNewWindow();
			Reporter.log("Closed Netflix home page");

		} catch (Exception e) {
			Assert.fail("Netflix home page is not displayed");
		}
		return this;
	}

	/**
	 * Enter Name Field values
	 * 
	 * @return
	 */
	public NetFlixPage getNetflixComfirmationPage(MyTmoData myTmoData) {
		checkPageIsReady();
		try {
			switchToDefaultContent();
			getDriver().get("https://" + myTmoData.getEnv() + ".my.t-mobile.com/account/benefit-redemption/Netflix");
			checkPageIsReady();
			Reporter.log("Netflix confirmation page loaded");
		} catch (Exception e) {
			Assert.fail("Netflix confirmation page is not loaded");
		}
		return this;
	}
}
