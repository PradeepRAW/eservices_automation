package com.tmobile.eservices.qa.pages.accounts.api;
import org.json.JSONArray;
import org.json.JSONObject;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class EOSPartnerBenefits extends EOSCommonLib {






public Response getResponsepartnerbenefits(String alist[]) throws Exception {
	Response response=null;   
	
	    if(alist!=null) {
	
	//requestbody
	   
	    
	    JSONObject requestParams = new JSONObject();
		requestParams.put("accountNumber",alist[3] );
		requestParams.put("phoneNumber", alist[0]);
		
		JSONArray array = new JSONArray();
		array.put("Netflix");
		requestParams.put("partnerName", array);
		
		
		  
		RestService restService = new RestService(requestParams.toString(), eep.get(environment));
		
	//requestheaders	
		    restService.setContentType("application/json");
		    restService.addHeader("Authorization",alist[2]);		   
		    restService.addHeader("Accept", "application/json");
		    restService.addHeader("interactionid", "321321312");
		    restService.addHeader("Cache-Control", "no-cache");
		    restService.addHeader("Postman-Token", "eef93768-ed14-4076-afe5-9cbbf947a8f2");
		
		
	    
	    response = restService.callService("cx/v1/benefits ",RestCallType.POST);
	    response.asString();
	}

	    return response;
}



public String getisregistered(Response response) {
	String isregistered=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("benefits[0].isRegistered")!=null) {
        	return jsonPathEvaluator.get("benefits[0].isRegistered").toString();
	}}
		
	
	return isregistered;
}  

public String hasPremiumSOC(Response response) {
	String hasPremiumSOC=null;
	
	if (response.body().asString() != null && response.getStatusCode() == 200) {
		JsonPath jsonPathEvaluator=response.jsonPath();
	
		if(jsonPathEvaluator.get("benefits[0].hasPremiumSOC")!=null) {
        	return jsonPathEvaluator.get("benefits[0].hasPremiumSOC").toString();
	}}
		
	
	return hasPremiumSOC;
}





}
