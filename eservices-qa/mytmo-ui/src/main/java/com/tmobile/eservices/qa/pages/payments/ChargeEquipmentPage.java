package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 */
public class ChargeEquipmentPage extends CommonPage {

	@FindBy(css = ".d-none.d-lg-inline-block")
	private WebElement backToSummaryLink;

	@FindBy(css = "img[alt='back']")
	private WebElement mobileBackToSummaryLink;

	@FindBy(css = "i.fa.fa-angle-left")
	private WebElement arrowLeft;

	@FindBy(css = "i.fa.fa-angle-right")
	private WebElement arrowRight;

	@FindBy(css = "bb-slider div.bb-slider-item-amount")
	private List<WebElement> amountBlock;

	private final String pageUrl = "charges/category/charge/Equipment";

	@FindBy(css = "img.loader-icon")
	private WebElement pageSpinner;

	@FindBy(css = "span.bb-back")
	private WebElement pageLoadElement;

	@FindBy(xpath = "//bb-slider//div[contains(text(),'Equipment')]")
	private WebElement equipmentTabInSlider;

	@FindBy(linkText = "View equipment details")
	private WebElement viewEquipmentDetailsLink;

	@FindBy(css = "div.bb-charge-details")
	private WebElement lineEquipmentDetails;

	@FindBy(css = "div.bb-charge-detail-total")
	private WebElement totalAmountLabel;

	/**
	 * Constructor
	 *
	 * @param webDriver
	 */
	public ChargeEquipmentPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public ChargeEquipmentPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @throws InterruptedException
	 */
	public ChargeEquipmentPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			// waitFor(ExpectedConditions.invisibilityOf(pageSpinner));
			waitFor(ExpectedConditions.visibilityOf(pageLoadElement));
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("Fail to load Charge Equipment page");
		}
		return this;
	}

	/**
	 * Verify page elements
	 *
	 * @throws InterruptedException
	 */
	public ChargeEquipmentPage verifyChargeEquipmentPageElements() {
		try {
			Assert.assertTrue(backToSummaryLink.isDisplayed(), "Back To Summary link is not displayed");
			Assert.assertTrue(equipmentTabInSlider.isDisplayed(), "Equipment header is not displayed");
			Assert.assertTrue(arrowLeft.isDisplayed(), "Arrow left is not displayed");
			Assert.assertTrue(arrowRight.isDisplayed(), "Arrow right is not displayed");
			for (WebElement amount : amountBlock) {
				Assert.assertTrue(amount.getText().contains("$"), "Dollar sign is not displayed");
			}
		} catch (Exception e) {
			Assert.fail("Fail to verify Charge Equipment page elements");
		}
		return this;
	}

	/**
	 * verify that Equipment is displayed on the slider
	 * 
	 * @return
	 */
	public ChargeEquipmentPage verifyEquipmentInSlider() {
		try {
			equipmentTabInSlider.isDisplayed();
			Reporter.log("One Time Charge slider is displayed");
		} catch (Exception e) {
			Assert.fail("One Time Charge slider is not present");
		}
		return this;
	}

	/**
	 * Click on View Equipment details link.
	 *
	 * @throws InterruptedException
	 */
	public ChargeEquipmentPage clickViewEquipmentDetailsLink() {
		try {
			viewEquipmentDetailsLink.click();
			Reporter.log("Clicked on View included taxes and fees link.");
		} catch (Exception e) {
			Assert.fail("Fail to click on View included taxes and fees link.");
		}
		return this;
	}

	/**
	 * Verify if Equipment details assigned to lines are displayed
	 *
	 * @throws InterruptedException
	 */
	public ChargeEquipmentPage verifyEquipmentDetails() {
		try {
			lineEquipmentDetails.isDisplayed();
			Reporter.log("Equipment Details w.r.to lines is displayed.");
		} catch (Exception e) {
			Assert.fail("Equipment Details not found.");
		}
		return this;
	}

	/**
	 * Verify if total Amount label is displayed
	 *
	 * @throws InterruptedException
	 */
	public ChargeEquipmentPage verifyTotalAmount() {
		try {
			totalAmountLabel.isDisplayed();
			Reporter.log("Total Amount label is displayed.");
		} catch (Exception e) {
			Assert.fail("Total Amount label not found.");
		}
		return this;
	}

	/**
	 * Click Back to Summary page link
	 *
	 */
	public ChargeEquipmentPage clickBackToSummaryLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				mobileBackToSummaryLink.click();
			} else {
				backToSummaryLink.click();
			}
			Reporter.log("Clicked on Back To Summary Link.");
		} catch (Exception e) {
			Assert.fail("Fail to click on Back To Summary Link.");
		}
		return this;
	}

}