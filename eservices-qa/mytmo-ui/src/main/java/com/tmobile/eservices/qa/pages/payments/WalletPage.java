package com.tmobile.eservices.qa.pages.payments;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author pshiva
 *
 */
public class WalletPage extends CommonPage {

	private final String pageUrl = "mywallet";

	@FindBy(css = "#walletHead")
	private WebElement pageHeader;

	@FindBy(css = "pc-wallet .dialog")
	private WebElement modal;

	@FindBy(css = "#closeId")
	private WebElement closeCTA;

	@FindBy(xpath = "//p[contains(text(),'Add a bank')]")
	private WebElement addBankBlade;

	@FindBy(xpath = "//p[contains(text(),'Add a card')]")
	private WebElement addCardBlade;

	@FindBy(xpath = "//span[contains(@class,'Bank') or contains(@class,'bank')]/../..//div/p[contains(@class,'gray')]/span")
	private List<WebElement> storedBankNumber;

	@FindBy(xpath = "//span[contains(@class,'icon')]/../..//div/p[contains(@class,'gray')]/span")
	private List<WebElement> storedCardNumber;

	@FindBy(xpath = "//span[contains(@class,'icon') or contains(@class,'Bank')]/../../div/p/span[@pid or text()='Edit']")
	private List<WebElement> storedPayments;

	@FindBy(css = ".body.text-center.black span")
	private WebElement walletFullMsg;

	public WalletPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public WalletPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public WalletPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".circle")));
			verifyPageUrl();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			Reporter.log("Wallet Page is loaded.");
			Thread.sleep(5000);
		} catch (Exception e) {
			Assert.fail("Wallet page is not loaded");
		}
		return this;
	}

	public void clickCloseModal() {
		try {
			checkPageIsReady();
			modal.isDisplayed();
			waitFor(ExpectedConditions.textToBePresentInElement(closeCTA, "Close"));
			closeCTA.click();
			Reporter.log("Clicked on close modal CTA");
		} catch (Exception e) {
			Assert.fail("Failed to close modal");
		}
	}

	/**
	 * click on add a Bank blade
	 */
	public void clickAddBankBlade() {
		try {
			waitFor(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".circle")));
			addBankBlade.click();
			Reporter.log("Clicked on Add Bank blade");
		} catch (Exception e) {
			Assert.fail("Failed to click on Add Bank blade");
		}
	}

	/**
	 * click on add a card blade
	 */
	public void clickAddCardBlade() {
		try {
			addCardBlade.click();
			Reporter.log("Clicked on Add Card blade");
		} catch (Exception e) {
			Assert.fail("Failed to click on Add Card blade");
		}
	}

	/**
	 * verify stored bank
	 * 
	 * @param accNo
	 */
	public void verifyStoredBank(String accNo) {
		try {
			Boolean isStored = false;
			Thread.sleep(5000);
			for (WebElement storedBank : storedBankNumber) {
				if (accNo.contains(storedBank.getText().replaceAll("[^\\d.]", ""))) {
					Reporter.log("Verified that the Bank is stored");
					isStored = true;
					break;
				}
			}
			if (!isStored) {
				Assert.fail("Bank account " + accNo + " is not displayed on the wallet page");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify that the stored bank is displayed on wallet ");
		}
	}

	/**
	 * verify stored card
	 * 
	 * @param cardNo
	 */
	public void verifyStoredCard(String cardNo) {
		try {
			Boolean isStored = false;
			Thread.sleep(5000);
			for (WebElement storedCard : storedCardNumber) {
				if (cardNo.contains(storedCard.getText().replaceAll("[^\\d.]", ""))) {
					Reporter.log("Verified that the Card is stored");
					isStored = true;
					break;
				}
			}
			if (!isStored) {
				Assert.fail("Card number " + cardNo + " is not displayed on the wallet page");
			}
		} catch (Exception e) {
			Assert.fail("Failed to verify that the stored bank is displayed on wallet ");
		}
	}

	/**
	 * method to click any saved bank for validating edit card page
	 */
	public String verifyAndClickStoredBank() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(storedBankNumber.get(0)));
			String storedBankNo = storedBankNumber.get(0).getText();
			WebElement editLink = getDriver().findElement(By.xpath(
					"//span[contains(@class,'Bank') or contains(@class,'bank')]/../../div/p/span[contains(@class,'body-link')]"));
			waitFor(ExpectedConditions.visibilityOf(editLink));
			editLink.click();
			Reporter.log("Clicked on Stored Bank number: " + storedBankNo);
			return storedBankNo;
		} catch (Exception e) {
			Assert.fail("Failed to click on saved bank");
		}
		return null;
	}

	/**
	 * method to click any saved card for validating edit card page
	 */
	public String verifyAndClickStoredCard() {
		try {
			checkPageIsReady();
			String storedCardNo = storedCardNumber.get(0).getText().replaceAll("[^\\d.]", "");
			WebElement editLink = getDriver().findElement(
					By.xpath("//span[contains(@class,'icon')]/../../div/p//span[contains(@class,'body-link')]"));
			editLink.click();
			Reporter.log("Clicked on Stored card number: " + storedCardNo);
			return storedCardNo;
		} catch (Exception e) {
			Assert.fail("Failed to click on saved card");
		}
		return null;
	}

	/**
	 * click updated stored bank
	 * 
	 * @param storedBankNo
	 */
	public void clickUpdatedStoredBank(String storedBankNo) {
		try {
			for (WebElement storedBank : storedBankNumber) {
				if (storedBank.getText().contains(storedBankNo)) {
					String pid = storedBank.getAttribute("pid");
					WebElement editLink = getDriver().findElement(
							By.xpath("//span[@pid='" + pid + "']//..//../p/span[contains(@class,'cursor')]"));
					editLink.click();
					Reporter.log("Clicked on Stored bank number: " + storedBankNo);
					break;
				}
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on Edit link of Saved Bank");
		}
	}

	public void verifyDeletedPaymentMethodIsNotDisplayed(String storedCardNo) {
		try {
			for (WebElement storedPayment : storedPayments) {
				if (storedPayment.getText().contains(storedCardNo)) {
					Assert.fail("Deleted Stored payment method is still displayed on Spoke page");
				}
			}
			Reporter.log("Verified that the deleted Stored payment method is not displayed on Spoke page");
		} catch (Exception e) {
			Assert.fail("Failed to verify if Deleted Stored payment method is still displayed on Spoke page");
		}
	}

	public void clickUpdatedStoredCard(String storedCardNo) {
		try {
			if (storedCardNumber.get(0).getText().contains(storedCardNo)) {
				String pid = storedCardNumber.get(0).getAttribute("pid");
				WebElement editLink = getDriver()
						.findElement(By.xpath("//span[@pid='" + pid + "']//..//../p/span[contains(@class,'cursor')]"));
				editLink.click();
				Reporter.log("Clicked on Stored card number: " + storedCardNo);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public boolean verifyWalletIsFullMsg() {
		boolean isDisplayed = false;
		try {
			isDisplayed = walletFullMsg.isDisplayed();
			isDisplayed = walletFullMsg.getText().contains("Your Wallet is full");
			Reporter.log("Wallet full message is displayed");
		} catch (Exception e) {
			Reporter.log("Wallet full message is not displayed");
		}
		return isDisplayed;
	}

	/**
	 * method to verify any saved card for validating edit card page
	 */
	public Boolean verifyStoredCard() {
		try {
			return storedCardNumber.isEmpty();
		} catch (Exception e) {
			Assert.fail("Failed to verify if any saved card exist");
		}
		return false;
	}

	/**
	 * method to verify any saved card for validating edit card page
	 */
	public Boolean verifyStoredBank() {
		try {
			return storedBankNumber.isEmpty();
		} catch (Exception e) {
			Assert.fail("Failed to verify if any saved bank exist");
		}
		return false;
	}

}
