package com.tmobile.eservices.qa.pages.payments.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;



public class PaymentsWalletsV3 extends ApiCommonLib {

	/***
	 * Summary: This API is used do the payments  through OrchestrateOrders API
	 * Headers:Authorization,
	 * 	-Content-Type,
	 * 	-Postman-Token,
	 * 	-cache-control,
	 * 	-interactionId
	 * 	-workflowId
	 * 	
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */

	private Map<String, String> buildWalletApiHeader(ApiTestData apiTestData) throws Exception {
		//clearCounters(apiTestData);

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type","application/json");
		headers.put("Postman-Token", "3d6cc24e-47d1-4b68-90ff-ea6834704d6e");
		headers.put("ban", apiTestData.getBan());
		headers.put("Authorization", getSmallAccessToken());
		return headers;
	}



	public Response WalletCreation(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		headers.put("X-B3-SpanId", "6029108652_522");
		headers.put("X-B3-TraceId", "6029108652_511");
		headers.put("applicationId", "autopay");
		headers.put("application_id", "MYTMO");
		headers.put("channel", "DESKTOP");
		headers.put("channel_id", "123");
		headers.put("client_id", "A-B7-_lpqSOr_");
		headers.put("interactionId", "123123123");
		headers.put("tmo-id", "123");
		headers.put("usn", "1000021111");
		headers.put("workflowId", "signupeasypay");
		headers.put("msisdn", apiTestData.getMsisdn());
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("wallet/v1/"+apiTestData.getBan(), RestCallType.POST);
		System.out.println(response.asString());
		return response;

	}

	public Response WalletUpdate(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		headers.put("X-B3-SpanId", "6029108652_522");
		headers.put("X-B3-TraceId", "6029108652_511");
		headers.put("applicationId", "autopay");
		headers.put("application_id", "MYTMO");
		headers.put("channel", "DESKTOP");
		headers.put("interactionId","123123123");
		headers.put("channel_id", "123");
		headers.put("client_id", "A-B7-_lpqSOr_");
		headers.put("tmo-id", "123");
		headers.put("usn", "1000021111");
		headers.put("workflowId", "signupeasypay");
		headers.put("msisdn", apiTestData.getMsisdn());
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("wallet/v1/"+apiTestData.getBan(), RestCallType.PUT);
		System.out.println(response.asString());
		return response;

	}
	
	public Response WalletSearch(ApiTestData apiTestData,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		
		headers.put("channel_id", "TSL");
		headers.put("eosWalletCapacity", "2");
		headers.put("interactionId", "123456787");
		headers.put("isLimitedWalletCapacity", "false");
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("wallet/v1/"+apiTestData.getBan(), RestCallType.GET);
		System.out.println(response.asString());
		return response;

	}

	public Response WalletDelete(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildWalletApiHeader(apiTestData);
		headers.put("X-B3-SpanId", "6029108652_522");
		headers.put("X-B3-TraceId", "6029108652_511");
		headers.put("applicationId", "autopay");
		headers.put("application_id", "MYTMO");
		headers.put("channel", "DESKTOP");
		headers.put("channel_id", "123");
		headers.put("client_id", "A-B7-_lpqSOr_");
		headers.put("interactionId", "123123123");
		headers.put("tmo-id", "123");
		headers.put("usn", "1000021111");
		headers.put("workflowId", "signupeasypay");
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("wallet/v1/delete/"+apiTestData.getBan()+"?msisdn="+apiTestData.getMsisdn(), RestCallType.POST);
		System.out.println(response.asString());
		return response;

	}


}
