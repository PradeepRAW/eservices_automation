package com.tmobile.eservices.qa.pages.shop;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class MyPhoneDetailsPage extends CommonPage {

	@FindBy(id = "MP_device_title")
	private WebElement phoneDetailsPage;

	/**
	 * 
	 * @param webDriver
	 */
	public MyPhoneDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * 
	 * @return
	 */
	public MyPhoneDetailsPage verifyPhoneDetailsPage() {
		checkPageIsReady();
		try {
			phoneDetailsPage.isDisplayed();
			Reporter.log("PhoneDetailsPage is displayed");
		} catch (Exception e) {
			Assert.fail("PhoneDetailsPage is not displayed");
		}
		return this;
	}
}
