package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

public class FeaturedPlanPage extends CommonPage {

	@FindBy(css = "#PlanServicesDiv_id .ui_mobile_headline")
	private WebElement featureplanPageHeader;

	@FindBy(css = "#IOTCalcareModalMsg > span.hidden-xs-mobile")
	private WebElement changePlanErrorforMobile;

	@FindBy(css = "p[id='IOTCalcareModalMsg'] span")
	private List<WebElement> changePlanError;

	@FindBy(css = "div.learn-more-div-class a#sharedPlans_id1 img")
	private List<WebElement> planFeatureTaxInclusive;

	/**
	 * 
	 * @param webDriver
	 */
	public FeaturedPlanPage(WebDriver webDriver) {
		super(webDriver);
	}

	public FeaturedPlanPage verifyFeaturedPlanPage() {
		try {
			checkPageIsReady();
			waitForFeaturedPlanPageSpinnerInvisibility();
			// Assert.assertTrue(featureplanPageHeader.isDisplayed());
			Reporter.log("feature plan page is displayed");
		} catch (Exception e) {
			Assert.fail("feature plan page not displayed");
		}
		return this;
	}

	/**
	 * invisibility
	 * 
	 * @return
	 */
	public void waitForFeaturedPlanPageSpinnerInvisibility() {
		waitforSpinner();
		waitForImageSpinnerInvisibility();
	}

	/**
	 * click Plan Feature Tax Inclusive
	 */

	public FeaturedPlanPage clickPlanFeatureTaxInclusive() {
		checkPageIsReady();
		waitForFeaturedPlanPageSpinnerInvisibility();
		try {
			if (!planFeatureTaxInclusive.isEmpty()) {
				if (getDriver() instanceof AppiumDriver) {
					planFeatureTaxInclusive.get(2).click();
					Reporter.log("Clicked on Feature tax exclusive");
				} else {
					planFeatureTaxInclusive.get(0).click();
					Reporter.log("Clicked on Feature tax exclusive");
				}
			}
		} catch (Exception e) {
			Assert.fail("Feature tax exclusive is not clciked");
		}
		return this;
	}

}
