package com.tmobile.eservices.qa.pages.payments.api;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource.PSpecified;
import javax.xml.bind.DatatypeConverter;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
public class EOSencription {


	
	
	  public static String RsaEncryption(String kty1,String e1,String n1,String CCNumber) throws Exception {

	       String jsonString="{\r\n" +
	               "                \"kty\": \""+kty1+"\",\r\n" +
	               "                \"e\": \""+e1+"\",\r\n" +
	               "                \"n\": \""+n1+"\"\r\n" +
	               "            }";
	       PublicKey pub=null;
	       ObjectMapper mapper = new ObjectMapper();
	        JsonNode readTree = mapper.readTree(jsonString);
	        if(!readTree.isMissingNode() && readTree.has("e")){
	            String e = readTree.path("e").asText();
	            String n = readTree.path("n").asText();
	            System.out.println("E::" + e + "::n::" + n);
	            byte exponentB[] = Base64.getUrlDecoder().decode(e);
	            byte modulusB[] = Base64.getUrlDecoder().decode(n);
	            BigInteger exponent = new BigInteger(toHexFromBytes(exponentB), 16);
	            BigInteger modulus = new BigInteger(toHexFromBytes(modulusB), 16);
	            RSAPublicKeySpec spec = new RSAPublicKeySpec(modulus, exponent);
	            KeyFactory factory = KeyFactory.getInstance("RSA");
	            pub = factory.generatePublic(spec);
	         }
	        String card = CCNumber;
	        //String card = "4358805977433343";
	        // --- encrypt given OAEPParameterSpec
	        Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPPadding");
	        OAEPParameterSpec oaepParams = new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256, PSpecified.DEFAULT);
	        cipher.init(Cipher.ENCRYPT_MODE, pub, oaepParams);
	        System.out.println(cipher.getProvider());
	        System.out.println(cipher.getAlgorithm());
	        byte[] encryptedCard = cipher.doFinal(card.getBytes("UTF-8"));

	        System.out.println("Encrypted Card Details:: " + DatatypeConverter.printHexBinary(encryptedCard));

	       return DatatypeConverter.printHexBinary(encryptedCard);

	    }
	    
	    
	    private static String toHexFromBytes(byte[] exponentB) {
	         return DatatypeConverter.printHexBinary(exponentB);
	    }


}
