package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class AlertsApi extends ApiCommonLib{
	
	/***
	 * Alerts BFF API.
	 * parameters:
    *	-  $ref: '#/parameters/Authorization'
	*	-  $ref: '#/parameters/Channel'
	*	-  $ref: '#/parameters/Channel-Version'
	*	-  $ref: '#/parameters/User-Token'
	*	-  $ref  '#/parameters/Tmo-Id'
	*	-  $ref: '#/parameters/Phone-Number'
	*	-  $ref: '#/parameters/Billing-Account-Number'
	*	-  $ref: '#/parameters/session-num'
	*	-  $ref: '#/parameters/Content-Type'
	*	-  $ref: '#/parameters/X-B3-TraceId'
	*	-  $ref: '#/parameters/X-B3-SpanId'
	*	-  $ref: '#/parameters/Cache-Control'

	 * @return
	 * @throws Exception 
	 */
    public Response fetchCustomerAlerts(ApiTestData apiTestData, String requestBody) throws Exception{
    	Map<String, String> headers = buildAlertAPIHeader(apiTestData);
    	String resourceURL = "v1/alerts/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
        System.out.println(response.asString());
         return response;
    }       
      
    private Map<String, String> buildAlertAPIHeader(ApiTestData apiTestData) throws Exception {
    	final String accessToken = getAccessToken();
		Map<String, String> headers = new HashMap<String, String>();				
		headers.put("Authorization", accessToken);
		headers.put("Channel", "aaa");
		headers.put("Channel-Version", "aaa");
		headers.put("User-Token", "some_my_tmo_token_if_you_want_12345");
		headers.put("Tmo-Id", "U-9f4bc262-ffa7-4d1c-9800-f4f871cb1ed9");
		headers.put("Phone-Number", apiTestData.getMsisdn());
		headers.put("Billing-Account-Number", apiTestData.getBan());
		headers.put("session-num", "aaa");
		headers.put("Content-Type", "application/json");
		headers.put("X-B3-TraceId", "FlexAPITest123");
		headers.put("X-B3-SpanId", "FlexAPITest456");
		headers.put("Cache-Control", "no-cache");
		headers.put("transactionid","FlexAPITest");
		return headers;
	}

}
