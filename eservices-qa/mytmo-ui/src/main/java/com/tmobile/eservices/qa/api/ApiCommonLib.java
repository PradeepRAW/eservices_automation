package com.tmobile.eservices.qa.api;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URL;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.MGF1ParameterSpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.crypto.Cipher;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource.PSpecified;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.response.Header;
import com.jayway.restassured.response.Headers;
import com.tmobile.eqm.testfrwk.ui.core.config.Config;
import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eqm.testfrwk.ui.core.service.ServiceTest;
import com.tmobile.eservices.qa.api.eos.JWTTokenApi;
import com.tmobile.eservices.qa.api.exception.ServiceFailureException;
import com.tmobile.eservices.qa.api.unlockdata.RequestData;
import com.tmobile.eservices.qa.common.Constants;
import com.tmobile.eservices.qa.data.ApiTestData;
import com.tmobile.eservices.qa.listeners.Verify;
import com.tmobile.eservices.qa.pages.tmng.api.JWTTokenEncodingAPI;

import groovy.json.StringEscapeUtils;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ApiCommonLib extends ServiceTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ApiCommonLib.class);
	public Map<String, String> eep;
	public Map<String, String> sl;
	public Map<String, String> yext;
	public Map<String, String> mep;
	public Map<String, String> cturl;
	public Map<String, String> clearCounterAccessTokenUrl;
	public Map<String, String> brassUserTokenResourceUrl;
	public Map<String, String> brassCodeResourceUrl;
	public static HashMap<String, String> jwtUserTokenUrl;
	public static String eosBrassUserTokenUrl;
	public static String eosBrassCodeUrl;
	public static Map<String, String> ah;
	public static Map<String, String> oAuth;
	public static Map<String, String> jwt;
	public static Map<String, String> soapUrl;
	public static Map<String, String> soapActions;
	public static HashMap<String, String> tmngApiClientIDMap;
	public static String eos_base_url;
	public static String eos_base_tmng_url;
	public static String tmng_base_url;
	public static String yext_base_url;
	public static String meezo_base_url;
	public static String backend = null;
	public static String environment = null;
	public static Map<String, String> tokenMapCommon;
	public static String eos_clear_token_url;
	public static String eosClearCounterAccessTokenUrl;
	public static Map<String, String> exceptionMsgs;
	public static Map<String, String> scopeInfo;

	/***
	 * Map for EOS end points
	 */
	public ApiCommonLib() {

		// tokenMapCommon = new HashMap<String, String>();

		yext = new HashMap<String, String>();
		yext.put("yext", "https://api.yext.com");

		eep = new HashMap<String, String>();
		eep.put("ms_qat03_ls_tmng", "https://qat03-pd.api.t-mobile.com");
		eep.put("ms_stage", "https://stage.eos.corporate.t-mobile.com");
		eep.put("ms_ppd", "https://stage.eos.corporate.t-mobile.com");
		eep.put("ms_stage2", "https://stage2.eos.corporate.t-mobile.com");
		eep.put("ms_qlab01", "https://qlab01.eos.corporate.t-mobile.com");
		eep.put("ms_qlab02", "https://qlab02.eos.corporate.t-mobile.com");
		eep.put("ms_qlab03", "https://qlab03.eos.corporate.t-mobile.com");
		eep.put("ms_qlab06", "https://qlab06.eos.corporate.t-mobile.com");
		eep.put("ms_qlab07", "https://qlab07.eos.corporate.t-mobile.com");
		eep.put("ms_prod", "https://eos.corporate.t-mobile.com");
		eep.put("ms_dev", "https://dev.eos.corporate.t-mobile.com");
		eep.put("ms_ilab02", "https://ilab02.core.op.api.t-mobile.com");
		eep.put("ms_aws_awsdev", "https://9yu1woe589.execute-api.us-west-2.amazonaws.com/sandbox");
		eep.put("ms_aws_awsprod", "https://onmyj41p3c.execute-api.us-west-2.amazonaws.com/prod");
		eep.put("ms_aws_awsstage", "https://kkcdrrnxwk.execute-api.us-west-2.amazonaws.com/dev");
		eep.put("ms_stgtleims_0002", "http://stgtleims0003.unix.gsm1900.org:10002");
		eep.put("ms_stgtleims_0003", "http://stgtleims0003.unix.gsm1900.org:10003");
		eep.put("ms_qlab03_ccp", "https://qlab03.eos.ccp.t-mobile.com");
		eep.put("ms_qlab02_ccp", "https://qlab02.eos.ccp.t-mobile.com");
		eep.put("ms_qat03", "https://qlab03.eos.ccp.t-mobile.com");
		eep.put("ms_tmng_prod", "https://api.t-mobile.com");
		eep.put("ms_tmng_qatProd", "https://dit03-pd.api.t-mobile.com");
		eep.put("ms_tmng_stage2", "https://stg02.api.t-mobile.com");
		eep.put("ms_tmobile_UNO", "https://qlab02.eos.ccp.t-mobile.com");

		cturl = new HashMap<String, String>();
		cturl.put("ms_prod", "https://eui.ws.iam.msg.t-mobile.com");
		cturl.put("ms_stage", "https://eui.ws.iam.msg.t-mobile.com");
		cturl.put("ms_stage2", "https://eui.ws.iam.msg.t-mobile.com");
		cturl.put("ms_ppd", "https://eui.ws.iam.msg.t-mobile.com");
		cturl.put("ms_qlab01", "https://auth3.iam.msg.lab.t-mobile.com");
		// cturl.put("ms_qlab02", "https://auth14.iam.msg.lab.t-mobile.com");
		cturl.put("ms_qlab02", "https://ws14.iam.msg.lab.t-mobile.com");
		cturl.put("ms_qlab03", "https://iam2lab11.msg.t-mobile.com");
		cturl.put("ms_stage2", "https://eui.ws.iam.msg.t-mobile.com");

		clearCounterAccessTokenUrl = new HashMap<String, String>();
		clearCounterAccessTokenUrl.put("ms_prod", "https://eui.ws.iam.msg.t-mobile.com");
		clearCounterAccessTokenUrl.put("ms_stage", "https://eui.ws.iam.msg.t-mobile.com");
		clearCounterAccessTokenUrl.put("ms_stage2", "https://eui.ws.iam.msg.t-mobile.com");
		clearCounterAccessTokenUrl.put("ms_ppd", "https://eui.ws.iam.msg.t-mobile.com");
		clearCounterAccessTokenUrl.put("ms_qlab02", "https://auth14.iam.msg.lab.t-mobile.com");
		clearCounterAccessTokenUrl.put("ms_qlab03", "https://iam2lab11.msg.t-mobile.com");
		clearCounterAccessTokenUrl.put("ms_qlab01", "https://auth3.iam.msg.lab.t-mobile.com");

		oAuth = new HashMap<String, String>();
		oAuth.put("qlab02", "https://qlab02.core.op.api.t-mobile.com");
		oAuth.put("qlab03", "https://qlab03.core.op.api.t-mobile.com");
		oAuth.put("stage", "https://core.op.api.internal.t-mobile.com");
		oAuth.put("ppd", "https://core.op.api.internal.t-mobile.com");
		oAuth.put("stage2", "https://core.op.api.internal.t-mobile.com");
		oAuth.put("prod", "https://core.op.api.internal.t-mobile.com");
		oAuth.put("dev", "https://qlab02.core.op.api.t-mobile.com");
		oAuth.put("qlab01", "https://qlab01.core.op.api.t-mobile.com");
		oAuth.put("qlab06", "https://qlab06.core.op.api.geo.t-mobile.com");
		oAuth.put("qlab07", "https://dlab03.core.op.api.t-mobile.com");
		oAuth.put("ilab02", "https://ilab02.core.op.api.t-mobile.com");
		oAuth.put("qlab03_ccp", "https://qlab03.core.op.api.t-mobile.com");
		oAuth.put("qat03", "https://qat03.api.t-mobile.com");
		oAuth.put("tmng_prod", "https://api.t-mobile.com");
		oAuth.put("tmng_stage2", "https://stg02.api.t-mobile.com");
		oAuth.put("prod_accesstoken_resource_url", "/oauth2-api/p/v1/token");
		oAuth.put("ppd_accesstoken_resource_url", "/oauth2-api/p/v1/token");
		oAuth.put("stage_accesstoken_resource_url", "/oauth2-api/p/v1/token");
		oAuth.put("stage2_accesstoken_resource_url", "/oauth2-api/p/v1/token");
		oAuth.put("qlab01_accesstoken_resource_url", "/oauth2/v1/token");
		oAuth.put("qlab02_accesstoken_resource_url", "/oauth2/v1/token");
		oAuth.put("qlab06_accesstoken_resource_url", "/oauth2/v1/token");
		oAuth.put("qlab03_accesstoken_resource_url", "/oauth2/v1/token");
		oAuth.put("prod_accesstoken_auth", "Basic QS1TR2xHZDE0LWl6MDp0dlZsUEZESG9p");
		oAuth.put("ppd_accesstoken_auth", "Basic QS1TR2xHZDE0LWl6MDp0dlZsUEZESG9p");
		oAuth.put("stage_accesstoken_auth", "Basic QS1TR2xHZDE0LWl6MDp0dlZsUEZESG9p");
		oAuth.put("stage2_accesstoken_auth", "Basic QS1TR2xHZDE0LWl6MDp0dlZsUEZESG9p");
		oAuth.put("qlab01_accesstoken_auth", "Basic TVlUTU86TVlUTU9TZWNyZXQ=");
		oAuth.put("qlab02_accesstoken_auth", "Basic TVlUTU86TVlUTU9TZWNyZXQ=");
		oAuth.put("qlab03_accesstoken_auth", "Basic TVlUTU86TVlUTU9TZWNyZXQ=");
		oAuth.put("qlab06_accesstoken_auth", "Basic TVlUTU86TVlUTU9TZWNyZXQ=");
		oAuth.put("stage2_accesstoken_auth", "Basic QS1TR2xHZDE0LWl6MDp0dlZsUEZESG9p");
		oAuth.put("qlab02_ccp", "https://qlab03.core.op.api.t-mobile.com");
		oAuth.put("tmobile_UNO", "https://tmobileqat-qat03-pro.apigee.net");
		oAuth.put("qlab06_ccp", "https://qlab06.eos.corporate.t-mobile.com");

		jwt = new HashMap<String, String>();
		jwt.put("qlab02", "https://uat.brass.account.t-mobile.com");
		jwt.put("stage", "https://brass.account.t-mobile.com");
		jwt.put("stage2", "https://brass.account.t-mobile.com");
		jwt.put("prod", "https://brass.account.t-mobile.com");
		jwt.put("qlab03", "https://qat.brass.account.t-mobile.com");
		jwt.put("qlab06", "https://uat.brass.account.t-mobile.com");
		jwt.put("qlab01", "https://brs3.brass.account.t-mobile.com");

		jwtUserTokenUrl = new HashMap<String, String>();
		jwtUserTokenUrl.put("qlab02", jwt.get("qlab02"));
		jwtUserTokenUrl.put("stage", jwt.get("stage"));
		jwtUserTokenUrl.put("stage2", jwt.get("stage2"));
		jwtUserTokenUrl.put("prod", jwt.get("prod"));
		jwtUserTokenUrl.put("qlab03", jwt.get("qlab03"));
		jwtUserTokenUrl.put("qlab06", jwt.get("qlab06"));
		// qlab01 is still having brass URL as host for user token service
		jwtUserTokenUrl.put("qlab01", jwt.get("qlab01"));

		brassCodeResourceUrl = new HashMap<String, String>();
		brassCodeResourceUrl.put("qlab02", "ras/v3/code");
		brassCodeResourceUrl.put("stage", "ras/v1/code");
		brassCodeResourceUrl.put("stage2", "ras/v1/code");
		brassCodeResourceUrl.put("prod", "ras/v1/code");
		brassCodeResourceUrl.put("qlab03", "ras/v3/code");
		brassCodeResourceUrl.put("qlab06", "ras/v1/code");
		brassCodeResourceUrl.put("qlab01", "ras/v1/code");

		brassUserTokenResourceUrl = new HashMap<String, String>();
		brassUserTokenResourceUrl.put("qlab02", "tms/v3/usertoken");
		brassUserTokenResourceUrl.put("stage", "tms/v3/usertoken");
		brassUserTokenResourceUrl.put("stage2", "tms/v3/usertoken");
		brassUserTokenResourceUrl.put("prod", "tms/v3/usertoken");
		brassUserTokenResourceUrl.put("qlab03", "tms/v3/usertoken");
		brassUserTokenResourceUrl.put("qlab01", "tms/v3/usertoken");
		brassUserTokenResourceUrl.put("qlab06", "tms/v3/usertoken");

		tmngApiClientIDMap = new HashMap<String, String>();
		tmngApiClientIDMap.put("tmng_prod", "hV6lfg3EBAiVX5EZjbpexLoLhGhuGiWR");
		tmngApiClientIDMap.put("tmng_qatProd", "regf2J8znJpfzOZjSijG9YiWwBtJc0BO");
		tmngApiClientIDMap.put("tmng_stage2", "hV6lfg3EBAiVX5EZjbpexLoLhGhuGiWR");

		scopeInfo = new HashMap<String, String>();
		scopeInfo.put("qlab02",
				"openid extended_lines associated_billing_accounts offer TMO_ID_profile token customer_addresses billing_information iam_account_lock_information vault associated_customers associated_lines token_validation");
		scopeInfo.put("stage",
				"TMO_ID_profile associated_lines billing_information associated_billing_accounts extended_lines token openid");
		scopeInfo.put("stage2",
				"TMO_ID_profile associated_lines billing_information associated_billing_accounts extended_lines token openid");
		scopeInfo.put("prod",
				"TMO_ID_profile associated_lines billing_information associated_billing_accounts extended_lines token openid");
		scopeInfo.put("qlab03",
				"openid extended_lines associated_billing_accounts offer TMO_ID_profile permission token customer_addresses billing_information iam_account_lock_information vault token_validation associated_customers associated_lines customer_party_identification");
		scopeInfo.put("qlab01",
				"openid extended_lines associated_billing_accounts permission TMO_ID_profile auto_generated_flag_write offer token customer_addresses billing_information iam_account_lock_information token_validation associated_customers email associated_lines customer_party_identification");
		scopeInfo.put("qlab01_UserToken", "TMO_ID_profile associated_lines extended_lines");
		scopeInfo.put("qlab06",
				"TMO_ID_profile associated_lines billing_information associated_billing_accounts extended_lines token openid");

			
		/***
		 * Map for basic headers
		 */
		ah = new HashMap<String, String>();
		ah.put("dev", "Basic NU1vSDJ0VlBEOVBPSkdKUmlZR3pOckwzODQzU3U2RU46ZFRPN0hiTndBMDJyZ2VBdA==");
		ah.put("qlab01", "Basic NU1vSDJ0VlBEOVBPSkdKUmlZR3pOckwzODQzU3U2RU46ZFRPN0hiTndBMDJyZ2VBdA==");
		// ah.put("qlab02", "Basic
		// R3FiNUNmTUpQMThuUW80TWNZSlV5TUdKTzk3ZTg0YWU6b3pQWGFkNUM5WFI3dnIxMg==");
		ah.put("qlab02",
				"Basic ZjNkMjc3YTE3NzdlNGVkNjc5N2ZmZjhhNzMwNTFmNzg3NDk3OmpDaEUwc09UdHNEOUh0VlRUY1ExNVNMdnVMWEFNVGphU2kxdUVsZmxmQWJ4ZGtVUU81");
		// ah.put("qlab03","Basic
		// Y1V0aHdHTGoyWEZ5b0RpMmRoQUZ5YUVFQVRQUEZ0anM6ckR2STgzQUlBTlhuQWt4SA==");
		// ah.put("qlab03", "Basic
		// R3FiNUNmTUpQMThuUW80TWNZSlV5TUdKTzk3ZTg0YWU6b3pQWGFkNUM5WFI3dnIxMg==");

		ah.put("qat03", "Basic VHFCbXJTUlhZVDFsWm9CblZJMG0yOVBRR25nOWk2R1A6R21CRFNXVHhEbllwOElHbw==");
		ah.put("qlab03", "Basic NU1vSDJ0VlBEOVBPSkdKUmlZR3pOckwzODQzU3U2RU46ZFRPN0hiTndBMDJyZ2VBdA==");
		ah.put("qlab03_ccp", "Basic VHFCbXJTUlhZVDFsWm9CblZJMG0yOVBRR25nOWk2R1A6R21CRFNXVHhEbllwOElHbw==");
		// ah.put("qlab03", "Basic
		// VHFCbXJTUlhZVDFsWm9CblZJMG0yOVBRR25nOWk2R1A6R21CRFNXVHhEbllwOElHbw==");
		ah.put("qlab03", "Basic d1hPcnM0V3o0MkdmRkRWYXBiNU1GTHRlVkdJNlJraDQ6cGg2QVlaYUZHRTFCR3NZTw==");
		ah.put("qlab06", "Basic R3FiNUNmTUpQMThuUW80TWNZSlV5TUdKTzk3ZTg0YWU6b3pQWGFkNUM5WFI3dnIxMg==");
		ah.put("qlab07", "Basic T2NBY0g5N3Q0Tm50VDJtYWl1dFhiaVdqa09rTHZwcDg6M2hMTXFzOGpqR3g5dTRCeg==");
		ah.put("stage", "Basic Uzg4U05KdmZLMVZCUEZvVldsMDVDSDF0YU1MRWJCTnM6d0hscEpXOHY2UGRNQm5DdQ==");
		ah.put("ppd", "Basic Uzg4U05KdmZLMVZCUEZvVldsMDVDSDF0YU1MRWJCTnM6d0hscEpXOHY2UGRNQm5DdQ==");
		ah.put("stage2", "Basic Uzg4U05KdmZLMVZCUEZvVldsMDVDSDF0YU1MRWJCTnM6d0hscEpXOHY2UGRNQm5DdQ==");
		ah.put("stage2_small", "Basic Uzg4U05KdmZLMVZCUEZvVldsMDVDSDF0YU1MRWJCTnM6d0hscEpXOHY2UGRNQm5DdQ==");
		ah.put("prod", "Basic Uzg4U05KdmZLMVZCUEZvVldsMDVDSDF0YU1MRWJCTnM6d0hscEpXOHY2UGRNQm5DdQ==");
		ah.put("ilab02", "Basic TjI3c01HYjNYU3J0STAwcVpTS1c4TlU1SXEyMVhGU1M6N1FjeHhnVXBucHlSb0tPaw==");
		ah.put("qlab03_small", "Basic NU1vSDJ0VlBEOVBPSkdKUmlZR3pOckwzODQzU3U2RU46ZFRPN0hiTndBMDJyZ2VBdA==");
		ah.put("tmobile_UNO", "Basic RjRGMEVXVEtXeXgwRUhicFpuNTA2ZDBIeTBaRDFUWk46SWU5TjBOT0cxdlNsc3gwcQ==");
		ah.put("stgtleims_0003", "Basic ZWJpbGw6cGE4OGIxbGx6");
		ah.put("stgtleims_0002", "Basic ZWJpbGw6cGE4OGIxbGx6");
		ah.put("tmngV3Prod", "Basic allmUHVjZGFEZjI0VWJHSzZCdTZBWTdTeFVuTmNiN0o6cWdwYXh5OXRsM0VZeXZUSw==");
		ah.put("tmng_prod", "Basic allmUHVjZGFEZjI0VWJHSzZCdTZBWTdTeFVuTmNiN0o6cWdwYXh5OXRsM0VZeXZUSw==");
		ah.put("tmng_stage2", "Basic allmUHVjZGFEZjI0VWJHSzZCdTZBWTdTeFVuTmNiN0o6cWdwYXh5OXRsM0VZeXZUSw==");

		/***
		 * Map for meezo end points
		 */
		mep = new HashMap<String, String>();
		// #ppd
		mep.put("meezo_usage_ppd", "https://mezppdusag.mezzo.eservice.t-mobile.com:8082/ws/usageservice.wsdl");
		mep.put("meezo_phone_ppd", "https://mezppdphon.mezzo.eservice.t-mobile.com:8084/ws/phoneservice.wsdl");
		mep.put("meezo_plan_ppd", "https://mezppdplan.mezzo.eservice.t-mobile.com:8081/ws/planservice.wsdl");
		mep.put("meezo_profile_ppd", "https://mezppdprof.mezzo.eservice.t-mobile.com:8083/ws/profileservice.wsdl");
		mep.put("meezo_billing_ppd", "https://mezppdbill.mezzo.eservice.t-mobile.com:8086/ws/billingservice.wsdl");
		mep.put("meezo_shop_ppd", "https://mezppdshop.mezzo.eservice.t-mobile.com:8087/ws/shopservice.wsdl");

		// #PROD
		mep.put("mz_prod_usage", "https://usag_mezzo_eservice_t-mobile_com/ws/usageservice");
		mep.put("mz_prod_phone", "https://phon_mezzo_eservice_t-mobile_com/ws/phoneservice");
		mep.put("mz_prod_plan", "https://plan_mezzo_eservice_t-mobile_com/ws/planservice");
		mep.put("mz_prod_profile", "https://prof_mezzo_eservice_t-mobile_com/ws/profileservice");
		mep.put("mz_prod_billing", "https://bill_mezzo_eservice_t-mobile_com/ws/billingservice");
		mep.put("mz_prod_shop", "https://shop_mezzo_eservice_t-mobile_com/ws/shopservice");

		// #qlab02
		mep.put("mz_qlab02_usage", "http://mezqlab02usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice.wsdl");
		mep.put("mz_qlab02_phone", "http://mezqlab02phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice.wsdl");
		mep.put("mz_qlab02_plan", "http://mezqlab02plan_mezzo_eservice_t-mobile_com:8081/ws/planservice.wsdl");
		mep.put("mz_qlab02_profile", "http://mezqlab02prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice.wsdl");
		mep.put("mz_qlab02_billing", "http://mezqlab02bill.mezzo.eservice.t-mobile.com:8086/ws/billingservice.wsdl");
		mep.put("mz_qlab02_shop", "http://mezqlab02shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice.wsdl");

		// #qlab03
		mep.put("mz_qlab03_usage", "http://mezqlab03usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice.wsdl");
		mep.put("mz_qlab03_phone", "http://mezqlab03phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice.wsdl");
		mep.put("mz_qlab03_plan", "http://mezqlab03plan_mezzo_eservice_t-mobile_com:8081/ws/planservice.wsdl");
		mep.put("mz_qlab03_profile", "http://mezqlab03prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice.wsdl");
		mep.put("mz_qlab03_billing", "http://mezqlab03bill.mezzo.eservice.t-mobile.com:8086/ws/billingservice.wsdl");
		mep.put("mz_qlab03_shop", "http://mezqlab03shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice.wsdl");

		// #dev2
		mep.put("mz_dev2_usage", "https://mezdev2usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice_wsdl");
		mep.put("mz_dev2_phone", "https://mezdev2phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("mz_dev2_plan", "https://mezdev2plan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("mz_dev2_profile", "https://mezdev2prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("mz_dev2_billing", "https://mezdev2bill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("mz_dev2_shop", "https://mezdev2shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");

		// #dev
		mep.put("mz_dev_usage", "https://mezdev2usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice_wsdl");
		mep.put("mz_dev_phone", "https://mezdev2phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("mz_dev_plan", "https://mezdev2plan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("mz_dev_profile", "https://mezdev2prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("mz_dev_billing", "https://mezdev2bill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("mz_dev_shop", "https://mezdev2shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");

		// #qlab01
		mep.put("mz_qlab01_usage", "http://mezqlab06usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice_wsdl");
		mep.put("mz_qlab01_phone", "http://mezqlab06phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("mz_qlab01_plan", "http://mezqlab06plan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("mz_qlab01_profile", "http://mezqlab06prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("mz_qlab01_billing", "http://mezqlab06bill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("mz_qlab01_shop", "http://mezqlab06shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");

		// #qlab06
		mep.put("mz_qlab06_phone", "http://mezqlab07phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("mz_qlab06_plan", "http://mezqlab07plan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("mz_qlab06_profile", "http://mezqlab07prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("mz_qlab06_usage", "http://mezqlab07usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice_wsdl");
		mep.put("mz_qlab06_billing", "http://mezqlab07bill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("mz_qlab06_shop", "http://mezqlab07shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");

		// #qlab07
		mep.put("mz_qlab07_phone", "http://mezqlab01phon_mezzo_eservice_t-mobile_com:8084/ws/phoneservice_wsdl");
		mep.put("mz_qlab07_plan", "http://mezqlab01plan_mezzo_eservice_t-mobile_com:8081/ws/planservice_wsdl");
		mep.put("mz_qlab07_profile", "http://mezqlab01prof_mezzo_eservice_t-mobile_com:8083/ws/profileservice_wsdl");
		mep.put("mz_qlab07_usage", "http://mezqlab01usag_mezzo_eservice_t-mobile_com:8082/ws/usageservice_wsdl");
		mep.put("mz_qlab07_billing", "http://mezqlab01bill_mezzo_eservice_t-mobile_com:8086/ws/billingservice_wsdl");
		mep.put("mz_qlab07_shop", "http://mezqlab01shop_mezzo_eservice_t-mobile_com:8087/ws/shopservice_wsdl");

		soapUrl = new HashMap<String, String>();
		// soapUrl.put("creditCheckRequest",
		// "https://qattbco533.unix.gsm1900.org:5116/genericesp/soap");
		// soapUrl.put("creditCheckRequest", mep.get("meezo_billing_ppd"));
		// soapUrl.put("creditCheckRequest", mep.get("mz_qlab03_billing"));

		if ("qlab03".equalsIgnoreCase(System.getProperty("environment"))) {
			soapUrl.put("creditCheckRequest", mep.get("mz_qlab03_billing"));
		} else if ("qlab02".equalsIgnoreCase(System.getProperty("environment"))) {
			soapUrl.put("creditCheckRequest", mep.get("mz_qlab02_billing"));
		} else {
			soapUrl.put("creditCheckRequest", mep.get("meezo_billing_ppd"));
		}

		soapUrl.put("getLines", mep.get("meezo_shop_ppd"));
		soapUrl.put("getPlan", mep.get("meezo_plan_ppd"));
		soapUrl.put("getPhone", mep.get("meezo_phone_ppd"));
		soapUrl.put("getUsage", mep.get("meezo_usage_ppd"));
		soapUrl.put("getProfile", mep.get("meezo_profile_ppd"));

		soapActions = new HashMap<String, String>();
		soapActions.put("crp_action", "gs.CustomerQualification.runCreditCheck");
		soapActions.put("crp_action", "gs.CustomerQualification.runCreditCheck");

		exceptionMsgs = new HashMap<String, String>();
	}

	@BeforeMethod(alwaysRun = true)
	public void clearTokenMapCommon() {
		tokenMapCommon = null;
	}

	/***
	 * Get endpoint for given EOS environment i_e Qlab02, Qlab03, ppd, Prod,
	 * Qlab06__etc
	 * 
	 * @param environment
	 * @return
	 */
	public String getEOSEndPoint(String environment) {
		String eosEndpoint = null;
		Set<Map.Entry<String, String>> eosEndPointSet = eep.entrySet();
		for (Map.Entry<String, String> eEndPoint : eosEndPointSet) {
			if (environment.equalsIgnoreCase(eEndPoint.getKey())) {
				eosEndpoint = eEndPoint.getValue();
				return eosEndpoint;
			}
		}
		return eosEndpoint;
	}

	public String getClearTokenUrl(String environment) {
		String eosClearTokenEndpoint = null;
		Set<Map.Entry<String, String>> eosClearTokenEndPointSet = cturl.entrySet();
		for (Map.Entry<String, String> eEndPoint : eosClearTokenEndPointSet)
			if (environment.equalsIgnoreCase(eEndPoint.getKey()))
				eosClearTokenEndpoint = eEndPoint.getValue();
		return eosClearTokenEndpoint;
	}

	public String getClearTokenAccessUrl(String environment) {
		String eosClearTokenEndpoint = null;
		Set<Map.Entry<String, String>> eosClearTokenEndPointSet = clearCounterAccessTokenUrl.entrySet();
		for (Map.Entry<String, String> eEndPoint : eosClearTokenEndPointSet)
			if (environment.equalsIgnoreCase(eEndPoint.getKey()))
				eosClearTokenEndpoint = eEndPoint.getValue();
		return eosClearTokenEndpoint;
	}

	/***
	 * Get endpoint for given meezo environment i_e Qlab02, Qlab03, ppd, Prod,
	 * Qlab06__etc
	 * 
	 * @param environment
	 */
	public String getMeezoEndPoint(String environment) {
		String meezoEndpoint = null;
		Set<Map.Entry<String, String>> meezoEndPointSet = mep.entrySet();
		for (Map.Entry<String, String> mEndPoint : meezoEndPointSet)
			if (environment.equalsIgnoreCase(mEndPoint.getKey()))
				meezoEndpoint = mEndPoint.getValue();
		return meezoEndpoint;

	}

	/***
	 * Get basic oAuth token
	 * 
	 * @return
	 */
	public static String getOauthUri() {
		String oAuthToken = null;
		Set<Map.Entry<String, String>> oAuthEndPointSet = oAuth.entrySet();
		for (Map.Entry<String, String> oEndPoint : oAuthEndPointSet)
			if (backend.equalsIgnoreCase(oEndPoint.getKey()))
				oAuthToken = oEndPoint.getValue();
		return oAuthToken;

	}

	/***
	 * Get basic oAuth token
	 * 
	 * @param env
	 * @return
	 */
	public static String getJwtUri() {
		String jwtTokenUri = null;
		Set<Map.Entry<String, String>> jwtEndPointSet = jwt.entrySet();
		for (Map.Entry<String, String> jEndPoint : jwtEndPointSet)
			if (backend.equalsIgnoreCase(jEndPoint.getKey()))
				jwtTokenUri = jEndPoint.getValue();
		return jwtTokenUri;

	}

	public static String getUserTokenUrl() {
		String jwtTokenUri = null;
		Set<Map.Entry<String, String>> jwtEndPointSet = jwtUserTokenUrl.entrySet();
		for (Map.Entry<String, String> jEndPoint : jwtEndPointSet)
			if (backend.equalsIgnoreCase(jEndPoint.getKey()))
				jwtTokenUri = jEndPoint.getValue();
		return jwtTokenUri;
	}

	public String getSmallAccessToken() throws Exception {
		String baseURI = oAuth.get(environment);
		// System.out.println("Auth Base URL : " + baseURI);
		RestService rest = new RestService("", baseURI);
		rest.addHeader("Authorization", getAuthHeaderSmall());
		rest.addQueryParam("grant_type", "client_credentials");
		rest.addQueryParam("transactiontype", "POSTPAID%20");
		Response response = rest.callService("v1/oauth2/accesstoken", RestCallType.GET);
		// System.out.println("REST " + environment + " OAuth Response::" +
		// response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			String access_token = response.path("access_token").toString();
			if (null == tokenMapCommon) {
				tokenMapCommon = new HashMap<String, String>();
			}
			tokenMapCommon.put("accessToken", access_token);
			// System.out.println("Access token : " + access_token);
			return access_token;
		}
		return null;

	}

	/***
	 * Get basic UNO token
	 * 
	 * @param env
	 * @return
	 */
	public String getUNOAccessToken() throws Exception {
		String baseURI = oAuth.get(environment);
		// System.out.println("Auth Base URL : " + baseURI);
		RestService rest = new RestService("", baseURI);
		rest.addHeader("Authorization", getAuthHeader());
		rest.addHeader("accept", "application/json");
		rest.addQueryParam("grant_type", "client_credentials");
		Response response = rest.callService("oauth2/v4/tokens", RestCallType.POST);
		// System.out.println("REST " + environment + " OAuth Response::" +
		// response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			String access_token = response.path("access_token").toString();
			if (null == tokenMapCommon) {
				tokenMapCommon = new HashMap<String, String>();
			}
			tokenMapCommon.put("accessToken", access_token);
			// System.out.println("Access token : " + access_token);
			return access_token;
		}
		return null;

	}

	/***
	 * Get basic oAuth token
	 * 
	 * @param env
	 * @return
	 */
	public static String getAuthHeader() {
		String authHeader = null;
		Set<Map.Entry<String, String>> authHeaderEndPointSet = ah.entrySet();
		for (Map.Entry<String, String> aEndPoint : authHeaderEndPointSet)
			if (backend.equalsIgnoreCase(aEndPoint.getKey()))
				authHeader = aEndPoint.getValue();
		return authHeader;

	}

	public static String getAuthHeaderSmall() {
		String authHeader = null;
		Set<Map.Entry<String, String>> authHeaderEndPointSet = ah.entrySet();
		for (Map.Entry<String, String> aEndPoint : authHeaderEndPointSet) {
			String url = backend + "_small";
			if (url.equalsIgnoreCase(aEndPoint.getKey())) {
				authHeader = aEndPoint.getValue();
				break;
			}
		}
		return authHeader;

	}

	public static String getScopeInfo() {
		String authHeader = null;
		Set<Map.Entry<String, String>> authHeaderEndPointSet = scopeInfo.entrySet();
		for (Map.Entry<String, String> aEndPoint : authHeaderEndPointSet)
			if (backend.equalsIgnoreCase(aEndPoint.getKey()))
				authHeader = aEndPoint.getValue();
		return authHeader;

	}

	public String getRedirectURL(String env) {
		String redirectURL;
		switch (env) {
		case "stage":
			redirectURL = "https://e3.my.t-mobile.com";
			break;
		case "stage2":
			redirectURL = "https://e3.my.t-mobile.com";
			break;
		case "qlab01":
			redirectURL = "https://dev20.eservice.t-mobile.com";
			break;
		case "qlab02":
			redirectURL = "https://e2.my.t-mobile.com";
			break;
		case "qlab06":
			redirectURL = "https://qata0c.eservice.t-mobile.com";
			break;
		case "qlab03":
			redirectURL = "https://e4.my.t-mobile.com";
			// redirectURL = "https://qata0c.eservice.t-mobile.com";
			break;
		case "prod":
			redirectURL = "https://my.t-mobile.com";
			break;
		default:
			throw new IllegalArgumentException("Invalid environment value: " + environment);
		}
		return redirectURL;
	}

	public String getAccessToken() throws Exception {
		String baseURI = oAuth.get(environment);
		// System.out.println("Auth Base URL : " + baseURI);
		RestService rest = new RestService("", baseURI);
		rest.addHeader("Authorization", getAuthHeader());
		rest.addQueryParam("grant_type", "client_credentials");
		rest.addQueryParam("transactiontype", "POSTPAID");
		Response response = rest.callService("v1/oauth2/accesstoken", RestCallType.GET);
		// System.out.println("REST " + environment + " OAuth Response::" +
		// response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			String access_token = response.path("access_token").toString();
			if (null == tokenMapCommon) {
				tokenMapCommon = new HashMap<String, String>();
			}
			// tokenMapCommon.put("accessToken", access_token);
			tokenMapCommon.put("oAuthAccessToken", access_token);
			// System.out.println("Access token : " + access_token);
			return access_token;
		}
		return null;

	}

	public String checkAndGetAccessToken() throws Exception {

		String accessToken = "01.USR.0Ox5rozfztHjBOTmr";
		if (null == tokenMapCommon || tokenMapCommon.size() == 0) {
			accessToken = getAccessToken();
		} else if (StringUtils.isEmpty(tokenMapCommon.get("oAuthAccessToken"))) {
			accessToken = getAccessToken();
		} else {
			accessToken = tokenMapCommon.get("oAuthAccessToken");
		}
		/*
		 * String accessToken =
		 * StringUtils.isNoneEmpty(tokenMapCommon.get("accessToken")) ?
		 * tokenMapCommon.get("accessToken") : getAccessToken();
		 */
		return accessToken;
	}

	public String getOAuthTMNGToken() throws Exception {
		String baseURI = oAuth.get(environment);
		// System.out.println("Auth Base URL : " + baseURI);
		RestService rest = new RestService("", baseURI);
		rest.addHeader("Authorization", getAuthHeader());
		rest.addHeader("Accept", "application/json");
		Response response = rest.callService("oauth2/v4/tokens", RestCallType.POST);
		// System.out.println("REST " + environment + " OAuth Response::" +
		// response.asString());
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			String access_token = response.path("access_token").toString();
			if (null == tokenMapCommon) {
				tokenMapCommon = new HashMap<String, String>();
			}
			// tokenMapCommon.put("accessToken", access_token);
			tokenMapCommon.put("oAuthAccessToken", access_token);
			// System.out.println("Access token : " + access_token);
			return access_token;
		}
		return null;

	}

	/**
	 * Will return JWT decoded token
	 * 
	 * @param data
	 * @param apiTestData
	 * @param tokenMap
	 * @return
	 * @throws Exception
	 */

	public String getJWTDecodeToken(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		String requestBody = new ServiceTest().getRequestFromFile("tmngAccessEncode.txt");
		String operationName = "get access code";
		logRequest(requestBody, operationName);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;
		String accessToken = null;
		String decodedJWT = null;

		JWTTokenEncodingAPI JWTTokenEncoding = new JWTTokenEncodingAPI();
		Response response = JWTTokenEncoding.accesscodeEncode(apiTestData, requestBody, tokenMap);
		if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
			logSuccessResponse(response, operationName);
			mapper = new ObjectMapper();
			jsonNode = mapper.readTree(response.asString());
			if (!jsonNode.isMissingNode()) {
				JsonPath jsonPath = new JsonPath(response.asString());
				accessToken = jsonPath.get("accessToken");
				Assert.assertNotNull(jsonPath.get("accessToken"), "accessToken is null");
				if (accessToken != null) {
					String[] pieces = accessToken.split("\\.");
					String b64payload = pieces[1];
					String jsonString = new String(Base64.getDecoder().decode(b64payload), "UTF-8");
					JsonPath encodedToken = new JsonPath(jsonString);
					decodedJWT = encodedToken.get("sub");

				} else {
					Reporter.log("accessToken is null");
				}

			}
		} else {
			failAndLogResponse(response, operationName);
		}

		return decodedJWT;

	}

	public String checkAndGetTMNGOauthToken() throws Exception {

		String accessToken = "";
		if (null == tokenMapCommon || tokenMapCommon.size() == 0) {
			accessToken = getOAuthTMNGToken();
		} else if (StringUtils.isEmpty(tokenMapCommon.get("oAuthAccessToken"))) {
			accessToken = getAccessToken();
			// System.out.println(accessToken);
		} else {
			accessToken = tokenMapCommon.get("oAuthAccessToken");
		}
		/*
		 * String accessToken =
		 * StringUtils.isNoneEmpty(tokenMapCommon.get("accessToken")) ?
		 * tokenMapCommon.get("accessToken") : getAccessToken();
		 */
		return accessToken;
	}

	public String checkAndGetTMNGUNOToken() throws Exception {

		String accessToken = "";
		if (null == tokenMapCommon || tokenMapCommon.size() == 0) {
			accessToken = getUNOAccessToken();
		} else if (StringUtils.isEmpty(tokenMapCommon.get("accessToken"))) {
			accessToken = getUNOAccessToken();
		} else {
			accessToken = tokenMapCommon.get("accessToken");
		}
		/*
		 * String accessToken =
		 * StringUtils.isNoneEmpty(tokenMapCommon.get("accessToken")) ?
		 * tokenMapCommon.get("accessToken") : getAccessToken();
		 */
		return accessToken;
	}

	/*
	 * public String checkAndGetOAuthAccessToken() throws Exception {
	 * 
	 * String accessToken=""; if (null == tokenMapCommon ||
	 * tokenMapCommon.size() == 0) { accessToken = getAccessToken(); }else
	 * if(StringUtils.isEmpty(tokenMapCommon.get("oAuthAccessToken"))){
	 * accessToken = getAccessToken(); } else{ accessToken =
	 * tokenMapCommon.get("oAuthAccessToken"); } String accessToken =
	 * StringUtils.isNoneEmpty(tokenMapCommon.get("accessToken")) ?
	 * tokenMapCommon.get("accessToken") : getAccessToken();
	 * 
	 * return accessToken; }
	 */

	public Map<String, String> checkAndGetJWTToken(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		Map<String, String> jwtTokens = null;

		if (StringUtils.isNoneBlank(tokenMap.get("jwtToken")) && StringUtils.isNoneBlank(tokenMap.get("jwtToken"))) {
			jwtTokens = new HashMap<String, String>();
			jwtTokens.put("jwtToken", tokenMap.get("jwtToken"));
			jwtTokens.put("accessToken", tokenMap.get("accessToken"));
			return jwtTokens;
		}

		if (null == tokenMapCommon || tokenMapCommon.size() == 0) {
			jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
			jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);
			jwtTokenApi.registerJWTTokenforAPIGEE("ONPREM", jwtTokens.get("jwtToken"), tokenMap);
			tokenMapCommon = new HashMap<String, String>();
			tokenMapCommon.put("jwtToken", jwtTokens.get("jwtToken"));
			tokenMapCommon.put("accessToken", jwtTokens.get("accessToken"));
			tokenMapCommon.put("emailVal", jwtTokens.get("emailVal"));
			System.out.println("tokenMapCommon= " + tokenMapCommon);
		} else {
			// System.out.println("tokenMapCommon1= " + tokenMapCommon);
			jwtTokens = new HashMap<String, String>();
			jwtTokens.put("jwtToken", tokenMapCommon.get("jwtToken"));
			jwtTokens.put("accessToken", tokenMapCommon.get("accessToken"));
		}
		return jwtTokens;
	}

	// This method is introduced as part of new design for monitors. Old method
	// should be deprecated once this is fully functional.
	public Map<String, String> getJwtToken(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		JWTTokenApi jwtTokenApi = new JWTTokenApi();
		Map<String, String> jwtTokens = null;

		jwtTokens = jwtTokenApi.getJwtAuthToken(apiTestData, tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);
		jwtTokenApi.registerJWTTokenforAPIGEE("ONPERM", jwtTokens.get("jwtToken"), tokenMap);

		tokenMap.put("jwtToken", jwtTokens.get("jwtToken"));
		tokenMap.put("accessToken", jwtTokens.get("accessToken"));
		tokenMap.put("emailVal", jwtTokens.get("emailVal"));

		return tokenMap;
	}

	@BeforeTest(alwaysRun = true)
	public void setUp(ITestContext testContext) {
		String serviceCategory = null;
		environment = System.getProperty("environment");
		backend = System.getProperty("iam_servers");
		Map<String, String> serviceType = testContext.getCurrentXmlTest().getAllParameters();
		if (serviceType.get(Config.PLATFORM_TYPE) != null) {
			serviceCategory = serviceType.get(Config.PLATFORM_TYPE);
		}
		if (null != environment && serviceCategory.contains("eos")) {
			eos_base_url = getEOSEndPoint("ms" + "_" + environment);
			eos_clear_token_url = getClearTokenUrl("ms" + "_" + environment);
			eosClearCounterAccessTokenUrl = getClearTokenAccessUrl("ms" + "_" + environment);
			eosBrassCodeUrl = getJwtUri() + getBrassCodeResourceUrl();
			eosBrassUserTokenUrl = getJwtUri() + getBrassUserTokenResourceUrl();
		} else if (null != environment && serviceCategory.contains("tmng")) {
			tmng_base_url = getEOSEndPoint("ms_aws" + "_" + environment);
			if (null != backend && serviceCategory.contains("eos"))
				eos_base_url = getEOSEndPoint("ms" + "_" + backend);
			else if (null != backend && serviceCategory.contains("tmng")) {
				tmng_base_url = getEOSEndPoint("ms_aws" + "_" + backend);
				yext_base_url = "https://api.yext.com";
			} else if (null != environment && serviceCategory.contains("meezo"))
				meezo_base_url = getEOSEndPoint(serviceCategory + "_" + environment);
		} else if (null != backend && serviceCategory.contains("meezo"))
			meezo_base_url = getEOSEndPoint(serviceCategory + "_" + backend);
		else {
			// System.out.println("Backend is null; please pass environment
			// details like qlab02,qlab03..etc");
			Reporter.log("Environemnt is null");
		}
	}

	public String getBrassCodeResourceUrl() {
		String brassTokenEndpoint = null;
		Set<Map.Entry<String, String>> eosEndPointSet = brassCodeResourceUrl.entrySet();
		for (Map.Entry<String, String> eEndPoint : eosEndPointSet) {
			if (environment.equalsIgnoreCase(eEndPoint.getKey())) {
				brassTokenEndpoint = eEndPoint.getValue();
				return brassTokenEndpoint;
			}
		}
		return brassTokenEndpoint;
	}

	public String getBrassUserTokenResourceUrl() {
		String brassTokenEndpoint = null;
		Set<Map.Entry<String, String>> eosEndPointSet = brassUserTokenResourceUrl.entrySet();
		for (Map.Entry<String, String> eEndPoint : eosEndPointSet) {
			if (environment.equalsIgnoreCase(eEndPoint.getKey())) {
				brassTokenEndpoint = eEndPoint.getValue();
				return brassTokenEndpoint;
			}
		}
		return brassTokenEndpoint;
	}

	@SuppressWarnings({ "rawtypes" })
	public String prepareRequestParam(String requestParam, Map<String, String> tokenMap) {
		String replacedRequestParam = requestParam;
		if (requestParam != null) {
			if (tokenMap != null) {
				Iterator it = tokenMap.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();
					String key = pair.getKey().toString();
					String depVal = pair.getValue().toString();
					depVal = StringEscapeUtils.escapeJava(depVal);
					String frameworkKey = key + "_frkey";
					if (depVal.startsWith("{")) {
					} else {
						depVal = "\"" + depVal + "\"";
					}
					// if(requestParam.contains(key+"_frkey")) {
					if (replacedRequestParam.contains(frameworkKey)) {
						replacedRequestParam = replacedRequestParam.replace(frameworkKey, depVal);

					}
				}
			}
		}
		return replacedRequestParam;
	}

	@SuppressWarnings({ "rawtypes" })
	public String prepareRequestParamXML(String requestParam, Map<String, String> tokenMap) {
		String replacedRequestParam = requestParam;
		if (requestParam != null) {
			if (tokenMap != null) {
				Iterator it = tokenMap.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry pair = (Map.Entry) it.next();
					String key = pair.getKey().toString();
					String depVal = pair.getValue().toString();
					depVal = StringEscapeUtils.escapeJava(depVal);
					String frameworkKey = key + "_frkey";
					// depVal = "\"" + depVal + "\"";
					// if(requestParam.contains(key+"_frkey")) {
					if (replacedRequestParam.contains(frameworkKey)) {
						replacedRequestParam = replacedRequestParam.replace(frameworkKey, depVal);
					}
				}
			}
		}
		return replacedRequestParam;
	}

	public String getPathVal(JsonNode node, String attributeName) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			String nodeVal = null;
			if (!node.isMissingNode()) {
				JsonNode path = null;
				if (!node.isMissingNode()) {
					if (attributeName.contains(".")) {
						JsonPath jsonPath = new JsonPath(mapper.writeValueAsString(node));
						// path = node.findPath(attributeName);
						if (StringUtils.isNoneEmpty(jsonPath.getString(attributeName))) {
							nodeVal = jsonPath.getString(attributeName);
						} else {
							throw new IllegalArgumentException(
									"Cannot find the element in the response : " + "<b>" + attributeName + "</b>");
						}

						// nodeVal = path.asText();
					} else {
						if (null != node.path(attributeName)) {
							path = node.path(attributeName);
						} else {
							throw new IllegalArgumentException(
									"Cannot find the element in the response : " + "<b>" + attributeName + "</b>");
						}
						if (path.isArray()) {
							nodeVal = path.toString();
						} else {
							nodeVal = path.asText();
						}
					}
					// nodeVal = response.path(attributeName);
					if (StringUtils.isNoneEmpty(nodeVal)) {
						return nodeVal.toString();
					} else {
						return "";
					}
				} else {
					return "";
				}

			} else {
				return "";
			}
		} catch (IllegalArgumentException ie) {
			Reporter.log(" <b>Exception Response : Element path missing in Json Response</b> ");
			Reporter.log(
					" " + "Cannot find the valid element in the response : " + "<b>" + attributeName + "</b>" + ie);
			// Assert.fail("<b>Exception Response : Element path missing in Json
			// Response</b> " + "Cannot find the valid element in the response
			// :"+"<b>"+ attributeName +"</b>"+ie);
			Assert.fail("Please check the data used, the below element/value is missing in the response. <b>"
					+ attributeName + "</b>");
		} catch (JsonProcessingException e) {
			Assert.fail("<b>Please check the json response, unable to process it");
		} catch (Exception e) {

			Assert.fail(e.toString());
		}
		return "";
	}

	public String getToken(String key) {
		return tokenMapCommon.get(key);
	}

	// Get public key

	public JsonNode getPublicKey(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildAccountPlansAPIpublicKeyHeader(apiTestData, tokenMap);

		JsonNode publicKeyNode = null;
		// String requestBody = "{\r\n \"applicationId\": \"45454\",\r\n
		// \"payment\": {\r\n \"encryption\": {\r\n \"encryptionFormat\":
		// \"JWK\"\r\n \r\n }\r\n }\r\n}";
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);

		Response response = service.callService("accounts/v3/publickey", RestCallType.GET);
		// System.out.println("public key" + response.asString());
		JsonNode jsonNode = null;

		if (response.statusCode() == 200) {
			ObjectMapper mapper = new ObjectMapper();
			jsonNode = mapper.readTree(response.asString());
			publicKeyNode = jsonNode.path("publicKey");
		}

		return publicKeyNode;
	}

	private Map<String, String> buildAccountPlansAPIpublicKeyHeader(ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {
		tokenMap.put("version", "v1");
		Map<String, String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("application_id", "MYTMO");
		headers.put("Authorization", jwtTokens.get("jwtToken"));
		// headers.put("X-Auth-Originator",checkAndGetAccessToken());
		headers.put("channel_id", "WEB");
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("ban", apiTestData.getBan());
		headers.put("usn", "5d57949634f1a941");

		return headers;
	}

	private Map<String, String> buildShopPublicKeyHeader(Map<String, String> tokenMap, ApiTestData apiTestData)
			throws Exception {

		// Map<String, String> jwtTokens = checkAndGetJWTToken(apiTestData,
		// tokenMap);
		// tokenMapCommon.put("accessToken", accessToken);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("applicationid", "MYTMO");
		headers.put("X-Auth-Originator", checkAndGetAccessToken());
		headers.put("Authorization", checkAndGetAccessToken());
		headers.put("channel_id", "WEB");
		headers.put("msisdn", "4706852958");
		headers.put("usn", "d2f6a3a9bd674399");
		// headers.put("Accept", "application/json");

		return headers;
	}

	public JsonNode getShopPublicKey(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {

		Map<String, String> headers = buildShopPublicKeyHeader(tokenMap, apiTestData);

		JsonNode publicKeyNode = null;
		// String requestBody = "{\r\n \"applicationId\": \"45454\",\r\n
		// \"payment\": {\r\n \"encryption\": {\r\n \"encryptionFormat\":
		// \"JWK\"\r\n \r\n }\r\n }\r\n}";
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);

		Response response = service.callService("accounts/v3/publickey", RestCallType.GET);
		// System.out.println("public key" + response.asString());

		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(response.asString());
		if (!jsonNode.isMissingNode()) {

			JsonPath jsonPath = new JsonPath(response.asString());

			Assert.assertNotNull(jsonPath.get("publicKey"), "public key is null");
			Reporter.log("public key is not null");
		}
		return publicKeyNode;
	}

	public String getShopEncryptedCardNumber(ApiTestData apiTestData, Map<String, String> tokenMap) {
		String encryptedCardStr = "";

		PublicKey pub = null;
		try {
			JsonNode publicKeyNode = getShopPublicKey(apiTestData, tokenMap);

			// ObjectMapper mapper = new ObjectMapper();
			// JsonNode readTree = mapper.readTree(publicKeyNode.asText());
			JsonNode readTree = publicKeyNode;
			if (!readTree.isMissingNode() && readTree.has("e")) {
				String e = readTree.path("e").asText();
				String n = readTree.path("n").asText();
				// System.out.println("E::" + e + "::n::" + n);
				byte exponentB[] = Base64.getUrlDecoder().decode(e);
				byte modulusB[] = Base64.getUrlDecoder().decode(n);
				BigInteger exponent = new BigInteger(toHexFromBytes(exponentB), 16);
				BigInteger modulus = new BigInteger(toHexFromBytes(modulusB), 16);
				RSAPublicKeySpec spec = new RSAPublicKeySpec(modulus, exponent);
				KeyFactory factory = KeyFactory.getInstance("RSA");
				pub = factory.generatePublic(spec);
			}
			// String card = "4358805977433343";
			String card = apiTestData.getCardNumber();
			// --- encrypt given OAEPParameterSpec
			Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPPadding");
			OAEPParameterSpec oaepParams = new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256,
					PSpecified.DEFAULT);
			cipher.init(Cipher.ENCRYPT_MODE, pub, oaepParams);
			// System.out.println(cipher.getProvider());
			// System.out.println(cipher.getAlgorithm());
			byte[] encryptedCard = cipher.doFinal(card.getBytes("UTF-8"));

			// System.out.println("Encrypted Card:: " +
			// DatatypeConverter.printHexBinary(encryptedCard));
			encryptedCardStr = DatatypeConverter.printHexBinary(encryptedCard);
		} catch (Exception e) {
			// System.out.println("Exception while encrypting card Number : " +
			// e);
		}
		return encryptedCardStr;
	}

	public String getEncryptedCardNumber(ApiTestData apiTestData, Map<String, String> tokenMap) {
		String encryptedCardStr = null;
		PublicKey pub = null;
		try {

			ObjectMapper mapper = new ObjectMapper();
			JsonNode publicKeyNode = getPublicKey(apiTestData, tokenMap);
			if (publicKeyNode != null) {
				String test = StringEscapeUtils.unescapeJava(publicKeyNode.asText());
				JsonNode readTree = mapper.readTree(test);
				if (!readTree.isMissingNode() && readTree.has("e")) {
					String e = readTree.path("e").asText();
					String n = readTree.path("n").asText();
					// System.out.println("E::" + e + "::n::" + n);
					byte exponentB[] = Base64.getUrlDecoder().decode(e);
					byte modulusB[] = Base64.getUrlDecoder().decode(n);
					BigInteger exponent = new BigInteger(toHexFromBytes(exponentB), 16);
					BigInteger modulus = new BigInteger(toHexFromBytes(modulusB), 16);
					RSAPublicKeySpec spec = new RSAPublicKeySpec(modulus, exponent);
					KeyFactory factory = KeyFactory.getInstance("RSA");
					pub = factory.generatePublic(spec);
				}
				// String card = "4358805977433343";
				String card = apiTestData.getCardNumber();
				// --- encrypt given OAEPParameterSpec
				Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPPadding");
				OAEPParameterSpec oaepParams = new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256,
						PSpecified.DEFAULT);
				cipher.init(Cipher.ENCRYPT_MODE, pub, oaepParams);
				// System.out.println(cipher.getProvider());
				// System.out.println(cipher.getAlgorithm());
				byte[] encryptedCard = cipher.doFinal(card.getBytes("UTF-8"));

				// System.out.println("Encrypted Card:: " +
				// DatatypeConverter.printHexBinary(encryptedCard));
				encryptedCardStr = DatatypeConverter.printHexBinary(encryptedCard);
			} else {
				// System.out.println("Exception while generating publickey
				// "+publicKeyNode );
			}
		} catch (Exception e) {
			// System.out.println("Exception while encrypting card Number : " +
			// e);
		}
		return encryptedCardStr;
	}

	private static String toHexFromBytes(byte[] exponentB) {
		return DatatypeConverter.printHexBinary(exponentB);
	}

	public String formatXML(String input) {
		try {
			Document doc = DocumentHelper.parseText(input);
			StringWriter sw = new StringWriter();
			OutputFormat format = OutputFormat.createPrettyPrint();
			format.setIndent(true);
			format.setIndentSize(3);
			XMLWriter xw = new XMLWriter(sw, format);
			xw.write(doc);
			return sw.toString();
		} catch (Exception e) {
			LOGGER.error("Exception occured is -- " + e);
			return input;
		}
	}

	public String createPlattoken(ApiTestData apiTestData) throws Exception {

		try {
			String requestBody = "{\"accountNumber\":\"" + apiTestData.getBan() + "\",\"phoneNumber\":\""
					+ apiTestData.getMsisdn() + "\"}";
			/*
			 * Reporter.log(
			 * "************************************************************************************************************************"
			 * ); Reporter.log("<b>DCP Token Request : </b>" + requestBody);
			 * Reporter.log("Request Type : POST");
			 */

			String correlationid = checkAndGetAccessToken();

			// String basePath = getEOSEndPoint(environment);

			RestService restService = new RestService(requestBody, eos_base_url);
			restService.addHeader("Authorization", getAuthHeader());
			restService.addHeader("accept", "application/json");
			restService.addHeader("applicationid", "MYTMO");//
			restService.addHeader("cache-control", "no-cache");
			restService.addHeader("channelid", "WEB");
			restService.addHeader("clientid", "ESERVICE");
			restService.addHeader("content-type", "application/json");//
			restService.addHeader("usn", "testUSN");
			restService.addHeader("interactionid", "api-testing");
			restService.addHeader("transactionBusinessKey", apiTestData.getBan());
			restService.addHeader("phoneNumber", apiTestData.getMsisdn());
			restService.addHeader("transactionbusinesskeytype", "BAN");
			restService.addHeader("correlationid", correlationid);
			restService.addHeader("transactionid", "xasdf1234asa4444");
			restService.addHeader("transactiontype", "UPGRADE");

			Response response = restService.callService("v1/initialize/plattoken/create", RestCallType.POST);
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {

				Reporter.log("<b>DCP Token Response : </b>");
				Reporter.log(response.body().asString());
				// System.out.println(response.body().asString());
				return correlationid;
			}

		} catch (Exception e) {
		}
		return null;

	}

	public String createPlattokenJWT(ApiTestData apiTestData, String jwtToken) throws Exception {

		try {
			String requestBody = "{\"accountNumber\":\"" + apiTestData.getBan() + "\",\"phoneNumber\":\""
					+ apiTestData.getMsisdn() + "\"}";

			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>DCP Token Request : </b>" + requestBody);
			Reporter.log("Request Type : POST");

			String correlationid = getValForCorrelation();

			// String basePath = getEOSEndPoint(environment);
			RestService restService = new RestService(requestBody, eos_base_url);
			restService.addHeader("Authorization", jwtToken);
			restService.addHeader("X-Auth-Originator", jwtToken);
			restService.addHeader("accept", "application/json");
			restService.addHeader("applicationid", "MYTMO");//
			restService.addHeader("cache-control", "no-cache");
			restService.addHeader("channelid", "WEB");
			restService.addHeader("clientid", "ESERVICE");
			restService.addHeader("content-type", "application/json");//
			restService.addHeader("usn", "testUSN");
			restService.addHeader("interactionid", "api-testing");
			restService.addHeader("transactionBusinessKey", apiTestData.getBan());
			restService.addHeader("phoneNumber", apiTestData.getMsisdn());
			restService.addHeader("transactionbusinesskeytype", "BAN");
			restService.addHeader("correlationid", correlationid);
			restService.addHeader("transactionid", "xasdf1234asa4444");
			restService.addHeader("transactiontype", "HSU");

			Response response = restService.callService("v2/initialize/plattoken/create", RestCallType.POST);
			// System.out.println("Response of createPlattoken in :" +
			// environment + ":" + response.asString());
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				Reporter.log("<b>DCP Token Response : </b>");
				Reporter.log(response.body().asString());
				if (null == tokenMapCommon) {
					tokenMapCommon = new HashMap<String, String>();
				}
				tokenMapCommon.put("plattokenJWT", correlationid);
				// System.out.println(response.body().asString());
				// String flatToken = response.path("access_token").toString();
				return correlationid;
			} else {
				Reporter.log("<b>Plattoken Failure Response : </b>");
				Reporter.log(response.body().asString());
			}

		} catch (Exception e) {
			Reporter.log(" <b>Exception Response :</b> ");
			Reporter.log(" " + e);
		}
		return null;

	}

	public String checkAndGetPlattokenJWT(ApiTestData apiTestData, String jwtToken) throws Exception {

		String plattoken = "";
		if (null == tokenMapCommon || tokenMapCommon.size() == 0) {
			plattoken = createPlattokenJWT(apiTestData, jwtToken);
		} else if (StringUtils.isEmpty(tokenMapCommon.get("plattokenJWT"))) {
			plattoken = createPlattokenJWT(apiTestData, jwtToken);

		} else {
			plattoken = tokenMapCommon.get("plattokenJWT");
		}

		return plattoken;
	}

	public String getValForCorrelation() {
		// char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder((100000 + rnd.nextInt((900000000))) + "");
		// for (int i = 0; i < 5; i++)
		// sb.append(chars[rnd.nextInt(chars.length)]);

		return sb.toString();
	}

	public static String getRandomValue(final Random random, final int lowerBound, final int upperBound,
			final int decimalPlaces) {

		if (lowerBound < 0 || upperBound <= lowerBound || decimalPlaces < 0) {
			throw new IllegalArgumentException("Put error message here");
		}

		final double dbl = ((random == null ? new Random() : random).nextDouble() //
				* (upperBound - lowerBound)) + lowerBound;
		return String.format("%." + decimalPlaces + "f", dbl);

	}

	public void logRequest(String requestBody, String operationName) {
		Reporter.log(
				"************************************************************************************************************************");
		Reporter.log("<b>" + operationName + " Request :</b> ");
		Reporter.log(requestBody);
	}

	public void logLargeResponse(Response response, String operationName) {
		Reporter.log(
				"************************************************************************************************************************");
		Reporter.log("<b>" + operationName + " Response :</b> ");
		Reporter.log("<textarea style=\"border:none;height:300px;\" cols=\"120\" rows=\"5\">"
				+ formatXML(response.body().asString()) + "</textarea>");
	}

	public void logLargeRequest(String request, String operationName) {
		Reporter.log(
				"************************************************************************************************************************");
		Reporter.log("<b>" + operationName + " Request :</b> ");
		Reporter.log("<textarea style=\"border:none;height:300px;\" cols=\"120\" rows=\"5\">" + formatXML(request)
				+ "</textarea>");
	}

	public void logXMLRequest(String requestBody, String operationName) {
		Reporter.log(
				"************************************************************************************************************************");
		Reporter.log("<b>" + operationName + " Request :</b> ");
		Reporter.log("<textarea style=\"border:none;height:300px;\" cols=\"120\" rows=\"5\">" + formatXML(requestBody)
				+ "</textarea>");
	}

	public void logSuccessResponse(Response response, String operationName) {
		Reporter.log(
				"************************************************************************************************************************");
		Reporter.log("<b>" + operationName + " Response :</b> ");
		Reporter.log("<textarea style=\"border:none;height:300px;\" cols=\"120\" rows=\"5\">"
				+ formatXML(response.body().asString()) + "</textarea>");
	}

	public void failAndLogResponse(Response response, String operationName) {
		Reporter.log(
				"************************************************************************************************************************");
		logStep(StepStatus.FAIL, "response.getStatusCode()", response.body().asString());
		Reporter.log("<b>" + operationName + " Exception Response :</b> ");
		Reporter.log("Response status code : " + response.getStatusCode());
		Reporter.log("<b>" + operationName + " Response : </b>" + response.body().asString());
		if (isResponseEmpty(response)) {
			Assert.fail("<b> <font color='red'>" + operationName + " Empty Response : " + response.body().asString() + " </font></b> ");
		} else {
			Assert.fail("<b> <font color='red'>" + operationName + " Response :</b> " + response.body().asString()+ " </font></b> ");
		}

	}

	public void addExceptionMessagesToMapAndLog(Response response, String exceptionMessage) {

		String respMsg = response.body().asString();
		Reporter.log(
				"************************************************************************************************************************");
		logStep(StepStatus.FAIL, "response.getStatusCode()", respMsg);
		Reporter.log("<b> <font color='red'>" + exceptionMessage+" </font></b>");
		exceptionMsgs.put("errorMessage", exceptionMessage);

	}
	
	public void failWithExcpetion(String errorMsg) {
		Assert.fail(errorMsg);
	}

	public void throwServiceFailException() throws ServiceFailureException{
		throw new ServiceFailureException(exceptionMsgs.get("errorMessage"), new Exception());
	}

	public boolean isResponseEmpty(Response response) {
		String resp = response.body().asString();
		if (StringUtils.isBlank(resp) || StringUtils.isEmpty(StringUtils.substringBetween(resp, "{", "}"))) {
			return true;
		}
		return false;
	}

	public void invokeClearCounters(ApiTestData apiTestData) throws Exception {

		clearCounters(apiTestData);
		// unlockAccount(apiTestData);
	}

	public boolean unlockAccount(ApiTestData apiTestData) {
		RequestData requestData = new RequestData();
		String environment = System.getProperty(Constants.IAM_SERVER_ENVIRONEMT);
		requestData.setMsisdn(apiTestData.getMsisdn());
		requestData.setEnvironment(environment);
		long startTime = System.currentTimeMillis();
		boolean unlockAccount = false;
		try {
			unlockAccount = new RestServiceHelper().unlockAccount(requestData);
		} catch (Exception e) {
			// System.out.println("Exception while unlocking ac" +
			// e.getMessage());
			// e.printStackTrace();
			// logger.error(Constants.UNABLE_TO_UNLOCK + e);
			// throw new FrameworkException(Constants.UNABLE_TO_UNLOCK, e);
		}
		long stopTime = System.currentTimeMillis();
		logger.info("stopTime::::::{}", stopTime);
		long elapsedTime = stopTime - startTime;
		logger.info("elapsedTime::::::{}", elapsedTime);
		return unlockAccount;
	}

	public void clearCounters(ApiTestData apiTestData) throws Exception {

		String accessTokenForUnlock = getAccessTokenForUnlockMsisdn();
		String respMessage = "";

		if (StringUtils.isNoneEmpty(accessTokenForUnlock)) {
			respMessage = toggleHardLockFlag(apiTestData.getMsisdn(), accessTokenForUnlock);
			respMessage = clearAccountLockCounters(apiTestData.getMsisdn(), accessTokenForUnlock);
		}
		// if(responseStatus==200){
		if ("success".equalsIgnoreCase(respMessage)) {
			Reporter.log("<b> Msisdn :" + apiTestData.getMsisdn() + " unlocked successfully</b>");
		} else {
			Reporter.log("<b> Msisdn :" + apiTestData.getMsisdn()
					+ " unlock is not successfull. Please Check the Msisdn and Try again</b>");
			Assert.fail("<b>Failed to unlock msisdn " + apiTestData.getMsisdn() + " Reason : " + respMessage + "</b>");
		}

	}

	public String clearAccountLockCounters(String msisdn, String accessToken) {

		String responseStatus = "";
		try {
			// String requestBody = "{\"accountNumber\":\"" +
			// apiTestData.getBan() + "\",\"phoneNumber\":\""+
			// apiTestData.getMsisdn() + "\"}";

			String requestBody = "{\n   \"user_id\": \"tel:" + msisdn + "\"\n}";

			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Unlock Misidn Request : </b>" + requestBody);
			Reporter.log("Request Type : POST");

			RestService restService = new RestService(requestBody, eos_clear_token_url);
			restService.addHeader("Authorization", "Bearer " + accessToken);
			restService.addHeader("Accept", "application/json");
			Reporter.log(eos_clear_token_url + "/oauth2/v1/clearaccountlockcounters");
			Response response = restService.callService("oauth2/v1/clearaccountlockcounters", RestCallType.POST);
			// System.out.println("Response of Unlock Misidn Service in :" +
			// environment + ":" + response.body().asString());
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				responseStatus = "success";
				logSuccessResponse(response, "clearAccountLockCounters");
				return responseStatus;
			} else {
				Reporter.log("<b>Unlock Misidn Service Failure Response : </b>");
				responseStatus = response.body().asString();
				failAndLogResponse(response, "clearAccountLockCounters");
				Assert.fail("<b>Failed to Unlock Misidn :</b> " + response.body().asString());
			}

		} catch (Exception e) {
			responseStatus = e.toString();
			Reporter.log(" <b>Failed to Unlock Misidn : Exception Response :</b> ");
			Reporter.log(" " + e);
		}
		return responseStatus;
	}

	public String toggleHardLockFlag(String msisdn, String accessToken) {

		String responseStatus = "";
		try {
			// String requestBody = "{\"accountNumber\":\"" +
			// apiTestData.getBan() + "\",\"phoneNumber\":\""+
			// apiTestData.getMsisdn() + "\"}";

			String requestBody = "{ \"is_account_locked\": \"false\", \"user_id\": \"tel:" + msisdn + "\" }";
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Unlock Misidn Request : </b>" + requestBody);
			Reporter.log("Request Type : POST");

			RestService restService = new RestService(requestBody, eos_clear_token_url);
			restService.addHeader("Authorization", "Bearer " + accessToken);
			restService.addHeader("Accept", "application/json");
			// restService.addHeader("Accept-Encoding", "gzip, deflate");
			// restService.addHeader("Device-Type", "desktop");
			// restService.addHeader("User-Agent", "python-requests/2.10.0");
			Reporter.log(eos_clear_token_url + "/oauth2/v1/togglehardlockflag");
			Response response = restService.callService("oauth2/v1/togglehardlockflag", RestCallType.POST);
			// System.out.println("Response of Unlock Misidn Service in :" +
			// environment + ":" + response.body().asString());
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				responseStatus = "success";
				logSuccessResponse(response, "clearAccountLockCounters");
				return responseStatus;
			} else {
				Reporter.log("<b>Unlock Misidn Service Failure Response : </b>");
				responseStatus = response.body().asString();
				failAndLogResponse(response, "clearAccountLockCounters");
				Assert.fail("<b>Failed to Unlock Misidn :</b> " + response.body().asString());
			}

		} catch (Exception e) {
			responseStatus = e.toString();
			Reporter.log(" <b>Failed to Unlock Misidn : Exception Response :</b> ");
			Reporter.log(" " + e);
		}
		return responseStatus;
	}

	public String getAccessTokenForUnlockMsisdn() {
		String accessTokenForUnlock = "";
		List<Header> headerList = new LinkedList<Header>();
		HashMap<String, String> formParametersMap = new HashMap<String, String>();
		Header header1 = new Header("Accept", "application/json");
		Header header2 = new Header("Authorization", oAuth.get(environment + "_accesstoken_auth"));
		headerList.add(header1);
		headerList.add(header2);
		Headers headers = new Headers(headerList);
		formParametersMap.put("Content-Type", "application/x-www-form-urlencoded");
		// String requestURL =
		// eos_clear_token_url+"/oauth2-api/p/v1/token?scope=TMO_ID_profile
		// token_validation&grant_type=client_credentials";
		String requestURL = eosClearCounterAccessTokenUrl + oAuth.get(environment + "_accesstoken_resource_url")
				+ "?scope=TMO_ID_profile token_validation&grant_type=client_credentials";
		Reporter.log(requestURL.trim());
		com.jayway.restassured.response.Response response = null;
		response = RestAssured.given().config(new RestAssuredConfig()).headers(headers)
				.contentType("application/x-www-form-urlencoded").formParameters(formParametersMap).when()
				.relaxedHTTPSValidation().post(requestURL.trim()).then().extract().response();

		// System.out.println("Clear Counter AccessToken :
		// "+response.statusCode()+" "+response.body().asString());

		if (response.statusCode() == 200) {
			Reporter.log("<b>" + "clearcounter" + " Response : </b>" + response.body().asString());
			ObjectMapper mapper = new ObjectMapper();
			JsonNode jsonNode = null;
			try {
				jsonNode = mapper.readTree(response.asString());
			} catch (JsonProcessingException e) {
				Assert.fail("<b>JsonProcessingException Response :</b> " + e);
				Reporter.log(" <b>JsonProcessingException Response :</b> ");
				Reporter.log(" " + e);
				e.printStackTrace();
			} catch (IOException e) {
				Assert.fail("<b>IOException Response :</b> " + e);
				Reporter.log(" <b>IOException Response :</b> ");
				Reporter.log(" " + e);
				e.printStackTrace();
			}
			if (!jsonNode.isMissingNode()) {

				accessTokenForUnlock = getPathVal(jsonNode, "access_token");
				if (StringUtils.isNoneEmpty(accessTokenForUnlock)) {
					return accessTokenForUnlock;
				}
			}

		} else {
			Reporter.log("<b>" + "clearcounter" + " Exception Response : </b>" + response.body().asString());
			Assert.fail("<b>Failed to Fetch Access Token For Unlocking Msisdn :</b> " + response.body().asString());

		}
		return accessTokenForUnlock;
	}

	public int invokeUnlockMsisdn(String msisdn, String accessToken) {

		int responseStatus = 0;
		try {
			// String requestBody = "{\"accountNumber\":\"" +
			// apiTestData.getBan() + "\",\"phoneNumber\":\""+
			// apiTestData.getMsisdn() + "\"}";

			String requestBody = "{\n    \"is_account_locked\": \"false\", \n    \"user_id\": \"tel:" + msisdn
					+ "\"\n}";
			Reporter.log(
					"************************************************************************************************************************");
			Reporter.log("<b>Unlock Misidn Request : </b>" + requestBody);
			Reporter.log("Request Type : POST");

			RestService restService = new RestService(requestBody, eos_clear_token_url);
			restService.addHeader("authorization", "Bearer " + accessToken);
			restService.addHeader("Accept", "application/json");
			Reporter.log(eos_clear_token_url + "/oauth2/v1/togglehardlockflag");
			Response response = restService.callService("oauth2/v1/togglehardlockflag", RestCallType.POST);
			// System.out.println("Response of Unlock Misidn Service in :" +
			// environment + ":" + response.body().asString());
			if (response != null && "200".equals(Integer.toString(response.getStatusCode()))) {
				responseStatus = response.getStatusCode();
				logSuccessResponse(response, "togglehardlockflag");
				return responseStatus;
			} else {
				Reporter.log("<b>Unlock Misidn Service Failure Response : </b>");
				failAndLogResponse(response, "togglehardlockflag");
				Assert.fail("<b>Failed to Unlock Misidn :</b> " + response.body().asString());
			}

		} catch (Exception e) {
			Reporter.log(" <b>Failed to Unlock Misidn : Exception Response :</b> ");
			Reporter.log(" " + e);
		}
		return responseStatus;
	}

	/**
	 * generate a unique amount value
	 * 
	 * @return double - amount
	 */
	public Double generateRandomAmount() {
		double min = 1.01;
		double max = 2.00;
		Random r = new Random();
		return r.nextDouble() * (max - min) + min;
	}

	public String getJSONfromResponse(Response response) {
		return response.asString();
	}

	public String invertBooleanValue(String value) {
		Boolean booleanVal = new Boolean(value);
		if (booleanVal == true) {
			booleanVal = false;
		} else {
			booleanVal = true;
		}
		return booleanVal.toString();
	}

	public String getMsisdnAndPaswordFromFilter(ApiTestData apiTestData) {
		try {
			String fil = apiTestData.getfilter();
			if (StringUtils.isEmpty(fil)) {

				return apiTestData.getMsisdn() + "/" + apiTestData.getPassword();
			}

			String query = "$.[?(" + fil + ")].misdin";
			List<String> tstdata = com.jayway.jsonpath.JsonPath
					.parse(new URL(Constants.TEST_DATA_FILTER_LOCATION_QLAB02)).read(query);
			if (tstdata.size() > 0) {
				Random rand = new Random();
				String randomElement = tstdata.get(rand.nextInt(tstdata.size()));
				return randomElement;
			}
			return null;

		} catch (Exception e) {
			return null;
		}
	}

	public void logServiceEndPoint(String eos_base_url, String resourceURL, String requestType) {
		Reporter.log("Service End Point: " + requestType + " : " + eos_base_url + "/" + resourceURL);

	}

	public Response invokeRestServiceCall(Map<String, String> headers, String eosBaseURL, String resourceUrl,
			String requestBody, RestServiceCallType serviceCallType) throws Exception {
		Response response = null;
		RestService service = new RestService(requestBody, eosBaseURL);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		logServiceEndPoint(eosBaseURL, resourceUrl, serviceCallType.toString());

		switch (serviceCallType) {
		case GET:
			response = service.callService(resourceUrl, RestCallType.GET);
			break;
		case POST:
			response = service.callService(resourceUrl, RestCallType.POST);
			break;
		case PUT:
			response = service.callService(resourceUrl, RestCallType.PUT);
			break;
		case DELETE:
			response = service.callService(resourceUrl, RestCallType.DELETE);
			break;
		}
		// System.out.println("Response: " +response.body().asString());
		return response;

	}

	public String getEncryptedValue(ApiTestData apiTestData, Map<String, String> tokenMap, String strToEncrypt) {
		String encryptedValStr = null;
		PublicKey pub = null;
		try {

			ObjectMapper mapper = new ObjectMapper();
			JsonNode publicKeyNode = getPublicKey(apiTestData, tokenMap);
			if (publicKeyNode != null) {
				String test = StringEscapeUtils.unescapeJava(publicKeyNode.asText());
				JsonNode readTree = mapper.readTree(test);
				if (!readTree.isMissingNode() && readTree.has("e")) {
					String e = readTree.path("e").asText();
					String n = readTree.path("n").asText();
					// System.out.println("E::" + e + "::n::" + n);
					byte exponentB[] = Base64.getUrlDecoder().decode(e);
					byte modulusB[] = Base64.getUrlDecoder().decode(n);
					BigInteger exponent = new BigInteger(toHexFromBytes(exponentB), 16);
					BigInteger modulus = new BigInteger(toHexFromBytes(modulusB), 16);
					RSAPublicKeySpec spec = new RSAPublicKeySpec(modulus, exponent);
					KeyFactory factory = KeyFactory.getInstance("RSA");
					pub = factory.generatePublic(spec);
				}
				Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPPadding");
				OAEPParameterSpec oaepParams = new OAEPParameterSpec("SHA-256", "MGF1", MGF1ParameterSpec.SHA256,
						PSpecified.DEFAULT);
				cipher.init(Cipher.ENCRYPT_MODE, pub, oaepParams);
				// System.out.println(cipher.getProvider());
				// System.out.println(cipher.getAlgorithm());
				byte[] encryptedVal = cipher.doFinal(strToEncrypt.getBytes("UTF-8"));

				// System.out.println("Encrypted Val :: " +
				// DatatypeConverter.printHexBinary(encryptedVal));
				encryptedValStr = DatatypeConverter.printHexBinary(encryptedVal);
			} else {
				// System.out.println("Exception while generating publickey
				// "+publicKeyNode );
			}
		} catch (Exception e) {
			// System.out.println("Exception while encrypting : " + e);
		}
		return encryptedValStr;
	}

	public void checkexpectedvalues(Response response, String tag, String expectedvalue) {

		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();

			if (jsonPathEvaluator.get(tag) != null) {
				if (jsonPathEvaluator.get(tag).toString().equalsIgnoreCase(expectedvalue))
					Reporter.log(tag + " value is " + expectedvalue);
				else
					Verify.fail(tag + " Expected value is: " + expectedvalue + " Actual Value:"
							+ jsonPathEvaluator.get(tag).toString());
			} else {
				Verify.fail(tag + " is not existing");
			}
		} else {
			Assert.fail("Response is:" + response.getStatusCode());
		}
	}

	public JsonNode getParentNodeFromResponse(Response response) {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = null;
		try {
			jsonNode = mapper.readTree(response.asString());
		} catch (JsonProcessingException e) {
		//	Assert.fail("<b>JsonProcessingException Response :</b> " + e);
			Reporter.log(" <b>JsonProcessingException Response :</b> ");
			Reporter.log(" " + e);
			// e.printStackTrace();
		} catch (IOException e) {
		//	Assert.fail("<b>IOException Response :</b> " + e);
			Reporter.log(" <b>IOException Response :</b> ");
			Reporter.log(" " + e);
			// e.printStackTrace();
		}
		return jsonNode;
	}

	public String extractPayLoadFromFile(String file) throws Exception {
		String requestBody = "";
		try {
			requestBody = new ServiceTest().getRequestFromFile(file);
		} catch (FileNotFoundException fe) {
			throw new FileNotFoundException("File not Found :" + file);
		} catch (Exception e) {
			throw new Exception("Issue while invoking file :" + file);
		}
		return requestBody;
	}

	public Map<String, String> getUserDetailsFromUsertokenResponse(Response response, String misdin) {
		Map<String, String> userDetailsMap = new HashMap<String, String>();
		if (response.body().asString() != null && response.getStatusCode() == 200) {
			JsonPath jsonPathEvaluator = response.jsonPath();
			if (jsonPathEvaluator.get("access_token") != null) {
				userDetailsMap.put("misdin", misdin);
				userDetailsMap.put("accessToken", jsonPathEvaluator.get("access_token").toString());
				userDetailsMap.put("jwtToken", jsonPathEvaluator.get("id_tokens[0].'id_token.direct'").toString());
				userDetailsMap.put("ban", jsonPathEvaluator.get(
						"userInfo.associatedLines.findAll{it.msisdn=='" + misdin + "'}[0].primaryBillingAccountCode")
						.toString());
				if (jsonPathEvaluator.get("userInfo.associatedLines.findAll{it.msisdn=='" + misdin
						+ "'}[0].multiLineCustomerType") != null) {
					userDetailsMap.put("multiLineCustomerType", jsonPathEvaluator.get(
							"userInfo.associatedLines.findAll{it.msisdn=='" + misdin + "'}[0].multiLineCustomerType")
							.toString());
				} else {
					userDetailsMap.put("multiLineCustomerType",
							jsonPathEvaluator.get("userInfo.extendedLines[0].contracts.findAll{it.msisdn=='" + misdin
									+ "'}[0].multiLineCustomerType").toString());
				}
				//userDetailsMap.put("userId", jsonPathEvaluator.get("userInfo.iamProfile.userId").toString());
				userDetailsMap.put("email", jsonPathEvaluator.get("userInfo.iamProfile.email").toString());
				userDetailsMap.put("firstname", jsonPathEvaluator.get("userInfo.iamProfile.firstname").toString());
				userDetailsMap.put("lastname", jsonPathEvaluator.get("userInfo.iamProfile.lastname").toString());
				userDetailsMap.put("accountTypeSubType",
						jsonPathEvaluator.get(
								"userInfo.associatedLines.findAll{it.msisdn=='" + misdin + "'}[0].accountTypeSubType")
								.toString());
				if (jsonPathEvaluator
						.get("userInfo.associatedLines.findAll{it.msisdn=='" + misdin + "'}[0].status") != null) {
					userDetailsMap.put("status", jsonPathEvaluator
							.get("userInfo.associatedLines.findAll{it.msisdn=='" + misdin + "'}[0].status").toString());
				} else {
					userDetailsMap.put("status", jsonPathEvaluator
							.get("userInfo.extendedLines[0].contracts.findAll{it.msisdn=='" + misdin + "'}[0].status")
							.toString());
				}
				if (jsonPathEvaluator.get("userInfo.extendedLines.contracts[0].multiLineCustomerType").toString()
						.indexOf(",") < 0) {
					userDetailsMap.put("totalLines", "1");
				} else {
					userDetailsMap.put("totalLines",
							Integer.toString(
									jsonPathEvaluator.get("userInfo.extendedLines.contracts[0].multiLineCustomerType")
											.toString().split(",").length));
				}

				return userDetailsMap;
			}

		}

		return null;
	}

}