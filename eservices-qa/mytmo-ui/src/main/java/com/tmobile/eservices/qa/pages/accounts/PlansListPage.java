/**
 * 
 */
package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author csudheer
 *
 */
public class PlansListPage extends CommonPage {

	@FindBy(css = "input[soc='NCTM1WRTE']")
	private WebElement tMobileOneNCCWearableTE;

	@FindBy(id = "changeNoticeContinueBtn")
	private WebElement changeNoticeBtn;

	@FindBy(id = "nextButton_id")
	private WebElement nextButton;

	@FindBy(css = ".alert p")
	private WebElement verifyNonIOTPlanMessage;

	@FindBy(css = "a.ui_primary_link.planServiceModal")
	private List<WebElement> moreDetailsLink;

	@FindBy(linkText = "Back")
	private WebElement backBtn;

	@FindAll({ @FindBy(css = "#addOrRemoveText_id > h1"), @FindBy(id = "addRemoveDataSrvc_id") })
	private WebElement changePlanheader;

	@FindBy(id = "planName_id")
	private WebElement planNameId;

	@FindBy(css = "table.ui_mobile_headline")
	private WebElement planDetailsHeader;

	@FindBy(css = "ul.plan-details-list")
	private WebElement planDetails;

	@FindBy(css = "th[class='change-plan-table-heading']")
	private List<WebElement> allOptions;

	private final String pageUrl = "/plans-list.html";

	public PlansListPage(WebDriver webDriver) {
		super(webDriver);
	}

	public PlansListPage verifyPlansListPage() {
		checkPageIsReady();
		waitForImageSpinnerInvisibility();
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public PlansListPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	public PlansListPage verifyIOTPlanMessage(String msg) {
		try {
			checkPageIsReady();
			verifyNonIOTPlanMessage.getText().contains(msg);
			Reporter.log("Looks like IOT plans are unavailable on web. Please contact care");
		} catch (Exception e) {
			Assert.fail("Looks like IOT plans are unavailable on web. Please contact care Message not displayed ");
		}
		return this;
	}

	public PlansListPage verifyOptionsareOnlyMBB() {
		try {
			checkPageIsReady();

			for (WebElement we : allOptions) {
				Assert.assertTrue(
						(we.getText().contains("Mobile Internet")) || (we.getText().contains("T-Mobile ONE Tablet")));
			}

			Reporter.log("Only MBB Options are displayed");
		} catch (Exception e) {
			Assert.fail("Only MBB Options are not displayed");
		}
		return this;
	}

	public PlansListPage verifysupressionofJUMPONDMI() {
		try {
			checkPageIsReady();

			for (WebElement we : allOptions) {
				Assert.assertFalse(we.getText().contains("LifetimeCoverageGuarantee"));
			}

			Reporter.log("LifetimeCoverageGuarantee is supressed");
		} catch (Exception e) {
			Assert.fail("LifetimeCoverageGuarantee is not supressed");
		}
		return this;
	}
}
