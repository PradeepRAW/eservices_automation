package com.tmobile.eservices.qa.pages.payments.api;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.EOSCommonLib;

import io.restassured.response.Response;

public class AccountActivitiesMiddleware extends EOSCommonLib {

	public Response getResponsePaymentHistory(String soaprequest) throws Exception {
		Response response = null;

		RestService restService = new RestService(soaprequest, "https://tugs-tmo.internal.t-mobile.com");
		restService.setContentType("text/xml");
		restService.addHeader("soapaction", "gs.BillPay.getPaymentHistory");

		// LocalDate a = LocalDate.now();
		response = restService.callService("genericesp/soap", RestCallType.POST);

		return response;
	}

}
