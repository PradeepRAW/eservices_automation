package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author charshavardhana
 *
 */
public class ReturnPolicyPage extends CommonPage {

	public static final String returnPolicyPageUrl = "/return-policy";
	public static final String returnPolicyPageLoadText = "Return policy";

	@FindBy(xpath = "//h3//em[contains(text(), 'Return policy')]")
	private WebElement legacyReturnPolicyPage;

	/**
	 * 
	 * @param webDriver
	 */
	public ReturnPolicyPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Legacy Coverage Page
	 * 
	 * @return
	 */
	public ReturnPolicyPage verifyReturnPolicyPage() {
		try {
			waitforSpinnerinProfilePage();
			checkPageIsReady();
			verifyPageLoaded();
			verifyPageUrl(returnPolicyPageUrl);
			Reporter.log("Return policy Page is displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Return policy Page not displayed");
		}
		return this;

	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the ReturnPolicyPage class instance.
	 */
	public ReturnPolicyPage verifyPageLoaded() {
		checkPageIsReady();
		getDriver().getPageSource().contains(returnPolicyPageLoadText);
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the ReturnPolicyPage class instance.
	 */
	public ReturnPolicyPage verifyPageUrl() {
		checkPageIsReady();
		getDriver().getCurrentUrl().contains(returnPolicyPageUrl);
		return this;
	}

}
