package com.tmobile.eservices.qa.api.eos.monitors;

import org.testng.Reporter;

import com.tmobile.eservices.qa.api.exception.ServiceFailureException;
import com.tmobile.eservices.qa.common.ShopConstants;
import com.tmobile.eservices.qa.data.ApiTestData;

public class StandardUpgradeFRPFlow extends OrderProcess {

	/*
	 * 
	 * Steps For Standard Upgrade EIP Flow,
	 * 1. getCatalogProducts
	 * 2. Create Cart
	 * 3. Create Quote
	 * 4. Update Quote
	 * 5. Make Payment
	 * 6. Place Order
	 * 
	 * */
	
	@Override
	public void createCart(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "createCartAPI_FRP_Order.txt");
			invokeCreateCart(apiTestData, requestBody);			
		} catch (Exception e) {
			Reporter.log("Service issue while making Create Cart : " + e);
			throwServiceFailException();
		}
	}

	@Override
	public void updateQuote(ApiTestData apiTestData) throws ServiceFailureException {
		try {
			String requestBody = extractPayLoadFromFile(ShopConstants.SHOP_MONITORS + "UpdateQuote_FRP_req.txt");
			invokeUpdateQuote(apiTestData, requestBody);
		} catch (Exception e) {
			Reporter.log("Service issue while invoking the Update Quote : " + e);
			throwServiceFailException();
		}

	}

}
