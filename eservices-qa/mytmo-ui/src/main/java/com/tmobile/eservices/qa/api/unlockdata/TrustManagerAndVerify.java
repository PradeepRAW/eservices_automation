package  com.tmobile.eservices.qa.api.unlockdata;

import java.security.KeyManagementException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.glassfish.jersey.SslConfigurator;


public class TrustManagerAndVerify{

	/**
	 * 
	 * @author blakshminarayana
	 *
	 */
	private static class TrustAllCertificates implements X509TrustManager {
		/**
		 * This method to check the trusted issuers or not
		 */
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return Collections.emptyList().toArray(new X509Certificate[0]);
		}

		/**
		 * This method is verifying checkClientTrusted This is override method
		 * which is not implemented
		 */
		public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
				throws CertificateException {
			// override method no need to write any logic here
		}

		/**
		 * This is override method which is not implemented
		 */
		public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
				throws CertificateException {
			// override method no need to write any logic here
		}

	}

	/**
	 * 
	 * @return
	 */
	public  HostnameVerifier insecureHostNameVerifier() {
		return new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};
	}
	/**
	 * 
	 * @return
	 */
	public SSLContext getContext(){
		SslConfigurator sslConfig = SslConfigurator.newInstance();
		TrustManager[] trustAll = new TrustManager[] {new TrustAllCertificates()};
		SSLContext sslContext = sslConfig.createSSLContext();
		try {
			sslContext.init(null, trustAll, new SecureRandom());
		} catch (KeyManagementException exp) {
			throw new ServiceException("unable to get the context", exp);
		}
		return sslContext;
	}

}
