package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author speddis
 *
 */
public class ShopHomePage extends CommonPage {

	@FindBy(css = "div.main-content p.Display2")
	private WebElement pageHeader;

	@FindBy(xpath = "//p[contains(text(),'Featured phones')]")
	private WebElement featuredPhonesTitle;

	private final String pageUrl = "purchase/shop";

	/**
	 * Constructor
	 * 
	 * @param webDriver
	 */
	public ShopHomePage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public ShopHomePage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}

	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public ShopHomePage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
		} catch (Exception e) {
			Assert.fail("'Shop Home Page found.");
		}
		return this;
	}

	/**
	 * verify AAL ineligible popup.
	 *
	 * @return the ShopPage class instance.
	 */
	public ShopHomePage verifyforWaitFeaturedPhoneTitle() {
		try {
			waitFor(ExpectedConditions.visibilityOf(featuredPhonesTitle));
		} catch (Exception e) {
			Assert.fail("featuredPhonesTitle is not Displayed");
		}
		return this;
	}

}
