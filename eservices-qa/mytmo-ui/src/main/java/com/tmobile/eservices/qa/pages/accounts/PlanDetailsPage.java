package com.tmobile.eservices.qa.pages.accounts;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * 
 * @author Sudheer Reddy Chilukuri
 *
 */
public class PlanDetailsPage extends CommonPage {

	// Headers
	@FindBy(css = "div.Display5")
	private List<WebElement> listOfheaders;

	@FindBy(css = "span[aria-label='Plan Details']")
	private List<WebElement> planDetailsTitle;

	// Buttons

	@FindBy(css = "button[aria-label='Select Plan']")
	private WebElement selectPlanButton;

	@FindBy(css = "button[class*='SecondaryCTA']")
	private List<WebElement> listOfBtns;

	@FindBy(css = "button.SecondaryCTA")
	private WebElement disabledChnageplanCta;

	@FindBy(css = "button.PrimaryCTA.full-btn-width-xs")
	private WebElement selectPlanCta;

	@FindBy(css = "button.SecondaryCTA.blackCTA.full-btn-width")
	private List<WebElement> manageAddonAndChangePlanBtns;

	// Bottom links

	@FindAll({ @FindBy(linkText = "contact care"), @FindBy(xpath = "//button[contains(text(),'Contact US')]") })
	private WebElement contactus;

	@FindBy(xpath = "//p[contains(text(),'Add TV Service')]")
	private WebElement lineName;

	@FindBy(css = "div[class*='H6-heading d-inline']")
	private List<WebElement> includedPlans;

	@FindBy(xpath = "//div[contains(text(),'Plan Details')]")
	private WebElement pageHeading;

	@FindBy(xpath = "//p[contains(text(),'Test Msisdn One')]")
	private WebElement pooledpalnName;

	@FindBy(xpath = "//span[contains(text(),'MBB Line - SOC2 - 4049666524')]")
	private WebElement pooledNavigatepalnName;

	@FindBy(css = "span.font-weight-bold")
	private WebElement netPrice;

	@FindBy(xpath = "//div[contains(text(),'Add TV Service')]")
	private WebElement associatedLine;

	@FindBy(css = "div.H6-heading.black")
	private List<WebElement> lineSharedAddons;

	@FindBy(css = "p.body")
	private List<WebElement> sharedAddonDiscount;

	@FindBy(xpath = "p.padding-bottom-xxxl.legal.black")
	private WebElement termsDescription;

	@FindBy(xpath = "//span[contains(text(),'See full details')]")
	private WebElement seeFullDetails;

	@FindBy(xpath = "//div[contains(text(),'Shared Add-ons')]")
	private WebElement sharedAddon;

	@FindBy(css = "div.Display5")
	private WebElement termsandcondition;

	@FindBy(css = "div.H6-heading.black")
	private List<WebElement> planDetailsSharedAddons;

	@FindBy(css = "div.Display4")
	private WebElement planDetailsPageHeader;

	@FindBy(css = "span.legal-bold-link.black.cursor.anchorColor.text-decoration-underline")
	private WebElement underlineSeemore;

	@FindBy(css = "span.H4-heading")
	private WebElement planName;

	@FindBy(css = "div.H6-heading.d-inline-block")
	private WebElement includedPlansList;

	@FindBy(css = "p.legal")
	private WebElement legalText;

	@FindBy(css = "span.legal-bold-link")
	private WebElement seeFullDetailsLink;

	@FindBy(css = "div.mt-10")
	private WebElement clickPlanName;

	@FindBy(css = "span.d-flex")
	private WebElement suspendedError;

	@FindBy(css = "li span[role*='link']")
	private List<WebElement> breadcrumbLInks;

	@FindBy(css = "div.TMO-DEVICE-USER-INFO-BLADE-PAGE div[class='H6-heading'")
	private List<WebElement> listOfAssociatedLines;

	@FindAll({ @FindBy(css = "div.plan-n-i.H4-heading"), @FindBy(css = "div.plan-n-i.body") })
	private WebElement planNameText;

	@FindBy(css = "p.padding-bottom-xl.legal.black")
	private WebElement notificationError;

	@FindBy(css = "div.plan-n-i.body")
	private WebElement verifyPlanName;

	@FindBy(css = "div.Display5")
	private List<WebElement> verifyAssociatedPlanName;

	@FindBy(css = "div.H6-heading.d-inline-block")
	private List<WebElement> includedInThePlanNamesList;

	@FindBy(css = "div[class*='Display6']")
	private List<WebElement> priceNetfliXPremium;

	@FindBy(css = "span.col-12.aal-modal-header.black.no-padding.pull-left.body.padding-vertical-message.ng-tns-c12-0.ng-star-inserted")
	private WebElement pendingError;

	@FindBy(css = "span.f-l.statusHeight")
	private WebElement supends;

	@FindBy(css = "span.d-none.d-md-block.body.white.f-l")
	private WebElement restore;

	@FindBy(xpath = "(//*[@class='plan-n-i body']/div)")
	private WebElement currentPlanName;

	@FindBy(xpath = "//div[contains(text(),'Change plan')]")
	private WebElement changePlanCTA;
	
	@FindBy(xpath = "//div[contains(@class,'dialog animation-out animation')]/div/span")
	private WebElement pendingRatePlanMessage;
	
	@FindBy(xpath = "//img[@class='close-img-aal']")
	private WebElement closeButtonForPendingRatePlanMessage;
	
	@FindBy(xpath = "//*[contains(@class,'PrimaryCTA-accent')]")
	private WebElement contactUsCTAOnPendingRatePlanDialogue;
	


	public PlanDetailsPage verifyContactUsPage() {
		checkPageIsReady();
		try {
			Assert.assertTrue(contactus.isDisplayed());
			Reporter.log("ContactUs Cta is available on the Error ModalWindow");
			contactus.click();
			String currentUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(currentUrl.contains("/contact-us"), "ContactUs Page is loaded");
		} catch (Exception e) {
			Assert.fail("ContactUs Cta is not available on the Error ModalWindow");
		}
		return this;
	}

	/**
	 * 
	 * @param webDriver
	 */

	public PlanDetailsPage verifyforNetflixPremiumAddonsPrice() {
		try {
			Assert.assertTrue(verifyElementBytext(priceNetfliXPremium, "$3.00"));
			Reporter.log("Netflix Premium addon Price is Diplayed as Expectedt");
		} catch (Exception e) {
			Assert.fail("Netflix Premium addon Price is not Diplayed as Expectedt");
		}
		return this;
	}

	public PlanDetailsPage verifyforNetflixPremiumAddons() {
		try {
			Assert.assertTrue(verifyElementBytext(planDetailsSharedAddons,
					"Netflix Premium $13.99 with Fam Allowances ($19 value)"));
			Reporter.log("Netflix Premium addon is available");
		} catch (Exception e) {
			Assert.fail("Netflix Premium addon is available");
		}
		return this;
	}

	public PlanDetailsPage verifySharedAddonComponent() {
		try {
			Assert.assertTrue(sharedAddon.isDisplayed());
			Reporter.log("Shared Addon Component is displayed");
		} catch (Exception e) {
			Assert.fail("Shared Addon Component is not displayed on planDetails Page");
		}
		return this;
	}

	public PlanDetailsPage verifyAssociatedPlanName() {
		checkPageIsReady();
		try {
			String name1 = verifyPlanName.getText().concat(" Lines");
			String name2 = verifyAssociatedPlanName.get(1).getText();
			Assert.assertTrue(name1.equalsIgnoreCase(name2));
			Reporter.log(name2 + " " + "Associated planName is available instead of Associated lineHeading");
		} catch (Exception e) {
			Assert.fail("Associated planName is available instead of Associated lineHeading");
		}
		return this;
	}

	public PlanDetailsPage verifyNotificationText() {
		try {
			checkPageIsReady();
			Assert.assertTrue(notificationError.getText()
					.contains("Your plan type does not support changing your rate plan online"));
			Reporter.log("Notication Error Message is displayed below the CTA");
		} catch (Exception e) {
			Assert.fail("ChangePlan CTA  is not available");
		}
		return this;
	}

	public PlanDetailsPage verifyDisabledChangePlanCta() {
		try {
			checkPageIsReady();
			Assert.assertTrue(disabledChnageplanCta.getAttribute("disabled").contains("true"),
					"Disabled button is not diaplsying");
			Reporter.log("Disabled button is Disabled");
		} catch (Exception e) {
			Assert.fail("Disabled button is not diaplsying");
		}
		return this;
	}

	public PlanDetailsPage clcikonBusinessPlan() {
		try {
			checkPageIsReady();
			Assert.assertTrue(planNameText.getText().contains("T-Mobile ONE @Work"));
			planNameText.click();
			Reporter.log("Clicked on planName and Navigated to PlanDetails Page");
		} catch (Exception e) {
			Assert.fail("Business Plan is not available");
		}
		return this;
	}

	public PlanDetailsPage verifyManageAddonCta() {
		try {
			checkPageIsReady();
			Assert.assertTrue(manageAddonAndChangePlanBtns.get(1).getText().contains("Manage Add-Ons"));
			Reporter.log("ManageAddonCta is availale on PlanDetails Page for Non-Pooled User");
		} catch (Exception e) {
			Assert.fail("ManageAddonCta is not availale");
		}
		return this;
	}

	public PlanDetailsPage clikOnPlanName() {
		try {
			checkPageIsReady();
			Assert.assertTrue(clickPlanName.isDisplayed());
			clickPlanName.click();
			Reporter.log("Clciked planNmae and Navigated PlanDetails Page");
		} catch (Exception e) {
			Assert.fail("Unable to Find the PlanNmae");
		}
		return this;
	}

	public PlanDetailsPage verifyTermsandcondition() {
		try {
			checkPageIsReady();
			seeFullDetails.isDisplayed();
			Reporter.log("TermsAndCondition is displyed under changePlan CTA");
		} catch (Exception e) {
			Assert.fail("TermsAndCondition is not available");
		}
		return this;
	}

	public PlanDetailsPage verifyPlanDetailsPage() {
		try {
			checkPageIsReady();
			Assert.assertTrue(planDetailsPageHeader.getText().equalsIgnoreCase("Plan Details"));
			Reporter.log("Plan Details header is displayed");
		} catch (Exception e) {
			Assert.fail("Plan Details header is not displaying");
		}
		return this;
	}

	public PlanDetailsPage verifyTermsandconditionPage() {
		try {
			checkPageIsReady();
			verifyElementByText(termsandcondition, "Important Rate Plan Information");
			Reporter.log("TermsAndCondition page is loaded");
		} catch (Exception e) {
			Assert.fail("TermsAndCondition page is not loaded");
		}
		return this;
	}

	public PlanDetailsPage clickSeeFullDetails() {
		try {
			checkPageIsReady();
			seeFullDetails.click();
			Reporter.log("Clicked on SeeFullDetails and Navigated to TermsandConditionPage");
		} catch (Exception e) {
			Assert.fail("SeeFullDetails link is not available to click");
		}
		return this;
	}

	public PlanDetailsPage sharedAddonDiscount() {
		try {
			checkPageIsReady();
			sharedAddonDiscount.get(2).isDisplayed();
			Reporter.log("SharedAddonDiscount is available on PlanDetailsPage");
		} catch (Exception e) {
			Assert.fail("SharedAddonDiscount is not available on PlanDetailsPage");
		}
		return this;
	}

	public PlanDetailsPage associatedlineSharedAddon() {
		try {
			checkPageIsReady();
			lineSharedAddons.get(0).isDisplayed();
			Reporter.log("AssociatedlineSharedAddon is displayed in planDetailsPage");
		} catch (Exception e) {
			Assert.fail("AssociatedlineSharedAddon is not displayed in planDetailsPage");
		}
		return this;
	}

	public PlanDetailsPage verifyPageHeading() {
		try {
			checkPageIsReady();
			pageHeading.isDisplayed();
			Reporter.log("PlanDetails heading is available on the planDetailsPage ");
		} catch (Exception e) {
			Assert.fail("PlanDetails heading is not available on the planDetailsPage ");
		}
		return this;
	}

	public PlanDetailsPage clickOnBackButton() {
		try {
			checkPageIsReady();
			clickOnBackBtn();
			Reporter.log("Clicked on backbutton and Navigated back to Account Overview Page ");
		} catch (Exception e) {
			Assert.fail("back is not available to click on PlanDetailsPage ");
		}
		return this;
	}

	public PlanDetailsPage clickOnChangePlanButton() {
		try {
			checkPageIsReady();
			clickElementBytext(listOfBtns, "Manage my plan");
			Reporter.log("Clicked on changePlanButton  ");
		} catch (Exception e) {
			Assert.fail("Not Clicked on changePlanButton ");
		}
		return this;
	}

	public PlanDetailsPage clickOnManageAddOnsButton() {
		try {
			checkPageIsReady();
			clickElementBytext(listOfBtns, "Manage Add-Ons");
			Reporter.log("Clicked on Manage Add ons button  ");
		} catch (Exception e) {
			Assert.fail("Not Clicked on Manage Add ons button ");
		}
		return this;
	}

	public PlanDetailsPage verifyPlanDetailsPageLoad() {
		try {
			checkPageIsReady();
			String currentUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(currentUrl.contains("/plan-details"), "PlanDetails Page is loaded");
		} catch (Exception e) {
			Assert.fail("PlanDetails Page is not loaded ");
		}

		return this;
	}

	public PlanDetailsPage(WebDriver webDriver) {
		super(webDriver);
	}

	public PlanDetailsPage verifyPlanDetailsPageHeaders() {
		try {
			checkPageIsReady();
			verifyListOfElementsBytext(listOfheaders, "Included in the plan");
			verifyListOfElementsBytext(listOfheaders, "Shared Add-ons");
			verifyListOfElementsBytext(listOfheaders, "Associated Lines");
			Reporter.log("includedInthePlan Heading is available on the planDetailsPage ");
		} catch (Exception e) {
			Assert.fail("includedInthePlan Heading is not available on the planDetailsPage ");
		}
		return this;
	}

	public PlanDetailsPage clickOnIncludedPlan() {
		try {
			checkPageIsReady();
			includedPlans.get(1).click();
			Reporter.log("Clicked on FeaturePlan and Navigated to Feature Details Page ");
		} catch (Exception e) {
			Assert.fail("FeaturePlan is not available to click ");
		}
		return this;
	}

	public PlanDetailsPage verifyPlanNameBreadCrump() {
		try {
			checkPageIsReady();
			Assert.assertTrue(planDetailsPageHeader.getText().contains("Plan Details"));
			Reporter.log("Plan Name bread crump verified");
		} catch (Exception e) {
			Assert.fail("Failed verify Plan Name bread crump");
		}
		return this;
	}

	/**
	 * Verify on plan details page
	 * 
	 */
	public PlanDetailsPage verifyIncludedPlansList() {
		try {
			Assert.assertTrue(includedPlansList.isDisplayed());
			Reporter.log("Plan name is displayed ");
		} catch (Exception e) {
			Assert.fail("Plan name is not displayed");
		}
		return this;
	}

	/**
	 * Verify on plan details page
	 * 
	 */
	public PlanDetailsPage clickOnButtonByName(String changePlan) {
		try {

			clickElementBytext(listOfBtns, changePlan);
			Reporter.log("Plan name is displayed ");
		} catch (Exception e) {
			Assert.fail("Plan name is not displayed");
		}
		return this;
	}

	/**
	 * Verify Legal Text
	 * 
	 */
	public PlanDetailsPage verifyLegalText() {
		try {
			Assert.assertTrue(legalText.isDisplayed());
			Reporter.log("Legal Text is displayed ");
		} catch (Exception e) {
			Assert.fail("Legal Text is not displayed");
		}
		return this;
	}

	/**
	 * Verify Legal Text
	 * 
	 */
	public PlanDetailsPage clickOnSeeFullDetailsLink() {
		try {
			seeFullDetailsLink.click();
			Reporter.log("Clicked on see Full Details Link");
		} catch (Exception e) {
			Assert.fail("See Full Details Link is not displayed");
		}
		return this;
	}

	public PlanDetailsPage clickOnbreadcrumbLInks(int index) {
		try {
			breadcrumbLInks.get(index).click();
		} catch (Exception e) {
			Assert.fail("Breadcrumb Links are not displayed");
		}
		return this;
	}

	public PlanDetailsPage verifyPlanDetails() {
		try {
			Assert.assertTrue(planNameText.isDisplayed(), "Plan Name is not displayed");
		} catch (Exception e) {
			Assert.fail("Plan Name is not displayed");
		}
		return this;
	}

	public PlanDetailsPage verifyIncludedInThePlanNamesList(List<String> list, String planName) {
		try {
			waitforSpinner();
			checkPageIsReady();
			Assert.assertTrue(planNameText.getText().contains(planName), planName + " is not displaying");
			for (WebElement string : includedInThePlanNamesList) {
				waitFor(ExpectedConditions.visibilityOf(string));
				String[] lines = string.getText().replaceAll("[^\\x00-\\x7f]", "").split("\\n");
				Assert.assertTrue(list.contains(lines[0]), lines[0] + "   not diaplying");
			}
		} catch (Exception e) {
			Assert.fail("Plan Name is not displayed");
		}
		return this;
	}

	public List<String> verifyTMOONENCIncludedInThePlanNamesList() {

		List<String> ls = new ArrayList<>();
		ls.add("Talk & text");
		ls.add("High-speed data");
		ls.add("Mobile hotspot");
		ls.add("No annual service contract");
		ls.add("Wi-Fi calling");
		ls.add("Taxes and fees included");
		ls.add("Netflix");
		ls.add("HD video streaming");
		ls.add("Optimized video streaming");
		ls.add("Unlimited international texting from home");
		ls.add("Simple Global (texting and data abroad)");
		ls.add("Overseas voice calling");
		ls.add("Use your device in Mexico and Canada");
		ls.add("Gogo in-flight benefits");
		ls.add("Family Allowances");
		ls.add("T-Mobile Family Mode");
		ls.add("Video streaming included with Binge On");
		ls.add("Music streaming included with Music Freedom");
		ls.add("Overseas voice calling");
		ls.add("Name ID");
		ls.add("Voicemail to Text");
		ls.add("AutoPay discount");

		return ls;
	}

	public List<String> verifySCNONPOOLEDIncludedInThePlanNamesList() {

		List<String> ls = new ArrayList<>();
		ls.add("Talk & text");
		ls.add("High-speed data");
		ls.add("Mobile hotspot");
		ls.add("Wi-Fi calling");
		ls.add("Unused data rollover");
		ls.add("HD video streaming");
		ls.add("Optimized video streaming");
		ls.add("Video streaming included with Binge On");
		ls.add("Music streaming included with Music Freedom");
		ls.add("Unlimited international texting from home");
		ls.add("Simple Global (texting and data abroad)");
		ls.add("Overseas voice calling");
		ls.add("Gogo in-flight benefits");
		ls.add("T-Mobile Family Mode");
		return ls;
	}

	public List<String> verifyTMOONETIIncludedInThePlanNamesList() {

		List<String> ls = new ArrayList<>();
		ls.add("Talk & text");
		ls.add("High-speed data");
		ls.add("Mobile hotspot");
		ls.add("No annual service contract");
		ls.add("Wi-Fi calling");
		ls.add("Taxes and fees included");
		ls.add("Netflix");
		ls.add("HD video streaming");
		ls.add("Optimized video streaming");
		ls.add("Video streaming included with Binge On");
		ls.add("Music streaming included with Music Freedom");
		ls.add("Unlimited international texting from home");
		ls.add("Simple Global (texting and data abroad)");
		ls.add("Overseas voice calling");
		ls.add("Gogo in-flight benefits");
		ls.add("Voicemail to Text");
		ls.add("Use your device in Mexico and Canada");
		ls.add("Family Allowances");
		ls.add("T-Mobile Family Mode");
		ls.add("AutoPay discount");
		return ls;
	}

	public List<String> verifyPappyTIIncludedInThePlanNamesList() {

		List<String> ls = new ArrayList<>();
		ls.add("Talk & text");
		ls.add("High-speed data");
		ls.add("Mobile hotspot");
		ls.add("No annual service contract");
		ls.add("Wi-Fi calling");
		ls.add("Taxes and fees included");
		ls.add("Netflix");
		ls.add("HD video streaming");
		ls.add("Optimized video streaming");
		ls.add("Unlimited international texting from home");
		ls.add("Simple Global (texting and data abroad)");
		ls.add("Overseas voice calling");
		ls.add("Use your device in Mexico and Canada");
		ls.add("Gogo in-flight benefits");
		ls.add("Family Allowances");
		ls.add("T-Mobile Family Mode");
		ls.add("AutoPay discount");
		return ls;
	}

	public List<String> verifyPappyEssentialIncludedInThePlanNamesList() {

		List<String> ls = new ArrayList<>();
		ls.add("Talk & text");
		ls.add("High-speed data");
		ls.add("Mobile hotspot");
		ls.add("Wi-Fi calling");
		ls.add("No annual service contract");
		ls.add("Optimized video streaming");
		ls.add("Simple Global (texting and data abroad)");
		ls.add("Overseas voice calling");
		ls.add("Gogo in-flight benefits");
		ls.add("Overseas voice calling");
		ls.add("Use your device in Mexico and Canada");
		ls.add("T-Mobile Family Mode");
		ls.add("AutoPay discount");
		return ls;
	}

	public List<String> verifyUnlimited55plusIncludedInThePlanNamesList() {

		List<String> ls = new ArrayList<>();
		ls.add("Talk & text");
		ls.add("High-speed data");
		ls.add("Mobile hotspot");
		ls.add("Wi-Fi calling");
		ls.add("No annual service contract");
		ls.add("Taxes and fees included");
		ls.add("HD video streaming");
		ls.add("Optimized video streaming");
		ls.add("Video streaming included with Binge On");
		ls.add("Music streaming included with Music Freedom");
		ls.add("Unlimited international texting from home");
		ls.add("Simple Global (texting and data abroad)");
		ls.add("Overseas voice calling");
		ls.add("Gogo in-flight benefits");
		ls.add("Overseas voice calling");
		ls.add("Family Allowances");
		ls.add("T-Mobile Family Mode");
		ls.add("AutoPay discount");
		ls.add("Plan Requirements");
		ls.add("Netflix");
		ls.add("Use your device in Mexico and Canada");
		ls.add("Voicemail to Text");
		ls.add("Name ID");
		return ls;
	}

	public List<String> verifymilitaryPlanIncludedInThePlanNamesList() {

		List<String> ls = new ArrayList<>();
		ls.add("Talk & text");
		ls.add("High-speed data");
		ls.add("Mobile hotspot");
		ls.add("No annual service contract");
		ls.add("Wi-Fi calling");
		ls.add("Taxes and fees included");
		ls.add("Netflix");
		ls.add("HD video streaming");
		ls.add("Optimized video streaming");
		ls.add("Unlimited international texting from home");
		ls.add("Simple Global (texting and data abroad)");
		ls.add("Overseas voice calling");
		ls.add("Use your device in Mexico and Canada");
		ls.add("Gogo in-flight benefits");
		ls.add("Family Allowances");
		ls.add("T-Mobile Family Mode");
		ls.add("AutoPay discount");

		return ls;
	}

	public List<String> verifyBusniessLineIncludedInThePlanNamesList() {

		List<String> ls = new ArrayList<>();
		// no features is present
		return ls;
	}

	public void clickOnIncludedplanByName(String includedPLan) {

		clickElementBytext(includedInThePlanNamesList, includedPLan);
	}

	public PlanDetailsPage verifyforNetflixAddonsandPrice(String netflix, String price) {
		checkPageIsReady();
		try {
			// Assert.assertTrue(
			// getDriver().findElement(getNewLocator(planDetailsSharedAddons,
			// netflix)).getText().contains(price));
			// above statament is wrong please modify it
			Reporter.log(netflix + "price is displayed");
		} catch (Exception e) {
			Assert.fail(netflix + "price is not displayed");
		}
		return this;
	}

	public PlanDetailsPage verifyExplorePlanDetailsPageLoad() {
		try {
			String currentUrl = getDriver().getCurrentUrl();
			Assert.assertTrue(currentUrl.contains("/change-plan/plan-details"),
					"PlanDetails Page is loaded from Explore Page");
		} catch (Exception e) {
			Assert.fail("PlanDetails Page is not loaded ");
		}

		return this;
	}

	public String getCurrentPlanName() {
		String actualPlanName = null;
		try {
			currentPlanName.isDisplayed();
			actualPlanName = currentPlanName.getText().trim();
			Reporter.log("Clicked on plan Name Text");
		} catch (Exception e) {
			Assert.fail("Current Plan name is not displayed on Account Overview page");
		}
		return actualPlanName;
	}

	/**
	 * Verify shared addon component
	 */
	public PlanDetailsPage verifySharedAddonComponentNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(sharedAddon));
			Reporter.log("Shared Addon Component is not displayed on planDetails Page");
		} catch (Exception e) {
			Assert.fail("Shared Addon Component is displayed on planDetails Page");
		}
		return this;
	}

	/**
	 * Verify change plan cta
	 */
	public PlanDetailsPage verifyChangePlanCTA() {
		try {
			Assert.assertFalse(isElementDisplayed(changePlanCTA));
			Reporter.log("Change plan cta not displayed");
		} catch (Exception e) {
			Assert.fail("Change plan cta displayed");
		}
		return this;
	}

	/**
	 * Verify associated shared line
	 */
	public PlanDetailsPage associatedlineSharedAddonNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(lineSharedAddons.get(0)));
			Reporter.log("AssociatedlineSharedAddon not  displayed in planDetailsPage");
		} catch (Exception e) {
			Assert.fail("AssociatedlineSharedAddon is displayed in planDetailsPage");
		}
		return this;
	}

	public boolean verifyPlanDetailsTitle() {
		return !planDetailsTitle.isEmpty();
	}

	public PlanDetailsPage clickOnSelectPlanButton() {
		try {
			selectPlanButton.click();
			Reporter.log("Clicked on Select Plan Button");
		} catch (Exception e) {
			Assert.fail("Not clicked on Select Plan Button");
		}
		return this;
	}

	public PlanDetailsPage checkPendingRatePlanMessage() {
		try {
			Assert.assertEquals(pendingRatePlanMessage.getText().trim(),"You already have a pending plan or service change in your account","Pending rate Plan message is mismatched");
			Reporter.log("Pending rate plan message is displayed.");
		} catch (Exception e) {
			Assert.fail("Pending rate plan message is not displayed.");
		}
		return this;
	}
	
	public PlanDetailsPage clickOnCloseButtonOfPendingRatePlanDialogue() {
		try {
			closeButtonForPendingRatePlanMessage.click();
			Reporter.log("Close button clicked on Pending rate plan dialogue.");
		} catch (Exception e) {
			Assert.fail("Close button is not displayed on Pending rate plan dialogue");
		}
		return this;
	}
	
	public PlanDetailsPage clickOnContactUsCTAOfPendingRatePlanDialogue() {
		try {
			contactUsCTAOnPendingRatePlanDialogue.click();
			Reporter.log("Contact us CTA is clicked");
		} catch (Exception e) {
			Assert.fail("Contact us CTA is not displayed");
		}
		return this;
	}
}