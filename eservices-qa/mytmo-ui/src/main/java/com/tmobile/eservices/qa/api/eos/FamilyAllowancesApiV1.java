package com.tmobile.eservices.qa.api.eos;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class FamilyAllowancesApiV1 extends ApiCommonLib{
	
	/***
	 * FamilyAllowancesApiV1 API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response getParent(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildFamilyAllowancesAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v1/familyallowances/parent";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
    }     
    
    /***
	 * Profile API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response getMemoDetails(ApiTestData apiTestData,String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildFamilyAllowancesAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v1/familyallowances/memodetails";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
    }   
    

	/***
	 * Profile API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response getUsageSummary(ApiTestData apiTestData,String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildFamilyAllowancesAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v1/familyallowances/usage";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.POST);
		return response;
    }     
    
    /***
	 * Profile API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response setParent(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildFamilyAllowancesAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v1/familyallowances/parent";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
		return response;
    } 
    
    
    /***
	 * Profile API.
	 * parameters:
	 *	-  $ref: '#/parameters/Accept
	 *	-  $ref: '#/parameters/Content-Type
	 *	-  $ref: '#/parameters/X-B3-TraceId
	 *	-  $ref: '#/parameters/X-B3-SpanId
	 *	-  $ref: '#/parameters/channel_id
	 *	-  $ref: '#/parameters/Authorization
	 *	-  $ref: '#/parameters/application_id
	 *	-  $ref: '#/parameters/user-token
	 *	-  $ref: '#/parameters/tmo-id
	 *	-  $ref: '#/parameters/msisdn
	 *	-  $ref: '#/parameters/ban
	 *	-  $ref: '#/parameters/access_token
	 *	-  $ref: '#/parameters/application_client
	 *	-  $ref: '#/parameters/application_version_code
	 *	-  $ref: '#/parameters/device_os
	 *	-  $ref: '#/parameters/os_version
	 *	-  $ref: '#/parameters/os_language
	 *	-  $ref: '#/parameters/device_manufacturer
	 *	-  $ref: '#/parameters/device_model
	 *	-  $ref: '#/parameters/marketing_cloud_id
	 *	-  $ref: '#/parameters/sd_id
	 *	-  $ref: '#/parameters/adobe_uuid

	 * @return
	 * @throws Exception 
	 */
    public Response updateUsageSettings(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap) throws Exception{
		Map<String, String> headers = buildFamilyAllowancesAPIHeader(apiTestData, tokenMap);
		String resourceURL = "v1/familyallowances/usage";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, requestBody ,RestServiceCallType.PUT);
		return response;
    }   
    private Map<String, String> buildFamilyAllowancesAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
    	Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
		Map<String, String> headers = new HashMap<String, String>();				
		headers.put("Accept", "application/json");
		headers.put("Content-Type", "application/json");
		headers.put("X-B3-TraceId", "{{X-B3-TraceId}}");
		headers.put("X-B3-SpanId", "{{X-B3-SpanId}}");
		headers.put("channel_id", "DESKTOP");
		headers.put("Authorization", jwtTokens.get("jwtToken"));
		headers.put("application_id", "MYTMO");
		headers.put("user-token", "{{user-token}}");
		headers.put("tmo-id", "{{tmo-id}}");
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("ban", apiTestData.getBan());
		headers.put("usn", "{{usn}}");
		headers.put("application_client", "{{application_client}}");
		headers.put("application_version_code", "{{application_version_code}}");
		headers.put("device_os", "{{device_os}}");
		headers.put("os_version", "{{os_version}}");
		headers.put("os_language", "{{os_language}}");
		headers.put("device_manufacturer", "{{device_manufacturer}}");
		headers.put("device_model", "{{device_model}}");
		headers.put("marketing_cloud_id", "{{marketing_cloud_id}}");
		headers.put("sd_id", "{{sd_id}}");
		headers.put("adobe_uuid", "{{adobe_uuid}}");
		headers.put("access_token", jwtTokens.get("accessToken"));
		return headers;
	}
    public Response getMidCycleChanges(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception{
    	Map<String, String> headers = buildMidCycleChanges(apiTestData, tokenMap);
		String resourceURL = "v1/subscriber/lines/";
    	Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "" ,RestServiceCallType.GET);
		return response;
    } 
    private Map<String, String> buildMidCycleChanges(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
    	
    	Map<String,String> jwtTokens = checkAndGetJWTToken(apiTestData, tokenMap);
    	//clearCounters(apiTestData);
		Map<String, String> headers = new HashMap<String, String>();				
		headers.put("Accept", "application/json");
		headers.put("Content-Type", "application/json");
		headers.put("channel_id", "Desktop");
		headers.put("Authorization", jwtTokens.get("jwtToken"));
		headers.put("application_id", "");
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("ban", apiTestData.getBan());
		headers.put("usn", "TestUSN");
		headers.put("access_token", jwtTokens.get("accessToken"));
		headers.put("X-B3-TraceId", "FlexAPITest123");
		headers.put("X-B3-SpanId", "FlexAPITest456");
		headers.put("transactionid","FlexAPITest");
		return headers;
	}
    
    public static boolean isValidFormat(String format, String value) {
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return date != null;
    }
}
