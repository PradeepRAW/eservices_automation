package com.tmobile.eservices.qa.pages.tmng.functional;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

import io.appium.java_client.ios.IOSDriver;

public class TradeInValuePopUpModal extends TmngCommonPage {

	@FindBy(css = "[ng-bind-html*='tradeInCtrl.authorValue.tradeInTool.header']")
	private WebElement tradeInModalHeader;

	@FindBy(css = "[ng-model*='tradeInCtrl.model.formData.carrier']")
	private WebElement carrierTextBox;

	@FindBy(xpath = "//label[@id='carrierOptions']/following-sibling::div/ul/li")
	private List<WebElement> carrierOptions;

	@FindBy(css = "[ng-model*='tradeInCtrl.model.formData.make']")
	private WebElement makeTextBox;

	@FindBy(xpath = "//label[@id='makeOptions']/following-sibling::div/ul/li")
	private List<WebElement> makeOptions;

	@FindBy(css = "[ng-model*='tradeInCtrl.model.formData.deviceModel']")
	private WebElement modalTextBox;

	@FindBy(xpath = "//label[@id='deviceOptions']/following-sibling::div/ul/li")
	private List<WebElement> modalOptions;

	@FindBy(css = "[ng-model*='tradeInCtrl.model.formData.imei']")
	private WebElement imeiTextBox;

	@FindBy(css = "[ng-bind-html*=\"tradeInCtrl.authorValue.tradeInTool.imeiToolTipLabel\"]")
	private WebElement imei;

	@FindBy(css = "#deviceConditionGood input")
	private WebElement goodConditionRadioBtn;

	@FindBy(css = "#deviceConditionDamage input")
	private WebElement badConditionRadioBtn;

	@FindBy(css = "[ng-bind-html*='tradeInCtrl.authorValue.tradeInTool.imeiToolTipLabel']")
	private WebElement whereCanIFindText;

	@FindBy(css = "#submitTradeInCTA")
	private WebElement getEstimatedCTA;

	@FindBy(css = "li[ng-repeat='match in matches track by $index']")
	private List<WebElement> dropTable;

	public TradeInValuePopUpModal(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Select Carrier
	 * 
	 * @param option
	 */
	public TradeInValuePopUpModal selectCarrier(String option) {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(carrierTextBox),2);
			carrierTextBox.sendKeys(option);
			dropTable.get(0).click();
		} catch (Exception e) {
			Assert.fail("Carrier Element not clicked");
		}
		return this;
	}

	/**
	 * Select make Options
	 * 
	 * @param option
	 */
	public TradeInValuePopUpModal selectMakeOptions(String option) {
		try {
			makeTextBox.sendKeys(option);
			dropTable.get(0).click();
		} catch (Exception e) {
			Assert.fail("Make Options Element not clicked");
		}
		return this;
	}

	/**
	 * Select modal Options
	 * 
	 * @param option
	 */
	public TradeInValuePopUpModal selectModalOptions(String option) {
		try {
			checkPageIsReady();
			modalTextBox.sendKeys(option);
			dropTable.get(0).click();
		} catch (Exception e) {
			Assert.fail("Modal Options Element not clicked");
		}
		return this;
	}

	/**
	 * Set IMEI NUmber
	 * 
	 * @param imeiNumber
	 */
	public TradeInValuePopUpModal setIMEINumber(String option) {

		try {
			moveToElement(imeiTextBox);
			sendTextData(imeiTextBox, option);
			imeiTextBox.sendKeys(Keys.ENTER);
			imeiTextBox.sendKeys(Keys.TAB);
		} catch (Exception e) {
			Assert.fail("IMEI NUmber Element not clicked");
		}
		return this;
	}

	/**
	 * Verify Trade In Modal Headeris displayed
	 *
	 */
	public TradeInValuePopUpModal verifyTradeInModalHeader() {

		try {
			waitForSpinnerInvisibility();
			waitFor(ExpectedConditions.visibilityOf(tradeInModalHeader),60);
			Assert.assertTrue(isElementDisplayed(tradeInModalHeader), "Trade In modal header is not present");
			Reporter.log("Trade In Modal Header is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Trade In Modal Header");
		}
		return this;
	}

	/**
	 * Click on Good radio button
	 *
	 */
	public TradeInValuePopUpModal clickOnGoodRadioBtn() {
		try {
			checkPageIsReady();
			if(getDriver() instanceof IOSDriver){
				goodConditionRadioBtn.click();
			}else {
				clickElementWithJavaScript(goodConditionRadioBtn);
			}			
			Reporter.log("Clicked on Good Condition button.");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Good Condition button.");
		}
		return this;
	}

	/**
	 * Click on Get estimation button
	 *
	 */
	public TradeInValuePopUpModal clickOnGetEstimatedBtn() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(getEstimatedCTA),60);
			if(getDriver() instanceof IOSDriver){
				getEstimatedCTA.click();
			}else {
				clickElementWithJavaScript(getEstimatedCTA);
				
			}
			Reporter.log("Clicked on Get Estimated  button.");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Get Estimated  button.");
		}
		return this;
	}

	/**
	 * Click on trade In Modal page
	 *
	 */
	public TradeInValuePopUpModal clickOnTradeInModal() {
		try {
			waitForSpinnerInvisibility();
			moveToElement(whereCanIFindText);
			clickElementWithJavaScript(whereCanIFindText);
			Reporter.log("Clicked on Trade In Modal page.");
		} catch (Exception e) {
			Assert.fail("Failed to Click on Trade In Modal page.");
		}
		return this;
	}
}
