/**
 * 
 */
package com.tmobile.eservices.qa.data;

import com.tmobile.eqm.testfrwk.ui.core.annotations.Data;
import com.tmobile.eqm.testfrwk.ui.core.web.WebTest;

/**
 * @author pshiva
 *
 */
public class BillingPaymentInfo extends WebTest {

	@Data(name = "CreditCardNumber")
	private String creditCardNumber;

	@Data(name = "CreditCardExpMonth")
	private String creditCardExpMonth;

	@Data(name = "CreditCardExpYear")
	private String creditCardExpYear;

	@Data(name = "CvvNumber")
	private String cvvNumber;

	@Data(name = "BankRoutingNumber")
	private String bankRoutingNumber;

	@Data(name = "CheckAccoutNumber")
	private String checkAccoutNumber;

	/**
	 * @return the creditCardNumber
	 */
	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	/**
	 * @param creditCardNumber
	 *            the creditCardNumber to set
	 */
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	/**
	 * @return the creditCardExpMonth
	 */
	public String getCreditCardExpMonth() {
		return creditCardExpMonth;
	}

	/**
	 * @param creditCardExpMonth
	 *            the creditCardExpMonth to set
	 */
	public void setCreditCardExpMonth(String creditCardExpMonth) {
		this.creditCardExpMonth = creditCardExpMonth;
	}

	/**
	 * @return the creditCardExpYear
	 */
	public String getCreditCardExpYear() {
		return creditCardExpYear;
	}

	/**
	 * @param creditCardExpYear
	 *            the creditCardExpYear to set
	 */
	public void setCreditCardExpYear(String creditCardExpYear) {
		this.creditCardExpYear = creditCardExpYear;
	}

	/**
	 * @return the cvvNumber
	 */
	public String getCvvNumber() {
		return cvvNumber;
	}

	/**
	 * @param cvvNumber
	 *            the cvvNumber to set
	 */
	public void setCvvNumber(String cvvNumber) {
		this.cvvNumber = cvvNumber;
	}

	/**
	 * @return the bankRoutingNumber
	 */
	public String getBankRoutingNumber() {
		return bankRoutingNumber;
	}

	/**
	 * @param bankRoutingNumber
	 *            the bankRoutingNumber to set
	 */
	public void setBankRoutingNumber(String bankRoutingNumber) {
		this.bankRoutingNumber = bankRoutingNumber;
	}

	/**
	 * @return the checkAccoutNumber
	 */
	public String getCheckAccoutNumber() {
		return checkAccoutNumber;
	}

	/**
	 * @param checkAccoutNumber
	 *            the checkAccoutNumber to set
	 */
	public void setCheckAccoutNumber(String checkAccoutNumber) {
		this.checkAccoutNumber = checkAccoutNumber;
	}

	@Override
	public String toString() {
		return "BillingPaymentInfo [" + (creditCardNumber != null ? "creditCardNumber=" + creditCardNumber + ", " : "")
				+ (creditCardExpMonth != null ? "creditCardExpMonth=" + creditCardExpMonth + ", " : "")
				+ (creditCardExpYear != null ? "creditCardExpYear=" + creditCardExpYear + ", " : "")
				+ (cvvNumber != null ? "cvvNumber=" + cvvNumber + ", " : "")
				+ (bankRoutingNumber != null ? "bankRoutingNumber=" + bankRoutingNumber + ", " : "")
				+ (checkAccoutNumber != null ? "checkAccoutNumber=" + checkAccoutNumber : "") + "]";
	}
}
