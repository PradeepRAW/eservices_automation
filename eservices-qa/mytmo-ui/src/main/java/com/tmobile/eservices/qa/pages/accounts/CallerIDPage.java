package com.tmobile.eservices.qa.pages.accounts;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.data.MyTmoData;
import com.tmobile.eservices.qa.pages.CommonPage;

public class CallerIDPage extends CommonPage {

	// caller id texts
	

	@FindAll({@FindBy(css = "p.Display3.gray85.padding-bottom-small span"),
		@FindBy(css = "p.body.font-sf-pro.font-16.gray85 span"),
		@FindBy(css = "p.body.gray85.font-sf-pro.font-14.padding-bottom-medium span"),
		@FindBy(css = "p.legal.font-sf-pro.gray85"),
		@FindBy(css = "div.bg-gray05.min-height div form div:nth-child(2) div p"),
		@FindBy(css = " div.col-12.col-md-5.offset-md-1.col-lg-3.padding-top-xsmall-xs div.col-12.no-padding  p"),
		@FindBy(css = " div.col-12.col-md-5.col-lg-3.padding-top-large-xs.padding-top-large-sm div.col-12.no-padding p"),
		@FindBy(css = "div.bg-gray05.min-height div div:nth-child(1) div span"),
		@FindBy(css = "div.bg-gray05.min-height div div:nth-child(2) > div > span")})
	private WebElement CallerIdTextMsgs ;


/*	@FindBy(css = "p.Display3.gray85.padding-bottom-small span")
	public  WebElement callerIdNameTxt;
	
	@FindBy(css = "p.body.font-sf-pro.font-16.gray85 span")
	public  WebElement callerIdTxt2;
	
	@FindBy(css = "p.body.gray85.font-sf-pro.font-14.padding-bottom-medium span")
	public  WebElement callerIdTxtNote;
	
	@FindBy(css = "p.legal.font-sf-pro.gray85")
	public  WebElement callerIdTxtNote2;
	
	@FindBy(css = "div.bg-gray05.min-height div form div:nth-child(2) div p")
	public  WebElement callerIdSTDText;
		
	@FindBy(css = " div.col-12.col-md-5.offset-md-1.col-lg-3.padding-top-xsmall-xs div.col-12.no-padding  p")
	public  WebElement callerIdFNameErrorTxt;
	
	@FindBy(css = " div.col-12.col-md-5.col-lg-3.padding-top-large-xs.padding-top-large-sm div.col-12.no-padding p")
	public  WebElement callerIdLNameErrorTxt;
	
	@FindBy(css = "div.bg-gray05.min-height div div:nth-child(1) div span")
	public WebElement callerIdConfTxtMsg1;
	
	@FindBy(css = "div.bg-gray05.min-height div div:nth-child(2) > div > span")
	public WebElement callerIdConfTxtMsg2;*/
	
	@FindBy(css = "#firstName")
	private  WebElement callerIdFName;
	
	@FindBy(css = "#lastName")
	private  WebElement callerIdLName;
	
	
	@FindAll({@FindBy(css = "div.floatingLabel.no-padding label")})
	private List<WebElement> callerIdNameLables ;


	@FindBy(css = "button[aria-label='Agree and submit']")
	private WebElement btnAgreeNSubmit;

	@FindBy(css = "acn-caller-id-confirmation")
	private WebElement callerIdConfirmation;


	@FindBy(css = "acn-caller-id-confirmation  div div.bg-gray05.min-height div div:nth-child(2) div div span a")
	private WebElement callerIdNamePageMsgLink;
	
	
	
	


	public CallerIDPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Login To MyTmo
	 * 
	 * @param myTmoData
	 */

	public CallerIDPage enterCallerIdDetails(MyTmoData myTmoData) {
	String FName =	myTmoData.getcallerIdFName();
	String LName =	myTmoData.getcallerIdLName();
	try {	
		enterFName(FName);
		enterLName(LName);
	}catch (Exception e) {
		Assert.fail("Entering email or phone value was unsuccessful.");
	}
	return this;
}
	
	public CallerIDPage enterNAgreeCallerId(MyTmoData myTmoData) {
		String FName = myTmoData.getcallerIdFName();
		String LName = myTmoData.getcallerIdLName();
		try {
			enterFName(FName);
			enterLName(LName);
		} catch (Exception e) {
			Assert.fail("Entering email or phone value was unsuccessful.");
		}
		return this;
	}


	

	/**
	 * Re enter first name
	 */
	private CallerIDPage enterFName(String value) {
		try {
			sendTextData(callerIdFName, value);
		} catch (Exception e) {
			Assert.fail("Entering First NAme was unsuccessful.");
		}
		return this;
	}

	/**
	 * Re enter last name
	 */
	private CallerIDPage enterLName(String value) {
		try {
			sendTextData(callerIdFName, value);
		} catch (Exception e) {
			Assert.fail("Entering Last was unsuccessful.");
		}
		return this;
	}

	/**
	 * click on agree and submit
	 */
	private CallerIDPage clickOnAgreeNsubmit() {
		try {
			btnAgreeNSubmit.click();
		} catch (Exception e) {
			Assert.fail("failed click on Agree and submit button");
		}
		return this;
	}

	/**
	 * click on agree and submit
	 */
	public CallerIDPage verifyCallerIdNamePage() {
		try {
			callerIdConfirmation.isDisplayed();
		} catch (Exception e) {
			Assert.fail("failed Caller ID Name page confirmation");
		}
		return this;
	}

	/**
	 * Verify the authorable text.
	 */

	public CallerIDPage verifyCalledIdNameAuthorableLables(String str) {
		checkPageIsReady();
		for (int i = 0; i < callerIdNameLables.size() - 1; i++) {
			try {
				if (callerIdNameLables.get(i).getText().contains(str)) {
					Assert.assertTrue(callerIdNameLables.get(i).getText().equalsIgnoreCase(str),
							"Expected result: Authorable text should appear " + str);
				}
				Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
						+ ".Actual result: Authorable text appearing is " + callerIdNameLables.get(i).getText());
			} catch (Exception e) {
				Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
						+ str + ".Actual result: Authorable text appearing is " + callerIdNameLables.get(i).getText());
			}
		}
		return this;
	}

	/**
	 * Verify the authorable text.
	 */

	public CallerIDPage verifyCallerIdNamePageBackToLineSettings(String str) {
		checkPageIsReady();
		try {
			if (callerIdNamePageMsgLink.getText().contains(str)) {
				Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
						+ ".Actual result: Authorable text appearing is " + callerIdNamePageMsgLink.getText());
				callerIdNamePageMsgLink.click();
			}
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + callerIdNamePageMsgLink.getText());
		}
		return this;
	}
	
	/**
	 * Verify the authorable text.
	 */

	public CallerIDPage verifyAuthorabletext(String str) {

		try {
			Assert.assertTrue(CallerIdTextMsgs.getText().equalsIgnoreCase(str), "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + CallerIdTextMsgs.getText());
			Reporter.log(" Verified the authorable text " + "Expected result: Authorable text should appear " + str
					+ ".Actual result: Authorable text appearing is " + CallerIdTextMsgs.getText());
		} catch (Exception e) {
			Assert.fail("Unable to verify the authorable text " + "Expected result: Authorable text should appear "
					+ str + ".Actual result: Authorable text appearing is " + CallerIdTextMsgs.getText());
		}
		return null;

	}
	
	public CallerIDPage isAgreeNSubmitDisabled() {
		
		try {
			if(btnAgreeNSubmit.isEnabled())
				Assert.fail("Agree and submit button should not be enabled when first and last name is not entered");
		} catch (Exception e) {
			Reporter.log("Agree and submit button is not enabled when first and last name is not entered");
			
		}
		return this;
	}
}
