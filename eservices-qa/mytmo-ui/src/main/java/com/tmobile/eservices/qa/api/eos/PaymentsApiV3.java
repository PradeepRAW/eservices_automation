package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class PaymentsApiV3 extends ApiCommonLib {

	/***
	 * * parameters: - $ref: '#/parameters/oAuth' - $ref: '#/parameters/Accept' -
	 * $ref: '#/parameters/interactionid'
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response oneTimePayment_OTP(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildBillingAPIHeader_AccessToken(apiTestData, tokenMap);
		String resourceURL = "payments/v3/otp";
		Response response = invokeRestServiceCall(headers, eos_base_url, resourceURL, "", RestServiceCallType.POST);
		return response;
	}

	private Map<String, String> buildBillingAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		tokenMap.put("version", "v1");
		JWTTokenApi jwtToken = new JWTTokenApi();
		Map<String, String> jwtTokens = jwtToken.checkAndGetJWTToken(apiTestData, tokenMap);
		jwtToken.registerJWTTokenforAPIGEE("SAAS", jwtTokens.get("jwtToken"), tokenMap);
		jwtToken.registerJWTTokenforAPIGEE("ONPREM", jwtTokens.get("jwtToken"), tokenMap);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("userRole", "PAH");
		headers.put("ban", apiTestData.getBan());
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("Authorization", "Bearer " + jwtTokens.get("jwtToken"));
		headers.put("unBilledCycleStartDate", "2017-10-01");
		headers.put("easyPayStatus", "true");
		headers.put("access_token", jwtTokens.get("accessToken"));
		headers.put("X-B3-TraceId", "PaymentsAPITest123");
		headers.put("X-B3-SpanId", "PaymentsAPITest456");
		headers.put("transactionid", "PaymentsAPITest");
		return headers;
	}

	private Map<String, String> buildBillingAPIHeader_AccessToken(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", checkAndGetAccessToken());
		headers.put("Content-Type", "application/json");
		headers.put("ban", apiTestData.getBan());
		headers.put("cache-control", "no-cache");
		headers.put("channel", "WEB");
		headers.put("channel_id", "WEB");
		headers.put("msisdn", apiTestData.getMsisdn());
		headers.put("application_id", "Desktop");
		headers.put("X-B3-TraceId", "PaymentsAPITest123");
		headers.put("X-B3-SpanId", "PaymentsAPITest456");
		headers.put("transactionid", "PaymentsAPITest");
		return headers;
	}
}
