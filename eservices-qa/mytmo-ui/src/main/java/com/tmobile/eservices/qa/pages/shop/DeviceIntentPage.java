/**
 * 
 */
package com.tmobile.eservices.qa.pages.shop;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;

/**
 * @author charshavardhana
 *
 */
public class DeviceIntentPage extends CommonPage {
	
	public static final String DEVICE_INTENT_URL = "device-intent";

	@FindBy(css = "div[class*='p-t-64-sm']")
	private WebElement pageTitle;

	@FindBy(xpath = "//p[contains(text(), 'Buy a new phone')]")
	private WebElement buyANewPhone;
	
	@FindBy(xpath = "//p[contains(text(), 'Buy a new watch')]")
	private WebElement buyANewWatch;

	@FindBy(xpath = "//p[contains(text(), 'Buy a new phone')]/../../img")
	private WebElement buyANewPhoneImage;
	
	@FindBy(xpath = "//p[contains(text(), 'Buy a new watch')]/../../img")
	private WebElement buyANewWatchImg;
	
    @FindBy(xpath = "//p[contains(text(), 'Bring a phone')]")
	private WebElement bringMyPhone;

	@FindBy(xpath = "//p[contains(text(), 'Bring a phone')]/../../img")
	private WebElement bringMyPhoneImage;
	
    @FindBy(xpath = "//*[contains(text(), 'Use my own phone')]")
	private WebElement useMyOwnPhone;

	@FindBy(xpath = "//*[contains(text(), 'Use my own phone')]/../../img")
	private WebElement useMyOwnPhoneImage;

	@FindBy(xpath = "//*[contains(text(), 'Watches')]")
	private WebElement watchesTablestsAndMore;

	@FindBy(xpath = "//*[contains(text(), 'Watches')]/../../img")
	private WebElement watchesTablestsAndMoreImage;

	@FindBy(css = "[class*='legal p-t-4 ng-binding ng-scope']")
	private WebElement useMyOwnPhoneLegaltext;

	@FindBy(css = "span[ng-bind*='ctrl.contentData.tradeInWarningStickyHeaderTxt']")
	private WebElement wantToTradeInADeviceText;
	
	@FindBy(css = "span[ng-bind*='ctrl.contentData.tradeInWarningStickyHeaderTxt']")
	private List<WebElement> wantToTradeInADeviceTxt;

	@FindBy(css = "a[class*='tmo_tfn_number']")
	private WebElement callNumberHyperlink;

	@FindBy(css = "i[ng-click*='ctrl.visibleSticky']")
	private WebElement cancelIconInStickyBanner;
	
	@FindBy(css = "div.footer-pict-nav")
	private WebElement deviceIntentPageFooter;
	
	@FindBy(xpath = "//p[contains(text(),'Buy a new phone')]/following-sibling::p//span")
	private WebElement messageOnBuyNewPhoneTile;
	
	@FindBy(xpath = "//p[contains(text(),'Buy a new watch')]/following-sibling::p//span")
	private WebElement messageOnBuyNewWatchTile;
	
	@FindBy(xpath = "//p[contains(text(),'Bring a phone')]/following-sibling::p//span")
	private WebElement messageOnUseMyOwnPhoneTile;
	
	@FindBy(xpath = "//p[contains(text(),'Buy a new phone')]/parent::div/parent::div")
	private WebElement buyNewPhoneTile;
	
	@FindBy(xpath = "//p[contains(text(),'Bring a phone')]/parent::div/parent::div")
	private WebElement useMyOwnPhoneTile;
	
	@FindBy(xpath = "//p[contains(text(),'Buy a new watch')]/parent::div/parent::div")
	private WebElement buyNewWatchTile;
	
	@FindBy(xpath = "//div[contains(@class,'modal-content modal-background')]")
	private WebElement aalIneligibleModalWindow;
	
	@FindBy(xpath = "//p[contains(text(),'Buy a new phone')]/following-sibling::p//span//a[contains(@class,'hidden')]")
	private WebElement phoneNumberLinkOnBuyANewPhoneTile;
	
	@FindBy(xpath = "//p[contains(text(),'Buy a new watch')]/following-sibling::p//span//a[contains(@class,'hidden')]")
	private WebElement phoneNumberLinkOnBuyANewWatchTile;
	
	@FindBy(xpath = "//p[contains(text(),'Buy a new watch')]/following-sibling::p//span//a[contains(@class,'visible')]")
	private WebElement phoneNumberLinkOnBuyANewWatchTileMobile;
	
	@FindBy(xpath = "//p[contains(text(),'Buy a new phone')]/following-sibling::p//span//a[contains(@class,'visible')]")
	private WebElement phoneNumberLinkOnBuyANewPhoneTileMobile;
	
	
	//p[contains(text(),'Buy a new phone')]/following-sibling::p
	/**
	 * 
	 * @param webDriver
	 */
	public DeviceIntentPage(WebDriver webDriver) {
		super(webDriver);
	}

	public DeviceIntentPage verifyDeviceIntentPage() {
		try {
			checkPageIsReady();
			verifyPageUrl();
			checkPageIsReady();
			waitforSpinner();
			Reporter.log("Navigated to Device Intent Page");
		} catch (Exception e) {
			Assert.fail("Failed to navigate to Device Intent Page");
		}
		return this;
	}

	/**
	 * verify title
	 */
	public DeviceIntentPage verifyTitle() {
		try {
			Assert.assertTrue(pageTitle.getText().contains("Are you adding a new phone or using your own?"),
					"Title is not displayed as expected");
			Reporter.log("Title displayed as expected");
		} catch (Exception e) {
			Assert.fail("Failed to verify title");
		}
		return this;
	}
	/**
	 * verify tile Buy a New Phone is Displayed
	 */

	public DeviceIntentPage verifyBuyANewWatchTileIsDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(buyANewWatch.isDisplayed(), "Buy A New Watch Tile is not displayed");
			Reporter.log("Buy A New Watch Tile is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Buy A New Watch Tile");
		}
		return this;
	}
	/**
	 * verify tile Buy a New Phone is Displayed
	 */

	public DeviceIntentPage verifyBuyANewPhoneTileIsDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(buyANewPhone.isDisplayed(), "Buy A New Phone Tile is not displayed");
			Reporter.log("Buy A New Phone Tile is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Buy A New Phone Tile");
		}
		return this;
	}

	/**
	 * verify tile Use My Own Phone is Displayed
	 */

	public DeviceIntentPage verifyUseMyOwnPhoneTileIsDisplayed() {
		try {
			Assert.assertTrue(useMyOwnPhone.isDisplayed(), "Use My Own Phone Tile is not displayed");
			Reporter.log("Use My Own Phone Tile is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Use My Own Phone Tile");
		}
		return this;
	}

	/**
	 * verify tile Watches Tablets & More is Displayed
	 */

	public DeviceIntentPage verifyWatchesTabletsAndMoreTileIsDisplayed() {
		try {
			Assert.assertTrue(watchesTablestsAndMore.isDisplayed(), "Watches Tablets & More Tile is not displayed");
			Reporter.log("Watches Tablets & More Tile is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Watches Tablets & More Tile");
		}
		return this;
	}

	/**
	 * verify Legal Text Under Use my Own Phone is Displayed
	 */

	public DeviceIntentPage verifyLegalTextUnderUseMyOwnPhoneIsDisplayed() {		
		try {
			Assert.assertTrue(useMyOwnPhoneLegaltext.isDisplayed(),
					"Watches Tablets & More Tile Legal text is not displayed");
			Reporter.log("Watches Tablets & More Tile Legal text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Watches Tablets & More Tile  Legal text");
		}
		return this;
	}

	/**
	 * verify Buy A New Phone Tile Image is Displayed buyANewWatchImg
	 */

	public DeviceIntentPage verifyBuyANewPhoneTileImageDisplayed() {
		try {
			Assert.assertTrue(buyANewPhoneImage.isDisplayed(), "Buy a New Phone Image is not displayed");
			Reporter.log("Buy A New Phone Tile Image is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Display Buy a New Phone Image");
		}
		return this;
	}
	
	/**
	 * verify Buy A New Watch Tile Image is Displayed 
	 */

	public DeviceIntentPage verifyBuyANewWatchTileImageDisplayed() {
		try {
			Assert.assertTrue(buyANewWatchImg.isDisplayed(), "Buy a New watch Image is not displayed");
			Reporter.log("Buy A New watch Tile Image is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to Display Buy a New Watch Image");
		}
		return this;
	}

	/**
	 * verify Use My Own Phone Tile Image is Displayed
	 */

	public DeviceIntentPage verifyUseMyOwnPhoneTileImageDisplayed() {
		try {
			Assert.assertTrue(useMyOwnPhoneImage.isDisplayed(), "Use My Own Phone Tile Image is not displayed");
			Reporter.log("Use My Own Phone Tile Image is displayed");
		} catch (Exception e) {
			Assert.fail("failed to Display Use My Own Phone Tile Image");
		}
		return this;
	}

	/**
	 * verify Watches, Tablets & More Tile Image is Displayed
	 */

	public DeviceIntentPage verifyWatchesTabletsAndMoreTileImageDisplayed() {
		waitforSpinner();
		try {

			Assert.assertTrue(watchesTablestsAndMoreImage.isDisplayed(),
					"Watches, Tablets & More Tile Image is not displayed");
			Reporter.log("Watches, Tablets & More Tile Image is displayed");
		} catch (Exception e) {
			Assert.fail("failed to Display Watches, Tablets & More Tile Image");
		}
		return this;
	}

	/**
	 * verify Want to Trade In a device text
	 */
	public DeviceIntentPage verifyWantToTradeInADeviceText() {
		try {
			Assert.assertTrue(wantToTradeInADeviceText.isDisplayed(),
					"Want To Trade In A Device text is not displayed");
			Reporter.log("Want To Trade In A Device text is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Want To Trade In A Device text ");
		}
		return this;
	}

	/**
	 * Verify Hyper link 'Call 1-800-T-Mobile' in Sticky message banner
	 */
	public DeviceIntentPage verifyCallHyperLink() {
		try {
			Assert.assertTrue(callNumberHyperlink.isDisplayed(), "Call hyperlink is not displayed");
			Assert.assertTrue(callNumberHyperlink.getText().contains("Call 1-800-T-Mobile"),
					"Hyper link 'Call 1-800-T-Mobile' in Sticky message banner is not displayed");
			Reporter.log("Hyper link 'Call 1-800-T-Mobile' in Sticky message banner is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Hyper link 'Call 1-800-T-Mobile' in Sticky message banner");
		}
		return this;
	}

	/**
	 * Verify 'X' icon in Sticky message banner
	 */
	public DeviceIntentPage verifyCancelIconInStickyBanner() {
		try {
			Assert.assertTrue(cancelIconInStickyBanner.isDisplayed(), "X Icon is not displayed");
			Reporter.log("X Icon is displayed in Stiky message banner");
		} catch (Exception e) {
			Assert.fail("Failed to verify X icon in sticky banner");
		}
		return this;
	}

	public DeviceIntentPage clickBuyANewPhoneOption() {
		checkPageIsReady();
		waitforSpinner();
		try {
			clickElement(buyANewPhone);
			Reporter.log("Clicked on Buy a New Phone option");
		} catch (Exception e) {
			Assert.fail("Unable to click on Buy a New Phone option");
		}
		return this;
	}
	
	/**
	 * Click on Phones section
	 * @return
	 */
	public DeviceIntentPage clickOnBuyaNewPhone() {

		try {
			
			buyANewPhone.click();
			Reporter.log("Clicked on Buy a new phone option");
		} catch (Exception e) {
			Assert.fail("Unable to click on Buy a new phone option");
		}
		return this;
	}

	/**
	 * Click Use My Phone Option
	 * 
	 * @return
	 */
	public DeviceIntentPage clickUseMyPhoneOption() {
		try {
			checkPageIsReady();
			if (useMyOwnPhone.isDisplayed()) {
				useMyOwnPhone.click();
				Reporter.log("Clicked on Use My Own Phone option");
			} else {
				bringMyPhone.click();
				Reporter.log("Clicked on Use My Own Phone option");
			}
		} catch (Exception e) {
			Assert.fail("Failed to click on Use My Own Phone option");
		}
		return this;
	}
	
	/**
	 * Verify Device Intent page footer
	 */
	public DeviceIntentPage verifyDeviceIntentsPageFooterNotDisplayed() {
		try {
			Assert.assertFalse(isElementDisplayed(deviceIntentPageFooter), "Device Intent Page footer is displayed");
			Reporter.log("Device Intent Page footer is  not displayedd");
		} catch (Exception e) {
			Assert.fail("Failed not to display Device Intent Page footer");
		}
		return this;
	}
	
	/**
	 * Verify Device Intent Page URL
	 * 
	 * @return
	 */
	public DeviceIntentPage verifyPageUrl() {
		try{
			getDriver().getCurrentUrl().contains(DEVICE_INTENT_URL);
		}catch(Exception e){
			Assert.fail("Device intent page is not displayed");
		}
		return this;
	}
	
	
	
	/**
	 * verify Want to Trade In a device text
	 */
	public DeviceIntentPage verifyWantToTradeInADeviceTextNotDisplayed() {
		try {
			Assert.assertEquals(wantToTradeInADeviceTxt.size(), 0, "Want To Trade In A Device text is displayed");
			Reporter.log("Want To Trade In A Device text is not displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify Want To Trade In A Device text ");
		}
		return this;
	}
	
	/**
	 * Click Buy A Watch Option
	 * @return
	 */
	public DeviceIntentPage clickBuyAWatchOption() {
		try {
			checkPageIsReady();
			watchesTablestsAndMore.click();
			Reporter.log("Clicked on Buy A Watch Option");
		} catch (Exception e) {
			Assert.fail("Failed to click on Buy A Watch Option");
		}
		return this;
	}
	
	/**
	 * verify Buy a New Phone authorable text is Displayed
	 */

	public DeviceIntentPage verifyBuyANewPhoneTextIsDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(buyANewPhone.getText().contains("Buy a new phone"), "Buy A New Phone text is not displayed");
			Reporter.log("Buy A New Phone text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Buy A New Phone text");
		}
		return this;
	}

	/**
	 * verify Bring a phone authorable text is Displayed
	 */

	public DeviceIntentPage verifyUseMyOwnPhoneTextIsDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(useMyOwnPhone.getText().contains("Bring a phone"), "Bring a phone text is not displayed");
			Reporter.log("Bring a phone text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Bring a phone text");
		}
		return this;
	}
	
	/**
	 * verify Buy a new watch authorable text is Displayed
	 */

	public DeviceIntentPage verifyWatchesTabletsTextIsDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(watchesTablestsAndMore.getText().contains("Buy a new watch"), "Watches text is not displayed");
			Reporter.log("Watches text is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Watches text");
		}
		return this;
	}
	
	/**
	 * verify Max Lines Error Message is Displayed for Buy a New Phone
	 */

	public DeviceIntentPage verifyErrorMessageOnBuyNewPhoneTileDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(messageOnBuyNewPhoneTile.getText().contains("option isn't available online.  Please call 1-800-937-8997."), "Error message is not displayed for Buy a new Phone");
			Reporter.log("Error message is displayed for Buy a new Phone");
		} catch (Exception e) {
			Assert.fail("Failed to display Error message is displayed for Buy a new Phone");
		}
		return this;
	}
	
	/**
	 * verify Max Lines Error Message is Displayed for Use Your Own Phone
	 */

	public DeviceIntentPage verifyErrorMessageOnUseYourOwnPhoneTileDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(messageOnUseMyOwnPhoneTile.getText().contains("option isn't available online.  Please call 1-800-937-8997."), "Error message is not displayed for Bring a Phone");
			Reporter.log("Error message is displayed for Bring a Phone");
		} catch (Exception e) {
			Assert.fail("Failed to display Error message is displayed for Bring a Phone");
		}
		return this;
	}
	
	/**
	 * verify Max Soc Error Message is Displayed for Buy a New Watch
	 */

	public DeviceIntentPage verifyMaxSocErrorMessageForCallLink() {
		
		try {
			Assert.assertTrue(messageOnBuyNewWatchTile.getText().contains("1-800-937-8997"), "Max soc error message contains call link number");
			Reporter.log("Max Soc error message contains call link  for Buy a new Watch");
		} catch (Exception e) {
			Assert.fail("Failed to display Max Soc error message contains call link for Buy a new Watch");
		}
		return this;
	}
	
	/**
	 * verify Max soc Error Message is Displayed for Buy a New Watch
	 */

	public DeviceIntentPage verifyMaxSocErrorMessageOnBuyNewWatchTileDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(messageOnBuyNewWatchTile.getText().contains("Sorry, we're currently unable to process this order type online"), "Max soc error message is not displayed for Buy a new Watch");
			Reporter.log("Max soc error message is displayed for Buy a new Watch");
		} catch (Exception e) {
			Assert.fail("Failed to display Max soc error message is displayed for Buy a new Watch");
		}
		return this;
	}
	
	/**
	 * verify Buy a New Phone Tile is disabled
	 */

	public DeviceIntentPage verifyBuyNewPhoneTileDisabled() {
		checkPageIsReady();
		try {
			Assert.assertTrue(buyNewPhoneTile.getAttribute("class").contains("disable"), "Buy a new Phone tile is enabled");
			Reporter.log("Buy a new Phone tile is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Buy a new Phone' tile is disabled");
		}
		return this;
	}
	
	/**
	 * verify Use Your Own Phone Tile is disabled
	 */

	public DeviceIntentPage verifyUseYourOwnPhoneTileDisabled() {
		checkPageIsReady();
		try {
			Assert.assertTrue(useMyOwnPhoneTile.getAttribute("class").contains("disable"), "Use your own phone tile is enabled");
			Reporter.log("Use your own phone tile is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Use your own phone' tile is disabled");
		}
		return this;
	}
	
	/**
	 * verify Buy a New Watch Tile is disabled
	 */

	public DeviceIntentPage verifyBuyANewWatchTileDisabled() {
		checkPageIsReady();
		try {
			Assert.assertTrue(buyNewWatchTile.getAttribute("class").contains("disable"), "Buy a New watch tile is enabled");
			Reporter.log("Buy A New Watch tile is disabled");
		} catch (Exception e) {
			Assert.fail("Failed to verify 'Buy A New watch' tile is disabled");
		}
		return this;
	}
	
	/**
	 * verify Max Mbb Error Message is Displayed for Buy a New Watch
	 */

	public DeviceIntentPage verifyMaxMbbErrorMessageOnBuyNewWatchTileDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(messageOnBuyNewWatchTile.getText().contains("Sorry, you are unable to add a watch at this time due to the amount of lines on your account."), "Max Mbb error message is not displayed for Buy a new Watch");
			Reporter.log("Max Mbb error message is displayed for Buy a new Watch");
		} catch (Exception e) {
			Assert.fail("Failed to display Max Mbb error message is displayed for Buy a new Watch");
		}
		return this;
	}
	
	/**
	 * verify No GSM Error Message is Displayed for Buy a New Watch
	 */

	public DeviceIntentPage verifyNoGsmErrorMessageOnBuyNewWatchTileDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(messageOnBuyNewWatchTile.getText().contains("Sorry, you are unable to add a watch at this time due to having no voice lines on your account."), "No GSM error message is not displayed for Buy a new Watch");
			Reporter.log("No GSM error message is displayed for Buy a new Watch");
		} catch (Exception e) {
			Assert.fail("Failed to display No GSM error message is displayed for Buy a new Watch");
		}
		return this;
	}

	/**
	 * verify MBB Error Message is Displayed for Buy a New Watch
	 */

	public DeviceIntentPage verifyMBBErrorMessageOnBuyNewWatchTileDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(messageOnBuyNewWatchTile.getText().contains("Sorry, you've reached the maximum number of mobile internet lines"), "Max MBB error message is not displayed for Buy a new Watch");
			Reporter.log("Max MBB error message is displayed for Buy a new Watch");
		} catch (Exception e) {
			Assert.fail("Failed to display Max MBB error message is displayed for Buy a new Watch");
		}
		return this;
	}
	
	/**
	 * verify Max Lines Error Message is Displayed for Buy a New Phone
	 */

	public DeviceIntentPage verifyMaxVoiceLinesErrorMessageOnBuyNewPhoneTileDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(messageOnBuyNewPhoneTile.getText().contains("Sorry, you've reached the maximum number of phone lines"), "Error message is not displayed for Buy a new Phone");
			Reporter.log("Max Voice Lines Error message is displayed for Buy a new Phone");
		} catch (Exception e) {
			Assert.fail("Failed to verify Max Voice Lines Error message is displayed for Buy a new Phone");
		}
		return this;
	}
	
	/**
	 * verify Max Lines Error Message is Displayed for Use Your Own Phone
	 */

	public DeviceIntentPage verifyMaxVoiceLinesErrorMessageOnUseYourOwnPhoneTileDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertTrue(messageOnUseMyOwnPhoneTile.getText().contains("Sorry, you've reached the maximum number of phone lines"), "Error message is not displayed for Bring a Phone");
			Reporter.log("Max Voice Lines Error message is displayed for Bring a Phone");
		} catch (Exception e) {
			Assert.fail("Failed to display Max Voice Lines Error message is displayed for Bring a Phone");
		}
		return this;
	}
	
	
	/**
	 * 
	 * Verify AAL ineligible modal window
	 * 
	 * @return
	 */
	public DeviceIntentPage verifyAALIneligibleModalWindow() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(aalIneligibleModalWindow));
			aalIneligibleModalWindow.isDisplayed();
			Reporter.log("AALIneligibleModalWindow is displayed");
		} catch (Exception e) {
			Assert.fail("AALIneligibleModalWindow is not displayed");
		}
		return this;
	}
	
	/**
	 * Click Buy A Watch Option
	 * @return
	 */
	public DeviceIntentPage clickBuyAWatch() {
		try {
			checkPageIsReady();
			clickElementWithJavaScript(buyANewWatch);
			Reporter.log("Clicked on Buy A Watch Option");
		} catch (Exception e) {
			Assert.fail("Failed to click on Buy A Watch Option");
		}
		return this;
	}
	
	/**
	 * Click Phone number link on Buy a new New Phone Tile
	 * @return
	 */
	public DeviceIntentPage clickOnPhoneNumberLinkOnBuyANewPhoneTile() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				clickElementWithJavaScript(phoneNumberLinkOnBuyANewPhoneTileMobile);
			}else {
				clickElementWithJavaScript(phoneNumberLinkOnBuyANewPhoneTile);
			}
			Reporter.log("Clicked on phone number link on Buy a New Phone Tile");
		} catch (Exception e) {
			Assert.fail("Failed to phone number link on Buy a New Phone Tile");
		}
		return this;
	}
	
	/**
	 * verify tile Bring My Phone is Displayed
	 */

	public DeviceIntentPage verifyBringMyPhoneTileIsDisplayed() {
		try {
			Assert.assertTrue(bringMyPhone.isDisplayed(), "Bring My Phone Tile is not displayed");	
			Reporter.log("Bring My Phone Tile is displayed");
		} catch (Exception e) {
			Assert.fail("Failed to display Bring My Phone Tile");
		}
		return this;
	}
	
	/**
	 * verify Bring My Phone Tile Image is Displayed
	 */

	public DeviceIntentPage verifyBringMyPhoneTileImageDisplayed() {
		try {
				Assert.assertTrue(bringMyPhoneImage.isDisplayed(), "Bring My Phone Tile Image is not displayed");
			Reporter.log("Bring My Phone Tile Image is displayed");
		} catch (Exception e) {
			Assert.fail("failed to Display Use Bring My Phone Tile Image");
		}
		return this;
	}
	
	/**
	 * Click Phone number link on Buy a new New Watch Tile
	 * @return
	 */
	public DeviceIntentPage clickOnPhoneNumberLinkOnBuyANewWatchTile() {
		try {
			checkPageIsReady();
			if (getDriver() instanceof AppiumDriver) {
				
				clickElementWithJavaScript(phoneNumberLinkOnBuyANewWatchTileMobile);
			}else {
				
				clickElementWithJavaScript(phoneNumberLinkOnBuyANewWatchTile);
			}
			
			Reporter.log("Clicked on phone number link on Buy a New Watch Tile");
		} catch (Exception e) {
			Assert.fail("Failed to phone number link on Buy a New Watch Tile");
		}
		return this;
	}
	
	/**
	 * verify Phone number link on Buy a New Watch Tile is Displayed
	 */

	public DeviceIntentPage verifyOnPhoneNumberLinkOnBuyANewWatchTile() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				String phoneNumberLink = phoneNumberLinkOnBuyANewWatchTileMobile.getText();
				Assert.assertTrue(phoneNumberLinkOnBuyANewWatchTileMobile.isDisplayed(), "Phone number link on buy a new Watch is not displayed");
				Assert.assertTrue(phoneNumberLink.matches("\\d{1}-\\d{3}-\\d{3}-\\d{4}"), "Phone number link on buy a new Watch is not displayed");
				
			}else {
				String phoneNumberLink = phoneNumberLinkOnBuyANewWatchTile.getText();
				Assert.assertTrue(phoneNumberLinkOnBuyANewWatchTile.isDisplayed(), "Phone number link on buy a new Watch is not displayed");
				Assert.assertTrue(phoneNumberLink.matches("\\d{1}-\\d{3}-\\d{3}-\\d{4}"), "Phone number link on buy a new Watch is not displayed");
			}
			Reporter.log("Phone number link on Buy a New Watch Tile is displayed");
		} catch (Exception e) {
			Assert.fail("failed to Display Phone number link on Buy a New Watch Tile");
		}
		return this;
	}
	
	/**
	 * verify Phone number link on Buy a New Phone Tile is Displayed
	 */

	public DeviceIntentPage verifyOnPhoneNumberLinkOnBuyANewPhoneTile() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				String phoneNumberLink = phoneNumberLinkOnBuyANewPhoneTileMobile.getText();
				Assert.assertTrue(phoneNumberLinkOnBuyANewPhoneTileMobile.isDisplayed(), "Phone number link on buy a new Phone is not displayed");
				Assert.assertTrue(phoneNumberLink.matches("\\d{1}-\\d{3}-\\d{3}-\\d{4}"), "Phone number link on buy a new Phone is not displayed");
			
			}else {
				String phoneNumberLink = phoneNumberLinkOnBuyANewPhoneTile.getText();
				Assert.assertTrue(phoneNumberLinkOnBuyANewPhoneTile.isDisplayed(), "Phone number link on buy a new Phone is not displayed");
				Assert.assertTrue(phoneNumberLink.matches("\\d{1}-\\d{3}-\\d{3}-\\d{4}"), "Phone number link on buy a new Phone is not displayed");
			}
				
			Reporter.log("Phone number link on Buy a New Phone Tile is displayed");
		} catch (Exception e) {
			Assert.fail("failed to Display Phone number link on Buy a New Phone Tile");
		}
		return this;
	}
	/**
	 * verify  Error Message should not Displayed for Buy a New Watch
	 */
	public DeviceIntentPage verifyErrorMessageOnBuyNewWatchTileNotDisplayed() {
		checkPageIsReady();
		try {
			Assert.assertFalse(messageOnBuyNewWatchTile.getText().contains("Sorry, you've reached the maximum number of mobile internet lines"), "Max MBB error message is not displayed for Buy a new Watch");
			Reporter.log("Error message for new watch tile is not Displayed");
		} catch (Exception e) {
			Assert.fail("Failed to verify message for new watch tile");
		}
		return this;
	}
	}
