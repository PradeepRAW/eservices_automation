package com.tmobile.eservices.qa.pages.tmng.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.response.Response;

public class AddressCredirProfileID extends ApiCommonLib {

	/***
	 * This is the API specification for the EOS AddressValidation . The EOS API
	 * will inturn invoke DCP apis to execute the quote functions. parameters: -
	 * $ref: '#/parameters/oAuth' - $ref: '#/parameters/transactionId' - $ref:
	 * '#/parameters/correlationId' - $ref: '#/parameters/applicationId' - $ref:
	 * '#/parameters/channelId' - $ref: '#/parameters/clientId' - $ref:
	 * '#/parameters/transactionBusinessKey' - $ref:
	 * '#/parameters/transactionBusinessKeyType' - $ref:
	 * '#/parameters/transactionType'
	 * 
	 * @return
	 * @throws Exception
	 */

	public Response getCreditProfileIdForCreditCheck(ApiTestData apiTestData, String requestBody,
			Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildCreditProfileIdForCreditCheckAPIHeader(apiTestData, tokenMap);
		String resourceURL = "commerce/v3/profiles/" + tokenMap.get("profileId") + "/credit-profiles";
		Response response = invokeRestServiceCall(headers, "https://tmobileqat-qat03-pro.apigee.net", resourceURL,
				requestBody, RestServiceCallType.POST);
		System.out.println(response.asString());
		return response;
	}

	private Map<String, String> buildCreditProfileIdForCreditCheckAPIHeader(ApiTestData apiTestData,
			Map<String, String> tokenMap) throws Exception {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("accept", "application/json");
		headers.put("Authorization", "Bearer " + checkAndGetTMNGUNOToken());
		headers.put("correlationId", "prescreen_999");
		headers.put("ApplicationId", "TMO");
		headers.put("channelId", "WEB");
		headers.put("interactionid", "xasdf1234assadia");
		// headers.put("transactionType","ACTIVATION");
		headers.put("Cache-Control", "no-cache");
		headers.put("Content-type", "application/json");
		headers.put("usn", "555555");
		headers.put("follow", "true");
		headers.put("X-Auth-Originator", checkAndGetTMNGUNOToken());
		return headers;
	}

}
