package com.tmobile.eservices.qa.pages.payments.api;
import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;



public class OrchestrateOrdersCollectionV2 extends ApiCommonLib {
	
	/***
	 * Summary: This API is used do the payments  through OrchestrateOrders API
	 * Headers:Authorization,
	 * 	-Content-Type,
	 * 	-Accept,
	 * 	-interactionId,
	 * 	-isV3OTPSedonaEnabled(true or false)
	 * 	-isV3ServicesEnabled (true or false)
	 * 	-workflowId
	 * 	-channel,
	 * 	
	 * @return
	 * @throws Exception
	 * 
	 * 
	 */
	
	private Map<String, String> buildorchestrateordersCollectionHeader(ApiTestData apiTestData) throws Exception {
		//clearCounters(apiTestData);
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type","application/json");
		headers.put("Accept","application/json");
		headers.put("interactionId","543219876");
		headers.put("Authorization", checkAndGetAccessToken());
		headers.put("isV3OTPSedonaEnabled", "true");
		headers.put("isV3ServicesEnabled", "true");
		headers.put("workflowId", "BILLPAY");
		headers.put("channel", "App_EOS");
		headers.put("X-B3-TraceId", "PaymentsAPITest123");
		headers.put("X-B3-SpanId", "PaymentsAPITest456");
		headers.put("transactionid","PaymentsAPITest");
		
	
		return headers;
	}
	
	public Response orchestrateorders_sedona(ApiTestData apiTestData,String requestBody,Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildorchestrateordersCollectionHeader(apiTestData);
		headers.put("Authorization", getSmallAccessToken());
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v2/orchestrateorders", RestCallType.POST);
		System.out.println(response.asString());
		return response;
	}
	
	
	
}
