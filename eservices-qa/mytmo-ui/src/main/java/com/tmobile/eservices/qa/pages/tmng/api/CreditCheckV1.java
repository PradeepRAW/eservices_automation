package com.tmobile.eservices.qa.pages.tmng.api;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class CreditCheckV1 extends ApiCommonLib {

	/***
	 * This is the API specification for the EOS Quote . The EOS API will inturn
	 * invoke DCP apis to execute the quote functions. parameters: - $ref:
	 * '#/parameters/oAuth' - $ref: '#/parameters/transactionId' - $ref:
	 * '#/parameters/correlationId' - $ref: '#/parameters/applicationId' - $ref:
	 * '#/parameters/channelId' - $ref: '#/parameters/clientId' - $ref:
	 * '#/parameters/transactionBusinessKey' - $ref:
	 * '#/parameters/transactionBusinessKeyType' - $ref:
	 * '#/parameters/transactionType'
	 * 
	 * @return
	 * @throws Exception
	 */

	public Response createCreditCheck(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildCreditCheckAPIHeader(apiTestData, tokenMap);
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("customer-credit/v1/check", RestCallType.POST);
		// System.out.println(response.asString());
		return response;
	}

	public Response createCreditRefresh(ApiTestData apiTestData, Map<String, String> tokenMap) throws Exception {
		Map<String, String> headers = buildCreditCheckAPIHeader(apiTestData, tokenMap);
		RestService service = new RestService("", eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("customer-credit/v1/refresh?creditReferenceNumber="
				+ tokenMap.get("referenceNumber") + "&creditCheckType=" + apiTestData.getHardCreditCheckName(),
				RestCallType.GET);
		// System.out.println(response.asString());
		return response;
	}

	public Response createHardCreditCheck(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildCreditCheckAPIHeader(apiTestData, tokenMap);
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("credit-screen/v1/identity/screen", RestCallType.POST);
		// System.out.println(response.asString());
		return response;
	}

	private Map<String, String> buildCreditCheckAPIHeader(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		headers.put("Authorization", getJWTDecodeToken(apiTestData, tokenMap));
		headers.put("correlationId", "prescreen_99");
		headers.put("ApplicationId", apiTestData.getApplicationForCreditCheck());
		headers.put("channelId", apiTestData.getChannelForCreditCheck());
		headers.put("clientId", "TMNG");
		headers.put("transactionId", "xasdf1234as");
		headers.put("transactionType", "Credit_Check");
		headers.put("Cache-Control", "no-cache");
		headers.put("Content-type", "application/json");
		headers.put("usn", "check_32");
		return headers;
	}

	/*
	 * private Map<String, String> buildHardCreditCheckAPIHeader(ApiTestData
	 * apiTestData,Map<String, String> tokenMap) throws Exception {
	 * 
	 * Map<String, String> headers = new HashMap<String, String>();
	 * headers.put("Accept", "application/json"); headers.put("Authorization",
	 * getJWTDecodeToken(apiTestData,tokenMap));
	 * headers.put("correlationId","prescreen_99");
	 * headers.put("ApplicationId","TMO"); headers.put("channelId","web");
	 * headers.put("clientId", "12345"); headers.put("transactionId",
	 * "xasdf1234as"); headers.put("transactionType","Credit_Check");
	 * headers.put("Cache-Control", "no-cache"); headers.put("Content-type",
	 * "application/json"); headers.put("usn", "wrfer"); return headers; }
	 */

	public Response createHardCreditCheckNew(ApiTestData apiTestData, String requestBody, Map<String, String> tokenMap)
			throws Exception {
		Map<String, String> headers = buildCreditCheckAPIHeaderNew(apiTestData, tokenMap);
		RestService service = new RestService(requestBody, eos_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("credit-screen/v1/identity/screen", RestCallType.POST);
		// System.out.println(response.asString());
		return response;
	}

	private Map<String, String> buildCreditCheckAPIHeaderNew(ApiTestData apiTestData, Map<String, String> tokenMap)
			throws Exception {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Accept", "application/json");
		headers.put("Authorization", checkAndGetTMNGUNOToken());
		headers.put("correlationId", "prescreen_9999");
		headers.put("ApplicationId", "TMO");
		headers.put("channelId", "WEB");
		headers.put("clientId", "TMNG");
		headers.put("transactionId", "xasdf1234as");
		headers.put("transactionType", "ACTIVATION");
		headers.put("Cache-Control", "no-cache");
		headers.put("Content-type", "application/json");
		headers.put("usn", "check_3256");

		return headers;
	}

}
