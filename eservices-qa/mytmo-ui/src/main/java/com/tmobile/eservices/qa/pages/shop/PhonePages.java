package com.tmobile.eservices.qa.pages.shop;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;

/**
 * @author ksrinivas
 *
 */
public class PhonePages extends CommonPage {

	@FindBy(id = "manageSharingTextId")
	private WebElement manageSharingLink;

	@FindBy(css = "#changeSIMLink > div > div > a")
	private WebElement changeSim;

	@FindBy(id = "MP_device_image")
	private WebElement deviceImage;

	@FindBy(linkText = "Report lost or stolen")
	private WebElement reportLostOrStolenLink;

	@FindBy(css = "#deviceLockStatusLink a[href='#device-unlock']")
	private WebElement devicesLockstatus;

	@FindBy(linkText = "Check device unlock status")
	private WebElement devicesLockstatusIos;

	@FindBy(css = "#assurant_claim_url")
	private WebElement assurantClaim;

	@FindBy(id = "MP_device_title")
	private WebElement deviceTitle;

	@FindBy(id = "addVoiceMail")
	private WebElement checkVoiceMail;

	@FindBy(id = "di_lineselector")
	private WebElement lineSelector;

	@FindBy(css = "#linePickerList .subscriberName")
	private List<WebElement> lineName;

	@FindBy(css = ".support_all a")
	private List<WebElement> supportLinks;

	@FindBy(css = "div.co_check-rebate a")
	private WebElement checkOnRebateNowMobile;

	@FindBy(css = "div#tempSuspendLink a")
	private WebElement suspendLink;

	@FindBy(id = "stolenReport")
	private WebElement restoreServiceLink;

	@FindBy(css="navigate-review[disabled]")
	private WebElement btnNextDisabled;

	@FindBy(id = "navigate-review")
	private WebElement btnNext;

	@FindBy(css = "#agreeTnC input#checkbox")
	private WebElement agreeBox;

	@FindBy(id = "navigate-confirm")
	private WebElement reviewSubmit;

	@FindBy(linkText = "Restore service")
	private WebElement restoreService;

	@FindBy(id = "restoreline")
	private WebElement restoreSubmit;

	@FindBy(id = "success")
	private WebElement restoreSuccessText;

	@FindBy(css = "#unblockconfmodalother a#ok")
	private WebElement restoreOk;

	@FindBy(css = "#restoreconfmodal a#ok")
	private WebElement restoreSuspendOk;

	@FindBy(id = "nextLocateDevice")
	private WebElement btnLocateDevice;

	@FindBy(id = "nextReportDevice")
	private WebElement btnReportDevice;

	@FindBy(id = "suspendY")
	private WebElement radiosuspendYes;

	@FindBy(id = "nextSuspendDevice")
	private WebElement btnSuspendDevice;

	@FindBy(id = "confirmRLS")
	private WebElement btnStolenSubmit;

	@FindBy(id = "acceptedTnC")
	private WebElement chkTermsCond;

	@FindBy(id = "nextTnC")
	private WebElement btnTermsNext;

	@FindBy(css = "#linePickerList [id='1']")
	private WebElement secondItemlineSelectordropDown;

	@FindBy(id = "myphonedetailsheadertext")
	private WebElement phonesPage;

	@FindBy(id = "php_claim_url")
	private WebElement assuranctClaim;

	@FindAll({ @FindBy(id = "idContinueButton"), @FindBy(id = "idcontinuebtn") })
	private WebElement continueButton;

	@FindBy(id = "go-to-myphone")
	private WebElement goBacktoPhonepoage;

	@FindBy(id = "foundDeviceLink")
	private WebElement foundDevicelink;

	@FindBy(id = "unblockrestoreDevice")
	private WebElement unblockRestoredevice;

	@FindBy(css = "input[value='LT']")
	private WebElement radioLostDevice;

	@FindAll({ @FindBy(css = "#tradeinNow_id > a"), @FindBy(css = "#tradeInNow > a") })
	private WebElement tradeInnow;

	@FindAll({ @FindBy(css = "#tradeinStatus_id > a"), @FindBy(css = "#tradeInStatus > a") })
	private WebElement tradeInstatus;

	@FindBy(css = "#tradeinStatus_id .ui_primary_link")
	private WebElement tradeInstatusForMobile;

	@FindAll({ @FindBy(css = "div#checkOrderStatus a"), @FindBy(css = "a#checkOrderStatus") })
	private WebElement checkOrderstatus;

	@FindBy(css = "Check order status")
	private WebElement orderStatus;

	// #completedSuspendDevice:not([style='display:none;'])
	@FindAll({ @FindBy(css = ".start_dt input"), @FindBy(id = "startDate") })
	private WebElement startDate;

	@FindAll({ @FindBy(id = "endDate"), @FindBy(css = ".end_dt input") })
	private WebElement endDate;

	@FindAll({ @FindBy(css = "#changeLines > img"), @FindBy(css = "#changedropdown  > img") })
	private WebElement lineSelectordropDown;

	@FindBy(css = "#unblockconfmodalother #ok")
	private WebElement modalOkButton;

	@FindBy(id = "notReqTnC")
	private WebElement notReqTerms;

	@FindBy(css = "div.co_updrade-device.container-curve div.ui_subhead.mb15")
	private WebElement upgradeYourDevice;

	@FindBy(id = "jumplogo")
	private WebElement jumpLogo;

	@FindBy(id = "jumpCusttxt")
	private WebElement jumpCustomText;

	@FindBy(id = "jumpEligibleText")
	private WebElement jumpEligibleText;

	@FindBy(id = "sgmntmsgcontr")
	private WebElement segmentedMessage;

	@FindBy(linkText = "Learn More")
	private WebElement learnMoreLink;

	@FindBy(id = "segmentedMessageModal")
	private WebElement segmentedMessageContent;

	@FindBy(id = "shop_now")
	private WebElement shopNow;

	@FindBy(linkText = "Check on rebate now")
	private WebElement checkOnRebate;

	@FindBy(linkText = "Upgrade now!")
	private WebElement upgradeNowLink;

	@FindBy(css = "a[id='1'] *.subscriberMsisdn")
	private WebElement secondLineDeviceName;

	@FindBy(css = "span#di_deviceLockStatus")
	private WebElement deviceUnlockStatus;

	@FindBy(css = "#MP_msisdn")
	private WebElement selectedMSISDNText;

	/**
	 * @param webDriver
	 */
	public PhonePages(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Phones Page
	 * 
	 * @return
	 */
	public PhonePages verifyPhonesPages() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyDuplicatedElements(this.getClass());
			phonesPage.isDisplayed();
			Reporter.log("Navigated to Phone Page");
		} catch (Exception e) {
			Assert.fail("Phones Page is Not Displayed");
		}
		return this;

	}

	/**
	 * Click Go back to Phone page link
	 * 
	 * @return
	 */
	public PhonePages clickGobackTophonePage() {
		try {
			goBacktoPhonepoage.click();
		} catch (Exception e) {
			Assert.fail("Back button is not displayed to click");
		}
		return this;
	}

	/**
	 * Click Found device link
	 */
	public PhonePages clickFounddeviceLink() {
		try {
			foundDevicelink.click();
		} catch (Exception e) {
			Assert.fail("Device found Link not displayed to click");
		}
		return this;
	}

	/**
	 * Click Restore the device
	 */
	public PhonePages clickUnblockRestoredevice() {
		try {
			waitFor(ExpectedConditions.visibilityOf(unblockRestoredevice));
			unblockRestoredevice.click();
		} catch (Exception e) {
			Assert.fail("Unblock Restore Device is not displayed to click");
		}
		return this;

	}

	/**
	 * Verify Phones Page
	 * 
	 * @return
	 */
	public PhonePages verifyPhonesPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(phonesPage));
			phonesPage.isDisplayed();
			Reporter.log("Phone page is displayed");
		} catch (Exception e) {
			Reporter.log("Phone page not displayed");
			Assert.fail("Phone page not displayed");
		}
		return this;
	}

	/**
	 * To Click Assurant Claim Button
	 */
	public PhonePages clickAssurantclaim() {
		try {
			assuranctClaim.click();
		} catch (Exception e) {
			Assert.fail("Assurant Claim link is not Displayed to click");
		}
		return this;
	}

	/**
	 * To click continue Button
	 */
	public PhonePages clickContinuebtn() {
		try {
			continueButton.click();
		} catch (Exception e) {
			Assert.fail("Continue Button is not Displayed to Click");
		}
		return this;

	}

	/**
	 * Click Line selector Drop down
	 */
	public PhonePages clickLineselectorDropdown() {
		try {
			checkPageIsReady();
			lineSelectordropDown.click();
			Reporter.log("Line selector dropdown displayed");
		} catch (Exception e) {
			Assert.fail("Line selector dropdown is not displayed to click");
		}
		return this;
	}

	/**
	 * Click Second item from the Line selector Drop down
	 */
	public PhonePages clickSecondItemlineSelectordropDown() {
		try {
			checkPageIsReady();
			secondItemlineSelectordropDown.click();
		} catch (Exception e) {
			Assert.fail("Second Item Line Dropdown is not Displayed to Click");

		}
		return this;
	}

	/**
	 * Click Change sim Link
	 */
	public PhonePages clickChangesim() {
		try {
			changeSim.click();
		} catch (Exception e) {
			Assert.fail("Change Sim is not available to click");

		}
		return this;
	}

	/**
	 * Verify Device Image displayed or not
	 * 
	 * @return
	 */
	public PhonePages isDeviceimageDisplayed() {
		try {
			deviceImage.isDisplayed();
			Reporter.log("Device Image Displayed");
		} catch (Exception e) {
			Assert.fail("Device Image is Not Displayed");
		}
		return this;
	}

	/**
	 * Verify Stolen Report button is displayed or not
	 * 
	 * @return
	 */
	public PhonePages isStolenreportbtnDisplayed() {
		try {
			reportLostOrStolenLink.isDisplayed();
			Reporter.log(
					"Verify Changes Sim button,Stolenreportbutton,DeviceLockstatus button and Temporary Suspension button");
		} catch (Exception e) {
			Assert.fail(
					"Verify Changes Sim button,Stolenreportbutton,DeviceLockstatus button and Temporary Suspension button is not Displayed");
		}
		return this;
	}

	/**
	 * Verify Device Lock status button is displayed or not
	 * 
	 * @return
	 */
	public PhonePages isDeivcelockStatusDisplayed() {
		try {
			devicesLockstatus.isDisplayed();
			Reporter.log("Device Lock Status is Dispalyed");
		} catch (Exception e) {
			Assert.fail("Device Lock Status is Not Dispalyed");
		}
		return this;
	}

	/**
	 * Click Lost Or Stolen Link
	 * 
	 * @return
	 */
	public PhonePages clickLostOrStolenLink() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("device_waitCursor")));
		try {
			if (foundDevicelink.isDisplayed()) {
				clickFounddeviceLink();
				clickUnblockRestoredevice();
				waitFor(ExpectedConditions.invisibilityOfElementLocated(By.id("device_waitCursor")));
				restoreOk.click();
			}
			reportLostOrStolenLink.click();
		} catch (Exception e) {
			Assert.fail("Report lost or stolen link is not displayed");
		}
		return this;
	}

	/**
	 * @return none
	 */
	public PhonePages clickTradeInStatusLink() {
		try {
			if (getDriver() instanceof AppiumDriver) {
				waitFor(ExpectedConditions.visibilityOf(tradeInstatusForMobile));
				tradeInstatusForMobile.click();
			} else {
				waitFor(ExpectedConditions.visibilityOf(tradeInstatus));
				tradeInstatus.click();
			}
		} catch (Exception e) {
			Assert.fail("Trade in status link not Displayed");
		}
		return this;
	}

	/**
	 * Click Suspend Link
	 */
	public PhonePages clickSuspendLink() {
		try {
			suspendLink.click();
			Reporter.log("Suspend link is clickable");
		} catch (Exception e) {
			Assert.fail("Suspend link is not clickable");
		}
		return this;
	}

	/**
	 * Click Start Date
	 */
	public PhonePages clickStartDate() {
		try {
			startDate.click();
			Reporter.log("Start Date is available to click");
		} catch (Exception e) {
			Assert.fail("Start Date is not Available to click");
		}
		return this;
	}

	/**
	 * Click Next button
	 */
	public PhonePages clickBtnNext() {
		try {
			getDriver().manage().timeouts().setScriptTimeout(2, TimeUnit.SECONDS);
			waitFor(ExpectedConditions.invisibilityOf(btnNextDisabled));
			btnNext.click();
			Reporter.log("Next button is clickable");
		} catch (Exception e) {
			Assert.fail("Next button is not clickable");
		}
		return this;
	}

	/**
	 * Click Agree Box
	 * 
	 * @return
	 */
	public PhonePages clickAgreeBox() {

		try {
			agreeBox.click();
			Reporter.log("Agree box is clickable");
		} catch (Exception e) {
			Assert.fail("Agree box not Available to click");
		}
		return this;
	}

	/**
	 * Click ReviewSubmit Button
	 */
	public PhonePages clickReviewSubmit() {
		try {
			reviewSubmit.click();
			Reporter.log("Review submit button is clickable");
		} catch (Exception e) {
			Assert.fail("Review submit button is not clickable");
		}
		return this;
	}

	/**
	 * CLick Stolen Link
	 */
	public PhonePages clickStolenLink() {
		try {
			restoreServiceLink.click();
			Reporter.log("Stolen link is clickable");
		} catch (Exception e) {
			Assert.fail("Stolen link is not clickable");
		}
		return this;
	}

	/**
	 * Click Location Device
	 */
	public PhonePages clickLocateDevice() {
		try {
			btnLocateDevice.click();
			Reporter.log("Location device is clickable");
		} catch (Exception e) {
			Assert.fail("Location device is not clickable");
		}
		return this;
	}

	/**
	 * Click the Report Device
	 */
	public PhonePages clickReportDevice() {
		try {
			btnReportDevice.click();
			Reporter.log("Report device button is clickable");
		} catch (Exception e) {
			Assert.fail("Report device button is not clickable");
		}
		return this;
	}

	/**
	 * Click Suspend Button Yes
	 */
	public PhonePages clickRadioSuspendYes() {
		try {
			if (radiosuspendYes.isDisplayed()) {
				radiosuspendYes.click();
			}
			Reporter.log("Suspend Yes button is clickable");
		} catch (Exception e) {
			Assert.fail("Suspend Yes button is not clickable");
		}
		return this;
	}

	/**
	 * To Click Suspend Device
	 */
	public PhonePages clickSuspendService() {
		try {
			btnSuspendDevice.click();
			Reporter.log("Suspend device service button is clickable");
		} catch (Exception e) {
			Assert.fail("Suspend device service button is not clickable");
		}
		return this;
	}

	/**
	 * Click Stolen Submit Button
	 */
	public PhonePages clickStolenSubmit() {
		try {
			getDriver().manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
			btnStolenSubmit.click();
			Reporter.log("Stolen submit button is clickable");
		} catch (Exception e) {
			Assert.fail("Stolen submit button is not clickable");
		}
		return this;
	}

	/**
	 * To Click Term Cond
	 */
	public PhonePages clickTermsCond() {
		try {
			if (!notReqTerms.isDisplayed()) {
				chkTermsCond.click();
			}
			Reporter.log("Terms and conditions is clickable");
		} catch (Exception e) {
			Assert.fail("Terms and conditions is not clickable");
		}
		return this;
	}

	/**
	 * Click Terms Next
	 */
	public PhonePages clickTermsNext() {
		try {
			btnTermsNext.click();
			Reporter.log("Terms next button is clickable");
		} catch (Exception e) {
			Assert.fail("Terms next button is not clickable");
		}
		return this;

	}

	/**
	 * Get a Text stolen Headers
	 * 
	 * @return
	 */
	public String getTextstolenHeader() {
		waitFor(ExpectedConditions.invisibilityOfElementLocated(spinner));
		return restoreServiceLink.getText();
	}

	/**
	 * Click End Date
	 */
	public PhonePages clickEndDate() {
		try {
			endDate.click();
			Reporter.log("End date is clickable");
		} catch (Exception e) {
			Assert.fail("End date is not clickable");
		}
		return this;
	}

	/**
	 * Verify the CloudLinks
	 */
	public void verifyCloudLinks() {
		List<WebElement> ele = new ArrayList<WebElement>() {
			{
				add(changeSim);
				add(reportLostOrStolenLink);
				add(suspendLink);
				add(assurantClaim);
				add(tradeInstatus);
				add(checkOrderstatus);
				if (getDriver() instanceof AppiumDriver) {
					addAll(supportLinks);
				}
			}
		};
		for (int i = 0; i < ele.size(); i++) {
			if (ele.get(i).isDisplayed()) {
				ele.get(i).click();
				getDriver().navigate().back();
			}
		}

		ele.removeAll(ele);
		ele = new ArrayList<WebElement>() {
			{
				add(tradeInnow);
				if (getDriver() instanceof AppiumDriver) {
					add(checkOnRebateNowMobile);
				} else {
					add(checkOnRebate);
					addAll(supportLinks);
				}
				add(checkVoiceMail);
			}
		};

		String originalHandle = getDriver().getWindowHandle();

		for (int i = 0; i < ele.size(); i++) {
			if (ele.get(i).isDisplayed()) {
				ele.get(i).click();
				closeOtherTabs(originalHandle);
			}
		}
	}

	/**
	 * click OK button after restore service is completed
	 */
	public PhonePages clickRestoreSuspendOkButton() {
		try {
			restoreSuspendOk.click();
			Reporter.log("Restore Suspend ok button is clickable");
		} catch (Exception e) {
			Assert.fail("Restore Suspend ok button is not clickable");
		}
		return this;
	}

	/**
	 * click OK button after Unblock service is completed
	 */
	public PhonePages clickRestoreOkButton() {
		try {
			waitFor(ExpectedConditions.visibilityOf(modalOkButton));
			modalOkButton.click();
		} catch (Exception e) {
			Assert.fail("Restore Ok button is not Displayed to click");
		}
		return this;
	}

	/**
	 * @return boolean
	 */
	public PhonePages verifyAndconfirmRestore() {
		try {
			waitFor(ExpectedConditions.visibilityOf(restoreSuccessText));
			restoreSuccessText.isDisplayed();
			Reporter.log("Restore Success text is displayed");
		} catch (Exception e) {
			Assert.fail("Restore Success text is not displayed");
		}
		return this;
	}

	/**
	 * click Check Order Status Link
	 */
	public PhonePages clickCheckOrderStatusLink() {
		try {
			waitFor(ExpectedConditions.elementToBeClickable(checkOrderstatus));
			clickElementWithJavaScript(checkOrderstatus);
		} catch (Exception e) {
			Assert.fail("Check Order Status is not Displayed to Click");
		}
		return this;
	}

	/**
	 * click on Restore service link
	 */
	public PhonePages restoreSuspendedService() {
		try {
			restoreService.click();
			restoreSubmit.click();
			Reporter.log("Restore services links is clickable");
		} catch (Exception e) {
			Assert.fail("Restore services links is not clickable");
		}
		return this;
	}

	/**
	 * CLick Radio Lost device
	 */
	public PhonePages clickRadiolostDevice() {
		try {
			radioLostDevice.click();
			Reporter.log("Radio lost device button is clickable");
		} catch (Exception e) {
			Assert.fail("Radio lost device button is not clickable");
		}
		return this;
	}

	/**
	 * verify T&C Next button
	 * 
	 * @return
	 */
	public PhonePages verifyTermsNextButton() {
		try {
			btnTermsNext.isDisplayed();
			Reporter.log("Terms and Conditions next button is displayed");
		} catch (Exception e) {
			Assert.fail("Terms and Conditions next button is not displayed");
		}
		return this;
	}

	/**
	 * verify Suspend device button
	 * 
	 * @return
	 */
	public PhonePages verifySuspendService() {
		try {
			btnSuspendDevice.isDisplayed();
			Reporter.log("Suspend device button is displayed");
		} catch (Exception e) {
			Assert.fail("Suspend device button is not displayed");
		}
		return this;
	}

	/**
	 * @return
	 */
	public PhonePages ifServiceSuspended() {
		try {
			foundDevicelink.isDisplayed();
			Reporter.log("Suspend service yes button is displayed");
		} catch (Exception e) {
			Assert.fail("Suspend service yes button is not displayed");
		}
		return this;
	}

	/**
	 * @return
	 */
	public PhonePages isRestoreServiceLinkDisplayed() {
		try {
			getDriver().manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);
			restoreServiceLink.isDisplayed();
			Reporter.log("Restore Service link is displayed");
		} catch (Exception e) {
			Assert.fail("Restore Service link is not displayed");
		}
		return this;
	}

	/**
	 * Switch To Window
	 */
	public void switchWindow() {
		switchToWindow();
	}

	/**
	 * verify if muanage multi line settings link is displayed
	 * 
	 * @return true/false
	 */
	public PhonePages verifyManageMultiLineSettingsLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(manageSharingLink));
			manageSharingLink.isDisplayed();
			Reporter.log("Manage sharing links is displayed");
		} catch (Exception e) {
			Assert.fail("Manage sharing links is not displayed");
		}
		return this;
	}

	/**
	 * Verify Upgrade Your Device Title
	 * 
	 * @return
	 */
	public PhonePages verifyUpgradeTitle() {
		try {
			upgradeYourDevice.isDisplayed();
			Reporter.log("Upgrade your device title is displayed");
		} catch (Exception e) {
			Assert.fail("Upgrade your device title is not displayed");
		}
		return this;
	}

	/**
	 * Verify Jump Logo
	 * 
	 * @return
	 */
	public PhonePages verifyJumpLogo() {
		try {
			jumpLogo.isDisplayed();
			Reporter.log("Jump logo is displayed");
		} catch (Exception e) {
			Assert.fail("Jump logo is not displayed");
		}
		return this;
	}

	/**
	 * Verify Jump Text
	 * 
	 * @return
	 */
	public PhonePages verifyJumpText() {
		try {
			jumpCustomText.isDisplayed();
			Reporter.log("Jump text is displayed");
		} catch (Exception e) {
			Assert.fail("Jump text is not displayed");
		}
		return this;
	}

	/**
	 * Verify Jump Eligible Text
	 * 
	 * @return
	 */
	public PhonePages verifyJumpEligibleText() {
		try {
			jumpEligibleText.isDisplayed();
			Reporter.log("Jump eligible text is displayed");
		} catch (Exception e) {
			Assert.fail("Jump eligible text is not displayed");
		}
		return this;
	}

	/**
	 * verify Virtual Line Name is not Displayed in line selector
	 * 
	 * @return true/false
	 */
	public Boolean isVirtualLineNameDisplayed() {
		for (WebElement name : lineName) {
			if (name.isDisplayed() && name.getText().contains("Virtual Line")) {
				return false;
			}
		}
		return true;
	}

	/**
	 * verify Virtual Line Name is not Displayed in line selector
	 * 
	 * @return true/false
	 */
	public PhonePages verifyVirtualLineNameDisplayed() {
		Boolean result = isVirtualLineNameDisplayed();
		Assert.assertTrue(result, "'Virtual lines' text is not removed from line selector in phone page");
		Reporter.log("Virtual line text is not displayed");
		return this;
	}

	/**
	 * Verify Multiline Settings Link
	 * 
	 * @return
	 */
	public PhonePages veifyMultiLineSettingsLink() {
		try {
			waitFor(ExpectedConditions.visibilityOf(manageSharingLink));
			manageSharingLink.isDisplayed();
			Reporter.log("Manage Sharing link found");
		} catch (Exception e) {
			Assert.fail("Manage Sharing link not found");
		}
		return this;
	}

	/**
	 * Verify segmented Message
	 * 
	 * @return
	 */
	public PhonePages verifySegmentedMessageSection() {
		try {
			segmentedMessage.isDisplayed();
			Reporter.log("Segmented message is displayed");
		} catch (Exception e) {
			Assert.fail("Segmented message is not displayed");
		}
		return this;
	}

	/**
	 * Click on segmented Message
	 * 
	 * @return
	 */
	public PhonePages clickOnSegmentedMessage() {
		try {
			segmentedMessage.click();
			Reporter.log("Segmented message link is clickable");
		} catch (Exception e) {
			Assert.fail("Segmented message link is not clickable");
		}
		return this;
	}

	/**
	 * Click on Learn More Link
	 * 
	 * @return
	 */
	public PhonePages clickOnLearnMoreLink() {
		try {
			learnMoreLink.click();
			Reporter.log("Learn more link is clickable");
		} catch (Exception e) {
			Assert.fail("Learn more link is not clickable");
		}
		return this;
	}

	/**
	 * Verify segmented Message Content
	 * 
	 * @return
	 */
	public PhonePages verifySegmentedMessageContent() {
		try {
			segmentedMessageContent.isDisplayed();
			Reporter.log("Segmented message content is displayed");
		} catch (Exception e) {
			Assert.fail("Segmented message content is not displayed");
		}
		return this;
	}

	/**
	 * Click Shop Now
	 */
	public PhonePages clickShopNow() {
		try {
			shopNow.click();
			Reporter.log("Shop now button is clickable");
		} catch (Exception e) {
			Assert.fail("Shop now button is not clickable");
		}
		return this;
	}

	/**
	 * Click On Check On Rebate Now
	 */
	public PhonePages ClickOnCheckOnRebateNow() {
		try {
			checkOnRebate.click();
			Reporter.log("Chech on rebate now is clickable");
		} catch (Exception e) {
			Assert.fail("Chech on rebate now is not clickable");
		}
		return this;
	}

	public PhonePages clickOnUpgradeNowLink() {
		try {
			upgradeNowLink.click();
			Reporter.log("Upgrade now link is clickable");
		} catch (Exception e) {
			Assert.fail("Upgrade now link is not clickable");
		}
		return this;
	}

	public PhonePages verifyShopPage() {
		try {
			upgradeNowLink.click();
			Reporter.log("Upgrade now link is clickable");
		} catch (Exception e) {
			Assert.fail("Upgrade now link is not clickable");
		}
		return this;
	}

	/**
	 * Get Second Line Device Name
	 * 
	 * @return
	 */
	public String getSecondLineDeviceName() {
		waitforSpinner();
		checkPageIsReady();
		return secondLineDeviceName.getText();
	}

	/**
	 * Get Second Line Device Name
	 * 
	 * @return
	 */
	public String getDeviceTitle() {
		waitforSpinner();
		return deviceTitle.getText();
	}

	/**
	 * Click Device Unlock Status
	 */
	public PhonePages clickDeviceUnlockStatus() {
		try {
			waitforSpinner();
			if (getDriver() instanceof IOSDriver) {
				devicesLockstatusIos.click();
			} else {
				devicesLockstatus.click();
			}
			Reporter.log("Device lock status is clickable");
		} catch (Exception e) {
			Assert.fail("Device lock status is not clickable");
		}
		return this;
	}

	/**
	 * Verify Device Unlock Status
	 * 
	 * @return
	 */
	public String verifyDeviceUnlockStatus() {
		waitforSpinner();
		waitFor(ExpectedConditions.visibilityOf(deviceUnlockStatus));
		return deviceUnlockStatus.getText();
	}

	public PhonePages verifySelectedMSISDNtext(String MSISDN) {
		try {
			Assert.assertTrue(selectedMSISDNText.getText().contains(MSISDN), "Selected line is not displaying");
		} catch (Exception e) {
			Assert.fail("Unable to select second line");
		}
		return this;
	}

}