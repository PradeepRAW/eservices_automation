package com.tmobile.eservices.qa.pages.tmng.functional;

import java.util.List;

import org.apache.commons.lang3.text.WordUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;
import com.tmobile.eservices.qa.pages.tmng.TmngCommonPage;

public class InternationalCallingPage extends TmngCommonPage {
	private int timeout = 30;

	private final String pageLoadedText = ", Mexico, or Canada you can get unlimited calls to landlines in 70+ countries, mobile lines in 30+ countries, plus unlimited texting to virtually anywhere";

	private final String pageUrl = "/coverage/international-calling";

	@FindBy(css = "a[href='//www.t-mobile.com/about-us']")
	private WebElement about;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/accessibility-policy']")
	private WebElement accessibility;

	@FindBy(css = "a[href='https://www.t-mobile.com/hub/accessories-hub?icid=WMM_TM_Q417UNAVME_DJSZDFWU45Q11780']")
	private WebElement accessories;

	@FindBy(css = "a[href='//prepaid-phones.t-mobile.com/prepaid-activate']")
	private WebElement activateYourPrepaidPhoneOrDevice;

	@FindBy(css = "a.btn-secondary.btn-brand.btn")
	private WebElement addToYourPlan;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement areYouCallingMexicoOr1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement areYouCallingMexicoOr2;

	@FindBy(css = "a[href='https://www.t-mobile.com/optional-services/international-calling-mobile-countries.html']")
	private WebElement availableCountries;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/bring-your-own-phone?icid=WMM_TM_Q417UNAVME_2JDVKVMA5T12838']")
	private WebElement bringYourOwnPhone;

	@FindBy(css = "a[href='//business.t-mobile.com/']")
	private WebElement business1;

	@FindBy(css = "a[href='https://business.t-mobile.com/?icid=WMM_TM_Q417UNAVME_I44PB47UAS11783']")
	private WebElement business2;

	@FindBy(css = "a.tat17237_TopA.tmo_tfn_number")
	private WebElement callUs;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(6) button.accordion-toggle.collapsed")
	private WebElement canIGetStatesideInternational1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(6) button.accordion-toggle.collapsed")
	private WebElement canIGetStatesideInternational2;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) button.accordion-toggle.collapsed.")
	private WebElement canIMakeInternationalCalls1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(1) button.accordion-toggle.collapsed")
	private WebElement canIMakeInternationalCalls2;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) button.accordion-toggle.collapsed.")
	private WebElement canIPlaceCallsTo1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement canIPlaceCallsTo2;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement canIReceiveCallsFrom1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(2) button.accordion-toggle.collapsed")
	private WebElement canIReceiveCallsFrom2;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(3) button.accordion-toggle.collapsed.")
	private WebElement canIUseAPrepaid1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement canIUseAPrepaid2;

	@FindBy(css = "a[href='//www.t-mobile.com/careers']")
	private WebElement careers;

	@FindBy(css = "a[href='http://www.t-mobile.com/orderstatus/default.aspx']")
	private WebElement checkOrderStatus;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/4g-lte-network?icid=WMM_TM_Q417UNAVME_FPAO3ZN8J3811775']")
	private WebElement checkOutTheCoverage;

	@FindBy(css = "button.btn")
	private WebElement checkRates;

	@FindBy(css = "#e85e80584a607d7f0f5a354b3263b15267330fd5 div:nth-of-type(1) div:nth-of-type(2) p.copy a")
	private WebElement checkYourPlan;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info']")
	private WebElement consumerInformation;

	@FindBy(css = "a[href='http://www.t-mobile.com/contact-us.html']")
	private WebElement contactInformation;

	@FindBy(css = "a[href='//www.t-mobile.com/offers/deals-hub?icid=WMM_TMNG_Q416DLSRDS_O8RLYN4ZJ4I6275']")
	private WebElement deals1;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/deals-hub?icid=WMM_TM_Q417UNAVME_QXMF61Q49FA11771']")
	private WebElement deals2;

	@FindBy(css = "a[href='//www.telekom.com/']")
	private WebElement deutscheTelekom;

	@FindBy(css = "a[href='//support.t-mobile.com/community/phones-tablets-devices']")
	private WebElement deviceSupport;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement doINeedASpecial1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(3) button.accordion-toggle.collapsed")
	private WebElement doINeedASpecial2;

	@FindBy(name = "country")
	private WebElement enterCountryName;

	@FindBy(css = "a[href='https://es.t-mobile.com/?icid=WMM_TM_Q417UNAVME_KG7W6OINJH11785']")
	private WebElement espanol;

	@FindBy(css = "a[href='//es.t-mobile.com/']")
	private WebElement espaol;

	@FindBy(css = "a.lang-toggle-wrapper")
	private WebElement espaolEspaol;

	@FindBy(css = "a[href='http://www.t-mobile.com/store-locator.html']")
	private WebElement findAStore;

	@FindBy(css = "a[href='http://www.t-mobile.com/promotions']")
	private WebElement getARebate;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx#choicesaboutadvertising']")
	private WebElement interestbasedAds;

	@FindBy(css = "a[href='https://www.t-mobile.com/coverage/international-calling?icid=WMM_TM_Q118UNAVIN_9ZQGKL9LLZD12592']")
	private WebElement internationalCalling;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global.html']")
	private WebElement internationalRates;

	@FindBy(css = "a[href='http://iot.t-mobile.com/']")
	private WebElement internetOfThings;

	@FindBy(css = "a[href='http://investor.t-mobile.com/CorporateProfile.aspx?iid=4091145']")
	private WebElement investorRelations;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement isThereAServiceFor1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement isThereAServiceFor2;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div.panel-collapse.collapse.anchorID-611751 div.panel-body p:nth-of-type(2) a")
	private WebElement learnMore1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(1) div.panel-collapse.collapse.anchorID-611751 div.panel-body p:nth-of-type(2) a")
	private WebElement learnMore2;

	@FindBy(css = ".marketing-page.ng-scope header.global-header div:nth-of-type(1) ul:nth-of-type(2) li:nth-of-type(3) a.tmo_tfn_number")
	private WebElement letsTalk1800tmobile;

	@FindBy(css = "a.tat17237_TopA.tat17237_CustLogInIconCont")
	private WebElement logIn;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div.panel-collapse.collapse.anchorID-611751 div.panel-body p:nth-of-type(5) a")
	private WebElement logInToMyTmobile1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(1) div.panel-collapse.collapse.anchorID-611751 div.panel-body p:nth-of-type(5) a")
	private WebElement logInToMyTmobile2;

	@FindBy(css = "button.hamburger.hamburger-border")
	private WebElement menuMenu;

	@FindBy(css = "a[href='//my.t-mobile.com/?icid=WMD_TMNG_HMPGRDSGNU_S6FV9BEA3L36130']")
	private WebElement myTmobile1;

	@FindBy(css = "#stateside-results div:nth-of-type(2) div.section-content div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) p:nth-of-type(2) a:nth-of-type(1)")
	private WebElement myTmobile2;

	@FindBy(css = "#stateside-results div:nth-of-type(2) div.section-content div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) p:nth-of-type(2) a:nth-of-type(1)")
	private WebElement myTmobile3;

	@FindBy(css = "#stateside-results div:nth-of-type(2) div.section-content table.result-data.result-data-desktop tbody tr:nth-of-type(2) th.plan-title div:nth-of-type(1) p:nth-of-type(2) a:nth-of-type(1)")
	private WebElement myTmobile4;

	@FindBy(css = "#stateside-results div:nth-of-type(2) div.section-content table.result-data.result-data-desktop tbody tr:nth-of-type(2) th.plan-title div:nth-of-type(2) p:nth-of-type(2) a:nth-of-type(1)")
	private WebElement myTmobile5;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/policies/internet-service']")
	private WebElement openInternet;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phones?icid=WMM_TM_HMPGRDSGNA_P7QGF8N5L3B5877']")
	private WebElement phones;

	@FindBy(css = "a[href='//www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q117TMO1PL_H85BRNKTDO37510']")
	private WebElement plans;

	@FindBy(css = "a[href='//support.t-mobile.com/community/plans-services']")
	private WebElement plansServices;

	@FindBy(css = ".marketing-page.ng-scope header.global-header div:nth-of-type(1) ul:nth-of-type(1) li:nth-of-type(2) a")
	private WebElement prepaid1;

	@FindBy(css = "#overlay-menu ul:nth-of-type(2) li:nth-of-type(2) a.tat17237_BotA")
	private WebElement prepaid2;

	@FindBy(css = "a[href='//www.t-mobile.com/news']")
	private WebElement press;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/privacy']")
	private WebElement privacyCenter;

	@FindBy(css = "a[href='//www.t-mobile.com/website/privacypolicy.aspx']")
	private WebElement privacyStatement;

	@FindBy(css = "a[href='//www.t-mobile.com/responsibility/consumer-info/safety/9-1-1']")
	private WebElement publicSafety911;

	@FindBy(css = "a[href='http://www.t-mobilepr.com/']")
	private WebElement puertoRico;

	@FindBy(css = "a[href='//support.t-mobile.com/community/billing']")
	private WebElement questionsAboutYourBill;

	@FindBy(css = "a[href='https://prepaid-phones.t-mobile.com/prepaid-refill-api-bridging']")
	private WebElement refillYourPrepaidAccount;

	@FindBy(css = ".marketing-page.ng-scope div:nth-of-type(8) button")
	private WebElement scrollToTop;

	@FindBy(id = "searchText")
	private WebElement search1;

	@FindBy(id = "searchText")
	private WebElement search2;

	@FindBy(css = "a[href='https://www.t-mobile.com/switch-to-t-mobile?icid=WMM_TM_Q417UNAVME_QAE0VIGZ14T11772']")
	private WebElement seeHowMuchYouCouldSave;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(4) div.panel-collapse.collapse.anchorID-196082 div.panel-body p a")
	private WebElement seeRates1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(5) div.panel-collapse.collapse.anchorID-733376 div.panel-body p a")
	private WebElement seeRates2;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(5) div.panel-collapse.collapse.anchorID-733376 div.panel-body p a")
	private WebElement seeRates3;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(4) div.panel-collapse.collapse.anchorID-196082 div.panel-body p a")
	private WebElement seeRates4;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phones?icid=WMM_TM_Q417UNAVME_OXNEXWO4KCB11769']")
	private WebElement shopPhones;

	@FindBy(css = "#stateside-results div:nth-of-type(2) div.section-content div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) p:nth-of-type(2) a:nth-of-type(2)")
	private WebElement shopPlans1;

	@FindBy(css = "#stateside-results div:nth-of-type(2) div.section-content div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) p:nth-of-type(2) a:nth-of-type(2)")
	private WebElement shopPlans2;

	@FindBy(css = "#stateside-results div:nth-of-type(2) div.section-content table.result-data.result-data-desktop tbody tr:nth-of-type(2) th.plan-title div:nth-of-type(1) p:nth-of-type(2) a:nth-of-type(2)")
	private WebElement shopPlans3;

	@FindBy(css = "#stateside-results div:nth-of-type(2) div.section-content table.result-data.result-data-desktop tbody tr:nth-of-type(2) th.plan-title div:nth-of-type(2) p:nth-of-type(2) a:nth-of-type(2)")
	private WebElement shopPlans4;

	@FindBy(css = "a[href='#divfootermain']")
	private WebElement skipToFooter;

	@FindBy(css = "a[href='#maincontent']")
	private WebElement skipToMainContent;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-of-things?icid=WMM_TM_Q417UNAVME_SZBCRCV105G11778']")
	private WebElement smartDevices;

	@FindBy(css = "a[href='//www.t-mobile.com/store-locator.html']")
	private WebElement storeLocator;

	@FindBy(css = "a[href='https://www.t-mobile.com/store-locator?icid=WMM_TM_Q417UNAVME_JP4FGXM0A6211900']")
	private WebElement stores;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/student-teacher-smartphone-tablet-discounts?icid=WMM_TM_CAMPUSEXCL_UHTZZ7GR1RT11028']")
	private WebElement studentteacherDiscount;

	@FindBy(css = ".marketing-page.ng-scope header.global-header nav.main-nav.content-wrap.clearfix div:nth-of-type(2) div:nth-of-type(3) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch1;

	@FindBy(css = ".marketing-page.ng-scope header.global-header div:nth-of-type(4) form.search-form.ng-pristine.ng-valid button.search-submit")
	private WebElement submitSearch2;

	@FindBy(css = "a[href='//support.t-mobile.com/']")
	private WebElement supportHome;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsAndConditions&print=true']")
	private WebElement termsConditions;

	@FindBy(css = "a[href='//www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_TermsOfUse&print=true']")
	private WebElement termsOfUse;

	@FindBy(css = "a[href='https://business.t-mobile.com']")
	private WebElement tmobileForBusiness;

	@FindBy(css = "a[href='http://www.t-mobile.com/cell-phone-trade-in.html']")
	private WebElement tradeInProgram;

	@FindBy(css = "a[href='https://www.t-mobile.com/travel-abroad-with-simple-global?icid=WMM_TM_Q417UNAVME_AE1B0D6WUNH11777']")
	private WebElement travelingAbroad;

	@FindBy(css = "a[href='https://www.t-mobile.com/cell-phone-plans?icid=WMM_TM_Q417UNAVME_GHBARMBPJEO11768']")
	private WebElement unlimitedPlan;

	@FindBy(css = "a[href='https://www.t-mobile.com/offers/t-mobile-digits?icid=WMM_TM_Q417UNAVME_VEBBQDCSN9W11779']")
	private WebElement useMultipleNumbersOnMyPhone;

	@FindBy(css = "a[href='http://www.t-mobile.com/Templates/Popup.aspx?PAsset=Ftr_Ftr_ReturnPolicy&print=true']")
	private WebElement viewReturnPolicy;

	@FindBy(css = "a[href='https://www.t-mobile.com/internet-devices?icid=WMM_TM_Q417UNAVME_KX7JJ8E0YH11770']")
	private WebElement watchesTablets;

	@FindBy(css = "a[href='https://www.t-mobile.com/resources/how-to-join-us?icid=WMM_TM_Q417UNAVME_7ZKBD6XWQ712837']")
	private WebElement wellHelpYouJoin;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(4) button.accordion-toggle.collapsed.")
	private WebElement whatAreTheRatesIf1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(5) button.accordion-toggle.collapsed.")
	private WebElement whatAreTheRatesIf2;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement whatAreTheRatesIf3;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(2) div.panel-right div:nth-of-type(5) button.accordion-toggle.collapsed")
	private WebElement whatAreTheRatesIf4;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(1) div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement whereCanIFindPerminute1;

	@FindBy(css = "#c6c985d48edddae8f2394cca9e7076a327c5a476 div:nth-of-type(1) div:nth-of-type(2) div:nth-of-type(1) div.panel-left div:nth-of-type(4) button.accordion-toggle.collapsed")
	private WebElement whereCanIFindPerminute2;
	
	@FindBy(xpath = "//input[@placeholder='Enter country name']/following::ul[1]/li/a/strong")
	private List<WebElement> typeAheadListForCountry;
	
	@FindBy(xpath = "//input[@placeholder='Enter country name']/following::ul[1]/li")
	private List<WebElement> typeAheadList;
	
	@FindBy(css = "section[id='stateside-results'] div[class='result-header'] div p span")
	private WebElement checkRatesMsgDestination;
	
	@FindBy(css ="table[class='result-data result-data-desktop']")
	private WebElement resultantDataTable;
	
	@FindBy(xpath="//th[@class='title']/p[contains(text(),'Mobile to Landline')]")
	private WebElement mobileToLandlineColumn;
	
	@FindBy(xpath="//th[@class='title']/p[contains(text(),'Mobile to Mobile')]")
	private WebElement mobileToMobileColumn;
	
	@FindBy(xpath="//th[@class='title']/p[contains(text(),'Text (SMS)')]")
	private WebElement textColumn;
	
	@FindBy(xpath="//tr[2][contains(@class,'table-row')]/th/p")
	private WebElement statesideInternationalTalkHeader;
	
	@FindBy(xpath="//tr[3][contains(@class,'table-row')]/th/p")
	private WebElement payPerUseHeader;
	
	@FindBy(xpath = "//p[contains(text(),'Stateside International Talk')]/../following-sibling::td")
	private List<WebElement> statesideInternationalTalkRowData;
	
	@FindBy(xpath="//p[contains(text(),'Stateside International Talk')]/../../td[1]/p[2]/span")
	private WebElement mobileToLandlineRateValueSIT;
	
	@FindBy(xpath="//p[contains(text(),'Stateside International Talk')]/../../td[1]/p[2]/span[contains(text(),'No Data')]")
	private WebElement emptymobileToLandlineRateSIT;
	
	@FindBy(xpath="//p[contains(text(),'Stateside International Talk')]/../../td[2]/p[2]/span")
	private WebElement mobileToMobileRateValueSIT;
	
	@FindBy(xpath="//p[contains(text(),'Stateside International Talk')]/../../td[2]/p[2]/span[contains(text(),'No Data')]")
	private WebElement emptymobileToMobileRateSIT;
	
	@FindBy(xpath="//p[contains(text(),'Stateside International Talk')]/../../td[3]/p[2]/span")
	private WebElement textRateValueSIT;
	
	@FindBy(xpath="//p[contains(text(),'Stateside International Talk')]/../../td[3]/p[2]/span[contains(text(),'No Data')]")
	private WebElement emptyTextRateSIT;
	
	@FindBy(xpath = "//p[contains(text(),'Pay Per Use')]/../following-sibling::td")
	private List<WebElement> payPerUseRowData;
	
	@FindBy(xpath="//p[contains(text(),'Pay Per Use')]/../../td[1]/p[2]/span")
	private WebElement mobileToLandlineRateValuePayPerUse;
	
	@FindBy(xpath="//p[contains(text(),'Pay Per Use')]/../../td[1]/p[2]/span[contains(text(),'No Data')]")
	private WebElement emptymobileToLandlineRatePayPerUse;
	
	@FindBy(xpath="//p[contains(text(),'Pay Per Use')]/../../td[2]/p[2]/span")
	private WebElement mobileToMobileRateValuePayPerUse;
	
	@FindBy(xpath="//p[contains(text(),'Pay Per Use')]/../../td[2]/p[2]/span[contains(text(),'No Data')]")
	private WebElement emptymobileToMobileRatePayPerUse;
	
	@FindBy(xpath="//p[contains(text(),'Pay Per Use')]/../../td[3]/p[2]/span")
	private WebElement textRateValuePayPerUse;
	
	@FindBy(xpath="//p[contains(text(),'Pay Per Use')]/../../td[3]/p[2]/span[contains(text(),'No Data')]")
	private WebElement emptyTextRatePayPerUse;
	
	private final String invalidDestinationMessage = "Sorry, there are no results for this entry.";
	private final String statesideInternationalTalk = "Stateside International Talk";
	private final String payPerUse = "Pay Per Use";
		
	public InternationalCallingPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify that the page loaded completely.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage verifyPageLoaded() {
		try {
			verifyPageUrl();
			Reporter.log("International Calling page loaded");
		} catch (Exception ex) {
			Assert.fail("International Calling Page not loaded");
		}
		return this;
	}

	/**
	 * Verify that current page URL matches the expected URL.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl),60);
		return this;
	}

	/**
	 * Click on About Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickAboutLink() {
		about.click();
		return this;
	}

	/**
	 * Click on Accessibility Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickAccessibilityLink() {
		accessibility.click();
		return this;
	}

	/**
	 * Click on Accessories Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickAccessoriesLink() {
		accessories.click();
		return this;
	}

	/**
	 * Click on Activate Your Prepaid Phone Or Device Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickActivateYourPrepaidPhoneOrDeviceLink() {
		activateYourPrepaidPhoneOrDevice.click();
		return this;
	}

	/**
	 * Click on Add To Your Plan Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickAddToYourPlanLink() {
		addToYourPlan.click();
		return this;
	}

	/**
	 * Click on Are You Calling Mexico Or Canada Exclusively Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickAreYouCallingMexicoOr1Button() {
		areYouCallingMexicoOr1.click();
		return this;
	}

	/**
	 * Click on Are You Calling Mexico Or Canada Exclusively Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickAreYouCallingMexicoOr2Button() {
		areYouCallingMexicoOr2.click();
		return this;
	}

	/**
	 * Click on Available Countries Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickAvailableCountriesLink() {
		availableCountries.click();
		return this;
	}

	/**
	 * Click on Bring Your Own Phone Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickBringYourOwnPhoneLink() {
		bringYourOwnPhone.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickBusiness1Link() {
		business1.click();
		return this;
	}

	/**
	 * Click on Business Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickBusiness2Link() {
		business2.click();
		return this;
	}

	/**
	 * Click on Call Us Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCallUsLink() {
		callUs.click();
		return this;
	}

	/**
	 * Click on Can I Get Stateside International Talk If I Have A Prepaid Plan
	 * Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCanIGetStatesideInternational1Button() {
		canIGetStatesideInternational1.click();
		return this;
	}

	/**
	 * Click on Can I Get Stateside International Talk If I Have A Prepaid Plan
	 * Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCanIGetStatesideInternational2Button() {
		canIGetStatesideInternational2.click();
		return this;
	}

	/**
	 * Click on Can I Make International Calls With Nocreditcheck Plans Outside Of
	 * The Unlimited Countries Included With Stateside Services Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCanIMakeInternationalCalls1Button() {
		canIMakeInternationalCalls1.click();
		return this;
	}

	/**
	 * Click on Can I Make International Calls With Nocreditcheck Plans Outside Of
	 * The Unlimited Countries Included With Stateside Services Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCanIMakeInternationalCalls2Button() {
		canIMakeInternationalCalls2.click();
		return this;
	}

	/**
	 * Click on Can I Place Calls To Satellite Phones Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCanIPlaceCallsTo1Button() {
		canIPlaceCallsTo1.click();
		return this;
	}

	/**
	 * Click on Can I Place Calls To Satellite Phones Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCanIPlaceCallsTo2Button() {
		canIPlaceCallsTo2.click();
		return this;
	}

	/**
	 * Click on Can I Receive Calls From Another Country While In The U.s. How Much
	 * Do They Cost Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCanIReceiveCallsFrom1Button() {
		canIReceiveCallsFrom1.click();
		return this;
	}

	/**
	 * Click on Can I Receive Calls From Another Country While In The U.s. How Much
	 * Do They Cost Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCanIReceiveCallsFrom2Button() {
		canIReceiveCallsFrom2.click();
		return this;
	}

	/**
	 * Click on Can I Use A Prepaid International Calling Card To Call From The U.s.
	 * To Another Country Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCanIUseAPrepaid1Button() {
		canIUseAPrepaid1.click();
		return this;
	}

	/**
	 * Click on Can I Use A Prepaid International Calling Card To Call From The U.s.
	 * To Another Country Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCanIUseAPrepaid2Button() {
		canIUseAPrepaid2.click();
		return this;
	}

	/**
	 * Click on Careers Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCareersLink() {
		careers.click();
		return this;
	}

	/**
	 * Click on Check Order Status Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCheckOrderStatusLink() {
		checkOrderStatus.click();
		return this;
	}

	/**
	 * Click on Check Out The Coverage Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCheckOutTheCoverageLink() {
		checkOutTheCoverage.click();
		return this;
	}

	/**
	 * Click on Check Rates Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCheckRatesButton() {
		checkRates.click();
		Reporter.log("Clicked on 'Check rates' CTA successfully");
		return this;
	}

	/**
	 * Click on Check Your Plan Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickCheckYourPlanLink() {
		checkYourPlan.click();
		return this;
	}

	/**
	 * Click on Consumer Information Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickConsumerInformationLink() {
		consumerInformation.click();
		return this;
	}

	/**
	 * Click on Contact Information Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickContactInformationLink() {
		contactInformation.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickDeals1Link() {
		deals1.click();
		return this;
	}

	/**
	 * Click on Deals Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickDeals2Link() {
		deals2.click();
		return this;
	}

	/**
	 * Click on Deutsche Telekom Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickDeutscheTelekomLink() {
		deutscheTelekom.click();
		return this;
	}

	/**
	 * Click on Device Support Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickDeviceSupportLink() {
		deviceSupport.click();
		return this;
	}

	/**
	 * Click on Do I Need A Special Phone To Call International Numbers From The
	 * U.s. Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickDoINeedASpecial1Button() {
		doINeedASpecial1.click();
		return this;
	}

	/**
	 * Click on Do I Need A Special Phone To Call International Numbers From The
	 * U.s. Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickDoINeedASpecial2Button() {
		doINeedASpecial2.click();
		return this;
	}

	/**
	 * Click on Espanol Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickEspanolLink() {
		espanol.click();
		return this;
	}

	/**
	 * Click on Espaol Espaol Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickEspaolEspaolLink() {
		espaolEspaol.click();
		return this;
	}

	/**
	 * Click on Espaol Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickEspaolLink() {
		espaol.click();
		return this;
	}

	/**
	 * Click on Find A Store Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickFindAStoreLink() {
		findAStore.click();
		return this;
	}

	/**
	 * Click on Get A Rebate Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickGetARebateLink() {
		getARebate.click();
		return this;
	}

	/**
	 * Click on Interestbased Ads Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickInterestbasedAdsLink() {
		interestbasedAds.click();
		return this;
	}

	/**
	 * Click on International Calling Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickInternationalCallingLink() {
		internationalCalling.click();
		return this;
	}

	/**
	 * Click on International Rates Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickInternationalRatesLink() {
		internationalRates.click();
		return this;
	}

	/**
	 * Click on Internet Of Things Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickInternetOfThingsLink() {
		internetOfThings.click();
		return this;
	}

	/**
	 * Click on Investor Relations Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickInvestorRelationsLink() {
		investorRelations.click();
		return this;
	}

	/**
	 * Click on Is There A Service For International Texting Only That Doesnt
	 * Include Calling Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickIsThereAServiceFor1Button() {
		isThereAServiceFor1.click();
		return this;
	}

	/**
	 * Click on Is There A Service For International Texting Only That Doesnt
	 * Include Calling Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickIsThereAServiceFor2Button() {
		isThereAServiceFor2.click();
		return this;
	}

	/**
	 * Click on Learn More Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickLearnMore1Link() {
		learnMore1.click();
		return this;
	}

	/**
	 * Click on Learn More Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickLearnMore2Link() {
		learnMore2.click();
		return this;
	}

	/**
	 * Click on Lets Talk 1800tmobile Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickLetsTalk1800tmobileLink() {
		letsTalk1800tmobile.click();
		return this;
	}

	/**
	 * Click on Log In Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickLogInLink() {
		logIn.click();
		return this;
	}

	/**
	 * Click on Log In To My Tmobile Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickLogInToMyTmobile1Link() {
		logInToMyTmobile1.click();
		return this;
	}

	/**
	 * Click on Log In To My Tmobile Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickLogInToMyTmobile2Link() {
		logInToMyTmobile2.click();
		return this;
	}

	/**
	 * Click on Menu Menu Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickMenuMenuButton() {
		menuMenu.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickMyTmobile1Link() {
		myTmobile1.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickMyTmobile2Link() {
		myTmobile2.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickMyTmobile3Link() {
		myTmobile3.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickMyTmobile4Link() {
		myTmobile4.click();
		return this;
	}

	/**
	 * Click on My Tmobile Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickMyTmobile5Link() {
		myTmobile5.click();
		return this;
	}

	/**
	 * Click on Open Internet Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickOpenInternetLink() {
		openInternet.click();
		return this;
	}

	/**
	 * Click on Phones Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickPhonesLink() {
		phones.click();
		return this;
	}

	/**
	 * Click on Plans Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickPlansLink() {
		plans.click();
		return this;
	}

	/**
	 * Click on Plans Services Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickPlansServicesLink() {
		plansServices.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickPrepaid1Link() {
		prepaid1.click();
		return this;
	}

	/**
	 * Click on Prepaid Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickPrepaid2Link() {
		prepaid2.click();
		return this;
	}

	/**
	 * Click on Press Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickPressLink() {
		press.click();
		return this;
	}

	/**
	 * Click on Privacy Center Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickPrivacyCenterLink() {
		privacyCenter.click();
		return this;
	}

	/**
	 * Click on Privacy Statement Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickPrivacyStatementLink() {
		privacyStatement.click();
		return this;
	}

	/**
	 * Click on Public Safety911 Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickPublicSafety911Link() {
		publicSafety911.click();
		return this;
	}

	/**
	 * Click on Puerto Rico Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickPuertoRicoLink() {
		puertoRico.click();
		return this;
	}

	/**
	 * Click on Questions About Your Bill Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickQuestionsAboutYourBillLink() {
		questionsAboutYourBill.click();
		return this;
	}

	/**
	 * Click on Refill Your Prepaid Account Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickRefillYourPrepaidAccountLink() {
		refillYourPrepaidAccount.click();
		return this;
	}

	/**
	 * Click on Scroll To Top Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickScrollToTopButton() {
		scrollToTop.click();
		return this;
	}

	/**
	 * Click on See How Much You Could Save Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickSeeHowMuchYouCouldSaveLink() {
		seeHowMuchYouCouldSave.click();
		return this;
	}

	/**
	 * Click on See Rates. Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickSeeRates1Link1() {
		seeRates1.click();
		return this;
	}

	/**
	 * Click on See Rates Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickSeeRates1Link2() {
		seeRates2.click();
		return this;
	}

	/**
	 * Click on See Rates. Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickSeeRatesLink3() {
		seeRates3.click();
		return this;
	}

	/**
	 * Click on See Rates Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickSeeRatesLink4() {
		seeRates4.click();
		return this;
	}

	/**
	 * Click on Shop Phones Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickShopPhonesLink() {
		shopPhones.click();
		return this;
	}

	/**
	 * Click on Shop Plans Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickShopPlans1Link() {
		shopPlans1.click();
		return this;
	}

	/**
	 * Click on Shop Plans Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickShopPlans2Link() {
		shopPlans2.click();
		return this;
	}

	/**
	 * Click on Shop Plans Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickShopPlans3Link() {
		shopPlans3.click();
		return this;
	}

	/**
	 * Click on Shop Plans Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickShopPlans4Link() {
		shopPlans4.click();
		return this;
	}

	/**
	 * Click on Skip To Footer Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickSkipToFooterLink() {
		skipToFooter.click();
		return this;
	}

	/**
	 * Click on Skip To Main Content Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickSkipToMainContentLink() {
		skipToMainContent.click();
		return this;
	}

	/**
	 * Click on Smart Devices Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickSmartDevicesLink() {
		smartDevices.click();
		return this;
	}

	/**
	 * Click on Store Locator Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickStoreLocatorLink() {
		storeLocator.click();
		return this;
	}

	/**
	 * Click on Stores Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickStoresLink() {
		stores.click();
		return this;
	}

	/**
	 * Click on Studentteacher Discount Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickStudentteacherDiscountLink() {
		studentteacherDiscount.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickSubmitSearch1Button() {
		submitSearch1.click();
		return this;
	}

	/**
	 * Click on Submit Search Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickSubmitSearch2Button() {
		submitSearch2.click();
		return this;
	}

	/**
	 * Click on Support Home Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickSupportHomeLink() {
		supportHome.click();
		return this;
	}

	/**
	 * Click on Terms Conditions Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickTermsConditionsLink() {
		termsConditions.click();
		return this;
	}

	/**
	 * Click on Terms Of Use Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickTermsOfUseLink() {
		termsOfUse.click();
		return this;
	}

	/**
	 * Click on Tmobile For Business Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickTmobileForBusinessLink() {
		tmobileForBusiness.click();
		return this;
	}

	/**
	 * Click on Trade In Program Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickTradeInProgramLink() {
		tradeInProgram.click();
		return this;
	}

	/**
	 * Click on Traveling Abroad Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickTravelingAbroadLink() {
		travelingAbroad.click();
		return this;
	}

	/**
	 * Click on Unlimited Plan Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickUnlimitedPlanLink() {
		unlimitedPlan.click();
		return this;
	}

	/**
	 * Click on Use Multiple Numbers On My Phone Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickUseMultipleNumbersOnMyPhoneLink() {
		useMultipleNumbersOnMyPhone.click();
		return this;
	}

	/**
	 * Click on View Return Policy Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickViewReturnPolicyLink() {
		viewReturnPolicy.click();
		return this;
	}

	/**
	 * Click on Watches Tablets Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickWatchesTabletsLink() {
		watchesTablets.click();
		return this;
	}

	/**
	 * Click on Well Help You Join Link.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickWellHelpYouJoinLink() {
		wellHelpYouJoin.click();
		return this;
	}

	/**
	 * Click on What Are The Rates If I Have A Tmobile Prepaid Plan Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickWhatAreTheRatesIf1Button() {
		whatAreTheRatesIf1.click();
		return this;
	}

	/**
	 * Click on What Are The Rates If I Have A Tmobile Prepaid Plan Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickWhatAreTheRatesIf2Button() {
		whatAreTheRatesIf2.click();
		return this;
	}

	/**
	 * Click on What Are The Rates If I Have The Stateside 10 Plan Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickWhatAreTheRatesIf3Button() {
		whatAreTheRatesIf3.click();
		return this;
	}

	/**
	 * Click on What Are The Rates If I Have A Tmobile Prepaid Plan Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickWhatAreTheRatesIf4Button() {
		whatAreTheRatesIf4.click();
		return this;
	}

	/**
	 * Click on Where Can I Find Perminute Rates For The Countries I Want To Call
	 * Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickWhereCanIFindPerminute1Button() {
		whereCanIFindPerminute1.click();
		return this;
	}

	/**
	 * Click on Where Can I Find Perminute Rates For The Countries I Want To Call
	 * Button.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage clickWhereCanIFindPerminute2Button() {
		whereCanIFindPerminute2.click();
		return this;
	}

	/**
	 * Set value to Enter Country Name Text field.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage setEnterCountryNameTextField(String enterCountryNameValue) {
		enterCountryName.sendKeys(enterCountryNameValue);
		Reporter.log("Entered country name: "+enterCountryNameValue+"in the text box");
		return this;
	}

	/**
	 * Set value to Search Text field.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage setSearch1TextField(String search1Value) {
		search1.sendKeys(search1Value);
		return this;
	}

	/**
	 * Set value to Search Text field.
	 *
	 * @return the InternationalCallingPage class instance.
	 */
	public InternationalCallingPage setSearch2TextField(String search2Value) {
		search2.sendKeys(search2Value);
		return this;
	}
	
	/***
	 * Verify Type ahead list is displayed of Country details
	 * 
	 */
	public InternationalCallingPage verifyTypeAheadListOfCountryDetails() {
		checkPageIsReady();
		try {
			JavascriptExecutor js = (JavascriptExecutor) getDriver();
			js.executeScript("window.scrollBy(0,20)");
			typeAheadListForCountry.get(0).click();
			Reporter.log("Selected the value from the type ahead list");
		} catch (Exception e) {
			Assert.fail("failed to select value in the type ahead list");
		}
		return this;
	}
	
	/***
	 * Verify user can see name of the country to which Alias belongs
	 * 
	 */
	public InternationalCallingPage verifyDestinationNameForTheAliasNameEntered() {
		checkPageIsReady();
		try {
			JavascriptExecutor js = (JavascriptExecutor) getDriver();
			js.executeScript("window.scrollBy(0,20)");
			typeAheadList.get(0).isDisplayed();
			Reporter.log("User can see name of the country '"+typeAheadList.get(0).getText()+"' to which Alias belongs");
		} catch (Exception e) {
			Assert.fail("failed to see the name of the country to which Alias belong");
		}
		return this;
	}
	
	/***
	 * Verify Type ahead list is displayed of Country details
	 * 
	 */
	public InternationalCallingPage selectDestinationForAliasFromTypeAheadList() {
		checkPageIsReady();
		try {
			typeAheadList.get(0).click();
			Reporter.log("Selected the value from the type ahead list");
		} catch (Exception e) {
			Assert.fail("failed to select value in the type ahead list");
		}
		return this;
	}
	
	/**
	 * verify entered country name in the header
	 * 
	 * @return
	 */
	public InternationalCallingPage verifyCountryNameInHeader(String destinationName) {
		checkPageIsReady();
		try {
			String actualDestination = checkRatesMsgDestination.getText();
			String expectedDestination = WordUtils.capitalize(destinationName);
			Assert.assertTrue(actualDestination.contains(expectedDestination),"Country name is not displayed in the header");{
			Reporter.log("Country name is displayed in the header");
			}
		} catch (Exception e) {
			Assert.fail("Unable to display the entered Country name in the header");
		}
		return this;
	}
	
	/***
	 * verify Resultant table rows and columns after clicking on 'Check Rates' Button
	 * 
	 * @return
	 */
	public InternationalCallingPage verifyResultantTableRowsandColumns() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(resultantDataTable),60);
			Assert.assertTrue(mobileToLandlineColumn.isDisplayed());
			Assert.assertTrue(mobileToMobileColumn.isDisplayed());
			Assert.assertTrue(textColumn.isDisplayed());
			Reporter.log("Mobile to Landline, Mobile to Mobile  and Text(SMS) columns are displayed");
		} catch (Exception e) {
			Assert.fail("Unable to display the Mobile to Landline, Mobile to Mobile  and Text(SMS) columns");
		}
		return this;
	}
	
	/***
	 * verify 'Stateside International Talk' Header including its data
	 * 
	 * @return
	 */
	public InternationalCallingPage verifyStatesideInternationalTalkRates() {
		checkPageIsReady();
		try {
			if (statesideInternationalTalkHeader.getText().equals(statesideInternationalTalk)) {
				Reporter.log("'Stateside International Talk' Header is displayed");
				verifyDataDisplayedForStatesideInternationalTalk();
			} else {
				Reporter.log("'Stateside International Talk' rate is not displayed for this destination");
			}
		} catch (Exception e) {
			Assert.fail("'Stateside International Talk' Header is not displayed");
		}
		return this;
	}
	
	/***
	 * verify 'Pay Per Use' header including its data
	 * 
	 * @return
	 */
	public InternationalCallingPage verifyPayPerUseRates() {
		checkPageIsReady();
		try {
			if (payPerUseHeader.getText().equals(payPerUse)) {
				Reporter.log("'Pay Per Use' Header is displayed");
				verifyDataDisplayedForPayPerUse();
				} else {
				Reporter.log("'Pay Per Use' rate is not displayed for this destination");
			}
		} catch (Exception e) {
			Assert.fail("'Pay Per Use' are not displayed for this destination");
		}
		return this;
	}
	
	/***
	 * verify the data displayed for Stateside International Talk
	 * 
	 * @return
	 */
	public InternationalCallingPage verifyDataDisplayedForStatesideInternationalTalk() {
		checkPageIsReady();
		try {
			for (WebElement ele : statesideInternationalTalkRowData) {
				Assert.assertTrue(ele.isDisplayed());
			}
			Reporter.log("Data is displayed successfully for Stateside International Talk");
			
			String emptyArrayMobileToLandlineValue = "//p[contains(text(),'Stateside International Talk')]/../../td[1]/p[2]/span[contains(text(),'No Data')]";
			if (mobileToLandlineRateValueSIT.getAttribute("innerText").contains("$")){
			String mobToLandlineValue = mobileToLandlineRateValueSIT.getAttribute("innerText").substring(1, mobileToLandlineRateValueSIT.getAttribute("innerText").length()-4);
			double mobileToLandlineValue = Double.parseDouble(mobToLandlineValue);
          	if(mobileToLandlineValue > 0){
				Reporter.log("Data is displayed successfully in Mobile To Landline column for Stateside International Talk");
			}
			}else if(mobileToLandlineRateValueSIT.getAttribute("innerText").contains("Unlimited")){
				Reporter.log("'Unlimited' is shown in Mobile To Landline column for Stateside International Talk, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayMobileToLandlineValue)).size() != 0){
				Assert.assertTrue(emptymobileToLandlineRateSIT.getAttribute("innerText").contains("No Data"));
				Reporter.log("'No Data' is shown in Mobile To Landline column for Stateside International Talk, as we received an Empty array values");
			}
			
			String emptyArrayMobileToMobileValue = "//p[contains(text(),'Stateside International Talk')]/../../td[2]/p[2]/span[contains(text(),'No Data')]";
			if (mobileToMobileRateValueSIT.getAttribute("innerText").contains("$")){
			String mobToMobileValue = mobileToMobileRateValueSIT.getAttribute("innerText").substring(1, mobileToMobileRateValueSIT.getAttribute("innerText").length()-4);
			double mobileToMobileValue = Double.parseDouble(mobToMobileValue);
			if(mobileToMobileValue > 0){
				Reporter.log("Data is displayed successfully in Mobile To Mobile column for Stateside International Talk");
			}
			}else if(mobileToMobileRateValueSIT.getAttribute("innerText").contains("Unlimited")){
				Reporter.log("'Unlimited' is shown in Mobile To Mobile column for Stateside International Talk, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayMobileToMobileValue)).size() != 0){
				Assert.assertTrue(emptymobileToMobileRateSIT.getAttribute("innerText").contains("No Data"));
				Reporter.log("'No Data' is shown in Mobile To Mobile column for Stateside International Talk, as we received an Empty array values");
			}
			
			String emptyArrayTextValue = "//p[contains(text(),'Stateside International Talk')]/../../td[3]/p[2]/span[contains(text(),'No Data')]";
			if (textRateValueSIT.getAttribute("innerText").contains("$")){
			String textValue = textRateValueSIT.getAttribute("innerText").substring(1, textRateValueSIT.getAttribute("innerText").length()-4);
			double textSMSValue = Double.parseDouble(textValue);
			if(textSMSValue > 0){
				Reporter.log("Data is displayed successfully in Text(SMS) column for Stateside International Talk");
			}
			}else if(textRateValueSIT.getAttribute("innerText").contains("Unlimited")){
				Reporter.log("'Unlimited' is shown in Text(SMS) column for Stateside International Talk, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayTextValue)).size() != 0){
				Assert.assertTrue(emptyTextRateSIT.getAttribute("innerText").contains("No Data"));
				Reporter.log("'No Data' is shown in Text(SMS) column for Stateside International Talk, as we received an Empty array values");
			}
		}catch (Exception e) {
			Assert.fail("Unable to display the data in the columns for Stateside International Talk");
		}
		return this;
	}
	
	/***
	 * verify the data displayed for Pay Per Use
	 * 
	 * @return
	 */
	public InternationalCallingPage verifyDataDisplayedForPayPerUse() {
		checkPageIsReady();
		try {
			for (WebElement ele : payPerUseRowData) {
				Assert.assertTrue(ele.isDisplayed());
			}
			Reporter.log("Data is displayed successfully for Pay Per Use");
			
			String emptyArrayMobileToLandlineValue = "//p[contains(text(),'Pay Per Use')]/../../td[1]/p/span[contains(text(),'No Data')]";
			if(mobileToLandlineRateValuePayPerUse.getAttribute("innerText").contains("$")){
				String mobToLandlineValue = mobileToLandlineRateValuePayPerUse.getAttribute("innerText").substring(1, mobileToLandlineRateValuePayPerUse.getAttribute("innerText").length()-4);
				double mobileToLandlineValue = Double.parseDouble(mobToLandlineValue);
				if(mobileToLandlineValue > 0){
				Reporter.log("Data is displayed successfully in Mobile To Landline column for Pay Per Use");
			}
		    }else if(mobileToLandlineRateValuePayPerUse.getAttribute("innerText").contains("Unlimited")){
				Reporter.log("'Unlimited' is shown in Mobile To Landline column for Pay Per Use, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayMobileToLandlineValue)).size() != 0){
				Assert.assertTrue(emptymobileToLandlineRatePayPerUse.getAttribute("innerText").contains("No Data"));
				Reporter.log("'No Data' is shown in Mobile To Landline column for Pay Per Use, as we received an Empty array values");
			}
			
			String emptyArrayMobileToMobileValue = "//p[contains(text(),'Pay Per Use')]/../../td[2]/p/span[contains(text(),'No Data')]";
			if(mobileToMobileRateValuePayPerUse.getAttribute("innerText").contains("$")){
				String mobToMobileValue = mobileToMobileRateValuePayPerUse.getAttribute("innerText").substring(1, mobileToMobileRateValuePayPerUse.getAttribute("innerText").length()-4);
				double mobileToMobileValue = Double.parseDouble(mobToMobileValue);
				if(mobileToMobileValue > 0){
				Reporter.log("Data is displayed successfully in Mobile To Mobile column for Pay Per Use");
			}
			}else if(mobileToMobileRateValuePayPerUse.getAttribute("innerText").contains("Unlimited")){
				Reporter.log("'Unlimited' is shown in Mobile To Mobile column for Pay Per Use, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayMobileToMobileValue)).size() != 0){
				Assert.assertTrue(emptymobileToMobileRatePayPerUse.getAttribute("innerText").contains("No Data"));
				Reporter.log("'No Data' is shown in Mobile To Mobile column for Pay Per Use, as we received an Empty array values");
			}
			
			String emptyArrayTextValue = "//p[contains(text(),'Pay Per Use')]/../../td[3]/p/span[contains(text(),'No Data')]";
			if(textRateValuePayPerUse.getAttribute("innerText").contains("$")){
				String textValue = textRateValuePayPerUse.getAttribute("innerText").substring(1, textRateValuePayPerUse.getAttribute("innerText").length()-4);
				double textSMSValue = Double.parseDouble(textValue);
				if(textSMSValue > 0){
				Reporter.log("Data is displayed successfully in Text(SMS) column for Pay Per Use");
			}
			}else if(textRateValuePayPerUse.getAttribute("innerText").contains("Unlimited")){
				Reporter.log("'Unlimited' is shown in Text(SMS) column for Pay Per Use, as there is no data");
			}
			else if(getDriver().findElements(By.xpath(emptyArrayTextValue)).size() != 0){
				Assert.assertTrue(emptyTextRatePayPerUse.getAttribute("innerText").contains("No Data"));
				Reporter.log("'No Data' is shown in Text(SMS) column for Pay Per Use, as we received an Empty array values");
			}
			}catch (Exception e) {
				Assert.fail("Unable to display the data in the columns for Pay Per Use");
		}
		return this;
	}
	
	/**
	 * verify selected destination name in the header
	 * 
	 * @return
	 */
	public InternationalCallingPage verifySelectedDestinationNameInHeader() {
		checkPageIsReady();
		try {
			String actualDestination = checkRatesMsgDestination.getText();
			String expectedDestination = WordUtils.capitalize(typeAheadList.get(0).getText());
			Assert.assertTrue(actualDestination.contains(expectedDestination),"Destination name is not displayed in the header");{
			Reporter.log("Destination name is displayed in the header");
			}
		} catch (Exception e) {
			Assert.fail("Unable to display the selected destination name in the header");
		}
		return this;
	}
	
	/***
	 * verify 'Sorry, there are no results for this entry.' message is displayed for invalid destination
	 * 
	 * @return
	 */
	public InternationalCallingPage verifyMsgForInvalidDestination() {
		checkPageIsReady();
		try {
			waitFor(ExpectedConditions.visibilityOf(checkRatesMsgDestination),60);
			Assert.assertTrue(checkRatesMsgDestination.isDisplayed());
			Reporter.log(
					"'Sorry, there are no results for this entry.' message is displayed, as the entered destination is invalid");
		} catch (Exception e) {
			Assert.fail("'Sorry, there are no results for this entry.' message is not displayed");
		}
		return this;
	}
	
	/**
	 * verify selected destination name in the header
	 * 
	 * @return
	 */
	public InternationalCallingPage verifyResults() {
		checkPageIsReady();
		try {
			if (checkRatesMsgDestination.getText().equals(invalidDestinationMessage)) {
				verifyMsgForInvalidDestination();
			} else {
				verifySelectedDestinationNameInHeader();
				verifyResultantTableRowsandColumns();
				verifyStatesideInternationalTalkRates();
			}
		} catch (Exception e) {
			Assert.fail("Unable to verify results for the Destination Alias entered ");
		}
		return this;
	}
	}
