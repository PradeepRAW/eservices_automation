package com.tmobile.eservices.qa.api.eos;

import java.util.HashMap;
import java.util.Map;

import com.tmobile.eqm.testfrwk.ui.core.service.BaseService.RestCallType;
import com.tmobile.eqm.testfrwk.ui.core.service.RestService;
import com.tmobile.eservices.qa.api.ApiCommonLib;
import com.tmobile.eservices.qa.api.RestServiceCallType;
import com.tmobile.eservices.qa.data.ApiTestData;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;

public class StoreLocatorApi extends ApiCommonLib {

	/***
	 * Gets all the Stores info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getStoreById(ApiTestData apiTestData, String storeId) throws Exception {
		Map<String, String> headers = buildGetStoreLocatorHeader(apiTestData);
		String resourceURL = "getStoreById?storeId=" + storeId;
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, "",RestServiceCallType.GET);
		return response;
	}

	/***
	 * Gets all the accessories description parameters: - $ref:
	 * '#/parameters/oAuth' - $ref: '#/parameters/transactionId' - $ref:
	 * '#/parameters/correlationId' - $ref: '#/parameters/applicationId' - $ref:
	 * '#/parameters/channelId' - $ref: '#/parameters/clientId' - $ref:
	 * '#/parameters/transactionBusinessKey' - $ref:
	 * '#/parameters/transactionBusinessKeyType' - $ref:
	 * '#/parameters/transactionType'
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response addCustomer(ApiTestData apiTestData, String requestBody) throws Exception {
		Map<String, String> headers = buildGetStoreLocatorHeader(apiTestData);
		String resourceURL = "addCustomer";
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, requestBody,RestServiceCallType.POST);
		return response;
	}

	/***
	 * Gets all the Stores info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getStoresInCity(ApiTestData apiTestData, String state, String city, String storeType)throws Exception {
		Map<String, String> headers = buildGetStoreLocatorHeader(apiTestData);
		String resourceURL = "getStoresInCity?state=" + "&city=" + city + "&storeType=" + storeType;
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, "",RestServiceCallType.GET);
		return response;
	}

	/***
	 * Gets all the Stores info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getStoreByName(ApiTestData apiTestData, String state, String city, String storeName)throws Exception {
		Map<String, String> headers = buildGetStoreLocatorHeader(apiTestData);
		String resourceURL = "getStoreByName?state=" + state + "&city=" + city + "&storeName=" + storeName;
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, "",RestServiceCallType.GET);
		return response;
	}

	/***
	 * Gets all the Stores info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getStoresInState(ApiTestData apiTestData, String state) throws Exception {
		Map<String, String> headers = buildGetStoreLocatorHeader(apiTestData);
		String resourceURL = "getStoresInState?state=" + state;
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, "",RestServiceCallType.GET);
		return response;
	}

	/***
	 * Gets all the Stores info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getStoresByCoordinates(ApiTestData apiTestData, String latitude, String longitude, String count,String radius) throws Exception {
		Map<String, String> headers = buildGetStoreLocatorHeader(apiTestData);
		String resourceURL = "getStoresByCoordinates?latitude=" + latitude + "&longitude="+ longitude + "&count=" + count + "&radius=" + radius;
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, "",RestServiceCallType.GET);
		return response;
	}

	/***
	 * Gets all the Stores info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getAllStoreUrls(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildGetStoreLocatorHeader(apiTestData);
		String resourceURL = "getAllStoreUrls";
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, "",RestServiceCallType.GET);
		return response;
	}

	/***
	 * Gets all the Stores info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getReasons(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildGetStoreLocatorHeader(apiTestData);
		String resourceURL = "getReasons";
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, "",RestServiceCallType.GET);
		return response;
	}

	/***
	 * Gets all the Stores info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getInStoreWaitTimes(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = buildGetStoreLocatorHeader(apiTestData);
		String resourceURL = "getInStoreWaitTimes";
		Response response = invokeRestServiceCall(headers, tmng_base_url, resourceURL, "",RestServiceCallType.GET);
		return response;
	}
	
	/***
	 * Gets all the Stores info parameters:
	 * 
	 * @return
	 * @throws Exception
	 */
	public Response getStoreByAddress(ApiTestData apiTestData, String address,String storeType,String radius,String count) throws Exception {
		Map<String, String> headers = buildGetStoreLocatorHeader(apiTestData);
		RestService service = new RestService("", tmng_base_url);
		RequestSpecBuilder reqSpec = service.getRequestbuilder();
		reqSpec.addHeaders(headers);
		reqSpec.addQueryParam("address", address);
		reqSpec.addQueryParam("storeType", storeType);
		if(radius!=null && Integer.parseInt(radius) > 0)
			reqSpec.addQueryParam("radius", radius);
		if(count!=null && Integer.parseInt(count) > 0)
			reqSpec.addQueryParam("count", count);
		service.setRequestSpecBuilder(reqSpec);
		Response response = service.callService("v2.1/getStoresByAddress", RestCallType.GET);
		System.out.println(response.asString());
		return response;
	}

	private Map<String, String> buildGetStoreLocatorHeader(ApiTestData apiTestData) throws Exception {
		Map<String, String> headers = new HashMap<String, String>();
		return headers;
	}

}
