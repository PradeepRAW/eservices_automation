package com.tmobile.eservices.qa.pages.global;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

/**
 * @author rnallamilli
 * 
 */
public class AccessibilityPage extends CommonPage {

	private static final String pageUrl = "accessiblity";

	public AccessibilityPage(WebDriver webDriver) {
		super(webDriver);
	}

	/**
	 * Verify Accessibility page.
	 */
	public AccessibilityPage verifyAccessibilityPage() {
		try {
			waitforSpinner();
			checkPageIsReady();
			verifyPageUrl(pageUrl);
			Reporter.log("Accessibility page displayed");
		} catch (NoSuchElementException e) {
			Assert.fail("Accessibility page not displayed");
		}
		return this;
	}
}
