package com.tmobile.eservices.qa.pages.payments;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.Reporter;

import com.tmobile.eservices.qa.pages.CommonPage;

public class EquipmentPaymentAmountPage extends CommonPage {
	private final String pageUrl = "equipment-payment-amount";
	
	@FindBy(css = "p.Display4")
	private WebElement pageHeader;


	@FindBy(css = "span#paymentLable")
	private WebElement paymentMethod;
	
	@FindBy(css = "button.PrimaryCTA")
	private WebElement agreeContinueButton;
	
	

	public EquipmentPaymentAmountPage(WebDriver webDriver) {
		super(webDriver);
	}
	
	/**
	 * Verify that current page URL matches the expected URL.
	 */
	public EquipmentPaymentAmountPage verifyPageUrl() {
		waitFor(ExpectedConditions.urlContains(pageUrl));
		return this;
	}
	
	/**
	 * Verify that the page loaded completely.
	 * 
	 * @throws InterruptedException
	 */
	public EquipmentPaymentAmountPage verifyPageLoaded() {
		try {
			checkPageIsReady();
			waitFor(ExpectedConditions.visibilityOf(pageHeader));
			verifyPageUrl();
			Reporter.log("EquipmentPaymentAmount Page is loaded.");
		} catch (Exception e) {
			Assert.fail("EquipmentPaymentAmount Page is not loaded");
		}
		return this;
	}

	
	
	/**
	 * click payment method blade
	 */
	public void clickCTAButton() {
		try {
			agreeContinueButton.click();
			Reporter.log("Clicked on Agree and Continue Button");
		} catch (Exception e) {
			Assert.fail("Failed to click on Agree and Continue Button");
		}
		
	}
	
}
