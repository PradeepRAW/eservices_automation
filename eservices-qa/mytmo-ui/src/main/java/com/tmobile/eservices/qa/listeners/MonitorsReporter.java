package com.tmobile.eservices.qa.listeners;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

public class MonitorsReporter implements IReporter {
	private static final Logger LOGGER = LoggerFactory.getLogger(MonitorsReporter.class);
	String testSuiteName = "";
	int totalTestCount = 0, totalPassCount = 0, totalFailCount = 0, totalSkipCount = 0;
	double passPercentage = 0, failPercentage = 0, skippedPercentage = 0;
	IResultMap testFailedResult = null, testSkippedResult = null, testPassedResult = null;
	ITestContext testContxt;
	Set<String> classSet = new HashSet<String>();
	StringBuffer pwtdBuf = new StringBuffer();
	StringBuffer svmBuf = new StringBuffer();

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		String reportTemplate = initReportTemplate();

		getTestSuiteSummary(suites);
		reportTemplate = reportTemplate.replace("$suiteName", String.format("%s", testSuiteName));
		reportTemplate = reportTemplate.replace("$totalTests", String.format("%s", totalTestCount));
		reportTemplate = reportTemplate.replace("$passCount", String.format("%s", totalPassCount));
		reportTemplate = reportTemplate.replace("$failCount", String.format("%s", totalFailCount));
		reportTemplate = reportTemplate.replace("$skipCount", String.format("%s", totalSkipCount));
		reportTemplate = reportTemplate.replace("$passRate", String.format("%s", passPercentage));
		reportTemplate = reportTemplate.replace("$failRate", String.format("%s", failPercentage));
		reportTemplate = reportTemplate.replace("$skipRate", String.format("%s", skippedPercentage));

		for (String className : getClassSet(suites)) {
			pwtdBuf.append(getServicewiseTestsDetails(className));
		}
		reportTemplate = reportTemplate.replaceFirst("<em></em>",
				String.format("%s<em></em>", pwtdBuf));
		
		for (String className : getClassSet(suites)) {
			svmBuf.append(getServicesMessages(className));
		}

		reportTemplate = reportTemplate.replaceFirst("<i></i>",
				String.format("%s<i></i>", svmBuf));

		saveReportTemplate(outputDirectory, reportTemplate);

	}

	private void getTestSuiteSummary(List<ISuite> suites) {
		try {
			for (ISuite tempSuite : suites) {
				testSuiteName = tempSuite.getName();

				Map<String, ISuiteResult> testResults = tempSuite.getResults();
				for (ISuiteResult result : testResults.values()) {

					testContxt = result.getTestContext();

					totalPassCount = testContxt.getPassedTests().getAllMethods().size();
					totalSkipCount = testContxt.getSkippedTests().getAllMethods().size();
					totalFailCount = testContxt.getFailedTests().getAllMethods().size();

					totalTestCount = totalPassCount + totalFailCount+totalSkipCount;

					passPercentage = totalPassCount * 100 / totalTestCount;
					failPercentage = totalFailCount * 100 / totalTestCount;
					skippedPercentage = totalSkipCount * 100 / totalTestCount;

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private Set<String> getClassSet(List<ISuite> suites) {
		try {
			for (ISuite tempSuite : suites) {
				testSuiteName = tempSuite.getName();

				Map<String, ISuiteResult> testResults = tempSuite.getResults();
				for (ISuiteResult result : testResults.values()) {

					ITestContext testContext = result.getTestContext();

					testFailedResult = testContext.getFailedTests();
					testPassedResult = testContext.getPassedTests();
					testSkippedResult = testContext.getSkippedTests();

					if (testFailedResult != null)
						for (ITestResult testResult : testFailedResult.getAllResults()) {
							String testClassName = "";
							testClassName = testResult.getTestClass().getName();
							classSet.add(testClassName);
						}
					if (testPassedResult != null)
						for (ITestResult testResult : testPassedResult.getAllResults()) {
							String testClassName = "";
							testClassName = testResult.getTestClass().getName();
							classSet.add(testClassName);
						}
					if (testSkippedResult != null)
						for (ITestResult testResult : testSkippedResult.getAllResults()) {
							String testClassName = "";
							testClassName = testResult.getTestClass().getName();
							classSet.add(testClassName);
						}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return classSet;
	}
	
	private String getServicewiseTestsDetails(String className) {
		int failedMethods = 0, passMethods = 0, skippedMethods = 0,totalMethods=0;

		if (testFailedResult != null)
			for (ITestResult classFailedName : testFailedResult.getAllResults())
				if (classFailedName.getTestClass().getName().equalsIgnoreCase(className))
					failedMethods++;

		if (testPassedResult != null)
			for (ITestResult classPassedName : testPassedResult.getAllResults())
				if (classPassedName.getTestClass().getName().equalsIgnoreCase(className))
					passMethods++;

		if (testSkippedResult != null)
			for (ITestResult classSkippedName : testSkippedResult.getAllResults())
				if (classSkippedName.getTestClass().getName().equalsIgnoreCase(className))
					skippedMethods++;
		
		totalMethods=skippedMethods+passMethods+failedMethods;
		className = className.substring(25, className.length());
		return "<tr><td>" + className + "</td><td style=\"text-align: center;\"><span style=\"color: #0000ff;\">" + totalMethods + "</span></td><td style=\"text-align: center;\"><span style=\"color: #339966;\">" + passMethods + "</span></td><td style=\"text-align: center;\"><span style=\"color: #ff0000;\">" + failedMethods + "</span></td><td style=\"text-align: center;\">"
				+ skippedMethods + "</td></tr>";
	}
	
	private String getServicesMessages(String className) {
		String serviceName = null, serviceMessage = null, serviceRequest = null, serviceResponse = null;
		StringBuffer smd = new StringBuffer();
		

		if (testFailedResult != null)
			for (ITestResult classFailedName : testFailedResult.getAllResults())
				if (classFailedName.getTestClass().getName().equalsIgnoreCase(className)) {
					serviceName = classFailedName.getMethod().getMethodName();
					Throwable throwable = classFailedName.getThrowable();
					serviceMessage = throwable.getMessage();
					serviceRequest = "<a href=\"\">request</a>";
					serviceResponse = "<a href=\"\">response</a>";
					String failFlow=className.substring(25, className.length());
					smd = smd.append("<tr><th style=\"text-align: center;\">" + failFlow
							+ "</th><td style=\"text-align: center;\">" + serviceName
							+ "</td><td style=\"text-align: center;\">" + serviceRequest
							+ "</td><td style=\"text-align: center;\">" + serviceResponse
							+ "</td><td style=\"text-align: center;\">" + serviceMessage + "</td></tr>");
				}

		if (testPassedResult != null)
			for (ITestResult classPassedName : testPassedResult.getAllResults())
				if (classPassedName.getTestClass().getName().equalsIgnoreCase(className)) {
					serviceName = classPassedName.getMethod().getMethodName();
					serviceMessage = "SUCCESS";
					serviceRequest = "<a href=\"\">request</a>";
					serviceResponse = "<a href=\"\">response</a>";
					String passFlow=className.substring(25, className.length());
					smd = smd.append("<tr><th style=\"text-align: center;\">" + passFlow
							+ "</th><td style=\"text-align: center;\">" + serviceName
							+ "</td><td style=\"text-align: center;\">" + serviceRequest
							+ "</td><td style=\"text-align: center;\">" + serviceResponse
							+ "</td><td style=\"text-align: center;\">" + serviceMessage + "</td></tr>");
				}

		if (testSkippedResult != null)
			for (ITestResult classSkippedName : testSkippedResult.getAllResults())
				if (classSkippedName.getTestClass().getName().equalsIgnoreCase(className)) {
					serviceName = classSkippedName.getMethod().getMethodName();
					Throwable throwable = classSkippedName.getThrowable();
					serviceMessage = throwable.getMessage();
					serviceRequest = "<a href=\"\">request</a>";
					serviceResponse = "<a href=\"\">response</a>";
					String skipFlow=className.substring(25, className.length());
					smd = smd.append("<tr><th style=\"text-align: center;\">" + skipFlow
							+ "</th><td style=\"text-align: center;\">" + serviceName
							+ "</td><td style=\"text-align: center;\">" + serviceRequest
							+ "</td><td style=\"text-align: center;\">" + serviceResponse
							+ "</td><td style=\"text-align: center;\">" + serviceMessage + "</td></tr>");
				}
		return smd.toString();
	}

	@SuppressWarnings("unused")
	private String getFailedTestDetails() {
		String erroredTestName = null, errorMessage = null, failedClass = null;
		StringBuffer ftd = new StringBuffer();
		try {
			if (testFailedResult != null)
				for (ITestResult classFailedName : testFailedResult.getAllResults()) {
					failedClass = classFailedName.getTestClass().getName();
					erroredTestName = classFailedName.getMethod().getMethodName();
					if (ITestResult.FAILURE == classFailedName.getStatus()) {
						Throwable throwable = classFailedName.getThrowable();
						String originalMessage = throwable.getMessage();
						errorMessage = originalMessage;
					}
					failedClass = failedClass.substring(25, failedClass.length());
					ftd.append("<tr><td>" + failedClass + "</td><td>" + erroredTestName + "</td><td>" + errorMessage
							+ "</td></tr>");
				}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ftd.toString();
	}

	private String initReportTemplate() {
		String template = null;
		byte[] reportTemplate;
		try {
			reportTemplate = Files.readAllBytes(Paths.get("src/test/resources/apiMonitorReporter.html"));
			template = new String(reportTemplate, "UTF-8");
		} catch (IOException e) {
			LOGGER.error("Problem initializing template", e);
		}
		return template;
	}

	private void saveReportTemplate(String outputDirectory, String reportTemplate) {
		new File(outputDirectory).mkdirs();
		try {
			PrintWriter reportWriter = new PrintWriter(
					new BufferedWriter(new FileWriter(new File(outputDirectory, "apiMonitorReport.html"))));
			reportWriter.println(reportTemplate);
			reportWriter.flush();
			reportWriter.close();
		} catch (IOException e) {
			LOGGER.error("Problem saving template", e);
		}
	}

}
